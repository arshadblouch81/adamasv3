using System;
using System.Text;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Dto;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.Function;
using Microsoft.Extensions.Configuration;
using Staff = Adamas.Models.Modules.Staff;

using MimeKit;
using MimeKit.Text;
using Newtonsoft.Json.Linq;

using AdamasV3.Models.Dto;
using AdamasV3.DAL;

using System.IO;

namespace Adamas.Controllers
{
    //[Authorize]
    // [Authorize(Policy = "markpolicy")]
    [Route("api/[controller]")]
    [Authorize]
    [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")] 
    public class ClientController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private readonly IEmailConfiguration _emailConfiguration;
        private IEmailService _emailService;
        private IClientService _clientService;
        private Adamas.DAL.IEmailService _newEmailService;
        private ILoginService _login;
        public ClientController(
            DatabaseContext context,
            IConfiguration config,
            IGeneralSetting setting,
            IEmailConfiguration emailConfiguration,
            Adamas.DAL.IEmailService newEmailService,
            IClientService clientService,
            ILoginService login,
            IEmailService emailService)
        {
            _context = context;
            GenSettings = setting;
            _emailConfiguration = emailConfiguration;
            _emailService = emailService;
            _newEmailService = newEmailService;
            _clientService = clientService;
            _login = login;
        }

        [HttpPut("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDto changePass)
        {
            var currentUser = await this._login.GetCurrentUser();

            if (String.Compare(changePass.Password, changePass.PasswordConfirmation) != 0)
            {
                throw new Exception("Password doesn't match");
            }

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"UPDATE UserInfo SET Password = @Password, StaffCode = @StaffCode WHERE Recnum = @Recnum", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@Password", PasswordProcess.Encrypt(changePass.Password));
                        cmd.Parameters.AddWithValue("@Recnum", currentUser.Recnum);
                        cmd.Parameters.AddWithValue("@StaffCode", changePass.NewAccountNo);

                        await conn.OpenAsync();

                        return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpGet, Route("usergroups/{id}")]
        public async Task<IActionResult> GetUserGroups(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, isnull(Name,''), isnull(MobileAlert, 0), isnull(Notes,'') FROM HumanResources WHERE PersonID = @id ", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<object> list = new List<object>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (rd.Read())
                    {
                        list.Add(
                            new
                            {
                                RecordNumber = rd.GetInt32(0),
                                Name = rd.GetString(1),
                                MobileAlert = rd.GetBoolean(2),
                                Notes = rd.GetString(3),
                            }
                        );
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("userinfo-name/{code}")]
        public async Task<IActionResult> GetUserInfoName(string code)
        {
            var userInfo = await (from r in _context.Recipients 
                                    join uinfo in _context.UserInfo on r.AccountNo equals uinfo.StaffCode where r.AccountNo == code select new UserInfo
                                    {
                                        Name = uinfo.Name
                                    }).FirstOrDefaultAsync();

            if (userInfo == null)
                return NotFound();

            return Ok(userInfo);
        }

        [HttpGet, Route("profile")]
        public async Task<IActionResult> GetRecipients(string code)
        {
            var recipients = await _context.Recipients.Where(x => x.AccountNo == code).FirstOrDefaultAsync();

            if (recipients == null)
                return NotFound();

            return Ok(recipients);
        }

        [HttpGet, Route("cald/gender/{id}")]
        public async Task<IActionResult> GetCaldGender(string id)
        {
            var oni = await _context.Oni.Where(x => x.PersonID == id).FirstOrDefaultAsync();

            if (oni == null)
                return NotFound();

            dynamic d = new {Id = id , CAL_IdentifiesAs = oni.CAL_IdentifiesAs};

            return Ok(d);
        }

          [HttpPost, Route("cald/gender")]
        public async Task<IActionResult> postCaldGender([FromBody] Cald_Gender data)
        {
            using (var db = _context)
            {
               var oni = await _context.Oni.Where(x => x.PersonID == data.Id).FirstOrDefaultAsync();

              if (oni == null)
                return NotFound();

                oni.CAL_IdentifiesAs = data.Gender;
                await db.SaveChangesAsync();
                return Ok(true);
            }
           

        }


        [HttpGet, Route("address")]
        public async Task<IActionResult> GetAddress(string id)
        {
            
            var address = await _context.NamesAndAddresses
            .Where(x => x.PersonID == id)
            .OrderByDescending(x => x.PrimaryAddress)
            .ThenBy(x => x.Type) // Ensure "Type" exists as a property or adjust accordingly
            .ToListAsync();

    if (address == null || !address.Any())
        return NotFound();


            return Ok(address);
        }

        [HttpGet, Route("agencydefinedgroup/{accountNo}")]
        public async Task<IActionResult> GetAgencyDefinedGroup(string accountNo)
        {
            var agencydefinedgroup = await _context.Recipients.Where(x => x.AccountNo == accountNo)
                                                .Select(x => x.AgencyDefinedGroup).FirstOrDefaultAsync();

            if (agencydefinedgroup == null)
                return NotFound();

            return Ok(new {
                data = agencydefinedgroup
            });
        }

        [HttpGet, Route("address/top")]
        public async Task<IActionResult> GetTopAddress(string idList)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();
                using (SqlCommand cmd = new SqlCommand())
                {
                    int count = 0;
                    var programParam = new List<string>();

                    var splitId = idList.Split(",");

                    foreach (var id in splitId)
                    {
                        var param = "@recordno" + count.ToString();
                        cmd.Parameters.AddWithValue(param, id);
                        programParam.Add(param);
                        count++;
                    }
                    cmd.CommandText = String.Format(@"SELECT RecordNumber,[PersonID], [Description], Address1, Address2, Suburb, PostCode, [Type], [STAT] FROM
                        (
                        SELECT *, ROW_NUMBER() OVER (PARTITION BY PersonID ORDER BY PersonID desc) rn
                            FROM NamesAndAddresses
                        ) v
                        WHERE rn = 1 AND PersonId IN ({0})
                        ORDER BY PersonID",
                    String.Join(",", programParam));

                    cmd.Connection = conn;

                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                PersonID = GenSettings.Filter(rd["PersonID"]),
                                Description = GenSettings.Filter(rd["Description"]),
                                Address1 = GenSettings.Filter(rd["Address1"]),
                                Address2 = GenSettings.Filter(rd["Address2"]),
                                Suburb = GenSettings.Filter(rd["Suburb"]),
                                PostCode = GenSettings.Filter(rd["PostCode"]),
                                Type = GenSettings.Filter(rd["Type"]),
                                State = GenSettings.Filter(rd["STAT"])
                            });
                        }
                    }
                    return Ok(list);
                }

            }

            // using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            // {
            //     using (SqlCommand cmd = new SqlCommand(@"SELECT TOP 1 RecordNumber, Description, Address1, Address2, Suburb, PostCode, Type FROM NamesAndAddresses WHERE PersonID = @id", (SqlConnection)conn))
            //     {
            //         cmd.Parameters.AddWithValue("@id", id);
            //         await conn.OpenAsync();

            //         List<PrimaryAddress> listad = new List<PrimaryAddress>();
            //         SqlDataReader dr = await cmd.ExecuteReaderAsync();

            //         while (dr.Read())
            //         {
            //             PrimaryAddress ad = new PrimaryAddress();
            //             ad.RecordNo = dr["RecordNumber"].ToString();
            //             ad.Address1 = dr["Address1"].ToString();
            //             ad.Address2 = dr["Address2"].ToString();
            //             ad.Description = dr["Description"].ToString();
            //             ad.Suburb = dr["Suburb"].ToString();
            //             ad.Postcode = dr["PostCode"].ToString();
            //             listad.Add(ad);
            //         }
            //         return Ok(listad);
            //     }
            // }
        }

        [HttpGet("address/primary/name/{name}")]
        public async Task<IActionResult> GetPrimaryAddressName(string name)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT NA.RecordNumber, NA.Description, NA.Address1, NA.Address2, NA.Suburb, NA.PostCode, NA.Type FROM NamesAndAddresses NA
                    INNER JOIN Recipients R ON R.UniqueID =  NA.PersonID WHERE R.AccountNo = @name AND isnull(primaryaddress, 0) = 1", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@name", name);
                    await conn.OpenAsync();

                    List<PrimaryAddress> list = new List<PrimaryAddress>();
                    SqlDataReader dr = await cmd.ExecuteReaderAsync();
                    while (dr.Read())
                    {
                        PrimaryAddress add = new PrimaryAddress();
                        add.RecordNo = dr["RecordNumber"].ToString();
                        add.Address1 = dr["Address1"].ToString();
                        add.Address2 = dr["Address2"].ToString();
                        add.Description = dr["Description"].ToString();
                        add.Suburb = dr["Suburb"].ToString();
                        add.Postcode = dr["PostCode"].ToString();
                        list.Add(add);
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet, Route("address/primary/{id}")]
        public async Task<IActionResult> GetPrimaryAddress(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, Description, Address1, Address2, Suburb, PostCode, Type FROM NamesAndAddresses WHERE PersonID = @id AND isnull(primaryaddress, 0) = 1", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<PrimaryAddress> list = new List<PrimaryAddress>();
                    SqlDataReader dr = await cmd.ExecuteReaderAsync();
                    while (dr.Read())
                    {
                        PrimaryAddress add = new PrimaryAddress();
                        add.RecordNo = dr["RecordNumber"].ToString();
                        add.Address1 = dr["Address1"].ToString();
                        add.Address2 = dr["Address2"].ToString();
                        add.Description = dr["Description"].ToString();
                        add.Suburb = dr["Suburb"].ToString();
                        add.Postcode = dr["PostCode"].ToString();
                        list.Add(add);
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet, Route("address/usual/{id}")]
        public async Task<IActionResult> GetUsualAddress(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT * FROM NamesAndAddresses WHERE PersonID = @id AND (Description = 'USUAL' OR Description = '<USUAL>')", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();

                    List<PrimaryAddress> listad = new List<PrimaryAddress>();
                    SqlDataReader dr = await cmd.ExecuteReaderAsync();
                    while (dr.Read())
                    {
                        PrimaryAddress ad = new PrimaryAddress();
                        ad.RecordNo = dr["RecordNumber"].ToString();
                        ad.Address1 = dr["Address1"].ToString();
                        ad.Address2 = dr["Address2"].ToString();
                        ad.Description = dr["Description"].ToString();
                        ad.Suburb = dr["Suburb"].ToString();
                        ad.Postcode = dr["PostCode"].ToString();
                        listad.Add(ad);
                    }
                    return Ok(listad);
                }
            }
        }

        [HttpGet, Route("contact")]
        public async Task<IActionResult> GetContacts(string id)
        {
            var contacts = await _context.PhoneFaxOther.Where(x => x.PersonID == id)
                                                        .OrderByDescending(x => x.PrimaryPhone)
                                                        .ThenBy(x => x.Type) // Ensure "Type" exists as a property or adjust accordingly
                                                        .ToListAsync();
            if (contacts == null)
                return NotFound();

            return Ok(contacts);
        }

        [HttpGet, Route("roster/master")]
        public async Task<IActionResult> GetMasterRoster(WorkerInput ccode)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT  [Recordno] As [RecordNumber],[Client Code] As [Recipient],[Date] As [Date], [Start Time] As [Start Time], (([Duration] * 5)/60) As [Duration],[Service Type] As [Activity],[Carer Code] As [Staff], [Anal] As [Analysis],
                        [Program] As [Program],[HACCType] As [Dataset Type], ([CostQty] * [Unit Pay Rate]) As [Service Cost] FROM Roster WHERE [Client Code] = @ccode AND [Date] BETWEEN '1900/01/01' AND '1905/05/28' ORDER BY Date, [Start Time]", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@ccode", ccode.ClientCode);

                        await conn.OpenAsync();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<Dictionary<string, object>> dicList = new List<Dictionary<string, object>>();
                        int numCol = rd.FieldCount;

                        while (await rd.ReadAsync())
                        {
                            Dictionary<string, object> dic = new Dictionary<string, object>();
                            for (var a = 0; a < numCol; a++)
                            {
                                dic.Add(rd.GetName(a).Replace(" ", ""), GenSettings.Filter(rd.GetValue(a)));
                            }
                            dicList.Add(dic);
                        }

                        var uniqueDates = dicList.Select(x => x["Date"]).Distinct().ToList();


                        List<KeyValuePair<string, object>> finalList = new List<KeyValuePair<string, object>>();
                        foreach (string date in uniqueDates)
                        {
                            finalList.Add(new KeyValuePair<string, object>(date.Trim(), dicList.Where(x => x["Date"].ToString() == date).Select(x => x)));
                        }
                        return Ok(finalList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("roster/worker")]
        public async Task<IActionResult> GetWorkerRoster(WorkerInput ccode)
        {
            try
            {
                List<object> list = new List<object>();
                if (ccode.ClientCode == null || ccode.EndDate == null)
                {
                    return NoContent();
                }
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT  [Recordno] As [RecordNumber],[Client Code] As [Recipient],[Date], [Start Time] As [Start Time],
                        (([Duration] * 5)/60) As [Duration],[Service Type] As [Activity],[Carer Code] As [Staff], [Anal] As [Analysis], [Program] As [Program],[HACCType] As [Dataset Type], ([CostQty] * [Unit Pay Rate]) As [Service Cost]
                        FROM Roster WHERE [Client Code] = @ccode AND [Date] BETWEEN @sDate AND @eDate ORDER BY Date, [Start Time]", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@ccode", ccode.ClientCode);
                        cmd.Parameters.AddWithValue("@sDate", ccode.StartDate);
                        cmd.Parameters.AddWithValue("@eDate", ccode.EndDate);
                        conn.Open();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                Recipient = GenSettings.Filter(rd["Recipient"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["Start Time"]),
                                Duration = GenSettings.Filter(rd["Duration"]),
                                Activity = GenSettings.Filter(rd["Activity"]),
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Analysis = GenSettings.Filter(rd["Analysis"]),
                                Program = (rd["Program"]),
                                DataSetType = GenSettings.Filter(rd["Dataset Type"]),
                                ServiceCost = GenSettings.Filter(rd["Service Cost"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("note/opnote/{id}")]
        public async Task<IActionResult> GetOpNote(string id)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT  RecordNumber, DetailDate, Detail, Creator FROM History WHERE PersonID = @uid AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr'))
                        AND ExtraDetail1 = 'OPNOTE' AND DeletedRecord <> 1 ORDER BY DetailDate DESC, RecordNumber DESC", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        conn.Open();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<dynamic> list = new List<dynamic>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                RecordNumber = rd.GetInt32(0),
                                DetailDate = rd.IsDBNull(1) ? "" : Convert.ToDateTime(rd.GetDateTime(1)).ToString("MMMM dd, yyyy"),
                                DetailTime = rd.IsDBNull(1) ? "" : Convert.ToDateTime(rd.GetDateTime(1)).ToString("H:mm"),
                                Detail = rd.GetString(2),
                                Creator = rd.GetString(3)
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("casemanager")]
        public async Task<IActionResult> GetCaseManager(string id)
        {

            return Ok();
        }


        [HttpGet, Route("note/caseprogress/{id}")]
        public async Task<IActionResult> GetCaseProgressNote(string id)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, DetailDate, Detail, Creator FROM History WHERE PersonID = @uid                                                                AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr'))
                        AND ExtraDetail1 = 'CASENOTE' AND DeletedRecord <> 1 ORDER BY DetailDate DESC, RecordNumber DESC", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        conn.Open();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<dynamic> list = new List<dynamic>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                RecordNumber = rd.GetInt32(0),
                                DetailDate = rd.IsDBNull(1) ? "" : Convert.ToDateTime(rd.GetDateTime(1)).ToString("MMMM dd, yyyy"),
                                DetailTime = rd.IsDBNull(1) ? "" : Convert.ToDateTime(rd.GetDateTime(1)).ToString("H:mm"),
                                Detail = rd.GetString(2),
                                Creator = rd.GetString(3)
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("careplan/current/{id}")]
        public async Task<IActionResult> GetCurrentCarePlan(string id)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT  Recnum, '' AS NoteWidth, PersonID,  PlanNumber, PlanName,  PlanCreator,  PlanStartDate, PlanEndDate, PlanCoPayAmount,
                        dbo.RTF2Text(PlanDetail) AS PlanDetail, PlanReminderDate, PlanReminderText FROM CarePlanItem WHERE PersonID = @uid AND DeletedRecord <> 1 ORDER BY PlanStartDate DESC, PlanNumber ASC", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        conn.Open();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<object> list = new List<object>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                PlanNumber = GenSettings.Filter(rd["PlanNumber"]),
                                PlanStartDate = GenSettings.Filter(rd["PlanStartDate"]),
                                PlanEndDate = GenSettings.Filter(rd["PlanEndDate"]),
                                PlanCoPayAmount = rd.IsDBNull(8) ? "" : rd["PlanCoPayAmount"],
                                PlanName = rd["PlanName"],
                                PlanCreator = rd["PlanCreator"],
                                PlanDetail = rd["PlanDetail"]
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("services/approved/{id}")]
        public async Task<IActionResult> GetApprovedServices(string id)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT [Service Type], [ServiceProgram] FROM ServiceOverview Where PersonID = @uid", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        conn.Open();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<object> list = new List<object>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                ServiceType = rd.GetString(0),
                                ServiceProgram = rd.GetString(1)
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("programs/approved/{id}")]
        public async Task<IActionResult> GetApprovedPrograms(string id)
        {
            try
            {
                using (var conn = _context.GetConnection())
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT hrt.[Type], rp.[Program], rp.[ProgramStatus], hrt.[Address2] FROM RecipientPrograms rp LEFT JOIN HumanResourceTypes hrt ON rp.[Program] = hrt.[Name] 
                        WHERE PersonID = @uid AND hrt.[Type] IS NOT NULL", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        conn.Open();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<object> list = new List<object>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                Type = rd.GetString(0),
                                Program = rd.GetString(1),
                                Status = rd.GetString(2),
                                Coordinator = GenSettings.Filter(rd["Address2"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("alerts/{id}")]
        public async Task<IActionResult> GetAlerts(string id)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT [Notes],[ContactIssues],[SpecialConsiderations] FROM Recipients WHERE UniqueID = @uid", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        await conn.OpenAsync();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        dynamic list = null;

                        while (rd.Read())
                        {
                            list = new
                            {
                                Notes = rd["Notes"] ?? "",
                                ContactIssues = rd["ContactIssues"] ?? "",
                                SpecialConsiderations = rd["SpecialConsiderations"] ?? ""
                            };
                        };
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("servicetasklist/{id}")]
        public async Task<IActionResult> GetServiceTaskList(string id)
        {
            try
            {
                using (var conn = _context.GetConnection())
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT [Service Type], [Activity Breakdown] FROM ServiceOverview WHERE Personid = @uid AND ServiceStatus = 'ACTIVE'", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        await conn.OpenAsync();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<object> list = new List<object>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                ServiceType = rd.GetString(0),
                                Breakdown = rd.IsDBNull(1) ? "" : rd.GetString(1)
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("contact/relative/{id}")]
        public async Task<IActionResult> GetRelativeContacts(string id)
        {
            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT * FROM HumanResources WHERE [Group] = 'CONTACT' AND PersonID = @uid", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@uid", id);
                        conn.Open();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        List<object> list = new List<object>();
                        while (rd.Read())
                        {
                            list.Add(new
                            {
                                RecordNumber = rd.GetInt32(0),
                                PersonId = rd.GetString(1),
                                Type = rd["Type"].ToString(),
                                Name = rd["Name"].ToString(),
                                Address = rd["Address1"].ToString(),
                                Address2 = rd["Address2"].ToString(),
                                Suburb = rd["Suburb"].ToString(),
                                Postcode = rd["Postcode"].ToString(),
                                Phone1 = rd["Phone1"].ToString(),
                                Fax = rd["Fax"].ToString(),
                                Mobile = rd["Mobile"].ToString(),
                                Email = rd["Email"].ToString()
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("address/type")]
        public async Task<IActionResult> GetAddressType()
        {
            var types = await _context.DataDomains.Where(x => x.Domain == "ADDRESSTYPE")
                                                    .Select(x => new DataDomains
                                                    {
                                                        Description = x.Description
                                                    }).ToListAsync();
            if (types == null)
                return NotFound();

            return Ok(types);
        }

        [HttpGet, Route("contact/type")]
        public async Task<IActionResult> GetContactType()
        {
            var types = await _context.DataDomains.Where(x => x.Domain == "CONTACTTYPE")
                                                    .Select(x => new DataDomains
                                                    {
                                                        Description = x.Description
                                                    }).ToListAsync();
            if (types == null)
                return NotFound();

            return Ok(types);
        }

        [HttpGet, Route("program/active")]
        public async Task<IActionResult> GetActivePrograms(ProgramActive program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string active = program.IsActive ? " AND ProgramStatus = 'ACTIVE' " : " ";
                using (SqlCommand cmd = new SqlCommand(@"SELECT Program FROM RecipientPrograms 
                                                        INNER JOIN Recipients on personid = uniqueid 
                                                        WHERE Program IS NOT NULL AND accountNo = @code" + active + "ORDER BY Program", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@code", program.Code);
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    List<string> list = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(rd.GetString(0));
                    }

                    return Ok(new
                    {
                        success = true,
                        data = list
                    });
                }
            }
        }

        [HttpGet, Route("package")]
        public async Task<IActionResult> GetPackage(GetPackage getpackage)
        {   
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@" SELECT
                      [client code] + '-' + [program] AS StatementID,
                      CASE
                        WHEN itemgroup = 'DIRECT SERVICE' THEN 4
                        WHEN itemgroup = 'CASE MANAGEMENT' THEN 6
                        WHEN itemgroup = 'GOODS/EQUIPMEN T' THEN 5
                        WHEN itemgroup = 'PACKAGE ADMIN' THEN 7
                        ELSE 4
                      END AS iSort,
                      [carer code],
                      [type],
                      [date],
                      [start time],
                      [service type],
                      'ADMIN' AS [Service Description],
                      [billunit],
                      [duration],
                      SUM([billqty]) AS BillQty,
                      [unit bill rate],
                      [taxpercent],
                      [taxamount],
                      [billto],
                      [client code],
                      [servicesetting],
                      [program],
                      [billdesc],
                      notes,
                      itemgroup,
                      [itminorgroup],
                      [billtext],
                      SUM(servicecharge) AS ServiceCharge,
                      gstamount,
                      SUM([amtinctax]) AS AmtIncTax,
                      CONVERT(nvarchar, [invoicenumber]) AS InvoiceNumber,
                      branch,
                      accountingidentifier
                    FROM (SELECT
                      CASE
                        WHEN ISNULL(billdesc1, '') = '' THEN [billtext]
                        ELSE billdesc1
                      END AS BillDesc,
                      *
                    FROM (SELECT
                      '!INTERNAL' AS [Carer Code],
                      r.[type],
                      @lastDate AS Date,
                      '00:00' AS [Start Time],
                      r.[service type],
                      'ADMIN' AS [Service Description],
                      r.[billunit],
                      r.[duration],
                      r.[billqty],
                      r.[unit bill rate],
                      r.[taxpercent],
                      r.[taxamount],
                      r.[billto] AS [BillTo],
                      r.[client code],
                      '' AS [ServiceSetting],
                      r.[program],
                      r.[billdesc] AS BillDesc1,
                      '' AS Notes,
                      it.[maingroup] AS ItemGroup,
                      it.[minorgroup] AS itMinorGroup,
                      it.[billtext],
                      ROUND(r.[billqty] * r.[unit bill rate], 2) AS ServiceCharge,
                      ROUND((r.[taxpercent] / 100) * ROUND(r.[billqty] * r.[unit bill rate], 2), 2) AS GSTAmount,
                      ROUND(r.[billqty] * r.[unit bill rate], 2) + ROUND((r.[taxpercent] / 100) * ROUND(r.[billqty] * r.[unit bill rate], 2), 2) AS AMTIncTax,
                      '0' AS [InvoiceNumber],
                      re.branch,
                      re.accountingidentifier
                    FROM roster r
                    LEFT JOIN itemtypes it
                      ON [service type] = [title]
                    INNER JOIN recipients re
                      ON [client code] = [accountno]
                    WHERE r.[client code] IN ( @clientcode )
                    AND r.[program] IN( @programcode )
                    AND(r.[date] BETWEEN @firstDate AND @lastDate)
                    AND ISNULL(r.[unit bill rate], 0) > 0
                    AND it.minorgroup = 'FEE'
                    AND ISNULL(excludefromusagestatements, 0) = 0
                    AND NOT(r.[type] = 9
                    AND r.[service description] = 'CONTRIBUTION')) t) T1
                    GROUP BY[carer code],
                             [type],
                             [date],
                             [start time],
                             [service type],
                             [service description],
                             [billunit],
                             [duration],
                             billqty,
                             [unit bill rate],
                             [taxpercent],
                             [taxamount],
                             [billto],
                             [client code],
                             [branch],
                             [accountingidentifier],
                             [servicesetting],
                             [program],
                             [billdesc],
                             notes,
                             itemgroup,
                             [itminorgroup],
                             [billtext],
                             servicecharge,
                             gstamount,
                             amtinctax,
                             invoicenumber
                    UNION
                    SELECT
                      r.[client code] + '-' + r.[program] AS StatementID,
                      CASE
                        WHEN it.[maingroup] = 'DIRECT SERVICE' THEN 4
                        WHEN it.[maingroup] = 'CASE MANAGEMENT' THEN 6
                        WHEN it.[maingroup] = 'GOODS/EQUIPMENT' THEN 5
                        ELSE 4
                      END AS iSort,
                      r.[carer code],
                      r.[type],
                      r.[date],
                      r.[start time],
                      r.[service type],
                      'ADMIN' AS[Service Description],
                      r.[billunit],
                      r.[duration],
                      r.[billqty],
                      r.[unit bill rate],
                      r.[taxpercent],
                      r.[taxamount],
                      r.[billto],
                      r.[client code],
                      r.[servicesetting],
                      r.[program],
                      r.[billdesc],
                      '' AS Notes,
                      it.[maingroup] AS ItemGroup,
                      it.[minorgroup],
                      it.[billtext],
                      ROUND(CONVERT(numeric(10, 4), [billqty] *[unit bill rate]), 2) AS ServiceCharge,
                     ROUND(CONVERT(numeric(10, 4), (r.[taxpercent] / 100) * [billqty] * [unit bill rate]), 2) AS GSTAmount,
                     ROUND(CONVERT(numeric(10, 4), (r.[billqty] * r.[unit bill rate])), 2) + ROUND(CONVERT(numeric(10, 4), r.[taxpercent] / 100 * r.[billqty] * r.[unit bill rate]), 2) AS AMTIncTax,
                     CONVERT(nvarchar, r.[invoicenumber]) AS InvoiceNumber,
                     re.branch,
                      re.accountingidentifier
                    FROM roster r
                    LEFT JOIN itemtypes it
                      ON[service type] = [title]
                            INNER JOIN recipients re
                      ON[client code] = [accountno]
                            WHERE r.[client code] IN ( @clientcode )
                    AND r.[program] IN ( @programcode )
                    AND NOT(r.[type] = 9
                    AND r.[service description] = 'CONTRIBUTION')
                    AND (r.[date] BETWEEN @firstDate AND @lastDate)
                    AND ISNULL(r.[unit bill rate], 0) > 0
                    AND it.minorgroup<> 'FEE'
                    AND ISNULL(it.excludefromusagestatements, 0) = 0
                    UNION
                    SELECT
                      [patient code] + '-' + [package]
                            AS StatementID,
                      CASE
                        WHEN htype IN('R') AND
                         type1 = 'GOVMT' THEN 2
                        WHEN htype IN('R') AND
                         type1 = 'PRSNL' THEN 3
                        WHEN htype IN('R') AND
                         type1 = 'OTHER' THEN 1
                        WHEN htype IN('R', 'A') AND
                         ISNULL(type1, '') = '' THEN 0
                      END AS iSort,
                      '' AS[Carer Code],
                      100 AS[Type],
                      [traccs processing date]
                            AS[Date],
                      '' AS[Start Time],
                      '' AS[Service Type],
                      '' AS[Service Description],
                      '' AS[Bill Unit],
                      1 AS[Duration],
                      1 AS[BillQty],
                      [invoice amount]
                            AS Amount,
                      0 AS[Taxpercent],
                      0 AS[Taxamount],
                      [client code]
                            AS[BillTo],
                      [patient code],
                      '' AS[ServiceSetting],
                      package AS[Program],
                      '' AS[tmpBilLDesc],
                      '' AS[tmpNotes],
                      CASE
                        WHEN htype = 'R' AND
                          type1 = 'GOVMT' THEN 'PAYMENT - Govt Contribution'
                        WHEN htype = 'R' AND
                          type1 = 'PRSNL' THEN 'PAYMENT - Personal Contribution'
                        WHEN htype = 'R' AND
                          type1 = 'OTHER' THEN 'PAYMENT - 3rd Party Contribution'
                        WHEN htype IN('R', 'A') AND
                         ISNULL(type1, '') = '' THEN 'ADJUSTMENT'
                      END + ' - ' + ISNULL(invoiceheader.notes, '') AS ItemGroup,
                      htype AS[itMinorGroup],
                      CASE
                        WHEN htype = 'R' AND
                          type1 = 'GOVMT' THEN 'PAYMENT - Federal Contribution'
                        WHEN htype = 'R' AND
                          type1 = 'PRSNL' THEN 'PAYMENT - Personal Contribution'
                        WHEN htype = 'R' AND
                          type1 = 'OTHER' THEN 'PAYMENT - 3rd Party Contribution'
                        WHEN htype IN('R', 'A') AND
                         ISNULL(type1, '') = '' THEN 'ADJUSTMENT'
                      END AS StText,
                      CONVERT(float, -1 * [invoice amount]) AS ServiceCharge,
                      CAST(0 AS float) AS GSTAmount,
                      CONVERT(float, -1 * [invoice amount]) AS AMTIncTax,
                      CONVERT(nvarchar, [invoice number]) AS[Invoice Number],
                      re.branch,
                      re.accountingidentifier
                    FROM invoiceheader
                    INNER JOIN recipients re
                      ON[patient code] = accountno
                    WHERE[patient code] IN( @clientcode )
                    AND htype IN('R', 'C', 'A')
                    AND package IN( @programcode )
                    AND[traccs processing date] BETWEEN @firstDate AND @lastDate
                    ORDER BY[client code] + '-' + [program], [isort], date", (SqlConnection)conn))
                {
                    DateTime time = DateTime.Parse(getpackage.Date);
                    var firstDayOfMonth = new DateTime(time.Year, time.Month, 1);
                    var firstDayString = firstDayOfMonth.ToString("yyyy/MM/dd");
                    var lastDayString = firstDayOfMonth.AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");

                    cmd.Parameters.AddWithValue("@clientcode", getpackage.Code);
                    cmd.Parameters.AddWithValue("@firstDate", firstDayString);
                    cmd.Parameters.AddWithValue("@lastDate", lastDayString);
                    cmd.Parameters.AddWithValue("@programcode", getpackage.PCode);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Package> list = new List<Package>();

                    while (await rd.ReadAsync())
                    {
                        Package package = new Package();
                        package.StatementId = rd["StatementID"] as string;
                        package.ServiceType = rd["service type"] as string;
                        package.ServiceCharge = rd["ServiceCharge"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(rd["ServiceCharge"]);
                        package.ItemGroup = rd["itemgroup"] as string;
                        package.UnitBillRate = Convert.ToDecimal(rd["unit bill rate"]);
                        package.BillQuant = rd["BillQty"] == DBNull.Value ? Convert.ToDecimal(0.00) : Convert.ToDecimal(rd["BillQty"]);
                        package.Date = Convert.ToDateTime(rd["date"]).ToString("dd/MM/yyyy");
                        package.BillUnit = rd["billunit"] as string;

                        if (rd["billtext"] == null || rd["billtext"].ToString() == "")
                            package.BillText = rd["Service Type"] as string;
                        else
                            package.BillText = rd["billtext"] as string;

                        package.ClientCode = rd["client code"] as string;
                        list.Add(package);
                    }

                    List<dynamic> listDyn = new List<dynamic>();
                    var uniqueItemGroupList = list.Select(x => x.ItemGroup).Distinct();

                    foreach (string item in uniqueItemGroupList)
                    {
                        var package = list.Where(x => x.ItemGroup == item).Select(x => x);
                        var total = package.Select(x => x.ServiceCharge).Sum();
                        listDyn.Add(new
                        {
                            name = item,
                            list = package,
                            total = total
                        });
                    }

                    return Ok(new { list = listDyn });
                }
            }
        }

        [HttpGet, Route("balances")]
        public async Task<IActionResult> GetBalances(GetPackage package)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Top 2 Date AS PeriodEnd, Balance, BankedContingency FROM PackageBalances PB 
                    INNER JOIN Recipients R ON PB.PersonID = R.SQLID INNER JOIN HumanResourceTypes PR ON PB.ProgramID = PR.RecordNumber 
                    WHERE R.AccountNo = @clientcode AND PR.Name = @programcode AND PR.[GROUP] = 'PROGRAMS' 
                    AND Convert(nVarChar, PB.Date, 111) < = @lastDate ORDER BY Date DESC", (SqlConnection)conn))
                {
                    DateTime time = DateTime.Parse(package.Date);
                    var lastDayString = DateTime.DaysInMonth(time.Year, time.Month).ToString("yyyy/MM/dd");

                    var firstDayOfMonth = new DateTime(time.Year, time.Month, 1);
                    var lastDsayString = firstDayOfMonth.AddMonths(1).AddDays(-1).ToString("yyyy/MM/dd");

                    cmd.Parameters.AddWithValue("@clientcode", package.Code);
                    cmd.Parameters.AddWithValue("@programcode", package.PCode);
                    cmd.Parameters.AddWithValue("@lastDate", lastDsayString);

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Balances> list = new List<Balances>();

                    while (await rd.ReadAsync())
                    {
                        Balances balance = new Balances();
                        balance.PeriodEnd = Convert.ToDateTime(rd["PeriodEnd"]);
                        balance.Balance = Convert.ToDecimal(rd["Balance"]);
                        balance.BankedContingency = rd["BankedContingency"] == null ? null : Convert.ToDecimal(rd["BankedContingency"]);
                        list.Add(balance);
                    }

                    return Ok(new
                    {
                        success = true,
                        list = list
                    });
                }
            }
        }

        [HttpGet, Route("approvedservices")]
        public async Task<IActionResult> GetApprovedServices(ApproveServices package)
        {

            List<Services> lst_ServiceTypes = new List<Services>();
            string s_status = "";
            string sqlstmt = "";

            string s_DayMask = GenSettings.GetDayMask(true, package.BookDate);
            string s_DayTimeSQL = "";

            string res = GenSettings.DBLookup("EnforceActivityLimits", "Registration", "", ref s_status);

            if (Convert.ToBoolean(res) == true)
            {
                if (s_DayMask != "" && s_DayMask != "00000000")
                {
                    if ((s_DayMask.Substring(0, 1) == "1"))
                    {
                        s_DayTimeSQL = "IsNull(it.NoMonday, 0) = 0";
                    }

                    if ((s_DayMask.Substring(1, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoTuesday, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoTuesday, 0) = 0"));
                    }

                    if ((s_DayMask.Substring(2, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoWednesday, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoWednesday, 0) = 0"));
                    }

                    if ((s_DayMask.Substring(3, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoThursday, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoThursday, 0) = 0"));
                    }

                    if ((s_DayMask.Substring(4, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoFriday, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoFriday, 0) = 0"));
                    }

                    if ((s_DayMask.Substring(5, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoSaturday, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoSaturday, 0) = 0"));
                    }

                    if ((s_DayMask.Substring(6, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoSunday, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoSunday, 0) = 0"));
                    }

                    if ((s_DayMask.Substring(7, 1) == "1"))
                    {
                        s_DayTimeSQL = ((GenSettings.NullToStr(s_DayTimeSQL) == "") ? "IsNull(it.NoPubHol, 0) = 0" : (s_DayTimeSQL + " AND IsNull(it.NoPubHol, 0) = 0"));
                    }

                    if ((s_DayTimeSQL != ""))
                    {
                        s_DayTimeSQL = (" AND ("
                                    + (s_DayTimeSQL + ")"));
                    }
                }
            }

            sqlstmt = " select case when isnull(it.[BillText],'') <> '' then [billtext] else [service type] end as billtext,[service type] as ServiceType,serviceProgram " +
                " ,dbo.GetBillingRate(accountno,[service type],ServiceProgram)  as BillingRate, it.Unit, it.[Recnum], IsNull(it.ExcludeFromClientPortalDisplay, 0) AS ExcludeFromClientPortalDisplay" +
                " ,isnull(DetailImageFile,GroupImageFile) as DetailImageFile" +
                " from serviceoverview o inner join recipients r on o.personid = r.uniqueid " +
                " join ItemTypes it on it.[title]=o.[service type] " + s_DayTimeSQL +
                 //    "  and '" + package.StartTime + "' between starttimelimit and  endtimelimit " +
                 "AND ( (ISNULL(starttimelimit, '') = '' OR ISNULL(endtimelimit, '') = '') OR ('" + package.StartTime + "' BETWEEN starttimelimit AND endtimelimit) )" +
                " where   accountno = '" + package.RecipientCode + "' AND (IsNull(it.ExcludeFromClientPortalDisplay, 0) = 0) AND  servicestatus = 'active' AND RosterGroup IN ( 'CENTREBASED', 'ONEONONE', 'SLEEPOVER', 'GROUPACTIVITY', 'TRANSPORT', 'TRAVELTIME' )";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sqlstmt, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        Services service = new Services();
                        service.ServiceName = (rd["BillText"] ?? "").ToString();
                        service.ServiceType = rd["ServiceType"].ToString();
                        service.ServiceProgram = rd["serviceProgram"].ToString();
                        service.ServiceBillRate = rd["BillingRate"].ToString();
                        service.ServiceUnit = rd["Unit"].ToString();
                        service.ExcludeFromClientPortal = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]);
                        service.Recnum = GenSettings.Filter(rd["Recnum"]);

                        if (rd["DetailImageFile"].ToString() == "" || rd["DetailImageFile"] == null)
                            service.ServiceImage = "letters/" + service.ServiceName.Substring(0, 1) + ".png";
                        else
                            service.ServiceImage = rd["DetailImageFile"].ToString();

                        lst_ServiceTypes.Add(service);
                    }

                    return Ok(new
                    {
                        list = lst_ServiceTypes,
                        sql = sqlstmt,
                        dayMask = s_DayMask
                    });
                }
            }
        }

        [HttpGet, Route("qualifiedstaff")]
        public async Task<IActionResult> GetQualifiedStaff(QualifiedStaff staff)
        {
            string SqlStmt = "[GETQUALIFIEDSTAFF]";
            //ImageProcessing img = new ImageProcessing();

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(SqlStmt, (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@FirstRecipient", staff.RecipientCode ?? "");
                    cmd.Parameters.AddWithValue("@username", staff.User ?? "");
                    cmd.Parameters.AddWithValue("@DATE", staff.BookDate ?? "");
                    cmd.Parameters.AddWithValue("@STARTTIME", staff.StartTime ?? "");
                    cmd.Parameters.AddWithValue("@ENDTIME", staff.EndTime ?? "");
                    cmd.Parameters.AddWithValue("@ENDLIMIT", staff.EndLimit ?? "");
                    cmd.Parameters.AddWithValue("@Gender", staff.Gender ?? "");
                    cmd.Parameters.AddWithValue("@Competencies", staff.Competencys ?? "");
                    cmd.Parameters.AddWithValue("@CompetenciesCount", staff.CompetenciesCount);
                    cmd.Parameters.AddWithValue("@TeamFilter", staff.TeamFilter ?? "");
                    cmd.Parameters.AddWithValue("@CategoryFilter", staff.CategoryFilter ?? "");

                    cmd.Parameters.AddWithValue("@RecordType", "Staff");

                    cmd.CommandTimeout = 20000;
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Staff> list = new List<Staff>();
                    while (await rd.ReadAsync())
                    {
                        Staff staffMember = new Staff();
                        staffMember.AccountNo = GenSettings.Filter(rd["AccountNo"]);
                        staffMember.FirstName = GenSettings.Filter(rd["Name"]);
                        staffMember.Gender = GenSettings.Filter(rd["Gender"]);
                        staffMember.Age = GenSettings.Filter(rd["Age"]) != "?" ? GenSettings.Filter(rd["Age"]) : "";
                        staffMember.SpecialSkill = GenSettings.Filter(rd["SpecialSkills"]);
                        staffMember.Rating = getRating((GenSettings.Filter(rd["Rating"])) ?? "");

                        // if (!GenSettings.HasColumn(rd,"agency_medal") || rd["agency_medal"] == null || rd["agency_medal"].ToString()=="")
                        // {
                        //     staffMember.agency_medal = "Silver.png";
                        // }
                        // else
                        // {
                        //     if (rd["agency_medal"].ToString()=="1")     staffMember.agency_medal =  "Gold.png";
                        //     else if(rd["agency_medal"].ToString() == "2")   staffMember.agency_medal = "Silver.png";
                        //     else    staffMember.agency_medal = "Bronz.png";
                        // }



                        // Staff stf = new Staff();
                        // if(dr["Filephoto"]!=null && dr["Filephoto"].ToString()!="") {
                        //     //stf.Filephoto = img.getImageFromPath(dr["Filephoto"].ToString());
                        //     stf.FilePhoto = dr["Filephoto"].ToString();
                        // }

                        // stf.AccountNo = dr["AccountNo"].ToString();
                        // stf.FirstName = dr["Name"].ToString();
                        // stf.MiddleName = "";
                        // stf.LastName = "";
                        // stf.Title ="";
                        // stf.DOB = ""; // dr["DOB"].ToString();
                        // stf.Age = dr["Age"].ToString();
                        // stf.Gender = dr["Gender"].ToString();
                        // //stf.FilePhoto = "";// dr["FilePhoto"].ToString();

                        // if (dr["distance"].ToString() == "" || dr["distance"] == null)
                        //     stf.KM = "0";
                        // else
                        //     stf.KM =Math.Round(Convert.ToDouble(dr["distance"].ToString()),2).ToString();

                        // stf.Price = "0";
                        // stf.SpecialSkill = dr["SpecialSkills"].ToString();
                        // if (stf.SpecialSkill.Length < 50)
                        //     stf.SpecialSkill_Short = stf.SpecialSkill;
                        // else
                        //     stf.SpecialSkill_Short = stf.SpecialSkill.Substring(0, 50);

                        // stf.Rating = getRating(dr["Rating"].ToString());
                        // stf.SpecialSkill_lined = stf.SpecialSkill.Replace(",", ", ");


                        // if (!GenSettings.HasColumn(dr,"agency_medal") || dr["agency_medal"] == null || dr["agency_medal"].ToString()=="")
                        // {
                        //     stf.agency_medal = "Silver.png";
                        // }else
                        // {
                        //     if (dr["agency_medal"].ToString()=="1")
                        //         stf.agency_medal =  "Gold.png";
                        //     else if(dr["agency_medal"].ToString() == "2")
                        //         stf.agency_medal = "Silver.png";
                        //     else
                        //         stf.agency_medal = "Bronz.png";
                        // }

                        // stf.Reviewes ="No ";                            
                        // lst_Staff.Add(stf); 
                        list.Add(staffMember);
                    }

                    var newList = list.Select(x => x).OrderBy(x => x.FirstName);
                    return Ok(newList);
                }
            }
        }

        [HttpPut("opnotes/{recordNo}")]
        public async Task<IActionResult> UpdateOPNote([FromBody] PostNote note, int recordNo)
        {
            using(var ctx = _context)
            {
                var dbNote = await (from h in _context.History
                                    where h.RecordNumber == recordNo && h.ExtraDetail1=="OPNOTE"
                                    select h).FirstOrDefaultAsync();

                dbNote.Detail = Validators.ValidateValue<string>(note.Notes);
                dbNote.AlarmDate = Validators.ValidateDate(note.AlarmDate);
                dbNote.ExtraDetail2 = Validators.ValidateValue<string>(note.Category);
                dbNote.Program = Validators.ValidateValue<string>(note.Program);
                dbNote.Discipline = Validators.ValidateValue<string>(note.Discipline);
                dbNote.Restrictions = Validators.ValidateValue<string>(note.Restrictions);
                dbNote.CareDomain = Validators.ValidateValue<string>(note.CareDomain);
                dbNote.PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp);
                dbNote.PrivateFlag = Validators.ValidateValue<Boolean>(Convert.ToBoolean(note.RestrictionsStr));
                
                await ctx.SaveChangesAsync();
                return Ok(true);
            }

            // using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            // {
            //     using (SqlCommand cmd = new SqlCommand(@"UPDATE History SET Detail = @note, AlarmDate = @alarmDate, ExtraDetail2 = @category, Program = @program, Discipline = @discipline,
            //         Restrictions = @restrictions, CareDomain = @careDomain, PublishToApp = @publishToApp WHERE RecordNumber = @recordNo", (SqlConnection)conn))
            //     {
            //         cmd.Parameters.AddWithValue("@note", note.Notes);
            //         cmd.Parameters.AddWithValue("@alarmDate", DateFilter.Convert((DateTime?)note.AlarmDate));
            //         cmd.Parameters.AddWithValue("@restrictions", note.Restrictions);
            //         cmd.Parameters.AddWithValue("@category", note.Category);
            //         cmd.Parameters.AddWithValue("@program", note.Program);
            //         cmd.Parameters.AddWithValue("@discipline", note.Discipline);
            //         cmd.Parameters.AddWithValue("@careDomain", note.CareDomain);
            //         cmd.Parameters.AddWithValue("@publishToApp", note.PublishToApp);
            //         cmd.Parameters.AddWithValue("@recordNo", recordNo);
            //         await conn.OpenAsync();

            //         return Ok(await cmd.ExecuteNonQueryAsync() > 0);
            //     }
            // }
        }

[HttpPost("post-notes")]
        public async Task<IActionResult> AddNote([FromBody] PostNote note)
        {
            using(var ctx = _context)
            {
                var dbNote = await (from h in _context.History                                   
                                    select h).FirstOrDefaultAsync();

                dbNote.Detail = Validators.ValidateValue<string>(note.Notes);
                dbNote.AlarmDate = Validators.ValidateDate(note.AlarmDate);
                dbNote.ExtraDetail2 = Validators.ValidateValue<string>(note.Category);
                dbNote.Program = Validators.ValidateValue<string>(note.Program);
                dbNote.Discipline = Validators.ValidateValue<string>(note.Discipline);
                dbNote.Restrictions = Validators.ValidateValue<string>(note.Restrictions);
                dbNote.CareDomain = Validators.ValidateValue<string>(note.CareDomain);
                dbNote.PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp);
                dbNote.PrivateFlag = Validators.ValidateValue<Boolean>(Convert.ToBoolean(note.RestrictionsStr));
                dbNote.WhoCode = Validators.ValidateValue<string>(note.WhoCode);
                dbNote.PublishToClientPortal = Validators.ValidateValue<Boolean>(note.PublishToClientPortal);
              
                await ctx.SaveChangesAsync();
                return Ok(true);
            }

           
        }
  [HttpPut("notes/{recordNo}")]
        public async Task<IActionResult> UpdateNote([FromBody] PostNote note, int recordNo)
        {
            using(var ctx = _context)
            {
                var dbNote = await (from h in _context.History
                                    where h.RecordNumber == recordNo 
                                    select h).FirstOrDefaultAsync();

                dbNote.Detail = Validators.ValidateValue<string>(note.Notes);
                dbNote.AlarmDate = Validators.ValidateDate(note.AlarmDate);
                dbNote.ExtraDetail2 = Validators.ValidateValue<string>(note.Category);
                dbNote.Program = Validators.ValidateValue<string>(note.Program);
                dbNote.Discipline = Validators.ValidateValue<string>(note.Discipline);
                dbNote.Restrictions = Validators.ValidateValue<string>(note.Restrictions);
                dbNote.CareDomain = Validators.ValidateValue<string>(note.CareDomain);
                dbNote.PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp);
                dbNote.PrivateFlag = Validators.ValidateValue<Boolean>(Convert.ToBoolean(note.RestrictionsStr));
                dbNote.WhoCode = Validators.ValidateValue<string>(note.WhoCode);
                dbNote.PublishToClientPortal = Validators.ValidateValue<Boolean>(note.PublishToClientPortal);

                await ctx.SaveChangesAsync();
                return Ok(true);
            }

           
        }

        [HttpGet, Route("note-permissions/{id}")]
        public async Task<IActionResult> GetNotePermissions(string id)
        {
            var view = await (from ui in _context.UserInfo where ui.Name == id select ui.EnableViewNoteCases).FirstOrDefaultAsync();

            if(view == null){
                return Ok(new {
                    OPNote = false,
                    CaseNote = false
                });
            }
            
            return Ok(new {
                OPNote =  view.Substring(0,1) == "1" ? true : false,
                CaseNote =  view.Substring(1,1) == "1" ? true : false
            });
        }

        [HttpGet, Route("opnotes-dates")]
        public async Task<IActionResult> GetOPNotesWithDates(ServiceNote note)
        {
            DateTime startDate = Convert.ToDateTime(note.StartDate);
            DateTime endDate = Convert.ToDateTime(note.EndDate);

            using(var ctx = _context)
            {
                var opnotes = await (from history in _context.History
                        where history.PersonID == note.Client && history.ExtraDetail1 == "OPNOTE" && history.DeletedRecord != true && (history.DetailDate >= startDate && history.DetailDate <= endDate) && history.PublishToClientPortal == true
                                     orderby history.DetailDate descending, history.RecordNumber descending 
                            select new {
                                PersonID = history.PersonID,
                                RecordNumber = history.RecordNumber,
                                PrivateFlag = history.PrivateFlag,
                                WhoCode = history.WhoCode,
                                DetailDate = history.DetailDate,
                                Creator = history.Creator,
                                Detail = DatabaseContext.RTF2Text(history.Detail),
                                DetailOriginal = history.Detail,
                                ExtraDetail2 = history.ExtraDetail2,
                                Restrictions = history.Restrictions,
                                AlarmDate = history.AlarmDate,
                                Program = history.Program,
                                Discipline = history.Discipline,
                                CareDomain = history.CareDomain,
                                PublishToApp = history.PublishToApp,
                                PublishToClientPortal = history.PublishToClientPortal
                            }).ToListAsync();

                return Ok(new {
                    list = opnotes,
                    success = true
                });
            }
        }


        [HttpPost, Route("gnotes-with-filters/{id}")]
        public async Task<IActionResult> GetGNotesWithFilters([FromBody] Filters filters, string id)
        {
            using(var ctx = _context)
            {
                var opnotes = (from history in _context.History
                        where history.PersonID == id && history.ExtraDetail1 == filters.Type && history.DeletedRecord != true
                        orderby history.DetailDate descending, history.RecordNumber descending 
                            select new {
                                PersonID = history.PersonID,
                                RecordNumber = history.RecordNumber,
                                PrivateFlag = history.PrivateFlag,
                                WhoCode = history.WhoCode,
                                DetailDate = history.DetailDate,
                                Creator = history.Creator,
                                Detail = DatabaseContext.RTF2Text(history.Detail),
                                DetailOriginal = history.Detail,
                                ExtraDetail2 = history.ExtraDetail2,
                                Restrictions = history.Restrictions,
                                AlarmDate = history.AlarmDate,
                                Program = history.Program,
                                Discipline = history.Discipline,
                                CareDomain = history.CareDomain,
                                PublishToApp = history.PublishToApp
                            });

                if(!filters.AllDates)
                {
                    opnotes = opnotes.Where(x => x.DetailDate > filters.StartDate && x.DetailDate <= filters.EndDate);
                }


                return Ok(new {
                    list = opnotes.Take(filters.Display).ToList(),
                    success = true
                });
            }
        }
          [HttpPost, Route("svcotes-with-filters/{id}")]
        public async Task<IActionResult> GetSVCNotesWithFilters([FromBody] FiltersDto filters, string id)
        {
             var opnotes = (from history in _context.History
                        where history.PersonID == id && history.ExtraDetail1 == "SVCNOTE" 
                        &&  (history.ExtraDetail2 =="#MTANOTE" || history.ExtraDetail2 =="#SHIFTREPORT" || history.ExtraDetail2 =="SHIFT NOTE")
                        && history.DeletedRecord != true
                        orderby history.DetailDate descending, history.RecordNumber descending
                            select new
                            {
                                PersonID = history.PersonID,
                                RecordNumber = history.RecordNumber,
                                PrivateFlag = history.PrivateFlag,
                                WhoCode = history.WhoCode,
                                DetailDate = history.DetailDate,
                                Creator = history.Creator,
                                Detail = DatabaseContext.RTF2Text(history.Detail),
                                DetailOriginal = history.Detail,
                                ExtraDetail2 = history.ExtraDetail2,
                                Restrictions = history.Restrictions,
                                AlarmDate = history.AlarmDate,
                                Program = history.Program,
                                Discipline = history.Discipline,
                                CareDomain = history.CareDomain,
                                PublishToApp = history.PublishToApp
                            });

                if (filters != null && !filters.AllDates)
                {
                    opnotes = opnotes.Where(x => x.DetailDate > filters.StartDate && x.DetailDate <= filters.EndDate);
                }

                if (filters != null && !string.IsNullOrEmpty(filters.LimitTo) && !string.IsNullOrEmpty(filters.StartingWith))
                {
                    if (filters.LimitTo == "CARE DOMAIN")
                    {
                        opnotes = opnotes.Where(x => x.CareDomain.CompareTo(filters.StartingWith) >= 0 && x.CareDomain.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "CATEGORY")
                    {
                        opnotes = opnotes.Where(x => x.ExtraDetail2.CompareTo(filters.StartingWith) >= 0 && x.ExtraDetail2.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "CREATOR")
                    {
                        opnotes = opnotes.Where(x => x.Creator.CompareTo(filters.StartingWith) >= 0 && x.Creator.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "DISCIPLINE")
                    {
                        opnotes = opnotes.Where(x => x.Discipline.CompareTo(filters.StartingWith) >= 0 && x.Discipline.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "PROGRAM")
                    {
                        opnotes = opnotes.Where(x => x.Program.CompareTo(filters.StartingWith) >= 0 && x.Program.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }
                }

                if (filters != null && filters.Display > 0)
                {
                    opnotes = opnotes.Take(filters.Display);
                }
                else
                {
                    opnotes = opnotes.Take(1000);
                }


                return Ok(new
                {
                    list = await opnotes.ToListAsync(),
                    success = true
                });
            
        }
        [HttpPost, Route("opnotes-with-filters/{id}")]
        public async Task<IActionResult> GetOPNotesWithFilters([FromBody] FiltersDto filters, string id)
        {
             var opnotes = (from history in _context.History
                        where history.PersonID == id && history.ExtraDetail1 == "OPNOTE" && history.DeletedRecord != true
                        orderby history.DetailDate descending, history.RecordNumber descending
                            select new
                            {
                                PersonID = history.PersonID,
                                RecordNumber = history.RecordNumber,
                                PrivateFlag = history.PrivateFlag,
                                WhoCode = history.WhoCode,
                                DetailDate = history.DetailDate,
                                Creator = history.Creator,
                                Detail = DatabaseContext.RTF2Text(history.Detail),
                                DetailOriginal = history.Detail,
                                ExtraDetail2 = history.ExtraDetail2,
                                Restrictions = history.Restrictions,
                                AlarmDate = history.AlarmDate,
                                Program = history.Program,
                                Discipline = history.Discipline,
                                CareDomain = history.CareDomain,
                                PublishToApp = history.PublishToApp
                            });

                if (filters != null && !filters.AllDates)
                {
                    opnotes = opnotes.Where(x => x.DetailDate > filters.StartDate && x.DetailDate <= filters.EndDate);
                }

                if (filters != null && !string.IsNullOrEmpty(filters.LimitTo) && !string.IsNullOrEmpty(filters.StartingWith))
                {
                    if (filters.LimitTo == "CARE DOMAIN")
                    {
                        opnotes = opnotes.Where(x => x.CareDomain.CompareTo(filters.StartingWith) >= 0 && x.CareDomain.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "CATEGORY")
                    {
                        opnotes = opnotes.Where(x => x.ExtraDetail2.CompareTo(filters.StartingWith) >= 0 && x.ExtraDetail2.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "CREATOR")
                    {
                        opnotes = opnotes.Where(x => x.Creator.CompareTo(filters.StartingWith) >= 0 && x.Creator.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "DISCIPLINE")
                    {
                        opnotes = opnotes.Where(x => x.Discipline.CompareTo(filters.StartingWith) >= 0 && x.Discipline.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "PROGRAM")
                    {
                        opnotes = opnotes.Where(x => x.Program.CompareTo(filters.StartingWith) >= 0 && x.Program.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }
                }

                if (filters != null && filters.Display > 0)
                {
                    opnotes = opnotes.Take(filters.Display);
                }
                else
                {
                    opnotes = opnotes.Take(1000);
                }


                return Ok(new
                {
                    list = await opnotes.ToListAsync(),
                    success = true
                });
            
        }

        [HttpGet, Route("opnotes/{id}")]
        public async Task<IActionResult> GetOPNotes(string id)
        {
            using(var ctx = _context)
            {
                var opnotes = await (from history in _context.History
                        where history.PersonID == id && history.ExtraDetail1 == "OPNOTE" && history.DeletedRecord != true
                        orderby history.DetailDate descending, history.RecordNumber descending 
                            select new {
                                PersonID = history.PersonID,
                                RecordNumber = history.RecordNumber,
                                PrivateFlag = history.PrivateFlag,
                                WhoCode = history.WhoCode,
                                DetailDate = history.DetailDate,
                                Creator = history.Creator,
                                Detail = DatabaseContext.RTF2Text(history.Detail),
                                DetailOriginal = history.Detail,
                                ExtraDetail2 = history.ExtraDetail2,
                                Restrictions = history.Restrictions,
                                AlarmDate = history.AlarmDate,
                                Program = history.Program,
                                Discipline = history.Discipline,
                                CareDomain = history.CareDomain,
                                PublishToApp = history.PublishToApp
                            }).ToListAsync();

                return Ok(new {
                    list = opnotes,
                    success = true
                });
            }
        }

        [HttpPost("profile")]
        public async Task<IActionResult> PostReferral([FromBody] JObject json)
        {
            SqlTransaction transaction = null;
            var rc = await GenSettings.GetAccountNameGenerator("recipient");
            await GenSettings.UpdateSysTable("recipient", rc.CustomerId);

            var recipient = json.ToObject<Recipient>();
            OutputRecipients rp = new OutputRecipients();

            if (await GenSettings.IfRecipientAccountNameExist(recipient.AccountNo))
            {
                return BadRequest("Account Name is already taken");
            }

            try
            {
                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    string id = string.Empty;
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();                  
                   

                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO Recipients([UniqueId], AccountNo,FirstName,[Surname/Organisation],Gender,DateOfBirth,[Type], Branch, AgencyDefinedGroup, RECIPIENT_CoOrdinator, BillTo, NDISNumber,title)
                            OUTPUT INSERTED.UniqueId VALUES(@uniqueId,@accountno,@firstname,@lastname,@gender,@bdate,'REFERRAL',@branch,@agencyGroup,@coordinator,@billTo,@ndis,@title)", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@uniqueId", rc.UniqueId);
                        cmd.Parameters.AddWithValue("@accountno", !string.IsNullOrEmpty(recipient.AccountNo) ? (recipient.AccountNo).ToUpper() : "");
                        cmd.Parameters.AddWithValue("@firstname", recipient.FirstName);
                        cmd.Parameters.AddWithValue("@lastname", recipient.LastName);
                        cmd.Parameters.AddWithValue("@gender", recipient.Gender);
                        cmd.Parameters.AddWithValue("@bdate", recipient.DOB);
                        cmd.Parameters.AddWithValue("@branch", recipient.Branch);
                        cmd.Parameters.AddWithValue("@agencyGroup", recipient.AgencyDefinedGroup);
                        cmd.Parameters.AddWithValue("@coordinator", recipient.RecipientCoordinator);
                        cmd.Parameters.AddWithValue("@billTo", !string.IsNullOrEmpty(recipient.AccountNo) ? (recipient.AccountNo).ToUpper() : "");
                        cmd.Parameters.AddWithValue("@ndis", recipient.NDISNumber ?? "");
                        cmd.Parameters.AddWithValue("@title", recipient.Title ?? "");
                        
                        id = (string)cmd.ExecuteScalar();
                    }   

                    using (SqlCommand cmd = new SqlCommand(@"SELECT [UniqueId], [AccountNo],[AgencyDefinedGroup] FROM Recipients WHERE UniqueId = @id", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                        {
                            if (await rd.ReadAsync())
                            {
                                rp = new OutputRecipients()
                                {
                                    Id = GenSettings.Filter(rd[0]),
                                    Code = GenSettings.Filter(rd[1]),
                                    AgencyDefinedGroup = GenSettings.Filter(rd[2]),
                                    View = "recipient",
                                    Contact = string.Join(" | ", recipient.contacts.Select(x => x.Contact).ToList()),
                                    Address = string.Join(" | ", recipient.addresses.Select(x => x.Suburb).ToList()),
									Branch = recipient.Branch,
                                    RecipientCoordinator = recipient.RecipientCoordinator
                                };
                            }
                        }
                    }

                    if(!string.IsNullOrEmpty(recipient.Branch))
                    {

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO humanresources ([PersonID],[Group],[type],[name]) VALUES(@accountName, @group, @type,@name)", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@group", "RECIPBRANCHES");
                            cmd.Parameters.AddWithValue("@type", "RECIPBRANCHES");
                            cmd.Parameters.AddWithValue("@name", recipient.Branch);   
                            cmd.Parameters.AddWithValue("@accountName", rc.UniqueId);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    foreach (var address in recipient.otherContacts)
                    {

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO humanresources ([PersonID],[Group],[state],[type],[subtype],[name], address1, address2, suburb, postcode, phone1, phone2, fax, email, notes, equipmentcode, mobile, creator) 
	                VALUES(@accountName, @group, @state, @type,@subtype,@name,@address1, @address2, @suburb, @postcode, @phone1, @phone2, @fax,@email,@notes, @ecode, @mobile, @creator)", (SqlConnection)conn, transaction))
                        {

                            var code = Regex.Match(address.Suburb, @"\d+", RegexOptions.IgnoreCase).Value;
                            var suburb = Regex.Match(address.Suburb, @"(?i)^[a-z]+", RegexOptions.IgnoreCase).Value;
                            var state = address.Suburb.Split(",").Length > 1 ? address.Suburb.Split(",")[1] : "";

                            cmd.Parameters.AddWithValue("@group", "CONTACT");
                            cmd.Parameters.AddWithValue("@state", state ?? "");
                            cmd.Parameters.AddWithValue("@type", address.Group ?? "");
                            cmd.Parameters.AddWithValue("@subtype", address.Type ?? "");

                            cmd.Parameters.AddWithValue("@name", address.Name ?? "");
                            cmd.Parameters.AddWithValue("@address1", address.Address1 ?? "");
                            cmd.Parameters.AddWithValue("@address2", address.Address2 ?? "");
                            cmd.Parameters.AddWithValue("@suburb", suburb ?? "");
                            cmd.Parameters.AddWithValue("@postcode", code ?? "");
                            cmd.Parameters.AddWithValue("@phone1", address.Phone1 ?? "");
                            cmd.Parameters.AddWithValue("@phone2", address.Phone2 ?? "");
                            cmd.Parameters.AddWithValue("@fax", address.Fax ?? "");
                            cmd.Parameters.AddWithValue("@email", address.Email ?? "");
                            cmd.Parameters.AddWithValue("@notes", address.Notes ?? "");
                            cmd.Parameters.AddWithValue("@ecode", address.Ecode ?? "");
                            cmd.Parameters.AddWithValue("@mobile", address.Mobile ?? "");
                            cmd.Parameters.AddWithValue("@creator", address.Creator ?? "");
                            cmd.Parameters.AddWithValue("@accountName", rc.UniqueId);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    foreach (var address in recipient.gpDetails)
                    {

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO humanresources ([PersonID],[Group],[state],[type],[subtype],[name], address1, address2, suburb, postcode, phone1, phone2, fax, email, notes, equipmentcode, mobile, creator) 
	                VALUES(@accountName, @group, @state, @type,@subtype,@name,@address1, @address2, @suburb, @postcode, @phone1, @phone2, @fax,@email,@notes, @ecode, @mobile, @creator)", (SqlConnection)conn, transaction))
                        {
                            var code = Regex.Match(address.Suburb, @"\d+", RegexOptions.IgnoreCase).Value;
                            var suburb = Regex.Match(address.Suburb, @"(?i)^[a-z]+", RegexOptions.IgnoreCase).Value;
                            var state = address.Suburb.Split(",").Length > 1 ? address.Suburb.Split(",")[1] : "";

                            cmd.Parameters.AddWithValue("@group", "CONTACT");
                            cmd.Parameters.AddWithValue("@state", state ?? "");
                            cmd.Parameters.AddWithValue("@type", address.Group ?? "");
                            cmd.Parameters.AddWithValue("@subtype", address.Type ?? "");

                            cmd.Parameters.AddWithValue("@name", address.Name ?? "");
                            cmd.Parameters.AddWithValue("@address1", address.Address1 ?? "");
                            cmd.Parameters.AddWithValue("@address2", address.Address2 ?? "");
                            cmd.Parameters.AddWithValue("@suburb", suburb ?? "");
                            cmd.Parameters.AddWithValue("@postcode", code ?? "");
                            cmd.Parameters.AddWithValue("@phone1", address.Phone1 ?? "");
                            cmd.Parameters.AddWithValue("@phone2", address.Phone2 ?? "");
                            cmd.Parameters.AddWithValue("@fax", address.Fax ?? "");
                            cmd.Parameters.AddWithValue("@email", address.Email ?? "");
                            cmd.Parameters.AddWithValue("@notes", address.Notes ?? "");
                            cmd.Parameters.AddWithValue("@ecode", address.Ecode ?? "");
                            cmd.Parameters.AddWithValue("@mobile", address.Mobile ?? "");
                            cmd.Parameters.AddWithValue("@creator", address.Creator ?? "");
                            cmd.Parameters.AddWithValue("@accountName", rc.UniqueId);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    foreach (var address in recipient.addresses)
                    {

                        if (string.IsNullOrEmpty(address.Type) || string.IsNullOrEmpty(address.Suburb))
                        {
                            break;
                        }

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO NamesAndAddresses(PersonID, [Type], [Description],[Address1],[Suburb],[PostCode],[PrimaryAddress]) VALUES (@personid, 'PERSONALADDRESS', @description,@address,@suburb,@postcode,@primaryAdd)", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@personid", rc.UniqueId);
                            cmd.Parameters.AddWithValue("@description", address.Type);

                            var code = Regex.Match(address.Suburb, @"\d+", RegexOptions.IgnoreCase).Value;
                            var suburb = Regex.Match(address.Suburb, @"(?i)^[a-z]+", RegexOptions.IgnoreCase).Value;

                            cmd.Parameters.AddWithValue("@address", address.Address1);
                            cmd.Parameters.AddWithValue("@suburb", suburb);
                            cmd.Parameters.AddWithValue("@postcode", code);

                            if(recipient.addresses.Count == 1)
                            {
                                cmd.Parameters.AddWithValue("@primaryAdd", true);
                            }
                            else
                            {
                                // If more than two addresses but no primary let first entry be the primary address
                                if (recipient.addresses.Count > 1 &&
                                        recipient.addresses.Count(x => x.Primary) == 0 &&
                                            recipient.addresses.First() == address)
                                {
                                    cmd.Parameters.AddWithValue("@primaryAdd", true);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@primaryAdd", address.Primary);
                                }
                            }

                           


                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    foreach (var contact in recipient.contacts)
                    {

                        if (string.IsNullOrEmpty(contact.ContactType) || string.IsNullOrEmpty(contact.Contact))
                        {
                            break;
                        }

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO PhoneFaxOther([PersonID], [Type], [Detail],[PrimaryPhone]) VALUES (@personid, @type,@detail,@primaryPhone)", (SqlConnection)conn, transaction))
                        {
                            
                            cmd.Parameters.AddWithValue("@personid", rc.UniqueId);
                            cmd.Parameters.AddWithValue("@type", contact.ContactType);
                            cmd.Parameters.AddWithValue("@detail", contact.Contact);

                            if (recipient.contacts.Count == 1)
                            {
                                cmd.Parameters.AddWithValue("@primaryPhone", true);
                            }
                            else
                            {
                                // If more than two contacts but no primary let first entry be the primary contact
                                if (recipient.contacts.Count > 1 &&
                                        recipient.contacts.Count(x => x.Primary) == 0 &&
                                            recipient.contacts.First() == contact)
                                {
                                    cmd.Parameters.AddWithValue("@primaryPhone", true);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@primaryPhone", contact.Primary);
                                }
                            }

                            

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }
                    await transaction.CommitAsync();
                    return Ok(rp);
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return BadRequest(ex);
            }
        }

        [HttpGet("is-accountno-unique/{accountNo}")]
        public async Task<IActionResult> GetIsAccountNoUnique(string accountNo)
        {
            if (String.IsNullOrEmpty(accountNo)) return Ok(false);
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) from Recipients WHERE AccountNo = @accountNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@accountNo", accountNo);
                    await conn.OpenAsync();

                    int resultSet = 0;
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        if (await rd.ReadAsync())
                            resultSet = rd.GetInt32(0);


                        return Ok(resultSet > 0 ? 0 : 1);
                    }
                }
            }
        }

    

        [HttpPost("opnotes/{personID}")]
        public async Task<IActionResult> PostOPNotes([FromBody] PostNote note, string personID)
        {
            using(var ctx = _context)
            {
                var newNote = new History()
                {
                    PersonID = Validators.ValidateValue<string>(personID),
                    DetailDate = DateTime.Now,
                    Detail = Validators.ValidateValue<string>(note.Notes),
                    AlarmDate = Validators.ValidateDate(note.AlarmDate),
                    ExtraDetail1 = "OPNOTE",
                    ExtraDetail2 = Validators.ValidateValue<string>(note.Category),
                    WhoCode = Validators.ValidateValue<string>(note.WhoCode),
                    Creator = "SYSMGR",
                    Restrictions = Validators.ValidateValue<string>(note.Restrictions),
                    Program = Validators.ValidateValue<string>(note.Program),
                    Discipline = Validators.ValidateValue<string>(note.Discipline),
                    CareDomain = Validators.ValidateValue<string>(note.CareDomain),
                    PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp),
                    PrivateFlag = Validators.ValidateValue<Boolean>(Convert.ToBoolean(note.RestrictionsStr)),
                };

                await ctx.History.AddAsync(newNote);
                await ctx.SaveChangesAsync();
                return Ok(true);
            }

            // using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            // {
            //     using (SqlCommand cmd = new SqlCommand(@"INSERT INTO History([PersonID],[DetailDate],[Detail],[AlarmDate],[ExtraDetail1],[ExtraDetail2],[WhoCode],[Creator],[Restrictions],[Program],[Discipline],[CareDomain],[PublishToApp],[PrivateFlag]) 
            //         VALUES(@personID, GETDATE(), @detail, @alarmDate, 'OPNOTE', @extraDetail2, @whocode, 'SYSMGR', @restrictions, @program, @discipline, @careDomain, @publishToApp,@privateflag)", (SqlConnection)conn))
            //     {
            //         cmd.Parameters.AddWithValue("@personID", personID);
            //         cmd.Parameters.AddWithValue("@detail", note.Notes);
            //         cmd.Parameters.AddWithValue("@alarmDate", DateFilter.Convert((DateTime?)note.AlarmDate));
            //         cmd.Parameters.AddWithValue("@extraDetail2", note.Category);
            //         cmd.Parameters.AddWithValue("@whocode", note.WhoCode);
            //         cmd.Parameters.AddWithValue("@restrictions", note.Restrictions);
            //         cmd.Parameters.AddWithValue("@privateflag", note.RestrictionsStr);
            //         cmd.Parameters.AddWithValue("@program", note.Program);
            //         cmd.Parameters.AddWithValue("@discipline", note.Discipline);
            //         cmd.Parameters.AddWithValue("@careDomain", note.CareDomain);
            //         cmd.Parameters.AddWithValue("@publishToApp", note.PublishToApp);

            //         await conn.OpenAsync();
            //         return Ok(await cmd.ExecuteNonQueryAsync() > 0);
            //     }
            // }
        }

        [HttpDelete("opnotes/{id}")]
        public async Task<IActionResult> DeleteOPNotes(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"DELETE FROM History WHERE RecordNumber = @id AND [ExtraDetail1]='OPNOTE'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPost("clinicalnote/{personID}")]
        public async Task<IActionResult> PostClinicalNotes([FromBody] PostNote note, string personID)
        {
            using(var ctx = _context)
            {
                var newNote = new History()
                {
                    PersonID = Validators.ValidateValue<string>(personID),
                    DetailDate = DateTime.Now,
                    Detail = Validators.ValidateValue<string>(note.Notes),
                    AlarmDate = Validators.ValidateDate(note.AlarmDate),
                    ExtraDetail1 = "CLINNOTE",
                    ExtraDetail2 = Validators.ValidateValue<string>(note.Category),
                    WhoCode = Validators.ValidateValue<string>(note.WhoCode),
                    Creator = "SYSMGR",
                    Restrictions = Validators.ValidateValue<string>(note.Restrictions),
                    Program = Validators.ValidateValue<string>(note.Program),
                    Discipline = Validators.ValidateValue<string>(note.Discipline),
                    CareDomain = Validators.ValidateValue<string>(note.CareDomain),
                    PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp),
                    PrivateFlag = Validators.ValidateValue<Boolean>(Convert.ToBoolean(note.RestrictionsStr)),
                };

                await ctx.History.AddAsync(newNote);
                await ctx.SaveChangesAsync();
                return Ok(true);
            }

           
        }
        [HttpPut("clinicalnote/{recordNo}")]
        public async Task<IActionResult> UpdateclinicalNote([FromBody] PostNote note, int recordNo)
        {
            using(var ctx = _context)
            {
                var dbNote = await (from h in _context.History
                                    where h.RecordNumber == recordNo && h.ExtraDetail1.Equals("CLINNOTE", StringComparison.OrdinalIgnoreCase)
                                    select h).FirstOrDefaultAsync();

                dbNote.Detail = Validators.ValidateValue<string>(note.Notes);
                dbNote.AlarmDate = Validators.ValidateDate(note.AlarmDate);
                dbNote.ExtraDetail2 = Validators.ValidateValue<string>(note.Category);
                dbNote.Program = Validators.ValidateValue<string>(note.Program);
                dbNote.Discipline = Validators.ValidateValue<string>(note.Discipline);
                dbNote.Restrictions = Validators.ValidateValue<string>(note.Restrictions);
                dbNote.CareDomain = Validators.ValidateValue<string>(note.CareDomain);
                dbNote.PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp);
                dbNote.PrivateFlag = Validators.ValidateValue<Boolean>(Convert.ToBoolean(note.RestrictionsStr));
                
                await ctx.SaveChangesAsync();
                return Ok(true);
            }
        }
        [HttpDelete("clinicalnote/{id}")]
        public async Task<IActionResult> DeleteClincalNotes(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"DELETE FROM History WHERE RecordNumber = @id AND [ExtraDetail1]='CLINNOTE'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("svcnote/{id}")]
        public async Task<IActionResult> DeleteSVCNotes(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"DELETE FROM History WHERE RecordNumber = @id AND [ExtraDetail1]='SVC'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpPost, Route("incidents/{id}")]
        public async Task<IActionResult> GetIncidents([FromBody] FiltersDto filters,string id)
        {
            var result = (from imm in _context.IM_Master
                             join it in _context.ItemTypes on imm.Service equals it.Title into dimmit
                             from subA in dimmit.DefaultIfEmpty()
                             join hr in _context.HumanResourceTypes on imm.Program equals hr.Name into dimmhr
                             from subB in dimmhr.DefaultIfEmpty()
                             where imm.PersonId == id
                             orderby imm.Status descending, imm.Date
                             select new
                             {
                                 imm.RecordNo,
                                 imm.PersonId,
                                 imm.Status,
                                 imm.Date,
                                 imm.Type,
                                 imm.PerpSpecify,
                                 imm.ShortDesc,
                                 imm.CurrentAssignee
                             }).ToList();
                

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP 20 RecordNo, PersonID, IMM.Status, Date, IMM.[Type], IMM.PerpSpecify AS SubCategory, IMM.ShortDesc AS Description,
                        CurrentAssignee AS [Assigned To] 
                        FROM IM_Master IMM LEFT JOIN ItemTypes IT ON  [IMM].[Service] = Title
                                        LEFT JOIN HumanResourceTypes HRT ON [HRT].[Name] = [IMM].Program
                        WHERE PersonID = @id 
                        ORDER BY IMM.STATUS DESC, DATE DESC", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNo"]),
                            PersonId = GenSettings.Filter(rd["PersonId"]),
                            Status = GenSettings.Filter(rd["Status"]),
                            Date = GenSettings.Filter(rd["Date"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            SubCategory = GenSettings.Filter(rd["SubCategory"]),
                            Description = GenSettings.Filter(rd["Description"])
                        });
                    }
                    return Ok(new
                    {
                        list = list,
                        success = true
                    });
                }
            }
        }

        [HttpGet, Route("loans/{id}")]
        public async Task<IActionResult> GetLoans(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, PersonID, [Type], [Name],[Notes], [Address1] As Program, Date1 As [Date Loaned], Date2 As [Collected], DateInstalled FROM HumanResources WHERE [PersonID] = @id
                    AND [GROUP] = 'LOANITEMS' ORDER BY State ASC, [Name] ASC", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    try
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                PersonID = GenSettings.Filter(rd["PersonID"]),
                                Type = GenSettings.Filter(rd["Type"]),
                                Name = GenSettings.Filter(rd["Name"]),
                                Notes = GenSettings.Filter(rd["Notes"]),
                                Program = GenSettings.Filter(rd["Program"]),
                                DateInstalled = GenSettings.Filter(rd["DateInstalled"]),
                                DateLoaned = GenSettings.Filter(rd["Date Loaned"]),
                                Collected = GenSettings.Filter(rd["Collected"]),
                            });
                        }
                        return Ok(new
                        {
                            list = list,
                            success = true
                        });
                    }
                    catch (Exception ex)
                    {
                        return BadRequest(new
                        {
                            error = ex
                        });
                    }
                }
            }
        }


        [HttpDelete, Route("loans/{id}")]
        public async Task<IActionResult> DeleteLoans(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"DELETE FROM HumanResources WHERE RecordNumber = @id AND [Group]='LOANITEMS'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpGet, Route("permroster/{name}")]
        public async Task<IActionResult> GetPermRoster(string name)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Recordno] As [RecordNumber], [Client Code] As [Recipient], [Date] As [Date], [Start Time] As [Start Time],
                        (([Duration] * 5)/60) As [Duration],[Service Type] As [Activity],
                        CASE WHEN [Carer Code] = '!MULTIPLE' THEN [ServiceSetting] ELSE [Carer Code] END  As [Staff/Loctn], [Program] As [Program],
                        [HACCType] As [Dataset Type],([CostQty] * [Unit Pay Rate]) As [Service Cost]
                        FROM Roster
                        WHERE [Client Code] = @name 
                        AND [Date] BETWEEN '1900/01/01' AND '1920/12/31'
                        ORDER BY Date, [Start Time]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@name", name);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            StartTime = GenSettings.Filter(rd["Start Time"]),
                            Duration = GenSettings.Filter(rd["Duration"]),
                            Activity = GenSettings.Filter(rd["Activity"]),
                            StaffLocation = GenSettings.Filter(rd["Staff/Loctn"]),
                            Program = GenSettings.Filter(rd["Program"]),
                            DataSet = GenSettings.Filter(rd["Dataset Type"]),
                            ServiceCost = GenSettings.Filter(rd["Service Cost"])
                        });
                    }
                    return Ok(new
                    {
                        list = list,
                        success = true
                    });
                }
            }
        }

        [HttpPost, Route("history/{name}")]
        public async Task<IActionResult> GetHistory([FromBody] FiltersDto filters, string name)
        {
            var query = (from r in _context.Roster
                         join it in _context.ItemTypes on r.ServiceType equals it.Title
                         join hr in _context.HumanResourceTypes on r.Program equals hr.Name
                         where r.ClientCode == name && r.YearNo > 2000 && r.Type == 7
                         orderby r.Date descending
                         select new { r, hr, it });



            if (filters != null && !string.IsNullOrEmpty(filters.LimitTo) && !string.IsNullOrEmpty(filters.StartingWith))
            {
                if(filters.LimitTo == "CARE DOMAIN")
                {
                    query = query.Where(x => x.hr.HRT_DATASET.CompareTo(filters.StartingWith) >= 0 && x.hr.HRT_DATASET.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                }

                if(filters.LimitTo == "CREATOR")
                {
                    query = query.Where(x =>  x.r.Editer.CompareTo(filters.StartingWith) >= 0 && x.r.Editer.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                }

                if (filters.LimitTo == "DISCIPLINE")
                {
                    query = query.Where(x => x.it.dataSet.CompareTo(filters.StartingWith) >= 0 && x.it.dataSet.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                }

                if (filters.LimitTo == "PROGRAM")
                {
                    query = query.Where(x => x.r.Program.CompareTo(filters.StartingWith) >= 0 && x.r.Program.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                }

                if (filters.LimitTo == "ROSTER/SVC GROUP")
                {
                    query = query.Where(x => x.hr.HRT_DATASET.CompareTo(filters.StartingWith) >= 0 && x.hr.HRT_DATASET.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                }
            }

            if(filters != null && filters.Display > 0)
            {
                query = query.Take(filters.Display);
            }
            else
            {
                query = query.Take(1000);
            }

            var result = query.Select(x => new
            {
                RecordNumber = x.r.RecordNo,
                Date = x.r.Date,
                Program = x.r.Program,
                Event = x.r.ServiceType,
                Staff = x.r.CarerCode,
                Amount = x.r.BillQty * x.r.UnitBillRate,
                Notes = x.r.Notes
            });


            return Ok(new
            {
                list = result.ToList(),
                success = true
            });
        }

        [HttpPut("casenotes/{recordNo}")]
        public async Task<IActionResult> UpdateCaseNote([FromBody] PostNote note, int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE History SET Detail = @note, AlarmDate = @alarmDate, ExtraDetail2 = @category, Program = @program, Discipline = @discipline,
                    PrivateFlag = @privateflag, Restrictions = @restrictions, CareDomain = @careDomain, PublishToApp = @publishToApp WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@note", note.Notes);
                    cmd.Parameters.AddWithValue("@alarmDate", DateFilter.Convert((DateTime?)note.AlarmDate));
                    cmd.Parameters.AddWithValue("@restrictions", note.Restrictions);
                    cmd.Parameters.AddWithValue("@privateflag", note.RestrictionsStr == "workgroup" ? 1 : note.RestrictionsStr == "public" ? 0 : 0);
                    cmd.Parameters.AddWithValue("@category", note.Category);
                    cmd.Parameters.AddWithValue("@program", note.Program);
                    cmd.Parameters.AddWithValue("@discipline", note.Discipline);
                    cmd.Parameters.AddWithValue("@careDomain", note.CareDomain);
                    cmd.Parameters.AddWithValue("@publishToApp", note.PublishToApp);
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    //return Ok(note);
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("casenotes/{id}")]
        public async Task<IActionResult> DeleteCaseNotes(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"DELETE FROM History WHERE RecordNumber = @id AND [ExtraDetail1]='CASENOTE'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("archivecasenotes/{id}")]
        public async Task<IActionResult> ArchiveCaseNotes(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE  History SET DeletedRecord = 1 WHERE RecordNumber = @id AND [ExtraDetail1]='CASENOTE'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPost("casenotes/{personId}")]
        public async Task<IActionResult> PostCaseNote([FromBody] PostNote note, string personId)
        {
            using(var ctx = _context)
            {
                ctx.History.Add(new History(){
                    PersonID = Validators.ValidateValue<string>(personId),
                    DetailDate = DateTime.Now,
                    Detail = Validators.ValidateValue<string>(note.Notes),
                    AlarmDate = Validators.ValidateDate(note.AlarmDate),
                    ExtraDetail1 = note.type==null ||  note.type==""  ? "CASENOTE" : note.type,
                    ExtraDetail2 = Validators.ValidateValue<string>(note.Category),
                    WhoCode = Validators.ValidateValue<string>(note.WhoCode),
                    Creator = "SYSMGR",
                    Restrictions = Validators.ValidateValue<string>(note.Restrictions),
                    PublishToApp = Validators.ValidateValue<Boolean>(note.PublishToApp),
                    CareDomain = Validators.ValidateValue<string>(note.CareDomain),
                    Discipline = Validators.ValidateValue<string>(note.Discipline),
                    Program = Validators.ValidateValue<string>(note.Program),
                });

                await ctx.SaveChangesAsync();
                return Ok(true);
            }
        }

        [HttpGet, Route("casenotes-dates")]
        public async Task<IActionResult> GetCaseNotesWithDates(ServiceNote note)
        {
            DateTime startDate = Convert.ToDateTime(note.StartDate);
            DateTime endDate = Convert.ToDateTime(note.EndDate);

            using(var ctx = _context)
            {
                // && history.PublishToClientPortal
                var casenotes = await (from history in _context.History
                        where history.PersonID == note.Client && history.ExtraDetail1 == "CASENOTE" && history.DeletedRecord != true && (history.DetailDate >= startDate && history.DetailDate <= endDate)
                        orderby history.DetailDate descending, history.RecordNumber descending 
                            select new {
                                PersonID = history.PersonID,
                                RecordNumber = history.RecordNumber,
                                PrivateFlag = history.PrivateFlag,
                                WhoCode = history.WhoCode,
                                DetailDate = history.DetailDate,
                                Creator = history.Creator,
                                Detail = DatabaseContext.RTF2Text(history.Detail),
                                DetailOriginal = history.Detail,
                                ExtraDetail2 = history.ExtraDetail2,
                                Restrictions = history.Restrictions,
                                AlarmDate = history.AlarmDate,
                                Program = history.Program,
                                Discipline = history.Discipline,
                                CareDomain = history.CareDomain,
                                PublishToApp = history.PublishToApp,
                                PublishToClientPortal = history.PublishToClientPortal
                            }).ToListAsync();

                return Ok(new {
                    list = casenotes,
                    success = true
                });
            }
        }

        [HttpPost, Route("casenotes-with-filters/{id}")]
        public async Task<IActionResult> GetCaseNotesWithFilters([FromBody] FiltersDto filters, string id)
        {
         
                var notes = (from hi in _context.History
                                        join re in _context.Recipients on hi.PersonID equals re.UniqueID into joinedTable     
                                            from subTable in joinedTable.DefaultIfEmpty()                                      
                                                where subTable.AccountNo == id && string.Equals(hi.ExtraDetail1, "CASENOTE") && hi.DeletedRecord != true
                                                    orderby hi.DetailDate descending, hi.RecordNumber descending
                                                    select new
                                                    {
                                                        RecordNumber = hi.RecordNumber,
                                                        PersonId = hi.PersonID,
                                                        PrivateFlag = Validators.ValidateValue<Boolean>(hi.PrivateFlag),
                                                        WhoCode = hi.WhoCode,
                                                        DetailDate = Validators.ValidateDate(hi.DetailDate),
                                                        Detail = DatabaseContext.RTF2Text(hi.Detail),
                                                        DetailOriginal = hi.Detail,
                                                        ExtraDetail2 = hi.ExtraDetail2,
                                                        AlarmDate = Validators.ValidateDate(hi.AlarmDate),
                                                        Creator = hi.Creator,
                                                        Category = hi.ExtraDetail2,
                                                        Restrictions = hi.Restrictions,
                                                        Program = hi.Program,
                                                        Discipline = hi.Discipline,
                                                        CareDomain = hi.CareDomain,
                                                        PublishToApp = Validators.ValidateValue<Boolean>(hi.PublishToApp)
                                                    });

                if (filters != null && !filters.AllDates)
                {
                    notes = notes.Where(x => x.DetailDate > filters.StartDate && x.DetailDate <= filters.EndDate);
                }

                if (filters != null && !string.IsNullOrEmpty(filters.LimitTo) && !string.IsNullOrEmpty(filters.StartingWith))
                {
                    if (filters.LimitTo == "CARE DOMAIN")
                    {
                        notes = notes.Where(x => x.CareDomain.CompareTo(filters.StartingWith) >= 0 && x.CareDomain.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "CATEGORY")
                    {
                        notes = notes.Where(x => x.ExtraDetail2.CompareTo(filters.StartingWith) >= 0 && x.ExtraDetail2.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "CREATOR")
                    {
                        notes = notes.Where(x => x.Creator.CompareTo(filters.StartingWith) >= 0 && x.Creator.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "DISCIPLINE")
                    {
                        notes = notes.Where(x => x.Discipline.CompareTo(filters.StartingWith) >= 0 && x.Discipline.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }

                    if (filters.LimitTo == "PROGRAM")
                    {
                        notes = notes.Where(x => x.Program.CompareTo(filters.StartingWith) >= 0 && x.Program.CompareTo(filters.StartingWith + "zzzzzzzzzzzz") <= 0);
                    }
                }

                if (filters != null && filters.Display > 0)
                {
                    notes = notes.Take(filters.Display);
                }
                else
                {
                    notes = notes.Take(1000);
                }


                return Ok(new
                {
                    list = await notes.ToListAsync(),
                    success = true
                });
        }

        [HttpGet("hideportalbalance/{uname}")]
        public async Task<IActionResult> GetHidePortalBalance(string uname)
        {
            var hideBalance =  await (from ui in _context.UserInfo where ui.Name == uname select ui.HidePortalBalance).FirstOrDefaultAsync();

            if(hideBalance.HasValue)
            {
                return Ok(hideBalance);
            }

            return Ok();
        }

        [HttpGet, Route("casenotes/{id}")]
        public async Task<IActionResult> GetCaseNotes(string id)
        {
         
                var notes = await (from hi in _context.History
                                        join re in _context.Recipients on hi.PersonID equals re.UniqueID into joinedTable     
                                            from subTable in joinedTable.DefaultIfEmpty()                                      
                                                where subTable.AccountNo == id && string.Equals(hi.ExtraDetail1, "CASENOTE") && hi.DeletedRecord != true
                                                    orderby hi.DetailDate descending, hi.RecordNumber descending
                                                    select new
                                                    {
                                                        RecordNumber = hi.RecordNumber,
                                                        PersonId = hi.PersonID,
                                                        PrivateFlag = Validators.ValidateValue<Boolean>(hi.PrivateFlag),
                                                        WhoCode = hi.WhoCode,
                                                        DetailDate = Validators.ValidateDate(hi.DetailDate),
                                                        Detail = DatabaseContext.RTF2Text(hi.Detail),
                                                        DetailOriginal = hi.Detail,
                                                        AlarmDate = Validators.ValidateDate(hi.AlarmDate),
                                                        Creator = hi.Creator,
                                                        Category = hi.ExtraDetail2,
                                                        Restrictions = hi.Restrictions,
                                                        Program = hi.Program,
                                                        Discipline = hi.Discipline,
                                                        CareDomain = hi.CareDomain,
                                                        PublishToApp = Validators.ValidateValue<Boolean>(hi.PublishToApp)
                                                    }).ToListAsync();              
                return Ok(new
                {
                    list = notes,
                    success = true
                });          
        }

        [HttpGet, Route("reminders/{id}")]
        public async Task<IActionResult> GetReminders(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, PersonID, [Name], Email, HR.Date1, HR.Date2, HR.Notes, HR.Recurring, HR.Address1, HR.Address2 FROM HumanResources HR INNER JOIN Recipients R ON R.UniqueID = HR.[PersonID] 
                    WHERE R.AccountNo = @id AND HR.[Type] = 'RECIPIENTALERT' AND IsNull(ReminderScope, '') <> 'CC' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Reminders> list = new List<Reminders>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new Reminders()
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            PersonID = GenSettings.Filter(rd["PersonID"]),
                            Name = GenSettings.Filter(rd["Name"]),
                            Email = GenSettings.Filter(rd["Email"]),
                            Date1 = GenSettings.Filter(rd["Date1"]),
                            Date2 = GenSettings.Filter(rd["Date2"]),
                            Address1 = GenSettings.Filter(rd["Address1"]),
                            Address2 = GenSettings.Filter(rd["Address2"]),
                            Notes = GenSettings.Filter(rd["Notes"]),
                            Recurring = GenSettings.Filter(rd["Recurring"]),
                        });
                    }
                    return Ok(new
                    {
                        list = list,
                        success = true
                    });
                }
            }
        }

        [HttpGet, Route("contacts/kin/{id}")]
        public async Task<IActionResult> GetContactsKin(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT HR.RecordNumber, 'LOCAL' AS [Source], HR.STATE, HR.PersonID, HR.[Group] AS [Category], CAST(0 AS BIT) AS APrimary, UPPER(HR.[Type]) AS Type,UPPER(HR.[SubType]) AS [Contact Type]
                    ,HR.[Name] AS [Contact Name] ,'No-one' AS [Connected To]
                    ,CASE 
                        WHEN ISNULL(HR.Phone1, '') <> ''
                            THEN '(p)' + HR.Phone1
                        ELSE ''
                        END + CASE 
                        WHEN ISNULL(HR.Phone1, '') <> ''
                            AND ISNULL(HR.Phone2, '') <> ''
                            THEN ', (p)' + HR.Phone2
                        ELSE CASE 
                                WHEN ISNULL(HR.Phone2, '') <> ''
                                    THEN '(p)' + HR.Phone2
                                ELSE ''
                                END
                        END + CASE 
                        WHEN ISNULL(HR.Phone1, '') <> ''
                            AND ISNULL(HR.Phone2, '') <> ''
                            AND ISNULL(HR.Mobile, '') <> ''
                            THEN ', (m)' + HR.Mobile
                        ELSE CASE 
                                WHEN ISNULL(HR.Mobile, '') <> ''
                                    THEN '(m)' + HR.Mobile
                                ELSE ''
                                END
                        END + CASE 
                        WHEN ISNULL(HR.Phone1, '') <> ''
                            AND ISNULL(HR.Phone2, '') <> ''
                            AND ISNULL(HR.Mobile, '') <> ''
                            AND ISNULL(HR.Email, '') <> ''
                            THEN ', (e)' + HR.Email
                        ELSE '(e)' + IsNull(HR.Email, '')
                        END AS Phones
                    ,CASE 
                        WHEN HR.Address1 <> ''
                            AND HR.Address1 IS NOT NULL
                            THEN HR.ADDRESS1
                        ELSE ''
                        END + ' ' + CASE 
                        WHEN HR.Address2 <> ''
                            AND HR.Address2 IS NOT NULL
                            THEN HR.ADDRESS2
                        ELSE ''
                        END + ' ' + CASE 
                        WHEN HR.Suburb <> ''
                            AND HR.Suburb IS NOT NULL
                            THEN HR.Suburb
                        ELSE ''
                        END + ' ' + CASE 
                        WHEN HR.Postcode <> ''
                            AND HR.Suburb IS NOT NULL
                            THEN HR.Postcode
                        ELSE ''
                        END AS AddressDetails
                    FROM HumanResources HR
                    WHERE PersonID = @id
                    AND (
                        User1 IS NULL
                        OR User1 <> 'IMPORT'
                        )
                    AND [GROUP] IN (
                        'CONTACT'
                        ,'OTHER CONTACT'
                        )", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            Type = GenSettings.Filter(rd["Contact Type"]),
                            Name = GenSettings.Filter(rd["Contact Name"]),
                            Phone = GenSettings.Filter(rd["Phones"]),
                            Details = GenSettings.Filter(rd["AddressDetails"])
                        });
                    }
                    return Ok(new
                    {
                        list = list,
                        result = true
                    });
                }
            }
        }

        [HttpGet, Route("competencies/{uname}")]
        public async Task<IActionResult> GetCompetencies(string uname)
        {
            string sp = "sp_getspecificCompentencies_desireable";
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();
                List<UnfilteredCompetencies> listCompetencies = new List<UnfilteredCompetencies>();
                List<RecipientPreferences> listPreferences = new List<RecipientPreferences>();

                using (SqlCommand cmd = new SqlCommand(@sp, (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RecipientCode", uname);
                    cmd.Parameters.AddWithValue("@ListType", "ALL");

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            listCompetencies.Add(new UnfilteredCompetencies
                            {
                                Group = rd.GetString(0),
                                Description = rd.GetString(1),
                                PersonId = rd.GetString(2),
                                Domain = rd.GetString(3),
                                Status = false
                            });
                        }
                    }
                }

                using (SqlCommand cmd = new SqlCommand(@"SELECT hr.RecordNumber, hr.[Name] AS [Preference], hr.Notes FROM HumanResources hr INNER JOIN Recipients r ON r.UniqueID = hr.PersonID WHERE r.[AccountNo] = @uname AND hr.[Type]='RECIPATTRIBUTE' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@uname", uname);

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            listPreferences.Add(new RecipientPreferences()
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                Preference = GenSettings.Filter(rd["Preference"]),
                                Notes = GenSettings.Filter(rd["Notes"]),
                            });
                        }
                    }
                }

                // Get Preference and transform to a list
                var selectedPreference = listPreferences.Select(x => x.Preference).ToList();

                //var selectedPreference = listCompetencies.Select(x => x.Group).ToList();

                // Transform Status to True if list contains one of the selectedPreference values  
                var samp = listCompetencies.Select(x => new UnfilteredCompetencies()
                {
                    Description = x.Description,
                    Status = selectedPreference.Contains(x.Description) ? true : false,
                    Group = x.Group,
                    Domain = x.Domain,
                    PersonId = x.PersonId
                }).ToList();


                List<Competencies> competencyList = new List<Competencies>();

                var uniq_group = samp.Select(x => x.Group).Distinct();
                foreach (string group in uniq_group)
                {
                    competencyList.Add(new Competencies
                    {
                        GroupName = group,
                        CompetenciesList = samp.Where(x => x.Group == group).Select(x => new CompetencyObject
                        {
                            Description = x.Description,
                            Domain = x.Domain,
                            Selected = x.PersonId != "0"
                        }).ToList()
                    });
                }

                return Ok(competencyList);
            }
        }

        [HttpPost, Route("booking")]
        public async Task<IActionResult> AddBooking([FromBody] AddBooking booking)
        {
            string SqlStmt = "";

            var Roster_Date = booking.StartDate;
            var Roster_Time = booking.StartTime;
            var Roster_Duration = booking.Duration;
            var Clientcode = booking.ClientCode;
            var Service_Unit = booking.Service.ServiceUnit;
            var bookingType = booking.BookingType;
            var ServiceType = booking.Service.ServiceType;
            var Service_Program = booking.Service.ServiceProgram;
            var Service_BillRate = booking.Service.ServiceBillRate;
            var UserName = booking.Duration;
            var StaffCode = booking.StaffCode;
            var AnyProvider = booking.AnyProvider;
            var Notes = string.IsNullOrEmpty(booking.Notes) ? null : booking.Notes;

            string UserId = "0";
            string alret_detail = "";
            string RecordNo = "0";

            string INSERT_SQL_STATIC = "INSERT INTO ROSTER ([Client Code], [Carer Code], [Service Type], [Service Description], [Program], [Date], [Start Time], [Duration], [YearNo], [MonthNo], [Dayno], [BlockNo], [UBDRef], [Type], [Status], [Date Entered], [Transferred], [GroupActivity], [BillTo], [notes], [Unit Bill Rate], [Unit Pay Rate], [ShiftName], BillQty, [BillUnit], [datasetClient], Creator, Editer, [Batch#])";

            DateTime idate = DateTime.Parse(Roster_Date);

            string rdate = GenSettings.GetSqlDate(Roster_Date);
            double BlockNo = GenSettings.GetBlocks(Roster_Time);
            double durtation = GenSettings.GetBlocks(Roster_Duration);

            durtation = durtation - BlockNo;

            double billQty = 0;
            string BatchNo = "";

            SqlTransaction transaction = null;
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    int dayCounter = 0;

                    if(bookingType != "FourWeekly" && bookingType != "Fortnightly" && bookingType != "Weekly"){
                        string RecordNo_exists = await GenSettings.getValue("SELECT RecordNo from ROSTER", new List<KeyValuePair<string, string>>() {
                            new KeyValuePair<string, string>("Client Code", Clientcode),
                            new KeyValuePair<string, string>("start time", Roster_Time),
                            new KeyValuePair<string, string>("date", rdate)
                        });

                        if (RecordNo_exists == "") RecordNo_exists = "0";

                        if (Convert.ToInt64(RecordNo_exists) > 0)
                        {
                            return BadRequest(new Error(){
                                Message = "You already have a booking in that timeslot"
                            });
                        }
                    }

                    // if (bookingType != "Normal")
                    //     BatchNo = await GenSettings.getValue("SELECT ISNULL(Max([Batch#]),0)+1 FROM ROSTER");
                    // else
                    BatchNo = "NULL";

                    if (Service_Unit.ToUpper() == "HOUR")
                        billQty = durtation * 5 / 60;
                    else
                        billQty = 1;

                    string Service_Description = ServiceType;
                    string program = Service_Program;
                    int roster_type = 1;

                    if (!AnyProvider)
                    {
                        string RosterGroup = await GenSettings.getValue("SELECT RosterGroup FROM itemtypes", new List<KeyValuePair<string, string>>() {
                            new KeyValuePair<string,string>("title", ServiceType)
                        });
                        roster_type = GenSettings.TypeNumber(RosterGroup);
                    }

                    if (AnyProvider)
                    {
                        StaffCode = "BOOKED";
                    }

                    // SqlStmt = INSERT_SQL_STATIC + " VALUES('" + Clientcode + "','" + StaffCode + "','" + ServiceType + "','" + Service_Description + "'" + ",'" + program + "','" + rdate + "','" + Roster_Time + "'" + "," + durtation + "," + rdate.Substring(0, 4) + "," + rdate.Substring(5, 2) + "," + rdate.Substring(8, 2) + "," + BlockNo + ",Null," +
                    // roster_type + ",1,getDate(),0,0, '" + Clientcode + "','" + Notes + "'," + Service_BillRate + ",0,'" + StaffCode + "'," + billQty + ",'" + Service_Unit.ToUpper() + "','" + Clientcode + "','" + UserName.ToUpper() + "','" + UserName.ToUpper() + "'," + BatchNo + "); select @@IDENTITY;";

                    if (bookingType == "Monthly" || bookingType == "FourWeekly")
                    {
                        foreach(var PermanentBooking in booking.PermanentBookings){
                            var PermBookingsDate = GenSettings.GetSqlDate(PermanentBooking.Time);
                            var StartTime = (PermanentBooking.Time).ToString("HH:mm");

                            BlockNo = GenSettings.GetBlocks(StartTime);

                            var PermDuration = ((PermanentBooking.Quantity) / 5);
                            var Calculated_BillQty = Service_Unit.ToUpper() == "SERVICE" ? 1 : Math.Round(Convert.ToDecimal(PermDuration/12),2);
                            SqlStmt = SqlStmt + INSERT_SQL_STATIC + $@" VALUES ('{Clientcode}','{StaffCode}','{ServiceType}','{Service_Description}','{program}','{PermBookingsDate}','{StartTime}',{PermDuration},{PermBookingsDate.Substring(0, 4)},{PermBookingsDate.Substring(5, 2)},{PermBookingsDate.Substring(8, 2)},{BlockNo},Null,{roster_type}, 1, GETDATE(), 0, 0, '{Clientcode}','{Notes}',{Service_BillRate},0,'{StaffCode}',{Calculated_BillQty},'{Service_Unit.ToUpper()}','{Clientcode}','{UserName.ToUpper()}','{UserName.ToUpper()}',{BatchNo}); ";
                        }
                    }
                    else if (bookingType == "Fortnightly")
                    {
                        foreach(var PermanentBooking in booking.PermanentBookings){
                            var PermBookingsDate = GenSettings.GetSqlDate(PermanentBooking.Time);
                            var StartTime = (PermanentBooking.Time).ToString("HH:mm");

                            BlockNo = GenSettings.GetBlocks(StartTime);

                            var PermDuration = ((PermanentBooking.Quantity) / 5);
                            var Calculated_BillQty = Service_Unit.ToUpper() == "SERVICE" ? 1 : Math.Round(Convert.ToDecimal(PermDuration/12),2);
                            SqlStmt = SqlStmt + INSERT_SQL_STATIC + $@" VALUES ('{Clientcode}','{StaffCode}','{ServiceType}','{Service_Description}','{program}','{PermBookingsDate}','{StartTime}',{PermDuration},{PermBookingsDate.Substring(0, 4)},{PermBookingsDate.Substring(5, 2)},{PermBookingsDate.Substring(8, 2)},{BlockNo},Null,{roster_type}, 1, GETDATE(), 0, 0, '{Clientcode}','{Notes}',{Service_BillRate},0,'{StaffCode}',{Calculated_BillQty},'{Service_Unit.ToUpper()}','{Clientcode}','{UserName.ToUpper()}','{UserName.ToUpper()}',{BatchNo}); ";
                        }
                    }
                    else if (bookingType == "Weekly")
                    {
                        foreach(var PermanentBooking in booking.PermanentBookings){
                            var PermBookingsDate = GenSettings.GetSqlDate(PermanentBooking.Time);
                            var StartTime = (PermanentBooking.Time).ToString("HH:mm");

                            BlockNo = GenSettings.GetBlocks(StartTime);

                            var PermDuration = ((PermanentBooking.Quantity) / 5);
                            var Calculated_BillQty = Service_Unit.ToUpper() == "SERVICE" ? 1 : Math.Round(Convert.ToDecimal(PermDuration/12),2);
                            SqlStmt = SqlStmt + INSERT_SQL_STATIC + $@" VALUES ('{Clientcode}','{StaffCode}','{ServiceType}','{Service_Description}','{program}','{PermBookingsDate}','{StartTime}',{PermDuration},{PermBookingsDate.Substring(0, 4)},{PermBookingsDate.Substring(5, 2)},{PermBookingsDate.Substring(8, 2)},{BlockNo},Null,{roster_type}, 1, GETDATE(), 0, 0, '{Clientcode}','{Notes}',{Service_BillRate},0,'{StaffCode}',{Calculated_BillQty},'{Service_Unit.ToUpper()}','{Clientcode}','{UserName.ToUpper()}','{UserName.ToUpper()}',{BatchNo}); ";
                        }
                    } else {

                         SqlStmt = INSERT_SQL_STATIC + " VALUES('" + Clientcode + "','" + StaffCode + "','" + ServiceType + "','" + Service_Description + "'" + ",'" + program + "','" + rdate + "','" + Roster_Time + "'" + "," + durtation + "," + rdate.Substring(0, 4) + "," + rdate.Substring(5, 2) + "," + rdate.Substring(8, 2) + "," + BlockNo + ",Null," +
                            roster_type + ",1,getDate(),0,0, '" + Clientcode + "','" + Notes + "'," + Service_BillRate + ",0,'" + StaffCode + "'," + billQty + ",'" + Service_Unit.ToUpper() + "','" + Clientcode + "','" + UserName.ToUpper() + "','" + UserName.ToUpper() + "'," + BatchNo + "); select @@IDENTITY;";
                    }
                    //+ rst.Carer_Phone + "','" + rst.UBDRef + "'," + rst.Type + ",'" + rst.Status + "','" + rst.Anal + "'" + ",getDate(),'" + gen_setting.GetSqlDate(rst.Date_Timesheet) + "'," + rst.Transferred + "," + rst.GroupActivity + ",'" + rst.BilledTo + "','" + rst.Bill.Pay_Unit.ToUpper() + "'," + rst.Bill.Quantity + ",'" + rst.HACCType + "','" + rst.Bill.Pay_Unit.ToUpper() + "'," + rst.Bill.Quantity + "," + rst.Bill.Tax + ",'" + rst.ServiceSetting + "','" + rst.DATASETClient + "','" + rst.Creator + "','" + rst.Notes + "'" + ",'" + rst.Link + "'," + rst.HasServiceNotes + ",Null,Null,Null,Null,Null,Null,Null,null," + rst.DischargeReasonType + "'," + rst.Attendees + "); Select @@Identity";

                    SqlStmt = SqlStmt.Replace("False", "0");
                    SqlStmt = SqlStmt.Replace("True", "1");
                    SqlStmt = SqlStmt.Replace("'0001/01/01'", "NULL");
                    SqlStmt = SqlStmt.Replace(",,", ",NULL,");
                    SqlStmt = SqlStmt.Replace(",''", ",NULL");

                    // return Ok(new {
                    //     SqlStatement = SqlStmt
                    // });
                    // return Ok(true);

                    using (SqlCommand cmd = new SqlCommand(SqlStmt, (SqlConnection)conn, transaction))
                    {
                        if(bookingType == "Normal"){
                            RecordNo = cmd.ExecuteScalar().ToString();
                        } else {
                            await cmd.ExecuteNonQueryAsync();
                        }                        
                        //if(dr.Read()) RecordNo = dr.GetValue(0).ToString();
                    }

                    SqlStmt = "";

                    // if (bookingType != "Normal")
                    // {

                    //     //---------------Publishing Rosters---------------------------

                    //     // Get Published Date
                    //     var pubDate = DateTime.Parse(await GenSettings.PublishedDate());

                    //     // Initialize the direct date variable                                
                    //     rdate = GenSettings.GetSqlDate(Roster_Date);

                    //     var date = DateTime.Parse(Roster_Date);

                    //     // Loop 
                    //     while (date < pubDate)
                    //     {
                    //         SqlStmt = SqlStmt + INSERT_SQL_STATIC + " VALUES ('" + Clientcode + "','BOOKED','" + ServiceType + "','" + Service_Description + "'" + ",'" + program + "','" + rdate + "','" +
                    //                 Roster_Time + "'" + "," + durtation + "," + rdate.Substring(0, 4) + "," + rdate.Substring(5, 2) + "," + rdate.Substring(8, 2) + "," +
                    //                 BlockNo + ",Null," + roster_type + ",1,GETDATE(),0,0, '" + Clientcode + "','" + Notes + "'," + Service_BillRate + ",0,'" + StaffCode + "'," +
                    //                 billQty + ",'" + Service_Unit.ToUpper() + "','" + Clientcode + "','" + UserName.ToUpper() + "','" + UserName.ToUpper() + "'," + BatchNo + "); ";
                    //         date = date.AddDays(dayCounter);
                    //         rdate = GenSettings.GetSqlDate(date);
                    //     }

                    //     // rdate = GenSettings.GetSqlDate(Roster_Date);

                    //     // SqlStmt = "INSERT INTO ROSTER ([Client Code],[Carer Code],[Service Type],[Service Description],[Program],[Date],[Start Time],[Duration],[YearNo],[MonthNo],[Dayno],[BlockNo],[UBDRef],[Type],[Status], [Date Entered],[Transferred],[GroupActivity],[BillTo],[notes],[Unit Bill Rate],[Unit Pay Rate],[ShiftName], " +
                    //     // " BillQty,[BillUnit],[datasetClient],Creator,Editer,[Batch#] )" +
                    //     // " select [Client Code],[Carer Code],[Service Type],[Service Description],[Program]," +
                    //     // " convert(varchar,([date]+convert(datetime,'" + rdate + "')+ " + MonthlyFactor + "-(DATEPART(dw,[date]+convert(datetime,'" + rdate + "'))-DATEPART(dw,[date]))),111) as [Date] " +
                    //     // ",[Start Time],[Duration],[YearNo],[MonthNo],[Dayno],[BlockNo],[UBDRef],[Type],[Status], [Date Entered],[Transferred],[GroupActivity],[BillTo],[notes],[Unit Bill Rate],[Unit Pay Rate],[ShiftName], " +
                    //     // " BillQty,[BillUnit],[datasetClient],Creator,Editer,[Batch#] " +
                    //     // " from Roster where year([date])=1900 and [Batch#]=" + BatchNo +
                    //     // " and convert(varchar, ([date] + convert(datetime, '" + rdate + "') - (DATEPART(dw,[date] + convert(datetime, '" + rdate + "')) - DATEPART(dw,[date]))), 111) > convert(varchar,(convert(datetime, '" + rdate + "')), 111)" +
                    //     // " and convert(varchar,([date]+convert(datetime,'" + rdate + "')-(DATEPART(dw,[date]+convert(datetime,'" + rdate + "'))-DATEPART(dw,[date]))),111) <= (select RosterPublishedTo from Registration)";

                    //     // SqlStmt = SqlStmt + ";Update Roster set YearNo=year([date]),MonthNo=month([date]),dayNo=day([date]) where YearNo=1900 and [Batch#]=" + BatchNo;

                    //     if (SqlStmt != "")
                    //     {
                    //         using (SqlCommand cmd = new SqlCommand(SqlStmt, (SqlConnection)conn, transaction))
                    //         {
                    //             await cmd.ExecuteNonQueryAsync();
                    //         }
                    //     }
                    // }


                    //-------------------Adding Device Reminders for Booking---------------------------

                    SqlStmt = "";
                    if (!AnyProvider)
                    {
                        try
                        {
                            UserId = await GenSettings.getValue("SELECT RecNum FROM Userinfo", new List<KeyValuePair<string, string>>() {
                                new KeyValuePair<string, string>("staffCode", StaffCode)
                            });

                        }
                        catch { UserId = "0"; }

                        alret_detail = "Booking Request by " + Clientcode + " For Service " + ServiceType + " on Date : " + Roster_Date + " and Time " + Roster_Time + "-" + Roster_Duration;

                        SqlStmt = "Insert into devicereminders([UserID],[ActivationDateTime],[Detail],[Status],[StaffCode],[DestinationStaff],MessageGroup,ExternalID)" +
                        " Values(" + UserId + ",convert(varchar,getDate(),111),'" + alret_detail + "',1,'" + StaffCode + "','" + StaffCode + "','BOOKING'," + RecordNo + ")";
                    }

                    if (UserId != "" && UserId != "0")
                    {
                        using (SqlCommand cmd = new SqlCommand(SqlStmt, (SqlConnection)conn, transaction))
                        {
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    await GenSettings.InsertAuditAsync(new AuditDTO()
                    {
                        Operator = booking.Username,
                        ActionDate = DateTime.Now,
                        ActionOn = "ROSTER",
                        WhoWhatCode = RecordNo,
                        TraccsUser = booking.Username,
                        AuditDescription = "Booking Roster Added From TRACCS Web Portal"
                    }, conn, transaction);

                    string rosterEmail = await GenSettings.getValue("SELECT TOP 1 RosterEmail from Registration");

                    string primaryManager = await _clientService.GetPrimaryManager(booking.ClientCode);
                    //string recipientEmail = await GenSettings.getValue(@$"SELECT Detail FROM PhoneFaxOther WHERE [TYPE] = '<EMAIL>' AND PERSONID = '{booking.RecipientPersonId}'");
                    //string managerEmail = await GenSettings.getValue(@$"SELECT Detail FROM PhoneFaxOther WHERE [TYPE] = '<EMAIL>' AND PERSONID = '{booking.ManagerPersonId}'");

                    if (string.IsNullOrEmpty(rosterEmail) && string.IsNullOrEmpty(primaryManager)){
                        transaction.Commit();
                        return Ok(1);
                    }

                    List<MailboxAddress> mbAddresses = new List<MailboxAddress>();

                    if (!(string.IsNullOrEmpty(rosterEmail)))
                    {
                        mbAddresses.Add(new MailboxAddress("", rosterEmail));
                    }

                    if (!(string.IsNullOrEmpty(primaryManager)))
                    {
                        mbAddresses.Add(new MailboxAddress("", primaryManager));
                    }

                

                    try
                    {
                        var result = await _newEmailService.SendBookingNotification(mbAddresses, booking, billQty);

                        if (result.Status.IsSuccess)
                        {
                            transaction.Commit();
                            return Ok(new
                            {
                                result = result,
                                rosterEmail = rosterEmail,
                                primaryManager = primaryManager
                            });
                        }
                        else
                        {
                            transaction.Commit();
                            return Ok(new
                            {
                                result = "",
                                rosterEmail = rosterEmail,
                                primaryManager = primaryManager
                            });
                        }
                    } catch(Exception ex)
                    {
                        transaction.Commit();
                        return Ok(new
                        {
                            result = "",
                            rosterEmail = rosterEmail,
                            primaryManager = primaryManager
                        });
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }

        }

        [HttpPost, Route("user/address")]
        public async Task<IActionResult> PostAddress([FromBody] List<NamesAndAddresses> addresses)
        {
            try
            {
                using (var db = _context)
                {
                    foreach (var address in addresses)
                    {
                        var na = new NamesAndAddresses()
                        {
                            Address1 = address.Address1,
                            Description = $"<{address.Description}>",
                            Suburb = address.Suburb,
                            PostCode = address.PostCode,
                            Stat = address.Stat,
                            PersonID = address.PersonID,
                            Longditude = address.Longditude,
                            Latitude = address.Latitude,
                            GridRef = address.GridRef,
                            GoogleAddress = address.GoogleAddress
                        };
                        await db.NamesAndAddresses.AddAsync(na);
                    }
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost, Route("user/contact")]
        public async Task<IActionResult> PostContact([FromBody] List<PhoneFaxOther> contacts)
        {
            try
            {
                using (var db = _context)
                {
                    foreach (var contact in contacts)
                    {
                        var c = new PhoneFaxOther()
                        {
                            PersonID = contact.PersonID,
                            Type = $"<{contact.Type}>",
                            Detail = contact.Detail,
                            PrimaryPhone = contact.PrimaryPhone
                        };
                        await db.PhoneFaxOther.AddAsync(c);
                    }
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut, Route("user/name")]
        public async Task<IActionResult> UpdateName([FromBody] Recipients recipient)
        {
            try
            {
                Recipients _recipient = await (from p in _context.Recipients
                                               where p.AccountNo == recipient.AccountNo
                                               select p).SingleOrDefaultAsync();

                _recipient.FirstName = recipient.FirstName;
                _recipient.MiddleNames = recipient.MiddleNames;
                _recipient.SurnameOrg = recipient.SurnameOrg;
                _recipient.Gender = recipient.Gender;
                _recipient.Title = recipient.Title;

                _recipient.Recipient_Coordinator = recipient.Recipient_Coordinator;
                _recipient.AgencyIdReportingCode = recipient.AgencyIdReportingCode;
                _recipient.AgencyDefinedGroup = recipient.AgencyDefinedGroup;
                _recipient.URNumber = recipient.URNumber;
                _recipient.Branch = recipient.Branch;
                _recipient.UBDMap = recipient.UBDMap;
                _recipient.NDISNumber = recipient.NDISNumber;
                _recipient.PreferredName = recipient.PreferredName;
                 _recipient.Mobility = recipient.Mobility;
                _recipient.CarerStatus = recipient.CarerStatus;
                  _recipient.Alias = recipient.Alias;

                if (recipient.DateOfBirth != null)
                    _recipient.DateOfBirth = recipient.DateOfBirth;

                await _context.SaveChangesAsync();

                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut, Route("user/address")]
        public async Task<IActionResult> UpdateAddress([FromBody] List<NamesAndAddresses> addresses)
        {
            try
            {
                using (var db = _context)
                {

                    var dbAddresses = await (from ct in _context.NamesAndAddresses where ct.PersonID == addresses.FirstOrDefault().PersonID && ct.RecordNumber == addresses.FirstOrDefault().RecordNumber select ct).ToListAsync();

                    foreach (var contact in dbAddresses)
                    {
                        contact.PrimaryAddress = false;
                    }

                    await db.SaveChangesAsync();

                    foreach (var address in addresses)
                    {
                        var query = await (from na in _context.NamesAndAddresses
                                           where na.PersonID == address.PersonID
                                               && na.RecordNumber == address.RecordNumber
                                           select na).FirstOrDefaultAsync();
                        query.PostCode = address.PostCode;
                        query.Address1 = address.Address1;
                        query.Suburb = address.Suburb;
                        query.Description = $"<{address.Description}>";
                        query.Stat = address.Stat;
                        query.PrimaryAddress = address.PrimaryAddress;
                        query.Longditude = address.Longditude;
                        query.Latitude = address.Latitude;
                        query.GridRef = address.GridRef;
                        query.GoogleAddress = address.GoogleAddress;
                    }
                    await db.SaveChangesAsync();

                    return Ok(new
                    {
                        success = true
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut, Route("user/contact")]
        public async Task<IActionResult> UpdateContact([FromBody] List<PhoneFaxOther> contacts)
        {
            try
            {
            
                using (var db = _context)
                {
                    var dbContacts = await (from ct in _context.PhoneFaxOther where ct.PersonID == contacts.FirstOrDefault().PersonID && ct.RecordNumber == contacts.FirstOrDefault().RecordNumber select ct).ToListAsync();

                    foreach (var contact in dbContacts)
                    {
                        contact.PrimaryPhone = false;
                    }
                    await db.SaveChangesAsync();


                    foreach (var contact in contacts)
                    {
                        var query = await (from px in _context.PhoneFaxOther
                                           where px.PersonID == contact.PersonID
                                                && px.RecordNumber == contact.RecordNumber
                                           select px).FirstOrDefaultAsync();

                        query.Detail = contact.Detail;
                        query.Type = $"<{contact.Type}>";
                        query.PrimaryPhone = contact.PrimaryPhone;
                    }

                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete, Route("user/contact")]
        public async Task<IActionResult> DeleteContact(PhoneFaxOther contacts)
        {
            try
            {
                using (var db = _context)
                {

                    var query = await (from px in _context.PhoneFaxOther
                                       where px.PersonID == contacts.PersonID
                                           && px.RecordNumber == contacts.RecordNumber
                                       select px).FirstOrDefaultAsync();

                    db.PhoneFaxOther.Remove(query);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete, Route("user/address")]
        public async Task<IActionResult> DeleteAddress(NamesAndAddresses address)
        {
            try
            {
                using (var db = _context)
                {
                    var query = await (from na in _context.NamesAndAddresses
                                       where na.PersonID == address.PersonID
                                           && na.RecordNumber == address.RecordNumber
                                       select na).FirstOrDefaultAsync();
                    db.NamesAndAddresses.Remove(query);
                    await db.SaveChangesAsync();
                    return Ok(new
                    {
                        success = true
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost("preferences/{id}")]
        public async Task<IActionResult> PostPreferences(string id, [FromBody] string[] preferences)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand cmd = new SqlCommand(@"DELETE FROM HumanResources WHERE PersonID = @id AND [Type]='RECIPATTRIBUTE'", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        await cmd.ExecuteNonQueryAsync();
                    }


                    foreach (var preference in preferences)
                    {
                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HumanResources(PersonID,[Group],[Type],[Name],Notes) VALUES(@id,'RECIPATTRIBUTE','RECIPATTRIBUTE', @preference, '')", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@id", id);
                            cmd.Parameters.AddWithValue("@preference", preference);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    transaction.Commit();

                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        [HttpGet("preferences/{id}")]
        public async Task<IActionResult> PostPreferences(string id)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, Name AS [Preference], Notes FROM HumanResources WHERE PersonID = @id AND [Type]='RECIPATTRIBUTE' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                Preference = GenSettings.Filter(rd["Preference"]),
                                Notes = GenSettings.Filter(rd["Notes"]),
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }
  
       
        [HttpPost("email-coordinator")]
        public async Task<IActionResult> PostEmailCoordinator([FromBody] Email email)
        {
            try
            {
                var message = new MimeMessage();

                message.From.Add(
                    new MailboxAddress(
                        _emailConfiguration.SmtpOriginName,
                        _emailConfiguration.SmtpOriginEmail
                    ));


                if (_emailConfiguration.SmtpEnableSampleMail)
                {
                    message.Cc.Add(new MailboxAddress("SAMPLE-CC", _emailConfiguration.SmtpCCEmail));
                    message.ReplyTo.Add(new MailboxAddress("SAMPLE-REPLYTO", _emailConfiguration.SmtpReplyTo));
                    message.To.Add(new MailboxAddress("SAMPLE-TO", _emailConfiguration.SmtpRecipientEmail));
                }
                else
                {
                    message.Cc.AddRange(email.GetCCAddresses());
                    message.To.AddRange(email.GetToAddresses());
                }

                message.Subject = email.Subject;
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text =
                        string.Format("<div style='width: 400px; display: inline-block; padding: 0 1rem 1rem 1rem; border: 1px solid #dddddd; border-top: 2px solid #32bee6; border-radius: 2px; color: #3a3a3a;'> " +
                            "<h3 style='border-bottom: 1px solid #32bee6; padding: 5px 0'>An email from  {0}</h3> " +
                            "<p style='margin:0;'>{1}</p>" +
                        "</div>", email.RecipientName, email.Content)
                };

                var emailReq = new EmailOutput();
                emailReq = await _emailService.SendEmail(message);

                if (emailReq.Status.IsSuccess)
                {
                    if (_emailConfiguration.SmtpEnableSampleMail)
                    {
                        return Ok(emailReq);
                    }
                    return Ok(new EmailOutput()
                    {
                        ReplyTo = "",
                        CC = "",
                        To = "",
                        From = "",
                        Status = new IsSuccessEmail()
                        {
                            IsSuccess = true
                        }
                    });
                }

                return BadRequest(new
                {
                    message = emailReq
                });

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpGet("servicenotes")]
        public async Task<IActionResult> GetServiceNotes(ServiceNote note)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT r.recordno, r.Date, r.[Start Time], r.[Service Type], dbo.RTF2Text(nt.detail) AS Note, nt.ExtraDetail2 FROM Roster r 
                    LEFT JOIN History nt ON CONVERT(varchar (20), r.recordno) = nt.personid WHERE r.[Client Code] = @client
                    AND r.[Date] BETWEEN @sDate AND @eDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@sDate", note.StartDate);
                    cmd.Parameters.AddWithValue("@eDate", note.EndDate);
                    cmd.Parameters.AddWithValue("@client", note.Client);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                    int numCol = rd.FieldCount;

                    while (rd.Read())
                    {
                        // CREATE KEYVALUE DICTIONARY
                        Dictionary<string, object> dic = new Dictionary<string, object>();

                        // LOOP COLUMNS IN ROW
                        for (var a = 0; a < numCol; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), rd.GetValue(a) != DBNull.Value
                                ? rd.GetValue(a)
                                : "");
                        }

                        list.Add(dic);
                    }

                    var distinctStaff = list.Select(x => x["Date"]).Distinct();
                    List<KeyValuePair<string, object>> finalList = new List<KeyValuePair<string, object>>();


                    foreach (string staff in distinctStaff)
                    {
                        var staffData = list.Where(x => x["Date"].ToString() == staff).Select(x => x);
                        finalList.Add(new KeyValuePair<string, object>(staff.Trim(), staffData));
                    }
                    return Ok(finalList);
                }
            }
        }

			
		[HttpPost("addrefreminder")] 
        public async Task<IActionResult> addRefreminder([FromBody] SqlString sql)  
        {

            try{
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(sql.Sql, (SqlConnection)conn))
                    {
                        await conn.OpenAsync();

                        var getValue = await cmd.ExecuteScalarAsync();
                        // newId = (Int32) cmd.ExecuteScalar();

                        return Ok(getValue);
                    }
                }
            }catch(Exception ex)
            {
                return Ok(false);
            }
        }	
			
			


        [HttpPost("cancel-booking")]
        public async Task<IActionResult> PostCancelBooking([FromBody] CancelBooking booking)
        {
            string SqlStmt = string.Empty;
            string rdate = GenSettings.GetSqlDate(booking.RosterDate);
            double BlockNo = GenSettings.GetBlocks(booking.RosterTime);
            // double durtation = gen_setting.GetBlocks(Roster_Duration);

            double billQty = 0;
            string Service_Description = "";
            string Service_Rate = "";

            // This code would determine if its coming from a client manager or a client. VERY IMPORTANT!!!
            booking.Username = booking.Username ?? booking.TraccsUser;


            if (booking.ServiceType == "WithNotice")
            {
                Service_Description = "(select DefaultWithNoticeCancel  from humanresourcetypes where[Group] = 'PROGRAMS' and name = r.[Program])";
                Service_Rate = "r.[Unit Bill Rate]";
            }
            else if (booking.ServiceType == "NoNotice")
            {
                Service_Description = "(select DefaultNoNoticeCancel from humanresourcetypes where[Group] = 'PROGRAMS' and name == r.[Program]) ";
                Service_Rate = "(select Round((NoNoticeCancelRate/100)*r.[Unit Bill Rate],2)   from humanresourcetypes where[Group] = 'PROGRAMS' and name = = r.[Program])";
            }
            else if (booking.ServiceType == "ShortNotice")
            {
                Service_Description = "(select DefaultShortNoticeCancel  from humanresourcetypes where[Group] = 'PROGRAMS' and name = r.[Program]) ";
                Service_Rate = "(select Round((ShortNoticeCancelRate/100)*r.[Unit Bill Rate],2)  from humanresourcetypes where[Group] = 'PROGRAMS' and name = = r.[Program])";
            }

            int roster_type = 4;

            SqlStmt = "DELETE FROM roster WHERE RecordNo=" + booking.RecordNo + ";" +
                  "INSERT INTO ROSTER ([Client Code],[Carer Code],[Service Type],[Service Description],[Program],[Date],[Start Time],[Duration],[YearNo],[MonthNo],[Dayno],[BlockNo],[UBDRef],[Type],[Status], [Date Entered],[Transferred],[GroupActivity],[BillTo],[notes],[Unit Bill Rate],[Unit Pay Rate],[ShiftName], " +
                  " BillQty,[BillUnit],[datasetClient],Creator,Editer )" +
                  " select [Client code],'!INTERNAL'," + Service_Description + " as ServiceType,'Booking Cancellation' as ServiceDescription,r.[Program],'" + rdate + "','" + booking.RosterTime + "'" + ",[duration]," + rdate.Substring(0, 4) + "," + rdate.Substring(5, 2) + "," + rdate.Substring(8, 2) + "," + BlockNo + ",Null," +
                    roster_type + ",1,getDate(),0,0, BillTo,'Booking Cancellation'," + Service_Rate + ",0,[ShiftName],BillQty,BillUnit,[datasetClient],'" + booking.Username.ToUpper() + "','" + booking.Username.ToUpper() + "'" +
                    " from Roster r " +
                    " where RecordNo=" + booking.RecordNo + " and isnull(" + Service_Description + ",'')<>''";


            SqlStmt = SqlStmt.Replace("False", "0");
            SqlStmt = SqlStmt.Replace("True", "1");
            SqlStmt = SqlStmt.Replace("'0001/01/01'", "NULL");
            SqlStmt = SqlStmt.Replace(",,", ",NULL,");
            SqlStmt = SqlStmt.Replace(",''", ",NULL");

            var roster = await (from rt in _context.Roster where rt.RecordNo == Convert.ToInt32(booking.RecordNo) select rt).FirstOrDefaultAsync();
            
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand cmd = new SqlCommand(SqlStmt, (SqlConnection)conn, transaction))
                    {
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // ADD entry in Audit column
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO Audit(ActionDate,WhoWhatCode,AuditDescription,TraccsUser, Operator,ActionOn) VALUES(GETDATE(),@whocode,@auditdesc,@user,@operator,@action)", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@whocode", booking.RecordNo);
                        cmd.Parameters.AddWithValue("@auditdesc", "BOOKING CANCELLED");
                        cmd.Parameters.AddWithValue("@user", booking.TraccsUser);
                        cmd.Parameters.AddWithValue("@operator", booking.TraccsUser);
                        cmd.Parameters.AddWithValue("@action", "ROSTERS");
                        await cmd.ExecuteNonQueryAsync();
                    }
                    // Roster Email
                    string rosterEmail = await GenSettings.getValue("SELECT TOP 1 RosterEmail from Registration");

                    // Recipient Email
                    //string recipientEmail = await GenSettings.getValue(@$"SELECT Detail FROM PhoneFaxOther WHERE [TYPE] = '<EMAIL>' AND PERSONID = '{booking.RecipientPersonId}'");

                    // Primary Case Manager
                    string primaryManager = await _clientService.GetPrimaryManager(roster.ClientCode);

                    // Manager Email
                    //string managerEmail = await GenSettings.getValue(@$"SELECT Detail FROM PhoneFaxOther WHERE [TYPE] = '<EMAIL>' AND PERSONID = '{booking.ManagerPersonId}'");



                    if (string.IsNullOrEmpty(rosterEmail) && string.IsNullOrEmpty(primaryManager))
                    {
                        transaction.Commit();
                        return Ok(1);
                    }

                    List<MailboxAddress> mbAddresses = new List<MailboxAddress>();

                    if (!(string.IsNullOrEmpty(rosterEmail)))
                    {
                        mbAddresses.Add(new MailboxAddress("", rosterEmail));
                    }

                    if (!(string.IsNullOrEmpty(primaryManager)))
                    {
                        mbAddresses.Add(new MailboxAddress("", primaryManager));
                    }

                    //if (!(string.IsNullOrEmpty(managerEmail)))
                    //{
                    //    mbAddresses.Add(new MailboxAddress("", managerEmail));
                    //}

                    var result = await _newEmailService.SendBookingCancelNotification(mbAddresses, booking, roster);

                    if (result.Status.IsSuccess)
                    {
                        transaction.Commit();
                        return Ok(new {
                            result = result,
                            rosterEmail = rosterEmail,
                            primaryManager = primaryManager
                        });;
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }

                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }





        private static string getRating(string rating)
        {
            if (rating == "" || string.IsNullOrEmpty(rating)) rating = "1";
            string ratingStr = string.Empty;
            for (int i = 0; i < rating.Length; i++) ratingStr += "*";
            return ratingStr;
        }

        private static string GetOrdinalSuffix(int num)
        {
            string number = num.ToString();
            if (number.EndsWith("11")) return "th";
            if (number.EndsWith("12")) return "th";
            if (number.EndsWith("13")) return "th";
            if (number.EndsWith("1")) return "st";
            if (number.EndsWith("2")) return "nd";
            if (number.EndsWith("3")) return "rd";
            return "th";
        }
    }

    public class OutputRecipients
    {
        public string Code { get; set; }
        public string Id { get; set; }
        public string AgencyDefinedGroup { get; set; }
        public string View { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string Branch { get; set; }
        public string RecipientCoordinator { get; set; }


    }

   public class  Cald_Gender{
         public string Id { get; set; }
        public string Gender { get; set; }
    }
}
