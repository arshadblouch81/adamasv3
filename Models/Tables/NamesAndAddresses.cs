using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables
{
    public class NamesAndAddresses
    {
        [Key]
        public int RecordNumber { get; set; }
        public string PersonID { get; set; }
        public bool PrimaryAddress { get; set; }
        public string Description { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string Stat { get; set; }
        public string Type{get;set;}
        public string GridRef {get;set;}
        public string GoogleAddress {get;set;}
        public decimal? Latitude {get;set;}
        public decimal? Longditude {get;set;}

    }
}