﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Net.Http;
using Microsoft.Extensions.Options;
using AdamasV3.Models.Modules;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Adamas.DAL;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrintController : ControllerBase
    {
        private readonly JSReportConfiguration _appsettings;

        public PrintController(
                IOptions<JSReportConfiguration> appsettings
            )
        {
            _appsettings = appsettings.Value;
        }

        [HttpPost]
        public async Task<IActionResult> PostGeneratePrint([FromBody] JObject json)
        {
            // This would add dbconnection to the template based on the appsettings.json value
            JObject options = json["options"] as JObject;
            options.Add("dbconn", this._appsettings.ConnectionString);

            var content = JsonConvert.SerializeObject(json);
            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
            var byteContent = new ByteArrayContent(buffer);

            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var base64EncodedCredentials = GeneralService.Base64Encode($"{this._appsettings.Username}:{this._appsettings.Password}");


            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedCredentials);
                client.DefaultRequestHeaders.Add("Accept", "application/json");

                try
                {
                    HttpResponseMessage response = client.PostAsync(this._appsettings.JSReportUrl, byteContent).Result;
                    response.EnsureSuccessStatusCode();

                    // Read file as byte array
                    var responseBody = response.Content.ReadAsByteArrayAsync().Result;
                    var mimeTypeStr = UploadService.GetMimeType(".pdf");
                    return File(responseBody, mimeTypeStr, "generate.pdf");
                }
                catch (HttpRequestException ex)
                {
                    return BadRequest(ex);
                }
            }
        }
    }
}
