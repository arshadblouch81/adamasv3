import { Component,TemplateRef, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef,ViewChild,ElementRef, ViewContainerRef, ComponentRef, ComponentFactoryResolver } from '@angular/core'
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, switchMap } from 'rxjs/operators';
import format from 'date-fns/format';
import { NzModalModule, NzTransferModule, NzCardModule, NzButtonModule } from 'ng-zorro-antd';
import { GlobalService,staffSecondMenuRoutes,staffnodes,StaffService,sbFieldsSkill, ShareService,timeSteps,conflictpointList,checkOptionsOne,sampleList,genderList,statusList,leaveTypes, ListService,PrintService, TimeSheetService, SettingsService, LoginService } from '@services/index';
import { NzFormatEmitEvent } from 'ng-zorro-antd/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { EMPTY, forkJoin, Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApplicationUser } from '@modules/modules';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DomSanitizer } from '@angular/platform-browser';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ContextMenuComponent } from 'ngx-contextmenu';

interface Person {
  key: string;
  name: string;
  age: number;
  address: string;
}

interface UserView{
  staffRecordView: string,
  staff: number
}

@Component({
  styles: [`
  nz-tabset {
    margin-top: 1rem;
  }
  
  nz-tabset  ::ngdeep div  div.ant-tabs-nav-container {
    height: 25px !important;
    font-size: 13px !important;
  }
  
  nz-tabset  ::ngdeep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    height: 25px;
    border-radius: 15px 4px 0 0;
    margin: 0 -10px 0 0;
  }
  
  nz-tabset  ::ngdeep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
  }
  
  ul.options {
    list-style: none;
    margin: 0;
    padding: 1rem 0 0 0;
  }
  
  ul.options li {
    display: inline-block;
    margin-right: 6px;
    padding: 5px 0;
    font-size: 13px;
  }
  
  .disabled {
    cursor: not-allowed;
  }
  
  ul.sub-menu {
    list-style: none;
    padding: 0;
    margin: 0;
  }
  
  ul.sub-menu li {
    font-size: 12px;
    padding: 5px 15px;
    background: #85b9d5;
  }
  
  ul.sub-menu li:hover:not(.disabled) {
    color: black;
    background: #edf9ff;
  }
  
  li div {
    text-align: center;
  }
  
  .recipient-controls button {
    margin-right: 5px;
  }
  
  nz-select {
    width: 100%;
  }
  
  .options button:disabled {
    color: #a3a3a3;
    cursor: no-drop;
  }
  
  .options button:hover:not([disabled]) {
    color: #177dff;
    cursor: pointer;
  }
  
  ul li button {
    border: 0;
    background: #ffffff00;
    float: left;
  }
  
  div.special-btn {
    padding: 5px 1rem !important;
    position: relative;
    display: inline-block;
    background-color: #85b9d5;
    color: #FFF;
  }
  
  div.special-btn  div {
    position: absolute;
    border: 1px solid #dfdfdf;
    background: #fff;
    z-index: 10;
    top: 34px;
    left: 0;
    width: 100%;
    border-radius: 4px;
  }
  
  .special-btn {
    flex: 10%;
    font-size: 1rem;
    padding: 8px;
    margin-right: 5px;
    cursor: pointer;
    color: #afafaf;
    border-radius: 3px;
  }
  
  /**
  .status {
    font-size: 11 px;
    padding: 3 px 5 px;
    border-radius: 11 px;
    color: #fff;
    margin-right: 10 px;
  }
  .status.active {
    background: #42ca46;
  }
  
  .status.inactive {
    background: #c70000;
  }
  .status.type {
    background: #c8f2ff;
    color: black;
  } */
  
  .status-program {
    display: inline-block;
    float: left;
  }
  
  .status-program i {
    font-size: 1.4rem;
    color: #000;
    margin-right: 10px;
    cursor: pointer;
  }
  
  .status-program i:hover {
    color: #000;
  }
  
  .tree-overflow {
    max-height: 24rem;
    overflow: auto;
  }
  
  label.columns {
    display: block;
    margin: 0;
  }
  
  :host .ant-card-small  .ant-card-head  .ant-card-head-wrapper  .ant-card-extra {
    margin-left: unset !important;
    float: none !important;
    color: green !important;
  }
  
  .ant-table-thead  tr  th {
    background: green;
  }
  nz-tabset{
    margin-top:1rem;
  }
  .staff-controls button{
    margin-right:6px;
    height: 33px;
    width: 88px
  }
  .staff-controls botton:disabled{
    background:#FFBA08;
  }
  
  
  nz-tabset ::ngdeep  div  div.ant-tabs-nav-container{
    height: 25px !important;
    font-size: 13px !important;
  }
  
  nz-tabset ::ngdeep  div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
    line-height: 24px;
    height: 25px;
  }
  nz-tabset ::ngdeep  div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
    background: #85B9D5;
    color: #fff;
  }
  nz-tabset ::ngdeep  div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
    border-radius: 15px 4px 0 0;
    margin:0 -10px 0 0;
  }
  ul.old-list-wrapper{
    list-style:none;
    float:right;
    margin:0;
  }
  ul.old-list-wrapper li{
    display: inline-block;
    margin-right: 10px;
    font-size: 12px;
    padding: 5px;
    cursor:pointer;
  }
  ul.old-list-wrapper li div{
    text-align: center;
    font-size: 17px;
  }
  .terminate:hover{
    color: #db2929;
  }
  .leave:hover{
    color: #1488db;
  }
  .checks label{
    display:block;
    margin:10px;
  }
  .spinner{
    margin:1rem auto;
    width:1px;
  }
  .drawer{           
    padding: 0px;
    width:100%
  }
  .status{
    font-size: 11px;
    padding: 3px 5px;
    border-radius: 11px;
    color: #fff;
    
    margin-right: 10px;
  }
  .hide{
    display:none;
  }
  .status.active{            
    background: #42ca46;
  }
  .status.inactive{            
    background: #c70000;
  }
  .status.type{
    background:#c8f2ff;
    color: black;
  }
  .status-program{
    display: inline-block;
    float: left;
    margin-right:1rem;
  }
  .status-program i{
    font-size: 1.4rem;
    color: #bfbfbf;
    margin-right:10px;
    cursor:pointer;
  }
  .status-program i:hover{
    color: #000;
  }
  
  .tree-overflow{
    max-height: 24rem;
    overflow: auto;
  }
  label.columns{
    display:block;
    margin:0;
  }
  :host .ant-card-small .ant-card-head .ant-card-head-wrapper .ant-card-extra {
    margin-left:unset !important;
    float:none !important;
    color:green !important;
  }
  :host .ant-table-thead tr th{
    background:green;
  }
  
  :host div.special-btn{
    padding: 8px 2rem !important;
    position:relative;
    display:inline-block;
  }
  :host div.special-btn  div{
    position: absolute;
    border: 1px solid #dfdfdf;
    background: #fff;
    z-index: 10;
    top: 34px;
    left: 0;
    width: 10rem;
    border-radius:4px;
  }
  .special-btn{
    flex: 10%;
    font-size: 1.1rem;
    padding: 8px;
    margin-left: 10px;
    border: 1px solid #dadada;
    border-radius: 7px;
    cursor: pointer;
    color: #afafaf;
  }
  
  ul.sub-menu{
    list-style:none;
    padding:0;
    margin:0;
  }
  ul.sub-menu li{
    font-size:12px;
    padding:5px 15px;
  }
 
  ul.sub-menu li:hover:not(.disabled){
    color:black;
    background:#edf9ff;
  }
  .disabled{
    background-color:#D6D3D3;
    color: #4E4D49;
    background-color: #D6D3D3;
    font-size: 12px;
    font-family: 'Segoe UI';
    font-weight:600;
    
  }
    
  .disabledmenulist{
    cursor:not-allowed;
    color:grey !important;
  }
    
  .hide {
    display: none;
  }
  
  .status-wrapper {
    color: #fff;
    border-radius: 10px;
    background: #85b9d5;
    padding: 5px 10px;
    font-size: 1rem;
    font-weight: 700;
    margin-left: 10px;
    display: inline-block;
  }
  
  ul.main-list {
    background: #002060 !important;
     height:fit-content;
    
    
  }
  
  nz-sider {
    background: #002060;
  }
  
  ul.main-list:disabled
  {
    color:grey;
    background: transparent !important;
  }
  
  .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected, .ant-menu.ant-menu-dark .ant-menu-item-selected {
    background-color: #85B9D5;
    color: #004165;
  }
  
  .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected, .ant-menu.ant-menu-dark .ant-menu-item-selected i {
    color: #002060;
  }
  
  .ant-menu-dark, .ant-menu-dark .ant-menu-sub {
    background: #002060;
  }
  nz-sider ::ngdeep  .ant-menu-submenu:hover{
    background-color: #85B9D5;
  }
  nz-sider ::ngdeep  .ant-menu-dark .ant-menu-submenu-title:hover {
    color: #004165 !important;
  }
  
  nz-sider ::ngdeep  .ant-menu-submenu-ul {
    background: #002060;
  }
  .switch {
    padding-top: 5px;
    margin-left: 1px;
    min-width:7vw;
    background: #85b9d5;
    color: #fff;
    border-radius: 3px;
    display: flex;
    flex-direction: row;
    justify-content: center;
  }
  
  .switch > div > span {
    width: 5rem;
    text-align: center;
    margin: 3px 10px;
    font-weight: 500;
  }
  
  nz-switch ::ngdeep  button.ant-switch-checked {
    background-color: #85b9d5 !important;
  }
  
  .ant-menu-vertical .ant-menu-item{
    height:30px;
    line-height:30px;
    color:#fff;
  }
    
  /* Set outer and inner divs to fit within viewport */
  
  .outer-div {
                      /* 5% of the viewport height for padding */
    border: 2px solid #85B9D5;
    display:block;   
    align-items: left;
     width : 73.5vw;                 /* width:calc(80vw - 30px);*/
    height:84vh;      /* 80vh;  90% of the viewport height for the outer div */
    padding: 5px;
   
  }
                      /* display: flex;*/
                      /* justify-content: center;*/
  .inner-div {
    border: 3px solid #dbdee2;
    display:block;
    padding:0px 10px;
    height:45.5rem;
    width:73vw;
    overflow-y: auto;           /* Add scrollbars if needed */
  }
  
  .top-text {
    padding: 2px 5px;
    text-align: left;
    background-color: #e9f7ff;
    color: #635f5f;
    width: -moz-fit-content;
    width: fit-content;
    font-family: 'Segoe UI';
    font-size: 15px;
    font-weight: 600;
  }
  
  
  @media (max-width: 767px) {
    .outer-div {
      padding: 10px;
      height: auto;
    }
    .inner-div {
      width: 100%; /* Make inner div 100% of parent width */
      height:auto
    }
  }
  
  
  .recipient-search{      
      display: block;       
     
      min-width:15vw;
      max-width:15vw;
      min-height: calc(87vh - 20px); /*80vh;  90% of the viewport height for the outer div */
      
     
    }
    .recipient-tabs{
      display:block;
      background : #002060;
      min-width: 10vw;      
      overflow: hidden;
      height:84vh;
      border-right: 1px solid #e7e7e7;
     
    }
   
     
    .recipient-main-container{      
      display: flex; 
      flex-direction: column;    
      /* overflow: auto;*/
      padding: 2px;
      
     
    }
    
    .height-main-container{      
      display: flex; 
      flex-direction: row;       
      
     
    }
    
    .height-main-properties{
      border: 1px solid #e7e7e7;
      overflow: hidden;
    }
    .code{
      font-family: 'Segoe UI';
      font-weight: bold;
      font-size: 15px;
    }
    .leveloneblock{
      margin-bottom:20px;
    }
    .title{
      font-family: 'Segoe UI';
      font-weight: bold;
      font-size: 21px; 
    }
    .contact-info-box2{
      width:59px
    }
    
    .switch-label{
    background-color:  #F18805; vertical-align: middle; height: 33px;    color: #fff; padding: 5px; text-align: center;
    border : 1px solid #F18805;
    border-top-left-radius: 3px;   
    border-bottom-left-radius: 3px;    
    border-right: none;
    width:60px;
    font-size:12px;
    font-family: 'segoe ui';
    font-weight:bold;
  }
    
  .switch-label2{
    background-color:  #85B9D5; vertical-align: middle; height: 33px;   color: #fff; padding: 5px; text-align: center;
    border : 1px solid #85B9D5;     
    border-top-right-radius: 3px;    
    border-bottom-right-radius:3px;
    border-left: none;
    width:60px;
    font-size:12px;
    font-family: 'segoe ui';
    font-weight:bold;
  }
    
  .deleted{
    background-color:  #85B9D5  ;
    padding-left: 2px; 
    padding-top: 4px; 
    color: white;  width:80px; text-align: center; vertical-align: middle; border-top-left-radius: 5px; border-bottom-left-radius: 3px; 
 }
    .active{
      background-color:  #ebebeb; vertical-align: middle; height: 33px;   color: #fff; padding: 5px; text-align: center;
      border : 1px solid #D6D3D3;
      width:60px;
      font-size:12px;
      font-family: 'segoe ui';
      font-weight:bold;
      color: #fff;
      border-top-left-radius: 3px;   
      border-bottom-left-radius: 3px;
  }
    
  .togglebutton{
     
     height: 33px !important;
     text-align: center;
     vertical-align: middle;     
     font-size: 10px;
     font-family: 'Segoe UI';
     border : 1px solid rgb(150, 148, 148);
     background-color: #e5e5e5;
     margin-left:1px;
     border-radius : 0px;
   
 }
    #left-search-bar label{
    color:#fff}
    
    ::ng-deep .search-modal .ant-modal-body {
      height: calc(100vh - 100px);
      overflow-y: auto;
      padding: 8px 0px;
    }
  ::ng-deep .search-modal .ant-modal-footer {
    padding: 0 !important; /* Remove padding */
    border-top: none !important; /* Optional: Remove border */
    text-align: center; /* Optional: Adjust alignment */
  }

    
.custom-table-container {
  max-height: 590px; /* Sets vertical scroll */
  overflow-y: auto;
  border: 1px solid #ddd;
}
    
.custom-table {
  width: 100%;
  table-layout: fixed; /* Fix column width */
  border-collapse: collapse;
}
    
.custom-table th,
.custom-table td {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  border: 1px solid #ddd;
  padding: 4px;
  text-align: left;
}
    
.custom-table th {
  font-weight: bold;
}
    .loading-overlay {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgba(255, 255, 255, 0.8);
  z-index: 10;
}
    
.custom-table-container {
  position: relative;
}
     .custom-table tbody  tr:nth-child(odd) {
    background-color: #E9F7FF;
      }
        .custom-table tbody  tr:nth-child(even) {
            background-color: #fff;
        }
    
div.cdk-overlay-container div.cdk-overlay-connected-position-bounding-box div context-menu-content div li.disabled {
  background: white;
  color:grey
}
div.cdk-overlay-container div.cdk-overlay-connected-position-bounding-box div context-menu-content div li.disabled:hover {
  background: white;
  color:grey
}
 ::ng-deep .ant-checkbox-inner {
                    border: 2px solid #002060 !important;
                }

`],
    templateUrl: './staff.html',
    changeDetection: ChangeDetectionStrategy.OnPush
  })
  
  
  export class StaffAdmin implements OnInit, OnDestroy {
    
    @ViewChild('menuSub') menuSub: ElementRef;
    @ViewChild('toggleMenu') toggleMenu: ElementRef
    
    @ViewChild('SearchListComponent')  SearchListComponent: any
    @ViewChild('modalTemplate', { static: true }) modalTemplate!: TemplateRef<any>; // Template reference
    @ViewChild('modalTemplate1', { static: true }) modalTemplate1!: TemplateRef<any>; // Template reference
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
    loadUnallocateStaff :Subject<any> = new Subject() ;
    
    user: any = null;
    nzSelectedIndex: number = 0;
    
    isFirstLoad: boolean = false;
    isConfirmLoading: boolean = false;
    sample: any;
    
    terminateModal: boolean = false;
    changeCodeModal: boolean = false;
    searchAvaibleModal : boolean = false;
    putonLeaveModal: boolean = false;
    newStaffModal: boolean = false;
    
    leaveBalanceList: Array<any>;
    terminateGroup: FormGroup;
    
    userview: UserView;
    currentDate = new Date();
    longMonth = this.currentDate.toLocaleString('en-us', { month: 'long' });
    userByPass: ApplicationUser;
    navigationExtras: { state: { StaffCode: string; ViewType: string; IsMaster: boolean; }; };
    printSummaryModal: boolean =  false;
    printSummaryGroup: FormGroup;
    updateStaff: FormGroup;
    
    tocken: any;
    trainingFrom:Date;
    trainingTo:Date;
    rosterTo :Date;
    rosterFrom:Date;
    hrnotesFrom :Date;
    hrnotesTo:Date;
    opnotesTo :Date;
    opnotesFrom:Date; 
    
    pdfTitle: string;
    tryDoctype: any;
    ifrmtryDoctype : boolean;
    SummarydrawerVisible: boolean; 
    spinloading: boolean = false ;
    dateFormat: string ='dd/MM/yyyy';
    Cycles: Array<any> = ['Cycle 1', 'Cycle 2', 'Cycle 3', 'Cycle 4', 'Cycle 5', 'Cycle 6', 'Cycle 7', 'Cycle 8', 'Cycle 9', 'Cycle 10'];
    DayNames: Array<any> = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    fDays : Array<any> = ['7','14','21','28',];
    Printrpt:boolean= false;
    
    sampleList: Array<any> = sampleList;
    cariteriaList:Array<any> = [];
    nodelist:Array<any> = [];
    staffSecondMenuRoutes:Array<any> = [];
    checkOptionsOne = checkOptionsOne;
    
    branchesList: any;
    diciplineList: any;
    casemanagers: any;
    categoriesList: any;
    skillsList:any;
    selectedRecpientTypes: any[];
    types: any[];
    
    extendedSearch: any;
    filteredResult: any;
    selectedTypes:any;
    selectedbranches: any[];
    testcheck : boolean = false;
    selectedPrograms: any;
    selectedCordinators: any;
    selectedCategories: any;
    selectedSkills:any;
    
    allBranches:boolean = true;
    allBranchIntermediate:boolean = false;
    
    allProgarms:boolean = true;
    allprogramIntermediate:boolean = false;
    
    allcat:boolean = true;
    allCatIntermediate:boolean = false;
    
    allCordinatore:boolean = true;
    allCordinatorIntermediate:boolean = false;
    
    allChecked: boolean = true;
    indeterminate: boolean = false;
    quicksearch: FormGroup;
    filters: FormGroup;
    loading: boolean;
    findModalOpen: boolean = false;
    statusList:any = statusList;
    genderList:any = genderList;
    conflictpointList:any = conflictpointList;
    timeSteps:Array<string>;
    sbFieldsSkill:any;
    lasturlSegmentBreedCrumb:string;
    current_tab: string ='Personal';
    headerInformation: any ;
    selectedStaff: any;
    headerCode: string;
    headerTitle: string;
    recipientStatus: string = null;
    inputValueStaffCode: any;
    isExportModalVisible: boolean;
    allstaffcoulmns: any;
    selectedFields: any;
    staffRows: any;
    exportFileName: any;
    address: any;
    emailJObqueryselectedstaff:boolean = false;
    smsjobqueryselectedstaff:boolean = false;
    tracssmessgage:boolean = false;
    outlookjobdairy:boolean = false;
    transformedUser: { name: any; view: number; id: any; sysmgr: any; };
    
    nzEvent(event: NzFormatEmitEvent): void {
      if (event.eventName === 'click') {
        var title = event.node.origin.title;
        
        this.extendedSearch.patchValue({
          title : title,
        });
        var keys       = event.keys;
        
      }    
    }
    
    showSubMenuStaff: boolean = false;
    
    columns: Array<any> = [
      {
        name: 'ID',
        checked: false
      },
      {
        name: 'URNumber',
        checked: false
      },
      {
        name: 'AccountNo',
        checked: false
      },
      {
        name: 'Surname',
        checked: false
      },
      {
        name: 'Firstname',
        checked: false
      },
      {
        name: 'Fullname',
        checked: false
      },
      {
        name: 'Gender',
        checked: true
      },
      {
        name: 'DOB',
        checked: true
      },
      {
        name: 'Address',
        checked: true
      },
      {
        name: 'Contact',
        checked: true
      },
      {
        name: 'Type',
        checked: true
      },
      {
        name: 'Branch',
        checked: true
      },
      {
        name: 'Coord',
        checked: false
      },
      {
        name: 'Category',
        checked: false
      },
      {
        name: 'ONI',
        checked: false
      },
      {
        name: 'Activated',
        checked: false
      },
      {
        name: 'Deactivated',
        checked: false
      },
      {
        name: 'Suburb',
        checked: false
      }
    ]
    avilibilityForm: FormGroup;
    operation: any;
    
    
    fields = [
      { key: 'UniqueID', title: 'Unique ID', selected: false },
      { key: 'AccountNo', title: 'Account No', selected: false },
      { key: 'LastName', title: 'Last Name', selected: false },
      // Add other fields here
    ];
    
    showModalExport() {
      this.listS.getstaffcoulmnnames().subscribe(data => {
        this.allstaffcoulmns = data; // Store the raw response (if needed)
        this.fields = this.formatFields(data); // Format the response into the fields array
        this.cd.detectChanges(); // Trigger change detection
      });
      this.isExportModalVisible = true; // Show the modal
    }
    
    // Helper function to format the API response into the fields array
    private formatFields(columnNames: string[]): { key: string; title: string; selected: boolean }[] {
      return columnNames.map((columnName) => ({
        key: columnName,
        title: columnName, // Format the column name
        selected: false,
      }));
    }
    
    handleExportOk() {
      this.isConfirmLoading = true;
      if (this.selectedFields.length === 0) {
        this.globalS.sToast('info',`No fields selected to export.`);
        return;
      }
      
      this.ModalS.confirm({
        nzTitle: 'Enter Filename',
        nzContent:this.modalTemplate1,
        nzOnOk: () => this.exportCSV()
      });

    }
    exportCSV() {
      const fileName = this.exportFileName ? `${this.exportFileName}.csv` : 'staff_data.csv';
    
      // Create headers for CSV
      const headersForCsvresult = this.selectedFields.map((field) => field.title);
      const headers = this.selectedFields.map((field) => field.title).join(',');
    
      // Fetch data for CSV
      this.listS.getstaffdataforcsv(headersForCsvresult).subscribe({
        next: (data) => {
          this.staffRows = data;
    
          // Create CSV data rows
          const rows = this.staffRows.map((row) =>
            this.selectedFields
              .map((field) => {
                const normalizedKey = Object.keys(row).find(
                  key => key.toLowerCase() === field.key.toLowerCase()
                );
                return normalizedKey ? row[normalizedKey] : ''; // Return value or empty string if key not found
              })
              .join(',')
          );
    
          // Combine header and rows
          const csvContent = [headers, ...rows].join('\n');
    
          // Create a Blob with the CSV content
          const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    
          // Trigger download
          const link = document.createElement('a');
          link.href = URL.createObjectURL(blob);
          link.download = fileName; // Use the user-entered filename
          link.click();
    
          // Reset UI states **AFTER** the download is triggered
          this.isExportModalVisible = false;
          this.isConfirmLoading = false;
          this.selectedFields = [];
        },
        error: (err) => {
          console.error('Error fetching staff data:', err);
          this.isConfirmLoading = false;
          this.isExportModalVisible = false;
        },
        complete: () => {
          this.handleCancelExport(); // Ensure modal cleanup
        }
      });
    }    
  
  // Add a field to the selected list
  addToSelected(field: { key: string; title: string }) {
    if (!this.selectedFields) {
      this.selectedFields = []; // Ensure selectedFields is initialized
    }
    
    const isDuplicate = this.selectedFields.some((f) => f.key === field.key);
    
    if (!isDuplicate) {
      this.selectedFields.push(field); // Add the field if it's not already selected
    } else {
      this.globalS.sToast('info',`Field with key "${field.key}" is already selected.`);
    }
  }
  
  // Remove a field from the selected list on right-click
  removeFromSelected(field: { key: string; title: string }, event: MouseEvent) {
    event.preventDefault(); // Prevent the default context menu
    this.selectedFields = this.selectedFields.filter((f) => f.key !== field.key); // Remove the field
  }
  
  
  handleCancelExport(): void {
    this.isExportModalVisible = false;
  }
  
  getSelectedFields() {
    return this.fields.filter(field => field.selected);
  }
  
  
  
  
  SaveAll_Changes(){
    let data = {
      type:'Staff',
      tab: this.current_tab
    }
    this.sharedS.emitSaveAll_Changes(data);
  }
  
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private timeS: TimeSheetService,
    private sharedS: ShareService,
    private globalS: GlobalService,
    private staffS:StaffService,
    private listS: ListService,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private settingS: SettingsService,
    private loginS: LoginService,
    private http: HttpClient,
    private ModalS: NzModalService,
    private sanitizer: DomSanitizer,
    private printS: PrintService
    
  ) {  
    
    
    this.sharedS.emitProfileStatus$.subscribe(data => {
      
      this.selectedStaff = data;
      this.headerInformation = 'STAFF' + ' / ' + this.current_tab + ' / ' + data.accountNo +' ('+data.uniqueID+')'
      
      if (data.admissionDate != null && data.dischargeDate == null) {
        this.recipientStatus = 'active';
      } else {
        this.recipientStatus = 'inactive';
      }
      
      
      
      
    })
    
    
    
  }
  
  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    this.sbFieldsSkill = sbFieldsSkill;
    this.nodelist = staffnodes;
    this.lasturlSegmentBreedCrumb = this.globalS.lastUrlSegement(this.router);
    this.buildForm();
    this.buildForms();
    this.timeSteps = timeSteps;
    this.getUserData();
    this.normalRoutePass();
  }
  listChange(event: any) {
    this.globalS.var1 = event.uniqueID;
    this.globalS.var2 = event.accountNo;
    if (event == null) {
      this.user = null;
      this.isFirstLoad = false;
      this.sharedS.emitChange(this.user);
      return;
    }
    if (!this.isFirstLoad) {
      this.view(0);
      this.isFirstLoad = true;
    }
    this.user = {
      code: event.accountNo,
      id: event.uniqueID,
      view: event.view,
      agencyDefinedGroup: event.agencyDefinedGroup,
      sysmgr: event.sysmgr
    }
    this.sharedS.emitChange(this.user);
    this.selectedStaff     = event;
    this.headerInformation = 'STAFF' + ' / ' + this.current_tab + ' / ' + event.accountNo +' ('+event.uniqueID+')';
    
    
    this.listS.Getheaderinfo(event.accountNo).subscribe(data => {
      this.headerCode  = data[0].code;
      this.headerTitle = data[0].title;
    })
    
    this.cd.detectChanges();
  }
  normalRoutePass(): void{
    const { user } = this.globalS.decode();
    
    this.listS.getstaffrecordview(user).subscribe(data => {
      this.userview = data;
      this.cd.detectChanges();
      
    })
    
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd ) => {
      if (event.url == '/admin/staff') {
        this.sample = { refresh: true };
        this.cd.detectChanges();
      }          
    });
    
    this.sharedS.emitRouteChangeSource$.subscribe(data => {});
    
  }
  activeInactive: boolean = false
  
  changeStatusBool(data: any) {
    this.activeInactive = data;
    this.SearchListComponent.searchStaff();
  }
  tab(tb:any){  
    this.current_tab = tb;
    this.headerInformation = 'STAFF' + ' / ' + this.current_tab + ' / ' + this.selectedStaff.accountNo +'('+this.selectedStaff.uniqueID+')';
  }
  buildForms(){
    
    this.quicksearch = this.fb.group({
      availble: false,
      option: false,
      status:'Active',
      gender:'Any Gender',
      surname:'',
      staff:true,
      brokers:true,
      volunteers:true,
      onleaveStaff:true,
      previousWork:false,
      searchText:'',
    });
    
    this.avilibilityForm = this.fb.group({
      date  :[new Date()],
      start :'09:00',
      end   :'10:00',
      drtn  :'01:00',
      conflict:true,
      conflictminutes:'',
    });
    
    this.filters = this.fb.group({
      activeprogramsonly:false,
    });
    
    this.extendedSearch = this.fb.group({
      title:'',
      rule:'',
      from:'',
      to:'',
      
      activeonly: true,
    });
    
  }
  buildForm(): void{
    var date = new Date();
    let monthStart = new Date(date.getFullYear(), date.getMonth(), 1);
    let monthend = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    
    this.terminateGroup = this.fb.group({
      terminateDate: [new Date(), Validators.required],
      unallocUnapproved: false,
      unallocMaster: false,
      deletePending: false
    });
    this.printSummaryGroup = this.fb.group({
      fileLabels:false,
      nameandContacts:true,
      contactIssue:false,
      otherContact:false,
      otherInfo:false,
      payrollInfo:false,
      workhourInfo:false,
      copmpetencies:false,
      otherSkills:false,
      training:false,
      roster:false,
      permanentRoster:false,
      miscellaneousNotes:false,
      operationalNotes:false,
      hrNotes:false,
      includeAdressress:false,
      ItemsOnLoan:false,
      trainingchk:false,
      hrchk:false,
      opchk:false,
      rosterchk:false,
      prosterchk:false,
      printOnSamePage:false,
      recepientSearc:'Show RECIPIENT CODE',
      recepientSearc1:'Show RECIPIENT CODE',
      Cycles : ['Cycle 1'],
      DayNames : ['Monday'],
      fDays: ['7'],
      trainingFrom  :monthStart,
      trainingTo :monthend,
      rosterTo :monthend,
      rosterFrom :monthStart,
      hrnotesFrom :monthStart,
      hrnotesTo :monthend,
      opnotesTo :monthend,
      opnotesFrom:monthStart,
    });
    
    this.printSummaryGroup.get('training').valueChanges.subscribe(data => {
      if(!data){
        this.printSummaryGroup.patchValue({
          trainingchk:false,
        });
      } else {
        this.printSummaryGroup.patchValue({
          trainingchk:true,
        });
      }
    })
    this.printSummaryGroup.get('roster').valueChanges.subscribe(data => {
      if(!data){
        this.printSummaryGroup.patchValue({
          rosterchk:false,
        });
      } else {
        this.printSummaryGroup.patchValue({
          rosterchk:true,
        });
      }
    })
    this.printSummaryGroup.get('permanentRoster').valueChanges.subscribe(data => {
      if(!data){
        this.printSummaryGroup.patchValue({
          prosterchk:false,
        });
      } else {
        this.printSummaryGroup.patchValue({
          prosterchk:true,
        });
      }
    })
    this.printSummaryGroup.get('hrNotes').valueChanges.subscribe(data => {
      if(!data){
        this.printSummaryGroup.patchValue({
          hrchk:false,
        });
      } else {
        this.printSummaryGroup.patchValue({
          hrchk:true,
        });
      }
    })
    this.printSummaryGroup.get('operationalNotes').valueChanges.subscribe(data => {
      if(!data){
        this.printSummaryGroup.patchValue({
          opchk:false,
        });
      } else {
        this.printSummaryGroup.patchValue({
          opchk:true,
        });
      }
    })
    
    /*    let monthend = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    
    this.trainingFrom = monthStart;
    this.trainingTo = monthend;
    this.rosterFrom = monthStart;
    this.rosterTo  = monthend;
    this.hrnotesFrom  = monthStart;
    this.hrnotesTo = monthend;
    this.opnotesFrom = monthStart;
    this.opnotesTo  = monthend;    */ 
    
    this.updateStaff = this.fb.group({
      staffCode : '',
      AccountNo : '',
    });
    
  }
  
  ngOnDestroy(): void {
    
  }
  handleOk(){ 
    
    this.ReportRender();
    //    this.printSummaryModal = false; 
    
  }
  InitiatePreview(index){ 
    if(index==2){
      this.Printrpt = true;
    }
    this.ReportRender();
    //    this.printSummaryModal = false; 
    this.tryDoctype = ""
    this.pdfTitle = ""
    
  }
  InitiatePrint(fileURL){
    const iframe = document.createElement('iframe');
    iframe.style.display = 'none';
    iframe.src = fileURL;
    iframe.name =  "iFrmRptprint";
    document.body.appendChild(iframe);
    iframe.contentWindow.print();
  }
  getUserData() {
    forkJoin([
      this.listS.getlisttimeattendancefilter("BRANCHES"),
      this.listS.getlisttimeattendancefilter("STAFFTEAM"),
      this.listS.getlisttimeattendancefilter("STAFFGROUP"),
      this.listS.getlisttimeattendancefilter("CASEMANAGERS"),
      this.listS.getskills(),
    ]).subscribe(data => {
      this.branchesList = data[0].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });
      this.diciplineList = data[1].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });
      this.categoriesList = data[2].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });
      this.casemanagers = data[3].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });
      this.skillsList = data[4];
    });
  }
  view(index: number) {
    this.nzSelectedIndex = index;
    
    if (index == 0) {
      this.router.navigate(['/admin/staff/personal'])
    }
    // if (index == 1) {
    //   this.router.navigate(['/admin/staff/contacts']);            
    // }
    // if (index == 2) {
    //   this.router.navigate(['/admin/staff/pay'])
    // }
    // if (index == 3) {
    //   this.router.navigate(['/admin/staff/leave'])
    // }
    // if (index == 4) {
    //   this.router.navigate(['/admin/staff/reminders'])
    // }
    // if (index == 5) {
    //   this.router.navigate(['/admin/staff/op-note'])
    // }
    // if (index == 6) {
    //   this.router.navigate(['/admin/staff/hr-note'])
    // }
    // if (index == 7) {
    //   this.router.navigate(['/admin/staff/competencies'])
    // }
    // if (index == 8) {
    //   this.router.navigate(['/admin/staff/training'])
    // }
    // if (index == 9) {
    //   this.router.navigate(['/admin/staff/incident'])
    // }
    // if (index == 10) {
    //   this.router.navigate(['/admin/staff/document'])
    // }
    // if (index == 11) {
    //   this.router.navigate(['/admin/staff/staff-time-attendance'])
    // }
    // if (index == 12) {
    //   this.router.navigate(['/admin/staff/position'])
    // }
    // if (index == 13) {
    //   this.router.navigate(['/admin/staff/groupings-preferences'])
    // }
    // if(index == 14){
    //   this.router.navigate(['/admin/staff/loans'])
    // }
  }
  
  terminateModalOpen() : void{
    this.terminateModal = true;
    this.listS.getleavebalances(this.user.id)
    .subscribe(data => this.leaveBalanceList = data)
  }
  changeStaffModalOpen() : void{
    // this.changeCodeModal = true;
    // this.updateStaff.patchValue({
    //   staffCode : this.user.code,
    //   accountNo : this.user.code,
    // });
    this.inputValueStaffCode = this.user.code;
    
    this.ModalS.confirm({
      nzTitle: 'Agency Defined Code',
      nzContent: this.modalTemplate, // Pass the template to the modal
      nzOkText: 'OK',
      nzOkType: 'danger',
      nzOnOk: () => this.updateStaffCode(),
      nzCancelText: 'Cancel',
    });
  }
  
  printSummaryModalOpen() : void{
    this.printSummaryModal = true;
  }
  
  updateStaffCode(){
    
    this.timeS.postchangestaffcode({
      AccountNo:this.user.code,
      StaffCode:this.inputValueStaffCode,
    }).subscribe(data => {
      if(data){
        this.globalS.sToast('Success','Staff Code Changed Successfully!');
        this.changeCodeModal = false;
        this.isConfirmLoading = false;
        this.router.navigate(["/admin/staff/personal"]);
        this.cd.detectChanges();
      }
      else{
        this.globalS.sToast('failure','Some thing Went Wrong !');
      }
    })
  }
  
  terminate(){
    
    for (const i in this.terminateGroup.controls) {
      this.terminateGroup.controls[i].markAsDirty();
      this.terminateGroup.controls[i].updateValueAndValidity();
    }
    
    if(!this.terminateGroup.valid)  return;
    
    this.isConfirmLoading = true;
    
    const { code, id } = this.user;
    
    this.timeS.posttermination({
      TerminationDate: this.terminateGroup.value.terminateDate,
      AccountNo: code,
      PersonID: id
    }).subscribe(data => {
      this.globalS.sToast('Success','Staff has been terminated!');
      this.terminateModal   = false;
      this.isConfirmLoading = false;
      this.cd.detectChanges();
    });
  }
  
  
  // this.timeS.postDeleteStaff({
  //   AccountNo: code,
  //   PersonID: id
  // }).subscribe(data => {
  //   this.globalS.sToast('Success', 'Staff has been deleted!');
  //   this.router.navigate(["/admin/staff"]);
  //   this.cd.detectChanges();
  //   this.reload(true);
  // });
  
  
  
  delete() {
    
    const { code, id } = this.user;
    
    this.listS.getrosterentries(code).subscribe(response => {
      
      if(response.recordNo){
        this.ModalS.confirm({
          nzTitle: 'T.R.A.C.C.S User Message',
          nzContent: 'There are unfinalised roster entries for this staff . if you delete the staff and subsequently run a timesheet update , you will receive warning message for any service being updated for this Staff. Please use timesheet entry/edit to view a service list for this Staff , and either delete or approve the entries . Do you Still want to permanetly DELETE this Staff ?',
          nzOkText: 'Delete',
          nzOkType: 'danger',
          nzOnOk: () => this.confirmRosterDelete(),
          nzCancelText: 'Cancel',
        });
        
        
      }else{
        this.ModalS.confirm({
          nzTitle: 'T.R.A.C.C.S User Message',
          nzContent: 'This Operation is irretrievably remove this staff from the database . Are you sure ? (if any doubt , make sure you have a current Backup you can refer to if necessary) .',
          nzOkText: 'Delete',
          nzOkType: 'danger',
          nzOnOk: () => this.confirmDelete(),
          nzCancelText: 'Cancel',
        });
      }
      
      
      
    });
  }
  
  confirmDelete() {
    const { code, id } = this.user;
    this.timeS.postDeleteStaff({
      AccountNo: code,
      PersonID: id
    }).subscribe(data => {
      this.globalS.sToast('Success', 'Staff has been deleted!');
      this.router.navigate(["/admin/staff"]);
      this.cd.detectChanges();
      this.reload(true);
    });
  }
  confirmRosterDelete(){
    
    this.ModalS.confirm({
      nzTitle: 'T.R.A.C.C.S User Message',
      nzContent: 'This Operation is ittrtrievably remove this staff from the database.Are you sure ? (if any doubt,make sure you have a current Backup you can refer to if necessary).',
      nzOkText: 'Delete',
      nzOkType: 'danger',
      nzOnOk: () => this.confirmDelete(),
      nzCancelText: 'Cancel',
    });
    
    
  }
  
  
  
  currentMonthRoster(){
    this.navigationExtras ={state : {StaffCode:this.user.code, ViewType:'Staff',IsMaster:false }};
    this.router.navigate(["/admin/rosters"],this.navigationExtras )
  }
  rosterMaster(){
    this.navigationExtras ={state : {StaffCode:this.user.code, ViewType:'Staff',IsMaster:true }};
    this.router.navigate(["/admin/rosters"],this.navigationExtras )
  }
  
  reloadVal: boolean = false;
  reload(reload: boolean){
    this.reloadVal = !this.reloadVal;
  }
  handleCancelTop(){
    this.SummarydrawerVisible = false;
    this.tryDoctype = ""
    this.pdfTitle = ""
  }
  handleCancel(){
    this.SummarydrawerVisible = false;
    this.findModalOpen = false;
  }
  ReportRender(){
    
    var id,rptfile , tempsdate, tempedate,temp1,temp2;
    var  Trainstrdate, Trainendate, hrstrdate, hrendate , OPstrdate, OPendate, Rstrdate, Rendate = '';
    
    if(this.printSummaryGroup.value.fileLabels == true){
      id = "AmNlnHzhrTOgmXah"
      var Title = "Address Labels"
      rptfile = "Staff Address Labels"
    }else{
      id = "2OIfvSiZPw5TUnvz"
      var Title = "Summary Sheet"
      rptfile = "Staff Summary Sheet"
    }
    if(id == "2OIfvSiZPw5TUnvz"){
      var date = new Date();
      
      if (this.trainingFrom != null) { Trainstrdate = format(this.trainingFrom, 'yyyy/MM/dd') } 
      else {                     
        Trainstrdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy/MM/dd')}
        if (this.trainingTo != null) { Trainendate = format(this.trainingTo, 'yyyy/MM/dd') } 
        else {       
          Trainendate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy/MM/dd');}
          
          
          if (this.opnotesFrom != null) { OPstrdate = format(this.opnotesFrom, 'MM-dd-yyyy') } 
          else {                     
            OPstrdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'MM-dd-yyyy')}
            if (this.opnotesTo != null) { OPendate = format(this.opnotesTo, 'MM-dd-yyyy') } 
            else {       
              OPendate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'MM-dd-yyyy');}
              
              
              
              
              if (this.hrnotesFrom != null) { hrstrdate = format(this.hrnotesFrom, 'MM-dd-yyyy') } 
              else {                     
                hrstrdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'MM-dd-yyyy')}
                if (this.hrnotesTo != null) { hrendate  = format(this.hrnotesTo, 'MM-dd-yyyy') } 
                else {       
                  hrendate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'MM-dd-yyyy');}
                  
                  
                  if (this.rosterFrom != null) { Rstrdate = format(this.rosterFrom, 'yyyy/MM/dd') } 
                  else {                     
                    Rstrdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy/MM/dd')}
                    if (this.rosterTo != null) { Rendate = format(this.rosterTo, 'yyyy/MM/dd') } 
                    else {       
                      Rendate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy/MM/dd');}
                      
                      
                      switch (this.printSummaryGroup.value.Cycles.toString()) {
                        case 'Cycle 1':
                        temp1 = "1900/01/01";                    
                        
                        break;
                        case "Cycle 2":
                        temp1 = "1900/10/01";
                        
                        break;
                        case 'Cycle 3':
                        temp1 = "1901/04/01";
                        
                        break;
                        case 'Cycle 4':
                        temp1 = "1901/07/01";
                        
                        break;
                        case 'Cycle 5':
                        temp1 = "1902/09/01";
                        
                        break;
                        
                        case 'Cycle 6':
                        temp1 = "1902/12/01";
                        
                        break;
                        case 'Cycle 7':
                        temp1 = "1903/06/01";
                        
                        break;
                        case 'Cycle 8':
                        temp1 = "1904/02/01";
                        
                        break;
                        case 'Cycle 9':
                        temp1 = "1904/08/01";
                        
                        break;
                        case 'Cycle 10':
                        temp1 = "1905/05/01";
                        
                        break;
                        default:
                        temp1 = "1900/01/01";
                        
                        
                        break;
                      }
                      let cyclestrdate =  new Date(temp1)
                      let enddate =  new Date(temp1)
                      switch (this.printSummaryGroup.value.fDays.toString()) {
                        case '14':
                        temp2 = enddate.setDate(14)
                        break;
                        case '21':
                        temp2 = enddate.setDate(21)
                        break;
                        case '28':
                        temp2 = enddate.setDate(28)
                        break;            
                        default:
                        temp2 = enddate.setDate(7)
                        break;
                      } 
                      let cycleendate =  new Date(temp2) 
                      
                      //console.log(this.globalS.var1.toString(),this.globalS.var2.toString())
                      const data = {
                        //"_id": "2OIfvSiZPw5TUnvz"
                        "template": { "_id": id },
                        
                        "options": {
                          "reports": { "save": false },                                  
                          "userid": this.tocken.user,
                          "txtTitle": Title,
                          "txtid":this.globalS.var1.toString(),
                          "txtacc":this.globalS.var2.toString(),
                          
                          "inclNameContact": this.printSummaryGroup.value.nameandContacts,
                          "inclContactIssues": this.printSummaryGroup.value.contactIssue,
                          "InclOtherContacts": this.printSummaryGroup.value.otherContact,
                          "InclOtherInfo": this.printSummaryGroup.value.otherInfo,
                          "InclPayRollInfo": this.printSummaryGroup.value.payrollInfo,
                          "InclWorkhrInfo": this.printSummaryGroup.value.workhourInfo,
                          "InclCompetencies": this.printSummaryGroup.value.copmpetencies,
                          "InclOtherSkills": this.printSummaryGroup.value.otherSkills,
                          "InclTraining": this.printSummaryGroup.value.training,
                          "InclPermanentRosters": this.printSummaryGroup.value.permanentRoster,
                          "InclRosterSheet": this.printSummaryGroup.value.roster,
                          "InclNotesSheet": this.printSummaryGroup.value.miscellaneousNotes,                 
                          "InclOPNotesS": this.printSummaryGroup.value.operationalNotes,
                          "InclLoanItems": this.printSummaryGroup.value.ItemsOnLoan,
                          "InclHRNotes": this.printSummaryGroup.value.hrNotes,
                          
                          //"InclFieldLabels": this.printSummaryGroup.value.fileLabels,
                          "RosterSDate": Rstrdate,
                          "RosterEDate": Rendate,
                          
                          "OPNotesSDate": OPstrdate,
                          "OPNotesEDate": OPendate,
                          
                          "HRNotesSDate": hrstrdate,
                          "HRNotesEDate": hrendate,
                          
                          "TrainingSDate":Trainstrdate ,
                          "TrainingEDate": Trainendate ,
                          
                          
                          "StaffInclusion":this.printSummaryGroup.value.recepientSearc,
                          
                          "cycleSDate":format(cyclestrdate,'yyyy/MM/dd'),
                          "cycleEDate":format(cycleendate,'yyyy/MM/dd'),
                          //"days":this.inputForm.value.fDays,
                          //"dayname":this.inputForm.value.DayNames,
                          
                        }
                      }
                      
                      this.spinloading = true;
                      this.SummarydrawerVisible = true;
                      
                      this.printS.printControl(data).subscribe((blob: any) => {
                        if(this.Printrpt == true){ 
                          this.pdfTitle = rptfile;
                          this.SummarydrawerVisible = false;                   
                          let _blob: Blob = blob;
                          let fileURL = URL.createObjectURL(_blob);
                          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                          this.spinloading = false;
                          this.cd.detectChanges();
                          this.InitiatePrint(fileURL);
                          this.Printrpt = false;
                        }else{
                          this.pdfTitle = rptfile;
                          this.SummarydrawerVisible = true;                   
                          let _blob: Blob = blob;
                          let fileURL = URL.createObjectURL(_blob);
                          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                          this.spinloading = false;
                          this.cd.detectChanges();
                        }
                      }, err => {
                        // console.log(err);
                        this.spinloading = false;
                        this.ModalS.error({
                          nzTitle: 'TRACCS',
                          nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                          nzOnOk: () => {
                            this.SummarydrawerVisible = false;
                          },
                        });
                      });
                      
                      return;
                    }
                    else{            
                      var date = new Date();            
                      const data = {
                        
                        "template": { "_id": id },
                        
                        "options": {
                          "reports": { "save": false },
                          //"sql": fQuery
                          "userid": this.tocken.user,
                          "txtTitle": Title,
                          "txtid":this.globalS.var1.toString(),
                          "txtacc":this.globalS.var2.toString(),
                        }
                      }
                      
                      this.spinloading = true;
                      this.SummarydrawerVisible = true; 
                      
                      this.printS.printControl(data).subscribe((blob: any) => {
                        if(this.Printrpt == true){ 
                          this.pdfTitle = rptfile;
                          this.SummarydrawerVisible = false;                   
                          let _blob: Blob = blob;
                          let fileURL = URL.createObjectURL(_blob);
                          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                          this.spinloading = false;
                          this.cd.detectChanges();
                          this.InitiatePrint(fileURL);
                          this.Printrpt = false;
                        }else{
                          this.pdfTitle = rptfile;
                          this.SummarydrawerVisible = true;                   
                          let _blob: Blob = blob;
                          let fileURL = URL.createObjectURL(_blob);
                          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                          this.spinloading = false;
                          this.cd.detectChanges();
                        }
                      }, err => {
                        console.log(err);
                        this.spinloading = false;
                        this.ModalS.error({
                          nzTitle: 'TRACCS',
                          nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                          nzOnOk: () => {
                            this.SummarydrawerVisible = false;
                          },
                        });
                      });
                      
                      return;
                    }
                  }
                  
                  
                  log(event: any,index:number) {
                    this.testcheck = false;   
                    if(index == 1)
                      this.selectedbranches = event;
                    if(index == 2)
                      this.selectedPrograms = event;
                    if(index == 3)
                      this.selectedCordinators = event;
                    if(index == 4)
                      this.selectedCategories = event;  
                    if(index == 5 && event.target.checked){
                      this.searchAvaibleModal = true;
                    }
                  }
                  
                  setCriteria(){ 
                    this.cariteriaList.push({
                      fieldName  : this.extendedSearch.value.title,
                      searchType : this.extendedSearch.value.rule,
                      textToLoc  : this.extendedSearch.value.from,
                      endText    : this.extendedSearch.value.to,
                    })
                  }
                  SearchOnEnter(event:KeyboardEvent){
                    if (event.key === 'Enter' ) {
                      event.preventDefault();
                      this.searchData();
                    }
                  }
                  searchData() : void{
                    this.loading = true;      
                    
                    this.selectedTypes = this.checkOptionsOne
                    .filter(opt => opt.checked)
                    .map(opt => opt.value).join("','")
                    
                    this.selectedPrograms = this.diciplineList
                    .filter(opt => opt.checked)
                    .map(opt => opt.value)
                    
                    this.selectedCordinators = this.casemanagers
                    .filter(opt => opt.checked)
                    .map(opt => opt.value)
                    
                    this.selectedCategories = this.categoriesList
                    .filter(opt => opt.checked)
                    .map(opt => opt.value)
                    
                    this.selectedbranches = this.branchesList
                    .filter(opt => opt.checked)
                    .map(opt => opt.value)
                    
                    this.selectedSkills  = this.skillsList
                    .filter(opt => opt.checked)
                    .map(opt => this.sbFieldsSkill[opt.identifier])
                    
                    var postdata = {
                      status:this.quicksearch.value.status,
                      gender:this.quicksearch.value.gender,
                      staff:this.quicksearch.value.staff,
                      brokers:this.quicksearch.value.brokers,
                      volunteers:this.quicksearch.value.volunteers,
                      onleaveStaff:this.quicksearch.value.onleaveStaff,
                      searchText:this.quicksearch.value.searchText,
                      
                      allTeamAreas      : this.allProgarms,
                      selectedTeamAreas : (this.allProgarms == false) ? this.selectedPrograms : '',
                      
                      allcat:this.allcat,
                      selectedCategories:(this.allcat == false) ? this.selectedCategories : '',
                      
                      allBranches:this.allBranches,
                      selectedbranches:(this.allBranches == false) ? this.selectedbranches : '',
                      
                      allCordinatore:this.allCordinatore,
                      selectedCordinators:(this.allCordinatore == false) ? this.selectedCordinators : '',
                      
                      allSkills:(this.selectedSkills.length) ? false : true,
                      selectedSkills: (this.selectedSkills.length) ? this.selectedSkills : '',
                      criterias:this.cariteriaList
                      // list of rules
                    }
                    
                    this.timeS.poststaffquicksearch(postdata).subscribe(data => {
                      this.filteredResult = data;
                      this.loading = false;
                      this.cd.detectChanges();
                    });
                  }
                  detectChanges() {
                    throw new Error('Method not implemented.');
                  }
                  allcompetencieschecked(): void {
                    
                    this.skillsList = this.skillsList.map(item => 
                      (
                        {
                          ...item,
                          checked: true
                        }
                      )
                    );
                  }
                  allcompetenciesunchecked(): void {
                    this.skillsList = this.skillsList.map(item => ({
                      ...item,
                      checked: false,
                    }));
                  }
                  updateAllChecked(): void {
                    this.indeterminate = false;
                    if (this.allChecked) {
                      this.checkOptionsOne = this.checkOptionsOne.map(item => ({
                        ...item,
                        checked: true
                      }));
                    } else {
                      this.checkOptionsOne = this.checkOptionsOne.map(item => ({
                        ...item,
                        checked: false
                      }));
                    }
                  }
                  updateAllCheckedFilters(filter: any): void {
                    
                    if(filter == 1 || filter == -1){
                      if(this.testcheck == false){  // why its returing undefined 
                        if (this.allBranches) {
                          this.branchesList.forEach(x => {
                            x.checked = true;
                          });
                        }else{
                          this.branchesList.forEach(x => {
                            x.checked = false;
                          });
                        }
                      }
                    }
                    if(filter == 2 || filter == -1){
                      if(this.testcheck == false){
                        if (this.allProgarms) {
                          this.diciplineList.forEach(x => {
                            x.checked = true;
                          });
                        }else{
                          this.diciplineList.forEach(x => {
                            x.checked = false;
                          });
                        }
                      }
                    }
                    if(filter == 3 || filter == -1){
                      if(this.testcheck == false){
                        if (this.allCordinatore) {
                          this.casemanagers.forEach(x => {
                            x.checked = true;
                          });
                        }else{
                          this.casemanagers.forEach(x => {
                            x.checked = false;
                          });
                        }
                      }
                    }
                    if(filter == 4 || filter == -1){
                      if(this.testcheck == false){
                        if (this.allcat) {
                          this.categoriesList.forEach(x => {
                            x.checked = true;
                          });
                        }else{
                          this.categoriesList.forEach(x => {
                            x.checked = false;
                          });
                        }
                      }
                    }
                  }
                  updateSingleChecked(): void {
                    this.testcheck == false
                    if (this.checkOptionsOne.every(item => !item.checked)) {
                      this.allChecked = false;
                      this.indeterminate = false;
                    } else if (this.checkOptionsOne.every(item => item.checked)) {
                      this.allChecked = true;
                      this.indeterminate = false;
                    } else {
                      this.indeterminate = true;
                      this.allChecked = false;
                      this.testcheck  = false;
                    }
                  }
                  updateSingleCheckedFilters(index:number): void {
                    if(index == 1){
                      this.testcheck = false;
                      if (this.branchesList.every(item => !item.checked)) {
                        this.allBranches = false;
                        this.allBranchIntermediate = false;
                      } else if (this.branchesList.every(item => item.checked)) {
                        this.allBranches = true;
                        this.allBranchIntermediate = false;
                      } else {
                        this.allBranchIntermediate = true;
                        this.allBranches = false;
                      }
                    }
                    if(index == 2){
                      this.testcheck = false;
                      if (this.diciplineList.every(item => !item.checked)) {
                        this.allProgarms = false;
                        this.allprogramIntermediate = false;
                      } else if (this.diciplineList.every(item => item.checked)) {
                        this.allProgarms = true;
                        this.allprogramIntermediate = false;
                      } else {
                        this.allprogramIntermediate = true;
                        this.allProgarms = false;
                      }
                    }
                    if(index == 3){
                      this.testcheck = false;
                      if (this.casemanagers.every(item => !item.checked)) {
                        this.allCordinatore = false;
                        this.allCordinatorIntermediate = false;
                      } else if (this.casemanagers.every(item => item.checked)) {
                        this.allCordinatore = true;
                        this.allCordinatorIntermediate = false;
                      } else {
                        this.allCordinatorIntermediate = true;
                        this.allCordinatore = false;
                      }
                    }
                    if(index == 4){
                      this.testcheck = false;
                      if (this.categoriesList.every(item => !item.checked)) {
                        this.allcat = false;
                        this.allCatIntermediate = false;
                      } else if (this.categoriesList.every(item => item.checked)) {
                        this.allcat = true;
                        this.allCatIntermediate = false;
                      } else {
                        this.allCatIntermediate = true;
                        this.allcat = false;
                      }
                    }
                  }
                  openFindModal(){
                    this.tabFindIndex = 0;
                    
                    this.updateAllCheckedFilters(-1);
                    
                    this.findModalOpen = true;
                    
                  }
                  
                  tabFindIndex: number = 0;
                  tabFindChange(index: number){
                    this.tabFindIndex = index;
                  }
                  
                  filterChange(index: number){
                    
                  }
                  
                  showLeaveModal() {
                    this.operation = {
                      process: 'ADD'
                    }
                    //this.putonLeaveModal = !this.putonLeaveModal;
                    this.load_LeaveModel();
                  }
                  load_LeaveModel(){
                    this.putonLeaveModal = true;
                    let _highlighted: any = [];
                    let selectedOption: any = {carercode:this.user.code,
                      recipient:''
                    };
                    
                    _highlighted.push(selectedOption);
                    this.loadUnallocateStaff.next({diary:_highlighted,selected:selectedOption, AddLeave:true});
                    
                  }
                  
                  UnallocateStaffDone(data:any){
                    this.putonLeaveModal = false;
                    
                    //this.search(this.user);
                    return;
                  }
                  clickOutsideMenu(data: any){   
                    if(data.value){
                      this.showSubMenuStaff = false;
                    }
                  }
                  getPermisson(index:number){
                    var permissoons = this.globalS.getStaffRecordView();
                    return permissoons.charAt(index-1);
                  }
                  
                  emitEvent(event: any) {
                    switch(event){
                      case 'New' :
                      this.newStaffModal = true;
                      break;
                      case 'Save' :
                      this.SaveAll_Changes();
                      break;
                      case 'Change' :
                      this.changeStaffModalOpen();
                      break;
                      case 'Delete' :
                      this.delete();
                      break;
                      case 'Export' :
                      this.showModalExport();
                      break;
                    }
                  }

                  menuAction(item: any,operation:number){

                            switch(operation){  
                                case 1:
                                  this.toMap(item);
                                break;
                              case 2:
                                this.toLastKnownMap(item);
                                break;
                                case 12:
                                  this.handleCancel();

                                  // Ensure staff details are set before navigation
                                  if (item) {
                                    this.sharedS.emitChange(this.transformedUser = {
                                      name: item.account,
                                      view: 1,
                                      id: item.id,
                                      sysmgr: true,
                                  });
                                  }
                                  this.router.navigate(['/admin/staff/personal']); 
                                break
                              }
                             
                  }


                  toMap(item:any){
                    this.address=item.address;
                      console.log(this.address);
                    
                    if(this.address.length > 0){         
                      window.open(`https://www.google.com/maps/dir///@${this.address}`,'_blank');
            
                        // var adds = this.address.reduce((acc,data) => {
                        //     var { postCode, address1, suburb, state } = data;
                        //     var address = new Address(postCode, address1, suburb, state);
                        //     return acc.concat('/',address.getAddress());                
                        // }, []);
              //          console.log(adds)
                //        console.log(adds.join(''))
                            
                        //window.open('https://www.google.com/maps/search/?api=1&query=' + encoded,'_blank');
                       // window.open(`https://www.google.com/maps/dir${adds.join('')}`,'_blank');
                        return false;
                    }
                    this.globalS.eToast('No address found','Error');
                }
                toLastKnownMap(item:any){
                  if (item.lastKnownLocation=='' || item.lastKnownLocation==null)
                    this.address=item.address;
                  else{
                   
                    let addr = item.lastKnownLocation;
                     addr=addr.replace("<Lattitude>","").replace("<Lattitude/>","")
                     this.address=addr.replace("<Longditude>","").replace("<Longditude/>","")
                  }
                 
                  if(this.address.length > 0){         
                 
                    window.open(`https://www.google.com/maps/dir///@${this.address}`,'_blank');
            
                      // window.open(`https://www.google.com/maps/@${this.address}`);
                    // var adds = this.address.reduce((acc,data) => {
                    //       var { postCode, address1, suburb, state } = data;
                    //       var address = new Address(postCode, address1, suburb, state);
                    //       return acc.concat('/',address.getAddress());                
                    //   }, []);
                    //  console.log(adds)
                  //    console.log(adds.join(''))
                          
                      //window.open('https://www.google.com/maps/search/?api=1&query=' + encoded,'_blank');
            //          window.open(`https://www.google.com/maps/dir${adds.join('')}`,'_blank');
                      return false;
                  }
                  this.globalS.eToast('No address found','Error');
              }
                  
                  
                }
                
                