import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';;
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';

import {
    RouteGuard,
    AdminStaffRouteGuard,
    CanDeactivateGuard,
    LoginGuard,
    AdminRouteGuard,
    ByPassGuard
  } from '@services/index'
  

import {
  ClaimratesComponent,
  TargetgroupsComponent,
  PurposestatementComponent,
  BudgetgroupsComponent,
  BudgetsComponent,
  ContactgroupsComponent,
  ContacttypesComponent,
  AddresstypesComponent,
  OccupationComponent,
  ReligionComponent,
  PhoneemailtypesComponent,
  FinancialclassComponent,
  DesktopUsersComponent,
  CustomDatasets,
  PostcodesComponent,
  HolidaysComponent,
  MedicalcontactComponent,
  DestinationaddressComponent,
  ProgramcoordinatesComponent,
  DistributionlistComponent,
  NotificationlistComponent,
  FollowupComponent,
  InitialactionsComponent,
  OngoingactionsComponent,
  IncidenttriggersComponent,
  IncidenttypesComponent,
  IncidentsubcatComponent,
  IncidentnotecategoryComponent,
  LocationCategoriesComponent,
  StaffincidentnotecategoryComponent,
  FillingclassificationComponent,
  DocumentcategoriesComponent,
  RecipientsCategoryComponent,
  RecipientsGroupComponent,
  RecipientsMinorGroupComponent,
  RecipientsBillingCyclesComponent,
  DebtorTermsComponent,
  RecipientGoalsComponent,
  ConsentTypesComponent,
  CarePlanTypesComponent,
  ClinicalNotesGroupsComponent,
  CaseNoteCategoriesComponent,
  OpNoteCategoriesComponent,
  CareDomainsComponent,
  DischargeReasonsComponent,
  RefferalReasonsComponent,
  UserDefinedRemindersComponent,
  RecipientPrefrencesComponent,
  MobilityCodesComponent,
  HealthConditionsComponent,
  TasksComponent,
  MedicationsComponent,
  NursingDignosisComponent,
  MedicalDignosisComponent,
  MedicalProceduresComponent,
  ClinicalRemindersComponent,
  ClinicalAlertsComponent,
  AdmittingPrioritiesComponent,
  ServicenotecatComponent,
  ReferralSourcesComponent,
  LifecycleEventsComponent,
  JobCategoryComponent,
  AdminCategoryComponent,
  StaffUserGroupsComponent,
  AwardLevelsComponent,
  AwardSetupComponent,
  StaffCompetencyGroupComponent,
  StaffNotesCategoriesComponent,
  StaffPositionsComponent,
  StaffTeamsComponent,
  AwardDetailsComponent,
  OpStaffNotesComponent,
  StaffRemindersComponent,
  ServiceDeciplinesComponent,
  StaffPreferencesComponent,
  LeaveDescriptionsComponent,
  ServiceNoteCategoriesComponent,
  VehiclesComponent,
  ActivityGroupsComponent,
  EquipmentsComponent,
  StaffCompetenciesComponent,
  CentrFacilityLocationComponent,
  FundingSourcesComponent,
  PayTypeComponent,
  ProgramPackagesComponent,
  ServicesComponent,
  ItemsConsumablesComponent,
  MenuMealsComponent,
  CaseMangementAdminComponent,
  StaffAdminActivitiesComponent,
  RecipientAbsenceComponent,
  CompaniesComponent,
  DocumentTemplateComponent,
  // BudgetAdmin,
  // GlanceAdmin,
  // MediaList,
  // UserDetail,
  Checklist,
  PortalUsers,
  ConfigurationMain,
  Othercontactaddress
  
  } from './index';




const routes: Routes = [
    {
        path: '',
        redirectTo: 'configuration',
        pathMatch: 'full'
    },
    {
      path: 'configuration',
      component: ConfigurationMain,
    },
    {
      path: 'companies',
      component: CompaniesComponent,
    },
    {
      path:"claim-rates",
      component:ClaimratesComponent
    },
    {
      path:"target-groups",
      component:TargetgroupsComponent
    },
    {
      path:"purpose-statement",
      component:PurposestatementComponent
    },
    {
      path:"budget-groups",
      component:BudgetgroupsComponent
    },
    {
      path:"budgets",
      component:BudgetsComponent
    },
    {
      path:"contact-groups",
      component:ContactgroupsComponent,
    },
    {
      path:"contact-types",
      component:ContacttypesComponent,
    },
    {
      path:"address-types",
      component:AddresstypesComponent,
    },
    {
      path:"religions",
      component:ReligionComponent,
    },
    {
      path:"occupations",
      component:OccupationComponent,
    },
    {
      path:"phone-email-types",
      component:PhoneemailtypesComponent,
    },
    {
      path:"financial-class",
      component:FinancialclassComponent,
    },
    {
      path:"desktopusers",
      component:DesktopUsersComponent,
    },
    {
      path:"portalusers",
      component:PortalUsers,
    },
    {
      path:"customdataset",
      component:CustomDatasets,
    },
    {
      path:"checklist",
      component:Checklist,
    },
    {
      path:"postcodes",
      component:PostcodesComponent,
    },
    {
      path:"holidays",
      component:HolidaysComponent,
    },
    {
      path:"medical-contact",
      component:MedicalcontactComponent,
    },
    {
      path:"destination-address",
      component:DestinationaddressComponent,
    },
    {
      path:"program-coordinates",
      component:ProgramcoordinatesComponent,
    },
    {
      path:"distribution-list",
      component:DistributionlistComponent,
    },
    {
      path:"notification-list",
      component:NotificationlistComponent,
    },
    {
      path:"workflow/:type", 
      component:FollowupComponent,
    },
    {
      path:"other-contact-addresses", 
      component:Othercontactaddress,
    },
    {
      path:"initial-actions",
      component:InitialactionsComponent,
    },
    {
      path:"ongoing-actions",
      component:OngoingactionsComponent,
    },
    {
      path:"incident-trigger",
      component:IncidenttriggersComponent,
    },
    {
      path:"incident-types",
      component:IncidenttypesComponent,
    },
    {
      path:"incident-sub-category",
      component:IncidentsubcatComponent
    },
    {
      path:"incident-location-categories",
      component:LocationCategoriesComponent
    },
    {
      path:"recipient-incident-note-category",
      component:IncidentnotecategoryComponent
    },
    {
      path:"staff-incident-note-category",
      component:StaffincidentnotecategoryComponent
    },
    {
      path:"filling-classification",
      component:FillingclassificationComponent
    },
    {
      path:"document-categories",
      component:DocumentcategoriesComponent,
    },
    {
      path:"document-template",
      component:DocumentTemplateComponent,
    },
    {
      path:"recipients-categories",
      component:RecipientsCategoryComponent
    },
    {
      path:"recipients-groups",
      component:RecipientsGroupComponent
    },
    {
      path:"recipients-minor-group",
      component:RecipientsMinorGroupComponent
    },
    {
      path:"recipients-billing-cycles",
      component:RecipientsBillingCyclesComponent,
    },
    {
      path:"debtor-terms",
      component:DebtorTermsComponent
    },
    {
      path:"recipient-goals",
      component:RecipientGoalsComponent
    },
    {
      path:"consent-types",
      component:ConsentTypesComponent
    },
    {
      path:"care-plan-types",
      component:CarePlanTypesComponent
    },
    {
      path:"clicnical-notes-groups",
      component:ClinicalNotesGroupsComponent
    },
    {
      path:"case-notes-categories",
      component:CaseNoteCategoriesComponent
    },
    {
      path:"op-notes-categories",
      component:OpNoteCategoriesComponent
    },
    {
      path:"care-domains",
      component:CareDomainsComponent
    },
    {
      path:"discharge-reasons",
      component:DischargeReasonsComponent
    },
    {
      path:"refferal-reasons",
      component:RefferalReasonsComponent
    },
    {
      path:"user-define-reminders",
      component:UserDefinedRemindersComponent
    },
    {
      path:"recipient-prefrences",
      component:RecipientPrefrencesComponent
    },
    {
      path:"referral-sources",
      component:ReferralSourcesComponent
    },
    {
      path:"lifecycle-events",
      component:LifecycleEventsComponent
    },
    {
      path:"job-category",
      component:JobCategoryComponent
    },
    {
      path:"admin-category",
      component:AdminCategoryComponent
    },
    {
      path:"admin-category",
      component:AdminCategoryComponent
    },
    {
      path:"user-groups",
      component:StaffUserGroupsComponent
    },
    {
      path:"award-levels",
      component:AwardLevelsComponent,
    },
    {
      path:"award-setup",
      component:AwardSetupComponent,
    },
    {
      path:"competency-groups",
      component:StaffCompetencyGroupComponent
    },
    {
      path:"staff-competency",
      component:StaffCompetenciesComponent
    },
    {
      path:"hr-notes-categories",
      component:StaffNotesCategoriesComponent
    },
    {
      path:"staff-positions",
      component:StaffPositionsComponent
    },
    {
      path:"staff-teams",
      component:StaffTeamsComponent
    },
    {
      path:"award-details",
      component:AwardDetailsComponent
    },
    {
      path:"op-staff-notes",
      component:OpStaffNotesComponent,
    },
    {
      path:"staff-reminder",
      component:StaffRemindersComponent,
    },
    {
      path:"service-deciplines",
      component:ServiceDeciplinesComponent
    },
    {
      path:"staff-preferences",
      component:StaffPreferencesComponent,
    },
    {
      path:"leave-description",
      component:LeaveDescriptionsComponent,
    },
    {
      path:"service-note-categories",
      component:ServiceNoteCategoriesComponent,
    },
    {
      path:"vehicles",
      component:VehiclesComponent,
    },
    {
      path:"activity-groups",
      component:ActivityGroupsComponent,
    },
    {
      path:"equipments",
      component:EquipmentsComponent,
    },
    {
      path:"center-facility-location",
      component:CentrFacilityLocationComponent,
    },{
      path:"funding-sources",
      component:FundingSourcesComponent,
    },
    {
      path:"pay-types",
      component:PayTypeComponent,
    },{
      path:"program-packages",
      component:ProgramPackagesComponent
    },
    {
      path:"services",
      component:ServicesComponent
    },
    {
      path:"items-consumables",
      component:ItemsConsumablesComponent
    },
    {
      path:"menu-meals",
      component:MenuMealsComponent,
    },
    {
      path:"case-management-admin",
      component:CaseMangementAdminComponent
    },
    {
      path:"staff-admin-activities",
      component:StaffAdminActivitiesComponent
    },
    {
      path:"recipient-absences",
      component:RecipientAbsenceComponent,
    },
    {
      path:"nursing-dignosis",
      component:NursingDignosisComponent,
    },
    {
      path:"medical-dignosis",
      component:MedicalDignosisComponent,
    },
    {
      path:"medical-procedures",
      component:MedicalProceduresComponent,
    },
    {
      path:"clinical-reminders",
      component:ClinicalRemindersComponent,
    }
    ,
    {
      path:"clinical-alerts",
      component:ClinicalAlertsComponent,
    },
    {
      path:"medications",
      component:MedicationsComponent,
    },
    {
      path:"health-conditions",
      component:HealthConditionsComponent,
    },
    {
      path:"mobility-codes",
      component:MobilityCodesComponent,
    },
    {
      path:"tasks",
      component:TasksComponent,
    }
    ,
    {
      path:"admitting-priorities",
      component:AdmittingPrioritiesComponent
    },
    // {
    //   path:"service-note-categories",
    //   component:ServicenotecatComponent,
    // }


  
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,
    NzSpinModule,
    NzDrawerModule,
    NzButtonModule,
    NzFormModule,
    NzDatePickerModule,
    NzModalModule,
    NzTableModule,
    NzDividerModule
  ],
  declarations: [
    
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class ConfigAdminModule   {}

