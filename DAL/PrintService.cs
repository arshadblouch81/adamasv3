﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Data;

using Adamas.Models;
using Adamas.Models.Modules;

using Microsoft.Office.Interop.Word;
using System.Text;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using Repositories;
using Adamas.Models.Dto;

namespace Adamas.DAL
{
    public class PrintService : IPrintService
    {
        private readonly DatabaseContext _context;
        private readonly ISqlDbConnection _conn;
        private IGeneralSetting GenSettings;
        private IUploadRepository uploadRepo;
        public PrintService(
            DatabaseContext context,
            ISqlDbConnection conn,
            IUploadRepository uploadRepo,
            IGeneralSetting setting
            )
        {
            _context = context;
            _conn = conn;
            GenSettings = setting;
            this.uploadRepo = uploadRepo;
        }

        public async Task<(MemoryStream, string, string)?> PrintQuotes(string sourcePath)
        {

            if (!File.Exists(sourcePath))
            {
                return null;
            }

            var title = "Operational Notes For Morganica Abbotts";

            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();

            //Set animation status for word application  
            wordApp.ShowAnimation = false;

            //Set status for word application is to be visible or not.  
            wordApp.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Open(Path.GetFullPath(sourcePath), ReadOnly: false, Visible: true);


            try
            {
                document.Activate();

                FindAndReplace(wordApp, "<<CUSTOMER_NAME>>", "Mark Aris Trinidad");
                FindAndReplace(wordApp, "<<STREET>>", "151 Dobie St");
                FindAndReplace(wordApp, "<<STATE_SUBURB>>", "GRAFTON NSW 2460");


                // Save Document
                string filename = @"quote_lines1.docx";
                object path = Path.Combine(Environment.CurrentDirectory, @"document\", filename);
                document.SaveAs2(ref path);

                // Close Document and Application
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return await uploadRepo.DownloadFileInDocumentDirectory(path.ToString(), filename); ;
            }
            catch (Exception ex)
            {
                // Close Document and Application when error occurs
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return null;
            }
        }





        public async Task<(MemoryStream, string, string)?> PrintNotes()
        {
            var title = "Operational Notes For Morganica Abbotts";

            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();

            //Set animation status for word application  
            wordApp.ShowAnimation = false;

            //Set status for word application is to be visible or not.  
            wordApp.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);


            try
            {
                document.PageSetup.Orientation = WdOrientation.wdOrientPortrait;
                document.PageSetup.TopMargin = (float)20;
                document.PageSetup.LeftMargin = (float)20;
                document.PageSetup.RightMargin = (float)20;
                document.PageSetup.BottomMargin = (float)20;

                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading1 = "Heading 1";
                para1.Range.set_Style(ref styleHeading1);
                para1.Range.Text = $"{ title }";
                para1.Range.InsertParagraphAfter();


                Table firstTable = document.Tables.Add(para1.Range, 5, 5, ref missing, ref missing);

                firstTable.Borders.Enable = 1;
                foreach (Row row in firstTable.Rows)
                {
                    foreach (Cell cell in row.Cells)
                    {
                        //Header row  
                        if (cell.RowIndex == 1)
                        {
                            if (cell.ColumnIndex == 1)
                            {
                                cell.Range.Columns.Width = 70.57f;
                            }
                            cell.Range.Text = "Column " + cell.ColumnIndex.ToString();
                            cell.Range.Font.Bold = 1;
                            //other format properties goes here  
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 10;
                            //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                            cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                            //Center alignment for the Header cells  
                            cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                        }
                        //Data row  
                        else
                        {
                            cell.Range.Text = (cell.RowIndex - 2 + cell.ColumnIndex).ToString();
                        }
                    }
                }
                // Save Document
                string filename = @"notes.docx";
                object path = Path.Combine(Environment.CurrentDirectory, @"document\", filename);
                document.SaveAs2(ref path);

                // Close Document and Application
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return await uploadRepo.DownloadFileInDocumentDirectory(path.ToString(), filename); ;
            } catch(Exception ex)
            {
                // Close Document and Application when error occurs
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return null;
            }
        }

        //public async Task<(MemoryStream, string, string)?> PrintQuoteLines(DocumentQuoteInputDto input)
        //{
        //    var recipient = await _context.Recipients.Where(x => x.UniqueID == input.Quote.PersonId).FirstOrDefaultAsync();

        //    //var quotes = await GetQuotes(input);

        //    Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();

        //    //Set animation status for word application  
        //    wordApp.ShowAnimation = false;

        //    //Set status for word application is to be visible or not.  
        //    wordApp.Visible = false;

        //    //Create a missing variable for missing value  
        //    object missing = System.Reflection.Missing.Value;

        //    Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);

        //    try
        //    {


        //        document.PageSetup.Orientation = WdOrientation.wdOrientLandscape;
        //        document.PageSetup.TopMargin = (float)20;
        //        document.PageSetup.LeftMargin = (float)20;
        //        document.PageSetup.RightMargin = (float)20;
        //        document.PageSetup.BottomMargin = (float)20;


        //        foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
        //        {
        //            //Get the header range and add the header details.  
        //            Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
        //            headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
        //            headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

        //            headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
        //            headerRange.Font.Size = 9;
        //            headerRange.Text = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
        //        }


        //        document.ActiveWindow.ActivePane.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekCurrentPageFooter;
        //        Object oMissing = System.Reflection.Missing.Value;

        //        document.ActiveWindow.Selection.TypeText("Page ");
        //        Object TotalPages = Microsoft.Office.Interop.Word.WdFieldType.wdFieldNumPages;
        //        Object CurrentPage = Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage;

        //        document.ActiveWindow.Selection.Fields.Add(document.ActiveWindow.Selection.Range, ref CurrentPage, ref oMissing, ref oMissing);
        //        document.ActiveWindow.Selection.TypeText(" of ");
        //        document.ActiveWindow.Selection.Fields.Add(document.ActiveWindow.Selection.Range, ref TotalPages, ref oMissing, ref oMissing);
        //        document.ActiveWindow.Selection.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

        //        //Add paragraph with Heading 1 style  
        //        Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
        //        object styleHeading1 = "Heading 1";
        //        para1.Range.set_Style(ref styleHeading1);
        //        para1.Range.Text = $"Quotes for { recipient.AccountNo }";
        //        para1.Range.InsertParagraphAfter();


        //        List<string> columnNames = new List<string>() { "Quote#", "Name/Description", "Quote Type", "St", "Valid From", "Valid To", "Created" };


        //        Table firstTable = document.Tables.Add(para1.Range, quotes.Count + 1, 7, ref missing, ref missing);

        //        firstTable.Borders.Enable = 1;
        //        foreach (Row row in firstTable.Rows)
        //        {
        //            foreach (Cell cell in row.Cells)
        //            {
        //                //Header row  
        //                if (cell.RowIndex == 1)
        //                {
        //                    if (cell.ColumnIndex == 1 || cell.ColumnIndex == 4)
        //                    {
        //                        cell.Range.Columns.Width = 40.57f;
        //                    }
        //                    else if (cell.ColumnIndex == 5 || cell.ColumnIndex == 6)
        //                    {
        //                        cell.Range.Columns.Width = 75.57f;
        //                    }
        //                    else
        //                    {
        //                        cell.Range.Columns.Width = 168.57f;
        //                    }

        //                    cell.Range.Text = columnNames[cell.ColumnIndex - 1];
        //                    cell.Range.Font.Bold = 1;
        //                    //other format properties goes here  
        //                    cell.Range.Font.Name = "verdana";
        //                    cell.Range.Font.Size = 8;
        //                    //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
        //                    cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
        //                    //Center alignment for the Header cells  
        //                    cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
        //                    cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

        //                }
        //                //Data row  
        //                else
        //                {
        //                    cell.Range.Font.Size = 7;
        //                    if (cell.ColumnIndex == 1)
        //                    {
        //                        cell.Range.Text = quotes[cell.RowIndex - 2].QuoteNumber;
        //                    }

        //                    if (cell.ColumnIndex == 2)
        //                    {
        //                        cell.Range.Text = quotes[cell.RowIndex - 2].Program;
        //                    }

        //                    if (cell.ColumnIndex == 3)
        //                    {
        //                        cell.Range.Text = quotes[cell.RowIndex - 2].PlanType;
        //                    }

        //                    if (cell.ColumnIndex == 4)
        //                    {
        //                        cell.Range.Text = quotes[cell.RowIndex - 2].St;
        //                    }

        //                    if (cell.ColumnIndex == 5)
        //                    {
        //                        var date = (DateTime?)quotes[cell.RowIndex - 2].StartDate;
        //                        cell.Range.Text = date != null ? date.Value.ToString("MM/dd/yyyy") : "";
        //                    }

        //                    if (cell.ColumnIndex == 6)
        //                    {
        //                        var date = (DateTime?)quotes[cell.RowIndex - 2].EndDate;
        //                        cell.Range.Text = date != null ? date.Value.ToString("MM/dd/yyyy") : null;
        //                    }

        //                    if (cell.ColumnIndex == 7)
        //                    {
        //                        cell.Range.Text = quotes[cell.RowIndex - 2].Created;
        //                    }
        //                }
        //            }
        //        }

        //        //Save the document
        //        string filename = @"quotes.docx";
        //        object path = Path.Combine(Environment.CurrentDirectory, @"document\", filename);

        //        document.SaveAs2(ref path);

        //        document.Close(ref missing, ref missing, ref missing);
        //        document = null;
        //        wordApp.Quit(ref missing, ref missing, ref missing);
        //        wordApp = null;

        //        return await uploadRepo.DownloadFileInDocumentDirectory(path.ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        document.Close(ref missing, ref missing, ref missing);
        //        document = null;
        //        wordApp.Quit(ref missing, ref missing, ref missing);
        //        wordApp = null;
        //        return null;
        //    }
        //}

        public async Task<(MemoryStream, string, string)?> PrintQuotes(DocumentQuoteInputDto input)
        {
            var recipient = await _context.Recipients.Where(x => x.UniqueID == input.Quote.PersonId).FirstOrDefaultAsync();

            var quotes = await GetQuotes(input);

            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();

            //Set animation status for word application  
            wordApp.ShowAnimation = false;

            //Set status for word application is to be visible or not.  
            wordApp.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);

            try
            {


                document.PageSetup.Orientation = WdOrientation.wdOrientLandscape;
                document.PageSetup.TopMargin = (float)20;
                document.PageSetup.LeftMargin = (float)20;
                document.PageSetup.RightMargin = (float)20;
                document.PageSetup.BottomMargin = (float)20;


                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {
                    //Get the header range and add the header details.  
                    Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

                    headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
                    headerRange.Font.Size = 9;
                    headerRange.Text = DateTime.Now.ToString("MM/dd/yyyy HH:mm");

                    //Microsoft.Office.Interop.Word.Range headerRange2 = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    //headerRange2.Fields.Add(headerRange2, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                    //headerRange2.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

                    //headerRange2.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
                    //headerRange2.Font.Size = 9;
                    //headerRange2.Text = $"Total - {quotes.Count } Pages";
                }

                ////Add the footers into the document  
                //foreach (Microsoft.Office.Interop.Word.Section wordSection in document.Sections)
                //{
                //    //Get the footer range and add the footer details.  
                //    Microsoft.Office.Interop.Word.Range footerRange = wordSection.Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                //    footerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
                //    footerRange.Font.Size = 9;
                //    footerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphLeft;
                //    footerRange.Text = $"Total - {quotes.Count } Pages";
                //}

                document.ActiveWindow.ActivePane.View.SeekView = Microsoft.Office.Interop.Word.WdSeekView.wdSeekCurrentPageFooter;
                Object oMissing = System.Reflection.Missing.Value;

                document.ActiveWindow.Selection.TypeText("Page ");
                Object TotalPages = Microsoft.Office.Interop.Word.WdFieldType.wdFieldNumPages;
                Object CurrentPage = Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage;

                document.ActiveWindow.Selection.Fields.Add(document.ActiveWindow.Selection.Range, ref CurrentPage, ref oMissing, ref oMissing);
                document.ActiveWindow.Selection.TypeText(" of ");
                document.ActiveWindow.Selection.Fields.Add(document.ActiveWindow.Selection.Range, ref TotalPages, ref oMissing, ref oMissing);
                document.ActiveWindow.Selection.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;

                //foreach (Microsoft.Office.Interop.Word.Section wordSection in document.Sections)
                //{
                //    Microsoft.Office.Interop.Word.Range footerRange = wordSection.Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                //    footerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlack;
                //    footerRange.Font.Size = 9;
                //    footerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphRight;
                //    footerRange.Text = $"Total - {quotes.Count } Pages";
                //}

                ////adding text to document  
                //document.Content.SetRange(0, 0);
                //document.Content.Text = "This is test document " + Environment.NewLine;

                //Add paragraph with Heading 1 style  
                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading1 = "Heading 1";
                para1.Range.set_Style(ref styleHeading1);
                para1.Range.Text = $"Quotes for { recipient.AccountNo }";
                para1.Range.InsertParagraphAfter();


                List<string> columnNames = new List<string>() { "Quote#", "Name/Description", "Quote Type", "St", "Valid From", "Valid To", "Created" };


                Table firstTable = document.Tables.Add(para1.Range, quotes.Count + 1, 7, ref missing, ref missing);

                firstTable.Borders.Enable = 1;
                foreach (Row row in firstTable.Rows)
                {
                    foreach (Cell cell in row.Cells)
                    {
                        //Header row  
                        if (cell.RowIndex == 1)
                        {
                            if (cell.ColumnIndex == 1 || cell.ColumnIndex == 4)
                            {
                                cell.Range.Columns.Width = 40.57f;
                            }
                            else if (cell.ColumnIndex == 5 || cell.ColumnIndex == 6)
                            {
                                cell.Range.Columns.Width = 75.57f;
                            }
                            else
                            {
                                cell.Range.Columns.Width = 168.57f;
                            }

                            cell.Range.Text = columnNames[cell.ColumnIndex - 1];
                            cell.Range.Font.Bold = 1;
                            //other format properties goes here  
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 8;
                            //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                            cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                            //Center alignment for the Header cells  
                            cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                        }
                        //Data row  
                        else
                        {
                            cell.Range.Font.Size = 7;
                            if (cell.ColumnIndex == 1)
                            {
                                cell.Range.Text = quotes[cell.RowIndex - 2].QuoteNumber;
                            }

                            if (cell.ColumnIndex == 2)
                            {
                                cell.Range.Text = quotes[cell.RowIndex - 2].Program;
                            }

                            if (cell.ColumnIndex == 3)
                            {
                                cell.Range.Text = quotes[cell.RowIndex - 2].PlanType;
                            }

                            if (cell.ColumnIndex == 4)
                            {
                                cell.Range.Text = quotes[cell.RowIndex - 2].St;
                            }

                            if (cell.ColumnIndex == 5)
                            {
                                var date = (DateTime?) quotes[cell.RowIndex - 2].StartDate;
                                cell.Range.Text = date != null ? date.Value.ToString("MM/dd/yyyy") : "";
                            }

                            if (cell.ColumnIndex == 6)
                            {
                                var date = (DateTime?)quotes[cell.RowIndex - 2].EndDate;
                                cell.Range.Text = date != null ? date.Value.ToString("MM/dd/yyyy") : null;
                            }

                            if (cell.ColumnIndex == 7)
                            {
                                cell.Range.Text = quotes[cell.RowIndex - 2].Created;
                            }
                        }
                    }
                }

                //Save the document
                string filename = @"quotes.docx";
                object path = Path.Combine(Environment.CurrentDirectory, @"document\", filename);

                document.SaveAs2(ref path);

                document.Close(ref missing, ref missing, ref missing);
                document = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return await uploadRepo.DownloadFileInDocumentDirectory(path.ToString());
            }
            catch (Exception ex)
            {
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;
                return null;
            }
        }

        public async Task<List<QuoteResults>> GetQuotes(DocumentQuoteInputDto input)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string IsAccepted = input.Filters.AcceptedQuotes ? ", 'CAREPLAN'" : "";
                string IsArchived = input.Filters.ArchiveDocs ? "" : " AND (D.DeletedRecord = 0 OR D.DeletedRecord Is NULL)";
                string DateFilter = string.Empty;

                if (!input.Filters.AllDates)
                {
                    var startDate = input.Filters.StartDate?.ToString("yyyy-MM-dd");
                    var endDate = input.Filters.EndDate?.ToString("yyyy-MM-dd");

                    DateFilter = $@" AND docstartdate > '{ startDate }' AND docenddate <= '{endDate}'";
                }

                string sql = $@"SELECT DISTINCT TOP {input.Filters.Display }
          d1.RecordNumber,
          doc_id,
          quotenumber,
          rp.programstatus,
          title AS [Careplan Name],
          [PlanType],
          Isnull(caredomain, 'VARIOUS') AS caredomain,
          Isnull(discipline, 'VARIOUS') AS discipline,
          Isnull(d1.program, 'VARIOUS') AS program,
          Isnull([Status], '')          AS [St],
            docstartdate             AS [Start Date],
          docenddate                    AS [End Date],
          [Created],
          [Modified],
          filename
FROM      (
                    SELECT    qh.RecordNumber,
							  doc_id,
                              CONVERT(varchar(10),d.created, 103) + ' ' +
                              (
                                     SELECT [Name]
                                     FROM   userinfo
                                     WHERE  recnum = d.author) AS created,
                                CONVERT(varchar(10),d.modified, 103) + ' ' +
                              (
                                     SELECT [Name]
                                     FROM   userinfo
                                     WHERE  recnum = d.typist) AS modified,
                              d.doc#                           AS careplanid,
                              (
                                     SELECT description
                                     FROM   datadomains
                                     WHERE  recordnumber = subid) AS plantype,
                              (
                                     SELECT [Name]
                                     FROM   humanresourcetypes
                                     WHERE  recordnumber = d.department
                                     AND    [Group] = 'PROGRAMS') AS program,
                               
                              (
                                     SELECT [Description]
                                     FROM   datadomains
                                     WHERE  recordnumber = d.dpid) AS discipline,
                              (
                                     SELECT [Description]
                                     FROM   datadomains
                                     WHERE  recordnumber = d.caredomain) AS caredomain,
                              d.title,
                              filename,
                              d.status,
                              d.docstartdate,
                                d.docenddate,
                              d.alarmdate,
                              d.alarmtext,
                              qh.doc# AS quotenumber
                    FROM      documents d
                    LEFT JOIN qte_hdr qh
                    ON        cpid = doc_id
                    WHERE     d.personid = @personID
                    AND       isnull([Status], '') <> ''
                    AND       documentgroup IN ('CP_QUOTE' { IsAccepted }) { IsArchived }) d1  
LEFT JOIN recipientprograms rp
ON        rp.program = d1.program 
ORDER BY  [DocStartDate] DESC";

                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", input.Quote.PersonId);

                    await conn.OpenAsync();
                    List<QuoteResults> list = new List<QuoteResults>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new QuoteResults()
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            DocID = GenSettings.Filter(rd["doc_id"]),
                            QuoteNumber = GenSettings.Filter(rd["QuoteNumber"]),
                            CarePlan = GenSettings.Filter(rd["CarePlan Name"]),
                            ProgramStatus = GenSettings.Filter(rd["ProgramStatus"]),
                            PlanType = GenSettings.Filter(rd["PlanType"]),
                            St = GenSettings.Filter(rd["St"]),
                            StartDate = GenSettings.Filter(rd["Start Date"]),
                            EndDate = GenSettings.Filter(rd["End Date"]),
                            Created = GenSettings.Filter(rd["Created"]),
                            Modified = GenSettings.Filter(rd["Modified"]),
                            Filename = GenSettings.Filter(rd["Filename"]),
                            Program = GenSettings.Filter(rd["Program"])
                        });
                    }

                    return list;
                }
            }
        }



        private void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

    }


}
