
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class CallReferralOutProcedure{

        public List<ProcedureRoster> Roster { get; set; }
        public ProcedureClientStaffNote Note { get; set; }

    }
}
