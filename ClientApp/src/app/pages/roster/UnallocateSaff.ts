
import { Component,AfterViewInit,Input,Output,EventEmitter,ChangeDetectorRef,ViewChild, ViewContainerRef, ComponentRef, ComponentFactoryResolver } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { GlobalService,staffnodes,StaffService,sbFieldsSkill, ShareService,timeSteps,conflictpointList,checkOptionsOne,sampleList,genderList,statusList, ListService, TimeSheetService,   } from '@services/index';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { filter, switchMap } from 'rxjs/operators';
import { EMPTY, forkJoin, Observable, Subject } from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import format from 'date-fns/format';

@Component({
    selector: 'unallocate-staff',
    templateUrl: './UnallocateSaff.html',
    styleUrls: ['./UnallocateSaff.css']

  
  })
  export class UnallocateStaff implements AfterViewInit{
  
    leaveData:Array<any> = [];
    unavailabilityList:Array<any> = [];
    datalist:Array<any> = [];
    lstPrograms:Array<any> = [];
    lstPayTypes:Array<any> = [];
    lstRosters:Array<any> = [];
    openRosterList:boolean;
     AddLeave:boolean=false;
    ViewList:boolean;
    UnavailableForBalanceDays:boolean=true;
    UnallocateAdminActivities:boolean=false;
    private observerable2:Observable<any>;
    ListType:any;
    token:any;
    UnallocateMaster:boolean=false;
    UnallocateAny:boolean=true

    loading:boolean;
    @Input() reallocate:boolean;
    @Input() findModalOpen:boolean;
    @Output() Done:EventEmitter<any>= new EventEmitter();
    @Input() loadUnallocateStaff :Subject<any> = new Subject() ;
    Main_Title:string;
    selected:any;
    _highlighted:any;
    s_Records:string
    selected_staff:any;
    chkPayCode:boolean=false;
    chkReason:boolean;
    PayType:any;
    currentDate:any;
    today = new Date();
    dateFormat: string ='dd/MM/yyyy';
    Program:any;
    recipient:any;
    HighlightRow2:number;
    serviceDuration:string ='1';
   
    chkProgram:boolean=true;
    s_Branch:string;
    s_LeavePayType:string;
    @Input()s_Mode:string;
    startDate:any;
    endDate:any;
    recipientList:Array<any> = [];
    ViewRecipientList:boolean;
    l_Transid:string;

    private unsubscribe = new Subject();
    constructor(private router: Router,
        private activeRoute: ActivatedRoute,
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private globalS: GlobalService,
        private listS: ListService,
        private cd: ChangeDetectorRef,
        private fb: FormBuilder,
        private componentFactoryResolver: ComponentFactoryResolver,
        private location: ViewContainerRef
        ){
        
      }
      ngOnInit(){
        this.currentDate=format(this.today,this.dateFormat);
           this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
 
            this.loadUnallocateStaff.subscribe(data=>{
            // this.findModalOpen=true;
            this.AddLeave=data.AddLeave??false;
            this.loading=true;
            this.selected = data.selected;
            this._highlighted = data.diary;
            this.serviceDuration='1';
            this.logDuration('1')
            this.load_data();
        });
      }
      ngAfterViewInit(): void {
        //if (this.selected!=null)
          //this.load_data();
      }
     

      load_data(){


        if(this.s_Mode==null || this.s_Mode==''){
          if (this._highlighted.length>1)
            this.s_Mode='UNALLOCATE MULTIPLE';
          else
            this.s_Mode='UNALLOCATE SINGLE';
        }
        
        
          this.s_Records='0';
         
          this._highlighted.forEach(v=>{
            if (v.recordno!=null)
            this.s_Records=this.s_Records + ',' + v.recordno
          })

         
        
        let sql = `SELECT UniqueID, Accountno, StaffGroup, JobStatus, stf_department
        ,DefaultUnallocateLeaveActivity AS Activity, DefaultUnallocateLeavePayType AS Paytype, '' AS Program, 
        DefaultLeavePayType
        FROM Staff WHERE accountNo='${this.selected.carercode}'`;

        this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(d=>{
        this.selected_staff=d[0];
        this.Main_Title = this.selected_staff.accountno + '(' + this.selected_staff.staffGroup+ ') ' + this.selected_staff.jobStatus;
        this.PayType = this.selected_staff.paytype
        this.s_Branch = this.selected_staff.stf_department;
        this.s_LeavePayType = this.selected_staff.defaultLeavePayType;        
        this.recipient = this.selected.recipient;

        this.getReasons();

         this.getLeaveBalances().subscribe(d=>{
          
            this.leaveData=d;
            this.loading=false;   
            this.cd.detectChanges();
            if (this.leaveData==null)
             this.leaveData=[];
            
            if (this.leaveData.length<=0){
              this.leaveData.push({
                recordNumber:'',
                leaveTypeCode:'', 
                leaveDescription:'',
                proRataDate:'', 
                balanceHours:'' 
              })
            }
          
         });
         
      })                  
       
       
    
      this.load_ServiceTypes();
     
      // If GetSecurityLevel(operatorid, "Staff") < 998 Then
      //    chk(3).Visible = False
      //    chk(4).Visible = False
      //    chk(0).Visible = False
      //    chk(11).Visible = False
      //   End If


      }
      
      getReasons(){
        let sql=`SELECT DISTINCT 0 as value, Title as label,0 as checked FROM ItemTypes WHERE MinorGroup = 'LEAVE' 
        AND ISNULL(EndDate, convert(varchar,getdate(),111)) >= convert(varchar,getdate(),111)`
        return this.listS.getlist(sql).subscribe(d=>{
          this.unavailabilityList=d;
          this.unavailabilityList.forEach(v=>{
            if (v.label==this.selected_staff.activity)
             v.checked=true;
            else
             v.checked=false;
          })
        });
      }

      getLeaveBalances(){
        let sql=`Select RecordNumber, Name As LeaveTypeCode, Address1 AS [LeaveDescription], Date1 As [ProRataDate], 
                Address2 AS [BalanceHours] 
                FROM HumanResources 
                WHERE PersonID = '${this.selected_staff.uniqueID}' 
                AND [Group] = 'LEAVEBALANCE' 
                ORDER BY  RecordNumber DESC`

        return this.listS.getlist(sql);
      }
     onItemSelected(sel:any ) : void {
         
    
    }
    onItemDbClick(sel:any ) : void {
        if (sel==null) return;
  
          
    }
    updateSingleCheckedFilters(index:number){

    }
    log(event:any,i:number){
      console.log(event);
    }
    logDuration(event:any){
      this.serviceDuration=event;
    }
    Clear_fields(){
      this.Main_Title=''
      this.Program=''
      this.s_Branch='';
      this.s_LeavePayType=''
      this.PayType=''
      this.selected=null;
      this.selected_staff=null;
      this.chkPayCode=false;
      this.chkProgram=false;
      this.chkReason=false;
      this.startDate=null;
      this.endDate=null;
    }
    handleCancel(){
      this.findModalOpen=false;  
      
      this.Clear_fields() ;  
      this.Done.emit(false)
    }
    setPayType(event:any){
      if (event){
       this.s_LeavePayType='QUERY FOR EACH ACTIVITY';
      // this.openPayTypesSelection();
      }else
       this.s_LeavePayType=this.selected_staff.defaultLeavePayType;
    }
  
    handleOk(){
     
      if (this.AddLeave){
        let leave_input={
          staffCode : this.selected_staff.accountno,
          FromDate : format(this.startDate,'yyyy/MM/dd'),
          ToDate : format(this.endDate,'yyyy/MM/dd'),
          User: this.token.user,         
          PayCode: this.s_LeavePayType,
          ActivityCode: this.selected_staff.activity,
          explanation : this.s_LeavePayType,
          Program: this.Program,
        

        }

        this.timeS.postleaveentry(leave_input).subscribe(d=>{         
          
          if (d.message!=null){
            this.globalS.eToast('Error',d.message);
            return ;
          }
           
    
        });

       ;
      }   
  
      if (this.UnallocateAdminActivities){
        this.openPayTypesSelection();
        //return;
      } 
    

      let input = {
        recordNo:this.s_Records,
        carerCode: this.selected_staff.accountno,
        serviceType: this.selected_staff.activity,
        serviceDescription: this.s_LeavePayType,
        program: this.Program,
        branch: this.s_Branch,
        user:this.token.user,
        reallocate: this.reallocate,
        addLeave : this.AddLeave,
        unavailableForBalanceDays: this.UnavailableForBalanceDays,
        unallocateAdminActivities: false,
        unallocateMaster: this.UnallocateMaster,
        unallocateAny: this.UnallocateAny,  
        serviceDuration: this.serviceDuration,
        acceptProgram: this.chkProgram,
      }
      this.timeS.unallocateStaff(input).subscribe(d=>{
        this.findModalOpen=false;        
       // this.Clear_fields();     
       if (this.UnallocateAdminActivities){
        if (this.lstRosters.length>0) 
          this.openRosterList=true;
        else
          this.Done.emit(true)
      }else  
          this.Done.emit(true);
      }) ;
      
     
    
    }
    DontRecord(){
      this.findModalOpen=false;  
     
      let inputs = {
        "opsType": Option,
        "user": this.token.user,
        "recordNo": this.selected.recordno,
        "isMaster": "0",
        "roster_Date" : this.selected.rdate,
        "start_Time": this.selected.start_Time,
        "carer_code": this.selected_staff.accountno,
        "recipient_code" : this.recipient,
        "notes" : this.selected.notes,
        'clientCodes' : this.recipient
    }
      this.timeS.ProcessRoster(inputs).subscribe(data => {
        this.Clear_fields();  
        this.Done.emit(true)
      });
     
    }
   
    openPayTypesSelection(){
      
      let sql = `SELECT recordno, [DATE] AS [Date], [START TIME] AS [StartTime], [carer code] AS Staff, 
                      [SERVICE TYPE] AS Activity, [SERVICE DESCRIPTION] AS [PayType], COSTQTY AS [PayQty]
                      FROM ROSTER WHERE type=6 and Recordno IN (${this.s_Records}) ORDER BY [DATE], [StartTime] `
  
      
            this.listS.getlist(sql).subscribe(data=>{
              this.lstRosters=data;
             
            });

 
    }

    RosterListDone(){
     // let sqlDuration=`UPDATE ROSTER SET DURATION = DURATION/5 WHERE RECORDNO IN " (${this.s_Records} ) "`
     // let sqlCost = `UPDATE Roster SET [CostUnit] = (SELECT TOP 1 [Unit] FROM Itemtypes WHERE Title = Roster.[Service Description] AND ProcessCLassification = 'INPUT') WHERE DMTransID = ${this.l_Transid}`

      let i=0;
      this.lstRosters.forEach(v=>{
        let input ={
    
        

          recordNo:v.recordno,
          carerCode: this.selected_staff.accountno,
          serviceType: this.selected_staff.activity,
          serviceDescription: v.payType,
          program: this.Program,
          branch: this.s_Branch,
          user:this.token.user,
          reallocate: this.reallocate,
          addLeave : this.AddLeave,
          unavailableForBalanceDays: this.UnavailableForBalanceDays,
          unallocateAdminActivities: this.UnallocateAdminActivities,
          unallocateMaster: this.UnallocateMaster,
          unallocateAny: this.UnallocateAny,  
          serviceDuration: this.serviceDuration,
          acceptProgram: this.chkProgram,
        }
        i=i+1
        this.timeS.unallocateStaff(input).subscribe(d=>{
         
          if(i>=this.lstRosters.length) {
            this.findModalOpen=false;     
            this.openRosterList=false;   
            this.Clear_fields();        
            this.Done.emit(true)
          }

        })   

       
      })
     
      
      
    }
    openList(type:string){
     this.ListType=type;
     this.datalist=[]
      switch(type){
          case 'Program':
             if(this.chkProgram) return;
             this.datalist=this.lstPrograms;
            break;
          case 'PayType':      
              if(this.chkPayCode) return;      
              this.datalist=this.lstPayTypes;
              break;               
          case 'Recipient':            
            this.datalist=this.recipientList;
            break;  
          
          default:
              this.datalist=[];  
      }

      this.ViewList=true;
    }
    logPayType(event:any){
      console.log(event);
    }
    load_ServiceTypes(){

   
      // if (!event) return;
      
      
       //let  sqlServiceType=`SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >= '${this.currentDate}') AND RosterGroup = 'ADMINISTRATION'  AND Status = 'NONATTRIBUTABLE'  AND ProcessClassification = 'OUTPUT'  ORDER BY Title`;
                 
       let  sqlpayType=`SELECT Recnum, LTRIM(RIGHT(Title, LEN(Title) - 0)) AS Title FROM ItemTypes WHERE RosterGroup = 'SALARY'   AND Status = 'NONATTRIBUTABLE'   AND ProcessClassification = 'INPUT'   AND Title BETWEEN ''                 AND 'zzzzzzzzzz'AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY TITLE`;
      
     
   
         this.observerable2 =  new Observable(observer => {
       
           forkJoin( 
              this.SelectFundingProgram()  ,  
              this.listS.getlist(sqlpayType) 
                             
           ).subscribe(data=>{
               this.lstPrograms=data[0];
               this.lstPayTypes=data[1];
              
           });
      
   });
   this.observerable2.subscribe(
       d => {
           console.log(d);
          
           this.loading=false;  
   
       },
       err => console.error('Observer got an error: ' + err),
       () => console.log('Observer got a complete notification')
     );

      }
     SelectFundingProgram()
     {   
     
         let SQLStmt='';
         let s_LimitScopeSQL :string
         let s_CloseDateSQL : string

         if (this.currentDate==null)
         this.currentDate=format(this.today,this.dateFormat);
     
         s_CloseDateSQL = ` (ISNULL(pr.CloseDate, '2000/01/01') < '${this.currentDate}') AND `;
         
         s_LimitScopeSQL = `SELECT DISTINCT [Program] AS Title FROM RecipientPrograms 
                       INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID 
                       INNER JOIN HumanResourceTypes pr ON RecipientPrograms.Program = pr.name 
                       WHERE Recipients.AccountNo = '${this.selected.recipient}' AND 
                       ${s_CloseDateSQL}
                       RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') 
                       ORDER BY [Program]`
     
         SQLStmt=`SELECT Name as Title FROM HumanResourceTypes HR WHERE  [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= '${this.currentDate}')  ORDER BY [Name]`
     
         if (!SQLStmt) return EMPTY;
             return this.listS.getlist(SQLStmt);     
     
     }
     
    

onListlItemSelected(data:any,i:number){
  this.HighlightRow2=i;
  this.SetListValue(data.title);
  
}
onListItemDbClick(data:any,i:number){
  this.HighlightRow2=i;
  this.SetListValue(data.title)
  this.ViewList=false;
 
}
ListSelectionDone(){
  this.ViewList=false;

        }
SetListValue(value:any){
 

  switch(this.ListType){
      case 'Program':
          this.Program=value;
          break;
      
      case 'PayType':
          this.s_LeavePayType=value;
          break;   
      case 'Recipient':
          this.recipient=value;
          break;  
      default:
         
    }
  }
    setProgram(event:any){
      this.chkProgram=event;
      if (event)
        this.Program= this.selected.rprogram
      else
      this.Program='';
    }
    RecipientSelectionDone(){
      this.ViewRecipientList=false;
    }
    openRecipient()
    {
      // if (this.s_Mode=='UNALLOCATE SINGLE') return;
      this.get_RecipList().subscribe(d=>{
        this.recipientList=d;        
       this. openList('Recipient')
      })
    }
get_RecipList()
{
  let sql='';
  
  console.log(this.s_Mode);

  switch (this.s_Mode){
    case 'PUT_STAFF_ON_LEAVE': //, PUT_STAFF_ON_LEAVE_ROSTERCOPY
        sql = `SELECT DISTINCT RECIPIENTS.UniqueID,
                             [Roster].[Client Code] as clientCode,Recipients.AccountNo as Title
                             FROM ROSTER INNER JOIN RECIPIENTS ON Roster.[Client Code] = Recipients.AccountNo 
                             WHERE ([Roster].[Status] = 1) AND 
                                   ([Carer Code] = '${this.selected_staff.accountno}') AND 
                                   ([Date] BETWEEN '${format(this.startDate,'yyyy/MM/dd')}' AND '${format(this.endDate,'yyyy/MM/dd')}') AND 
                                   ([Client Code] <> '!MULTIPLE') AND 
                                   ([Carer Code] <> '!INTERNAL')`
     break;
    case 'TERMINATE_STAFF':
      sql = `SELECT DISTINCT
      RECIPIENTS.UniqueID,[Roster].[Client Code] as clientCode, Recipients.AccountNo as Title
                             FROM ROSTER INNER JOIN RECIPIENTS ON Roster.[Client Code] = Recipients.AccountNo 
                             WHERE ([Roster].[Status] = 1) AND 
                                   ([Carer Code] =' ${this.selected_staff.accountno} ') AND 
                                   ([Date] >= '${format(this.startDate,'yyyy/MM/dd')}') AND 
                                   ([Client Code] <> '!MULTIPLE') AND 
                                   ([Carer Code] <> '!INTERNAL')`
      break;
    case 'UNALLOCATE MULTIPLE' :
         
        sql=` SELECT DISTINCT RE.UniqueID, ro.[Client Code] as clientCode, re.AccountNo as Title  FROM Roster ro INNER JOIN Recipients RE ON ro.[Client Code] = re.[Accountno] 
        WHERE RecordNo IN (${this.s_Records}) AND [Client Code] NOT IN ('!INTERNAL', '!MULTIPLE')`
       
    
    }
    return this.listS.getlist(sql);
  }

SetAllocationMessages(s_Type : string)
{
  let Msg:Array<any>=[];
  switch (s_Type)
  {
  case 'STAFFALLOCATION' :
      Msg.push( `Do you want to record staff leave or administration time, </br>
               to replace the original shift against this staff member?`)
      Msg .push( ` MANDATORY COMPETENCY COMPLIANCE is in place.  </br> 
               You have selected multiple services to allocate. </br>
               Apart from Agency wide mandatory competencies, there may be recipients or </br>
               service specific competencies in one or more of these services, </br> 
               and the staff member may be on leave for one or more of selected days. </br>    
               You may not be able to successfully allocate all of these services  </br>  
               to a selected staff member if the staff member is missing a    </br>
               current mandatory competency or is on leave for one or more of the days being allocated.  </br>   
               Do you wish to continue the allocation process??`)
      Msg.push( `COMPETENCY CHECKING is in place.      </br>
               You have selected multiple services to allocate. Apart from    </br>
               Agency wide mandatory competencies, there may be recipient or   </br> 
               service specific competencies in one or more of these services.  </br>  
               You will receive a warning for any of these services where the    </br>
               selected staff member is missing a current mandatory competency.  </br>   
               Do you wish to continue the allocation process??`)
      Msg.push( ` NB. PLEASE NOTE - Where a staff member has missing or expired competencies  </br>  
               for a specific service allocation - this allocation will not be completed -    </br>
               and you will need to redo the allocation for such services to an alternative qualified staff member`)
    break;
   case 'SERVICE DELETION' :
      Msg.push('')
      Msg.push('')
      Msg.push('')
      Msg.push('')
      break;
  case 'LOCATIONCHANGE' :
    Msg.push(` MANDATORY COMPETENCY COMPLIANCE is in place.      </br>
               You have selected to change the location of multiple services. Apart from   </br> 
               Agency wide mandatory competencies, there may be facility or    </br>
               service specific competencies in one or more of these facilities.    </br>
               You may not be able to successfully change the location of all of these services   </br> 
               to a selected location if the staff member is missing a    </br>
               current mandatory competency.     </br>
               Do you wish to continue the allocation process??`)
    Msg.push(` COMPETENCY CHECKING is in place.   </br>   
               You have selected to change the location of multiple services. Apart from    </br>
               Agency wide mandatory competencies, there may be facility or    </br>
               service specific competencies in one or more of these facilities.    </br>
               You will receive a warning for any of these services where the    </br>
               selected staff member is missing a current mandatory competency.   </br>  
               Do you wish to continue the allocation process??`)

      Msg.push(` NB. PLEASE NOTE - Where a staff member has missing or expired competencies </br>   
               for a specific service - this location change will not be completed -    </br>
               and you will need to redo the location change for such services to an alternative qualified staff member`)
               
               break;
    }
    return Msg;
  }




}

  