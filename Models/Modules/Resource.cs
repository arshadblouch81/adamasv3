namespace Adamas.Models.Modules{
    public class Resource{
        public string ResourceType { get; set; }
        public string CarerCode { get; set; }
        public string rDate { get; set; }
        public string RecipientCode { get; set; }
    }
}