using System;

namespace Adamas.Models.Modules{
    public class PayPeriod {        
        public DateTime Start_Date { get; set; }
        public DateTime End_Date { get; set; }
        public int PayPeriod_Length { get; set; }
        public string StartDay { get; set; }
      
        public DateTime PayPeriodEndDate { get; set; }

        public string get_Pay_Period()
        {
            return Start_Date.Date + " to " + End_Date.Date;
        }
    }
}