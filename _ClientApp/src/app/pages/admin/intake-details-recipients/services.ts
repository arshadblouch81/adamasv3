import { Component, OnInit,OnDestroy,ViewChild, ViewEncapsulation } from '@angular/core'
import { FormBuilder, FormGroup, FormArray } from '@angular/forms'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, ListService, status, period, billunit } from '@services/index'
import { GlobalService } from '@services/global.service'

import * as moment from 'moment';
import { BsModalComponent } from 'ng2-bs3-modal';
@Component({
    selector: '',
    templateUrl:'./services.html',
    styles: [`
        .form-group{
            margin-bottom: 5px;
        }
        i.add-i{
            float:right;
            margin-top: 5px;
        }
        table#imptTable{
            width: 100%;
            margin:0;
        }
        table#imptTable tr th {
            float:left;
        }
        table#imptTable thead {
            display:block;
        }
        table#imptTable tbody{
            display:block;
            height:4.3rem;
            overflow-y: overlay;
            width: 100%;
        }
        table#imptTable tr{
            display:block;
        }
        table#imptTable td{
            display:block;
        }
        table#imptTable th{
            display:block;
        }
        table#imptTable tbody td{
            float:left;
        }
        checkbox{
            display:block;
        }
        bs-modal >>> div.modal-dialog{
            width:50rem;
        }
    `]

})

export class IntakeServices implements OnInit, OnDestroy{
    @ViewChild('addServiceModal', { static: false }) addServiceModal: BsModalComponent;

    user: any
    dbAction: number
    private services$: Subscription
    services: Array<any>
    loading: boolean

    statusArr: Array<string> = status
    programsArr: Array<string>
    activityArr: Array<string>
    periodArr: Array<string> = period
    billUnitArr: Array<string> = billunit
    billToArr: Array<string>
    competenciesArr: Array<string>
    gstDisable: boolean = false
    excludeNDIADisable: boolean = false
    selectedCompetency: string;

    _competencies: Array<Dto.IntakeCompetency> = []


    serviceGroup: FormGroup
    activity: any;
    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private listS: ListService,
            private router: Router,
            private globalS: GlobalService,
            private formBuilder: FormBuilder
        ){
            this.services$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'services')){                    
                    this.reloadAll();
                }
            });
    }

    ngOnInit(){
        this.resetGroup();
        this.reloadAll();
    }

    ngOnDestroy(){
        this.services$.unsubscribe();
    }

    reloadAll(){
        this.search();
        this.dropDowns();
    }

    resetGroup(){
        this.serviceGroup = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            program: '',
            activity: '',
            status: '',
            frequency: '',
            competencies: this.formBuilder.array([]),
            period: '',
            duration: '',
            includeCase: false,
            gst: false,
            excludeNDIA: false,
            notes: '',
            billUnit: '',
            billTo: '',
            amount: '',
            autoInsertNotes: false,
            specialPricing: false
        })
        if(this.globalS.isEmpty(this.serviceGroup.value.program)){            
            this.serviceGroup.controls['activity'].disable()
        }
        this.pricingChange(false);
    }

    loadForm(recordNo: number){
        this.timeS.getservicedata(recordNo)
                    .subscribe(data => {       
                        console.log(data)       
                        this.activity = data.serviceType;

                        this.serviceGroup.patchValue({
                            recordNumber: data.recordNumber,
                            personID: data.personID,
                            program: data.serviceProgram,
                            activity: data.serviceType,
                            status: data.serviceStatus,
                            frequency: data.frequency,
                            period: data.period,
                            duration: data.duration,
                            includeCase: false,
                            gst: data.taxRate == "10" ? true : false,
                            excludeNDIA: data.excludeFromNDIAPriceUpdates,
                            notes: data.activityBreakDown,
                            billUnit: data.unitType,
                            billTo: data.serviceBiller,
                            amount: data.unitBillRate,
                            autoInsertNotes: data.autoInsertNotes,
                            specialPricing: data.forceSpecialPrice
                        })

                        this.addCompetency(data.humanResources)
                        this.programChange(data.serviceProgram)
                    })
    }

    dropDowns(){
        this.listS.getintakeprogram(this.user.uniqueID)
                    .subscribe(data => this.programsArr = data)        
    }

    competencyDropDown(){
        this.selectedCompetency = '';
        this.listS.getcompetenciesall().subscribe(data => this.competenciesArr = data)
    }

    billtoDropDown(){
        this.activity = ""
        this.activityArr = []
        this.listS.getintakerecipientall().subscribe(data => this.billToArr = data)
    }

    search(){
        this.user = this.sharedS.getPicked();     
        this.loading = true;    
        this.timeS.getintakeservices(this.user.uniqueID).subscribe(services => {
            this.loading = false
            this.services = services
        })
    }

    updateservice(data: any){
        this.addServiceModal.open();
        this.loadForm(data.recordNumber)
    }

    deleteservice(data: any){
        
    }

    programChange(program: string){

        if(this.globalS.isEmpty(program)) {
            this.serviceGroup.controls['activity'].disable()
            return;
        };
        this.serviceGroup.controls['activity'].enable()
        const _program = program.trim()
        this.activityArr = [] 

        this.listS.getintakeactivity(this.user.uniqueID, _program, moment().format('MM-DD-YYYY'))
                    .subscribe(data => { 
                        this.activityArr = data 
                        if(this.dbAction == 1){
                            this.activityArr.unshift(this.activity)
                        }
                    })
    }

    serviceProcess(){
        const data = this.serviceGroup.value

        const _service: Dto.IntakeServices = {
            recordNumber: data.recordNumber || null,
            personID: data.personID || this.user.uniqueID || '',
            serviceType: data.activity || '',
            frequency: data.frequency ? parseFloat(data.frequency) : null,
            period: data.period || '',
            duration: data.duration ? parseInt(data.duration) : null,
            unitType: data.billUnit || '',
            unitBillRate: data.amount ? parseFloat(data.amount) : null,
            activityBreakDown: data.notes || '',
            serviceProgram: data.program || '',
            serviceBiller: data.billTo || '',
            serviceStatus: data.status || '',
            taxRate: data.gst ? '10' : '0',
            forceSpecialPrice: data.specialPricing,
            autoInsertNotes: data.autoInsertNotes,
            excludeFromNdiaPriceUpdates: data.excludeNDIA,
            humanresources: data.competencies                                                    
        }
        
        if(this.dbAction == 0){
            this.timeS.postintakeservices(_service)
                    .subscribe(data => {
                        if(data){
                            this.reloadAll();
                            this.globalS.sToast('Success','Date Inserted')
                        }
                    })
        }

        if(this.dbAction == 1){           
            this.timeS.updateintakeservices(_service)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll();
                                this.globalS.sToast('Success','Date Updated')
                            }
                        })
        }
    }

    createItem(comp: any | string): FormGroup {
        const typeOf = typeof comp;
        if(typeOf === 'string'){
            return this.formBuilder.group({
                name: comp,
                group: this.serviceGroup.value.activity,
                recurring: false,
                recordNumber: null,
                personID: ''
            });
        }

        if(typeOf === 'object'){
            return this.formBuilder.group({
                name: comp.name,
                group: comp.group,
                recurring: comp.recurring,
                recordNumber: comp.recordNumber || null,
                personID: comp.personID
            });
        }

  
      }

    addCompetency(data: Array<any> | string){
        let compArrList = this.serviceGroup.get('competencies') as FormArray

        if(typeof data === 'string'){
           if(this.searchForSameCompetency(data)) {
                this.globalS.wToast('Competency is already listed','Warning')
                return
            }            
            compArrList.push(this.createItem(data))
        }

        if(Array.isArray(data)){
            const competencies = data
            for(let competency of data){       
                compArrList.push(
                    this.createItem(competency)
            )}
        }    
    }

    deleteCompetency(name: string){      
        let compArrList = this.serviceGroup.get('competencies') as FormArray;
        let found = -1;
        for(var a = 0; a < compArrList.length; a++){
            if(this.serviceGroup.value.competencies[a].name == name){
                found = a;
                break;
            }
        }

        if(found !== -1 && compArrList.value[a].recordNumber != null){
            this.timeS.deleteintakeservicecompetency(compArrList.value[a].recordNumber)
                        .subscribe(data => {
                            if(data){
                                compArrList.removeAt(found);
                                this.globalS.sToast('Sucess','Competency deleted')
                            }
                        })
        }
    }

    searchForSameCompetency(name: string): boolean{
        const compLen = this.serviceGroup.value.competencies.length;
        let found = false;
        if(compLen == 0) return;

        for(var a = 0, len = compLen; a < len; a++){
            if(this.serviceGroup.value.competencies[a].name == name){
                found = true;
                break;
            }
        }
        return found
    }

    pricingChange(data: boolean){
        if(!data){
            this.serviceGroup.controls['billUnit'].disable()
            this.serviceGroup.controls['gst'].disable()
            this.serviceGroup.controls['amount'].disable()
            this.serviceGroup.controls['billTo'].disable()
            this.gstDisable = true
            this.excludeNDIADisable = true
        }

        if(data){
            this.serviceGroup.controls['billUnit'].enable()
            this.serviceGroup.controls['amount'].enable()
            this.serviceGroup.controls['gst'].enable()
            this.serviceGroup.controls['billTo'].enable()
            this.gstDisable = false
            this.excludeNDIADisable = false
        }
    }
}