import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';

import {
    RouteGuard,
    AdminStaffRouteGuard,
    CanDeactivateGuard,
    LoginGuard,
    AdminRouteGuard,
    ByPassGuard
  } from '@services/index'
  
import {  
    RostersAdmin,
    ShiftDetail,
    AddRoster,
    RecipientExternal,
    StaffExternal,
    GNotes,
    CarerSearch,
    StaffSearch,
    RecipientSearch,
    DMRoster,
    ServiceTasks, 
    RosterExtraInfo,
    CancelShift,
    UnallocateStaff,
    RosterMain
  } from '../roster/index';
  
import {
    AttendanceAdmin,
    
    HomeAdmin,
    HomeV2Admin,
    LandingAdmin,
    SideMainMenu,
    RecipientsAdmin,
    ReportsAdmin,
    UserReports,  
    SessionsAdmin,
    StaffAdmin,
    TimesheetAdmin,
    ConfigurationAdmin,
    NDIAAdmin,
    chspDexAdmin,
    HCPComponent,
    PrintComponent,
    // BudgetAdmin, //AHSAN
    // GlanceAdmin, //AHSAN
    BillingAdmin,   //AHSAN
    DashboardAdmin, //AHSAN
    TimesheetProcessingAdmin,
    AccountingAdmin,   //AHSAN
    PurchaseOrder,
    Transport,
    ChatBot,
    XeroProcesses,
   
  
  } from '@admin/index';

  import {
    DayManagerAdmin,
    DayManagerMain,
    
  } from '@admin/daymanager/index'

  //branches
  import {  BranchesComponent  } from '../../pages/admin/configuration/genrel-setup/branches/branches.component';

//funding regions
// import{ FundingRegionsComponent } from './pages/admin/configuration/genrel-setup/funding-regions/funding-regions.component';
import{ FundingRegionsComponent } from '../../pages/admin/configuration/genrel-setup/funding-regions/funding-regions.component';


import { BudgetAdmin } from '@admin/dashboards/analyse-budget.component'; //AHSAN
import { GlanceAdmin } from '@admin/dashboards/glance.component'; //AHSAN
import { UserDetail } from '@admin/configuration/genrel-setup/userdetail';
import { Checklist } from '@admin/configuration/genrel-setup/checklist';



const routesmain: Routes = [ 
  {
  path: '',
  redirectTo: 'landing',
  pathMatch: 'full'
},
{
  path: 'landing',
  component: SideMainMenu
},
{
  path: 'home',
  component: HomeV2Admin
},
{
  path: 'daymanager-main',
  component: DayManagerMain
}, 

{
  path: 'time-attendance',
  component: AttendanceAdmin
},
{
  path: 'reports',
  component: ReportsAdmin
},
{
  path: 'user-reports',
  component: UserReports
},
{
path: 'rostermain',
component: RosterMain,

 },
{
  path: 'rosters',
  component: RostersAdmin,
  canDeactivate: [CanDeactivateGuard]
},
{
  path: 'Print',
  component: PrintComponent
},
{
  path: 'timesheet',
  component: TimesheetAdmin
},

{
  path: 'billing', //AHSAN
  component: BillingAdmin
},
{
  path: 'dashboards', //AHSAN
  component: DashboardAdmin
},
{
  path: 'pays', //AHSAN
  component: TimesheetProcessingAdmin,
  canActivate: [AdminRouteGuard]
},
{
  path: 'ndia',
  component: NDIAAdmin,
},
  {
  path: 'hcp',
  component: HCPComponent
  },
  {
    path: 'purcahse-order',
    component: PurchaseOrder
  },
  {
    path: 'transport',
    component: Transport
  },
  {
    path: '',
    component: RecipientsAdmin,
    children: [
        {
            path: 'recipient',
            loadChildren: () => import('./recipient-views/recipients-admin.module').then(m => m.RecipientsAdminModule)
        }
    ]
},
{
    path: '',
    component: StaffAdmin,
    children: [
        {
            path: 'staff',
            loadChildren: () => import('./staff-views/staff-admin.module').then(m => m.StaffAdminModule)
        }
    ]
}
,
{
  path: '',
  component: ConfigurationAdmin,

    children: [
        {
            path: 'configuration',
            loadChildren: () => import('./configuration/config-admin.module').then(m => m.ConfigAdminModule )
        }
    ]
}
]

const routes: Routes = [
    {
        path: '',
        redirectTo: 'landing',
        pathMatch: 'full'
    },
    {
        path: 'landing',
        component: SideMainMenu
    },
    {
        path: 'daymanager',
        component: DayManagerAdmin
    },
    {
      path: 'daymanager-main',
      component: DayManagerMain
  },
    {
        path: 'time-attendance',
        component: AttendanceAdmin
    },
    {
        path: 'reports',
        component: ReportsAdmin
    },
    {
      path: 'user-reports',
      component: UserReports
    },
    {
      path: 'rostermain',
      component: RosterMain,
    
  },
    {
        path: 'rosters',
        component: RostersAdmin,
        canDeactivate: [CanDeactivateGuard]
    },
    {
      path: 'Print',
      component: PrintComponent
    },
    {
        path: 'timesheet',
        component: TimesheetAdmin
    },
   
      {
        path: 'ndia',
        component: NDIAAdmin,
      },
      {
        path: 'chspDex',
        component:chspDexAdmin,
      },
      {
        path: 'analyse-budget', //AHSAN
        component: BudgetAdmin
      },
      {
        path: 'glance', //AHSAN
        component: GlanceAdmin
      },
      {
        path: 'billing', //AHSAN
        component: BillingAdmin
      },
      {
        path: 'dashboards', //AHSAN
        component: DashboardAdmin
      },
      // {
      //   path: 'award-list', //AHSAN
      //   component: AwardListComponent
      // },
      {
        path: 'pays', //AHSAN
        component: TimesheetProcessingAdmin,
        canActivate: [AdminRouteGuard]
      },
     
      {
        path: 'hcp',
        component: HCPComponent
      },
    
      {
        path: 'branches',
        component: BranchesComponent
      },
      {
        path: 'funding-region',
        component: FundingRegionsComponent
      },
   
      {
        path:"user-detail",
        component:UserDetail
      },
     
      {
        path: 'shiftdetail',
        component: ShiftDetail
      },
      {
        path: 'addroster',
        component: AddRoster
      },
      {
        path: 'recipient-external',
        component: RecipientExternal
      },
      {
        path: 'staff-external',
        component:StaffExternal
      },
      {
        path: 'gnotes',
        component:GNotes
      },
      {
        path: 'carer-search',
        component:CarerSearch
      },
      {
        path: 'staff-search',
        component:StaffSearch
      },
      
      {
        path: 'recipient-search',
        component:RecipientSearch
      },
      
      {
        path: 'dm-roster',
        component:DMRoster
      },
      {
        path: 'service-tasks',
        component:ServiceTasks
        
      },
      {
        path: 'roster-extrainfo',
        component:RosterExtraInfo
        
      },
      {
        path: 'cancel-shift',
        component:CancelShift
        
      },
      {
        path: 'unallocate-staff',
        component:UnallocateStaff      
      },        
      {
        path: 'purcahse-order',
        component: PurchaseOrder
      },
      {
        path: 'transport',
        component: Transport
      },
      {
        path :'XeroProcesses',
        component : XeroProcesses
      },
    {
        path: '',
        component: RecipientsAdmin,
        children: [
            {
                path: 'recipient',
                loadChildren: () => import('./recipient-views/recipients-admin.module').then(m => m.RecipientsAdminModule)
            }
        ]
    },
   
    {
        path: '',
        component: StaffAdmin,
        children: [
            {
                path: 'staff',
                loadChildren: () => import('./staff-views/staff-admin.module').then(m => m.StaffAdminModule)
            }
        ]
    }
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routesmain),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,
    NzSpinModule,
    NzDrawerModule,
    NzButtonModule,
    NzFormModule,
    NzDatePickerModule,
    NzModalModule,
    NzTableModule,
    NzDividerModule
  ],
  declarations: [
    
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class AdminMainModule {}

