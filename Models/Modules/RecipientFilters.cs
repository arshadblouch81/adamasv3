using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Adamas.Models.Modules{
    public class RecipientFilters {

        public string searchText { get; set; }
        public Boolean? alltypes { get; set; }
        public string selectedTypes {get;set;}
        public string surname {get;set;}
        public string firstname { get; set; }
        public string phoneno { get; set; }
        public string dob { get; set; }
        public string fileno { get; set; }        
        public string suburb {get;set;}
        public Boolean? active { get; set; }
        public Boolean? inactive { get; set; }
        public Boolean? allBranches { get; set; }
        public string selectedbranches {get;set;}
        public Boolean? allProgarms { get; set; }
        public string selectedPrograms {get;set;}
        public Boolean? activeprogramsonly { get; set; }
        public Boolean? allCordinatore { get; set; }
        public string selectedCordinators {get;set;}
        public Boolean? allcat { get; set; }
        public string selectedCategories {get;set;}
        public List<CriteriaList> Criterias { get; set; }
    }
}