using System;

namespace Adamas.Models.Modules{
    public class Insurance{
        public string RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string MedicareNumber { get; set; }
        public string MedicareRefNumber { get; set; }
        
        public DateTime? MedicareExpiry { get; set; }
        public string MedicareRecipientID { get; set; }
        public string PensionStatus { get; set; }     
        public string ConcessionNumber { get; set; }
        public string DVANumber { get; set; }
        public  string AmbulanceType { get; set; }
        public string HACCDVACardholderStatus { get; set; }
        public bool? DVABenefits { get; set; } = false;
        public bool? PensionVoracity { get; set; } = false;
        public bool? Ambulance { get; set; } = false;
        public DateTime? DateOfDeath { get; set; }
        public bool? WillAvailable { get; set; }
        public string WhereWillHeld { get; set; }
        public string FuneralArrangements { get; set; }


    }
}