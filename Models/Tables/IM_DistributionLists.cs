using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Adamas.Models.Tables {
    public class IM_DistributionLists
    {
        [Key]
        public int RecordNo { get; set; }
        public string Program { get; set; }
        public string Location { get; set; }
        public string Activity { get; set; }
        public string Recipient { get; set; }
        public string Staff { get; set; }
        public string StaffToNotify { get; set; }

        [Column("Mandatory", TypeName = "bit")]
        [DefaultValue(false)]
        public Boolean Mandatory { get; set; }
        
        [Column("DefaultAssignee", TypeName = "bit")]
        public Boolean? DefaultAssignee { get; set; }

        public string ListName { get; set; }
        public string ListGroup { get; set; }
        public string Severity { get; set; }
    }
}