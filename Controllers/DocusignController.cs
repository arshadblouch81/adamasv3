﻿using AdamasV3.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;
using Xero.NetStandard.OAuth2.Model.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Adamas.Models.Modules;
using AdamasV3.Models.Dto;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using UserInfo = DocuSign.eSign.Client.Auth.OAuth.UserInfo;
using Account = DocuSign.eSign.Client.Auth.OAuth.UserInfo.Account;
using System.Linq;
using Microsoft.Office.Interop.Word;
using Microsoft.AspNetCore.Hosting;
using static DocuSign.eSign.Client.Auth.OAuth;
using AdamasV3.DAL.DocusignRepository;
using MailKit;

using System.Management;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocusignController : ControllerBase
    {

        private IConfiguration Configuration;
        private IDocusignService DocusignService;
        private readonly IWebHostEnvironment env;
        private string SendingType = string.Empty;
        public DocusignController(
                IConfiguration _configuration,
                IDocusignService _DocusignService,
                IWebHostEnvironment _env
            ) { 
            Configuration = _configuration;
            DocusignService = _DocusignService;
            env = _env;

            this.SendingType = this.Configuration.GetSection("Docusign:SendingType")?.Value;
        }

        [HttpGet("login")]
        public async Task<IActionResult> GetLogin()
        
        {
            
        // public async Task<IActionResult> GetLogin()
        // https://account-d.docusign.com/oauth/auth?
        // response_type=code
        // &scope=YOUR_REQUESTED_SCOPES
        // &client_id=YOUR_INTEGRATION_KEY
        // &redirect_uri=YOUR_REDIRECT_URI
        
            // Called Integration Key in the Admin Demo Dashboard
            var ClientId = Configuration.GetSection("Docusign:ClientId")?.Value;
            var AuthorizationEndPoint = Configuration.GetSection("Docusign:AuthorizationEndpoint")?.Value;
            var CallBackUrl = Configuration.GetSection("Docusign:CallBackUrl")?.Value;
            var state = Configuration.GetSection("Docusign:State")?.Value;

         //  string redirectUrl = @$"{AuthorizationEndPoint}?client_id={ClientId}&scope=signature&response_type=code&redirect_uri={CallBackUrl}&state=CfDJ8A7vXswLjSFPoFuT_ByeZnDfp43gxhfuK1fxjpuBRPqrbbBrv9YBY7dObiZcoN4eLFt0YdzYjwubsJzJ655sx1n0D_FnaGLxjSuit0Nab7VUgyWqjf9U6i0ksEUt7C0iTUWdAGEtby8x7o_TeHdUAiBqsEEaVrib2_8y5D8IalnVTlpMWHIXhUDa7yHKRwtmEfT2glKzTmUTX6x1qGXIXsc";
           string redirectUrl = @$"{AuthorizationEndPoint}?client_id={ClientId}&scope=signature&response_type=code&redirect_uri={CallBackUrl}&state={state}";

            OpenBrowser(redirectUrl);
            return Ok();
        }

        [HttpGet("test-copy-paste-file-procedure")]
        public async Task<IActionResult> TestCopyPasteProcedure(string f, string s, string d)
        {
            await DocusignService.TestCopyPasteProcedure(f,s,d);
            return Ok(true);
        }

        [HttpGet("callback")]
        public async Task<IActionResult> GetCallBack(string code, string state)
        {
            var ClientId = Configuration.GetSection("Docusign:ClientId")?.Value;
            var TokenEndpoint = Configuration.GetSection("Docusign:TokenEndpoint")?.Value;
            var ClientSecret = Configuration.GetSection("Docusign:ClientSecret")?.Value;

            //Headers
            var BTOA = DocusignService.BTOAEncoding(@$"{ClientId}:{ClientSecret}");

            //Body
            List<KeyValuePair<string, string>> postData = new List<KeyValuePair<string, string>>();
            postData.Add(new KeyValuePair<string, string>("grant_type", "authorization_code"));
            postData.Add(new KeyValuePair<string, string>("code", code));

            try
            {
                using (HttpClient httpClient = new HttpClient())
                using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, TokenEndpoint))
                {
                    requestMessage.Headers.Clear();
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", BTOA);

                    var content = new FormUrlEncodedContent(postData);
                    requestMessage.Content = content;

                    var response = await httpClient.SendAsync(requestMessage);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        DocusignService.PostToken(result);
                        return Ok(result);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return Ok();
        }

        [HttpGet("envelope/list")]
        public async Task<IActionResult> GetEnvelopeStatus()
        {
            var token =  await DocusignService.GetToken();
            var accountId = Configuration.GetSection("Docusign:APIAccountId")?.Value;
            var baseUri = Configuration.GetSection("Docusign:BaseUri")?.Value;

            var url = $@"{baseUri}/restapi/v2.1/accounts/{accountId}/envelopes";
            var query = new Dictionary<string, string>()
            {
                ["from_date"] = "2022-05-02T01:44Z"
            };

            var uri = QueryHelpers.AddQueryString(url, query);


            using (HttpClient httpClient = new HttpClient())
            using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, uri))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);

                var response = await httpClient.SendAsync(requestMessage);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    return Ok(result);
                }
            }

            return Ok();
        }

        //[HttpPost("create")]
        //public async Task<IActionResult> PostEnvelope([FromBody] DocusignDocument document = null)
        //{
        //    var redirect = await DocusignService.PostEnvelope(document);
        //    OpenBrowser(redirect);
        //    return Redirect(redirect);
        //}

        [HttpPost("create")]
        public async Task<IActionResult> PostJwtEnvelope([FromBody] DocusignDocument document)
        {
            OAuthToken accessToken = null;
            try
            {
                accessToken = DocusignService.AuthenticateWithJWT();
              //  Console.WriteLine("Access Token: " + accessToken.access_token);
              //  Console.WriteLine("client_id: " + Configuration.GetSection("DocuSignJWT:ClientId")?.Value );
              
                
            } catch(ApiException apiExp)
            {
                // Consent for impersonation must be obtained to use JWT Grant
                if (apiExp.Message.Contains("consent_required"))
                {
                    // Caret needed for escaping & in windows URL
                    string caret = "";
                    //if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    //{
                    //    caret = "^";
                    //}

                    // build a URL to provide consent for this Integration Key and this userId
                    string url = "https://" + Configuration.GetSection("DocuSignJWT:AuthServer")?.Value + "/oauth/auth?response_type=code" + caret + "&scope=impersonation%20signature" + caret +
                        "&client_id=" + Configuration.GetSection("DocuSignJWT:ClientId")?.Value + caret + "&redirect_uri=" + Configuration.GetSection("DocuSignJWT:RedirectURL")?.Value;

                    return Ok(url);
                }
            }

            var ContentRootPath =  env.ContentRootPath;// System.IO.Path.Combine(env.ContentRootPath, "document");
          
            var filesDirectory = await DocusignService.GetDocumentDetails(document.DOC_ID);
                   
            var result = DocusignService.CopyMTADocument(filesDirectory.Filename, filesDirectory.OriginalLocation, ContentRootPath);

            var docuSignClient = new DocuSignClient();
            docuSignClient.SetOAuthBasePath(Configuration.GetSection("DocuSignJWT:AuthServer")?.Value);

            Account acct = DocusignService.GetAccount(docuSignClient, accessToken);
            //Console.WriteLine("Email: " +this.SendingType);
          
            if(this.SendingType == "Email")
            {

                DocusignDto dSignDto = SigningViaEmail.SendEnvelopeViaEmail(
                    document.Signers,
                    document.Documents,
                    "sent",
                    acct.BaseUri + "/restapi",
                    accessToken.access_token,
                    acct.AccountId,
                    env.ContentRootPath
                    );

                await DocusignService.UpdateDocumentStatus(dSignDto.EnvelopeId, "Sent", "", document.DOC_ID);
                return Ok(dSignDto.EnvelopeId);

            }
           
            if(this.SendingType == "EmbeddedSigning")
            {
                DocusignDto dSignDto = EmbeddedSending.SendEnvelopeUsingEmbeddedSending(
                    document.Signers,
                    document.Documents,
                    "created",
                    acct.BaseUri + "/restapi",
                    accessToken.access_token,
                    acct.AccountId,
                    ContentRootPath); //

                await DocusignService.UpdateDocumentStatus(dSignDto.EnvelopeId, "Sent", "", document.DOC_ID);
                OpenBrowser(dSignDto.DocusignEmbeddedUrl);
                return Ok(dSignDto.EnvelopeId);
            }

            return Ok();
        }

        [HttpPost("run-pshell")]
        public async Task<IActionResult> PostRunPshell()
        {
            string pathToScript = Path.Combine(env.ContentRootPath, "Scripts", "browserRun.ps1");

            var scriptArguments = "-ExecutionPolicy Bypass -File \"" + pathToScript + "\"";
            var processStartInfo = new ProcessStartInfo("powershell.exe", scriptArguments);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;

            using var process = new Process();
            process.StartInfo = processStartInfo;
            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            return Ok();
        }


        [HttpPost("copy-mta")]
        public async Task<IActionResult> PostCopyMTA([FromBody] RemoteDocumentCopy docCopy)
        {
            // \\SJCC - SYDFP01.SJCC.local\CompanyData\Corp
            if (!docCopy.DisableDirectoryExists)
            {
                if (string.IsNullOrEmpty(docCopy.UncPathDirectory)) throw new Exception("UNC Path is empty");
                if (!Directory.Exists(docCopy.UncPathDirectory))
                {
                    throw new Exception("UNC Path does not exist");
                }

                if (string.IsNullOrEmpty(docCopy.UncPathDirectory)) throw new Exception("File Path is empty");
                if (!Directory.Exists(docCopy.FilePath))
                {
                    throw new Exception("File Path does not exist");
                }
            }

            var directory = this.DocusignService.RemovePathRoot(docCopy.FilePath, docCopy.UncPathDirectory);
  
            var filePath = Path.Combine(directory, docCopy.FileName);

            var result = this.DocusignService.CopyMTADocument(docCopy.FileName, directory);

            return Ok(new
            {
                Directory = directory,
                FilePath = filePath,
                Result = result
            });
        }

        public static void OpenBrowser(string url)  
        {
            try
            {
                Process.Start(url);
            }
            catch
            {
                // hack because of this: https://github.com/dotnet/corefx/issues/10361
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    url = url.Replace("&", "^&");
                    Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    Process.Start("xdg-open", url);
                }
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    Process.Start("open", url);
                }
                else
                {
                    throw;
                }
            }
        }

    }
}
