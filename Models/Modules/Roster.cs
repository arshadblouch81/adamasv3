namespace Adamas.Models.Modules{
    public class Roster{
        public string RosterType { get; set; }
        public string AccountNo { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}