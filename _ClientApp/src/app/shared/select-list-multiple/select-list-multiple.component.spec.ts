import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectListMultipleComponent } from './select-list-multiple.component';

describe('SelectListMultipleComponent', () => {
  let component: SelectListMultipleComponent;
  let fixture: ComponentFixture<SelectListMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectListMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectListMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
