
namespace Adamas.Models.Modules{
    public class ServiceNote {
        public string Client { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}