﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MimeKit;
using MimeKit.Text;
using Newtonsoft.Json.Linq;

using Adamas.Models.Modules;
using System.Text;
using MailKit.Security;

namespace Adamas.DAL
{
    public class EmailService: IEmailService
    {
        private readonly IEmailConfiguration _emailConfiguration;

        public EmailService(
            IEmailConfiguration emailConfiguration
            )
        {
            _emailConfiguration = emailConfiguration;
        }

        public async Task<EmailOutput> SendBookingNotification(List<MailboxAddress> addresses, AddBooking booking, double billQty)
        {
            var message = new MimeMessage();

            try
            {
                

                message.From.Add(
                    new MailboxAddress(
                        _emailConfiguration.SmtpOriginName,
                        _emailConfiguration.SmtpOriginEmail
                    ));

                message.To.AddRange(addresses);
                message.Subject = "BOOKING";

                string emailContent = string.Empty;

                if (booking.BookingType != "FourWeekly" && booking.BookingType != "Fortnightly" && booking.BookingType != "Weekly")
                {

                    emailContent = string.Format($@"<div style='background:#f9f9f9;font-family: Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;'>
                                <div style='margin: 0 auto; max-width: 640px; background: #ffffff; border-radius: 4px; overflow: hidden;padding: 24px 24px 0px;'>
                                    <h4 style='color:#4f545c;margin:0'><span style='color:#7289da;font-size:25px'>BOOKING from #</span> {booking.ClientCode}</h4>
                                    <div style='margin:0px auto;max-width:640px;background:#ffffff'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 40px 0px'><div aria-labelledby='mj-column-per-100' class='m_5740320362880984956mj-column-per-100' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px'><p style='font-size:1px;margin:0px auto;border-top:1px solid #f0f2f4;width:100%'></p></td></tr></tbody></table></div></td></tr></tbody></table></div>
                                    <div style='padding: 24px 24px 0px;'>
                                        <div style='margin-bottom:1rem;'>
                                            <label for='' style='font-weight: bold; color: #1a9dff; width: 12rem; display: inline-block;'>Date</label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{ Convert.ToDateTime(booking.StartDate).ToString("dd/MM/yyyy") }</span>
                                        </div>
                                        <div style='margin-bottom:1rem;'>
                                            <label for='' style='font-weight: bold; color: #1a9dff; width: 12rem; display: inline-block;'>Time</label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{booking.StartTime} - {booking.Duration}</span>
                                        </div>
                                        <div style='margin-bottom:1rem;'>
                                            <label for='' style='font-weight: bold; color: #1a9dff; width: 12rem; display: inline-block;'>Service Type</label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{booking.Service.ServiceType}</span>
                                        </div>
                                        <div style='margin-bottom:1rem;'>
                                            <label for='' style='font-weight: bold; color: #1a9dff; width: 12rem; display: inline-block;'>Notes</label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{booking.Notes ?? "-"}</span>
                                        </div>
                                    </div>
                                    <div style='margin:0px auto;max-width:640px;background:#ffffff'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 40px 0px'><div aria-labelledby='mj-column-per-100' class='m_5740320362880984956mj-column-per-100' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px'><p style='font-size:1px;margin:0px auto;border-top:1px solid #f0f2f4;width:100%'></p></td></tr></tbody></table></div></td></tr></tbody></table></div>
                                    <div  style='padding: 24px 24px 0px;'>
                                        <div style='margin-bottom:1rem;'>
                                            <label for='' style='font-weight: bold; color: #74d474; text-transform: uppercase; width: 12rem; display: inline-block; '>Total</label>
                                            <div style='padding:10px 20px 0 20px;display:flex'>
                                                <div style='float:left;width: 8rem; align-items: center;'>
                                                    <div style='font-size: 12px; margin-bottom: 5px;font-weight: bold; color: #636363;'>Rate</div>
                                                    <div>{Convert.ToDecimal(booking.Service.ServiceBillRate).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au"))}</div>
                                                </div>
                                                <div style='float:left;width: 5rem; align-items: center; display: flex;'>
                                                   X
                                                </div>
                                                <div style='float:left;width: 5rem; align-items: center;'>
                                                    <div style='font-size: 12px; margin-bottom: 5px;font-weight: bold; color: #636363;'>Duration</div>
                                                    <div>{billQty}</div>
                                                </div>
                                                <div style='float:left;width: 5rem; align-items: center; display: flex;'>
                                                    =
                                                 </div>
                                                 <div style='float:left;width: 5rem; align-items: center;'>
                                                    <div style='font-size: 12px; margin-bottom: 5px;font-weight: bold; color: #636363;'>Total</div>
                                                    <div>{ ((Convert.ToDecimal(booking.Service.ServiceBillRate) * Convert.ToDecimal(billQty)).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")))}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px' align='center'><div style='color:#99aab5;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:12px;line-height:24px;text-align:center'>
                                    This is a generated Email                
                                </div>
                           </div>");
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var rb in booking.RealDateBookings)
                    {
                        sb.Append(
                        $@"<li>
                                 <div>
                                    <h4>{ rb.Time.ToString("dd/MM/yyyy")} -  {rb.Week}{ GetOrdinalSuffix(rb.Week) } Week <span style='color:#7289da'>({rb.Time.ToString("dddd")})</span></h4>
                                    <table style='margin-left:4rem;width: 25rem;'>
                                        <tr>
                                            <td style='width:200'>Start Time : {(rb.Time).ToString("HH:mm")}</td>
                                            <td style='width:200'>Duration : { rb.Quantity } hr</td>
                                        </tr>
                                    </table>
                                 </div>
                              </li>");
                    };

                    emailContent = $@"<div style='background:#f9f9f9;font-family: Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;'>
                            <div style='margin: 0 auto; max-width: 640px; background: #ffffff; border-radius: 4px; overflow: hidden;padding: 24px 24px 0px;'>
                                <h4 style='color:#4f545c;margin:0'><span style='color:#7289da;font-size:25px'>BOOKING from </span> {booking.ClientCode}</h4>
                                <div style='margin:0px auto;max-width:640px;background:#ffffff'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 40px 0px'><div aria-labelledby='mj-column-per-100' class='m_5740320362880984956mj-column-per-100' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px'><p style='font-size:1px;margin:0px auto;border-top:1px solid #f0f2f4;width:100%'></p></td></tr></tbody></table></div></td></tr></tbody></table></div>
                                <div style='padding: 24px 24px 0px;'>
                                    <div style='margin-bottom:1rem;'>
                                        <div style='white-space: pre-wrap;'>{booking.Summary}</div>
                                    </div>                                    
                                </div>
                                <div style='margin:0px auto;max-width:640px;background:#ffffff'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 40px 0px'><div aria-labelledby='mj-column-per-100' class='m_5740320362880984956mj-column-per-100' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px'><p style='font-size:1px;margin:0px auto;border-top:1px solid #f0f2f4;width:100%'></p></td></tr></tbody></table></div></td></tr></tbody></table></div>
                                 <div style='padding: 24px 24px 0px;'>
                                    <div style='margin-bottom:1rem;'>
                                        <label for='' style='font-weight: bold; color: #1a9dff; width: 12rem; display: inline-block;'>Service Type</label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{booking.Service.ServiceType}</span>
                                    </div>
                                    <div style='margin-bottom:1rem;'>
                                        <label for='' style='font-weight: bold; color: #1a9dff; width: 12rem; display: inline-block;'>Notes</label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{booking.Notes ?? "-"}</span>
                                    </div>
                                </div>
                                <div style='margin:0px auto;max-width:640px;background:#ffffff'><table role='presentation' cellpadding='0' cellspacing='0' style='font-size:0px;width:100%;background:#ffffff' align='center' border='0'><tbody><tr><td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 40px 0px'><div aria-labelledby='mj-column-per-100' class='m_5740320362880984956mj-column-per-100' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px'><p style='font-size:1px;margin:0px auto;border-top:1px solid #f0f2f4;width:100%'></p></td></tr></tbody></table></div></td></tr></tbody></table></div>
                                <div  style='padding: 24px 24px 0px;'>
                                    <div style='margin-bottom:1rem;'>
                                        <label for='' style='font-weight: bold; color: #74d474; text-transform: uppercase; width: 12rem; display: inline-block; '>Dates Booked</label>
                                        <ul style='list-style:none'>                                        
                                          {sb}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div aria-labelledby='mj-column-per-100' style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'><table role='presentation' cellpadding='0' cellspacing='0' width='100%' border='0'><tbody><tr><td style='word-break:break-word;font-size:0px;padding:0px' align='center'><div style='color:#99aab5;font-family:Whitney,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:12px;line-height:24px;text-align:center'>
                                This is a generated Email                
                            </div>
                        </div>";             
                }

                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = emailContent
                };

                try
                {
                    using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                    {
                        //emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;

                        //await emailClient.ConnectAsync(
                        //    _emailConfiguration.SmtpServer,
                        //    _emailConfiguration.SmtpPort,
                        //    true).ConfigureAwait(false);

                        //emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        await emailClient.ConnectAsync(
                            _emailConfiguration.SmtpServer,
                            _emailConfiguration.SmtpPort,
                            SecureSocketOptions.StartTls);

                        await emailClient.AuthenticateAsync(
                            _emailConfiguration.SmtpUsername,
                            _emailConfiguration.SmtpPassword)
                            .ConfigureAwait(false);

                        await emailClient.SendAsync(message);
                        await emailClient.DisconnectAsync(true);

                        return new EmailOutput()
                        {
                            ReplyTo = message.ReplyTo.ToString(),
                            CC = message.Cc.ToString(),
                            To = message.To.ToString(),
                            From = message.From.ToString(),
                            Status = new IsSuccessEmail()
                            {
                                IsSuccess = true,
                                Log = ""
                            }
                        };
                    }
                }
                catch (Exception ex)
                {
                    return new EmailOutput()
                    {
                        ReplyTo = message.ReplyTo.ToString(),
                        CC = message.Cc.ToString(),
                        To = message.To.ToString(),
                        From = message.From.ToString(),
                        Status = new IsSuccessEmail()
                        {
                            IsSuccess = false,
                            Log = ex.ToString()
                        }
                    };
                }
            } catch(Exception ex)
            {
                return new EmailOutput()
                {
                    ReplyTo = message.ReplyTo.ToString(),
                    CC = message.Cc.ToString(),
                    To = message.To.ToString(),
                    From = message.From.ToString(),
                    Status = new IsSuccessEmail()
                    {
                        IsSuccess = false,
                        Log = ex.ToString()
                    }
                }; ;
            }
        }

        public async Task<EmailOutput> SendBookingCancelNotification(List<MailboxAddress> addresses, CancelBooking booking, Adamas.Models.Tables.Roster roster)
        {

            var message = new MimeMessage();

            message.From.Add(
                new MailboxAddress(
                    _emailConfiguration.SmtpOriginName,
                    _emailConfiguration.SmtpOriginEmail
                ));

            message.To.AddRange(addresses);
            message.Subject = "BOOKING-CANCELLATION";

            string emailContent = string.Empty;

            var time = TimeSpan.Parse(roster.StartTime);
            var startTime = DateTime.Today.Add(time);
            
            var endTimeStr = startTime.AddMinutes(Convert.ToInt32(roster.Duration) * 5);
            var endTime = endTimeStr.ToString("HH:mm");

            var dateParse = DateTime.Parse(roster.Date);


            message.Body = new TextPart(TextFormat.Html)
            {
                Text =
                       string.Format($@"<div style='width: 800px; font-size:11px;display: inline-block; padding: 0 1rem 1rem 1rem; border: 1px solid #dddddd; border-top: 2px solid #32bee6; border-radius: 2px; color: #3a3a3a;'> 
                           <h3 style='border-bottom: 1px solid #32bee6; padding: 5px 0'>A booking for { dateParse.ToString("dd/MM/yyyy") }, {roster.ClientCode} has been cancelled</h3> 
                            <div style='padding: 24px 24px 0px;'>
                                <div style='margin-bottom:1rem;'>
                                    <label for='' style='font-weight: bold; color: #1a9dff; width: 8rem; display: inline-block;'>Service Type: </label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{roster.ServiceType}</span>
                                </div>
                                <div style='margin-bottom:1rem;'>
                                    <label for='' style='font-weight: bold; color: #1a9dff; width: 8rem; display: inline-block;'>Start & End Period: </label><span style='color: #615d5d; font: small/ 1.5 Arial,Helvetica,sans-serif; font-weight: bold;'>{roster.StartTime} - {endTime}</span>
                                </div>
                            </div>
                       </div>")
            };

            try
            {
                using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                {
                    //emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    //await emailClient.ConnectAsync(
                    //    _emailConfiguration.SmtpServer,
                    //    _emailConfiguration.SmtpPort,
                    //    true).ConfigureAwait(false);
                    //emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.ConnectAsync(
                        _emailConfiguration.SmtpServer,
                        _emailConfiguration.SmtpPort,
                        SecureSocketOptions.StartTls);


                    await emailClient.AuthenticateAsync(
                        _emailConfiguration.SmtpUsername,
                        _emailConfiguration.SmtpPassword)
                        .ConfigureAwait(false);

                    await emailClient.SendAsync(message);
                    await emailClient.DisconnectAsync(true);

                    return new EmailOutput()
                    {
                        ReplyTo = message.ReplyTo.ToString(),
                        CC = message.Cc.ToString(),
                        To = message.To.ToString(),
                        From = message.From.ToString(),
                        Status = new IsSuccessEmail()
                        {
                            IsSuccess = true,
                            Log = ""
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return new EmailOutput()
                {
                    ReplyTo = message.ReplyTo.ToString(),
                    CC = message.Cc.ToString(),
                    To = message.To.ToString(),
                    From = message.From.ToString(),
                    Status = new IsSuccessEmail()
                    {
                        IsSuccess = false,
                        Log = ex.ToString()
                    }
                };
            }
        }

        private static string GetOrdinalSuffix(int num)
        {
            string number = num.ToString();
            if (number.EndsWith("11")) return "th";
            if (number.EndsWith("12")) return "th";
            if (number.EndsWith("13")) return "th";
            if (number.EndsWith("1")) return "st";
            if (number.EndsWith("2")) return "nd";
            if (number.EndsWith("3")) return "rd";
            return "th";
        }
    }
}
