import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';

import {
    HomeClient,
    NotesClient,
    HistoryClient,
    BookingClient,
    DocumentClient,
    PackageClient,
    PreferencesClient,
    ShiftClient,
    CalendarClient,
    ProfileClient
  } from '@client/index'


  import {
    ClientGuard
  } from '@services/index'
const routes: Routes = [
    {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'notes',
        component: NotesClient,
        canActivate: [ClientGuard],
      },
      {
        path: 'history',
        component: HistoryClient,
        canActivate: [ClientGuard],
      },
      {
        path: 'booking',
        component: BookingClient,
        // canActivate: [RouteGuard],
      },
      {
        path: 'document',
        component: DocumentClient,
        canActivate: [ClientGuard],
      },
      {
        path: 'package',
        component: PackageClient,
        // canActivate: [RouteGuard],
      },
      {
        path: 'preferences',
        component: PreferencesClient,
        // canActivate: [RouteGuard],
      },
      {
        path: 'manage-services',
        component: ShiftClient,
        // canActivate: [RouteGuard],
      },
      {
        path: 'profile',
        component: ProfileClient,
        // canActivate: [RouteGuard],
      },
      {
        path: 'calendar',
        component: CalendarClient,
        // canActivate: [RouteGuard],
      },
      {
        path: 'members',
        // component: MembersComponent
      },
      {
        path: 'unauthorized',
        // component: UnauthorizedComponent
      }
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule
    
  ],
  declarations: [
   
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class ClientModule {}

