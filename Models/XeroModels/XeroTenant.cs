using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Adamas.Models.XeroModels {
    public class XeroTenant
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}