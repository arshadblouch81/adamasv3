using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class FileUploadClass {
        public List<IFormFile> Files { get; set; }
        public string UniqueID { get; set; }
        public string Role { get; set; }
    }

}