﻿using System.IO;
using System;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Threading;
using Adamas.DAL;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Principal;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using AdamasV3.DAL;

namespace Adamas.ServiceWorker
{

    public class TokenVerificationService : BackgroundService
    {
        private readonly ILogger _logger;
        private Timer timer;
        private Timer tokenTimer;
        private string _hostPath;
        private ICacheService cache;
        private readonly IServiceProvider _serviceProvider;
        public IConfiguration Configuration { get; }
        public TokenVerificationService(
            ILoggerFactory loggerFactory,
            ICacheService _cache,
            IServiceProvider serviceProvider,
            IConfiguration configuration)
        {
            _logger = loggerFactory.CreateLogger<TokenVerificationService>();
            cache = _cache;
            Configuration = configuration;
            _serviceProvider = serviceProvider;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await RemoveExpiredTokens(stoppingToken);
            }
        }

        private async Task RemoveExpiredTokens(CancellationToken stoppingToken)
        {
            //defaults to 60 minutes
            var minuteInterval = Configuration.GetSection("BackgroundServiceTimeInterval").Value == null 
                                        ? 55 
                                        : Configuration.GetSection("BackgroundServiceTimeInterval").Get<int>();

            // This CreateScope is required to call scoped services inside a backgroundservice
            using (var scope = _serviceProvider.CreateScope())
            {
                var scopedService = scope.ServiceProvider.GetRequiredService<ITokenService>();

                //60 as minutes value
                await scopedService.BackgroundServiceProcess();

                await Task.Delay(TimeSpan.FromSeconds(minuteInterval), stoppingToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stop EmailNotification1HourService");
            return base.StopAsync(cancellationToken);
        }



    }
}