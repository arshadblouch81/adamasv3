import { Component, OnInit, OnDestroy, Input,Output,EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef,AfterViewInit } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService ,PrintService} from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import format from 'date-fns/format';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Filters } from '@modules/modules';
import { values } from 'lodash';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularEditorConfig } from '@kolkov/angular-editor';

import { saveAs } from 'file-saver';

@Component({
    styles: [`
        nz-table{
            margin-top:20px;
        }
         nz-select{
            width:100%;
        }
        label.chk{
            position: absolute;
            top: 1.5rem;
        }
        .overflow-list{
            overflow: auto;
            height: 8rem;
            border: 1px solid #e3e3e3;
        }
        ul{
            list-style:none;
        }
        li{
            margin:5px 0;
        }
        .chkboxes{
            padding:4px;
        }
        button{
            margin-right:1rem;
        }

.header{
    display: flex; 
    flex-direction: row; 
    padding-top: 5px; 
    padding-left:10px;
    background-color: transparent; 
    color: black;     
    border : 1px solid #e5e5e5;
    border-radius : 5px;
}

td {
    padding: 5px;
    font-size: 0.75rem;
    font-family: 'Segoe UI';
    border: 0.5px solid #d0d4d573; 
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    
}
 th{
    padding: 5px;
    border: 1px solid #9a9d9f73; 
    height: 30px;
    background-color: #e7e9ea73;
}
tr{
    border: 1px solid #bcbdbe73; 
    height: 30px;
}

tr:nth-child(odd) {
    background-color:#fff;
    color: black; 
}
 tr:nth-child(even) {
    background-color: #e5f1f8;
    color: black;  
    
 }
 .spinner{
    margin:1rem auto;
    width:1px;
}  

    `],
    templateUrl: './gnotes.html',
    selector:'gnotes',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class GNotes implements OnInit, OnDestroy, AfterViewInit {
    private unsubscribe: Subject<void> = new Subject();
  //  user:any;
    @Input() user:any;    
    @Input() loadNote: Subject<any>;
    @Input() viewOnly:boolean=false;
    inputForm: FormGroup;
    caseFormGroup: FormGroup;
    tableData: Array<any>;
    rptData: Array<any>;

    checked: boolean = false;
    isDisabled: boolean = false;
    loading: boolean = false;
    printLoading: boolean = false;

    modalOpen: boolean = false;
    addOrEdit: number;
    dateFormat: string = 'dd/MM/yyyy';
    printLoad: boolean = false;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    tocken: any;
    ActiveRowData:any;
    activeRowIndex:number;

    public editorConfig:AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '20rem',
        minHeight: '5rem',
        translate: 'no',
        customClasses: []
    };

    filters: Filters = {
        acceptedQuotes: false,
        allDates: false,
        archiveDocs: true,
        display: 20,
        type:'OPNOTE'
    };


    alist: Array<any> = [];
    blist: Array<any> = [];
    clist: Array<any> = [];
    dlist: Array<any> = [];
    mlist: Array<any> = [];

    recipientStrArr: Array<any> = [];

    private default = {
        notes: '',
        publishToApp: false,
        restrictions: '',
        restrictionsStr: 'public',
        alarmDate: null,
        whocode: '',
        program: '*VARIOUS',
        discipline: '*VARIOUS',
        careDomain: '*VARIOUS',
        category: null,
        recordNumber: null
    }

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private printS: PrintService,
        private listS:ListService
    ) {
        

        // this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     if (this.globalS.isCurrentRoute(this.router, 'opnote')) {
        //         this.user = data;
        //         this.search(data);
        //     }
        // });
    }

    ngOnInit(): void {
     
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    //     this.user = this.sharedS.getPicked();
      
    //    if(this.user){
    //        this.search(this.user);
    //        this.buildForm();           
    //    }             
        
            // user s coming from component call
             
        this.search(this.user);
        this.buildForm();        
        this.loadNote.subscribe(d=>{
            this.search(d);
        })
        
    }

    ngAfterViewInit(){
      
    }
    print(){

    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
onItemSelect(data:any, i:number){
    this.activeRowIndex = i;
    this.ActiveRowData = data;
}
    search(user: any = this.user) {
        if (user==null) return;
        this.filters.type=this.user.noteType;
        this.getNotes(this.user);
        this.getSelect();
    }

    filterChange(data: any){
        this.search(this.user);
    }
    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }
    
    getNotes(user:any) {
        this.loading = true;
        
        this.clientS.getgnoteswithfilters(user.id, this.filters).subscribe(data => {
            let list: Array<any> = data.list || [];
            
            if (list.length > 0) {
                list.forEach(x => {
                    if (!this.globalS.IsRTF2TextRequired(x.detailOriginal)) {
                        x.detail = x.detailOriginal
                    }
                   // x.detail = this.convertToPlain(x.detailOriginal)
                });
                this.tableData = list;
            } else {
                this.tableData = list;
            }
            
            this.loading = false;
            this.cd.markForCheck();
            this.cd.detectChanges();
        })

        // this.clientS.getopnotes(user.id).subscribe(data => {
        //     let list: Array<any> = data.list || [];
            
        //     if (list.length > 0) {
        //         list.forEach(x => {
        //             if (!this.globalS.IsRTF2TextRequired(x.detailOriginal)) {
        //                 x.detail = x.detailOriginal
        //             }
        //         });
        //         this.tableData = list;
        //     }
            
        //     this.loading = false;
        //     this.cd.markForCheck();
        // });

    }

    patchData(data: any) {
        this.inputForm.patchValue({
            autoLogout: data.autoLogout,
            emailMessage: data.emailMessage,
            excludeShiftAlerts: data.excludeShiftAlerts,
            inAppMessage: data.inAppMessage,
            logDisplay: data.logDisplay,
            pin: data.pin,
            publishToApp: data.publishToApp,
            shiftChange: data.shiftChange,
            smsMessage: data.smsMessage
        });
    }


    buildForm() {

        this.inputForm = this.formBuilder.group({
            autoLogout: [''],
            emailMessage: false,
            excludeShiftAlerts: false,
            inAppMessage: false,
            logDisplay: false,
            pin: [''],
            publishToApp: false,
            shiftChange: false,
            smsMessage: false
        });

        this.caseFormGroup = this.formBuilder.group({
            notes: '',
            publishToApp: false,
            restrictions: '',
            restrictionsStr: 'public',
            alarmDate: null,
            whocode: '',
            program: '*VARIOUS',
            discipline: '*VARIOUS',
            careDomain: '*VARIOUS',
            category: ['', [Validators.required]],
            recordNumber: null,
            type:"CASENOTE"
        });

        this.caseFormGroup.get('restrictionsStr').valueChanges.subscribe(data => {
            if (data == 'restrict') {
                this.getSelect();
            } 
        });
    }

    getSelect() {
        this.timeS.getmanagerop().subscribe(data => {
            this.mlist = data;
            this.cd.markForCheck();
        });

        this.timeS.getdisciplineop().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            data.push('*VARIOUS');
            this.blist = data;
        });
        this.timeS.getcaredomainop().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            data.push('*VARIOUS');
            this.clist = data;
        });
        this.timeS.getprogramop(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            data.push('*VARIOUS');
            this.alist = data;
        });

        this.timeS.getcategoryop().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.dlist = data;
        })
    }

    onKeyPress(data: KeyboardEvent) {
        return this.globalS.acceptOnlyNumeric(data);
    }

    trackByFn(index, item) {
        return item.id;
    }



    save() {        
        if (!this.globalS.IsFormValid(this.caseFormGroup))
            return;
        
        const { alarmDate, restrictionsStr, whocode, restrictions,publishToApp } = this.caseFormGroup.value;
        const cleanDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(alarmDate);

        let privateFlag = restrictionsStr == 'workgroup' ? true : false;
        let restricts = restrictionsStr != 'restrict';

        this.caseFormGroup.controls["restrictionsStr"].setValue(privateFlag);

        this.caseFormGroup.controls["alarmDate"].setValue(cleanDate);
        this.caseFormGroup.controls["whocode"].setValue(this.user.code);
        this.caseFormGroup.controls["restrictions"].setValue(restricts ? '' : this.listStringify());
        this.caseFormGroup.controls["type"].setValue(this.user.noteType);
       // this.caseFormGroup.controls["notes"].setValue(this.rtfToHtml(this.caseFormGroup.value.notes));
    
        console.log(this.caseFormGroup.value);

        this.loading = true;
        if (this.addOrEdit == 1) {    
            if (this.user.noteType=='OPNOTE')       
                this.clientS.postopnotes(this.caseFormGroup.value, this.user.id)
                    .subscribe(data => {
                        this.globalS.sToast('Success', 'Note Added');
                        this.handleCancel();
                        this.getNotes(this.user);
                    });
            else
                this.clientS.postcasenotes(this.caseFormGroup.value, this.user.id)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Note Added');
                    this.handleCancel();
                    this.getNotes(this.user);
                });
        }
        if (this.addOrEdit == 2) {

            if (this.user.noteType=='OPNOTE')      

                this.clientS.updateopnotes(this.caseFormGroup.value, this.caseFormGroup.value.recordNumber)
                    .subscribe(data => {
                        this.globalS.sToast('Success', 'Note updated');
                        this.handleCancel();
                        this.getNotes(this.user);                    
                    });
            else
                this.clientS.updatenotes(this.caseFormGroup.value, this.caseFormGroup.value.recordNumber)
                    .subscribe(data => {
                        this.globalS.sToast('Success', 'Note updated');
                        this.handleCancel();
                        this.getNotes(this.user);                    
                    });
        }
    }

    listStringify(): string {
        let tempStr = '';
        this.recipientStrArr.forEach((data, index, array) => {
            array.length - 1 != index ?
                tempStr += data.trim() + '|' :
                tempStr += data.trim();
        });
        return tempStr;
    }

    canDeactivate() {
        if (this.inputForm && this.inputForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Save changes before exiting?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {

                }
            });
        }
        return true;
    }


    
    
      convertToPlain(rtf) {
        rtf = rtf.replace(/\\par[d]?/g, "");
        rtf = rtf.replace(/\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?/g, "")
        return rtf.replace(/\\'[0-9a-zA-Z]{2}/g, "").trim();
    }

    convertHtmlToRtf(html: string) {
        const tempElement = document.createElement('div');
        tempElement.innerHTML = html;
        const rtfContent = tempElement.innerText;
        const blob = new Blob([rtfContent], { type: 'text/rtf' });
        return blob;
    }
    
    htmlToRtf(html: string): string {
        // Replace special characters in HTML
        let rtfText = html.replace(/\\/g, '\\\\').replace(/{/g, '\\{').replace(/}/g, '\\}');
        // Remove HTML tags
        rtfText = rtfText.replace(/<[^>]*>/g, '');
        // Escape special characters in RTF
        rtfText = rtfText.replace(/\\/g, '\\\\').replace(/{/g, '\\{').replace(/}/g, '\\}');
        // RTF header
        let rtf = '{\\rtf1\\ansi\\ansicpg1252\\deff0\\nouicompat\\deflang1033\n';
        rtf += '{\\fonttbl\\f0\\fnil\\fcharset0 Calibri;}\n';
        rtf += '\\viewkind4\\uc1\\pard\\f0\\fs20 ';
        // RTF content
        rtf += rtfText + '\\par\n';
        // RTF footer
        rtf += '}';
        return rtf;
    }
    rtfToHtml(rtf: string): string {
        // Remove RTF control words and convert escaped characters
        let htmlText = rtf
            .replace(/\\par[d]?/g, '<br/>') // Replace \par or \pard with <br/>
            .replace(/\\[bB]/g, '<strong>') // Replace \b or \B with <strong>
            .replace(/\\[iI]/g, '<em>') // Replace \i or \I with <em>
            .replace(/\\[uU]/g, '<u>') // Replace \u or \U with <u>
            .replace(/\\[0-9]+/g, '') // Remove other RTF control words (e.g., font, color)
            .replace(/\\'([0-9a-zA-Z]{2})/g, (match, group) => String.fromCharCode(parseInt(group, 16))) // Replace escaped characters
        // Remove leading and trailing whitespace
        htmlText = htmlText.trim();
        // Convert remaining RTF encoded characters
        htmlText = htmlText.replace(/\\'([0-9a-zA-Z]{2})/g, (match, group) => String.fromCharCode(parseInt(group, 16)));
        return htmlText;
    }
      
    showAddModal() {
        this.addOrEdit = 1;
        this.modalOpen = true;
    }

    showEditModal(index: number) {
        this.addOrEdit = 2;
        const { personID, recordNumber, privateFlag, whoCode, detailDate, craetor, detail, detailOriginal, extraDetail2, restrictions, alarmDate, program,discipline, careDomain, publishToApp } = this.tableData[index];



        this.caseFormGroup.patchValue({
            notes: detail,
            publishToApp: publishToApp==null? 0 : publishToApp,
            restrictions: '',
            restrictionsStr: this.determineRadioButtonValue(privateFlag, restrictions),
            alarmDate: alarmDate==null ? detailDate :alarmDate,
            program: program,
            discipline: discipline,
            careDomain: careDomain,
            category: extraDetail2,
            recordNumber: recordNumber
        });
        this.modalOpen = true;
    }

    determineRadioButtonValue(privateFlag: Boolean, restrictions: string): string {
        if (!privateFlag && this.globalS.isEmpty(restrictions)) {
            return 'public';
        }

        if (!privateFlag && !this.globalS.isEmpty(restrictions)) {
            return 'restrict'
        }

        return 'workgroup';
    }

    delete(index: any) {
        const { recordNumber } = this.tableData[index];

        this.clientS.deletesvcnotes(recordNumber).subscribe(data => {
            this.globalS.sToast('Success', 'Note deleted');
            this.handleCancel();           
            this.search(this.user);
        });
    }

    log(event: any) {
        this.recipientStrArr = event;
    }

    handleCancel() {
        this.modalOpen = false;
        this.loading = false;
        this.caseFormGroup.reset(this.default);
    }

    handleOkTop() {
        //this.generatePdf();
        this.tryDoctype = ""
        this.pdfTitle = ""
    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
    }
    generatePdf(){
        var SQL,Title;
        
        switch(this.filters.type){
            case'CASENOTE':
                SQL = "Select top "+this.filters.display +" RecordNumber, DetailDate, dbo.rtf2text(Detail) as Detail , AlarmDate, Creator  FROM History WHERE PersonID = '"+this.user.id+"' AND ExtraDetail1 = 'CASENOTE'  AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr')) AND DeletedRecord <> 1  ORDER BY DetailDate DESC, RecordNumber DESC  "
                Title = "Case/Progress Notes for " +this.user.code
            break;
            case'OPNOTE':
                SQL = "Select top "+this.filters.display +" RecordNumber, DetailDate, dbo.rtf2text(Detail) as Detail , AlarmDate, Creator  FROM History WHERE PersonID = '"+this.user.id+"' AND ExtraDetail1 = 'OPNOTE'  AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr')) AND DeletedRecord <> 1  ORDER BY DetailDate DESC, RecordNumber DESC  "
                Title = "OP Notes for " +this.user.code
            break;
            case'SVCNOTE':
                SQL = "Select top "+this.filters.display +" RecordNumber,Creator,DetailDate, dbo.rtf2text(Detail) as Detail,AlarmDate from History where ExtraDetail1 = 'SVCNOTE' AND PersonID = '"+this.user.id+"'  AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr')) AND DeletedRecord <> 1 ORDER BY DetailDate DESC, RecordNumber DESC"
                Title = "Service Notes Notes for " +this.user.code
            break;
            default:

            break;
        }
        //console.log(SQL)
        this.printLoading = true;
        this.drawerVisible = true;
        const temp =  forkJoin([
            this.listS.getlist(SQL), 
            ]);    
            temp.subscribe(x => {                         
            this.rptData =  x[0]; 
            //console.log(this.rptData)      
            
                const data = {
                    "template": { "_id": "6BoMc2ovxVVPExC6" },
                    "options": {
                        "reports": { "save": false },                        
                        "sql": this.rptData,                        
                        "userid": this.tocken.user,
                        "txtTitle":  Title,                      
                    }
                }              
                this.loading = true;
               
                        
                this.drawerVisible = true;
                this.printS.printControl(data).subscribe((blob: any) => {
                            this.pdfTitle = Title+".pdf"
                            this.drawerVisible = true;                   
                            let _blob: Blob = blob;
                            let fileURL = URL.createObjectURL(_blob);
                            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                            this.printLoading = false;
                            this.cd.detectChanges();
                        }, err => {
                            console.log(err);
                            this.printLoading = false;
                            this.ModalS.error({
                                nzTitle: 'TRACCS',
                                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                nzOnOk: () => {
                                    this.drawerVisible = false;
                                },
                            });
                });                            
                return;        
            });         
    }
}