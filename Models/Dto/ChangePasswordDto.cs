﻿namespace Adamas.Models.Dto
{
    public class ChangePasswordDto
    {
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public string NewAccountNo { get; set; }
    }
}
