using System;

namespace Adamas.Models.Modules{
    public class CopyMtaDocument {
       public string PersonId { get; set; }
       public string Extension { get; set; }
       public string Filename { get; set; }
       public string DocPath { get; set; }
    }
}