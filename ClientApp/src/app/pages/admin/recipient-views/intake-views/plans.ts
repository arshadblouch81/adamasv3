import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, UploadService, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Filters } from '@modules/modules';
import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
    selector: '',
    templateUrl: './plans.html',
    styles: [`
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakePlans implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    tableData: Array<any> = [];
    alist: Array<any> = [];
    activeRowData:any;
    selectedRowIndex:number;
    selectedServiceAgreement:any;
    isLoading:boolean;
    newQuoteModal:boolean=false;
    option: string = 'add';
    record:any;
    DocusingModel:boolean=false;
    showUnset:boolean=false;
    doclist: Array<any> = [];
    selectedRow2:number;
    activeRowDocData:any;
    showFilters:boolean=true;
    filterClicked:boolean;
    LIMIT_TABS : Array<any> = ['CARE DOMAIN','CREATOR','DISCIPLINE','PROGRAM'];
    filters:any;
    displayLast: number = 20;
    archivedDocs: boolean = false;
    acceptedQuotes: boolean = false;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private message: NzMessageService,
        private uploadS: UploadService,
        private cd: ChangeDetectorRef,
        private printS:PrintService,
        private sanitizer:DomSanitizer
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'plans')) {
                this.user = data;
                this.search(data);
            }
        });

       
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }

    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm( data: any): void {
      
       
          this.delete(data);
        

    }
  
    search(user: any = this.user) {
        this.cd.reattach();

        this.loading = true;
        let input = {
            quote: {
                PersonID: user.id,                
                IncludeArchived: this.archivedDocs,
                IncludeAccepted: this.acceptedQuotes
            },
            filters: this.filters
        }
         this.timeS.getplans(input).subscribe(plans => {
            this.tableData = plans;
            this.loading = false;
            this.cd.markForCheck();
            if (!this.filterClicked){
                this.showFilters=false;
            }
        });

        this.listS.gettemplatelistAll().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.alist = data;
           
        });

      /*  let sql=`SELECT * FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY Title`;
        this.listS.getlist(sql).subscribe(data => {
            this.alist = data;
            //this.cd.markForCheck();
        });*/

    }

    buildForm() {

    }
    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }
      showAllDocuments() {

    }

    showEditModal(index: number) {

        this.downloadFile(index)
    }
    downloadFile(index: any){
        var doc = this.tableData[index];
        var notifId = this.globalS.loadingMessage('Downloading Document');
        
        this.uploadS.download({
            PersonID: this.user.id,
            Extension: doc.documentType,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {

            let data = window.URL.createObjectURL(blob);      
            let link = document.createElement('a');
            link.href = data;
            link.download = doc.filename;
            link.click();

            setTimeout(() =>
            {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                this.message.remove(notifId);

                this.globalS.sToast('Success','Download Complete');
                this.globalS.clearMessage();
            }, 100);

        }, err =>{
            this.message.remove(notifId);
            this.globalS.eToast('Error', "Document can't be found");
            this.globalS.clearMessage();
        })
    }

    delete(data: any) {
        this.timeS.deleteintakerservice(data.recordNumber).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                            if(data){
                                this.search()
                                this.loading = false
                                this.globalS.sToast('Success','Service Deleted')
                            }
             })
    }

    handleCancel() {
        this.modalOpen = false;   
    }

    selectService(data:any, i:number){
        this.selectedServiceAgreement=data;
        this.selectedRowIndex=i;
      }

    showAddModal() {
        this.addOREdit = 1;
        this.modalOpen = true;
    }
    AllocatePlan(data:any, i:number){
        this.modalOpen = false;
      this.newQuoteModal=!this.newQuoteModal;
    this.option='add';
    
    this.record  =  {    
                  recordNumber: data.recordNumber,
                  program: '',
                  template: data.title,
                  docNo: data.docNo,
                  templateFile : data.template,
                  maingroup : data.mainGroup,
                  docID : data.docID,
                  tabIndex:0,
                  plan : 'CAREPLAN',
                  }   

    //   recordNumber: data.recordNumber,
    //   program: data.program,
    //   no: data.docNo,
       // this.router.navigate(['admin/recipient/quotes'], { state: { data: data } });
    }
    refreshQuote(data: any){
        if(data){
            console.log('refresh')
            this.search();
        }
    }

    signingStatus(){
        this.DocusingModel=true;
        this.timeS.getdocusingStatus(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.doclist = data;
            this.cd.detectChanges();
        });
    }
    selectDcocusign(data,i){
        this.selectedRow2=i;
        this.activeRowDocData=data;
    }
    print(){
        
    }
    generatePdf(){
      
        //this.drawerVisible = true;
        //this.loading = true;
        
        //var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY RecordNumber) AS Field1 , Name AS Field2, Notes AS Field3 from HumanResources  WHERE PersonID = '"+this.user.id+"' AND [Type] = 'RECIPBRANCHES' ORDER BY Name";
        //console.log(this.tableData)
        var Title = " Plans "
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  Title+" for "+this.user.code,                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = Title+" for "+this.user.code;
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.modalService.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
    
  ViewFilters(){
    this.showFilters=!this.showFilters;
    this.filterClicked=true;
   // this.search();
  }
  filterChange(data: any){
       
    this.filters=data;
    this.displayLast = data.display;
    this.archivedDocs = data.archiveDocs;
    this.acceptedQuotes = data.acceptedQuotes;
    this.search(this.user);
}
}