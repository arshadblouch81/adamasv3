﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MimeKit;
using MimeKit.Text;
using Newtonsoft.Json.Linq;

using Adamas.Models.Modules;

using System.Text;

namespace Adamas.DAL
{
    public interface IEmailService
    {
        Task<EmailOutput> SendBookingNotification(List<MailboxAddress> addresses, AddBooking booking, double billQty);
        Task<EmailOutput> SendBookingCancelNotification(List<MailboxAddress> addresses, CancelBooking booking, Adamas.Models.Tables.Roster roster);
    }
}
