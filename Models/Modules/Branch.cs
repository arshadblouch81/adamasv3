namespace Adamas.Models.Modules{
    public class Branch {
        public int? RecordNumber { get; set; } = 0;
        public string domain{get;set;}
         public string name { get; set; } 
        public string glRevene{ get; set; } 
        public string glCost{ get; set; } 
        public string end_date{ get;set; }
        public string centerName{ get; set; } 
       public string  addrLine1 { get; set; } 
        public string addrLine2{ get; set; } 
        public string Phone{ get; set; } 
        public string startHour{ get; set; } 
        public string finishHour{ get; set; } 
        public string earlyStart{ get; set; } 
        public string lateStart{ get; set; } 
        public string earlyFinish{ get; set; } 
        public string lateFinish{ get; set; } 
        public string overstay{ get; set; } 
        public string understay{ get; set; } 
        public string t2earlyStart{ get; set; } 
        public string t2lateStart{ get; set; } 
        public string t2earlyFinish{ get; set; } 
        public string t2lateFinish{ get; set; } 
        public string t2overstay{ get; set; } 
        public string t2understay{ get; set; } 
    }
}