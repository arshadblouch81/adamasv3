import {
  Component,
  OnInit,
  forwardRef,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  NgZone,
} from "@angular/core";
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
  NG_VALUE_ACCESSOR,
  ControlValueAccessor,
  FormArray,
} from "@angular/forms";
import { TitleCasePipe } from "@angular/common";
import { Router, NavigationEnd } from "@angular/router";

import {
  GlobalService,
  ListService,
  TimeSheetService,
  ShareService,
  expectedOutcome,
  qoutePlantype,
  leaveTypes,
  PrintService,
  Quotesdropdowns
} from "@services/index";
import * as _ from "lodash";
import { forkJoin, Observable, EMPTY, Subject, combineLatest } from "rxjs";
import parseISO from "date-fns/parseISO";
import { RemoveFirstLast } from "../../pipes/pipes";
import {
  mergeMap,
  debounceTime,
  distinctUntilChanged,
  first,
  take,
  takeUntil,
  switchMap,
  concatMap,
} from "rxjs/operators";
import * as moment from "moment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";
import { dateFormat, fundingDropDowns, period } from "@services/global.service";

import {
  Filters,
  QuoteLineDTO,
  QuoteHeaderDTO,
  AcceptCharges,
} from "@modules/modules";
import { billunit, periodQuote, basePeriod,Pensions } from "@services/global.service";
import { MedicalProceduresComponent } from "@admin/recipient-views/medical-procedures.component";
// import { Console } from 'node:console';

import addYears from "date-fns/addYears";
import startOfMonth from "date-fns/startOfMonth";
import endOfMonth from "date-fns/endOfMonth";

import { NzModalRef, NzModalService } from "ng-zorro-antd/modal";
import { PrintPdfComponent } from "@components/print-pdf/print-pdf.component";
import { filter, toLength, xor } from "lodash";
import { format, parseJSON } from "date-fns";

import { DecimalPipe } from "@angular/common";
// import { Console } from 'node:console';
import { toNumber, values } from 'lodash';
import { createDebugEle } from "ng-zorro-antd";
import * as groupArray from 'group-array';

const noop = () => { };

interface Goals {
  RecordNumber?: number;
  PersonID: string;
  Level: string,
  Notes: string;
  Goal: string;
  Percent: string,
  Anticipated?: Date;
  LastReviewed?: Date;
  DateArchived?: Date;
  Strategies?: Array<PlanStrategy>


}

interface PlanStrategy {
  RecordNumber?: number;
  PersonID: string;
  Detail: string;
  Outcome: string;
  StrategyId: string;
  dateArchived?: Date;
}

const goalsDefault: any = {
  recordnumber: null,
  goal: null,
  goalText: null,
  PersonID: null,
  completed: null,
  percent: null,
  level: "",
  notes: "",
  dateArchived: null,
  lastReviewed: null,
  anticipated: null,
  chkStrategies: false,
  chkDocuments: false


};

enum GoalStrategyJob {
  ADD,
  UPDATE,
}
enum GoalCarePlanJob {
  ADD,
  UPDATE,
}



@Component({
  selector: "app-add-quote",
  templateUrl: "./add-quote.component.html",
  styleUrls: ["./add-quote.component.css"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => AddQuoteComponent),
    },
  ],
})
export class AddQuoteComponent implements OnInit {
  formatterDollar = (value: number) =>
    `$ ${this._decimalPipe.transform(value, "1.2-2")}`;

  @Input() open: boolean = false;
  @Input() user: any;
  @Input() option: any;
  @Input() record: any;

  @Output() refresh = new EventEmitter<any>();

  document: any;
  ViewNeedsRisk: Subject<any> = new Subject();
  showNeedRisks: boolean = false;
  showDocProperties: boolean = false;
  atRate = "@";
  GOALSTRATEGY_JOB: GoalStrategyJob = GoalStrategyJob.ADD;
  GOALSCAREPLAN_JOB: GoalCarePlanJob = GoalCarePlanJob.ADD;
  selectedprogram: any;
  centreLinkListDropdown: string = "Daily";
  selectGoalModel: boolean;
  selectedGoal: any;
  marginLeftValue: string = '-20rem';
  marginToValue: string = '18px';
  documentId: number;
  disableProgram: boolean = true;
  _tempGovtContribution: any;
  lstInterval = fundingDropDowns.cycle
  incomeTestedFee: number = 0;
  incomeTestedFeeTotal: number = 0;

  annualBasicCareFee: number = 0;
  dailyBasicCareFee: number = 0;
  monthlyBasicCareFee: number = 0;

  annualIncomeTestedFee: number = 0;
  dailyIncomeTestedFee: number = 0;
  monthlyIncomeTestedFee: number = 0;
  selectedGoals: Array<any> = [];
  confirmModal?: NzModalRef;

  disableAddTabs: boolean = false;
  templateLoading: boolean = false;
  serviceLoading: boolean = false;

  private unsubscribe: Subject<void> = new Subject();

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  size: string = "default";
  title: string = "Add New Quote";

  slots: any;

  acceptCharges: AcceptCharges;

  billUnitArr: Array<string> = billunit;
  periodArr: Array<string> = periodQuote;
  basePeriodArr: Array<string> = basePeriod;
  Pensions: Array<string>  = Pensions;

  dateFormat: string = dateFormat;
  clientId: number;
  weekly: string = "Weekly";
  date: Date = new Date();
  nzSize: string = "small";

  tabFindIndex: number = 1;
  chkStrategies: boolean = false;
  chkDocuments: boolean = false;
  inputForm: FormGroup;
  activeForm: FormGroup;
  inActiveForm: FormGroup;
  quoteForm: FormGroup;
  quoteGeneralForm: FormGroup;
  quoteIdsForm: FormGroup;
  quoteListForm: FormGroup;

  tableData: Array<any> = [];
  checked: boolean = false;
  isDisabled: boolean = false;

  displayLast: number = 20;
  archivedDocs: boolean = false;
  acceptedQuotes: boolean = false;

  loading: boolean = false;
  postLoading: boolean = false;
  quotesOpen: boolean = false;
  quoteLineOpen: boolean = false;
  activeOpen: boolean = false;
  inActiveOpen: boolean = false;
  newFileNameOpen: boolean = false;

  goalAndStrategiesmodal: boolean = false;
  isUpdateGoal: boolean = false;
  isUpdateStrategy: boolean = false;
  value: any;
  expandDescription: true;

  quoteTemplateList: Array<string>;
  quoteProgramList: Array<string>;

  IS_CDC: boolean = true;
  IS_HCP: boolean = false;
  programLevel: any;

  newFileName: string;
  SupplementVisibility: any;
  fundingprioritylist: any;
  fundingAriaList: Array<any> = [];
  codes: Array<any>;
  strategies: Array<any>;
  recipientProperties: any;
  cpid: any;
  budgethData:any;
  radioValue: any;
  filters: any;
  disciplineList: any;
  typeList: Array<any> = [];
  careDomainList: any;
  programList: any;
  quotePlanType: string[];
  carePlanID: any;
  goalOfCarelist: any;
  originalgoalOfCarelist: any;
  goalsAndStratergies: any;
  HighLightRow2: number;
  userCopy: any;
  totalPackageValue: number = 28425;
  contribution: number = 0;
  Created: any;
  Modified: any;
  Creator: any;
  temp1: any;
  topup: any;
  basiccarefee: any;
  PercAmt: any;
  AdmPErcAmt: any;
  BudgetNo: any;
  opnDoc:boolean = false;
  quoteLines: Array<any> = [];
  quoteLines_display: Array<any> = [];
  fquotehdr: Array<any> = [];
  mainGroup: Array<any> = [];

  quoteLinesTemp: Array<any> = [];

  loggedInUser: any;
  admincharges: number = 0;

  tempIds: any;

  specindex: number;
  dochdr: any;
 
  selectedProgram: any;

  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean = false;
  goalsAndStratergiesForm: FormGroup;
  reportDataParent: any;
  stratergiesList: Array<any> = [];
  pdfdata: any;
  stratergiesForm: FormGroup;
  strategiesmodal: boolean;
  selectedStrategyIndex:number;
  expecteOutcome: string[];
  plangoalachivementlis: any;
  personIdForStrategy: any;
  supplements: FormGroup;
  viewData: Subject<any> = new Subject();
  price: number;
  quantity: number;
  length: number;
  fdata: any;
  pUser:any;

  enableSaveQuote: boolean = false;
  quoteDetails: any;
  activeRowData: any;
  activeRowIndex: number;
  @Input() ViewBudget: Subject<any> = new Subject();
  recipdata:any;
  quote_label:string;

  constructor(
    private timeS: TimeSheetService,
    private sharedS: ShareService,
    private listS: ListService,
    private printS: PrintService,
    private router: Router,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private modalService: NzModalService,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    private modal: NzModalService,
    private zone: NgZone,
    private _decimalPipe: DecimalPipe,
    private cd: ChangeDetectorRef,
   
  ) { }

  ngOnInit(): void {
    //if (this.user==null) return;
   // console.log(this.user);
    this.buildForm();
    this.quoteGeneralForm.controls.discipline.setValue("VARIOUS");
    this.quoteGeneralForm.controls.careDomain.setValue("VARIOUS");

    

    this.quote_label = 'New Individual Budget';

 

    this.loggedInUser = this.globalS.decode();
    this.listS.getgoalofcare().subscribe((data) => {

      this.goalOfCarelist = data.map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });
      this.originalgoalOfCarelist = this.goalOfCarelist
    });

    this.listDropDowns();

  }

  ngOnChanges(changes: SimpleChanges) {


    for (let property in changes) {
      if (
        property == "open" &&
        !changes[property].firstChange &&
        changes[property].currentValue != null
      ) {

        this.quoteForm.reset();
        this.quoteGeneralForm.reset();
        this.supplements.reset();
        this.quoteDetails = null;
        this.quoteLines = [];
        this.quoteLines_display =[];
        this.goalsAndStratergies = [];
        this.BudgetNo='';
        let quote = {
          program: this.record.program.program,
          user: this.loggedInUser.user,
        };

     
      
        // this.listS.checkpostquote(quote).subscribe((data) => {
        //     this.tableDocumentId = data.docID;
        //     this.cpid = data.docID;
        // });
        this.selectedprogram = this.record.program.program;
        this.tableDocumentId = this.record.docID;
        this.cpid = this.record.docID;

       

        if (this.record.template != null) {
          if (this.record.tabIndex != null) {
            this.tabFindIndex = this.record.tabIndex;
            this.disableProgram = false;
          }
        
          let today = new Date();
          this.quoteForm.patchValue({
            template: this.record.template,
            program: this.record.program.program,
            programid: this.record.program.id,
            templateFile: this.record.templateFile,
            id: this.record.carePlanId,
            recordNumber: null,
            type :'QUOTE/BUDGET',
            period: [ this.globalS.getStartOfMonth(today), this.globalS.getEndOfMonth(today), 1],
            basePeriod:'ANNUALLY'
            //initialBudget: this.budgethData.gtotal,

          });
          this.recipdata =this.sharedS.getRecipientData();
          if (this.recipdata != null) {
            let hcp='';
            hcp=this.record.isCDC ? 'HCP' : '';

              if (this.option == "update") {                
                  this.quote_label = 'Edit Individual Budget For '  + this.recipdata.firstName + ' '  + this.recipdata.surnameOrg + ' ' + this.record.level + ' ' + hcp;
              }else { 
                this.quote_label = 'New Individual Budget For ' + this.recipdata.firstName + ' '  + this.recipdata.surnameOrg + ' ' + this.record.level + ' ' + hcp;
              }
          }


          this.quoteGeneralForm.patchValue({
            recordNumber: this.record.recordNumber,
            program: this.record.program.program ?? this.record.program ,
            id: this.record.carePlanId,
            name: this.record.title ??  this.record.template,
            careDomain: this.record.careDomain,
            discipline: this.record.discipline,
            classification : this.record.classification,
            starDate: this.record.startDate,
            signOfDate: this.record.endDate,
            reviewDate: this.record.alarmDate,
            modified: this.record.modified,
            planType: this.record.maingroup,
            rememberText: this.record.alarmText,
            publishToApp: this.record.publishToApp,
          });
          this.Modified = this.record.modified;
          this.Created = this.record.created;

          this.cd.detectChanges();

          //
        }

        this.search(this.user).then((data) => {

          if (data == null) {
            this.showQuoteModal();
            return;
          }
          this.showQuoteModal();
          this.quoteForm.patchValue({
            program: data.program,
            template: this.record.template,
            templateFile: this.record.templateFile,
            id: data.id,
            no: data.recordNumber,
            type: this.record.maingroup,

            basePeriod: data.quoteBase,
            initialBudget: null,
            daysCalc: 365,
            govtContrib: null,

            programId: data.programId,
            period: [this.record.startDate, this.record.endDate, 1],
            charges: false,
            basis: data.basis,
            packageSupplements: data.packageSupplements??'00000000000000000000000000',

            dailyBasicCareFee: data.dailyBasicCareFee,
            dailyIncomeTestedFee: data.dailyIncomeTestedFee,
            incomeTestedFee: data.incomeTestedFee,
            govtContribution: data.govtContribution,
            govtDistribution: data.govtContribution,
            agreedTopUp: data.dailyAgreedTopUp,
            amount: data.amount,
            balanceAtQuote: data.balanceAtQuote,
            basePension: data.basePension,
            budget: data.budget,
            CLAssessedIncomeTestedFee: data.cLAssessedIncomeTestedFee,
            clientId: data.clientId,
            contribution: data.contribution,
            cpid: data.cpid,
            created: data.created,
            dailyAgreedTopUp: data.dailyAgreedTopUp,
            dailyCDCRate: data.dailyCDCRate,

            docNo: data.docNo,
            feesAccepted: data.feesAccepted,
            goals: data.goals,
            hardshipSupplement: data.hardshipSupplement,
            modified: data.modified,
            newFileName: data.newFileName,

            personId: data.personId,
            quoteBase: data.quoteBase,
            quoteLines: data.quoteLines,
            quoteView: data.quoteView,
            recordNumber: data.recordNumber,
            user: data.user,
           

          });
        
          if (data.packageSupplements!=null)
            this.setPackageSupplements(data.packageSupplements);

          if (data.budget != null) {
          this.totalPackageValue = data.budget??0;
          this.contribution = data.contribution;
          }
          //  this.Modified = data.modified;
          //  this.Created  = data.created;
          //  this.Creator = data.user;

          this.quoteDetails = data;
          this.cpid = data.cpid;
          this.fdata = data.quoteLines;

          //  this.tableDocumentId = data.docID;
          this.annualBasicCareFee = data.annualBasicCareFee ?? 0,
            this.dailyBasicCareFee = data.dailyBasicCareFee,
            this.dailyIncomeTestedFee = data.dailyIncomeTestedFee,
            this.incomeTestedFee = data.incomeTestedFee,

            // this.supplements.patchValue({
            //   dailyBasicCareFee : data.dailyBasicCareFee,
            //   dailyIncomeTestedFee : data.dailyIncomeTestedFee,
            //   basePension : data.basePension
            // });

            this.incomeTestedFee = parseFloat(
              data.dailyIncomeTestedFee.replace(/\$|,/g, "")
            );
          this.calculateIncomeTestedFee();

          if (!this.IS_CDC) {
            this.quoteForm.patchValue({
              initialBudget: data.budget,
              govtContribution: data.govtContribution,
              basis: data.basis
            });
          } else {
            this.quoteForm.patchValue({
              govtContribution: data.govtContribution,
              basis: data.basis
            });
          }

          this.quoteLines =
            data.quoteLines.length > 0
              ? data.quoteLines.map((x) => {
                this.fquotehdr = x;
                this.dochdr = x.docHdrId;

                return {
                  code: x.code,
                  displayText: x.displayText,
                  quantity: x.qty,
                  billUnit: x.billUnit,
                  frequency: x.frequency,
                  price: x.unitBillRate,
                  recordNumber: x.recordNumber,
                  tax: x.tax,
                  lengthInWeeks: x.lengthInWeeks,
                  quoteQty: x.quoteQty,
                  mainGroup: x.mainGroup,
                  amount :   this.totalamount(
                    x.unitBillRate,
                    x.quoteQty,
                    x.tax,
                    x.qty
                    ) 
                };
              })
              : [];

          this.total_base_quote = this.generate_total().toFixed(2);
          this.total_admin = this.generate_total_admin();
          this.total_quote = (this.generate_total() + this.total_admin).toFixed(
            2
          );
          this.BudgetNo = data.docNo;
          this.formatBudgetNo();
          this.generate_lines() ;

          this.globalS.clearMessage();
        });

       

      }
    }
  }
  setBudget(titleData :any, indx:number){
    this.quoteLines_display[indx-1].budget = titleData.reduce((sum, item) => sum + item["amount"], 0);
   
  }
  generate_lines(){

    this.quoteLines_display = this.quoteLines;
    const dragColumns = "mainGroup" ;//this.quoteLines.map(x => "mainGroup");
   
    var convertedObj = groupArray(this.quoteLines_display, dragColumns);

    //console.log(convertedObj)
    var flatten = this.flatten(convertedObj, [], 0);

    if(dragColumns.length == 0){
      this.quoteLines_display = this.quoteLines;
    } else {
        this.quoteLines_display = flatten;
    }

    console.log( this.quoteLines_display)
}
flatten(obj: any, res: Array<any> = [], counter = null){
  for (const key of Object.keys(obj)) {
      const propName = key;
      if(typeof propName == 'string'){                   
          res.push({key: propName, counter: counter, display:true, budget:0});
          counter++;
      }
      if (!Array.isArray(obj[key])) {
          this.flatten(obj[key], res, counter);
          counter--;
      } else {
          res.push(obj[key]);
          counter--;
      }
  }
  return res;
}

isArray(data: any){
  return Array.isArray(data);
}

isSome(data: any){
  if(data){
      return data.some(d => 'key' in d);
  }
  return true;        
}
trackByFn(index, item) {
  return item.id;
}
toggleTable(index: number) {

  this.quoteLines_display[index].display = !this.quoteLines_display[index].display;
 

}
  selectGoal(event: any, value: any, index: number) {
    this.selectedGoal = value;
    this.HighLightRow2 = index

    if (event.ctrlKey || event.shiftKey) {
      if (!this.selectedGoals.includes(value))
        this.selectedGoals.push(value);


    } else {

      this.selectedGoals = [];
      // this.selected = [];

      if (this.selectedGoals.length <= 0) {
        this.selectedGoals.push(value)

      }
    }

  }
  formatBudgetNo() {
    this.BudgetNo = this.padLeft(this.BudgetNo, "0", 6);
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr(size * -1, size);
  }
  documentList: Array<any> = []

  reload(reload: boolean) {
    if (reload) {

      this.timeS.getdocumentsrecipientTop(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.document = data;       
        this.updateCareplanId();   

      });

    } else if (this.chkDocuments) {
      this.refreshDocuments();
    }
  }


  updateCareplanId() {
    let sql = `update DOCUMENTS set SubID=${this.carePlanGoalId}, Subcat = 'CAREPLANGOALS' where DOC_ID=${this.document.docID} `

    this.listS.executeSql(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.showDocProperties = true;
      setTimeout(() => {
     
        this.viewData.next(this.document);      
        this.cd.detectChanges();
        },500);
     
    
       
      })

  
  }
  refreshDocuments() {
    this.timeS.getcareplandocuments(this.user.id, this.carePlanGoalId).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.documentList = data;
      this.loading = false;
      if (this.documentList.length > 0) {
        this.chkDocuments = true;
      }
      this.cd.detectChanges();
    });
  }
  showEditDocumentModal(data: any) {
  
    this.showDocProperties = true;
    this.chkDocuments=true;
    setTimeout(() => {
    this.viewData.next(data);
    this.cd.detectChanges();
    },200);
  }
  DocCallback(doc:any){
    
    this.refreshDocuments();
  }
  showQuoteModal() {
    //this.tableDocumentId = null;
    this.quotesOpen = true;
    //this.tabFindIndex = 0;

    let id = this.user.id.substr(this.user.id - 5);

    this.populateDropdDowns();

    this.listS.getglobaltemplate()
      .subscribe((data) => (this.quoteTemplateList = data));

    //this.listCarePlanAndGolas();
    this.quoteGeneralForm.controls.careDomain.setValue("VARIOUS");
    this.quoteGeneralForm.controls.discipline.setValue("VARIOUS");
    this.quoteForm.get("program").value
      ? this.quoteGeneralForm.controls.careDomain.setValue("VARIOUS")
      : this.quoteForm.get("program").value;

    console.log(this.user)
    this.listS.getrecipientsqlid(this.user.id).subscribe((data) => {
      this.clientId = data;
    });

    // this.quoteForm.reset({
    //   program: null,
    //   template: null,
    //   templateFile: null,
    //   no: null,
    //   type: null,
    //   period: [],
    //   basePeriod: "ANNUALLY",
    //   programId: null,
    //   charges: false,
    //   basis:''
    // });

    this.detectChanges();
  }

  get remaining_fund_quote_line() {
    if (this.remaining_fund) {
      return this.remaining_fund;
    } else {
      var contribution = 0;
      if (!this.IS_CDC) {
        contribution = this.quoteForm.value.initialBudget;
      } else {
        contribution = this.quoteForm.value.govtContrib;
      }
      return contribution;
    }
  }

  get remaining_fund() {
    var contribution = 0;

    if (this.IS_CDC) {
      // contribution = this.quoteForm.value.initialBudget;
      contribution = this.get_total_package_value;
    } else {
      contribution = this.quoteForm.value.govtContrib;
    }

    contribution =
      this.globalS.isEmpty(contribution) || !contribution ? 0 : contribution;

    contribution = contribution - this.total_quote;

    return contribution;
  }

  buildForm() {
    // reset add quote line button for update or add
    this.addNewQuoteLine = false;

    if (this.option == "add") {
      this.quoteLines = [];

      this.total_base_quote = 0;
      this.total_admin = 0;
      this.total_quote = 0;
      // this.remaining_fund = 0;
    }

    //this.disableAddTabs = true;

    this.inputForm = this.formBuilder.group({
      autoLogout: [""],
      emailMessage: false,
      excludeShiftAlerts: false,
      excludeFromTravelinterpretation: false,
      inAppMessage: false,
      logDisplay: false,
      pin: [""],
      staffTimezoneOffset: [""],
      rosterPublish: false,
      shiftChange: false,
      smsMessage: false,
    });

    this.inActiveForm = this.formBuilder.group({
      timePeriod: [],
    });

    this.activeForm = this.formBuilder.group({
      aUpdateActivities: true,
      aUpdateApplicable: true,
      aCreateBookings: true,
      aDeleteActivities: false,

      bUpdateActivities: true,
      bUpdateApplicable: true,
      bCreateBookings: true,
      bDeleteActivities: false,

      timePeriod: [],
    });

    this.quoteGeneralForm = this.formBuilder.group({
      id: null,
      planType: null,
      name: null,
      program: "VARIOUS",
      discipline: "VARIOUS",
      careDomain: "VARIOUS",    
      classification : null,
      starDate: null,
      signOfDate: null,
      reviewDate: null,
      rememberText: null,
      publishToApp: false,
    });

    this.quoteForm = this.formBuilder.group({
      recordNumber: null,
      program: null,
      template: null,
      templateFile: null,
      no: null,
      type: null,
      period: [],
      basePeriod: "ANNUALLY",
      initialBudget: null,
      daysCalc: 365,
      govtContrib: null,
      programId: null,
      charges: false,
      basis: '',
      packageSupplements: "00000000000000000000000000",
      annualBasicCareFee: 0,
      dailyBasicCareFee: 0,
      dailyIncomeTestedFee: 0,
      incomeTestedFee: 0,
      monthlyBasicCareFee:0,
      annualIncomeTestedFee:0,
      monthlyIncomeTestedFee:0,
      calculateBasicCareFee: 0,
      govtContribution: 0,
      govtDistribution: 0,
      agreedTopUp: 0,
      amount: 0,
      
      balanceAtQuote: 0,
      basePension: null,
      budget: 0,
      CLAssessedIncomeTestedFee: 0,
      clientId: null,
      contribution: 0,
      cpid: null,
      created: '',
      dailyAgreedTopUp: '',
      dailyCDCRate: 0,
      docNo: null,
      documentId: null,
      feesAccepted: false,
      goals: null,
      hardshipSupplement: null,
      modified: null,
      newFileName: null,
      personId: null,
      quoteBase: null,
      quoteLines: null,
      quoteView: null,
      user: null,
      chkExpandGoalDescription: true

    });

    this.quoteIdsForm = this.formBuilder.group({
      itemId: null,
    });

    this.goalsAndStratergiesForm = this.formBuilder.group(goalsDefault);

    this.stratergiesForm = this.formBuilder.group({
      detail: "",
      PersonID: this.personIdForStrategy,
      outcome: "",
      strategyId: "",
      serviceTypes: "",
      recordNumber: ""

    });

    this.goalsAndStratergiesForm.get('chkStrategies').valueChanges.subscribe(data => { this.chkStrategies = data; })
    this.goalsAndStratergiesForm.get('chkDocuments').valueChanges.subscribe(data => {
      this.pUser = this.user;      
      this.chkDocuments = data;
      this.showDocProperties = data;
      this.viewData.next(null);
      //this.reload(false);
    })

    this.quoteForm.get('chkExpandGoalDescription').valueChanges.subscribe(data => {
      this.expandDescription = data;
      setTimeout(() => {
        this.cd.detectChanges();
      });

    });


    this.quoteListForm = this.formBuilder.group({
      chargeType: ['0', Validators.required],
      code: ['', Validators.required],
      displayText: null,
      strategy: null,
      sortOrder: ['0', Validators.required],
      
      quantity: [null, Validators.required],
      billUnit: null,
      period: null,
      weekNo: null,
      itemId: null,
      frequency: null,
      price: [null, Validators.required],
      tax: null,
      min: null,
      quoteQty:null,
      quotePerc: null,
      budgetPerc: null,
     
      roster: null,
      rosterString: null,
      notes: null,
     
      editable: true,
      recordNumber: null,
      bTotal: 0,
      
      interval: null,
      budgetCode:null,
      budgetUsage:null
    });



    this.supplements = this.formBuilder.group({
      domentica: false,
      levelSupplement: '',
      oxygen: false,
      feedingSuplement: false,
      feedingSupplement: '',
      EACHD: false,

      feedingSupplementLevel: '',
      visibilitySuplement: false,
      supplementVisibilityOption: '',
      visibilitySuplementLevel: '',
      visibilitySuplementLevel2: '',
      basePension:'',
      dailyBasicCareFee:0,
      dailyIncomeTestedFee:0


    });

    this.quoteListForm
      .get("chargeType")
      .valueChanges.pipe(
        switchMap((x) => {
          console.log(x);
          if (!x) return EMPTY;
          this.resetQuotePrimary();
          if (this.globalS.isEmpty(this.tableDocumentId)) {
            //console.log(this.tableDocumentId);
            //   this.globalS.eToast('Error', 'Program and Template are required');
            return EMPTY;
          }
         
          this.listStrategiesDropDown(this.tableDocumentId);
          if (!x) return EMPTY;
          return this.listS.getchargetype({
            program: this.quoteForm.get("program").value,
            index: x,
            AccountNo: this.user.code
            
          });
        })
      )
      .subscribe((data) => {
        this.codes = data;
       
        //   if(this.option == 'update')
        //   {
        //        var x = this.updateValues;
        //        console.log(x);
        //        setTimeout(() => {
        //         this.quoteListForm.patchValue({
        //             code: x.title,
        //             displayText: x.displayText,
        //         })
        //         this.detectChanges();
        //        }, 100);
        //   }
      });

    this.quoteListForm
      .get("code")
      .valueChanges.pipe(
        switchMap((x) => {
          this.slots = null;
          if (!x) return EMPTY;
          // return this.listS.getprogramproperties(x);
          let parm = {
            Program: this.quoteForm.get("program").value,
            ServiceType: x,
            AccountNo: this.user.code,
            DocHdrId :  this.tempIds ? this.tempIds.quoteHeaderId : this.record.recordNumber,
            viewFactor : this.getViewFActor()
          }
          this.serviceLoading=true;
          return this.listS.getserviceproperties(parm);
        })
      )
      .subscribe((data) => {
        if (!data){
          this.quoteListForm.patchValue({
            roster: "NONE",
            displayText: null,
            price: null,
            itemId: null,
            tax: null,
            bTotal: 0
          });
          return EMPTY;
        } 
        this.recipientProperties = data;
        this.serviceLoading=false;
        if (this.addNewQuoteLine == true) {
          this.quoteListForm.patchValue({
            displayText: data.billText,     
            bTotal: this.total_base_quote,
            budgetCode: data.budgetGroup,
            budgetUsage: this.total_base_quote,
            roster: "NONE",
            itemId: data.recnum,
            tax: data.tax,
            price: data.amount,
            quantity:1,
            billUnit: data.unit,
            frequency: 'WEEKLY',
          
            
          });
          
        }

        //   if(this.option == 'update'){
        //     console.log(this.updateValues)
        //     this.quoteListForm.patchValue({
        //         displayText: data.billText,
        //         price: data.amount,
        //         itemId: data.recnum,
        //         roster: this.determineRosterType(this.updateValues)
        //     });
        //   }
      });

    
   

    // this.quoteListForm.get("frequency").valueChanges.subscribe((data) => {
    //   if (!data) return;
    
    //   this.calculate_Quote_lines_total();
    // });

    // this.quoteListForm.get("quantity").valueChanges.subscribe((data) => {
    //   if (data || this.quoteListForm.get("frequency").value) {
       
    //     this.calculate_Quote_lines_total();
    //   }
    // });

    // this.quoteListForm.get("price").valueChanges.subscribe((data) => {
    //   if (data || this.quoteListForm.get("frequency").value) {
      
    //     this.calculate_Quote_lines_total();
    //   }
    
    // });

    this.quoteForm
      .get("program")
      .valueChanges.pipe(
        switchMap((x) => {
          this.IS_CDC = false;
          if (!x) {
            return EMPTY;
          }
          this.quoteGeneralForm.patchValue({
            program: x,
          });
          return this.listS.getprogramlevel(x);
        }),
        switchMap((x) => {
          this.IS_CDC = x.isCDC;
          this.IS_HCP = x.type == "DOHA";

          if (x.isCDC) {
            this._tempGovtContribution = x.quantity
              ? (x.quantity * 365).toFixed(2)
              : 0;

            this.quoteForm.patchValue({
              programId: x.recordNumber,
            });

            this.dailyBasicCareFee = x.defaultDailyFee;
            this.calculateBasicCareFee();
            this.detectChanges();
          }

          this.quoteForm.patchValue({
            programId: x.recordNumber,
          });

          if (this.option == "add") {
            this.quoteForm.patchValue({
              govtContribution: x.quantity ? (x.quantity * 365).toFixed(2) : 0,
            });
          }

          if (this.option == "update") {
            this.quoteForm.patchValue({
              govtContribution: this.quoteDetails.govtContribution,
            });
          }

          this.detectChanges();
          return EMPTY;
        })
      )
      .subscribe((data) => {
        this.detectChanges();
      });

    this.quoteForm.get("template").valueChanges.subscribe((data) => {
      if (!data) return;
      // this.showConfirm();
      this.load_Template();
    });

    this.quoteForm.get("basePeriod").valueChanges.subscribe((x) => {
      if (!x) return EMPTY;
      if (x == "ANNUALLY") {
        setTimeout(() => {
          this.quoteForm.patchValue({
            period: [new Date(), addYears(new Date(), 1)],
          });
        }, 0);
      }

      if (x == "FIXED PERIOD") {
        setTimeout(() => {
          this.quoteForm.patchValue({
            period: [startOfMonth(new Date()), endOfMonth(new Date())],
          });
        }, 0);
      }
      this.detectChanges();
    });

    this.quoteListForm.get("roster").valueChanges.subscribe((data) => {
     
      if (!data) {
           this.marginLeftValue = '-20rem';
          this.marginToValue = '25px'
          this.slots='';
          this.cd.detectChanges();
        return;

      }
      this.weekly = data;
      this.setPeriod(data);
      this.marginToValue = '18px'
      switch (data) {
        case 'WEEKLY':
          this.marginLeftValue = '-38rem';
          break;
        case 'FORTNIGHTLY':
          this.marginLeftValue = '-28rem';
          break;
        case 'MONTHLY':
          this.marginLeftValue = '-11rem';
          break;
        default:
          this.marginLeftValue = '-20rem';
          this.marginToValue = '25px'
          this.slots='';
          break;

      }
      this.cd.detectChanges();
    });

    combineLatest([
      this.quoteListForm.get("quantity").valueChanges,
      this.quoteListForm.get("period").valueChanges,
      this.quoteListForm.get("price").valueChanges,
      this.quoteListForm.get("frequency").valueChanges
    ]).subscribe((x) => {
      this.calculate_Quote_lines_total();
      // let weekNo = this.convert_period_to_value(x[1]);
      // this.current_line_value = weekNo * x[0] * x[2];
      // this.quoteListForm.patchValue({      
      //   quoteQty: weekNo *x[0],
      //   bTotal :  this.current_line_value
      // });

     // this.formatCurrency();

      this.cd.detectChanges();

    });

    this.NeedsRisksForm = this.formBuilder.group({
      name: [null, Validators.required],
      address: [null, Validators.required],
      genderOfBirth: [null, Validators.required],
      identity: [null],
      religion: [null],
      ausBirth: [''],
      arrivalYear: [null, Validators.required],
      SerialNo: [''],

      RecordNumber: 0



    });
  }

  calculate_Quote_lines_total(){
    let wk= this.convert_period_to_value(this.quoteListForm.get("frequency").value);
    this.current_line_value = this.quoteListForm.get('quantity').value * wk * this.quoteListForm.get('price').value;
    let budgetUsage :number = this.current_line_value  +  Number(this.total_base_quote);
    this.quoteListForm.patchValue({
      weekNo: wk,
      quoteQty: wk,
      bTotal :  this.current_line_value ,
      budgetUsage: budgetUsage,
    });

    this.current_quote_value = Number(this.total_quote) + this.current_line_value ;
  }

  formatCurrency() {
    const value = this.quoteListForm.get('bTotal').value;

    // Remove any non-numeric characters, including commas and dollar signs
    const numericValue = parseFloat(value.replace(/[^0-9.-]+/g, ''));

    if (!isNaN(numericValue)) {
      // Format the value as currency using Angular's DecimalPipe
      const formattedValue = this._decimalPipe.transform(numericValue, '1.2-2');
      // Add currency symbol
      this.quoteListForm.get('bTotal').setValue(`$${formattedValue}`, { emitEvent: false });
    } else {
      this.quoteListForm.get('bTotal').setValue('', { emitEvent: false });
    }
  }
  show_Rows(index: number) {
    this.showRows[index] = !this.showRows[index];
    this.cd.detectChanges();
  }
  listDropDowns() {
    let currentYear = new Date().getFullYear();
    for (let i = 1920; i <= currentYear; i++) {
      this.arrivals.push(i);
    }

    forkJoin([
      this.listS.getgendersIdenties(),
      this.listS.getreligions()
    ]).subscribe(data => {
      this.Identities = data[0];
      this.Religions = data[1];
      this.cd.detectChanges();
    });
  }
  tableDocumentId: number;

  get hideQuoteDetails() {
    return (
      (this.quoteForm.get("program").value != null &&
        this.quoteForm.get("template").value != null &&
        this.option &&
        this.option == "add" &&
        this.tableDocumentId) ||
      this.option == "update"
    );
  }

  get hideQuoteDetailsAnd() {
    return (
      (this.quoteForm.get("program").value &&
        this.quoteForm.get("template").value &&
        this.option && 
        this.option == "add" &&
        this.tableDocumentId) ||
      this.option == "update"
    );
  }

  load_Template(): void {

    if (this.option == "update") {
      this.listCarePlanAndGolas(this.tableDocumentId);
      this.disableAddTabs = false;


      return;
    }

    let qteHeader: any; //QuoteHeaderDTO;

    qteHeader = {
      personId: this.user.id,
      DocumentGroup: this.record.plan,
    };

    this.templateLoading = true;

    this.listS.createtempdoc(qteHeader).subscribe((data) => {
      this.tempIds = data;
      this.templateLoading = false;

      this.tableDocumentId = data.docId;
      this.cpid = data.docId;
      this.listCarePlanAndGolas(this.tableDocumentId);
      this.disableAddTabs = false;
    });


  }
  getPermisson(index: number) {
    var permissoons = this.globalS.getRecipientRecordView();
    return permissoons.charAt(index - 1);
  }
  showConfirm(): void {
    this.confirmModal = this.modal.confirm({
      nzTitle: "Do you want to select this template?",
      nzContent:
        "Clicking OK will record the quote against the selected program and service agreement template",
      nzOnOk: () => {
        let qteHeader: any; //QuoteHeaderDTO;

        qteHeader = {
          personId: this.user.id,
        };

        this.templateLoading = true;

        this.listS.createtempdoc(qteHeader).subscribe((data) => {
          this.tempIds = data;
          this.templateLoading = false;

          this.tableDocumentId = data.docId;
          this.listCarePlanAndGolas(this.tableDocumentId);
          this.disableAddTabs = false;
        });
      },
      nzOnCancel: () => {
        this.quoteForm.get("template").patchValue(null);
      },
    });
  }

  convert_period_to_value(data: string) {
    let weekNo = 1;

    if (data == "ONCE OFF") weekNo = 1;
    else if (data == "WEEKLY") weekNo = 52;
    else if (data == "FORTNIGHTLY") weekNo = 26;
    else if (data == "MONTHLY") weekNo = 12;
    else if (data == "QUARTERLY") weekNo = 4;
    else if (data == "HALF YEARLY") weekNo = 2;
    else if (data == "YEARLY") weekNo = 1;

    return weekNo;
  }

  determineRosterType(data: any) {
    if (data.roster && data.roster.length > 0) {
      return data.frequency;
    }

    if (data.roster == null || this.globalS.isEmpty(data.roster)) {
      return "NONE";
    }
  }

  resetQuotePrimary() {
    this.quoteListForm.patchValue({
      code: null,
      displayText: null,
      roster: "NONE",
      strategy: null,

    });
  }

  listStrategiesDropDown(docId: any) {
    this.listS.getstrategyList(docId.toString()).subscribe((data) => {
      this.strategies = data;
    });
  }

  detectChanges() {
    this.cd.detectChanges();
    this.cd.markForCheck();
  }

  setPeriod(data: any) {
    if (data && data != "NONE") {
      this.quoteListForm.get("period").enable();

      if (this.option == "add") {
        this.quoteListForm.patchValue({
          period: data.toUpperCase(),
          billUnit: "HOUR",
          weekNo: this.convert_period_to_value(data),
        });
      }

      if (this.option == "update") {
        this.quoteListForm.patchValue({
          period: data.toUpperCase(),
          billUnit: "HOUR",
          weekNo: this.convert_period_to_value(data),
        });
      }
      return;
    }

    // this.quoteListForm.patchValue({
    //   period: "WEEKLY",
    //   billUnit: "HOUR",
    //   weekNo: 52,
    //   roster: "NONE",
    // });

    this.quoteListForm.get("period").enable();
  }

  saveNewFilename() {
    this.qteHeaderDTO = { ...this.qteHeaderDTO, newFileName: this.newFileName };

    this.listS.getpostquote(this.qteHeaderDTO).subscribe(
      (data) => {
        if (this.option == "add") {
          this.globalS.sToast("Success", "Quote Added");
        } else {
          this.globalS.sToast("Success", "Quote Updated");
        }

        this.globalS.bToast("File Location", data.documentFileFolder);
        this.loadingSaveQuote = false;
        this.refresh.emit(true);
        this.newFileNameOpen = false;
        this.detectChanges();

        this.quotesOpen = false;
      },
      (err: any) => {
        // this.loadingSaveQuote = false;
        // this.newFileNameOpen = false;
        // this.quotesOpen = false;
        this.detectChanges();
      }
    );
  }

  handleOkTop(type: any) {  //doing      
    this.generatePdf(type, this.user.id);
    this.tryDoctype = "";
    this.pdfTitle = "";
  }

  handleOk() { }

  handleCancel() {
    //   this.listS.deletetempdoc(this.tableDocumentId).subscribe(data => console.log(data))
    this.quotesOpen = false;
    this.inActiveOpen = false;
    this.activeOpen = false;
  }

  closeFileName() {
    this.newFileNameOpen = false;
  }

  handleCancelTop(): void {
    this.drawerVisible = false;
    this.pdfTitle = ""
    this.tryDoctype = ""
  }

  handlegoalsStarCancel() {
    this.goalAndStrategiesmodal = false;
    this.isUpdateGoal = false;
    this.personIdForStrategy = "";
  }

  handleStarCancel() {
    this.strategiesmodal = false;
    this.isUpdateStrategy = false;
  }

  generatePdf(type: any, personid) {    

    var fQuery,fRecipient, fQuery_Recipient, txtTitle, Rptid, heading1, heading2, heading3, heading4, heading5;
    this.drawerVisible = true;
    this.loading = true;

    switch (type) {
      case 1:
        fQuery = "SELECT RecordNumber as recordnumber,CONVERT(varchar, [Date1],105) as Field1,CONVERT(varchar, [Date2],105) as Field2,CONVERT(varchar, [DateInstalled],105) as Field3,[State] as Field4, User1 AS Field5 FROM HumanResources WHERE PersonID = '" + personid + "' AND [Type] = 'RECIPIENTGOALS' ORDER BY Name";
        txtTitle = "Care Plan Goals List";
        heading1 = "Ant Complete";
        heading2 = "Last Review";
        heading3 = "Completed";
        heading4 = "Percent";
        heading5 = "Goal";
        Rptid = "0RYYxAkMCftBE9jc" //x8QVE8KhcjiJvD6c
        break;

      case 2:
        //StratagySQL
        fQuery = "SELECT RecordNumber, Notes AS Field1, Address1 AS Field2, [State] AS Field3, User1 AS Field4 FROM HumanResources WHERE [PersonID] = '" + personid + "' AND [GROUP] = 'PLANSTRATEGY'";
        txtTitle = "Care Plan Strategies List";
        heading1 = "Strategy";
        heading2 = "Achieved";
        heading3 = "Contracted ID";
        heading4 = "DS Services";
        Rptid = "0RYYxAkMCftBE9jc" //x8QVE8KhcjiJvD6c
        break;
      case 99:         
        fQuery = this.goalsAndStratergies;        
        fQuery_Recipient = this.user.code ;
        txtTitle = "Goals & Strategies";       
        Rptid = "iesmJwKJjPuRgMrZ";
        break;

      default:
        break;
    }

    // console.log(this.loggedInUser);    
    const data = {
      "template": { "_id": Rptid },
      "options": {
        "reports": { "save": false },
        "sql": fQuery,
        "txtTitle": txtTitle,
        "userid": this.loggedInUser.nameid,
        "Recipient": fQuery_Recipient,
        head1: heading1,
        head2: heading2,
        head3: heading3,
        head4: heading4,
        head5: heading5,
      }
    }
    this.loading = true;

    this.drawerVisible = true;
    this.printS.printControl(data).subscribe((blob: any) => {
      this.pdfTitle = txtTitle
      this.drawerVisible = true;
      let _blob: Blob = blob;
      let fileURL = URL.createObjectURL(_blob);
      this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
      this.loading = false;
      this.cd.detectChanges();
    }, err => {
      console.log(err);
      this.loading = false;
      this.ModalS.error({
        nzTitle: 'TRACCS',
        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
        nzOnOk: () => {
          this.drawerVisible = false;
        },
      });
    });
    return;
  }
  onChangeStatus(event: any) {
    console.log(event);

  }
  onSearch(event: any) {

    //   this.listS.getprogramproperties(event).pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
    //     this.recipientProperties = data;

    //     if (this.addNewQuoteLine == true) {
    //       this.quoteListForm.patchValue({
    //         displayText: data.billText,
    //         price: data.amount,
    //         roster: "NONE",
    //         itemId: data.recnum,
    //       });
    // }
    // });

  }
  saveGoals(data: any) {

    this.selectedGoals.forEach(data => {
      this.selectGoalModel = false;
      this.goalsAndStratergiesForm.patchValue({
        goalText: '',
        notes: '',
        goal: data.label,
        percent: 0,
        anticipated: null,
        achievementDate: null,
        dateArchived: null,
        lastReviewed: null,
        level: null,

      });

      this.saveCarePlan();
    });
    if (this.GOALSCAREPLAN_JOB == GoalCarePlanJob.ADD && this.saveGoals.length > 1) {
      this.selectedGoals = [];
      this.globalS.sToast("Success", "Data Inserted");
      this.personIdForStrategy = data;
      this.listCarePlanAndGolas(this.cpid);
      this.cd.markForCheck();
      this.goalAndStrategiesmodal = false;


    } else if (this.GOALSCAREPLAN_JOB == GoalCarePlanJob.UPDATE && this.saveGoals.length > 1) {
      this.globalS.sToast("Success", "Data Updated");
      this.personIdForStrategy = data;
      this.listCarePlanAndGolas(this.cpid);
      this.cd.markForCheck();
      this.goalAndStrategiesmodal = false;
      this.selectedGoals = [];
    }

  }

 
  saveCarePlan() {


    console.log(this.goalsAndStratergiesForm.value);
    const {

      notes,
      goal,
      level,
      anticipated,
      dateArchived,
      lastReviewed,
      percent,
      recordnumber
    } = this.goalsAndStratergiesForm.value;

    var params: Goals = {
      RecordNumber: recordnumber,
      PersonID: this.cpid.toString(),
      Notes: notes,
      Goal: `${goal} `,
      Level: level,
      Anticipated: anticipated,
      LastReviewed: lastReviewed,
      DateArchived: dateArchived,
      Percent: percent,
      Strategies: this.strategiesList
    };



    if (this.GOALSCAREPLAN_JOB ==GoalCarePlanJob.ADD) {
      this.timeS
        .postGoalsAndStratergies(params)
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
         if (this.saveGoals.length == 1) {
            this.selectedGoals = [];
            this.globalS.sToast("Success", "Data Inserted");
            this.personIdForStrategy = data;
            this.listCarePlanAndGolas(this.cpid);
            this.cd.markForCheck();
            this.goalAndStrategiesmodal = false;
          }
        });
    }

    if (this.GOALSCAREPLAN_JOB == GoalCarePlanJob.UPDATE) {
      this.timeS
        .updateGoalsAndStratergies(params)
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          if (this.saveGoals.length == 1) {
            this.selectedGoals = [];
            this.globalS.sToast("Success", "Data Updated");
            this.listCarePlanAndGolas(this.cpid);
            this.isUpdateGoal = false;
            this.goalAndStrategiesmodal = false;
            this.cd.markForCheck();
          }
        });
    }
    this.cd.markForCheck();
  }

  strategiesList: Array<any> = [];
  carePlanGoalList: Array<any> = [];

  saveStrategy() {

    this.stratergiesForm.controls.PersonID.setValue(this.carePlanGoalId);

    const {
      PersonID,
      detail,
      outcome,
      recordNumber,
      serviceTypes,
      strategyId,
    } = this.stratergiesForm.getRawValue();

    var params: PlanStrategy = {
      RecordNumber: recordNumber,
      PersonID: PersonID,
      Detail: detail,
      Outcome: outcome,
      StrategyId: strategyId,
    };

    // if (this.GOALSTRATEGY_JOB == GoalStrategyJob.ADD) {
    //   params.RecordNumber=null;
    //   params.PersonID=null;    
    //     this.strategiesList = [...this.strategiesList, params];
    // }

    // if (this.GOALSTRATEGY_JOB == GoalStrategyJob.UPDATE) {
    //   this.strategiesList[this.selectedStrategyIndex] = params
    // }

    this.strategiesmodal = false;

    if (!this.isUpdateStrategy) {
      this.timeS
        .postplanStrategy(params)
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          this.globalS.sToast("Success", "Data Inserted");
          this.strategiesmodal = false;
          this.listStrtegies(PersonID);
          this.cd.markForCheck();
        });
    } else {
      this.timeS
        .updateplanStrategy(this.stratergiesForm.value)
        .pipe(takeUntil(this.unsubscribe))
        .subscribe((data) => {
          this.globalS.sToast("Success", "Data Updated");
          this.strategiesmodal = false;
          this.listStrtegies(PersonID);
          this.cd.markForCheck();
        });
    }
    this.cd.markForCheck();
  }

  listCarePlanAndGolas(docid: any = null) {
    if (this.globalS.isEmpty(docid)) return;
    this.loading = true;
    this.listS.getCareplangoals(docid).subscribe((data) => {
      this.goalsAndStratergies = data;
      this.loading = false;
      this.cd.markForCheck();
    });
  }

  listStrtegies(personID: any) {
    this.loading = true;
    this.listS.getStrategies(personID).subscribe((data) => {
      var strategies = data.map(x => {
        return {
          RecordNumber: x.recordnumber,
          Detail: x.strategy,
          Outcome: x.achieved,
          StrategyId: x.state,
        }
      });
      this.strategiesList =strategies
      if (strategies.length > 0) {
        this.chkStrategies = true;
        this.goalsAndStratergiesForm.patchValue({chkStrategies:true});
        //this.strategiesList = [...strategies];
      }
      this.loading = false;
      this.cd.markForCheck();
    });
  }

  deleteCarePlanGoal(data: any) {
    this.timeS
      .deleteCarePlangoals(data.recordnumber)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => {
        if (data) {
          this.globalS.sToast("Success", "Data Deleted!");
          this.listCarePlanAndGolas(this.tableDocumentId);
          this.cd.markForCheck();
          return;
        }
        this.cd.markForCheck();
      });
  }

  deleteCarePlanStrategy(data: any) {
    this.timeS
      .deleteCarePlanStrategy(data.RecordNumber)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => {
        if (data) {
          this.globalS.sToast("Success", "Data Deleted!");
          this.listStrtegies(this.carePlanGoalId);
          this.cd.markForCheck();
          return;
        }
        this.cd.markForCheck();
      });
  }

  deleteDocument(data: any) {
    this.listS
      .deleteDocument(data.docID)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => {
        if (data) {
          this.globalS.sToast("Success", "Data Deleted!");
          this.refreshDocuments();
          this.cd.markForCheck();
          return;
        }
        this.cd.markForCheck();
      });
  }
  calculateAllQuotes() {
    this.total_admin = this.generate_total_admin();
    this.total_quote = (this.generate_total() + this.total_admin).toFixed(2);
    this.total_base_quote = (this.total_quote - this.total_admin).toFixed(2);
  }

  deleteQuoteList(data: any, index: number) {
    if (this.option == "update") {
      this.listS.deletequoteline(data.recordNumber).subscribe((data) => {
        this.quoteLines = this.quoteLines.filter((x, i) => i !== index);

        this.calculateAllQuotes();
        this.detectChanges();
      });
    }

    if (this.option == "add") {
      this.quoteLines = this.quoteLines.filter((x, i) => i !== index);

      this.calculateAllQuotes();
    }
  }

  showCarePlanStrategiesModal() {

    this.GOALSCAREPLAN_JOB = GoalCarePlanJob.ADD;
    this.strategiesList = [];

    this.stratergiesList = [];

    this.personIdForStrategy = "";
    this.selectGoalModel = true;

  }

  showStrategiesModal() {

    this.stratergiesForm.reset({
      detail: null,
      outcome: null,
      strategyId: null,
    });
    this.stratergiesForm.reset({
      PersonId:  this.carePlanGoalId,
    
    });
    
    this.isUpdateStrategy = false;   
    this.GOALSTRATEGY_JOB = GoalStrategyJob.ADD
    this.strategiesmodal = true;
  }

  carePlanGoalId: any;
  showEditCarePlanModal(data: any) {
    
    this.goalsAndStratergiesForm.reset();
    this.GOALSTRATEGY_JOB = GoalStrategyJob.UPDATE
    this.GOALSCAREPLAN_JOB = GoalCarePlanJob.UPDATE
    this.carePlanGoalId = data.recordnumber;

    this.tabFinderIndexbtn = 0;
    this.goalAndStrategiesmodal = true;
    this.isUpdateGoal = true;

    this.listStrtegies(data.recordnumber);
    this.personIdForStrategy = data.recordnumber;

    this.goalsAndStratergiesForm.patchValue({

      goal: data.goal,
      anticipated: data.anticipated,
      dateArchived: data.dateArchived,
      lastReviewed: data.lastReviewed,
      level: data.level,
      notes: data.notes,
      recordnumber: data.recordnumber,
      percent: data.percent,
    });

    this.timeS.getcareplandocuments(this.user.id, this.carePlanGoalId).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.documentList = data;
      if (this.documentList.length > 0) {
       // this.chkDocuments = true;
        this.goalsAndStratergiesForm.patchValue({chkDocuments:true});
      }
      this.loading = false;
      this.cd.detectChanges();
    });
   // this.cd.detectChanges();
  }

  showEditStrategyModal(data: any, index:number) {
    //console.log(data)
    this.isUpdateStrategy = true;
    this.strategiesmodal = true;
    this.GOALSTRATEGY_JOB = GoalStrategyJob.UPDATE
   
    if (this.GOALSTRATEGY_JOB == GoalStrategyJob.UPDATE) {
      this.stratergiesForm.patchValue({
        detail: data.Detail,
        PersonID: this.carePlanGoalId,
        outcome: data.Outcome,
        strategyId: data.StrategyId,
        recordNumber: data.RecordNumber,
      });
    }
  }

  getChargeType(type: string): any {
    if (type == "DIRECT SERVICE" || type == "SERVICE") return "1";

    else if (type == "GOODS/EQUIPMENT" || type == "ITEM") return "2";

    else if (type == "PACKAGE ADMIN") return "3";

    else if (type == "CASE MANAGEMENT") return "4";
    else return "1"
  }

  getChargeType_label(type: string): any {
    if (type == "DIRECT SERVICE" || type == "SERVICE") return "SERVICES";

    else if (type == "GOODS/EQUIPMENT" || type == "ITEM") return "GOODS AND CONSUMABLES";

    else if (type == "PACKAGE ADMIN") return "ADMINISTRATION";

    // else if (type == "CASE MANAGEMENT") return "4";
    else return type
  }
  updateValues: any;
  firstLoadQuoteLine: boolean = true;
  quoteLineIndex: number;

  GET_CORRECT_ROSTER(data: any): string {
    if (this.globalS.isEmpty(data.roster)) {
      return "NONE";
    }

    return data.frequency;
  }

  showEditQuoteModal(data: any, index: number) {
    console.log(data);

    this.addNewQuoteLine = false;
    this.quoteLineIndex = index;

    this.listS.getquotelinedetails(data.recordNumber).subscribe((x) => {
      this.updateValues = x;
      setTimeout(() => {
      this.quoteListForm.patchValue({
        chargeType: this.getChargeType(x.mainGroup),
        code: x.title,
        roster: this.GET_CORRECT_ROSTER(x),
        displayText: x.displayText,
        frequency: x.frequency,
        quantity: x.qty,
        period: x.frequency,
        billUnit: x.billUnit,
        weekNo: x.lengthInWeeks,
        price: x.rate,
        notes: x.notes,
        recordNumber: data.recordNumber,
        sortOrder : x.sortOrder,
      });

      this.slots = x.roster;
    },300)
      this.detectChanges();
    });

    this.quoteLineOpen = true;
    this.firstLoadQuoteLine = true;
    if (this.selectedProgram.packageSupplements!=null)
      this.setPackageSupplements(this.selectedProgram.packageSupplements);

  
  }
 
  tabFinderIndexbtn: number = 0;

  tabFindChangeStrategies(index: number) {
    this.tabFinderIndexbtn = index;
    if (index == 1 && !this.globalS.isEmpty(this.carePlanGoalId)) {
      this.listStrtegies(this.carePlanGoalId);
    }


  }

  tabFindChange(index: number) {
    this.tabFindIndex = index;
    //if (this.tabFindIndex == 0) {
    //   this.quoteGeneralForm.patchValue({
    //     name: this.quoteForm.get("template").value,
    //     program: this.globalS.isEmpty(this.quoteForm.get("program").value)
    //       ? "VARIOUS"
    //       : this.quoteForm.get("program").value,
    //     id: this.carePlanID.itemId,
    //     planType: "SUPPORT PLAN",
    //     careDomain: "VARIOUS",
    //     discipline: "VARIOUS",
    //     starDate: this.date,
    //     signOfDate: this.date,
    //     reviewDate: this.date,
    //     publishToApp: false,
    //   });
    // }
    if (this.tabFindIndex == 2 && this.option == "update") {
      this.listCarePlanAndGolas(this.cpid);

    }
    if (this.tabFindIndex == 4) {
      if (this.record.packageSupplements != "00000000000000000000000000")
        this.setPackageSupplements(this.record.packageSupplements);

    }

    if (this.tabFindIndex == 1) {
      this.showNeedRisks = true;
      this.ViewNeedsRisk.next(this.user);
      this.cd.detectChanges();

    }
  }

  getPackageSupplements() {
    let data = "00000000000000000000000000";
    let len = 0;
    let bits: any = data.split('');


    if (this.supplements.value.domentica) {
      bits[0] = '1';
      len = this.supplements.value.levelSupplement.length;
      let ind = toNumber((this.supplements.value.levelSupplement).charAt(len - 1));
      bits[ind] = '1';
    }

    if (this.supplements.value.oxygen)
      bits[5] = '1';


    if (this.supplements.value.feedingSuplement)
      bits[6] = '1';

    if (this.supplements.value.feedingSupplementLevel == 'Bolus')
      bits[7] = '1';
    if (this.supplements.value.feedingSupplementLevel == 'Non-Bolus')
      bits[8] = '1';

    if (this.supplements.value.EACHD)
      bits[9] = '1';

    if (this.supplements.value.visibilitySuplement) {
      bits[10] = '1';
      len = this.supplements.value.visibilitySuplementLevel.length;
      let ind = toNumber((this.supplements.value.visibilitySuplementLevel).charAt(len - 1));
      if (ind > 0)
        bits[ind + 10] = '1';

      if (this.supplements.value.supplementVisibilityOption == 'ARIA')
        bits[18] = '1';

      let selected = this.fundingAriaList.find(x => x.description == this.supplements.value.visibilitySuplementLevel2);
      let i = this.fundingAriaList.indexOf(selected)
      if (i > -1)
        bits[i + 19] = '1';

    }


    this.quoteForm.patchValue({
      packageSupplements: bits.join('')
    })

  }

  setPackageSupplements(data: any) {
    if (data == null) return;

    let bits: any = data.split('');

    let levelSupplement: string = '';
    if (bits[1] == '1')
      levelSupplement = 'LEVEL 1'
    else if (bits[2] == '1')
      levelSupplement = 'LEVEL 2'
    else if (bits[3] == '1')
      levelSupplement = 'LEVEL 3'
    else if (bits[4] == '1')
      levelSupplement = 'LEVEL 4'

    let feedingSupplementLevel: string = '';
    if (bits[7] == '1')
      feedingSupplementLevel = 'Bolus'
    else if (bits[8] == '1')
      feedingSupplementLevel = 'Non-Bolus'

    let visibilitySuplementLevel: string = '';
    let visibilitySuplementLevel2: string = '';
    if (bits[11] == '1')
      visibilitySuplementLevel = 'MMM 1'
    else if (bits[12] == '1')
      visibilitySuplementLevel = 'MMM 2'
    else if (bits[13] == '1')
      visibilitySuplementLevel = 'MMM 3'
    else if (bits[14] == '1')
      visibilitySuplementLevel = 'MMM 4'
    else if (bits[15] == '1')
      visibilitySuplementLevel = 'MMM 5'
    else if (bits[16] == '1')
      visibilitySuplementLevel = 'MMM 6'
    else if (bits[17] == '1')
      visibilitySuplementLevel = 'MMM 7'


    let supplementVisibilityOption = "";
    if (bits[18] == '1')
      supplementVisibilityOption = 'ARIA';
    else
      supplementVisibilityOption = 'MM';

    this.SupplementVisibility = supplementVisibilityOption;

    for (let i = 19; i < 25; i++) {
      if (bits[i] == 1) {
        visibilitySuplementLevel2 = this.fundingAriaList[i - 19].description
      }
    }

    this.supplements.patchValue({

      domentica: bits[0] == 1 ? true : false,
      oxygen: bits[5] == 1 ? true : false,
      feedingSuplement: bits[6] == 1 ? true : false,
      EACHD: bits[9] == 1 ? true : false,
      visibilitySuplement: bits[10] == 1 ? true : false,
      levelSupplement: levelSupplement,
      feedingSupplementLevel: feedingSupplementLevel,
      visibilitySuplementLevel: visibilitySuplementLevel,
      visibilitySuplementLevel2: visibilitySuplementLevel2,
      supplementVisibilityOption: supplementVisibilityOption

    })

  }

  generate_total() {
    var total: number = 0;
    this.quoteLines.forEach((x) => {
      if (
        x.mainGroup != "ADMIN" &&
        x.mainGroup != "CASE MANAGEMENT" &&
        x.mainGroup != "ADMIN-DEFAULT"
      ) {
        total =
          total + this.totalamount(x.price, x.quoteQty, x.tax, x.quantity);
      }
    });
    return total;
  }

  generate_total_admin() {
    var total: number = 0;
    this.quoteLines.forEach((x) => {
      if (
        x.mainGroup == "ADMIN" ||
        x.mainGroup == "CASE MANAGEMENT" ||
        x.mainGroup == "ADMIN-DEFAULT"
      ) {
        total =
          total + this.totalamount(x.price, x.quoteQty, x.tax, x.quantity);

         
      }
    });
    return total;
  }

  total_quote: any;
  total_base_quote: any;
  total_admin: any;
  // remaining_fund: any;

  calculate_HCP_Admin(acceptCharges: AcceptCharges = this.acceptCharges) {
    if (!acceptCharges) return;

    var govtContrib = parseFloat(this.quoteForm.get("govtContrib").value);

    if (!acceptCharges.isPercent) {
      return parseFloat(acceptCharges.p_Def_Admin_Admin_PercAmt);
    }

    if (acceptCharges.isPercent) {
      var percentage =
        parseFloat(acceptCharges.p_Def_Admin_Admin_PercAmt) / 100.0;

      if (
        acceptCharges.p_Def_IncludeBasicCareFeeInAdmin == false &&
        acceptCharges.p_Def_IncludeIncomeTestedFeeInAdmin == false
      ) {
        return percentage * (govtContrib / 365);
      }

      if (
        acceptCharges.p_Def_IncludeBasicCareFeeInAdmin == true &&
        acceptCharges.p_Def_IncludeIncomeTestedFeeInAdmin == false
      ) {
        return percentage * ((govtContrib + this.annualBasicCareFee) / 365);
      }

      if (
        acceptCharges.p_Def_IncludeBasicCareFeeInAdmin == true &&
        acceptCharges.p_Def_IncludeIncomeTestedFeeInAdmin == true
      ) {
        return (
          percentage *
          ((govtContrib +
            this.annualBasicCareFee +
            this.annualIncomeTestedFee) /
            365)
        );
      }
    }
  }

  calculate_CM_Admin(acceptCharges: AcceptCharges = this.acceptCharges) {
    if (!acceptCharges) return;
    var govtContrib = parseFloat(this.quoteForm.get("govtContrib").value);

    if (!acceptCharges.isPercent) {
      return parseFloat(acceptCharges.p_Def_Admin_CM_PercAmt);
    }

    if (acceptCharges.isPercent) {
      var percentage = parseFloat(acceptCharges.p_Def_Admin_CM_PercAmt) / 100.0;

      if (
        acceptCharges.p_Def_IncludeBasicCareFeeInAdmin == false &&
        acceptCharges.p_Def_IncludeIncomeTestedFeeInAdmin == false
      ) {
        return percentage * (govtContrib / 365);
      }

      if (
        acceptCharges.p_Def_IncludeBasicCareFeeInAdmin == true &&
        acceptCharges.p_Def_IncludeIncomeTestedFeeInAdmin == false
      ) {
        return percentage * ((govtContrib + this.annualBasicCareFee) / 365);
      }

      if (
        acceptCharges.p_Def_IncludeBasicCareFeeInAdmin == true &&
        acceptCharges.p_Def_IncludeIncomeTestedFeeInAdmin == true
      ) {
        return (
          percentage *
          ((govtContrib +
            this.annualBasicCareFee +
            this.annualIncomeTestedFee) /
            365)
        );
      }
    }
  }

  get get_total_package_value() {
    return (
      this.annualBasicCareFee +
      parseFloat(this.quoteForm.get("govtContrib").value) +
      this.annualIncomeTestedFee
    );
  }

  CHECKACCEPTCHARGES() {
    var _quote, _quote2;

    // let _quote: QuoteLineDTO = {
    //     docHdrId: this.tempIds.quoteHeaderId,
    //     billUnit: quote.billUnit,
    //     code: quote.code,
    //     displayText: quote.displayText,
    //     qty: quote.quantity,
    //     frequency: quote.period,
    //     quoteQty: quote.weekNo,
    //     roster: quote.rosterString,
    //     unitBillRate: quote.price,
    //     tax: quote.tax,
    //     itemId: quote.itemId,
    //     mainGroup: quote.mainGroup,
    //     recordNumber: quote.recordNumber
    // }

    var samp = {
      billUnit: "",
      code: "",
      displayText: "",
      docHdrId: 123,
      frequency: "",
      itemId: 123,
      qty: 1,
      quoteQty: 52,
      recordNumber: null,
      roster: null,
      tax: null,
      unitBillRate: "23",
    };

    forkJoin([
      this.listS.getaccceptcharges(this.quoteForm.value.program),
    ]).subscribe((x) => {
      this.acceptCharges = x[0];
      var temp: number = this.quoteForm.value.govtContrib;

      // this.dailyBasicCareFee = parseFloat(this.acceptCharges.p_Def_Fee_BasicCare);
      // this.annualBasicCareFee = this.dailyBasicCareFee * 365;
      // this.monthlyBasicCareFee = (this.dailyBasicCareFee * 365) / 12;

      _quote = {
        code: this.acceptCharges.p_Def_Admin_AdminType,
        displayText: "Charges",
        billUnit: "Service",
        quantity: 1,
        frequency: "Daily",
        quoteQty: 365,
        mainGroup: "ADMIN-DEFAULT",
        price: this.calculate_HCP_Admin(this.acceptCharges),
      };

      _quote2 = {
        code: this.acceptCharges.p_Def_Admin_CMType,
        displayText: "Charges",
        billUnit: "Service",
        quantity: 1,
        frequency: "Daily",
        mainGroup: "ADMIN-DEFAULT",
        quoteQty: 365,
        price: this.calculate_CM_Admin(this.acceptCharges),
      };

      this.quoteLines = [...this.quoteLines, _quote, _quote2];

      this.total_admin = this.generate_total_admin();
      this.total_quote = (this.generate_total() + this.total_admin).toFixed(2);
      this.total_base_quote = (this.total_quote - this.total_admin).toFixed(2);
      this.refreshQuoteLines()
      this.handleCancelLine();
      this.detectChanges();
    });
  }

  GENERATE_QUOTE_LINE() {

    if (!this.quoteListForm.valid) {
      this.globalS.eToast("Error", "Please fill all required fields");
      return;
    }

    const quote = this.quoteListForm.getRawValue();

    if (this.option == "add") {
      if (this.addNewQuoteLine) {
        let _quote: QuoteLineDTO = {
          docHdrId: this.tempIds ? this.tempIds.quoteHeaderId : this.record.recordNumber,
          billUnit: quote.billUnit,
          code: quote.code,
          displayText: quote.displayText,
          qty: quote.quantity,
          frequency: quote.period,
          quoteQty: quote.weekNo,
          roster: quote.rosterString,
          unitBillRate: quote.price,
          tax: quote.tax,
          itemId: quote.itemId,
          mainGroup: quote.mainGroup,
          recordNumber: quote.recordNumber,
          lengthInWeeks: quote.weekNo,
          rcycle: quote.rcycle,
          notes: quote.notes,
          sortOrder: quote.sortOrder,
          quotePerc: quote.quotePerc,
          budgetPerc: quote.budgetPerc,
          priceType: quote.priceType,
          lineNo : this.quoteLines.length + 1
        };

        this.listS.createQuoteLine(_quote).subscribe((data) => {
          this.quoteLines = [
            ...this.quoteLines,
            {
              code: data.code,
              displayText: data.displayText,
              quantity: data.qty,
              billUnit: data.billUnit,
              frequency: data.frequency,
              quoteQty: data.quoteQty,
              price: data.unitBillRate,
              tax: data.tax,
              mainGroup: data.mainGroup,
              recordNumber: data.recordNumber,
            },
          ];

          this.total_base_quote = this.generate_total().toFixed(2);
          this.total_admin = this.generate_total_admin();
          this.total_quote = (this.generate_total() + this.total_admin).toFixed(
            2
          );

          this.handleCancelLine();
          this.globalS.sToast("Success", "Added QuoteLine");
          this.generate_lines() ;
          this.detectChanges();
        });

        return;
      }

      let da: QuoteLineDTO = {
        billUnit: quote.billUnit,
        code: quote.code,
        displayText: quote.displayText,
        qty: quote.quantity,
        frequency: quote.period,
        quoteQty: quote.weekNo,
        roster: quote.rosterString,
        unitBillRate: quote.price,
        tax: quote.tax,
        mainGroup: quote.mainGroup,
        recordNumber: quote.recordNumber,
        lengthInWeeks: quote.weekNo,
        rcycle: quote.rcycle,
        notes: quote.notes,
        sortOrder: quote.sortOrder,
        quotePerc: quote.quotePerc,
        budgetPerc: quote.budgetPerc,
        priceType: quote.priceType,
      };

      this.listS.updatequoteline(da, quote.recordNumber).subscribe((data) => {
        // this.globalS.sToast("Success", "Quote Line updated");

        const quoteLinesTemp = [...this.quoteLines];
        quoteLinesTemp[this.quoteLineIndex] = {
          ...quoteLinesTemp[this.quoteLineIndex],
          code: data.code,
          displayText: data.displayText,
          quantity: data.qty,
          billUnit: data.billUnit,
          frequency: data.frequency,
          lengthInWeeks: data.lengthInWeeks,
          price: data.unitBillRate,
          tax: data.tax,
          recordNumber: data.recordNumber,
          quoteQty: data.quoteQty,
        };
        this.quoteLines = quoteLinesTemp;
        this.total_base_quote = this.generate_total().toFixed(2);
        this.total_admin = this.generate_total_admin();
        this.total_quote = (this.generate_total() + this.total_admin).toFixed(
          2
        );

        // this.globalS.sToast("Success", "Updated QuoteLine");
        this.handleCancelLine();
        this.detectChanges();

        this.quoteLineOpen = false;
        this.detectChanges();
      });
      this.generate_lines() ;
      this.handleCancelLine();
      this.detectChanges();
    }

    if (this.option == "update") {
      var quoteForm = this.quoteForm.value;
      var quoteLine = this.quoteListForm.getRawValue();

      if (this.addNewQuoteLine) {
        // console.log("add");
        let _quote: QuoteLineDTO = {
          docHdrId: this.tempIds ? this.tempIds.quoteHeaderId : this.record.recordNumber,
          billUnit: quote.billUnit,
          code: quote.code,
          displayText: quote.displayText,
          qty: quote.quantity,
          frequency: quote.period,
          quoteQty: quote.weekNo,
          unitBillRate: quote.price,
          roster: quote.rosterString,
          tax: quote.tax,
          itemId: quote.itemId,
          mainGroup: quote.mainGroup,
          lengthInWeeks: quote.lengthInWeeks,
          rcycle: quote.rcycle,
          notes: quote.notes,
          sortOrder: quote.sortOrder,
          quotePerc: quote.quotePerc,
          budgetPerc: quote.budgetPerc,
          priceType: quote.priceType,



        };
        // console.log(_quote);

        this.listS.createQuoteLine(_quote).subscribe((data) => {
          this.quoteLines = [
            ...this.quoteLines,
            {
              code: data.code,
              displayText: data.displayText,
              quantity: data.qty,
              billUnit: data.billUnit,
              frequency: data.frequency,
              quoteQty: data.quoteQty,
              price: data.unitBillRate,
              tax: data.tax,
              mainGroup: data.mainGroup,
            },
          ];

      

          this.total_base_quote = this.generate_total().toFixed(2);
          this.total_admin = this.generate_total_admin();
          this.total_quote = (this.generate_total() + this.total_admin).toFixed(
            2
          );

          // this.remaining_fund = (this.quoteForm.value.govtContrib - this.total_quote).toFixed(2);
          this.generate_lines() ;
          this.globalS.sToast("Success", "Quote Line Updated Successfully");
          this.handleCancelLine();
          
          this.detectChanges();
        });
        return;
      }

      let da: QuoteLineDTO = {

        billUnit: quoteLine.billUnit,
        itemId: quoteLine.itemId,
        qty: quoteLine.quantity,
        displayText: quoteLine.displayText,

        unitBillRate: quoteLine.price,
        frequency: quoteLine.period,
        lengthInWeeks: quoteLine.weekNo,
        roster: quoteLine.rosterString,
        serviceType: quoteLine.code,
        quoteQty: quoteLine.weekNo,
        code: quote.code,
        tax: quote.tax,
        mainGroup: quote.mainGroup,
        rcycle: quote.rcycle,
        notes: quote.notes,
        sortOrder: quote.sortOrder,
        quotePerc: quote.quotePerc,
        budgetPerc: quote.budgetPerc,
        priceType: quote.priceType,
      };

      this.listS
        .updatequoteline(da, this.updateValues.recordNumber)
        .subscribe((data) => {
          this.globalS.sToast("Success", "Quote Line updated");

          const quoteLinesTemp = [...this.quoteLines];
          quoteLinesTemp[this.quoteLineIndex] = {
            ...quoteLinesTemp[this.quoteLineIndex],
            code: quoteLine.code,
            displayText: quoteLine.displayText,
            quantity: quoteLine.quantity,
            billUnit: quoteLine.billUnit,
            frequency: quoteLine.period,
            lengthInWeeks: quoteLine.weekNo,
            price: quoteLine.price,
            tax: quoteLine.tax,
            recordNumber: quoteLine.recordNumber,
            quoteQty: quoteLine.weekNo,
          };
          this.quoteLines = quoteLinesTemp;

          this.total_base_quote = this.generate_total().toFixed(2);
          this.total_admin = this.generate_total_admin();
          this.total_quote = (this.generate_total() + this.total_admin).toFixed(
            2
          );
      
          //  this.globalS.sToast("Success", "Updated QuoteLine");
          this.handleCancelLine();
          this.detectChanges();
          this.generate_lines() ;
          this.quoteLineOpen = false;
          this.detectChanges();
      
          
        });
    }
  }
  setDates(dates: any) {
    //let dates = this.quoteForm.get('period').value;
    if (dates.length < 2) return;
    this.quoteGeneralForm.patchValue({
      starDate: dates[0],
      signOfDate: dates[1],

    });
  }
  handleCancelLine() {
    this.quoteLineOpen = false;
  }

  refreshQuoteLines() {
    
    this.total_base_quote = this.generate_total().toFixed(2);
    this.total_admin = this.generate_total_admin();
    this.total_quote = (this.generate_total() + this.total_admin).toFixed(
      2
    );

    //  this.globalS.sToast("Success", "Updated QuoteLine");
    this.handleCancelLine();
    this.detectChanges();

    this.quoteLineOpen = false;
    this.detectChanges();

    this.generate_lines() ;
  }
  loadingSaveQuote: boolean = false;

  qteHeaderDTO: QuoteHeaderDTO;

  saveQuote() {
    
    
  
    let qteLineArr: Array<QuoteLineDTO> = [];
    let goals: Array<any> = [];
    let qteHeader: any; //QuoteHeaderDTO;

    this.getPackageSupplements();

    const quoteForm = this.quoteForm.getRawValue();
   

    this.goalsAndStratergies.forEach((e) => {
      goals.push({
        Goal: e.goal,
        Strategies: e.strategies,
      });
    });

    //console.log(this.goalsAndStratergies);
    // return;

    if (this.clientId == null) {
      this.listS.getrecipientsqlid(this.user.id).subscribe((data) => {
        this.clientId = data;
      });
    }

    this.quoteLines.forEach((x) => {
      let da: QuoteLineDTO = {
        sortOrder: x.sortOrder ?? 0,
        billUnit: x.billUnit,
        itemId: x.itemId,
        qty: x.quantity,
        displayText: x.displayText,

        unitBillRate: x.price,
        frequency: x.frequency,
        lengthInWeeks: x.quoteQty,
        roster: x.rosterString,
        serviceType: x.code,
        strategyId: x.strategy,
      };

      qteLineArr.push(da);
    });

     let created ;
     if (this.option == "add" || this.Created == null){
      created= format(new Date(), "yyyy/MM/dd") ;
      this.Created =created;
      this.Modified =created;
    
     }else{
      created = this.Created.toString().substring(0,10);
      created = format(moment(created, 'DD/MM/YYYY').toDate(), "yyyy/MM/dd")
      
    }
     

      qteHeader = {
      recordNumber: this.tempIds ? this.tempIds.quoteHeaderId : this.record.recordNumber,
      docNo: this.BudgetNo,
      clientId: this.clientId,
      programId: quoteForm.programId,
      program: quoteForm.program ?? this.quoteGeneralForm.get("program").value,
      cpid: this.cpid,
      contribution: this.contribution ?? 0,
      govtContribution: quoteForm.govtContribution ?? 0,
      amount: this.total_quote ?? 0,

      daysCalc: 365,
      budget: quoteForm.budget ?? 0,
      quoteBase: quoteForm.quoteBase ?? 'ANNUALLY',
      incomeTestedFee: quoteForm.incomeTestedFee ?? 0,
      dailyCDCRate: quoteForm.dailyCDCRate ?? 0,
      agreedTopUp: quoteForm.agreedTopUp ?? 0,
      balanceAtQuote: quoteForm.balanceAtQuote ?? 0,
      CLAssessedIncomeTestedFee: quoteForm.CLAssessedIncomeTestedFee ?? 0,
      basis: quoteForm.Basis ?? 0,
      feesAccepted: quoteForm.feesAccepted ?? false,
      basePension: quoteForm.basePension ?? 'SINGLE',

      dailyBasicCareFee: quoteForm.dailyBasicCareFee ?? 0,
      dailyIncomeTestedFee: quoteForm.dailyIncomeTestedFee ?? 0,
      dailyAgreedTopUp: quoteForm.dailyAgreedTopUp ?? 0,
      hardshipSupplement: quoteForm.hardshipSupplement ?? 0,
      quoteView: quoteForm.quoteView,

      personId: this.user.id,
      quoteLines: qteLineArr,
      goals: goals,
      user: this.loggedInUser.user,
      template: quoteForm.template,
      type: quoteForm.type,
      documentId: this.tableDocumentId,

      packageSupplements: quoteForm.packageSupplements,
      created: created,
      modified:  format(new Date(), "yyyy/MM/dd"),
      newFileName: quoteForm.newFileName,

    };

   
    this.qteHeaderDTO = qteHeader;
    this.loadingSaveQuote = true;

    // console.log(qteHeader);
    // return;
    this.update_document();
if (this.option == "update")
  this.listS.getpostquote(this.qteHeaderDTO).pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
    this.globalS.sToast("Success", "Quote has been Updated");
    this.loadingSaveQuote = false;
    this.refresh.emit(true);
    this.quotesOpen = false;
    this.detectChanges();
  });
 else if (this.option == "add")
    this.listS
      .checkpostquote(this.qteHeaderDTO)
      .pipe(
        switchMap((x) => {
          if (x == null) return this.listS.getpostquote(this.qteHeaderDTO);
          else {
            this.newFileName = x ??  this.qteHeaderDTO.template;
            this.qteHeaderDTO.newFileName = x;
           
             this.loadingSaveQuote = false;
            this.newFileNameOpen = true;           
            this.detectChanges();
            return EMPTY;
           // return this.listS.getpostquote(this.qteHeaderDTO);
            
          }
        })
      )
      .subscribe(
        (data) => {
          if (this.option == "update")
            this.globalS.sToast("Success", "Quote Updated");
          else
            this.globalS.sToast("Success", "Quote Added");

         // this.globalS.bToast("File Location", data.documentFileFolder);
          this.loadingSaveQuote = false;
          this.refresh.emit(true);
          this.detectChanges();
          this.newFileNameOpen = false;
          this.quotesOpen = false;
        },
        (err: any) => {
          this.loadingSaveQuote = false;
          // this.quotesOpen = false;
          this.detectChanges();
          this.globalS.eToast("Error", err.error);
          
          this.newFileName =  this.qteHeaderDTO.template;
          this.qteHeaderDTO.newFileName = this.qteHeaderDTO.template;;
         this.newFileNameOpen = true;
        
        }
      );

    // this.listS.getpostquote(qteHeader)
    //     .subscribe(data => {
    //         this.globalS.sToast('Success','Quote Added');

    //         this.globalS.bToast('File Location', data.documentFileFolder);
    //         this.loadingSaveQuote = false;
    //         this.refresh.emit(true);
    //         this.detectChanges();

    //         this.quotesOpen = false;
    //     }, (err: any) => {
    //         this.loadingSaveQuote = false;
    //         this.detectChanges();
    //         this.quotesOpen = false;
    //     });
  }

  

  slotsChange(data: any) {
    // This would stop process at the bottom on first load of QuoteLineModal
    if (this.firstLoadQuoteLine) {
      this.firstLoadQuoteLine = false;
      //   return;
    }

    this.quoteListForm.patchValue({
      quantity: data.quantity > 0 ? data.quantity : 1,
      rosterString: data.roster,
    });
  }

  addNewQuoteLine: boolean = false;

  quoteLineModal() {

    if (this.quoteForm.get("type").value == null) {

      this.globalS.eToast("Error", "Please select a quote plan type");
      return;

    }
    if (this.quoteForm.get("period").value == null) {

      this.globalS.eToast("Error", "Please select a period of quote plan");
      return;

    }
    

    this.addNewQuoteLine = true;

    this.quoteLineOpen = true;
    this.firstLoadQuoteLine = true;
    this.quoteListForm.reset();

    this.quoteListForm.patchValue({
      sortOrder: this.quoteLines_display.length + 1,
    });
  }

  printQuoteLines() {
    let qteLineArr: Array<QuoteLineDTO> = [];
    let goals: Array<any> = [];
    let qteHeader: any;// QuoteHeaderDTO;

    const quoteForm = this.quoteForm.getRawValue();

    this.goalsAndStratergies.forEach((e) => {
      goals.push(e.goal);
    });

    this.quoteLines.forEach((x) => {
      let da: QuoteLineDTO = {
        sortOrder: 0,
        billUnit: x.billUnit,
        itemId: x.itemId,
        qty: x.quantity,
        displayText: x.displayText,

        unitBillRate: x.price,
        frequency: x.frequency,
        lengthInWeeks: x.quoteQty,
        roster: x.rosterString,
        serviceType: x.code,
        strategyId: x.strategy,
      };

      qteLineArr.push(da);
    });

    qteHeader = {
      recordNumber: this.tempIds ? this.tempIds.quoteHeaderId : this.record.recordNumber,
      programId: quoteForm.programId,
      program: quoteForm.program,
      clientId: this.clientId,
      quoteLines: qteLineArr,
      daysCalc: 365,
      budget: 51808.1,
      quoteBase: "ANNUALLY",
      govtContribution: 51808.1,
      packageSupplements: "00000000000000000000000000",
      agreedTopUp: "0.00",
      balanceAtQuote: "0.00",
      CLAssessedIncomeTestedFee: "0.00",

      feesAccepted: false,
      basePension: "SINGLE",
      dailyBasicCareFee: "$0.00",
      dailyIncomeTestedFee: "$0.00",
      dailyAgreedTopUp: "$0.00",

      quoteView: "ANNUALLY",

      personId: this.user.id,
      user: this.loggedInUser.user,
      template: quoteForm.template,
      type: quoteForm.type,
      documentId: this.tableDocumentId,
      goals: goals,
    };

    this.listS.postprintline(qteHeader).subscribe((blob) => {
      let data = window.URL.createObjectURL(blob);
      console.log(data);

      let link = document.createElement("a");
      link.href = data;
      link.download = "ota.docx";
      link.click();

      setTimeout(() => {
        window.URL.revokeObjectURL(data);
      }, 100);
    });
    this.generatePdf(99, this.cpid);
    this.pdfTitle = "";
    this.tryDoctype = "";
  }

  populateDropdDowns() {
    this.expecteOutcome = expectedOutcome;

    let notSpecified = ["VARIOUS"];

    this.listS.getquotetype().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
      this.typeList = data;

      this.typeList = ['QUOTE/BUDGET', ...this.typeList];
      if (data && data.length == 1) {
        this.quoteForm.patchValue({
          type: data[0],
        });
      }
    });

    this.listS.getdiscipline().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
      data.push("VARIOUS");
      this.disciplineList = data;
    });
    this.listS.getcaredomain().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
      data.push("VARIOUS");
      this.careDomainList = data;
    });
    this.listS.getndiaprograms().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
      data.push("VARIOUS");
      this.programList = data;
    });
    this.listS.getcareplan().pipe(takeUntil(this.unsubscribe)).subscribe((data) => {
      this.quotePlanType = data;
    });

    this.listS.getplangoalachivement().pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => (this.plangoalachivementlis = data));

    this.listS.getprogramcontingency(this.user.id).pipe(takeUntil(this.unsubscribe))
      .subscribe((data) => {
        this.quoteProgramList = data;

        if (data && data.length == 1) {
          this.quoteForm.patchValue({
            program: data[0],
          });
          //this.detectChanges();
        }
      });

      this.listS.getfundingAria().pipe(takeUntil(this.unsubscribe)).subscribe(data => this.fundingAriaList= data)
           
    //
  }

  search(user: any): Promise<any> {
    this.loading = true;
    this.user = user;

    let data = {
      quote: {
        PersonID: user.id,
        DisplayLast: this.displayLast,
        IncludeArchived: this.archivedDocs,
        IncludeAccepted: this.acceptedQuotes,
      },
      filters: this.filters,
    };

    // this.listS.getlistquotes(data).subscribe(data => {
    //     this.tableData = data;
    //     this.loading = false;
    //     this.cd.markForCheck();
    // })

    if (this.option == "add") {

      this.timeS.getCarePlanID().subscribe((data) => {
        this.carePlanID = data.itemId;
        this.BudgetNo = data.quoteNo;
        this.formatBudgetNo();
        this.quoteGeneralForm.patchValue({
          id: this.carePlanID,
          planType: "SUPPORT PLAN",
          starDate: this.date,
          signOfDate: this.date,
          reviewDate: this.date,
          publishToApp: false,
        });

        this.cd.markForCheck();
      });
    }

    if (this.option == "update" && this.record) {
      return this.listS.getquotedetails(this.record.recordNumber).toPromise();
      //   this.listS.getquotedetails(this.record).subscribe(data => {
      //       console.log(data);
      //     this.quoteDetails = data;
      //     this.cpid = data.cpid;
      //     this.fdata = data.quoteLines;

      //     this.tableDocumentId = data.cpid;

      //     this.incomeTestedFee = parseFloat(data.dailyIncomeTestedFee.replace(/\$|,/g, ''));
      //     this.calculateIncomeTestedFee();

      //     this.quoteForm.patchValue({
      //         recordNumber: data.recordNumber,
      //         program: data.program,
      //         no: data.docNo
      //     });

      //     if(!this.IS_CDC){
      //         this.quoteForm.patchValue({
      //             initialBudget: data.budget
      //         });
      //     } else {
      //         this.quoteForm.patchValue({
      //             govtContrib: data.govtContribution
      //         });
      //     }

      //       this.quoteLines = data.quoteLines.length > 0 ? data.quoteLines.map(x => {
      //             this.fquotehdr = x;
      //             this.dochdr = x.docHdrId

      //             return {
      //                 code:           x.code,
      //                 displayText:    x.displayText,
      //                 quantity:       x.qty,
      //                 billUnit:       x.billUnit,
      //                 frequency:      x.frequency,
      //                 price:          x.unitBillRate,
      //                 recordNumber:   x.recordNumber,
      //                 tax:            x.tax ,
      //                 lengthInWeeks:  x.lengthInWeeks,
      //                 quoteQty:       x.quoteQty,
      //                 mainGroup:      x.mainGroup
      //             }
      //       }) : [];

      //     this.total_base_quote = (this.generate_total()).toFixed(2);
      //     this.total_admin = this.generate_total_admin();
      //     this.total_quote = (this.generate_total() + this.total_admin).toFixed(2);
      //   })
    }

    return Promise.resolve(null);
  }
  update_document(){
   
  

    let  input = this.quoteGeneralForm.value;
     var inp = {
        
         PersonID: this.user.id,
         DocID: this.tableDocumentId,
         Title: input.name,
         Program: input.program,
         Discipline:  input.discipline,
         CareDomain:  input.careDomain,
         Classification:  input.classification,
         Category:  input.planType,
         DocStartDate :  format(new Date(input.starDate),'yyyy/MM/dd')  ,
         DocEndDate :  format(new Date(input.signOfDate),'yyyy/MM/dd') ,
         ReminderDate:  format(new Date(input.reviewDate),'yyyy/MM/dd'), 
         ReminderText:  input.rememberText,
         PublishToApp:  input.publishToApp,
         User:  this.loggedInUser.user,      
         Notes:  input.notes ?? ''
 
     };
 
     this.timeS.updateDocument(inp).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      
       //  this.globalS.sToast('Success','Quote has been updated');       
       
         this.cd.detectChanges();
        
     }, (err) =>{
         
         this.globalS.eToast('Error', err.error.message);
     });
 
 
 }
  getViewFActor() {
    let i_ViewFactor;

    let basePeriod = this.quoteForm.get('quoteBase').value;
    switch (basePeriod) {
      case "DAILY":
        i_ViewFactor = 365;
        break;
      case "WEEKLY":
        i_ViewFactor = 52;
        break;
      case "FORTNIGHTLY":
        i_ViewFactor = 26;
        break;
      case "MONTHLY":
        i_ViewFactor = 12;
        break;
      case "QUARTERLY":
        i_ViewFactor = 4;
        break;
      default:
        i_ViewFactor = 1;
        break;       
    }
    return i_ViewFactor;
  }
  amount(var1: number, var2: number) {
    var product;
    product = var1 * var2;

    return product.toFixed(2);
  }
  totalamount(price: number, quoteQty: number, tax: number, quantity: number) {
    var product: number;

    

    if (tax != null && tax != 0) {
      product = price * (quoteQty/this.getViewFActor())  +   tax * (quoteQty/this.getViewFActor()) ;
    } else {
      product = price * (quoteQty/this.getViewFActor()) ;
    }

    this.globalS.baseamount = product;
    return product;
  }
  fbasequote() {
    //let temp = this.fquotehdr
    let temp = this.fdata;
    var test: number;

    //    let price,quantity,length;
    //    console.log(temp)

    if (this.option == "update") {
      for (let i = 0; i < temp.length; i++) {
        //    console.log(this.quoteLines[i].lengthInWeeks)
        //    console.log(this.quoteLines[i].quantity)
        //    console.log(this.quoteLines[i].price)
        //if(stype == "ADMINISTRATION"){ }
        this.price = temp[i].unitBillRate;
        this.quantity = temp[i].qty;
        this.length = temp[i].quoteQty;
        //console.log(price)

        test = this.price * this.quantity * this.length;
        //       this.globalS.baseamount  = this.globalS.baseamount + test
        this.globalS.baseamount += +test;
        //       console.log(temp1)
      }
    }

    // console.log(this.quoteLines)

    return this.globalS.baseamount.toFixed(2);
  }
  domenticaChange(event: any) {
    if (event.target.checked) {
      this.supplements.patchValue({
        levelSupplement: this.programLevel,
      });
    } else {
      this.supplements.patchValue({
        levelSupplement: "",
      });
    }
  }
  //  govtContribution
  govtcontribute() {
    var temp: number;
    temp = this.quoteForm.value.govtContrib;
    //    temp =  51808.10;
    return temp.toFixed(2);
  }

  remainingfund() {
    var temp: Number;
    temp =
      this.quoteForm.value.govtContrib -
      this.admincharges -
      this.globalS.baseamount;
    return temp.toFixed(2);
  }

  current_quote_value: number;
  current_line_value: number;


  calculatequotePerc() {
    this.zone.runOutsideAngular(() => {
      var sss = this.quoteListForm.value;
      console.log(sss);
      var temp: number;
      temp = this.admincharges + this.globalS.baseamount;
      // return temp.toFixed(2)
     // this.current_quote_value = temp;
    });
  }

  admincharge() {
    var id, stype;

    id = this.cpid;
    // id  = '46010'  ;
    if (!id) return;
    this.listS.GetQuotetype(id).subscribe((x) => {
      this.specindex = x.indexOf("ADMINISTRATION");
      //console.log(stype)
    });

    //   if(stype == "ADMINISTRATION" ){

    let temp = this.fdata;
    console.log(temp);
    var temp1: number;
    var test: number;

    //    let price,quantity,length;
    //    console.log(this.specindex)
    if (this.option == "update") {
      console.log(temp.length);
      let j = this.specindex;
      this.admincharges = 0;

      //   for(let i =0;i < temp.length ,i =j;i++){

      //this.price = temp[j].unitBillRate
      this.price = temp[j].unitBillRate;
      this.quantity = temp[j].qty;
      this.length = temp[j].quoteQty;
      //    console.log((temp[i].quantity).toString());
      console.log(this.quantity);
      test = this.price * this.quantity * this.length;
      this.admincharges = this.admincharges + test;
      //   this.admincharges =  this.admincharges + this.admincharges

      //}

      console.log(this.admincharges);
    }

    return this.admincharges;
  }

  dailyliving() {
    let daily;
    var temp = this.dochdr;
    if (!temp) return;

    this.listS.GetDailyliving(temp).subscribe((x) => {
      daily = x;
    });

    return daily;
  }

  calculateTotal() {
    this.total_admin = this.generate_total_admin();
    this.total_quote = (this.generate_total() + this.total_admin).toFixed(2);
    this.total_base_quote = (this.total_quote - this.total_admin).toFixed(2);
    this.globalS.baseamount = this.total_base_quote;
  }

  checkValue(event) {
    if (event.target.checked) {
      this.CHECKACCEPTCHARGES();
    } else {
      this.quoteLines = [
        ...this.quoteLines.filter((x) => x.mainGroup !== "ADMIN-DEFAULT"),
      ];

      this.calculateTotal();

      this.handleCancelLine();
      this.detectChanges();
    }
  }

  calculateIncomeTestedFee() {
    this.dailyIncomeTestedFee = this.incomeTestedFee;
    this.annualIncomeTestedFee = this.dailyIncomeTestedFee * 365;
    this.monthlyIncomeTestedFee = (this.dailyIncomeTestedFee * 365) / 12;

    this.incomeTestedFeeTotal = this.incomeTestedFee * 365;

    this.quoteForm.patchValue({
      govtContrib: this._tempGovtContribution - this.annualIncomeTestedFee,
    });

    this.calculate_HCP_Admin();
    this.calculate_CM_Admin();
  }

  calculateBasicCareFee() {
    // if(this.dailyBasicCareFee && this.dailyBasicCareFee > -1){
    this.annualBasicCareFee = this.dailyBasicCareFee * 365;
    this.monthlyBasicCareFee = (this.dailyBasicCareFee * 365) / 12;

    this.calculate_HCP_Admin();
    this.calculate_CM_Admin();
    // }
  }

  selectCareplan(data) {
    this.activeRowData = data;

  }

  selectQuoteLine(data, i: number) {
    this.activeRowData = data;
    this.activeRowIndex = i;
  }

  print() {
    this.printQuoteLines();
  }

  txtSearch: string;

  onTextChangeEvent(event: any, listTye: any) {
    // console.log(this.txtSearch);
    let value = this.txtSearch.toUpperCase();
    if (listTye == 'Goals') {
      this.goalOfCarelist = this.originalgoalOfCarelist.filter(element => element.label.includes(value));
    }
  }

  NeedsRisksForm: FormGroup;
  showRows: Array<boolean> = [false, false, false, false, false, false, false, false, false, false, false, false];
  Genders: Array<any> = Quotesdropdowns.Genders;
  Identities: Array<any> = [];
  MaritalStatuses: Array<any> = [];
  Religions: Array<any> = [];
  borns: Array<any> = Quotesdropdowns.borns;
  arrivals: Array<number> = []


}

