using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Adamas.Models.Modules
{
    public class FileProperties
    {
        public string Directory { get; set; }
    }

}