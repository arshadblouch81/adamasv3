﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class MTAPendingDto
    {
        public int JobNo { get; set; }
        public string Staff { get; set; }
        public string Recipient { get; set; }
        public string ServiceType { get; set; }
        public string RosterStart { get; set; }
        public double Duration { get; set; }
        public string RosterEnd { get; set; }
        public string S1 { get; set; }
        public string S2 { get; set; }
        public int StartVar { get; set; }
        public string PanlateStartTh { get; set; }
        public bool? TaMultishift { get; set; }
    }
}
