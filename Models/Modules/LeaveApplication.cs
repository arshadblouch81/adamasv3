using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class LeaveApplication
    {
        public int? RecordNo { get; set; }
        public string PersonId { get; set; }
        public string Service { get; set; }
        public string Program { get; set; }
        public string Type { get; set; }
        public string PerpSpecify { get; set; }
        public string Location { get; set; }
        public string Setting { get; set; }
        public string LocationNotes { get; set; }
        public DateTime? DateReported { get; set; } 
        public DateTime? Date { get; set; }
        public DateTime? Time { get; set; }
        public int Duration { get; set; }
        public string ReportedBy { get; set; }
        public string Severity { get; set; }
        public string CurrentAssignee { get; set; }
        public string ShortDesc { get; set; }
        public string FullDesc { get; set; }
        public string TriggerShort { get; set; }
        public string Triggers { get; set; }
        public string InitialNotes { get; set; }
        public string OngoingNotes { get; set; }
        public List<StaffInput> Staffs { get; set; }
    }


    public class StaffInput {
        public int? RecordNo { get; set; } 
        public string AccountNo { get; set; }
        public bool? Selected { get; set ;}
    }
}