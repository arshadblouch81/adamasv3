import { Component, OnInit, OnDestroy, Input, AfterViewInit,ChangeDetectorRef,} from '@angular/core'
import { Router,ActivatedRoute } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService,NavigationService,TimeSheetService, MenuService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';

//Sets defaults of Criteria Model

const inputFormDefault = {
    
    AgingDays: ['30'],
    DisplayArr : ['Display Invoices to be Both Printed OR Emailed'],
    PrintFormatArr : ['Default'],
    recip: ['ALL'],
    debtor: ['ALL'],
    radioFilter: ['NoFilter'],
    batchNo: ['ALL'],
    InvoiceNo : ['ALL'],
    ReciptNo : ['ALL'],
    transType :[''],
    tableData : [[]],
    
    Rptformat : ['Default'],
    displayby : ['DISPLAY BY STAFF CODE'],
    
    
    filterArr : ['Invoiced Only'],
    
   
    AgingCycles: [30],
    
    txtterm1 : [''],
    txtterm2 : [''],
    ContributioLine : [''],
    bankdetails: [''],
    FormCloseid :[''],
    
    frm_lblinvoiceno :false,
    frm_lblreciptno : false,
    frm_formats : false,
    frm_display : false,
    frm_displayAcc: false,
    frm_tbldata : false,
    frm_invoiceNumber :false,   
    frm_ReciptNumber: false,
    chkPrintZeroInvoices : [true],
    chkPrintZeroLines : [true],
    chkSeperateInvoices : [true],
    chkSeperatePages : [true],
    chkUsePreferredName : [true],
    chkIncludeStaffCode : [true],
    chkIncTimes : [true],
    chkInclAdditionalInfo: [true], 
    chkHideAddressBox : [true],
    
    
    
}

@Component({
    // host: {
    //     '[style.display]': 'flex',
    //     '[style.flex-direction]': 'column',
    //     '[style.overflow]': 'auto'
    //   },
    styles: [`
    
    button{
        width: 220pt !important;
        text-align: left !important;
        text-transform: uppercase;
    }        
    .btn{
        border:none;
        cursor:pointer;
        outline:none;
        transition: background-color 0.5s ease;
        padding: 5px 7px;
        border-radius: 3px;
        text-align: center !important;
        width: 100px !important;
    }
    .btn-rounded{
        border:none;
        cursor:pointer;
        outline:none;
        transition: background-color 0.5s ease;
        padding: 5px 7px;
        border-radius: 7px !important;
        text-align: center !important;
        width: 100px !important;
    }
    button.success{
        background-color: #85B9D5 !important;
        color: #fff !important;
        border-color: #85B9D5 !important;
    }
    label{
        font-weight: bold; 
    }
    
    .form-group label{
        font-weight: bold;
    }
    nz-select{
        width:100%;
        
    }
    label.checks{
        margin-top: 4px;
        font-weight: 300 !important;
    }
    nz-date-picker{
        margin:5pt;
    }
    .frm_number{
        width:250px;
        height:32px;
        
    }
    .frm_AgingNumber{
        width:50px;
        height:32px;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    .custom-btn{
        background:#f7f7f7 !important;
    }
    .ant-menu-horizontal {
        line-height: 32px;
        height:auto !important;
        border-bottom:0px !important;
    }
    .ant-menu-horizontal>.ant-menu-item-selected, .ant-menu-horizontal>.ant-menu-item:hover{
         border-bottom:0px !important;
    }
    .conf-header{
        background-color: #85B9D5;
        color: #fff;
        padding: 10px;
        border-color: #85B9D5;
    }
    .conf-header-darkorange{
        background-color: #f18805;
        color: #fff;
        padding: 10px;
        border-color: #f18805;
    }
    .conf-header-orange{
        background-color: #ffba08;
        color: #fff;
        padding: 10px;
        border-color: #ffba08;
    }
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
        line-height: 24px;
        background: #fff;
        height: 25px;
        margin: 0 0px 0 0;
        text-align: center;
        border-color:#85B9D5;
    }
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
        background: #85B9D5;
        color: #fff;        
    }
    .ant-tabs.ant-tabs-card .ant-tabs-card-bar .ant-tabs-tab {
        border-radius: 0px;
    }
    nz-container ::ngdeep  nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
        
        min-width: auto;
    }
    nz-tabset ::ng-deep .ant-tabs-bar {
        border-bottom: 0px;
    }
    .SingleInputNo{
        width:50px;
        height:32px;
    }
    .ant-btn{
        height:auto;
        border-radius: 0px;
    }
    .btn-darkorange{
        line-height: 1;
        background-color: #f18805;
        color: #fff;        
        border-color: #f18805;        
        cursor:pointer;
        outline:none;
        transition: background-color 0.5s ease;
        padding: 5px 7px;
        border-radius: 3px;
        text-align: center !important;
        width: 150px !important;
    }                 
    .container {
        display: flex;
        justify-content: center;
        align-items: center;
        min-height: 94vh;
        max-height: 94vh;
    }
    .centered-div {
        height: 60vh;
        border: 2px solid #85B9D5;
    }
        .center-menu{
            height: 90vh;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .main-box {
            display: flex;
            flex-direction: row;        
            height: 65vh;
            width:  55vw;
            border: 2px solid #85B9D5;
            border-radius: 5px;
            background-color: white;  
            padding : 25px 10px; 
            flex-wrap: wrap !important;
            margin: auto;
            column-gap: 3vw;
            column-fill: balance;
        }
    
    .tab-title {
        
    font-family: Segoe UI;
        display: inline-block;
        position: relative;
        align-items: left;
        height: 25px;
        padding-left: 10px;
        padding-right: 10px; /* Adjust padding for better text alignment */
        font-size: 14px;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        font-weight: bold;
        background-color: #fff; /* Background color for tabs */
        border: 1px solid #d0d2d6;
        border-bottom: none; /* Remove bottom border for a connected look */
        border-top-left-radius: 15px;
        border-top-right-radius: 15px; /* Add right border-radius for rounded corners */
        text-wrap: nowrap;
        cursor: pointer; /* Change cursor to pointer for better UX */
        margin-right: -1px; /* Overlap right border for seamless tabs */
        z-index: 1; /* Ensure active tab is above the content area */   
    }

    .fade-in-text {
        font-family: Segoe UI;
        font-size: 14px;
        color: #85B9D5;
        animation: fadeIn 1.5s infinite alternate;
    }
    
    @keyframes fadeIn {
        0% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
        @media  (min-width:300px) and (max-width:700px)  {

    .main {
       
        /* padding : 14px;          */
        margin:4px;
    }

        .main-box{
          
           flex-direction: column;    
            height: 90vh;
            padding: 5px !important;
            align-items: center !important;;
           
        }
        .button {
            width: 60vw !important;
            font-size:  12px !important;
            margin-top: 2px !important;
           
        }

        
    .header-powder-blue {
    
        width: 60vw !important;
        font-size: 12px !important;;
    
    }

    .header-dark-orange {
    
        width: 60vw !important;
        font-size:  12px !important;
    

    }
   
    #block1,  #block2,  #block3{
        margin-top: 2px !important;
    }
   

    #ps1{
        display: none !important;
    }
    #block0{
       margin-left: 22px !important; 
    
    } 

    .tab{
        height: 90vh;
    }
    :host ::ng-deep nz-tab {
        color: red;
    }
    }
    `],
    templateUrl: './configuration.html'
})
export class ConfigurationMain implements OnInit, OnDestroy, AfterViewInit{
    
    // drawerVisible: boolean = false;
    
    bodystyle:object;
    tocken :any;
    frm_formats : boolean;
    frm_display : boolean;
    frm_displayAcc : boolean;
    frm_tbldata : boolean;
    frm_invoiceNumber : boolean;  
    frm_ReciptNumber :boolean;
    frm_Outlets: boolean;
    tabset = false;
    isVisibleTop =false;
    isVisibleAcc = false;
    frm_lblinvoiceno : boolean;
    frm_lblreciptno :boolean;
    radioFilter: ['NoFilter'];
    Single_input_integer = false; 
    isVisible = false; 
    showtabother = false;
    showtabRostcriteria= false;
    showtabstaffcriteria= false;
    showtabRegcriteria= false;
    showtabrecpcriteria = false;
    show =false ;
    showoption = true;
    tryDoctype: any;
    drawerVisible:boolean;

    showAwardList: any; //AHSAN
        
    //MUFEED's START
    id: string;
    btnid: string;
    pdfTitle: string;
    inputForm: FormGroup;
    ModalName: string;
    Modal : string;
    single_input_number: number;
    txtInvoiceCopies :number;
    ModalTitle : string;
    AccModelTitle :string;
    subTabModelAcc : string;
    AgingCycles : Number;
    txtterm1 : string;
    txtterm2 : string;
    ContributioLine : string;
    bankdetails: string;
    transType : string;
    tableData: Array<any> = [] ;
    
    dateFormat: string = 'dd/MM/yyyy';    
    enddate: Date;
    startdate: Date;
    
    AgingDays: string;
    agedate : Date;
    DisplayArr : Array<any> = [];
    PrintFormatArr : Array<any> = [];
    recip : string;
    debtor : string;
    batchNo : string;
    InvoiceNo : string;
    ReciptNo : string;
    
    
    Rptformat : Array<any> = [];
    displayby : Array<any> = [];
    
    outletsArr: Array<any> = [];
    branchesArr: Array<any> = [];    
    managersArr: Array<any> = [];
    recipientArr: Array<any> = [];
    serviceRegionsArr: Array<any> = [];
    programsArr: Array<any> = [];
    AccountsArr : Array<any> = [];
    vehiclesArr : Array<any> = [];    
    PackagesArr : Array<any> = [];
    BatchNoArr : Array<any> = [];
    MealGroups : Array<any> = [];
    staffteamArr : Array<any> = [];
    staffgroupsArr: Array<any> = [];
    staffArr: Array<any> = [];
    batchclientsArr : Array<any> = [];
    RptFiltersArr : Array<any> = [];
    
    
    
    filterArr: Array<any> = [];    
    
    
    selection : boolean;
    options : boolean;    
    s_AccountsSQL: string;
    FormCloseid : string;
    

    
    loading: boolean = false;
    navigationExtras:any;
    chkPrintZeroInvoices : boolean;
    chkPrintZeroLines : boolean;
    chkSeperateInvoices : boolean;
    chkSeperatePages : boolean;
    chkUsePreferredName : boolean;
    chkIncludeStaffCode : boolean;
    chkIncTimes : boolean;
    chkInclAdditionalInfo : boolean;
    chkHideAddressBox : boolean;    
    rights: any;
    fulladmin: any = true;
    licenseRegistrationOpen: boolean = false;
    systemNumbersOpen: boolean = false;
    licenseRegForm: FormGroup;
    systemNumbersForm:FormGroup;
    registrationDetail: any;
    loggedOnAppUserUrl: any;
    tabFindIndex: number = 0;
    sysnumberDetail: any;
    //MUFEED's END
    // MUFEED ADDED fb FormBuilder,modalService


    

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private formBuilder: FormBuilder,
        private sanitizer: DomSanitizer,
        private modalService: NzModalService,
        private listS: ListService,
        private menuS: MenuService,
        private timeS:TimeSheetService,
        private printS: PrintService,
        private cd: ChangeDetectorRef,
        private GlobalS:GlobalService,
        private route: ActivatedRoute,
        private navigationService: NavigationService
        )
        {
            
        }
        handleCancelTop(): void {
            this.isVisibleTop = false;          
            this.drawerVisible = false;
                  }
        handleCancel(){
            this.drawerVisible = false;
            this.pdfTitle = "";
            this.tryDoctype = ""           
        }
        LisenceRegistrationhandleCancel(){
            this.licenseRegistrationOpen = false;
            this.licenseRegForm.reset();
        }
        systemNumberHandleCancel(){
            this.systemNumbersOpen = false;
            this.systemNumbersForm.reset();
        }
        onChange(result: Date): void {
            console.log('onChange: ', result);
        }
        buildForm() {
            this.licenseRegForm = this.formBuilder.group({
                adamas1: '',
                adamas2: '',
                adamas3: '',
                adamas4: '',
                adamas5: '',
                adamas6: '',
                adamas7: '',
                adamas8: '',
                adamas9: '',
                sqlId:1,
                ndiaTravel:1,
            });  
            this.systemNumbersForm = this.formBuilder.group({
                sqlid:0,
                batch :'',
                invoice:'',
                tsheetID:'',
                customerID:'',
                carerID:'',
            })
        }  
        saveSystemNumber(){

        }
        saveRegistration(index: number){
            
            if (index == 1) {
                this.licenseRegForm.controls["ndiaTravel"].setValue((1));
                
                this.menuS.updatelicenseRegistration(this.licenseRegForm.value)
                .subscribe(data => {
                    
                    this.GlobalS.sToast('success','Updated Successfuly');
                    this.licenseRegForm.reset;
                    this.handleCancel();
                });
                
            }
            if (index == 2) {
                this.licenseRegForm.controls["ndiaTravel"].setValue((2));
                this.menuS.updatelicenseRegistration(this.licenseRegForm.value)
                .subscribe(data => {
                    this.GlobalS.sToast('success','Updated Successfuly');
                    this.licenseRegForm.reset;
                    this.handleCancel();
                    
                });
            }
            if (index == 3) {
                this.licenseRegForm.controls["ndiaTravel"].setValue((3));
                this.menuS.updatelicenseRegistration(this.licenseRegForm.value)
                .subscribe(data => {
                    this.GlobalS.sToast('success','Updated Successfuly');
                    this.licenseRegForm.reset;
                    this.handleCancel();
                });
            }
        }
        
        view(index: number) {
            if (index == 0) {
                this.router.navigate(['/admin/configuration/branches'])
            }
            if (index == 1) {
                this.router.navigate(['/admin/configuration/funding-region'])
            }
            if(index == 2){
                this.router.navigate(['/admin/configuration/claim-rates'])
            }
            if(index == 3){
                this.router.navigate(['/admin/configuration/target-groups'])
            }
            if(index == 4 ){
                this.router.navigate(['/admin/configuration/purpose-statement']);
            }
            if(index == 5){
                this.router.navigate(['/admin/configuration/budget-groups']);
            }
            if(index == 6){
                this.router.navigate(['/admin/configuration/budgets']);
            }
            if(index == 7){
                this.router.navigate(['/admin/configuration/user-detail']);
            }
            if(index == 8){
                this.router.navigate(['/admin/configuration/contact-groups']);
            }
            if(index == 9){
                this.router.navigate(['/admin/configuration/contact-types']);
            }
            if(index == 10){
                this.router.navigate(['/admin/configuration/address-types']);
            }
            if(index == 11){
                this.router.navigate(['/admin/configuration/phone-email-types']);
            }
            if(index == 12){
                this.router.navigate(['/admin/configuration/occupations']);
            }
            if(index == 13){
                this.router.navigate(['/admin/configuration/religions']);
            }
            if(index == 14){
                this.router.navigate(['/admin/configuration/financial-class']);
            }
            if(index == 15){
                this.router.navigate(['/admin/configuration/postcodes']);
            }
            if(index == 16){
                this.router.navigate(['/admin/configuration/holidays']);
            }
            if(index == 17){
                this.router.navigate(['/admin/configuration/medical-contact']);
            }
            if(index == 18){
                this.router.navigate(['/admin/configuration/destination-address']);
            }
            if(index == 19){
                this.router.navigate(['/admin/configuration/program-coordinates']);
            }
            if(index == 20){
                this.router.navigate(['/admin/configuration/distribution-list']);
            }
            if(index == 91){
                this.router.navigate(['/admin/configuration/notification-list']);
            }
            if(index == 92){
                this.router.navigate(['/admin/configuration/customdataset']);
            }
            if(index == 93){
                this.router.navigate(['/admin/configuration/checklist']);
            }
            if(index == 21){
                this.router.navigate(['/admin/configuration/initial-actions']);
            }
            if(index == 22){
                this.router.navigate(['/admin/configuration/ongoing-actions']);
            }
            if(index == 23){
                this.router.navigate(['/admin/configuration/incident-sub-category']);
            }
            if(index == 24){
                this.router.navigate(['/admin/configuration/incident-trigger']);
            }
            if(index == 25){
                this.router.navigate(['/admin/configuration/incident-types']);
            }
            if(index == 26){
                this.router.navigate(['/admin/configuration/incident-location-categories']);
            }
            if(index == 27){
                this.router.navigate(['/admin/configuration/recipient-incident-note-category']);
            }
            if(index == 28){
                this.router.navigate(['/admin/configuration/staff-incident-note-category']);
            }
            if(index == 29){
                this.router.navigate(['/admin/configuration/filling-classification']);
            }
            if(index == 30){
                this.router.navigate(['/admin/configuration/document-categories']);
            }
            if(index == 31){
                this.router.navigate(['/admin/configuration/document-template']);
            }
            if(index == 32){
                this.router.navigate(['/admin/configuration/recipients-categories']);
            }
            if(index == 33){
                this.router.navigate(['/admin/configuration/recipients-groups']);
            }
            if(index == 34){
                this.router.navigate(['/admin/configuration/recipients-minor-group']);
            }
            if(index == 35){
                this.router.navigate(['/admin/configuration/recipients-billing-cycles']);
            }
            if(index == 36){
                this.router.navigate(['/admin/configuration/debtor-terms']);
            }
            if(index == 37){
                this.router.navigate(['/admin/configuration/recipient-goals']);
            }
            if(index == 38){
                this.router.navigate(['/admin/configuration/consent-types']);
            }
            if(index == 39){
                this.router.navigate(['/admin/configuration/care-plan-types']);
            }
            if(index == 40){
                this.router.navigate(['/admin/configuration/clicnical-notes-groups']);
            }
            if(index == 41){
                this.router.navigate(['/admin/configuration/case-notes-categories']);
            }
            if(index == 42){
                this.router.navigate(['/admin/configuration/op-notes-categories']);
            }
            if(index == 43){
                this.router.navigate(['/admin/configuration/care-domains']);
            }
            if(index == 44){
                this.router.navigate(['/admin/configuration/discharge-reasons']);
            }
            if(index == 45){
                this.router.navigate(['/admin/configuration/refferal-reasons'])
            }
            if(index == 46){
                this.router.navigate(['/admin/configuration/user-define-reminders'])
            }
            if(index == 47){
                this.router.navigate(['/admin/configuration/recipient-prefrences'])
            }
            if(index == 48){
                this.router.navigate(['/admin/configuration/mobility-codes'])
            }
            if(index == 49){
                this.router.navigate(['/admin/configuration/tasks'])
            }
            if(index == 50){
                this.router.navigate(['/admin/configuration/health-conditions'])
            }
            if(index == 51){
                this.router.navigate(['/admin/configuration/medications'])
            }
            if(index == 52){
                this.router.navigate(['/admin/configuration/nursing-dignosis'])
            }
            if(index == 53){
                this.router.navigate(['/admin/configuration/medical-dignosis'])
            }
            if(index == 54){
                this.router.navigate(['/admin/configuration/medical-procedures'])
            }
            if(index == 55){
                this.router.navigate(['/admin/configuration/clinical-reminders'])
            }
            if(index == 56){
                this.router.navigate(['/admin/configuration/clinical-alerts'])
            }
            if(index == 57){
                this.router.navigate(['/admin/configuration/admitting-priorities']);
            }
            if(index == 58){
                this.router.navigate(['/admin/configuration/service-note-categories']);
            }
            if(index == 59){
                this.router.navigate(['/admin/configuration/referral-sources']);
            }
            if(index == 60){
                this.router.navigate(['/admin/configuration/lifecycle-events']);
            }
            if(index == 61){
                this.router.navigate(['/admin/configuration/job-category']);
            }
            if(index == 62){
                this.router.navigate(['/admin/configuration/admin/configuration-category']);
            }
            if(index == 63){
                this.router.navigate(['/admin/configuration/user-groups']);
            }
            if(index == 64){
                this.router.navigate(['/admin/configuration/staff-positions']);
            }
            if(index == 65){
                this.router.navigate(['/admin/configuration/staff-teams']);
            }
            if(index == 66){
                this.router.navigate(['/admin/configuration/award-levels']);
            }
            if(index == 67){
                this.router.navigate(['/admin/configuration/award-details']);
            }
            if(index == 68){
                this.router.navigate(['/admin/configuration/competency-groups']);
            }
            if(index == 69){
                this.router.navigate(['/admin/configuration/staff-competency']);
            }
            if(index == 70){
                this.router.navigate(['/admin/configuration/hr-notes-categories']);
            }
            if(index == 71){
                this.router.navigate(['/admin/configuration/op-staff-notes']);
            }
            if(index == 72){
                this.router.navigate(['/admin/configuration/staff-reminder']);
            }
            if(index == 73){
                this.router.navigate(['/admin/configuration/service-deciplines']);
            }
            if(index == 74){
                this.router.navigate(['/admin/configuration/leave-description']);
            }
            if(index == 75){
                this.router.navigate(['/admin/configuration/staff-preferences']);
            }
            if(index == 76){
                this.router.navigate(['/admin/configuration/service-note-categories']);
            }
            if(index == 77){
                this.router.navigate(['/admin/configuration/vehicles']);
            }
            if(index == 78){
                this.router.navigate(['/admin/configuration/activity-groups']);
            }
            if(index == 79){
                this.router.navigate(['/admin/configuration/equipments']);
            }
            if(index == 80){
                this.router.navigate(['/admin/configuration/center-facility-location']);
            }
            if(index == 81){
                this.router.navigate(['/admin/configuration/funding-sources']);
            }
            if(index == 82){
                this.router.navigate(['/admin/configuration/pay-types']);
            }
            if(index == 83){
                this.router.navigate(['/admin/configuration/program-packages']);
            }
            if(index == 84){
                this.router.navigate(['/admin/configuration/services']);
            }
            if(index == 85){
                this.router.navigate(['/admin/configuration/items-consumables']);
            }
            if(index == 86){
                this.router.navigate(['/admin/configuration/menu-meals']);
            }
            if(index == 87){
                this.router.navigate(['/admin/configuration/case-management-admin']);
            }
            if(index == 88){
                this.router.navigate(['/admin/configuration/staff-admin-activities']);
            }
            if(index == 89){
                this.router.navigate(['/admin/configuration/recipient-absences']);
            }
            if(index == 90){
                this.router.navigate(['/admin/configuration/companies']);
            }
            if(index == 94){
                this.licenseRegistrationOpen = true;
            }
            if(index == 95){
                this.router.navigate(['/admin/configuration/desktopusers']);
            }
            if(index == 96){
                this.menuS.loggedOnAppUser().subscribe(data => {
                    this.loggedOnAppUserUrl = data[0];
                    var mailurl = this.loggedOnAppUserUrl.mobilUserURL;
                    window.open(`${mailurl}`,'_blank');
                });
            }
            if(index == 97){
                this.systemNumbersForm.patchValue(this.sysnumberDetail);
                this.systemNumbersOpen = true;
            }
            if(index == 98){
                this.router.navigate(['/admin/configuration/portalusers']);
            }
            if(index == 101){ //AHSAN
                this.router.navigate(['/admin/configuration/award-setup']);
            }
            if(index == 102){
                this.router.navigate(['/admin/configuration/other-contact-addresses']);
            }
        }
                        
        ngOnInit(): void {
            
            this.tocken = this.GlobalS.pickedMember ? this.GlobalS.GETPICKEDMEMBERDATA(this.GlobalS.GETPICKEDMEMBERDATA):this.GlobalS.decode();
            this.inputForm = this.fb.group(inputFormDefault);
            
            var date = new Date();
            let d_strdate = new Date(date.getFullYear(), date.getMonth(), 1);
            let d_enddate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            let d_agedate = new Date(date.getFullYear(), 11, 22);
            
            this.startdate = d_strdate;
            this.enddate = d_enddate;
            this.agedate = d_agedate;


            this.route.queryParams.subscribe(params => {
                const tab = params['tab'];
            
                if (tab !== undefined) {
                  this.tabFindIndex = +tab; // Use the passed tab index
                } else {
                  const previousTabIndex = this.navigationService.getPreviousTabIndex();
                  if (previousTabIndex !== null) {
                    this.tabFindIndex = previousTabIndex; // Use the stored tab index
                  } else {
                    this.tabFindIndex = 0; // Default to tab 0 if no index is stored
                  }
                }
              });

            
            this.buildForm();
                                                   
        }
        ngAfterViewInit(): void {                    
            
            //testing 
            this.listS.getreportcriterialist({
                listType: 'BRANCHES',
                includeInactive: false
            }).subscribe(x => this.branchesArr = x);
            //
            this.listS.getreportcriterialist({
                listType: 'BRANCHES',
                includeInactive: false
            }).subscribe(x => this.branchesArr = x);
            
            
            this.listS.getreportcriterialist({
                listType: 'MANAGERS',
                includeInactive: false
            }).subscribe(x => this.managersArr = x)
            
            this.listS.getreportcriterialist({
                listType: 'PROGRAMS',
                includeInactive:false
            }).subscribe(x => this.programsArr = x);
            
            this.listS.GetRecipientAll().subscribe(x => this.recipientArr = x);
            this.listS.GetRecipientActive().subscribe(x => this.AccountsArr = x);
           // this.listS.Getpackages().subscribe(x => this.PackagesArr = x);
            this.listS.GetBatchNo().subscribe(x => this.BatchNoArr = x);
            this.listS.GetGroupMeals().subscribe(x => this.MealGroups = x);
            this.listS.getliststaffteam().subscribe(x => this.staffteamArr = x)
            this.listS.getliststaffgroup().subscribe(x => this.staffgroupsArr = x)
            this.listS.getserviceregion().subscribe(x => this.serviceRegionsArr = x)
            this.listS.GetVehicles().subscribe(x => this.vehiclesArr = x);
            this.listS.GetTraccsStaffCodes().subscribe(x => this.staffArr = x) 
            this.listS.getcstdaoutlets().subscribe(x => this.outletsArr = x);
            
            this.menuS.getsystemnumber(1).subscribe(data=>{this.sysnumberDetail = data;})
        }
        ngOnDestroy(): void {
            
            
        }
        Handlegrid(id){
            this.FormCloseid = id;
        }
        Handle_grid(){
            
            this.isVisibleTop = false; 
            this.Single_input_integer = false;  
            this.isVisible = false;                
          //  this.outPutForm(this.FormCloseid);
            this.tryDoctype = ""
            this.pdfTitle = ""
            this.inputForm = this.fb.group(inputFormDefault);
            
        }
        /*
        handleOkTop() {            
            this.isVisibleTop = false; 
            this.Single_input_integer = false;  
            this.isVisible = false;                
            this.reportRender(this.btnid);
            this.tryDoctype = ""
            this.pdfTitle = ""
            this.inputForm = this.fb.group(inputFormDefault);                        
        } */
        toggle_Selection_Option() {
            this.selection = !this.selection;
            this.options = !this.options;
            if (this.options == true) {
                
            }
            else {
                
            }
        }
        tabFindChange(index: number){
            this.navigationService.setPreviousTabIndex(index);
            this.tabFindIndex = index;   
        }


}//ConfigurationAdmin
