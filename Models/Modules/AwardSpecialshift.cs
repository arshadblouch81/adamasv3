using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class AwardSpecialshift
    {
      public int? RecordNumber { get; set; } = 0;

      public int? AwardID { get; set; }= null;
      
      public bool? PayBackToBack { get; set; }=null;
      
      public string Name { get; set; }=null;
      
      public string Activity { get; set; }=null;
      
      public string Notes {get;set;} = null;
      
      public string JobType {get;set;} = null; 

      public string BackToBackPayType {get;set;} = null; 
 
    }
}
