using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Adamas.Models.Tables {
    public class Recipients
    {
        [Key]
        public int SqlId { get; set; }
        public string UniqueID { get; set;}
        public string FilePhoto { get; set; }
        public string AccountNo { get; set; }
        public string FirstName { get; set; }
        public string MiddleNames { get; set; }
        [Column("Surname/Organisation")]
        public string SurnameOrg{ get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
        public string PreferredName { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set;}
        public string Branch { get; set; }
        public string ContactIssues { get; set; }
        public string Notes { get; set; }
        public string SpecialConsiderations { get; set; }
        public string AgencyIdReportingCode { get; set; }
        public string URNumber { get; set; }
        public string UBDMap { get; set; }
        public string Recipient_Coordinator { get; set; }
        public string AgencyDefinedGroup { get; set; }
        public string BelongsTo { get; set; }
        public string RECIPIENT_PARENT_SITE { get; set; }
        public string MedicareNumber { get; set; }
         public string MedicareRefNumber  { get; set; }
        public DateTime? MedicareExpiry { get; set; }
        public string MedicareRecipientId { get; set; }
        public string PensionStatus { get; set; }

        [Column("PensionVoracity", TypeName = "bit")]
        [DefaultValue(false)]
        public bool PensionVoracity { get; set; }
        public string ConcessionNumber { get; set; }

        [Column("DVABenefits", TypeName = "bit")]
        [DefaultValue(false)]
        public bool DVABenefits { get; set; }

        public string DVANumber { get; set; }

        [Column("Ambulance", TypeName = "bit")]
        [DefaultValue(false)]
        public bool Ambulance { get; set; }
        public string AmbulanceType { get; set; }

        [Column("WillAvailable", TypeName = "bit")]
        [DefaultValue(false)]
        public bool WillAvailable { get; set; }
        public string WhereWillHeld { get; set; }
        public string FuneralArrangements { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public DateTime? DischargeDate { get; set; }
        public string Type { get; set; }
        public string NDISNumber { get; set; }
        
        public string Pan_Status { get; set; }
        public string PinCode { get; set; }
        public string Pan_Manager { get; set; }
        public int? TimeZoneOffset { get; set; }

        [Column("AllowRegisterSignature", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? AllowRegisterSignature { get; set; }
        public double? PanNoGoTH { get; set; }
        public double? PanNoShowTH { get; set; } 
        public double? PanNoWorkTH { get; set; } 
        public double? PanOverStayTH { get; set; } 
        public double? PanUnderStayTH { get; set; } 
        public double? panEarlyFinishTH { get; set; } 

        public double? PercentageRate { get; set; } 
        [Column("PermDisability", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PermDisability { get; set; }

        [Column("PAN_TAEarlyStartTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAEarlyStartTHEmail { get; set; }

        [Column("PAN_TAEarlyStartTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAEarlyStartTHSMS { get; set; }

        public string PAN_TAEarlyStartTHWho { get; set; }
        public double? panLateStartTH { get; set; }  

        [Column("PAN_TALateStartTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TALateStartTHEmail { get; set; }

        [Column("PAN_TALateStartTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TALateStartTHSMS { get; set; }
        public string PAN_TALateStartTHWho { get; set; }

        public string PAN_TALateFinishTHWho { get; set; }
        

        [Column("PAN_TAEarlyFinishTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAEarlyFinishTHEmail { get; set; }

        [Column("PAN_TAEarlyFinishTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAEarlyFinishTHSMS { get; set; }

        public string PAN_TAEarlyFinishTHWho { get; set; }
        public double? panlatefinishth { get; set; }
        
        [Column("PAN_TALateFinishTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TALateFinishTHEmail { get; set; }

        [Column("PAN_TALateFinishTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TALateFinishTHSMS { get; set; }


        [Column("PAN_TAOverstayTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAOverstayTHEmail { get; set; }

        [Column("PAN_TAOverstayTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAOverstayTHSMS { get; set; }
        public string PAN_TAOverstayTHWho { get; set; }


        [Column("PAN_TAUnderstayTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAUnderstayTHSMS { get; set; }


        [Column("PAN_TAUnderstayTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TAUnderstayTHEmail { get; set; }

        public string PAN_TAUnderstayTHWho { get; set; }

        
        [Column("PAN_TANoWorkTHEmail", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TANoWorkTHEmail { get; set; }


        [Column("PAN_TANoWorkTHSMS", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PAN_TANoWorkTHSMS { get; set; }

        public string PAN_TANoWorkTHWho { get; set; }
        public string DAELIBSID { get; set; }
        
        public double? panearlystartth { get; set; }
        
        
        [Column("ExcludeFromRosterCopy", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? ExcludeFromRosterCopy { get; set; }
        
        [Column("GeneratesReferrals", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? GeneratesReferrals { get; set; }
        
        [Column("AcceptsReferrals", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? AcceptsReferrals { get; set; }

        public string MainSupportWorker { get; set; }
        public string FinancialClass { get; set; }
        public string Occupation { get; set; }
        public string DischargedBy { get; set; }
        
        [Column("CompanyFlag", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? CompanyFlag { get; set; }
        public string AdmittedBy { get; set; }
        public string OhsProfile { get; set; }
        public string BillTo { get; set; }
        [Column("DirectDebit", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? DirectDebit { get; set; }

        public string BPayRef { get; set; }
        [Column("DvaCoBiller", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? DvaCoBiller { get; set; } 
        [Column("Recipient_Split_Bill", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? Recipient_Split_Bill { get; set; }

        public string BillingCycle { get; set; }
        public string BillingMethod { get; set; }
        
        [Column("CappedBill", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? CappedBill { get; set; }
        
        public double? DonationAmount { get; set; }
        public string AccountingIdentifier { get; set; }
        [Column("Order#")]
        public string OrderNo { get; set; }
        public double? ReportingId { get; set; }
        
        [Column("PrintInvoice", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PrintInvoice { get; set; }
        
        [Column("EmailInvoice", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? EmailInvoice { get; set; }
        
        [Column("PrintStatement", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? PrintStatement { get; set; }
        
        [Column("EmailStatement", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? EmailStatement { get; set; }

        public int? Terms { get; set; }
        public string BillProfile { get; set; }
        public string CreditCardType { get; set; }
        public string CreditCardTypeOther { get; set; }
        public string CreditCardNumber { get; set; }
        public DateTime? CreditCardExpiry { get; set; }
        public string CreditCardName { get; set; }
        public string CreditCardCCV { get; set; }
        
        [Column("Fdp", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? Fdp { get; set; }
        [Column("Wh&s")]
        public DateTime? Whs { get; set; } 
        public string InterpreterRequired { get; set; }
        
        [Column("CareplanChange", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? CareplanChange { get; set; }
        
        [Column("HideTransportFare", TypeName = "bit")]
        [DefaultValue(false)]   

          
        public bool? HideTransportFare { get; set; }

        public string? Mobility { get; set; }
        public string? CarerStatus { get; set; }
        public string? MTAMethod { get; set; }

         public bool SMSServiceReminders { get; set; }

        public string?  ReferralSource { get; set; } 
        public string?  CountryOfBirth { get; set; }

        public string  HomeLanguage { get; set; }
        
        public string  LivingArrangements { get; set; }
        public string  IndiginousStatus { get; set; }
        public string  DwellingAccomodation { get; set; }
        public string  CarerAvailability { get; set; }

        public string  DatasetCarer { get; set; }

         public string  CarerResidency { get; set; }

         public string  CarerRelationship { get; set; }

         public string  ResidencyVisaStatus { get; set; }

         public string  QCSS_Disabilities { get; set; }

        public bool  CARER_MORE_THAN_ONE { get; set; }

         
       public string CSTDA_OtherDisabilities  { get; set; }
         
        public string KLMArrangements { get; set; } 
        
           public string Alias { get; set; }  


    }
}