using System.IO;
using System;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Threading;
using Adamas.DAL;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Security.Principal;
using System.Collections.Generic;

namespace Adamas.ServiceWorker {

    public class TimerHostedService : BackgroundService
    {
        private readonly ILogger _logger;
        private IHostingEnvironment _hostingEnvironment;
        private Timer timer;
        private Timer tokenTimer;
        private string _hostPath;
        private ICacheService cache;
        public IConfiguration Configuration { get; }
        public TimerHostedService(
            ILoggerFactory loggerFactory,
            IHostingEnvironment hostingEnvironment,
            ICacheService _cache,
            IConfiguration configuration)
        {
            _logger = loggerFactory.CreateLogger<TimerHostedService>();
            _hostingEnvironment = hostingEnvironment;
            _hostPath = _hostingEnvironment.ContentRootPath;
            cache = _cache;
            Configuration = configuration;
        }

       protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            timer = new Timer(DeleteFilesOlderThan20Seconds, null, TimeSpan.Zero, TimeSpan.FromMinutes(30));
            // tokenTimer = new Timer(ScourExpiredTokens, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));
            await Task.CompletedTask;   
        }

        private void DeleteFilesOlderThan20Seconds(object state)
        {
            var documentPath = Path.Combine(this._hostPath, $"document");
     
            string[] files = Directory.GetFiles(documentPath);
            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastAccessTime < DateTime.Now.AddMinutes(-10)){                        
                    fi.Delete();
                }
            }           
        }
    }
}