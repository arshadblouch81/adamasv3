
using System.Collections.Generic;
using System;

namespace Adamas.Models.Modules{
    public class AdmitProcedure{

        // public List<AdmitProgram> Programs { get; set; }
        public int DocId { get; set; }
        public string Program { get; set; }
        public string ClientCode { get; set; }
        public string CarerCode { get; set; }
        public string ServiceType { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Creator { get; set; }
        public string Editer { get; set; }
        public string BillUnit { get; set; }
        public string AgencyDefinedGroup { get; set; }
        public string ReferralCode { get; set; }
        public string TimeSpent { get; set; }
        public string TimePercentage { get; set; }
        public string Notes { get; set; }
        public int Type { get; set; }
        public double Duration { get; set; }
        public int BlockNo { get; set; }
        public string ReasonType { get; set; }
        public string TabType { get; set; }
        public string AdmissionType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Notes NoteDetails { get; set; }
       
    }

    public class AdmitProgram {
        public string Program { get; set; }
        public string AdmissionType { get; set; }        
        public List<ApprovedServices> ApprovedServices { get; set; }
    }

    public class ApprovedServices {
        public string Name { get; set; }
    }

    public class Notes {
        public string PersonId { get; set; }
        public string Program { get; set; }
        public DateTime DetailDate { get; set; }
        public string ExtraDetail1 { get; set; }
        public string ExtraDetail2 { get; set; }
        public string WhoCode { get; set; }
        public string Creator { get; set; }
        public string Note { get; set; }
        public string AlarmDate { get; set; }
        public  int PublishToApp { get; set; }
        public string ReminderTo { get; set; }

    }
}
