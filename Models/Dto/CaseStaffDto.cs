﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class CaseStaffDto
    {
        public string PersonId { get; set; }
        public string Notes { get; set; }
        public string CaseManagerId { get; set; }
        public string Program { get; set; }
    }
}
