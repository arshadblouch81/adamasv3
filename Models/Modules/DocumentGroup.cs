
namespace Adamas.Models.Modules{
    public class DocumentGroup {
        public string PersonId { get; set; }
        public string DocumentType { get; set; }
    }
}