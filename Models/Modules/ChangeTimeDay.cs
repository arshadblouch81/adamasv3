
using System;
namespace Adamas.Models.Modules{
    public class ChangeDayTime {
        public int RecordNo { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Duration { get; set; }
        public string PayQuant { get; set; }
        public string BillQuant { get; set; }
    }
}