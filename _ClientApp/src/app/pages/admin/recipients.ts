
import {forkJoin,  of ,  Subject ,  Observable, observable, EMPTY } from 'rxjs';

import {mergeMap,takeUntil,debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators } from '@angular/forms'
import { Router } from '@angular/router'

import  { TimeSheetService, GlobalService, ClientService, ListService, statuses, contactGroups, recurringInt, recurringStr } from '@services/index';
import {  SqlWizardService } from '@services/sqlwizard.service'
import { SharedService } from '../../shared/sharedService/shared-service';

import { BsModalComponent } from 'ng2-bs3-modal';
import { ClrWizard } from "@clr/angular";
import * as moment from 'moment';
import * as _ from 'lodash';

const NOTE_TYPE: Array<string> = ['CASENOTE','OPNOTE','CLINNOTE']
const quantity: Array<number> = [1,2,3,4,5,6,7,8,9,10,11]
const title: Array<string> = ['EACH','PACK','CTN','PKT','ROLL/S']

interface ModalVariables {
    title?: string,
    programsArr?: Array<any>,
    referralTypesArr?: Array<string>,
    referralCodeArr?: Array<string>,
    referralSourceArr?:Array<string>,
    reminderToArr?: Array<string>
    wizardTitle?: string
}

interface UserTab{
    code: string,
    id: string,
    view: string,
    sysmgr: boolean,
    agencyDefinedGroup?: string
}

@Component({
    templateUrl:'./recipients.html',
    styles:[
        `    
        clr-checkbox-wrapper input[type="checkbox"]:disabled+label::before{
            background:#e4e4e4;
            border:0;
        }
        clr-checkbox-wrapper input[type="checkbox"]:disabled+label{
            color: #e4e4e4;
            cursor: not-allowed;
        }
        .program-wrapper{
            padding: 0 0 0 1.5rem; 
            border-top: 1px solid #e6e6e6;
            height: 12.5rem;
            overflow:hidden;
        } 
        .program-wrapper ul{
            overflow-y: auto;
            max-height: 100%;
            padding: 10px 0 0 15px;
        }
        .search{
            width: 20rem;
            margin: 10px auto;
        }   
        .tooltiptext {
            visibility:hidden;
            position: absolute;
            left: -7px;
            bottom: -30px;
            font-size: 10px;
            background-color: #4f4f4f;
            color: #fff;
            border-radius: 3px;
            z-index: 1;
            padding: 0 5px;        
        }
        select{
            display:block;
            margin-top:5px;
            width:100%;
        }
        .tabs-custom li:hover > .tooltiptext {
            visibility: visible;
        }

        table.usergroups tr td:first-child{
            text-align:left;
        }
        table thead th {
            white-space:nowrap;
        }

        table.usergroups tr th:first-child{
            text-align:left;
        }

        table.usergroups tr th:nth-child(2){
            text-align:left;
        }
        ul{
            list-style:none;
        }
        td i:first-child{
            
        }
        .disableOverFlow bs-modal-body >>> .modal-body{
            padding: 0 .125rem;
            display: inline-block;
            max-height: 70vh;
            overflow-y: inherit;
            overflow-x: inherit;
        } 
        input[type=radio]{
            width: initial;
        }
        ul.option-list{
            display: flex;
            font-size: 10px;
            text-align: center;
            margin: 20px auto 0 auto;
        }
        ul.option-list i{
            font-size:0.8rem;
        }
        ul.option-list li{
            flex:1;
            line-height: 20px;
        }
        ul.option-list li >  div:hover{
            cursor:pointer;
            color:#177dff;
        }
        ul.option-list li > div.disable{
            color:#dedede;
            cursor: auto; 
        }
        .notes-loans{
            height:6rem;
        }
        .form-inline-group{
            display:flex;
            margin:0;
        }
        .form-inline-group label{
            margin:5px 0;
            flex:2;
        }
        .form-inline-group input, select, datepicker{
            flex:3;
        }
        .form-bubble{
            border: 1px solid #ededed;
            border-radius: 3px;
            margin-bottom: 10px;
            display: inline-block;
            width: 100%;
        }
        .val-options div{
            display: block;
        }
        .detail h4{
            margin:0;
            font-weight:bold;
        }
        .detail span{
            font-size:11px;
        }
        ul.detail-list{
            margin: 10px;
            font-size: 12px;
        }

        img.side-img{
            width: 1.5rem;
            float: right;
            margin: 10px 0;
        }
        .notif-message{
            font-size: 11px;
            float: left;
            color: #fc4141;
        }
        .front-up{
            z-index:100000 !important;
        }
        h5{
            margin:0;
        }
        clr-wizard-page >>> .spinner{
            left: inherit !important;
            position: absolute;
            top: 40%;        
        }
        .side-options{
            float:right;
        }
        .side-options button{
            margin-left:10px;
        }
        ul.list:nth-child(even){
            background:#f5f5f5;
        }
        .flex-list{
            display:flex;
            padding:5px;
            align-items:center;
        }
        .flex-list > div:first-child{
            flex:4;
        }
        .flex-list > div:last-child{
            flex:1;
        }
        .flex-list clr-checkbox-wrapper{
            float:right;
        }
        .sample-fixed{
            position: fixed;
            top:0;
            left:0;
            transform: translateX(0) translateY(5rem);
            display: flex;
            flex-direction: column;
            justify-content: center;
            text-align: center;
        }
        .sample-fixed > div{
            margin-top: 10px;
            padding: 6px;
            color: #fff;
            border-radius: 2px;
            width: 3rem;
            height: 2.5rem;
            line-height: 16px;
            font-size: 10px;
            cursor: pointer;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        .sample-fixed > div:hover{
            background: #ffffffc7;
        }
        .sub-options{
            margin-right: 15px;
        }
        .sub-options i{
            cursor:pointer;
            font-size: 1rem;
        }
        .sub-options i:hover{
            color: #2d2d2d;
        }
        .filter-wrapper{
            margin-top: 15px;
            padding: 10px 10px 5px 10px;
            background: #ffffff;
            border-top: 1px solid #dedede;
            border-bottom: 1px solid #e6e6e6;
        }
        i.active{
            color: #177dff;
        }
        .casemanager-list{
            height: 6rem;
            overflow: auto;
            border: 1px solid beige;
        }
        .casemanager-list ul{
            margin: 10px;
        }
        `
    ]
})

export class RecipientsAdmin implements OnInit, OnDestroy {
    private unsubscribe$ = new Subject();
    whatOptionModalNo = new Subject<number>();
    whatOptionVar: ModalVariables
    packageCodeChanges = new Subject<string>();
    referralVariables: any;

    @ViewChild("wizard", { static: false }) wizard: ClrWizard;

    // @ViewChild('modalApprovedPrograms') modalApprovedPrograms: BsModalComponent;
    @ViewChild('addOPNoteModal', { static: false }) addOPNoteModal: BsModalComponent;
    @ViewChild('addCaseNoteModal', { static: false }) addCaseNoteModal: BsModalComponent;
    @ViewChild('addPensionInsuranceModal', { static: false }) addPensionInsuranceModal: BsModalComponent;
    @ViewChild('modalPackageTemplate', { static: false }) modalPackageTemplate: BsModalComponent;
    
    @ViewChild('addRemindersModal', { static: false }) addRemindersModal: BsModalComponent;
    @ViewChild("addcontactskinModal", { static: false }) addcontactskinModal: ClrWizard;
    @ViewChild("modalDecease", { static: false }) modalDecease: ClrWizard;
    @ViewChild("addLoanModal", { static: false }) addLoanModal: ClrWizard;
    @ViewChild("modalAdmitPrograms", { static: false }) modalAdmitPrograms: ClrWizard;
    @ViewChild('optionModal', { static: false }) optionModal: BsModalComponent;
    @ViewChild("wizardlg", { static: false }) wizardLarge: ClrWizard;
    @ViewChild("wizardQuote", { static: false }) wizardQuote: ClrWizard;

    lgOpen: boolean = false;
    refreshSearch: boolean = false;

    currentDate: any
    timeSpent: string = '00:15'
    time: string =''

    statusOfWizard: any;

    noteArray: Array<string> = [];

    displayLast: number = 20;
    archivedDocs: boolean = false;
    acceptedQuotes: boolean = false;

    options: any = {
        edit: false,
        archive: false,
        print: false,
        refresh: false
    }

    filter: any = {
        create: false,
        delete: false,
        view: false
    }

    addContactOpen: boolean = false;
    modalAdmitOpen: boolean = false;
    modalDeceaseOpen: boolean =  false;
    servicesOpen: boolean = false;
    newReferralOpen: boolean = false;
    newOtherOpen: boolean = false;
    quoteModalOpen: boolean = false;
    addQuoteModalOpen: boolean = false;

    recipientStrArr: Array<string> = [];
    selected: any;    
    statuses: Array<string> = statuses;
    contactGroups: Array<string> = contactGroups;
    types: Array<string>;

    opnoteModalObject: {
        category: Array<string>,
        program: Array<string>,
        discipline: Array<string>,
        caredomain: Array<string>,
        manager: Array<string>
    }

    user: UserTab;
    list: Array<any> = []
    tableData: Array<any> = null
    loanitemArray: Array<any> = []
    loanprogramsArray: Array<any> = []
    pensionArray: Array<any> = []
    pensionAllArr: Array<string>
    caseStatusArray: Array<string> = []
    remindersArr: Array<string>;
    recurringIntArray: Array <number> = recurringInt
    recurringStrArray: Array <string> = recurringStr

    tab:number = 1;

    inOverflow: boolean = true;
    loadResult: boolean = false;
    loading: boolean = false;
   
    changeTab = new Subject<number>();
    statusTab = new Subject<any>();
    status: any

    contactFormGroup: FormGroup;
    opFormGroup: FormGroup;
    caseFormGroup: FormGroup;
    loanFormGroup: FormGroup;
    quoteFormGroup: FormGroup;
    insuranceFormGroup: FormGroup; 
    pensionFormGroup: FormGroup;   
    remindersGroup: FormGroup;
    referralGroup: FormGroup;
    admitGroup: FormGroup;

    dbAction: number;
    insurance: any;

    programTicked: boolean = false;

    admitPrograms: Array<any>
    admitServices: Array<any>;

    quantity: Array<number> = quantity;
    title: Array<string> = title;

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private clientS: ClientService,
        private sharedS: SharedService,
        private listS: ListService,
        private sqlWizS: SqlWizardService,
        private formBuilder: FormBuilder
    ){
       this.resetGroup();
    }

    resetGroup(){

        // this.admitGroup = this.formBuilder.group({
        //     programDetails: '',
        //     dateDetails: '',
        //     noteDetails: '',
        //     reminderDetails: ''
        // })

        this.insuranceFormGroup = this.formBuilder.group({   
            personID: '',  
            medicareNumber: '',
            medicareExpiry: null,
            medicareRecipientID: '',
            pensionStatus: '',
            concessionNumber: '',
            dvaNumber: '',
            ambulanceType: '',
            haccDvaCardHolderStatus: '',
            dvaBenefits: false,
            pensionVoracity: false,
            ambulance: false,        
            dateofDeath: null,
            willAvailable: '',
            whereWillHeld: '',
            funeralArrangements: ''
        })

        this.contactFormGroup = this.formBuilder.group({
            group: [''],
            listOrder: [''],
            type: [''],
            name: [''],
            email: [''],
            address1: [''],
            address2: [''],
            suburbcode: [''],
            suburb: [''],
            postcode: [''],
            phone1: [''],
            phone2: [''],
            mobile: [''],
            fax: [''],
            notes: [''],
            oni1: false,
            oni2: false,
            ecode: [''],
            creator: [''],
            recordNumber: null
        })

        this.quoteFormGroup = this.formBuilder.group({
            program: [''],
            template: ['']
        })

        this.opFormGroup = this.formBuilder.group({
            notes: '',
            publishToApp: false,
            restrictions: '',
            restrictionsStr: 'public',
            alarmDate: null,
            whocode: '',
            program: '*VARIOUS',
            discipline:'*VARIOUS',
            careDomain: '*VARIOUS',
            category:'',
            recordNumber: null
        })

        this.caseFormGroup = this.formBuilder.group({
            notes: '',
            publishToApp: false,
            restrictions: '',
            restrictionsStr: 'public',
            alarmDate: null,
            whocode: '',
            program: '*VARIOUS',
            discipline:'*VARIOUS',
            careDomain: '*VARIOUS',
            category:'',
            recordNumber: null
        })

        this.loanFormGroup = this.formBuilder.group({
            type: '',
            item: '',
            loaned: null,
            collected: null,
            dateInstalled: null,
            program: '',
            notes: ''
        })
        
        this.remindersGroup = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            name: new FormControl('',[Validators.required]),
            address1: '',
            address2: '',
            email: new FormControl('',[Validators.email]),
            date1: null,
            date2: null,
            recurring: false,
            notes: ''
        })
           
        this.resetPension();
        this.recurringChangeEvent(false);
        this.recipientStrArr = [];

        this.contactFormGroup.get('group').valueChanges
            .pipe(mergeMap(data => this.listS.getlistkintype(data.trim())))
            .subscribe(group => {                
                this.contactFormGroup.patchValue({type: ''})
                this.types = group;
            });
    }

    clearFilters(){
        this.options = {
            edit: false,
            archive: false,
            print: false,
            refresh: false
        }
    
        this.filter = {
            create: false,
            delete: false,
            view: false
        }
    }

    resetPension(){
        this.pensionFormGroup = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            name: '',
            address1: '',
            address2: '',
            notes: ''
        })
    }

    resetReferral(){    
        this.referralGroup = this.formBuilder.group({
            date: moment(),    
            notes:'',
            noteType: 'CASENOTE',
            noteCategory: '',    
            time: moment().format('HH:mm'),
            timeSpent: '00:15',
            publishToApp: false,     
            referralSource: '',
            referralCode: '',
            referralType: '',
            reminderDate: '',
            reminderTo: '',
            notifTo: '',

            quantity: '',
            quantityTitle: '',
            charge: '',
            gst: '',
            suppRefNo: '',
            suppDate: '',
            dateOfDeath: ''
        })
    }

    heheOpen: boolean = false;

    referTab: number;
    doPackageExist: boolean = false;

    referOpen: any;

    ngOnInit(){   
        this.resetReferral();

        this.packageCodeChanges.pipe(debounceTime(300),distinctUntilChanged()).subscribe(data => {
            this.checkPackageCodeIfExist(data)
        })

        this.referralGroup.get('notes').valueChanges.pipe(debounceTime(200)).subscribe(data => {
                if(data && data.length > 0 && this.showThisOnTab_A()){
                    
                    this.referralGroup.get('noteCategory').enable();
                    this.noteChangeEvent(this.noteNumber)
                    return;
                }

                if(this.whatOptionVar){
                    this.referralGroup.get('noteCategory').disable();
                    this.referralGroup.patchValue({
                        noteCategory: this.whatOptionVar.wizardTitle
                    })
                }

            })

        this.referralGroup.get('referralType').valueChanges
            .pipe(debounceTime(200))
            .subscribe(data => {
              if(!this.globalS.isEmpty(data)){
                  this.listS.getchargeamount(data)
                        .subscribe(data => {
                            if(data){
                                this.referralGroup.patchValue({
                                    charge: data.amount
                                })
                            }
                        })
              }
            })


        this.statusTab
            .pipe(
                mergeMap(x => this.listS.getstatusofwizard(x.id))
            ).subscribe(data => {
            this.status = data
        })    

        this.whatOptionModalNo.subscribe(data => {            

            if( !this.disableWizardButtons(data) ){
                return;
            }

            this.wizard.reset();
            this.programTicked = false;

            this.referralGroup.patchValue({
                date: moment(),    
                notes:'',
                noteType: 'CASENOTE',
                noteCategory: '',    
                time: moment().format('HH:mm'),
                timeSpent: '00:15',
                publishToApp: false,     
                referralSource: '',
                referralCode: '',
                referralType: '',
                reminderDate: '',
                reminderTo: '',
                notifTo: ''
            });
            
            this.referTab = data;

            switch(data){
                case 1:    
                    this.populateWizards('Referral Registration Wizard',this.user,'REFERRAL-IN')
                    //this.heheOpen = true;
                    this.userReferIn = this.user;
                    this.referOpen = {};
                    break;
                case 2: 
                    this.populateWizards('Referral Out Registration Wizard',this.user,'REFERRAL-OUT')
                    this.heheOpen = true;
                    break;
                case 3:
                    this.populateWizards('Referral Not Proceeding Wizard',this.user,'NOT PROCEEDING')
                    this.heheOpen = true;
                    break;
                case 4:
                    this.populateWizards('Assessment Registration Wizard',this.user,'ASSESSMENT')
                    this.heheOpen = true;
                    break;
                case 5: 
                    this.populateWizards('Waitlist Management Wizard',this.user,'WAITLIST')
                    this.heheOpen = true;
                    break;
                case 6:
                    this.populateWizards('Discharge Registration Wizard',this.user,'DISCHARGE')                
                    this.heheOpen = true;
                    break;
                case 7:         
                    this.populateWizards('Service Suspension Wizard',this.user,'SUSPENSION')
                    this.heheOpen = true;    
                    break;
                case 8:
                    this.populateWizards('Administration Registration Wizard',this.user,'OTHER')
                    this.heheOpen = true;
                    break;
                case 9:
                    this.populateWizards('Record Items Wizard',this.user,'ITEM')
                    this.heheOpen = true;
                    break;
                case 10:
                    this.populateWizards('Record Items Wizard',this.user,'ADMISSION')
                    this.modalAdmitInit();
                    break;
                case 11: 
                    this.populateWizards('Reinstate Suspension Services',this.user,'ADMIN');
                    this.heheOpen = true;
                    break;
                case 12: 
                    this.populateWizards('Deceased', this.user, 'DECEASE')
                    this.modalDeceaseOpen = true;
                    break;

            }

            this.referralGroup.get('noteCategory').disable();

            if(this.referTab == 5 || this.referTab == 7 || this.referTab == 8){
                this.listS.getcasenotecategory(0).subscribe(data => this.noteArray = data);
            }
        });



        this.changeTab.pipe(
            takeUntil(this.unsubscribe$))
            .subscribe(data =>{
            this.tableData = null;
            this.loadResult = false;
            this.loading = true;

            const user = this.user.code;
            const id = this.user.id;

            switch(data){
                case 1:
                    this.tableData = [];
                    this.loading = false;
                    break;
                case 2:
                    this.timeS.getcontactskinrecipient(id).subscribe(data => {                        
                        this.tableData = data.list;
                        this.selected = data.list && data.list.length > 0 ? data.list[0] : '';
                        this.loading = false;
                    });
                    this.loadResult = true;
                    break;
                case 3:
                    this.loading = false;
                    break;
                case 4:
                    this.tableData = [];
                    this.loading = false;
                    break;
                case 5:
                    this.tableData = [];
                    this.loading = false;
                    break;
                case 6:
                    this.clientS.getreminders(user).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    });
                    this.listS.getlistrecipientreminders().subscribe(data => this.remindersArr = data);
                    this.loadResult = true;
                    break;
                case 7:
                    this.populateOPNoteModal();
                    this.clientS.getopnotes(id).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 8:
                    this.populateOPNoteModal();
                    this.clientS.getcasenotes(user).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 9:
                    this.clientS.getincidents(id).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 10: 
                    this.clientS.getloans(id).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 11:
                    this.clientS.getpermroster(user).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 12:
                    this.clientS.gethistory(user).subscribe(data => {
                        this.tableData = data.list;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 13:
                    this.timeS.getinsurance(id).subscribe(data => { 
                        this.insuranceFormGroup.patchValue({                   
                            medicareNumber: data.medicareNumber,
                            medicareExpiry: data.medicareExpiry ,
                            medicareRecipientID: data.medicareRecipientID,
                            pensionStatus: data.pensionStatus,
                            concessionNumber: data.concessionNumber,
                            dvaNumber: data.dvaNumber,
                            ambulanceType: data.ambulanceType,
                            haccDvaCardHolderStatus: data.haccdvaCardholderStatus,
                            dvaBenefits: data.dvaBenefits,
                            pensionVoracity: data.pensionVoracity,
                            ambulance: data.ambulance,
                            dateofDeath: data.dateOfDeath,
                            willAvailable: data.willAvailable,
                            whereWillHeld: data.whereWillHeld,
                            funeralArrangements: data.funeralArrangements
                        })
                        this.loading = false;
                    })
                    this.loadResult = false;
                    this.reloadPension()
                    this.listS.getcardstatus().subscribe(data => this.caseStatusArray = data)
                    break;
                case 14:
                    
                    break;
                case 15:
                    this.listS.getlistquotes({
                        PersonID: id,
                        DisplayLast: this.displayLast,
                        IncludeArchived: this.archivedDocs,
                        IncludeAccepted: this.acceptedQuotes
                    }).subscribe(data => {
                        this.tableData = data;
                        this.loading = false;
                    })
                    this.loadResult = true;
                    break;
                case 16:

                    break;
                case 17:

                    break;
                case 18:

                    break;
            }

            this.tab = data;
        })
    }

    modalAdmitInit(){
        this.modalAdmitOpen = true;
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    disableWizardButtons(tab: number): boolean {
        if(this.status.b_IsDead == 1 && tab != 8)   return false;        
        if(this.status.b_HasReferrals == 0 && (tab == 3 || tab == 4 || tab == 10))  return false;        
        if(this.status.b_HasActivePrograms == 0 && (tab == 5 || tab == 6 || tab == 7))  return false;
        if((this.status.b_HasOnHoldPrograms == 0 && this.status.b_HasOnHoldServices == 0) && tab == 11)   return false;
        return true;
    }

    noteNumber: number;
    _whatOptionVar: any

    populateWizards(title: string, user: any, wizardTitle: string){

       this.noteNumber = 0;
       switch(this.referTab){
           case 1:
                forkJoin([
                    this.listS.getwizardprograms(user.id)
                ]).subscribe((data: any) => {
                    var programsList = data[0].map(x => {
                        return {
                            program: x.name,
                            type: x.type,
                            status: false,
                            disabled: false
                        }
                    });
                    
                    this.whatOptionVar = {
                        title: title,
                        wizardTitle: wizardTitle,
                        programsArr: programsList
                    }
                    this.changeNoteEvent();
                })
                break;
            case 2: 
            case 3:
                this.listS.getlist(`SELECT DISTINCT UPPER(rp.[Program]) AS Program, hr.[type] FROM RecipientPrograms rp INNER JOIN HumanResourceTypes hr ON hr.NAME = rp.program WHERE rp.PersonID = '${this.user.id}' AND ProgramStatus = 'REFERRAL' AND isnull([Program], '') <> ''                 `)
                    .subscribe(data => {
                        this.whatOptionVar = {
                            title: title,
                            wizardTitle: wizardTitle,
                            programsArr: data.map(x => {
                                return {
                                    program: x.program,
                                    type: x.type,
                                    status: false,
                                }
                            })
                        }
                        this._whatOptionVar = this.whatOptionVar
                        this.changeNoteEvent();
                    })
                    break;
            case 4:
                this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program FROM RecipientPrograms WHERE PersonID = '${ this.user.id }' AND ProgramStatus = 'REFERRAL' AND isnull([Program], '') <> ''`)
                    .subscribe(data => {
                        this.whatOptionVar = {
                            title: title,
                            wizardTitle: wizardTitle,
                            programsArr: data.map(x => {
                                return {
                                    program: x.program,
                                    type: x.type,
                                    status: false,
                                }
                            })
                        }
                        this._whatOptionVar = this.whatOptionVar
                        this.changeNoteEvent();
                    })
                    break;
            case 5: 
                this.listS.getlist(`SELECT DISTINCT UPPER( [Service Type] + ' (Pgm): ' + [ServiceProgram]) AS Program FROM  ServiceOverview SO INNER JOIN RecipientPrograms RP ON RP.PersonID = SO.PersonID WHERE  SO.PersonID = '${this.user.id}' AND  (ProgramStatus IN  ('ACTIVE', 'WAITING LIST') AND ServiceStatus IN ('ACTIVE', 'WAIT LIST'))  AND isnull([Program], '') <> ''`)
                    .subscribe(data => {
                        this.whatOptionVar = {
                            title: title,
                            wizardTitle: wizardTitle,
                            programsArr: data.map(x => {
                                return {
                                    program: x.program, 
                                }

                            })
                        }
                        this._whatOptionVar = this.whatOptionVar
                        this.changeNoteEvent();
                    })
                    break;
            case 6:
                this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program, HumanResourceTypes.[Type] AS Type FROM RecipientPrograms LEFT JOIN HumanResourceTypes ON RecipientPrograms.Program = HumanResourceTypes.Name WHERE PersonID = '${this.user.id}' AND ProgramStatus IN ('ACTIVE', 'WAITING LIST') AND isnull([Program], '') <> '' AND ISNULL([UserYesNo3], 0) <> 1 AND ISNULL([User2], '') <> 'Contingency' `)
                    .subscribe(data => {
                        this.whatOptionVar = {
                            title: title,
                            wizardTitle: wizardTitle,
                            programsArr: data.map(x => {
                                return {
                                    program: x.program, 
                                    type: x.Type
                                }

                            })
                        }
                        this._whatOptionVar = this.whatOptionVar
                        this.changeNoteEvent();
                    })
                    break;
            case 7: 
                this.listS.getlist(`SELECT DISTINCT '[' + CASE WHEN ([serviceprogram] <> '') AND ([serviceprogram] IS NOT NULL) THEN [serviceprogram] ELSE '?' END + '] ~> ' + [service type] AS Program FROM serviceoverview INNER JOIN recipientprograms ON serviceoverview.personid = recipientprograms.personid WHERE serviceoverview.personid = '${ this.user.id }' AND programstatus = 'ACTIVE' AND servicestatus = 'ACTIVE'`)
                    .subscribe(data => {
                        this.whatOptionVar = {
                            title: title,
                            wizardTitle: wizardTitle,
                            programsArr: data.map(x => {
                                return {
                                    program: x.program, 
                                    type: x.Type
                                }

                            })
                        }
                        this._whatOptionVar = this.whatOptionVar
                        this.changeNoteEvent();
                    })
                break;
            case 9:
            case 8: 
                    this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program FROM RecipientPrograms WHERE PersonID = '${ this.user.id }'AND ProgramStatus <> 'INACTIVE' AND isnull([Program], '') <> '' `)
                        .subscribe(data => {
                            this.whatOptionVar = {
                                title: title,
                                wizardTitle: wizardTitle,
                                programsArr: data.map(x => {
                                    return {
                                        program: x.program, 
                                    }

                                })
                            }
                            this._whatOptionVar = this.whatOptionVar
                            this.changeNoteEvent();
                        })
                    break;
            case 10:
                this.listS.getadmitprograms(this.user.id).subscribe(data => {
                    this.whatOptionVar = {
                        title: title,
                        wizardTitle: wizardTitle,
                        programsArr: data.map(x => {
                            return {
                                title: x,
                                status: false
                            }
                        })
                    }

                    this.changeNoteEvent();
                })
                break;
            case 11:
                this.listS.getlist(`SELECT DISTINCT '[' + CASE WHEN ([serviceprogram] <> '') AND ([serviceprogram] 
                    IS NOT NULL) THEN 
                    [serviceprogram] ELSE '?' END + '] ~> ' + [service type] AS 
                    Program, [Service Type] AS ServiceType, [ServiceProgram] AS ProgramName
    FROM   serviceoverview 
           INNER JOIN recipientprograms 
                   ON serviceoverview.personid = recipientprograms.personid 
    WHERE  serviceoverview.personid = '${ this.user.id }' 
           AND ( programstatus = 'ON HOLD' 
                  OR servicestatus = 'ON HOLD' ) `).subscribe(data => {
                    this.whatOptionVar = {
                        title: title,
                        wizardTitle: wizardTitle,
                        programsArr: data.map(x => {
                            return {
                                program: x.program,
                                serviceType: x.ServiceType,
                                programName: x.ProgramName
                            }

                        })
                    }
                    this._whatOptionVar = this.whatOptionVar
                    this.changeNoteEvent();
                  })
                  break;
            case 12: 
                this.listS.getlist(`SELECT DISTINCT Upper([program]) AS Activity 
                FROM   recipientprograms 
                       LEFT JOIN humanresourcetypes 
                              ON recipientprograms.program = humanresourcetypes.NAME 
                WHERE  personid = '${this.user.id}' 
                       AND programstatus IN ( 'ACTIVE', 'WAITING LIST' ) 
                       AND Isnull([program], '') <> '' 
                       AND Isnull([useryesno3], 0) <> 1 
                       AND Isnull([user2], '') <> 'Contingency' `).subscribe(data => {
                           this.referralGroup.patchValue({ noteCategory: 'DISCHARGE' })
                           this.selectedAdmitPrograms = data.map(x => {
                               return {
                                   title: x.activity,
                                   status: true
                               }
                           })
                           if(this.selectedAdmitPrograms.length > 0)    this.programChange(true,this.selectedAdmitPrograms[0])
                       })
                break;
                
       }
    }

    selectedAdmitPrograms: Array<any> = [];
    adminType: Array<any>;

    programChange(state: boolean, data: any){
        
        if(state){
            const programsChecked = this.selectedAdmitPrograms.map(x => `'${x.title}'`).join();

            this.listS.getlist(`SELECT DISTINCT([service type]) AS Activity 
            FROM   serviceoverview SO 
                   INNER JOIN humanresourcetypes HRT 
                           ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid 
            WHERE  HRT.[name] IN (${ programsChecked }) 
                   AND EXISTS (SELECT title 
                               FROM   itemtypes ITM 
                               WHERE  title = SO.[service type] 
                                      AND ITM.[rostergroup] IN ( 'ADMISSION' ) 
                                      AND ITM.[minorgroup] IN ( 'DISCHARGE' ) 
                                      AND ( ITM.enddate IS NULL 
                                             OR ITM.enddate >= '04-04-2019' )) 
            ORDER  BY [service type] `).subscribe(data => {                
                this.adminType = data.map(x =>{
                    return x.Activity
                });

                if(this.adminType.length > 0){
                    this.referralGroup.patchValue({
                        referralType: this.adminType[0]
                    })
                }
            })

            this.listS.getlist(`SELECT DISTINCT Description  FROM DataDomains WHERE Domain = 'REASONCESSSERVICE'`)
                .subscribe(data => {
                    this.referralVariables = {
                        referralSourceArr: data.map(x => {
                            return (x.Description).toUpperCase()
                        })
                    }

                    this.referralGroup.patchValue({
                        referralSource: 'CLIENT DIED'
                    })

                })
        }
    }

    reloadTab(page: number){
        this.changeTab.next(page);
    }

    picked(data: any){   
        this.user = {
            code: data.accountNo,
            id: data.uniqueID,
            view: 'recipient',
            agencyDefinedGroup: data.agencyDefinedGroup,
            sysmgr: true
        }
        console.log(this.user);
        
        this.statusTab.next(this.user);
        this.changeTab.next(this.tab);
        this.sharedS.emitChange(data);
    }

    populateOPNoteModal(){
        forkJoin([
            this.timeS.getcategoryop(),
            this.timeS.getdisciplineop(),
            this.timeS.getcaredomainop(),
            this.timeS.getmanagerop(),
            this.timeS.getprogramop(this.user.id)
        ]).subscribe(data => {
            this.opnoteModalObject = {
                category: data[0],
                discipline: data[1],
                caredomain: data[2],
                manager: data[3],
                program: data[4]
            }
            this.opnoteModalObject.discipline.push("*VARIOUS");
            this.opnoteModalObject.caredomain.push("*VARIOUS");
            this.opnoteModalObject.program.push("*VARIOUS");
        })
    }

    /**
     * Contacts
     */

    reset(): void {
        this.addcontactskinModal.reset();
        this.modalAdmitPrograms.reset();
        this.resetGroup();
    }



    addcontactskin(){
        if(this.contactFormGroup.controls['suburbcode'].dirty){
            const rs = this.contactFormGroup.get('suburbcode').value;
            let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0] : "";
            let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0] : "";
    
            this.contactFormGroup.controls["postcode"].setValue(pcode)
            this.contactFormGroup.controls["suburb"].setValue(suburb)
        }

        if(this.contactFormGroup.get('oni1').value){
            this.contactFormGroup.controls['ecode'].setValue('PERSON1')
        } else if (this.contactFormGroup.get('oni2').value){
            this.contactFormGroup.controls['ecode'].setValue('PERSON2')
        } 
        
        this.timeS.postcontactskinrecipientdetails(
            this.contactFormGroup.value,
            this.user.id
        ).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success','Contact Inserted');
                this.changeTab.next(2);
                return;
            }
            this.globalS.eToast('Error','Process Not Completed')
        })

        this.reset()
    }

    /** */

    /** 
     * Loans 
     * */
    deleteLoans(data: any){
        this.clientS.deleteloans(data.recordNumber).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Loan deleted')
                this.reloadTab(10)
                return;
            }
        })
    }
    /** */

    /**
     *  OP Notes
     */

    OPNoteAction(){
        if(this.dbAction == 0){
            this.opFormGroup.controls["whocode"].setValue(this.user.code);
            this.opFormGroup.controls["restrictions"].setValue(this.listStringify());
            
            this.clientS.postopnotes(this.opFormGroup.value, this.user.id)
                .subscribe(data => {
                    if(data){
                        this.globalS.sToast('Success','OPNote Posted')
                        this.reloadTab(7)
                        return;
                    }
    
                    this.resetGroup();
                })
        }

        if(this.dbAction == 1){


            if(this.opFormGroup.value.restrictionsStr === 'restrict' && this.recipientStrArr.length  === 0){
                this.globalS.wToast('Warning','You need to tick to proceed')
                return;
            }

            if(this.opFormGroup.value.restrictionsStr !== 'restrict') this.recipientStrArr = [];
            if(this.opFormGroup.value.alarmDate) this.opFormGroup.controls["alarmDate"].setValue(moment(this.opFormGroup.value.alarmDate).format())
            

            this.recipientStrArr = this.recipientStrArr.sort();
            this.opFormGroup.controls["restrictions"].setValue(this.listStringify());

            this.clientS.updateopnotes(this.opFormGroup.value, this.opFormGroup.value.recordNumber)
                        .subscribe(data => {
                            if(data){
                                this.globalS.sToast('Success','OPNote Posted')
                                this.reloadTab(7)
                                return;
                            }
            
                            this.resetGroup();
                        })
        }
    }

    deleteOPNote(data: any){
        this.clientS.deleteopnotes(data.recordNumber).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Note deleted')
                this.reloadTab(7)
                return;
            }
        })
    }

    updateOPNoteModal(record: any){
        this.dbAction = 1;

        this.addOPNoteModal.open();
        this.opFormGroup.patchValue({
            notes: record.detail,
            publishToApp: record.publishToApp,
            restrictions: record.restrictions,
            restrictionsStr: record.restrictions && record.restrictions.length > 0 ? 'restrict' : record.privateFlag ? 'workgroup' : 'public',
            alarmDate: record.alarmDate,
            whocode: '',
            program: record.program,
            discipline: record.discipline,
            careDomain: record.careDomain,
            category: record.category,
            recordNumber: record.recordNumber
        })

        if(this.opFormGroup.value.restrictions && this.opFormGroup.value.restrictions.length > 0){
            const splitStr = this.opFormGroup.value.restrictions.split('|')
            this.recipientStrArr = splitStr;
        }
    }

    /** */

    /**
     * Case Notes
     */

    updateCaseNoteModal(record: any){
        this.dbAction = 1;

        this.addCaseNoteModal.open();
        this.caseFormGroup.patchValue({
            notes: record.detail,
            publishToApp: record.publishToApp,
            restrictions: record.restrictions,
            restrictionsStr: record.restrictions && record.restrictions.length > 0 ? 'restrict' : record.privateFlag ? 'workgroup' : 'public',
            alarmDate: record.alarmDate,
            whocode: '',
            program: record.program,
            discipline: record.discipline,
            careDomain: record.careDomain,
            category: record.category,
            recordNumber: record.recordNumber
        })

        if(this.caseFormGroup.value.restrictions && this.caseFormGroup.value.restrictions.length > 0){
            const splitStr = this.caseFormGroup.value.restrictions.split('|')
            this.recipientStrArr = splitStr;
        }

    }

    checkManagerIsPresent(manager: string, groupName: string): boolean{
        if(this.dbAction == 0){
            return false;
        }

        if(this.dbAction == 1){
            const formGroup = groupName == 'opnote' ? this.opFormGroup : this.caseFormGroup;
            if(formGroup.value.restrictions && formGroup.value.restrictions.length > 0){
                const splitStr = formGroup.value.restrictions.split('|')
                var found = false;
                for(var a = 0; a < splitStr.length; a++){
                    if(splitStr[a] === manager){
                        found = true;
                        break;                    
                    }
                }
                return found;
            }
            return false;
        }
    }

    deleteCaseNote(data:any){
        this.clientS.deletecasenotes(data.recordNumber).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Note deleted')
                this.reloadTab(8)
                return;
            }
        })
    }

    CaseNoteAction(){
        if(this.dbAction == 0){
            this.caseFormGroup.controls["whocode"].setValue(this.user.code);
            this.caseFormGroup.controls["restrictions"].setValue(this.listStringify());
            
            this.clientS.postcasenotes(this.caseFormGroup.value, this.user.id)
                .subscribe(data => {
                    if(data){
                        this.globalS.sToast('Success','OPNote Posted')
                        this.reloadTab(8)
                        return;
                    }
    
                    this.resetGroup();
                })
        }

        if(this.dbAction == 1){
            if(this.caseFormGroup.value.restrictionsStr === 'restrict' && this.recipientStrArr.length  === 0){
                this.globalS.wToast('Warning','You need to tick to proceed')
                return;
            }

            if(this.caseFormGroup.value.restrictionsStr !== 'restrict') this.recipientStrArr = [];
            if(this.caseFormGroup.value.alarmDate) this.caseFormGroup.controls["alarmDate"].setValue(moment(this.caseFormGroup.value.alarmDate).format())
            

            this.recipientStrArr = this.recipientStrArr.sort();
            this.caseFormGroup.controls["restrictions"].setValue(this.listStringify());
          
         
            this.clientS.updatecasenotes(this.caseFormGroup.value, this.caseFormGroup.value.recordNumber)
                .subscribe(data => {
                    if(data){
                        this.globalS.sToast('Success','Case Note Posted')
                        this.reloadTab(8)
                        return;
                    }
    
                    this.resetGroup();
                })
        }
    }

    /** */

  
    listChckBoxes(state: boolean, name: string){
        if(state)   this.recipientStrArr.push(name);
        else this.recipientStrArr.splice(this.recipientStrArr.indexOf(name), 1)
    }

    listStringify(): string{
        let tempStr = '';
        this.recipientStrArr.forEach((data,index,array) =>{
            array.length-1 != index ?
                tempStr+= data.trim() + '|' :
                    tempStr += data.trim() ;                
        });
        return tempStr;
    }
    

    openModal(tabName: string){
        this.dbAction = 0;
        this.resetGroup();
        if(tabName === 'casenote'){ 
            this.addCaseNoteModal.open()
        }

        if(tabName === 'opnote'){
            this.addOPNoteModal.open()
        }

        if(tabName === 'loan'){
            this.addLoanModal.open()
            this.listS.getloanitems().subscribe(data => this.loanitemArray = data)
            this.listS.getloanprograms().subscribe(data => this.loanprogramsArray = data)
        }
    }

    loanTypeChange(event: any){       
        var item = this.loanitemArray.find(data => {
            return data.item == event.target.value
        })
        this.loanFormGroup.controls["item"].setValue(event.target.value)
        this.loanFormGroup.controls["type"].setValue(item.type)
    }
    /**
     *  Loan
     */

    addLoan(){
        this.listS.postloan(this.loanFormGroup.value, this.user.id).subscribe(data => {
            if(data){
                this.globalS.sToast('Sucess','Item inserted')
                return;
            }
            this.globalS.eToast('Error','Item not inserted')
        })

        this.addLoanReset();
    }

    addLoanReset(){
        this.addLoanModal.reset();
    }

    /** */

    /** Insurance & Pension */
    insuranceProcess(){
        this.insuranceFormGroup.controls["medicareExpiry"].setValue(this.insuranceFormGroup.value.medicareExpiry ? moment(this.insuranceFormGroup.value.medicareExpiry).format() : null)  
        this.insuranceFormGroup.controls["dateofDeath"].setValue(this.insuranceFormGroup.value.dateofDeath ? moment(this.insuranceFormGroup.value.dateofDeath).format() : null)  
        this.timeS.updateinsurance(this.insuranceFormGroup.value, this.user.id)
                    .subscribe(data => {
                        this.globalS.sToast('Success','Data Updated')
                    })
    }

    pensionProcess(){
        if(this.dbAction == 0){
            this.pensionFormGroup.controls["personID"].setValue(this.user.id)
            this.timeS.postpension(this.pensionFormGroup.value)
                        .subscribe(data => {
                            if(data){
                                this.reloadPension();
                                this.globalS.sToast('Success','Data Inserted')
                            }
                        })
        }

        if(this.dbAction == 1){
            this.timeS.updatepension(this.pensionFormGroup.value)
                        .subscribe(data => {
                            if(data){
                                this.reloadPension()
                                this.globalS.sToast('Success','Data Updated')
                            }
                        })
        }
    }

    deletePension(data: any){
        this.timeS.deletespension(data.recordNumber)
                    .subscribe(data => {
                        if(data){
                            this.reloadPension()
                            this.globalS.sToast('Success', 'Data Deleted')
                        }
                    })
    }

    updatePension(data: any){    
        this.addPensionInsuranceModal.open()        
        this.pensionFormGroup.patchValue({
            recordNumber: data.recordNumber,
            personID: data.personID,
            name: data.name,
            address1: data.address1,
            address2: data.address2,
            notes: data.notes
        })
    }

    reloadPension(): void {
        this.timeS.getpension(this.user.id).subscribe(data => this.tableData = data)
        this.listS.getpension(this.user.id).subscribe(data => this.pensionArray = data)
        this.listS.getpensionall().subscribe(data => this.pensionAllArr = data)
    }
    /** */

    /** Reminder */

    updateReminder(data: any){
        this.addRemindersModal.open()

        this.remindersGroup.patchValue(data)
        setTimeout(() => {
            this.recurringChangeEvent(this.remindersGroup.value.recurring)   
        });
    }

    reminderProcess(){
        this.remindersGroup.controls["date1"].setValue(GlobalService.filterDate(this.remindersGroup.value.date1))
        this.remindersGroup.controls["date2"].setValue(GlobalService.filterDate(this.remindersGroup.value.date2))
        this.remindersGroup.controls['personID'].setValue(this.user.id)

        if(this.dbAction == 0){            
            this.timeS.postremindersrecipient(this.remindersGroup.value)
                        .subscribe(data => { 
                            if(data){
                                this.globalS.sToast('Success','Data Inserted')
                                this.reloadTab(6)
                            }
                        })
        }

        if(this.dbAction == 1){
            this.timeS.updateremindersrecipient(this.remindersGroup.value)
                        .subscribe(data => {
                            if(data){
                                this.globalS.sToast('Success','Data Updated')
                                this.reloadTab(6)
                            }
                        })
        }
    }

    deleteReminder(data: any){
        this.timeS.deleteremindersrecipient(data.recordNumber).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Data Deleted')
                this.reloadTab(6)
            }
        })
    }

    recurringChangeEvent(event: any){
        if(!event){
            this.remindersGroup.controls['address1'].setValue('')
            this.remindersGroup.controls['address2'].setValue('')
            this.remindersGroup.controls['address2'].disable()
            this.remindersGroup.controls['address1'].disable()
        } else {
            this.remindersGroup.controls['address2'].enable()
            this.remindersGroup.controls['address1'].enable()
        }
    }

    GetReferralSource(data: any): Observable<any> {
        var ObservableResult: Observable<any>;
        if(this.referTab == 1 || this.referTab == 2){
            ObservableResult = this.listS.getwizardreferralsource('default');
            
            var haccList: Array<any> = this.whatOptionVar.programsArr.filter(x => x.type == 'HACC' && x.status);        
            if(haccList && haccList.length > 0) ObservableResult = this.listS.getwizardreferralsource('HACC')        
    
            var decList: Array<any> = this.whatOptionVar.programsArr.filter(x => x.type == 'DEX' && x.status);
            if(decList && decList.length > 0)   ObservableResult = this.listS.getwizardreferralsource('DEX')        
        }

        if(this.referTab == 6){
            ObservableResult = this.listS.getlist(`SELECT DISTINCT Description, HACCCode, RecordNumber FROM DataDomains WHERE Domain = 'REASONCESSSERVICE'`);

            var haccList: Array<any> = this.whatOptionVar.programsArr.filter(x => x.type == 'HACC' && x.status);                 
            if(haccList && haccList.length > 0) ObservableResult = this.listS.getlist(`SELECT Description,HACCCode, RecordNumber FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' AND DATASET = 'HACC'`)           
        }

        return ObservableResult || EMPTY;
    }

    GetReferralCode(program : string = null): Observable<any>{
        if(this.referTab == 5){
            return this.listS.getlist(`SELECT [title] 
            FROM   itemtypes 
            WHERE  processclassification = 'OUTPUT' 
                   AND [rostergroup] IN ( 'ADMISSION' ) 
                   AND [minorgroup] IN ( 'OTHER', 'REVIEW' ) 
                   AND ( enddate IS NULL 
                          OR enddate >= '03-28-2019' ) 
            ORDER  BY [title] `)
        }

        if(this.referTab == 7){
            return this.listS.getlist(`SELECT title 
            FROM   itemtypes 
            WHERE  ( enddate IS NULL 
                    OR enddate >= '04-08-2019' ) 
                AND rostergroup = 'RECPTABSENCE' 
                AND status = 'ATTRIBUTABLE' 
                AND processclassification = 'EVENT' 
            ORDER  BY title`)
        }

        if(this.referTab == 8){
            return this.listS.getlist(`SELECT [service type] AS Activity 
            FROM   serviceoverview SO 
                   INNER JOIN humanresourcetypes HRT 
                           ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid 
            WHERE  HRT.[name] = '${program}' 
                   AND EXISTS (SELECT title 
                               FROM   itemtypes ITM 
                               WHERE  title = SO.[service type] 
                                      AND ITM.[rostergroup] IN ( 'ADMISSION' ) 
                                      AND ITM.[minorgroup] IN ( 'OTHER', 'REVIEW' ) 
                                      AND ( ITM.enddate IS NULL 
                                             OR ITM.enddate >= '04-12-2019' )) 
            ORDER  BY [service type] `)
        }
        return this.listS.getwizardreferralcode();
    }

    changeNoteEvent(note: string = ''){
        this.noteArray = []  
        
        this.referralGroup.value.noteCategory = this.whatOptionVar.wizardTitle;
        if(this.whatOptionVar.wizardTitle !== 'ADMIN'){
            this.referralGroup.patchValue({ noteCategory: this.whatOptionVar.wizardTitle })
        } else {
            // this.listS.getwizardnote(note).subscribe(data => this.noteArray = data)
            this.referralGroup.patchValue({ noteCategory: 'OTHER' })
        }
    } 

    branchesArr: Array<any>
    staffsArr: Array<string>
    multipleStaffModalOpen(){
        this.listS.getlistbranches()
            .subscribe(data => this.branchesArr = data.map(x => {
                return {
                    name: x,
                    status: false
                }
            }))
        
    }

    branchChangeEvent(state: boolean,index:number){
        var hehe = this.branchesArr.filter(x => x.status === true);
        var lists: Array<string> = [];

        for(var list of hehe){
            lists.push(list.name);
        }
        this.listS.getstaffofbranch(lists)
            .subscribe(data => this.staffsArr = data)
    }



    accept(){

        var checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status)
        var specialPrograms = checkedPrograms.filter(x => x.program === '*HCP REFERRAL' || x.program === '*NDIA REFERRAL')  
        

        if(specialPrograms.length > 0){
            this.heheOpen = false;
            this.packageTemplateModalOpen(specialPrograms[0].program);
            return;
        } else {
            this.CreateRosterAndNotes();
        }

    }

    saveDeath(){
        
        const checkedPrograms = this.selectedAdmitPrograms.filter(x => x.status);
        var referralGroup = this.referralGroup.value;
        const user = this.globalS.decode();
        const timeInMinutes = this.globalS.getMinutes(referralGroup.timeSpent)
        const timePercentage = (Math.floor(timeInMinutes/60 * 100) / 100).toString()

        var initialPrograms: Array<any> = [];

        for( var a = 0; a < checkedPrograms.length; a++){
            let data: Dto.ProcedureRoster = {         
                clientCode: this.user.code,
                carerCode: user.code,
                serviceType: referralGroup.referralType,
                program: checkedPrograms[a].title,
                date: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'),
                time: referralGroup.time,
                creator: user.user,
                editer: user.user,
                billUnit: 'HOUR',
                agencyDefinedGroup: this.user.agencyDefinedGroup,
                referralCode: referralGroup.referralCode,
                timePercent: timePercentage,
                Notes: '',
                type: 7,
                duration: timeInMinutes / 5,
                blockNo: timeInMinutes / 5,
                reasonType: '',
                tabType: 'DISCHARGE'                
            } 
            initialPrograms.push(data)
        }

        let final: Dto.CallDeceaseProcedure = {
            roster: initialPrograms,
            dateOfDeath: moment(referralGroup.dateOfDeath).format('YYYY-MM-DD 00:00:00:000'),
            note: {
                personId: this.user.id,
                program: this.globalS.isVarious(checkedPrograms),
                detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                extraDetail1: NOTE_TYPE[this.noteNumber],
                extraDetail2: 'DISCHARGE',
                whoCode: this.user.code,
                publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                creator: user.user,
                note: referralGroup.notes,
                alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                reminderTo: referralGroup.reminderTo
            }
        }

        this.listS.postdeath(final).subscribe(data => {
                        if(data)    this.successResult()
                    });
    }

    saveAdmission(){        
      
        var referralGroup = this.referralGroup.value;

        const user = this.globalS.decode();
        const blockNoTime = Math.floor(this.globalS.getMinutes(referralGroup.time)/5)
        const timeInMinutes = this.globalS.getMinutes(referralGroup.timeSpent)
        const timePercentage = (Math.floor(timeInMinutes/60 * 100) / 100).toString()

        let data = {
            programs: this.checkedPrograms,
            clientCode: this.user.code,
            carerCode: user.code,
            serviceType: referralGroup.referralType,
            date: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'), 
            time: referralGroup.time,
            creator: user.user,
            editer: user.user,
            billUnit: 'HOUR',
            agencyDefinedGroup: this.user.agencyDefinedGroup,
            referralCode: '',
            timePercentage: timePercentage,
            notes: '',
            type: 7,
            duration: timeInMinutes / 5,
            blockNo: blockNoTime,
            reasonType: '',
            tabType: 'ADMISSION',
            noteDetails: {
                personId: this.user.id,
                program: this.globalS.isVarious(this.checkedPrograms),
                detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                extraDetail1: NOTE_TYPE[this.noteNumber],
                extraDetail2: 'ADMISSION',
                whoCode: this.user.code,
                publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                creator: user.user,
                note: referralGroup.notes,
                alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                reminderTo: referralGroup.reminderTo
            }
        }

        this.listS.postadmission(data).subscribe(data => {
            if(data)    this.successResult()
        })      
    }

    packageTemplates: Array<any>;
    packageCode: string = ''
    selectedTemplate: any;

    packageTemplateModalOpen(name: string){
        this.modalPackageTemplate.open();
        this.newPackageName = name == '*HCP REFERRAL' ? 'DOHA' : name == '*NDIA REFERRAL' ? 'NDIA' : null;
        this.listS.getpackagetemplate(this.newPackageName)
            .subscribe(data => {
                this.packageTemplates = data.map((x,index) => {
                    return {
                        name: x.name,
                        packageLevel: x.packageLevel,
                        status: false
                    }
                })
                this.changeTemplate(true, 0);
                //this.packageTemplateEvent(type, this.packageTemplates);
            })        
    }

    newPackageName: string


    packageTemplateEvent(name: string, index: number): void{
        this.packageCode = ''
        if(name === 'DOHA'){
            var level  = '';
            if((this.packageTemplates[index].packageLevel).split(' ').length == 1){
                level = ''
            } else {
                level = '-L' +(this.packageTemplates[index].packageLevel).split(' ')[1]
            }
            this.packageCode =  'HCP' + level + " " + this.user.code
        }
        
        if(name === 'NDIA'){          
            this.packageCode =  'NDIA'  + " " + this.user.code;       
        }

        this.NDIA_HCP = {
            sourcePackage: this.packageTemplates[index].name,
            level: this.packageTemplates[index].packageLevel,
            type: name            
        }

        this.checkPackageCodeIfExist(this.packageCode);
    }

    checkPackageCodeIfExist(packageCode: string){
        this.packageCode = packageCode;
        this.listS.getlist(`SELECT COUNT(Name) AS Count FROM HumanResourceTypes WHERE [Name] = '${packageCode}'`)
            .subscribe(data => {
                this.doPackageExist = data[0].Count > 0 ? true : false;
            })
    }

    async changeTemplate(state: boolean, index: number){       
        await this.loopAndDisableChckBoxes(state, index);
    }

    loopAndDisableChckBoxes(state: boolean, index: number){
        for(var a = 0, len = this.packageTemplates.length ; a < len ; a++){
            this.packageTemplates[a].status = false;
        }
        this.packageTemplates[index].status = state
        this.selectedTemplate = this.packageTemplates[index];
        this.packageTemplateEvent(this.newPackageName, index)
    }

    NDIA_HCP: any
    saveNewPackageName(){      
        if(!this.doPackageExist){         
            this.NDIA_HCP.newPackageName = this.packageCode;
            this.CreateRosterAndNotes(true);
        } else {
            this.globalS.eToast('Error','An error occured');
        }
    }

    isSpecialProgram(program: string){
        return program == '*HCP REFERRAL' || program == '*NDIA REFERRAL';
    }
 
    CreateRosterAndNotes(isNDIAOrHCP: boolean = false){
        var referralGroup = this.referralGroup.value;
        const blockNoTime = Math.floor(this.globalS.getMinutes(referralGroup.time)/5)
        const timeInMinutes = this.globalS.getMinutes(referralGroup.timeSpent)
        const timePercentage = (Math.floor(timeInMinutes/60 * 100) / 100).toString()
        const user = this.globalS.decode();

        let defaultValues = {
            // '' is the default
            billDesc: '',
            
            // 7 for everything except 14 for item
            type: '7',          

            // 2 is the default,
            rStatus: '2',    

            // HOUR is the default     
            billUnit: 'HOUR',

            // 0 is the default  
            billType: '0',

            // '' is the default
            payType: '',

            // '' is the default
            payRate: '',

            // '' is the default
            payUnit: '',

            // '' is the default
            apInvoiceDate: '',

            // '' is the default
            apInvoiceNumber: '',

            // 0 is default
            groupActivity: '0',

            // '' is the default
            serviceSetting: ''
        }

        var checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status)
        var finalRoster: Array<Dto.ProcedureRoster> = [];

        if(this.referTab == 1){
            
            for(var a = 0, len = this.checkedPrograms.length; a < len ; a++){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: this.checkedPrograms[a].selected,
                    date: moment(referralGroup.date).format('YYYY/MM/DD'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: referralGroup.referralSource,
                    tabType: 'REFERRAL-IN',
                    program: this.isSpecialProgram(this.checkedPrograms[a].program) ? this.NDIA_HCP.newPackageName : this.checkedPrograms[a].program,
                    packageStatus: 'REFERRAL'
                }
                finalRoster.push(data);
            }

            let data: Dto.CallProcedure = {
                isNDIAHCP: isNDIAOrHCP,
                oldPackage: isNDIAOrHCP ? this.NDIA_HCP.sourcePackage : '',
                newPackage: isNDIAOrHCP ? this.NDIA_HCP.newPackageName : '',
                level: isNDIAOrHCP ? this.NDIA_HCP.level : '',
                type: isNDIAOrHCP ? this.NDIA_HCP.type: '',
                roster: finalRoster,
                staffNote: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: 'REFERRAL-IN',
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }   

            // this.listS.postreferralin(data).subscribe(data => {
            //     if(data)    this.successResult()
            // });            
        }

        if(this.referTab == 2){

            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: checkProgram.selected,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: referralGroup.referralSource,
                    tabType: 'REFERRAL-OUT',
                    program: checkProgram.program
                }
                finalRoster.push(data);
            }

            let data: Dto.CallReferralOutProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: 'REFERRAL-OUT',
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }
            this.listS.postreferralout(data)
                .subscribe(data => {
                    if(data)    this.successResult()
                })
        }

        // NOT PROCEEDING
        if(this.referTab == 3){

            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: checkProgram.selected,
                    date: moment(referralGroup.date).format('YYYY/MM/DD'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: '',
                    tabType: 'REFERRAL-IN',
                    program: checkProgram.program
                }
                finalRoster.push(data)
            }
            
            let data: Dto.CallReferralOutProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: 'REFERRAL-IN',
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }       

            this.listS.postnotproceed(data).subscribe(data => {
                if(data)    this.successResult()
            })
        }

        // ASSESSMENT
        if(this.referTab == 4){
            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: checkProgram.selected,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: '',
                    program: checkProgram.program,
                    tabType: 'ASSESSMENT'
                }
                finalRoster.push(data);
            }

            let data: Dto.CallAssessmentProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: 'SCREEN/ASSESS',
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }
            this.listS.postassessment(data).subscribe(data =>{
                if(data)    this.successResult()
            })
        }

        // WAITLIST 
        if(this.referTab == 5){

            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: referralGroup.referralCode,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: '',
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: '',
                    program: checkProgram.program,
                    tabType: 'WAITLIST'           
                }
                finalRoster.push(data)
            }

            let data: Dto.CallReferralOutProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: this.referralGroup.get('noteCategory').value,
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            } 
            this.listS.postwaitlist(data).subscribe(data => {
                if(data)    this.successResult()
            })
        }

        // DISCHARGE
        if(this.referTab == 6){

            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: checkProgram.selected,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: referralGroup.referralSource,
                    program: checkProgram.program,
                    tabType: 'DISCHARGE'    
                }
                finalRoster.push(data);
            }

            let data: Dto.CallAssessmentProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.packageCode,
                    detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: 'DISCHARGE',
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }
            this.listS.postdischarge(data)
                .subscribe(data => {
                    if(data)    this.successResult()
                })

        }

        // ADMINISTRATION
        if(this.referTab == 8){
            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: checkProgram.selected,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: referralGroup.notes,
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: referralGroup.referralSource,
                    program: checkProgram.program,
                    tabType: this.referralGroup.get('noteCategory').value       
                }
                finalRoster.push(data);
            }
            let data: Dto.CallAssessmentProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: this.referralGroup.get('noteCategory').value,
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }
            console.log(data);
            // this.listS.postadministration(data)
            //     .subscribe(data => {
            //         if(data)    this.successResult()
            //     })
        }

        // ITEM
        if(this.referTab == 9){
            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: referralGroup.referralType,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),

                    // no time in item
                    time: '00:00',

                    creator: user.user,
                    editer: user.user,
                    billUnit: 'SERVICE',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,

                    timePercent: referralGroup.quantity,
                    costUnit: referralGroup.quantityTitle,

                    Notes: `${this.referralGroup.get('noteCategory').value}-${referralGroup.notes}`,
                    billDesc: `${referralGroup.notes}`,
                    unitBillRate: referralGroup.charge,

                    // type 14 in item
                    type: 14, 

                    duration: timeInMinutes / 5,

                    //item 0 in item
                    blockNo: 0, 

                    apiInvoiceDate: referralGroup.suppDate != '' ? moment(referralGroup.suppDate).format('YYYY/MM/DD h:mm:ss a'): '',
                    apiInvoiceNumber: referralGroup.suppRefNo,

                    reasonType: referralGroup.referralSource,
                    program: checkProgram.program,
                    tabType: 'ITEM'
                }

                finalRoster.push(data);
            }

            let data: Dto.CallAssessmentProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.globalS.isVarious(this.checkedPrograms),
                    detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: this.referralGroup.get('noteCategory').value,
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }

            // console.log(data);


            this.listS.postitem(data)
                .subscribe(data => {
                    if(data)    this.successResult('Item Added')
                })
        }


        // REINSTATE
        if(this.referTab == 11){
            for(var checkProgram of this.checkedPrograms){
                let data: Dto.ProcedureRoster = {
                    clientCode: this.user.code,
                    carerCode: user.code,
                    serviceType: this.reinstateDetails.serviceType,
                    date: moment(referralGroup.date).format('YYYY/MM/DD h:mm:ss a'),
                    time: referralGroup.time,
                    creator: user.user,
                    editer: user.user,
                    billUnit: 'HOUR',
                    agencyDefinedGroup: this.user.agencyDefinedGroup,
                    referralCode: referralGroup.referralCode,
                    timePercent: timePercentage,
                    Notes: '',
                    type: 7,
                    duration: timeInMinutes / 5,
                    blockNo: blockNoTime,
                    reasonType: referralGroup.referralSource,
                    program: this.reinstateDetails.programName,
                    tabType: this.referralGroup.get('noteCategory').value 
                }
                finalRoster.push(data);
            }

            let data: Dto.CallAssessmentProcedure = {
                roster: finalRoster,
                note: {
                    personId: this.user.id,
                    program: this.packageCode,
                    detailDate: moment(referralGroup.date).format('MM-DD-YYYY HH:mm:ss'),
                    extraDetail1: NOTE_TYPE[this.noteNumber],
                    extraDetail2: this.referralGroup.get('noteCategory').value,
                    whoCode: this.user.code,
                    publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                    creator: user.user,
                    note: referralGroup.notes,
                    alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                    reminderTo: referralGroup.reminderTo
                }
            }
            
            this.listS.postreinstate(data)
                .subscribe(data => {
                    if(data)    this.successResult()
                })
        }
    }

    successResult(message: string = 'Transaction Complete'){
        this.globalS.sToast('Success', message);
        this.statusTab.next(this.user)
    }

    clearOptionModal(){
        this.whatOptionVar = {
            title: '',
            wizardTitle: '',
            programsArr: [],
            referralTypesArr: [],
            referralCodeArr: [],
            //referralSourceArr: data[3],
            reminderToArr: []
        }
    }

    forceDisplay(state: boolean){
        if(state){
            this.listS.getlist(`SELECT DISTINCT UPPER([Name]) AS Program, type FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= '03-18-2019')`)
            .subscribe(data => {
                this.whatOptionVar = {
                    programsArr: data.map(x => {
                        return {
                            program: x.Program,
                            type: x.type,
                            status: false
                        }
                    }),
                   
                }
            })
        } else {
            this.whatOptionVar = this._whatOptionVar
        }
    }

    doNextAdmitProgram(buttonType: string): void {
        if ("custom-next" === buttonType ) {
            this.getAdmitDetails();
            this.modalAdmitPrograms.next();
        }
    }

    doCustomClick(buttonType: string): void {
        if ("custom-next" === buttonType) {
            this.getOtherDetails();
            this.wizard.next();
        }
    }

    checkedPrograms: Array<any> = []
    _checkedPrograms: Array<any> = []

    pickedProgram(data: any){
        this.checkedPrograms[data.index] = data
    }

    getAdmitDetails(){
        this.checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status).map((x, index, arr) => {
            return {
                program: this.referTab == 10 ? x.title : x.program,
                index: index,
                tab: this.referTab,
                checkedProgram: arr,
                admissionType: '',
                approvedServices: []
            }
        })
    }

    getOtherDetails(){     
        this.checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status).map((x, index, arr) => {
            return {
                program: x.program,
                index: index,
                tab: this.referTab,
                checkedProgram: arr,
                selected: ''
            }
        })
        this.getReferralList(this.checkedPrograms);
    }

    async showWhenProgramIsTicked(state: boolean, index: number){  
        await this._showWhenProgramIsTicked(state, index);
    }

    reinstateDetails: any;

    _showWhenProgramIsTicked(state: boolean, index: number){

        if(this.referTab == 1 || this.referTab == 2){
            if(this.whatOptionVar.programsArr[index].program === '*HCP REFERRAL'){
                    let seachedIndex = this.globalS.findIndex('*NDIA REFERRAL', this.whatOptionVar.programsArr, 'program')
                    this.whatOptionVar.programsArr[seachedIndex].disabled = state
            }
            if(this.whatOptionVar.programsArr[index].program === '*NDIA REFERRAL'){     
                    let seachedIndex = this.globalS.findIndex('*HCP REFERRAL', this.whatOptionVar.programsArr, 'program')
                    this.whatOptionVar.programsArr[seachedIndex].disabled = state
            }
        }

        if(this.referTab == 11){
            this.reinstateDetails = {
                serviceType: this.whatOptionVar.programsArr[index].serviceType,
                program: this.whatOptionVar.programsArr[index].program,
                programName: this.whatOptionVar.programsArr[index].programName
            }
        }

        this.programTicked = true  
    }

    getReferralList(programs: Array<any>){

        this.referralVariables = {
            referralSourceArr: [],
            referralCodeArr: [],
            referralTypeArr: []
        }

        if(this.referTab == 9){
            this.sqlWizS.GetReferralType(programs[0]).subscribe(data => this.referralVariables.referralTypeArr = data)
        }

        this.sqlWizS.GetReferralSource(programs[0]).subscribe(data => this.referralVariables.referralSourceArr = data)

        this.sqlWizS.GetReferralCode(programs[0]).subscribe(data => {
                if(this.referTab == 7){
                    this.referralVariables.referralCodeArr = data.map(x => x.title)
                    return;
                }
                if(this.referTab == 8){
                    this.referralVariables.referralCodeArr = data.map(x => x.Activity)
                    return;
                }                 
                this.referralVariables.referralCodeArr = data           
        })

    }

    showThisOnTab_A(): boolean{
        return this.referTab == 5 || this.referTab == 7  || this.referTab == 8 || this.referTab == 9;
    }

    noteChangeEvent(index: number){                
        if( this.referralGroup.value.notes != '' && this.showThisOnTab_A()){
            this.referralGroup.patchValue({ noteCategory: '' })
            this.listS.getcasenotecategory(index).subscribe(data => this.noteArray = data);
            return;
        }
        this.referralGroup.patchValue({ noteCategory: this.whatOptionVar.wizardTitle })        
    }
    
    referralTypesArr: Array<any> = []
    getServiceType(program: string, tabType: string){
       
        if(this.referTab == 1){
            this.listS.getwizardreferraltypes({
                Program: '*HCP REFERRAL',
                TabType: tabType
            }).subscribe(data => this.referralTypesArr = data)
        }
    }

    servicesList: Array<any>;

    services(){
        this.timeS.getactiveservices({
            RecipientCode: 'ABBERTON B'
        }).subscribe(services => {
            this.servicesList = services;
        })
        this.servicesOpen = true;
    }

    serviceEventChange(event, data: any){

        this.timeS.updatetactiveservices({
            Recnum: data.recnum,
            ExcludeFromClientPortal: data.excludeFromClientPortal
        }).subscribe(data => console.log(data));

    }
    userReferIn: any;
    openReferModal(user: any){
        this.sharedS.emitOnSearchListNext(user.code);
        console.log(user);
        this.userReferIn = user;
        this.referOpen = {}
        // this.openRefer = !this.openRefer;
    }

    openDocumentQuote: boolean = false;
    documentQuote: any;
    selectedQuoteIndex: number;

    detailsQuotes(quote: any){
        this.documentQuote = quote;
        this.openDocumentQuote = !this.openDocumentQuote;
    }

    displayLastChangeEvent(data: any){
        if(!data || data < 0){
            this.displayLast = 0
        } else {
            this.displayLast = data;
        }
        
        this.changeTab.next(15)
    }

    quoteProgramList: Array<any> = [];
    quoteTemplateList: Array<any> = [];

    quoteModalOpenProcess(){
        this.resetGroup();
        this.wizardQuote.reset();
        this.listS.getprogramcontingency(this.user.id).subscribe(data => this.quoteProgramList = data.map(x => x.toUpperCase()));
        this.listS.getglobaltemplate().subscribe(data => this.quoteTemplateList = data.map(x => x.toUpperCase()))

        this.quoteModalOpen = true;
    }

    nextQuote(){
        this.documentQuote = '';
        this.openDocumentQuote = !this.openDocumentQuote;
    }


}
