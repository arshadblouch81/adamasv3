namespace Adamas.Models.Modules{
    public class ClientNote {
        public string RecipientCode { get; set; }
        public string PersonId { get; set; }
        public string OperatorID { get; set; }
        public string Note { get; set; }
        public string Note_Type { get; set; }
    }
}