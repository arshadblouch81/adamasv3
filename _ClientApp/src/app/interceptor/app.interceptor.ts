import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse }  from '@angular/common/http';
import { Router } from '@angular/router'

import { GlobalService } from '@services/index';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable()
export class AppInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private globalS: GlobalService
  ){

  }

  intercept(req: HttpRequest<any>,next: HttpHandler): Observable<HttpEvent<any>> {

      req = req.clone({
        setHeaders: {
          RequestToken: `12312312312312312`
        }
      });

    return next.handle(req).pipe(
      map(evt => {
        if (evt instanceof HttpResponse) {
          //console.log('---> status:', evt);
          // console.log('---> filter:', req.params.get('filter'));
        }
        return evt;
      }),
       catchError((error: HttpErrorResponse) => {

          let data = {};
          
          data = {
              reason: error.statusText,
              status: error.status,
              error: error.error
          };

          if((data as any).reason == 'Unauthorized' && (data as any).status == 401 || (data as any).status == 403 && (data as any).error == 'Error Unauthorized'){
            this.globalS.clearTokens();
            this.router.navigate(['']);
          }
          //this.errorDialogService.openDialog(data);
          return throwError(error);
       })
    );

  }
}