import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';



import {
  ProfilePage
} from '@components/index';

import {
  MembersComponent
} from './components/members/members.component'

import { HomeClientManager } from './pages/client-manager/home';
import { ProfileClientManager } from './pages/client-manager/profile';
import { BookingClientManager } from './pages/client-manager/booking';
import { CalendarClientManager } from './pages/client-manager/calendar';
import { DocumentClientManager } from './pages/client-manager/document';
import { HistoryClientManager } from './pages/client-manager/history';
import { NotesClientManager } from './pages/client-manager/notes';
import { PackageClientManager } from './pages/client-manager/package';
import { PreferencesClientManager } from './pages/client-manager/preferences';
import { ShiftClientManager } from './pages/client-manager/shift';
import { SettingsClientManager } from './pages/client-manager/settings';
// Docusign
import {  DocusignComponent } from './pages/docusign/docusign';

import {
  UnauthorizedComponent
} from './pages/unauthorized/unauthorized';

import {
  HomeV2Admin as HomeStaffRedirect,
  StaffAdmin as StaffRedirect
} from './pages/staff-direct/index'

import {
  StaffPersonalAdmin as StaffPersonalAdminRedirect,
  StaffContactAdmin as StaffContactAdminRedirect,
  StaffPayAdmin as StaffPayAdminRedirect,
  StaffLeaveAdmin as StaffLeaveAdminRedirect,
  StaffReminderAdmin as StaffReminderAdminRedirect,
  StaffOPAdmin as StaffOPAdminRedirect,
  StaffHRAdmin as StaffHRAdminRedirect,
  StaffCompetenciesAdmin as StaffCompetenciesAdminRedirect,
  StaffTrainingAdmin as StaffTrainingAdminRedirect,
  StaffIncidentAdmin as StaffIncidentAdminRedirect,
  StaffDocumentAdmin as StaffDocumentAdminRedirect,
  StaffAttendanceAdmin as StaffAttendanceAdminRedirect,
  StaffPositionAdmin  as StaffPositionAdminRedirect,
  StaffGroupingsAdmin as StaffGroupingsAdminRedirect,
  StaffLoansAdmin     as StaffLoansAdminRedirect,
} from './pages/staff-direct/staff-views/index'


import {
  HomeClient,
  NotesClient,
  HistoryClient,
  BookingClient,
  DocumentClient,
  PackageClient,
  PreferencesClient,
  ShiftClient,
  CalendarClient,
  ProfileClient
} from '@client/index'

import {
  ProfileProvider,
  HomeProvider,
  CalendarProvider,
  DocumentProvider,
  HistoryProvider,
  LeaveProvider,
  PackageProvider
} from '@provider/index';

import {
  AttendanceAdmin,
  DayManagerAdmin,
  DayManagerMain,
  HomeAdmin,
  HomeV2Admin,
  LandingAdmin,
  SideMainMenu,
  RecipientsAdmin,
  ReportsAdmin,
  UserReports,
  SessionsAdmin,
  StaffAdmin,
  TimesheetAdmin,
  ConfigurationAdmin,
  NDIAAdmin,
  chspDexAdmin,
  HCPComponent,
  PrintComponent,
  // BudgetAdmin, //AHSAN
  // GlanceAdmin, //AHSAN
  BillingAdmin,   //AHSAN
  DashboardAdmin, //AHSAN
  TimesheetProcessingAdmin,
  AccountingAdmin, //AHSAN
  PurchaseOrder,
  Transport,
  ChatBot,
  XeroProcesses,
  
  VehicleAvailability,
  StaffAvailability,
  AIAssistant
 

} from '@admin/index'

import {
  StaffAttendanceAdmin,
  StaffCompetenciesAdmin,
  StaffContactAdmin,
  StaffDocumentAdmin,
  StaffGroupingsAdmin,
  StaffLoansAdmin,
  StaffHRAdmin,
  StaffPSAdmin,
  StaffIncidentAdmin,
  StaffLeaveAdmin,
  StaffOPAdmin,
  StaffPayAdmin,
  StaffPersonalAdmin,
  StaffPositionAdmin,
  StaffhistoryAdmin,
  StaffReminderAdmin,
  StaffTrainingAdmin,
  StaffInformationAdmin
} from './pages/admin/staff-views/index';

import {
  RecipientCasenoteAdmin,
  RecipientContactsAdmin,
  RecipientHistoryAdmin,
  RecipientIncidentAdmin,
  RecipientIntakeAdmin,
  RecipientClinicalAdmin,
  RecipientDatasetAdmin,
  RecipientOpnoteAdmin,
  RecipientPensionAdmin,
  RecipientPermrosterAdmin,
  RecipientPersonalAdmin,
  RecipientQuotesAdmin,
  RecipientRemindersAdmin,
  RecipientFormsAdmin,
  RecipientDocumentsAdmin,
  RecipientAttendanceAdmin,
  RecipientOthersAdmin,
  RecipientAccountingAdmin,
  RecipientShiftReportsAdmin,
  RecipientSuspensionAdmin,
  RecipientLoans,
  ContactsAdmin, 
  OthersContact,
  
} from './pages/admin/recipient-views/index'

import {

  ClaimratesComponent,
  TargetgroupsComponent,
  PurposestatementComponent,
  BudgetgroupsComponent,
  BudgetsComponent,
  ContactgroupsComponent,
  ContacttypesComponent,
  AddresstypesComponent,
  OccupationComponent,
  ReligionComponent,
  PhoneemailtypesComponent,
  FinancialclassComponent,
  DesktopUsersComponent,
  CustomDatasets,
  PostcodesComponent,
  HolidaysComponent,
  MedicalcontactComponent,
  DestinationaddressComponent,
  ProgramcoordinatesComponent,
  DistributionlistComponent,
  NotificationlistComponent,
  FollowupComponent,
  InitialactionsComponent,
  OngoingactionsComponent,
  IncidenttriggersComponent,
  IncidenttypesComponent,
  IncidentsubcatComponent,
  IncidentnotecategoryComponent,
  LocationCategoriesComponent,
  StaffincidentnotecategoryComponent,
  FillingclassificationComponent,
  DocumentcategoriesComponent,
  RecipientsCategoryComponent,
  RecipientsGroupComponent,
  RecipientsMinorGroupComponent,
  RecipientsBillingCyclesComponent,
  DebtorTermsComponent,
  RecipientGoalsComponent,
  ConsentTypesComponent,
  CarePlanTypesComponent,
  ClinicalNotesGroupsComponent,
  CaseNoteCategoriesComponent,
  OpNoteCategoriesComponent,
  CareDomainsComponent,
  DischargeReasonsComponent,
  RefferalReasonsComponent,
  UserDefinedRemindersComponent,
  RecipientPrefrencesComponent,
  
  ReferralSourcesComponent,
  LifecycleEventsComponent,
  JobCategoryComponent,
  AdminCategoryComponent,
  StaffUserGroupsComponent,
  AwardLevelsComponent,
  AwardSetupComponent,
  StaffCompetencyGroupComponent,
  StaffNotesCategoriesComponent,
  StaffPositionsComponent,
  StaffTeamsComponent,
  AwardDetailsComponent,
  OpStaffNotesComponent,
  StaffRemindersComponent,
  ServiceDeciplinesComponent,
  StaffPreferencesComponent,
  LeaveDescriptionsComponent,
  ServiceNoteCategoriesComponent,
  VehiclesComponent,
  ActivityGroupsComponent,
  EquipmentsComponent,
  StaffCompetenciesComponent,
  CentrFacilityLocationComponent,
  FundingSourcesComponent,
  PayTypeComponent,
  ProgramPackagesComponent,
  ServicesComponent,
  ItemsConsumablesComponent,
  MenuMealsComponent,
  CaseMangementAdminComponent,
  StaffAdminActivitiesComponent,
  RecipientAbsenceComponent,
  CompaniesComponent,
  DocumentTemplateComponent,
  PortalUsers,
  NursingDignosisComponent,
  MedicalDignosisComponent,
  TasksComponent,
  ClinicalRemindersComponent,
  ClinicalAlertsComponent,
  MedicationsComponent,
  MobilityCodesComponent,
  HealthConditionsComponent,
  AdmittingPrioritiesComponent,
  ServicenotecatComponent,
  Othercontactaddress,
  ConfigurationMain
} from './pages/admin/configuration/index'



import {
  RouteGuard,
  AdminStaffRouteGuard,
  CanDeactivateGuard,
  LoginGuard,
  AdminRouteGuard,
  ByPassGuard,
  ClientGuard
} from '@services/index'

import {
  IntakeAlerts,
  IntakeBranches,
  IntakeConsents,
  IntakeFunding,
  IntakeGoals,
  IntakeGroups,
  IntakePlans,
  IntakeServices,
  IntakeStaff,
  IntakePrefStaff,
  ServicesPlan,
  IntakeCompetencies,
  IntakePlacement
  
} from '@intakes/index';

import
{
  ChildSafety,
  DatasetQccsmda,
  DSS,
  MentalHlth,
  CHSPDEX,
  QCSSHAC
} from './pages/admin/recipient-views/dataset-views/index';

import {
  PhysicalHealth,
  ClinicalDiagnose,
  ClinicalProcedure,
  ClinicalMedication,
  ClinicalReminder,
  ClinicalAlert,
  ClinicalNote,
  ClinicalHealthConditions,
  ClinicalVaccinations,
  ClinicalNursingDiagnose,
  ClinicalMedicalDiagnose,
  ClinicalHistoryComponent,
  ClinicalPrimaryIssues,
  ClinicalOtherIssues,
  ClinicalCurrentServices,
  ClinicalDailyLiving,
  ClinicalCareerStatus,
  ClinicalActionPlans,
  ClinicalMentalHealth,
  ClinicalHealthBehaviours,
  
} from './pages/admin/recipient-views/clinical-views/index';

import {
  ProfileAccounting,
  AccountingHistory,
  Invoices,
  Receipts
} from '@accounting/index';


import {
  RostersAdmin,ShiftDetail,AddRoster,RecipientExternal,StaffExternal,GNotes,CarerSearch,StaffSearch,RecipientSearch,DMRoster
  ,ServiceTasks, RosterExtraInfo,CancelShift,UnallocateStaff,RosterMain
} from './pages/roster/index';

import { ExtraComponent } from './pages/extra/extra';


import { MedicalProceduresComponent } from '@admin/recipient-views/medical-procedures.component';

import { DebtorComponent } from '@admin/billing/debtor.component'; //AHSAN
import { TravelComponent } from '@admin/pays/travel.component'; //AHSAN
import { PayComponent } from '@admin/pays/pay.component'; //AHSAN
import { WorkHoursComponent } from '@admin/pays/workHours.component'; //AHSAN
import { PayIntegrityComponent } from '@admin/pays/payIntegrity.component'; //AHSAN
import { CloseRosterComponent } from '@admin/pays/closeRoster.component'; //AHSAN
import { PayPeriodDateComponent } from '@admin/pays/payPeriodDate.component'; //AHSAN
import { RollbackInvoiceComponent } from '@admin/billing/rollbackInvoice.component'; //AHSAN
import { RollbackPayrollComponent } from '@admin/pays/rollbackPayroll.component'; //AHSAN
import { RollbackRosterComponent } from '@admin/billing/rollbackRoster.component'; //AHSAN
import { BudgetAdmin } from '@admin/dashboards/analyse-budget.component'; //AHSAN
import { GlanceAdmin } from '@admin/dashboards/glance.component'; //AHSAN
import { MediaList } from '@admin/recipient-views/mediaList';
import { UserDetail } from '@admin/configuration/genrel-setup/userdetail';
import { Checklist } from '@admin/configuration/genrel-setup/checklist';
import { RecipientComponent } from './pages/standalone-app/recipient/recipient.component';
import { FormIndex } from './pages/forms/index';
import { IntakeSelectability } from './pages/forms/selectability/intake';
import { ServiceSelectability } from './pages/forms/selectability/service-agreement';


const lazyRoutes: Routes = [
   {
    path: '',
    component: LoginComponent,
    canActivate: [ByPassGuard]
   },
   {
    path: '',
    component: HomeProvider,
    children:[
        {
            path: 'provider',
            loadChildren: () => import('./pages/provider/provider.module').then(m => m.ProviderModule)
        }
    ]
   },
   {
    path: '',
    component: HomeClient,
    canActivate: [RouteGuard],
    children:[
        {
            path: 'client',
            loadChildren: () => import('./pages/client/client.module').then(m => m.ClientModule)
        }
    ]
   },
   {
        path: '',
        component: HomeV2Admin,
        canActivate: [AdminStaffRouteGuard],
        children:[
            {
                path: 'admin',                
               loadChildren: () => import('./pages/admin/admin-main.module').then(m => m.AdminMainModule)
            }
        ]
   },
   {
    path: 'forms',
    component: FormIndex,
    children:[
                {
                    path: 'selectability',
                    loadChildren: () => import('./pages/forms/selectability/selectability.module').then(m => m.SelectabilityModule)
                }
      ]
   },
   {
    path: 'extra',
    component: ExtraComponent
  },
  {
    path: 'traccsadmin',
    component: StaffAdmin
  },
  {
    path: 'docusign',
    component: DocusignComponent
  },
 

]

@NgModule({
  imports: [RouterModule.forRoot(lazyRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const PAGE_COMPONENTS = [

  // Importing Client
  HomeClient,
  NotesClient,
  HistoryClient,
  BookingClient,
  DocumentClient,
  PackageClient,
  PreferencesClient,
  ShiftClient,
  CalendarClient,
  ProfileClient,

  // Importing Providers
  ProfileProvider,
  HomeProvider,
  CalendarProvider,
  DocumentProvider,
  HistoryProvider,
  LeaveProvider,
  PackageProvider,

  // Importing Admin
  AttendanceAdmin,
  DayManagerAdmin,  

  HomeAdmin,
  HomeV2Admin,
  SideMainMenu,
  LandingAdmin,
  RecipientsAdmin,
  ReportsAdmin,
  UserReports,
  SessionsAdmin,
  StaffAdmin,
  TimesheetAdmin,
  ConfigurationAdmin,
  NDIAAdmin,
  chspDexAdmin,
  HCPComponent,
  PrintComponent,
  BudgetAdmin, //AHSAN
  GlanceAdmin, //AHSAN
  BillingAdmin, //AHSAN
  DashboardAdmin, //AHSAN
  TimesheetProcessingAdmin, //AHSAN
  AccountingAdmin,

  // Components
  ProfilePage,
  //Roster Module
  RostersAdmin,
  ShiftDetail,
  AddRoster,
  RecipientExternal,
  StaffExternal,
  GNotes,
  CarerSearch,
  StaffSearch,
  RecipientSearch,
  DMRoster,
  ServiceTasks,
  RosterExtraInfo,
  CancelShift,
  UnallocateStaff,
  RosterMain,
  //Configuration
  // CompaniesComponent,
  // BranchesComponent,
  // FundingRegionsComponent,
  // ClaimratesComponent,
  // TargetgroupsComponent,
  UserDetail,
  // PurposestatementComponent,
  HCPComponent,
  DebtorComponent, //AHSAN
  TravelComponent, //AHSAN
  PayComponent, //AHSAN
  WorkHoursComponent, //AHSAN
  PayIntegrityComponent, //AHSAN
  CloseRosterComponent, //AHSAN
  PayPeriodDateComponent, //AHSAN
  RollbackInvoiceComponent, //AHSAN
  RollbackPayrollComponent, //AHSAN
  RollbackRosterComponent, //AHSAN
  Checklist,  
  MedicalProceduresComponent,  
  StaffAttendanceAdmin,
  StaffCompetenciesAdmin,
  StaffContactAdmin,
  StaffDocumentAdmin,
  StaffGroupingsAdmin,
  StaffLoansAdmin,
  StaffHRAdmin,
  StaffPSAdmin,
  StaffIncidentAdmin,
  StaffLeaveAdmin,
  StaffOPAdmin,
  StaffPayAdmin,
  StaffPersonalAdmin,
  StaffPositionAdmin,
  StaffhistoryAdmin,
  StaffReminderAdmin,
  StaffTrainingAdmin,
  StaffInformationAdmin,
  // Recipient Views
  RecipientCasenoteAdmin,
  RecipientContactsAdmin,
  RecipientHistoryAdmin,
  RecipientIncidentAdmin,
  RecipientIntakeAdmin,
  RecipientClinicalAdmin,
  RecipientDatasetAdmin,
  RecipientOpnoteAdmin,
  RecipientPensionAdmin,
  RecipientPermrosterAdmin,
  RecipientPersonalAdmin,
  RecipientQuotesAdmin,
  MediaList,
  RecipientRemindersAdmin,
  RecipientFormsAdmin,
  RecipientDocumentsAdmin,
  RecipientAttendanceAdmin,
  RecipientOthersAdmin,
  RecipientAccountingAdmin,
  RecipientShiftReportsAdmin,
  RecipientSuspensionAdmin,


  // Intake Views
  IntakeAlerts,
  IntakeBranches,
  IntakeConsents,
  IntakeFunding,
  IntakeGoals,
  IntakeGroups,
  IntakePlans,
  IntakeServices,
  IntakeStaff,
  IntakePrefStaff,
  ServicesPlan,
  IntakeCompetencies,
  IntakePlacement,

  //dataset
  DatasetQccsmda,
  ChildSafety,
  DSS,
  MentalHlth,
  CHSPDEX,
  QCSSHAC,
  ExtraComponent,
  UnauthorizedComponent,

  // Clinical
  PhysicalHealth,
  ClinicalDiagnose,
  ClinicalProcedure,
  ClinicalMedication,
  ClinicalReminder,
  ClinicalAlert,
  ClinicalNote,
  ClinicalHealthConditions,
  ClinicalVaccinations,
  ClinicalNursingDiagnose,
  ClinicalMedicalDiagnose,
  ClinicalHistoryComponent,
  ClinicalMentalHealth,
  ClinicalHealthBehaviours,

  // Clinical Primary Issues
  ClinicalPrimaryIssues,
  ClinicalOtherIssues,
  ClinicalCurrentServices,
  ClinicalDailyLiving,
  ClinicalCareerStatus,
  ClinicalActionPlans,

  // Client Manager
  HomeClientManager,
  ProfileClientManager,
  BookingClientManager,
  CalendarClientManager,
  DocumentClientManager,
  HistoryClientManager,
  NotesClientManager,
  PackageClientManager,
  PreferencesClientManager,
  ShiftClientManager,

  //Staff Redirect
  HomeStaffRedirect,
  StaffRedirect,

  StaffPersonalAdminRedirect,
  StaffContactAdminRedirect,
  StaffPayAdminRedirect,
  StaffLeaveAdminRedirect,
  StaffReminderAdminRedirect,
  StaffOPAdminRedirect,
  StaffHRAdminRedirect,
  StaffCompetenciesAdminRedirect,
  StaffTrainingAdminRedirect,
  StaffIncidentAdminRedirect,
  StaffDocumentAdminRedirect,
  StaffAttendanceAdminRedirect,
  StaffPositionAdminRedirect,
  StaffGroupingsAdminRedirect,
  StaffLoansAdminRedirect,
// REcipient Accounting

  ProfileAccounting,
  AccountingHistory,
  Invoices,
  Receipts,
  RecipientLoans,
  PurchaseOrder,
  Transport,
  ChatBot,
  //------Contact-View----------
  ContactsAdmin,
  OthersContact,
  DayManagerMain,
  XeroProcesses,
  AIAssistant,
  VehicleAvailability,
  StaffAvailability,
  RecipientSearch,
  //---------------configurations
  ConfigurationMain,
  
  ClaimratesComponent,
  TargetgroupsComponent,
  PurposestatementComponent,
  BudgetgroupsComponent,
  BudgetsComponent,
  ContactgroupsComponent,
  ContacttypesComponent,
  AddresstypesComponent,
  OccupationComponent,
  ReligionComponent,
  PhoneemailtypesComponent,
  FinancialclassComponent,
  DesktopUsersComponent,
  CustomDatasets,
  PostcodesComponent,
  HolidaysComponent,
  MedicalcontactComponent,
  DestinationaddressComponent,
  ProgramcoordinatesComponent,
  DistributionlistComponent,
  NotificationlistComponent,
  InitialactionsComponent,
  OngoingactionsComponent,
  IncidenttriggersComponent,
  IncidenttypesComponent,
  IncidentsubcatComponent,
  IncidentnotecategoryComponent,
  LocationCategoriesComponent,
  StaffincidentnotecategoryComponent,
  FillingclassificationComponent,
  DocumentcategoriesComponent,
  RecipientsCategoryComponent,
  RecipientsGroupComponent,
  RecipientsMinorGroupComponent,
  RecipientsBillingCyclesComponent,
  DebtorTermsComponent,
  RecipientGoalsComponent,
  ConsentTypesComponent,
  CarePlanTypesComponent,
  ClinicalNotesGroupsComponent,
  CaseNoteCategoriesComponent,
  OpNoteCategoriesComponent,
  CareDomainsComponent,
  DischargeReasonsComponent,
  RefferalReasonsComponent,
  UserDefinedRemindersComponent,
  RecipientPrefrencesComponent,
  
  ReferralSourcesComponent,
  LifecycleEventsComponent,
  JobCategoryComponent,
  AdminCategoryComponent,
  StaffUserGroupsComponent,
  AwardLevelsComponent,
  AwardSetupComponent,
  StaffCompetencyGroupComponent,
  StaffNotesCategoriesComponent,
  StaffPositionsComponent,
  StaffTeamsComponent,
  AwardDetailsComponent,
  OpStaffNotesComponent,
  StaffRemindersComponent,
  ServiceDeciplinesComponent,
  StaffPreferencesComponent,
  LeaveDescriptionsComponent,
  ServiceNoteCategoriesComponent,
  VehiclesComponent,
  ActivityGroupsComponent,
  EquipmentsComponent,
  StaffCompetenciesComponent,
  CentrFacilityLocationComponent,
  FundingSourcesComponent,
  PayTypeComponent,
  ProgramPackagesComponent,
  ServicesComponent,
  ItemsConsumablesComponent,
  MenuMealsComponent,
  CaseMangementAdminComponent,
  StaffAdminActivitiesComponent,
  RecipientAbsenceComponent,
  CompaniesComponent,
  DocumentTemplateComponent,
  PortalUsers,
  NursingDignosisComponent,
  MedicalDignosisComponent,
  TasksComponent,
  ClinicalRemindersComponent,
  ClinicalAlertsComponent,
  MedicationsComponent,
  MobilityCodesComponent,
  FollowupComponent,
  HealthConditionsComponent,
  AdmittingPrioritiesComponent,
  ServicenotecatComponent,
  Othercontactaddress
 
]
