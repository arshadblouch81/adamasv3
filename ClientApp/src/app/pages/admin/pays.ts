import { Component, OnInit, OnDestroy, Input, AfterViewInit,ChangeDetectorRef } from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { Subject,forkJoin } from 'rxjs';


const inputFormDefault = {
    inputBatchnNumber: [1],
}
@Component({
    styleUrls: ['./billing.css'],
    templateUrl: './pays.html',
    selector: 'TimesheetProcessingAdmin',
})
export class TimesheetProcessingAdmin implements OnInit, OnDestroy, AfterViewInit {

    rollbackPayrollBatchOpen: any;
    unApprovedWorkHours: any;
    doPayIntegrityCheck: any;
    closePayRosterPeriod: any;
    setPayPeriodDate: any;
    processTravelInterpreter: any;
    processPayUpdate: any;
    eventID: string;
    id: string;

    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    isVisibleTop: boolean =  false;
    tocken: any;
    loading: boolean = false;
    inputForm: FormGroup;
    inputBatchnNumber: number;


    dateFormat: string = 'dd/MM/yyyy';
    format1 = (): string => `1`;
    format2 = (): string => `2`;
    format3 = (): string => `3`;
    format4 = (): string => `4`;
    format5 = (): string => `5`;
    format6 = (): string => `6`;
    CircleSize = 26;

    constructor(
        private router: Router,
        private modalService: NzModalService,
        private sanitizer: DomSanitizer,
        private printS: PrintService,
        private listS: ListService,
        private globalS: GlobalService,        
        private fb: FormBuilder,
        private cd: ChangeDetectorRef,
        
    ) { }
    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.inputForm = this.fb.group(inputFormDefault);

    }
    ngAfterViewInit(): void {

    }
    onChange(result: Date): void {
        // console.log('onChange: ', result);
    }
    ngOnDestroy(): void {

    }

    payEvent(i) {
        i = i || window.event;
        i = i.target || i.srcElement;
        this.eventID = (i.id).toString();

        switch (this.eventID) {
            case 'btn-ReviewUnapprvdWorkHour':
                this.unApprovedWorkHours = {}
                break;
            case 'btn-DoPayIntegrityCheck':
                this.doPayIntegrityCheck = {}
                break;
            case 'btn-CloseRostersForEditing':
                this.closePayRosterPeriod = {}
                break;
            case 'btn-SetPayPeriodEndDate':
                this.setPayPeriodDate = {}
                break;
            case 'btn-ProcessTravelBetweenClaims':
                this.processTravelInterpreter = {}
                break;
            case 'btn-ProcessPayUpdate':
                this.processPayUpdate = {}
                break;
            case 'btn-RollbackPayUpdateBatch':
                this.rollbackPayrollBatchOpen = {}
                break;
            case 'btn-NonAwardIntPayReprt':
                break;
            case 'btn-AwardIntPayReprt':
                break;
            case 'btn-Timesheets':
               this.isVisibleTop = true;
                break;

            default:
                break;
        }
    }
    ModalOk(){
        var s_BatchNo = this.inputForm.value.inputBatchnNumber;
        var SQL = "SELECT tsheetheader.tsheetnumber,  roster.[client code], roster.[service type], roster.[service description], roster.program, roster.costqty, roster.[unit pay rate] AS PayRate,  roster.date,staff.accountno FROM (roster INNER JOIN tsheetheader ON roster.timesheetnumber = tsheetheader.tsheetnumber) INNER JOIN staff ON tsheetheader.[carer code] = staff.accountno WHERE roster.[ros_pay_batch] = "+ s_BatchNo +" ORDER BY tsheetheader.tsheetnumber, roster.[date]"
        forkJoin([
            this.listS.getlist(SQL),            
          ]).subscribe(data => {
            this.generatePdf(data[0])
          });

    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.isVisibleTop = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
    }
    generatePdf(dataset){
        var Title;                  
        
          Title = "Staff Time Sheet/Service List";
          this.loading = true;
          this.drawerVisible = true;        
                   
            //console.log(dataset)      
            
                const data = {
                    "template": { "_id": "vFiiDmwTRPdn4VB5" },
                    "options": {
                        "reports": { "save": false },                        
                        "sql": dataset,                        
                        "userid": this.tocken.user,
                        "txtTitle":  Title,                      
                    }
                }              
                this.loading = true;           
                        
                this.drawerVisible = true;
                this.printS.printControl(data).subscribe((blob: any) => {
                            this.pdfTitle = Title+".pdf"
                            this.drawerVisible = true;                   
                            let _blob: Blob = blob;
                            let fileURL = URL.createObjectURL(_blob);
                            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                            this.loading = false;
                            this.cd.detectChanges();                       
                        }, err => {
                            console.log(err);
                            this.loading = false;
                            this.modalService.error({
                                nzTitle: 'TRACCS',
                                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                nzOnOk: () => {
                                    this.drawerVisible = false;
                                },
                            });
                });                            
                return;        
               
                 
    }
}

