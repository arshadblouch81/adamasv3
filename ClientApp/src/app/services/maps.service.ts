import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

import { GlobalService } from './global.service';

const maps: string = "api/maps";


@Injectable()
export class MapService {

    constructor(
        public http: HttpClient,
        public auth: AuthService,
        public globalS: GlobalService
    ) { }

    // getLocationDistance():Observable<any> {
    //     return this.auth.get(`${maps}/GetMapDistance`);
    // }

    //AHSAN
    getDistanceMatrix(data: any): Observable<any> {
        return this.auth.get(`${maps}/GetDistanceMatrix`, data);
    }

}

