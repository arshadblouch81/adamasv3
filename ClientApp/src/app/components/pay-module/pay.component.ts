import { ChangeDetectorRef, Component, Input, OnInit, SimpleChanges, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, DllService } from '@services/index';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { debounceTime, timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';
import { DateHelperService } from 'ng-zorro-antd';

@Component({
  selector: 'app-process-pay-update',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css'],
})
export class PayComponent implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;
  confirmModal?: NzModalRef;
  loggedComputerName: any;
  loggedUserName: any;

  branchList: Array<any>;
  programList: Array<any>;
  categoriesList: Array<any>;
  staffList: Array<any>;
  fundingList: Array<any>;
  batchHistoryList: Array<any>;
  tableData: Array<any>;
  loading: boolean = false;
  allchecked: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  dateFormat: string = 'dd/MM/yyyy';
  postLoading: boolean = false;
  token: any;
  tocken: any;
  check: boolean = false;
  userRole: string = "userrole";
  chkAllDates: boolean;
  endDate: any;
  startDate: any;
  id: string;
  private unsubscribe: Subject<void> = new Subject();
  PayPeriodLength: number;

  selectedBranches: any;
  selectedPrograms: any;
  selectedStaffs: any;
  selectedCategories: any;
  selectedFunding: any;
  allBranchesChecked: boolean;
  allProgramsChecked: boolean;
  allStaffsChecked: boolean;
  allCategoriesChecked: boolean;
  allFundingChecked: boolean;
  allChecked = false;
  RSC_Program: any;
  RSC_Cycle: any;
  RSC_BRANCH: any;
  RSC_STAFF: any;
  indeterminate = true;
  batchNumber: any;
  BatchDetail: any;
  BatchType: any;
  operatorID: any;
  currentDateTime: any;
  updatedRecords: any;
  lockBranches: any = false;
  lockPrograms: any = false;
  lockCategories: any = false;
  lockStaff: any = false;
  lockFunding: any = false;
  chkStaffPays: any;
  chkPayBrokerage: any;

  chkCreateExportFile: any;
  chkAwardInterExport: any;
  chkExportZeroValueLines: any;
  chkIncludeRecipientAdmin: any;
  chkIncludeADMINPaytype: any;
  invPrefix: any;
  selectedPackage: any;
  chkIncActivityCode: any;
  chkActCostAcc: any;
  chkPrefixGL: any;
  chkPrefGL: any;
  chkStaffNumberPosID: any;
  chkUseActCostAcct: any;
  chkExpExtChkColumn: any;
  chkIncCostCenter: any;
  chkUseTraccsPayType: any;
  chkUseTraccsCostAcc: any;
  inputEasyTimeBranch: any;
  inputEasyTimeTimesheetID: any;
  chkMyObIncActCode: any;
  chkIncBillingText: any;
  chk4DecimalQty: any;
  chkAutoPayRequried: any;
  chkGenerateStd: any;
  chkGenerateOccupational: any;
  chkExportJobNum: any;
  chkTraccsDateMyOB: any;
  chkExcClientDesc: any;
  chkExcContIdDesc: any;
  chkHyphenatedGLAcc: any;
  chkUseActivityGL: any;
  chkUseNDIAItem: any;
  chkUseNDIAClaim: any;
  chkFirstNameSurNam: any;
  chkAppendInvT: any;
  chkRecipientTermDate: any;
  chkBlankInventoryCode: any;
  chkAlternativeDesc: any;
  chkBlankTrackingNamOpt: any;
  chkIncluEmailPosAdd: any;
  inputDefaultAccount: any;
  inputDefaultProject: any;
  inputDefaultActivity: any;

  s_FirstError: any;
  s_LastError: any;
  s_LogFilePath: any;
  s_Messages: any;

  normalPayType: Array<any>;
  allowanceType: Array<any>;
  superannuationType: Array<any>;
  glLocation: Array<any>;
  glPayAccount: Array<any>;
  glCostCenter: Array<any>;
  payrunGroupCode: Array<any>;
  payFreqSel: Array<any>;
  departCodeSel: Array<any>;
  PayrollPackage: Array<any>;
  test: string = '';
  @ViewChild('easytimeModalBranchName', { static: true }) easytimeModalBranchName!: TemplateRef<any>;
  @ViewChild('easytimeModalTimesheetId', { static: true }) easytimeModalTimesheetId!: TemplateRef<any>;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private modal: NzModalService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    private Dlls: DllService
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.buildForm();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.modalOpen = true;
  }

  resetModal() {
    this.inputForm.reset();
    this.selectedPackage = '';
    this.postLoading = false;
  }

  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.postLoading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/pays']);
  }

  buildForm() {
    this.inputForm = this.formBuilder.group({
      timesheetDate: new Date(),
      chkAllDates: false,
      startDate: '01/01/2021',
      endDate: '01/01/2021',

      //Export Type Tab
      chkStaffPays: false,
      chkPayBrokerage: false,

      //Interface Tab
      chkCreateExportFile: false,
      chkAwardInterExport: false,
      chkExportZeroValueLines: false,
      chkIncludeRecipientAdmin: false,
      chkIncludeADMINPaytype: false,
      invPrefix: '',
      selectedPackage: '',
      chkIncActivityCode: false,
      chkActCostAcc: false,
      chkPrefixGL: false,
      chkPrefGL: false,
      chkStaffNumberPosID: false,
      chkUseActCostAcct: false,
      chkExpExtChkColumn: false,
      chkIncCostCenter: false,
      chkUseTraccsPayType: false,
      chkUseTraccsCostAcc: false,
      inputEasyTimeBranch: '',
      inputEasyTimeTimesheetID: '',
      chkMyObIncActCode: false,
      chkIncBillingText: false,
      chk4DecimalQty: false,
      chkAutoPayRequried: false,
      chkGenerateStd: false,
      chkGenerateOccupational: false,
      chkExportJobNum: false,

      chkTraccsDateMyOB: false,
      chkExcClientDesc: false,
      chkExcContIdDesc: false,
      chkHyphenatedGLAcc: false,
      chkUseActivityGL: false,
      chkUseNDIAItem: false,
      chkUseNDIAClaim: false,
      chkFirstNameSurNam: false,
      chkAppendInvT: false,
      chkRecipientTermDate: false,
      chkBlankInventoryCode: false,
      chkAlternativeDesc: false,
      chkBlankTrackingNamOpt: false,
      chkIncluEmailPosAdd: false,

      inputDefaultAccount: '',
      inputDefaultProject: '',
      inputDefaultActivity: '',

      selectedDirectoryPath: '',
      normalPayType: '',
      allowanceType: '',
      superannuationType: '',
      glLocation: '',
      glPayAccount: '',
      glCostCenter: '',
      payrunGroupCode: '',
      payFreqSel: '',
      departCodeSel: '',
    });

    // Dropdown lists
    this.normalPayType = ['O', 'N'];
    this.allowanceType = ['A'];
    this.superannuationType = ['OFF', 'E'];
    this.glLocation = [''];
    this.glPayAccount = [''];
    this.glCostCenter = [''];
    this.payrunGroupCode = [''];
    this.payFreqSel = ['OFF', 'A', 'F', 'M', 'W', '4'];
    this.departCodeSel = ['Use Branch/Department GL Code as Department Code', 'Use Program GL Code as Department Code'];

    this.PayrollPackage = [
      'ABM',
      'ALESCO',
      'ATTACHE',
      'ATTACHE SUPPLIER',
      'AURION',
      'AURION (AWARD)',
      'CARE SYSTEMS',
      'CHRIS21',
      'CHRONOS',
      'CIM',
      'CLOUD PAYROLL',
      'CONNX',
      'EASYTIME',
      'GREENTREE',
      'HR3',
      'LATTICE/CONSISTO',
      'MICROPAY',
      'MYOB',
      'NELLER-PRECEDA',
      'PAYGLOBAL',
      'QUICKBOOKS',
      'ROSTERLIVE',
      'WAGE EASY PAYROLL',
      'XERO PAYROLL'
    ];

    this.loading = true;

    //load Payperiod End Date
    this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
      if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
        this.endDate = dataPayPeriodEndDate[0].payPeriodEndDate;
        this.inputForm.patchValue({
          endDate: this.endDate,
        })
      }
      else {
        this.inputForm.patchValue({
          endDate: new Date()
        });
        this.endDate = new Date;
      }
      this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
        if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
          this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
        }
        else {
          this.PayPeriodLength = 14
        }
        var firstDate = new Date(this.endDate);
        firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
        this.startDate = formatDate(firstDate, 'yyyy/MM/dd', 'en_US');
        this.inputForm.patchValue({
          startDate: this.startDate,
        });
      });
    });

    //load Branches Detail
    this.menuS.getlistbranches(this.check).subscribe(dataBranches => {
      this.branchList = dataBranches;
      this.tableData = dataBranches;
      this.allBranchesChecked = true;
      this.checkAll(1);
    });

    //load Programs Detail
    let dataPassPrograms = {
      condition: formatDate(new Date(), 'MM-dd-yyyy', 'en_US'),
    }
    this.billingS.getProgramslist(dataPassPrograms).subscribe(dataPrograms => {
      this.programList = dataPrograms;
      this.tableData = dataPrograms;
      this.allProgramsChecked = true;
      this.checkAll(2);
    });

    //load Staffs Detail
    this.billingS.getStaffDetails().subscribe(dataStaffDetail => {
      this.staffList = dataStaffDetail;
      this.tableData = dataStaffDetail;
      this.allStaffsChecked = true;
      this.checkAll(4);
    });

    //load Fundings Detail
    this.billingS.getFundingDetails().subscribe(dataFundingDetails => {
      this.fundingList = dataFundingDetails;
      this.tableData = dataFundingDetails;
      this.allFundingChecked = true;
      this.checkAll(5);
    });

    //load Categories Detail
    this.billingS.getlistcategories().subscribe(dataCategories => {
      this.categoriesList = dataCategories;
      this.tableData = dataCategories;
      this.allCategoriesChecked = true;
      this.checkAll(3);
    });

    //load Batch History Detail
    this.billingS.getPaysBatchHistory().subscribe(data => {
      this.batchHistoryList = data;
      this.tableData = data;
    });

    //get Windows Logged User
    this.billingS.getCurrentUser().subscribe(getCurrentUser => {
      this.loggedComputerName = getCurrentUser.computerName;
      this.loggedUserName = getCurrentUser.userName;
    })
    this.selectedDirectoryPath = `C:\\Users\\${this.loggedUserName}\\Documents\\`
      
    this.loading = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  onKeyPress(data: KeyboardEvent) {
    return this.globalS.acceptOnlyNumeric(data);
  }

  keyPressDecimalNumbers(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57)) {
      event.preventDefault();
      return false;
    }
    return true;
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedBranches = event;
    if (index == 2)
      this.selectedPrograms = event;
    if (index == 3)
      this.selectedCategories = event;
    if (index == 4)
      this.selectedStaffs = event;
    if (index == 5)
      this.selectedFunding = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allBranchesChecked == false) {
        this.lockBranches = true
      }
      else {
        this.branchList.forEach(x => {
          x.checked = true;
          this.allBranchesChecked = x.description;
          this.allBranchesChecked = true;
        });
        this.lockBranches = false
      }
    }
    if (index == 2) {
      if (this.allProgramsChecked == false) {
        this.lockPrograms = true
      }
      else {
        this.programList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockPrograms = false
      }
    }
    if (index == 3) {
      if (this.allCategoriesChecked == false) {
        this.lockCategories = true
      }
      else {
        this.categoriesList.forEach(x => {
          x.checked = true;
          this.allCategoriesChecked = x.description;
          this.allCategoriesChecked = true;
        });
        this.lockCategories = false
      }
    }
    if (index == 4) {
      if (this.allStaffsChecked == false) {
        this.lockStaff = true
      }
      else {
        this.staffList.forEach(x => {
          x.checked = true;
          this.allStaffsChecked = x.description;
          this.allStaffsChecked = true;
        });
        this.lockStaff = false
      }
    }
    if (index == 5) {
      if (this.allFundingChecked == false) {
        this.lockFunding = true
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = false
      }
    }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockBranches = true;
      this.branchList.forEach(x => {
        x.checked = false;
        this.allBranchesChecked = false;
        this.selectedBranches = [];
      });
    }
    if (index == 2) {
      this.lockPrograms = true;
      this.programList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
    if (index == 3) {
      this.lockCategories = true;
      this.categoriesList.forEach(x => {
        x.checked = false;
        this.allCategoriesChecked = false;
        this.selectedCategories = [];
      });
    }
    if (index == 4) {
      this.lockStaff = true;
      this.staffList.forEach(x => {
        x.checked = false;
        this.allStaffsChecked = false;
        this.selectedStaffs = [];
      });
    }
    if (index == 5) {
      this.lockFunding = true;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
  }

  startProcess() {
    this.loading = true;
    this.billingS.getPayBatchRecord(null).subscribe(data => {
      this.batchNumber = data[0].batchRecordNumber;
      this.validateModalFields();
    });
  }

  validateModalFields(): void {
    if (this.lockBranches == false) {
      this.selectedBranches = null;
      this.RSC_BRANCH = 'ALL'
    } else {
      this.RSC_BRANCH = ""
      this.selectedBranches = this.branchList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockPrograms == false) {
      this.selectedPrograms = null;
      this.RSC_Program = 'ALL'
    } else {
      this.RSC_Program = ""
      this.selectedPrograms = this.programList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockCategories == false) {
      this.selectedCategories = null;
      this.RSC_Cycle = 'ALL'
    } else {
      this.RSC_Cycle = ""
      this.selectedCategories = this.categoriesList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockStaff == false) {
      this.selectedStaffs = null;
      this.RSC_STAFF = 'ALL'
    } else {
      this.RSC_STAFF = ""
      this.selectedStaffs = this.staffList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockFunding == false) {
      this.selectedFunding = null
    } else {
      this.selectedFunding = this.fundingList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }


    const modalVal = this.inputForm;
    this.selectedPackage = modalVal.get('selectedPackage').value

    if (this.selectedBranches != '' && this.selectedPrograms != '' && this.selectedCategories != '' && this.selectedStaffs != '' && this.selectedFunding != '') {
      if (this.selectedPackage === 'EASYTIME') {
        this.modal.create({
          nzTitle: 'Traccs',
          nzContent: this.easytimeModalBranchName,
          nzOnOk: () => {
            this.modal.create({
              nzTitle: 'Traccs',
              nzContent: this.easytimeModalTimesheetId,
              nzOnOk: () => {
                this.processExecution();
              },
              nzOnCancel: () => {
                // this.globalS.sToast('Success', 'CANCEL Button Selected.');
              }
            });
          },
          nzOnCancel: () => {
            // this.globalS.sToast('Success', 'CANCEL Button Selected.');
          }
        });
      } else if (this.selectedBranches != 'EASYTIME') {
        this.processExecution();
      }
    } else if (this.selectedBranches == '') {
      this.globalS.eToast('Error', 'Please select atleast one Branch to proceed')
    } else if (this.selectedFunding == '') {
      this.globalS.eToast('Error', 'Please select atleast one Funding to proceed')
    } else if (this.selectedPrograms == '') {
      this.globalS.eToast('Error', 'Please select atleast one Program to proceed')
    } else if (this.selectedCategories == '') {
      this.globalS.eToast('Error', 'Please select atleast one Category to proceed')
    } else if (this.selectedStaffs == '') {
      this.globalS.eToast('Error', 'Please select atleast one Staff to proceed')
    }
    this.postLoading = false;
    this.resetModal();
    this.ngOnInit();
  }

  processExecution() {
    this.postLoading = true;
    const modalVal = this.inputForm;

    let _glLocation = modalVal.get('glLocation')?.value ?? '';
    let _glPayAccount = modalVal.get('glPayAccount')?.value ?? '';
    let _glCostCenter = modalVal.get('glCostCenter')?.value ?? '';
    let _payrunGroupCode = modalVal.get('payrunGroupCode')?.value ?? '';
    let _payFreqSel = modalVal.get('payFreqSel')?.value ?? '';
    let _departCodeSel = modalVal.get('departCodeSel')?.value ?? '';

    if (this.selectedBranches = null) { this.selectedBranches = false };
    if (this.selectedCategories = null) { this.selectedCategories = false };
    if (this.selectedFunding = null) { this.selectedFunding = false };
    if (this.selectedPrograms = null) { this.selectedPrograms = false };
    if (this.selectedStaffs = null) { this.selectedStaffs = false };

    const params = {
      User: this.token.nameid,
      Password: '', // Ignored by Mansoor
      WindowsUser: this.loggedUserName,
      b_AllDates: modalVal.get('chkAllDates').value,
      b_Award: modalVal.get('chkAwardInterExport').value,
      b_Export: modalVal.get('chkCreateExportFile').value,
      b_ExportZeroValueLines: modalVal.get('chkExportZeroValueLines').value,
      b_GLCodesRequired: '', // Ignored by Mansoor
      b_IncludeADMINPaytype: modalVal.get('chkIncludeADMINPaytype').value,
      b_IncludeRecipientAdmin: modalVal.get('chkIncludeRecipientAdmin').value,
      b_IncUnapproved: '', // Ignored by Mansoor
      b_Option0: this.allowanceType,
      b_Option1: modalVal.get('chkIncActivityCode').value,
      b_Option2: modalVal.get('chkUseTraccsCostAcc').value,
      b_Option3: modalVal.get('chkActCostAcc').value,
      b_Option4: modalVal.get('chkStaffNumberPosID').value,
      b_Option5: modalVal.get('chkPrefixGL').value,
      b_Option9: this.chkPrefixGL,
      b_PayBrokerage: modalVal.get('chkPayBrokerage').value,
      b_UpdateCompletion: '', // Ignored by Mansoor
      ProgressBar_Max: '',
      ProgressBar_Text: '',
      ProgressBar_Value: '',
      dt_ProcessStartTime: '',
      OperatorID: this.operatorID,
      s_ABMDefaultAccount: modalVal.get('inputDefaultAccount').value,
      s_ABMDefaultActivity: modalVal.get('inputDefaultActivity').value,
      s_ABMDefaultProject: modalVal.get('inputDefaultProject').value,
      s_EasyTimeBranch: this.inputEasyTimeBranch,
      s_EasyTimeTimesheetID: this.inputEasyTimeTimesheetID,
      s_EndDate: formatDate(modalVal.get('endDate').value, 'yyyy/MM/dd', 'en_US'),
      s_EndOfInvoiceString: '', // Ignored by Mansoor
      s_ExportLocation: modalVal.get('selectedDirectoryPath').value,
      s_FirstError: this.test,
      s_Interface: modalVal.get('selectedPackage').value,
      s_LastError: this.test,
      s_LogFilePath: this.test,
      s_Messages: this.test,
      s_Option1: modalVal.get('normalPayType')?.value ?? '',
      s_Option2: modalVal.get('superannuationType')?.value ?? '',
      s_Option3: modalVal.get('allowanceType')?.value ?? '',
      s_ProcDate: this.test,
      s_ProductDataPath: this.test,
      s_SelectedBranches: this.selectedBranches,
      s_SelectedCategories: this.selectedCategories,
      s_SelectedFunding: this.selectedFunding,
      s_SelectedPrograms: this.selectedPrograms,
      s_SelectedStaff: this.selectedStaffs,
      s_StartDate: formatDate(modalVal.get('startDate').value, 'yyyy/MM/dd', 'en_US'),
      s_StartOfLinString: this.test,
      s_status: this.test,
      T1_LineNo: this.test
    };

    // const queryString = new URLSearchParams(params as any).toString();
    // const apiUrl = `myUR - need to code?${queryString}`;

    // this.http.get(apiUrl).subscribe(
    //   (response) => {
    //     console.log('API response:', response);
    //   },
    //   (error) => {
    //     console.error('API error:', error);
    //   }
    // );

    this.Dlls.PayBillUpdate({
      User: params.User,
      Password: params.Password,
      WindowsUser: params.WindowsUser,
      b_AllDates: params.b_AllDates,
      b_Award: params.b_Award,
      b_Export: params.b_Export,
      b_ExportZeroValueLines: params.b_ExportZeroValueLines,
      b_GLCodesRequired: params.b_GLCodesRequired,
      b_IncludeADMINPaytype: params.b_IncludeADMINPaytype,
      b_IncludeRecipientAdmin: params.b_IncludeRecipientAdmin,
      b_IncUnapproved: params.b_IncUnapproved,
      b_Option0: params.b_Option0,
      b_Option1: params.b_Option1,
      b_Option2: params.b_Option2,
      b_Option3: params.b_Option3,
      b_Option4: params.b_Option4,
      b_Option5: params.b_Option5,
      b_Option9: params.b_Option9,
      b_PayBrokerage: params.b_PayBrokerage,
      b_UpdateCompletion: params.b_UpdateCompletion,
      ProgressBar_Max: params.ProgressBar_Max,
      ProgressBar_Text: params.ProgressBar_Text,
      ProgressBar_Value: params.ProgressBar_Value,
      dt_ProcessStartTime: params.dt_ProcessStartTime,
      OperatorID: params.OperatorID,
      s_ABMDefaultAccount: params.s_ABMDefaultAccount,
      s_ABMDefaultActivity: params.s_ABMDefaultActivity,
      s_ABMDefaultProject: params.s_ABMDefaultProject,
      s_EasyTimeBranch: params.s_EasyTimeBranch,
      s_EasyTimeTimesheetID: params.s_EasyTimeTimesheetID,
      s_EndDate: params.s_EndDate,
      s_EndOfInvoiceString: params.s_EndOfInvoiceString,
      s_ExportLocation: params.s_ExportLocation,
      s_FirstError: params.s_FirstError,
      s_Interface: params.s_Interface,
      s_LastError: params.s_LastError,
      s_LogFilePath: params.s_LogFilePath,
      s_Messages: params.s_Messages,
      s_Option1: params.s_Option1,
      s_Option2: params.s_Option2,
      s_Option3: params.s_Option3,
      s_ProcDate: params.s_ProcDate,
      s_ProductDataPath: params.s_ProductDataPath,
      s_SelectedBranches: params.s_SelectedBranches,
      s_SelectedCategories: params.s_SelectedCategories,
      s_SelectedFunding: params.s_SelectedFunding,
      s_SelectedPrograms: params.s_SelectedPrograms,
      s_SelectedStaff: params.s_SelectedStaff,
      s_StartDate: params.s_StartDate,
      s_StartOfLinString: params.s_StartOfLinString,
      s_status: params.s_status,
      T1_LineNo: params.T1_LineNo
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      console.log(data)
      if (data == 'True') {
        this.updatedRecords = data[0].updatedRecords
        if (this.updatedRecords == 0) {
          this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
        } else {
          this.globalS.sToast('Success', this.updatedRecords + ' - Payroll Records Updated')
          this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Payroll Run is: ' + this.batchNumber)
        }
        this.postLoading = false;
        this.ngOnInit();
        return false;
      }
    });
  }

  processExecutionTest() {
    this.postLoading = true;
    const modalVal = this.inputForm;
    let _currentDate = formatDate(this.globalS.getCurrentDateTime(), 'yyyy-MM-dd hh:mm', 'en_US')
    let _startDate = formatDate(modalVal.get('startDate').value, 'yyyy/MM/dd', 'en_US')
    let _endDate = formatDate(modalVal.get('endDate').value, 'yyyy/MM/dd', 'en_US')
    let _batchNumber = this.batchNumber + 1;

    this.billingS.postPayUpdate({
      BatchNumber: _batchNumber,
      Branches: this.selectedBranches,
      Fundings: this.selectedFunding,
      Categories: this.selectedCategories,
      Programs: this.selectedPrograms,
      Staffs: this.selectedStaffs,
      StartDate: _startDate,
      EndDate: _endDate,
      ChkStaffPays: modalVal.get('chkStaffPays').value,
      ChkPayBrokerage: modalVal.get('chkPayBrokerage').value,
      ChkCreateExportFile: modalVal.get('chkCreateExportFile').value,
      ChkAwardInterExport: modalVal.get('chkAwardInterExport').value,
      SelectedPackage: modalVal.get('selectedPackage').value,
      InputDefaultAccount: modalVal.get('inputDefaultAccount').value,
      InputDefaultProject: modalVal.get('inputDefaultProject').value,
      InputDefaultActivity: modalVal.get('inputDefaultActivity').value,
      OperatorID: this.token.nameid,
      CurrentDateTime: _currentDate
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      if (data) {
        this.updatedRecords = data[0].updatedRecords
        if (this.updatedRecords == 0) {
          this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
        } else {
          this.globalS.sToast('Success', this.updatedRecords + ' - Payroll Records Updated')
          this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Payroll Run is: ' + this.batchNumber)
        }
        this.postLoading = false;
        this.ngOnInit();
        return false;
      }
    });
    this.postLoading = false;
  }

  async selectSavePath() {
    const modalVal = this.inputForm;
    var packageName = modalVal.get('selectedPackage').value;

    try {
      // let mimeType = '';
      // let suggestedName = '';
      // let types: { description: string; accept: { [key: string]: string[] } }[] = []; // Initialize types array

      // switch (packageName) {
      //   case 'Pay Update':
      //     suggestedName = 'pay_update.csv';
      //     mimeType = 'text/csv';
      //     types = [
      //       {
      //         description: 'CSV Files',
      //         accept: { 'text/csv': ['.csv'] },
      //       },
      //     ];
      //     break;
      //   case 'Billing Update':
      //     suggestedName = 'billing_update.wtc';
      //     mimeType = 'application/octet-stream';
      //     types = [
      //       {
      //         description: 'WTC Files',
      //         accept: { 'application/octet-stream': ['.wtc'] },
      //       },
      //     ];
      //     break;
      //   default:
      //     suggestedName = 'default_file.txt';
      //     mimeType = 'text/plain';
      //     types = [
      //       {
      //         description: 'Text Files',
      //         accept: { 'text/plain': ['.txt'] },
      //       },
      //     ];
      //     break;
      // }

      let s_Location: string = '';
      let suggestedName: string = '';
      let mimeType: string = '';
      let types: { description: string, accept: { [key: string]: string[] } }[] = [];
      let selectedDirectoryPath: string = '';

      switch (packageName) {
        case 'AURION':
        case 'AURION (AWARD)':
          suggestedName = 'AURION.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'GREENTREE':
          suggestedName = 'GREENTREE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CHRIS21':
          suggestedName = 'PAYROLL.DAT';
          mimeType = 'application/dat';
          types = [
            {
              description: 'DAT Files',
              accept: { 'application/dat': ['.dat'] },
            },
          ];
          break;

        case 'MYOB':
          suggestedName = 'MYOB.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'QUICKBOOKS':
          suggestedName = 'QUICKBOOK.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'WAGE EASY PAYROLL':
          suggestedName = 'WAGE.WTC';
          mimeType = 'application/octet-stream';
          types = [
            {
              description: 'WTC Files',
              accept: { 'application/octet-stream': ['.wtc'] },
            },
          ];
          break;

        case 'HR3':
          suggestedName = 'HR3.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ABM':
          suggestedName = 'ABMSUPPLIER.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'QUICKEN PREMIER':
          suggestedName = 'QUICKEN.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'ATTACHE':
        case 'ATTACHE SUPPLIER':
          suggestedName = 'PAYTSHT.INP';
          mimeType = 'application/inp';
          types = [
            {
              description: 'INP Files',
              accept: { 'application/inp': ['.inp'] },
            },
          ];
          break;

        case 'CIM':
          s_Location = getOutputFolder('PayExportFolder', 'Force_PayExportFolder', 'Select the Payroll Export Folder', this.operatorID);
          break;

        case 'MICROPAY':
          suggestedName = 'MicroPayPayrollPath' + '\\TRACCS.MIF';
          mimeType = 'application/mif';
          types = [
            {
              description: 'MIF Files',
              accept: { 'application/mif': ['.mif'] },
            },
          ];
          break;

        case 'PAYGLOBAL':
          suggestedName = 'PAYGLOBAL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'EASYTIME':
          suggestedName = 'EASYTIME.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'NELLER-PRECEDA':
          suggestedName = 'PAY34T.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ALESCO':
          suggestedName = 'AL-PAY.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'LATTICE/CONSISTO':
          suggestedName = 'CONSISTO.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ROSTERLIVE':
          suggestedName = 'ROSTERLIVE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CLOUD PAYROLL':
          suggestedName = 'CLOUDPAYROLL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CONNX':
          suggestedName = 'CONNXPAYROLL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CARE SYSTEMS':
          suggestedName = 'CARESYSTEMS.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        default:
          break;
      }

      function getOutputFolder(folder: string, forceFolder: string, dialogTitle: string, operatorId: string): string {
        return `/output/folder/path/${folder}`;
      }

      const handle = await (window as any).showSaveFilePicker({
        suggestedName: suggestedName,
        types: types,
      });

      const filePath = handle.name;
      const dirHandle = await (window as any).showDirectoryPicker();
      const dirName = filePath;
      const fileHandle = await dirHandle.getFileHandle(dirName, { create: true });

      // console.log(`File path in: ${dirHandle.name}/${dirName}`);

      this.inputForm.patchValue({
        selectedDirectoryPath: `C:\\Users\\${this.loggedUserName}\\Documents\\${dirHandle.name}\\${dirName}`
      })

    } catch (error) {
      // console.error('Failed to get path:', error);
      this.globalS.iToast('Information', 'Directory Path not selected.');
    }
  }
  selectedDirectoryPath: any;
}
