using System.Text;
using Microsoft.VisualBasic;

namespace Adamas.Models.Modules{
    public class DataEncryption{
        private bool isValid = false;
        private bool isChanged = false;

        private ApplicationUser _user;
        public DataEncryption(ApplicationUser user){
            _user = user;
            this.Validate(_user);
            Decrypt();
        }

        public bool IsAuthenticated(){
            return this.isValid;
        }

        private void Validate(ApplicationUser user){
            string encrypted = Encrypt(user.Password);
            if(Strings.Equals(user.PasswordHandler,encrypted))
                this.isValid = true;
        }

        public bool ChangePassword(ApplicationUser user){
            string encrypted = Encrypt(user.Password);

            if(StringBuilder.Equals(user.PasswordHandler, encrypted))
                return true;
            
            return false;
        }

        public string Encrypt(string passwd){
            string enryptedStr = "";

            for (var a = 0; a < passwd.Length; a++){
                byte[] bytes = new byte[1];
                bytes[0] = (byte)(Strings.AscW(passwd[a]) + 50);
                enryptedStr = enryptedStr + new string(Encoding.GetEncoding(1252).GetChars(bytes));
            }

            return enryptedStr;
        }

        public string Decrypt(string passwd = "†¡–“«dcS"){
            string _passwd = passwd;
            string enryptedStr = string.Empty;

            for (var a = 0; a < _passwd.Length; a++)
            {
                enryptedStr = enryptedStr + Strings.ChrW((Strings.AscW(_passwd[a]) - 50)).ToString();
            }

            return enryptedStr;
        }   
    }
}