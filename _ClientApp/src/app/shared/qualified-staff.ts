
import {forkJoin, Subject } from 'rxjs';
import {takeUntil, switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';

import { Component, forwardRef, Input, OnInit, OnDestroy,ViewChild, ElementRef, NgZone, OnChanges, SimpleChanges } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { ListService, ClientService, GlobalService } from '@services/index'

import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';


interface PopulateListInterface {
    branches: Array<string>,
    teams: Array<string>,
    roles: Array<string>
}

export interface InputParams{
    RecipientCode: any,
    User: string,
    BookDate: string,
    StartTime: string,
    EndTime: string,
    EndLimit: string,
    Gender: string,
    Competencys: string,
    CompetenciesCount: number  
}

@Component({
    selector: 'qualified-staff',
    templateUrl: './qualified-staff.html',
    styles: [`
    .filter-container{
        width:20rem;
    }
    :host{
        width: 95%;
        margin: 0 auto;
    }
    .table-body{
        height:inherit;
    }
    .qs-container{
        margin-top: 10px;
        display: inline-block;
        width: 100%;
    }
    .search{
        position:relative;
    }
    .active{
        background: #49d27e;
    }
    clr-toggle-wrapper{
        float: right;
        margin-top: 10px;
    }
    .badge-pink{
        background: #FF69B4;
    }
    .table-data:hover:not(.selected){
        background: #f3f3f3;
        cursor:pointer;
    }
    input{
        text-transform: uppercase;
    }
    .selected{
        background: #ddeeff !important;
    }
    `],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => QualifiedStaffComponent),
            multi: true
        }
    ]
})

export class QualifiedStaffComponent implements ControlValueAccessor, OnInit, OnDestroy, OnChanges{

    @Input('format') private _format: string = 'simple'
    @Input() public height: number = 10;
    @Input() public hideFilter: boolean = false;
    @Input() private inputParams: InputParams

    inputSearch: string

    get format(){
        return this._format
    }
    set format(value: string){
        this._format = value
    }

    unsubscribe$ = new Subject<any>()
    inputChange$ = new Subject<string>();

    showFilter: boolean = false
    populateList: PopulateListInterface;

    private onChange: Function
    private onTouch: Function
    user: any

    allBranch: boolean = true
    allTeam: boolean = true
    allRole: boolean = true

    results: Array<any>;
    original: Array<any>;

    filterListGroup: FormGroup;
    selectedIndex: number;

    constructor(
        private listS: ListService,
        private clientS: ClientService,
        private ngZone: NgZone,
        private formBuilder: FormBuilder
    ){
        this.inputChange$.pipe(
            debounceTime(500),
            distinctUntilChanged()
        ).subscribe(data => {
            this.selectedIndex = -1;
            if(!data)
                this.results = this.original;

            this.results = this.original.filter(x => (x.firstName.toLowerCase()).indexOf(data) > -1 )
        });
    }

    ngOnInit(){
        this.filterListGroup = this.formBuilder.group({
            branches: '',
            teams:'',
            roles: ''
        })
        
        this.populate();
    }

    ngOnChanges(changes: SimpleChanges){
        for(let property in changes){
            if(property == 'inputParams' && changes[property].currentValue != null){
                this.search(changes[property].currentValue);
            } else {
                console.log('reset')
                this.reset();
            }
        }
    }

    reset(){
        this.selectedIndex = -1;
        this.inputSearch = ''
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.unsubscribe();
    }

    filterToggleChange(event: boolean){
        this.showFilter = event;       
    }

    populate(){
        forkJoin([
            this.listS.getlistbranches(),
            this.listS.getliststaffteam(),
            this.listS.getliststaffgroup()
        ]).pipe(
        takeUntil(this.unsubscribe$))
        .subscribe(data => {
            this.populateList = {
                branches: data[0],
                teams: data[1],
                roles: data[2]
            }
        })
    }

    writeValue(value: any){
        if(value != null) {
            this.user = value;
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }


    allBranchChanges(event: boolean){
       
        this.ngZone.runOutsideAngular(() => {
            var branches: any = this.populateList.branches;
            for(var a = 0, len = branches.length; a < len ; a++ ){
                branches[a].isChecked = event;
            }
        })

    }

    allTeamChanges(event: boolean){
        this.ngZone.runOutsideAngular(() => {
            var teams: any = this.populateList.teams;
            for(var a = 0, len = teams.length; a < len ; a++ ){
                teams[a].isChecked = event;
            }
        })        
    }

    allRoleChanges(event: boolean){
        this.ngZone.runOutsideAngular(() => {
            var roles: any = this.populateList.roles;
            for(var a = 0, len = roles.length; a < len ; a++ ){
                roles[a].isChecked = event;
            }
        })    
    }

    selectedRoleChanges(event: boolean, index: number){
        if(!event){
            this.allRole = event
        } else {
            this.allRole = this.detectCheckedList(this.populateList.roles)
        }
    }

    selectedBranchChanges(event: boolean, index: number){
        if(!event){
            this.allBranch = event
        } else {
            this.allBranch = this.detectCheckedList(this.populateList.branches)
        }
    }

    selectedTeamChanges(event: boolean, index: number){
        if(!event){
            this.allTeam = event
        } else {
            this.allTeam = this.detectCheckedList(this.populateList.teams)
        }
    }

    detectCheckedList(list: Array<any>): boolean{
        var notChecked = list.filter(x => !(x.isChecked));
        return notChecked.length > 0 ? false : true;
    }

    search(input: InputParams){

        this.clientS.getqualifiedstaff({
            RecipientCode: input.RecipientCode,
            User: input.User,
            BookDate: input.BookDate,
            StartTime: input.StartTime,
            EndTime: input.EndTime,
            EndLimit: input.EndLimit,
            Gender: input.Gender,
            Competencys: input.Competencys,
            CompetenciesCount: input.CompetenciesCount
        }).subscribe((data: any) => {
            console.log(data);
            this.original = data.map(x => {
                var gender = -1;
                
                if(x.gender && (x.gender[0]).toUpperCase() == 'F'){
                    gender = 0;
                }

                if(x.gender && (x.gender[0]).toUpperCase() == 'M'){
                    gender = 1;
                }

                return {
                    firstName: x.firstName,
                    age: x.age,
                    rating: x.rating,
                    km: x.km,
                    gender: gender,
                    accountNo: x.accountNo
                };      
            });

            this.results = this.original;
        });
    }

    selected(index: number){
        if(index == this.selectedIndex){
            this.onChange('');
            this.selectedIndex = -1;
            return;
        }

        this.onChange(this.results[index]);
        this.selectedIndex = index;
    }

}