using System;
using System.ComponentModel;
public static class DateFilter {
    public static dynamic Convert(DateTime? input)
    {
        if(input != null){
            return input;
        }
        return DBNull.Value;
    }
}