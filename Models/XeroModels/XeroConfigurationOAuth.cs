using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Adamas.Models.XeroModels {
    public class XeroConfigurationOAuth
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set;}
        public string CallbackUri { get; set;}
        public string Scope { get; set; }
        public string State { get; set;}
    }
}