﻿using Adamas.Models;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Adamas.Models.Tables;
using System.Collections.Generic;

namespace AdamasV3.DAL
{
    public class TokenService : ITokenService 
    {
        private readonly DatabaseContext _context;
        public TokenService(
            DatabaseContext context
            ) {
            _context = context;
        }
        public async Task BackgroundServiceProcess()
        {
            await RemoveExpiredTokens();
        }

        public async Task RemoveExpiredTokens()
        {
            var listOfTokens = await (from lu in _context.LoggedUsers select lu).ToListAsync();
            List<LoggedUsers> invalidUsers = new List<LoggedUsers>();

            foreach ( var token in listOfTokens )
            {
                if (!CheckTokenIsValid(token.Token))
                {
                    invalidUsers.Add(token);
                }
            }

            _context.LoggedUsers.RemoveRange(invalidUsers);
            _context.SaveChanges();
        }

        public static long GetTokenExpirationTime(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            var tokenExp = jwtSecurityToken.Claims.First(claim => claim.Type.Equals("exp")).Value;
            var ticks = long.Parse(tokenExp);
            return ticks;
        }

        public static bool CheckTokenIsValid(string token)
        {
            var tokenTicks = GetTokenExpirationTime(token);
            var tokenDate = DateTimeOffset.FromUnixTimeSeconds(tokenTicks).UtcDateTime;

            var now = DateTime.Now.ToUniversalTime();
            var valid = tokenDate >= now;
            return valid;
        }
    }
}
