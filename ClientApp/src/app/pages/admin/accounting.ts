import { Component, OnInit, OnDestroy, Input, AfterViewInit} from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';

@Component({
    templateUrl: './accounting.html',
    styles: [`
    
 
    `]
})

export class AccountingAdmin implements OnInit, OnDestroy, AfterViewInit{
    


    constructor(
        private router:         Router,
        private http:           HttpClient,
        private fb:             FormBuilder,
        private sanitizer:      DomSanitizer,
        private modalService:   NzModalService,
        private listS:          ListService
        ){}

        ngOnInit(): void {
            
        }

        ngAfterViewInit(): void {
            
        }

        onChange(result: Date): void {
            // console.log('onChange: ', result);
        }  
        
        ngOnDestroy(): void {
        }
    }
    
    