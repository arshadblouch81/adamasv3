using System;
using System.Threading.Tasks;

namespace Adamas.DAL
{
    public interface IClientService
    {
        Task<string> GetPrimaryManager(string accountNo);
    }
}