using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class IM_NotifiableStaff
    {
        [Key]
        public int RecordNo { get; set; }
        [Column("IM_Master#")]
        public int IM_MasterId { get; set; }
        [Column("Staff#")]
        public string Staff { get; set; }
    }
}