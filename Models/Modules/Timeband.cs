using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class Timeband
    {
      public int? RECORDNO { get; set; } = 0;
      public string Award { get; set; }
      public DateTime? StartTime { get; set; }=null;
      public DateTime? EndTime { get; set; }=null;
      public string PayType { get; set; }=null;
      public string vRank {get;set;} = null;
      public string DayType {get;set;} = null; 
      
    }
}
