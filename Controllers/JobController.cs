using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;

using ProgramEntities = Adamas.Models.Modules.Program;
using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;  


namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    public class JobController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;

        public JobController(
            DatabaseContext context, 
            IConfiguration config, 
            IGeneralSetting setting)
        {
            _context = context;
            GenSettings = setting;
        }

      [HttpPost("start-job")]
      public async Task<IActionResult> PostJob([FromBody] StartJob job){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                     using(SqlCommand sp = new SqlCommand(@"[Porocess_Start_Job]", (SqlConnection) conn, transaction)){
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@Recordno", job.RecordNo);
                        sp.Parameters.AddWithValue("@cancel", job.Cancel);                        
                        sp.Parameters.AddWithValue("@timeStamp", job.TimeStamp);                    
                        sp.Parameters.AddWithValue("@latitude", job.Latitude);
                        sp.Parameters.AddWithValue("@longitude", job.Longitude);
                        sp.Parameters.AddWithValue("@location", job.Location);

                        await sp.ExecuteNonQueryAsync();
                    }
                      
                    transaction.Commit();
                    return Ok(true);
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
      }

      [HttpPost("end-job")]
      public async Task<IActionResult> PostEndJob([FromBody] StartJob job){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                     using(SqlCommand sp = new SqlCommand(@"[Process_End_Job]", (SqlConnection) conn, transaction)){
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@Recordno", job.RecordNo);
                        sp.Parameters.AddWithValue("@cancel", job.Cancel);                        
                        sp.Parameters.AddWithValue("@timeStamp", job.TimeStamp);                    
                        sp.Parameters.AddWithValue("@latitude", job.Latitude);
                        sp.Parameters.AddWithValue("@longitude", job.Longitude);
                        sp.Parameters.AddWithValue("@location", job.Location);

                        await sp.ExecuteNonQueryAsync();
                    }
                    transaction.Commit();
                    return Ok(true);
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
      }

    }
}
