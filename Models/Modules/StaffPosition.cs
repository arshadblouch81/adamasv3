using System;

namespace Adamas.Models.Modules
{
    public class StaffPosition
    {
        public string PersonID { get; set; }
        public string Position { get; set; }        
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PositionID { get; set; }
        public string Notes { get; set; }
    }
}

