import { ChangeDetectorRef, Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { takeUntil, timeout } from 'rxjs/operators';
import { setDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common'
import { formatDate } from '@angular/common';
import addYears from 'date-fns/addYears';
import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
    selector: 'app-hcp-claim-rate-update',
    templateUrl: './hcpCdcClaimRateUpdate.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}
  `]
})
export class HcpCdcClaimRateUpdateComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;

    loading: boolean = false;
    modalOpen: boolean = false;
    inputForm: FormGroup;
    dateFormat: string = 'dd/MM/yyyy';
    postLoading: boolean = false;
    title: string = "HCP Claim Rate Update";
    token: any;
    check: boolean = false;
    userRole: string = "userrole";
    operatorID: any;
    id: string;
    private unsubscribe: Subject<void> = new Subject();
    allFundingChecked: boolean;
    date: moment.MomentInput;
    chkDoUnAcceptedQuotes: boolean; // --UPDATE_UNACCEPTED
    chkDoAcceptedQuotes: boolean; // --UPDATE_ACCEPTED
    chkDoQuoteAdmin: boolean; // --UPDATE_QUOTE_ADMIN
    chkDoMasterAdmin: boolean; // --UPDATE_MASTER_ADMIN
    chkDoCurrentAdmin: boolean; // --UPDATE_CURRENT_ADMIN
    startingDate: string = 'dd/MM/yyyy';
    s_Filt: any;
    timeSheetUpdateValue: any;
    confirmModal?: NzModalRef;
    updatedRecords: any;
    s_DateFilter: any;

    constructor(
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private menuS: MenuService,
        private listS: ListService,
        private billingS: BillingService,
        private modal: NzModalService,
        public datepipe: DatePipe,
    ) { }

    ngOnInit(): void {
        this.token = this.globalS.decode();
        this.buildForm();
        this.loading = false;
        this.modalOpen = true;
    }
    loadTitle() {
        return this.title
    }
    handleCancel() {
        this.inputForm.reset();
        this.buildForm();
        this.postLoading = false;
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/hcp']);
    }
    buildForm() {
        this.inputForm = this.formBuilder.group({
            chkDoUnAcceptedQuotes: true,
            chkDoAcceptedQuotes: false,
            chkDoQuoteAdmin: true,
            chkDoMasterAdmin: true,
            chkDoCurrentAdmin: true,
            startingDate: startOfMonth(new Date()),
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    runProcess() {
        this.operatorID = this.token.nameid;
        let sql = "SELECT timesheetupdate AS timeSheetUpdate FROM userinfo WHERE name = '" + this.operatorID + "'";
        this.loading = true;
        this.listS.getlist(sql).subscribe(data => {
            this.timeSheetUpdateValue = data[0].timeSheetUpdate;
            if (this.timeSheetUpdateValue != 0) {
                this.confirmModal = this.modal.confirm({
                    nzTitle: 'CDC Packages Rate Update Confirmation',
                    nzContent: 'Selecting this option will update all active recipient CDC packages to the currently set rates in the CDC Rates file - and will take effect immediately. You should not do this until all claim periods using the old rates have been completed and closed off. Do you wish to proceed?',
                    nzOnOk: () => {
                        this.chkDoUnAcceptedQuotes = this.inputForm.get('chkDoUnAcceptedQuotes').value;
                        this.chkDoAcceptedQuotes = this.inputForm.get('chkDoAcceptedQuotes').value;
                        this.chkDoQuoteAdmin = this.inputForm.get('chkDoQuoteAdmin').value;
                        this.chkDoMasterAdmin = this.inputForm.get('chkDoMasterAdmin').value;
                        this.chkDoCurrentAdmin = this.inputForm.get('chkDoCurrentAdmin').value;
                        this.startingDate = this.inputForm.get('startingDate').value;
                        this.startingDate = formatDate(this.startingDate, 'yyyy/MM/dd', 'en_US');

                        if (this.chkDoAcceptedQuotes == true && this.chkDoUnAcceptedQuotes == true) {
                            this.s_Filt = ` AND DOCUMENTGROUP IN ('CP_QUOTE', 'CAREPLAN') `
                        }
                        if (this.chkDoAcceptedQuotes == true && this.chkDoUnAcceptedQuotes == false) {
                            this.s_Filt = ` AND DOCUMENTGROUP IN ('CAREPLAN') `
                        }
                        if (this.chkDoAcceptedQuotes == false && this.chkDoUnAcceptedQuotes == true) {
                            this.s_Filt = ` AND DOCUMENTGROUP IN ('CP_QUOTE') `
                        }
                        this.billingS.hcpUpdateRecipientPrograms().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                                if (data) {
                                    if (data == true) {
                                        this.globalS.sToast('Success', 'Recipient Programs package record with new income rate has been udpated.')
                                    } else {
                                    }
                                    return false;
                                }
                            });
                        if (this.chkDoAcceptedQuotes == true || this.chkDoUnAcceptedQuotes == true) {
                            this.billingS.hcpUpdateQuotes(this.s_Filt).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                                if (data) {
                                    if (data == true) {
                                        this.globalS.sToast('Success', 'Quotes with new income rate has been udpated.')
                                    } else {
                                    }
                                    return false;
                                }
                            });
                        }
                        if (this.chkDoMasterAdmin == true || this.chkDoCurrentAdmin == true) {
                            if (this.chkDoMasterAdmin == true) {
                                if (this.chkDoCurrentAdmin == true) {
                                    this.s_DateFilter = ` AND (r1.DATE < '2000/01/01' OR r1.DATE >= '` + this.startingDate + `') `
                                } else {
                                    this.s_DateFilter = ` AND (r1.DATE < '2000/01/01') `
                                }
                            } else {
                                if (this.chkDoCurrentAdmin == true) {
                                    this.s_DateFilter = ` AND (r1.DATE < '2000/01/01') `
                                }
                            }
                            this.billingS.hcpAdjustRostersCmPkg({
                                s_DateFilter: this.s_DateFilter
                            }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                                    if (data) {
                                        console.log(data[0].updatedRecords);
                                        if (data == true) {
                                            this.globalS.sToast('Success', 'Rosters CM and PKG admin for new income rate has been updated.')
                                        } else {
                                        }
                                        return false;
                                    }
                                });
                        }
                    },
                    nzOnCancel: () => {
                    }
                });
            } else {
            }
        });
    }
}


