import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { takeUntil, timeout } from 'rxjs/operators';
import { setDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { DatePipe } from '@angular/common'
import { formatDate } from '@angular/common';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import addYears from 'date-fns/addYears';
import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
  selector: 'app-billing',
  templateUrl: './closeRoster.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  },
  textarea{
    resize:none;
  },
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  .header{
      background-color: #85B9D5;
      border-color: #85B9D5;
      color: #fff;
      padding: 10px;
  }
  .header-dark-orange{
      line-height: 1;
      padding: 10px;
      background-color: #f18805;
      color: #fff;
      border-color: #f18805;
  }
  .header-orange{
      color: #fff;
      padding: 10px;
      line-height: 1;
      background-color: #ffba08;
      border-color: #ffba08;
  }
  .uns-button{
        font-size: 10pt; 
        text-align: center;
        background-color: #0E86D4;
        border-color: #0E86D4;
        padding: 3px; 
        height: 1.75pc; 
        width: 5.5pc;
  } 
  .sav-button{
        font-size: 14pt;
        margin-top: 10px; 
        height: 2.4pc; 
        float: right;
        text-align: center;
        background-color: #85B9D5;
        padding: 3px; 
        width: 6pc;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: #0E86D4 !important;
    border-color: #0E86D4 !important;
  }
  `]

  // .ant-checkbox-checked .ant-checkbox-inner {
  //   background-color: #85B9D5 !important;
  //   border-color: #000000 !important;
  // }

})
export class CloseRosterComponent implements OnInit {

  fundingList: Array<any>;
  programList: Array<any>;
  tableData: Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  chkId: string;
  confirmModal?: NzModalRef;
  modalVariables: any;
  dateFormat: string = 'dd/MM/yyyy';
  inputVariables: any;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  title: string = "Close Rostering";
   
  token: any;
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean = false;
  check: boolean = false;
  temp_title: any;
  settingForm: FormGroup;
  userRole: string = "userrole";
  whereString: string = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  dtpEndDate: any;
  lastMonthEndDate: any;
  id: string;
  btnid: any;
  btnid1: any;
  private unsubscribe: Subject<void> = new Subject();
  allFundingChecked: boolean;
  allProgramsChecked: boolean;
  selectedPrograms: any;
  selectedFunding: any;
  date: moment.MomentInput;
  updatedRecords: any;
  chkBlockFunded: boolean = true;
  chkIndividualPackages: boolean = true;
  chkNonCDCPackages: boolean = true;
  chkCDCPackages: boolean = true;
  // vBlockFunded: number;
  // vIndividualPackages: number;
  // vNonCDCPackages: number;
  // vCDCPackages: number;
  // confirmModal?: NzModalRef;
  vBlockFunded: string = '1';
  vIndividualPackages: string = '1';
  vNonCDCPackages: string = '1';
  vCDCPackages: string = '1';
  xValue: string;
  xCondition: string;
  lockNonCDCPackages: any = true;
  lockCDCPackages: any = true;
  lockFunding: any = true;
  lockProgram: any = true;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private modal: NzModalService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    public datepipe: DatePipe,
  ) { }

  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    this.EndofMOnth();
    this.loadFunding();
    this.getProgramPackageDetail();
    // this.loadPrograms();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  resetModal() {
    this.current = 0;
    this.inputForm.reset();
    this.postLoading = false;
  }
  handleCancel() {
    this.modalOpen = false;
    this.router.navigate(['/admin/pays']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      dtpEndDate: null,
      chkBlockFunded: false,
      chkIndividualPackages: true,
      chkNonCDCPackages: true,
      chkCDCPackages: true,
    });
  }

  EndofMOnth() {
    const lastMonthEndDate = new Date();
    lastMonthEndDate.setMonth(lastMonthEndDate.getMonth() - 1);
    this.dtpEndDate = endOfMonth(lastMonthEndDate);
    this.inputForm.patchValue({
      dtpEndDate: this.dtpEndDate,
    });
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedFunding = event;
    if (index == 2)
      this.selectedPrograms = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      // this.fundingList.forEach(x => {
      //   x.checked = true;
      //   this.allFundingChecked = x.description;
      //   this.allFundingChecked = true;
      // });
      if (this.allFundingChecked == false) {
        this.lockFunding = false;
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = true;
      }
    }
    // if (index == 1) {
    //   this.lockFunding = true;
    //   this.fundingList.forEach(x => {
    //     x.checked = true;
    //     this.allFundingChecked = x.description;
    //     this.allFundingChecked = true;
    //   });
    // }
    if (index == 2) {
      // this.programList.forEach(x => {
      //   x.checked = true;
      //   this.allProgramsChecked = x.description;
      //   this.allProgramsChecked = true;
      // });
      if (this.allProgramsChecked == false) {
        this.lockProgram = false;
      }
      else {
        this.programList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockProgram = true;
      }
    }
    // if (index == 2) {
    //   this.lockProgram = true;
    //   this.programList.forEach(x => {
    //     x.checked = true;
    //     this.allProgramsChecked = x.description;
    //     this.allProgramsChecked = true;
    //   });
    // }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockFunding = false;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
    if (index == 2) {
      this.lockProgram = false;
      this.programList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
  }

  checkedStatus(index: any): void {
    if (index == 11) {
      if (this.chkBlockFunded == false) {
        this.vBlockFunded = '0';
      }
      else {
        this.vBlockFunded = '1';
      }
    }
    if (index == 12) {
      if (this.chkIndividualPackages == false) {
        this.lockNonCDCPackages = false;
        this.lockCDCPackages = false;
        this.vIndividualPackages = '0';
      }
      else {
        this.vIndividualPackages = '1';
        this.chkNonCDCPackages = true;
        this.chkCDCPackages = true;
        this.vNonCDCPackages = '1';
        this.vCDCPackages = '1';
        this.lockNonCDCPackages = true;
        this.lockCDCPackages = true;
      }
    }
    if (index == 13) {
      if (this.chkNonCDCPackages == false) {
        this.vNonCDCPackages = '0';
      }
      else {
        this.vNonCDCPackages = '1';
      }
    }
    if (index == 14) {
      if (this.chkCDCPackages == false) {
        this.vCDCPackages = '0';
      }
      else {
        this.vCDCPackages = '1';
      }
    }
    this.getProgramPackageDetail();
  }

  loadFunding() {
    this.loading = true;
    this.menuS.getlistFundingSource(this.check).subscribe(data => {
      this.fundingList = data;
      this.tableData = data;
      this.loading = false;
      this.allFundingChecked = true;
      this.checkAll(1);
    });
  }

  getProgramPackageDetail() {

    this.xValue = this.vBlockFunded + this.vIndividualPackages + this.vNonCDCPackages + this.vCDCPackages;

    // console.log(this.xValue)

    switch (this.xValue) {
      case ("0000" || "0001" || "0010" || "0011" || "0100"):
        this.confirmModal = this.modal.info({
          nzTitle: 'Alert',
          nzContent: 'You must have at least Block Funded or Individual Funding type of package/program selected for display.',
          nzOnOk: () => { }
        });
        break;
      case ("1000" || "1001" || "1011" || "1010" || "1100"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 0"
        break;
      case ("0101"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 1"
        break;
      case ("0110"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 0"
        break;
      case ("0111"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 1"
        break;
      case ("1110"):
        this.xCondition = " AND (ISNULL(UserYesNo2, 0) = 0) OR (ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 0)"
        break;
      case ("1101"):
        this.xCondition = " AND (ISNULL(UserYesNo2, 0) = 0) OR (ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 1)"
        break;
      case ("1111"):
        this.xCondition = " AND (ISNULL(UserYesNo2, 0) = 0) OR (ISNULL(UserYesNo2, 0) = 1)"
        break;
      default:
        break;
    }

    let dataPass = {
      condition: this.xCondition
    }
    this.billingS.getProgramPackageslist(dataPass).subscribe(data => {
      this.programList = data;
      this.tableData = data;
      this.allProgramsChecked = true;
      this.checkAll(2);
      this.loading = false;
      this.postLoading = false;
    });
    // console.log(this.chkBlockFunded + " , " + this.chkIndividualPackages + " , " + this.chkNonCDCPackages + " , " + this.chkCDCPackages);
  }

  // loadPrograms() {
  //   this.loading = true;
  //   this.menuS.getlistProgramPackages(this.check).subscribe(data => {
  //     this.programList = data;
  //     this.tableData = data;
  //     this.loading = false;
  //     this.allProgramsChecked = true;
  //     this.checkAll(2);
  //   });
  // }

  // fetchAll(e) {
  //   if (e.target.checked) {
  //     this.whereString = "WHERE";
  //     this.loadPrograms();
  //   } else {
  //     this.whereString = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  //     this.loadPrograms();
  //   }
  // }

  checkValidation() {
    this.selectedFunding = this.fundingList
      .filter(opt => opt.checked)
      .map(opt => opt.description).join(",")

    this.selectedPrograms = this.programList
      .filter(opt => opt.checked)
      .map(opt => opt.title).join(",")

    this.dtpEndDate = this.inputForm.get('dtpEndDate').value;
    this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy-MM-dd hh:mm', 'en_US');

    if (this.selectedFunding != '' && this.selectedPrograms != '' && this.dtpEndDate != '') {
      this.closeRosterPeriod();
    } else if (this.selectedFunding == '') {
      this.globalS.eToast('Error', 'Please select atleast one Funding to proceed')
    } else if (this.selectedPrograms == '') {
      this.globalS.eToast('Error', 'Please select atleast one Program to proceed')
    } else if (this.dtpEndDate == '') {
      this.globalS.eToast('Error', 'Please select valid Date to proceed')
    }
  }

  closeRosterPeriod() {
    this.postLoading = true;
    this.billingS.closeRosterPeriod({
      Closedate: this.dtpEndDate,
      Programs: this.selectedPrograms,
      Fundings: this.selectedFunding,
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'No Roster Entries are existing to Close Date.')
          } else {
            this.globalS.sToast('Success', 'Close Roster Date has been updated successfully on ' + this.updatedRecords + ' Records.')
          }
          this.postLoading = false;
          this.ngOnInit();
          return false;
        }
      });
  }
}


