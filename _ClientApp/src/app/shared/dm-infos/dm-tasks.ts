import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { TimeSheetService } from '@services/index'

@Component({
    selector: 'dm-tasks',
    templateUrl: './dm-tasks.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DmTasksComponent),
            multi: true
        }
    ]
})

export class DmTasksComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    results: Array<any>

    constructor(
        private timeS: TimeSheetService
    ){ }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        this.timeS.gettasks(user.recordno)
            .subscribe(data => {
                this.results = data
            })
    }
}