using System;

namespace Adamas.Models.Modules
{
    public class Person
    {

        public int PersonId { get; set; }
        public string AccountNo { get; set; }
        public string Suberb { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FullAddress { get; set; }      

        public int GetAgeInYears()
        {
            return Math.Abs(DateTime.Now.AddYears ( DateOfBirth.Year).Year);
        }   
       
    }
}
