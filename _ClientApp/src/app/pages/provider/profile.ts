import { Component, OnInit } from '@angular/core';
import { ClientService,StaffService, GlobalService, months, days } from '../../services/index';

import { Observable } from 'rxjs';




import { FormControl, FormGroup, FormArray , Validators, FormBuilder } from '@angular/forms';
import { group } from '@angular/animations';
import { RemoveFirstLast } from '../../pipes/pipes';

@Component({
    selector: '',
    templateUrl: './profile.html',
    styles:[
        `
        label{
            display:block;
        }
        .row{
            margin:15px 0;
        }
        .margin-space{
            margin:0 5px;
        }
        .table{
            margin:0;
        }
        `
    ]
})

export class ProfileProvider implements OnInit {

    private token: any;
    user:any;

    constructor(
        private globalS: GlobalService,
    ){

    }

    ngOnInit(){
        this.token = this.globalS.decode()
        this.user = {
            code: this.token.code,
            view: 'staff'
        };

    }
   
}