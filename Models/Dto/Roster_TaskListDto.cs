using System;

namespace AdamasV3.Models.Dto
{
    public class Roster_TaskListDto
    {
        public int RecordNo { get; set; }
        public int RosterID { get; set; }
        public int TaskID { get; set; }
        public string TaskDetail { get; set; }
        public string TaskNotes { get; set; }
        public string TaskOrder { get; set; }
        public bool? TaskComplete { get; set; }
        public bool? XDeletedRecord { get; set; }
        public DateTime? XEndDate { get; set; }
    }
}