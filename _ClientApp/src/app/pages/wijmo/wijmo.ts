import { Component, AfterViewInit, ViewChild } from '@angular/core'
import { HttpClientModule, HttpClient } from '@angular/common/http';

import * as wjcViewer from 'wijmo/wijmo.viewer';



@Component({
    selector: '',
    templateUrl: './wijmo.html' 
})

export class WijmoComponent implements AfterViewInit {

    private static None = 'None';
    @ViewChild('reportViewer', { static: false }) public reportViewer: wjcViewer.ReportViewer;

    public arItems: IArJsonCategories;
    public flexItems: IFlexJsonCategories;
    public ssrsItems: ISsrsJsonCategories;
    public filePath: string;
    public reportName: string;
    public serviceUrl: string;
    public paginated: boolean;
    public reportParameters: parameter[];

    private _arReportInfo = '';
    private _flexReportInfo = '';
    private _ssrsReportInfo = '';
    private _arServiceUrl = 'http://45.77.37.207:8001/ActiveReports.ReportService.asmx';

    urlJson = "../../arReport.config.json"

    constructor(
        private http: HttpClient
    ){
        this.http.get('assets/arReport.config.json')
            .subscribe(res => {
            var arReports = <IArJson>res
            this.arItems = arReports.categories;
            this.arReportInfo = this._getSelectedArReport(arReports);
            })
    }

    ngAfterViewInit() {
        var resize = () => {
            if (this.reportViewer) {
                this.reportViewer.hostElement.style.height = (Math.max(window.innerHeight * 0.95, 300)) + 'px';
            }
        };

        resize();
        window.addEventListener('resize', resize);
    }

    get arReportInfo(): string {
        return this._arReportInfo;
    }
    set arReportInfo(value: string) {
        if (this._arReportInfo !== value) {
            this._loadArReport(this._arReportInfo = value);
        }
    }    
        

    private _getSelectedArReport(json: IArJson): string {
        var res: string = '' ;

        for (var i = 0; i < json.categories.length && !res; i++) {
            var category = json.categories[i];

            for (var j = 0; j < category.reports.length && !res; j++) {
                var report = category.reports[j];

                if (category.name === json.selectedReport.categoryName && report.reportName === json.selectedReport.reportName) {
                    res = report.fileName;
                   
                }
            }
        }
      //  res = 'Reports/RdlReport1.rdlx';
        return res;
    }

   

    private _loadArReport(info: string): void {
        console.log(' Object Received : '+ info);
        var parm: parameter[];

        if (this.reportViewer && info) {

            parm = [{ name:"Date", value:"2019/01/18"}, { name:"Date",value:"2019/01/19"}];
            this.filePath = info;
            this.reportName = '';//info.reportName;
            this.serviceUrl = this._arServiceUrl;
            this.paginated = false;
            this.reportParameters = parm;
        }
    }
}

interface Report {    
    fileName: string,
    reportName: string 
}

interface parameter {
    name: string,
    value: string
}

interface IArJson {
    selectedReport: {
        categoryName: string,
        reportName: string
    },
    categories: IArJsonCategories
}

export interface IArJsonCategories extends Array<{
name: string,
reports: {
    fileName: string,
    reportName: string
   
}[]
}> {
}

interface IFlexJson {
selectedReport: {
    categoryName: string,
    reportName: string
   
},
categories: IFlexJsonCategories
}

export interface IFlexJsonCategories extends Array<{
name: string,
text: string,
reports: {
    fileName: string,
    reportName: string,
    reportTitle: string
   
}[]
}> {
}

interface ISsrsJson {
selectedReport: {
    categoryName: string,
    reportName: string
    
},
categories: ISsrsJsonCategories
}

export interface ISsrsJsonCategories extends Array<{
name: string,
text: string,
reports: {
    reportPath: string,
    reportName: string,
    reportTitle: string
   
}[]
}> {
}
