namespace Adamas.Models.Modules{
    public class GetBillingRate {
        public string RecipientCode { get; set; }
        public string Program { get; set; }
        public string ActivityCode { get; set; }
    }

}