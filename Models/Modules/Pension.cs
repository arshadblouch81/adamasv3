namespace Adamas.Models.Modules
{
    public class Pension
    {
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Notes { get; set; }
    }

}
