namespace Adamas.Models.Modules{
    public class DataDomain
    {
        public int RecordNumber { get; set;}
        public string Domain { get; set; }
        public string Description { get; set; }
        public string  Group { get; set; }
        public string PersonId { get; set; }
        public bool status { get; set; }      

    }
}