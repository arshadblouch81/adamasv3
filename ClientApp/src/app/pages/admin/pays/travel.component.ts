import { ChangeDetectorRef, Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { HttpClient, HttpClientModule, HttpRequest, HttpEventType, HttpResponse, HttpEvent } from '@angular/common/http';
import { ConnectableObservable, Observable, Subject } from 'rxjs';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, MapService } from '@services/index';
import { timeout } from 'rxjs/operators';
import { formatDate, formatNumber } from '@angular/common';
import { setDate } from 'date-fns';
import { round, toNumber } from 'lodash';
import { filter } from 'rxjs/operators';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ConsoleService } from '@ng-select/ng-select/lib/console.service';

@Component({
  selector: 'app-billing1',
  templateUrl: './travel.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  },
  textarea{
    resize:none;
  },
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  .header{
      background-color: #85B9D5;
      border-color: #85B9D5;
      color: #fff;
      padding: 10px;
  }
  .header-dark-orange{
      line-height: 1;
      padding: 10px;
      background-color: #f18805;
      color: #fff;
      border-color: #f18805;
  }
  .header-orange{
      color: #fff;
      padding: 10px;
      line-height: 1;
      background-color: #ffba08;
      border-color: #ffba08;
  }
  `]
})
export class TravelComponent implements OnInit {

  branchList: Array<any>;
  programList: Array<any>;
  categoriesList: Array<any>;
  staffList: Array<any>;
  fundingList: Array<any>;
  tableData: Array<any>;
  PayPeriodLength: number;
  PayPeriodEndDate: any;
  lockBranches: any = false;
  lockPrograms: any = false;
  lockCategories: any = false;
  lockStaff: any = false;
  lockFunding: any = false;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  dateFormat: string = 'dd/MM/yyyy';
  postLoading: boolean = false;
  title: string = "Travel Audit And Update";
   
  token: any;
  check: boolean = false;
  dtpEndDate: any;
  dtpStartDate: any;
  selectedBranches: any;
  selectedPrograms: any;
  selectedStaffs: any;
  selectedCategories: any;
  selectedFunding: any;
  allBranchesChecked: boolean;
  allProgramsChecked: boolean;
  allStaffsChecked: boolean;
  allCategoriesChecked: boolean;
  allFundingChecked: boolean;
  chkSeperateClaim: boolean;
  chkAutoFillGap: boolean;
  chkClearTravelAudit: boolean;
  chkDelExistingTravel: boolean;
  chkStartBranch: boolean;
  chkFinishBranch: boolean;
  chkAvoidTolls: boolean;
  chkAvoidHighways: boolean;
  chkAvoidFerries: boolean;
  chkIncUnapprovedEntries: boolean;

  // modelAutoCreateKM: boolean = false;
  // modelAutoCreateTime: boolean = false;

  // modeltravelKMProgram: boolean;
  // modeltravelKMActivity: boolean;
  // modeltravelKMPaytype: boolean;
  // modeltravelTimeProgram: boolean;
  // modeltravelTimeActivity: boolean;
  // modeltravelTimePaytypeWD: boolean;
  // modeltravelTimePaytypeSat: boolean;
  // modeltravelTimePaytypeSun: boolean;
  // modeltravelTimePaytypePH: boolean;

  travelProgramList: Array<any>;
  travelActivityList: Array<any>;
  travelPaytypeList: Array<any>;
  paytypeList: Array<any>;
  activityList: Array<any>;
  travelPayList: Array<any>;

  lockTravelKMProgram: any = false;
  lockTravelKMActivity: any = false;
  lockTravelKMPaytype: any = false;
  lockTravelTimeProgram: any = false;
  lockTravelTimeActivity: any = false;
  lockTravelTimePaytypeWD: any = false;
  lockTravelTimeProgramSat: any = false;
  lockTravelTimeActivitySun: any = false;
  lockTravelTimePaytypePH: any = false;

  chkAutoCreateTime: boolean;
  cmbTravelProgram1: any;
  cmbTravelActivity1: any;
  cmbTravelPaytype1: any;
  cmbTravelPaytype2: any;
  cmbTravelPaytype3: any;
  cmbTravelPaytype4: any;

  chkAutoCreateKM: boolean;
  cmbTravelProgram0: any;
  cmbTravelActivity0: any;
  cmbTravelPaytype0: any;

  private unsubscribe: Subject<void> = new Subject();
  operatorID: any;
  batchNumber: any;
  uniqueBatchKey: any;
  currentDateTime: any;
  RSC_Program: any;
  RSC_Cycle: any;
  RSC_BRANCH: any;
  RSC_STAFF: any;
  travelProvider: any;
  googleCustID: any;
  providerKey: any;
  b_FirstRecord: boolean;
  panUpdatePercText: any;
  processUpdatePerc: any;
  s_StartAddress: any;
  s_EndAddress: any;
  s_StartClient: any;
  s_StartService: any;
  s_StartRecord: any;
  s_VehicleType: any;
  s_Client: any;
  mapApiQuery: any;
  d_Status: any;
  b_KeyOnly: boolean;
  b_ChangeOfDate: boolean;
  b_ChangeOfStaff: boolean;
  s_EndClient: any;
  s_EndService: any;
  s_EndRecord: any;
  s_Mask: any;
  s_TravelTime: any;
  s_BaseAddress: any;
  s_StartClientStartTime: any;
  s_StartClientEndTime: any;
  s_CalculatedEndAddress: any;
  s_AlternatePickupAddress: any;
  timeDuration: any;
  s_Date: any;
  s_Staff: any;
  s_EndClientStartTime: any;
  s_EndClientEndTime: any;
  s_TakeToAddress: any;
  d_Meters: any;
  d_Seconds: any;
  updatedRecords: any;
  s_Log: any = '';
  s_StartTimer: any;
  s_EndTimer: any;
  confirmModal?: NzModalRef;
  i_Count: number = 0;
  d_RequestDuration: any;
  s_Duration: any;

  // TEST to REMOVE
  awardHistoryList: Array<any>;


  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    private mapS: MapService,
    private modal: NzModalService,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    // this.GetPayPeriodLength();
    this.GetPayPeriodEndDate();
    this.buildForm();
    this.getTravelDetails();
    this.loadBranches();
    this.loadPrograms();
    this.getStaffDetails();
    this.getFundingDetails();
    this.loadCategories();
    // this.getAwardList();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  resetModal() {
    this.current = 0;
    this.inputForm.reset();
    this.postLoading = false;
  }
  handleCancel() {
    this.modalOpen = false;
    this.router.navigate(['/admin/pays']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      name: null,
      chkSeperateClaim: true,
      chkClearTravelAudit: true,
      chkDelExistingTravel: false,
      chkStartBranch: false,
      chkFinishBranch: false,
      chkAvoidTolls: false,
      chkAvoidHighways: false,
      chkAvoidFerries: false,
      chkAutoCreateTime: false,
      cmbTravelProgram1: '',
      cmbTravelActivity1: '',
      cmbTravelPaytype1: '',
      cmbTravelPaytype2: '',
      cmbTravelPaytype3: '',
      cmbTravelPaytype4: '',
      chkAutoCreateKM: false,
      cmbTravelProgram0: '',
      cmbTravelActivity0: '',
      cmbTravelPaytype0: '',
      chkAutoFillGap: false,
      chkIncUnapprovedEntries: false,
      dtpStartDate: '01/12/2021',
      dtpEndDate: '01/12/2021',
      invoiceDate: new Date(),
    });
    this.checkAll(11);
  }

  onKeyPress(data: KeyboardEvent) {
    return this.globalS.acceptOnlyNumeric(data);
  }

  loadBranches() {
    this.loading = true;
    this.menuS.getlistbranches(this.check).subscribe(dataBranches => {
      this.branchList = dataBranches;
      this.tableData = dataBranches;
      this.loading = false;
      this.allBranchesChecked = true;
      this.checkAll(1);
    });
  }

  loadPrograms() {
    this.loading = true;
    let dataPassPrograms = {
      condition: formatDate(new Date(), 'MM-dd-yyyy', 'en_US'),
    }
    this.billingS.getProgramslist(dataPassPrograms).subscribe(dataPrograms => {
      this.programList = dataPrograms;
      this.tableData = dataPrograms;
      this.loading = false;
      this.allProgramsChecked = true;
      this.checkAll(2);
    });
  }

  getStaffDetails() {
    this.loading = true;
    this.billingS.getStaffDetails().subscribe(dataStaffDetail => {
      this.staffList = dataStaffDetail;
      this.tableData = dataStaffDetail;
      this.loading = false;
      this.allStaffsChecked = true;
      this.checkAll(4);
    });
  }

  getFundingDetails() {
    this.loading = true;
    this.billingS.getFundingDetails().subscribe(dataFundingDetails => {
      this.fundingList = dataFundingDetails;
      this.tableData = dataFundingDetails;
      this.loading = false;
      this.allFundingChecked = true;
      this.checkAll(5);
    });
  }

  getTravelDetails() {
    this.loading = true;
    this.billingS.getTravelProgram().subscribe(dataTravelPrograms => {
      this.travelProgramList = dataTravelPrograms;
    });
    this.billingS.getTravelActivity().subscribe(datadataTravelActivity => {
      this.travelActivityList = datadataTravelActivity;
    });
    this.billingS.getTravelPaytype().subscribe(dataTravelPaytype => {
      this.travelPaytypeList = dataTravelPaytype;
    });
    this.loading = false;
  }

  loadCategories() {
    this.loading = true;
    this.billingS.getlistcategories().subscribe(dataCategories => {
      this.categoriesList = dataCategories;
      this.tableData = dataCategories;
      this.loading = false;
      this.allCategoriesChecked = true;
      this.checkAll(3);
    });
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedBranches = event;
    if (index == 2)
      this.selectedPrograms = event;
    if (index == 3)
      this.selectedCategories = event;
    if (index == 4)
      this.selectedStaffs = event;
    if (index == 5)
      this.selectedFunding = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allBranchesChecked == false) {
        this.lockBranches = true
      }
      else {
        this.branchList.forEach(x => {
          x.checked = true;
          this.allBranchesChecked = x.description;
          this.allBranchesChecked = true;
        });
        this.lockBranches = false
      }
    }
    if (index == 2) {
      if (this.allProgramsChecked == false) {
        this.lockPrograms = true
      }
      else {
        this.programList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockPrograms = false
      }
    }
    if (index == 3) {
      if (this.allCategoriesChecked == false) {
        this.lockCategories = true
      }
      else {
        this.categoriesList.forEach(x => {
          x.checked = true;
          this.allCategoriesChecked = x.description;
          this.allCategoriesChecked = true;
        });
        this.lockCategories = false
      }
    }
    if (index == 4) {
      if (this.allStaffsChecked == false) {
        this.lockStaff = true
      }
      else {
        this.staffList.forEach(x => {
          x.checked = true;
          this.allStaffsChecked = x.description;
          this.allStaffsChecked = true;
        });
        this.lockStaff = false
      }
    }
    if (index == 5) {
      if (this.allFundingChecked == false) {
        this.lockFunding = true
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = false
      }
    }
    // if (index == 11) {
    //   if (this.modelAutoCreateKM == true) {
    //     this.lockTravelKMProgram = true;
    //     this.lockTravelKMActivity = true;
    //     this.lockTravelKMPaytype = true;
    //   }
    //   else {
    //     this.lockTravelKMProgram = false;
    //     this.lockTravelKMActivity = false;
    //     this.lockTravelKMPaytype = false;
    //   }
    // }
    // if (index == 21) {
    //   if (this.modelAutoCreateTime == true) {
    //     this.lockTravelTimeProgram = true;
    //     this.lockTravelTimeActivity = true;
    //     this.lockTravelTimePaytypeWD = true;
    //     this.lockTravelTimeProgramSat = true;
    //     this.lockTravelTimeActivitySun = true;
    //     this.lockTravelTimePaytypePH = true;
    //   }
    //   else {
    //     this.lockTravelTimeProgram = false;
    //     this.lockTravelTimeActivity = false;
    //     this.lockTravelTimePaytypeWD = false;
    //     this.lockTravelTimeProgramSat = false;
    //     this.lockTravelTimeActivitySun = false;
    //     this.lockTravelTimePaytypePH = false;
    //   }
    // }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockBranches = true;
      this.branchList.forEach(x => {
        x.checked = false;
        this.allBranchesChecked = false;
        this.selectedBranches = [];
      });
    }
    if (index == 2) {
      this.lockPrograms = true;
      this.programList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
    if (index == 3) {
      this.lockCategories = true;
      this.categoriesList.forEach(x => {
        x.checked = false;
        this.allCategoriesChecked = false;
        this.selectedCategories = [];
      });
    }
    if (index == 4) {
      this.lockStaff = true;
      this.staffList.forEach(x => {
        x.checked = false;
        this.allStaffsChecked = false;
        this.selectedStaffs = [];
      });
    }
    if (index == 5) {
      this.lockFunding = true;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
  }

  GetPayPeriodEndDate() {
    let sql = "SELECT convert(varchar, PayPeriodEndDate, 101) AS PayPeriodEndDate FROM SysTable"
    this.loading = true;
    this.listS.getlist(sql).subscribe(dataPayPeriodEndDate => {
      if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
        this.dtpEndDate = dataPayPeriodEndDate[0].payPeriodEndDate;
        this.inputForm.patchValue({
          dtpEndDate: this.dtpEndDate,
        })
      }
      else {
        this.inputForm.patchValue({
          dtpEndDate: new Date()
        });
        this.dtpEndDate = new Date
      }
      let fsql = "SELECT DefaultPayPeriod as DefaultPayPeriod FROM Registration";
      this.listS.getlist(fsql).subscribe(dataDefaultPayPeriod => {
        if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
          this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
        }
        else {
          this.PayPeriodLength = 14
        }
        var firstDate = new Date(this.dtpEndDate);
        firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
        this.dtpStartDate = formatDate(firstDate, 'MM-dd-yyyy', 'en_US');
        this.inputForm.patchValue({
          dtpStartDate: this.dtpStartDate,
        });
      });
    });
  }

  deleteAwardList() {
    let sql = "DELETE from [dbo].[Award_Roster_Syste]"
    this.listS.getlist(sql).subscribe(data => {
      this.globalS.sToast('Success', 'Award details deleted successfully.')
      this.getAwardList();
    });
  }

  getAwardList() {
    let sql = "select RECNUM, award, DATE, DURATION, PAYTYPE, RULETYPE, PROGRAM, ACTIVITY, ACTIVITY_TYPE, JOBTYPE, INFOONLY, NOOVER, ROS_DAY from [dbo].[Award_Roster_Syste]"
    this.listS.getlist(sql).subscribe(data => {
      this.awardHistoryList = data;
      this.tableData = data;
    });
  }

  processAwardInterpreter() {
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Do you want to process Award Interpreter?',
      nzContent: 'This process cannot be undone. <br> Are you sure you want to process Award Interpreter ?',
      nzOnOk: () => {
        this.globalS.iToast('Information', 'Calling Award process ...')
        this.billingS.postAwardInterpreterTest("sysmgr", "sysmgr", "MELLOW MARSHA", "18/06/2023", "True").subscribe(d => {
          this.globalS.sToast('Success', 'Award process executed successfully.')
          this.getAwardList();
        });
      },
      nzOnCancel: () => {
        // this.globalS.sToast('Success', 'CANCEL Button Selected.')
      }
    });
  }

  validateTravelUpdate() {
    if (this.lockBranches == false) {
      this.selectedBranches = null;
      this.RSC_BRANCH = 'ALL'
    } else {
      this.RSC_BRANCH = ""
      this.selectedBranches = this.branchList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockPrograms == false) {
      this.selectedPrograms = null;
      this.RSC_Program = 'ALL'
    } else {
      this.RSC_Program = ""
      this.selectedPrograms = this.programList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockCategories == false) {
      this.selectedCategories = null;
      this.RSC_Cycle = 'ALL'
    } else {
      this.RSC_Cycle = ""
      this.selectedCategories = this.categoriesList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockStaff == false) {
      this.selectedStaffs = null;
      this.RSC_STAFF = 'ALL'
    } else {
      this.RSC_STAFF = ""
      this.selectedStaffs = this.staffList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockFunding == false) {
      this.selectedFunding = null
    } else {
      this.selectedFunding = this.fundingList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.selectedBranches != '' && this.selectedPrograms != '' && this.selectedCategories != '' && this.selectedStaffs != '' && this.selectedFunding != '') {
      // console.log("Operator ID: " + this.operatorID);
      // console.log("Branches: " + this.selectedBranches);
      // console.log("Funding: " + this.selectedFunding);
      // console.log("Programs: " + this.selectedPrograms);
      // console.log("Categories: " + this.selectedCategories);
      // console.log("Staffs: " + this.selectedStaffs);
      // console.log("RSC_ID: " + this.batchNumber);
      // console.log("Current Date: " + this.currentDateTime);
      // console.log("Start Date: " + this.dtpStartDate);
      // console.log("End Date: " + this.dtpEndDate);

      this.startTravelUpdate();
    } else if (this.selectedBranches == '') {
      this.globalS.eToast('Error', 'Please select atleast one Branch to proceed')
    } else if (this.selectedFunding == '') {
      this.globalS.eToast('Error', 'Please select atleast one Funding to proceed')
    } else if (this.selectedPrograms == '') {
      this.globalS.eToast('Error', 'Please select atleast one Program to proceed')
    } else if (this.selectedCategories == '') {
      this.globalS.eToast('Error', 'Please select atleast one Category to proceed')
    } else if (this.selectedStaffs == '') {
      this.globalS.eToast('Error', 'Please select atleast one Staff to proceed')
    }
  }

  startTravelUpdate() {
    this.loading = true;
    this.operatorID = this.token.nameid;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');
    this.dtpStartDate = this.inputForm.get('dtpStartDate').value;
    this.dtpEndDate = this.inputForm.get('dtpEndDate').value;
    let xStartDate = formatDate(this.dtpStartDate, 'yyyy/MM/dd', 'en_US');
    let xEndDate = formatDate(this.dtpEndDate, 'yyyy/MM/dd', 'en_US');
    this.dtpStartDate = formatDate(this.dtpStartDate, 'dd/MM/yyyy', 'en_US');
    this.dtpEndDate = formatDate(this.dtpEndDate, 'dd/MM/yyyy', 'en_US');
    this.chkClearTravelAudit = this.inputForm.get('chkClearTravelAudit').value;
    this.chkDelExistingTravel = this.inputForm.get('chkDelExistingTravel').value;
    this.chkFinishBranch = this.inputForm.get('chkFinishBranch').value;
    this.chkStartBranch = this.inputForm.get('chkStartBranch').value;
    this.chkSeperateClaim = this.inputForm.get('chkSeperateClaim').value;
    this.chkAutoFillGap = this.inputForm.get('chkAutoFillGap').value;
    let dataPassDates = {
      StartDate: this.dtpStartDate,
      EndDate: this.dtpEndDate,
      IsWhere: null
    }

    // let insValues = this.operatorID+"',convert(varchar, '"+this.currentDateTime+"', 103),'"+this.RSC_Program+"','"+this.RSC_Cycle+"','"+this.RSC_BRANCH+"','"+this.RSC_STAFF+"', convert(varchar, '"+this.dtpStartDate+"', 103), convert(varchar, '"+this.dtpEndDate+"', 103), convert(varchar, '"+this.dtpStartDate+"', 103), convert(varchar, '"+this.dtpEndDate+"', 103)";
    let insValues = this.operatorID + "','" + this.currentDateTime + "','" + this.RSC_Program + "','" + this.RSC_Cycle + "','" + this.RSC_BRANCH + "','" + this.RSC_STAFF + "','" + this.dtpStartDate + "','" + this.dtpEndDate + "','" + this.dtpStartDate + "','" + this.dtpEndDate + "'";
    let sql = "INSERT INTO rostercopy (RSC_USER, RSC_COPY_DATE, RSC_Program, RSC_Cycle, RSC_BRANCH, RSC_STAFF, RSC_SourceStart, RSC_SourceEnd, RSC_DestinationStart, RSC_DestinationEnd) VALUES ('" + insValues + ")";
    this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(dataInsertRoster => {
      if (dataInsertRoster) { }
      else {
        this.s_Staff = '';
        this.s_Date = '';
        this.billingS.getRscID(null).subscribe(dataRscID => {
          this.batchNumber = dataRscID[0].rscID;
          this.billingS.getUniqueBatchKey(null).subscribe(dataGetUniqueBatchKey => {
            this.uniqueBatchKey = dataGetUniqueBatchKey[0].uniqueBatchKey;
            this.billingS.updateUniqueBatchKey(this.uniqueBatchKey).pipe(takeUntil(this.unsubscribe)).subscribe(dataUpdateUniqueBatchKey => {
              if (dataUpdateUniqueBatchKey) {
                this.billingS.getTravelUpdate({
                  Branches: this.selectedBranches,
                  Programs: this.selectedPrograms,
                  Categories: this.selectedCategories,
                  Staffs: this.selectedStaffs,
                  Fundings: this.selectedFunding,
                  StartDate: xStartDate,
                  EndDate: xEndDate
                }).pipe(takeUntil(this.unsubscribe)).subscribe(dataTravelPayList => {
                  this.travelPayList = dataTravelPayList;
                  if (dataTravelPayList) {
                    if (dataTravelPayList.length > 0) {
                      this.billingS.getTravelProvider(null).subscribe(dataGetTravelProvider => {
                        this.travelProvider = dataGetTravelProvider[0].travelProvider;
                        this.billingS.getGoogleCustID(null).subscribe(dataGetGoogleCustID => {
                          this.googleCustID = dataGetGoogleCustID[0].googleCustID;
                          this.billingS.getProviderKey(null).subscribe(dataGetProviderKey => {
                            this.providerKey = dataGetProviderKey[0].providerKey;
                            if (this.chkClearTravelAudit == true) {
                              this.billingS.truncateTable(null).pipe(takeUntil(this.unsubscribe)).subscribe(dataTruncateTable => {
                              });
                            }
                            if (this.chkDelExistingTravel == true) {
                              this.billingS.deleteFromRoster(dataPassDates).subscribe(dataDeleteFromRoster => {
                              });
                            }
                            // this.buildTravelPayList(dataTravelPayList); //'With r_TR
                            let index = 0;
                            if (!dataTravelPayList) {
                              while (index < dataTravelPayList.length) {
                                index++;
                              }
                            } else {
                              this.b_FirstRecord = true;
                              this.processUpdatePerc = ((index * 100) / dataTravelPayList.length);
                              this.panUpdatePercText = 'Calculating Travel Distances';
                              this.s_StartTimer = new Date();

                              //============================================================
                              //'START POPULATE TRAVELCALC TABLE
                              //============================================================

                              //'DO UNTIL PROCESSSED ALL ROSTER FOR PAY PERIOD SELECTION
                              while (index < dataTravelPayList.length) {
                                this.panUpdatePercText = dataTravelPayList[index].date + ':' + this.s_Date + ':' + dataTravelPayList[index].staff + ':' + this.s_Staff;
                                if (dataTravelPayList[index].date != this.s_Date) {
                                  this.b_ChangeOfDate = true;
                                } else {
                                  this.b_ChangeOfDate = false;
                                }
                                if (dataTravelPayList[index].staff != this.s_Staff) {
                                  this.b_ChangeOfStaff = true;
                                } else {
                                  this.b_ChangeOfStaff = false;
                                }
                                this.s_Staff = dataTravelPayList[index].staff;
                                this.s_Date = dataTravelPayList[index].date;
                                this.panUpdatePercText = this.s_Staff + ':' + this.s_Date + ':' + dataTravelPayList[index].client;

                                //'IF DATE OR STAFF CODE HAS CHANGED THEN CURRENT SHIFT IS FIRST SHIFT OF THE DAY
                                if (this.b_ChangeOfDate == true || this.b_ChangeOfStaff == true) {
                                  this.panUpdatePercText = 'Date/Staff Changed' + this.s_Staff + ':' + this.s_Date + ':' + dataTravelPayList[index].client;

                                  //'IF PAYING FROM LAST SHIFT TO BASE ADD A TRAVEL RECORD FOR LAST SHIFT OF THE PREVIOUS DAY TO BASE
                                  if (this.chkFinishBranch == true && this.b_FirstRecord == false) {
                                    this.s_EndAddress = this.s_BaseAddress;
                                    this.s_StartClient = "BASE";
                                    this.s_StartRecord = 0;
                                    this.s_StartService = "N/A";
                                    if (this.s_StartAddress == '') {
                                      this.s_Log = this.s_Log + 'Missing (FROM) Client address ' + this.s_StartClient + '<br>';
                                    }
                                    if (this.s_EndAddress == '') {
                                      this.s_Log = this.s_Log + 'Missing (TO) Client Address ' + this.s_EndClient + '<br>';
                                      this.panUpdatePercText = this.s_Log;
                                    }
                                    if (this.s_StartAddress != '' || this.s_EndAddress != '') {
                                      this.d_Meters = 0;
                                      this.d_Seconds = 0;
                                      this.panUpdatePercText = 'Getting Travel Detail';
                                      this.panUpdatePercText = 'Querying Google Maps API';
                                      // this.mapS.getDistanceMatrix({
                                      //   OriginDestination: this.s_StartAddress,
                                      //   FinalDestination: this.s_EndAddress,
                                      //   ResultType: 'json',
                                      //   GoogleCustID: this.googleCustID,
                                      //   TravelProvider: this.travelProvider,
                                      //   MapApiKey: this.providerKey
                                      // }).subscribe(dataDistanceMatrix => {
                                      //   this.d_Status = dataDistanceMatrix.rows[0].elements[0].status;
                                      //   this.d_Meters = dataDistanceMatrix.rows[0].elements[0].distance.text;
                                      //   this.d_Seconds = dataDistanceMatrix.rows[0].elements[0].duration.text;
                                      this.billingS.getTravelDistance({
                                        Origin: this.s_StartAddress,
                                        Destination: this.s_EndAddress,
                                        GoogleCustID: this.googleCustID,
                                        TravelProvider: this.travelProvider,
                                        MapApiKey: this.providerKey
                                      }).pipe(takeUntil(this.unsubscribe)).subscribe(dataDistanceMatrix => {
                                        this.d_Meters = (dataDistanceMatrix[0].apiDistance / 1000).toFixed(0);
                                        this.d_Seconds = ((dataDistanceMatrix[0].apiDuration / 60 / 60).toFixed(0)) + ":" + (((dataDistanceMatrix[0].apiDuration / 60) % 60).toFixed(0));
                                        this.panUpdatePercText = 'Google Maps API Query Successful';
                                        this.panUpdatePercText = 'Creating Travel Audit Record';
                                        // this.billingS.insertTravelCalc({
                                        //   Batchno: this.batchNumber,
                                        //   Staff: this.s_Staff,
                                        //   Date: this.s_Date,
                                        //   StartClientEndTime: this.s_StartClientEndTime,
                                        //   TimeDuration: this.timeDuration,
                                        //   StartClient: this.s_StartClient,
                                        //   StartAddress: this.s_StartAddress,
                                        //   StartRecord: this.s_StartRecord,
                                        //   StartService: this.s_StartService,
                                        //   EndClient: this.s_EndClient,
                                        //   EndAddress: this.s_EndAddress,
                                        //   EndRecord: this.s_EndRecord,
                                        //   EndService: this.s_EndService,
                                        //   DisMeters: this.d_Meters,
                                        //   DisSeconds: this.d_Seconds,
                                        //   VehicleType: dataTravelPayList[index].vehicleType
                                        // }).pipe(takeUntil(this.unsubscribe)).subscribe(dataInsertTravelCalc => {
                                        //   if (dataInsertTravelCalc) {
                                        //   }
                                        // });
                                        this.billingS.insertTravelCalc({
                                          Origin: this.s_StartAddress, //Get Travel Distance
                                          Destination: this.s_EndAddress,
                                          GoogleCustID: this.googleCustID,
                                          TravelProvider: this.travelProvider,
                                          MapApiKey: this.providerKey, //End Travel Distance
                                          Batchno: this.batchNumber, //Insert TravelCalc
                                          Staff: this.s_Staff,
                                          Date: this.s_Date,
                                          StartClientEndTime: this.s_StartClientEndTime,
                                          TimeDuration: this.timeDuration,
                                          StartClient: this.s_StartClient,
                                          StartAddress: this.s_StartAddress,
                                          StartRecord: this.s_StartRecord,
                                          StartService: this.s_StartService,
                                          EndClient: this.s_EndClient,
                                          EndAddress: this.s_EndAddress,
                                          EndRecord: this.s_EndRecord,
                                          EndService: this.s_EndService,
                                          DisMeters: this.d_Meters,
                                          DisSeconds: this.d_Seconds,
                                          VehicleType: this.s_VehicleType //End Insert TravelCalc
                                        }).pipe(takeUntil(this.unsubscribe)).subscribe(dataInsertTravelCalc => {
                                          this.panUpdatePercText = 'Script Executed'
                                        });
                                        // if (this.d_Status != 'OK') {
                                        //   this.panUpdatePercText = 'Google Maps API Query Unsuccessful';
                                        //   this.s_Log = this.s_Log + this.s_StartAddress + ' to ' + this.s_EndAddress + '<br>';
                                        // }
                                      });
                                    } else {
                                      this.globalS.iToast('Information', 'Either ' + this.s_StartClient + ' OR ' + this.s_EndClient + ' has an invalid google address. Please correct and rerun the travel update.')
                                    }
                                  } //'END IF PAYING TRAVEL FROM LAST SHIFT TO BASE (BRANCH)

                                  //'IF PAYING FROM BASE TO FIRST SHIFT OF THE DAY ADD A START ADDRESS TO PRIME FIRST SHIFT POF THE DAY
                                  if (this.chkStartBranch == true) {
                                    this.s_StartAddress = this.s_BaseAddress;
                                    this.s_StartClient = 'BASE';
                                    this.s_StartService = 'N/A';
                                    this.s_StartRecord = 0;
                                    this.s_EndAddress = dataTravelPayList[index].address;
                                    this.s_EndClient = dataTravelPayList[index].client;
                                    this.s_EndService = dataTravelPayList[index].activity;
                                    this.s_EndRecord = dataTravelPayList[index].recordID;
                                    this.s_Mask = "11"
                                  } else {
                                    //'IF FIRST SHIFT OF THE DAY IS USING A FLEET VEHICLE THEN USE FLEET VEHICLE DEFAULT ADDRESS AS START ADDRESS
                                    if (dataTravelPayList[index].vehicleType == 'FLEET VEHICLE') {
                                      this.s_StartAddress = dataTravelPayList[index].vehicleAddress;
                                    } else {
                                      this.s_StartAddress = dataTravelPayList[index].takeTo;
                                      if (this.s_StartAddress == '') {
                                        this.s_StartAddress = dataTravelPayList[index].address;
                                      }
                                    }
                                    this.s_StartRecord = dataTravelPayList[index].recordID;
                                    this.s_StartClient = dataTravelPayList[index].client;
                                    this.s_StartClientStartTime = dataTravelPayList[index].startTime;
                                    if (dataTravelPayList[index].vehicleType == 'FLEET VEHICLE') {
                                      this.s_StartClientEndTime = dataTravelPayList[index].startTime;
                                    } else {
                                      this.s_StartClientEndTime = dataTravelPayList[index].endTime;
                                    }
                                    this.s_StartService = dataTravelPayList[index].activity;
                                    this.s_Mask = '10';
                                  }
                                  this.b_FirstRecord = false;
                                } else {
                                  //'IF DATE OR STAFF CODE HAS NOT CHANGED
                                  switch (this.s_Mask) {
                                    case '10': //'WE HAVE START ADDRESS BUT NO END ADDRESS
                                      this.s_EndClientStartTime = dataTravelPayList[index].startTime;
                                      this.s_EndClientEndTime = dataTravelPayList[index].endTime;
                                      if (dataTravelPayList[index].vehicleType == 'FLEET VEHICLE') {
                                        this.s_EndAddress = dataTravelPayList[index].vehicleAddress;
                                      } else {
                                        // this.s_EndAddress = dataTravelPayList[index].pinkUpFrom == '' === true ? dataTravelPayList[index].pinkUpFrom : dataTravelPayList[index].address;
                                        // this.s_TakeToAddress = dataTravelPayList[index].takeTo == '' === true ? dataTravelPayList[index].takeTo : '';
                                        this.s_CalculatedEndAddress = dataTravelPayList[index].address;
                                        if (dataTravelPayList[index].pickUpFrom != '') {
                                          this.s_AlternatePickupAddress = dataTravelPayList[index].pickUpFrom;
                                          this.s_EndAddress = this.s_AlternatePickupAddress;
                                        } else {
                                          this.s_AlternatePickupAddress = '';
                                          this.s_EndAddress = dataTravelPayList[index].address;
                                        }
                                        if (dataTravelPayList[index].takeTo != '') {
                                          this.s_TakeToAddress = dataTravelPayList[index].takeTo;
                                        } else {
                                          this.s_TakeToAddress = '';
                                        }
                                      }
                                      this.s_EndClient = dataTravelPayList[index].client;
                                      this.s_EndService = dataTravelPayList[index].activity;
                                      this.s_EndRecord = dataTravelPayList[index].recordID;
                                      this.s_Mask = '11';
                                      break;
                                    case '11': //'WE HAVE START AND END ADDRESS
                                      this.s_StartClientStartTime = this.s_EndClientStartTime;
                                      this.s_StartClientEndTime = this.s_EndClientEndTime;
                                      // if (this.s_EndAddress != '' || this.s_TakeToAddress != '') {
                                      //   this.s_StartAddress = this.s_TakeToAddress == '' === true ? this.s_TakeToAddress : this.s_EndAddress;
                                      // }
                                      this.s_StartClient = this.s_EndClient;
                                      this.s_StartService = this.s_EndService;
                                      this.s_StartRecord = this.s_EndRecord;
                                      this.s_EndClientStartTime = dataTravelPayList[index].startTime;
                                      this.s_EndClientEndTime = dataTravelPayList[index].endTime;
                                      // if (dataTravelPayList[index].vehicleType == 'FLEET VEHICLE') {
                                      //   this.s_EndAddress = dataTravelPayList[index].vehicleAddress;
                                      // } else {
                                      //   this.s_EndAddress = dataTravelPayList[index].pinkUpFrom == '' === true ? dataTravelPayList[index].pinkUpFrom : dataTravelPayList[index].address;
                                      //   this.s_TakeToAddress = dataTravelPayList[index].takeTo == '' === true ? dataTravelPayList[index].takeTo : '';
                                      // }
                                      this.s_EndClient = dataTravelPayList[index].client;
                                      this.s_EndService = dataTravelPayList[index].activity;
                                      this.s_EndRecord = dataTravelPayList[index].recordID;
                                      if (this.s_EndAddress != '' || this.s_TakeToAddress != '') {
                                        if (this.s_TakeToAddress != '') {
                                          this.s_StartAddress = this.s_TakeToAddress;
                                        } else {
                                          if (this.s_AlternatePickupAddress == '') {
                                            this.s_StartAddress = this.s_EndAddress;
                                          } else {
                                            this.s_StartAddress = this.s_CalculatedEndAddress;
                                          }
                                        }
                                      }
                                      if (dataTravelPayList[index].vehicleType == 'FLEET VEHICLE') {
                                        this.s_EndAddress = dataTravelPayList[index].vehicleAddress;
                                      } else {
                                        this.s_CalculatedEndAddress = dataTravelPayList[index].address;
                                        if (dataTravelPayList[index].pickUpFrom != '') {
                                          this.s_AlternatePickupAddress = dataTravelPayList[index].pickUpFrom;
                                          this.s_EndAddress = this.s_AlternatePickupAddress;
                                        } else {
                                          this.s_AlternatePickupAddress = '';
                                          this.s_EndAddress = dataTravelPayList[index].address;
                                        }
                                        if (dataTravelPayList[index].takeTo != '') {
                                          this.s_TakeToAddress = dataTravelPayList[index].takeTo;
                                        } else {
                                          this.s_TakeToAddress = '';
                                        }
                                      }
                                      break; //' END PROCESS START AND END ADDRESSES
                                    default:
                                  }

                                  if (this.s_StartAddress == '' && this.s_StartClient != '!INTERNAL') {
                                    this.s_Log = this.s_Log + 'Missing (FROM) Client address ' + this.s_StartClient + '<br>';
                                    this.panUpdatePercText = this.s_Log;
                                  }

                                  if (this.s_EndAddress == '' && this.s_EndClient != ':!INTERNAL') {
                                    this.s_Log = this.s_Log + 'Missing (TO) Client Address ' + this.s_EndClient + '<br>';
                                    this.panUpdatePercText = this.s_Log;
                                  }

                                  //'IF WE HAVE A VALID START AND END ADDRESS THEN ADD TO TRAVELCALC TABLE
                                  this.d_Meters = 0;
                                  this.d_Seconds = 0;
                                  this.s_VehicleType = dataTravelPayList[index].vehicleType;
                                  this.s_Client = dataTravelPayList[index].client;
                                  this.panUpdatePercText = '"Getting Travel Detail: ' + this.s_Staff + ':' + this.s_Date + ':' + this.s_Client;
                                  if (this.s_StartAddress != '' || this.s_EndAddress != '') {//'CHECK FOR OVERLAPPING PLACEMARKES AND REMOVE OVERLAP TIME
                                    if (this.s_StartClientEndTime > this.s_EndClientStartTime) {
                                      this.s_StartClientEndTime = this.s_EndClientStartTime
                                    }
                                    this.panUpdatePercText = 'Creating Travel Audit'
                                    let xs_EndClientStartTime = '2022-01-01 ' + this.s_EndClientStartTime;
                                    let xs_StartClientEndTime = '2022-01-01 ' + this.s_StartClientEndTime;
                                    let xxs_EndClientStartTime = new Date(xs_EndClientStartTime);
                                    let xxs_StartClientEndTime = new Date(xs_StartClientEndTime);
                                    // this.timeDuration = xxs_StartClientEndTime.getTime() - xxs_EndClientStartTime.getTime();
                                    this.timeDuration = xxs_EndClientStartTime.getTime() - xxs_StartClientEndTime.getTime();
                                    this.timeDuration = Math.floor(this.timeDuration / 1000 / 60);
                                    this.panUpdatePercText = 'Executing Script for: ' + this.s_Staff + ':' + this.s_Date + ':' + this.s_Client;
                                    this.billingS.insertTravelCalc({
                                      Origin: this.s_StartAddress, //Get Travel Distance
                                      Destination: this.s_EndAddress,
                                      GoogleCustID: this.googleCustID,
                                      TravelProvider: this.travelProvider,
                                      MapApiKey: this.providerKey, //End Travel Distance
                                      Batchno: this.batchNumber, //Insert TravelCalc
                                      Staff: this.s_Staff,
                                      Date: this.s_Date,
                                      StartClientEndTime: this.s_StartClientEndTime,
                                      TimeDuration: this.timeDuration,
                                      StartClient: this.s_StartClient,
                                      StartAddress: this.s_StartAddress,
                                      StartRecord: this.s_StartRecord,
                                      StartService: this.s_StartService,
                                      EndClient: this.s_EndClient,
                                      EndAddress: this.s_EndAddress,
                                      EndRecord: this.s_EndRecord,
                                      EndService: this.s_EndService,
                                      DisMeters: this.d_Meters,
                                      DisSeconds: this.d_Seconds,
                                      VehicleType: this.s_VehicleType //End Insert TravelCalc
                                    }).pipe(takeUntil(this.unsubscribe)).subscribe(dataInsertTravelCalc => {
                                      this.panUpdatePercText = 'Script Executed'
                                    });
                                  }
                                }
                                index++;
                                this.panUpdatePercText = 'Finding Next Travel Record';
                                this.processUpdatePerc = ((index * 100) / dataTravelPayList.length);
                                this.panUpdatePercText = 'Found Next Travel Record';
                                this.i_Count = this.i_Count + 1;
                              }
                            }
                            this.s_EndTimer = new Date();
                            if (this.s_EndTimer != this.s_StartTimer) {
                              this.d_RequestDuration = (this.s_EndTimer - this.s_StartTimer) / this.i_Count;
                            } else {
                              this.d_RequestDuration = 0;
                            }
                            //'END POPULATE TRAVELCALC TABLE

                            //'START CREATE ROSTER ENTRIES FOR TRAVEL TIME AND KM
                            this.s_Duration = this.d_RequestDuration.toFixed(4) + ' Seconds For ' + dataTravelPayList.length + ' Records.';

                            if (this.chkSeperateClaim == true) {
                              if (this.chkAutoFillGap == true) {
                                this.billingS.travelToFitGapTrue(this.batchNumber).subscribe(dataTravelClaimList => {
                                  console.log(dataTravelClaimList);
                                });
                              } else {
                                this.billingS.travelToFitGapFalse(this.batchNumber).subscribe(dataTravelClaimList => {
                                  console.log(dataTravelClaimList);
                                });
                              }
                            } else {
                              this.billingS.travelSeparteClaimFalse(this.batchNumber).subscribe(dataTravelClaimList => {
                                console.log(dataTravelClaimList);
                              });
                            }

                            //'END CREATE ROSTER ENTRIES FOR TRAVEL TIME AND KM
                            this.panUpdatePercText = 'Travel Processing Completed.';
                            if (this.s_Log != '') {
                              this.confirmModal = this.modal.info({
                                nzTitle: 'Travel Processing Completed (with exceptions)',
                                nzContent: 'Batch : ' + this.batchNumber + '<br>' + '<br>' + 'MISSING OR INVALID START/DESTINATION ADDRESSES EXIST...' + '<br>' + 'SEE BELOW LIST ' + '<br>' + '<br>' + this.s_Log + '<br>' + 'Average Google Request Time = ' + this.d_RequestDuration.toFixed(4) + ' ' + '<br>' + this.s_Duration,
                                nzWidth: 500,
                                nzOnOk: () => {
                                  this.handleCancel();
                                  // this.panUpdatePercText = '';
                                  // this.processUpdatePerc = 0;
                                  // this.s_Log = '';
                                  // this.resetModal();
                                  // this.ngOnInit();
                                },
                              });
                            } else {
                              this.confirmModal = this.modal.info({
                                nzTitle: 'Travel Processing Completed',
                                nzContent: 'Batch : ' + this.batchNumber + '<br>' + 'Average Google Request Time = ' + this.d_RequestDuration.toFixed(4) + ' ' + '<br>' + this.s_Duration,
                                nzWidth: 500,
                                nzOnOk: () => {
                                  this.handleCancel();
                                  // this.panUpdatePercText = '';
                                  // this.processUpdatePerc = 0;
                                  // this.s_Log = '';
                                  // this.resetModal();
                                  // this.ngOnInit();
                                },
                              });
                            }
                          });
                        });
                      });
                    } else {
                      this.globalS.iToast('Information', 'There are no Travel Pay entries to process for the selected date range and program/s')
                    }
                    this.postLoading = false;
                    this.panUpdatePercText = '';
                    this.processUpdatePerc = 0;
                    this.s_Log = '';
                    this.resetModal();
                    this.ngOnInit();
                    return false;
                  };
                });
              }
            });
          });
        });
      };
    });
  };



  travelDistanceTest() {
    this.billingS.getTravelDistance({
      Origin: this.s_StartAddress,
      Destination: this.s_EndAddress,
      GoogleCustID: this.googleCustID,
      TravelProvider: this.travelProvider,
      MapApiKey: this.providerKey
    }).pipe(takeUntil(this.unsubscribe)).subscribe(dataDistanceMatrix => {
      this.d_Meters = (dataDistanceMatrix[0].apiDistance / 1000).toFixed(0);
      this.d_Seconds = ((dataDistanceMatrix[0].apiDuration / 60 / 60).toFixed(0)) + ":" + (((dataDistanceMatrix[0].apiDuration / 60) % 60).toFixed(0));
    });
  }


  testCase() {

    this.chkAutoCreateKM = this.inputForm.get('chkAutoCreateKM').value;
    this.cmbTravelProgram0 = this.inputForm.get('cmbTravelProgram0').value;
    this.cmbTravelActivity0 = this.inputForm.get('cmbTravelActivity0').value;
    this.cmbTravelPaytype0 = this.inputForm.get('cmbTravelPaytype0').value;

    this.chkAutoCreateTime = this.inputForm.get('chkAutoCreateTime').value;
    this.cmbTravelProgram1 = this.inputForm.get('cmbTravelProgram1').value;
    this.cmbTravelActivity1 = this.inputForm.get('cmbTravelActivity1').value;
    this.cmbTravelPaytype1 = this.inputForm.get('cmbTravelPaytype1').value;
    this.cmbTravelPaytype2 = this.inputForm.get('cmbTravelPaytype2').value;
    this.cmbTravelPaytype3 = this.inputForm.get('cmbTravelPaytype3').value;
    this.cmbTravelPaytype4 = this.inputForm.get('cmbTravelPaytype4').value;

    console.log('Travel KM' + this.chkAutoCreateKM)
    console.log(this.cmbTravelProgram0)
    console.log(this.cmbTravelActivity0)
    console.log(this.cmbTravelPaytype0)

    console.log('Travel Time' + this.chkAutoCreateTime)
    console.log(this.cmbTravelProgram1)
    console.log(this.cmbTravelActivity1)
    console.log(this.cmbTravelPaytype1)
    console.log(this.cmbTravelPaytype2)
    console.log(this.cmbTravelPaytype3)
    console.log(this.cmbTravelPaytype4)

    // this.s_StartAddress = ''
    // this.s_StartAddress = '33 Stewart Street, BROOME, WA, 6725'
    // this.s_EndAddress = '34 CROCODILE STREET, WAREEK, VIC, 3465'
    // this.googleCustID = 'KEY'
    // this.travelProvider = 'GOOGLE'
    // this.providerKey = 'AIzaSyAMIBJrZxsPVqBAxzoZJgmaxfYIoCpYGWc'

    // // this.billingS.GetMapDistanceTest().subscribe(data => { console.log(data) })

    // this.mapS.getDistanceMatrix({
    //   OriginDestination: this.s_StartAddress,
    //   FinalDestination: this.s_EndAddress,
    //   ResultType: 'json',
    //   GoogleCustID: this.googleCustID,
    //   TravelProvider: this.travelProvider,
    //   MapApiKey: this.providerKey
    // }).subscribe(dataDistanceMatrix => {
    //   this.d_Status = dataDistanceMatrix.rows[0].elements[0].status;
    //   this.d_Meters = dataDistanceMatrix.rows[0].elements[0].distance.text;
    //   this.d_Seconds = dataDistanceMatrix.rows[0].elements[0].duration.text;
    //   console.log(dataDistanceMatrix);
    //   console.log(this.d_Status);
    //   console.log(this.d_Meters);
    //   console.log(this.d_Seconds);
    // })

    // this.s_StartAddress = this.s_StartAddress.replace(/\s/g, '+');
    // this.s_EndAddress = this.s_EndAddress.replace(/\s/g, '+');
    // const address = {
    //   startAddress: this.s_StartAddress,
    //   endAddress: this.s_EndAddress,
    //   isHttpsEnabled: false
    // };
  };
}




//Delete
//console
//Debugger
