import * as ɵngcc0 from '@angular/core';
import * as ɵngcc1 from './spreadSheets.component';
import * as ɵngcc2 from './worksheet.component';
import * as ɵngcc3 from './column.component';
import * as ɵngcc4 from '@angular/common';
export declare class SpreadSheetsModule {
    static ɵfac: ɵngcc0.ɵɵFactoryDef<SpreadSheetsModule, never>;
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<SpreadSheetsModule, [typeof ɵngcc1.SpreadSheetsComponent, typeof ɵngcc2.WorksheetComponent, typeof ɵngcc3.ColumnComponent], [typeof ɵngcc4.CommonModule], [typeof ɵngcc1.SpreadSheetsComponent, typeof ɵngcc2.WorksheetComponent, typeof ɵngcc3.ColumnComponent]>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<SpreadSheetsModule>;
}

//# sourceMappingURL=spreadSheets.module.d.ts.map