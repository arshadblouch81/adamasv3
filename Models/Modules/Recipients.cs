using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class Recipient {
        public int? Id { get; set; }
        public string UniqueID { get; set; }
        public string AccountNo { get; set; }
        public string RecipName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string PreferredName { get; set; }
        public string Alias { get; set; }
        public string Type { get; set; }
        public string Branch { get; set; }
        public string Gender { get; set; }
        public string Mobility { get; set; }
        public string AgencyDefinedGroup { get; set; }
        public string PrimaryCoordinator { get; set; }
        public string File1 { get; set; }
        public string File2 { get; set; }
        public string DOB { get; set; }
        public string ContactIssues { get; set; }
        public string RosterIssues { get; set; }
        public string RunsheetIssues { get; set; }
        public string RecipientCoordinator { get; set; }
        public string NDISNumber { get; set; }
        public List<Contacts> contacts { get; set; }
        public List<Address> addresses{ get; set; }
        public List<ContactKinDetails> gpDetails { get; set; }
        public List<ContactKinDetails> otherContacts { get; set; }

        public Recipient Create()
        {
            return new Recipient
            {
                AccountNo = AccountNo,
                RecipName = RecipName,
                LastName = LastName,
                FirstName = FirstName,
                Id = Id ?? 0,
                PreferredName = PreferredName,
                Alias = Alias,
                Title = Title,
                MiddleName = MiddleName,
                Type = Type,
                Branch = Branch,
                Gender = Gender,
                Mobility = Mobility,
                AgencyDefinedGroup = AgencyDefinedGroup,
                PrimaryCoordinator = PrimaryCoordinator,
                File1 = File1,
                File2 = File2,
                DOB = DOB,
                ContactIssues = ContactIssues,
                RosterIssues = RosterIssues,
                RunsheetIssues = RunsheetIssues,
                contacts = contacts,
                addresses = addresses
    };
        }

        internal static Recipient Create(Recipient recipient)
        {
            return new Recipient
            {
                AccountNo = recipient.AccountNo,
                RecipName = recipient.RecipName,
                LastName = recipient.LastName,
                FirstName = recipient.FirstName,
                Id = recipient.Id,
                PreferredName = recipient.PreferredName,
                Alias = recipient.Alias,
                Title = recipient.Title,
                MiddleName = recipient.MiddleName,
                Type = recipient.Type,
                Branch = recipient.Branch,
                Gender = recipient.Gender,
                Mobility = recipient.Mobility,
                AgencyDefinedGroup = recipient.AgencyDefinedGroup,
                PrimaryCoordinator = recipient.PrimaryCoordinator,
                File1 = recipient.File1,
                File2 = recipient.File2,
                DOB = recipient.DOB,
                ContactIssues = recipient.ContactIssues,
                RosterIssues = recipient.RosterIssues,
                RunsheetIssues = recipient.RunsheetIssues,
                contacts = recipient.contacts,
                addresses = recipient.addresses
            };
        }
    }
}