﻿using Adamas.Models;
using AdamasV3.Models.Dto;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperController : ControllerBase
    {
        private IConfiguration CONFIG;
        private IHostingEnvironment ENV;
        private readonly DatabaseContext context;
        public SuperController(
                IConfiguration config,
                IHostingEnvironment env,
                DatabaseContext context
            ) {
            ENV = env;
            CONFIG = config;
            this.context = context;
        }

        [HttpPost("select")]
        public IEnumerable<dynamic> GetQuery([FromBody] QueryDto query)
        {
            using (var conn = context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(query.RawQuery, (SqlConnection)conn))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        var names = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
                        foreach (IDataRecord record in reader as IEnumerable)
                        {
                            var expando = new ExpandoObject() as IDictionary<string, object>;
                            foreach (var name in names)
                                expando[name] = record[name];

                            yield return expando;
                        }
                    }                    
                }
                
            }
        }
    }
}
