using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class Qte_Lne
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int RecordNumber { get; set; }

    public int? Doc_Hdr_Id { get; set; }

    public int? ItemId { get; set; }

    public double? Qty { get; set; }

    [MaxLength(10)]
    [Column("Bill Unit")]
    public string BillUnit { get; set; }

    [Column("Unit Bill Rate",TypeName = "money")]
    public decimal? UnitBillRate { get; set; }

    [Column("Line Bill Ex Tax",TypeName = "money")]
    public decimal? LineBillExTax { get; set; }

    [Column(TypeName = "money")]
    public decimal? Tax { get; set; }

    [Column("Line Bill Inc Tax",TypeName = "money")]
    public decimal? LineBillIncTax { get; set; }

    public int? COID { get; set; }

    public int? BRID { get; set; }

    public int? DPID { get; set; }

    [MaxLength(150)]
    public string DisplayText { get; set; }

    [MaxLength(255)]
    public string Notes { get; set; }

    [Column("Line#",TypeName = "int")]
    public int? LineNo { get; set; }

    [MaxLength(15)]
    public string Frequency { get; set; }

    [Column(TypeName = "numeric(16, 2)")]
    public decimal? QuoteQty { get; set; }

    [MaxLength(2)]
    public string PriceType { get; set; }

    [MaxLength(6)]
    public string QuotePerc { get; set; }

    [MaxLength(6)]
    public string BudgetPerc { get; set; }

    [MaxLength(10)]
    public string Day { get; set; }

    [MaxLength(5)]
    public string DTime { get; set; }

    [MaxLength(10)]
    public string Week { get; set; }

    [MaxLength(15)]
    public string RCycle { get; set; }

    public int? LengthInWeeks { get; set; }

    [MaxLength(4)]
    public string BudgetEnforcement { get; set; }

    [MaxLength(35)]
    public string BudgetGroup { get; set; }

    public string Roster { get; set; } // since this is nvarchar(max), no max length

    public int? RosterRecurrence { get; set; }

    public int? StrategyId { get; set; }

    public int SortOrder { get; set; }

    public bool? xDeletedRecord { get; set; }

    public DateTime? xEndDate { get; set; }
}
}