import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ViewEncapsulation, AfterViewInit } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,expectedOutcome,qoutePlantype, leaveTypes, UploadService,PrintService,DocusignService, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { dateFormat } from '@services/global.service'

import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { billunit, periodQuote, basePeriod } from '@services/global.service';

import { Filters, QuoteLineDTO, QuoteHeaderDTO } from '@modules/modules';

import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { RECIPIENT_OPTION } from '@modules/modules';
import { DomSanitizer } from '@angular/platform-browser';
import { NotesClient } from '@client/notes';

import { NzMessageService } from 'ng-zorro-antd/message';

import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';

@Component({
    styleUrls:['./quotes.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './quotes.html',
    styles:[`
    nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
        height: 25px !important;
        font-size: 13px !important;
    }

    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
        line-height: 24px;
        height: 25px;
    }
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
        background: #717e94;
        color: #fff;
    }
    nz-table th{
    font-weight: 600;
    font-family: 'Segoe UI';
    font-size: 14px;
    border: 1px solid #f3f3f3;
}
    .line{
        margin-top: 10px;
        padding-top: 10px;
        border-top: 1px solid #f3f3f3;
    }
    .controls div{
        display: inline-block;
        border-radius: 50%;
        background: #f1f1f1;
        border: 1px solid #cecece;
        width: 24px;
        text-align: center;
        margin-right:10px;
    }
    .controls div i{
        cursor:pointer;
    }
    .controls div:hover{
        background:#d6d6d6;
    }
    .right{
        float:right;
    }
    .right >  * {        
        margin-right:5px;
    }
    nz-range-picker{
        width:15rem;
    }
    .div-table{
        display: table;
        line-height: 25px;
    }
    .div-table > div{
        display: table-row;
    }
    .div-table > div > div{
        display: table-cell;
        padding: 2px 5px;
    }
    .form-group nz-select{
        width:100%
    }
    nz-select.select{
        min-width:10rem;
    }
    .inline{
        display:flex;
    }
    .inline > .title{
        font-size:12px;
        margin-right:5px;
    }
    .mini{
        width:5rem;
    }
    .spacing > *{
        margin-right:5px;
    }
    .calc-wrapper > div{
        margin-top:8px;
    }
    .three-wid{
        width:3rem;
    }
    .three-five{
        width:5rem;
    }
    .flex{
        display:flex;
    }
    .flex > *{
        margin-right:5px;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    } 
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    .exists{
        background-color: #eeb8b8;
        color: black;
    }
    ::ng-deep .ant-modal .ant-modal-header {
    background-color: #85B9D5 !important;
    
  }
    `]
})


export class RecipientQuotesAdmin implements OnInit, OnDestroy, AfterViewInit {

    private unsubscribe: Subject<void> = new Subject();

    option: string = 'add';

    quoteIdsForm: FormGroup;
    quoteForm: FormGroup;
    quoteListForm: FormGroup;

    billUnitArr: Array<string> = billunit;
    periodArr: Array<string> = periodQuote;
    basePeriodArr: Array<string> = basePeriod;

    clientId: number;

    size: string = 'small'
    from: any;
    quoteGeneralForm : FormGroup;
    title: string = 'Add New Quote';
    slots: any;
    weekly: string = 'Weekly';
    filterClicked:boolean;
    program: string;

    listOfData = [];
    viewTree:boolean;
    showRecipientList:boolean;
    cloneBudgetType:number;

    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
    // @ViewChild(ContextMenuComponent) public basicSubMenu: ContextMenuComponent;

    dateFormat: string = dateFormat;
    date: Date = new Date();
    nzSize: string = "small"
    user: any;

    inputForm: FormGroup;
    activeForm: FormGroup;
    inActiveForm: FormGroup;

    tableData: Array<any> = [];
    checked: boolean = false;
    isDisabled: boolean = false;

    displayLast: number = 20;
    archivedDocs: boolean = false;
    acceptedQuotes: boolean = false;

    loading: boolean = false;
    loading2: boolean = false;
    postLoading: boolean = false;
    quotesOpen: boolean = false;
    quoteLineOpen: boolean = false;
    activeOpen: boolean = false;
    inActiveOpen: boolean = false;
    admitOpen: boolean = false;
    newQuoteModal: boolean = false;
    atRate='@';
    goalAndStrategiesmodal : boolean = false;
    isUpdateGoal:boolean = false;
    isUpdateStrategy:boolean = false;  
    value: any;
    activeRowData:any;
    SelectedRecipeint:any
    selectedRowIndex:any;
    modalOpen:boolean;
    modalOpen2:boolean;
    quoteTemplateList: Array<string>;
    quoteProgramList: Array<string>;
    HighlightRow2:number;

    IS_CDC: boolean = false;
    tabs :any = 'CARE DOMAIN'
     LIMIT_TABS : Array<any> = ['CARE DOMAIN','CREATOR','DISCIPLINE','PROGRAM'];
     showFilters:boolean=true;
    codes: Array<any>
    recipientProperties: any;

    radioValue: any;
    filters: any;
    disciplineList: any;
    careDomainList: any;
    programList: any;
    quotePlanType: string[];
    carePlanID: any;
    goalOfCarelist: any;
    goalsAndStratergies: any;
    userCopy: any;

    quoteLines: Array<any> = [];

    quoteLinesTemp: Array<any> = [];
    
     
    token:any;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;  
    goalsAndStratergiesForm: FormGroup;
    reportDataParent:any;
    stratergiesList: any;
    pdfdata:any;
    stratergiesForm: FormGroup;
    strategiesmodal: boolean;
    expecteOutcome: string[];
    plangoalachivementlis: any;
    personIdForStrategy: any;
    supplements: FormGroup;
    isSuplement: boolean = false;
    disabledRadio:boolean = true;
    recipientOptionOpen: any;
    recipientOption: string;
    RECIPIENT_OPTION = RECIPIENT_OPTION;

    document_print_quote: any;
    selectedProgram:any;
    selectedServiceAgreement:any;
    isLoading:boolean;
    alist:Array<any> = [];
    alist_original:Array<any> = [];
    alist2:Array<any> = [];
    alist2_original:Array<any> = [];
    DocuSignModel:boolean=false;
    showUnset:boolean=false;
    doclist: Array<any> = [];
    selectedRow2:number;
    activeRowDocData:any;
    txtSearch:string;
    RecipientList:Array<any> = [];
    originalList:Array<any> = [];
    showDocRename:boolean=false;
    topBelow = { top: '40px' }
    loadingQuote:boolean=false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private printS:PrintService,
        private cd: ChangeDetectorRef,
        private message: NzMessageService,
        private clientS:ClientService,
        private docusign:DocusignService,
        private uploadS: UploadService,
        private nzContextMenuService: NzContextMenuService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal']);
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'quotes')) {
                this.search(data);
                this.userCopy = data;
                
            }
        });
    }

    ngAfterViewInit(){        
        // this.search(this.user);

        this.load_RecipientList();
       
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.buildForm();
        this.quoteGeneralForm.controls.discipline.setValue("NOT SPECIFIED");
        this.quoteGeneralForm.controls.careDomain.setValue("NOT SPECIFIED");

        
    }

    load_RecipientList(){

       this.listS.getrecipientList().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.RecipientList = data;
            this.originalList = data;
            this.cd.markForCheck();
        });
       

    }
    buildForm() {

        this.inputForm = this.formBuilder.group({
            autoLogout: [''],
            emailMessage: false,
            excludeShiftAlerts: false,
            excludeFromTravelinterpretation:false,
            inAppMessage: false,
            logDisplay: false,
            pin: [''],
            staffTimezoneOffset:[''],
            rosterPublish: false,
            shiftChange: false,
            smsMessage: false,
            title :'',
        });
        

        this.inActiveForm = this.formBuilder.group({
            timePeriod: []
        });

        this.activeForm = this.formBuilder.group({
            aUpdateActivities: true,
            aUpdateApplicable: true,
            aCreateBookings: true,
            aDeleteActivities: false,

            bUpdateActivities: true,
            bUpdateApplicable: true,
            bCreateBookings: true,
            bDeleteActivities: false,

            timePeriod: []
        });

        this.quoteGeneralForm = this.formBuilder.group({
            id:null,
            planType:null,
            name:null,
            program: 'NOT SPECIFIED',
            discipline: 'NOT SPECIFIED',
            careDomain: 'NOT SPECIFIED',
            starDate:null,
            signOfDate:null,
            reviewDate:null,
            rememberText:null,
            publishToApp:false
        });

        this.quoteForm = this.formBuilder.group({
            program: null,
            template: null,
            no: null,
            type: null,
            period: [],
            basePeriod: null,

            initialBudget: null,
            daysCalc: 365,
            govtContrib: null,

            programId: null

        });

        this.quoteIdsForm = this.formBuilder.group({
            itemId: null
        });

        this.goalsAndStratergiesForm = this.formBuilder.group({
            recordnumber:null,
            goal:'',
            PersonID:'45976',
            title : "Goal Of Care : ",
            ant   : null,
            lastReview : null,
            completed:null,
            percent : null,
            achievementIndex:'',
            notes:'',
            dateAchieved:null,
            lastReviewed:null,
            achievementDate:null,
            achievement:'',
        });

        this.stratergiesForm = this.formBuilder.group({
            detail:'',
            PersonID:this.personIdForStrategy,
            outcome:null,
            strategyId:'',
            serviceTypes:'',
            recordNumber:'',
        });

        this.quoteListForm = this.formBuilder.group({
            chargeType: null,
            code: null,
            displayText: null,
            strategy: null,
            sequenceNo: null,

            quantity: null,
            billUnit: null,
            period: null,
            weekNo: null,
            itemId: null,

            price: null,
            gst: null,
            min: null,
            quoteValue: null,
            quoteBudget: null,

            roster: null,
            rosterString: null,
            notes: null,

            editable: true
        });

        this.supplements = this.formBuilder.group({
            domentica:false,
            levelSupplement:'',
            oxygen:false,
            feedingSuplement:false,
            feedingSupplement:'',
            EACHD:false,
            viabilitySuplement:false,
            viabilitySupplement:'',
            financialSup:''
        });

        this.quoteListForm.get('chargeType').valueChanges
        .pipe(
            switchMap(x => {   
                console.log(x)             
                this.resetQuotePrimary();
                if(!x) return EMPTY;
                return this.listS.getchargetype({
                    program: this.quoteForm.get('program').value,
                    index: x
                  })
            })
        ).subscribe(data => {
            this.codes = data;
        });

        this.quoteListForm.get('code').valueChanges.pipe(
            switchMap(x => {
                if(!x)
                    return EMPTY;

                return this.listS.getprogramproperties(x)
            })
        ).subscribe(data => {
            this.recipientProperties = data;
            
            this.quoteListForm.patchValue({ displayText: data.billText, price: data.amount, roster: 'None',itemId: data.recnum })
        });

        this.quoteListForm.get('roster').valueChanges.subscribe(data => {
            this.weekly = data;
            this.setPeriod(data);
        });

        this.quoteForm.get('program').valueChanges
        .pipe(
            switchMap(x => {
                if(!x) return EMPTY;
                return this.listS.getprogramlevel(x)
            }),
            switchMap(x => {                
                this.IS_CDC = false;
                if(x.isCDC){
                    this.IS_CDC = true;
                    if(x.quantity && x.timeUnit == 'DAY'){
                        this.quoteForm.patchValue({
                            govtContrib: (x.quantity*365).toFixed(2),
                            programId: x.recordNumber
                        });
                    }
                    this.detectChanges();
                    return this.listS.getpensionandfee();
                }
                this.detectChanges();
                return EMPTY;
            })
        ).subscribe(data => {
            console.log(  + data)
            this.detectChanges();
        });

        this.supplements.get('levelSupplement').valueChanges.pipe(
            switchMap(x => {
                if(!x) return EMPTY;
                console.log("value "+ x);
                return this.listS.getprogramlevel(x)
            }),
            switchMap(x => {                
                this.IS_CDC = false;
                if(x.isCDC){
                    this.IS_CDC = true;
                    if(x.quantity && x.timeUnit == 'DAY'){
                        this.quoteForm.patchValue({
                            govtContrib: (x.quantity*365).toFixed(2),
                            programId: x.recordNumber
                        });
                    }
                    this.detectChanges();
                    return this.listS.getpensionandfee();
                }
                this.detectChanges();
                return EMPTY;
            })
           ).subscribe(data => {
            console.log(data)
            this.detectChanges();
        });

        // .pipe(
        //     switchMap(x => {
        //         if(!x) return EMPTY;
        //         return this.listS.getprogramlevel(x)
        //     }),
        //     switchMap(x => {                
        //         this.IS_CDC = false;
        //         if(x.isCDC){
        //             this.IS_CDC = true;
        //             if(x.quantity && x.timeUnit == 'DAY'){
        //                 this.quoteForm.patchValue({
        //                     govtContrib: (x.quantity*365).toFixed(2),
        //                     programId: x.recordNumber
        //                 });
        //             }
        //             this.detectChanges();
        //             return this.listS.getpensionandfee();
        //         }
        //         this.detectChanges();
        //         return EMPTY;
        //     })
        // ).subscribe(data => {
        //     console.log(data)
        //     this.detectChanges();
        // });

    }

    // buildForm() {
    //     this.inputForm = this.formBuilder.group({
    //         autoLogout: [''],
    //         emailMessage: false,
    //         excludeShiftAlerts: false,
    //         excludeFromTravelinterpretation:false,
    //         inAppMessage: false,
    //         logDisplay: false,
    //         pin: [''],
    //         staffTimezoneOffset:[''],
    //         rosterPublish: false,
    //         shiftChange: false,
    //         smsMessage: false
    //     });

    //     this.quoteGeneralForm = this.formBuilder.group({
    //         id:null,
    //         planType:null,
    //         name:null,
    //         careDomain:null,
    //         discipline:null,
    //         program:null,
    //         starDate:null,
    //         signOfDate:null,
    //         reviewDate:null,
    //         rememberText:null,
    //         publishToApp:false
    //     });

    //     this.quoteForm = this.formBuilder.group({
    //         program: null,
    //         template: null,

    //         no: null,
    //         type: null,
    //         period: [],
    //         basePeriod: null,


    //     });

    //     this.quoteListForm = this.formBuilder.group({
    //         chargeType: null,
    //         code: null,
    //         displayText: null,
    //         strategy: null,
    //         sequenceNo: null,

    //         quantity: null,
    //         billUnit: null,
    //         period: null,
    //         weekNo: null,

    //         price: null,
    //         gst: null,
    //         min: null,
    //         quoteValue: null,
    //         quoteBudget: null,

    //         roster: null,
    //         rosterString: null,
    //         notes: null,

    //         editable: true
    //     });

    //     this.quoteListForm.get('chargeType').valueChanges
    //     .pipe(
    //         switchMap(x => {                
    //             this.resetQuotePrimary();
    //             if(!x) return EMPTY;
    //             return this.listS.getchargetype({
    //                 program: this.quoteForm.get('program').value,
    //                 index: x
    //               })
    //         })
    //     ).subscribe(data => {
    //         this.codes = data;
    //     });

    //     this.quoteListForm.get('code').valueChanges.pipe(
    //         switchMap(x => {
    //             if(!x)
    //                 return EMPTY;

    //             return this.listS.getprogramproperties(x)
    //         })
    //     ).subscribe(data => {
    //         this.recipientProperties = data;
    //         this.quoteListForm.patchValue({ displayText: data.billText, price: data.amount, roster: 'None' })
    //     });

    //     this.quoteListForm.get('roster').valueChanges.subscribe(data => {
    //         this.weekly = data;
    //         this.setPeriod(data);
    //     });

    //     this.quoteForm.get('program').valueChanges
    //     .pipe(
    //         switchMap(x => this.listS.getprogramlevel(x)),
    //         switchMap(x => {                
    //             this.IS_CDC = false;
    //             if(x.isCDC){
    //                 this.IS_CDC = true;
    //                 this.detectChanges();
    //                 return this.listS.getpensionandfee();
    //             }
    //             this.detectChanges();
    //             return EMPTY;
    //         })
    //     ).subscribe(data => {
    //         console.log(data)
    //         this.detectChanges();
    //     });
    // }

    setPeriod(data: any){

        if(data && data != 'None'){

            this.quoteListForm.get('period').disable();

            this.quoteListForm.patchValue({
                period: data.toUpperCase(),
                billUnit: 'HOUR',
                weekNo: 52
            })

            return;
        }

        this.quoteListForm.patchValue({
            period: 'WEEKLY',
            billUnit: 'HOUR',    
            weekNo: 52
        })

        this.quoteListForm.get('period').enable();

        
        
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    slotsChange(data: any){
        this.quoteListForm.patchValue({
            quantity: data.quantity,
            rosterString: data.roster
        });
    }

    showDocument(item: any){

        var notifId = this.globalS.loadingMessage('Downloading Document');

        this.uploadS.downloadquotedocument(item.docID)
            .subscribe(blob => {

                let data = window.URL.createObjectURL(blob);      
                let link = document.createElement('a');
                link.href = data;
                link.download = item.filename;
                link.click();
    
                setTimeout(() =>
                {
                    // For Firefox it is necessary to delay revoking the ObjectURL
                    window.URL.revokeObjectURL(data);
                    this.message.remove(notifId);
    
                    this.globalS.sToast('Success','Download Complete');
                }, 100);
    
            }, err =>{
                this.message.remove(notifId);
                this.globalS.eToast('Error', "Document can't be found");
            })

    }
    contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, data:any): void {
        this.activeRowData = data;
        this.selectedRowIndex = this.tableData.indexOf(data);
        this.nzContextMenuService.create($event, menu);
      }
      showConfirm(): void {
        //var deleteRoster = new this.deleteRoster();
        this.showRecipientList=false;
        let Client = this.user.code;
        if (this.cloneBudgetType==2){
            Client=this.SelectedRecipeint.accountNo;
        }
        this.modalService.confirm({
        nzTitle: 'Confirm',
        nzContent: `You have selected to clone the currently highlighted quote to the Recipient ${Client} . Do you wish to proceed?`,
        nzOkText: 'Yes',
        nzCancelText: 'No',
        nzOnOk: () =>
        new Promise((resolve,reject) => {
            setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
            this.showDocRename=true;
            this.cd.detectChanges();
        
        }).catch(() => console.log('Oops errors!'))

        
        });
    }
    showRecipients(): void {
        this.showRecipientList=true;
    }

    cloneBudget(item: any, i:number){
        this.cloneBudgetType=i;
        if (i==1){
            this.showConfirm()
        }else if (i==2){
            this.showRecipients()
        }
     

    }
    postCloneBudget(){
        this.showDocRename=false;
        this.showRecipientList=false;
        if (this.cloneBudgetType==1){
            this.postCloneBudget_Same();
        }else if (this.cloneBudgetType==2){
            this.postCloneBudget_Diff(this.SelectedRecipeint.uniqueId);
        }
    }
    postCloneBudget_Same(){
        let doc :any ;
        doc = this.activeRowData;
        let title = this.inputForm.get('title').value;
        let fileName = this.user.id + title + + doc.program + doc.documentType;
        let input= {
            Doc_Id : doc.docID,
            DocNo: doc.carePlanId,
            PersonID : this.user.id,                
            Title : title,
            FileName : fileName,
            Author : this.tocken.user,
            Category: doc.program
        }
        this.timeS.cloneBudget(input).pipe(
            takeUntil(this.unsubscribe))
            .subscribe(data => {
                this.globalS.sToast('Success', 'Quote cloned successfully');
               this.search(this.user);
                this.cd.markForCheck();
            });
    }

    postCloneBudget_Diff(personId:string){
        let doc :any ;
        doc = this.activeRowData;
        let title = this.inputForm.get('title').value;
        let fileName = this.user.id + title + + doc.program + doc.documentType;

        let input= {
            Doc_Id : doc.docID,
            DocNo: doc.carePlanId,
            PersonID : personId,                
            Title : title,
            FileName : fileName,
            Author : this.tocken.user,
            Category: doc.program
        }
        this.timeS.cloneBudget(input).pipe(
            takeUntil(this.unsubscribe))
            .subscribe(data => {
                this.globalS.sToast('Success', 'Quote cloned successfully');
               this.search(this.user);
                this.cd.markForCheck();
            });
    }
    showAcceptModal(event: any, i:number) {

        let item = this.activeRowData;
        this.user = {...this.user, docId: item.docID };

        
        let data = {
            program: item.program,
            docId: item.docID,
            personId: this.user.id,
            recordNumber: item.recordNumber
        }
        
        this.listS.getprogramstatus(data)
            .subscribe(data => {
                if(['REFERRAL','INACTIVE'].includes(data)){

                    this.program = item.program;
                    this.from = { display: 'quote' };
        
                    this.recipientOption =  this.RECIPIENT_OPTION.ADMIT;
                    this.recipientOptionOpen = {};
                    this.detectChanges();
                    return;
                }
                if(['ACTIVE','ONHOLD'].includes(data)){
                    this.activeOpen = true;
                    this.detectChanges();
                    return;
                }
            });
    }
    OpenServiceAgreement(event:any,i:number){
        var doc = this.activeRowData;
        this.cd.detectChanges();
       // this.showDocument(doc);

       this.downloadFile(0);
    }
    downloadFile(index: any){
        var doc = this.activeRowData;
        var notifId = this.globalS.loadingMessage('Downloading Document');
        
        this.uploadS.download({
            PersonID: this.user.id,
            Extension: doc.documentType,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {

            let data = window.URL.createObjectURL(blob);      
            let link = document.createElement('a');
            link.href = data;
            link.download = doc.filename;
            link.click();

            setTimeout(() =>
            {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                this.message.remove(notifId);

                this.globalS.sToast('Success','Download Complete');
            }, 100);

        }, err =>{
            this.message.remove(notifId);
            this.globalS.eToast('Error', "Document can't be found");
        })
    }
    filterChange(data: any){
       
        this.displayLast = data.display;
        this.archivedDocs = data.archiveDocs;
        this.acceptedQuotes = data.acceptedQuotes;
        this.filters=data;
        this.search(this.user);
    }

    showQuoteModal(){
        this.quotesOpen = true;
        this.IS_CDC = false;
        this.tabFindIndex = 2; 
        this.loadingQuote = true;
        let id = this.user.id.substr(this.user.id - 5)

        this.populateDropdDowns();
        
        this.listS.getprogramcontingency(this.user.id).subscribe(data => this.quoteProgramList = data);
        
        this.listS.getglobaltemplate().subscribe(data => this.quoteTemplateList = data);
        
        this.listCarePlanAndGolas();
        this.quoteGeneralForm.controls.careDomain.setValue("NOT SPECIFIED");
        this.quoteGeneralForm.controls.discipline.setValue("NOT SPECIFIED");
        this.quoteForm.get('program').value ? this.quoteGeneralForm.controls.careDomain.setValue("NOT SPECIFIED") : this.quoteForm.get('program').value;
        this.quoteGeneralForm.patchValue({
            id: this.carePlanID.itemId,
            planType:"SUPPORT PLAN",
            name:this.quoteForm.get('template').value,
            starDate   :this.date,
            signOfDate :this.date,
            reviewDate :this.date,
            publishToApp:false
        });

        // console.log(this.user)
        this.listS.getrecipientsqlid(this.user.id).subscribe(data => {
            this.clientId = data;
            this.loadingQuote = false;
        })

        this.quoteForm.reset({
            program: null,
            template: null,
            no: null,
            type: null,
            period: [],
            basePeriod: null,
            programId: null
        });


        this.detectChanges();       
    }
    listCarePlanAndGolas(){
        this.loading = true;
        this.listS.getCareplangoals('45976').subscribe(data => {
            this.goalsAndStratergies = data;
            this.loading = false;
            this.cd.markForCheck();
        });
    }
    listStrtegies(personID:any){
        this.loading = true;
        this.listS.getStrategies(personID).subscribe(data => {
            this.stratergiesList = data;
            this.loading = false;
            this.cd.markForCheck();
        });
    }
    saveCarePlan(){

        if(!this.isUpdateGoal){
            this.timeS.postGoalsAndStratergies(this.goalsAndStratergiesForm.value).pipe(
                takeUntil(this.unsubscribe))
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data Inserted');
                    this.goalAndStrategiesmodal = false;
                    this.listCarePlanAndGolas();
                    this.cd.markForCheck();
                });
        }else{
            this.timeS.updateGoalsAndStratergies(this.goalsAndStratergiesForm.value).pipe(
                takeUntil(this.unsubscribe))
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data Inserted');
                    this.goalAndStrategiesmodal = false;
                    this.listCarePlanAndGolas();
                    this.isUpdateGoal = false;
                    this.cd.markForCheck();

            });
        }
        this.cd.markForCheck();
    }
    

    saveStrategy(){
        this.stratergiesForm.controls.PersonID.setValue(this.personIdForStrategy);
        if(!this.isUpdateStrategy){
            this.timeS.postplanStrategy(this.stratergiesForm.value).pipe(
                takeUntil(this.unsubscribe))
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data Inserted');
                    this.strategiesmodal = false;
                    this.listStrtegies(this.personIdForStrategy);
                    this.cd.markForCheck();
                });
        }else{
            this.timeS.updateplanStrategy(this.stratergiesForm.value).pipe(
                takeUntil(this.unsubscribe))
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data Inserted');
                    this.strategiesmodal = false;
                    this.listStrtegies(this.personIdForStrategy);
                    this.cd.markForCheck();
                });
        }
        this.cd.markForCheck();
    }

    selectData(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData = data;
        

    }
    showCarePlanStrategiesModal(){
        this.goalAndStrategiesmodal = true;
        this.personIdForStrategy = '';
        this.listS.getgoalofcare().subscribe(data => this.goalOfCarelist = data);
    }
    showStrategiesModal(){
        this.stratergiesForm.reset();
        this.isUpdateStrategy = false;
        this.strategiesmodal = true;
    }
    showEditCarePlanModal(data:any){
        this.goalAndStrategiesmodal = true;
        this.isUpdateGoal = true;
        this.listStrtegies(data.recordnumber);
        this.personIdForStrategy = data.recordnumber;
        this.goalsAndStratergiesForm.patchValue({
            title : "Goal Of Care : ",
            goal  : data.goal,
            achievementDate  : data.achievementDate,
            dateAchieved     : data.dateAchieved,
            lastReviewed     : data.lastReviewed,
            achievementIndex : data.achievementIndex,
            achievement      : data.achievement,
            notes            : data.notes,
            recordnumber     : data.recordnumber,
        })
    }
    showEditStrategyModal(data:any){
        this.isUpdateStrategy = true;
        this.strategiesmodal = true;
        this.stratergiesForm = this.formBuilder.group({
            detail:data.strategy,
            PersonID:data.recordnumber,
            outcome:data.achieved,
            strategyId:data.contractedId,
            serviceTypes:data.dsServices,
            recordNumber:data.recordnumber,
        });
    }
    quoteLineModal(){
        this.quoteLineOpen = true;
        this.quoteListForm.reset();
    }

    handleOk(){

    }

    handleCancel(){
        this.quotesOpen = false;
        this.inActiveOpen = false;
        this.activeOpen = false;
    }

    handleCancelLine(){
        this.quoteLineOpen = false;
    }

    tabFindIndex: number = 0;
    tabFindChange(index: number){
        this.tabFindIndex = index;
        if(this.tabFindIndex == 0){
            this.quoteGeneralForm.patchValue({
                name:this.quoteForm.get('template').value,
                program:this.globalS.isEmpty(this.quoteForm.get('program').value) ? "NOT SPECIFIED" : this.quoteForm.get('program').value,
                id: this.carePlanID.itemId,
                planType:"SUPPORT PLAN",
                careDomain:"NOT SPECIFIED",
                discipline:"NOT SPECIFIED",
                starDate   :this.date,
                signOfDate :this.date,
                reviewDate :this.date,
                publishToApp:false
            })
        }
    }
    tabFinderIndexbtn:number = 0;
    tabFindChangeStrategies(index: number){
        this.tabFinderIndexbtn = index;
    }

    printLoad: boolean = false;
    print(){            
       
        /*
            this.printLoad = true;
            this.listS.printquote(this.document_print_quote)
            .subscribe(blob => {
                var file = new Blob([blob], {type: "application/vnd.openxmlformats-officedocument.wordprocessingml.document"});
                console.log(file);
                this.printLoad = false;

                let data = window.URL.createObjectURL(file);
                let link = document.createElement('a');
                link.href = data;
                link.download = "quotes.docx";
                link.click();
        
                setTimeout(() => {
                  window.URL.revokeObjectURL(data);
                }, 100);
            });
            */
        // console.log(this.user)
        
        var fQuery =" SELECT TOP 20 DOC_ID, QuoteNumber, Title AS [Careplan Name], [PlanType], IsNull(CareDomain, 'VARIOUS') AS CareDomain, IsNull(Discipline, 'VARIOUS') AS Discipline, IsNull(Program, 'VARIOUS') AS Program, IsNull([Status], '') AS [St], FORMAT(convert(datetime,DocStartDate), 'MM/dd/yyyy')  AS [Start Date], FORMAT(convert(datetime,DocEndDate), 'MM/dd/yyyy') AS [End Date], [Created], [Modified], Filename  FROM (SELECT DOC_ID, CONVERT(Varchar(10),D.Created, 103) + ' ' + (SELECT [Name] FROM UserInfo WHERE Recnum = D.Author) AS Created, CONVERT(Varchar(10),D.Modified, 103) + ' ' + (SELECT [Name] FROM UserInfo WHERE Recnum = D.Typist) AS Modified, D.Doc# AS CareplanID, (SELECT Description FROM DataDomains WHERE RecordNumber = SubId) AS PlanType, (SELECT [Name] FROM HumanResourceTypes WHERE RecordNumber = D.Department AND [Group] = 'PROGRAMS') AS Program, (SELECT [Description] FROM DataDomains WHERE RecordNumber = D.DPID) AS Discipline, (SELECT [Description] FROM DataDomains WHERE RecordNumber = D.CareDomain) AS CareDomain, D.Title, Filename, D.Status, D.DocStartDate, D.DocEndDate, D.AlarmDate, D.AlarmText, QH.Doc# AS QuoteNumber FROM Documents D LEFT JOIN qte_hdr QH ON CPID = DOC_ID WHERE PersonID = '"+this.user.id+"' AND ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CP_QUOTE') AND (DeletedRecord = 0 OR DeletedRecord Is NULL)) D1  ORDER BY [DocStartDate] DESC "
        //var fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+personid+"' "
        var txtTitle = "Quote Listing";
        var Rptid = "CFLjCMEi8jMAAW47"
        var s_Extra = this.user.code;

        this.ReportRender(fQuery,txtTitle,Rptid,s_Extra);
        this.tryDoctype = "";        
        this.pdfTitle = "";

    }

    //Added by Mufeed 4 April 2024
    ReportRender(fQuery,txtTitle,Rptid,s_Extra){
        var lblcriteria;

        const data = {
                    
            "template": { "_id": Rptid },                                
            "options": {
                "reports": { "save": false },                
                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "txtTitle": txtTitle, 
                "Extra": s_Extra,                                     
            }
        }
        this.drawerVisible = true;         
            this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = txtTitle+".pdf"
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();
            }, err => {
                console.log(err);
                this.loading = false;
                this.ModalS.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            return;

    }


    //End Mufeed

    search(user: any = this.user) {
        this.loading = true;
        this.user = user;
        

        let data = {
            quote: {
                PersonID: user.id,               
                IncludeArchived: this.archivedDocs,
                IncludeAccepted: this.acceptedQuotes
            },
            filters: this.filters
        }

        this.document_print_quote = data;
      
        this.showFilters=false;
        this.listS.getlistquotes(data).subscribe(data => {
            this.tableData = data;
           // console.log(this.tableData)
            this.originalTableData = data;           
            this.loading = false;
            this.cd.markForCheck();
            if (!this.filterClicked){
                this.showFilters=false;
            }
        })
        
        this.timeS.getCarePlanID().subscribe(data => {this.carePlanID = data[0];this.cd.markForCheck();});
       // this.showFilters = false;
       if (this.user != null) {
        //let sql=`SELECT [Program] FROM RecipientPrograms rp INNER JOIN HumanResourceTypes pr ON rp.Program = pr.Name  WHERE  [PersonID] = '${this.user.id}' AND IsNull([User2], '') <> 'Contingency' AND rp.ProgramStatus <> 'INACTIVE' ORDER BY [Program]`;
        this.timeS.getfunding(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
       //this.listS.getprogramcontingency(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.alist = data;
            this.alist_original = data;
            if (this.alist.length <= 0)
                this.allPrpograms();
                
           
        });       
              
      }

    }
    allPrpograms(){
   
        this.timeS.getPrograms(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.alist = data;     
          this.alist_original = data;
        
      });
    }
    populateDropdDowns() {
        
        this.expecteOutcome = expectedOutcome;

        let notSpecified =["NOT SPECIFIED"];
        
        this.listS.getdiscipline().subscribe(data => {
            data.push('NOT SPECIFIED');
            this.disciplineList = data;
        });
        this.listS.getcaredomain().subscribe(data => {
            data.push('NOT SPECIFIED');
            this.careDomainList = data
        }); 
        this.listS.getndiaprograms().subscribe(data => {
            data.push('NOT SPECIFIED');
            this.programList = data;
        });
        this.listS.getcareplan().subscribe(data => {this.quotePlanType = data;})
        
        this.listS.getplangoalachivement().subscribe(data=> this.plangoalachivementlis = data)
        this.detectChanges();
    }
    patchData(data: any) {
        this.inputForm.patchValue({
            autoLogout: data.autoLogout,
            emailMessage: data.emailMessage,
            excludeShiftAlerts: data.excludeShiftAlerts,
            inAppMessage: data.inAppMessage,
            logDisplay: data.logDisplay,
            pin: data.pin,
            rosterPublish: data.rosterPublish,
            shiftChange: data.shiftChange,
            smsMessage: data.smsMessage
        });
    }    

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }
    

    onKeyPress(data: KeyboardEvent) {
        return this.globalS.acceptOnlyNumeric(data);
    }

    resetQuotePrimary(){
        this.quoteListForm.patchValue({
            code: null,
            displayText: null,
            roster: null,
            strategy: null
        });
    }

    save() {
        const group = this.inputForm;
        this.timeS.updatetimeandattendance({
            AutoLogout: group.get('autoLogout').value,
            EmailMessage: group.get('emailMessage').value,
            ExcludeShiftAlerts: group.get('excludeShiftAlerts').value,
            ExcludeFromTravelinterpretation: group.get('excludeFromTravelinterpretation').value,
            InAppMessage: group.get('inAppMessage').value,
            LogDisplay: group.get('logDisplay').value,
            Pin: group.get('pin').value,
            StaffTimezoneOffset:group.get('staffTimezoneOffset').value,
            RosterPublish: group.get('rosterPublish').value,
            ShiftChange: group.get('shiftChange').value,
            SmsMessage: group.get('smsMessage').value,
            Id: this.user.id
        }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success', 'Change successful');
                this.inputForm.markAsPristine();
                return;
            }
        });
    }

    canDeactivate() {
        if (this.inputForm && this.inputForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Save changes before exiting?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {

                }
            });
        }
        return true;
    }

    trackByFn(index, item) {
        return item.id;
    }

    showAddModal() {
        
    }

    hello(data: any){
        console.log(data);
        return false;
    }

    GENERATE_QUOTE_LINE(){
        
        setTimeout(() => {
            this.quoteLines = [...this.quoteLines, this.quoteListForm.getRawValue()];
            this.handleCancelLine();
            this.detectChanges();
        }, 0);
    }

    delBudget(data:any){
        this.timeS.deleteBudget(data.docID).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success', 'Data Deleted!');
                this.search(this.user);
                this.cd.markForCheck();
                return;
            }
        });
    }

    saveQuote(){
        let qteLineArr: Array<QuoteLineDTO> = [];
        let qteHeader: any ; // QuoteHeaderDTO;

        const quoteForm = this.quoteForm.getRawValue();
        console.log(quoteForm)

        console.log( this.quoteLines);
        // console.log(qteHeader);

        this.quoteLines.forEach(x => {
            let da: QuoteLineDTO = {
                sortOrder: 0,
                billUnit: x.billUnit,
                itemId: x.itemId,
                qty: x.quantity,
                displayText: x.displayText,

                unitBillRate: x.price,
                frequency: x.period,
                lengthInWeeks: x.weekNo,
                roster: x.rosterString
            };
            qteLineArr.push(da);
        });

        qteHeader = {
            programId: quoteForm.programId,
            clientId: this.clientId,
            quoteLines: qteLineArr,
            daysCalc: 365,
            budget: 51808.1,
            quoteBase: 'ANNUALLY',
            govtContribution: 51808.10,
            packageSupplements: '000000000000000000',
            agreedTopUp: '0.00',
            balanceAtQuote: '0.00',
            CLAssessedIncomeTestedFee: '0.00',

            feesAccepted: false,
            basePension: 'SINGLE',
            dailyBasicCareFee: '$0.00',
            dailyIncomeTestedFee: '$0.00',
            dailyAgreedTopUp: '$0.00',
            quoteView: 'ANNUALLY',

            personId: this.user.id,
           

        }
        // console.log(qteHeader)
       return;
        this.listS.getpostquote(qteHeader).subscribe(data => {
                console.log(data);
                this.globalS.sToast('Success','Quote Added');
            }, (error: any) => {
                console.log(error)
                this.globalS.eToast('Error', error.error.message)
            }) 
    }
    
    deleteCarePlanGoal(data: any){
    this.timeS.deleteCarePlangoals(data.recordnumber)
        .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success', 'Data Deleted!');
                this.listCarePlanAndGolas();
                this.cd.markForCheck();
            return;
         }
         this.cd.markForCheck();
      });
    }

    deleteCarePlanStrategy(data: any){
        this.timeS.deleteCarePlanStrategy(data.recordnumber)
        .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success', 'Data Deleted!');
                this.listStrtegies(this.personIdForStrategy);
                this.cd.markForCheck();
            return;
         }
         this.cd.markForCheck();
      });
    }

      handleOkTop(type:any) {
        this.generatePdf(type);
        this.tryDoctype = ""
        this.pdfTitle = ""
      }
      handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
      }
      generatePdf(type:any){
        this.drawerVisible = true;
        this.loading = true;
        if(type==1)
        var fQuery = "SELECT RecordNumber as recordnumber,CONVERT(varchar, [Date1],105) as Field1,CONVERT(varchar, [Date2],105) as Field2,CONVERT(varchar, [DateInstalled],105) as Field3,[State] as Field4, User1 AS Field5 FROM HumanResources WHERE PersonID = '45976' AND [Type] = 'CAREPLANGOALS' ORDER BY Name";
        else
        var fQuery = "SELECT RecordNumber, Notes AS Field1, Address1 AS Field2, [State] AS Field3, User1 AS Field4 FROM HumanResources WHERE [PersonID] = '45976' AND [GROUP] = 'PLANSTRATEGY'";
        
        
        const headerDict = {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        }
        
        const requestOptions = {
          headers: new HttpHeaders(headerDict)
        };
        
        if(type ==1 ){
             this.pdfdata = {
                "template": { "_id": "0RYYxAkMCftBE9jc" },
                "options": {
                  "reports": { "save": false },
                  "txtTitle": "Care Plan Goals List",
                  "sql": fQuery,
                  "userid":this.tocken.user,
                  "head1" : "Ant Complete",
                  "head2" : "Last Review",
                  "head3" : "Completed",
                  "head4" : "Percent",
                  "head5" : "Goal",
                }
              }
        }else{
            this.pdfdata = {
                "template": { "_id": "0RYYxAkMCftBE9jc" },
                "options": {
                  "reports": { "save": false },
                  "txtTitle": "Care Plan Strategies List",
                  "sql": fQuery,
                  "userid":this.tocken.user,
                  "head1" : "Strategy",
                  "head2" : "Achieved",
                  "head3" : "Contracted ID",
                  "head4" : "DS Services",
                }
    
              }
        }
        

        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(this.pdfdata).subscribe((blob: any) => {
                    this.pdfTitle = "Billing Integrity Check"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      
    handlegoalsStarCancel(){
        this.goalAndStrategiesmodal = false;
        this.isUpdateGoal = false;
        this.personIdForStrategy = '';
    }
    handleStarCancel(){
        this.strategiesmodal = false;
        this.isUpdateStrategy = false;
    }

    record: any;
    updateQuoteModal(data: any){
        this.option = 'update';
        this.record = data.recordNumber;
        this.globalS.loadingMessage('Loading Budget Details');
       // this.loadingQuote = true;
        this.newQuoteModal = !this.newQuoteModal;
        this.record  =  {    
            recordNumber: data.recordNumber,
            program: data.program,
            template: data.carePlan,
            title : data.carePlan,
            carePlanId: data.carePlanId,
            docNo: data.quoteNumber,
            templateFile : data.filename,
            originalLocation : data.originalLocation,
            maingroup : data.planType,
            docID : data.docID,
            packageSupplements : data.packageSupplements,
            startDate : data.startDate,
            endDate:data.endDate,
            created : data.created,
            modified : data.modified,            
            documentType : data.documentType,
            alarmText : data.alarmText,
            alarmDate : data.alarmDate,
            careDomain : data.careDomian,
            classification : data.classification,
            discipline : data.discipline,
            publishToApp : data.publishToApp,
            level   : data.level,
            isCDC   : data.isCDC,

            }   

    }

    refreshQuote(data: any){
        if(data){
            console.log('refresh')
            this.search();
        }
    }

   


    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Quote #',
        value: 'quoteNumber'
    },{
        key: 'Quote Type',
        value: 'planType'
    },{
        key: 'Name/Description',
        value: 'carePlan'
    },{
        key: 'St',
        value: 'st'
    },{
        key: 'Valid From',
        value: 'startDate'
    },{
        key: 'Valid To',
        value: 'endDate'
    },{
        key: 'Created',
        value: 'created'
    },{
        key: 'Modified',
        value: 'modified'
    },{
        key:'Related Careplan',
        value:'filename'
    }];
    
    
    

    dragDestination = [
        'Quote #',
        'Name/Description',
        'Quote Type',
        'St',
        'Valid From',
        'Valid To',
        'Created',
        'Modified',
        'Related Careplan'
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }

    openBudgetModal() {
        this.tabFindIndex = 0;
        this.txtSearch="";
        if (this.alist.length>1){
          this.modalOpen = true;
         
        } else  if (this.alist.length==1){
          this.selectedProgram=this.alist[0];
         
          this.openServiceAgreementModal();     
        
          this.loading2=true;
        
        }
        
        //let sql=`SELECT IDENT_CURRENT('Documents') AS [DocID],* FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY Title`;
    
        this.listS.gettemplatelistAll().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.alist2 = data;
            this.alist2_original = data;
            this.loading2=false;
            this.cd.detectChanges()
           
        });
     
        // this.uploadS.getFileTemplates().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     this.alist2 = data;
        //     this.loading2=false;
        //     this.cd.detectChanges()
           
        // });
        
    
      }

    selectProgram(data:any, i:number){
        this.selectedProgram=data;
        this.HighlightRow2=i;
      }
      AllocateProgram(data:any, i:number){
        
        this.selectedProgram=data;
        this.modalOpen=false;
        this.openServiceAgreementModal();
        
      //   recordNumber: data.recordNumber,
      //   program: data.program,
      //   no: data.docNo,
       // this.router.navigate(['admin/recipient/quotes'], { state: { data: data } });
      }
      selectService(data:any, i:number){
        this.selectedServiceAgreement=data;
        this.HighlightRow2=i;
      }
      AllocateServiceAgreeement(data:any, i:number){
        this.selectedServiceAgreement=data;;
        this.modalOpen2 = false;
        this.option = 'add';
        
      // Usage example
         if(!data.exists){
                this.globalS.eToast('Error', "Document can't be found");
                return;
           }
        

      this.newQuoteModal=!this.newQuoteModal;
      this.record  =  {    
                    recordNumber: data.recordNumber,
                    program: this.selectedProgram,
                    template: data.title,
                    docNo: 0,
                    templateFile : data.template,
                    maingroup : data.mainGroup,
                    docID : 0,
                    plan : 'CP_QUOTE',
                    level : data.level,
                    isCDC : data.isCDC
                   
                    }   
    // this.router.navigate(['admin/recipient/quotes'], { state: { data: data } });
    }
    openServiceAgreementModal() {
        this.tabFindIndex = 0;
        this.modalOpen2 = true;
        this.txtSearch="";
       /* let sql=`SELECT * FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY Title`;
        this.listS.getlist(sql).subscribe(data => {
            this.alist2 = data;
           
        });*/
        
      }
      
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(data: any): void {
      
       
        this.deleteCarePlanGoal(data);
      

  }
  ImportData(){

  }

  Refresh(){
    this.search();
  }

  Filters(){
    this.showFilters=!this.showFilters;
    this.filterClicked=true;
   // this.search();
  }

  
  signingStatus(){
    this.DocuSignModel=true;
    this.timeS.getdocusingStatus(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.doclist = data;
        this.cd.detectChanges();
    });
}
selectDocusign(data,i){
    this.selectedRow2=i;
    this.activeRowDocData=data;
}
showAllDocuments(){

}

sendDocusignEnvelope(index:any) {
    var doc = this.activeRowData;
    //var notifId = this.globalS.loadingMessage('Downloading Document');

    let filename = doc.filename;
    let docID = doc.docID;
    
    this.uploadS.download({
        PersonID: this.user.id,
        Extension: doc.documentType,
        FileName: doc.filename,
        DocPath: doc.originalLocation
    }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {
    })



        this.clientS.getcontacts(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(contacts => {

            let primaryContact = contacts.find((x: any) => x.primaryPhone == 1);
            if (primaryContact==null)
                primaryContact = contacts[0];

    const envelopeData = {
      emailSubject: 'Please sign this document',
      emailBlurb: 'Here is the document you need to sign.',
      status: 'sent',
      
        signers: [
          {
            email: primaryContact.detail,
            name: this.user.code,                  
            company : 'Adamas',
            title : 'MS',
            recipientId: '1',
            routingOrder: '1',
            tabs: {
              signHereTabs: [
                {
                  xPosition: '100',
                  yPosition: '150',
                  documentId: '1',
                  pageNumber: '1'
                }
              ]
            }
          }
        ]
      ,
      documents: [
        {
          documentId: docID,
          name: doc.carePlan,
          documentBase64: filename,
          fileExtension: doc.documentType,
          remoteUrl: doc.originalLocation 
        }
      ],
      startingView : 'tagging',
      DOC_ID: docID,
      ccRecipients: [ {
        "email": "arshad@adamas.net.au",
        "name": "Arshad",
        "recipientId": "2",
        "routingOrder": "2"
      }, {
        "email": "tw@adamas.net.au",
        "name": "Timwatts",
        "recipientId": "3",
        "routingOrder": "3"
      }]

    };
    
    this.docusign.sendDocument(envelopeData).pipe(takeUntil(this.unsubscribe)).subscribe(res => {
      console.log(res);    
    });

});


  }

  async convertToBase64(file: File) {
    let base64String :any;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
     base64String = reader.result?.toString().split(',')[1]; // Remove the prefix if needed
      return (base64String);
    };
    reader.onerror = (error) => {
      console.log('Error: ', error);
    };

   
  }
  
  RecordClicked(event: any, value: any, i: number) {

    this.SelectedRecipeint=value;
   
        this.HighlightRow2=i;
       
    
  }

 onItemDbClick(sel:any , i:number) : void {
     this.SelectedRecipeint=sel;
     this.showConfirm();
    
 
 }
 onTextChangeEvent(event:any, listTye:any){
        // console.log(this.txtSearch);
         let value = this.txtSearch.toUpperCase();
        if (listTye=='Recipient'){
         this.RecipientList=this.originalList.filter(element=>element.accountNo.includes(value));
    } else   if (listTye=='Program'){
         this.alist=this.alist_original.filter(element=>element.program.includes(value));

    }else   if (listTye=='Service'){
        this.alist2=this.alist2_original.filter(element=>element.title.includes(value));

   }


     }


}