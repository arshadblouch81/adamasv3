using System;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Text;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Configuration;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;

using System.Linq;
using AdamasV3.Models.Dto;
using Adamas.Models.Tables;
using Adamas.Models.DTO;
using System.IO;

namespace Adamas.DAL
{

    public class ListService : IListService
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private ILoginService login;

        public ListService(
            DatabaseContext context,
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            IGeneralSetting setting,
            ILoginService _login
            )
        {
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _conn = conn;
            GenSettings = setting;
            login = _login;
        }

        public async Task<int> PostCaseStaff(CaseStaffDto details)
        {
            var user = await login.GetCurrentUser();
            HumanResources hr = new HumanResources()
            {
                PersonID = details.PersonId,
                Group = "COORDINATOR",
                Type = "COORDINATOR",
                SubType = "COORDINATOR",
                Name = details.CaseManagerId,
                Address1 = details.Program,
                Creator = user.User,
                ReminderCreator = user.User,
                Notes = details.Notes,
                MobileAlert = false,
                PurchaseDate = DateTime.Now,
                ReminderProcessed = false,
                DeletedRecord = false

            };

            try
            {
                _context.HumanResources.Add(hr);
                await _context.SaveChangesAsync();
            } catch(Exception ex)
            {
                throw ex;
            }

            return (int)hr.RecordNumber;
        }

        public async Task<bool> DeleteCaseStaff(int recordNo)
        {
            var hr = _context.HumanResources.Where(x => x.RecordNumber == recordNo).FirstOrDefault();
            _context.HumanResources.Remove(hr);
            await _context.SaveChangesAsync();
            return true;
        }

        // This would remove record that has filename started with string TEMP
        public async Task<bool> DeleteDocumentTempFiles()
        {
            try
            {
                var tempFiles = await (from doc in _context.Documents where doc.FileName.StartsWith("TEMP") select doc).ToListAsync();

                foreach (var doc in tempFiles)
                {

                    var quoteHeader = await (from header in _context.Qte_Hdr where header.CPID == doc.Doc_Id select header).FirstOrDefaultAsync();

                    if (quoteHeader != null)
                    {
                        var quoteLines = await (from lines in _context.Qte_Lne where lines.Doc_Hdr_Id == quoteHeader.RecordNumber select lines).ToListAsync();
                        if (quoteLines != null)
                        {
                            _context.Qte_Lne.RemoveRange(quoteLines);
                            await _context.SaveChangesAsync();
                        }
                        _context.Qte_Hdr.Remove(quoteHeader);
                        await _context.SaveChangesAsync();
                    }

                }
                _context.Documents.RemoveRange(tempFiles);

            
                var quoteHeaderList = await (from header in _context.Qte_Hdr where header.DocId == null && header.ClientId == null && header.ProgramId == null select header).ToListAsync();
               // _context.Qte_Hdr.RemoveRange(quoteHeaderList);

              //  await _context.SaveChangesAsync();

                return true;
            } catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public async Task<String> CheckQuoteDocument(QuoteHeaderDTO quote)
        {
            var docFolderLoggedUser = await (from ui in _context.UserInfo where ui.Name == quote.User select ui.RecipientDocFolder).FirstOrDefaultAsync();
            var newFileName = string.Empty;

            if (string.IsNullOrEmpty(quote.NewFileName))
            {
                newFileName = $@"{quote.PersonId}-{quote.Program} {DateTime.Now.ToString("dd_M_yyyy")}.docx";
            }
            else
            {
                newFileName = quote.NewFileName;
            }

            // if file does not exist return null
            if (!File.Exists(Path.Combine(docFolderLoggedUser, newFileName)))
            {
                return null;
            }
            return newFileName;
        }


        public async Task<QuoteMergeFields> GetRecipientQuoteMergeFields(string personID)
        {
            var user = new SqlParameter("@PersonID", personID);

            var qmFields =  await _context.QuoteMergeFields.FromSqlRaw($@"SELECT DISTINCT R.uniqueid                                     AS [ID],
                R.agencyidreportingcode                        AS [UR Number],
                R.accountno                                    AS [Account#],
                R.title                                        AS [Title],
                R.[surname/organisation],
                R.firstname                                    AS [First Name],
                R.middlenames                                  AS [Other Names],
                R.preferredname                                AS
                [Preferred Name],
                Upper([surname/organisation]) + ', ' + CASE WHEN firstname <> ''
                THEN firstname
                ELSE ' ' END + CASE WHEN middlenames <> '' THEN ' ' +
                middlenames ELSE ' ' END
                                                               AS
                [FullNameSurnameFirst],
                CASE WHEN firstname <> '' THEN firstname ELSE ' ' END + CASE
                WHEN middlenames <>
                '' THEN ' ' + middlenames ELSE ' ' END + CASE WHEN
                [surname/organisation] <> ''
                THEN ' ' + [surname/organisation] ELSE ' ' END AS
                [FullNameSurnameLast],
                R.gender,
                R.dateofbirth                                  AS [D.O.B.],
                R.ndisnumber,
                CONVERT(VARCHAR (4000), R.[ohsprofile])        AS [OHSProfile],
                CONVERT(VARCHAR (4000), R.[notes])             AS [Notes],
                --'        SELECT DISTINCT
                --'                  R.UniqueID AS [ID],
                --'                  R.AgencyIdReportingCode AS [UR Number],
                --'                  R.AccountNo AS [Account#],
                --'                  R.Title AS [Title],
                --'                  R.[Surname/Organisation],
                --'                  R.FirstName AS [First Name],
                --'                  R.MiddleNames AS [Other Names],
                --'                  R.PreferredName AS [Preferred Name],
                --'                  UPPER([Surname/Organisation]) + ', ' + CASE WHEN FirstName <> '' THEN FirstName ELSE ' ' END + CASE WHEN MiddleNames <> '' Then ' ' + MiddleNames ELSE ' ' END AS [FullNameSurnameFirst],
                --'                  CASE WHEN FirstName <> '' THEN FirstName ELSE ' ' END + CASE WHEN MiddleNames <> '' Then ' ' + MiddleNames ELSE ' ' END + CASE WHEN [Surname/Organisation] <> '' THEN ' ' + [Surname/Organisation] ELSE ' ' END AS [FullNameSurnameLast],
                --'                  R.Gender,
                --'                  R.DateOfBirth AS [D.O.B.],
                --'                  CONVERT(varchar (4000), R.[OHSProfile]) AS [OHSProfile],
                --'                  CONVERT(varchar (4000), R.[Notes]) AS [Notes],
                R.type,
                R.branch,
                R.recipient_coordinator                        AS
                [Primary Coord],
                R.agencydefinedgroup                           AS [Category],
                R.onirating,
                R.admissiondate                                AS
                [Activation Date],
                R.dischargedate                                AS
                [DeActivation Date],
                CASE
                  WHEN N0.primarystreetaddress <> '' THEN
                  N0.primarystreetaddress
                  ELSE ' '
                END                                            AS
                PrimaryStreetAddress,
                CASE
                  WHEN N0.primarysuburb <> '' THEN N0.primarysuburb
                  ELSE ' '
                END                                            AS PrimarySuburb,
                CASE
                  WHEN N0.primarystate <> '' THEN N0.primarystate
                  ELSE ' '
                END                                            AS PrimaryState,
                CASE
                  WHEN N0.primarypostcode <> '' THEN N0.primarypostcode
                  ELSE ' '
                END                                            AS
                PrimaryPostcode,
                CASE
                  WHEN N1.usualstreetaddress <> '' THEN N1.usualstreetaddress
                  ELSE ' '
                END                                            AS
                UsualStreetAddress,
                CASE
                  WHEN N1.usualsuburb <> '' THEN N1.usualsuburb
                  ELSE ' '
                END                                            AS UsualSuburb,
                CASE
                  WHEN N1.usualstate <> '' THEN N1.usualstate
                  ELSE ' '
                END                                            AS UsualState,
                CASE
                  WHEN N1.usualpostcode <> '' THEN N1.usualpostcode
                  ELSE ' '
                END                                            AS UsualPostcode,
                CASE
                  WHEN N2.contactaddress <> '' THEN N2.contactaddress
                  ELSE ' '
                END                                            AS ContactAddress
                ,
                CASE
                  WHEN N2.contactsuburb <> '' THEN N2.contactsuburb
                  ELSE ' '
                END                                            AS ContactSuburb,
                CASE
                  WHEN N2.contactstate <> '' THEN N2.contactstate
                  ELSE ' '
                END                                            AS ContactState,
                CASE
                  WHEN N2.contactpostcode <> '' THEN N2.contactpostcode
                  ELSE ' '
                END                                            AS
                ContactPostcode,
                CASE
                  WHEN P0.primaryphone <> '' THEN P0.primaryphone
                  ELSE ' '
                END                                            AS PrimaryPhone,
                CASE
                  WHEN P1.homephone <> '' THEN P1.homephone
                  ELSE ' '
                END                                            AS HomePhone,
                CASE
                  WHEN P2.workphone <> '' THEN P2.workphone
                  ELSE ' '
                END                                            AS WorkPhone,
                CASE
                  WHEN P3.mobilephone <> '' THEN P3.mobilephone
                  ELSE ' '
                END                                            AS MobilePhone,
                CASE
                  WHEN P4.fax <> '' THEN P4.fax
                  ELSE ' '
                END                                            AS Fax,
                CASE
                  WHEN P5.email <> '' THEN P5.email
                  ELSE ' '
                END                                            AS Email
FROM   recipients R
       LEFT JOIN recipientprograms
              ON recipientprograms.personid = R.uniqueid
       LEFT JOIN humanresourcetypes
              ON humanresourcetypes.NAME = recipientprograms.program
       LEFT JOIN (SELECT TOP 1 personid,
                               suburb,
                               CASE WHEN address1 <> '' THEN address1 + ' ' ELSE
                               ' '
       END + CASE
                 WHEN address2
                               <> '' THEN address2 + ' ' ELSE ' ' END AS
              PrimaryStreetAddress,
                               CASE
                                 WHEN suburb <> '' THEN suburb + ' '
                                 ELSE ' '
                               END                                    AS
                               PrimarySuburb,
                               CASE
                                 WHEN LEFT(postcode, 1) = '0' THEN 'NT'
                                 WHEN LEFT(postcode, 1) = '2'
                                      AND LEFT(postcode, 2) = '26' THEN 'ACT'
                                 WHEN LEFT(postcode, 1) = '2'
                                      AND LEFT(postcode, 2) <> '26' THEN 'NSW'
                                 WHEN LEFT(postcode, 1) = '3' THEN 'VIC'
                                 WHEN LEFT(postcode, 1) = '4' THEN 'QLD'
                                 WHEN LEFT(postcode, 1) = '5' THEN 'SA'
                                 WHEN LEFT(postcode, 1) = '6' THEN 'WA'
                                 WHEN LEFT(postcode, 1) = '7' THEN 'TAS'
                               END                                    AS
                               PrimaryState,
                               CASE
                                 WHEN postcode <> '' THEN postcode
                                 ELSE ' '
                               END                                    AS
              PrimaryPostcode
                  FROM   namesandaddresses
                  WHERE  primaryaddress = 1
                         AND personid = @PersonID) AS N0
              ON N0.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               suburb,
                               CASE WHEN address1 <> '' THEN address1 + ' ' ELSE
                               ' '
                                                                  END + CASE
                                                                            WHEN
                                                                  address2
                               <> '' THEN address2 + ' ' ELSE ' ' END AS
                                                    UsualStreetAddress,
                               CASE
                                 WHEN suburb <> '' THEN suburb + ' '
                                 ELSE ' '
                               END                                    AS
                               UsualSuburb,
                               CASE
                                 WHEN LEFT(postcode, 1) = '0' THEN 'NT'
                                 WHEN LEFT(postcode, 1) = '2'
                                      AND LEFT(postcode, 2) = '26' THEN 'ACT'
                                 WHEN LEFT(postcode, 1) = '2'
                                      AND LEFT(postcode, 2) <> '26' THEN 'NSW'
                                 WHEN LEFT(postcode, 1) = '3' THEN 'VIC'
                                 WHEN LEFT(postcode, 1) = '4' THEN 'QLD'
                                 WHEN LEFT(postcode, 1) = '5' THEN 'SA'
                                 WHEN LEFT(postcode, 1) = '6' THEN 'WA'
                                 WHEN LEFT(postcode, 1) = '7' THEN 'TAS'
                               END                                    AS
                               UsualState,
                               CASE
                                 WHEN postcode <> '' THEN postcode
                                 ELSE ' '
                               END                                    AS
                               UsualPostcode
                  FROM   namesandaddresses
                  WHERE  description = '<USUAL>'
                         AND personid = @PersonID) AS N1
              ON N1.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               suburb,
                               CASE WHEN address1 <> '' THEN address1 + ' ' ELSE
                               ' '
                                                                  END + CASE
                                                                            WHEN
                                                                  address2
                               <> '' THEN address2 + ' ' ELSE ' ' END AS
                               ContactAddress
                                                                  ,
              CASE
                WHEN suburb <> '' THEN suburb + ' '
                ELSE ' '
              END                                    AS ContactSuburb,
              CASE
                WHEN suburb <> '' THEN suburb + ' '
                ELSE ' '
              END                                    AS UsualSuburb,
              CASE
                WHEN LEFT(postcode, 1) = '0' THEN 'NT'
                WHEN LEFT(postcode, 1) = '2'
                     AND LEFT(postcode, 2) = '26' THEN 'ACT'
                WHEN LEFT(postcode, 1) = '2'
                     AND LEFT(postcode, 2) <> '26' THEN 'NSW'
                WHEN LEFT(postcode, 1) = '3' THEN 'VIC'
                WHEN LEFT(postcode, 1) = '4' THEN 'QLD'
                WHEN LEFT(postcode, 1) = '5' THEN 'SA'
                WHEN LEFT(postcode, 1) = '6' THEN 'WA'
                WHEN LEFT(postcode, 1) = '7' THEN 'TAS'
              END                                    AS ContactState,
              CASE
                WHEN postcode <> '' THEN postcode
                ELSE ' '
              END                                    AS ContactPostcode
                  FROM   namesandaddresses
                  WHERE  description = '<CONTACT>'
                         AND personid = @PersonID) AS N2
              ON N1.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               CASE
                                 WHEN detail <> '' THEN detail
                                 ELSE ' '
                               END AS PrimaryPhone
                  FROM   phonefaxother
                  WHERE  primaryphone = 1
                         AND personid = @PersonID) AS P0
              ON P0.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               CASE
                                 WHEN detail <> '' THEN detail
                                 ELSE ' '
                               END AS HomePhone
                  FROM   phonefaxother
                  WHERE  [type] = '<HOME>'
                         AND personid = @PersonID) AS P1
              ON P1.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               CASE
                                 WHEN detail <> '' THEN detail
                                 ELSE ' '
                               END AS WorkPhone
                  FROM   phonefaxother
                  WHERE  [type] = '<WORK>'
                         AND personid = @PersonID) AS P2
              ON P1.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               CASE
                                 WHEN detail <> '' THEN detail
                                 ELSE ' '
                               END AS MobilePhone
                  FROM   phonefaxother
                  WHERE  [type] = '<MOBILE>'
                         AND personid = @PersonID) AS P3
              ON P1.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               CASE
                                 WHEN detail <> '' THEN detail
                                 ELSE ' '
                               END AS Fax
                  FROM   phonefaxother
                  WHERE  [type] = '<FAX>'
                         AND personid = @PersonID) AS P4
              ON P1.personid = R.uniqueid
       LEFT JOIN (SELECT TOP 1 personid,
                               CASE
                                 WHEN detail <> '' THEN detail
                                 ELSE ' '
                               END AS Email
                  FROM   phonefaxother
                  WHERE  [type] = '<EMAIL>'
                         AND personid = @PersonID) AS P5
              ON P1.personid = R.uniqueid
WHERE  uniqueid = @PersonID ", user).FirstOrDefaultAsync();
            
            return qmFields;
        }
    }
}