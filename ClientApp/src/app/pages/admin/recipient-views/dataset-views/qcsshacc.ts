
import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,functionalStatus,leaveTypes, ClientService, dateFormat, dataSetDropDowns } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';


@Component({
    selector: '',
    styles: [`
     nz-select {
            width: 100%;
            background-color: #cbe8f7 !important;
        }

     input{
        background-color: #cbe8f7;
    }

        input:focus {
            background-color: #85B9D5;
        }   

        nz-select:focus {
            background-color: #85B9D5;
        }  

        .row{
            margin:3px;
        }
    nz-tabset{
        margin-top:1rem;
    }
    .ant-divider-horizontal.ant-divider-with-text-center, .ant-divider-horizontal.ant-divider-with-text-left, .ant-divider-horizontal.ant-divider-with-text-right {
        margin:1px 0
    }
    nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
        height: 25px !important;
        font-size: 13px !important;
    }
    
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
        line-height: 24px;
        height: 25px;
        border-radius:15px 4px 0 0;
        margin:0 -10px 0 0;
    }
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
        background: #85B9D5;
        color: #fff;
    }
    nz-select{
        width:100%;
        background-color: #cbe8f7 !important;
    }
  
    
    :host ::ng-deep  .ant-select-selection  {
      background-color: #cbe8f7 !important;
    }

    `],
    templateUrl: './qcsshacc.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class QCSSHAC implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    dateFormat: string = dateFormat;
    loading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    functionalStatus = functionalStatus;
    dataSetDropDowns=dataSetDropDowns;
    tableData: Array<any> = [];
    residenyStatus=dataSetDropDowns.residenyStatus;
    branches: Array<any> = [];

    private default: any = {
        recordNumber: '',
        personID: '',
        branch: null,
        notes: ''
    }

    carerRecipientList: any;
    carerRelationshipList: any;
    carerAvailabilityList: any;
    carerResidencyList: any;
    genderlist: any;
    countries: any;
    languages: any;
    indigniousStatusList: any;
    livingArrangemntslist: any;
    accomodationSetting: any;
    pensionAll: any;
    dvaCardStatus: any;
    referalSource: any;
    today:any;
    IncomeSource: any;
    IncomeFrequency: any;
    housework:any;
    transport:any;
    shopping:any;
    medication:any;
    money:any;
    walking:any;
    bathShower:any;
    confusion:any;
    behaviouralProblems:any;
    communication:any;
    dressing:any;
    eating:any;
    toileting:any;
    gettingUp:any;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/recipient/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'qcsshac')) {
                    this.user = data;
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {

            this.today= new Date();
            this.user = this.sharedS.getPicked();
           
            this.getGeneralDataLists(this.user);
            this.buildForm();
            this.getCarerDataLists();

            this.search(this.user);

            this.sharedS.emitSaveAll$.subscribe(data => {
                if (data.type=='Recipient' &&  data.tab=='Datasets'){
                    if (this.globalS.isCurrentRoute(this.router, 'qcsshac')) 
                         this.save();
                }

              })

           
           
        }   
        tabFindIndexScope: number = 0;
        view(index: number){
            if(index == 2){
                this.getCarerDataLists();
            }
            this.tabFindIndexScope = index;
        }
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            this.inputForm = this.formBuilder.group({
                referralSource: '',              
                countryOfBirth: [null, [Validators.required]],
                homeLanguage: '',
                indiginousStatus:'',
                livingArrangements:'',
                dwellingAccomodation:'',
                carerResidency :'',
                pensionStatus:'',
                haccdvaCardHolderStatus:'',                
                fP1_Housework:'',
                fP2_WalkingDistance:'',
                fP3_Shopping:'',
                fP4_Medicine:'',
                fP5_Money:'',
                fP6_Walking:'',
                fP7_Bathing:'',
                fP8_Memory:'',
                fP9_Behaviour:'',
                fpA_Communication:'',
                fpA_Dressing:'',
                fpA_Eating:'',
                fpA_Toileting:'',
                fpA_GetUp:'',               
                carerAvailability:'',
                carerRelationship :'',
                datasetCarer:'',
                residencyVisaStatus:'',
                careR_MORE_THAN_ONE: false,
                qcsS_Disabilities:'00000',
                psychiatric : false,
                sensory : false,
                physical : false,
                chronic : false,
                inadequatelyDescribed : false
            




            });
        }
        
        search(user: any = this.user) {
            this.timeS.getdatasetqacchac(user.id)
            .subscribe(data => {
                this.inputForm.patchValue(data);   
                this.set_qcsS_Disabilities();
                        
        });
        }
        
        listDropDown(user: any = this.user) {
            this.branches = [];
            this.listS.getintakebranches(user.id)
            .subscribe(data => this.branches = data)

           
        }
        getGeneralDataLists(user: any = this.user){
            this.cd.reattach();
            this.loading = true;
            
            this.IncomeSource= dataSetDropDowns.IncomeSource;
            this.IncomeFrequency= dataSetDropDowns.IncomeFrquency;
            this. housework = functionalStatus.housework;    
            this.transport =  functionalStatus.transport;
            this.shopping =  functionalStatus.shopping;
            this.medication=  functionalStatus.medication;
            this.money=  functionalStatus.money;
            this.walking=  functionalStatus.walking;
            this.bathShower=  functionalStatus.bathShower;
            this.confusion=  functionalStatus.confusion;
            this.behaviouralProblems=  functionalStatus.behaviouralProblems;
            this.communication=  functionalStatus.communication;
            this.dressing=  functionalStatus.dressing;
            this.eating=  functionalStatus.eating;
            this.toileting=  functionalStatus.toileting;
            this.gettingUp = functionalStatus.gettingUp;

        
            return forkJoin([
                this.listS.GetHaccSex(),
                this.listS.GetLanguages(),
                this.listS.GetIndigniousStatus(),
                this.listS.GetLivingArrangments(),
                this.listS.GetAccomodationSetting(),
                this.listS.getpensionall(),
                this.listS.GetHACCVaCardStatus(),
                this.listS.GetHACCReferralSource(),
                this.listS.GetCountries(),

            ]).subscribe(x => {
                this.genderlist             = x[0];
                this.languages              = x[1];
                this.indigniousStatusList       = x[2];
                this.livingArrangemntslist      = x[3];
                this.accomodationSetting    = x[4];
                this.pensionAll             = x[5];
                this.dvaCardStatus          = x[6];
                this.referalSource          = x[7];
                this.countries              = x[8];
                this.loading = false;
                this.cd.detectChanges();
            });
        }
        getCarerDataLists(){
            return forkJoin([
                this.listS.GetCarerDataRecipientcarer(),
                this.listS.GetCarerDataRelationship(),
                this.listS.GetCarerDataAvailability(),
                this.listS.GetCarerDataResidency(),
            ]).subscribe(x => {
                this.carerRecipientList     = x[0];
                this.carerRelationshipList  = x[1];
                this.carerAvailabilityList  = x[2];
                this.carerResidencyList     = x[3];
                this.search(this.user);
            });
        }
        get_qcsS_Disabilities(){

            let bitStr = `${this.inputForm.value.psychiatric ? '1' :'0'}${this.inputForm.value.sensory ? '1' : '0' }${this.inputForm.value.physical ? '1' : '0' }${this.inputForm.value.chronic ? '1' : '0'}${this.inputForm.value.inadequatelyDescribed ? '1' : '0'}`;

            this.inputForm.patchValue({qcsS_Disabilities:bitStr});

        }
        set_qcsS_Disabilities(){

            let str=this.inputForm.value.qcsS_Disabilities
            
            this.inputForm.patchValue ({
                psychiatric  : str.charAt(0)=='1'? true : false,
                sensory  : str.charAt(1)=='1'? true : false,
                physical  : str.charAt(2)=='1'? true : false,
                chronic  : str.charAt(3)=='1'? true : false,
                inadequatelyDescribed  : str.charAt(4)=='1'? true : false
            });
        }
        save(){

            this.get_qcsS_Disabilities();
            this.timeS.updatedatsetqcchac(this.inputForm.value, this.user.id)
                .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
        }
        
        handleCancel() {
            this.modalOpen = false;
            this.loading = false;
            this.inputForm.reset(this.default);
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        getYears(){
            let years : Array<any>=[];

            for (let i = 1900; i <= this.today.getFullYear(); i++) {
                years.push(i);
            }
            return years;
        }
        getMonths(){
            let months : Array<any>=[
                {value:1,month:'January'},
                {value:2,month:'February'},
                {value:3,month:'March'},
                {value:4,month:'April'},
                {value:5,month:'May'},
                {value:6,month:'June'},
                {value:7,month:'July'},
                {value:8,month:'August'},
                {value:9,month:'September'},
                {value:10,month:'October'},
                {value:11,month:'November'},
                {value:12,month:'December'},
            ];
            return months;

        }
        showAddModal() {
            this.addOREdit = 1;
            this.listDropDown();
            this.modalOpen = true;
        }
        
        showEditModal(index: number) {
            this.addOREdit = 2;
            const { branch, recordNumber, notes } = this.tableData[index];
            this.inputForm.patchValue({
                recordNumber,
                branch,
                notes
            });
            
            this.modalOpen = true;
        }
        
        delete(index: number) {
            const { recordNumber } = this.tableData[index];
            this.timeS.deletebranches(recordNumber)
            .subscribe(data => {
                this.globalS.sToast('Success', 'Branch Deleted');
                this.search();
            })
        }
    }