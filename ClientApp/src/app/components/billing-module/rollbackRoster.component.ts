import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { takeUntil, timeout } from 'rxjs/operators';
import { setDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common'
import { formatDate } from '@angular/common';
import addYears from 'date-fns/addYears';
import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
    selector: 'app-billing',
    templateUrl: './rollbackRoster.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
.btn1 {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}
  `]
})
export class RollbackRosterComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;
  
    isVisible: boolean = false;
    
    loading: boolean = false;
    modalOpen: boolean = false;
    inputForm: FormGroup;
    dateFormat: string = 'dd/MM/yyyy';
    postLoading: boolean = false;
    title: string = "Rolling Back Roster Batch";
    token: any;
    check: boolean = false;
    userRole: string = "userrole";
    operatorID: any;
    id: string;
    private unsubscribe: Subject<void> = new Subject();
    allFundingChecked: boolean;
    date: moment.MomentInput;
    batchList: Array<any>;
    batchNumber: any;
    batchUser: any;
    batchDate: any;
    batchDetail: any;
    selectedBatch: any;
    updatedRecords: any;
    currentDateTime: any;
    batchDescription: any;
    confirmModal?: NzModalRef;

    constructor(
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private menuS: MenuService,
        private billingS: BillingService,
        private modal: NzModalService,
        public datepipe: DatePipe,
    ) { }

    ngOnInit(): void {
        this.token = this.globalS.decode();
        this.buildForm();
        this.loadRosterBatch();
        this.loading = false;
        this.modalOpen = true;
    }
    loadTitle() {
        return this.title
    }
    resetModal() {
        this.inputForm.reset();
        this.selectedBatch = '';
        this.postLoading = false;
    }
    handleCancel() {
        this.resetModal();
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/billing']);
    }
    buildForm() {
        this.inputForm = this.formBuilder.group({
            selectedBatch: '',
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
      for (let property in changes) {
        if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
          this.isVisible = true;
        }
      }
    }

    loadRosterBatch() {
        this.loading = true;
        this.billingS.getRosterBatch().subscribe(data => {
            if (data != '') {
            this.batchList = data;
            this.batchNumber = data[0].batchNumber;
            this.batchUser = data[0].batchUser;
            this.batchDate = data[0].batchDate;
            this.batchDetail = data[0].batchDetail;
            this.loading = false;
            }
        });
    }

    rollBackBatch() {
        this.postLoading = true;
        this.billingS.rollbackRosterBatch({
            OperatorID: this.operatorID,
            CurrentDateTime: this.currentDateTime,
            BatchNumber: this.selectedBatch,
            BatchDescription: this.batchDescription,
        }).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    this.updatedRecords = data[0].updatedRecords;
                    this.globalS.sToast('Success','The Roster Batch # ' + this.selectedBatch + ' has been rolled back successfully.');
                    Promise.resolve().then(() => {
                        this.inputForm.setControl('selectedBatch', new FormControl());
                    })
                }
                this.postLoading = false;
                this.resetModal();
                this.ngOnInit();
                return false;
            });
    }

    rollBack() {
        this.selectedBatch = this.inputForm.get('selectedBatch').value;
        this.selectedBatch = this.selectedBatch.substr(0, this.selectedBatch.indexOf(' -'));
        this.operatorID = this.token.nameid;
        this.currentDateTime = this.globalS.getCurrentDateTime();
        this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');
        this.batchDescription = 'Billing Batch#:' + this.selectedBatch + ' rolled back. Dated ' + this.currentDateTime;
        if (this.selectedBatch != '') {
            this.confirmModal = this.modal.confirm({
                nzTitle: 'Do you want to Roll Back Batch?',
                nzContent: 'This process cannot be undone. <br> Are you sure you want to delete all roster records copied/created under roster-copy-batch #: ' + this.selectedBatch + ' ?',
                nzOnOk: () => {
                    // this.globalS.iToast('Information', 'OK Button Selected.')
                    this.rollBackBatch();
                },
                nzOnCancel: () => {
                    // this.globalS.sToast('Success', 'CANCEL Button Selected.')
                }
            });
        } else if (this.selectedBatch == '') {
            this.globalS.eToast('Error', 'Please select Roster Batch to Rollback.')
        }
    }
}


