import { NgModule, ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'

import { TableMark } from './tablemark'
import { TableMarkColumn } from './tablemark-column'

@NgModule({
    declarations: [
        TableMark,
        TableMarkColumn
    ],
    imports: [
        CommonModule
    ],
    exports:[
        TableMark,
        TableMarkColumn
    ]
})

export class TableMarkModule{
    static forRoot(): ModuleWithProviders{
        return {
            ngModule: TableMarkModule
        }
    }
}