﻿namespace AdamasV3.Models.Dto
{
    public class TokenRefreshValidation
    {
        public string? AccessToken { get; set; }
        public string? RefreshToken { get; set; }
    }
}
