﻿using Adamas.Models.Modules;
using AdamasV3.Models.Modules;
using System.Threading.Tasks;
using static DocuSign.eSign.Client.Auth.OAuth;
using AdamasV3.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using DocuSign.eSign.Client;


using User = eg_03_csharp_auth_code_grant_core.Models.User;
using UserInfo = DocuSign.eSign.Client.Auth.OAuth.UserInfo;
using Account = DocuSign.eSign.Client.Auth.OAuth.UserInfo.Account;

namespace AdamasV3.DAL
{
    public interface IDocusignService
    {
        string BTOAEncoding(string toEncode);
        bool PostToken(string token);
        Task<DocusignToken> GetToken();
        Task<string> PostEnvelope(DocusignDocument document);
        Task<bool> PostCopyMTA(int docID);
        bool CopyMTADocument(string FileName, string pathDirectory, string sourcePath = null);
        string RemovePathRoot(string folder, string directoryPath);
        OAuthToken AuthenticateWithJWT();
        Task<DocumentDetails> GetDocumentDetails(int docId);
        Task<bool> UpdateDocumentStatus(string envelopeId, string status, string fileName, int Doc_Id);
        Stream DownloadCompletedDocument(string envelopeId, string docSelect);
        Account GetAccount(DocuSignClient client, OAuthToken accessToken);
        void RefreshJWT();
        Task<bool> TestCopyPasteProcedure(string filename, string sourcePath, string destinationPath);

    }
}
