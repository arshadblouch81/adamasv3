﻿using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;

namespace Adamas.Models
{
    public class SqlDbConnection: ISqlDbConnection
    {
        private readonly IConfiguration _config;
        public SqlDbConnection(IConfiguration config) {
            _config = config;
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(_config["ConnectionStrings:Production"]);
        }
    }
}
