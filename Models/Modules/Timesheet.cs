using System;

namespace Adamas.Models.Modules{
    public class Timesheet{
        public int? RecordNo { get; set; } 
        public string ClientCode { get; set; }
        public string CarerCode { get; set; }
        public string ServiceType { get; set; }
        public string ServiceDescription { get; set; }
        public string Program { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }

        // not string datatype
        public Single? Duration { get; set; }        
        public double? UnitPayRate { get; set; }
        public double? UnitBillRate { get; set; }
        public double? YearNo { get; set; }
        public Single? MonthNo { get; set; }
        public Single? Dayno { get; set; }
        public Single? BlockNo { get; set; }        
        public double? Type { get; set; }

        public string Status { get; set; }
        public string Anal { get; set; }

        // not string datatype
        public DateTime? DateEntered { get; set; }
        public DateTime? DateLastMod { get; set; }        
        public double Transferred { get; set; }        
        public Boolean GroupActivity { get; set; }

        public string BillTo { get; set; }
        public string CostUnit { get; set; }

        // not string datatype
        public double? CostQty { get; set; }

        public string HACCType { get; set; }
        public string DischargeReasonType { get; set; }
        
        public string BillUnit { get; set; }

        // not string datatype
        public double? BillQty { get; set; }
        public double? TaxPercent { get; set; }

        public string UniqueID { get; set; }
        public string ServiceSetting { get; set; }

        // not string datatype
        public int? StaffPosition { get; set; }
        public string ServiceTypePortal { get; set; }
        public string Creator { get; set; }
        public string DatasetClient { get; set; }
        
        public string GroupFlag { get; set; }

        public string TAMode { get; set; }
        public bool? TA_Multishift { get; set; }
        public bool? TA_EXCLUDEGEOLOCATION { get; set; }
        public bool? TA_EXCLUDEFROMAPPALERTS { get; set; }
        public string Attendees { get; set; }

    }
}