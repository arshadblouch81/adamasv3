﻿using DocumentFormat.OpenXml.Bibliography;

namespace AdamasV3.Models.Dto
{
    public class RemoteDocumentCopy
    {
        public string UncPathDirectory { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public bool DisableDirectoryExists { get; set; }
    }
}
