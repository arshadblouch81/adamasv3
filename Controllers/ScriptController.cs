﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.IO;
using System.Collections.Generic;
using System.Transactions;

using Adamas.DAL;
using Adamas.Models;
using Microsoft.EntityFrameworkCore;
using AdamasV3.DAL;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScriptController : Controller
    {
        private readonly DatabaseContext _context;

        public ScriptController(
            DatabaseContext context
            )
        {
            _context = context;
        }

        [HttpGet("checkup")]
        public async Task<IActionResult> GetCheckUp()
        {
            var exeDir = System.IO.Directory.GetCurrentDirectory();
            var fileDirectory = System.IO.Path.Combine(exeDir, "StaticFiles\\Alara.csv");
            var lines = System.IO.File.ReadAllLines(fileDirectory).Where(x => x.Length > 1).Select(UserFile.ParseCSV);

            List<UserFile> usersWithNoRecipientRecord = new List<UserFile>();
            List<UserFile> usersWithRecipientRecord = new List<UserFile>();

            foreach (var line in lines)
            {
                var userDetails = await (from u in _context.UserInfo where u.Name == line.Name select u).FirstOrDefaultAsync();

                if (userDetails != null)
                {
                    var recipientDetails = await (from rd in _context.Recipients where rd.UniqueID == userDetails.Password select rd).FirstOrDefaultAsync();

                    if(recipientDetails != null)
                    {
                        userDetails.StaffCode = recipientDetails.AccountNo;
                        userDetails.CustomDM2 = "DONE";
                        userDetails.Password = PasswordProcess.Encrypt(userDetails.Password);

                        usersWithRecipientRecord.Add(line);
                        _context.SaveChanges();
                    }
                    else
                    {
                        usersWithNoRecipientRecord.Add(line);
                    }
                } else
                {
                    usersWithNoRecipientRecord.Add(line);
                }

                
            }

                
            return Ok(new
            {
                LinesSuccessful = usersWithRecipientRecord,
                LinesFailed = usersWithNoRecipientRecord,
                CountSuccesful = usersWithRecipientRecord.Count(),
                CountFailed = usersWithNoRecipientRecord.Count()
            });
            
        }
    }

    public class UserFile
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Coordinator { get; set; }
        public string NDIA { get; set; }
        public DateTime? Birthdate { get; set; }


        internal static UserFile ParseCSV(string line)
        {
            var columns = line.Split(",");

            return new UserFile
            {
                Title       = columns[0],
                FirstName   = columns[1],
                MiddleName  = columns[2],
                Surname     = columns[3],
                Address     = columns[4],
                Suburb      = columns[5],          
                Postcode    = columns[6],
                State       = columns[7],
                Name        = columns[8],
                Password    = columns[9],
                Coordinator = columns[10],
                NDIA        = columns[11],
                Birthdate   = ReturnValidDateTimeOrNull(columns[13])
            };
        }

        internal static DateTime? ReturnValidDateTimeOrNull(string datetime)
        {
            DateTime value;
            if (datetime == null) return null;

            if (DateTime.TryParse(datetime, out value))
            {
                return value;
            }
            return null;
        }

    }


}
