import { Component, AfterContentInit } from '@angular/core'


@Component({
    selector: 'table-nested',
    template: `
        <ng-content></ng-content>
    `
})

export class Table implements AfterContentInit {

    constructor(){ }

    ngAfterContentInit(){

    }
}