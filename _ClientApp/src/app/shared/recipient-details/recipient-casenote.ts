import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { ClientService } from '@services/index'
@Component({
    selector: 'recipient-casenote',
    templateUrl: './recipient-casenote.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RecipientCaseNoteComponent),
            multi: true
        }
    ]
})

export class RecipientCaseNoteComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    result: Array<any>

    constructor(
        private clientS: ClientService
    ){ }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        this.clientS.getcaseprogressnote(user.code)
            .subscribe(data => {
                console.log(data)
                this.result = data
            })
    }
}