
import {switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component,forwardRef, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Subject ,  Subscription ,  Observable } from 'rxjs';

import { ClientService } from '../services/index';

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => SuburbComponent),
};

@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'suburb',
    templateUrl: './suburb.html',
    styles:[`
    input {
        outline:none;
        text-transform: uppercase;
    }
    .list{
        z-index: 5;
        margin: 0;
        list-style: none;
        max-height: 10rem;
        width: 100%;
        overflow: auto;
        border: 1px solid #dfdfdf;
        position: absolute;
        background: #fff;
        font-size:12px;
        cursor:pointer;
        max-width:10rem;
    }
    .list li{
        padding: 3px 10px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
    }
    .list li:hover{
        background:#1e90ff;
        color:#fff;
        cursor:default;
    }
    li b{
        float:right;
    }
    `],
    providers:[
        CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR
    ]
})


export class SuburbComponent implements OnDestroy, ControlValueAccessor {
     //Placeholders for the callbacks which are later provided
     //by the Control Value Accessor
     private onTouchedCallback: () => void = noop;
     private onChangeCallback: (_: any) => void = noop;
 

    private _subscription$: Subscription;
    private searchResult$: Observable<any>;
    private searchStream = new Subject<string>();
    show: boolean = false;
    
    lists: Array<any> = [];

    constructor(
        private clientS: ClientService,
        private elem: ElementRef
    ){
        this.searchResult$ = this.searchStream.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(data =>{
                let pcode = /(\d+)/g.test(data) ? data.match(/(\d+)/g)[0] : "";
                let suburb = /(\D+)/g.test(data) ? data.match(/(\D+)/g)[0] : "";

                return this.clientS.getsuburb({ 
                    Postcode: pcode, 
                    SuburbName: suburb 
                });
            }),);
        
        this._subscription$ = this.searchResult$.subscribe(data => {
            this.lists = data;
            this.show = true;
        })
        
    }

    ngOnDestroy(){
        this._subscription$.unsubscribe();
    }

    select(index: number){
        let data = this.lists[index];
        let subStr = data.suburb + ' ' + data.postcode;
        this.onChangeCallback(subStr);
        this.writeValue(subStr);
        this.show = false;
    }

    onClick(event: Event){
        if(!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }
     //The internal data model
     private innerValue: any = '';

     change(data: string){
        this.searchStream.next(data);
        //this.writeValue(data);
     }

     //get accessor
     get value(): any {
         return this.innerValue;
     };
 
     //set accessor including call the onchange callback
     set value(v: any) {
         if (v !== this.innerValue) {
             this.innerValue = v;
             this.onChangeCallback(v);
         }
     }
 
     //Set touched on blur
     onBlur() {
         this.onTouchedCallback();
     }
 
     //From ControlValueAccessor interface
     writeValue(value: any) {
         if (value !== this.innerValue) {
             this.innerValue = value;
         }
     }
 
     //From ControlValueAccessor interface
     registerOnChange(fn: any) {
         this.onChangeCallback = fn;
     }
 
     //From ControlValueAccessor interface
     registerOnTouched(fn: any) {
         this.onTouchedCallback = fn;
     }

}