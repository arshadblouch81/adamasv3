using System;

namespace Adamas.Models.Modules{
    public class ActivityCodesInput {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}