import { Component, OnInit } from '@angular/core';
import { GlobalService, ClientService } from '../../services/index';
import {takeUntil, switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';

import { Subject } from 'rxjs';
import * as moment from 'moment';

@Component({
    templateUrl: './notes.html',
    styles:[`
    .form-group{
        margin-top:20px;
    }
    .date{
        font-size:14px;
        font-weight:500;
        padding-left:10px;
        color: #4d99d0;
    }
    .even{
        background: #efefef;
    }
    .package-table th{
        font-size:15px;
        padding: 5px !important;
    }
    `]
})

export class NotesClient implements OnInit {

    view = new Subject<number>();
    dateStream = new Subject<any>();

    viewNo: number = 1;

    private token: any;
    user: any = "";
    accountNo: string;

    loading: boolean = false;
    tableData: Array<any>;

    sDate: any;
    eDate:any;

    constructor(
        private globalS: GlobalService,
        private clientS: ClientService
    ){ 
        this.view.subscribe(view => {
            this.viewNo = view;

            if(this.viewNo == 1)
                this.getOPNotes(this.token.uniqueID);            

            if(this.viewNo == 2)
                this.dateStream.next();            
        });

        this.dateStream.pipe(
            debounceTime(100),
            switchMap((x: any) => {
                this.tableData = []
                this.loading = true;
                return this.getShiftNotes();
            }))
            .subscribe(data => {
                this.loading = false;
                this.tableData = data;              
            })
    }

    ngOnInit(){
        var token = this.globalS.decode();

        this.accountNo = token.code;
        this.token = token;

        this.view.next(1);

        this.sDate = moment().startOf('month');
        this.eDate = moment().endOf('month');
    }

    isEven(index: number){
        return index % 2 == 0;
    }

    getOPNotes(uniqueId: string){
        this.loading = true;
        this.clientS.getopnotes(uniqueId).subscribe(data => {
            if(data)           
                this.tableData = data.list;
                
            this.loading = false;
        });
    }

    getShiftNotes(){
        return this.clientS.getservicenotes({
            client: this.accountNo,
            startDate: moment(this.sDate).format('YYYY/MM/DD'),
            endDate: moment(this.eDate).format('YYYY/MM/DD')
        });
    }

}