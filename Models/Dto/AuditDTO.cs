﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adamas.Models.Dto
{
    public class AuditDTO
    {
        public string Operator { get; set; }
        public DateTime? ActionDate { get; set; }
        public string ActionOn { get; set; }
        public string WhoWhatCode { get; set; }
        public string TraccsUser { get; set; }
        public string AuditDescription { get; set; }
    }
}
