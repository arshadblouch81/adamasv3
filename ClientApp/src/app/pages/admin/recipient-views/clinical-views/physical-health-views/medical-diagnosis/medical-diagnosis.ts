


import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, SimpleChanges, AfterContentChecked, AfterViewChecked, OnChanges, DoCheck } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject, EMPTY } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import {oni} from '@services/global.service'

@Component({
    selector: '',      
     styleUrls: ['./medical-diagnosis.css'],
  templateUrl: './medical-diagonsis.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ClinicalMedicalDiagnose  implements OnInit, OnChanges,  OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    loading2: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;

    tableData: Array<any> = [];
    actions: any = [];
    activeRowData:any;
    selectedRowIndex:number;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    type :number=6;
    items: Array<any>=[];
    showList:boolean=false;
    selectedItem:any;
    HighlightRow:number;
    txtSearch:string;
    originalList:Array<any>=[];

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef,
        private nzContextMenuService: NzContextMenuService
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'health-conditions')) {
                this.user = data;
                this.search(data);
            }
        });
    }
   
  
    ngOnChanges(changes: SimpleChanges): void {
    }
  
    form_title:string;
    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();

        this.form_title='Add/Change Medical DIAGNOSIS';

        this.loading2=true;
        this.listS.getDiagnosistypes(this.user.id,2).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.items = data;
          this.originalList = data;
          this.loading2=false;
          this.cd.detectChanges();
         
        });
    }   

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            recordNumber: '',
            personId: [null, [Validators.required]],
            action: null,
            description: '',
            type: this.type,
            code: '',
        });

        this.inputForm.get('description').valueChanges.subscribe(data => {
          let d=this.items.find(x => x.description === data);
          this.inputForm.patchValue({action : d.code});
        })

        // this.inputForm.get('description').valueChanges.pipe(
        //     switchMap(x => {
        //         if(!x)
        //             return EMPTY;

        //         return this.listS.getDiagnosistypeCode(x,2)
        //     })
        // ).subscribe(data => {
            
        //    this.inputForm.patchValue({action : data.code});
       
        // });
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(view: number, data: any): void {
      
          data = this.activeRowData;
          this.delete(this.selectedRowIndex);
        

    }
    search(user: any = this.user) {
        this.cd.reattach();
        this.loading = true;
        this.listS.getOnimainissues(user.id,this.type).subscribe(data => {
            this.loading = false;
            this.tableData = data;
            this.cd.detectChanges();
        });

        this.listDropDown();
    }

    listDropDown(user: any = this.user) {
         this.actions =oni.actions;
        // this.listS.getintakebranches(user.id)
        //     .subscribe(data => this.actions = data)
    }

    save() {

        this.inputForm.patchValue ({personId :this.user.id , type:this.type, });

        if (!this.globalS.IsFormValid(this.inputForm)){
          this.globalS.sToast('Error', 'Please fill all fields');
          return;
      }
          
        this.loading = true;
        if (this.addOREdit == 1) {
            this.listS.postOnimainissues(this.inputForm.value)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Record Inserted');
                    this.loading = false;
                    this.search();
                    this.handleCancel();
                });
        }

        if (this.addOREdit == 2) {
            this.listS.updateOnimainissues(this.inputForm.value)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Record Updated');
                    this.loading = false;
                    this.search();
                    this.handleCancel();
                });
        }
    }

    handleCancel() {
        this.modalOpen = false;
        this.loading = false;
        this.inputForm.reset();
    }

    trackByFn(index, item) {
        return item.id;
    }

    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }

    showAddModal() {
        this.addOREdit = 1;
        this.listDropDown();
        this.showList = true;
    }

    showEditModal(data: any, index:number) {
        this.activeRowData = data;
        this.selectedRowIndex=index;
        this.addOREdit = 2;
        this.items = [data, ...this.items];
        const { action, recordNumber, description, personId,code } = this.activeRowData;
        this.inputForm.patchValue({
            recordNumber,
            action,
            description,
            personId,
            code,
            type : this.type,
            
        });

        this.modalOpen = true;
    }

    delete(index: number) {
        const { recordNumber } = this.activeRowData;
        this.listS.deleteOnimainissues(recordNumber,this.type)
                    .subscribe(data => {
                        this.globalS.sToast('Success', 'Record Deleted');
                        this.search();
                    })
    }

    contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, data:any): void {
        this.activeRowData = data;
        this.selectedRowIndex = this.tableData.indexOf(data);
        this.nzContextMenuService.create($event, menu);
      }
    menuclick(event:any,i:number){
        if (i==1){
        this.showEditModal(this.activeRowData,this.selectedRowIndex);

        }else if (i==2){
            this.delete(this.selectedRowIndex);
        }
    }
    
    showConfirm(){
      this.showList=false;
    }

  onItemClick(sel:any , i:number) : void {
      this.selectedItem=sel;
      this.HighlightRow=i;

      this.sendToSave(sel)
  }
 
      
 onItemDbClick(sel:any , i:number) : void {
  this.selectedItem=sel;
  this.HighlightRow=i;
  
  this.sendToSave(sel)

}
sendToSave(sel:any){
    this.inputForm.patchValue({
        description: sel.description,
        action: sel.code,
      });
      this.showList=false;
      this.save();

    // this.listS.getDiagnosistypeCode(sel,2).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //     this.inputForm.patchValue({
    //         description: data.description,
    //         action: data.code,
    //       });
    //       this.showList=false;
    //       this.save();
    // });
  
}
onTextChangeEvent(event:any, listTye:any){
     // console.log(this.txtSearch);
      let value = this.txtSearch.toUpperCase();
   
      this.items=this.originalList.filter(element=>element.description.includes(value));
 
}


    generatePdf(){
      
        
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Medical Diagnosis",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Medical Diagnosis "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;  
                    this.cd.detectChanges();                     
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}


