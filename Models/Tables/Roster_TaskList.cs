using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;


namespace Adamas.Models.Tables
{
    public class Roster_TaskList
    {
        [Key]
        public int RecordNo { get; set; }
        public int RosterID { get; set; }
        public int TaskID { get; set; }
        public string TaskDetail { get; set; }
        public string TaskNotes { get; set; }
        public string TaskOrder { get; set; }
        public bool? TaskComplete { get; set; }
        public bool? XDeletedRecord { get; set; }
        public DateTime? XEndDate { get; set; }
    }
}