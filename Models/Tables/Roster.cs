using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Text;

namespace Adamas.Models.Tables
{
    public class Roster
    {
        [Key]
        public int RecordNo { get; set; }

        [Column("Carer Code")]
        public string CarerCode { get; set; }

        [Column("Client Code")]
        public string ClientCode { get; set; }

        [Column("Service Type")]
        public string ServiceType { get; set; }

        [Column("Service Description")]
        public string ServiceDescription { get; set; }
        public string Program { get; set; }
        public string Date { get; set; }

        [Column("Start Time")]
        public string StartTime { get; set; }
        public Single? Duration { get; set; }
        public double? YearNo { get; set; }
        public Single? MonthNo { get; set; }
        public Single? DayNo { get; set; }
        public Single? BlockNo { get; set; }

        [Column("Unit Pay Rate")]
        public double? UnitPayRate { get; set; }

        [Column("Unit Bill Rate")]
        public double? UnitBillRate { get; set; }
        public double? Type { get; set; }
        public string Status { get; set; }
        public string Anal { get; set; }

        [Column("Date Entered")]
        public DateTime? DateEntered { get; set; }

        [Column("Date Last Mod")]
        public DateTime? DateLastMod { get; set; }
        public double Transferred { get; set; }

        [Column("GroupActivity", TypeName = "bit")]
        [DefaultValue(false)]
        public bool GroupActivity { get; set; }
        public string BillTo { get; set; }
        public string CostUnit { get; set; }
        public double? CostQty { get; set; }
        public string HACCType { get; set; }
        public string DischargeReasonType { get; set; }
        
        public string BillUnit { get; set; }
        public double? BillQty { get; set; }
        public double? TaxPercent { get; set; }
        public string UniqueId { get; set; }
        public string ServiceSetting { get; set; }
        public int? StaffPosition { get; set; }
        public string ServiceTypePortal { get; set; }
        [Column("Creator")]
        public string Creator { get; set; }
        public string Notes { get; set; }
        public string Editer { get; set; }
          public string DatasetClient { get; set; }
        public string GroupFlag { get; set; }

        public string TAMode { get; set; }
        public bool? TA_Multishift { get; set; }
        public bool? TA_EXCLUDEGEOLOCATION { get; set; }
        public bool? TA_EXCLUDEFROMAPPALERTS { get; set; }
        public string Attendees { get; set; }
    }
}