using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using Microsoft.VisualBasic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.Function;
using Microsoft.Extensions.Configuration;
using Staff = Adamas.Models.Modules.Staff;
using Microsoft.AspNetCore.Http;
using MimeKit;
using MimeKit.Text;
using Newtonsoft.Json.Linq;
using AdamasV3.Models.Dto;
using System.DirectoryServices.AccountManagement;
using Org.BouncyCastle.Crypto.Digests;

namespace Adamas.Controllers
{
    //[Authorize]
    // [Authorize(Policy = "markpolicy")]
    [Route("api/[controller]")]
    // [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")] 
    public class MenuController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private readonly IEmailConfiguration _emailConfiguration;
        private IEmailService _emailService;
        public MenuController(
            DatabaseContext context,
            IConfiguration config,
            IGeneralSetting setting,
            IEmailConfiguration emailConfiguration,
            IEmailService emailService)
        {
            _context = context;
            GenSettings = setting;
            _emailConfiguration = emailConfiguration;
            _emailService = emailService;
        }

        [HttpPut("addBranch")]
        public async Task<IActionResult> AddBranch([FromBody] Branch branch)
        {
         using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                int PersonId=0;
                string sql;
                sql="INSERT INTO DataDomains(Description, Domain, Dataset, Embedded, User1, User2, EndDate, User3, User4) " +
                    " VALUES (@Description, @Domain,@Dataset , @Embedded,@User1,@User2,@EndDate,@User3,@User4);SELECT SCOPE_IDENTITY();";
               
                //;select @@identity
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                   try{
                        cmd.Parameters.AddWithValue("@Description", branch.name );
                        cmd.Parameters.AddWithValue("@Domain",  "BRANCHES");
                        cmd.Parameters.AddWithValue("@Dataset", "USER");
                        cmd.Parameters.AddWithValue("@Embedded", 0);
                        cmd.Parameters.AddWithValue("@User1", branch.glRevene);
                        cmd.Parameters.AddWithValue("@User2", branch.glCost);
                        cmd.Parameters.AddWithValue("@EndDate",(object)branch.end_date?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@User3", branch.startHour);
                        cmd.Parameters.AddWithValue("@User4", branch.finishHour);
                        await conn.OpenAsync();
                        var modified = await cmd.ExecuteScalarAsync();
                        PersonId = Convert.ToInt32(modified);

                    sql= "INSERT INTO NAMESANDADDRESSES(PersonID, GoogleAddress, Address1, Address2, Suburb, User1) " +
                    " Values(@PersonID, @GoogleAddress, @Address1, @Address2, @Suburb, @User1)";

                    cmd.Parameters.Clear();
                    cmd.CommandText=sql;
                    cmd.Parameters.AddWithValue("@PersonID", PersonId);
                    cmd.Parameters.AddWithValue("@GoogleAddress", branch.centerName);
                    cmd.Parameters.AddWithValue("@Address1", branch.addrLine1);
                    cmd.Parameters.AddWithValue("@Address2", branch.addrLine2);
                    cmd.Parameters.AddWithValue("@Suburb", branch.Phone);
                    cmd.Parameters.AddWithValue("@User1","C:\\Adamas\\AdamasLogo.gif");
                    await cmd.ExecuteNonQueryAsync();

                  
                   sql= "Insert into BRANCHEMAILS  (BranchID,BH_EarlyStart,BH_LateStart,BH_EarlyFinish,BH_LateFinish,BH_OverStay,BH_UndrStay,BH_NoWork,AH_EarlyStart,AH_LateStart,AH_EarlyFinish,AH_LateFinish,AH_OverStay,AH_UndrStay,AH_NoWork)" +
                        " Values (@BranchID,@BH_EarlyStart,@BH_LateStart,@BH_EarlyFinish,@BH_LateFinish,@BH_OverStay,@BH_UndrStay,@BH_NoWork,@AH_EarlyStart,@AH_LateStart,@AH_EarlyFinish,@AH_LateFinish,@AH_OverStay,@AH_UndrStay,@AH_NoWork)" ;

                     cmd.Parameters.Clear();
                     cmd.CommandText=sql;
                     cmd.Parameters.AddWithValue("@BranchID",PersonId);
                     cmd.Parameters.AddWithValue("@BH_EarlyStart",branch.earlyStart);
                     cmd.Parameters.AddWithValue("@BH_LateStart",branch.lateStart);
                     cmd.Parameters.AddWithValue("@BH_EarlyFinish",branch.earlyFinish);
                     cmd.Parameters.AddWithValue("@BH_LateFinish",branch.lateFinish);
                     cmd.Parameters.AddWithValue("@BH_OverStay",branch.overstay);
                     cmd.Parameters.AddWithValue("@BH_UndrStay",branch.understay);
                     cmd.Parameters.AddWithValue("@BH_NoWork",0);
                     cmd.Parameters.AddWithValue("@AH_EarlyStart",branch.t2earlyStart);
                     cmd.Parameters.AddWithValue("@AH_LateStart",branch.t2lateStart);
                     cmd.Parameters.AddWithValue("@AH_EarlyFinish",branch.t2earlyFinish );
                     cmd.Parameters.AddWithValue("@AH_LateFinish",branch.t2lateFinish);
                     cmd.Parameters.AddWithValue("@AH_OverStay",branch.t2overstay);
                     cmd.Parameters.AddWithValue("@AH_UndrStay",branch.t2understay);
                     cmd.Parameters.AddWithValue("@AH_NoWork",0);

                    await cmd.ExecuteNonQueryAsync();                   

                    return Ok(PersonId > 0);
                   }catch(Exception ex) {return Ok(ex);}
                }
            }
        }
[HttpPut("updateBranch")]
        public async Task<IActionResult> UpdateBranch([FromBody] Branch branch)
        {
         using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                int PersonId=0;
                string sql;
               
                  sql="update  DataDomains  set Description=@Description, Domain=@Domain, Dataset=@Dataset, Embedded=@Embedded, User1=@User1, User2=@User2, EndDate=@EndDate, User3='', User4='' " +
                      " where RecordNumber=@RecordNumber" ;

                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                   
                   try{
                    await conn.OpenAsync();
                        cmd.Parameters.AddWithValue("@Description", branch.name );
                        cmd.Parameters.AddWithValue("@Domain",  "BRANCHES");
                        cmd.Parameters.AddWithValue("@Dataset", "USER");
                        cmd.Parameters.AddWithValue("@Embedded", 0);
                        cmd.Parameters.AddWithValue("@User1", branch.glRevene);
                        cmd.Parameters.AddWithValue("@User2", branch.glCost);
                        cmd.Parameters.AddWithValue("@EndDate",(object)branch.end_date?? DBNull.Value);
                        // cmd.Parameters.AddWithValue("@EndDate", branch.end_date);
                        cmd.Parameters.AddWithValue("@RecordNumber", branch.RecordNumber);
                        PersonId = (int)await  cmd.ExecuteScalarAsync();
                        await cmd.ExecuteNonQueryAsync();
                        
                        sql= " Update  NAMESANDADDRESSES set GoogleAddress=@GoogleAddress, Address1= @Address1, Address2= @Address2, Suburb=@Suburb, User1=@User1 " +
                            "  where PersonID=@PersonID";
                            // ;update DataDomains set EndDate=null where EndDate='1900/01/01' and RecordNumber = " + PersonId 
                    cmd.Parameters.Clear();
                    cmd.CommandText=sql;
                  
                    cmd.Parameters.AddWithValue("@GoogleAddress", branch.centerName);
                    cmd.Parameters.AddWithValue("@Address1", branch.addrLine1);
                    cmd.Parameters.AddWithValue("@Address2", branch.addrLine2);
                    cmd.Parameters.AddWithValue("@Suburb", branch.Phone);
                    cmd.Parameters.AddWithValue("@User1","C:\\Adamas\\AdamasLogo.gif");
                    cmd.Parameters.AddWithValue("@PersonID", branch.RecordNumber );
                    await cmd.ExecuteNonQueryAsync();

                  
                   sql= "update BRANCHEMAILS  set BH_EarlyStart=@BH_EarlyStart,BH_LateStart=@BH_LateStart" +
                                ",BH_EarlyFinish=@BH_EarlyFinish,BH_LateFinish=@BH_LateFinish,BH_OverStay=@BH_OverStay,BH_UndrStay=@BH_UndrStay " +
                                ",BH_NoWork=@BH_NoWork,AH_EarlyStart=@AH_EarlyStart,AH_LateStart=@AH_LateStart,AH_EarlyFinish=@AH_EarlyFinish " +
                                ",AH_LateFinish=@AH_LateFinish,AH_OverStay=,@AH_OverStay,AH_UndrStay=@AH_UndrStay,AH_NoWork=@AH_NoWork" +
                                " where BranchID=BranchID" ;

                     cmd.Parameters.Clear();
                     cmd.CommandText=sql;
                    
                     cmd.Parameters.AddWithValue("@BH_EarlyStart",branch.earlyStart);
                     cmd.Parameters.AddWithValue("@BH_LateStart",branch.lateStart);
                     cmd.Parameters.AddWithValue("@BH_EarlyFinish",branch.earlyFinish);
                     cmd.Parameters.AddWithValue("@BH_LateFinish",branch.lateFinish);
                     cmd.Parameters.AddWithValue("@BH_OverStay",branch.overstay);
                     cmd.Parameters.AddWithValue("@BH_UndrStay",branch.understay);
                     cmd.Parameters.AddWithValue("@BH_NoWork",0);
                     cmd.Parameters.AddWithValue("@AH_EarlyStart",branch.t2earlyStart);
                     cmd.Parameters.AddWithValue("@AH_LateStart",branch.t2lateStart);
                     cmd.Parameters.AddWithValue("@AH_EarlyFinish",branch.t2earlyFinish );
                     cmd.Parameters.AddWithValue("@AH_LateFinish",branch.t2lateFinish);
                     cmd.Parameters.AddWithValue("@AH_OverStay",branch.t2overstay);
                     cmd.Parameters.AddWithValue("@AH_UndrStay",branch.t2understay);
                     cmd.Parameters.AddWithValue("@AH_NoWork",0);
                     cmd.Parameters.AddWithValue("@BranchID",branch.RecordNumber);
                     await cmd.ExecuteNonQueryAsync();

                    return Ok(true );
                   }catch(Exception ex) {return Ok(false);}
                }
            }
        }
        [HttpGet("branches/{is_where}")]
        public async Task<IActionResult> GetBranches(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
            if(is_where){
                query = "WHERE";
            }else{
                query = "WHERE ISNULL(d.DeletedRecord, 0) = 0 AND (d.EndDate Is Null OR d.EndDate >= GETDATE()) AND ";
            }
            
                //SELECT RecordNumber, DESCRIPTION from DATADOMAINS WHERE DOMAIN = 'BRANCHES'
                String sql="SELECT d.RecordNumber, d.DESCRIPTION,d.EndDate as end_date,d.DELETEDRECORD as is_deleted,  " +
                            " PersonID, GoogleAddress, Address1 as addrLine1, Address2 as addrLine2, Suburb as phone,d.User1 as glRevene,d.User2 as glCost,d.User3 as stratHour,d.User4 as finishHour," +
                            " BranchID,BH_EarlyStart,BH_LateStart,BH_EarlyFinish,BH_LateFinish,BH_OverStay,BH_UndrStay,BH_NoWork," +
                            " AH_EarlyStart,AH_LateStart,AH_EarlyFinish,AH_LateFinish,AH_OverStay,AH_UndrStay,AH_NoWork " +
                            " FROM DATADOMAINS d left  join NAMESANDADDRESSES n on convert(varchar,d.RecordNumber)=n.PersonID" +
                            " left join BRANCHEMAILS b on d.RecordNumber=b.BranchID " +
                            " "+query+" DOMAIN = 'BRANCHES' " +
                            " order by  d.DESCRIPTION " ;
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                DESCRIPTION = GenSettings.Filter(rd["DESCRIPTION"]),
                                end_date    = GenSettings.Filter(rd["end_date"]),
                                is_deleted  = GenSettings.Filter(rd["is_deleted"]),
                                PersonID    = GenSettings.Filter(rd["PersonID"]),
                                GoogleAddress = GenSettings.Filter(rd["GoogleAddress"]),
                                addrLine1 = GenSettings.Filter(rd["addrLine1"]),
                                addrLine2 = GenSettings.Filter(rd["addrLine2"]),
                                addrphone = GenSettings.Filter(rd["phone"]),
                                glrevene = GenSettings.Filter(rd["glRevene"]),
                                glcost = GenSettings.Filter(rd["glCost"]),
                                startHour = GenSettings.Filter(rd["stratHour"]),
                                finishHour = GenSettings.Filter(rd["finishHour"]),
                                BranchID = GenSettings.Filter(rd["BranchID"]),
                                BH_EarlyStart = GenSettings.Filter(rd["BH_EarlyStart"]),
                                BH_LateStart = GenSettings.Filter(rd["BH_LateStart"]),
                                BH_EarlyFinish = GenSettings.Filter(rd["BH_EarlyFinish"]),
                                BH_LateFinish = GenSettings.Filter(rd["BH_LateFinish"]),
                                BH_OverStay = GenSettings.Filter(rd["BH_OverStay"]),
                                BH_UndrStay = GenSettings.Filter(rd["BH_UndrStay"]),
                                BH_NoWork = GenSettings.Filter(rd["BH_NoWork"]),
                                AH_EarlyStart = GenSettings.Filter(rd["AH_EarlyStart"]),
                                AH_LateStart = GenSettings.Filter(rd["AH_LateStart"]),
                                AH_EarlyFinish = GenSettings.Filter(rd["AH_EarlyFinish"]),
                                AH_LateFinish = GenSettings.Filter(rd["AH_LateFinish"]),
                                AH_OverStay = GenSettings.Filter(rd["AH_OverStay"]),
                                AH_UndrStay = GenSettings.Filter(rd["AH_UndrStay"]),
                                AH_NoWork = GenSettings.Filter(rd["AH_NoWork"])
                         });
                         //,,,,,, 
                       // list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("userDetails")]
        public async Task<IActionResult> PostUserDetails([FromBody] UserInfo Userinfo)
        {

             if (!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                return BadRequest(errors);
            }

            try
            {

                 Userinfo.Password = EncryptValue(Userinfo.Password);


                using (var db = _context)
                {
                    await db.AddRangeAsync(Userinfo);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("userDetails")]
        public async Task<IActionResult> UpdateUserDetails([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    //General

                    // singleuserinfo.Name           =   userinfo.Name;
                    // singleuserinfo.Usertype       =   userinfo.Usertype;
                    // singleuserinfo.StaffCode      =   userinfo.StaffCode;
                    // singleuserinfo.loginMode      =   userinfo.loginMode;
                    // singleuserinfo.homeBranch     =   userinfo.homeBranch;
                    
                    // singleuserinfo.system         =   userinfo.system;

                    // singleuserinfo.Recipients     =   userinfo.Recipients;
                    // singleuserinfo.Staff          =   userinfo.Staff;
                    // singleuserinfo.Roster         =   userinfo.Roster;
                    // singleuserinfo.DayManager     =   userinfo.DayManager;

                    // singleuserinfo.timesheetPreviousPeriod = userinfo.timesheetPreviousPeriod;
                    // singleuserinfo.timesheet           =   userinfo.timesheet;
                    // singleuserinfo.statistics          =   userinfo.statistics;
                    // singleuserinfo.financial           =   userinfo.financial;
                    // singleuserinfo.reportPreview       =   userinfo.reportPreview;
                    // singleuserinfo.invoiceEnquiry      =   userinfo.invoiceEnquiry;
                    // singleuserinfo.allowTypeAhead      =   userinfo.allowTypeAhead;
                    // singleuserinfo.suggestedTimesheets =   userinfo.suggestedTimesheets;
                    // singleuserinfo.payIntegrityCheck   =   userinfo.payIntegrityCheck;
                    // singleuserinfo.timesheetUpdate     =   userinfo.timesheetUpdate;
                    // singleuserinfo.accessCDC           =   userinfo.accessCDC;
                    
                    //recipients Tab

                    // singleuserinfo.recipientRecordView      = userinfo.recipientRecordView;
                    // singleuserinfo.DisableBtnReferral       = userinfo.DisableBtnReferral;
                    // singleuserinfo.DisableBtnReferOn        = userinfo.DisableBtnReferOn;
                    // singleuserinfo.DisableBtnReferOn        = userinfo.DisableBtnReferOn;
                    // singleuserinfo.DisableBtnNotProceeding  = userinfo.DisableBtnNotProceeding;
                    // singleuserinfo.DisableBtnAssess         = userinfo.DisableBtnAssess;
                    // singleuserinfo.DisableBtnAdmit          = userinfo.DisableBtnAdmit;
                    // singleuserinfo.DisableBtnUnWait         = userinfo.DisableBtnUnWait;
                    // singleuserinfo.DisableBtnDischarge      = userinfo.DisableBtnDischarge;
                    // singleuserinfo.DisableBtnReinstate      = userinfo.DisableBtnReinstate;
                    // singleuserinfo.DisableBtnDecease        = userinfo.DisableBtnDecease;
                    // singleuserinfo.DisableBtnAdmin          = userinfo.DisableBtnAdmin;
                    // singleuserinfo.DisableBtnItem           = userinfo.DisableBtnItem;
                    // singleuserinfo.DisableBtnPrint          = userinfo.DisableBtnPrint;
                    // singleuserinfo.DisableBtnRoster         = userinfo.DisableBtnRoster;
                    // singleuserinfo.DisableBtnMaster         = userinfo.DisableBtnMaster;
                    // singleuserinfo.DisableBtnOnHold         = userinfo.DisableBtnOnHold;
                    // singleuserinfo.AddNewRecipient          = userinfo.AddNewRecipient;
                    // singleuserinfo.CanChangeClientCode      = userinfo.CanChangeClientCode;
                    // singleuserinfo.CanEditNDIA              = userinfo.CanEditNDIA;
                    // singleuserinfo.AllowProgramTransition   = userinfo.AllowProgramTransition;

                    //Documents Tab
                    // singleuserinfo.CanMoveImportedDocuments      = userinfo.CanMoveImportedDocuments;
                    // singleuserinfo.KeepOriginalAsImport          = userinfo.KeepOriginalAsImport;
                    // singleuserinfo.RecipientDocFolder            = userinfo.RecipientDocFolder;                    
                    // singleuserinfo.Force_RecipDocFolder          = userinfo.Force_RecipDocFolder;
                    // singleuserinfo.ONIImportExportFolder         = userinfo.ONIImportExportFolder;
                    // singleuserinfo.Force_ONIImportFolder         = userinfo.Force_ONIImportFolder;
                    // singleuserinfo.ONIArchiveFolder              = userinfo.ONIArchiveFolder;
                    // singleuserinfo.Force_ONIArchiveFolder        = userinfo.Force_ONIArchiveFolder;
                    // singleuserinfo.StaffDocFolder                = userinfo.StaffDocFolder;
                    // singleuserinfo.Force_StaffDocFolder          = userinfo.Force_StaffDocFolder;
                    // singleuserinfo.StaffRostersFolder            = userinfo.StaffRostersFolder;
                    // singleuserinfo.Force_StaffRosterFolder       = userinfo.Force_StaffRosterFolder;
                    // singleuserinfo.ReportExportFolder            = userinfo.ReportExportFolder;
                    // singleuserinfo.Force_ReportExportFolder      = userinfo.Force_ReportExportFolder;
                    // singleuserinfo.ReportSavesFolder             = userinfo.ReportSavesFolder;
                    // singleuserinfo.Force_ReportSavesFolder       = userinfo.Force_ReportSavesFolder;
                    // singleuserinfo.HACCMDSFolder                 = userinfo.HACCMDSFolder;
                    // singleuserinfo.Force_HACCMDSFolder           = userinfo.Force_HACCMDSFolder;
                    // singleuserinfo.Force_CSTDAMDSFolder          = userinfo.Force_CSTDAMDSFolder;
                    // singleuserinfo.NRCPMDSFolder                 = userinfo.NRCPMDSFolder;
                    // singleuserinfo.Force_NRCPMDSFolder           = userinfo.Force_NRCPMDSFolder;
                    // singleuserinfo.PayExportFolder               = userinfo.PayExportFolder;
                    // singleuserinfo.Force_PayExportFolder         = userinfo.Force_PayExportFolder;
                    // singleuserinfo.BillingExportFolder           = userinfo.BillingExportFolder;
                    // singleuserinfo.Force_BillingExportFolder     = userinfo.Force_BillingExportFolder;
                    
                    //roster form 
                    
                    // singleuserinfo.ChangeMasterRoster                = userinfo.ChangeMasterRoster;
                    // singleuserinfo.AllowRosterReallocate             = userinfo.AllowRosterReallocate;
                    // singleuserinfo.AllowMasterSaveAs                 = userinfo.AllowMasterSaveAs;
                    // singleuserinfo.ManualRosterCopy                  = userinfo.ManualRosterCopy;
                    // singleuserinfo.AutoCopyRoster                    = userinfo.AutoCopyRoster;
                    // singleuserinfo.CanRosterOvertime                 = userinfo.CanRosterOvertime;
                    // singleuserinfo.CanRosterBreakless                = userinfo.CanRosterBreakless;
                    // singleuserinfo.CanRosterConflicts                = userinfo.CanRosterConflicts;
                    // singleuserinfo.EditRosterRecord                  = userinfo.EditRosterRecord;
                    // singleuserinfo.OwnRosterOnly                     = userinfo.OwnRosterOnly;
                    
                    // day manager form 

                    // singleuserinfo.UseDMv2                          = userinfo.UseDMv2;
                    // singleuserinfo.APPROVEDAYMANAGER                = userinfo.APPROVEDAYMANAGER;
                    // singleuserinfo.RECIPMGTVIEW                     = userinfo.RECIPMGTVIEW;
                    // singleuserinfo.AllowStaffSwap                   = userinfo.AllowStaffSwap;
                    // singleuserinfo.AdminChangeOutputType            = userinfo.AdminChangeOutputType;
                    // singleuserinfo.AdminChangeActivityCode          = userinfo.AdminChangeActivityCode;
                    // singleuserinfo.AdminChangePaytype               = userinfo.AdminChangePaytype;
                    // singleuserinfo.AdminChangeDebtor                = userinfo.AdminChangeDebtor;
                    // singleuserinfo.AdminChangeBillAmount            = userinfo.AdminChangeBillAmount;
                    // singleuserinfo.adminChangeBillQty               = userinfo.adminChangeBillQty;
                    // singleuserinfo.AttendeesChangeBillAmount        = userinfo.AttendeesChangeBillAmount;
                    // singleuserinfo.AttendeesChangeBillQty           = userinfo.AttendeesChangeBillQty;
                    // singleuserinfo.AdminChangePayQty                = userinfo.AdminChangePayQty;
                    // singleuserinfo.LowChangeActivityCode            = userinfo.LowChangeActivityCode;
                
                    // day manager form 

                    // singleuserinfo.AllowsMarketing                     = userinfo.AllowsMarketing;
                    // singleuserinfo.ViewPackageStatement                = userinfo.ViewPackageStatement;
                    // singleuserinfo.CanManagePreferences                = userinfo.CanManagePreferences;
                    // singleuserinfo.AllowBooking                        = userinfo.AllowBooking;
                    // singleuserinfo.CanCreateBooking                    = userinfo.CanCreateBooking;
                    // singleuserinfo.BookingLeadTime                     = userinfo.BookingLeadTime;
                    // singleuserinfo.CanChooseProvider                   = userinfo.CanChooseProvider;
                    // singleuserinfo.ShowProviderPhoto                   = userinfo.AdminChangeDebtor;
                    // singleuserinfo.CanSeeProviderPhoto                 = userinfo.CanSeeProviderPhoto;
                    // singleuserinfo.CanSeeProviderGender                = userinfo.CanSeeProviderGender;
                    // singleuserinfo.CanSeeProviderAge                   = userinfo.CanSeeProviderAge;
                    // singleuserinfo.CanSeeProviderReviews               = userinfo.CanSeeProviderReviews;
                    // singleuserinfo.CanEditProviderReviews              = userinfo.CanEditProviderReviews;
                    // singleuserinfo.HideProviderName                    = userinfo.HideProviderName;
                    // singleuserinfo.CanManageServices                   = userinfo.CanManageServices;
                    // singleuserinfo.CanCancelService                    = userinfo.CanCancelService;
                    // singleuserinfo.CanQueryService                     = userinfo.CanQueryService;

                    //main menu form
                    // singleuserinfo.MMPublishPrintRosters                  = userinfo.MMPublishPrintRosters;
                    // singleuserinfo.MMTimesheetProcessing                  = userinfo.MMTimesheetProcessing;
                    // singleuserinfo.MMBilling                              = userinfo.MMBilling;
                    // singleuserinfo.MMPriceUpdates                         = userinfo.MMPriceUpdates;
                    // singleuserinfo.MMDexUploads                           = userinfo.MMDexUploads;
                    // singleuserinfo.MMNDIA                                 = userinfo.MMNDIA;
                    // singleuserinfo.MMHacc                                 = userinfo.MMHacc;
                    // singleuserinfo.MMOtherDS                              = userinfo.MMOtherDS;
                    // singleuserinfo.MMAccounting                           = userinfo.MMAccounting;
                    // singleuserinfo.MMAnalyseBudget                        = userinfo.MMAnalyseBudget;
                    // singleuserinfo.MMAtAGlance                            = userinfo.MMAtAGlance;
                    
                    //mobile and TA

                        // singleuserinfo.AllowTravelEntry= userinfo.AllowTravelEntry;
                        // singleuserinfo.AllowLeaveEntry= userinfo.AllowLeaveEntry;

                        // singleuserinfo.AllowIncidentEntry= userinfo.AllowIncidentEntry;
                        // singleuserinfo.AllowPicUpload= userinfo.AllowPicUpload;
                        // singleuserinfo.EnableRosterAvailability= userinfo.AllowPicUpload;
                        // singleuserinfo.AllowViewBookings= userinfo.AllowPicUpload;
                        // singleuserinfo.AcceptBookings= userinfo.AcceptBookings;
                        // singleuserinfo.ViewClientDocuments= userinfo.ViewClientDocuments;
                        // singleuserinfo.ViewClientCareplans= userinfo.ViewClientCareplans;
                        // singleuserinfo.AllowViewGoalPlans= userinfo.AllowViewGoalPlans;
                        // singleuserinfo.AllowTravelClaimWithoutNote= userinfo.AllowTravelClaimWithoutNote;
                        // singleuserinfo.AllowMTASaveUserPass= userinfo.AllowMTASaveUserPass;

                        // singleuserinfo.AllowOPNote= userinfo.AllowOPNote;
                        // singleuserinfo.AllowCaseNote= userinfo.AllowCaseNote;
                        // singleuserinfo.AllowClinicalNoteEntry= userinfo.AllowClinicalNoteEntry;
                        // singleuserinfo.AllowRosterNoteEntry= userinfo.AllowRosterNoteEntry;
                        // singleuserinfo.SuppressEmailOnRosterNote= userinfo.SuppressEmailOnRosterNote;
                        // singleuserinfo.EnableEmailNotification= userinfo.EnableEmailNotification;
                        // singleuserinfo.UseOPNoteAsShiftReport= userinfo.UseOPNoteAsShiftReport;
                        // singleuserinfo.UseServiceNoteAsShiftReport= userinfo.UseServiceNoteAsShiftReport;
                        // singleuserinfo.EnableViewNoteCases= userinfo.EnableViewNoteCases;
                        // singleuserinfo.ShiftReportReminder= userinfo.ShiftReportReminder;
                        // singleuserinfo.UserSessionLimit= userinfo.UserSessionLimit;
                        // singleuserinfo.MobileFutureLimit= userinfo.MobileFutureLimit;
                        // singleuserinfo.TMMode= userinfo.TMMode;
                        // singleuserinfo.MTAAutRefreshOnLogin= userinfo.MTAAutRefreshOnLogin;
                        // singleuserinfo.HideClientPhoneInApp= userinfo.HideClientPhoneInApp;
                        // singleuserinfo.HideAddress= userinfo.HideAddress;
                        // singleuserinfo.AllowSetTime= userinfo.AllowSetTime;
                        
                        // singleuserinfo.AllowAddAttendee= userinfo.AllowAddAttendee;
                        // singleuserinfo.MultishiftAdminAndMultiple= userinfo.MultishiftAdminAndMultiple;
                        // singleuserinfo.RestrictTravelSameDay= userinfo.RestrictTravelSameDay;
                        // singleuserinfo.PushPhonePrefix= userinfo.PushPhonePrefix;
                        // singleuserinfo.PhonePrefix= userinfo.PhonePrefix;
                        // singleuserinfo.Enable_Shift_End_Alarm= userinfo.Enable_Shift_End_Alarm;
                        // singleuserinfo.Enable_Shift_Start_Alarm= userinfo.Enable_Shift_Start_Alarm;
                        // singleuserinfo.CheckAlertInterval= userinfo.CheckAlertInterval;

                    //singleuserinfo.HidePortalBalance = userinfo.HidePortalBalance.HasValue && userinfo.HidePortalBalance.Value ? userinfo.HidePortalBalance :  false;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        

        
        [HttpPost("userDetailsGeneral")]
        public async Task<IActionResult> saveUserDetailsGeneral([FromBody] UserDetailGeneral userinfo){
            
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO Userinfo(Name,[Usertype],StaffCode,[loginMode],[homeBranch],[system]]) 
                        VALUES(@name,@usertype,@staffCode,@loginMode,@homeBranch,@system);SELECT SCOPE_IDENTITY();", (SqlConnection)conn, transaction))
                    {
                        // INSERT INTO Userinfo(Name,[Usertype],StaffCode,[loginMode],[homeBranch],[system],[Recipients],[Staff],
                        // [Roster],[DayManager],[timesheetPreviousPeriod],[timesheet],[statistics],[financial],[reportPreview],[invoiceEnquiry],[allowTypeAhead],[suggestedTimesheets],[payIntegrityCheck],[timesheetUpdate],[accessCDC]) 
                        // VALUES(@name,@usertype,@staffCode,@loginMode,@homeBranch,@system,@Recipients,
                        // @staff,@roster,@dayManager,@timesheetPreviousPeriod,@timesheet,@statistics,
                        // @financial,@reportPreview,@invoiceEnquiry,@allowTypeAhead,@suggestedTimesheets,@payIntegrityCheck,@timesheetUpdate,@accessCDC);SELECT SCOPE_IDENTITY();



                        cmd.Parameters.AddWithValue("@name", userinfo.name.ToUpper());
                        cmd.Parameters.AddWithValue("@usertype", userinfo.usertype);
                        cmd.Parameters.AddWithValue("@staffCode", userinfo.staffCode);
                        cmd.Parameters.AddWithValue("@loginMode", userinfo.loginMode);
                        cmd.Parameters.AddWithValue("@homeBranch", userinfo.homeBranch);
                        cmd.Parameters.AddWithValue("@system", userinfo.system);
                        
                        // cmd.Parameters.AddWithValue("@Recipients", userinfo.Recipients);
                        // cmd.Parameters.AddWithValue("@staff", userinfo.Staff);
                        // cmd.Parameters.AddWithValue("@roster", userinfo.Roster);
                        // cmd.Parameters.AddWithValue("@dayManager", userinfo.DayManager);
                        // cmd.Parameters.AddWithValue("@timesheetPreviousPeriod", userinfo.timesheetPreviousPeriod);
                        // cmd.Parameters.AddWithValue("@timesheet", userinfo.timesheet);
                        // cmd.Parameters.AddWithValue("@statistics", userinfo.statistics);
                        // cmd.Parameters.AddWithValue("@financial", userinfo.financial);
                        // cmd.Parameters.AddWithValue("@reportPreview", userinfo.reportPreview);
                        // cmd.Parameters.AddWithValue("@invoiceEnquiry", userinfo.invoiceEnquiry);
                        // cmd.Parameters.AddWithValue("@allowTypeAhead", userinfo.allowTypeAhead);
                        // cmd.Parameters.AddWithValue("@suggestedTimesheets", userinfo.suggestedTimesheets);
                        // cmd.Parameters.AddWithValue("@payIntegrityCheck", userinfo.payIntegrityCheck);
                        // cmd.Parameters.AddWithValue("@timesheetUpdate", userinfo.timesheetUpdate);
                        // cmd.Parameters.AddWithValue("@accessCDC", userinfo.accessCDC);

                        Convert.ToInt32((decimal)cmd.ExecuteScalar());
                    }
                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }

            }
        }

        [HttpPut("userDetailsGeneral")]
        public async Task<IActionResult> UpdateUserDetailsGeneral([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    //General

                    singleuserinfo.Name           =   userinfo.Name;
                    singleuserinfo.Usertype       =   userinfo.Usertype;
                    singleuserinfo.StaffCode      =   userinfo.StaffCode;
                    singleuserinfo.LoginMode      =   userinfo.LoginMode;
                    singleuserinfo.homeBranch     =   userinfo.homeBranch;
                    
                    singleuserinfo.System         =   userinfo.System;

                    singleuserinfo.Recipients     =   userinfo.Recipients;
                    singleuserinfo.Staff          =   userinfo.Staff;
                    singleuserinfo.Roster         =   userinfo.Roster;
                    singleuserinfo.DayManager     =   userinfo.DayManager;

                    singleuserinfo.TimesheetPreviousPeriod = userinfo.TimesheetPreviousPeriod;
                    singleuserinfo.Timesheet =   userinfo.Timesheet;
                    singleuserinfo.Statistics          =   userinfo.Statistics;
                    singleuserinfo.Financial =   userinfo.Financial;
                    singleuserinfo.reportPreview       =   userinfo.reportPreview;
                    singleuserinfo.InvoiceEnquiry =   userinfo.InvoiceEnquiry;
                    singleuserinfo.AllowTypeAhead      =   userinfo.AllowTypeAhead;
                    singleuserinfo.SuggestedTimesheets =   userinfo.SuggestedTimesheets;
                    singleuserinfo.PayIntegrityCheck   =   userinfo.PayIntegrityCheck;
                    singleuserinfo.TimesheetUpdate =   userinfo.TimesheetUpdate;
                    singleuserinfo.AccessCDC           =   userinfo.AccessCDC;
                    
                
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        
        
        
        
        
        [HttpPut("userDetailsRecipients")]
        public async Task<IActionResult> UpdateUserDetailsRecipients([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    //recipients Tab
                    singleuserinfo.RecipientRecordView      = userinfo.RecipientRecordView;
                    singleuserinfo.DisableBtnReferral       = userinfo.DisableBtnReferral;
                    singleuserinfo.DisableBtnReferOn        = userinfo.DisableBtnReferOn;
                    singleuserinfo.DisableBtnReferOn        = userinfo.DisableBtnReferOn;
                    singleuserinfo.DisableBtnNotProceeding  = userinfo.DisableBtnNotProceeding;
                    singleuserinfo.DisableBtnAssess         = userinfo.DisableBtnAssess;
                    singleuserinfo.DisableBtnAdmit          = userinfo.DisableBtnAdmit;
                    singleuserinfo.DisableBtnUnWait         = userinfo.DisableBtnUnWait;
                    singleuserinfo.DisableBtnDischarge      = userinfo.DisableBtnDischarge;
                    singleuserinfo.DisableBtnReinstate      = userinfo.DisableBtnReinstate;
                    singleuserinfo.DisableBtnDecease        = userinfo.DisableBtnDecease;
                    singleuserinfo.DisableBtnAdmin          = userinfo.DisableBtnAdmin;
                    singleuserinfo.DisableBtnItem           = userinfo.DisableBtnItem;
                    singleuserinfo.DisableBtnPrint          = userinfo.DisableBtnPrint;
                    singleuserinfo.DisableBtnRoster         = userinfo.DisableBtnRoster;
                    singleuserinfo.DisableBtnMaster         = userinfo.DisableBtnMaster;
                    singleuserinfo.DisableBtnOnHold         = userinfo.DisableBtnOnHold;
                    singleuserinfo.AddNewRecipient          = userinfo.AddNewRecipient;
                    singleuserinfo.CanChangeClientCode      = userinfo.CanChangeClientCode;
                    singleuserinfo.CanEditNDIA              = userinfo.CanEditNDIA;
                    singleuserinfo.AllowProgramTransition   = userinfo.AllowProgramTransition;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpPut("userDetailsStaff")]
        public async Task<IActionResult> UpdateUserDetailsStaff([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    //recipients Tab
                    singleuserinfo.StaffRecordView      = userinfo.StaffRecordView;
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
       
        [HttpPut("userDetailsDocuments")]
        public async Task<IActionResult> UpdateUserDetailsDocuments([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    

                    //Documents Tab
                    singleuserinfo.CanMoveImportedDocuments      = userinfo.CanMoveImportedDocuments;
                    singleuserinfo.KeepOriginalAsImport          = userinfo.KeepOriginalAsImport;
                    singleuserinfo.RecipientDocFolder            = userinfo.RecipientDocFolder;                    
                    singleuserinfo.Force_RecipDocFolder          = userinfo.Force_RecipDocFolder;
                    singleuserinfo.ONIImportExportFolder         = userinfo.ONIImportExportFolder;
                    singleuserinfo.Force_ONIImportFolder         = userinfo.Force_ONIImportFolder;
                    singleuserinfo.ONIArchiveFolder              = userinfo.ONIArchiveFolder;
                    singleuserinfo.Force_ONIArchiveFolder        = userinfo.Force_ONIArchiveFolder;
                    singleuserinfo.StaffDocFolder                = userinfo.StaffDocFolder;
                    singleuserinfo.Force_StaffDocFolder          = userinfo.Force_StaffDocFolder;
                    singleuserinfo.StaffRostersFolder            = userinfo.StaffRostersFolder;
                    singleuserinfo.Force_StaffRosterFolder       = userinfo.Force_StaffRosterFolder;
                    singleuserinfo.ReportExportFolder            = userinfo.ReportExportFolder;
                    singleuserinfo.Force_ReportExportFolder      = userinfo.Force_ReportExportFolder;
                    singleuserinfo.ReportSavesFolder             = userinfo.ReportSavesFolder;
                    singleuserinfo.Force_ReportSavesFolder       = userinfo.Force_ReportSavesFolder;
                    singleuserinfo.HACCMDSFolder                 = userinfo.HACCMDSFolder;
                    singleuserinfo.Force_HACCMDSFolder           = userinfo.Force_HACCMDSFolder;
                    singleuserinfo.Force_CSTDAMDSFolder          = userinfo.Force_CSTDAMDSFolder;
                    singleuserinfo.NRCPMDSFolder                 = userinfo.NRCPMDSFolder;
                    singleuserinfo.Force_NRCPMDSFolder           = userinfo.Force_NRCPMDSFolder;
                    singleuserinfo.PayExportFolder               = userinfo.PayExportFolder;
                    singleuserinfo.Force_PayExportFolder         = userinfo.Force_PayExportFolder;
                    singleuserinfo.BillingExportFolder           = userinfo.BillingExportFolder;
                    singleuserinfo.Force_BillingExportFolder     = userinfo.Force_BillingExportFolder;
                    

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpPut("userDetailsRoster")]
        public async Task<IActionResult> UpdateUserDetailsRoster([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    //roster form 
                    
                    singleuserinfo.ChangeMasterRoster                = userinfo.ChangeMasterRoster;
                    singleuserinfo.AllowRosterReallocate             = userinfo.AllowRosterReallocate;
                    singleuserinfo.AllowMasterSaveAs                 = userinfo.AllowMasterSaveAs;
                    singleuserinfo.ManualRosterCopy                  = userinfo.ManualRosterCopy;
                    singleuserinfo.AutoCopyRoster                    = userinfo.AutoCopyRoster;
                    singleuserinfo.CanRosterOvertime                 = userinfo.CanRosterOvertime;
                    singleuserinfo.CanRosterBreakless                = userinfo.CanRosterBreakless;
                    singleuserinfo.CanRosterConflicts                = userinfo.CanRosterConflicts;
                    singleuserinfo.EditRosterRecord                  = userinfo.EditRosterRecord;
                    singleuserinfo.OwnRosterOnly                     = userinfo.OwnRosterOnly;
                    
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpPut("userDetailsDayManager")]
        public async Task<IActionResult> UpdateUserDetailsDayManager([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    // day manager form 

                    singleuserinfo.UseDMv2                          = userinfo.UseDMv2;
                    singleuserinfo.APPROVEDAYMANAGER                = userinfo.APPROVEDAYMANAGER;
                    singleuserinfo.RECIPMGTVIEW                     = userinfo.RECIPMGTVIEW;
                    singleuserinfo.AllowStaffSwap                   = userinfo.AllowStaffSwap;
                    singleuserinfo.AdminChangeOutputType            = userinfo.AdminChangeOutputType;
                    singleuserinfo.AdminChangeActivityCode          = userinfo.AdminChangeActivityCode;
                    singleuserinfo.AdminChangePaytype               = userinfo.AdminChangePaytype;
                    singleuserinfo.AdminChangeDebtor                = userinfo.AdminChangeDebtor;
                    singleuserinfo.AdminChangeBillAmount            = userinfo.AdminChangeBillAmount;
                    singleuserinfo.adminChangeBillQty               = userinfo.adminChangeBillQty;
                    singleuserinfo.AttendeesChangeBillAmount        = userinfo.AttendeesChangeBillAmount;
                    singleuserinfo.AttendeesChangeBillQty           = userinfo.AttendeesChangeBillQty;
                    singleuserinfo.AdminChangePayQty                = userinfo.AdminChangePayQty;
                    singleuserinfo.LowChangeActivityCode            = userinfo.LowChangeActivityCode;
                
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpPut("userDetailsClientPortal")]
        public async Task<IActionResult> UpdateUserDetailsClientPortal([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                    // clientportal form 

                    singleuserinfo.AllowsMarketing                     = userinfo.AllowsMarketing;
                    singleuserinfo.ViewPackageStatement                = userinfo.ViewPackageStatement;
                    singleuserinfo.CanManagePreferences                = userinfo.CanManagePreferences;
                    singleuserinfo.AllowBooking                        = userinfo.AllowBooking;
                    singleuserinfo.CanCreateBooking                    = userinfo.CanCreateBooking;
                    singleuserinfo.BookingLeadTime                     = userinfo.BookingLeadTime;
                    singleuserinfo.CanChooseProvider                   = userinfo.CanChooseProvider;
                    singleuserinfo.ShowProviderPhoto                   = userinfo.AdminChangeDebtor;
                    singleuserinfo.CanSeeProviderPhoto                 = userinfo.CanSeeProviderPhoto;
                    singleuserinfo.CanSeeProviderGender                = userinfo.CanSeeProviderGender;
                    singleuserinfo.CanSeeProviderAge                   = userinfo.CanSeeProviderAge;
                    singleuserinfo.CanSeeProviderReviews               = userinfo.CanSeeProviderReviews;
                    singleuserinfo.CanEditProviderReviews              = userinfo.CanEditProviderReviews;
                    singleuserinfo.HideProviderName                    = userinfo.HideProviderName;
                    singleuserinfo.CanManageServices                   = userinfo.CanManageServices;
                    singleuserinfo.CanCancelService                    = userinfo.CanCancelService;
                    singleuserinfo.CanQueryService                     = userinfo.CanQueryService;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpPut("userDetailsMobile")]
        public async Task<IActionResult> UpdateUserDetailsMobile([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                        //mobile and TA

                        singleuserinfo.AllowTravelEntry= userinfo.AllowTravelEntry;
                        singleuserinfo.AllowLeaveEntry= userinfo.AllowLeaveEntry;

                        singleuserinfo.AllowIncidentEntry= userinfo.AllowIncidentEntry;
                        singleuserinfo.AllowPicUpload= userinfo.AllowPicUpload;
                        singleuserinfo.EnableRosterAvailability= userinfo.AllowPicUpload;
                        singleuserinfo.AllowViewBookings= userinfo.AllowPicUpload;
                        singleuserinfo.AcceptBookings= userinfo.AcceptBookings;
                        singleuserinfo.ViewClientDocuments= userinfo.ViewClientDocuments;
                        singleuserinfo.ViewClientCareplans= userinfo.ViewClientCareplans;
                        singleuserinfo.AllowViewGoalPlans= userinfo.AllowViewGoalPlans;
                        singleuserinfo.AllowTravelClaimWithoutNote= userinfo.AllowTravelClaimWithoutNote;
                        singleuserinfo.AllowMTASaveUserPass= userinfo.AllowMTASaveUserPass;

                        singleuserinfo.AllowOPNote= userinfo.AllowOPNote;
                        singleuserinfo.AllowCaseNote= userinfo.AllowCaseNote;
                        singleuserinfo.AllowClinicalNoteEntry= userinfo.AllowClinicalNoteEntry;
                        singleuserinfo.AllowRosterNoteEntry= userinfo.AllowRosterNoteEntry;
                        singleuserinfo.SuppressEmailOnRosterNote= userinfo.SuppressEmailOnRosterNote;
                        singleuserinfo.EnableEmailNotification= userinfo.EnableEmailNotification;
                        singleuserinfo.UseOPNoteAsShiftReport= userinfo.UseOPNoteAsShiftReport;
                        singleuserinfo.UseServiceNoteAsShiftReport= userinfo.UseServiceNoteAsShiftReport;
                        singleuserinfo.EnableViewNoteCases= userinfo.EnableViewNoteCases;
                        singleuserinfo.ShiftReportReminder= userinfo.ShiftReportReminder;
                        singleuserinfo.UserSessionLimit= userinfo.UserSessionLimit;
                        singleuserinfo.MobileFutureLimit= userinfo.MobileFutureLimit;
                        singleuserinfo.TMMode= userinfo.TMMode;
                        singleuserinfo.MTAAutRefreshOnLogin= userinfo.MTAAutRefreshOnLogin;
                        singleuserinfo.HideClientPhoneInApp= userinfo.HideClientPhoneInApp;
                        singleuserinfo.HideAddress= userinfo.HideAddress;
                        singleuserinfo.AllowSetTime= userinfo.AllowSetTime;
                        
                        singleuserinfo.AllowAddAttendee= userinfo.AllowAddAttendee;
                        singleuserinfo.MultishiftAdminAndMultiple= userinfo.MultishiftAdminAndMultiple;
                        singleuserinfo.RestrictTravelSameDay= userinfo.RestrictTravelSameDay;
                        singleuserinfo.PushPhonePrefix= userinfo.PushPhonePrefix;
                        singleuserinfo.PhonePrefix= userinfo.PhonePrefix;
                        singleuserinfo.Enable_Shift_End_Alarm= userinfo.Enable_Shift_End_Alarm;
                        singleuserinfo.Enable_Shift_Start_Alarm= userinfo.Enable_Shift_Start_Alarm;
                        singleuserinfo.CheckAlertInterval= userinfo.CheckAlertInterval;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpPut("userDetailsMainMenuForm")]
        public async Task<IActionResult> UpdateUserDetailsMainMenuForm([FromBody] UserInfo userinfo)
        {
            try
            {
                using (var db = _context)
                {
                    var singleuserinfo = (from rp in _context.UserInfo
                                          where rp.Recnum == userinfo.Recnum
                                    select rp).FirstOrDefault();

                    
                    if (singleuserinfo == null) return BadRequest();
                    
                   
                    //main menu form

                    singleuserinfo.MMPublishPrintRosters                  = userinfo.MMPublishPrintRosters;
                    singleuserinfo.MMTimesheetProcessing                  = userinfo.MMTimesheetProcessing;
                    singleuserinfo.MMBilling                              = userinfo.MMBilling;
                    singleuserinfo.MMPriceUpdates                         = userinfo.MMPriceUpdates;
                    singleuserinfo.MMDexUploads                           = userinfo.MMDexUploads;
                    singleuserinfo.MMNDIA                                 = userinfo.MMNDIA;
                    singleuserinfo.MMHacc                                 = userinfo.MMHacc;
                    singleuserinfo.MMOtherDS                              = userinfo.MMOtherDS;
                    singleuserinfo.MMAccounting                           = userinfo.MMAccounting;
                    singleuserinfo.MMAnalyseBudget                        = userinfo.MMAnalyseBudget;
                    singleuserinfo.MMAtAGlance                            = userinfo.MMAtAGlance;
                    
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
       

        [HttpGet("agencyRegistartion")]
        public async Task<IActionResult> agencyRegistartion()
        {
            
                    var registration = await (from reg in _context.Registration select reg).FirstOrDefaultAsync();
                    
                    return Ok(registration);
        }

         [HttpPut("agencyRegistartion")]
        public async Task<IActionResult> UpdateAgencyRegistartion([FromBody] Registration activities)
        {




            if (!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                                    // Log validation errors
                        foreach (var error in errors)
                        {
                            // Log or handle the error as needed
                             var fieldName = error.Field; // This will give you the name of the property causing the validation error
                             var errorMessage = error.Error; // This will give you the error message
                            // Log or handle the error as needed
                             Console.WriteLine($"Field: {fieldName}, Error: {errorMessage}");
                        }
                return BadRequest(errors);
            }
            
            try
            {
                using (var db = _context)
                {
                    
                    var agency = (from rp in _context.Registration
                                    where rp.SqlId == activities.SqlId
                                    select rp).FirstOrDefault();
                    
                    if (agency == null) return BadRequest();

                    agency.coName           =   activities.coName;
                    agency.coPhone2         =   activities.coPhone2;
                    agency.ACN              =   activities.ACN;
                    agency.coPhone1         =   activities.coPhone1;
                    agency.coFax            =   activities.coFax;
                    agency.coMobile         =   activities.coMobile;
                    agency.coAddress1       =   activities.coAddress1;
                    agency.coEmail          =   activities.coEmail;
                    agency.coAddress2       =   activities.coAddress2;
                    agency.coAddress3       =   activities.coAddress3;
                    agency.coSuburb         =   activities.coSuburb;
                    agency.coPostCode       =   activities.coPostCode;
                    agency.prodDesc         =   activities.prodDesc;
                    agency.DOHTransportProviderID       =   activities.DOHTransportProviderID;
                    agency.logoFile             =   activities.logoFile;
                    agency.bankName             =   activities.bankName;
                    agency.bpaybillercode       =   activities.bpaybillercode;
                    agency.bankBSB              =   activities.bankBSB;
                    agency.bankAccountName      =   activities.bankAccountName;
                    agency.bankAccountNumber    =   activities.bankAccountNumber;
                    agency.CSTDAContact         =   activities.CSTDAContact;
                    agency.prodVer              =   activities.prodVer;
                    agency.DoesWADSC            =   activities.DoesWADSC;
                    agency.reg_contact_position = activities.reg_contact_position;
                    agency.CSTDAEmail    = activities.CSTDAEmail;
                    agency.DSCUserName   = activities.DSCUserName;
                    agency.DSCPassword   = activities.DSCPassword;
                    
                    agency.SQLVersion                   = activities.SQLVersion;
                    agency.ODBCTimeout                  = activities.ODBCTimeout;
                    agency.auditTriggers                = activities.auditTriggers;
                    agency.SessionCheck                 = activities.SessionCheck;                 
                    agency.brokerageRequestModule       = activities.brokerageRequestModule;
                    agency.TRACCS_MAX_SESSIONS          = activities.TRACCS_MAX_SESSIONS;
                    agency.RECIPIENT_MAX_SESSIONS       = activities.RECIPIENT_MAX_SESSIONS;
                    agency.STAFF_MAX_SESSIONS           = activities.STAFF_MAX_SESSIONS;
                    agency.ROSTERS_MAX_SESSIONS         = activities.ROSTERS_MAX_SESSIONS;
                    agency.DAYMANAGER_MAX_SESSIONS      = activities.DAYMANAGER_MAX_SESSIONS;
                    agency.TIMESHEET_MAX_SESSIONS       = activities.TIMESHEET_MAX_SESSIONS;
                    agency.REPORTS_MAX_SESSIONS         = activities.REPORTS_MAX_SESSIONS;
                    agency.AllowCDCMultiClaim           = activities.AllowCDCMultiClaim;

                    agency.ForceRecipientOptions        = activities.ForceRecipientOptions;
                    agency.AccountWidth                 = activities.AccountWidth; 
                    agency.AccountingIDgeneration       = activities.AccountingIDgeneration;
                    agency.LegacyCareplanLabel          = activities.LegacyCareplanLabel;
                    agency.WizardDefaultNote            = activities.WizardDefaultNote;
                    agency.AutoExportCaseNotes          = activities.AutoExportCaseNotes;
                    agency.AutoExportCaseNotesFolder    = activities.AutoExportCaseNotesFolder;
                    agency.UppercaseSurname             = activities.UppercaseSurname;
                    agency.AutoGenerateFileNumber       = activities.AutoGenerateFileNumber;
                    agency.DisplayAllRecipients         = activities.DisplayAllRecipients;
                    agency.ONIArchive                   = activities.ONIArchive;
                    agency.ForceCaseNote1               = activities.ForceCaseNote1;
                    agency.ForceSuspendReminder         = activities.ForceSuspendReminder;
                    agency.AutoExportONI                = activities.AutoExportONI;
                    agency.PrintONIOnReferral           = activities.PrintONIOnReferral;
                    agency.CustomPkgBudgetFormat        = activities.CustomPkgBudgetFormat;
                    agency.CustomPkgStatementFormat     = activities.CustomPkgStatementFormat;
                    agency.DisplaySaveWarningInRecipients = activities.DisplaySaveWarningInRecipients;
                    agency.CaseNotesWarning             = activities.CaseNotesWarning;
                    agency.CaseNotesSecurityCheck       = activities.CaseNotesSecurityCheck;
                    agency.StoreNameAsCreator           = activities.StoreNameAsCreator;
                    agency.IncludeReferralsInActive     = activities.IncludeReferralsInActive;
                    agency.IncludeWaitingListInActive   = activities.IncludeWaitingListInActive;
                    agency.RestrictReferrals            = activities.RestrictReferrals;

                    agency.IMStyleRecip                 = activities.IMStyleRecip;
                    agency.IMStyleStaff                 = activities.IMStyleStaff;

                    agency.EnforceActivityLimits        = activities.EnforceActivityLimits;
                    agency.RosterAwardEnforcement       = activities.RosterAwardEnforcement;
                    agency.IncrementalRosterCreation    = activities.IncrementalRosterCreation;
                    agency.IncrementalCompetencyAction  = activities.IncrementalCompetencyAction;
                    agency.UseNewDayManager             = activities.UseNewDayManager;
                    agency.MealLabelAgencyName          = activities.MealLabelAgencyName;
                    agency.DotPointXtraInfo             = activities.DotPointXtraInfo;
                    agency.nmCycle1                     = activities.nmCycle1;
                    agency.nmCycle2                     = activities.nmCycle2;
                    agency.nmCycle3                     = activities.nmCycle3;
                    agency.nmCycle4                     = activities.nmCycle4;
                    agency.nmCycle5                     = activities.nmCycle5;
                    agency.nmCycle6                     = activities.nmCycle6;
                    agency.nmCycle7                     = activities.nmCycle7;
                    agency.nmCycle8                     = activities.nmCycle8;
                    agency.nmCycle9                     = activities.nmCycle9;
                    agency.nmCycle10                    = activities.nmCycle10;
                    agency.RosterEmail                  = activities.RosterEmail;

                    agency.DefaultPayPeriod             = activities.DefaultPayPeriod;
                    agency.DefaultPayStartDay           = activities.DefaultPayStartDay;
                    agency.CompetencyEnforcement        = activities.CompetencyEnforcement;
                    agency.payPrimacy                   = activities.payPrimacy;
                    agency.KeepPayOnAllocate            = activities.KeepPayOnAllocate;
                    agency.AutoAutoApproveRecipientAdmin= activities.AutoAutoApproveRecipientAdmin;
                    agency.DisableDayManagerNotes       = activities.DisableDayManagerNotes;
                    agency.UseAwards                    = activities.UseAwards;
                    agency.UsePositions                 = activities.UsePositions;
                    agency.NewTimesheet                 = activities.NewTimesheet;
                    agency.TimesheetIsRestricted        = activities.TimesheetIsRestricted;
                    agency.googletravel                 = activities.googletravel;
                    agency.TravelProvider               = activities.TravelProvider;
                    agency.ProviderKey                  = activities.ProviderKey;

                    agency.BillingMethod                = activities.BillingMethod;
                    agency.CustomInvoiceFormat          = activities.CustomInvoiceFormat;
                    agency.InclAgedSumInvoice           = activities.InclAgedSumInvoice;
                    agency.Bill_DirectDebit             = activities.Bill_DirectDebit;
                    agency.Bill_OnSiteDetails           = activities.Bill_OnSiteDetails;
                    agency.Bill_MailToDetails           = activities.Bill_MailToDetails;
                    agency.Bill_ByPhoneDetails          = activities.Bill_ByPhoneDetails;
                    agency.EmailInvoiceSubjectSetting   = activities.EmailInvoiceSubjectSetting;
                    agency.EmailInvoiceSubjectText      = activities.EmailInvoiceSubjectText;

                    agency.ShortNoticeThreshold         =activities.ShortNoticeThreshold;
                    agency.NoNoticeThreshold            =activities.NoNoticeThreshold;
                    agency.MinimumCancellationLeadTime  =activities.MinimumCancellationLeadTime;
                    agency.BookingLeadTime              =activities.BookingLeadTime;
                    agency.CancellationFeeText          =activities.CancellationFeeText;
                    agency.ClientPortalManagerMethod    =activities.ClientPortalManagerMethod;

                    agency.POP3Server=activities.POP3Server;
                    agency.POP3User=activities.POP3User;
                    agency.POP3Password=activities.POP3Password;
                    agency.SMTPServer=activities.SMTPServer;
                    agency.SMTPUser=activities.SMTPUser;
                    agency.SMTPPassword=activities.SMTPPassword;
                    agency.IMEmailAddress=activities.IMEmailAddress;
                    agency.IMEmailDisplay=activities.IMEmailDisplay;
                    agency.HelpDeskEmail=activities.HelpDeskEmail;
                    agency.DefaultInvoiceEmailSubject=activities.DefaultInvoiceEmailSubject;
                    agency.smsPassword=activities.smsPassword;
                    agency.eMailMode=activities.eMailMode;
                    agency.OutlookPath=activities.OutlookPath;

                    agency.MinimumInternetSpeedForOnline=activities.MinimumInternetSpeedForOnline;
                    agency.MobileGEOCodeLimit=activities.MobileGEOCodeLimit;
                    agency.StaffLocationUpdateInterval=activities.StaffLocationUpdateInterval;
                    agency.MobilUserURL=activities.MobilUserURL;
                    agency.WebKioskUserURL=activities.WebKioskUserURL;
                    agency.LockWebkiosk=activities.LockWebkiosk;
                    agency.TA_T1=activities.TA_T1;
                    agency.TA_T2=activities.TA_T2;
                    agency.TA_T3=activities.TA_T3;
                    agency.ShowClientPhoneInApp=activities.ShowClientPhoneInApp;
                    agency.TA_TRAVELDEFAULT=activities.TA_TRAVELDEFAULT;
                    agency.TA_ForceNoteOnForcedLogOnOff=activities.TA_ForceNoteOnForcedLogOnOff;
                    agency.DefaultAppNoteCategory=activities.DefaultAppNoteCategory;
                    agency.TA_ApproveWithoutCorrection=activities.TA_ApproveWithoutCorrection;


                    agency.NDIATravel = activities.NDIATravel;
                    agency.NDIACancelFeePercent = activities.NDIACancelFeePercent;

                    agency.Pension = activities.Pension;
                    agency.PensionCoupleEach = activities.PensionCoupleEach;
                    agency.PensionCoupleCombined = activities.PensionCoupleCombined;
                    agency.PensionCoupleEachIll = activities.PensionCoupleEachIll;
                    agency.BasicFeePercent = activities.BasicFeePercent;
                    agency.UseMedicareImport = activities.UseMedicareImport;
                    agency.AllowChangeIncomeTestedFee = activities.AllowChangeIncomeTestedFee;


                    agency.StaffLeaveEmailOverrides = activities.StaffLeaveEmailOverrides;
                    agency.StaffLeaveEmail = activities.StaffLeaveEmail;
                    agency.ClientNoteEmailOverrides = activities.ClientNoteEmailOverrides;
                    agency.ClientNoteEmail = activities.ClientNoteEmail;

                    agency.DocusignServerPort = activities.DocusignServerPort;
                    agency.DocusignServerRef  = activities.DocusignServerRef;


                    
                    agency.PAN_TANoShowTH = activities.PAN_TANoShowTH;
                    agency.PAN_TANoGoResend = activities.PAN_TANoGoResend;
                    agency.PAN_TAEarlyStartTH = activities.PAN_TAEarlyStartTH;
                    agency.PAN_TALateStartTH = activities.PAN_TALateStartTH;
                    agency.PAN_TAEarlyFinishTH = activities.PAN_TAEarlyFinishTH;
                    agency.PAN_TALateFinishTH = activities.PAN_TALateFinishTH;
                    agency.PAN_TAOverstayTH = activities.PAN_TAOverstayTH;
                    agency.PAN_TAUnderstayTH = activities.PAN_TAUnderstayTH;
                    agency.PAN_TANoWorkTH = activities.PAN_TANoWorkTH;
                    agency.PAN_TAEarlyStartTHEmail = activities.PAN_TAEarlyStartTHEmail;
                    agency.PAN_TAEarlyStartTHSMS = activities.PAN_TAEarlyStartTHSMS;
                    agency.PAN_TAEarlyStartTHWho = activities.PAN_TAEarlyStartTHWho;
                    agency.PAN_TAEarlyFinishTHEmail = activities.PAN_TAEarlyFinishTHEmail;
                    agency.PAN_TAEarlyFinishTHSMS = activities.PAN_TAEarlyFinishTHSMS;
                    agency.PAN_TAEarlyFinishTHWho = activities.PAN_TAEarlyFinishTHWho;
                    agency.PAN_TALateStartTHEmail = activities.PAN_TALateStartTHEmail;
                    agency.PAN_TALateFinishTHSMS = activities.PAN_TALateFinishTHSMS;
                    agency.PAN_TALateStartTHWho = activities.PAN_TALateStartTHWho;
                    agency.PAN_TAOverstayTHEmail = activities.PAN_TAOverstayTHEmail;
                    agency.PAN_TAOverstayTHSMS = activities.PAN_TAOverstayTHSMS;
                    agency.PAN_TAOverstayTHWho = activities.PAN_TAOverstayTHWho;
                    agency.PAN_TAUnderstayTHEmail = activities.PAN_TAUnderstayTHEmail;
                    agency.PAN_TAUnderstayTHSMS = activities.PAN_TAUnderstayTHSMS;
                    agency.PAN_TAUnderstayTHWho = activities.PAN_TAUnderstayTHWho;
                    agency.PAN_TANoWorkTHEmail = activities.PAN_TANoWorkTHEmail;
                    agency.PAN_TANoWorkTHSMS = activities.PAN_TANoWorkTHSMS;
                    agency.PAN_TANoWorkTHWho = activities.PAN_TANoWorkTHWho;
                    agency.PanLogFileLocation = activities.PanLogFileLocation;
                    agency.PAN_DMRefreshInterval = activities.PAN_DMRefreshInterval;


                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }

        [HttpGet("companycstdalist")]
        public async Task<IActionResult> comapnycstalist(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, [Name], ServiceOutletID, AddressLine1 + CASE WHEN Suburb is null Then ' ' ELSE ' ' + Suburb END as Address FROM CSTDAOutlets WHERE CSTDA = 1 AND ( EndDate is NULL OR EndDate >= Getdate()) ORDER BY [NAME]";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                RecordNumber          = GenSettings.Filter(rd["RecordNumber"]),
                                Name                  = GenSettings.Filter(rd["Name"]),
                                ServiceOutletID       = GenSettings.Filter(rd["ServiceOutletID"]),
                                Address               = GenSettings.Filter(rd["Address"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }

        // [HttpGet("Services")]
        [HttpGet("staffAdminActivities/{is_where}")]
        public async Task<IActionResult> staffAdminActivities(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE ProcessClassification <> 'INPUT' ";
                }else{
                    query = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE())";
                }

                //SELECT RecordNumber, DESCRIPTION from DATADOMAINS WHERE DOMAIN = 'BRANCHES'
                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Title]) AS Row_num, [Recnum] As [Recnum], [Title] As [Title],CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [RosterGroup],"+
                " [MinorGroup] As [Sub Group],[IT_Dataset] As [DataSet],[HACCType] As [Dataset Code],[CSTDAOutletID] As [OutletID],[DatasetGroup] As [Dataset Group],"+
                " [NDIA_ID] As [NDIA ID],[AccountingIdentifier] As [Accounting Code],[Amount] As [Bill Amount],[Unit] As [Bill Unit],[EndDate] As [End Date],"+
                " [Status],[ProcessClassification],[DataSet] as dicipline,[GLRevenue]," + 
                " [BudgetAmount],[BudgetPeriod],[PayGroup],[PayType],[ItemType],[Price2],[Price3],[Price4],[Price5],[Price6],[MainGroup],[AutoRecipientDetails], " + 
                " [AutoActivityNotes],[DefaultAddress],[DefaultPhone],[ActivityNotes],[GLCost],[UnitCostUOM],[UnitCost],[InfoOnly],[AutoApprove], " + 
                " [ExcludeFromPayExport],[BudgetGroup],[DefaultPayOnLeaveID],[DefaultActivityOnLeaveID],[DefaultProgramOnLeaveID],[BillText],[NoChangeDate], " + 
                " [NoChangeTime],[TimeChangeLimit],[JobType],[ExcludeFromUsageStatements],[TAExclude1],[MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday], " + 
                " [NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday],[NoPubHol],[JobSheetPrompt],[ExcludeFromInterpretation], " + 
                " [ExcludeFromAutoLeave],[CDCFee],[AppExclude1],[TA_LOGINMODE],[TA_EXCLUDEGEOLOCATION],[DEXID],[ExcludeFromTravelCalc],[FixedTime], " + 
                " [NoOvertimeAccumulation],[DMColor],[RetainOriginalDataset],[MinDurtn],[MaxDurtn],[NDIAPriceType],[TAEarlyStartTHWho],[TAEarlyFinishTHWho], " +
                " [TALateFinishTHWho],[TALateStartTHWho],[TAOverstayTHWho],[TAUnderstayTHWho],[TANoWorkTHWho],[TAEarlyStartTH],[TAEarlyFinishTH],[TALateFinishTH], " + 
                " [TALateStartTH],[TAOverstayTH],[TAUnderstayTH],[TANoWorkTH],[TAEarlyStartTHEmail],[TAEarlyFinishTHEmail],[TALateFinishTHEmail],[TALateStartTHEmail], " +
                " [TAOverstayTHEmail],[TAUnderstayTHEmail],[TANoWorkTHEmail],[TANoShowResend],[TANoGoResend],[ExcludeFromHigherPayCalculation],[PayAsRostered], " +
                " [NDIA_LEVEL2],[NDIA_LEVEL3],[NDIA_LEVEL4],[ExcludeFromTimebands],[NDIATravel],[ExcludeFromConflicts],[ExcludeFromClientPortalDisplay],[Job], " + 
                " [Lifecycle],[NDIAClaimType] FROM ItemTypes "+query+" AND (RosterGroup IN ('STAFF ADMINISTRATION','ADMINISTRATION', 'TRAVELTIME','TRAVEL TIME')) ORDER BY Title";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recnum          = GenSettings.Filter(rd["Recnum"]),
                                row_num         = GenSettings.Filter(rd["Row_num"]),
                                title           = GenSettings.Filter(rd["Title"]),
                                billText        = GenSettings.Filter(rd["BillText"]),
                                rosterGroup     = GenSettings.Filter(rd["RosterGroup"]),
                                minorGroup      = GenSettings.Filter(rd["Sub Group"]),//minor group
                                dataSet         = GenSettings.Filter(rd["dicipline"]),
                                accountingIdentifier = GenSettings.Filter(rd["accounting Code"]),
                                amount          = GenSettings.Filter(rd["Bill Amount"]),
                                unit            = GenSettings.Filter(rd["Bill Unit"]),
                                IT_Dataset      = GenSettings.Filter(rd["DataSet"]),
                                haccType        = GenSettings.Filter(rd["Dataset Code"]),
                                status          = GenSettings.Filter(rd["Status"]),
                                datasetGroup    = GenSettings.Filter(rd["Dataset Group"]),
                                endDate = GenSettings.Filter(rd["End Date"]),
                                NDIA_ID = GenSettings.Filter(rd["NDIA ID"]),
                                outletID = GenSettings.Filter(rd["OutletID"]),
                                processClassification = GenSettings.Filter(rd["ProcessClassification"]),
                                glRevenue = GenSettings.Filter(rd["GLRevenue"]),
                                budgetAmount = GenSettings.Filter(rd["BudgetAmount"]),
                                budgetPeriod = GenSettings.Filter(rd["BudgetPeriod"]),
                                payGroup = GenSettings.Filter(rd["PayGroup"]),
                                payType = GenSettings.Filter(rd["PayType"]),
                                itemType = GenSettings.Filter(rd["ItemType"]),
                                price2 = GenSettings.Filter(rd["Price2"]),
                                price3 = GenSettings.Filter(rd["Price3"]),
                                price4 = GenSettings.Filter(rd["Price4"]),
                                price5 = GenSettings.Filter(rd["Price5"]),
                                price6 = GenSettings.Filter(rd["Price6"]),
                                mainGroup = GenSettings.Filter(rd["MainGroup"]),
                                autoRecipientDetails = GenSettings.Filter(rd["AutoRecipientDetails"]),
                                autoActivityNotes = GenSettings.Filter(rd["AutoActivityNotes"]),
                                defaultAddress = GenSettings.Filter(rd["DefaultAddress"]),
                                defaultPhone  = GenSettings.Filter(rd["DefaultPhone"]),
                                activityNotes = GenSettings.Filter(rd["ActivityNotes"]),
                                glCost = GenSettings.Filter(rd["GLCost"]),
                                unitCostUOM = GenSettings.Filter(rd["UnitCostUOM"]),
                                unitCost = GenSettings.Filter(rd["UnitCost"]),
                                infoOnly = GenSettings.Filter(rd["InfoOnly"]),
                                autoApprove = GenSettings.Filter(rd["AutoApprove"]),
                                excludeFromPayExport = GenSettings.Filter(rd["ExcludeFromPayExport"]),
                                budgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
                                defaultPayOnLeaveID = GenSettings.Filter(rd["DefaultPayOnLeaveID"]),
                                defaultActivityOnLeaveID = GenSettings.Filter(rd["DefaultActivityOnLeaveID"]),
                                defaultProgramOnLeaveID = GenSettings.Filter(rd["DefaultProgramOnLeaveID"]),
                                noChangeDate = GenSettings.Filter(rd["NoChangeDate"]),
                                noChangeTime = GenSettings.Filter(rd["NoChangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                jobType  = GenSettings.Filter(rd["JobType"]),
                                excludeFromUsageStatements = GenSettings.Filter(rd["ExcludeFromUsageStatements"]),
                                taexclude1 = GenSettings.Filter(rd["TAExclude1"]),
                                minChargeRate = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday = GenSettings.Filter(rd["NoThursday"]),
                                noFriday = GenSettings.Filter(rd["NoFriday"]),
                                noSunday = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday  = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt = GenSettings.Filter(rd["JobSheetPrompt"]),
                                excludeFromInterpretation = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                excludeFromAutoLeave = GenSettings.Filter(rd["ExcludeFromAutoLeave"]),
                                cdcFee = GenSettings.Filter(rd["CDCFee"]),
                                appExclude1 = GenSettings.Filter(rd["AppExclude1"]),
                                tA_LOGINMODE= GenSettings.Filter(rd["TA_LOGINMODE"]),
                                tA_EXCLUDEGEOLOCATION = GenSettings.Filter(rd["TA_EXCLUDEGEOLOCATION"]),
                                dexid = GenSettings.Filter(rd["DEXID"]),
                                excludeFromTravelCalc = GenSettings.Filter(rd["ExcludeFromTravelCalc"]),
                                fixedTime = GenSettings.Filter(rd["FixedTime"]),
                                noOvertimeAccumulation = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                dmColor = GenSettings.Filter(rd["DMColor"]),
                                retainOriginalDataset = GenSettings.Filter(rd["RetainOriginalDataset"]),
                                minDurtn = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn = GenSettings.Filter(rd["MaxDurtn"]),
                                ndiaPriceType = GenSettings.Filter(rd["NDIAPriceType"]),
                                TAEarlyStartTHWho = GenSettings.Filter(rd["TAEarlyStartTHWho"]),
                                TAEarlyFinishTHWho = GenSettings.Filter(rd["TAEarlyFinishTHWho"]),
                                TALateFinishTHWho = GenSettings.Filter(rd["TALateFinishTHWho"]),
                                TALateStartTHWho = GenSettings.Filter(rd["TALateStartTHWho"]),
                                TAOverstayTHWho = GenSettings.Filter(rd["TAOverstayTHWho"]),
                                TAUnderstayTHWho = GenSettings.Filter(rd["TAUnderstayTHWho"]),
                                TANoWorkTHWho = GenSettings.Filter(rd["TANoWorkTHWho"]),
                                TAEarlyStartTH = GenSettings.Filter(rd["TAEarlyStartTH"]),
                                TAEarlyFinishTH = GenSettings.Filter(rd["TAEarlyFinishTH"]),
                                TALateFinishTH = GenSettings.Filter(rd["TALateFinishTH"]),
                                TALateStartTH = GenSettings.Filter(rd["TALateStartTH"]),
                                TAOverstayTH = GenSettings.Filter(rd["TAOverstayTH"]),
                                TAUnderstayTH = GenSettings.Filter(rd["TAUnderstayTH"]),
                                TANoWorkTH = GenSettings.Filter(rd["TANoWorkTH"]),
                                TAEarlyStartTHEmail = GenSettings.Filter(rd["TAEarlyStartTHEmail"]),
                                TAEarlyFinishTHEmail = GenSettings.Filter(rd["TAEarlyFinishTHEmail"]),
                                TALateFinishTHEmail = GenSettings.Filter(rd["TALateFinishTHEmail"]),
                                TALateStartTHEmail = GenSettings.Filter(rd["TALateStartTHEmail"]),
                                TAOverstayTHEmail = GenSettings.Filter(rd["TAOverstayTHEmail"]),
                                TAUnderstayTHEmail = GenSettings.Filter(rd["TAUnderstayTHEmail"]),
                                TANoWorkTHEmail = GenSettings.Filter(rd["TANoWorkTHEmail"]),
                                TANoShowResend = GenSettings.Filter(rd["TANoShowResend"]),
                                TANoGoResend = GenSettings.Filter(rd["TANoGoResend"]),
                                ExcludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                PayAsRostered = GenSettings.Filter(rd["PayAsRostered"]),
                                NDIA_LEVEL2 = GenSettings.Filter(rd["NDIA_LEVEL2"]),
                                NDIA_LEVEL3 = GenSettings.Filter(rd["NDIA_LEVEL3"]),
                                NDIA_LEVEL4  = GenSettings.Filter(rd["NDIA_LEVEL4"]),
                                ExcludeFromTimebands = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                NDIATravel = GenSettings.Filter(rd["NDIATravel"]),
                                ExcludeFromConflicts = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                ExcludeFromClientPortalDisplay = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]),
                                Job = GenSettings.Filter(rd["Job"]),
                                Lifecycle = GenSettings.Filter(rd["Lifecycle"]),
                                NDIAClaimType = GenSettings.Filter(rd["NDIAClaimType"]),
                         });
                         //,,,,,, 
                       // list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        } 

        [HttpPost("staffAdminActivities")]
        public async Task<IActionResult> PostAdminActivities([FromBody] ItemTypes activities)
        {
           
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                return BadRequest(errors);
            }
            try
            {

                using (var db = _context)
                {
                    await db.AddRangeAsync(activities);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        
        [HttpPut("staffAdminActivities")]
        public async Task<IActionResult> UpdateAdminActivities([FromBody] ItemTypes activities)
        {
            try
            {
                using (var db = _context)
                {
                    var singleActivity = (from rp in _context.ItemTypes
                                    where rp.Recnum == activities.Recnum
                                    select rp).FirstOrDefault();

                    if (singleActivity == null) return BadRequest();
                    //done
                    singleActivity.Title           =   activities.Title;
                    singleActivity.BillText        =   activities.BillText;
                    singleActivity.Status          =   activities.Status;
                    singleActivity.RosterGroup     =   activities.RosterGroup;
                    singleActivity.ProcessClassification = activities.ProcessClassification;
                    singleActivity.MinorGroup      =   activities.MinorGroup;
                    singleActivity.Amount          =   activities.Amount;
                    singleActivity.MinChargeRate   =   activities.MinChargeRate;
                    singleActivity.Unit            =   activities.Unit;
                    singleActivity.Lifecycle       =   activities.Lifecycle;
                    singleActivity.BudgetGroup     =   activities.BudgetGroup;
                    singleActivity.dataSet         =   activities.dataSet;
                    singleActivity.AutoApprove     =   activities.AutoApprove;
                    singleActivity.ExcludeFromAutoLeave = activities.ExcludeFromAutoLeave;
                    singleActivity.InfoOnly             = activities.InfoOnly;
                    //done
                    singleActivity.AccountingIdentifier  = activities.AccountingIdentifier;
                    singleActivity.GLRevenue             = activities.GLRevenue;
                    singleActivity.Job                   = activities.Job;
                    singleActivity.UnitCostUOM           = activities.UnitCostUOM;
                    singleActivity.UnitCost              = activities.UnitCost;
                    singleActivity.ExcludeFromPayExport    = activities.ExcludeFromPayExport;
                    singleActivity.ExcludeFromUsageStatements  = activities.ExcludeFromUsageStatements;
                    //done 
                    singleActivity.Price2               = activities.Price2;
                    singleActivity.Price3               = activities.Price3;
                    singleActivity.Price4               = activities.Price4;
                    singleActivity.Price5               = activities.Price5;
                    singleActivity.Price6               = activities.Price6;
                    singleActivity.excludeFromConflicts = activities.excludeFromConflicts; //new
                    //done
                    singleActivity.noMonday             = activities.noMonday;
                    singleActivity.noTuesday            = activities.noTuesday;
                    singleActivity.noWednesday          = activities.noWednesday;
                    singleActivity.noThursday           = activities.noThursday;
                    singleActivity.noFriday             = activities.noFriday;
                    singleActivity.noSaturday           = activities.noSaturday;
                    singleActivity.noSunday             = activities.noSunday;
                    singleActivity.noPubHol             = activities.noPubHol;
                    //done
                    singleActivity.StartTimeLimit       = activities.StartTimeLimit;
                    singleActivity.EndTimeLimit         = activities.EndTimeLimit;
                    singleActivity.MinDurtn             = activities.MinDurtn;
                    singleActivity.MaxDurtn             = activities.MaxDurtn;
                    singleActivity.FixedTime            = activities.FixedTime;
                    singleActivity.NoChangeDate         = activities.NoChangeDate;
                    singleActivity.NoChangeTime         = activities.NoChangeTime;
                    singleActivity.TimeChangeLimit      = activities.TimeChangeLimit;
                    //done
                    singleActivity.AutoRecipientDetails = activities.AutoRecipientDetails; 
                    singleActivity.DefaultAddress = activities.DefaultAddress; 
                    singleActivity.DefaultPhone = activities.DefaultPhone; 
                    singleActivity.AutoActivityNotes = activities.AutoActivityNotes; 
                    singleActivity.JobSheetPrompt = activities.JobSheetPrompt; 
                    singleActivity.ActivityNotes = activities.ActivityNotes; 
                    //done
                    singleActivity.IT_Dataset            = activities.IT_Dataset;
                    singleActivity.datasetGroup          = activities.datasetGroup;
                    singleActivity.HACCType              = activities.HACCType;
                    singleActivity.NDIAPriceType         = activities.NDIAPriceType;
                    singleActivity.NDIAClaimType         = activities.NDIAClaimType;
                    singleActivity.NDIATravel            = activities.NDIATravel;
                    singleActivity.NDIA_LEVEL2           = activities.NDIA_LEVEL2;
                    singleActivity.NDIA_LEVEL3           = activities.NDIA_LEVEL3;
                    singleActivity.NDIA_LEVEL4           = activities.NDIA_LEVEL4;
                    singleActivity.ALT_NDIANonLabTravelKmActivity = activities.ALT_NDIANonLabTravelKmActivity;
                    singleActivity.ALT_AppKmWithinActivity  = activities.ALT_AppKmWithinActivity;


                    //DONE
                    singleActivity.HACCUse          = activities.HACCUse;
                    singleActivity.CSTDAUse         = activities.CSTDAUse;
                    singleActivity.NRCPUse          = activities.NRCPUse;
                    singleActivity.ExcludeFromRecipSummarySheet = activities.ExcludeFromRecipSummarySheet;
                    singleActivity.ExcludeFromHigherPayCalculation = activities.ExcludeFromHigherPayCalculation;
                    singleActivity.NoOvertimeAccumulation = activities.NoOvertimeAccumulation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.ExcludeFromInterpretation = activities.ExcludeFromInterpretation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.JobType = activities.JobType;
                    singleActivity.TA_LOGINMODE = activities.TA_LOGINMODE;
                    singleActivity.ExcludeFromClientPortalDisplay = activities.ExcludeFromClientPortalDisplay;
                    singleActivity.ExcludeFromTravelCalc = activities.ExcludeFromTravelCalc;
                    singleActivity.TA_EXCLUDEGEOLOCATION = activities.TA_EXCLUDEGEOLOCATION;
                    singleActivity.AppExclude1 = activities.AppExclude1;
                    singleActivity.TAExclude1 = activities.TAExclude1;

                    singleActivity.TAEarlyStartTHEmail = activities.TAEarlyStartTHEmail;
                    singleActivity.TALateStartTHEmail = activities.TALateStartTHEmail;
                    singleActivity.TALateStartTH = activities.TALateStartTH;
                    singleActivity.TAEarlyStartTH = activities.TAEarlyStartTH;
                    singleActivity.TAEarlyStartTHWho = activities.TAEarlyStartTHWho;
                    singleActivity.TALateStartTHWho = activities.TALateStartTHWho;

                    singleActivity.TANoGoResend = activities.TANoGoResend;
                    singleActivity.TANoShowResend = activities.TANoShowResend;
                    singleActivity.TAEarlyFinishTHEmail = activities.TAEarlyFinishTHEmail;
                    singleActivity.TALateFinishTHEmail = activities.TALateFinishTHEmail;

                    singleActivity.TAEarlyFinishTH = activities.TAEarlyFinishTH;
                    singleActivity.TALateFinishTH = activities.TALateFinishTH;
                    singleActivity.TALateFinishTHWho = activities.TALateFinishTHWho;
                    singleActivity.TAEarlyFinishTHWho = activities.TAEarlyFinishTHWho;

                    singleActivity.TANoWorkTHEmail = activities.TANoWorkTHEmail;
                    singleActivity.TAUnderstayTHEmail = activities.TAUnderstayTHEmail;
                    singleActivity.TAOverstayTHEmail = activities.TAOverstayTHEmail;
                    singleActivity.TANoWorkTHWho = activities.TANoWorkTHWho;

                    singleActivity.TAOverstayTHWho = activities.TAOverstayTHWho;
                    singleActivity.TANoWorkTH = activities.TANoWorkTH;
                    singleActivity.TAUnderstayTH = activities.TAUnderstayTH;
                    singleActivity.TAOverstayTH = activities.TAOverstayTH;
                    
                    singleActivity.EndDate              = activities.EndDate;
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }

        [HttpGet("itemConsumable/{is_where}")]
        public async Task<IActionResult> GetItemConsumable(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE ProcessClassification <> 'INPUT' ";
                }else{
                    query = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE())";
                }
                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Title]) As row_num,[Recnum] As [Recnum],[Title] As [Title], CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [RosterGroupD],[RosterGroup],"+
                " [MinorGroup] As [Sub Group],[IT_Dataset] As [Dataset],[HACCType] As [Dataset Code], [CSTDAOutletID] As [OutletID],  [DatasetGroup] As [Dataset Group],  [NDIA_ID] As [NDIA ID],  [AccountingIdentifier] As [Accounting Code],        [Amount] As [Bill Amount],          [Unit] As [Bill Unit],     [EndDate] As [End Date],"+
                " [Status],[ProcessClassification],[DataSet] as dicipline,[DatasetGroup],[GLRevenue]," + 
                " [BudgetAmount],[BudgetPeriod],[PayGroup],[PayType],[ItemType],[Price2],[Price3],[Price4],[Price5],[Price6],[MainGroup],[AutoRecipientDetails], " + 
                " [AutoActivityNotes],[DefaultAddress],[DefaultPhone],[ActivityNotes],[GLCost],[UnitCostUOM],[UnitCost],[InfoOnly],[AutoApprove], " + 
                " [ExcludeFromPayExport],[BudgetGroup],[DefaultPayOnLeaveID],[DefaultActivityOnLeaveID],[DefaultProgramOnLeaveID],[BillText],[NoChangeDate], " + 
                " [NoChangeTime],[TimeChangeLimit],[JobType],[ExcludeFromUsageStatements],[TAExclude1],[MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday], " + 
                " [NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday],[NoPubHol],[JobSheetPrompt],[ExcludeFromInterpretation], " + 
                " [ExcludeFromAutoLeave],[CDCFee],[AppExclude1],[TA_LOGINMODE],[TA_EXCLUDEGEOLOCATION],[DEXID],[ExcludeFromTravelCalc],[FixedTime], " + 
                " [NoOvertimeAccumulation],[DMColor],[RetainOriginalDataset],[MinDurtn],[MaxDurtn],[NDIAPriceType],[TAEarlyStartTHWho],[TAEarlyFinishTHWho], " +
                " [TALateFinishTHWho],[TALateStartTHWho],[TAOverstayTHWho],[TAUnderstayTHWho],[TANoWorkTHWho],[TAEarlyStartTH],[TAEarlyFinishTH],[TALateFinishTH], " + 
                " [TALateStartTH],[TAOverstayTH],[TAUnderstayTH],[TANoWorkTH],[TAEarlyStartTHEmail],[TAEarlyFinishTHEmail],[TALateFinishTHEmail],[TALateStartTHEmail], " +
                " [TAOverstayTHEmail],[TAUnderstayTHEmail],[TANoWorkTHEmail],[TANoShowResend],[TANoGoResend],[ExcludeFromHigherPayCalculation],[PayAsRostered], " +
                " [NDIA_LEVEL2],[NDIA_LEVEL3],[NDIA_LEVEL4],[ExcludeFromTimebands],[NDIATravel],[ExcludeFromConflicts],[ExcludeFromClientPortalDisplay],[Job], " + 
                " [Lifecycle],[NDIAClaimType] FROM ItemTypes "+query+" AND (RosterGroup IN ('ITEM')) ORDER BY Title" ;
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                Recnum = GenSettings.Filter(rd["Recnum"]),
                                row_num = GenSettings.Filter(rd["Row_num"]),
                                title = GenSettings.Filter(rd["Title"]),
                                accountingCode = GenSettings.Filter(rd["accounting Code"]),
                                billAmount = GenSettings.Filter(rd["Bill Amount"]),
                                billUnit = GenSettings.Filter(rd["Bill Unit"]),
                                datasetCode = GenSettings.Filter(rd["Dataset Code"]),
                                datasetGroup = GenSettings.Filter(rd["Dataset Group"]),
                                endDate = GenSettings.Filter(rd["End Date"]),
                                ndiaid = GenSettings.Filter(rd["NDIA ID"]),
                                outletID = GenSettings.Filter(rd["OutletID"]),
                                rosterGroupD = GenSettings.Filter(rd["RosterGroupD"]),
                                rosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                                minorGroup = GenSettings.Filter(rd["Sub Group"]),
                                status = GenSettings.Filter(rd["Status"]),
                                processClassification = GenSettings.Filter(rd["ProcessClassification"]),
                                dataSet         = GenSettings.Filter(rd["dicipline"]),
                                accountingIdentifier = GenSettings.Filter(rd["accounting Code"]),
                                amount          = GenSettings.Filter(rd["Bill Amount"]),
                                unit            = GenSettings.Filter(rd["Bill Unit"]),
                                IT_Dataset      = GenSettings.Filter(rd["DataSet"]),
                                haccType        = GenSettings.Filter(rd["Dataset Code"]),
                                dicipline = GenSettings.Filter(rd["dicipline"]),
                                glRevenue = GenSettings.Filter(rd["GLRevenue"]),
                                budgetAmount = GenSettings.Filter(rd["BudgetAmount"]),
                                budgetPeriod = GenSettings.Filter(rd["BudgetPeriod"]),
                                payGroup = GenSettings.Filter(rd["PayGroup"]),
                                payType = GenSettings.Filter(rd["PayType"]),
                                itemType = GenSettings.Filter(rd["ItemType"]),
                                price2 = GenSettings.Filter(rd["Price2"]),
                                price3 = GenSettings.Filter(rd["Price3"]),
                                price4 = GenSettings.Filter(rd["Price4"]),
                                price5 = GenSettings.Filter(rd["Price5"]),
                                price6 = GenSettings.Filter(rd["Price6"]),
                                mainGroup = GenSettings.Filter(rd["MainGroup"]),
                                autoRecipientDetails = GenSettings.Filter(rd["AutoRecipientDetails"]),
                                autoActivityNotes = GenSettings.Filter(rd["AutoActivityNotes"]),
                                defaultAddress = GenSettings.Filter(rd["DefaultAddress"]),
                                defaultPhone  = GenSettings.Filter(rd["DefaultPhone"]),
                                activityNotes = GenSettings.Filter(rd["ActivityNotes"]),
                                glCost = GenSettings.Filter(rd["GLCost"]),
                                unitCostUOM = GenSettings.Filter(rd["UnitCostUOM"]),
                                unitCost = GenSettings.Filter(rd["UnitCost"]),
                                infoOnly = GenSettings.Filter(rd["InfoOnly"]),
                                autoApprove = GenSettings.Filter(rd["AutoApprove"]),
                                excludeFromPayExport = GenSettings.Filter(rd["ExcludeFromPayExport"]),
                                budgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
                                defaultPayOnLeaveID = GenSettings.Filter(rd["DefaultPayOnLeaveID"]),
                                defaultActivityOnLeaveID = GenSettings.Filter(rd["DefaultActivityOnLeaveID"]),
                                defaultProgramOnLeaveID = GenSettings.Filter(rd["DefaultProgramOnLeaveID"]),
                                billText = GenSettings.Filter(rd["BillText"]),
                                noChangeDate = GenSettings.Filter(rd["NoChangeDate"]),
                                noChangeTime = GenSettings.Filter(rd["NoChangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                jobType  = GenSettings.Filter(rd["JobType"]),
                                excludeFromUsageStatements = GenSettings.Filter(rd["ExcludeFromUsageStatements"]),
                                taexclude1 = GenSettings.Filter(rd["TAExclude1"]),
                                minChargeRate = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday = GenSettings.Filter(rd["NoThursday"]),
                                noFriday = GenSettings.Filter(rd["NoFriday"]),
                                noSunday = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday  = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt = GenSettings.Filter(rd["JobSheetPrompt"]),
                                excludeFromInterpretation = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                excludeFromAutoLeave = GenSettings.Filter(rd["ExcludeFromAutoLeave"]),
                                cdcFee = GenSettings.Filter(rd["CDCFee"]),
                                appExclude1 = GenSettings.Filter(rd["AppExclude1"]),
                                tA_LOGINMODE= GenSettings.Filter(rd["TA_LOGINMODE"]),
                                tA_EXCLUDEGEOLOCATION = GenSettings.Filter(rd["TA_EXCLUDEGEOLOCATION"]),
                                dexid = GenSettings.Filter(rd["DEXID"]),
                                excludeFromTravelCalc = GenSettings.Filter(rd["ExcludeFromTravelCalc"]),
                                fixedTime = GenSettings.Filter(rd["FixedTime"]),
                                noOvertimeAccumulation = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                dmColor = GenSettings.Filter(rd["DMColor"]),
                                retainOriginalDataset = GenSettings.Filter(rd["RetainOriginalDataset"]),
                                minDurtn = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn = GenSettings.Filter(rd["MaxDurtn"]),
                                ndiaPriceType = GenSettings.Filter(rd["NDIAPriceType"]),
                                TAEarlyStartTHWho = GenSettings.Filter(rd["TAEarlyStartTHWho"]),
                                TAEarlyFinishTHWho = GenSettings.Filter(rd["TAEarlyFinishTHWho"]),
                                TALateFinishTHWho = GenSettings.Filter(rd["TALateFinishTHWho"]),
                                TALateStartTHWho = GenSettings.Filter(rd["TALateStartTHWho"]),
                                TAOverstayTHWho = GenSettings.Filter(rd["TAOverstayTHWho"]),
                                TAUnderstayTHWho = GenSettings.Filter(rd["TAUnderstayTHWho"]),
                                TANoWorkTHWho = GenSettings.Filter(rd["TANoWorkTHWho"]),
                                TAEarlyStartTH = GenSettings.Filter(rd["TAEarlyStartTH"]),
                                TAEarlyFinishTH = GenSettings.Filter(rd["TAEarlyFinishTH"]),
                                TALateFinishTH = GenSettings.Filter(rd["TALateFinishTH"]),
                                TALateStartTH = GenSettings.Filter(rd["TALateStartTH"]),
                                TAOverstayTH = GenSettings.Filter(rd["TAOverstayTH"]),
                                TAUnderstayTH = GenSettings.Filter(rd["TAUnderstayTH"]),
                                TANoWorkTH = GenSettings.Filter(rd["TANoWorkTH"]),
                                TAEarlyStartTHEmail = GenSettings.Filter(rd["TAEarlyStartTHEmail"]),
                                TAEarlyFinishTHEmail = GenSettings.Filter(rd["TAEarlyFinishTHEmail"]),
                                TALateFinishTHEmail = GenSettings.Filter(rd["TALateFinishTHEmail"]),
                                TALateStartTHEmail = GenSettings.Filter(rd["TALateStartTHEmail"]),
                                TAOverstayTHEmail = GenSettings.Filter(rd["TAOverstayTHEmail"]),
                                TAUnderstayTHEmail = GenSettings.Filter(rd["TAUnderstayTHEmail"]),
                                TANoWorkTHEmail = GenSettings.Filter(rd["TANoWorkTHEmail"]),
                                TANoShowResend = GenSettings.Filter(rd["TANoShowResend"]),
                                TANoGoResend = GenSettings.Filter(rd["TANoGoResend"]),
                                ExcludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                PayAsRostered = GenSettings.Filter(rd["PayAsRostered"]),
                                NDIA_LEVEL2 = GenSettings.Filter(rd["NDIA_LEVEL2"]),
                                NDIA_LEVEL3 = GenSettings.Filter(rd["NDIA_LEVEL3"]),
                                NDIA_LEVEL4  = GenSettings.Filter(rd["NDIA_LEVEL4"]),
                                ExcludeFromTimebands = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                NDIATravel = GenSettings.Filter(rd["NDIATravel"]),
                                ExcludeFromConflicts = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                ExcludeFromClientPortalDisplay = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]),
                                Job = GenSettings.Filter(rd["Job"]),
                                Lifecycle = GenSettings.Filter(rd["Lifecycle"]),
                                NDIAClaimType = GenSettings.Filter(rd["NDIAClaimType"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpPost("itemConsumable")]
        public async Task<IActionResult> itemConsumable([FromBody] ItemTypes itemConsumable)
        {
            try
            {
                using (var db = _context)
                {
                    await db.AddRangeAsync(itemConsumable);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPut("itemConsumable")]
        public async Task<IActionResult> UpdateitemConsumable([FromBody] ItemTypes activities)
        {
            try
            {
                using (var db = _context)
                {
                    var singleActivity = (from rp in _context.ItemTypes
                                where rp.Recnum == activities.Recnum
                                select rp).FirstOrDefault();

                    if (singleActivity == null) return BadRequest();

                     //done
                    singleActivity.Title           =   activities.Title;
                    singleActivity.BillText        =   activities.BillText;
                    singleActivity.Status          =   activities.Status;
                    singleActivity.RosterGroup     =   activities.RosterGroup;
                    singleActivity.ProcessClassification = activities.ProcessClassification;
                    singleActivity.MinorGroup      =   activities.MinorGroup;
                    singleActivity.Amount          =   activities.Amount;
                    singleActivity.MinChargeRate   =   activities.MinChargeRate;
                    singleActivity.Unit            =   activities.Unit;
                    singleActivity.Lifecycle       =   activities.Lifecycle;
                    singleActivity.BudgetGroup     =   activities.BudgetGroup;
                    singleActivity.dataSet         =   activities.dataSet;
                    singleActivity.AutoApprove     =   activities.AutoApprove;
                    singleActivity.ExcludeFromAutoLeave = activities.ExcludeFromAutoLeave;
                    singleActivity.InfoOnly             = activities.InfoOnly;
                    //done
                    singleActivity.AccountingIdentifier  = activities.AccountingIdentifier;
                    singleActivity.GLRevenue             = activities.GLRevenue;
                    singleActivity.Job                   = activities.Job;
                    singleActivity.UnitCostUOM           = activities.UnitCostUOM;
                    singleActivity.UnitCost              = activities.UnitCost;
                    singleActivity.ExcludeFromPayExport    = activities.ExcludeFromPayExport;
                    singleActivity.ExcludeFromUsageStatements  = activities.ExcludeFromUsageStatements;
                    //done 
                    singleActivity.Price2               = activities.Price2;
                    singleActivity.Price3               = activities.Price3;
                    singleActivity.Price4               = activities.Price4;
                    singleActivity.Price5               = activities.Price5;
                    singleActivity.Price6               = activities.Price6;
                    singleActivity.excludeFromConflicts = activities.excludeFromConflicts; //new
                    //done
                    singleActivity.noMonday             = activities.noMonday;
                    singleActivity.noTuesday            = activities.noTuesday;
                    singleActivity.noWednesday          = activities.noWednesday;
                    singleActivity.noThursday           = activities.noThursday;
                    singleActivity.noFriday             = activities.noFriday;
                    singleActivity.noSaturday           = activities.noSaturday;
                    singleActivity.noSunday             = activities.noSunday;
                    singleActivity.noPubHol             = activities.noPubHol;
                    //done
                    singleActivity.StartTimeLimit       = activities.StartTimeLimit;
                    singleActivity.EndTimeLimit         = activities.EndTimeLimit;
                    singleActivity.MinDurtn             = activities.MinDurtn;
                    singleActivity.MaxDurtn             = activities.MaxDurtn;
                    singleActivity.FixedTime            = activities.FixedTime;
                    singleActivity.NoChangeDate         = activities.NoChangeDate;
                    singleActivity.NoChangeTime         = activities.NoChangeTime;
                    singleActivity.TimeChangeLimit      = activities.TimeChangeLimit;
                    //done
                    singleActivity.AutoRecipientDetails = activities.AutoRecipientDetails; 
                    singleActivity.DefaultAddress = activities.DefaultAddress; 
                    singleActivity.DefaultPhone = activities.DefaultPhone; 
                    singleActivity.AutoActivityNotes = activities.AutoActivityNotes; 
                    singleActivity.JobSheetPrompt = activities.JobSheetPrompt; 
                    singleActivity.ActivityNotes = activities.ActivityNotes; 
                    //done
                    singleActivity.IT_Dataset            = activities.IT_Dataset;
                    singleActivity.datasetGroup          = activities.datasetGroup;
                    singleActivity.HACCType              = activities.HACCType;
                    singleActivity.NDIAPriceType         = activities.NDIAPriceType;
                    singleActivity.NDIAClaimType         = activities.NDIAClaimType;
                    singleActivity.NDIATravel            = activities.NDIATravel;
                    singleActivity.NDIA_LEVEL2           = activities.NDIA_LEVEL2;
                    singleActivity.NDIA_LEVEL3           = activities.NDIA_LEVEL3;
                    singleActivity.NDIA_LEVEL4           = activities.NDIA_LEVEL4;
                    singleActivity.ALT_NDIANonLabTravelKmActivity = activities.ALT_NDIANonLabTravelKmActivity;
                    singleActivity.ALT_AppKmWithinActivity  = activities.ALT_AppKmWithinActivity;


                    //DONE
                    singleActivity.HACCUse          = activities.HACCUse;
                    singleActivity.CSTDAUse         = activities.CSTDAUse;
                    singleActivity.NRCPUse          = activities.NRCPUse;
                    singleActivity.ExcludeFromRecipSummarySheet = activities.ExcludeFromRecipSummarySheet;
                    singleActivity.ExcludeFromHigherPayCalculation = activities.ExcludeFromHigherPayCalculation;
                    singleActivity.NoOvertimeAccumulation = activities.NoOvertimeAccumulation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.ExcludeFromInterpretation = activities.ExcludeFromInterpretation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.JobType = activities.JobType;
                    singleActivity.TA_LOGINMODE = activities.TA_LOGINMODE;
                    singleActivity.ExcludeFromClientPortalDisplay = activities.ExcludeFromClientPortalDisplay;
                    singleActivity.ExcludeFromTravelCalc = activities.ExcludeFromTravelCalc;
                    singleActivity.TA_EXCLUDEGEOLOCATION = activities.TA_EXCLUDEGEOLOCATION;
                    singleActivity.AppExclude1 = activities.AppExclude1;
                    singleActivity.TAExclude1 = activities.TAExclude1;

                    singleActivity.TAEarlyStartTHEmail = activities.TAEarlyStartTHEmail;
                    singleActivity.TALateStartTHEmail = activities.TALateStartTHEmail;
                    singleActivity.TALateStartTH = activities.TALateStartTH;
                    singleActivity.TAEarlyStartTH = activities.TAEarlyStartTH;
                    singleActivity.TAEarlyStartTHWho = activities.TAEarlyStartTHWho;
                    singleActivity.TALateStartTHWho = activities.TALateStartTHWho;

                    singleActivity.TANoGoResend = activities.TANoGoResend;
                    singleActivity.TANoShowResend = activities.TANoShowResend;
                    singleActivity.TAEarlyFinishTHEmail = activities.TAEarlyFinishTHEmail;
                    singleActivity.TALateFinishTHEmail = activities.TALateFinishTHEmail;

                    singleActivity.TAEarlyFinishTH = activities.TAEarlyFinishTH;
                    singleActivity.TALateFinishTH = activities.TALateFinishTH;
                    singleActivity.TALateFinishTHWho = activities.TALateFinishTHWho;
                    singleActivity.TAEarlyFinishTHWho = activities.TAEarlyFinishTHWho;

                    singleActivity.TANoWorkTHEmail = activities.TANoWorkTHEmail;
                    singleActivity.TAUnderstayTHEmail = activities.TAUnderstayTHEmail;
                    singleActivity.TAOverstayTHEmail = activities.TAOverstayTHEmail;
                    singleActivity.TANoWorkTHWho = activities.TANoWorkTHWho;

                    singleActivity.TAOverstayTHWho = activities.TAOverstayTHWho;
                    singleActivity.TANoWorkTH = activities.TANoWorkTH;
                    singleActivity.TAUnderstayTH = activities.TAUnderstayTH;
                    singleActivity.TAOverstayTH = activities.TAOverstayTH;
                    
                    singleActivity.EndDate              = activities.EndDate;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpGet("menuMeals/{is_where}")]
        public async Task<IActionResult> GetMenuMeals(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE ProcessClassification <> 'INPUT' ";
                }else{
                    query = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE())";
                }

                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Title]) AS Row_num,[Recnum] As [Recnum],[Title] As [Title], CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [RosterGroupD],[RosterGroup],"+
                " [MinorGroup] As [Sub Group],[IT_Dataset] As [Dataset],[HACCType] As [Dataset Code], [CSTDAOutletID] As [OutletID],[DatasetGroup] As [Dataset Group],[NDIA_ID] As [NDIA ID],[AccountingIdentifier] As [Accounting Code],[Amount] As [Bill Amount],[Unit] As [Bill Unit],[EndDate] As [End Date]," +
                " [Status],[ProcessClassification],[DataSet] as dicipline,[DatasetGroup],[GLRevenue]," + 
                " [BudgetAmount],[BudgetPeriod],[PayGroup],[PayType],[ItemType],[Price2],[Price3],[Price4],[Price5],[Price6],[MainGroup],[AutoRecipientDetails], " + 
                " [AutoActivityNotes],[DefaultAddress],[DefaultPhone],[ActivityNotes],[GLCost],[UnitCostUOM],[UnitCost],[InfoOnly],[AutoApprove], " + 
                " [ExcludeFromPayExport],[BudgetGroup],[DefaultPayOnLeaveID],[DefaultActivityOnLeaveID],[DefaultProgramOnLeaveID],[BillText],[NoChangeDate], " + 
                " [NoChangeTime],[TimeChangeLimit],[JobType],[ExcludeFromUsageStatements],[TAExclude1],[MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday], " + 
                " [NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday],[NoPubHol],[JobSheetPrompt],[ExcludeFromInterpretation], " + 
                " [ExcludeFromAutoLeave],[CDCFee],[AppExclude1],[TA_LOGINMODE],[TA_EXCLUDEGEOLOCATION],[DEXID],[ExcludeFromTravelCalc],[FixedTime], " + 
                " [NoOvertimeAccumulation],[DMColor],[RetainOriginalDataset],[MinDurtn],[MaxDurtn],[NDIAPriceType],[TAEarlyStartTHWho],[TAEarlyFinishTHWho], " +
                " [TALateFinishTHWho],[TALateStartTHWho],[TAOverstayTHWho],[TAUnderstayTHWho],[TANoWorkTHWho],[TAEarlyStartTH],[TAEarlyFinishTH],[TALateFinishTH], " + 
                " [TALateStartTH],[TAOverstayTH],[TAUnderstayTH],[TANoWorkTH],[TAEarlyStartTHEmail],[TAEarlyFinishTHEmail],[TALateFinishTHEmail],[TALateStartTHEmail], " +
                " [TAOverstayTHEmail],[TAUnderstayTHEmail],[TANoWorkTHEmail],[TANoShowResend],[TANoGoResend],[ExcludeFromHigherPayCalculation],[PayAsRostered], " +
                " [NDIA_LEVEL2],[NDIA_LEVEL3],[NDIA_LEVEL4],[ExcludeFromTimebands],[NDIATravel],[ExcludeFromConflicts],[ExcludeFromClientPortalDisplay],[Job], " + 
                " [Lifecycle],[NDIAClaimType] FROM ItemTypes "+query+" AND (MinorGroup IN ('MEALS')) ORDER BY Title" ;
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                Recnum = GenSettings.Filter(rd["Recnum"]),
                                row_num = GenSettings.Filter(rd["Row_num"]),
                                title = GenSettings.Filter(rd["Title"]),
                                accountingCode = GenSettings.Filter(rd["accounting Code"]),
                                billAmount = GenSettings.Filter(rd["Bill Amount"]),
                                billUnit = GenSettings.Filter(rd["Bill Unit"]),
                                datasetCode = GenSettings.Filter(rd["Dataset Code"]),
                                datasetGroup = GenSettings.Filter(rd["Dataset Group"]),
                                endDate = GenSettings.Filter(rd["End Date"]),
                                ndiaid = GenSettings.Filter(rd["NDIA ID"]),
                                outletID = GenSettings.Filter(rd["OutletID"]),
                                rosterGroupD = GenSettings.Filter(rd["RosterGroupD"]),
                                rosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                                minorGroup = GenSettings.Filter(rd["Sub Group"]),
                                status = GenSettings.Filter(rd["Status"]),
                                processClassification = GenSettings.Filter(rd["ProcessClassification"]),
                                dataSet         = GenSettings.Filter(rd["dicipline"]),
                                accountingIdentifier = GenSettings.Filter(rd["accounting Code"]),
                                amount          = GenSettings.Filter(rd["Bill Amount"]),
                                unit            = GenSettings.Filter(rd["Bill Unit"]),
                                IT_Dataset      = GenSettings.Filter(rd["DataSet"]),
                                haccType        = GenSettings.Filter(rd["Dataset Code"]),
                                dicipline = GenSettings.Filter(rd["dicipline"]),
                                glRevenue = GenSettings.Filter(rd["GLRevenue"]),
                                budgetAmount = GenSettings.Filter(rd["BudgetAmount"]),
                                budgetPeriod = GenSettings.Filter(rd["BudgetPeriod"]),
                                payGroup = GenSettings.Filter(rd["PayGroup"]),
                                payType = GenSettings.Filter(rd["PayType"]),
                                itemType = GenSettings.Filter(rd["ItemType"]),
                                price2 = GenSettings.Filter(rd["Price2"]),
                                price3 = GenSettings.Filter(rd["Price3"]),
                                price4 = GenSettings.Filter(rd["Price4"]),
                                price5 = GenSettings.Filter(rd["Price5"]),
                                price6 = GenSettings.Filter(rd["Price6"]),
                                mainGroup = GenSettings.Filter(rd["MainGroup"]),
                                autoRecipientDetails = GenSettings.Filter(rd["AutoRecipientDetails"]),
                                autoActivityNotes = GenSettings.Filter(rd["AutoActivityNotes"]),
                                defaultAddress = GenSettings.Filter(rd["DefaultAddress"]),
                                defaultPhone  = GenSettings.Filter(rd["DefaultPhone"]),
                                activityNotes = GenSettings.Filter(rd["ActivityNotes"]),
                                glCost = GenSettings.Filter(rd["GLCost"]),
                                unitCostUOM = GenSettings.Filter(rd["UnitCostUOM"]),
                                unitCost = GenSettings.Filter(rd["UnitCost"]),
                                infoOnly = GenSettings.Filter(rd["InfoOnly"]),
                                autoApprove = GenSettings.Filter(rd["AutoApprove"]),
                                excludeFromPayExport = GenSettings.Filter(rd["ExcludeFromPayExport"]),
                                budgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
                                defaultPayOnLeaveID = GenSettings.Filter(rd["DefaultPayOnLeaveID"]),
                                defaultActivityOnLeaveID = GenSettings.Filter(rd["DefaultActivityOnLeaveID"]),
                                defaultProgramOnLeaveID = GenSettings.Filter(rd["DefaultProgramOnLeaveID"]),
                                billText = GenSettings.Filter(rd["BillText"]),
                                noChangeDate = GenSettings.Filter(rd["NoChangeDate"]),
                                noChangeTime = GenSettings.Filter(rd["NoChangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                jobType  = GenSettings.Filter(rd["JobType"]),
                                excludeFromUsageStatements = GenSettings.Filter(rd["ExcludeFromUsageStatements"]),
                                taexclude1 = GenSettings.Filter(rd["TAExclude1"]),
                                minChargeRate = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday = GenSettings.Filter(rd["NoThursday"]),
                                noFriday = GenSettings.Filter(rd["NoFriday"]),
                                noSunday = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday  = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt = GenSettings.Filter(rd["JobSheetPrompt"]),
                                excludeFromInterpretation = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                excludeFromAutoLeave = GenSettings.Filter(rd["ExcludeFromAutoLeave"]),
                                cdcFee = GenSettings.Filter(rd["CDCFee"]),
                                appExclude1 = GenSettings.Filter(rd["AppExclude1"]),
                                tA_LOGINMODE= GenSettings.Filter(rd["TA_LOGINMODE"]),
                                tA_EXCLUDEGEOLOCATION = GenSettings.Filter(rd["TA_EXCLUDEGEOLOCATION"]),
                                dexid = GenSettings.Filter(rd["DEXID"]),
                                excludeFromTravelCalc = GenSettings.Filter(rd["ExcludeFromTravelCalc"]),
                                fixedTime = GenSettings.Filter(rd["FixedTime"]),
                                noOvertimeAccumulation = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                dmColor = GenSettings.Filter(rd["DMColor"]),
                                retainOriginalDataset = GenSettings.Filter(rd["RetainOriginalDataset"]),
                                minDurtn = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn = GenSettings.Filter(rd["MaxDurtn"]),
                                ndiaPriceType = GenSettings.Filter(rd["NDIAPriceType"]),
                                TAEarlyStartTHWho = GenSettings.Filter(rd["TAEarlyStartTHWho"]),
                                TAEarlyFinishTHWho = GenSettings.Filter(rd["TAEarlyFinishTHWho"]),
                                TALateFinishTHWho = GenSettings.Filter(rd["TALateFinishTHWho"]),
                                TALateStartTHWho = GenSettings.Filter(rd["TALateStartTHWho"]),
                                TAOverstayTHWho = GenSettings.Filter(rd["TAOverstayTHWho"]),
                                TAUnderstayTHWho = GenSettings.Filter(rd["TAUnderstayTHWho"]),
                                TANoWorkTHWho = GenSettings.Filter(rd["TANoWorkTHWho"]),
                                TAEarlyStartTH = GenSettings.Filter(rd["TAEarlyStartTH"]),
                                TAEarlyFinishTH = GenSettings.Filter(rd["TAEarlyFinishTH"]),
                                TALateFinishTH = GenSettings.Filter(rd["TALateFinishTH"]),
                                TALateStartTH = GenSettings.Filter(rd["TALateStartTH"]),
                                TAOverstayTH = GenSettings.Filter(rd["TAOverstayTH"]),
                                TAUnderstayTH = GenSettings.Filter(rd["TAUnderstayTH"]),
                                TANoWorkTH = GenSettings.Filter(rd["TANoWorkTH"]),
                                TAEarlyStartTHEmail = GenSettings.Filter(rd["TAEarlyStartTHEmail"]),
                                TAEarlyFinishTHEmail = GenSettings.Filter(rd["TAEarlyFinishTHEmail"]),
                                TALateFinishTHEmail = GenSettings.Filter(rd["TALateFinishTHEmail"]),
                                TALateStartTHEmail = GenSettings.Filter(rd["TALateStartTHEmail"]),
                                TAOverstayTHEmail = GenSettings.Filter(rd["TAOverstayTHEmail"]),
                                TAUnderstayTHEmail = GenSettings.Filter(rd["TAUnderstayTHEmail"]),
                                TANoWorkTHEmail = GenSettings.Filter(rd["TANoWorkTHEmail"]),
                                TANoShowResend = GenSettings.Filter(rd["TANoShowResend"]),
                                TANoGoResend = GenSettings.Filter(rd["TANoGoResend"]),
                                ExcludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                PayAsRostered = GenSettings.Filter(rd["PayAsRostered"]),
                                NDIA_LEVEL2 = GenSettings.Filter(rd["NDIA_LEVEL2"]),
                                NDIA_LEVEL3 = GenSettings.Filter(rd["NDIA_LEVEL3"]),
                                NDIA_LEVEL4  = GenSettings.Filter(rd["NDIA_LEVEL4"]),
                                ExcludeFromTimebands = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                NDIATravel = GenSettings.Filter(rd["NDIATravel"]),
                                ExcludeFromConflicts = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                ExcludeFromClientPortalDisplay = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]),
                                Job = GenSettings.Filter(rd["Job"]),
                                Lifecycle = GenSettings.Filter(rd["Lifecycle"]),
                                NDIAClaimType = GenSettings.Filter(rd["NDIAClaimType"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpPost("menuMeals")]
        public async Task<IActionResult> menuMeals([FromBody] ItemTypes menuMeals)
        {
            if(!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                return BadRequest(errors);
            }
            try
            {
                using (var db = _context)
                {
                    await db.AddRangeAsync(menuMeals);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }


        }
        [HttpPut("menuMeals")]
        public async Task<IActionResult> UpdatemenuMeals([FromBody] ItemTypes activities)
        {
            try
            {
                using (var db = _context)
                {
                    var singleActivity = (from rp in _context.ItemTypes
                                where rp.Recnum == activities.Recnum
                                select rp).FirstOrDefault();

                    if (singleActivity == null) return BadRequest();

                     //done
                    singleActivity.Title           =   activities.Title;
                    singleActivity.BillText        =   activities.BillText;
                    singleActivity.Status          =   activities.Status;
                    singleActivity.RosterGroup     =   activities.RosterGroup;
                    singleActivity.ProcessClassification = activities.ProcessClassification;
                    singleActivity.MinorGroup      =   activities.MinorGroup;
                    singleActivity.Amount          =   activities.Amount;
                    singleActivity.MinChargeRate   =   activities.MinChargeRate;
                    singleActivity.Unit            =   activities.Unit;
                    singleActivity.Lifecycle       =   activities.Lifecycle;
                    singleActivity.BudgetGroup     =   activities.BudgetGroup;
                    singleActivity.dataSet         =   activities.dataSet;
                    singleActivity.AutoApprove     =   activities.AutoApprove;
                    singleActivity.ExcludeFromAutoLeave = activities.ExcludeFromAutoLeave;
                    singleActivity.InfoOnly             = activities.InfoOnly;
                    //done
                    singleActivity.AccountingIdentifier  = activities.AccountingIdentifier;
                    singleActivity.GLRevenue             = activities.GLRevenue;
                    singleActivity.Job                   = activities.Job;
                    singleActivity.UnitCostUOM           = activities.UnitCostUOM;
                    singleActivity.UnitCost              = activities.UnitCost;
                    singleActivity.ExcludeFromPayExport    = activities.ExcludeFromPayExport;
                    singleActivity.ExcludeFromUsageStatements  = activities.ExcludeFromUsageStatements;
                    //done 
                    singleActivity.Price2               = activities.Price2;
                    singleActivity.Price3               = activities.Price3;
                    singleActivity.Price4               = activities.Price4;
                    singleActivity.Price5               = activities.Price5;
                    singleActivity.Price6               = activities.Price6;
                    singleActivity.excludeFromConflicts = activities.excludeFromConflicts; //new
                    //done
                    singleActivity.noMonday             = activities.noMonday;
                    singleActivity.noTuesday            = activities.noTuesday;
                    singleActivity.noWednesday          = activities.noWednesday;
                    singleActivity.noThursday           = activities.noThursday;
                    singleActivity.noFriday             = activities.noFriday;
                    singleActivity.noSaturday           = activities.noSaturday;
                    singleActivity.noSunday             = activities.noSunday;
                    singleActivity.noPubHol             = activities.noPubHol;
                    //done
                    singleActivity.StartTimeLimit       = activities.StartTimeLimit;
                    singleActivity.EndTimeLimit         = activities.EndTimeLimit;
                    singleActivity.MinDurtn             = activities.MinDurtn;
                    singleActivity.MaxDurtn             = activities.MaxDurtn;
                    singleActivity.FixedTime            = activities.FixedTime;
                    singleActivity.NoChangeDate         = activities.NoChangeDate;
                    singleActivity.NoChangeTime         = activities.NoChangeTime;
                    singleActivity.TimeChangeLimit      = activities.TimeChangeLimit;
                    //done
                    singleActivity.AutoRecipientDetails = activities.AutoRecipientDetails; 
                    singleActivity.DefaultAddress = activities.DefaultAddress; 
                    singleActivity.DefaultPhone = activities.DefaultPhone; 
                    singleActivity.AutoActivityNotes = activities.AutoActivityNotes; 
                    singleActivity.JobSheetPrompt = activities.JobSheetPrompt; 
                    singleActivity.ActivityNotes = activities.ActivityNotes; 
                    //done
                    singleActivity.IT_Dataset            = activities.IT_Dataset;
                    singleActivity.datasetGroup          = activities.datasetGroup;
                    singleActivity.HACCType              = activities.HACCType;
                    singleActivity.NDIAPriceType         = activities.NDIAPriceType;
                    singleActivity.NDIAClaimType         = activities.NDIAClaimType;
                    singleActivity.NDIATravel            = activities.NDIATravel;
                    singleActivity.NDIA_LEVEL2           = activities.NDIA_LEVEL2;
                    singleActivity.NDIA_LEVEL3           = activities.NDIA_LEVEL3;
                    singleActivity.NDIA_LEVEL4           = activities.NDIA_LEVEL4;
                    singleActivity.ALT_NDIANonLabTravelKmActivity = activities.ALT_NDIANonLabTravelKmActivity;
                    singleActivity.ALT_AppKmWithinActivity  = activities.ALT_AppKmWithinActivity;


                    //DONE
                    singleActivity.HACCUse          = activities.HACCUse;
                    singleActivity.CSTDAUse         = activities.CSTDAUse;
                    singleActivity.NRCPUse          = activities.NRCPUse;
                    singleActivity.ExcludeFromRecipSummarySheet = activities.ExcludeFromRecipSummarySheet;
                    singleActivity.ExcludeFromHigherPayCalculation = activities.ExcludeFromHigherPayCalculation;
                    singleActivity.NoOvertimeAccumulation = activities.NoOvertimeAccumulation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.ExcludeFromInterpretation = activities.ExcludeFromInterpretation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.JobType = activities.JobType;
                    singleActivity.TA_LOGINMODE = activities.TA_LOGINMODE;
                    singleActivity.ExcludeFromClientPortalDisplay = activities.ExcludeFromClientPortalDisplay;
                    singleActivity.ExcludeFromTravelCalc = activities.ExcludeFromTravelCalc;
                    singleActivity.TA_EXCLUDEGEOLOCATION = activities.TA_EXCLUDEGEOLOCATION;
                    singleActivity.AppExclude1 = activities.AppExclude1;
                    singleActivity.TAExclude1 = activities.TAExclude1;

                    singleActivity.TAEarlyStartTHEmail = activities.TAEarlyStartTHEmail;
                    singleActivity.TALateStartTHEmail = activities.TALateStartTHEmail;
                    singleActivity.TALateStartTH = activities.TALateStartTH;
                    singleActivity.TAEarlyStartTH = activities.TAEarlyStartTH;
                    singleActivity.TAEarlyStartTHWho = activities.TAEarlyStartTHWho;
                    singleActivity.TALateStartTHWho = activities.TALateStartTHWho;

                    singleActivity.TANoGoResend = activities.TANoGoResend;
                    singleActivity.TANoShowResend = activities.TANoShowResend;
                    singleActivity.TAEarlyFinishTHEmail = activities.TAEarlyFinishTHEmail;
                    singleActivity.TALateFinishTHEmail = activities.TALateFinishTHEmail;

                    singleActivity.TAEarlyFinishTH = activities.TAEarlyFinishTH;
                    singleActivity.TALateFinishTH = activities.TALateFinishTH;
                    singleActivity.TALateFinishTHWho = activities.TALateFinishTHWho;
                    singleActivity.TAEarlyFinishTHWho = activities.TAEarlyFinishTHWho;

                    singleActivity.TANoWorkTHEmail = activities.TANoWorkTHEmail;
                    singleActivity.TAUnderstayTHEmail = activities.TAUnderstayTHEmail;
                    singleActivity.TAOverstayTHEmail = activities.TAOverstayTHEmail;
                    singleActivity.TANoWorkTHWho = activities.TANoWorkTHWho;

                    singleActivity.TAOverstayTHWho = activities.TAOverstayTHWho;
                    singleActivity.TANoWorkTH = activities.TANoWorkTH;
                    singleActivity.TAUnderstayTH = activities.TAUnderstayTH;
                    singleActivity.TAOverstayTH = activities.TAOverstayTH;
                    
                    singleActivity.EndDate              = activities.EndDate;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpGet("caseManagement/{is_where}")]
        public async Task<IActionResult> GetCaseManagement(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE ProcessClassification <> 'INPUT' ";
                }else{
                    query = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE())";
                }

                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Title]) AS Row_num,[Recnum] As Recnum, [Title] As [Title], CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [RosterGroupD],[RosterGroup],"+
                " [MinorGroup] As [Sub Group],[IT_Dataset] As [Dataset],[HACCType] As [Dataset Code],[CSTDAOutletID] As [OutletID],[DatasetGroup] As [Dataset Group],[NDIA_ID] As [NDIA ID],[AccountingIdentifier] As [Accounting Code],[Amount] As [Bill Amount],[Unit] As [Bill Unit],[EndDate] As [End Date],"+
                " [Status],[ProcessClassification],[DataSet] as dicipline,[DatasetGroup],[GLRevenue]," + 
                " [BudgetAmount],[BudgetPeriod],[PayGroup],[PayType],[ItemType],[Price2],[Price3],[Price4],[Price5],[Price6],[MainGroup],[AutoRecipientDetails], " + 
                " [AutoActivityNotes],[DefaultAddress],[DefaultPhone],[ActivityNotes],[GLCost],[UnitCostUOM],[UnitCost],[InfoOnly],[AutoApprove], " + 
                " [ExcludeFromPayExport],[BudgetGroup],[DefaultPayOnLeaveID],[DefaultActivityOnLeaveID],[DefaultProgramOnLeaveID],[BillText],[NoChangeDate], " + 
                " [NoChangeTime],[TimeChangeLimit],[JobType],[ExcludeFromUsageStatements],[TAExclude1],[MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday], " + 
                " [NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday],[NoPubHol],[JobSheetPrompt],[ExcludeFromInterpretation], " + 
                " [ExcludeFromAutoLeave],[CDCFee],[AppExclude1],[TA_LOGINMODE],[TA_EXCLUDEGEOLOCATION],[DEXID],[ExcludeFromTravelCalc],[FixedTime], " + 
                " [NoOvertimeAccumulation],[DMColor],[RetainOriginalDataset],[MinDurtn],[MaxDurtn],[NDIAPriceType],[TAEarlyStartTHWho],[TAEarlyFinishTHWho], " +
                " [TALateFinishTHWho],[TALateStartTHWho],[TAOverstayTHWho],[TAUnderstayTHWho],[TANoWorkTHWho],[TAEarlyStartTH],[TAEarlyFinishTH],[TALateFinishTH], " + 
                " [TALateStartTH],[TAOverstayTH],[TAUnderstayTH],[TANoWorkTH],[TAEarlyStartTHEmail],[TAEarlyFinishTHEmail],[TALateFinishTHEmail],[TALateStartTHEmail], " +
                " [TAOverstayTHEmail],[TAUnderstayTHEmail],[TANoWorkTHEmail],[TANoShowResend],[TANoGoResend],[ExcludeFromHigherPayCalculation],[PayAsRostered], " +
                " [NDIA_LEVEL2],[NDIA_LEVEL3],[NDIA_LEVEL4],[ExcludeFromTimebands],[NDIATravel],[ExcludeFromConflicts],[ExcludeFromClientPortalDisplay],[Job], " + 
                " [Lifecycle],[NDIAClaimType] FROM ItemTypes "+query+" AND (RosterGroup IN ('ADMISSION')) ORDER BY Title";
                
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                Recnum = GenSettings.Filter(rd["Recnum"]),
                                row_num = GenSettings.Filter(rd["Row_num"]),
                                title = GenSettings.Filter(rd["Title"]),
                                accountingCode = GenSettings.Filter(rd["accounting Code"]),
                                billAmount = GenSettings.Filter(rd["Bill Amount"]),
                                billUnit = GenSettings.Filter(rd["Bill Unit"]),
                                datasetCode = GenSettings.Filter(rd["Dataset Code"]),
                                datasetGroup = GenSettings.Filter(rd["Dataset Group"]),
                                endDate = GenSettings.Filter(rd["End Date"]),
                                ndiaid = GenSettings.Filter(rd["NDIA ID"]),
                                outletID = GenSettings.Filter(rd["OutletID"]),
                                rosterGroupD = GenSettings.Filter(rd["RosterGroupD"]),
                                rosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                                minorGroup = GenSettings.Filter(rd["Sub Group"]),
                                status = GenSettings.Filter(rd["Status"]),
                                processClassification = GenSettings.Filter(rd["ProcessClassification"]),
                                dataSet         = GenSettings.Filter(rd["dicipline"]),
                                accountingIdentifier = GenSettings.Filter(rd["accounting Code"]),
                                amount          = GenSettings.Filter(rd["Bill Amount"]),
                                unit            = GenSettings.Filter(rd["Bill Unit"]),
                                IT_Dataset      = GenSettings.Filter(rd["DataSet"]),
                                haccType        = GenSettings.Filter(rd["Dataset Code"]),
                                dicipline = GenSettings.Filter(rd["dicipline"]),
                                glRevenue = GenSettings.Filter(rd["GLRevenue"]),
                                budgetAmount = GenSettings.Filter(rd["BudgetAmount"]),
                                budgetPeriod = GenSettings.Filter(rd["BudgetPeriod"]),
                                payGroup = GenSettings.Filter(rd["PayGroup"]),
                                payType = GenSettings.Filter(rd["PayType"]),
                                itemType = GenSettings.Filter(rd["ItemType"]),
                                price2 = GenSettings.Filter(rd["Price2"]),
                                price3 = GenSettings.Filter(rd["Price3"]),
                                price4 = GenSettings.Filter(rd["Price4"]),
                                price5 = GenSettings.Filter(rd["Price5"]),
                                price6 = GenSettings.Filter(rd["Price6"]),
                                mainGroup = GenSettings.Filter(rd["MainGroup"]),
                                autoRecipientDetails = GenSettings.Filter(rd["AutoRecipientDetails"]),
                                autoActivityNotes = GenSettings.Filter(rd["AutoActivityNotes"]),
                                defaultAddress = GenSettings.Filter(rd["DefaultAddress"]),
                                defaultPhone  = GenSettings.Filter(rd["DefaultPhone"]),
                                activityNotes = GenSettings.Filter(rd["ActivityNotes"]),
                                glCost = GenSettings.Filter(rd["GLCost"]),
                                unitCostUOM = GenSettings.Filter(rd["UnitCostUOM"]),
                                unitCost = GenSettings.Filter(rd["UnitCost"]),
                                infoOnly = GenSettings.Filter(rd["InfoOnly"]),
                                autoApprove = GenSettings.Filter(rd["AutoApprove"]),
                                excludeFromPayExport = GenSettings.Filter(rd["ExcludeFromPayExport"]),
                                budgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
                                defaultPayOnLeaveID = GenSettings.Filter(rd["DefaultPayOnLeaveID"]),
                                defaultActivityOnLeaveID = GenSettings.Filter(rd["DefaultActivityOnLeaveID"]),
                                defaultProgramOnLeaveID = GenSettings.Filter(rd["DefaultProgramOnLeaveID"]),
                                billText = GenSettings.Filter(rd["BillText"]),
                                noChangeDate = GenSettings.Filter(rd["NoChangeDate"]),
                                noChangeTime = GenSettings.Filter(rd["NoChangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                jobType  = GenSettings.Filter(rd["JobType"]),
                                excludeFromUsageStatements = GenSettings.Filter(rd["ExcludeFromUsageStatements"]),
                                taexclude1 = GenSettings.Filter(rd["TAExclude1"]),
                                minChargeRate = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday = GenSettings.Filter(rd["NoThursday"]),
                                noFriday = GenSettings.Filter(rd["NoFriday"]),
                                noSunday = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday  = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt = GenSettings.Filter(rd["JobSheetPrompt"]),
                                excludeFromInterpretation = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                excludeFromAutoLeave = GenSettings.Filter(rd["ExcludeFromAutoLeave"]),
                                cdcFee = GenSettings.Filter(rd["CDCFee"]),
                                appExclude1 = GenSettings.Filter(rd["AppExclude1"]),
                                tA_LOGINMODE= GenSettings.Filter(rd["TA_LOGINMODE"]),
                                tA_EXCLUDEGEOLOCATION = GenSettings.Filter(rd["TA_EXCLUDEGEOLOCATION"]),
                                dexid = GenSettings.Filter(rd["DEXID"]),
                                excludeFromTravelCalc = GenSettings.Filter(rd["ExcludeFromTravelCalc"]),
                                fixedTime = GenSettings.Filter(rd["FixedTime"]),
                                noOvertimeAccumulation = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                dmColor = GenSettings.Filter(rd["DMColor"]),
                                retainOriginalDataset = GenSettings.Filter(rd["RetainOriginalDataset"]),
                                minDurtn = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn = GenSettings.Filter(rd["MaxDurtn"]),
                                ndiaPriceType = GenSettings.Filter(rd["NDIAPriceType"]),
                                TAEarlyStartTHWho = GenSettings.Filter(rd["TAEarlyStartTHWho"]),
                                TAEarlyFinishTHWho = GenSettings.Filter(rd["TAEarlyFinishTHWho"]),
                                TALateFinishTHWho = GenSettings.Filter(rd["TALateFinishTHWho"]),
                                TALateStartTHWho = GenSettings.Filter(rd["TALateStartTHWho"]),
                                TAOverstayTHWho = GenSettings.Filter(rd["TAOverstayTHWho"]),
                                TAUnderstayTHWho = GenSettings.Filter(rd["TAUnderstayTHWho"]),
                                TANoWorkTHWho = GenSettings.Filter(rd["TANoWorkTHWho"]),
                                TAEarlyStartTH = GenSettings.Filter(rd["TAEarlyStartTH"]),
                                TAEarlyFinishTH = GenSettings.Filter(rd["TAEarlyFinishTH"]),
                                TALateFinishTH = GenSettings.Filter(rd["TALateFinishTH"]),
                                TALateStartTH = GenSettings.Filter(rd["TALateStartTH"]),
                                TAOverstayTH = GenSettings.Filter(rd["TAOverstayTH"]),
                                TAUnderstayTH = GenSettings.Filter(rd["TAUnderstayTH"]),
                                TANoWorkTH = GenSettings.Filter(rd["TANoWorkTH"]),
                                TAEarlyStartTHEmail = GenSettings.Filter(rd["TAEarlyStartTHEmail"]),
                                TAEarlyFinishTHEmail = GenSettings.Filter(rd["TAEarlyFinishTHEmail"]),
                                TALateFinishTHEmail = GenSettings.Filter(rd["TALateFinishTHEmail"]),
                                TALateStartTHEmail = GenSettings.Filter(rd["TALateStartTHEmail"]),
                                TAOverstayTHEmail = GenSettings.Filter(rd["TAOverstayTHEmail"]),
                                TAUnderstayTHEmail = GenSettings.Filter(rd["TAUnderstayTHEmail"]),
                                TANoWorkTHEmail = GenSettings.Filter(rd["TANoWorkTHEmail"]),
                                TANoShowResend = GenSettings.Filter(rd["TANoShowResend"]),
                                TANoGoResend = GenSettings.Filter(rd["TANoGoResend"]),
                                ExcludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                PayAsRostered = GenSettings.Filter(rd["PayAsRostered"]),
                                NDIA_LEVEL2 = GenSettings.Filter(rd["NDIA_LEVEL2"]),
                                NDIA_LEVEL3 = GenSettings.Filter(rd["NDIA_LEVEL3"]),
                                NDIA_LEVEL4  = GenSettings.Filter(rd["NDIA_LEVEL4"]),
                                ExcludeFromTimebands = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                NDIATravel = GenSettings.Filter(rd["NDIATravel"]),
                                ExcludeFromConflicts = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                ExcludeFromClientPortalDisplay = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]),
                                Job = GenSettings.Filter(rd["Job"]),
                                Lifecycle = GenSettings.Filter(rd["Lifecycle"]),
                                NDIAClaimType = GenSettings.Filter(rd["NDIAClaimType"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        
        [HttpPost("caseManagement")]
        public async Task<IActionResult> caseManagement([FromBody] ItemTypes casemanage)
        {
           
            if(!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                return BadRequest(errors);
            }
            try
            {
                using (var db = _context)
                {
                    await db.AddRangeAsync(casemanage);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPut("caseManagement")]
        public async Task<IActionResult> UpdatecaseManagement([FromBody] ItemTypes activities)
        {
            try
            {
                using (var db = _context)
                {
                    var singleActivity = (from rp in _context.ItemTypes
                                where rp.Recnum == activities.Recnum
                                select rp).FirstOrDefault();

                    if (singleActivity == null) return BadRequest();

                     //done
                    singleActivity.Title           =   activities.Title;
                    singleActivity.BillText        =   activities.BillText;
                    singleActivity.Status          =   activities.Status;
                    singleActivity.RosterGroup     =   activities.RosterGroup;
                    singleActivity.ProcessClassification = activities.ProcessClassification;
                    singleActivity.MinorGroup      =   activities.MinorGroup;
                    singleActivity.Amount          =   activities.Amount;
                    singleActivity.MinChargeRate   =   activities.MinChargeRate;
                    singleActivity.Unit            =   activities.Unit;
                    singleActivity.Lifecycle       =   activities.Lifecycle;
                    singleActivity.BudgetGroup     =   activities.BudgetGroup;
                    singleActivity.dataSet         =   activities.dataSet;
                    singleActivity.AutoApprove     =   activities.AutoApprove;
                    singleActivity.ExcludeFromAutoLeave = activities.ExcludeFromAutoLeave;
                    singleActivity.InfoOnly             = activities.InfoOnly;
                    //done
                    singleActivity.AccountingIdentifier  = activities.AccountingIdentifier;
                    singleActivity.GLRevenue             = activities.GLRevenue;
                    singleActivity.Job                   = activities.Job;
                    singleActivity.UnitCostUOM           = activities.UnitCostUOM;
                    singleActivity.UnitCost              = activities.UnitCost;
                    singleActivity.ExcludeFromPayExport    = activities.ExcludeFromPayExport;
                    singleActivity.ExcludeFromUsageStatements  = activities.ExcludeFromUsageStatements;
                    //done 
                    singleActivity.Price2               = activities.Price2;
                    singleActivity.Price3               = activities.Price3;
                    singleActivity.Price4               = activities.Price4;
                    singleActivity.Price5               = activities.Price5;
                    singleActivity.Price6               = activities.Price6;
                    singleActivity.excludeFromConflicts = activities.excludeFromConflicts; //new
                    //done
                    singleActivity.noMonday             = activities.noMonday;
                    singleActivity.noTuesday            = activities.noTuesday;
                    singleActivity.noWednesday          = activities.noWednesday;
                    singleActivity.noThursday           = activities.noThursday;
                    singleActivity.noFriday             = activities.noFriday;
                    singleActivity.noSaturday           = activities.noSaturday;
                    singleActivity.noSunday             = activities.noSunday;
                    singleActivity.noPubHol             = activities.noPubHol;
                    //done
                    singleActivity.StartTimeLimit       = activities.StartTimeLimit;
                    singleActivity.EndTimeLimit         = activities.EndTimeLimit;
                    singleActivity.MinDurtn             = activities.MinDurtn;
                    singleActivity.MaxDurtn             = activities.MaxDurtn;
                    singleActivity.FixedTime            = activities.FixedTime;
                    singleActivity.NoChangeDate         = activities.NoChangeDate;
                    singleActivity.NoChangeTime         = activities.NoChangeTime;
                    singleActivity.TimeChangeLimit      = activities.TimeChangeLimit;
                    //done
                    singleActivity.AutoRecipientDetails = activities.AutoRecipientDetails; 
                    singleActivity.DefaultAddress = activities.DefaultAddress; 
                    singleActivity.DefaultPhone = activities.DefaultPhone; 
                    singleActivity.AutoActivityNotes = activities.AutoActivityNotes; 
                    singleActivity.JobSheetPrompt = activities.JobSheetPrompt; 
                    singleActivity.ActivityNotes = activities.ActivityNotes; 
                    //done
                    singleActivity.IT_Dataset            = activities.IT_Dataset;
                    singleActivity.datasetGroup          = activities.datasetGroup;
                    singleActivity.HACCType              = activities.HACCType;
                    singleActivity.NDIAPriceType         = activities.NDIAPriceType;
                    singleActivity.NDIAClaimType         = activities.NDIAClaimType;
                    singleActivity.NDIATravel            = activities.NDIATravel;
                    singleActivity.NDIA_LEVEL2           = activities.NDIA_LEVEL2;
                    singleActivity.NDIA_LEVEL3           = activities.NDIA_LEVEL3;
                    singleActivity.NDIA_LEVEL4           = activities.NDIA_LEVEL4;
                    singleActivity.ALT_NDIANonLabTravelKmActivity = activities.ALT_NDIANonLabTravelKmActivity;
                    singleActivity.ALT_AppKmWithinActivity  = activities.ALT_AppKmWithinActivity;


                    //DONE
                    singleActivity.HACCUse          = activities.HACCUse;
                    singleActivity.CSTDAUse         = activities.CSTDAUse;
                    singleActivity.NRCPUse          = activities.NRCPUse;
                    singleActivity.ExcludeFromRecipSummarySheet = activities.ExcludeFromRecipSummarySheet;
                    singleActivity.ExcludeFromHigherPayCalculation = activities.ExcludeFromHigherPayCalculation;
                    singleActivity.NoOvertimeAccumulation = activities.NoOvertimeAccumulation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.ExcludeFromInterpretation = activities.ExcludeFromInterpretation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.JobType = activities.JobType;
                    singleActivity.TA_LOGINMODE = activities.TA_LOGINMODE;
                    singleActivity.ExcludeFromClientPortalDisplay = activities.ExcludeFromClientPortalDisplay;
                    singleActivity.ExcludeFromTravelCalc = activities.ExcludeFromTravelCalc;
                    singleActivity.TA_EXCLUDEGEOLOCATION = activities.TA_EXCLUDEGEOLOCATION;
                    singleActivity.AppExclude1 = activities.AppExclude1;
                    singleActivity.TAExclude1 = activities.TAExclude1;

                    singleActivity.TAEarlyStartTHEmail = activities.TAEarlyStartTHEmail;
                    singleActivity.TALateStartTHEmail = activities.TALateStartTHEmail;
                    singleActivity.TALateStartTH = activities.TALateStartTH;
                    singleActivity.TAEarlyStartTH = activities.TAEarlyStartTH;
                    singleActivity.TAEarlyStartTHWho = activities.TAEarlyStartTHWho;
                    singleActivity.TALateStartTHWho = activities.TALateStartTHWho;

                    singleActivity.TANoGoResend = activities.TANoGoResend;
                    singleActivity.TANoShowResend = activities.TANoShowResend;
                    singleActivity.TAEarlyFinishTHEmail = activities.TAEarlyFinishTHEmail;
                    singleActivity.TALateFinishTHEmail = activities.TALateFinishTHEmail;

                    singleActivity.TAEarlyFinishTH = activities.TAEarlyFinishTH;
                    singleActivity.TALateFinishTH = activities.TALateFinishTH;
                    singleActivity.TALateFinishTHWho = activities.TALateFinishTHWho;
                    singleActivity.TAEarlyFinishTHWho = activities.TAEarlyFinishTHWho;

                    singleActivity.TANoWorkTHEmail = activities.TANoWorkTHEmail;
                    singleActivity.TAUnderstayTHEmail = activities.TAUnderstayTHEmail;
                    singleActivity.TAOverstayTHEmail = activities.TAOverstayTHEmail;
                    singleActivity.TANoWorkTHWho = activities.TANoWorkTHWho;

                    singleActivity.TAOverstayTHWho = activities.TAOverstayTHWho;
                    singleActivity.TANoWorkTH = activities.TANoWorkTH;
                    singleActivity.TAUnderstayTH = activities.TAUnderstayTH;
                    singleActivity.TAOverstayTH = activities.TAOverstayTH;
                    
                    singleActivity.EndDate              = activities.EndDate;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpGet("services/{is_where}")]
        public async Task<IActionResult> GetGetServices(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE ProcessClassification <> 'INPUT' ";
                }else{
                    query = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) ";
                }

                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Title]) AS Row_num,[Recnum] As Recnum, [Title] As [Title], CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [RosterGroupD],[RosterGroup],"+
                " [MinorGroup] As [Sub Group],[IT_Dataset] As [Dataset],[HACCType] As [Dataset Code],[CSTDAOutletID] As [OutletID],[DatasetGroup] As [Dataset Group],[NDIA_ID] As [NDIA ID],[AccountingIdentifier] As [Accounting Code],[Amount] As [Bill Amount],[Unit] As [Bill Unit],[EndDate] As [End Date],"+
                " [Status],[ProcessClassification],[DataSet] as dicipline,[DatasetGroup],[GLRevenue]," + 
                " [BudgetAmount],[BudgetPeriod],[PayGroup],[PayType],[ItemType],[Price2],[Price3],[Price4],[Price5],[Price6],[MainGroup],[AutoRecipientDetails], " + 
                " [AutoActivityNotes],[DefaultAddress],[DefaultPhone],[ActivityNotes],[GLCost],[UnitCostUOM],[UnitCost],[InfoOnly],[AutoApprove], " + 
                " [ExcludeFromPayExport],[BudgetGroup],[DefaultPayOnLeaveID],[DefaultActivityOnLeaveID],[DefaultProgramOnLeaveID],[BillText],[NoChangeDate], " + 
                " [NoChangeTime],[TimeChangeLimit],[JobType],[ExcludeFromUsageStatements],[TAExclude1],[MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday], " + 
                " [NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday],[NoPubHol],[JobSheetPrompt],[ExcludeFromInterpretation], " + 
                " [ExcludeFromAutoLeave],[CDCFee],[AppExclude1],[TA_LOGINMODE],[TA_EXCLUDEGEOLOCATION],[DEXID],[ExcludeFromTravelCalc],[FixedTime], " + 
                " [NoOvertimeAccumulation],[DMColor],[RetainOriginalDataset],[MinDurtn],[MaxDurtn],[NDIAPriceType],[TAEarlyStartTHWho],[TAEarlyFinishTHWho], " +
                " [TALateFinishTHWho],[TALateStartTHWho],[TAOverstayTHWho],[TAUnderstayTHWho],[TANoWorkTHWho],[TAEarlyStartTH],[TAEarlyFinishTH],[TALateFinishTH], " + 
                " [TALateStartTH],[TAOverstayTH],[TAUnderstayTH],[TANoWorkTH],[TAEarlyStartTHEmail],[TAEarlyFinishTHEmail],[TALateFinishTHEmail],[TALateStartTHEmail], " +
                " [TAOverstayTHEmail],[TAUnderstayTHEmail],[TANoWorkTHEmail],[TANoShowResend],[TANoGoResend],[ExcludeFromHigherPayCalculation],[PayAsRostered], " +
                " [NDIA_LEVEL2],[NDIA_LEVEL3],[NDIA_LEVEL4],[ExcludeFromTimebands],[NDIATravel],[ExcludeFromConflicts],[ExcludeFromClientPortalDisplay],[Job], " + 
                " [Lifecycle],[NDIAClaimType] FROM ItemTypes "+query+" AND (RosterGroup IN ('ONEONONE', 'CENTREBASED', 'GROUPACTIVITY', 'TRANSPORT','SLEEPOVER') " + 
                " AND MinorGroup <> 'MEALS') order by Title";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                Recnum = GenSettings.Filter(rd["Recnum"]),
                                row_num = GenSettings.Filter(rd["Row_num"]),
                                title = GenSettings.Filter(rd["Title"]),
                                accountingCode = GenSettings.Filter(rd["accounting Code"]),
                                billAmount = GenSettings.Filter(rd["Bill Amount"]),
                                billUnit = GenSettings.Filter(rd["Bill Unit"]),
                                datasetCode = GenSettings.Filter(rd["Dataset Code"]),
                                datasetGroup = GenSettings.Filter(rd["Dataset Group"]),
                                endDate = GenSettings.Filter(rd["End Date"]),
                                ndiaid = GenSettings.Filter(rd["NDIA ID"]),
                                outletID = GenSettings.Filter(rd["OutletID"]),
                                rosterGroupD = GenSettings.Filter(rd["RosterGroupD"]),
                                rosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                                minorGroup = GenSettings.Filter(rd["Sub Group"]),
                                status = GenSettings.Filter(rd["Status"]),
                                processClassification = GenSettings.Filter(rd["ProcessClassification"]),
                                dataSet         = GenSettings.Filter(rd["dicipline"]),
                                accountingIdentifier = GenSettings.Filter(rd["accounting Code"]),
                                amount          = GenSettings.Filter(rd["Bill Amount"]),
                                unit            = GenSettings.Filter(rd["Bill Unit"]),
                                IT_Dataset      = GenSettings.Filter(rd["DataSet"]),
                                haccType        = GenSettings.Filter(rd["Dataset Code"]),
                                dicipline = GenSettings.Filter(rd["dicipline"]),
                                glRevenue = GenSettings.Filter(rd["GLRevenue"]),
                                budgetAmount = GenSettings.Filter(rd["BudgetAmount"]),
                                budgetPeriod = GenSettings.Filter(rd["BudgetPeriod"]),
                                payGroup = GenSettings.Filter(rd["PayGroup"]),
                                payType = GenSettings.Filter(rd["PayType"]),
                                itemType = GenSettings.Filter(rd["ItemType"]),
                                price2 = GenSettings.Filter(rd["Price2"]),
                                price3 = GenSettings.Filter(rd["Price3"]),
                                price4 = GenSettings.Filter(rd["Price4"]),
                                price5 = GenSettings.Filter(rd["Price5"]),
                                price6 = GenSettings.Filter(rd["Price6"]),
                                mainGroup = GenSettings.Filter(rd["MainGroup"]),
                                autoRecipientDetails = GenSettings.Filter(rd["AutoRecipientDetails"]),
                                autoActivityNotes = GenSettings.Filter(rd["AutoActivityNotes"]),
                                defaultAddress = GenSettings.Filter(rd["DefaultAddress"]),
                                defaultPhone  = GenSettings.Filter(rd["DefaultPhone"]),
                                activityNotes = GenSettings.Filter(rd["ActivityNotes"]),
                                glCost = GenSettings.Filter(rd["GLCost"]),
                                unitCostUOM = GenSettings.Filter(rd["UnitCostUOM"]),
                                unitCost = GenSettings.Filter(rd["UnitCost"]),
                                infoOnly = GenSettings.Filter(rd["InfoOnly"]),
                                autoApprove = GenSettings.Filter(rd["AutoApprove"]),
                                excludeFromPayExport = GenSettings.Filter(rd["ExcludeFromPayExport"]),
                                budgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
                                defaultPayOnLeaveID = GenSettings.Filter(rd["DefaultPayOnLeaveID"]),
                                defaultActivityOnLeaveID = GenSettings.Filter(rd["DefaultActivityOnLeaveID"]),
                                defaultProgramOnLeaveID = GenSettings.Filter(rd["DefaultProgramOnLeaveID"]),
                                billText = GenSettings.Filter(rd["BillText"]),
                                noChangeDate = GenSettings.Filter(rd["NoChangeDate"]),
                                noChangeTime = GenSettings.Filter(rd["NoChangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                jobType  = GenSettings.Filter(rd["JobType"]),
                                excludeFromUsageStatements = GenSettings.Filter(rd["ExcludeFromUsageStatements"]),
                                taexclude1 = GenSettings.Filter(rd["TAExclude1"]),
                                minChargeRate = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday = GenSettings.Filter(rd["NoThursday"]),
                                noFriday = GenSettings.Filter(rd["NoFriday"]),
                                noSunday = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday  = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt = GenSettings.Filter(rd["JobSheetPrompt"]),
                                excludeFromInterpretation = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                excludeFromAutoLeave = GenSettings.Filter(rd["ExcludeFromAutoLeave"]),
                                cdcFee = GenSettings.Filter(rd["CDCFee"]),
                                appExclude1 = GenSettings.Filter(rd["AppExclude1"]),
                                tA_LOGINMODE= GenSettings.Filter(rd["TA_LOGINMODE"]),
                                tA_EXCLUDEGEOLOCATION = GenSettings.Filter(rd["TA_EXCLUDEGEOLOCATION"]),
                                dexid = GenSettings.Filter(rd["DEXID"]),
                                excludeFromTravelCalc = GenSettings.Filter(rd["ExcludeFromTravelCalc"]),
                                fixedTime = GenSettings.Filter(rd["FixedTime"]),
                                noOvertimeAccumulation = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                dmColor = GenSettings.Filter(rd["DMColor"]),
                                retainOriginalDataset = GenSettings.Filter(rd["RetainOriginalDataset"]),
                                minDurtn = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn = GenSettings.Filter(rd["MaxDurtn"]),
                                ndiaPriceType = GenSettings.Filter(rd["NDIAPriceType"]),
                                TAEarlyStartTHWho = GenSettings.Filter(rd["TAEarlyStartTHWho"]),
                                TAEarlyFinishTHWho = GenSettings.Filter(rd["TAEarlyFinishTHWho"]),
                                TALateFinishTHWho = GenSettings.Filter(rd["TALateFinishTHWho"]),
                                TALateStartTHWho = GenSettings.Filter(rd["TALateStartTHWho"]),
                                TAOverstayTHWho = GenSettings.Filter(rd["TAOverstayTHWho"]),
                                TAUnderstayTHWho = GenSettings.Filter(rd["TAUnderstayTHWho"]),
                                TANoWorkTHWho = GenSettings.Filter(rd["TANoWorkTHWho"]),
                                TAEarlyStartTH = GenSettings.Filter(rd["TAEarlyStartTH"]),
                                TAEarlyFinishTH = GenSettings.Filter(rd["TAEarlyFinishTH"]),
                                TALateFinishTH = GenSettings.Filter(rd["TALateFinishTH"]),
                                TALateStartTH = GenSettings.Filter(rd["TALateStartTH"]),
                                TAOverstayTH = GenSettings.Filter(rd["TAOverstayTH"]),
                                TAUnderstayTH = GenSettings.Filter(rd["TAUnderstayTH"]),
                                TANoWorkTH = GenSettings.Filter(rd["TANoWorkTH"]),
                                TAEarlyStartTHEmail = GenSettings.Filter(rd["TAEarlyStartTHEmail"]),
                                TAEarlyFinishTHEmail = GenSettings.Filter(rd["TAEarlyFinishTHEmail"]),
                                TALateFinishTHEmail = GenSettings.Filter(rd["TALateFinishTHEmail"]),
                                TALateStartTHEmail = GenSettings.Filter(rd["TALateStartTHEmail"]),
                                TAOverstayTHEmail = GenSettings.Filter(rd["TAOverstayTHEmail"]),
                                TAUnderstayTHEmail = GenSettings.Filter(rd["TAUnderstayTHEmail"]),
                                TANoWorkTHEmail = GenSettings.Filter(rd["TANoWorkTHEmail"]),
                                TANoShowResend = GenSettings.Filter(rd["TANoShowResend"]),
                                TANoGoResend = GenSettings.Filter(rd["TANoGoResend"]),
                                ExcludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                PayAsRostered = GenSettings.Filter(rd["PayAsRostered"]),
                                NDIA_LEVEL2 = GenSettings.Filter(rd["NDIA_LEVEL2"]),
                                NDIA_LEVEL3 = GenSettings.Filter(rd["NDIA_LEVEL3"]),
                                NDIA_LEVEL4  = GenSettings.Filter(rd["NDIA_LEVEL4"]),
                                ExcludeFromTimebands = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                NDIATravel = GenSettings.Filter(rd["NDIATravel"]),
                                ExcludeFromConflicts = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                ExcludeFromClientPortalDisplay = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]),
                                Job = GenSettings.Filter(rd["Job"]),
                                Lifecycle = GenSettings.Filter(rd["Lifecycle"]),
                                NDIAClaimType = GenSettings.Filter(rd["NDIAClaimType"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        
        [HttpPost("services")]
        public async Task<IActionResult> services([FromBody] ItemTypes services)
        {
            if(!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                return BadRequest(errors);
            }
            try
            {
                using (var db = _context)
                {
                    await db.AddRangeAsync(services);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPut("services")]
        public async Task<IActionResult> Updateservices([FromBody] ItemTypes activities)
        {
            try
            {
                using (var db = _context)
                {
                    var singleActivity = (from rp in _context.ItemTypes
                                where rp.Recnum == activities.Recnum
                                select rp).FirstOrDefault();

                    if (singleActivity == null) return BadRequest();

                     //done
                    singleActivity.Title           =   activities.Title;
                    singleActivity.BillText        =   activities.BillText;
                    singleActivity.Status          =   activities.Status;
                    singleActivity.RosterGroup     =   activities.RosterGroup;
                    singleActivity.ProcessClassification = activities.ProcessClassification;
                    singleActivity.MinorGroup      =   activities.MinorGroup;
                    singleActivity.Amount          =   activities.Amount;
                    singleActivity.MinChargeRate   =   activities.MinChargeRate;
                    singleActivity.Unit            =   activities.Unit;
                    singleActivity.Lifecycle       =   activities.Lifecycle;
                    singleActivity.BudgetGroup     =   activities.BudgetGroup;
                    singleActivity.dataSet         =   activities.dataSet;
                    singleActivity.AutoApprove     =   activities.AutoApprove;
                    singleActivity.ExcludeFromAutoLeave = activities.ExcludeFromAutoLeave;
                    singleActivity.InfoOnly             = activities.InfoOnly;
                    //done
                    singleActivity.AccountingIdentifier  = activities.AccountingIdentifier;
                    singleActivity.GLRevenue             = activities.GLRevenue;
                    singleActivity.Job                   = activities.Job;
                    singleActivity.UnitCostUOM           = activities.UnitCostUOM;
                    singleActivity.UnitCost              = activities.UnitCost;
                    singleActivity.ExcludeFromPayExport    = activities.ExcludeFromPayExport;
                    singleActivity.ExcludeFromUsageStatements  = activities.ExcludeFromUsageStatements;
                    //done 
                    singleActivity.Price2               = activities.Price2;
                    singleActivity.Price3               = activities.Price3;
                    singleActivity.Price4               = activities.Price4;
                    singleActivity.Price5               = activities.Price5;
                    singleActivity.Price6               = activities.Price6;
                    singleActivity.excludeFromConflicts = activities.excludeFromConflicts; //new
                    //done
                    singleActivity.noMonday             = activities.noMonday;
                    singleActivity.noTuesday            = activities.noTuesday;
                    singleActivity.noWednesday          = activities.noWednesday;
                    singleActivity.noThursday           = activities.noThursday;
                    singleActivity.noFriday             = activities.noFriday;
                    singleActivity.noSaturday           = activities.noSaturday;
                    singleActivity.noSunday             = activities.noSunday;
                    singleActivity.noPubHol             = activities.noPubHol;
                    //done
                    singleActivity.StartTimeLimit       = activities.StartTimeLimit;
                    singleActivity.EndTimeLimit         = activities.EndTimeLimit;
                    singleActivity.MinDurtn             = activities.MinDurtn;
                    singleActivity.MaxDurtn             = activities.MaxDurtn;
                    singleActivity.FixedTime            = activities.FixedTime;
                    singleActivity.NoChangeDate         = activities.NoChangeDate;
                    singleActivity.NoChangeTime         = activities.NoChangeTime;
                    singleActivity.TimeChangeLimit      = activities.TimeChangeLimit;
                    //done
                    singleActivity.AutoRecipientDetails = activities.AutoRecipientDetails; 
                    singleActivity.DefaultAddress = activities.DefaultAddress; 
                    singleActivity.DefaultPhone = activities.DefaultPhone; 
                    singleActivity.AutoActivityNotes = activities.AutoActivityNotes; 
                    singleActivity.JobSheetPrompt = activities.JobSheetPrompt; 
                    singleActivity.ActivityNotes = activities.ActivityNotes; 
                    //done
                    singleActivity.IT_Dataset            = activities.IT_Dataset;
                    singleActivity.datasetGroup          = activities.datasetGroup;
                    singleActivity.HACCType              = activities.HACCType;
                    singleActivity.NDIAPriceType         = activities.NDIAPriceType;
                    singleActivity.NDIAClaimType         = activities.NDIAClaimType;
                    singleActivity.NDIATravel            = activities.NDIATravel;
                    singleActivity.NDIA_LEVEL2           = activities.NDIA_LEVEL2;
                    singleActivity.NDIA_LEVEL3           = activities.NDIA_LEVEL3;
                    singleActivity.NDIA_LEVEL4           = activities.NDIA_LEVEL4;
                    singleActivity.ALT_NDIANonLabTravelKmActivity = activities.ALT_NDIANonLabTravelKmActivity;
                    singleActivity.ALT_AppKmWithinActivity  = activities.ALT_AppKmWithinActivity;


                    //DONE
                    singleActivity.HACCUse          = activities.HACCUse;
                    singleActivity.CSTDAUse         = activities.CSTDAUse;
                    singleActivity.NRCPUse          = activities.NRCPUse;
                    singleActivity.ExcludeFromRecipSummarySheet = activities.ExcludeFromRecipSummarySheet;
                    singleActivity.ExcludeFromHigherPayCalculation = activities.ExcludeFromHigherPayCalculation;
                    singleActivity.NoOvertimeAccumulation = activities.NoOvertimeAccumulation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.ExcludeFromInterpretation = activities.ExcludeFromInterpretation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.JobType = activities.JobType;
                    singleActivity.TA_LOGINMODE = activities.TA_LOGINMODE;
                    singleActivity.ExcludeFromClientPortalDisplay = activities.ExcludeFromClientPortalDisplay;
                    singleActivity.ExcludeFromTravelCalc = activities.ExcludeFromTravelCalc;
                    singleActivity.TA_EXCLUDEGEOLOCATION = activities.TA_EXCLUDEGEOLOCATION;
                    singleActivity.AppExclude1 = activities.AppExclude1;
                    singleActivity.TAExclude1 = activities.TAExclude1;

                    singleActivity.TAEarlyStartTHEmail = activities.TAEarlyStartTHEmail;
                    singleActivity.TALateStartTHEmail = activities.TALateStartTHEmail;
                    singleActivity.TALateStartTH = activities.TALateStartTH;
                    singleActivity.TAEarlyStartTH = activities.TAEarlyStartTH;
                    singleActivity.TAEarlyStartTHWho = activities.TAEarlyStartTHWho;
                    singleActivity.TALateStartTHWho = activities.TALateStartTHWho;

                    singleActivity.TANoGoResend = activities.TANoGoResend;
                    singleActivity.TANoShowResend = activities.TANoShowResend;
                    singleActivity.TAEarlyFinishTHEmail = activities.TAEarlyFinishTHEmail;
                    singleActivity.TALateFinishTHEmail = activities.TALateFinishTHEmail;

                    singleActivity.TAEarlyFinishTH = activities.TAEarlyFinishTH;
                    singleActivity.TALateFinishTH = activities.TALateFinishTH;
                    singleActivity.TALateFinishTHWho = activities.TALateFinishTHWho;
                    singleActivity.TAEarlyFinishTHWho = activities.TAEarlyFinishTHWho;

                    singleActivity.TANoWorkTHEmail = activities.TANoWorkTHEmail;
                    singleActivity.TAUnderstayTHEmail = activities.TAUnderstayTHEmail;
                    singleActivity.TAOverstayTHEmail = activities.TAOverstayTHEmail;
                    singleActivity.TANoWorkTHWho = activities.TANoWorkTHWho;

                    singleActivity.TAOverstayTHWho = activities.TAOverstayTHWho;
                    singleActivity.TANoWorkTH = activities.TANoWorkTH;
                    singleActivity.TAUnderstayTH = activities.TAUnderstayTH;
                    singleActivity.TAOverstayTH = activities.TAOverstayTH;
                    
                    singleActivity.EndDate              = activities.EndDate;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpGet("recipientAbsenses/{is_where}")]
        public async Task<IActionResult> GetRecipientAbsenses(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                 string query = string.Empty;
                if(is_where){
                    query = "WHERE ProcessClassification <> 'INPUT' ";
                }else{
                    query = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) ";
                }

                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Title]) AS Row_num,[Recnum] As [Recnum],[Title] As [Title],CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [RosterGroupD],[RosterGroup],"+
                " [MinorGroup] As [Sub Group],[IT_Dataset] As [Dataset],[HACCType] As [Dataset Code], [CSTDAOutletID] As [OutletID],[DatasetGroup] As [Dataset Group],[NDIA_ID] As [NDIA ID],[AccountingIdentifier] As [Accounting Code],[Amount] As [Bill Amount],[Unit] As [Bill Unit],[EndDate] As [End Date],"+
                " [Status],[ProcessClassification],[DataSet] as dicipline,[DatasetGroup],[GLRevenue],"+ 
                " [BudgetAmount],[BudgetPeriod],[PayGroup],[PayType],[ItemType],[Price2],[Price3],[Price4],[Price5],[Price6],[MainGroup],[AutoRecipientDetails], " + 
                " [AutoActivityNotes],[DefaultAddress],[DefaultPhone],[ActivityNotes],[GLCost],[UnitCostUOM],[UnitCost],[InfoOnly],[AutoApprove], " + 
                " [ExcludeFromPayExport],[BudgetGroup],[DefaultPayOnLeaveID],[DefaultActivityOnLeaveID],[DefaultProgramOnLeaveID],[BillText],[NoChangeDate], " + 
                " [NoChangeTime],[TimeChangeLimit],[JobType],[ExcludeFromUsageStatements],[TAExclude1],[MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday], " + 
                " [NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday],[NoPubHol],[JobSheetPrompt],[ExcludeFromInterpretation], " + 
                " [ExcludeFromAutoLeave],[CDCFee],[AppExclude1],[TA_LOGINMODE],[TA_EXCLUDEGEOLOCATION],[DEXID],[ExcludeFromTravelCalc],[FixedTime], " + 
                " [NoOvertimeAccumulation],[DMColor],[RetainOriginalDataset],[MinDurtn],[MaxDurtn],[NDIAPriceType],[TAEarlyStartTHWho],[TAEarlyFinishTHWho], " +
                " [TALateFinishTHWho],[TALateStartTHWho],[TAOverstayTHWho],[TAUnderstayTHWho],[TANoWorkTHWho],[TAEarlyStartTH],[TAEarlyFinishTH],[TALateFinishTH], " + 
                " [TALateStartTH],[TAOverstayTH],[TAUnderstayTH],[TANoWorkTH],[TAEarlyStartTHEmail],[TAEarlyFinishTHEmail],[TALateFinishTHEmail],[TALateStartTHEmail], " +
                " [TAOverstayTHEmail],[TAUnderstayTHEmail],[TANoWorkTHEmail],[TANoShowResend],[TANoGoResend],[ExcludeFromHigherPayCalculation],[PayAsRostered], " +
                " [NDIA_LEVEL2],[NDIA_LEVEL3],[NDIA_LEVEL4],[ExcludeFromTimebands],[NDIATravel],[ExcludeFromConflicts],[ExcludeFromClientPortalDisplay],[Job], " + 
                " [Lifecycle],[NDIAClaimType] FROM ItemTypes "+query+" AND ( RosterGroup IN ('RECPTABSENCE') ) ORDER BY Title";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                Recnum = GenSettings.Filter(rd["Recnum"]),
                                row_num = GenSettings.Filter(rd["Row_num"]),
                                title = GenSettings.Filter(rd["Title"]),
                                accountingCode = GenSettings.Filter(rd["accounting Code"]),
                                billAmount = GenSettings.Filter(rd["Bill Amount"]),
                                billUnit = GenSettings.Filter(rd["Bill Unit"]),
                                datasetCode = GenSettings.Filter(rd["Dataset Code"]),
                                datasetGroup = GenSettings.Filter(rd["Dataset Group"]),
                                endDate = GenSettings.Filter(rd["End Date"]),
                                ndiaid = GenSettings.Filter(rd["NDIA ID"]),
                                outletID = GenSettings.Filter(rd["OutletID"]),
                                rosterGroupD = GenSettings.Filter(rd["RosterGroupD"]),
                                rosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                                minorGroup = GenSettings.Filter(rd["Sub Group"]),
                                status = GenSettings.Filter(rd["Status"]),
                                processClassification = GenSettings.Filter(rd["ProcessClassification"]),
                                dataSet         = GenSettings.Filter(rd["dicipline"]),
                                accountingIdentifier = GenSettings.Filter(rd["accounting Code"]),
                                amount          = GenSettings.Filter(rd["Bill Amount"]),
                                unit            = GenSettings.Filter(rd["Bill Unit"]),
                                IT_Dataset      = GenSettings.Filter(rd["DataSet"]),
                                haccType        = GenSettings.Filter(rd["Dataset Code"]),
                                dicipline = GenSettings.Filter(rd["dicipline"]),
                                glRevenue = GenSettings.Filter(rd["GLRevenue"]),
                                budgetAmount = GenSettings.Filter(rd["BudgetAmount"]),
                                budgetPeriod = GenSettings.Filter(rd["BudgetPeriod"]),
                                payGroup = GenSettings.Filter(rd["PayGroup"]),
                                payType = GenSettings.Filter(rd["PayType"]),
                                itemType = GenSettings.Filter(rd["ItemType"]),
                                price2 = GenSettings.Filter(rd["Price2"]),
                                price3 = GenSettings.Filter(rd["Price3"]),
                                price4 = GenSettings.Filter(rd["Price4"]),
                                price5 = GenSettings.Filter(rd["Price5"]),
                                price6 = GenSettings.Filter(rd["Price6"]),
                                mainGroup = GenSettings.Filter(rd["MainGroup"]),
                                autoRecipientDetails = GenSettings.Filter(rd["AutoRecipientDetails"]),
                                autoActivityNotes = GenSettings.Filter(rd["AutoActivityNotes"]),
                                defaultAddress = GenSettings.Filter(rd["DefaultAddress"]),
                                defaultPhone  = GenSettings.Filter(rd["DefaultPhone"]),
                                activityNotes = GenSettings.Filter(rd["ActivityNotes"]),
                                glCost = GenSettings.Filter(rd["GLCost"]),
                                unitCostUOM = GenSettings.Filter(rd["UnitCostUOM"]),
                                unitCost = GenSettings.Filter(rd["UnitCost"]),
                                infoOnly = GenSettings.Filter(rd["InfoOnly"]),
                                autoApprove = GenSettings.Filter(rd["AutoApprove"]),
                                excludeFromPayExport = GenSettings.Filter(rd["ExcludeFromPayExport"]),
                                budgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
                                defaultPayOnLeaveID = GenSettings.Filter(rd["DefaultPayOnLeaveID"]),
                                defaultActivityOnLeaveID = GenSettings.Filter(rd["DefaultActivityOnLeaveID"]),
                                defaultProgramOnLeaveID = GenSettings.Filter(rd["DefaultProgramOnLeaveID"]),
                                billText = GenSettings.Filter(rd["BillText"]),
                                noChangeDate = GenSettings.Filter(rd["NoChangeDate"]),
                                noChangeTime = GenSettings.Filter(rd["NoChangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                jobType  = GenSettings.Filter(rd["JobType"]),
                                excludeFromUsageStatements = GenSettings.Filter(rd["ExcludeFromUsageStatements"]),
                                taexclude1 = GenSettings.Filter(rd["TAExclude1"]),
                                minChargeRate = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday = GenSettings.Filter(rd["NoThursday"]),
                                noFriday = GenSettings.Filter(rd["NoFriday"]),
                                noSunday = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday  = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt = GenSettings.Filter(rd["JobSheetPrompt"]),
                                excludeFromInterpretation = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                excludeFromAutoLeave = GenSettings.Filter(rd["ExcludeFromAutoLeave"]),
                                cdcFee = GenSettings.Filter(rd["CDCFee"]),
                                appExclude1 = GenSettings.Filter(rd["AppExclude1"]),
                                tA_LOGINMODE= GenSettings.Filter(rd["TA_LOGINMODE"]),
                                tA_EXCLUDEGEOLOCATION = GenSettings.Filter(rd["TA_EXCLUDEGEOLOCATION"]),
                                dexid = GenSettings.Filter(rd["DEXID"]),
                                excludeFromTravelCalc = GenSettings.Filter(rd["ExcludeFromTravelCalc"]),
                                fixedTime = GenSettings.Filter(rd["FixedTime"]),
                                noOvertimeAccumulation = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                dmColor = GenSettings.Filter(rd["DMColor"]),
                                retainOriginalDataset = GenSettings.Filter(rd["RetainOriginalDataset"]),
                                minDurtn = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn = GenSettings.Filter(rd["MaxDurtn"]),
                                ndiaPriceType = GenSettings.Filter(rd["NDIAPriceType"]),
                                TAEarlyStartTHWho = GenSettings.Filter(rd["TAEarlyStartTHWho"]),
                                TAEarlyFinishTHWho = GenSettings.Filter(rd["TAEarlyFinishTHWho"]),
                                TALateFinishTHWho = GenSettings.Filter(rd["TALateFinishTHWho"]),
                                TALateStartTHWho = GenSettings.Filter(rd["TALateStartTHWho"]),
                                TAOverstayTHWho = GenSettings.Filter(rd["TAOverstayTHWho"]),
                                TAUnderstayTHWho = GenSettings.Filter(rd["TAUnderstayTHWho"]),
                                TANoWorkTHWho = GenSettings.Filter(rd["TANoWorkTHWho"]),
                                TAEarlyStartTH = GenSettings.Filter(rd["TAEarlyStartTH"]),
                                TAEarlyFinishTH = GenSettings.Filter(rd["TAEarlyFinishTH"]),
                                TALateFinishTH = GenSettings.Filter(rd["TALateFinishTH"]),
                                TALateStartTH = GenSettings.Filter(rd["TALateStartTH"]),
                                TAOverstayTH = GenSettings.Filter(rd["TAOverstayTH"]),
                                TAUnderstayTH = GenSettings.Filter(rd["TAUnderstayTH"]),
                                TANoWorkTH = GenSettings.Filter(rd["TANoWorkTH"]),
                                TAEarlyStartTHEmail = GenSettings.Filter(rd["TAEarlyStartTHEmail"]),
                                TAEarlyFinishTHEmail = GenSettings.Filter(rd["TAEarlyFinishTHEmail"]),
                                TALateFinishTHEmail = GenSettings.Filter(rd["TALateFinishTHEmail"]),
                                TALateStartTHEmail = GenSettings.Filter(rd["TALateStartTHEmail"]),
                                TAOverstayTHEmail = GenSettings.Filter(rd["TAOverstayTHEmail"]),
                                TAUnderstayTHEmail = GenSettings.Filter(rd["TAUnderstayTHEmail"]),
                                TANoWorkTHEmail = GenSettings.Filter(rd["TANoWorkTHEmail"]),
                                TANoShowResend = GenSettings.Filter(rd["TANoShowResend"]),
                                TANoGoResend = GenSettings.Filter(rd["TANoGoResend"]),
                                ExcludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                PayAsRostered = GenSettings.Filter(rd["PayAsRostered"]),
                                NDIA_LEVEL2 = GenSettings.Filter(rd["NDIA_LEVEL2"]),
                                NDIA_LEVEL3 = GenSettings.Filter(rd["NDIA_LEVEL3"]),
                                NDIA_LEVEL4  = GenSettings.Filter(rd["NDIA_LEVEL4"]),
                                ExcludeFromTimebands = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                NDIATravel = GenSettings.Filter(rd["NDIATravel"]),
                                ExcludeFromConflicts = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                ExcludeFromClientPortalDisplay = GenSettings.Filter(rd["ExcludeFromClientPortalDisplay"]),
                                Job = GenSettings.Filter(rd["Job"]),
                                Lifecycle = GenSettings.Filter(rd["Lifecycle"]),
                                NDIAClaimType = GenSettings.Filter(rd["NDIAClaimType"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        
        [HttpPost("recipientAbsenses")]
        public async Task<IActionResult> RecipientAbsenses([FromBody] ItemTypes recipientAbsenses)
        {
            if(!ModelState.IsValid)
            {
                var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
                return BadRequest(errors);
            }
            try
            {
                using (var db = _context)
                {
                    await db.AddRangeAsync(recipientAbsenses);
                    await db.SaveChangesAsync();
                }
                return Ok(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpPut("recipientAbsenses")]
        public async Task<IActionResult> UpdaterecipientAbsenses([FromBody] ItemTypes activities)
        {
            try
            {
                using (var db = _context)
                {
                    var singleActivity = (from rp in _context.ItemTypes
                                where rp.Recnum == activities.Recnum
                                select rp).FirstOrDefault();

                    if (singleActivity == null) return BadRequest();

                     //done
                    singleActivity.Title           =   activities.Title;
                    singleActivity.BillText        =   activities.BillText;
                    singleActivity.Status          =   activities.Status;
                    singleActivity.RosterGroup     =   activities.RosterGroup;
                    singleActivity.ProcessClassification = activities.ProcessClassification;
                    singleActivity.MinorGroup      =   activities.MinorGroup;
                    singleActivity.Amount          =   activities.Amount;
                    singleActivity.MinChargeRate   =   activities.MinChargeRate;
                    singleActivity.Unit            =   activities.Unit;
                    singleActivity.Lifecycle       =   activities.Lifecycle;
                    singleActivity.BudgetGroup     =   activities.BudgetGroup;
                    singleActivity.dataSet         =   activities.dataSet;
                    singleActivity.AutoApprove     =   activities.AutoApprove;
                    singleActivity.ExcludeFromAutoLeave = activities.ExcludeFromAutoLeave;
                    singleActivity.InfoOnly             = activities.InfoOnly;
                    //done
                    singleActivity.AccountingIdentifier  = activities.AccountingIdentifier;
                    singleActivity.GLRevenue             = activities.GLRevenue;
                    singleActivity.Job                   = activities.Job;
                    singleActivity.UnitCostUOM           = activities.UnitCostUOM;
                    singleActivity.UnitCost              = activities.UnitCost;
                    singleActivity.ExcludeFromPayExport    = activities.ExcludeFromPayExport;
                    singleActivity.ExcludeFromUsageStatements  = activities.ExcludeFromUsageStatements;
                    //done 
                    singleActivity.Price2               = activities.Price2;
                    singleActivity.Price3               = activities.Price3;
                    singleActivity.Price4               = activities.Price4;
                    singleActivity.Price5               = activities.Price5;
                    singleActivity.Price6               = activities.Price6;
                    singleActivity.excludeFromConflicts = activities.excludeFromConflicts; //new
                    //done
                    singleActivity.noMonday             = activities.noMonday;
                    singleActivity.noTuesday            = activities.noTuesday;
                    singleActivity.noWednesday          = activities.noWednesday;
                    singleActivity.noThursday           = activities.noThursday;
                    singleActivity.noFriday             = activities.noFriday;
                    singleActivity.noSaturday           = activities.noSaturday;
                    singleActivity.noSunday             = activities.noSunday;
                    singleActivity.noPubHol             = activities.noPubHol;
                    //done
                    singleActivity.StartTimeLimit       = activities.StartTimeLimit;
                    singleActivity.EndTimeLimit         = activities.EndTimeLimit;
                    singleActivity.MinDurtn             = activities.MinDurtn;
                    singleActivity.MaxDurtn             = activities.MaxDurtn;
                    singleActivity.FixedTime            = activities.FixedTime;
                    singleActivity.NoChangeDate         = activities.NoChangeDate;
                    singleActivity.NoChangeTime         = activities.NoChangeTime;
                    singleActivity.TimeChangeLimit      = activities.TimeChangeLimit;
                    //done
                    singleActivity.AutoRecipientDetails = activities.AutoRecipientDetails; 
                    singleActivity.DefaultAddress = activities.DefaultAddress; 
                    singleActivity.DefaultPhone = activities.DefaultPhone; 
                    singleActivity.AutoActivityNotes = activities.AutoActivityNotes; 
                    singleActivity.JobSheetPrompt = activities.JobSheetPrompt; 
                    singleActivity.ActivityNotes = activities.ActivityNotes; 
                    //done
                    singleActivity.IT_Dataset            = activities.IT_Dataset;
                    singleActivity.datasetGroup          = activities.datasetGroup;
                    singleActivity.HACCType              = activities.HACCType;
                    singleActivity.NDIAPriceType         = activities.NDIAPriceType;
                    singleActivity.NDIAClaimType         = activities.NDIAClaimType;
                    singleActivity.NDIATravel            = activities.NDIATravel;
                    singleActivity.NDIA_LEVEL2           = activities.NDIA_LEVEL2;
                    singleActivity.NDIA_LEVEL3           = activities.NDIA_LEVEL3;
                    singleActivity.NDIA_LEVEL4           = activities.NDIA_LEVEL4;
                    singleActivity.ALT_NDIANonLabTravelKmActivity = activities.ALT_NDIANonLabTravelKmActivity;
                    singleActivity.ALT_AppKmWithinActivity  = activities.ALT_AppKmWithinActivity;


                    //DONE
                    singleActivity.HACCUse          = activities.HACCUse;
                    singleActivity.CSTDAUse         = activities.CSTDAUse;
                    singleActivity.NRCPUse          = activities.NRCPUse;
                    singleActivity.ExcludeFromRecipSummarySheet = activities.ExcludeFromRecipSummarySheet;
                    singleActivity.ExcludeFromHigherPayCalculation = activities.ExcludeFromHigherPayCalculation;
                    singleActivity.NoOvertimeAccumulation = activities.NoOvertimeAccumulation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.ExcludeFromInterpretation = activities.ExcludeFromInterpretation;
                    singleActivity.PayAsRostered = activities.PayAsRostered;
                    singleActivity.ExcludeFromTimebands = activities.ExcludeFromTimebands;
                    singleActivity.JobType = activities.JobType;
                    singleActivity.TA_LOGINMODE = activities.TA_LOGINMODE;
                    singleActivity.ExcludeFromClientPortalDisplay = activities.ExcludeFromClientPortalDisplay;
                    singleActivity.ExcludeFromTravelCalc = activities.ExcludeFromTravelCalc;
                    singleActivity.TA_EXCLUDEGEOLOCATION = activities.TA_EXCLUDEGEOLOCATION;
                    singleActivity.AppExclude1 = activities.AppExclude1;
                    singleActivity.TAExclude1 = activities.TAExclude1;

                    singleActivity.TAEarlyStartTHEmail = activities.TAEarlyStartTHEmail;
                    singleActivity.TALateStartTHEmail = activities.TALateStartTHEmail;
                    singleActivity.TALateStartTH = activities.TALateStartTH;
                    singleActivity.TAEarlyStartTH = activities.TAEarlyStartTH;
                    singleActivity.TAEarlyStartTHWho = activities.TAEarlyStartTHWho;
                    singleActivity.TALateStartTHWho = activities.TALateStartTHWho;

                    singleActivity.TANoGoResend = activities.TANoGoResend;
                    singleActivity.TANoShowResend = activities.TANoShowResend;
                    singleActivity.TAEarlyFinishTHEmail = activities.TAEarlyFinishTHEmail;
                    singleActivity.TALateFinishTHEmail = activities.TALateFinishTHEmail;

                    singleActivity.TAEarlyFinishTH = activities.TAEarlyFinishTH;
                    singleActivity.TALateFinishTH = activities.TALateFinishTH;
                    singleActivity.TALateFinishTHWho = activities.TALateFinishTHWho;
                    singleActivity.TAEarlyFinishTHWho = activities.TAEarlyFinishTHWho;

                    singleActivity.TANoWorkTHEmail = activities.TANoWorkTHEmail;
                    singleActivity.TAUnderstayTHEmail = activities.TAUnderstayTHEmail;
                    singleActivity.TAOverstayTHEmail = activities.TAOverstayTHEmail;
                    singleActivity.TANoWorkTHWho = activities.TANoWorkTHWho;

                    singleActivity.TAOverstayTHWho = activities.TAOverstayTHWho;
                    singleActivity.TANoWorkTH = activities.TANoWorkTH;
                    singleActivity.TAUnderstayTH = activities.TAUnderstayTH;
                    singleActivity.TAOverstayTH = activities.TAOverstayTH;
                    
                    singleActivity.EndDate              = activities.EndDate;

                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }
        [HttpGet("services/competency/{id}")]
        public async Task<IActionResult> GetCenterLocationCompetency(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, PersonID, Name AS Competency, UserYesNo1 AS Mandatory FROM HumanResources WHERE PersonID = @id AND [Type] = 'SVC_COMP' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                personID = GenSettings.Filter(rd["personID"]),
                                competency = GenSettings.Filter(rd["Competency"]),
                                mandatory = GenSettings.Filter(rd["Mandatory"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpPost("services/competency")]
        public async Task<IActionResult> PostServicesCompetency([FromBody] Competency competency)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HumanResources([PersonID],[Group],[Type],[Name],[Notes])
                        VALUES(@personID,'SVC_COMP','SVC_COMP',@name,@notes)", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@personID", competency.personID);
                        cmd.Parameters.AddWithValue("@name", competency.competencyValue);
                        cmd.Parameters.AddWithValue("@notes", competency.notes);
                        await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                    }
            }
        }

        [HttpPut("services/competency")]
        public async Task<IActionResult> UpdateServicesCompetency([FromBody] Competency competency)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE HumanResources SET [Name]=@name, [UserYesNo1]=@mandatory WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", competency.recordNumber);
                    cmd.Parameters.AddWithValue("@name", competency.competencyValue);
                    cmd.Parameters.AddWithValue("@mandatory", competency.mandatory);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("services/competency/{recordNo}")]
        public async Task<IActionResult> DeleteServicesCompetency(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE RecordNumber = @recordNo AND Type = 'SVC_COMP'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpGet("services/checklist/{id}")]
        public async Task<IActionResult> GetCenterLocationChecklist(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, PersonID, Name AS checklist FROM HumanResources WHERE PersonID = @id AND [Type] = 'CHECKLIST' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                personID     = GenSettings.Filter(rd["personID"]),
                                checklist    = GenSettings.Filter(rd["checklist"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("loggedOnDesktopUser")]
        public async Task<IActionResult> GetLoggedOnDesktopUsers(ApplicationUser user)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT * FROM (SELECT  DISTINCT Rtrim(loginame) AS [ServerLoginName], rtrim(nt_username) AS [WindowsUserName], rtrim(PROGRAM_NAME) AS [TraccsModule],
                hostname AS [WorkstationServer]  From MASTER.DBO.SYSPROCESSES   WHERE  PROGRAM_NAME IN ('TRACCS - MAIN MENU', 'TRACCS - Recipients', 'TRACCS - Day Manager', 'TRACCS - Rosters', 'TRACCS - Staff',
                'TRACCS - Timesheets', 'Reports')) t ORDER BY [ServerLoginName], [WindowsUserName]", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serverLoginName     = GenSettings.Filter(rd["ServerLoginName"]),
                                windowsUserName     = GenSettings.Filter(rd["WindowsUserName"]),
                                traccsModule        = GenSettings.Filter(rd["TraccsModule"]),
                                workstationServer   = GenSettings.Filter(rd["WorkstationServer"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("loggedOnAppUser")]
        public async Task<IActionResult> GetloggedOnAppUser(ApplicationUser user)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 MobilUserURL FROM Registration", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                mobilUserURL     = GenSettings.Filter(rd["MobilUserURL"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("licenseRegistration")]
        public async Task<IActionResult> GetLicenseRegistration(ApplicationUser user)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT adamas1,adamas2,adamas3,adamas4,adamas5,adamas6,adamas7,adamas8,adamas9 FROM registration", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                /* The above code is written in C# and appears to
                                be a comment block. It mentions a method or
                                function called "DecryptValue" but does not
                                provide any implementation details. It seems
                                like a placeholder or reminder for a
                                decryption operation that needs to be
                                implemented in the codebase. */
                                adamas1             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas1"]))),
                                adamas2             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas2"]))),
                                adamas3             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas3"]))),
                                adamas4             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas4"]))),
                                adamas5             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas5"]))),
                                adamas6             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas6"]))),
                                adamas7             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas7"]))),
                                adamas8             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas8"]))),
                                adamas9             = DecryptValue(DecryptValue(GenSettings.Filter(rd["adamas9"])))
                                
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
         [HttpPut("licenseRegistration")]
        public async Task<IActionResult> updatelicenseRegistration([FromBody] Registration activities)
        {
            try
            {
                using (var db = _context)
                {
                    
                    var agency = (from rp in _context.Registration
                                    where rp.SqlId == activities.SqlId
                                    select rp).FirstOrDefault();
                    
                     //  Console.WriteLine(agency.Adamas1);

                    if (agency == null) return BadRequest();

                    if(activities.NDIATravel == 1){
                    agency.Adamas1           =  EncryptValue(EncryptValue(activities.Adamas1));
                    agency.Adamas2           =  EncryptValue(EncryptValue(activities.Adamas2));
                    agency.Adamas3           =  EncryptValue(EncryptValue(activities.Adamas3));
                    }
                    if(activities.NDIATravel == 2){
                    agency.Adamas4           =   EncryptValue(EncryptValue(activities.Adamas4));
                    agency.Adamas5           =   EncryptValue(EncryptValue(activities.Adamas5));
                    agency.Adamas6           =   EncryptValue(EncryptValue(activities.Adamas6));
                    }
                    if(activities.NDIATravel == 3){
                    agency.Adamas7           =   EncryptValue(EncryptValue(activities.Adamas7));
                    agency.Adamas8           =   EncryptValue(EncryptValue(activities.Adamas8));
                    agency.Adamas9           =   EncryptValue(EncryptValue(activities.Adamas9));
                    }

                    await db.SaveChangesAsync();

                    return Ok(true);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            
        }
 public string EncryptValue(string strValue)
        {
            string DecryptValueRet = default;

            int i;
            string strTemp;
            i = 1;
            strTemp = "";
            try
            {
                while (i <= Strings.Len(strValue))
                {
                    strTemp = strTemp + Strings.Chr(Strings.Asc(Strings.Mid(strValue, i, 1)) + 50);
                    i = i + 1;
                }
                DecryptValueRet = strTemp;
                return DecryptValueRet;
            }
            catch (Exception ex)
            {
                DecryptValueRet = strTemp;
            }

            return DecryptValueRet;
        }
 public string DecryptValue(string strValue)
        {
            string DecryptValueRet = default;

            int i;
            string strTemp;
            i = 1;
            strTemp = "";
            try
            {
                while (i <= Strings.Len(strValue))
                {
                    strTemp = strTemp + Strings.Chr(Strings.Asc(Strings.Mid(strValue, i, 1)) - 50);
                    i = i + 1;
                }
                DecryptValueRet = strTemp;
                return DecryptValueRet;
            }
            catch (Exception ex)
            {
                DecryptValueRet = strTemp;
            }

            return DecryptValueRet;
        }

        public string DecryptRegistration(string encryptedStr)
        {
            string strTemp = string.Empty;
            int i = 1;
            while (i < encryptedStr.Length + 1)
            {
                var mid = Mid(encryptedStr, i, 1);
                var asc = Strings.Asc(mid);
                var chr = Strings.Chr(asc - 100);
                strTemp = strTemp + (chr).ToString();
                i++;
            }
            return strTemp;
        }

        public string EncryptRegistration(string toBeEncryptedValue)
        {
            string enryptedStr = string.Empty;

            for (var a = 0; a < toBeEncryptedValue.Length; a++)
            {
                byte[] bytes = new byte[1];
                bytes[0] = (byte)(Strings.AscW(toBeEncryptedValue[a]) + 50);
                enryptedStr = enryptedStr + new string(Encoding.GetEncoding(1252).GetChars(bytes));
            }
            return enryptedStr;
        }

        public static string Mid(string s, int a, int b)
        {
            string temp = s.Substring(a - 1, b);
            return temp;
        }
        

        [HttpPost("services/checklist")]
        public async Task<IActionResult> PostServicesChecklist([FromBody] Competency competency)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HumanResources([PersonID],[Group],[Type],[Name])
                        VALUES(@personID,'CHECKLIST','CHECKLIST',@name)", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@personID", competency.personID);
                        cmd.Parameters.AddWithValue("@name", competency.competencyValue);
                        await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                    }
            }
        }

        [HttpPut("services/checklist")]
        public async Task<IActionResult> UpdateServicesChecklist([FromBody] Competency competency)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE HumanResources SET [Name]=@name WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", competency.recordNumber);
                    cmd.Parameters.AddWithValue("@name", competency.competencyValue);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("services/checklist/{recordNo}")]
        public async Task<IActionResult> DeleteServicesChecklist(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE RecordNumber = @recordNo AND Type = 'CHECKLIST'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }


        [HttpGet("equipments/{is_where}")]
        public async Task<IActionResult> Getequipments(bool is_where){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "";
                }else{
                    query = "WHERE ISNULL(xDeletedRecord,0) = 0 AND (xEndDate Is Null OR xEndDate >= GETDATE()) ";
                }
                
                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [itemid]) AS Row_num,[recordnumber] AS [RecordNumber],[xDeletedRecord],[type] AS [Type], [itemid] AS [ItemID], [datedisposed] AS [DateDisposed], [lastservice] AS [LastService], [equipcode] AS [EquipCode], [serialno] AS [SerialNo], [purchasedate] AS [PurchaseDate], [purchaseamount] AS [PurchaseAmount], [lockboxcode] AS [LockBoxCode], [lockboxlocation] AS [LockBoxLocation], [notes] AS [Notes] FROM equipment "+query+" ";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                row_num = GenSettings.Filter(rd["Row_num"]),
                                recordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                type = GenSettings.Filter(rd["Type"]),
                                itemID = GenSettings.Filter(rd["ItemID"]),
                                dateDisposed = GenSettings.Filter(rd["DateDisposed"]),
                                lastService = GenSettings.Filter(rd["LastService"]),
                                equipCode = GenSettings.Filter(rd["EquipCode"]),
                                serialNo = GenSettings.Filter(rd["SerialNo"]),
                                lockBoxCode = GenSettings.Filter(rd["LockBoxCode"]),
                                lockBoxLocation = GenSettings.Filter(rd["LockBoxLocation"]),
                                purchaseDate = GenSettings.Filter(rd["PurchaseDate"]),
                                is_deleted   = GenSettings.Filter(rd["xDeletedRecord"]),
                                purchaseAmount = GenSettings.Filter(rd["PurchaseAmount"]),
                                notes = GenSettings.Filter(rd["Notes"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpDelete("configuration/delete/Equipments/{recordNo}")]
        public async Task<IActionResult> DeleteEquipments(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE Equipment SET xDeletedRecord = 1 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/Equipments/{recordNo}")]
        public async Task<IActionResult> activateEquipments(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE Equipment SET xDeletedRecord = 0 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpGet("agencyPayTypes/{is_where}")]
        public async Task<IActionResult> GetagencyPayTypes(bool is_where){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                string query = string.Empty;
                if(is_where){
                    query = "WHERE processClassification <> 'INPUT' ";
                }else{
                    query = "WHERE processClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (enddate Is Null OR enddate >= GETDATE()) ";
                }




                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [title]) AS Row_num,[recnum] AS [RecordNumber],[DeletedRecord], [title] AS [Code], [rostergroup] AS [Pay Category], [minorgroup] AS [Sub Group], " + 
                " [amount] AS [Pay Amount], [unit] AS [Pay Unit], [enddate] AS [End Date], [billtext] AS [Description], [accountingidentifier] AS [Pay ID], " + 
                " [paygroup] AS [Pay Group], [paytype] AS [Pay Type],[excludefrompayexport] AS [No Pay Export],[UnitCostUOM]" +
                " [MinChargeRate],[StartTimeLimit],[EndTimeLimit],[NoMonday],[NoTuesday],[NoWednesday],[NoThursday],[NoFriday],[NoSunday],[NoSaturday], " +
                " [NoPubHol],[JobSheetPrompt],[NochangeDate],[NochangeTime],[TimeChangeLimit],[ExcludeFromInterpretation],[FixedTime] as RostedTime,[NoOvertimeAccumulation], "+
                " [MinDurtn],[MaxDurtn],[ExcludeFromHigherPayCalculation],[PayAsRostered],[ExcludeFromTimebands],[ExcludeFromConflicts], " + 
                " [ExcludeFromRecipSummarySheet],[AutoApprove] as ApplicableCasulas FROM itemtypes "+query+" ORDER BY title";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber    = GenSettings.Filter(rd["RecordNumber"]),
                                row_num         = GenSettings.Filter(rd["Row_num"]),
                                is_deleted      = GenSettings.Filter(rd["DeletedRecord"]),
                                code            = GenSettings.Filter(rd["Code"]),
                                payCategory     = GenSettings.Filter(rd["Pay Category"]),
                                subGroup        = GenSettings.Filter(rd["Sub Group"]),
                                payAmount       = GenSettings.Filter(rd["Pay Amount"]),
                                payUnit         = GenSettings.Filter(rd["Pay Unit"]),
                                endDate         = GenSettings.Filter(rd["End Date"]),
                                description     = GenSettings.Filter(rd["Description"]),
                                payID           = GenSettings.Filter(rd["Pay ID"]),
                                payType         = GenSettings.Filter(rd["Pay Type"]),
                                payGroup        = GenSettings.Filter(rd["Pay Group"]),
                                noPayExport     = GenSettings.Filter(rd["No Pay Export"]),
                                minChargeRate   = GenSettings.Filter(rd["MinChargeRate"]),
                                startTimeLimit  = GenSettings.Filter(rd["StartTimeLimit"]),
                                endTimeLimit    = GenSettings.Filter(rd["EndTimeLimit"]),
                                noMonday        = GenSettings.Filter(rd["NoMonday"]),
                                noTuesday       = GenSettings.Filter(rd["NoTuesday"]),
                                noWednesday     = GenSettings.Filter(rd["NoWednesday"]),
                                noThursday      = GenSettings.Filter(rd["NoThursday"]),
                                noFriday        = GenSettings.Filter(rd["NoFriday"]),
                                noSunday        = GenSettings.Filter(rd["NoSunday"]),
                                noSaturday      = GenSettings.Filter(rd["NoSaturday"]),
                                noPubHol        = GenSettings.Filter(rd["NoPubHol"]),
                                jobSheetPrompt  = GenSettings.Filter(rd["JobSheetPrompt"]),
                                nochangeDate    = GenSettings.Filter(rd["NochangeDate"]),
                                nochangeTime    = GenSettings.Filter(rd["NochangeTime"]),
                                timeChangeLimit = GenSettings.Filter(rd["TimeChangeLimit"]),
                                excludeFromInterpretation       = GenSettings.Filter(rd["ExcludeFromInterpretation"]),
                                rostedTime                      = GenSettings.Filter(rd["RostedTime"]),
                                noOvertimeAccumulation          = GenSettings.Filter(rd["NoOvertimeAccumulation"]),
                                minDurtn                        = GenSettings.Filter(rd["MinDurtn"]),
                                maxDurtn                        = GenSettings.Filter(rd["MaxDurtn"]),
                                excludeFromHigherPayCalculation = GenSettings.Filter(rd["ExcludeFromHigherPayCalculation"]),
                                payAsRostered                   = GenSettings.Filter(rd["PayAsRostered"]),
                                excludeFromTimebands            = GenSettings.Filter(rd["ExcludeFromTimebands"]),
                                excludeFromConflicts            = GenSettings.Filter(rd["ExcludeFromConflicts"]),
                                excludeFromRecipSummarySheet    = GenSettings.Filter(rd["ExcludeFromRecipSummarySheet"]),
                                applicableCasulas               = GenSettings.Filter(rd["ApplicableCasulas"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpDelete("configuration/delete/PayTypes/{recordNo}")]
        public async Task<IActionResult> DeletePayTypes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("Update itemtypes set DeletedRecord = 1  WHERE recnum = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/PayTypes/{recordNo}")]
        public async Task<IActionResult> activatePayTypes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("Update itemtypes set DeletedRecord = 0 WHERE recnum = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpGet("fundingSource/{is_where}")]
        public async Task<IActionResult> GetfundingSource(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE";
                }else{
                    query = "WHERE ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
                }
                String sql="Select ROW_NUMBER() OVER(ORDER BY [Description]) AS Row_num,[RecordNumber],[Description],[User1],[User2],[EndDate] ,[DELETEDRECORD] as is_deleted From DataDomains "+query+" Domain = 'FUNDINGBODIES' ORDER BY DESCRIPTION";
                
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                row_num      = GenSettings.Filter(rd["Row_num"]),
                                endDate      = GenSettings.Filter(rd["EndDate"]),
                                description  = GenSettings.Filter(rd["Description"]),
                                name         = GenSettings.Filter(rd["Description"]),
                                user1        = GenSettings.Filter(rd["User1"]),
                                user2        = GenSettings.Filter(rd["User2"]),
                                is_deleted   = GenSettings.Filter(rd["is_deleted"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("programPackages/{is_where}")]
        public async Task<IActionResult> GetprogramPackages(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                if(is_where){
                    query = "WHERE ( [group] = 'PROGRAMS' ) ";
                }else{
                    query = "WHERE ( [group] = 'PROGRAMS' ) AND ISNULL(xDeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) ";
                }
                
                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [name],recordnumber) AS Row_num,[recordnumber] AS [RecordNumber], [name] AS [Title],[type] AS [Funding Source], [address1] AS [AgencyID],[address2] AS [Cordinators],[Suburb] AS [FundingRegion],[HRT_DATASET] AS [CareDomain],[USER1] as [FundingType],[Phone2] AS [State],[gst] AS [GST], [gstrate] AS [Rate], [budgetamount] AS [Budget $],[budget_1] AS [Budget Hrs], [budgetperiod] AS [Bgt Cycle], [fax] AS [GLExp], [email] AS [GLRev], [phone1] AS [GLSuper],[CloseDate] AS [CloseDate],[Postcode] AS [ProgramJob],[UserYesNo3] AS [template],[User2] AS [contiguency],[UserYesNo2] AS [IndividuallyFunded],[UserYesNo1] AS [AgedCarePackage],[User3] AS [Level],[User4] AS [TargetGroup],[User10] AS [ConguencyPackage],[BudgetEnforcement] AS [BudgetEnforcement],[BudgetRosterEnforcement],[User11],[User12],[User13],[P_Def_Alert_Type],[P_Def_Alert_BaseOn],[P_Def_Alert_Period],[P_Def_Alert_Allowed],[P_Def_Alert_Yellow],[P_Def_Alert_Orange],[P_Def_Alert_Red],[P_Def_Expire_Amount],[P_Def_Expire_CostType],[P_Def_Expire_Unit],[P_Def_Expire_Period],[P_Def_Expire_Length],[P_Def_Fee_BasicCare],[P_Def_Contingency_PercAmt],[P_Def_Admin_AdminType],[P_Def_Admin_CMType],[P_Def_Admin_CM_PercAmt],[p_Def_Admin_Admin_PercAmt],[P_Def_StdDisclaimer],[P_Def_Admin_AdminFrequency],[P_Def_Admin_CMFrequency],[P_Def_Admin_AdminDay],[P_Def_Admin_CMDay],[P_Def_Expire_Using],[P_Def_Contingency_Max],[P_Def_QueryAutoDeleteAdmin],[DefaultCHGTravelWithinActivity],[DefaultCHGTravelWithinPayType],[DefaultNCTravelWithinProgram],[DefaultNCTravelWithinActivity],[DefaultNCTravelWithinPayType],[DefaultCHGTravelBetweenActivity],[DefaultCHGTravelBetweenPayType],[DefaultNCTravelBetweenProgram],[DefaultNCTravelBetweenActivity],[DefaultNCTravelBetweenPayType],[P_Def_IncludeClientFeesInCont],[P_Def_IncludeIncomeTestedFeeInAdmin],[P_Def_IncludeBasicCareFeeInAdmin],[P_Def_IncludeTopUpFeeInAdmin],[CDCStatementText1],[DefaultCHGTRAVELWithInProgram],[DefaultCHGTRAVELBetweenProgram],[DefaultDailyFee],[NoNoticeLeadTime],[ShortNoticeLeadTime],[NoNoticeLeaveActivity],[ShortNoticeLeaveActivity],[DefaultNoNoticeCancel],[DefaultNoNoticeBillProgram],[DefaultNoNoticePayProgram],[DefaultNoNoticePayType],[DefaultShortNoticeCancel],[DefaultShortNoticeBillProgram],[DefaultShortNoticePayProgram],[DefaultShortNoticePayType],[DefaultWithNoticeCancel],[NoNoticeCancelRate],[ShortNoticeCancelRate],[DefaultWithNoticeProgram] FROM humanresourcetypes "+query+" ORDER BY title";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                row_num           = GenSettings.Filter(rd["Row_num"]),
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                title             = GenSettings.Filter(rd["Title"]),
                                type              = GenSettings.Filter(rd["Funding Source"]),
                                agencyID          = GenSettings.Filter(rd["AgencyID"]),
                                cordinators       = GenSettings.Filter(rd["Cordinators"]),
                                fundingRegion     = GenSettings.Filter(rd["FundingRegion"]),
                                careDomain        = GenSettings.Filter(rd["CareDomain"]),
                                fundingType       = GenSettings.Filter(rd["FundingType"]),
                                state             = GenSettings.Filter(rd["State"]),
                                gst               = GenSettings.Filter(rd["GST"]),
                                rate              = GenSettings.Filter(rd["Rate"]),
                                budgetdollar      = GenSettings.Filter(rd["Budget $"]),
                                budgetHrs         = GenSettings.Filter(rd["Budget Hrs"]),
                                bgtCycle          = GenSettings.Filter(rd["Bgt Cycle"]),
                                glExp             = GenSettings.Filter(rd["GLExp"]),
                                gLRev             = GenSettings.Filter(rd["GLRev"]),
                                gLSuper           = GenSettings.Filter(rd["GLSuper"]),
                                closeDate         = GenSettings.Filter(rd["CloseDate"]),
                                programJob        = GenSettings.Filter(rd["ProgramJob"]),
                                template          = GenSettings.Filter(rd["template"]),
                                contiguency       = GenSettings.Filter(rd["contiguency"]),
                                individuallyFunded              = GenSettings.Filter(rd["IndividuallyFunded"]),
                                agedCarePackage                 = GenSettings.Filter(rd["AgedCarePackage"]),
                                level                           = GenSettings.Filter(rd["Level"]),
                                targetGroup                     = GenSettings.Filter(rd["TargetGroup"]),
                                conguencyPackage                = GenSettings.Filter(rd["ConguencyPackage"]),
                                budgetEnforcement               = GenSettings.Filter(rd["BudgetEnforcement"]),
                                budgetRosterEnforcement         = GenSettings.Filter(rd["BudgetRosterEnforcement"]),
                                User11                          = GenSettings.Filter(rd["User11"]),
                                User12                          = GenSettings.Filter(rd["User12"]),
                                User13                          = GenSettings.Filter(rd["User13"]),
                                p_Def_Alert_Type                = GenSettings.Filter(rd["P_Def_Alert_Type"]),
                                p_Def_Alert_BaseOn              = GenSettings.Filter(rd["P_Def_Alert_BaseOn"]),
                                p_Def_Alert_Orange              = GenSettings.Filter(rd["P_Def_Alert_Orange"]),
                                p_Def_Alert_Period              = GenSettings.Filter(rd["P_Def_Alert_Period"]),
                                p_Def_Alert_Red                 = GenSettings.Filter(rd["P_Def_Alert_Red"]),
                                p_Def_Alert_Yellow              = GenSettings.Filter(rd["P_Def_Alert_Yellow"]),
                                p_Def_Alert_Allowed             = GenSettings.Filter(rd["P_Def_Alert_Allowed"]),
                                p_Def_Expire_Amount             = GenSettings.Filter(rd["P_Def_Expire_Amount"]),
                                p_Def_Expire_CostType           = GenSettings.Filter(rd["P_Def_Expire_CostType"]),
                                p_Def_Expire_Unit               = GenSettings.Filter(rd["P_Def_Expire_Unit"]),
                                p_Def_Expire_Period             = GenSettings.Filter(rd["P_Def_Expire_Period"]),
                                p_Def_Expire_Length             = GenSettings.Filter(rd["P_Def_Expire_Length"]),
                                p_Def_Expire_Using              = GenSettings.Filter(rd["P_Def_Expire_Using"]),
                                p_Def_Fee_BasicCare             = GenSettings.Filter(rd["P_Def_Fee_BasicCare"]),
                                p_Def_Contingency_PercAmt       = GenSettings.Filter(rd["P_Def_Contingency_PercAmt"]),
                                p_Def_Admin_AdminType           = GenSettings.Filter(rd["P_Def_Admin_AdminType"]),
                                p_Def_Admin_CMType              = GenSettings.Filter(rd["P_Def_Admin_CMType"]),
                                p_Def_Admin_CM_PercAmt          = GenSettings.Filter(rd["P_Def_Admin_CM_PercAmt"]),
                                p_Def_Admin_Admin_PercAmt       = GenSettings.Filter(rd["p_Def_Admin_Admin_PercAmt"]),
                                p_Def_StdDisclaimer             = GenSettings.Filter(rd["P_Def_StdDisclaimer"]),
                                p_Def_Admin_AdminFrequency      = GenSettings.Filter(rd["P_Def_Admin_AdminFrequency"]),
                                p_Def_Admin_CMFrequency         = GenSettings.Filter(rd["P_Def_Admin_CMFrequency"]),
                                p_Def_Admin_AdminDay            = GenSettings.Filter(rd["P_Def_Admin_AdminDay"]),
                                p_Def_Admin_CMDay               = GenSettings.Filter(rd["P_Def_Admin_CMDay"]),
                                p_Def_Contingency_Max           = GenSettings.Filter(rd["P_Def_Contingency_Max"]),
                                p_Def_QueryAutoDeleteAdmin      = GenSettings.Filter(rd["P_Def_QueryAutoDeleteAdmin"]),
                                defaultCHGTravelWithinActivity  = GenSettings.Filter(rd["DefaultCHGTravelWithinActivity"]),
                                defaultCHGTravelWithinPayType   = GenSettings.Filter(rd["DefaultCHGTravelWithinPayType"]),
                                defaultNCTravelWithinProgram    = GenSettings.Filter(rd["DefaultNCTravelWithinProgram"]),
                                defaultNCTravelWithinActivity   = GenSettings.Filter(rd["DefaultNCTravelWithinActivity"]),
                                defaultNCTravelWithinPayType    = GenSettings.Filter(rd["DefaultNCTravelWithinPayType"]),
                                defaultCHGTravelBetweenActivity = GenSettings.Filter(rd["DefaultCHGTravelBetweenActivity"]),
                                defaultCHGTravelBetweenPayType  = GenSettings.Filter(rd["DefaultCHGTravelBetweenPayType"]),
                                defaultNCTravelBetweenProgram   = GenSettings.Filter(rd["DefaultNCTravelBetweenProgram"]),
                                defaultNCTravelBetweenActivity  = GenSettings.Filter(rd["DefaultNCTravelBetweenActivity"]),
                                defaultNCTravelBetweenPayType   = GenSettings.Filter(rd["DefaultNCTravelBetweenPayType"]),
                                p_Def_IncludeClientFeesInCont       = GenSettings.Filter(rd["P_Def_IncludeClientFeesInCont"]),
                                p_Def_IncludeIncomeTestedFeeInAdmin = GenSettings.Filter(rd["P_Def_IncludeIncomeTestedFeeInAdmin"]),
                                p_Def_IncludeBasicCareFeeInAdmin    = GenSettings.Filter(rd["P_Def_IncludeBasicCareFeeInAdmin"]),
                                p_Def_IncludeTopUpFeeInAdmin        = GenSettings.Filter(rd["P_Def_IncludeTopUpFeeInAdmin"]),
                                cDCStatementText1                   = GenSettings.Filter(rd["CDCStatementText1"]),
                                defaultCHGTRAVELWithInProgram       = GenSettings.Filter(rd["DefaultCHGTRAVELWithInProgram"]),
                                defaultCHGTRAVELBetweenProgram      = GenSettings.Filter(rd["DefaultCHGTRAVELBetweenProgram"]),
                                defaultDailyFee                     = GenSettings.Filter(rd["DefaultDailyFee"]),
                                noNoticeLeadTime                    = GenSettings.Filter(rd["NoNoticeLeadTime"]),
                                shortNoticeLeadTime                 = GenSettings.Filter(rd["ShortNoticeLeadTime"]),
                                noNoticeLeaveActivity               = GenSettings.Filter(rd["NoNoticeLeaveActivity"]),
                                shortNoticeLeaveActivity            = GenSettings.Filter(rd["ShortNoticeLeaveActivity"]),
                                defaultNoNoticeCancel               = GenSettings.Filter(rd["DefaultNoNoticeCancel"]),
                                defaultNoNoticeBillProgram          = GenSettings.Filter(rd["DefaultNoNoticeBillProgram"]),
                                defaultNoNoticePayProgram           = GenSettings.Filter(rd["DefaultNoNoticePayProgram"]),
                                defaultNoNoticePayType              = GenSettings.Filter(rd["DefaultNoNoticePayType"]),
                                defaultShortNoticeCancel            = GenSettings.Filter(rd["DefaultShortNoticeCancel"]),
                                defaultShortNoticeBillProgram       = GenSettings.Filter(rd["DefaultShortNoticeBillProgram"]),
                                defaultShortNoticePayProgram        = GenSettings.Filter(rd["DefaultShortNoticePayProgram"]),
                                defaultShortNoticePayType           = GenSettings.Filter(rd["DefaultShortNoticePayType"]),
                                defaultWithNoticeCancel             = GenSettings.Filter(rd["DefaultWithNoticeCancel"]),
                                noNoticeCancelRate                  = GenSettings.Filter(rd["NoNoticeCancelRate"]),
                                shortNoticeCancelRate               = GenSettings.Filter(rd["ShortNoticeCancelRate"]),
                                defaultWithNoticeProgram            = GenSettings.Filter(rd["DefaultWithNoticeProgram"]), 
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpDelete("configuration/delete/ProgarmPackages/{recordNo}")]
        public async Task<IActionResult> DeleteProgarmPackages(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE humanresourcetypes WHERE ( [group] = 'PROGRAMS' ) AND [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/ProgarmPackages/packageLeaveType/{recordNo}")]
        public async Task<IActionResult> packageLeaveType(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE ( [group] = 'PROG_LEAVE' ) AND [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/ProgarmPackages/deleteCompetency/{recordNo}")]
        public async Task<IActionResult> deleteCompetency(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE ( [group] = 'PROG_COMP' ) AND [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/ProgarmPackages/approvedStaff/{recordNo}")]
        public async Task<IActionResult> deleteapprovedStaff(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE ( [group] = 'PROG_STAFFI' ) AND [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/ProgarmPackages/excludedStaff/{recordNo}")]
        public async Task<IActionResult> deleteexcludedStaff(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE ( [group] = 'PROG_STAFFX' ) AND [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/ProgarmPackages/deleteApprovedServices/{recordNo}")]
        public async Task<IActionResult> deleteApprovedServices(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE ServiceOverview WHERE [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpGet("vehicles")]
        public async Task<IActionResult> Getvehicles(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="select ROW_NUMBER() OVER(ORDER BY [Description]) AS Row_num,[Description] as name,[EndDate] as expiry,[DELETEDRECORD] as is_deleted [RecordNumber] from DataDomains where Domain='VEHICLES'";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                row_num      = GenSettings.Filter(rd["Row_num"]),
                                name         = GenSettings.Filter(rd["name"]),
                                expiry       = GenSettings.Filter(rd["expiry"]),
                                is_deleted   = GenSettings.Filter(rd["is_deleted"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("packgeLeaveTypes")]
        public async Task<IActionResult> GetpackgeLeaveTypes(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, PersonID, Name AS [Leave Activity Code], SubType AS [Accumulated By], User3 AS [Max Days], User4 AS [Claim Reduced To] FROM HumanResources WHERE PersonID = '' AND [Type] = 'PROG_LEAVE' ORDER BY Name";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                personId          = GenSettings.Filter(rd["PersonID"]),
                                leaveActivityCode = GenSettings.Filter(rd["Leave Activity Code"]),
                                accumulatedBy     = GenSettings.Filter(rd["Accumulated By"]),
                                maxDays           = GenSettings.Filter(rd["Max Days"]),
                                claimReducedTo    = GenSettings.Filter(rd["Claim Reduced To"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("competencyByPersonId/{recordNo}")]
        public async Task<IActionResult> GetCompetencyByPersonId(int recordNo){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, PersonID, Name AS Competency, UserYesNo1 AS Mandatory FROM HumanResources WHERE PersonID = @recordNo AND [Type] = 'PROG_COMP' ORDER BY Name";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                      cmd.Parameters.AddWithValue("@recordNo", recordNo);
                      await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                personId          = GenSettings.Filter(rd["PersonID"]),
                                mandatory         = GenSettings.Filter(rd["Mandatory"]),
                                competency        = GenSettings.Filter(rd["Competency"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("serviceCompetencyByPersonId/{recordNo}")]
        public async Task<IActionResult> GetServiceCompetencyByPersonId(string recordNo){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, PersonID, Name AS Competency, [Recurring] AS Mandatory,Notes as Notes FROM HumanResources WHERE PersonID = @recordNo AND [Type] = 'STAFFATTRIBUTE' ORDER BY Name";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                      cmd.Parameters.AddWithValue("@recordNo", recordNo);
                      await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                personId          = GenSettings.Filter(rd["PersonID"]),
                                mandatory         = GenSettings.Filter(rd["Mandatory"]),
                                competency        = GenSettings.Filter(rd["Competency"]),
                                notes             = GenSettings.Filter(rd["Notes"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("approvedServicesByPersonId/{recordNo}")]
        public async Task<IActionResult> GetServicesByPersonIdByPersonId(string recordNo){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, PersonID, [Service Type] AS Activity, [ServiceStatus] AS Status FROM ServiceOverview WHERE PersonID = @recordNo AND ServiceStatus IN ('ACTIVE', 'INACTIVE') ORDER BY [Service Type]";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                      cmd.Parameters.AddWithValue("@recordNo", recordNo);
                      await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                personId          = GenSettings.Filter(rd["PersonID"]),
                                activity          = GenSettings.Filter(rd["Activity"]),
                                status            = GenSettings.Filter(rd["Status"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("approvedStaffByPersonId/{recordNo}")]
        public async Task<IActionResult> GetapprovedStaffByPersonId(string recordNo){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, PersonID, Name, Notes FROM HumanResources WHERE PersonID = @recordNo AND [Type] = 'PROG_STAFFI' ORDER BY Name";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                      cmd.Parameters.AddWithValue("@recordNo", recordNo);
                      await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                personId          = GenSettings.Filter(rd["PersonID"]),
                                name              = GenSettings.Filter(rd["Name"]),
                                notes             = GenSettings.Filter(rd["Notes"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("excludeStaffByPersonId/{recordNo}")]
        public async Task<IActionResult> GetexcludeStaffByPersonId(string recordNo){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT RecordNumber, PersonID, Name, Notes FROM HumanResources WHERE PersonID = @recordNo AND [Type] = 'PROG_STAFFX' ORDER BY Name";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                      cmd.Parameters.AddWithValue("@recordNo", recordNo);
                      await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber      = GenSettings.Filter(rd["RecordNumber"]),
                                personId          = GenSettings.Filter(rd["PersonID"]),
                                name              = GenSettings.Filter(rd["Name"]),
                                notes             = GenSettings.Filter(rd["Notes"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("activityGroups/{is_where}")]
        public async Task<IActionResult> GetactivityGroups(bool is_where = false){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
            if(is_where){
                query = "WHERE";
            }else{
                query = "WHERE ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
            }
                String sql="select ROW_NUMBER() OVER(ORDER BY [Description]) AS Row_num, [RecordNumber],[Description] as name,[User1] as branch,[User2] as agroup,[EndDate] as expiry,[DELETEDRECORD] as is_deleted From DataDomains "+query+" Domain = 'ACTIVITYGROUPS'  ORDER BY DESCRIPTION";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                row_num      = GenSettings.Filter(rd["Row_num"]),
                                name         = GenSettings.Filter(rd["name"]),
                                branch       = GenSettings.Filter(rd["branch"]),
                                agroup       = GenSettings.Filter(rd["agroup"]),
                                expiry       = GenSettings.Filter(rd["expiry"]),
                                is_deleted   = GenSettings.Filter(rd["is_deleted"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("centerFacilityLoc/{is_where}")]
        public async Task<IActionResult> GetcenterFacilityLoc(bool is_where){
            string query = string.Empty;
            if(is_where){
                query = "";
            }else{
                query = "WHERE ISNULL(xDeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) ";
            }
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="Select ROW_NUMBER() OVER(ORDER BY [Name],[RecordNumber]) AS Row_num,[xDeletedRecord] AS is_deleted,[FundingSource],[Jurisdiction],[AgencySector],[WeeksPerYear],[DaysPerWeek],[HoursPerDay] as hour,[NoServiceUsers],[CSTDAServiceType],[ServiceAnnualHours],[DaysPerWeekOfOperation_NoPattern],[WeeksPerCollectionPeriodOfOperation_NoPattern],[HoursPerDayOfOperation_NoPattern],[MAXWeeklyRecipientHours],[MINWeeklyRecipientHours],[MAXWeeklyStaffHours],[MINWeeklyStaffHours],[GLRevenue],[GLCost],[GLOverride],[CSTDA],[DCSI],[Branch],[Places],[BH_EarlyStart],[BH_LateStart],[BH_EarlyFinish],[BH_LateFinish],[BH_OverStay],[BH_UndrStay],[BH_NoWork],[AH_EarlyStart],[AH_LateStart],[AH_EarlyFinish],[AH_LateFinish],[AH_OverStay],[AH_UndrStay],[FundingType],[RunsheetAlerts],[ServiceOutletID],[RecordNumber],[Name],[Suburb],[CSTDASLA],[Postcode],AddressLine1 + CASE WHEN Suburb is null Then ' ' ELSE ' ' + Suburb END as Address FROM CSTDAOutlets "+query+" ORDER BY [NAME]";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber                                   = GenSettings.Filter(rd["RecordNumber"]),
                                row_num                                        = GenSettings.Filter(rd["Row_num"]),
                                is_deleted                                     = GenSettings.Filter(rd["is_deleted"]),
                                name                                           = GenSettings.Filter(rd["Name"]),
                                fundingSource                                  = GenSettings.Filter(rd["FundingSource"]),
                                jurisdiction                                   = GenSettings.Filter(rd["Jurisdiction"]),
                                agencySector                                   = GenSettings.Filter(rd["AgencySector"]),
                                weeksPerYear                                   = GenSettings.Filter(rd["WeeksPerYear"]),
                                daysPerWeek                                    = GenSettings.Filter(rd["DaysPerWeek"]),
                                hour                                           = GenSettings.Filter(rd["hour"]),
                                noServiceUsers                                 = GenSettings.Filter(rd["NoServiceUsers"]),
                                cstdaServiceType                               = GenSettings.Filter(rd["CSTDAServiceType"]),
                                ServiceAnnualHours                             = GenSettings.Filter(rd["ServiceAnnualHours"]),
                                daysPerWeekOfOperation_NoPattern               = GenSettings.Filter(rd["DaysPerWeekOfOperation_NoPattern"]),
                                weeksPerCollectionPeriodOfOperation_NoPattern  = GenSettings.Filter(rd["WeeksPerCollectionPeriodOfOperation_NoPattern"]),
                                hoursPerDayOfOperation_NoPattern               = GenSettings.Filter(rd["HoursPerDayOfOperation_NoPattern"]),
                                maxWeeklyRecipientHours                        = GenSettings.Filter(rd["MAXWeeklyRecipientHours"]),
                                minWeeklyRecipientHours                        = GenSettings.Filter(rd["MINWeeklyRecipientHours"]),
                                maxWeeklyStaffHours                            = GenSettings.Filter(rd["MAXWeeklyStaffHours"]),
                                minWeeklyStaffHours                            = GenSettings.Filter(rd["MINWeeklyStaffHours"]),
                                glRevenue                                      = GenSettings.Filter(rd["GLRevenue"]),
                                glCost                                         = GenSettings.Filter(rd["GLCost"]),
                                glOverride                                     = GenSettings.Filter(rd["GLOverride"]),
                                cstda                                          = GenSettings.Filter(rd["CSTDA"]),
                                dcsi                                           = GenSettings.Filter(rd["DCSI"]),
                                branch                                         = GenSettings.Filter(rd["Branch"]),
                                places                                         = GenSettings.Filter(rd["Places"]),
                                bH_EarlyStart                                  = GenSettings.Filter(rd["BH_EarlyStart"]),
                                bH_LateStart                                   = GenSettings.Filter(rd["BH_LateStart"]),
                                bH_EarlyFinish                                 = GenSettings.Filter(rd["BH_EarlyFinish"]),
                                bH_LateFinish                                  = GenSettings.Filter(rd["BH_LateFinish"]),
                                bH_OverStay                                    = GenSettings.Filter(rd["BH_OverStay"]),
                                bH_UndrStay                                    = GenSettings.Filter(rd["BH_UndrStay"]),
                                bH_NoWork                                      = GenSettings.Filter(rd["BH_NoWork"]),
                                aH_EarlyStart                                  = GenSettings.Filter(rd["AH_EarlyStart"]),
                                aH_LateStart                                   = GenSettings.Filter(rd["AH_LateStart"]),
                                aH_EarlyFinish                                 = GenSettings.Filter(rd["AH_EarlyFinish"]),
                                aH_LateFinish                                  = GenSettings.Filter(rd["AH_LateFinish"]),
                                aH_OverStay                                    = GenSettings.Filter(rd["AH_OverStay"]),
                                aH_UndrStay                                    = GenSettings.Filter(rd["AH_UndrStay"]),
                                fundingType                                    = GenSettings.Filter(rd["FundingType"]),
                                runsheetAlerts                                 = GenSettings.Filter(rd["RunsheetAlerts"]),
                                serviceOutletID                                = GenSettings.Filter(rd["ServiceOutletID"]),
                                suburb                                         = GenSettings.Filter(rd["Suburb"]),
                                cstdasla                                       = GenSettings.Filter(rd["CSTDASLA"]),
                                postcode                                       = GenSettings.Filter(rd["Postcode"]),
                                address                                        = GenSettings.Filter(rd["address"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpDelete("configuration/delete/CenterFacilityLoc/{recordNo}")]
        public async Task<IActionResult> DeleteCenterFacilityLoc(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE CSTDAOutlets SET xDeletedRecord = 1 WHERE [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/centerFacilityLoc/{recordNo}")]
        public async Task<IActionResult> activateCenterFacilityLoc(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE CSTDAOutlets SET xDeletedRecord = 0 WHERE [RecordNumber] = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        
        [HttpGet, Route("configurationworkflows/{group}/{is_where}")]
        public async Task<IActionResult> configurationworkflows(string group,int is_where = 0){
            string query = string.Empty;
            
            if(is_where == 1){
                query = " Where ";
            }else{
                query = " Where ISNULL(DeletedRecord, 0) = 0 AND (xEndDate Is Null OR xEndDate >= GETDATE()) AND ";
            }

            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Name],[RecordNumber]) AS Row_num,[RecordNumber],[Name] AS name,[Type],[User2] as branch,[User3] as funding,[User4] as casemanager,[User5] as Staff,[Address1] as address,[xEndDate] as endDate,[DeletedRecord] as is_deleted FROM   humanresources "+query+" [Group]='"+group+"' AND personid = 'W1'";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                      await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber     = GenSettings.Filter(rd["recordNumber"]),
                                row_num          = GenSettings.Filter(rd["row_num"]),
                                name             = GenSettings.Filter(rd["staff"]),
                                type             = GenSettings.Filter(rd["type"]),
                                branch           = GenSettings.Filter(rd["branch"]),
                                funding          = GenSettings.Filter(rd["funding"]),
                                casemanager      = GenSettings.Filter(rd["casemanager"]),
                                staff            = GenSettings.Filter(rd["name"]),
                                address          = GenSettings.Filter(rd["address"]),
                                endDate          = GenSettings.Filter(rd["endDate"]),
                                is_deleted       = GenSettings.Filter(rd["is_deleted"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("configurationfollowups")]
        
        public async Task<IActionResult> Postconfigurationfollowups([FromBody] Configfollowups follwoups)
        {
            try
            {
                foreach (var staff in follwoups.selectedStaff)
                {

                    var hr = new HumanResources()
                    {
                        PersonID = "W1",
                        Type  = follwoups.Activity,
                        Group = follwoups.Group,
                        Name  = staff,
                        User2 = follwoups.Branch,
                        User3 = follwoups.FundingSource,
                        User4 = follwoups.Casemanager,
                        User5 = follwoups.Name,
                    };

                    _context.HumanResources.Add(hr);
                }
                await _context.SaveChangesAsync();
                return Ok(true);
            }
            catch(Exception ex)
            {
                throw ex;
            }
                   
        }

        [HttpPut("configurationfollowups")]
        public async Task<IActionResult> UpdateConsents([FromBody] Configfollowups follwoups)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE HumanResources SET [Name]=@name,[Type]=@actvity,[User2]=@branch,[User3]=@fundingSource,[User4]=@casemanager,[User5]=@staff,[xEndDate]=@endate WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", follwoups.RecordNumber);
                    cmd.Parameters.AddWithValue("@actvity", follwoups.Activity);
                    cmd.Parameters.AddWithValue("@name", follwoups.Staff);
                    cmd.Parameters.AddWithValue("@branch", follwoups.Branch);
                    cmd.Parameters.AddWithValue("@fundingSource", follwoups.FundingSource);
                    cmd.Parameters.AddWithValue("@casemanager", follwoups.Casemanager);
                    cmd.Parameters.AddWithValue("@staff", follwoups.Name);
                    cmd.Parameters.AddWithValue("@endate",(object)follwoups.EndDate?? DBNull.Value);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configurationfollowups/{group}/{recordNo}")]
        public async Task<IActionResult> configurationfollowupsdelete(string group,int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE HumanResources SET DeletedRecord = 1 WHERE  personid = 'W1' AND [group] = @group AND RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    cmd.Parameters.AddWithValue("@group",group);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("activeconfigurationfollowups/{group}/{recordNo}")]
        public async Task<IActionResult> activeconfigurationfollowups(string group,int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE HumanResources SET DeletedRecord = 0 WHERE  personid = 'W1' AND [group] = @group AND RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    cmd.Parameters.AddWithValue("@group",group);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        
        [HttpGet("serviceNotesCat")]
        public async Task<IActionResult> GetserviceNotesCat(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT ROW_NUMBER() OVER(ORDER BY [Description]) AS Row_num,[Description] as name,[RecordNumber] from DataDomains where Domain='SVCNOTECAT'";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                recordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                row_num      = GenSettings.Filter(rd["Row_num"]),
                                name         = GenSettings.Filter(rd["name"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }
        
        [HttpGet("getUserList")]
        public async Task<IActionResult> Getuserslist(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="Select Recnum, Name, [StaffCode] AS StaffRecord, LoginMode,[UserType],[EndDate],[viewallcoordinators],[viewfiltercoord],[viewallbranches],[viewallcategories],[viewfiltercategory],[viewfilterbranches],[viewallstaffcategories],[ViewAllReminders],[ViewFilterReminders],[viewfilterstaffcategory] From UserInfo WHERE (EndDate Is Null OR EndDate >= GETDATE())  ORDER BY NAME";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                Recnum = GenSettings.Filter(rd["Recnum"]),
                                name                    = GenSettings.Filter(rd["Name"]),
                                StaffCode               = GenSettings.Filter(rd["StaffRecord"]),
                                LoginMode               = GenSettings.Filter(rd["LoginMode"]),
                                LoginType               = GenSettings.Filter(rd["UserType"]),
                                viewallcoordinators     = GenSettings.Filter(rd["viewallcoordinators"]),
                                viewfiltercoord         = GenSettings.Filter(rd["viewfiltercoord"]),
                                viewallbranches         = GenSettings.Filter(rd["viewallbranches"]),
                                viewfilterbranches      = GenSettings.Filter(rd["viewfilterbranches"]),
                                viewallcategories       = GenSettings.Filter(rd["viewallcategories"]),
                                viewfiltercategory      = GenSettings.Filter(rd["viewfiltercategory"]),
                                ViewAllReminders        = GenSettings.Filter(rd["ViewAllReminders"]),
                                ViewFilterReminders     = GenSettings.Filter(rd["ViewFilterReminders"]),
                                viewallstaffcategories  = GenSettings.Filter(rd["viewallstaffcategories"]),
                                viewfilterstaffcategory = GenSettings.Filter(rd["viewfilterstaffcategory"]),
                                EndDate                 = GenSettings.Filter(rd["EndDate"]),
                                
                         });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("workflowstafflist")]
        public async Task<IActionResult> workflowstafflist(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                String sql="SELECT '' as selected, SQLID, AccountNo AS [Staff Code] FROM Staff WHERE ISNULL(TerminationDate, '') = '' AND Accountno > '!MULTIPLE' ORDER BY AccountNo";
                using(SqlCommand cmd = new SqlCommand(sql, (SqlConnection) conn))
                {
                    await conn.OpenAsync();
                      List<dynamic> list = new List<dynamic>();
                      SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync())
                    {
                         list.Add(new {
                                SQLID = GenSettings.Filter(rd["SQLID"]),
                                StaffCode = GenSettings.Filter(rd["Staff Code"]),
                         });
                    }
                    return Ok(list);
                }
            }
        }

       [HttpPost("insertSql")]
        public async Task<IActionResult> PostSql([FromBody] JObject obj){

            var objFilterVariables =  obj.SelectToken("variables").Children().OfType<JProperty>().ToDictionary(p => p.Name , p => p.Value);

            var column = objFilterVariables.Select(x => x.Key).ToList();
            var colValues = objFilterVariables.Select(x => (x.Value.Type == JTokenType.String) ? "'"+ x.Value+"'" : 
                                (x.Value.Type == JTokenType.Integer) ? x.Value : 
                                    (x.Value.Type == JTokenType.Null) ? "NULL" : x.Value).ToList();

            var tableName = obj["table"];

            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using(SqlCommand cmd = new SqlCommand()){
                    cmd.CommandText = $"INSERT INTO {tableName} ({ string.Join(",", column) }) VALUES({ string.Join(",", colValues) })";
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }  
        }
        
       [HttpPost("addDomain")]
        public async Task<IActionResult> addDomain([FromBody] SqlString sql)  
        {

            try{
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(sql.Sql, (SqlConnection)conn))
                    {
                        await conn.OpenAsync();

                        var getValue = cmd.ExecuteScalar();
                        // newId = (Int32) cmd.ExecuteScalar();
                        return Ok(getValue);
                    }
                }
            }catch(Exception ex)
            { return Ok(0);}
        }

        [HttpGet("getDomains/{domain}/{is_where}")]
        public async Task<IActionResult> GetDomainByType(string domain, bool is_where = false){
            string query = string.Empty;
            if(is_where){
                query = "WHERE";
            }else{
                query = "WHERE ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
            }
            
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using(SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT[Description] as name,ROW_NUMBER() OVER(ORDER BY [Description]) AS row_num,[Description] as title,
                [recordNumber] as recordNumber,[HACCCode] as staff,[User1] as user1,[EndDate] as end_date , [DeletedRecord] as is_deleted 
                from DataDomains "+query+" Domain=@domain",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@domain",domain);

                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while(await rd.ReadAsync()) {
                        list.Add(new {
                                recordNumber     = GenSettings.Filter(rd["recordNumber"]),
                                row_num          = GenSettings.Filter(rd["row_num"]),
                                name             = GenSettings.Filter(rd["name"]),
                                title            = GenSettings.Filter(rd["title"]),
                                staff            = GenSettings.Filter(rd["staff"]),
                                user1            = GenSettings.Filter(rd["user1"]),
                                is_deleted       = GenSettings.Filter(rd["is_deleted"]),
                                end_date         = GenSettings.Filter(rd["end_date"]),
                         });
                    }  
                    return Ok(list);
                }
            }
        }

        [HttpDelete("configuration/delete/datadomains/{recordNo}")]
        public async Task<IActionResult> Deletedomain(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE DataDomains SET DeletedRecord = 1 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("configuration/activate/datadomains/{recordNo}")]
        public async Task<IActionResult> Activedomain(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE DataDomains SET DeletedRecord = 0 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("configuration/delete/distribution/{recordNo}")]
        public async Task<IActionResult> Deletedistribution(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE IM_DistributionLists SET xDeletedRecord = 1 WHERE recordNo = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/distribution/{recordNo}")]
        public async Task<IActionResult> activateDistribution(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE IM_DistributionLists SET xDeletedRecord = 0 WHERE recordNo = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/documentTemplate/{recordNo}")]
        public async Task<IActionResult> documentTemplate(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE DOC_Associations SET xDeletedRecord = 1 WHERE recordNo = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/documentTemplate/{recordNo}")]
        public async Task<IActionResult> activateDocumentTemplate(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE DOC_Associations SET xDeletedRecord = 0 WHERE recordNo = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/budgets/{recordNo}")]
        public async Task<IActionResult> DeleteBudget(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE Budgets SET xDeletedRecord = 1 WHERE recordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/postcodes/{recordNo}")]
        public async Task<IActionResult> DeletePostcodes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE Pcodes SET DeletedRecord = 1 WHERE recnum = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/postcodes/{recordNo}")]
        public async Task<IActionResult> activatePostcodes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE Pcodes SET DeletedRecord = 1 WHERE recnum = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        
        [HttpDelete("configuration/delete/holidays/{recordNo}")]
        public async Task<IActionResult> DeleteHolidays(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE PUBLIC_HOLIDAYS SET xDeletedRecord = 1 WHERE RECORDNO = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/holidays/{recordNo}")]
        public async Task<IActionResult> activateHolidays(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE PUBLIC_HOLIDAYS SET xDeletedRecord = 0 WHERE RECORDNO = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/medicalContacts/{recordNo}")]
        public async Task<IActionResult> DeleteMedicalContacts(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE HumanResourceTypes SET xDeletedRecord = 1 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/medicalContacts/{recordNo}")]
        public async Task<IActionResult> ActivateMedicalContacts(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE HumanResourceTypes SET xDeletedRecord = 0 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        
        [HttpDelete("configuration/delete/nursingDiagnosis/{recordNo}")]
        public async Task<IActionResult> DeleteNursingDiagnosis(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE NDiagnosisTypes SET xDeletedRecord = 1 WHERE Recordno = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/nursingDiagnosis/{recordNo}")]
        public async Task<IActionResult> activateNursingDiagnosis(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE NDiagnosisTypes SET xDeletedRecord = 0 WHERE Recordno = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/MDiagnosisTypes/{recordNo}")]
        public async Task<IActionResult> DeleteMDiagnosisTypes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE MDiagnosisTypes SET xDeletedRecord = 1 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/MDiagnosisTypes/{recordNo}")]
        public async Task<IActionResult> activateMDiagnosisTypes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE MDiagnosisTypes SET xDeletedRecord = 0 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/MProcedureTypes/{recordNo}")]
        public async Task<IActionResult> DeleteMProcedureTypes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE MProcedureTypes SET xDeletedRecord = 1 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/MProcedureTypes/{recordNo}")]
        public async Task<IActionResult> activateMProcedureTypes(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE MProcedureTypes SET xDeletedRecord = 0 WHERE RecordNumber = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/delete/ActivityServices/{recordNo}")]
        public async Task<IActionResult> DeleteActivityServices(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE ItemTypes SET DeletedRecord = 1  WHERE Recnum = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpDelete("configuration/activate/ActivityServices/{recordNo}")]
        public async Task<IActionResult> ActivateActivityServices(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE ItemTypes SET DeletedRecord = 0  WHERE Recnum = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
            }

            [HttpPost("mergeHistories/{newServiceType}")]
            public async Task<IActionResult> mergeHistories(string newServiceType, [FromBody] dynamic data)
            {
                try
                {
                    using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                    {
                        await conn.OpenAsync();
                        using (var transaction = conn.BeginTransaction())
                        {
                            try
                            {
                                AuditHistory audit = data.audit.ToObject<AuditHistory>();
                                string[] existingServiceTypes = data.existingServiceTypes.ToObject<string[]>();

                                foreach (var existingServiceType in existingServiceTypes)
                                {
                                    // Example: Insert an audit record
                                    using (var cmd = new SqlCommand(@"INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) 
                                                                        VALUES (@operator, @actionDate, @auditDescription, @actionOn, @whoWhatCode, @traccsUser)", conn, transaction))
                                    {
                                        cmd.Parameters.AddWithValue("@operator", audit.Operator);
                                        cmd.Parameters.AddWithValue("@actionDate", audit.ActionDate);
                                        cmd.Parameters.AddWithValue("@auditDescription", $"MERGE ACTIVITY {existingServiceType} TO {newServiceType}");
                                        cmd.Parameters.AddWithValue("@actionOn", audit.ActionOn);
                                        cmd.Parameters.AddWithValue("@whoWhatCode", existingServiceType);
                                        cmd.Parameters.AddWithValue("@traccsUser", audit.Operator);
                                        await cmd.ExecuteNonQueryAsync(); // Ensure async operation is awaited
                                    }

                                    // Update roster table
                                    using (var cmd = new SqlCommand("UPDATE roster SET [Service Type] = @newServiceType WHERE [Service Type] = @existingServiceType", conn, transaction))
                                    {
                                        cmd.Parameters.AddWithValue("@newServiceType", newServiceType);
                                        cmd.Parameters.AddWithValue("@existingServiceType", existingServiceType);
                                        await cmd.ExecuteNonQueryAsync(); // Ensure async operation is awaited
                                    }

                                    // Update ServiceOverview table
                                    using (var cmd = new SqlCommand(@"UPDATE ServiceOverview SET [Service Type] = @newServiceType 
                                                                    WHERE [Service Type] = @existingServiceType AND NOT EXISTS 
                                                                    (SELECT * FROM [SERVICEOVERVIEW] SO WHERE SO.[PersonID] = SERVICEOVERVIEW.PersonID AND 
                                                                    SO.[SERVICE TYPE] = @newServiceType AND SO.SERVICEPROGRAM = SERVICEOVERVIEW.SERVICEPROGRAM)", conn, transaction))
                                    {
                                        cmd.Parameters.AddWithValue("@newServiceType", newServiceType);
                                        cmd.Parameters.AddWithValue("@existingServiceType", existingServiceType);
                                        await cmd.ExecuteNonQueryAsync();
                                    }

                                    // Delete from ServiceOverview where Service Type matches existingServiceType
                                    using (var cmd = new SqlCommand(@"DELETE FROM ServiceOverview WHERE [Service Type] = @existingServiceType", conn, transaction))
                                    {
                                        cmd.Parameters.AddWithValue("@existingServiceType", existingServiceType);
                                        await cmd.ExecuteNonQueryAsync();
                                    }
                                }

                                // Commit transaction after all successful iterations
                                transaction.Commit();
                                return Ok(true); // Return success response
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback(); // Rollback transaction on error
                                Console.WriteLine($"Error: {ex.Message}"); // Log the exception message
                                return BadRequest(ex); // Return error response
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}"); // Log the exception message
                    return BadRequest(ex); // Return error response
                }
            }

            // [HttpPost("mergeHistories/{newServiceType}")]
            // public async Task<IActionResult> mergeHistories(string newServiceType, [FromBody] dynamic data)
            // {
            //     try
            //         {
            //             using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            //             {
            //                 await conn.OpenAsync();
            //                 using (var transaction = conn.BeginTransaction())
            //                 {
            //                     try
            //                     {

            //                         AuditHistory audit = data.audit.ToObject<AuditHistory>();
            //                         string[] existingServiceTypes = data.existingServiceTypes.ToObject<string[]>();


            //                         foreach (var existingServiceType in existingServiceTypes)
            //                         {
            //                             // Example: Insert an audit record
            //                             using (var cmd = new SqlCommand(@"INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) 
            //                                                             VALUES (@operator, @actionDate, @auditDescription, @actionOn, @whoWhatCode, @tracsUser)", conn, transaction))
            //                             {
            //                                 cmd.Parameters.AddWithValue("@operator", audit.Operator);
            //                                 cmd.Parameters.AddWithValue("@actionDate", audit.ActionDate);
            //                                 cmd.Parameters.AddWithValue("@auditDescription", $"MERGE ACTIVITY {existingServiceType} TO {newServiceType}");
            //                                 cmd.Parameters.AddWithValue("@actionOn", audit.ActionOn);
            //                                 cmd.Parameters.AddWithValue("@whoWhatCode", existingServiceType);
            //                                 cmd.Parameters.AddWithValue("@tracsUser", audit.TraccsUser);
            //                                 var insertedId = await cmd.ExecuteNonQueryAsync(); // Ensure async operation is awaited
            //                                 int auditId = Convert.ToInt32(insertedId);
            //                                 Console.WriteLine($"Inserted Audit ID: {auditId}");
            //                             }

            //                             // Update roster table
            //                             using (var cmd = new SqlCommand("UPDATE roster SET [Service Type] = @newServiceType WHERE [Service Type] = @existingServiceType", conn, transaction))
            //                             {
            //                                 cmd.Parameters.AddWithValue("@newServiceType", newServiceType);
            //                                 cmd.Parameters.AddWithValue("@existingServiceType", existingServiceType);
            //                                 await cmd.ExecuteNonQueryAsync(); // Ensure async operation is awaited
            //                             }

            //                              // Update ServiceOverview table
            //                             using (var cmd = new SqlCommand(@"UPDATE ServiceOverview SET [Service Type] = @newServiceType 
            //                                                             WHERE [Service Type] = @existingServiceType AND NOT EXISTS 
            //                                                             (SELECT * FROM [SERVICEOVERVIEW] SO WHERE SO.[PersonID] = SERVICEOVERVIEW.PersonID AND 
            //                                                             SO.[SERVICE TYPE] = @newServiceType AND SO.SERVICEPROGRAM = SERVICEOVERVIEW.SERVICEPROGRAM)", conn, transaction))
            //                             {
            //                                 cmd.Parameters.AddWithValue("@newServiceType", newServiceType);
            //                                 cmd.Parameters.AddWithValue("@existingServiceType", existingServiceType);
            //                                 await cmd.ExecuteNonQueryAsync();
            //                             }

            //                             // Delete from ServiceOverview where Service Type matches existingServiceType
            //                             using (var cmd = new SqlCommand(@"DELETE FROM ServiceOverview WHERE [Service Type] = @existingServiceType", conn, transaction))
            //                             {
            //                                 cmd.Parameters.AddWithValue("@existingServiceType", existingServiceType);
            //                                 await cmd.ExecuteNonQueryAsync();
            //                             }

            //                             // Commit transaction after each successful iteration
            //                             transaction.Commit();
            //                         }

            //                         return Ok(true); // Return success response
            //                     }
            //                     catch (Exception ex)
            //                     {
            //                         transaction.Rollback(); // Rollback transaction on error
            //                         return BadRequest(ex); // Return error response
            //                     }
            //                 }
            //             }
            //         }
            //         catch (Exception ex)
            //         {
            //             return BadRequest(ex); // Return error response
            //         }

            // }



            [HttpPost("programMergeHistories/{newServiceType}")]
            public async Task<IActionResult> ProgramMergeHistories(string newServiceType, [FromBody] dynamic data)
            {
                try
                {
                    AuditHistory audit = data.audit.ToObject<AuditHistory>();
                    string[] existingServiceTypes = data.existingServiceTypes.ToObject<string[]>();
                    // Ensure only one transaction is used
                   //await using var transaction = await _context.Database.BeginTransactionAsync();
                    try
                    {
                        foreach (var existingServiceType in existingServiceTypes)
                        {
                            // Perform the necessary SQL commands
                            await ExecuteProgramMergeQueries(audit, existingServiceType, newServiceType);

                            // Commit transaction after each successful iteration
                           // await transaction.CommitAsync();
                        }
                        return Ok(true); // Return success response
                    }
                    catch (Exception ex)
                    {
                      //  await transaction.RollbackAsync(); // Rollback transaction on error
                        return BadRequest(ex); // Return error response
                    }
                }
                catch (Exception ex)
                {
                    return BadRequest(ex); // Return error response
                }
            }
            private async Task ExecuteProgramMergeQueries(AuditHistory audit, string existingServiceType, string newServiceType)
            {
                // Insert into Audit
                var auditQuery = @"
                    INSERT INTO Audit 
                    (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) 
                    VALUES 
                    (@Operator, @ActionDate, @AuditDescription, @ActionOn, @WhoWhatCode, @TraccsUser)";
                await _context.Database.ExecuteSqlRawAsync(auditQuery, 
                    new SqlParameter("@Operator", audit.Operator),
                    new SqlParameter("@ActionDate", audit.ActionDate),
                    new SqlParameter("@auditDescription", $"MERGE PROGRAM  {existingServiceType} TO {newServiceType}"),
                    new SqlParameter("@ActionOn", audit.ActionOn),
                    new SqlParameter("@WhoWhatCode", existingServiceType),
                    new SqlParameter("@TraccsUser", audit.TraccsUser));

                // Update roster
                var updateRosterQuery = @"
                    UPDATE roster 
                    SET [Program] = @NewServiceType 
                    WHERE [Program] = @ExistingServiceType";
                await _context.Database.ExecuteSqlRawAsync(updateRosterQuery, 
                    new SqlParameter("@NewServiceType", newServiceType),
                    new SqlParameter("@ExistingServiceType", existingServiceType));

                // Update ServiceOverview
                var updateServiceOverviewQuery = @"
                    UPDATE ServiceOverview 
                    SET [ServiceProgram] = @NewServiceType 
                    WHERE [ServiceProgram] = @ExistingServiceType 
                    AND NOT EXISTS (
                        SELECT * 
                        FROM SERVICEOVERVIEW S 
                        WHERE S.PersonID = SERVICEOVERVIEW.PersonID 
                        AND S.SERVICEPROGRAM = @NewServiceType 
                        AND S.[SERVICE TYPE] = SERVICEOVERVIEW.[SERVICE TYPE])";
                await _context.Database.ExecuteSqlRawAsync(updateServiceOverviewQuery, 
                    new SqlParameter("@NewServiceType", newServiceType),
                    new SqlParameter("@ExistingServiceType", existingServiceType));

                // Update RecipientPrograms
                var updateRecipientProgramsQuery = @"
                    UPDATE RecipientPrograms 
                    SET [Program] = @NewServiceType 
                    WHERE [Program] = @ExistingServiceType 
                    AND NOT EXISTS (
                        SELECT * 
                        FROM RecipientPrograms RP 
                        WHERE RP.PersonID = RecipientPrograms.PersonID 
                        AND RP.Program = @NewServiceType)";
                await _context.Database.ExecuteSqlRawAsync(updateRecipientProgramsQuery, 
                    new SqlParameter("@NewServiceType", newServiceType),
                    new SqlParameter("@ExistingServiceType", existingServiceType));

                // Update InvoiceHeader
                var updateInvoiceHeaderQuery = @"
                    UPDATE InvoiceHeader 
                    SET [Package] = @NewServiceType 
                    WHERE [Package] = @ExistingServiceType";
                await _context.Database.ExecuteSqlRawAsync(updateInvoiceHeaderQuery, 
                    new SqlParameter("@NewServiceType", newServiceType),
                    new SqlParameter("@ExistingServiceType", existingServiceType));

                // Update USERINFO
                var updateUserinfoQuery = @"
                    UPDATE USERINFO 
                    SET [VIEWFILTER] = REPLACE(SUBSTRING(VIEWFILTER, 1, 100000), @ExistingServiceType, @NewServiceType) 
                    WHERE [NAME] = 'sysmgr'";
                await _context.Database.ExecuteSqlRawAsync(updateUserinfoQuery, 
                    new SqlParameter("@NewServiceType", newServiceType),
                    new SqlParameter("@ExistingServiceType", existingServiceType));
            }










            [HttpPost("cloneActivity/{programId}/{newTitle}")]
            public async Task<IActionResult> CloneActivity(int programId, string newTitle)
            {
                try
                {
                    // Retrieve the existing program
                    var existingProgram = _context.ItemTypes.SingleOrDefault(p => p.Recnum == programId);
                    if (existingProgram == null)
                    {
                        return NotFound($"Activity with ID {programId} does not exist.");
                    }

                    // Clone the program and set the new title
                    var newProgram = CloneWithNewActivityTcitle(existingProgram, newTitle);

                    // Add the new program to the context without setting the primary key
                    _context.ItemTypes.Add(newProgram);
                    await _context.SaveChangesAsync();

                    return Ok(newProgram);
                }
                catch (Exception ex)
                {
                    // Log the detailed exception
                    var innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                    return StatusCode(500, $"Internal server error: {innerExceptionMessage}");
                }
            }



            [HttpPost("cloneProgram/{programId}/{newTitle}")]
            public async Task<IActionResult> CloneProgram(int programId, string newTitle)
            {
                try
                {
                    // Retrieve the existing program
                    var existingProgram = _context.HumanResourceTypes.SingleOrDefault(p => p.RecordNumber == programId);
                    if (existingProgram == null)
                    {
                        return NotFound($"Program with ID {programId} does not exist.");
                    }

                    // Clone the program and set the new title
                    var newProgram = CloneWithNewTitle(existingProgram, newTitle);

                    // Add the new program to the context without setting the primary key
                    _context.HumanResourceTypes.Add(newProgram);
                    await _context.SaveChangesAsync();

                    return Ok(newProgram);
                }
                catch (Exception ex)
                {
                    // Log the detailed exception
                    var innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                    return StatusCode(500, $"Internal server error: {innerExceptionMessage}");
                }
            }

private HumanResourceTypes CloneWithNewTitle(HumanResourceTypes source, string newTitle)
{
    var newProgram = new HumanResourceTypes();
    var properties = typeof(HumanResourceTypes).GetProperties().Where(p => p.CanWrite && p.Name != "DefaultDailyFee"); 

    foreach (var property in properties)
    {
        if (property.Name == "Name")
        {
            property.SetValue(newProgram, newTitle.ToUpper());
        }
        else if (property.Name != "RecordNumber") // Ensure the primary key is not copied
        {
            property.SetValue(newProgram, property.GetValue(source));
        }
    }

    return newProgram;
}

private ItemTypes CloneWithNewActivityTcitle(ItemTypes source, string newTitle)
{
    var newActivity = new ItemTypes();
    var properties = typeof(ItemTypes).GetProperties().Where(p => p.CanWrite); 

    foreach (var property in properties)
    {
        if (property.Name == "Title")
        {
            property.SetValue(newActivity, newTitle.ToUpper());
        }
        else if (property.Name != "Recnum") // Ensure the primary key is not copied
        {
            property.SetValue(newActivity, property.GetValue(source));
        }
    }

    return newActivity;
}

    [HttpPost("awardpossetup")]
    public async Task<IActionResult> PostAwardPosSetup([FromBody] AwardPos awardsetup)
    {
        // Validate the model state
        if (!ModelState.IsValid)
        {
            var errors = ModelState.Where(x => x.Value.Errors.Any())
                                    .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                    .ToList();
            return BadRequest(errors);
        }

        try
        {
            // Insert the data into the database
            using (var db = _context)
            {
                await db.AwardPos.AddAsync(awardsetup); // Add the awardsetup object to the context
                await db.SaveChangesAsync();            // Save changes to the database
            }

            return Ok(new
            {
                success = true,
                message = "AwardPos setup has been successfully created."
            });
        }
        catch (Exception ex)
        {
            // Return a BadRequest with the exception details
            return BadRequest(new
            {
                success = false,
                message = ex.InnerException?.Message ?? ex.Message  // Log the inner exception if present
            });
        }
    }



    [HttpPut("awardpossetup")]
    public async Task<IActionResult> UpdateAwardPosSetup([FromBody] AwardPos awardsetup)
    {
    // Validate the model state
    if (!ModelState.IsValid)
    {
        var errors = ModelState.Where(x => x.Value.Errors.Any())
                                .Select(x => new { Field = x.Key, Error = x.Value.Errors.First().ErrorMessage })
                                .ToList();
        return BadRequest(errors);
    }

    try
    {
        using (var db = _context)
        {
            var existingAwardPos = (from ap in db.AwardPos
                                    where ap.RECORDNO == awardsetup.RECORDNO
                                    select ap).FirstOrDefault();

            if (existingAwardPos == null)
                return BadRequest("AwardPos record not found.");

            // Update the existing record with new values
            existingAwardPos.Day_MaxOrdHrWD1stOTBreak_PayType = awardsetup.Day_MaxOrdHrWD1stOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrWD2ndOTBreak_PayType = awardsetup.Day_MaxOrdHrWD2ndOTBreak_PayType;
            existingAwardPos.FN_MaxOrdHrWD1stOTBreak_PayType = awardsetup.FN_MaxOrdHrWD1stOTBreak_PayType;
            existingAwardPos.FN_MaxOrdHrWD2ndOTBreak_PayType = awardsetup.FN_MaxOrdHrWD2ndOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrSat1stOTBreak_PayType = awardsetup.Day_MaxOrdHrSat1stOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrSat2ndOTBreak_PayType = awardsetup.Day_MaxOrdHrSat2ndOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrSun1stOTBreak_PayType = awardsetup.Day_MaxOrdHrSun1stOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrSun2ndOTBreak_PayType = awardsetup.Day_MaxOrdHrSun2ndOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrPH1stOTBreak_PayType = awardsetup.Day_MaxOrdHrPH1stOTBreak_PayType;
            existingAwardPos.Day_MaxOrdHrPH2ndOTBreak_PayType = awardsetup.Day_MaxOrdHrPH2ndOTBreak_PayType;
            existingAwardPos.Week_MaxOrdHrWD1stOTBreak = awardsetup.Week_MaxOrdHrWD1stOTBreak;
            existingAwardPos.Week_MaxOrdHrWD1stOTBreak_PayType = awardsetup.Week_MaxOrdHrWD1stOTBreak_PayType;
            existingAwardPos.OvertimeLeeway = awardsetup.OvertimeLeeway;
            existingAwardPos.TOILDefaultAccrualPaytype = awardsetup.TOILDefaultAccrualPaytype;
            existingAwardPos.Day_MaxOrdHrWD1stOTBreak = awardsetup.Day_MaxOrdHrWD1stOTBreak;
            existingAwardPos.FN_MaxOrdHrWD1stOTBreak = awardsetup.FN_MaxOrdHrWD1stOTBreak;
            existingAwardPos.Day_MaxOrdHrSat1stOTBreak = awardsetup.Day_MaxOrdHrSat1stOTBreak;
            existingAwardPos.Day_MaxOrdHrSun1stOTBreak = awardsetup.Day_MaxOrdHrSun1stOTBreak;
            existingAwardPos.Day_MaxOrdHrPH1stOTBreak = awardsetup.Day_MaxOrdHrPH1stOTBreak;
            existingAwardPos.Week_MaxOrdHrWD2ndOTBreak_PayType = awardsetup.Week_MaxOrdHrWD2ndOTBreak_PayType;
            existingAwardPos.TOILDefaultAccrualMinutes = awardsetup.TOILDefaultAccrualMinutes;
            existingAwardPos.PayHigherRateEntireShift = awardsetup.PayHigherRateEntireShift;
            existingAwardPos.PayHigherRateEntireDay = awardsetup.PayHigherRateEntireDay;
            existingAwardPos.PayHigherPreviousDay = awardsetup.PayHigherPreviousDay;
            existingAwardPos.NoNoticeCancelPayType = awardsetup.NoNoticeCancelPayType;
            existingAwardPos.ShtNoticeCancelPayType = awardsetup.ShtNoticeCancelPayType;
            existingAwardPos.KMAllowanceType = awardsetup.KMAllowanceType;
            existingAwardPos.MakeUp_Pay_Threshold = awardsetup.MakeUp_Pay_Threshold;
            existingAwardPos.MakeUpLeadTime = awardsetup.MakeUpLeadTime;
            existingAwardPos.CancellationPayThreshold = awardsetup.CancellationPayThreshold;
            existingAwardPos.TeaBreakPaidIncWkdHrs = awardsetup.TeaBreakPaidIncWkdHrs;
            existingAwardPos.UnpaidMealBreak_PayType = awardsetup.UnpaidMealBreak_PayType;
            existingAwardPos.UnpaidMealBreakMin = awardsetup.UnpaidMealBreakMin;
            existingAwardPos.TeaBreakPaidTime = awardsetup.TeaBreakPaidTime;
            existingAwardPos.UnpaidMealBreakStartAt = awardsetup.UnpaidMealBreakStartAt;
            existingAwardPos.TeaBreakStartAt = awardsetup.TeaBreakStartAt;
            existingAwardPos.TeaBreakPaidTimeNext = awardsetup.TeaBreakPaidTimeNext;
            existingAwardPos.TeaBreakStartAtNext = awardsetup.TeaBreakStartAtNext;
            existingAwardPos.PayOvertimeForTeaBreaks = awardsetup.PayOvertimeForTeaBreaks;
            existingAwardPos.BrokenShiftAllowanceExtra_Threshold1 = awardsetup.BrokenShiftAllowanceExtra_Threshold1;
            existingAwardPos.BrokenShiftAllowanceExtra_Threshold = awardsetup.BrokenShiftAllowanceExtra_Threshold;
            existingAwardPos.BrokenshiftLimit = awardsetup.BrokenshiftLimit;
            existingAwardPos.BrokenShiftAllowance_PayType = awardsetup.BrokenShiftAllowance_PayType;
            existingAwardPos.IncludePreviousDayInBrokenShiftCalculation = awardsetup.IncludePreviousDayInBrokenShiftCalculation;
            existingAwardPos.DisableBrokenShiftAllowance = awardsetup.DisableBrokenShiftAllowance;
            existingAwardPos.MinBetweenShiftBreak = awardsetup.MinBetweenShiftBreak;
            existingAwardPos.MinBreakBetweenSleepovers = awardsetup.MinBreakBetweenSleepovers;
            existingAwardPos.MinBreakPayType = awardsetup.MinBreakPayType;
            existingAwardPos.PayOvertimeNoMinBreak = awardsetup.PayOvertimeNoMinBreak;
            existingAwardPos.Day_SatOrdHr_PayType = awardsetup.Day_SatOrdHr_PayType;
            existingAwardPos.Day_SunOrdHr_PayType = awardsetup.Day_SunOrdHr_PayType;
            existingAwardPos.Day_PHOrdHr_PayType = awardsetup.Day_PHOrdHr_PayType;
            existingAwardPos.PayOvertimeInsuficientMinDays = awardsetup.PayOvertimeInsuficientMinDays;
            existingAwardPos.Week_MinFreeDays = awardsetup.Week_MinFreeDays;
            existingAwardPos.FN_FreeDays = awardsetup.FN_FreeDays;
            existingAwardPos.W4_FreeDays = awardsetup.W4_FreeDays;
            existingAwardPos.MinHoursPerDay = awardsetup.MinHoursPerDay;
            existingAwardPos.MinHoursPerService = awardsetup.MinHoursPerService;
            existingAwardPos.BreakTimeWorked = awardsetup.BreakTimeWorked;
            existingAwardPos.OrdHoursStartTime = awardsetup.OrdHoursStartTime;
            existingAwardPos.OrdHoursEndTime = awardsetup.OrdHoursEndTime;
            existingAwardPos.BASE_PayType = awardsetup.BASE_PayType;
            existingAwardPos.Week_MaxOrdHr = awardsetup.Week_MaxOrdHr;
            existingAwardPos.Week_MaxOrdDays = awardsetup.Week_MaxOrdDays;
            existingAwardPos.Day_MaxOrdHr = awardsetup.Day_MaxOrdHr;
            existingAwardPos.FN_MaxOrdDays = awardsetup.FN_MaxOrdDays;
            existingAwardPos.FN_MaxOrdHr = awardsetup.FN_MaxOrdHr;
            existingAwardPos.FN_MaxOrdHrsDay = awardsetup.FN_MaxOrdHrsDay;
            existingAwardPos.W4_MaxOrdHrsDay = awardsetup.W4_MaxOrdHrsDay;
            existingAwardPos.W4_MaxOrdHours = awardsetup.W4_MaxOrdHours;
            existingAwardPos.W4_MaxOrdDays = awardsetup.W4_MaxOrdDays;
            existingAwardPos.Notes = awardsetup.Notes;
            existingAwardPos.Category = awardsetup.Category;
            existingAwardPos.Level = awardsetup.Level;
            existingAwardPos.Code = awardsetup.Code;
            existingAwardPos.Description = awardsetup.Description;

            // Save changes to the database
            await db.SaveChangesAsync();
        }

        return Ok(new
        {
            success = true,
            message = "AwardPos setup has been successfully updated."
        });
    }
    catch (Exception ex)
    {
        return BadRequest(new
        {
            success = false,
            message = ex.Message
        });
    }
}


    

        [HttpGet("awardsetup/allownces/{id}")]
        public async Task<IActionResult> GetAwardSetupAllownces(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, AwardID, Name, Category1, PayType AS [Pay Type], [Service Type] AS [WorkType/Activity], [Program] AS [Default Program], [Default] AS [Default For Autocreate],[ReplaceActivitiesWithAllowance] FROM AWARD_Allowances WHERE AwardID = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                awardId = GenSettings.Filter(rd["AwardID"]),
                                name = GenSettings.Filter(rd["Name"]),
                                category1 = GenSettings.Filter(rd["Category1"]),
                                payType = GenSettings.Filter(rd["Pay Type"]),
                                higherpayrank = GenSettings.Filter(rd["WorkType/Activity"]),
                                defaultProgram = GenSettings.Filter(rd["Default Program"]),
                                autocreate = GenSettings.Filter(rd["Default For Autocreate"]),
                                replaceActivitiesWithAllowance = GenSettings.Filter(rd["ReplaceActivitiesWithAllowance"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("awardsetup/allownces")]
        public async Task<IActionResult> PostAwardSetupAllowance([FromBody] AwardAllowance awardAllowance)
        {
            if (awardAllowance == null)
            {
                return BadRequest("Allowance data is required.");
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (var cmd = new SqlCommand(@"INSERT INTO AWARD_Allowances ([Default], [PayType], [Name], [Notes], [Service Type], [Program], [Category1], [ReplaceActivitiesWithAllowance], [AwardID]) VALUES 
                (@default,@paytype,@name,@notes,@serviceType,@program,@category1,@replaceActivitiesWithAllowance,@awardID)", conn))
                {
                    cmd.Parameters.AddWithValue("@default", awardAllowance.Default ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@paytype", awardAllowance.PayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@name", awardAllowance.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", awardAllowance.Notes ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@serviceType", awardAllowance.ServiceType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@program", awardAllowance.Program ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@category1", awardAllowance.Category1 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@replaceActivitiesWithAllowance",awardAllowance.ReplaceActivitiesWithAllowance);
                    cmd.Parameters.AddWithValue("@awardID", awardAllowance.AwardID ?? (object)DBNull.Value);
                    try
                    {
                        await conn.OpenAsync();
                        var result = await cmd.ExecuteNonQueryAsync();
                        return result > 0 ? Ok("Award Allowance created successfully.") : StatusCode(500, "Failed to create timeband.");
                    }
                    catch (Exception ex)
                    {
                        // Log the exception (ex) as necessary
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                }
            }
        }

        [HttpPut("awardsetup/allownces")]
        public async Task<IActionResult> updatAwardSetupAllowance([FromBody] AwardAllowance awardAllowance)
        {
            if (awardAllowance == null)
            {
                return BadRequest("Allowance data is required.");
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (var cmd = new SqlCommand(@"UPDATE AWARD_Allowances SET [Default] = @default, [PayType] =@paytype, [Name] = @name, [Notes] = @notes, [Service Type] = @serviceType, [Program] = @program, [Category1] = @category1, [ReplaceActivitiesWithAllowance] = @replaceActivitiesWithAllowance, [AwardID] = @awardID WHERE RecordNumber = @recordNumber", conn))
                {
                    cmd.Parameters.AddWithValue("@default", awardAllowance.Default ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@paytype", awardAllowance.PayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@name", awardAllowance.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", awardAllowance.Notes ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@serviceType", awardAllowance.ServiceType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@program", awardAllowance.Program ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@category1", awardAllowance.Category1 ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@replaceActivitiesWithAllowance",awardAllowance.ReplaceActivitiesWithAllowance);
                    cmd.Parameters.AddWithValue("@awardID", awardAllowance.AwardID ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@recordNumber", awardAllowance.RecordNumber);
                    
                    try
                    {
                        await conn.OpenAsync();
                        var result = await cmd.ExecuteNonQueryAsync();
                        return result > 0 ? Ok("Award Allowance updated successfully.") : StatusCode(500, "Failed to create Allowance.");
                    }
                    catch (Exception ex)
                    {
                        // Log the exception (ex) as necessary
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                }
            }
        }


        [HttpGet("awardsetup/specialshifts/{id}")]
        public async Task<IActionResult> GetAwardSetupTSpecialshifts(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, AwardID, Name,Activity,JobType,PayBackToBack,BackToBackPayType,Notes FROM AWARD_Special WHERE AwardID = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recordNumber    = GenSettings.Filter(rd["RecordNumber"]),
                                awardId         = GenSettings.Filter(rd["AwardID"]),
                                name            = GenSettings.Filter(rd["Name"]),
                                notes           = GenSettings.Filter(rd["Notes"]),
                                activity        = GenSettings.Filter(rd["activity"]),
                                jobType         = GenSettings.Filter(rd["jobType"]),
                                payBackToBack   = GenSettings.Filter(rd["payBackToBack"]),
                                backToBackPayType = GenSettings.Filter(rd["BackToBackPayType"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpPost("awardsetup/specialshifts")]
        public async Task<IActionResult> PostAwardSetupspecialshifts([FromBody] AwardSpecialshift awardSpecialshift)
        {
            if (awardSpecialshift == null)
            {
                return BadRequest("Allowance data is required.");
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (var cmd = new SqlCommand(@"INSERT INTO AWARD_Special ([PayBackToBack], [Name], [Notes], [Activity], [JobType], [BackToBackPayType], [AwardID])
                    VALUES (@payBackToBack,@name,@Notes,@activity,@jobtype,@backToBackPayType,@awardID)", conn))
                {
                    cmd.Parameters.AddWithValue("@payBackToBack", awardSpecialshift.PayBackToBack ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@name", awardSpecialshift.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", awardSpecialshift.Notes ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@activity", awardSpecialshift.Activity ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@jobtype", awardSpecialshift.JobType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@backToBackPayType", awardSpecialshift.BackToBackPayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@awardID", awardSpecialshift.AwardID ?? (object)DBNull.Value);
                    try
                    {
                        await conn.OpenAsync();
                        var result = await cmd.ExecuteNonQueryAsync();
                        return result > 0 ? Ok("Award Special Shifts created successfully.") : StatusCode(500, "Failed to create timeband.");
                    }
                    catch (Exception ex)
                    {
                        // Log the exception (ex) as necessary
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                }
            }
        }

        [HttpPut("awardsetup/specialshifts")]
        public async Task<IActionResult> updateAwardSetupspecialshifts([FromBody] AwardSpecialshift awardSpecialshift)
        {
            if (awardSpecialshift == null)
            {
                return BadRequest("Allowance data is required.");
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (var cmd = new SqlCommand(@"UPDATE AWARD_Special SET [PayBackToBack] = @payBackToBack, [Name] = @name, [Notes] =@notes, [Activity] = @activity, [JobType] = @jobtype, [BackToBackPayType] = @backToBackPayType, [AwardID] = @awardID WHERE RecordNumber = @recordnumber", conn))
                {
                    cmd.Parameters.AddWithValue("@payBackToBack", awardSpecialshift.PayBackToBack ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@name", awardSpecialshift.Name ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", awardSpecialshift.Notes ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@activity", awardSpecialshift.Activity ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@jobtype", awardSpecialshift.JobType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@backToBackPayType", awardSpecialshift.BackToBackPayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@awardID", awardSpecialshift.AwardID ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@recordNumber", awardSpecialshift.AwardID ?? (object)DBNull.Value);
                    try
                    {
                        await conn.OpenAsync();
                        var result = await cmd.ExecuteNonQueryAsync();
                        return result > 0 ? Ok("Award Special Shifts udated successfully.") : StatusCode(500, "Failed to create timeband.");
                    }
                    catch (Exception ex)
                    {
                        // Log the exception (ex) as necessary
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                }
            }
        }

        [HttpGet("awardsetup/timebands/{id}")]
        public async Task<IActionResult> GetAwardSetupTimebands(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNo, [Award], CONVERT(VARCHAR(5), CAST(StartTime AS TIME), 108) AS FormattedTime, CONVERT(VARCHAR(5), CAST(EndTime AS TIME), 108) AS FormattedEnd,[PayType],[DayType],[vRank] AS [Higher Pay Order] FROM TimeBand WHERE Award = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recordNo = GenSettings.Filter(rd["RecordNo"]),
                                award         = GenSettings.Filter(rd["Award"]),
                                startTime     = GenSettings.Filter(rd["FormattedTime"]),
                                endTime       = GenSettings.Filter(rd["FormattedEnd"]),
                                payType       = GenSettings.Filter(rd["PayType"]),
                                dayType       = GenSettings.Filter(rd["DayType"]),
                                higherpayrank = GenSettings.Filter(rd["Higher Pay Order"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }


        [HttpPost("awardsetup/timebands")]
        public async Task<IActionResult> PostAwardSetupTimebands([FromBody] Timeband timeband)
        {
            if (timeband == null)
            {
                return BadRequest("Timeband data is required.");
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (var cmd = new SqlCommand(@"INSERT INTO TimeBand([AWARD],[StartTime],[EndTime],[PayType],[DayType],[vRank])
                                                VALUES(@award,@starttime,@endtime,@paytype,@daytype,@vrank)", conn))
                {
                    cmd.Parameters.AddWithValue("@award", timeband.Award ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@starttime", timeband.StartTime ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@endtime", timeband.EndTime ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@paytype", timeband.PayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@daytype", timeband.DayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@vrank", timeband.vRank ?? (object)DBNull.Value);
                    try
                    {
                        await conn.OpenAsync();
                        var result = await cmd.ExecuteNonQueryAsync();
                        return result > 0 ? Ok("Timeband created successfully.") : StatusCode(500, "Failed to create timeband.");
                    }
                    catch (Exception ex)
                    {
                        // Log the exception (ex) as necessary
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                }
            }
        }


        [HttpPut("awardsetup/timebands")]
        public async Task<IActionResult> updateAwardSetupTimebands([FromBody] Timeband timeband)
        {
            if (timeband == null)
            {
                return BadRequest("Timeband data is required.");
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (var cmd = new SqlCommand(@"UPDATE TimeBand SET  [Award] =@award,[StartTime] =@starttime,[EndTime] = @endtime,[PayType] = @paytype,[vRank] = @vrank,[DayType] = @daytype WHERE RecordNo = @recordNo", conn))
                {
                    cmd.Parameters.AddWithValue("@award", timeband.Award);
                    cmd.Parameters.AddWithValue("@starttime", timeband.StartTime ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@endtime", timeband.EndTime ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@paytype", timeband.PayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@daytype", timeband.DayType ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@vrank", timeband.vRank ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@recordNo",timeband.RECORDNO);
                    try
                    {
                        await conn.OpenAsync();
                        var result = await cmd.ExecuteNonQueryAsync();
                        return result > 0 ? Ok("Timeband updated successfully.") : StatusCode(500, "Failed to create timeband.");
                    }
                    catch (Exception ex)
                    {
                        // Log the exception (ex) as necessary
                        return StatusCode(500, $"Internal server error: {ex.Message}");
                    }
                }
            }
        }

        [HttpDelete("awardsetup/timebands/{recordNo}")]
        public async Task<IActionResult> DeleteAwardSetupTimebands(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE HumanResources WHERE RecordNumber = @recordNo AND Type = 'SVC_COMP'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }



        [HttpGet("configuration/sysnumbers/{id}")]
        public async Task<IActionResult> GetsysNumbers(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT SQLID,CustomerID as recipient,CarerID as Staff,Batch# as batch,Invoice# as invoice,TsheetID as tsheetID FROM SYSTABLE where SQLID= @id", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                SQLID         = GenSettings.Filter(rd["SQLID"]),
                                customerID    = GenSettings.Filter(rd["recipient"]),
                                carerID       = GenSettings.Filter(rd["staff"]),
                                batch         = GenSettings.Filter(rd["batch"]),
                                invoice       = GenSettings.Filter(rd["invoice"]),
                                tsheetID      = GenSettings.Filter(rd["tsheetID"]),
                            });
                        }
                    }
                    return Ok(list[0]);
                }
            }
        }
        
        [HttpGet("configuration/loggedPortalUsers")]
        public async Task<IActionResult> GetLoggedOnPortalUsers(ApplicationUser user)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [User],[BrowserName] from LoggedUsers", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                User            = GenSettings.Filter(rd["User"]),
                                BrowserName     = GenSettings.Filter(rd["BrowserName"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }


          [HttpGet("awardsetup/specialshifts/breakdown/{id}")]
        public async Task<IActionResult> GetAwardSetupTSpecialshiftsBreakdown(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, JobID, StartTime, EndTime, PayType FROM AWARD_Special_Detail WHERE JobID = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;

                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                JobID        = GenSettings.Filter(rd["JobID"]),
                                StartTime    = GenSettings.Filter(rd["StartTime"]),
                                EndTime      = GenSettings.Filter(rd["EndTime"]),
                                PayType      = GenSettings.Filter(rd["PayType"]),
                            });
                            }
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("awardsetup/allowances/rules/{id}")]
        public async Task<IActionResult> GetAwardSetupAllowancesRules(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, AllowanceID, RuleName AS [Rule Type], [Rule] FROM AWARD_Allowance_Rules WHERE AllowanceID = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = id;
                    
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                allowanceID  = GenSettings.Filter(rd["AllowanceID"]),
                                ruleType     = GenSettings.Filter(rd["Rule Type"]),
                                rule         = GenSettings.Filter(rd["Rule"]),
                            });
                            }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("allowancesinclusionactivity/data/{personID}")]
        public async Task<IActionResult> GetAllowancesinclusionactivityData(string personID)
        {
            var conn = _context.Database.GetDbConnection() as SqlConnection;

            

            string query = @"SELECT RecordNumber, Name, Notes 
                            FROM HumanResources 
                            WHERE PersonID = @personID AND [Type] = 'ALLINCACT'
                            ORDER BY Name";

            try
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.Add("@personID", SqlDbType.NVarChar).Value = personID;

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]?.ToString()),
                                Name = GenSettings.Filter(rd["Name"]?.ToString()),
                                Notes = GenSettings.Filter(rd["Notes"]?.ToString()),
                            });
                        }
                    }

                    return Ok(list);
                }
            }
            catch (SqlException ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database error: " + ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,"An error occurred: " + ex.Message);
            }
            finally
            {
                await conn.CloseAsync();
            }
        }
       


        [HttpGet("allowancesinclusioncompetency/data/{personID}")]
        public async Task<IActionResult> GetAllowancesinclusioncompetencyesData(string personID)
        {
            var conn = _context.Database.GetDbConnection() as SqlConnection;

            

            string query = @"SELECT RecordNumber, Name, Notes 
                            FROM HumanResources 
                            WHERE PersonID = @personID AND [Type] = 'ALLINCCOMP'
                            ORDER BY Name";

            try
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.Add("@personID", SqlDbType.NVarChar).Value = personID;

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]?.ToString()),
                                Name = GenSettings.Filter(rd["Name"]?.ToString()),
                                Notes = GenSettings.Filter(rd["Notes"]?.ToString()),
                            });
                        }
                    }

                    return Ok(list);
                }
            }
            catch (SqlException ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database error: " + ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,"An error occurred: " + ex.Message);
            }
            finally
            {
                await conn.CloseAsync();
            }
        }



        [HttpGet("allowancesexclusionactivities/data/{personID}")]
        public async Task<IActionResult> GetallowancesexclusionactivitiesData(string personID)
        {
            var conn = _context.Database.GetDbConnection() as SqlConnection;

            

            string query = @"SELECT RecordNumber, Name, Notes 
                            FROM HumanResources 
                            WHERE PersonID = @personID AND [Type] = 'ALLEXCACT'
                            ORDER BY Name";

            try
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.Add("@personID", SqlDbType.NVarChar).Value = personID;

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]?.ToString()),
                                Name = GenSettings.Filter(rd["Name"]?.ToString()),
                                Notes = GenSettings.Filter(rd["Notes"]?.ToString()),
                            });
                        }
                    }

                    return Ok(list);
                }
            }
            catch (SqlException ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database error: " + ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,"An error occurred: " + ex.Message);
            }
            finally
            {
                await conn.CloseAsync();
            }
        }



          [HttpGet("allowancesexclusioncompetency/data/{personID}")]
        public async Task<IActionResult> GetallowancesexclusioncompetencyData(string personID)
        {
            var conn = _context.Database.GetDbConnection() as SqlConnection;

            

            string query = @"SELECT RecordNumber, Name, Notes 
                            FROM HumanResources 
                            WHERE PersonID = @personID AND [Type] = 'ALLEXCOMP'
                            ORDER BY Name";

            try
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.Add("@personID", SqlDbType.NVarChar).Value = personID;

                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]?.ToString()),
                                Name = GenSettings.Filter(rd["Name"]?.ToString()),
                                Notes = GenSettings.Filter(rd["Notes"]?.ToString()),
                            });
                        }
                    }

                    return Ok(list);
                }
            }
            catch (SqlException ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database error: " + ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,"An error occurred: " + ex.Message);
            }
            finally
            {
                await conn.CloseAsync();
            }
        }







    }
}