import { Component, OnInit, ElementRef } from '@angular/core'
import { Router } from '@angular/router'

import { GlobalService, roles, LoginService } from '@services/index'

export const appView = {
    provider: 'Provider',
    client: 'Client',
    admin: 'Admin'
}

@Component({
    host: {
        '(document:click)': 'onClick($event)'
    },
    selector: 'headernav',
    templateUrl: './header.html',
    styles: [
        `
    header{
        height:2rem;
    }
    .branding{
        height:inherit;
    }
    .branding .nav-link{
        height:inherit;
    }
    img{
        width: 65px;
        margin-right: 12px;
    }
    .material-icons{
        line-height:2.5rem;
    }
    .header-wrapper{
        position: relative;
        height: 100%;
        width: 95%;
        margin: 0 auto;
    }
    .list-wrapper{
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }
    .right{
        float:right;
    }
    .header.header-3, header.header-3{
        background-color:#546baf;
    }
    .nav-link{
        cursor:pointer;
    }
    .btn-logout{
        float: right;
        border: 0;
        background: none;
        outline: none;
        color: #fafafa;
    }
    .dropdowns{
        position: absolute;
        right: 5px;
        top: 2rem;
        z-index: 15;
        background: white;
        color: #000;
        box-shadow: 0 1px 0.125rem rgba(115,115,115,.25);
        border-radius: 3px;
        width: 5.5rem;
        text-align: center;
    }
    ul{
        list-style:none;
        margin: 5px 0 ;
    }
    li{
        padding:5px 10px;
        cursor:pointer;
    }
    li:hover{
        background:#f5f5f5;
    }   
        `
    ]
})

export class HeaderNav implements OnInit {
    bgColor: any;
    view: string = "";
    open: boolean = false;

    ifClientManager: boolean = false;
    ifClientAdmin: boolean = false;

    tempRole: string;
    constructor(
        private globalS: GlobalService,
        private loginS: LoginService,
        private router: Router,
        private elem: ElementRef
    ) {

    }

    ngOnInit() {
        this.tempRole = this.globalS.isRole();

        if (this.tempRole == roles.admin) {
            this.view = appView.admin;
            this.bgColor = {
                'background': 'linear-gradient(90deg, #235ba2, #13498e)'
            };
            this.ifClientAdmin = true;
        }
        else if (this.tempRole == roles.client || this.tempRole == roles.manager) {
            this.view = appView.client;
            this.bgColor = {
                'background': 'linear-gradient(to right, #00d2ff, #3a7bd5)'
            }
            this.ifClientManager = this.tempRole == roles.manager ? true : false;
        }
        else if (this.tempRole == roles.provider) {
            this.view = appView.provider;
            this.bgColor = {
                'background': 'linear-gradient(rgb(75, 208, 165), rgb(92, 222, 180))'
            }
        }
    }

    onClick(event: Event) {
        if (!this.elem.nativeElement.contains(event.target))
            this.open = false;
    }

    toHome() {
        var tempRole = this.globalS.isRole();
        if (tempRole == roles.admin) {
            this.router.navigate(['admin'])
        }
        else if (tempRole == roles.client || tempRole == roles.manager) {
            this.router.navigate(['client'])

        }
        else if (tempRole == roles.provider) {
            this.router.navigate(['provider'])
        }

    }

    toLoginSessions() {
        this.router.navigate(['admin/sessions'])
    }

    toUserSettings() {
        var tempRole = this.globalS.isRole();

        if (tempRole == roles.client || tempRole == roles.manager) {
            this.router.navigate(['client/settings'])

        }
        else if (tempRole == roles.provider) {
            this.router.navigate(['settings'])
        }
    }

    toBelongsTo() {
        var tempRole = this.globalS.isRole();
        if (tempRole == roles.admin) {
            this.router.navigate(['admin/members'])
        }
        else if (tempRole == roles.client || tempRole == roles.manager) {
            this.router.navigate(['client/members'])

        }
        else if (tempRole == roles.provider) {
            this.router.navigate(['provider'])
        }
    }

    logout() {
        this.loginS.logout(this.globalS.decode().uniqueID).subscribe(data => {
            // this.globalS.logout();             
        });

        this.globalS.logout();
    }
}