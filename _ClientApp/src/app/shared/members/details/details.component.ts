import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MemberService, GlobalService } from '@services/index';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  id: any;
  view: number = 1;
  member: any;
  user: any;

  constructor(
    private route: ActivatedRoute,
    private globalS: GlobalService
  ) { }

  ngOnInit() {


    this.member = JSON.parse(this.globalS.member);
    
    this.user = {
      code: this.member.accountNo,
      view: 'recipient'
  };
  }

}
