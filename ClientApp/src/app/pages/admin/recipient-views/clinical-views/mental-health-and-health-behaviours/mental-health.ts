

import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, othersType, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { functionalStatus} from '@services/global.service'

@Component({
    
   styleUrls: ['./mental-health.css'],
  templateUrl: './mental-health.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ClinicalMentalHealth implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

  
    timeReasons : Array<string> = functionalStatus.timeReasons;      
   
    confusion:Array<any> = functionalStatus.confusion;
    availability:Array<any> = functionalStatus.availability;
    friendship:Array<any> = functionalStatus.friendship;
    problems:Array<any> = functionalStatus.problems;
    surity:Array<any> = functionalStatus.surity;

   

    mentalHealthForm: FormGroup;
    staffs: Array<string> = []

    dateFormat: string = dateFormat;

    occupationDefault: string;
    financialClassDefault: string;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'mental-health')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        this.user = this.sharedS.getPicked();
        this.buildForm();        
       // this.populate();
        this.search(this.user);

       

        this.sharedS.emitSaveAll$.subscribe(data => {
            if (data.type=='Recipient' && data.tab =='Clinical Detail' && this.globalS.isCurrentRoute(this.router, 'mental-health')){
            
              this.save();
            }
          })

        
    
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    search(data: any){
        this.timeS.getMenalHealth(data.id)
            .subscribe(data => {
                this.mentalHealthForm.patchValue(data)               
                this.detectChanges();
            });
    }
    //FP2_WalkingDistance, FP3_Shopping, FP4_Medicine,FP5_Money , FP6_Walking, FP7_Bathing, FP8_Memory, FP9_Behaviour, FPA_Communication, FPA_Dressing, FPA_Eating, FPA_Toileting, FPA_GetUp


    buildForm(){
        this.mentalHealthForm = this.formBuilder.group({
            recordNumber : 0,
            pP_K10_1: [null],
            pP_K10_2: [null],
            pP_K10_3: [null],
            pP_K10_4: [null],
            pP_K10_5: [null],
            pP_K10_6: [null],
            pP_K10_7: [null],
            pP_K10_8: [null],
            pP_K10_9: [null],
            pP_K10_10: [null],
            pP_K10_Score: [null],
            pP_SleepingDifficulty: [null],
            pP_SleepingDifficultyComments: [null],
            pP_PersonalSupport: [null],
            pP_PersonalSupportComments: [null],
            pP_Relationships_KeepUp: [null],
            pP_Relationships_Problem: [null],
            pP_RelationshipsComments: [null],
            pP_Relationships_SP: [null],
            pP_Relationships_SPComments: [null]

        });
    }

    canDeactivate() {
        if (this.mentalHealthForm && this.mentalHealthForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Changes have been detected. Save Changes?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                    
                }
            });
        }
        return true;
    }

  

    save(){
        this.timeS.updateMentalHealth(this.mentalHealthForm.value, this.user.id).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
    }

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }


}