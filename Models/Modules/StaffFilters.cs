using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Adamas.Models.Modules{
    public class StaffFilters {

        public string searchText { get; set; }
        public string status { get; set; }
        public string gender { get; set; }
        public Boolean? staff {get;set;}
        public Boolean? brokers {get;set;}
        public Boolean? volunteers {get;set;}
        public Boolean? onleaveStaff {get;set;}
        public Boolean? allTeamAreas { get; set; }
        public List<String> selectedTeamAreas { get; set; }
        public Boolean? allcat { get; set; }
        public List<String> selectedCategories {get;set;}
        public Boolean? allBranches { get; set; }
        public  List<String> selectedbranches {get;set;}
        public Boolean? allCordinatore { get; set; }
        public List<String> selectedCordinators {get;set;}

        public Boolean? allSkills { get; set; }
        public List<String> selectedSkills {get;set;}
        public List<CriteriaList> Criterias { get; set; }

        public string firstRecipient { get; set; }
          public string userName { get; set; }       
         public string date { get; set; }        
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string endLimit { get; set; }
            
    }
}