import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';

import { RECIPIENT_OPTION, ModalVariables, ProcedureRoster, UserToken, CallAssessmentProcedure, CallProcedure, CallReferralOutProcedure } from '../../modules/modules';
import { ListService, GlobalService, quantity, unit, dateFormat, ClientService, timeSteps, ShareService } from '@services/index';
import { SqlWizardService } from '@services/sqlwizard.service';


import { HttpClient, HttpEvent, HttpEventType, HttpRequest, HttpResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { mergeMap, takeUntil, debounceTime, distinctUntilChanged, map, switchMap, skip, startWith } from 'rxjs/operators';
import { EMPTY, combineLatest, Observable, of } from 'rxjs';
import { startsWith } from 'lodash';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { UploadChangeParam } from 'ng-zorro-antd/upload';
import format from 'date-fns/format';
import { setDate } from 'date-fns';
import { filter } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { pensioners } from '@services/global.service'
import addDays from 'date-fns/addDays'

import { ReferralSourceDto } from '@modules/modules';
import { NoopAnimationPlayer } from '@angular/animations';

// enum RECIPIENT_OPTION {
//   REFER_IN = "REFER_IN",
//   REFER_ON = "REFER_ON",
//   NOT_PROCEED = "NOT_PROCEED",
//   ASSESS = "ASSESS",
//   ADMIT = "ADMIT",
//   WAIT_LIST = "WAIT_LIST",
//   DISCHARGE = "DISCHARGE",
//   SUSPEND = "SUSPEND",
//   REINSTATE = "REINSTATE",
//   DECEASE = "DECEASE",
//   ADMIN = "ADMIN",
//   ITEM = "ITEM",
// }
const NOTE_TYPE: {} = {
  'case': 'CASENOTE',
  'op': 'OPNOTE',
  'clinical': 'CLINICALNOTE'
}

const defaultTimeSpent = new Date().setHours(0, 15);
const defaultDate = new Date().setHours(0, 0);
const defaultDateTime = new Date();

@Component({
  selector: 'app-recipients-options',
  templateUrl: './recipients-options.component.html',
  styleUrls: ['./recipients-options.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,


})
export class RecipientsOptionsComponent implements OnInit, OnChanges, OnDestroy {
  private unsubscribe$ = new Subject()

  @Input() open: any;
  @Input() option: RECIPIENT_OPTION;
  @Input() user: any;
  @Input() from: any;
  @Input() program: string;

  dateFormat: string = dateFormat;

  topBelow = { top: '10px' }
  otherTitleDetais: any;
  FUNDING_TYPE: string;
  BRANCH_NAME: string;
  DOCUMENTID: number;
  COORDINATOR: string;
  EMAIL_OF_COORDINATOR: string;

  lstusers: Array<any> = [];
  caseManagers: Array<any> = [];
  mainTitle = `{background-color: #F18805 !important;}`;
  ViewStaffList: boolean;
  chkMultiStaff:boolean;
  usersettings:any;
  docId:string

  referralRadioValue: any;
  referralCheckOptions: Array<any> = [
    {
      name: 'NDIA',
      index: 1,
      checked: false
    },
    {
      name: 'HCP',
      index: 2,
      checked: false
    },
    {
      name: 'Support At Home',
      index: 3,
      checked: false
    },
    {
      name: 'DEX',
      index: 4,
      checked: false
    },
    {
      name: 'OTHER',
      index: 5,
      checked: false
    }
  ]

  CURRENT_DATE: Date = new Date();

  gwapo: Array<any> = [
    {
      program: 'hello',
      checked: true,
      selected: '',
      type: null
    },
    {
      program: 'aaaa',
      checked: true,
      selected: '',
      type: null
    },
    {
      program: 'bbbb',
      checked: true,
      selected: '',
      type: null
    }
  ]


  itemGroup: FormGroup;
  adminGroup: FormGroup;
  deceaseGroup: FormGroup;
  reinstateGroup: FormGroup;
  suspendGroup: FormGroup;
  dischargeGroup: FormGroup;
  dischargeGroupPerson: FormGroup;
  waitListGroup: FormGroup;
  admitGroup: FormGroup;
  assessGroup: FormGroup;
  notProceedGroup: FormGroup;
  referOnGroup: FormGroup;
  referInGroup: FormGroup;
  deactivateGroup: FormGroup;

  whatOptionVar: ModalVariables;
  recipient: any;
  current: number = 0;

  itemOpen: boolean = false;
  adminOpen: boolean = false;
  deceaseOpen: boolean = false;
  reinstateOpen: boolean = false;
  suspendOpen: boolean = false;
  dischargeOpen: boolean = false;
  dischargeOpenPerson: boolean = false;
  waitListOpen: boolean = false;
  admitOpen: boolean = false;
  assessOpen: boolean = false;
  notProceedOpen: boolean = false;
  referOnOpen: boolean = false;
  referInOpen: boolean = false;
  deactivateOpen: boolean = false;
  referIndocument: boolean = false;
  referdocument: boolean = false;
  admission: boolean = false;
  loadPrograms: boolean = false;

  inputValue: any;
  radioValue: any = 'case';
  checkedPrograms: any;
  uncheckedPrograms: Array<any>;

  checked: boolean = false;
  noteArray: Array<any> = [];
  remindersRecipientArray: Array<string>;


  //last 4 tabs
  notifCheckBoxGroup: any;
  notifCheckBoxes: Array<string> = []
  notifFollowUpGroup: any;
  notifDocumentsGroup: any;
  followups: Array<string>;
  notifications: Array<string>;
  timeSteps: Array<string>;
  documentlist: Array<string>;
  datalist: Array<string>;
  private destroy$ = new Subject();
  AllServices: boolean ;
  SelectAllServices: boolean = true;
  SelectedReferralTypes: Array<string> = []
  dischargeTypes: Array<string> = []

  dischargeTypes$: Observable<any>;

  itemTypes$: Observable<any>;
  reasons$: Observable<any>;
  dischargeReason$: Observable<any>;
  cancellationCode$: Observable<any>;
  referralCode$: Observable<any>;
  referralType$: Observable<any>;
  referralSource$: Observable<any>;
  suspendreasons: Array<any>=[];

  globalFormGroup: FormGroup;

  newReferralUser: any;

  globalProgramSelection: any;

  quantity: Array<any> = quantity;
  unit: Array<any> = unit;

  programs: Array<any> = [];
  token: UserToken;

  fileList2: Array<any> = [];

  date: any;
  urlPath: string = `api/v2/file/upload-document-remote`;
  acceptedTypes: string = "image/png,image/jpeg,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf";
  file: File;
  originalPackageName: string;
  admissionActiviType: any=['*ADMISSION'];

  activityCodes: Array<any> = [];
  loadActivityCodes: boolean = false;
  pensioners: any = pensioners;
  brnachList: Array<any> = [];
  staffList_original: Array<any> = [];
  staffList: Array<any> = [];
  IncludeNonBranch: boolean = false;
  @Input() loadOption: Subject<any> = new Subject();

  constructor(
    private listS: ListService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef,
    private globalS: GlobalService,
    private sqlWiz: SqlWizardService,
    private clientS: ClientService,
    private http: HttpClient,
    private msg: NzMessageService,
    private SharedS: ShareService
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.token = this.globalS.decode();
    this.date = format(new Date(), 'MM-dd-yyyy');
  
    this.buildForm();
    this.loadOption.subscribe(data=>{
      this.user = data;
      
        if (this.option !=  RECIPIENT_OPTION.REFER_IN) return;
      // if ('branch' in this.user) {
      //   this.BRANCH_NAME = this.user.branch;
      // } else {
      //   this.listS.getspecificbranch(this.user.id)
      //     .subscribe(data => {
      //       this.BRANCH_NAME = data.branch;
      //       this.COORDINATOR = data.coordinator;
      //     });
      // }
      // if ('docId' in this.user) {
      //   this.DOCUMENTID = this.user.docId;
      // }

      
      // this.buildForm();
       this.populate();
       this.populateList();
       this.VALUE_CHANGES();
      // this.openModal();

    });
  }

  ngOnDestroy() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
     // if (this.option ==  RECIPIENT_OPTION.REFER_IN) return;
      if (property == 'open' &&  !changes[property].firstChange && changes[property].currentValue != null) {
        // GETS Branch name or Gets it through database

        if ('branch' in this.user) {
          this.BRANCH_NAME = this.user.branch;
        } else {
          this.listS.getspecificbranch(this.user.id)
            .subscribe(data => {
              this.BRANCH_NAME = data.branch;
              this.COORDINATOR = data.coordinator;
            });
        }
        if ('docId' in this.user) {
          this.DOCUMENTID = this.user.docId;
        }
  
        this.buildForm();
        this.populate();
        this.populateList();
        this.VALUE_CHANGES();
        this.openModal();

      }
    }
  }

  selectOption: number;
  ForceAllPrograms: boolean;
  DisplayallProgram(event: any) {
    this.ForceAllPrograms = event;
    let dd = { startDate: null, endDate: format(new Date(), 'yyy/MM/dd') };
    var prog = this.referOnGroup.get('programs') as FormArray;

    if (this.option == RECIPIENT_OPTION.ASSESS) {
      prog = this.assessGroup.get('programs') as FormArray;

    }
    if (this.option == RECIPIENT_OPTION.ADMIN) {
      prog = this.adminGroup.get('programs') as FormArray;

    }
    if (this.option == RECIPIENT_OPTION.DISCHARGE) {
      prog = this.dischargeGroup.get('programs') as FormArray;

    }
    if (this.option == RECIPIENT_OPTION.ITEM) {
      prog = this.itemGroup.get('programs') as FormArray;

    }
    prog.clear();

    if (this.ForceAllPrograms == true) {
      this.listS.getprograms(dd).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        data.map(x => prog.push(this.createProgramForm({ program: x, checked: false, type: 0, selected: false })));

        if (this.option == RECIPIENT_OPTION.REFER_ON) 
          this.referOnGroup.get('programs').patchValue(data);
        else if (this.option == RECIPIENT_OPTION.ASSESS) 
          this.assessGroup.get('programs').patchValue(data);
        else if (this.option == RECIPIENT_OPTION.ADMIN) 
          this.adminGroup.get('programs').patchValue(data);
        else if (this.option == RECIPIENT_OPTION.DISCHARGE) 
          this.dischargeGroup.get('programs').patchValue(data);
        else if (this.option == RECIPIENT_OPTION.ITEM) 
          this.itemGroup.get('programs').patchValue(data);

        this.cd.detectChanges();
      });

    } else {
      if (this.option == RECIPIENT_OPTION.REFER_ON) 
        this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.REFER_ON);
      else if (this.option == RECIPIENT_OPTION.ASSESS) 
        this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ASSESS);
      else if (this.option == RECIPIENT_OPTION.ADMIN) 
        this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ADMIN);
      else if (this.option == RECIPIENT_OPTION.DISCHARGE) 
        this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.DISCHARGE);
      else if (this.option == RECIPIENT_OPTION.ITEM) 
        this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ITEM);
    }
  }
  openModal() {
    this.current = 0;
    this.selectOption
    if (this.option == RECIPIENT_OPTION.REFER_IN) { this.referInOpen = true; this.selectOption = 0; this.globalFormGroup = this.referInGroup; }
    if (this.option == RECIPIENT_OPTION.REFER_ON) { this.referOnOpen = true; this.selectOption = 1; this.globalFormGroup = this.referOnGroup; }
    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) { this.notProceedOpen = true; this.selectOption = 2; this.globalFormGroup = this.notProceedGroup; }
    if (this.option == RECIPIENT_OPTION.ASSESS) { this.assessOpen = true; this.selectOption = 3; this.globalFormGroup = this.assessGroup; }
    if (this.option == RECIPIENT_OPTION.ADMIT) { this.admitOpen = true; this.selectOption = 4; this.globalFormGroup = this.adminGroup; }
    if (this.option == RECIPIENT_OPTION.WAIT_LIST) { this.waitListOpen = true; this.selectOption = 5; this.globalFormGroup = this.waitListGroup; }
    if (this.option == RECIPIENT_OPTION.DISCHARGE) { this.dischargeOpen = true; this.selectOption = 6; this.globalFormGroup = this.dischargeGroup; }
    if (this.option == RECIPIENT_OPTION.SUSPEND) { this.suspendOpen = true; this.selectOption = 7; this.globalFormGroup = this.suspendGroup; }
    if (this.option == RECIPIENT_OPTION.REINSTATE) { this.reinstateOpen = true; this.selectOption = 8; this.globalFormGroup = this.reinstateGroup; }
    if (this.option == RECIPIENT_OPTION.DECEASE) { this.deceaseOpen = true; this.selectOption = 9; this.globalFormGroup = this.deceaseGroup; }
    if (this.option == RECIPIENT_OPTION.ADMIN) { this.adminOpen = true; this.selectOption = 10; this.globalFormGroup = this.adminGroup; }
    if (this.option == RECIPIENT_OPTION.ITEM) { this.itemOpen = true; this.selectOption = 11; this.globalFormGroup = this.itemGroup; }
    if (this.option == RECIPIENT_OPTION.DEACTIVATE) { this.deactivateOpen = true; this.selectOption = 12; this.globalFormGroup = this.deactivateGroup; }
    
    this.changeDetection();
  }

  buildForm(): void {

    this.usersettings= this.globalS.settings;

    this.itemGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,

      referralType: null,
      suppliedDate: new Date(defaultDate),

      date: new Date(defaultDate),
      refNo: null,
      quantity: null,
      unit: null,
      charge: null,
      gst: false,

      radioGroup: this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'ITEM',
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null,
      allPrograms:false
      
    });
    this.itemGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
     this.itemGroup.get('programChecked').valueChanges
    .subscribe(x => {
      this.selectedProgramChange(x); // Call your function
    });
    this.adminGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,
      serviceType:null,
      actionDate: new Date(),
      time: new Date(),
      timeSpent: new Date(defaultTimeSpent),
      radioGroup:this.usersettings.wizardDefaultNote,
      notes: '',
      caseCategory: 'ADMISSION',
      publishToApp: false,
      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      adminsssion: null,
      multipleStaff: null,
      allPrograms: false,
      referralType: 'ADMISSION', 
      referralCode: 'ADMISSION', 
      referralSource: 'ADMISSION'
    });

    this.adminGroup.get('programChecked').valueChanges
    .pipe(
      switchMap(x => {
        if (!x) return EMPTY;
        this.globalProgramSelection = {
          program: x,
          option: 'ADMIN'
        }
        return this.listS.getreferraltype(this.globalProgramSelection)
      }),
      switchMap(data => {
        this.admissionActiviType = data;
        if (data.length == 1) {
          this.admitGroup.patchValue({
            adminGroup: this.admissionActiviType[0],
            caseCategory : 'ADMISSION'
          });
        }
        return this.listS.gethumanresourcetypes({ program: this.globalProgramSelection.program })
      }),
      switchMap(data => {
        this.GET_PRIMARY_ADDRESS()
        this.FUNDING_TYPE =   data;
        this.loadActivityCodes = true;
        return this.listS.getreferraltypes({
          ProgramName: this.globalProgramSelection.program,
          ProgramType: RECIPIENT_OPTION.ADMIN
        })
      }),
    ).subscribe(data => {
      this.activityCodes = data;
      this.loadActivityCodes = false;
    });
    this.adminGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });

    this.reinstateGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,

      referralDate: new Date(),
      time: new Date(),
      timeSpent: new Date(defaultTimeSpent),
      radioGroup:this.usersettings.wizardDefaultNote,
      notes: '',
      caseCategory: 'ADMISSION',
      publishToApp: false,
      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      adminsssion: null,
      multipleStaff: null,
    });

    this.reinstateGroup.get('programChecked').valueChanges
    .pipe(
      switchMap(x => {
        if (!x) return EMPTY;
        this.globalProgramSelection = {
          program: x,
          option: 'ADMIN'
        }
        return this.listS.getreferraltype(this.globalProgramSelection)
      }),
      switchMap(data => {
        this.admissionActiviType = data;
        if (data.length == 1) {
          this.admitGroup.patchValue({
            adminGroup: this.admissionActiviType[0],
            caseCategory : 'ADMISSION'
          });
        }
        return this.listS.gethumanresourcetypes({ program: this.globalProgramSelection.program })
      }),
      switchMap(data => {
        this.GET_PRIMARY_ADDRESS()
        this.FUNDING_TYPE = data;
        this.loadActivityCodes = true;
        return this.listS.getreferraltypes({
          ProgramName: this.globalProgramSelection.program,
          ProgramType: RECIPIENT_OPTION.ADMIN
        })
      }),
    ).subscribe(data => {
      this.activityCodes = data;
      this.loadActivityCodes = false;
    });

    this.deceaseGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,

      dateOfDeath: null,
      reason: null,
      dischargeType: null,

      dischargeDate: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: null,
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null
    });

    this.suspendGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,
      reason: 'NSDF : No show due to family issues.',
      cancellationCode: null,
      recordFormal: true,
      recordHours: true,
      date: new Date(),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),
      suspendDateFrom: null,

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: null,
      publishToApp: false,
      referralCode : 'SUSPEND',
      referralType : 'SUSPEND',
      referralSource : 'SUSPEND',
      reminderDate: null,
      reminderTo: null,
      startDate: new Date(),
      endDate : new Date()
    });

    this.dischargeGroupPerson = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,
      reminderDate: null,
      officialDischargeDate: new Date(),
      deleteApprovedRoster:true,
      deleteUnApprovedRoster:true,
      afterDate:new Date(),
      deleteMasterRoster:true,
      deleteDate: new Date()

    });

    this.dischargeGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,

      referralCode: null,
      dischargeType: null,

      dischargeDate: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'DISCHARGE',
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null,
      allPrograms:false
    });
   this.dischargeGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
     this.dischargeGroup.get('programChecked').valueChanges
    .subscribe(x => {
      this.selectedProgramChange(x); // Call your function
    });


    this.waitListGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,

      activityCode: null,
    
      suppliedRef: null,

      date: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'WAIT LIST',
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null
    });
    this.waitListGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
     this.waitListGroup.get('programChecked').valueChanges
    .subscribe(x => {
      this.selectedProgramChange(x); // Call your function
    });

    this.admitGroup = this.fb.group({
      programs: this.fb.array([]),
      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      programChecked: null,
      referralSource:null,
      referralCode :null,
      caseCategory: 'ADMISSION',
      publishToApp: false,
      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null,
      referralDate: new Date(),
      ServiceType:null,
      admissionDate: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),
      rosterCreationDates: [],
      serviceType : null,
      adminsssion: null,
      admissionType: null,
      timePeriod: [],

    });
    this.admitGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
 
    // this.admitGroup.get('programChecked').valueChanges
    //   .pipe(
    //     switchMap(x => {
    //       console.log(x)
    //       if (!x) return EMPTY;
    //       this.selectedProgramChange(x); 
    //       this.globalProgramSelection = {
    //         program: x,
    //         option: 'ADMIT'
    //       }
    //       return  this.listS.getreferraltypes({
    //         ProgramName: this.globalProgramSelection.program,
    //         ProgramType: RECIPIENT_OPTION.ADMIN
    //       })
    //     }),
    //     switchMap(data => {
    //       this.admissionActiviType = data;

    //       if (data.length==0)
    //         this.admissionActiviType.add('*ADMISSION');
         
    //       if (data.length == 1) {
    //         this.admitGroup.patchValue({
    //           admissionType: this.admissionActiviType[0],
    //           caseCategory : 'ADMISSION'
    //         });
    //       }
        
    //       return this.listS.gethumanresourcetypes({ program: this.globalProgramSelection.program })
    //     })
    //   ).subscribe(data => {
    //     this.FUNDING_TYPE = data;
    //   });

   

      this.admitGroup.get('multipleStaff').valueChanges
      .subscribe(x => {
        this.viewMultipleStaff(x); // Call your function
      });

    this.assessGroup = this.fb.group({
      programs: this.fb.array([]),    
      programChecked: null,
      serviceType: '*ASSESSMENT',
      date: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),
      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'SCREEN/ASSESS',
      publishToApp: false,
      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null,
      allPrograms: false,
      referralType: '*ASSESSMENT',
      referralCode: '*ASSESSMENT',
      referralSource: '*ASSESSMENT'
    });
    
    this.assessGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
  
    this.assessGroup.get('allPrograms').valueChanges.pipe(
      switchMap(x => {
        this.isPackageNameAvailable = null;
        this.DisplayallProgram(x);
        return of(x);
      }),
      debounceTime(200),
      switchMap(x => {
        return this.listS.checkIfPackageNameExists(x)
      })
    );

    this.notProceedGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,
      reasonCode: null,
      referralCode: null,
      referralType: null,
      referralSource: null,
      date: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'NOT PROCEEDING',
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null
    });
    this.notProceedGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
  
    this.referOnGroup = this.fb.group({
      programs: this.fb.array([]),
      programChecked: null,
      referralSource: null,
      referralCode: null,
      serviceType :null,
      referralType: 'REFERAL ON',
      date: new Date(defaultDate),
      time: new Date(defaultDateTime),
      timeSpent: new Date(defaultTimeSpent),

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'REFERRAL-OUT',
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null,
      allPrograms: null,

    });
    this.referOnGroup.get('allPrograms').valueChanges.pipe(
      switchMap(x => {
        this.isPackageNameAvailable = null;
        this.DisplayallProgram(x);
        return of(x);
      }),
      debounceTime(200),
      switchMap(x => {
        return this.listS.checkIfPackageNameExists(x)
      })
    );
    this.referOnGroup.get('multipleStaff').valueChanges
    .subscribe(x => {
      this.viewMultipleStaff(x); // Call your function
    });
    this.referOnGroup.get('programChecked').valueChanges
    .pipe(
      switchMap(x => {
        if (!x) return EMPTY;
        this.globalProgramSelection = {
          program: x,
          option: 'REFER_ON'
        }
        return  this.listS.getreferraltypes({
          ProgramName: this.globalProgramSelection.program,
          ProgramType: RECIPIENT_OPTION.REFER_ON
        })
      }),     
    
    ).subscribe(data => {
      this.referralType$ = data;
        if (data.length == 1) {
          this.referOnGroup.patchValue({
            serviceType: this.referralType$[0],
            caseCategory : 'REFERRAL-OUT'
          });
        }
      
    });

    this.referInGroup = this.fb.group({
      type: null,
      programChecked: null,
      packageName: null,
      programs: this.fb.array([]),

      referralSource: null,
      referralCode: null,
      referralType: '*REFERRAL',

      date: new Date(),
      time: new Date(),
      timeSpent: new Date(defaultTimeSpent),

      radioGroup:this.usersettings.wizardDefaultNote,
      notes: null,
      caseCategory: 'REFERRAL-IN',
      publishToApp: false,

      reminderDate: null,
      reminderTo: null,
      emailNotif: null,
      multipleStaff: null,
      pensionStatus: null,
      coPayment: 5,
      dailyLiving: 17.5


    });
  

  this.referInGroup.get('multipleStaff').valueChanges
  .subscribe(x => {
    this.viewMultipleStaff(x); // Call your function
  });
  let today= new Date();
  today.setDate(1);
  this.deactivateGroup = this.fb.group({
    type: null,
    programChecked: null,
    packageName: null,
    programs: this.fb.array([]),
    startDate: today,
    endDate: new Date(),
    referralSource: null,
    referralCode: null,
    referralType: '*REFERRAL',

    date: new Date(),
    time: new Date(),
    timeSpent: new Date(defaultTimeSpent),

    radioGroup:this.usersettings.wizardDefaultNote,
    notes: null,
    caseCategory: 'REFERRAL-IN',
    publishToApp: false,

    reminderDate: null,
    reminderTo: null,
    emailNotif: null,
    multipleStaff: null,
    pensionStatus: null,
    coPayment: 5,
    dailyLiving: 17.5


  });

  }

  mutateToCheckboxes(list: Array<any>): Array<any> {
    return list.map(x => {
      return {
        name: x
      }
    })
  }

  selectedProgram: any;
  fundingSource: any;

  selectedProgramChange(name: any) {
    console.log(name);
    this.selectedProgram = name;
    const { type } = this.referInGroup.getRawValue();

    if (type == 2) {
      const level = name.trim().split(' ')[1];
      this.referInGroup.patchValue({
        packageName: `HCP-L${level}-${this.user.code} ${format(new Date(this.recipient.dateOfBirth), 'ddMMyyyy')}`
      });
    }

    if (type >= 4) {
      this.listS.gethumanresourcetypes({ program: this.selectedProgram })
        .subscribe(data => this.FUNDING_TYPE = data);

        this.FUNDING_TYPE = this.selectedProgram
    }


    this.populateNotificationDetails();
    this.populateOtherDetails();

  }



  referralInChange(index: any) {
    this.selectedProgram = null;
    this.programs = [];
    this.referralCheckOptions[index-1].checked = true;
    if (index == 1) {
      this.isPackageNameAvailable = null;
      this.listS.getndiaprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data);
        this.changeDetection();
      });

      this.originalPackageName = `NDIA-${this.recipient.accountNo} ${format(new Date(this.recipient.dateOfBirth), 'ddMMyyyy')}`;

      this.referInGroup.patchValue({
        packageName: this.originalPackageName
      });

      this.FUNDING_TYPE = 'NDIA';
    }

    if (index == 2) {
      this.isPackageNameAvailable = null;
      this.listS.gethcpprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = data;
        this.changeDetection();
        this.loadPrograms=false;
      })

      this.originalPackageName = `HCP-L1-${this.recipient.accountNo} ${format(new Date(this.recipient.dateOfBirth), 'ddMMyyyy')}`;;

      this.referInGroup.patchValue({
        packageName: this.originalPackageName
      });

      this.FUNDING_TYPE =  'HOME CARE PACKAGE' ;
    }

    if (index == 3) {
      this.listS.getsahprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = data;
        this.loadPrograms=false;
        this.changeDetection();
      });
      this.originalPackageName = `SAH-L1-${this.recipient.accountNo} ${format(new Date(this.recipient.dateOfBirth), 'ddMMyyyy')}`;

      this.referInGroup.patchValue({
        packageName: this.originalPackageName
      });
      this.FUNDING_TYPE = 'SAH';
    }

    if (index == 4) {
      this.listS.getdexprograms(this.user.id).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data);
        this.loadPrograms=false;
        this.changeDetection();
      });
      this.FUNDING_TYPE = this.selectedProgram.value.program; //'DSS';
    }

    if (index == 5) {
      this.listS.getotherprograms(this.user.id).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data);
        this.loadPrograms=false;
        this.changeDetection();
      });

      this.FUNDING_TYPE = this.selectedProgram.value.program;;
    }

  }

  isPackageNameAvailable: boolean = null;

  VALUE_CHANGES() {

    if (this.option == RECIPIENT_OPTION.DISCHARGE) {
      this.noteArray = ['DISCHARGE'];
      this.dischargeGroup.get('caseCategory').disable();
      return;
    }

    if (this.option == RECIPIENT_OPTION.ASSESS) {
      this.noteArray = ['SCREEN/ASSESS'];
      this.assessGroup.get('caseCategory').disable();
      return;
    }

    if (this.option == RECIPIENT_OPTION.REFER_ON) {
      this.noteArray = ['REFERRAL-OUT'];
      this.referOnGroup.get('caseCategory').disable();
      return;
    }

    if (this.option == RECIPIENT_OPTION.ITEM) {
      this.noteArray = ['ITEM'];
      this.itemGroup.get('caseCategory').disable();
      return;
    }
    if (this.option == RECIPIENT_OPTION.ADMIT) {
    
      this.noteArray = ['ADMISSION'];
      this.adminGroup.patchValue({ caseCategory: 'ADMISSION' })
      this.adminGroup.get('caseCategory').disable();
      this.cd.detectChanges();
      return;
    }
    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {
      this.noteArray = ['NOT PROCEEDING'];
      this.notProceedGroup.patchValue({ caseCategory: 'NOT PROCEEDING' })
      this.notProceedGroup.get('caseCategory').disable();
      this.cd.detectChanges();
      return;
    }
    if (this.option == RECIPIENT_OPTION.REFER_IN) {
      this.noteArray = ['REFERRAL-IN'];
      this.referInGroup.get('caseCategory').disable();

      this.referInGroup.get('packageName')
        .valueChanges
        .pipe(
          switchMap(x => {
            this.isPackageNameAvailable = null;
            return of(x);
          }),
          debounceTime(200),
          switchMap(x => {
            return this.listS.checkIfPackageNameExists(x)
          })
        )
        .subscribe(data => {
          this.isPackageNameAvailable = data;
          this.changeDetection();
        });
      return;
    }

    combineLatest([
      this.adminGroup.get('radioGroup').valueChanges.pipe(startWith('case')),
      this.adminGroup.get('notes').valueChanges.pipe(startWith(''))
    ]).pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(x => {
        // this.adminGroup.patchValue({ caseCategory: null });
        if (!x || this.globalS.isEmpty(x[0]) || this.globalS.isEmpty(x[1])) {
          this.noteArray = ['OTHER'];
          this.adminGroup.patchValue({ caseCategory: 'OTHER' });
          return EMPTY;
        }

        let index = x[0] === 'case' ? 0 : x[0] === 'op' ? 1 : 2;
        return this.listS.getcasenotecategory(index);
      })
    ).subscribe(data => {
      this.noteArray = data;
    });
  }

  populateList() {
    this.listS.getremindersrecipient()
      .subscribe(d => {
        this.remindersRecipientArray = d;
      });
  }

  IsNDIAorHCP(): boolean {
    return this.referInGroup.get('type').value == 1 || this.referInGroup.get('type').value == 2;
  }

  PATCH_NOTES(data: string) {
    if (this.option == RECIPIENT_OPTION.REFER_IN) {
      this.referInGroup.get('notes').patchValue(data);

    }

    if (this.option == RECIPIENT_OPTION.REFER_ON) {
      this.referOnGroup.get('notes').patchValue(data);
    }

    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {
      // this.notProceedGroup.get('notes').patchValue(data);
      this.noteArray = ['NOT PROCEEDING'];
      this.notProceedGroup.patchValue({ caseCategory: 'NOT PROCEEDING', referralType: 'NOT PROCEEDING', referralCode: 'NOT PROCEEDING', referralSource: 'NOT PROCEEDING' })
      this.cd.detectChanges();
    }

    if (this.option == RECIPIENT_OPTION.ASSESS) {
     // this.assessGroup.get('notes').patchValue(data);
      
      this.noteArray = ['SCREEN/ASSESS'];
      this.notProceedGroup.patchValue({ caseCategory: 'SCREEN/ASSESS', referralType: 'SCREEN/ASSESS', referralCode: 'SCREEN/ASSESS', referralSource: 'SCREEN/ASSESS' })
    }

    if (this.option == RECIPIENT_OPTION.ADMIT) {
      this.admitGroup.get('notes').patchValue(data);
      this.noteArray = ['ADMISSION'];
      this.notProceedGroup.patchValue({ caseCategory: 'ADMISSION', referralType: 'ADMISSION', referralCode: 'ADMISSION', referralSource: 'ADMISSION' })
   
    }

    if (this.option == RECIPIENT_OPTION.WAIT_LIST) {
     // this.waitListGroup.get('notes').patchValue(data);
      this.noteArray = ['WAIT LIST'];
      this.waitListGroup.patchValue({ caseCategory: 'WAIT LIST', referralType: 'WAIT LIST', referralCode: 'WAIT LIST', referralSource: 'WAIT LIST' })
      this.cd.detectChanges();
    }

    if (this.option == RECIPIENT_OPTION.DISCHARGE) {
      this.dischargeGroup.get('notes').patchValue(data);
    }

    if (this.option == RECIPIENT_OPTION.SUSPEND) {
      this.suspendGroup.get('notes').patchValue(data);
    }

    if (this.option == RECIPIENT_OPTION.DECEASE) {
      this.deceaseGroup.get('notes').patchValue(data);
    }

    if (this.option == RECIPIENT_OPTION.ADMIN) {
      this.adminGroup.get('notes').patchValue(data);
    }

    if (this.option == RECIPIENT_OPTION.ITEM) {
      //this.itemGroup.get('notes').patchValue(data);
    }
  }

  GET_FUNDING_TITLE(): string {
    if (this.option == RECIPIENT_OPTION.REFER_IN) {
      return `${this.FUNDING_TYPE} REFERRAL`;
    }

    if (this.option == RECIPIENT_OPTION.REFER_ON) {
      return 'REFERRAL ON';
    }

    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {
      return 'NOT PROCEED';
    }

    if (this.option == RECIPIENT_OPTION.ASSESS) {
      return 'ASSESSMENT'
    }

    if (this.option == RECIPIENT_OPTION.ADMIT) {
      return 'ADMISSION'
    }

    if (this.option == RECIPIENT_OPTION.WAIT_LIST) {
      return 'WAIT LIST'
    }

    if (this.option == RECIPIENT_OPTION.DISCHARGE) {
      return 'DISCHARGE'
    }

    if (this.option == RECIPIENT_OPTION.SUSPEND) {
      return 'SUSPENSION'
    }

    if (this.option == RECIPIENT_OPTION.DECEASE) {
      return 'DECEASE'
    }

    if (this.option == RECIPIENT_OPTION.ADMIN) {
      return 'ADMIN REGISTRATION'
    }

    if (this.option == RECIPIENT_OPTION.ITEM) {
      return 'ITEM'
    }
    if (this.option == RECIPIENT_OPTION.DEACTIVATE) {
      return 'DEACTIVATE'
    }
  }
  saving: boolean;
  reInstate:boolean;
  Reinstate(){
    //code to reinstate
    this.handleCancel();
  }
  deleteRecipient:boolean=false;
  deleteRecipientMasterRosters:boolean=false;
  deleteRecipientRostersDateRange:boolean=false;
  FinalConfirmation:boolean=false;

  deactivate(index:number){
    if (index==0){
      this.deleteRecipient=true;
    }else if (index==2){
      this.deleteRecipientRostersDateRange=true;
    }else if (index==3){
      this.deleteRecipientMasterRosters=true;
    }else if (index==4){
      this.FinalConfirmation=true;
      let input={
        id : this.user.id,
        code : this.user.code,
        deleteRecipient:true,
        deleteRecipientMasterRosters:this.deleteRecipientMasterRosters,
        deleteRecipientRostersDateRange:this.deleteRecipientRostersDateRange,
        FinalConfirmation:true,
        startDate : format(this.deactivateGroup.value.startDate,'yyyy/MM/dd'),
        endDate : format(this.deactivateGroup.value.endDate,'yyyy/MM/dd'),
        user: this.token.user
        
      }
      this.listS.processDeactivateRecipient(input).pipe(takeUntil(this.unsubscribe$)).subscribe(d=>{
        this.globalS.sToast('Recipient', 'Recipient Deactivated Successfully')
        this.handleCancel();
      });
    }
    this.current+=1;

  }
  pre_done(){
    this.dischargeOpen=false;
    this.dischargeOpenPerson=true;
    
  }
  deaseseDone(){

    
    const
    {
      dateOfDeath,
      referralCode,
      referralType,
      packageName,
      radioGroup,
      notes,
      date,
      time,
      timeSpent,
      caseCategory,
      publishToApp,
      reminderTo,
      reminderDate,
      emailNotif


    } = this.deceaseGroup.getRawValue();
 
      let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
            
      sql.TableName='Recipients '; 
      sql.SetClause=`SET DateOfDeath = '${ format(dateOfDeath,'yyyy/MM/dd')}'`;
      sql.WhereClause=` WHERE UniqueID = '${this.user.id}' `;

       this.listS.updatelist(sql).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
      this.deceaseOpen=false;
      this.option = RECIPIENT_OPTION.DISCHARGE;
      this.populate();
      this.populateList();
      this.VALUE_CHANGES();
      this.openModal();
    })
  }
  done() {

    if (this.globalFormGroup.get('reminderTo')?.value && !this.globalFormGroup.get('reminderDate')?.value) {
      this.globalS.eToast('Reminder Date Required', 'Please enter a Reminder Date when selecting Reminder To.');
      return;
    }

    if (this.globalFormGroup.get('reminderTo')?.value && this.globalFormGroup.get('reminderDate')?.value){
      let today = new Date();
      if (this.globalFormGroup.get('reminderDate')?.value<today) {
        this.globalS.eToast('Invalid Reminder Date', 'Please enter a valid Reminder Date greater than today');
        return;
    }
  }

    var finalRoster: Array<ProcedureRoster> = [];

    this.checkedPrograms = this.GET_CHECKEDPROGRAMS();
    this.saving = true;
    var checkedPrograms = this.checkedPrograms ?? [];

    const defaultValues = {
      // '' is the default
      billDesc: '',

      // 7 for everything except 14 for item
      type: 7,

      // 2 is the default,
      rStatus: '2',

      // HOUR is the default     
      billUnit: 'HOUR',

      // 0 is the default  
      billType: '0',

      // '' is the default
      payType: '',

      // '' is the default
      payRate: '',

      // '' is the default
      payUnit: '',

      // '' is the default
      apInvoiceDate: '',

      // '' is the default
      apInvoiceNumber: '',

      // 0 is default
      groupActivity: '0',

      // '' is the default
      serviceSetting: ''
    }

    if (this.option == RECIPIENT_OPTION.REFER_IN) {

      const
        {
          referralSource,
          referralCode,
          referralType,
          packageName,
          radioGroup,
          notes,
          date,
          time,
          timeSpent,
          caseCategory,
          publishToApp,
          reminderTo,
          reminderDate,
          emailNotif


        } = this.referInGroup.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      if (this.SelectedReferralTypes.length<=0)
        this.SelectedReferralTypes.push(referralType);
       
    
        let program: ProcedureRoster = {
          clientCode: this.user.code,
          carerCode: this.token.code,

          serviceType: referralType,
          date: format(date, 'yyyy/MM/dd'),
          time: format(time, 'HH:mm'),

          creator: this.token.code,
          editer: this.token.user,

          billUnit: defaultValues.billUnit,
          billDesc: defaultValues.billDesc,
          agencyDefinedGroup: this.user.agencyDefinedGroup,
          referralCode: referralCode,
          timePercent: timePercentage,
          notes: notes || "",
          type: defaultValues.type,
          duration: timeInMinutes / 5,
          blockNo: blockNoTime,
          reasonType: '',

          tabType: 'REFERRAL-IN',
          program: this.IsNDIAorHCP() ? packageName : this.selectedProgram,
          packageStatus: 'REFERRAL'
        }

        finalRoster.push(program);
     

      const data: CallProcedure = {
        isNDIAHCP: this.IsNDIAorHCP(),
        oldPackage: this.selectedProgram,
        newPackage: packageName,
        roster: finalRoster,
        staffNote: {
          personId: this.user.id,
          program: 'VARIOUS',
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'REFERRAL-IN',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: reminderTo || ''
        }
      }



      this.listS.postreferralin(data).subscribe(x => {
        this.globalS.sToast('Success', 'Success');
       
        if (reminderTo!=null) {
          //this.writereminder(this.user.id, notes, this.notifFollowUpGroup);
        }
        //console.log(emailNotif.includes('@'))
        if (emailNotif==null ){
          this.handleCancel();
          return;
        }
          

        this.globalS.emailaddress =emailNotif;
            
        if ( this.globalS.emailaddress.length>0) 
          this.emailnotify();
        else        
          this.handleCancel();

      }
      );
    }

    if (this.option == RECIPIENT_OPTION.REFER_ON) {

      const
        {
          referralSource,
          referralCode,
          referralType,
          programChecked,
          radioGroup,
          notes,
          date,
          time,
          timeSpent,
          caseCategory,
          publishToApp,
          reminderTo,
          reminderDate,
          emailNotif
        } = this.referOnGroup.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.saving = true;
      this.checkedPrograms.forEach(x => {
        //this.SelectedReferralTypes.forEach(x => {
        let program: ProcedureRoster = {
          clientCode: this.user.code,
          carerCode: this.token.code,

          serviceType: referralType,
          date: format(date, 'yyyy/MM/dd'),
          time: format(time, 'HH:mm'),

          creator: this.token.code,
          editer: this.token.user,

          billUnit: defaultValues.billUnit,
          billDesc: defaultValues.billDesc,
          agencyDefinedGroup: this.user.agencyDefinedGroup,
          referralCode: referralCode,
          
          timePercent: timePercentage,
          notes: notes || "",
          type: defaultValues.type,
          duration: timeInMinutes / 5,
          blockNo: blockNoTime,
          reasonType: '',

          tabType: 'REFERRAL-OUT',
          program: x.program,
          packageStatus: 'REFERRAL'
        }

        finalRoster.push(program);
      });

      const data: CallReferralOutProcedure = {


        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: 'VARIOUS',
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'REFERRAL-OUT',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: reminderTo
        }
      }
      // let program: ProcedureRoster = {
      //     clientCode: this.user.code,
      //     carerCode: this.token.code,                   

      //     serviceType: programChecked,
      //     date: format(date,'yyyy/MM/dd'),
      //     time: format(time,'HH:mm'),

      //     creator: this.token.code,
      //     editer: this.token.user,

      //     billUnit: defaultValues.billUnit,
      //     billDesc: defaultValues.billDesc,
      //     agencyDefinedGroup: this.user.agencyDefinedGroup,
      //     referralCode: referralCode,
      //     timePercent: timePercentage,
      //     notes: notes || "",
      //     type: defaultValues.type,
      //     duration: timeInMinutes / 5,
      //     blockNo: blockNoTime,
      //     reasonType: referralSource,

      //     tabType: 'REFERRAL-OUT',
      //     program: programChecked,
      //     packageStatus: 'REFERRAL'
      //   }

      //   finalRoster.push(program);

      //   const data: CallReferralOutProcedure = {                  
      //     roster: finalRoster,
      //     note: {
      //       personId: this.user.id,
      //       program: programChecked,
      //       detailDate: format(new Date,'yyyy/MM/dd'),
      //       extraDetail1: radioGroup,
      //       extraDetail2: 'REFERRAL-OUT',
      //       whoCode: this.user.code,
      //       publishToApp: publishToApp ? 1 : 0,
      //       creator: this.token.user,
      //       note: notes || "" ,
      //       alarmDate: format(new Date,'yyyy/MM/dd'),
      //       reminderTo: ''
      //     }
      //   }

      // console.log(data);
      // return;

      this.listS.postreferralout(data).subscribe(data => {
        this.saving = false;
        this.globalS.sToast('Success', 'Success');
        if (emailNotif==null ){
          this.handleCancel();
          return;
        }
          
        this.globalS.emailaddress =emailNotif;
            
        if ( this.globalS.emailaddress.length>0) 
          this.emailnotify();
        else        
          this.handleCancel();

      });
    }

    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {

      const
        {
          referralSource,
          referralCode,
          referralType,
          packageName,
          radioGroup,
          notes,
          date,
          time,
          timeSpent,
          programChecked,
          caseCategory,
          publishToApp,
          reminderDate,
          reminderTo
        } = this.notProceedGroup.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.checkedPrograms.forEach(x => {

        let program: ProcedureRoster = {
          clientCode: this.user.code,
          carerCode: this.token.code,

          serviceType: referralType,
          date: format(date, 'yyyy/MM/dd'),
          time: format(time, 'HH:mm'),

          creator: this.token.code,
          editer: this.token.user,

          billUnit: defaultValues.billUnit,
          billDesc: defaultValues.billDesc,
          agencyDefinedGroup: this.user.agencyDefinedGroup,
          referralCode: referralCode,
          timePercent: timePercentage,
          notes: notes || "",
          type: defaultValues.type,
          duration: timeInMinutes / 5,
          blockNo: blockNoTime,
          reasonType: '',

          tabType: 'NOT-PROCEED',
          program: x.program,
          packageStatus: 'REFERRAL'
        }

        finalRoster.push(program);
      });
      const data: CallReferralOutProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: this.selectedProgram.value.program,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'REFERRAL-OUT',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: ''
        }
      }

      this.listS.postnotproceed(data).subscribe(data => {
        this.globalS.sToast('Success', 'Success');
        this.handleCancel();
        this.cd.detectChanges();
      });
    }

    if (this.option == RECIPIENT_OPTION.ASSESS) {

      const
        {
          referralSource,
          referralCode,
          referralType,
          serviceType,
          packageName,
          radioGroup,
          notes,
          date,
          time,
          timeSpent,
          caseCategory,
          programChecked,
          publishToApp,
          reminderDate,
        reminderTo
        } = this.assessGroup.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.checkedPrograms.forEach(x => {

      let program: ProcedureRoster = {
        clientCode: this.user.code,
        carerCode: this.token.code,

        serviceType: serviceType,
        date: format(date, 'yyyy/MM/dd'),
        time: format(time, 'HH:mm'),

        creator: this.token.code,
        editer: this.token.user,

        billUnit: defaultValues.billUnit,
        billDesc: defaultValues.billDesc,
        agencyDefinedGroup: this.user.agencyDefinedGroup,
        referralCode: referralCode,
        timePercent: timePercentage,
        notes: notes || "",
        type: defaultValues.type,
        duration: timeInMinutes / 5,
        blockNo: blockNoTime,
        reasonType: '',

        tabType: 'ASSESSMENT',
        program: x.program,
        packageStatus: ''
      }

      finalRoster.push(program);
  
      const data: CallReferralOutProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: programChecked,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'SCREEN/ASSESS',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: reminderTo
        }
      }


      this.listS.postassessment(data).subscribe(data => {
        this.globalS.sToast('Success', 'Success');
        this.handleCancel();
      });

     }); // program loop
    }

    if (this.option == RECIPIENT_OPTION.ADMIT) {

      const {
        date,
        time,
        timeSpent,
        admissionDate,
        notes,
        referralCode,
        referraltype,
        referralSource,
        programChecked,
        admissionType,
        serviceType,       
        reminderDate,
        reminderTo
      
      } = this.admitGroup.value;

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.checkedPrograms.forEach(x => {

        let program: ProcedureRoster = {
          docId : this.DOCUMENTID,
          clientCode: this.user.code,
          carerCode: this.token.code,
  
          serviceType: serviceType,
          date: format(admissionDate, 'yyyy/MM/dd'),
          time: format(time, 'HH:mm'),
         
          creator: this.token.code,
          editer: this.token.user,
  
          billUnit: defaultValues.billUnit,
          billDesc: defaultValues.billDesc,
          agencyDefinedGroup: this.user.agencyDefinedGroup,
         
          referralCode : admissionType,
       
          timePercent: timePercentage,
          notes: notes || "",
          type: defaultValues.type,
          duration: timeInMinutes / 5,
          blockNo: blockNoTime,
          reasonType: '',
  
          tabType: 'ADMISSION',
          program: x.program,
          packageStatus: '',         
          services:  x.services.split(',').map(e=>  {return {name : e}}),
          AdmissionType :admissionType
          
        }
      
        finalRoster.push(program);
      });
        const data: CallReferralOutProcedure = {
          roster: finalRoster,
          note: {
            personId: this.user.id,
            program: 'ADMISSION',
            detailDate: admissionDate,
            extraDetail1: 'ADMISSION',
            extraDetail2: 'ADMISSION',
            whoCode: this.user.code,
            publishToApp: 0,
            creator: this.token.user,
            note: notes,
            alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
            reminderTo: reminderTo
          }
        }
     
      // this.listS.postadmissionacceptquote(data).subscribe(data => {
      //   this.globalS.sToast('Success', 'Success');
      //   this.handleCancel();
      // });
      this.listS.postadmission(data).subscribe(data => {
        this.globalS.sToast('Success', 'Success');
        this.handleCancel();
      });
      

    }


    if (this.option == RECIPIENT_OPTION.WAIT_LIST) {

      // console.log(this.waitListGroup.value)

      const
        {
          referralSource,
          referralCode,
          referralType,
          packageName,
          activityCode,
          radioGroup,
          notes,
          date,
          time,
          timeSpent,
          programChecked,
          caseCategory,
          publishToApp,
          reminderDate,
          reminderTo
        } = this.waitListGroup.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.checkedPrograms.forEach(x => {
      let program: ProcedureRoster = {
        clientCode: this.user.code,
        carerCode: this.token.code,

        serviceType: activityCode,
        date: format(date, 'yyyy/MM/dd'),
        time: format(time, 'HH:mm'),

        creator: this.token.code,
        editer: this.token.user,

        billUnit: defaultValues.billUnit,
        billDesc: defaultValues.billDesc,
        agencyDefinedGroup: this.user.agencyDefinedGroup,
        referralCode: activityCode,
        timePercent: timePercentage,
        notes: notes || "",
        type: defaultValues.type,
        duration: timeInMinutes / 5,
        blockNo: blockNoTime,
        reasonType: '',

        tabType: 'WAITLIST',
        program: x.program,
        packageStatus: ''
      }

      finalRoster.push(program);
    });

      const data: CallReferralOutProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: programChecked,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'WAITLIST',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: reminderTo
        }
      }

      this.listS.postwaitlist(data).subscribe(data => {
        this.globalS.sToast('Success', 'Success');
        this.handleCancel()
        this.cd.detectChanges();
      });
    }
    if (this.option == RECIPIENT_OPTION.REINSTATE) {

      // console.log(this.waitListGroup.value)

      const
        {
          referralSource,
          referralCode,
          referralType,
          packageName,
          activityCode,
          radioGroup,
          notes,
          date,
          time,
          timeSpent,
          programChecked,
          caseCategory,
          publishToApp,
          reminderDate,
        reminderTo,
        } = this.reinstateGroup.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.checkedPrograms.forEach(x => {
      let program: ProcedureRoster = {
        clientCode: this.user.code,
        carerCode: this.token.code,

        serviceType: x.program,
        date: format(new Date, 'yyyy/MM/dd'),
        time: format(new Date, 'HH:mm'),

        creator: this.token.code,
        editer: this.token.user,

        billUnit: defaultValues.billUnit,
        billDesc: defaultValues.billDesc,
        agencyDefinedGroup: this.user.agencyDefinedGroup,
        referralCode: x.program,
        timePercent: timePercentage,
        notes: notes || "",
        type: defaultValues.type,
        duration:0,
        blockNo: 0,
        reasonType: '',

        tabType: 'REINSTATE',
        program: x.program,
        packageStatus: ''
      }

      finalRoster.push(program);
    });

      const data: CallReferralOutProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: programChecked,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'WAITLIST',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: format(new Date (reminderDate), 'yyyy/MM/dd'),
          reminderTo: reminderTo
        }
      }

      this.listS.postreinstate(data).subscribe(data => {
        this.globalS.sToast('Success', 'Success');
        this.handleCancel()
        this.cd.detectChanges();
      });
    }

    if (this.option == RECIPIENT_OPTION.DISCHARGE) {

      console.log(this.dischargeGroup.value)

      const
        {
          
          referralCode,
          dischargeType,
          packageName,
          programChecked,
          radioGroup,
          notes,
          date,
          dischargeDate,
          time,
          timeSpent,
          caseCategory,
          publishToApp,
          reminderDate,
          reminderTo
        } = this.dischargeGroup.getRawValue();

        const
        {
          
          officialDischargeDate,
          deleteApprovedRoster,
          deleteUnApprovedRoster,          
          deleteMasterRoster,
          deleteDate
        } = this.dischargeGroupPerson.getRawValue();

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
      this.checkedPrograms.forEach(x => {
      let program: ProcedureRoster = {
        clientCode: this.user.code,
        carerCode: this.token.code,

        serviceType: dischargeType,        
        date: format(this.dischargeGroup.value.dischargeDate, 'yyyy/MM/dd'),
        time: format(time, 'HH:mm'),

        creator: this.token.code,
        editer: this.token.user,

        billUnit: defaultValues.billUnit,
        billDesc: defaultValues.billDesc,
        agencyDefinedGroup: this.user.agencyDefinedGroup,
        referralCode: referralCode.description,
        timePercent: timePercentage,
        notes: notes || "",
        type: defaultValues.type,
        duration: timeInMinutes / 5,
        blockNo: blockNoTime,
        reasonType: '',
        officialDischargeDate : officialDischargeDate ? format(new Date(officialDischargeDate),'yyyy/MM/dd') : null ,
        deleteApproved : deleteApprovedRoster,
        deleteUnApproved : deleteUnApprovedRoster,          
        deleteMaster : deleteMasterRoster,
        deleteDate : deleteDate? format(new Date(deleteDate),'yyyy/MM/dd') : null,
        tabType: 'DISCHARGE',
        program: x.program,
        packageStatus: ''
      }

      finalRoster.push(program);

      const data: CallReferralOutProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: programChecked,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: radioGroup,
          extraDetail2: 'WAITLIST',
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes || "",
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: reminderTo
        }
      }

      this.listS.postdischarge(data).subscribe(data => {
        this.globalS.sToast('Success', 'Success');
        this.handleCancel()
        this.cd.detectChanges();
      });
    });
    }

    if (this.option == RECIPIENT_OPTION.SUSPEND) {
      //console.log(this.suspendGroup.value)
      const
      {
        cancellationCode,
        reason,
        referralSource,
        referralCode,
        referralType,
        packageName,
        radioGroup,
        notes,
        date,
        time,
        timeSpent,
        programChecked,
        caseCategory,
        publishToApp,
        startDate,
        endDate,
        reminderTo,
        reminderDate

      } = this.suspendGroup.getRawValue();

    const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
    const timeInMinutes = this.globalS.getMinutes(timeSpent)
    const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString();
    this.checkedPrograms.forEach(x => {

      let programvalue = String(x.program).split('~>');
      let program: ProcedureRoster = {
        clientCode: this.user.code,
        carerCode: this.token.code,       
        serviceType: programvalue[1],
        date: format(date, 'yyyy/MM/dd'),
        time: format(time, 'HH:mm'),

        creator: this.token.code,
        editer: this.token.user,

        billUnit: defaultValues.billUnit,
        billDesc: defaultValues.billDesc,
        agencyDefinedGroup: this.user.agencyDefinedGroup,
        referralCode: referralCode,
        timePercent: timePercentage,
        notes: notes || "",
        type: defaultValues.type,
        duration: timeInMinutes / 5,
        blockNo: blockNoTime,
        reasonType: '0',

        tabType: 'SUSEPND',
        program: programvalue[0],
        packageStatus: ''
       
      }

      finalRoster.push(program);
    });
    const data: CallReferralOutProcedure = {
      roster: finalRoster,
      note: {
        personId: this.user.id,
        program: this.selectedProgram.value.program,
        detailDate: format(new Date, 'yyyy/MM/dd'),
        extraDetail1: radioGroup,
        extraDetail2: 'REFERRAL-OUT',
        whoCode: this.user.code,
        publishToApp: publishToApp ? 1 : 0,
        creator: this.token.user,
        note: notes || "",
        alarmDate: format(new Date(reminderDate), 'yyyy/MM/dd'),
        reminderTo: reminderTo,
        cancellationCode: cancellationCode,
        reasonType: reason,
        startDate : format(new Date(startDate), 'yyyy/MM/dd'), 
        endDate : format(new Date(endDate), 'yyyy/MM/dd'), 
      }
    }

    this.listS.postsuspend(data).subscribe(data => {
      this.globalS.sToast('Success', 'Success');
      this.handleCancel();
      this.cd.detectChanges();
    });

  }
    

    if (this.option == RECIPIENT_OPTION.REINSTATE) {
      console.log(this.reinstateGroup.value)


      const {
        referralType,
        quantity,
        unit,
        radioGroup,
        notes,
        charge,
        suppliedDate,
        date,
        programChecked,
        referralDate,
        refNo,
        time,
        timeSpent,
        reminderDate,
        caseCategory,
        publishToApp,
        reminderTo
      } = this.reinstateGroup.getRawValue()

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString()


      for (var checkProgram of checkedPrograms) {

        let data: ProcedureRoster = {
          clientCode: this.user.code,
          carerCode: this.token.code,
          serviceType: checkProgram.selected,
          date: referralDate,
          time: time,

          creator: this.token.user,
          editer: this.token.user,

          billUnit: 'HOUR',
          agencyDefinedGroup: this.user.agencyDefinedGroup,
          referralCode: '',
          timePercent: timePercentage,
          notes: notes,
          type: 7,
          duration: timeInMinutes / 5,
          blockNo: blockNoTime,
          reasonType: '',
          program: programChecked,
          tabType: caseCategory
        }
        finalRoster.push(data);
      }

      let data: CallAssessmentProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: programChecked,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: NOTE_TYPE[radioGroup],
          extraDetail2: caseCategory,
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes,
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : '',
          reminderTo: reminderTo
        }
      }

      console.log(data);

      this.listS.postreinstate(data)
        .subscribe(data => {
          this.globalS.sToast('Success', 'Success');
          this.handleCancel()
          this.cd.detectChanges();
        });

    }

    if (this.option == RECIPIENT_OPTION.DECEASE) {
      console.log(this.deceaseGroup.value)


    }

    if (this.option == RECIPIENT_OPTION.ADMIN) {

      const {
        referralType,
        serviceType,
        quantity,
        unit,
        radioGroup,
        notes,
        charge,
        actionDate,         
        time,
        timeSpent,
        reminderDate,
        caseCategory,
        publishToApp,
        reminderTo,       
        referralCode, 
        referralSource
      } = this.adminGroup.getRawValue()

      const blockNoTime = Math.floor(this.globalS.getMinutes(time) / 5);
      const timeInMinutes = this.globalS.getMinutes(timeSpent)
      const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString()
      let prog = this.adminGroup.get('programChecked').value;
      let programs = this.adminGroup.get('programs').value;

     // checkedPrograms.push(programs.filter(x => x.checked == true));

      for (var checkProgram of checkedPrograms) {

        let data: ProcedureRoster = {
          clientCode: this.user.code,
          carerCode: this.token.code,
          serviceType: serviceType, //checkProgram.selected,
          date: format(new Date(actionDate), 'yyyy/MM/dd'),
          time: format(new Date(time), 'HH:mm'),

          creator: this.token.user,
          editer: this.token.user,

          billUnit: 'HOUR',
          agencyDefinedGroup: this.user.agencyDefinedGroup,
          referralCode: serviceType,
        
          timePercent: timePercentage,
          notes: notes,
          type: 7,
          duration: timeInMinutes / 5,
          blockNo: blockNoTime,
          reasonType: '',
          program: checkProgram.program,
          tabType: caseCategory
        }
        finalRoster.push(data);
      }

      let data: CallAssessmentProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: this.globalS.isVarious(checkedPrograms),
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: NOTE_TYPE[radioGroup],
          extraDetail2: caseCategory,
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes,
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : '',
          reminderTo: reminderTo
        }
      }

      console.log(data);

      this.listS.postadministration(data)
        .subscribe(data => {
          this.globalS.sToast('Success', 'Success');
          this.adminOpen = false;
          this.handleCancel()
     
          this.cd.detectChanges();
        });
    }

    if (this.option == RECIPIENT_OPTION.ITEM) {

      const {
        referralType,
        quantity,
        unit,
        radioGroup,
        notes,
        charge,
        suppliedDate,
        date,
        refNo,
        reminderDate,
        caseCategory,
        publishToApp,
        programChecked,
        reminderTo
      } = this.itemGroup.getRawValue()

      for (var checkProgram of checkedPrograms) {
        let data: ProcedureRoster = {

          clientCode: this.user.code,
          carerCode: this.token.code,
          serviceType: referralType,
          date: date,

          // NO TIME IN ITEM
          time: '00:00',

          creator: this.token.user,
          editer: this.token.user,

          billUnit: 'SERVICE',
          agencyDefinedGroup: this.user.agencyDefinedGroup,

          referralCode: '',

          timePercent: quantity.toString(),
          costUnit: unit,

          notes: `${caseCategory}-${notes}`,
          billDesc: `${notes}`,
          unitBillRate: charge,

          // type 14 in item
          type: 14,

          duration: 0,

          //item 0 in item
          blockNo: 0,

          apiInvoiceDate: suppliedDate ? format(new Date(suppliedDate),'yyyy/MM/dd') : '',
          apiInvoiceNumber: refNo,

          reasonType: '',
          program: checkProgram.program,
          tabType: 'ITEM'
        }
        finalRoster.push(data);
      }

      let data: CallAssessmentProcedure = {
        roster: finalRoster,
        note: {
          personId: this.user.id,
          program: programChecked,
          detailDate: format(new Date, 'yyyy/MM/dd'),
          extraDetail1: NOTE_TYPE[radioGroup],
          extraDetail2: caseCategory,
          whoCode: this.user.code,
          publishToApp: publishToApp ? 1 : 0,
          creator: this.token.user,
          note: notes,
          alarmDate: reminderDate!=null ? format((reminderDate), 'yyyy/MM/dd') : null,
          reminderTo: reminderTo
        }
      }

      this.listS.postitem(data).subscribe(data => {
        this.globalS.sToast('Success', 'Item Added');
        this.itemOpen = false;
          this.handleCancel()     
          this.cd.detectChanges();
        
      });

    }
  }

  populate() {

    this.whatOptionVar = {};
    this.loadPrograms = true;
    this.recipient = this.SharedS.getRecipientData();
    this.otherTitleDetais = this.recipient.firstName + ' ' + this.recipient.surnameOrg + ' : ' + this.recipient.gender + ' : b ' + format(new Date(this.recipient.dateOfBirth), 'dd/MM/yyyy');

   // this.listS.casemanagerslist().pipe(takeUntil(this.unsubscribe$)).subscribe(data => { this.caseManagers = data.map(x => x.description) });
   // this.listS.GetTraccsStaffCodes().pipe(takeUntil(this.unsubscribe$)).subscribe(data => { this.lstusers = data });
   
   this.listS.getUsersList().pipe(takeUntil(this.unsubscribe$)).subscribe(data => { this.lstusers = data.map(d=>d.name); this.caseManagers = this.lstusers });

    switch (this.option) {
      case RECIPIENT_OPTION.REFER_IN:
        this.whatOptionVar = {
          title: this.globalS.titleCase(this.otherTitleDetais).trimLeft() + ' Referral Registration Wizard',
        }
       // this.referInGroup.patchValue ({type:1})
       // this.referralInChange(1)
        break;

      case RECIPIENT_OPTION.REFER_ON:
        this.listS.getlist(`SELECT DISTINCT UPPER(rp.[Program]) AS Program, hr.[type] FROM RecipientPrograms rp INNER JOIN HumanResourceTypes hr ON hr.NAME = rp.program WHERE rp.PersonID = '${this.user.id}' AND ProgramStatus = 'REFERRAL' AND isnull([Program], '') <> ''                 `)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Referral Out Registration Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  type: x.type,
                  status: false,
                }
              })
            }
            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.REFER_ON);
            this.changeDetection();
          })
        break;
      case RECIPIENT_OPTION.NOT_PROCEED:
        this.listS.getlist(`SELECT DISTINCT UPPER(rp.[Program]) AS Program, hr.[type] FROM RecipientPrograms rp INNER JOIN HumanResourceTypes hr ON hr.NAME = rp.program WHERE rp.PersonID = '${this.user.id}' AND ProgramStatus = 'REFERRAL' AND isnull([Program], '') <> ''                 `)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Referral Not Proceeding Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  type: x.type,
                  status: false,
                }
              })
            }
            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.NOT_PROCEED);
            this.changeDetection();
          })
        break;
      case RECIPIENT_OPTION.ASSESS:
        this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program FROM RecipientPrograms WHERE PersonID = '${this.user.id}' AND ProgramStatus = 'REFERRAL' AND isnull([Program], '') <> ''`)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Assessment Registration Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  type: x.type,
                  status: false,
                }
              })
            }
            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ASSESS);
            this.changeDetection();
            // this._whatOptionVar = this.whatOptionVar
            // this.changeNoteEvent();
          })
        break;
      case RECIPIENT_OPTION.WAIT_LIST:
        this.listS.getlist(`SELECT DISTINCT UPPER( [Service Type] + ' (Pgm): ' + [ServiceProgram]) AS Program FROM  ServiceOverview SO INNER JOIN RecipientPrograms RP ON RP.PersonID = SO.PersonID WHERE  SO.PersonID = '${this.user.id}' AND  (ProgramStatus IN  ('ACTIVE', 'WAITING LIST') AND ServiceStatus IN ('ACTIVE', 'WAIT LIST'))  AND isnull([Program], '') <> ''`)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Waitlist Management Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                }

              })
            }
            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.WAIT_LIST);
            this.changeDetection();
            // this._whatOptionVar = this.whatOptionVar
            // this.changeNoteEvent();
          })
        break;
      case RECIPIENT_OPTION.ADMIT:

        if (this.from.display == 'admit') {
          this.listS.getadmitprograms(this.user.id).subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Admission Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x,
                  status: false
                }
              })
            }
            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ADMIT);

            //SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = 'CBI-80203' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] NOT IN ('ADMISSION') AND (ITM.EndDate Is Null OR ITM.EndDate >= convert(varchar,getDate(),111))) ORDER BY [Service Type]

           
           
            this.changeDetection();
          });

         
        }

        if (this.from.display == 'quote') {
          this.whatOptionVar = {
            title: this.globalS.titleCase(this.otherTitleDetais) + ' Admission Wizard',
            wizardTitle: '',
            programsArr: [{
              program: this.program,
              status: false
            }]
          }

          this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ADMIT);
          this.changeDetection();
        }

        this.timeSteps = timeSteps;


        this.listS.getrosterpublishdate().subscribe(data => [
          this.admitGroup.patchValue({
            rosterCreationDates: [new Date(defaultDate), new Date(data.date)]
          })
        ]);

        break;
      case RECIPIENT_OPTION.DISCHARGE:
        this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program, HumanResourceTypes.[Type] AS Type FROM RecipientPrograms LEFT JOIN HumanResourceTypes ON RecipientPrograms.Program = HumanResourceTypes.Name WHERE PersonID = '${this.user.id}' AND ProgramStatus IN ('ACTIVE', 'WAITING LIST') AND isnull([Program], '') <> '' AND ISNULL([UserYesNo3], 0) <> 1 AND ISNULL([User2], '') <> 'Contingency' `)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Discharge Registration Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  type: x.Type
                }

              })
            }

            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.DISCHARGE);
            this.changeDetection();
            // this._whatOptionVar = this.whatOptionVar
            // this.changeNoteEvent();
          })
        break;
      case RECIPIENT_OPTION.SUSPEND:
        this.listS.getlist(`SELECT DISTINCT '' + CASE WHEN ([serviceprogram] <> '') AND ([serviceprogram] IS NOT NULL) THEN [serviceprogram] ELSE '?' END + ' ~> ' + [service type] AS Program FROM serviceoverview INNER JOIN recipientprograms ON serviceoverview.personid = recipientprograms.personid WHERE serviceoverview.personid = '${this.user.id}' AND programstatus = 'ACTIVE' AND servicestatus = 'ACTIVE'`)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Service Suspension Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  checked: false,
                  type: x.Type
                }
              })
            }

            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.SUSPEND);
            this.changeDetection();

            // this._whatOptionVar = this.whatOptionVar
            // this.changeNoteEvent();
          })
        break;
      case RECIPIENT_OPTION.REINSTATE:
        this.listS.getlist(`select distinct '[' + CASE WHEN ([ServiceProgram] <> '') AND ([ServiceProgram] IS NOT Null) THEN [ServiceProgram] ELSE '?' END + '] ~> ' + [Service Type] As activity  
          FROM ServiceOverview INNER JOIN RecipientPrograms ON ServiceOverview.PersonID = RecipientPrograms.PersonID 
          WHERE ServiceOverview.PersonID = '${this.user.id}'  AND (ProgramStatus = 'ON HOLD' OR ServiceStatus = 'ON HOLD') `).subscribe(data => {

              console.log(data);
              this.whatOptionVar = {
                title: this.globalS.titleCase(this.otherTitleDetais) + ' Reinstate Wizard',
                wizardTitle: '',
                programsArr: data.map(x => {
                  return {
                    program: x.activity,
                    checked: false
                  }
                })
              }
          
          this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.REINSTATE);
          this.changeDetection();
        });
      break;
    case RECIPIENT_OPTION.DECEASE:
        this.listS.getlist(`SELECT DISTINCT Upper([program]) AS Activity 
                FROM   recipientprograms 
                LEFT JOIN humanresourcetypes 
                ON recipientprograms.program = humanresourcetypes.NAME 
                WHERE  personid = '${this.user.id}' 
                AND programstatus IN ( 'ACTIVE', 'WAITING LIST' ) 
                AND Isnull([program], '') <> '' 
                AND Isnull([useryesno3], 0) <> 1 
                AND Isnull([user2], '') <> 'Contingency' `).subscribe(data => {

          console.log(data);
          this.whatOptionVar = {
            title: this.globalS.titleCase(this.otherTitleDetais) + 'Deceased Wizard',
            wizardTitle: '',
            programsArr: data.map(x => {
              return {
                program: x.activity,
                checked: false
              }
            })
          }

          this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.DECEASE);
          this.changeDetection();

        
        })
        break;
      case RECIPIENT_OPTION.ADMIN:
       // this.otherTitleDetais = this.recipient.firstName + ' ' + this.recipient.surnameOrg + ' : ' + this.recipient.gender;


        this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program FROM RecipientPrograms WHERE PersonID = '${this.user.id}'AND ProgramStatus <> 'INACTIVE' AND isnull([Program], '') <> '' `)
          .subscribe(data => {
            this.whatOptionVar = {

              title: `${this.globalS.titleCase(this.otherTitleDetais)}   Administration Registration Wizard`,
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  checked: false,
                  selected: ''
                }
              })
            }

            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ADMIN);
            this.changeDetection();
          })

        // this.listS.getfollowups({
        //   branch: this.BRANCH_NAME,
        //   fundingType: this.FUNDING_TYPE
        // }).pipe(takeUntil(this.destroy$)).subscribe(data => {

        //   this.notifFollowUpGroup = data.map(x => {
        //     return {
        //       label: x.reminders,
        //       value: x.reminders,
        //       dateCounter: x.user1,
        //       disabled: false,
        //       checked: false
        //     }
        //   });
        // })


        // this.listS.getdocumentslist(
        //   {
        //     branch: this.BRANCH_NAME,
        //     fundingType: this.FUNDING_TYPE
        //   }
        // ).pipe(takeUntil(this.destroy$)).subscribe(data => {
        //   this.notifDocumentsGroup = data.map(x => {
        //     return {
        //       label: x,
        //       value: x,
        //       disabled: false,
        //       checked: false
        //     }
        //   })
        // })

        // this.listS.getdatalist({
        //   branch: this.BRANCH_NAME,
        //   fundingType: this.FUNDING_TYPE
        // }).pipe(takeUntil(this.destroy$)).subscribe(data =>  {
        //   this.datalist = data.map(x =>{
        //     return {
        //       form: x.form,
        //       link: (x.link).toLowerCase()
        //     }
        //   })
        // });

        break;
      case RECIPIENT_OPTION.ITEM:
        this.listS.getlist(`SELECT DISTINCT UPPER([Program]) AS Program FROM RecipientPrograms WHERE PersonID = '${this.user.id}'AND ProgramStatus <> 'INACTIVE' AND isnull([Program], '') <> '' `)
          .subscribe(data => {
            this.whatOptionVar = {
              title: this.globalS.titleCase(this.otherTitleDetais) + ' Record Items Wizard',
              wizardTitle: '',
              programsArr: data.map(x => {
                return {
                  program: x.program,
                  checked: false
                }
              })
            }

            this.formProgramArray(this.whatOptionVar, RECIPIENT_OPTION.ITEM);
          
            // this._whatOptionVar = this.whatOptionVar
            // this.changeNoteEvent();
          })
        break;
        case RECIPIENT_OPTION.DEACTIVATE:
          this.whatOptionVar = {
            title: this.globalS.titleCase(this.otherTitleDetais) + ' Deactivate Wizard',
            wizardTitle: '',
            
          }
          this.changeDetection();
        break;
      
    }
  }

  aaa(data) {
    console.log(data)
  }

  GETREFERRAL_LIST() {
    if (this.option == RECIPIENT_OPTION.ITEM) {
      console.log('item');
    }
  }

  GET_FUNDINGTYPE() {

    this.globalFormGroup.get('programChecked')
      .valueChanges
      .pipe(
        distinctUntilChanged(),
        switchMap(x => {
          return this.listS.getspecifictype(x)
        })
      )
      .subscribe(d => {
        this.FUNDING_TYPE = d;
      })
  }

  formProgramArray(data: any, type: RECIPIENT_OPTION) {

    this.loadPrograms = false;

    if (type == RECIPIENT_OPTION.ITEM) {
      var prog = this.itemGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.itemGroup;

      this.noteArray = ['ITEM'];
      this.GET_FUNDINGTYPE();
      if (this.itemGroup.get('programs').value.length == 1) {
        let programs = (<FormArray>this.adminGroup.controls['programs']).at(0);
        programs.patchValue({ checked: true });
        this.itemGroup.patchValue({
          programChecked: programs.value.program
        });
      }
      return;
    }

    if (type == RECIPIENT_OPTION.ADMIN) {
      var prog = this.adminGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.adminGroup;

      if (this.adminGroup.get('programs').value.length == 1) {
        let programs = (<FormArray>this.adminGroup.controls['programs']).at(0);
        this.adminGroup.patchValue({
          programChecked: programs.value.program
        });
      }

      this.GET_FUNDINGTYPE()
    }
    if (type == RECIPIENT_OPTION.REINSTATE) {
      var prog = this.reinstateGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.reinstateGroup;

      if (this.reinstateGroup.get('programs').value.length == 1) {
        let programs = (<FormArray>this.reinstateGroup.controls['programs']).at(0);
        this.reinstateGroup.patchValue({
          programChecked: programs.value.program
        });
      }

      this.GET_FUNDINGTYPE()
    }
    if (type == RECIPIENT_OPTION.ADMIT) {
      var prog = this.admitGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.admitGroup;

      if ((this.admitGroup.get('programs').value as Array<any>).length == 1) {
        let programs = (<FormArray>this.admitGroup.controls['programs']).at(0);
        this.selectedProgram= programs;
        this.admitGroup.patchValue({
          programChecked: programs.value.program,
         
        });

       
        this.populateOtherDetails();
      }

      this.GET_FUNDINGTYPE();
    }
    if (type == RECIPIENT_OPTION.DECEASE) {
      var prog = this.deceaseGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.deceaseGroup;
      this.GET_FUNDINGTYPE()
      if (this.deceaseGroup.get('programs').value.length == 1) {
        let programs = (<FormArray>this.adminGroup.controls['programs']).at(0);
        this.deceaseGroup.patchValue({
          programChecked: programs.value.program
        });
      }
      this.cd.detectChanges();
    }

    if (type == RECIPIENT_OPTION.SUSPEND) {
      var prog = this.suspendGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.suspendGroup;
      this.GET_FUNDINGTYPE();

      if((this.suspendGroup.get('programs').value as Array<any>).length == 1){
        let programs = (<FormArray>this.suspendGroup.controls['programs']).at(0);
        programs.patchValue({ checked: true });
        this.selectedProgramChange( programs)
        this.suspendGroup.patchValue({
          programChecked: programs.value.program
        });
      }

     
    }

    if (type == RECIPIENT_OPTION.DISCHARGE) {
      var prog = this.dischargeGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.dischargeGroup;
      this.noteArray = ['DISCHARGE'];
      this.GET_FUNDINGTYPE();

      if((this.dischargeGroup.get('programs').value as Array<any>).length == 1){
        let programs = (<FormArray>this.dischargeGroup.controls['programs']).at(0);
        programs.patchValue({ checked: true });
        this.selectedProgramChange( programs)
        this.dischargeGroup.patchValue({
          programChecked: programs.value.program
        });
      }

 
      this.cd.detectChanges();
      return;
    }

    if (type == RECIPIENT_OPTION.WAIT_LIST) {
      var prog = this.waitListGroup.get('programs') as FormArray;

      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.waitListGroup;

      

      this.GET_FUNDINGTYPE()
    }

    if (type == RECIPIENT_OPTION.ASSESS) {
      var prog = this.assessGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.assessGroup;

      if((this.assessGroup.get('programs').value as Array<any>).length == 1){
        let programs = (<FormArray>this.assessGroup.controls['programs']).at(0);
        programs.patchValue({ checked: true });
        this.selectedProgramChange( programs)
        this.assessGroup.patchValue({
          programChecked: programs.value.program
        });
      }

      this.noteArray = ['SCREEN/ASSESS'];
      this.GET_FUNDINGTYPE()
      return;
    }

    if (type == RECIPIENT_OPTION.NOT_PROCEED) {
      var prog = this.notProceedGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.notProceedGroup;

       if((this.notProceedGroup.get('programs').value as Array<any>).length == 1){
        let programs = (<FormArray>this.notProceedGroup.controls['programs']).at(0);
        programs.patchValue({ checked: true });
        this.selectedProgramChange( programs)
        this.notProceedGroup.patchValue({
          programChecked: programs.value.program
        });
      }

      this.GET_FUNDINGTYPE()
    }

    if (type == RECIPIENT_OPTION.REFER_ON) {
      var prog = this.referOnGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.referOnGroup;


      if((this.referOnGroup.get('programs').value as Array<any>).length == 1){
        let programs = (<FormArray>this.referOnGroup.controls['programs']).at(0);
        programs.patchValue({ checked: true });
        this.selectedProgramChange( programs)
        this.referOnGroup.patchValue({
          programChecked: programs.value.program
        });
      }

      this.noteArray = ['REFERRAL-OUT'];
      this.GET_FUNDINGTYPE()
      return;
    }

    if (type == RECIPIENT_OPTION.REFER_IN) {
      var prog = this.referInGroup.get('programs') as FormArray;
      data.programsArr.map(x => prog.push(this.createProgramForm(x)));
      this.globalFormGroup = this.referInGroup;


      this.noteArray = ['REFERRAL-IN'];
      return;
    }

    // set for all option
    
    if((this.globalFormGroup.get('programs').value as Array<any>).length == 1){
      let programs = (<FormArray>this.globalFormGroup.controls['programs']).at(0);
      programs.patchValue({ checked: true });
      this.selectedProgramChange( programs)
      this.globalFormGroup.patchValue({
        programChecked: programs.value.program
      });
    }

    this.changeDetection();

    combineLatest([
      this.globalFormGroup.get('radioGroup').valueChanges.pipe(startWith('case')),
      this.globalFormGroup.get('notes').valueChanges.pipe(startWith(''))
    ]).pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap(x => {
        //this.globalFormGroup.patchValue({ caseCategory: null });

        if (!x || this.globalS.isEmpty(x[0]) || this.globalS.isEmpty(x[1])) {
          this.noteArray = ['OTHER'];
          this.globalFormGroup.patchValue({ caseCategory: 'OTHER' });
          return EMPTY;
        }

        let index = x[0] === 'case' ? 0 : x[0] === 'op' ? 1 : 2;
        return this.listS.getcasenotecategory(index);
      })
    ).subscribe(data => {
      if (this.option == RECIPIENT_OPTION.SUSPEND)
      this.noteArray = data;
      this.changeDetection();
    });
  }


  createProgramForm(data: any): FormGroup {
    return this.fb.group({
      program: new FormControl(data.program),
      checked: new FormControl(data.checked),
      type: new FormControl(data.type),
      selected: new FormControl(data.selected),
      services : null
    });
  }

  changeDetection() {
    this.cd.markForCheck();
    this.cd.detectChanges();
  }


  pre() {
    this.current -= 1;
  }

  next() {

    if (this.adminOpen) {
      if (this.current < 4) {
        this.current += 1;
      }
    }

    if (this.dischargeOpen) {
      if (this.current < 7) {
        this.current += 1;
      }
    }

    if (this.waitListOpen) {
      if (this.current < 7) {
        this.current += 1;
      }
    }

    if (this.admitOpen) {
      if (this.current < 8) {
        this.current += 1;
      }
    }

    if (this.assessOpen) {
      if (this.current < 7) {
        this.current += 1;
      }
    }

    if (this.notProceedOpen) {
      if (this.current < 7) {
        this.current += 1;
      }
    }

    if (this.referInOpen) {
      if (this.current < 9) {
        this.current += 1;
      }
    }

    if (this.referOnOpen) {
      if (this.current < 7) {
        this.current += 1;
      }
    }

    if (this.suspendOpen) {
      if (this.current < 7) {
        this.current += 1;
      }
    }

    if (this.itemOpen) {
      if (this.current < 4) {
        this.current += 1;
      }
    }

    if (this.deceaseOpen) {
      if (this.current < 4) {
        this.current += 1;
      }
    }

    // Other Details Tab Populate 
    if (this.current == 1) {
      this.populateOtherDetails();
    }

    if (this.current == 2) {
      this.populateNotificationDetails();

    }
    // this.checkedPrograms = this.GET_CHECKEDPROGRAMS();
  }

  GETLISTNAME(data: any): string {
    if (data == RECIPIENT_OPTION.REFER_IN) {
      return "Referral Notification"
    }

    if (data == RECIPIENT_OPTION.REFER_ON) {
      return "Refer On Notification"
    }

    if (data == RECIPIENT_OPTION.ASSESS) {
      return "Assessment Notification"
    }

    if (data == RECIPIENT_OPTION.ADMIT) {
      return "Admission Notification"
    }

    if (data == RECIPIENT_OPTION.NOT_PROCEED) {
      return "Not Proceed Notification"
    }

    if (data == RECIPIENT_OPTION.DISCHARGE) {
      return "Discharge Notification"
    }

    if (data == RECIPIENT_OPTION.SUSPEND) {
      return "Suspend Notification"
    }

    if (data == RECIPIENT_OPTION.REINSTATE) {
      return "Reinstate Notification"
    }
  }

  GET_SERVICE_TYPE(): string {
    if (this.option == RECIPIENT_OPTION.REFER_IN) {
      return this.referInGroup.get('referralType').value;
    }

    if (this.option == RECIPIENT_OPTION.REFER_ON) {
      return this.referOnGroup.get('referralType').value;
    }

    if (this.option == RECIPIENT_OPTION.ASSESS) {
      return this.assessGroup.get('serviceType').value;
    }

    if (this.option == RECIPIENT_OPTION.ADMIT) {
      return this.admitGroup.get('admissionType').value;
    }

    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {
      return this.notProceedGroup.get('referralType').value;
    }

    if (this.option == RECIPIENT_OPTION.DISCHARGE) {
      return this.dischargeGroup.get('dischargeType').value;
    }

    if (this.option == RECIPIENT_OPTION.ADMIN) {
      console.log(this.adminGroup.value);
    }

    if (this.option == RECIPIENT_OPTION.WAIT_LIST) {
      return this.waitListGroup.get('activityCode').value;
    }

    // if(this.option == RECIPIENT_OPTION.SUSPEND)
    // { 
    //   return this.suspendGroup.get('').value;
    // }

    // if(this.option == RECIPIENT_OPTION.REINSTATE)
    // { 
    //   return this.reinstateGroup.get('').value;
    // }    
  }

  populateNotificationDetails(): void {

    var type = this.GET_SERVICE_TYPE();

    this.listS.getnotifications({
      branch: this.BRANCH_NAME,
      coordinator: this.COORDINATOR,
      listname: this.GETLISTNAME(this.option),
      fundingsource: this.FUNDING_TYPE
    }).subscribe(data => {
      this.notifCheckBoxes = data.map(x => {
        return {
          label: x.staffToNotify,
          value: x.staffToNotify,
          email: x.email,
          disabled: x.mandatory ? true : false,
          checked: x.mandatory ? true : false
        }
      });
      this.changeDetection();
    });

    //Temporary skip next code
    if (1 == 1) return;
    this.listS.getfollowups({
      branch: this.BRANCH_NAME,
      fundingType: this.FUNDING_TYPE,
      type: type,
      group: 'FOLLOWUP'
    }).pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.notifFollowUpGroup = data.map(x => {
        return {
          label: x.reminders,
          value: x.reminders,
          dateCounter: x.user1,
          disabled: false,
          checked: false
        }
      });
      this.changeDetection();
    })


    this.listS.getdocumentslist({
      branch: this.BRANCH_NAME,
      fundingType: this.FUNDING_TYPE,
      type: type,
      group: 'DOCUMENTS'
    }).pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.notifDocumentsGroup = data.map(x => {
        return {
          label: x,
          value: x,
          disabled: false,
          checked: false
        }
      })
      this.changeDetection();
    });

    this.listS.getdatalist({
      branch: this.BRANCH_NAME,
      fundingType: this.FUNDING_TYPE,
      type: type,
      group: 'XTRADATA'
    }).pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.datalist = data.map(x => {
        return {
          form: x.form,
          link: (x.link).toLowerCase()
        }
      })
      this.changeDetection();
    });

  }

  GET_PRIMARY_ADDRESS() {

  
    let otherInfo='';
    let name = '';
    if (this.selectOption == 0 && (this.referInGroup.get('type').value == 2 || this.referInGroup.get('type').value == 3)){
      otherInfo = (this.referInGroup.get('pensionStatus').value??'Full Pensioner');
      name= this.selectedProgram 
    }
  
    this.listS.getprimaryphoneaddress({
      PersonId: this.user.id,
      Type: this.GET_FUNDING_TITLE(),
      Name: name ,
      otherInfo: otherInfo
    }).subscribe(data => {
      this.PATCH_NOTES(data);
    });
  }

  populateOtherDetails() {

    this.GET_PRIMARY_ADDRESS();

    if (this.option == RECIPIENT_OPTION.REFER_IN) {

      this.listS.getfundingSourcePerProgram(this.selectedProgram).subscribe(data => this.fundingSource = data);

      this.listS.getspecificemailmanager(this.COORDINATOR)
        .subscribe(data => {
          this.EMAIL_OF_COORDINATOR = data;
        });

      // this.populateNotificationDetails();
      this.referralCode$ = this.listS.getwizardreferralcode(); //.pipe(takeUntil(this.unsubscribe$)).subscribe(data => {=data});

      if (this.referInGroup.get('type').value == 1) {
        this.referralSource$ = of(['NDIA']);
        this.referInGroup.patchValue({
          referralSource: 'NDIA'
        });
      }

      if (this.referInGroup.get('type').value == 2 || this.referInGroup.get('type').value == 3 || this.referInGroup.get('type').value == 4) {
        this.referralSource$ = of(['My Aged Care Gateway']);
        this.referInGroup.patchValue({
          referralSource: 'My Aged Care Gateway'
        });
      }

      if (this.referInGroup.get('type').value == 5) {
        this.referralSource$ = of(['OTHER']);
        this.referInGroup.patchValue({
          referralSource: 'OTHER'
        });
      }
      // this.referralSource$ = this.listS.getreferraltypeAll()
      //   .pipe(
      //     switchMap(x => {
      //       if (x.length == 1) {
      //         this.referInGroup.patchValue({
      //           referralType: x[0]
      //         });
      //       }
      //       return of(x);
      //     })
      //   );

      //this.selectedProgram
      let pkg = this.referralCheckOptions.find(x => x.checked).name;
      this.referralType$ = this.listS.getreferraltype_latest(pkg)
        .pipe(
          switchMap(x => {
            if (x.length == 1) {
              this.referInGroup.patchValue({
                referralType: x[0]
              });
            }
            return of(x);
          })
        );
      return;
    }

    if (this.option == RECIPIENT_OPTION.REFER_ON) {
      // this.populateNotificationDetails();
      this.referralSource$ = this.listS.getwizardreferralsource('default');

      this.checkedPrograms = this.GET_CHECKEDPROGRAMS();

      var haccList: Array<any> = this.checkedPrograms.filter(x => x.type == 'HACC');
      if (haccList && haccList.length > 0)
        this.referralSource$ = this.listS.getwizardreferralsource('HACC');

      var decList: Array<any> = this.checkedPrograms.filter(x => x.type == 'DEX');
      if (decList && decList.length > 0)
        this.referralSource$ = this.listS.getwizardreferralsource('DEX');


      this.referralCode$ = this.listS.getwizardreferralcode();

      this.referralType$ =  this.listS.getreferraltypes({
        ProgramName: this.selectedProgram.value.program,
        ProgramType: RECIPIENT_OPTION.REFER_ON
      })
    }
   
    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {
      this.reasons$ = this.listS.getreferraltypes({
        ProgramName: this.selectedProgram.value.program,
        ProgramType: RECIPIENT_OPTION.ADMIT
      })



    }
    if (this.option == RECIPIENT_OPTION.ASSESS) {
      // this.populateNotificationDetails();
      this.reasons$ = this.listS.getreferraltypes({
        ProgramName: this.selectedProgram.value.program,
        ProgramType: RECIPIENT_OPTION.ASSESS
      })

    }

    if (this.option == RECIPIENT_OPTION.ADMIN) {
      // this.populateNotificationDetails();
    }

    if (this.option == RECIPIENT_OPTION.WAIT_LIST) {
      this.reasons$ = this.listS.getwaitlist();
    }

    if (this.option == RECIPIENT_OPTION.DECEASE) {
      // this.populateNotificationDetails();
      this.reasons$ = this.listS.getreasons();
      this.dischargeTypes$ = this.listS.getlist(`SELECT [Title] FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ADMISSION') AND [MinorGroup] IN ('DISCHARGE') AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111)) ORDER BY [Title]`);
       this.dischargeTypes$.pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
        this.dischargeTypes=data.map(x=>x.title);
        if ( this.dischargeTypes.length==1){
          this.deceaseGroup.patchValue({dischargeType:this.dischargeTypes[0]})
        }
        this.cd.detectChanges();
       });
    }

    if (this.option == RECIPIENT_OPTION.SUSPEND) {
      // this.populateNotificationDetails();
      this.listS.getlist(`SELECT title FROM itemtypes WHERE ( enddate IS NULL OR enddate >= '${this.CURRENT_DATE.toISOString()}' ) AND rostergroup = 'RECPTABSENCE' AND status = 'ATTRIBUTABLE' AND processclassification = 'EVENT' ORDER BY title`)
      .pipe(takeUntil(this.unsubscribe$)).subscribe(d=>{ this.cancellationCode$=d
        if (d.length==1){
          this.suspendGroup.patchValue({cancellationCode: d[0].title})
        }
      })
      this.suspendGroup.patchValue({reason: 'NSDF : No show due to family issues.'});
      this.suspendreasons.push('NSDF : No show due to family issues.');
       this.listS.getsuspendereasons().pipe(takeUntil(this.unsubscribe$)).subscribe(d=>{ 
        this.suspendreasons=d
        if (d.length==1){
          this.suspendGroup.patchValue({reason: d[0]})
        }
      });
      
    }
   
    if (this.option == RECIPIENT_OPTION.DISCHARGE) {
      // this.populateNotificationDetails();
      //, HACCCode, RecordNumber
      this.dischargeReason$ = this.listS.getlist(`SELECT Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' Group By Description`);
       this.dischargeTypes$ = this.listS.getlist(`SELECT [Title] FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ADMISSION') AND [MinorGroup] IN ('DISCHARGE') AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111)) ORDER BY [Title]`);
       this.dischargeTypes$.pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
        this.dischargeTypes=data.map(x=>x.title);
        if ( this.dischargeTypes.length==1){
          this.dischargeGroup.patchValue({dischargeType:this.dischargeTypes[0]})
        }
        this.cd.detectChanges();
       });
    }

    if (this.option == RECIPIENT_OPTION.ITEM) {
      // this.populateNotificationDetails();
      let _input = {
        program: '',
        option: this.option
      }
      this.itemTypes$ = this.sqlWiz.GETREFERRALTYPE_V2(_input);
    }

    if (this.option == RECIPIENT_OPTION.ADMIT) {
      // this.populateNotificationDetails();
      this.OpenServiceList(this.selectedProgram.value.program)

      this.listS.getadmissiontypes(this.selectedProgram).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
        if (data.length>0){
          this.admissionActiviType=data;
        if (data.length==1)
          this.admitGroup.patchValue({admissionType :data[0]});
        
      }else{
        this.admissionActiviType=[];
        this.admissionActiviType.push('*ADMISSION');
        this.admitGroup.patchValue({admissionType :'*ADMISSION'})
      }
      })
    }

    if (this.option == RECIPIENT_OPTION.NOT_PROCEED) {
      // this.populateNotificationDetails();
    }

    this.checkedPrograms = this.GET_CHECKEDPROGRAMS();
  }


  GET_CHECKEDPROGRAMS() {
    var programs: Array<any> = this.globalFormGroup.get('programs').value || [];
    if (programs.length == 0) return [];
    let checked = programs.filter(x => x.checked);

    return checked;
  }

  get canGoNext(): boolean {

    if (this.option == RECIPIENT_OPTION.REFER_IN) {
      if (this.FUNDING_TYPE == 'NDIA' && this.selectedProgram && !this.isPackageNameAvailable) {
        return true;
      }

      if (this.FUNDING_TYPE == 'DOHA' && this.selectedProgram && !this.isPackageNameAvailable) {
        return true;
      }

      if (this.FUNDING_TYPE == 'DSS' && this.selectedProgram) {
        return true;
      }

      if (this.selectedProgram && this.isPackageNameAvailable == false) {
        return true;
      }
    }

    if (this.option == RECIPIENT_OPTION.ADMIT && this.admitGroup.get('programChecked').value) {
      return true;
    }

    if (this.option == RECIPIENT_OPTION.DECEASE) {
      return true;
    }

    if (this.option == RECIPIENT_OPTION.WAIT_LIST && this.waitListGroup.get('programChecked').value) {
      return true;
    }

    if (this.option == RECIPIENT_OPTION.REFER_ON && this.referOnGroup.get('programChecked').value) {
      if (this.current == 1 && this.globalS.isEmpty(this.referOnGroup.get('referralType').value)) {
        return false;
      }
      return true;
    }

    if (this.option == RECIPIENT_OPTION.NOT_PROCEED && this.notProceedGroup.get('programChecked').value) {
      if (this.current == 1 && this.globalS.isEmpty(this.notProceedGroup.get('referralType').value)) {
        return false;
      }
      return true;
    }

    if (this.option == RECIPIENT_OPTION.ASSESS && this.assessGroup.get('programChecked').value) {
      if (this.current == 1 && this.globalS.isEmpty(this.assessGroup.get('serviceType').value)) {
        return false;
      }
      return true;
    }

    if (this.option == RECIPIENT_OPTION.DISCHARGE && this.selectedProgram) {
      return true;
    }

    if (this.option == RECIPIENT_OPTION.SUSPEND && this.selectedProgram) {
      return true;
    }

    if (this.option == RECIPIENT_OPTION.ITEM && this.selectedProgram) {
      return true;
    }

    if (this.option == RECIPIENT_OPTION.ADMIN && this.selectedProgram) {
      return true;
    }




    return false;
  }

  get canBeDone(): boolean {
    return true;
  }

  handleClose() {
    // this.option = null;
    // this.open = false;
    // this.itemOpen = false;
    // this.adminOpen = false;
    // this.deceaseOpen = false;
    // this.reinstateOpen = false;
    // this.suspendOpen = false;
    // this.dischargeOpen = false;
    // this.waitListOpen = false;
    // this.admitOpen = false;
    // this.assessOpen = false;
    // this.notProceedOpen = false;
    // this.referOnOpen = false;
    // this.referInOpen = false;

    this.referdocument = false;
    this.referIndocument = false;
    //  this.option = null
  }

  handleCancel() {
    this.open = false;
    this.itemOpen = false;
    this.adminOpen = false;
    this.deceaseOpen = false;
    this.reinstateOpen = false;
    this.suspendOpen = false;
    this.dischargeOpen = false;
    this.dischargeOpenPerson = false;
    this.waitListOpen = false;
    this.admitOpen = false;
    this.assessOpen = false;
    this.notProceedOpen = false;
    this.referOnOpen = false;
    this.referInOpen = false;
    this.saving = false;
    this.referdocument = false;
    this.selectedProgram = null;
    this.referInGroup.reset()
    this.referOnGroup.reset();
    this.deactivateOpen=false;

    this.changeDetection();

  }

  handleOk() {
    this.referdocument = false;
  }

  haha(data: any) {
    console.log(data);
  }

  onChange(data: any) {

  }
  handleChange({ file, fileList }: UploadChangeParam): void {
    const status = file.status;
    if (status !== 'uploading') {
      // console.log(file, fileList);
    }
    if (status === 'done') {
      this.globalS.sToast('Success', `${file.name} file uploaded successfully.`);


    } else if (status === 'error') {
      this.globalS.sToast('Success', `${file.name} file upload failed.`);

    }
  }

  emailnotify() {

    const { notes } = this.referInGroup.getRawValue();

    let notifications = this.notifCheckBoxes.filter((x: any) => x.checked == true);
    let emails = notifications.map((x: any) => x.email).join(';')
    let validEmails =  this.globalS.emailaddress.filter(email => email.includes('@')).join(';');

    var emailSubject = "ADAMAS NOTIFICATION";
    var emailBody = notes;
    

    // location.href = "mailto:" + validEmails + "?" +
    //   (emailSubject ? "subject=" + emailSubject : "") +
    //   (emailBody ? "&body=" + emailBody : "");

      // Suppose validEmails is an array or string of emails separated by semicolons.
      var toEmails = validEmails; // Ensure it's a string of emails separated by commas or semicolons.
      var subjectParam = emailSubject ? "subject=" + encodeURIComponent(emailSubject) : "";
      var bodyParam = emailBody ? "body=" + encodeURIComponent(emailBody) : "";

      // Determine the connector for subject/body parameters.
      let params = "";
      if (subjectParam && bodyParam) {
        params = subjectParam + "&" + bodyParam;
      } else {
        params = subjectParam || bodyParam;
      }

      const mailtoLink = "mailto:" + toEmails + (params ? "?" + params : "");
      console.log(mailtoLink);
      location.href = mailtoLink;


    this.globalS.emailaddress = [];
    this.handleCancel();
  }

  writereminder(personid: string, notes: string, followups: Array<any>) {
    var sql = '', temp = '';

    // console.log(this.notifFollowUpGroup);

    for (var followup of followups) {
      var dateCounter = parseInt(followup.dateCounter);
      var reminderDatePlusDateCounter = format(addDays(new Date(), dateCounter), 'dd/MM/yyyy');

      if (followup.checked) {
        sql = sql + "INSERT INTO HumanResources([PersonID], [Notes], [Group],[Type],[Name],[Date1],[Date2]) VALUES ('" + personid + "','" + notes + "'," + "'RECIPIENTALERT','RECIPIENTALERT','" + followup.label + "','" + reminderDatePlusDateCounter + "','" + reminderDatePlusDateCounter + "');";
      }
    }

    // let Date1 : Date   = new Date();
    // let Date2 : Date   = new Date(Date1);

    // temp = (this.globalS.followups.label).toString().substring(0,2);

    // switch (temp) {
    //   case '10':
    //   Date2.setDate(Date2.getDate() + 10)

    //   break;
    //   case '30':
    //   Date2.setDate(Date2.getDate() + 30)
    //   break;

    //   default:
    //   break;
    // }

    // sql = "INSERT INTO HumanResources([PersonID], [Notes], [Group],[Type],[Name],[Date1],[Date2]) VALUES ('"+this.globalS.id.toString()+"','"+ this.globalS.followups.label.toString()+"',"+"'RECIPIENTALERT','RECIPIENTALERT','FOLLOWUP REMINDER','" +format(Date1,'yyyy/MM/dd') +"','"+format(Date2,'yyyy/MM/dd') +"') ";

    this.clientS.addRefreminder(sql).subscribe(x => console.log(x));
    // this.globalS.followups = null;
  }

  addRefdoc() {
    //console.log(this.globalS.doc.toString());
    /*if (this.globalS.doc.toString() != null){ 
      console.log(this.globalS.doc.toString());                 
      this.referdocument = true;
    } */

    //this.referdocument = true;
    this.referIndocument = true;

    // this.customReq()

  }
  customReq = () => {
    //console.log(this.globalS.doc.label)

    console.log(this.file);
    //  this.referdocument = false;
    const formData = new FormData();

    //const { program, discipline, careDomain, classification, category, reminderDate, publishToApp, reminderText, notes  } = this.incidentForm.value;

    formData.append('file', this.file as any);
    /*formData.append('data', JSON.stringify({
      PersonID: this.innerValue.id,
      DocPath: this.token.recipientDocFolder,
      
      Program: program,
      Discipline: discipline,
      CareDomain: careDomain,
      Classification: classification,
      Category: category,
      ReminderDate: reminderDate,
      PublishToApp: publishToApp,
      ReminderText: reminderText,
      Notes: notes,
      SubId: this.innerValue.incidentId
    })) */

    const req = new HttpRequest('POST', this.urlPath, formData, {
      reportProgress: true,
      withCredentials: true
    });

    var id = this.globalS.loadingMessage(`Uploading file ${this.file.name}`)
    this.http.request(req).pipe(filter(e => e instanceof HttpResponse)).subscribe(
      (event: HttpEvent<any>) => {
        this.msg.remove(id);
        this.globalS.sToast('Success', 'Document uploaded');
      },
      err => {
        console.log(err);
        this.msg.error(`${this.file.name} file upload failed.`);
        this.msg.remove(id);
      }
    );

  };

  doc(data: any) {
    var temp = data.filter(x => x.checked)
    this.globalS.doc = temp.map(x => x.value);
    return this.globalS.doc
  }

  notif(data: any) {
    var temp1 = data.filter(x => x.checked === true).map(x => x.email);
    this.globalS.emailaddress = temp1;
  }

  followup(data: any) {
    var temp
    temp = data.find(x => x.checked === true)
    this.globalS.followups = temp
  }
  ListType: any;
  ViewList: boolean;
  listTitle: any = '';
  alist2: Array<any> = [];
  alist2_original: Array<any> = [];
  HighLightRow2: number;
  activeRowData: any;
  selectedRowIndex: any;
  loading2: boolean = false;
  txtSearch: any;
  ViewRefferaltypeList: boolean;
  ViewServiceList:boolean;

  OpenServiceList(program){
    this.ListType = 'serviceType';
    this.listTitle = 'Select services ';
    this.loading2 = true;
     this.listS.getreferraltypes({
      ProgramName: program,
      ProgramType: RECIPIENT_OPTION.ADMIT
    }).subscribe(d => {
      this.alist2 = d.map(x => {
        return {
          value: x,
          checked: false

        }
      })

      if (this.alist2.length == 1){
        this.alist2[0].checked=true;
      }
  
      if (this.alist2.length <= 0)
        this.alist2.push({ value: this.option, checked: false });
      this.ViewRefferaltypeList = true;
      this.alist2_original = this.alist2;
      this.cd.detectChanges();
      this.loading2 = false;

    });
  }

  openList(type: string) {
    this.ListType = type;

    this.alist2 = [];
    this.loading2 = true;

    setTimeout(() => {
      switch (type) {
        case 'referralSource':
          this.listTitle = 'Select a Referral Source ';
          this.referralSource$.subscribe(d => {

            if (this.selectOption == 1)
              this.alist2 = d.map(x => x.description);
            else
              this.alist2 = d;

            this.ViewList = true;
            this.alist2_original = this.alist2;
            this.cd.detectChanges();
            this.loading2 = false;
          });
          break;
        case 'referralCode':
          this.listTitle = 'Select a Referral Code ';
          if (this.referralCode$ == null)
            this.referralCode$ = this.listS.getwizardreferralcode();


          this.referralCode$.subscribe(d => {
            this.alist2 = d;
            this.ViewList = true;
            this.alist2_original = this.alist2;
            this.cd.detectChanges();
            this.loading2 = false;
          });
          break;
        case 'ReferralType':
          this.listTitle = 'Select a Referral Type ';
          let pkg = '';
          if (this.referralType$ == null)
            this.referralType$ = this.listS.getreferraltype_latest(pkg)

          this.referralType$.subscribe(d => {
            this.alist2 = d.map(x => {
              return {
                value: x,
                checked: false

              }
            }
            );
            if (this.alist2.length <= 0)
              this.alist2.push({ value: this.option, checked: false });
            this.ViewRefferaltypeList = true;
            this.alist2_original = this.alist2;
            this.cd.detectChanges();
            this.loading2 = false;

          });
          break;
        case 'ReasonCode':
          this.listTitle = 'Select a Reason Code ';
          this.reasons$.subscribe(d => {
            this.alist2 = d;//.map(x=> x.activity);
            if (this.alist2.length <= 0) {
              this.alist2.push('NOT PROCEEDING');
              this.notProceedGroup.patchValue({ caseCategory: 'NOT PROCEEDING', reasonCode: 'NOT PROCEEDING', referralType: 'NOT PROCEEDING', referralCode: 'NOT PROCEEDING', referralSource: 'NOT PROCEEDING' });
              this.cd.detectChanges();
              return;
            }
            this.ViewList = true;
            this.alist2_original = this.alist2;
            this.cd.detectChanges();
            this.loading2 = false;

          });
          break;
        case 'Assesment':
          this.listTitle = 'Select a Screening Type ';
          this.reasons$.subscribe(d => {
            this.alist2 = d;//.map(x=> x.activity);
            if (this.alist2.length <= 0) {
              this.alist2.push('*ASSESSMENT');
              this.notProceedGroup.patchValue({ caseCategory: '*ASSESSMENT', reasonCode: '*ASSESSMENT', referralType: '*ASSESSMENT', referralCode: '*ASSESSMENT', referralSource: '*ASSESSMENT' });
              this.cd.detectChanges();
              return;
            }
            this.ViewList = true;
            this.alist2_original = this.alist2;
            this.cd.detectChanges();
            this.loading2 = false;

          });
          break;
          case 'AdminCode':
            this.listTitle = 'Approved Service ';
            
            let sql=` SELECT [Title] as activity FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ADMISSION') AND [MinorGroup] IN ('OTHER', 'REVIEW') AND (EndDate Is Null OR EndDate >= convert(varchar,getDate())) ORDER BY [Title]`
            this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe$)).subscribe(d => {
             this.alist2 = d.map(x =>  x.activity );
              //{
            //     return {
            //       value: x.activity,
            //       checked: false
  
            //     }
            //}
             
              if (this.alist2.length <= 0) {
                this.alist2.push('ADMISSION');
                this.adminGroup.patchValue({ caseCategory: 'ADMISSION', serviceType: 'ADMISSION', referralType: 'ADMISSION', referralCode: 'ADMISSION', referralSource: 'ADMISSION' });
                this.cd.detectChanges();
                
                return;
              }
             
              this.ViewList = true;
              this.alist2_original = this.alist2;
              this.cd.detectChanges();
              this.loading2 = false;
  
            });
            break;
        default:
          this.datalist = [];
      }
    }, 500);



  }

  selectService(data: any, i: number) {
    this.HighLightRow2 = i;
    this.selectedRowIndex = i;
    this.activeRowData = data;
  }
  AllocateService(data: any, i: number) {
    this.HighLightRow2 = i;
    this.selectedRowIndex = i;
    this.activeRowData = data;

    switch (this.ListType) {
      case 'referralSource':
        this.referInGroup.patchValue({ referralSource: data, serviceType:data });
        this.referOnGroup.patchValue({ referralSource: data, serviceType:data });
        break;
      case 'referralCode':
        this.referInGroup.patchValue({ referralCode: data });
        this.referOnGroup.patchValue({ referralCode: data });
        break;
      case 'ReasonCode':
        this.notProceedGroup.patchValue({ caseCategory: 'NOT PROCEEDING', reasonCode: data, referralType: data, referralCode: data, referralSource: data });
        break;
      case 'Assesment':
        this.assessGroup.patchValue({ caseCategory: 'SCREEN/ASSESS', serviceType: data, referralType: data, referralCode: data, referralSource: data });
        break;
      case 'AdminCode':
       
          this.adminGroup.patchValue({ caseCategory: 'ADMIN', serviceType: data, referralType: data, referralCode: data, referralSource: data });
          break;
      
      default: {

      }
    }

    this.ViewList = false;
  }

  onTextChangeEvent(event: any, listTye: any) {
    // console.log(this.txtSearch);
    let value = this.txtSearch.toUpperCase();

    if (listTye == 'ReferralSource') {
      this.alist2 = this.alist2_original.filter(element => element.includes(value));

    } else if (listTye == 'referralCode') {
      this.alist2 = this.alist2_original.filter(element => element.includes(value));

    }
  }
  logs(event: any) {

  }

  AllocateReferraltype() {
    this.ViewRefferaltypeList = false;
    let selected: any;
    selected = this.alist2.filter(x => x.checked).map(x => x.value).join(',');
    this.SelectedReferralTypes = this.alist2.filter(x => x.checked).map(x => x.value)

    if (this.AllServices) {

      selected = this.alist2.map(x => x.value).join(',');
      this.SelectedReferralTypes = this.alist2.map(x => x.value)
    }
    this.referInGroup.patchValue({ referralType: selected });
    this.referOnGroup.patchValue({ referralType: selected });
    if (this.option == RECIPIENT_OPTION.ADMIN ){
      let data = selected;
      this.adminGroup.patchValue({ caseCategory: 'ADMISSION', serviceType: data, referralType: data, referralCode: data, referralSource: data });
     
    }else  if (this.option == RECIPIENT_OPTION.ADMIT ){
    
      if (this.ListType== 'serviceType'){
         var programs: Array<any> = this.admitGroup.get('programs').value
       let prog=this.selectedProgram; // programs.find(x=>x.program==this.selectedProgram).program
        
        prog.patchValue({ services: selected });
        this.admitGroup.patchValue({ caseCategory: 'ADMISSION', serviceType : selected});
      }
    }

  }



  viewMultipleStaff(event:any) {
    if (!event) return;
    if (this.brnachList.length <= 0)
      this.listS.getlistbranches().pipe(takeUntil(this.unsubscribe$)).subscribe(d => {
        this.brnachList = d.map(d => {
          return {
            checked: false,
            value: d
          }

        });
        this.cd.detectChanges();
      });

    this.ViewStaffList = true;

  }

  SearchStaff() {
   
    this.listS.getUsersList().pipe(takeUntil(this.unsubscribe$)).subscribe(d => {
      this.staffList_original = d.map(d => {
        return {
          checked: false,
          name: d.name,
          branch: d.branch
        }

      });
      if (!this.IncludeNonBranch) {
        let branches = this.brnachList.filter(d => d.checked == true).map(x => x.value);

        this.staffList = this.staffList_original
        .filter(x => x.branch !== '' && x.branch !== null)
        .filter(x => branches.some(branch => x.branch.includes(branch)));
      
      } else {
        this.staffList = this.staffList_original
      }
      this.cd.detectChanges();
    });
  }
  selectAll(list: any, status: boolean) {
    list = list.filter(x => x.checked = status);
   
  }
  selectAllService(list: any, status: boolean) {
    status = this.SelectAllServices; // Use status from toggle variable

    let programs = this.suspendGroup.get('programs') as FormArray;

    // Update all checkboxes in the FormArray
    let newData = programs.value.map(x => ({
        ...x,
        checked: status  // Set checkbox status
    }));

    // Patch updated values back to the form
    this.suspendGroup.get('programs').patchValue(newData);

    // Refresh UI
  
   this.selectedProgramChange(newData[0])
    // Toggle select all state
    this.SelectAllServices = !this.SelectAllServices;
    this.cd.detectChanges();
}
showReminderError:boolean;
showInvalidDateError:boolean;

checkReminderValidation() {
  const reminderTo = this.globalFormGroup.get('reminderTo')?.value;
  const reminderDate = this.globalFormGroup.get('reminderDate')?.value;
  
  this.showReminderError = !!reminderTo && !reminderDate;
  this.showInvalidDateError = false;
  if (this.globalFormGroup.get('reminderDate')?.value){
    let today = new Date();
    if (this.globalFormGroup.get('reminderDate')?.value<today) {
      //this.globalS.eToast('Invalid Reminder Date', 'Please enter a valid Reminder Date greater than today');
      this.showInvalidDateError=true;
  }
}
 
}
disabledDate = (current: Date): boolean => {
  // Get today's date with time set to 00:00:00
  const today = new Date(new Date().setHours(0, 0, 0, 0));
  return current && current < today;
};

AllocateReminderStaff(){
  this.globalFormGroup.patchValue({emailNotif:this.staffList.filter(x=>x.checked==true).map(x=>x.name).join(';')})
  this.globalS.emailaddress = this.staffList.filter(x=>x.checked==true).map(x=>x.name);
  this.ViewStaffList=false;
}

staffdischargeList:Array<any>=[];
affectedStaffList:Array<any>=[];
viewStaffdischargeList:boolean;

showAffectedStaff(){

  this.loading2=true;
  this.listS.getaffectedStaff(this.user.code).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
    this.affectedStaffList =data;
    this.loading2=false;
    this.cd.detectChanges();
  })
}

showStaff(){

  let programs =  this.checkedPrograms.map(x=>x.program).join(',') ;
  this.listS.getDischargeStaff({code:this.user.code, program:programs}).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
    this.staffdischargeList =data;
    this.viewStaffdischargeList=true;
    this.cd.detectChanges();
  })
}
loanList:Array<any>=[];
viewStaffLoanList:boolean;

showLoans(){
  let programs =  this.checkedPrograms.map(x=>x.program).join(',') ;
  this.listS.getDischargeLoans({id:this.user.id, program:programs}).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
    this.loanList =data;
    this.viewStaffLoanList=true;
    this.cd.detectChanges();
  })
}
}