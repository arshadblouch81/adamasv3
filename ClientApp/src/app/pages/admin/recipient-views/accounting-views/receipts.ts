import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService, AccountReceiptDto, AccountAdjustmentDto } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { milliseconds } from 'date-fns';
import { DomSanitizer } from '@angular/platform-browser';
import { ContextMenuComponent } from 'ngx-contextmenu';

import { NzMessageService } from 'ng-zorro-antd/message';
@Component({
    selector: '',
    templateUrl: './receipts.html',
    styles: [`
        .controls button{
            margin-right: 5px;
        }
        textarea{
            height:5rem;
        }

        label{
            font-family: 'Segoe UI';
            font-size: 14px;
            margin-left:5px;
            
        }
    .row{
        margin:5px;
    }
    .box{
        
        padding:5px;
        border: 1px solid #d9d9d9;
        border-radius: 5px;
        margin-bottom: 5px;
    }
.btnMenu{
    background-color: #002060;
    color: white;  
    border: 1px solid #002060 !important;
    margin: 3px;    
    font-family : 'Segoe UI';
    font-size: 12px; 
    text-transform: uppercase;
    border-radius: 5px;
    height: 40px;
}
    .mrgn-top{        
        margin-top: 10px;
    }
    .hide {
      display: none;
    }
    :host ::ng-deep   .ant-select-dropdown  {
            background-color: #F18805 !important;
            }

    :host ::ng-deep  .nz-menu ul {
            background-color: #F18805 !important;
            }
         
            :host ::ng-deep  .custom-dropdown-menu {
                background-color: #f0f0f0;
                border: 1px solid #d9d9d9;
                border-radius: 4px;
                }

                :host ::ng-deep  .custom-dropdown-menu li {
                color: #F18805;
                padding: 10px;
                }

                :host ::ng-deep  .custom-dropdown-menu li:hover {
                background-color: #e6f7ff;
                color: #1890ff;
                }

                .checked-class {
                    background-color: #F18805;
                    }

                    .unchecked-class {
                    background-color: #fff;
                    }


   
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class Receipts implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    // @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    dateFormat: string = "dd/MM/yyyy";
    inputForm: FormGroup;
    recipientGroup: FormGroup;
    adjustmentGroup: FormGroup;
    invoiceHeader:Array<any> = [];
    tableData: Array<any> = [];
    includeInactive:boolean=false;
    title='Are you sure to record receipt'
    receiptModal: number;
    showSubMenu: boolean;
    modalOpen2:boolean;
    amount = 0;
    amountLeft=0;
    favorValue:any;
    formatterDollar = (value: number): string => `$ ${value}`;
    parserDollar = (value: string): string => value.replace('$ ', '');
    checked:boolean;
    
    setOfCheckedId = new Set<number>();
    branches: Array<any> = [];
    programs: Array<any> = [];
    sources: Array<any> = ['N/A', 'PERSONAL CONTRIBUTION', '3RD PARTY CONTRIBUTION', 'GOVERNMENT CONTRIBUTION'];
    paymenttypes: Array<any> = ['CHEQUE', 'CASH', 'CREDIT CARD', 'EFTPOS','DIRECT DEBIT','DIRECT CREDIT','OTHER'];
    types: Array<any> = ['RECEIPT', 'ADJUSTMENT']
    activeRowData: any;
    selectedRowIndex: number;
    showFilters: boolean = false;
    filterClicked: boolean = false;
    filters: any;
    CurrentBalance:number=0;
    bank:boolean;

    LIMIT_TABS: Array<any> = ['CARE DOMAIN', 'CREATOR', 'DISCIPLINE', 'PROGRAM'];

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS: PrintService,
        private modalService: NzModalService,
        private sanitizer:DomSanitizer,
        private cd: ChangeDetectorRef,
        private nzMessageService: NzMessageService
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'accounting')) {

                if ('code' in data) {
                    this.user = data;
                    this.search(this.user);
                }
            }
        });

        
    }
onCheckboxChange(value: boolean): void {
    console.log('Checkbox value:', value);
  }
    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        console.log(this.user);
        

       
        this.buildForm();
        this.listS.getrecipientprograms(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {this.programs = data
            if (this.programs.length > 0) {
                this.recipientGroup.patchValue({ programs: this.programs[0].value });
              }
        });
        this.listS.getlistbranches().subscribe(data => {this.branches = data
            if (this.branches.length > 0) {
                this.recipientGroup.patchValue({ branch: this.branches[0].value });
              }
        });

        this.search(this.user);
    }

    changeProgram(event: any) {
        if (this.includeInactive){
            let sql=`select distinct Program FROM RecipientPrograms WHERE PersonID = 'T0100004652'`
            this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.programs = data
                if (this.programs.length > 0) {
                    this.recipientGroup.patchValue({ programs: this.programs[0].value });
                  }
                });

        }else{
            this.listS.getrecipientprograms(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {this.programs = data
                if (this.programs.length > 0) {
                    this.recipientGroup.patchValue({ programs: this.programs[0].value });
                  }
            });
            
        }
    }
    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }
    getPermisson(index: number) {
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index - 1);
    }
    getStaffPermisson(index: number) {
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index - 1);
    }
    // confirm(view: number, data: any): void {

    //     if (view == 1) {
    //         data = this.activeRowData;
    //         this.delete(this.selectedRowIndex);
    //     }

    // }


    search(user: any = this.user) {
        this.cd.reattach();
        this.loading = true;

        this.listS.getaccountingreceipts(user.code).subscribe(plans => {
            this.tableData = plans;
            this.loading = false;
            this.cd.markForCheck();
        });
        this.listS.getinvoiceheader(this.user.code).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.invoiceHeader = data;
            this.loading = false;
           
        });
        
    }

    buildForm() {
        let today= new Date();
        this.recipientGroup = this.formBuilder.group({
            branch: [null,[Validators.required]],
            source: [this.sources[0],[Validators.required]],
            programs: [null,[Validators.required]],
            type: [null,[Validators.required]],
            paymentType: [this.paymenttypes[0],[Validators.required]],
            date: today,
            amount: 0,
            amountLeft:0,
            notes: '',
            bankAccountNo:'',
            bankName:'',
            bankBranch:'',
            drawer:'',
            accountName :'',
            bank: false,
            includeInactive: false,
            currentBalance:0,
            
        });

        this.recipientGroup.get('includeInactive')?.valueChanges.subscribe(value => {
            this.includeInactive = value;
          });

          this.recipientGroup.get('bank')?.valueChanges.subscribe(value => {
            this.bank = value;
          });
    }
    
    save(n:number) {

       
        let input = this.recipientGroup.value;
        input.amount = -this.amount;
        input.accountName = this.user.code;

        if ( this.receiptModal==1){     
            input.type = 'RECEIPT';
        }else{     
            input.type = 'ADJUSTMENT';
        }

        this .timeS.postaccountingreceipt(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.loading = false;
            this.modalOpen = false;
            if (n==2)
                this.ApplySelected(data);
            else if (n==3)
                this.AutoApply();
            this.search(this.user);
            this.globalS.sToast('Success', 'Receipt Recorded');
        });

        

    }
    showEditModal(index: number) {

    }

    delete(index: number) {

    }

    handleCancel() {
        this.modalOpen = false;

    }

    handleOk() {

    }
    changebank(event: any){
      console.log(event); 
    }
    
    
      showConfirm(n:number): void {
        //var deleteRoster = new this.deleteRoster();
        this.modalService.confirm({
          nzTitle: 'Confirm',
          nzContent: `Yu have selected to record a RECEIPT of ${this.amount} to apply this on an account\n meaning it will not apply to any specific specific invoice/s\nIs this correct, Are you sure, you want to continue`,
          nzOkText: 'Yes',
          nzCancelText: 'No',
          nzOnOk: () =>
          new Promise((resolve,reject) => {
           
            setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
        
            this.save(n);
          

          }).catch(() => console.log('Oops errors!'))
    
          
        });
      }


    updateCheckedSet(id: number, checked: boolean): void {
        if (checked) {
          this.setOfCheckedId.add(id);
        } else {
          this.setOfCheckedId.delete(id);
        }
        console.log(this.setOfCheckedId)
      }
    onItemChecked(id: number, checked: boolean): void {
        this.updateCheckedSet(id, checked);
   
      }

    showAddModal() {
        this.addOREdit = 1;
        this.modalOpen = true;
    }

    showReceiptModal(event: any, index: number) {
        this.addOREdit = 1;
        this.modalOpen = true;
        this.receiptModal = index;
        if (index==1){
            this.populateDropdownReceipt();
            this.recipientGroup.patchValue({type: 'RECEIPT', amount:0, amountLeft:0});
        }else{
            this.populateDropdownAdjustment();
            this.recipientGroup.patchValue({type: 'ADJUSTMENT', amount:0, amountLeft:0});
        }


    }
    populateDropdownReceipt(): void {
        this.recipientGroup.reset();

        this.recipientGroup.patchValue({
            type: 'RECEIPT'
        });

       this.recipientGroup.controls['type'].disable();

       
    }

    populateDropdownAdjustment(): void {
        this.populateDropdownReceipt();

        this.recipientGroup.patchValue({
            type: 'ADJUSTMENT'
        });

     this.recipientGroup.controls['type'].disable();
    }
    Refresh() {
        this.search(this.user);
    }
    print() {

    }
    generatePdf(){
        var Title;            
      //console.log(this.dataSet)
        
          Title = "Receipts";
          
                  
            
                const data = {
                    "template": { "_id": "6BoMc2ovxVVPExC6" },
                    "options": {
                        "reports": { "save": false },                        
                        "sql": this.tableData,                        
                        "userid": this.tocken.user,
                        "txtTitle":  Title,                      
                    }
                }              
                this.loading = true;           
                        
                this.drawerVisible = true;
                this.printS.printControl(data).subscribe((blob: any) => {
                            this.pdfTitle = Title+".pdf"
                            this.drawerVisible = true;                   
                            let _blob: Blob = blob;
                            let fileURL = URL.createObjectURL(_blob);
                            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                            this.loading = false; 
                            this.cd.detectChanges();                      
                        }, err => {
                            console.log(err);
                            this.loading = false;
                            this.modalService.error({
                                nzTitle: 'TRACCS',
                                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                nzOnOk: () => {
                                    this.drawerVisible = false;
                                },
                            });
                });                            
                return;        
               
                 
    }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
    close() {
        this.router.navigate(['/admin/recipient/personal']);
    }

    filterChange(data: any) {

        this.filters = data;
        this.search(this.user);
    }
    Filters() {
        this.showFilters = !this.showFilters;
        this.filterClicked = true;
        // this.search();
    }
    onAccount(){

    }
    ApplySelected(ReceiptNo:string){
        let selectedReceipt = this.setOfCheckedId; //this.invoiceHeader.filter(x=>x.sqlid in(this.setOfCheckedId)).map(x=>x.number);

        this .timeS.postaccountingreceiptUpdate(ReceiptNo,selectedReceipt).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.loading = false;
            this.modalOpen = false;
            this.search(this.user);
            this.globalS.sToast('Success', 'Receipt Recorded');
        });

    }
AutoApply(){

}

}