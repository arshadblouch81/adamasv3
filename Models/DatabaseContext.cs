using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System;
using Adamas.Models.Tables;
using QuoteMergeFields = Adamas.Models.Modules.QuoteMergeFields;
using LastIdDto = Adamas.Models.Modules.LastIdDto;
using AdamasV3.Helper;
using AdamasV3.Models.Tables;
using Microsoft.Extensions.Logging;

namespace Adamas.Models
{
    public class DatabaseContext : DbContext
    {
        private readonly IConfiguration _config;
        public DatabaseContext(IConfiguration config, DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            this._config = config;
        }

        public SqlConnection GetConnection()
        {
            return new SqlConnection(this._config["ConnectionStrings:Production"]);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (DbManager.DbName != null)
            {
                var dbName = DbManager.DbName;
                var connString = DbManager.ConnString;
                var maxRetryCount=5;
                var maxRetryDelaySeconds=30;

                var initString = DbManager.GetDbConnectionString(dbName, connString);
                initString = initString + @$"Database={dbName}";
                optionsBuilder.UseSqlServer(initString, optionsBuilder => optionsBuilder.EnableRetryOnFailure(maxRetryCount, TimeSpan.FromSeconds(maxRetryDelaySeconds), null))
                .EnableSensitiveDataLogging()
                .LogTo(Console.WriteLine, LogLevel.Error);
            }
           

        }


        [DbFunction("RTF2Text", "dbo")]
        public static string RTF2Text(string rtf)
        {
            throw new Exception(); // this code doesn't get executed; the call is passed through to the database function
        }

        //[DbFunction("RTF2Text")]
        //public string RTF2Text(string rtf) => throw new NotSupportedException();


        // Keyless Entities
        public DbSet<QuoteMergeFields> QuoteMergeFields { get; set; }
        // END Keyless Entities

        public DbSet<Staff> Staff { get; set; }
        public DbSet<History> History { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<Recipients> Recipients { get; set; }
        public DbSet<Roster> Roster { get; set; }
        public DbSet<HumanResources> HumanResources { get; set; }
        public DbSet<HumanResourceTypes> HumanResourceTypes { get; set; }
        public DbSet<ServiceOverview> ServiceOverview { get; set; }
        public DbSet<NamesAndAddresses> NamesAndAddresses { get; set; }
        public DbSet<PhoneFaxOther> PhoneFaxOther { get; set; }
        public DbSet<DataDomains> DataDomains { get; set; }
        public DbSet<Pcodes> Pcodes { get; set; }
        public DbSet<PublicHoliday> PublicHoliday { get; set; }
        public DbSet<RecipientPrograms> RecipientPrograms { get; set; }
        public DbSet<Medias> Media { get; set; }
        public DbSet<Documents> Documents { get; set; }
        public DbSet<Oni> Oni { get; set; }
        public DbSet<IM_Master> IM_Master { get; set; }
        public DbSet<IM_InvolvedStaff> IM_InvolvedStaff { get; set; }
        public DbSet<IM_NotifiableStaff> IM_NotifiableStaff { get; set; }        
        public DbSet<IM_DistributionLists> IM_DistributionLists { get; set; }
        public DbSet<Registration> Registration { get; set; }
        public DbSet<Qte_Hdr> Qte_Hdr { get; set; }
        public DbSet<Qte_Lne> Qte_Lne { get; set; }
        public DbSet<DOC_Associations> DOC_Associations { get; set; }
        public DbSet<ItemTypes> ItemTypes { get; set; }
        public DbSet<AwardPos> AwardPos { get; set; }
        public DbSet<SysTable> SysTable { get; set; }
        public DbSet<InvoiceHeader> InvoiceHeader { get; set; }
        public DbSet<LoggedUsers> LoggedUsers { get; set; }
        public DbSet<WebPortalLog> WebPortalLog { get; set; }
        public DbSet<DSSXtra> DSSXtra { get; set; }
        public DbSet<ONIActionPlan> ONIActionPlan { get; set; }
         public DbSet<Roster_TaskList> Roster_TaskList { get; set; }
        
    }
}