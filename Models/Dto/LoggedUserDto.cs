﻿namespace AdamasV3.Models.Dto
{
    public class LoggedUserDto
    {
        public string Token { get; set; }
        public string BrowserName { get; set; }
        public string User { get; set; }
    }
}
