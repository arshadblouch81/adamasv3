using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.Interfaces;
using AdamasV3.Models.Dto;
using Microsoft.Extensions.Configuration;

using StaffTable = Adamas.Models.Tables.Staff;
using Staff = Adamas.Models.Tables.Staff;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using MailKit;
using MailKit.Security;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using Repositories;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER,SUPPORT WORKER")]
    public class StaffController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private IEmailService _emailService;
        private readonly IEmailConfiguration  _emailConfiguration;
        private IUploadService _uploadService;
        private ILoginService _loginService;
        private IUploadRepository uploadRepo;

        public StaffController(
            DatabaseContext context, 
            IConfiguration config, 
            IGeneralSetting setting,
            IEmailService emailService,
            IUploadService uploadService,
            ILoginService loginService,
            IUploadRepository uploadRepo,
            IEmailConfiguration  emailConfiguration)
        {
            _context = context;
            _emailService = emailService;
            _emailConfiguration = emailConfiguration;
            _uploadService = uploadService;
            _loginService = loginService;
            this.uploadRepo = uploadRepo;

            GenSettings = setting;
        }

        [HttpGet,Route("profile/{code}")]
        public async Task<IActionResult> GetStaff(string code){
            try
            {
                var staff = await _context.Staff.Where(x => x.AccountNo == code).FirstOrDefaultAsync();
  
                if(staff == null)
                    return NotFound();

                return Ok(staff);

            } catch(Exception ex){
                return BadRequest(ex);
            }
        }

        [HttpGet("images")]
        public async Task<IActionResult> GetImage(FileProperties props){                        

            if (System.IO.File.Exists(props.Directory))
            {
                FileAttributes attr = System.IO.File.GetAttributes(props.Directory);

                if (!attr.HasFlag(FileAttributes.Directory)){
                    Byte[] b = await Task.Run(() =>  System.IO.File.ReadAllBytes(props.Directory));

                    return Ok(new{
                        IsValid = true,
                        Image = Convert.ToBase64String(b)
                    });
                }
            }
            
            return Ok(new{
                IsValid = false,
                Image = ""
            });
        }

        [HttpPost, Route("profile")]
        public async Task<IActionResult> PostStaff([FromBody] JObject json){

            var user = await _loginService.GetCurrentUser();

            string _prodId = string.Empty;
            string finalCarerId = string.Empty;

            dynamic jsonObject = json;

            JObject _staff = jsonObject.Staff;
            JArray _address = jsonObject.NamesAndAddresses as JArray;
            JArray _contact = jsonObject.PhoneFaxOther as JArray;
            JArray _competencies = jsonObject.Competencies as JArray;
            JArray _otherContacts = jsonObject.OtherContacts as JArray;
            JArray _documents = jsonObject.Documents as JArray;
            JArray _reminders = jsonObject.Reminders as JArray;
            JObject _notes = jsonObject.Notes;

            var staff = _staff.ToObject<Staff>();
            var notes = _notes.ToObject<NotesDto>();
            var addresses = _address.ToObject<List<NamesAndAddresses>>();
            var contacts = _contact.ToObject<List<PhoneFaxOther>>();
            var competencies = _competencies.ToObject<List<string>>();
            var documents = _documents.ToObject<List<string>>();
            var otherContacts = _otherContacts.ToObject<List<ContactKinDetails>>();
            var reminders = _reminders.ToObject<List<FollowupsDto>>();

            // GENERATES UniqueID AND returns an object with neccessary information
            var rc = await GenSettings.GetAccountNameGenerator("staff");

            SqlTransaction transaction = null;

            try
            {
                
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                

                    using(SqlCommand cmd = new SqlCommand(@"UPDATE SysTable SET CarerID = @carerID", (SqlConnection) conn, transaction)){
                        cmd.Parameters.AddWithValue("@carerId", rc.CarerId);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    using(SqlCommand cmd = new SqlCommand(@"INSERT INTO Staff([UniqueId], AccountNo,FirstName,LastName,Gender,Pan_Manager,StaffGroup,Stf_Department,Category,CommencementDate,DOB,[PreferredName])
                            VALUES(@uniqueId,@accountno,@firstname,@lastname,@gender,@manager,@staffgroup,@department,@category,@cdate,@bdate,@preferred)", (SqlConnection) conn,transaction))
                    {
                        cmd.Parameters.AddWithValue("@uniqueId", rc.UniqueId);
                        cmd.Parameters.AddWithValue("@accountno", !string.IsNullOrEmpty(staff.AccountNo) ? (staff.AccountNo).ToUpper() :"");
                        cmd.Parameters.AddWithValue("@firstname", staff.FirstName);
                        cmd.Parameters.AddWithValue("@preferred", staff.PreferredName);
                        cmd.Parameters.AddWithValue("@lastname", staff.LastName);
                        cmd.Parameters.AddWithValue("@gender", staff.Gender);
                        cmd.Parameters.AddWithValue("@manager", staff.Pan_Manager);
                        cmd.Parameters.AddWithValue("@staffgroup", staff.StaffGroup);
                        cmd.Parameters.AddWithValue("@department", staff.Stf_Department);
                        cmd.Parameters.AddWithValue("@category", staff.Category);
                        cmd.Parameters.AddWithValue("@cdate", (object)staff.CommencementDate ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@bdate", ((object)staff.DOB) ?? DBNull.Value);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    foreach(var address in addresses){
                        using(SqlCommand cmd = new SqlCommand(@"INSERT INTO NamesAndAddresses(PersonID, [Type], [Description],[Address1],[Suburb],[PostCode],[PrimaryAddress]) VALUES (@personid, 'PERSONALADDRESS', @description,@address,@suburb,@postcode,@primary)", (SqlConnection) conn,transaction)){
                            cmd.Parameters.AddWithValue("@personid", rc.UniqueId);
                            cmd.Parameters.AddWithValue("@description", address.Description);
                            cmd.Parameters.AddWithValue("@address", address.Address1);
                            cmd.Parameters.AddWithValue("@suburb", address.Suburb);
                            cmd.Parameters.AddWithValue("@postcode", address.PostCode);

                            if(addresses.Count == 1)
                            {
                                cmd.Parameters.AddWithValue("@primary", true);
                            }
                            else
                            {
                                // If more than two contacts but no primary let first entry be the primary contact
                                if (addresses.Count > 1 &&
                                        addresses.Count(x => x.PrimaryAddress) == 0 &&
                                            addresses.First() == address)
                                {
                                    cmd.Parameters.AddWithValue("@primary", true);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@primary", address.PrimaryAddress);
                                }
                            }

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    // Contacts
                    foreach(var contact in contacts){
                        using(SqlCommand cmd = new SqlCommand(@"INSERT INTO PhoneFaxOther([PersonID], [Type], [Detail],[PrimaryPhone]) VALUES (@personid, @type,@detail,@primary)", (SqlConnection) conn,transaction)){
                            cmd.Parameters.AddWithValue("@personid", rc.UniqueId);
                            cmd.Parameters.AddWithValue("@type", contact.Type);
                            cmd.Parameters.AddWithValue("@detail", contact.Detail);

                            if (contacts.Count == 1)
                            {
                                cmd.Parameters.AddWithValue("@primary", true);
                            }
                            else
                            {
                                // If more than two contacts but no primary let first entry be the primary contact
                                if (contacts.Count > 1 &&
                                        contacts.Count(x => x.PrimaryPhone) == 0 &&
                                            contacts.First() == contact)
                                {
                                    cmd.Parameters.AddWithValue("@primary", true);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@primary", contact.PrimaryPhone);
                                }
                            }

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    // Notes
                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HISTORY(DetailDate, PersonId, Creator, Detail, PrivateFlag, ExtraDetail1, ExtraDetail2, whocode)
                                                            VALUES(GETDATE(), @pid, @oid, @note, 0, @notetype, @notetype, @rcode)", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@pid", rc.UniqueId);
                        cmd.Parameters.AddWithValue("@oid", user.StaffCode);
                        cmd.Parameters.AddWithValue("@note", notes.Notes ?? "");

                        cmd.Parameters.AddWithValue("@notetype", notes.Type ?? "");
                        cmd.Parameters.AddWithValue("@rcode", user.StaffCode);


                        await cmd.ExecuteNonQueryAsync();
                    }


                    // Competencies
                    foreach (var competency in competencies)
                    {
                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HumanResources([PersonID],[Group], [Type], Name) VALUES (@personid,'STAFFATTRIBUTE', 'STAFFATTRIBUTE',@name)", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@personid", rc.UniqueId);
                            cmd.Parameters.AddWithValue("@name", competency);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    //Other Contacts
                    foreach (var address in otherContacts)
                    {

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO humanresources ([PersonID],[Group],[state],[type],[subtype],[name], address1, address2, suburb, postcode, phone1, phone2, fax, email, notes, equipmentcode, mobile, creator) 
	                VALUES(@accountName, @group, @state, @type,@subtype,@name,@address1, @address2, @suburb, @postcode, @phone1, @phone2, @fax,@email,@notes, @ecode, @mobile, @creator)", (SqlConnection)conn, transaction))
                        {

                            var code = Regex.Match(address.Suburb, @"\d+", RegexOptions.IgnoreCase).Value;
                            var suburb = Regex.Match(address.Suburb, @"(?i)^[a-z]+", RegexOptions.IgnoreCase).Value;
                            var state = address.Suburb.Split(",").Length > 1 ? address.Suburb.Split(",")[1] : "";

                            cmd.Parameters.AddWithValue("@group", "CONTACT");
                            cmd.Parameters.AddWithValue("@state", state ?? "");
                            cmd.Parameters.AddWithValue("@type", address.Group ?? "");
                            cmd.Parameters.AddWithValue("@subtype", address.Type ?? "");

                            cmd.Parameters.AddWithValue("@name", address.Name ?? "");
                            cmd.Parameters.AddWithValue("@address1", address.Address1 ?? "");
                            cmd.Parameters.AddWithValue("@address2", address.Address2 ?? "");
                            cmd.Parameters.AddWithValue("@suburb", suburb ?? "");
                            cmd.Parameters.AddWithValue("@postcode", code ?? "");
                            cmd.Parameters.AddWithValue("@phone1", address.Phone1 ?? "");
                            cmd.Parameters.AddWithValue("@phone2", address.Phone2 ?? "");
                            cmd.Parameters.AddWithValue("@fax", address.Fax ?? "");
                            cmd.Parameters.AddWithValue("@email", address.Email ?? "");
                            cmd.Parameters.AddWithValue("@notes", address.Notes ?? "");
                            cmd.Parameters.AddWithValue("@ecode", address.Ecode ?? "");
                            cmd.Parameters.AddWithValue("@mobile", address.Mobile ?? "");
                            cmd.Parameters.AddWithValue("@creator", user.User);
                            cmd.Parameters.AddWithValue("@accountName", rc.UniqueId);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

        
                    foreach (var reminder in reminders)
                    {

                        using (SqlCommand cmd = new SqlCommand(@"INSERT INTO humanresources ([PersonID],[Notes],[Group],[Type],[Name], [Date1], [Date2]) VALUES(@personId, @notes, @group, @type,@name,@date1,@date2)", (SqlConnection)conn, transaction))
                        {
                            
                            Int32.TryParse(reminder.DateCounter, out int dateCounter);
                            var dateValue  = DateTime.Now.AddDays(Convert.ToDouble(dateCounter));

                            cmd.Parameters.AddWithValue("@personId", rc.UniqueId);
                            cmd.Parameters.AddWithValue("@notes", "");
                            cmd.Parameters.AddWithValue("@group", "STAFFALERT");
                            cmd.Parameters.AddWithValue("@type", "STAFFALERT");

                            cmd.Parameters.AddWithValue("@name", reminder.Label);
                            cmd.Parameters.AddWithValue("@date1", dateValue);
                            cmd.Parameters.AddWithValue("@date2", dateValue);

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }


                    transaction.Commit();
                    return Ok(new
                    {
                        UniqueId = rc.UniqueId
                    });

                }

              

            } catch(Exception ex){
                transaction.Rollback();
                return BadRequest(ex);
            }
      
           
        }

        [HttpPost("incident-application")]
        public async Task<IActionResult> PostLeaveApplication([FromBody] LeaveApplication leave){

            SqlTransaction transaction = null;
            try
            {
                int id = 0;
                using(var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using(SqlCommand cmd = new SqlCommand(@"INSERT INTO IM_Master ([PersonId], [Service], [Program], [Type], [PerpSpecify], [Location], [Setting], [LocationNotes], [DateReported], [Date], [Time], [Duration], ReportedBy, [Severity], [CurrentAssignee], [ShortDesc], [FullDesc], [TriggerShort], [Triggers], [InitialNotes], [OngoingNotes], [Status] ) 
                        VALUES( @personid, @service, @program, @type, @perpspecify, @location, @setting, @locationnotes, @datereported, @date, @time, @duration, @reportedby, @severity, @assignee, @shortdesc, @fulldesc, @triggershort, @triggers, @initnotes, @ongoingnotes,'OPEN');SELECT SCOPE_IDENTITY();", (SqlConnection) conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@personid", leave.PersonId);
                        cmd.Parameters.AddWithValue("@service", leave.Service);
                        cmd.Parameters.AddWithValue("@program", leave.Program);
                        cmd.Parameters.AddWithValue("@type", leave.Type);
                        cmd.Parameters.AddWithValue("@perpspecify", leave.PerpSpecify);
                        cmd.Parameters.AddWithValue("@location", leave.Location);
                        cmd.Parameters.AddWithValue("@setting", leave.Setting);
                        cmd.Parameters.AddWithValue("@locationnotes", leave.LocationNotes);
                        cmd.Parameters.AddWithValue("@datereported", leave.DateReported);
                        cmd.Parameters.AddWithValue("@date", leave.Date);
                        cmd.Parameters.AddWithValue("@time", leave.Time);
                        cmd.Parameters.AddWithValue("@duration", leave.Duration);
                        cmd.Parameters.AddWithValue("@reportedby", leave.ReportedBy);
                        cmd.Parameters.AddWithValue("@severity", leave.Severity);
                        cmd.Parameters.AddWithValue("@assignee", leave.CurrentAssignee);
                        cmd.Parameters.AddWithValue("@shortdesc", leave.ShortDesc);
                        cmd.Parameters.AddWithValue("@fulldesc", leave.FullDesc);
                        cmd.Parameters.AddWithValue("@triggershort", leave.TriggerShort);
                        cmd.Parameters.AddWithValue("@triggers", leave.Triggers);
                        cmd.Parameters.AddWithValue("@initnotes", leave.InitialNotes);
                        cmd.Parameters.AddWithValue("@ongoingnotes", leave.OngoingNotes);

                 
                        var modified = await cmd.ExecuteScalarAsync();
                        id = Convert.ToInt32(modified);
                       
                    }
                   

                    foreach(var staff in leave.Staffs)
                    {
                         using(SqlCommand cmd = new SqlCommand(@"INSERT INTO IM_InvolvedStaff([IM_Master#], [Staff#]) VALUES(@id,@staff)",(SqlConnection) conn, transaction)){
                            cmd.Parameters.AddWithValue("@id", id);
                            cmd.Parameters.AddWithValue("@staff", staff.AccountNo);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }                    

                    transaction.Commit();
                    return Ok(id > 0);


                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return BadRequest(ex);
            }
        }

        [HttpGet("leave-balances/{uniqueID}")]
        public async Task<IActionResult> GetLeaveBalances(string uniqueID)
        {
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using(SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, Name As [Leave Type Code], Address1 AS [Leave Description], Date1 As [Pro Rata Date], Address2 AS [Balance Hours] FROM HumanResources WHERE PersonID = @uniqueid
                     AND [Group] = 'LEAVEBALANCE' ORDER BY  RecordNumber DESC",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@uniqueid", uniqueID);
                    await conn.OpenAsync();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();
                        while(await rd.ReadAsync())
                        {
                            list.Add(new {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                LeaveCode = GenSettings.Filter(rd["Leave Type Code"]),
                                LeaveDescription = GenSettings.Filter(rd["Leave Description"]),
                                ProRataDate = GenSettings.Filter(rd["Pro Rata Date"]),
                                Hours = GenSettings.Filter(rd["Balance Hours"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("address/{id}")]
        public async Task<IActionResult> GetAddress(string id){
            var address = await _context.NamesAndAddresses.Where(x => x.PersonID == id)
                                                    .OrderByDescending(x => x.PrimaryAddress)
                                                    .ToListAsync();
            if(address == null)
                return NotFound();
            
            return Ok(address);
        }

        [HttpPost, Route("address/{id}")]
        public async Task<IActionResult> PostAddress([FromBody] List<NamesAndAddresses> addresses, string id){
            try
            {
                using(var db = _context)
                {
                    foreach(var address in addresses)
                    {
                        var _address = new NamesAndAddresses(){

                        };
                    }
                    return Ok(true);
                }
            } catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet, Route("contact/{id}")]
        public async Task<IActionResult> GetContacts(string id){
            var contacts = await _context.PhoneFaxOther.Where(x => x.PersonID == id)
                                                        .OrderByDescending(x => x.PrimaryPhone)
                                                        .ToListAsync();
            if(contacts == null)
                return NotFound();

            return Ok(contacts);
        }

        [HttpGet, Route("payperiod")]
        public async Task<IActionResult> GetPayPeriod(){            
            return Ok(await GenSettings.GetPayPeriod());
        }

        [HttpGet, Route("mobilefuturelimit/{name}")]    
        public async Task<IActionResult> GetMobileFutureLimit(string name)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using(SqlCommand cmd = new SqlCommand(@"SELECT  MobileFutureLimit  FROM UserInfo WHERE Name = @name", (SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@name", name);
                    await conn.OpenAsync();
 
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    int? dayLimit = 0;

                    try
                    {
                        if(await rd.ReadAsync())                    
                            dayLimit = GenSettings.Filter(rd["MobileFutureLimit"]) ?? 0;                    

                        return Ok(new {
                            dayLimit = dayLimit
                        });
                    } catch (Exception ex)
                    {
                        return BadRequest(new {
                            message = ex.ToString()
                        });
                    }
                }
            }
        }

        [HttpPost, Route("leave")]
        public async Task<IActionResult> PostLeave([FromBody] LeaveEntry entry)
        {
            using( var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string SqlCmd = "";
                string Email = "";
                string AccountNo = ""; 
                string UniqueID = "";
                string StaffEmail = "";

                await conn.OpenAsync();
                SqlTransaction sqlTrans = conn.BeginTransaction();

                if(entry.CoordinatorEmail.IsRecipient)
                    SqlCmd = "SELECT  AccountNo,dbo.getCoordinator_Email('RECIPIENTS',AccountNo) as CoordEmail,UniqueId FROM Recipients WHERE AccountNo = @uname";
                else
                    SqlCmd = "SELECT AccountNo,dbo.getCoordinator_Email('STAFF',AccountNo) as CoordEmail,UniqueId FROM Staff WHERE AccountNo = @uname";
                
                try {

                    using(SqlCommand cmd1 = new SqlCommand(SqlCmd,(SqlConnection) conn, sqlTrans))
                    {
                        cmd1.Parameters.AddWithValue("@uname", entry.CoordinatorEmail.AccountName);
                        SqlDataReader rd1 = await cmd1.ExecuteReaderAsync();
                        
                        if(await rd1.ReadAsync()){
                            Email = GenSettings.Filter(rd1["CoordEmail"]);
                            AccountNo = GenSettings.Filter(rd1["AccountNo"]);
                            UniqueID = GenSettings.Filter(rd1["UniqueId"]);
                        }
                        rd1.Close();
                    }

                    
                    using(SqlCommand cmd2 = new SqlCommand(@"SELECT Detail FROM PhoneFaxOther WHERE PersonID = @uniqueID AND Type = '<EMAIL>'",(SqlConnection) conn, sqlTrans))
                    {
                        cmd2.Parameters.AddWithValue("@uniqueID", UniqueID);
                        SqlDataReader rd2 = await cmd2.ExecuteReaderAsync();
                        
                        if(await rd2.ReadAsync()){
                            StaffEmail = GenSettings.Filter(rd2["Detail"]);
                        }
                        rd2.Close();
                    }                

                    // if(string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(_emailConfiguration.SmtpUsername)){                            
                    //     return BadRequest(new {
                    //         message = "Email Server Or Destination Mail is null"
                    //     });
                    // }
                    
                    using(SqlCommand cmd = new SqlCommand(@"INSERT INTO HUMANRESOURCES (PersonID,[TYPE],[Group],Date1,Date2,Notes,Address1,Name) VALUES( @PersonId, 'LEAVEAPP','LEAVEAPP', @StartDate, @EndDate, @Note, @Address, @LeaveType);SELECT SCOPE_IDENTITY();",(SqlConnection) conn ,sqlTrans))
                    {
                        cmd.Parameters.AddWithValue("@PersonId", entry.StaffCode);
                        cmd.Parameters.AddWithValue("@StartDate", entry.StartDate);
                        cmd.Parameters.AddWithValue("@EndDate", entry.EndDate);
                        cmd.Parameters.AddWithValue("@Note", entry.message.Notes);
                        cmd.Parameters.AddWithValue("@Address", entry.Address);
                        cmd.Parameters.AddWithValue("@LeaveType", entry.message.LeaveType);

                        var modified = await cmd.ExecuteScalarAsync();
                        var res = Convert.ToInt32(modified) > 0 ? true : false;
                        

                        var message = new MimeMessage();                        

                        message.From.Add(new MailboxAddress("BBCRI-Notifier", _emailConfiguration.SmtpOriginEmail));

                        if(!string.IsNullOrEmpty(_emailConfiguration.SmtpCCEmail)){
                            message.Cc.Add(new MailboxAddress("", _emailConfiguration.SmtpCCEmail));
                        }

                        if(!string.IsNullOrEmpty(_emailConfiguration.SmtpReplyTo)){
                            message.ReplyTo.Add(new MailboxAddress("", _emailConfiguration.SmtpReplyTo));
                        }

                        if(_emailConfiguration.SmtpEnableSampleMail){
                            message.To.Add(new MailboxAddress("", _emailConfiguration.SmtpRecipientEmail)); 
                        } else {
                            
                            if(!string.IsNullOrEmpty(Email)){
                                message.To.Add(new MailboxAddress("", Email));
                            }

                            if(!string.IsNullOrEmpty(StaffEmail)){
                                message.Cc.Add(new MailboxAddress("",StaffEmail));
                            }
                        }

                        message.Subject = entry.message.Subject;
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = 
                            string.Format("<div style='width: 400px; display: inline-block; padding: 0 1rem 1rem 1rem; border: 1px solid #dddddd; border-top: 2px solid #32bee6; border-radius: 2px; color: #3a3a3a;'> " +
                                "<h3 style='border-bottom: 1px solid #32bee6; padding: 5px 0'>{0}</h3> "+
                                "<p style='margin:0;'><b style='width: 6rem; display: inline-block;'>Leave Type:  </b> {2}</p>" + 
                                "<p style='margin:0;'><b style='width: 6rem; display: inline-block;'>Dates:</b> {1}</p>" +
                                "<p style='margin:0;'><b style='width: 6rem; display: inline-block;'>Notes: </b> {3}</p>" + 
                            "</div>", entry.message.Subject, entry.message.Content, entry.message.LeaveType, entry.message.Notes)
                        };
                        
                        var emailReq = new EmailOutput();
                        if(res){
                            emailReq = await _emailService.SendEmail(message);
                            if(emailReq.Status.IsSuccess){
                                sqlTrans.Commit();                            
                                if(_emailConfiguration.SmtpEnableSampleMail){
                                    return Ok(emailReq);
                                }
                                return Ok(new EmailOutput(){
                                    ReplyTo = "",
                                    CC = "",
                                    To = "",
                                    From = "",
                                    Status = new IsSuccessEmail() {
                                        IsSuccess = true
                                    }
                                });
                            }
                        }

                        // sqlTrans.Rollback();
                        // return BadRequest(new {
                        //     message = emailReq
                        // });
                    }
                } catch (Exception ex)
                {
                    // sqlTrans.Rollback();                    
                    // return BadRequest(new {
                    //     message = ex.ToString()
                    // });
                }
                sqlTrans.Commit();  
                return Ok(new EmailOutput(){
                                    ReplyTo = "",
                                    CC = "",
                                    To = "",
                                    From = "",
                                    Status = new IsSuccessEmail() {
                                        IsSuccess = true
                                    }
                                });
            }
        }


         [HttpPost, Route("claimvariation")]
         public async Task<IActionResult> PostClaimVariation([FromBody] ClaimVariation cv)
         {
            var recnum = "";
            recnum = await GenSettings.getValue("SELECT recnum FROM UserInfo WHERE Name='" + cv.ClaimedBy + "'");
            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    using (SqlCommand cmd = new SqlCommand(@"UPDATE Roster SET ClaimedStart = @start, ClaimedEnd = @end, ClaimedDate = @date, ClaimedBy = @by, Notes = @notes WHERE RecordNo = @recordNo ", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@start", Convert.ToDateTime(cv.ClaimedStart));
                        cmd.Parameters.AddWithValue("@end", Convert.ToDateTime(cv.ClaimedEnd));
                        cmd.Parameters.AddWithValue("@date", Convert.ToDateTime(cv.ClaimedDate));
                        cmd.Parameters.AddWithValue("@by", recnum);
                        cmd.Parameters.AddWithValue("@recordNo", cv.RecordNo);
                        cmd.Parameters.AddWithValue("@notes", cv.Notes ?? "");
                        //return Ok(true);
                        return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                    }
                }
            } catch(Exception ex)
            {
                return NotFound(new {
                    message = ex.ToString()
                });
            }
        }

        [HttpPost, Route("travelclaim")]
        public async Task<IActionResult> PostTravelClaim([FromBody] TravelClaim claim)
        {
            string RecordNo = claim.RecordNo; 
            string distance = claim.Distance;
            string Travel_Type = claim.TravelType;
            string Charge_Type = claim.ChargeType;
            string StartKm = claim.StartKm;
            string EndKM = claim.EndKm;
            string Notes = claim.Notes;

            double km = Convert.ToDouble(EndKM) - Convert.ToDouble(StartKm);         
            int charge = 0;
            string s_ServiceActivity = "";
            string s_RecipientCode = "";
            string s_NewBillingRate = "0";

            // string s_NewDatasetType = "";
            // string s_Status = "";
            // string s_NewBiller = "";
            // string s_NewBillingUnit = "";

            string s_Program = "";
            string SqlStmt = "";

 
            SqlStmt = "select [service type],[Client Code],program, dbo.GetBillingRate([Client code],[service type],[program]) as BillingRate from roster where RecordNo=" + RecordNo;

            using (var conn = _context.GetConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@SqlStmt, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rs_roster = cmd.ExecuteReader();
                    if (rs_roster.Read())
                    {
                        s_ServiceActivity = rs_roster["Service Type"].ToString();
                        s_RecipientCode = rs_roster["Client Code"].ToString();
                        s_Program = rs_roster["program"].ToString();
                        s_NewBillingRate = rs_roster["BillingRate"].ToString();
                        // _logger.LogWarning(SqlStmt);
                        // _logger.LogWarning(s_NewBillingRate);
                    }
                }
            }


            using (var conn = _context.GetConnection() as SqlConnection)
            {
                await conn.OpenAsync();
                SqlTransaction trans = conn.BeginTransaction();
                SqlCommand cmd = conn.CreateCommand();

                //GetBillingRate(ref s_RecipientCode, ref s_ServiceActivity, ref s_NewBillingRate, ref s_NewBillingUnit, ref s_NewDatasetType, s_Program, s_Status, s_NewBiller);
                switch (Travel_Type)
                {
                    case "TRAVEL WITHIN":
                        if (Charge_Type == "Chargeable")
                        {
                            charge = 1;
                            //", isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Type] AND PROCESSCLASSIFICATION = 'OUTPUT'),0) as Unit_Bill_Rate" & _
                            if (Convert.ToDouble(s_NewBillingRate) > 0)
                            {
                                SqlStmt = @"INSERT INTO Roster ([Creator],[Editer],[Client Code],[Carer Code],Program,[date],[Start Time],Duration,YearNo,MonthNo,DayNo,BlockNo,[Service Type],[Service Description],BillQty,CostQty,CostUnit,BillTo,StartKm,EndKM,KM, charge,Notes,[type],[status],[Date Entered],[Date Last Mod],[BillUnit],[Unit Pay Rate],[Unit Bill Rate]) SELECT *, isnull( (SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Description] AND PROCESSCLASSIFICATION = 'INPUT'),0) AS Unit_Pay_Rate, dbo.GetBillingRate([client code],[Service_Type],Program) AS Unit_Bill_Rate FROM (SELECT @user AS Creator,@user AS Editer,[Client Code], [Carer Code], Program, r.date, left(DATEADD(MINUTE,duration* 5,convert(TIME,[Start Time])),5) AS start_time, '0' AS duration, r.YearNo, r.MonthNo, r. DayNo, 0 AS BlockNo, isnull( (SELECT DefaultCHGTravelWithinActivity FROM Recipients WHERE AccountNo =r.[Client Code]) ,( (SELECT DefaultCHGTravelWithinActivity FROM humanresourcetypes WHERE [Group] ='PROGRAMS' AND name=r.Program))) AS [Service_Type], isnull( (SELECT DefaultCHGTravelWithinPayType FROM Staff WHERE AccountNo =r.[Carer Code]) ,( (SELECT DefaultCHGTravelWithinPayType FROM humanresourcetypes WHERE [Group] ='PROGRAMS' AND name=r.Program))) AS [Service_Description], @distance AS BillQty, @distance AS PayQty, 'SERVICE' AS CostUnit, (SELECT billto FROM Recipients WHERE AccountNo=r.[Client Code]) AS BillTo, @startKM AS StartKm, @endKM AS EndKM, @km AS km, @charge AS charge, @notes AS Notes, 9 AS TYPE, 1 AS status, getdate() AS DateEntered, getDate() AS DateModify, 'SERVICE' AS BillUnit FROM Roster r WHERE recordNo= @recordNo) AS travel_entry WHERE [Service_Type] IS NOT NULL AND [Service_Description] IS NOT NULL AND program IS NOT NULL AND [Service_Type]<>'' AND [Service_Description]<>'' AND program<>''";
                                cmd.Parameters.AddWithValue("@startKM", StartKm);
                                cmd.Parameters.AddWithValue("@endKM", EndKM);
                                cmd.Parameters.AddWithValue("@km", km);
                                cmd.Parameters.AddWithValue("@user", claim.User);
                                cmd.Parameters.AddWithValue("@charge", charge);
                                cmd.Parameters.AddWithValue("@notes", Notes);
                                cmd.Parameters.AddWithValue("@distance", distance);
                                cmd.Parameters.AddWithValue("@recordNo", RecordNo);
                            }
                            else
                            {
                                SqlStmt = "Insert into Roster ([Client Code],[Carer Code],Program,[date],[Start Time],Duration,YearNo,MonthNo,DayNo,BlockNo,[Service Type],[Service Description],BillQty,CostQty,CostUnit,BillTo,StartKm,EndKM,KM, charge,Notes" + ",[type],[status],[Date Entered],[Date Last Mod],[BillUnit],[Unit Pay Rate],[Unit Bill Rate])" + " select *  " + ", isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Description] AND PROCESSCLASSIFICATION = 'INPUT'),0) as Unit_Pay_Rate" + ", isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Type] AND PROCESSCLASSIFICATION IN ('OUTPUT','EVENT')),0) as Unit_Bill_Rate" + " From " + "( select [Client Code],[Carer Code]    " + ",Program" + ",r.date " + ",left(DATEADD(MINUTE,duration*5,convert(time,[Start Time])),5) as start_time,'0' as duration,year(getdate()) as YearNo,month(getdate()) MonthNo,day(getdate()) as DayNo,0 as BlockNo " + ", isnull((select DefaultCHGTravelWithinActivity  from Recipients where AccountNo =r.[Client Code]) ," + "((select DefaultCHGTravelWithinActivity from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Type]" + " ,isnull((select DefaultCHGTravelWithinPayType from Staff where AccountNo =r.[Carer Code]) ," + "((select DefaultCHGTravelWithinPayType  from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Description]" + ", " + distance + " as BillQty, " + distance + " as PayQty,'SERVICE' as CostUnit" + " ,(select billto from Recipients where AccountNo=r.[Client Code]) as BillTo " + "," + StartKm + " as StartKm , " + EndKM + " as EndKM," + km + " as km," + charge + "  as charge,'" + Notes + "' as Notes " + ",9 as type,1 as status,getdate() as DateEntered,getDate() as DateModify,'SERVICE' as BillUnit " + "  from Roster r where recordNo= " + RecordNo + ") as travel_entry" + " where [Service_Type] Is Not null And [Service_Description] Is Not null and program is not null" + " and [Service_Type]<>'' And [Service_Description]<>'' and program<>''";
                            }
                        }
                        else
                        { 
                                SqlStmt = "Insert into Roster ([Creator],[Editer],[Client Code],[Carer Code],Program,[date],[Start Time],Duration,YearNo,MonthNo,DayNo,BlockNo,[Service Type],[Service Description],BillQty,CostQty,CostUnit,BillTo,StartKm,EndKM,KM, charge,Notes" + ",[type],[status],[Date Entered],[Date Last Mod],[BillUnit],[Unit Pay Rate],[Unit Bill Rate])" + " select *  " + ", isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Description] AND PROCESSCLASSIFICATION = 'INPUT'),0) as Unit_Pay_Rate" + ", 0 as Unit_Bill_Rate" + " From " + " ( select '" + claim.User +"' AS Creator, '" + claim.User+ "' AS Editer, '!INTERNAL' as [Client Code],[Carer Code]    " + ",isnull((select DefaultNCTravelWithinProgram from Staff where AccountNo =r.[Carer Code] ), " + "((select DefaultNCTravelWithinProgram   from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as Program" + ",r.date" + ",left(DATEADD(MINUTE,duration*5,convert(time,[Start Time])),5) as start_time,'0' as duration,year(getdate()) as YearNo,month(getdate()) MonthNo,day(getdate()) as DayNo,0 as BlockNo " + ", isnull((select DefaultNCTravelWithinActivity   from Staff where AccountNo =r.[Carer Code]) ," + "((select DefaultNCTravelWithinActivity  from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Type]" + " ,isnull((select DefaultNCTravelWithinPayType  from Staff where AccountNo =r.[Carer Code]) ," + "((select DefaultNCTravelWithinPayType from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Description]" + ", " + distance + " as BillQty, " + distance + " as PayQty,'SERVICE' as CostUnit" + ",(select billto from Recipients where AccountNo=r.[Client Code]) as BillTo " + "," + StartKm + " as StartKm , " + EndKM + " as EndKM," + km + " as km," + charge + "  as charge,'" + Notes + "' as Notes " + ",9 as type,1 as status,getdate() as DateEntered,getDate() as DateModify,'SERVICE' as BillUnit " + "  from Roster r where recordNo= " + RecordNo + ") as travel_entry" + " where [Service_Type] Is Not null And [Service_Description] Is Not null and program is not null" + " and [Service_Type]<>'' And [Service_Description]<>'' and program<>''";
                        }
                        break;
                    case "TRAVEL BETWEEN":
                        if (Charge_Type == "Chargeable")
                        {
                            charge = 1;
                            if (Convert.ToDouble(s_NewBillingRate) > 0)
                            {
                                SqlStmt = @"INSERT INTO Roster ([Creator],[Editer],[Client Code], [Carer Code], Program, [date], [Start Time], Duration, YearNo, MonthNo, DayNo, BlockNo, [Service Type], [Service Description], BillQty, CostQty, CostUnit, BillTo, StartKm, EndKM, KM, charge, Notes, [type], [status], [Date Entered], [Date Last Mod], [BillUnit], [Unit Pay Rate], [Unit Bill Rate]) SELECT *, isnull( ( SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Description] AND PROCESSCLASSIFICATION = 'INPUT'), 0) AS Unit_Pay_Rate, dbo.GetBillingRate([client code], [Service_Type], Program) AS Unit_Bill_Rate FROM ( SELECT [Client Code], [Carer Code], Program, r.date, left(DATEADD(MINUTE, duration* 5, convert(TIME, [Start Time])), 5) AS start_time, '0' AS duration, r.YearNo, r.MonthNo, r. DayNo, 0 AS BlockNo, isnull( ( SELECT DefaultCHGTravelBetweenActivity FROM Recipients WHERE AccountNo = r.[Client Code]) , ( ( SELECT DefaultCHGTravelBetweenPayType FROM humanresourcetypes WHERE [Group] = 'PROGRAMS' AND name = r.Program) ) ) AS [Service_Type], isnull( ( SELECT DefaultCHGTravelBetweenPayType FROM Staff WHERE AccountNo = r.[Carer Code]) , ( ( SELECT DefaultCHGTravelBetweenActivity FROM humanresourcetypes WHERE [Group] = 'PROGRAMS' AND name = r.Program) ) ) AS [Service_Description], @distance AS BillQty, @distance AS PayQty, 'SERVICE' AS CostUnit, ( SELECT billto FROM Recipients WHERE AccountNo = r.[Client Code] ) AS BillTo, @startKM AS StartKm, @endKM AS EndKM, @km AS km, @charge AS charge, @notes AS Notes, 9 AS TYPE, 1 AS status, getdate() AS DateEntered, getDate() AS DateModify, 'SERVICE' AS BillUnit FROM Roster r WHERE recordNo = @recordNo ) AS travel_entry WHERE [Service_Type] IS NOT NULL AND [Service_Description] IS NOT NULL AND program IS NOT NULL AND [Service_Type] <> '' AND [Service_Description] <> '' AND program <> ''";
                                cmd.Parameters.AddWithValue("@startKM", StartKm);
                                cmd.Parameters.AddWithValue("@endKM", EndKM);
                                cmd.Parameters.AddWithValue("@km", km);
                                cmd.Parameters.AddWithValue("@charge", charge);
                                cmd.Parameters.AddWithValue("@notes", Notes);
                                cmd.Parameters.AddWithValue("@distance", distance);
                                cmd.Parameters.AddWithValue("@recordNo", RecordNo);
                            }
                            else
                            {
                                SqlStmt = "Insert into Roster ([Client Code],[Carer Code],Program,[date],[Start Time],Duration,YearNo,MonthNo,DayNo,BlockNo,[Service Type],[Service Description],BillQty,CostQty,CostUnit,BillTo,StartKm,EndKM,KM, charge,Notes" + ",[type],[status],[Date Entered],[Date Last Mod],[BillUnit],[Unit Pay Rate],[Unit Bill Rate])" + " select *  " + ", isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Description] AND PROCESSCLASSIFICATION = 'INPUT'),0) as Unit_Pay_Rate" + ", isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Type] AND PROCESSCLASSIFICATION IN ('OUTPUT','EVENT')),0) as Unit_Bill_Rate" + " From " + " ( select [Client Code],[Carer Code]    " + ",Program" + ",r.date " + ",left(DATEADD(MINUTE,duration*5,convert(time,[Start Time])),5) as start_time,'0' as duration,year(getdate()) as YearNo,month(getdate()) MonthNo,day(getdate()) as DayNo,0 as BlockNo" + ", isnull((select DefaultCHGTravelBetweenActivity from Recipients where AccountNo =r.[Client Code]) ," + "((select DefaultCHGTravelBetweenActivity from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Type]" + " ,isnull((select DefaultCHGTravelBetweenPayType from Staff where AccountNo =r.[Carer Code]) ," + "((select DefaultCHGTravelBetweenPayType from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Description]" + ", " + distance + " as BillQty, " + distance + " as PayQty,'SERVICE' as CostUnit" + " ,(select billto from Recipients where AccountNo=r.[Client Code]) as BillTo " + "," + StartKm + " as StartKm , " + EndKM + " as EndKM," + km + " as km," + charge + "  as charge,'" + Notes + "' as Notes" + ",9 as type,1 as status,getdate() as DateEntered,getDate() as DateModify,'SERVICE' as BillUnit " + "  from Roster r where recordNo= " + RecordNo + ") as travel_entry" + " where [Service_Type] Is Not null And [Service_Description] Is Not null and program is not null" + " and [Service_Type]<>'' And [Service_Description]<>'' and program<>''";
                            }
                        }
                        else
                        {
                            SqlStmt = "Insert into Roster ([Creator],[Editer],[Client Code],[Carer Code],Program,[date],[Start Time],Duration,YearNo,MonthNo,DayNo,BlockNo,[Service Type],[Service Description],BillQty,CostQty,CostUnit,BillTo,StartKm,EndKM,KM, charge,Notes" + ",[type],[status],[Date Entered],[Date Last Mod],[BillUnit],[Unit Pay Rate],[Unit Bill Rate])" + " select *  " + ",isnull((SELECT AMOUNT FROM ITEMTYPES WHERE TITLE = [Service_Description] AND PROCESSCLASSIFICATION = 'INPUT'),0) as Unit_Pay_Rate" + ", 0 as Unit_Bill_Rate" + " From " + " ( select '" + claim.User +"' AS Creator, '" + claim.User+"' AS Editer, '!INTERNAL' as [Client Code],[Carer Code]    " + ",isnull((select DefaultNCTravelBetweenProgram  from Staff where AccountNo =r.[Carer Code] ), " + "((select DefaultNCTravelBetweenProgram  from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as Program " + ",r.date " + ",left(DATEADD(MINUTE,duration*5,convert(time,[Start Time])),5) as start_time,'0' as duration,year(getdate()) as YearNo,month(getdate()) MonthNo,day(getdate()) as DayNo,0 as BlockNo " + ", isnull((select DefaultNCTravelBetweenActivity from Staff where AccountNo =r.[Carer Code]) ," + "((select DefaultNCTravelBetweenActivity  from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Type]" + " ,isnull((select [DefaultNCTravelBetweenPayType] from Staff where AccountNo =r.[Carer Code]) ," + "((select [DefaultNCTravelBetweenPayType] from humanresourcetypes where [Group] ='PROGRAMS' and name=r.Program))) as [Service_Description]" + ", " + distance + " as BillQty, " + distance + " as PayQty,'SERVICE' as CostUnit" + ",(select billto from Recipients where AccountNo=r.[Client Code]) as BillTo " + "," + StartKm + " as StartKm , " + EndKM + " as EndKM," + km + " as km," + charge + "  as charge,'" + Notes + "' as Notes " + ",9 as type,1 as status,getdate() as DateEntered,getDate() as DateModify,'SERVICE' as BillUnit " + "  from Roster r where recordNo= " + RecordNo + ") as travel_entry" + " where [Service_Type] Is Not null And [Service_Description] Is Not null and program is not null" + " and [Service_Type]<>'' And [Service_Description]<>'' and program<>''";
                        }
                        break;
                }                                
                    
                SqlStmt += ";SELECt @@identity";
                cmd.Transaction = trans;
                string identity = string.Empty;
                try
                {
                    cmd.CommandText = SqlStmt;
                    using(var rd2 = await cmd.ExecuteReaderAsync())
                    {
                        if(await rd2.ReadAsync())
                        {
                            identity = rd2.GetValue(0).ToString();
                        }                           
                    }   
                    trans.Commit();
                    return Ok(Int32.Parse(identity) > 0);   
                } catch (Exception ex) {
                    trans.Rollback();
                    return BadRequest(new {
                        message = ex,
                        result = false
                    });
                }
            }
        }

        [HttpPost, Route("clientnote")]
        public async Task<IActionResult> PostClientNote([FromBody] ClientNote note)
        {
            note.PersonId = await GenSettings.getValue("SELECT UniqueId FROM Recipients where AccountNo='" + note.RecipientCode + "'");

            using ( var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using( SqlCommand cmd = new SqlCommand(@"INSERT INTO HISTORY(detaildate,PersonId,Creator,Detail,PrivateFlag,ExtraDetail1,ExtraDetail2, whocode) 
                                                            VALUES(getDate(),@pid,@oid,@note,0,@notetype,@notetype,@rcode)",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@pid", note.PersonId);
                    cmd.Parameters.AddWithValue("@oid", note.OperatorID);
                    cmd.Parameters.AddWithValue("@note", note.Note);
                    cmd.Parameters.AddWithValue("@notetype", note.Note_Type);
                    cmd.Parameters.AddWithValue("@rcode", note.RecipientCode);

                    await conn.OpenAsync();
                    if(await cmd.ExecuteNonQueryAsync() > 0)
                        return Ok(true);
                    
                    return Ok(false);
                }
            }
        }

        [HttpPut, Route("notes")]
        public async Task<IActionResult> UpdateRosterNotes([FromBody] RosterNote note)
        {
            using ( var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("UPDATE Roster SET Notes = @notes WHERE RecordNo = @record", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@notes", note.Note);
                    cmd.Parameters.AddWithValue("@record", note.RecordNo);
                    await conn.OpenAsync();

                    if(await cmd.ExecuteNonQueryAsync() > 0)
                        return Ok(true);                    
                    return Ok(false);                    
                }
            }
        }

        [HttpPut("disabilitystatus")]
        public async Task<IActionResult> UpdateDisabilityStatus([FromBody] Staff staff){
            try 
            {
                Staff _staff = await _context.Staff.Where(x => x.AccountNo == staff.AccountNo && x.UniqueID == staff.UniqueID).FirstOrDefaultAsync();

                if(staff == null)   return NotFound();

                _staff.ContactIssues = staff.ContactIssues;
                _staff.Cstda_DisabilityGroup = staff.Cstda_DisabilityGroup;
                _staff.Cstda_OtherDisabilities = staff.Cstda_OtherDisabilities;
                _staff.Cstda_Indiginous = staff.Cstda_Indiginous;
                _staff.CaldStatus = staff.CaldStatus;

                await _context.SaveChangesAsync();
                return Ok(true);
            } catch(Exception ex)
            {
                return BadRequest(new {
                    error = ex.ToString()
                });
            }
        }

        [HttpPost, Route("incident")]
        public async Task<IActionResult> PostIncident([FromBody] RecordIncident ri)
        {
            string return_val = "0";
            string SqlStmt = "";
            string IncidnetNo = "";         

                if (ri.NoRecipient)               
                    ri.PersonId = await GenSettings.getValue("Select UniqueId from Staff where AccountNo='" + ri.Staff + "'");                
                else
                    ri.PersonId = await GenSettings.getValue("Select UniqueId from Recipients where AccountNo='" + ri.PersonId + "'");                
                
                SqlStmt = "Insert into [IM_Master](PersonId,[Type],[Service],Severity,[Location],[date],[time],ShortDesc,FullDesc,ReportedBy,CurrentAssignee,DateReported,Status,program)" + "Values ('" + ri.PersonId + "','" + ri.IncidentType + "','" + ri.Service + "','" + ri.IncidentSeverity + "','" + ri.Location + "',getdate(), '1899-12-30 ' +convert(varchar,getDate(),114),'" + ri.Note + "','" + ri.Note + "','" + ri.Staff + "','" + ri.Staff + "',getDate(),'OPEN','" + ri.Program + "');select @@identity";

                try
                {
                    using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                    {

                        using (SqlCommand cmd = new SqlCommand(@SqlStmt, (SqlConnection)conn))
                        {
                            conn.Open();
                            SqlDataReader dr = await cmd.ExecuteReaderAsync();
                            if (dr.Read())                         
                                IncidnetNo = dr.GetValue(0).ToString();
                            
                        }
                    }
                }
                catch 
                {
                    return new NoContentResult();
                }

              
                SqlStmt = "insert into [IM_NotifiableStaff] (IM_Master#,Staff#) values (" + IncidnetNo + ",'" + ri.Staff + "')";
                SqlStmt = SqlStmt + ";insert into [IM_InvolvedStaff] (IM_Master#,Staff#) values (" + IncidnetNo + ",'" + ri.Staff + "')";
                SqlStmt = SqlStmt + ";insert into history(detaildate,PersonId,Creator,Detail,PrivateFlag,ExtraDetail1,ExtraDetail2, whocode) " + " values(getDate(),'" + ri.PersonId + "','" + ri.OperatorId + "','" + ri.Note + "',0,'RECIMNOTE','FIELD NOTE','" + ri.RecipientCode + "'); select @@identity";

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {

                    using (SqlCommand cmd = new SqlCommand(@SqlStmt, (SqlConnection)conn))
                    {
                        await conn.OpenAsync();
                        SqlDataReader dr = await cmd.ExecuteReaderAsync();

                        if (dr.Read())                        
                            return_val = dr.GetValue(0).ToString();
                        
                    }
                }
                return Ok(return_val);
            }
            catch 
            {
                return NoContent();                
            }   
        }

        [HttpPost, Route("casenote")]
        public async Task<IActionResult> PostCaseNote([FromBody] CaseNote note){
            using (var conn = _context.Database.GetDbConnection() as SqlConnection){
                using (SqlCommand cmd = new SqlCommand("INSERT INTO History(PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator, GroupID, PrivateFlag) " +
                                                       "VALUES(@pid, GETDATE(), @detail, 'CASENOTE', 'CASE NOTE ONE', @whocode,@creator,@groupID,@pflag)",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@date", (DateTime) note.DetailDate);
                    cmd.Parameters.AddWithValue("@pid", note.PersonID);
                    cmd.Parameters.AddWithValue("@creator", note.Creator);
                    cmd.Parameters.AddWithValue("@detail", note.Detail);
                    cmd.Parameters.AddWithValue("@pflag", 0);
                    cmd.Parameters.AddWithValue("@edetail1", note.ExDetails);
                    cmd.Parameters.AddWithValue("@edetail2", note.ExDetails);
                    cmd.Parameters.AddWithValue("@whocode", note.WhoCode);

                    await conn.OpenAsync();
                    
                    if(await cmd.ExecuteNonQueryAsync() > 0)
                        return Ok(true);
                    
                    return Ok(false);
                }
            }
        }

        [HttpPut,Route("user/name")]
        public async Task<IActionResult> UpdateProfile([FromBody] Staff staff){
            try{
                Staff _staff = await (from p in _context.Staff
                                        where p.AccountNo == staff.AccountNo
                                        select p).SingleOrDefaultAsync();

                _staff.FirstName = staff.FirstName;
                _staff.MiddleNames = staff.MiddleNames;
                _staff.LastName = staff.LastName;
                _staff.Gender = staff.Gender;
                _staff.Title = staff.Title;
                _staff.Rating = staff.Rating;
                _staff.Pan_Manager = staff.Pan_Manager;
                _staff.Category = staff.Category;
                _staff.Stf_Department = staff.Stf_Department;
                _staff.StaffGroup = staff.StaffGroup;
                _staff.SubCategory = staff.SubCategory;
                _staff.StaffTeam = staff.StaffTeam;
                _staff.ServiceRegion = staff.ServiceRegion;
                _staff.UBDMap = staff.UBDMap;
                _staff.DLicence = staff.DLicence;
                _staff.VRegistration = staff.VRegistration;
                _staff.NRegistration = staff.NRegistration;
                _staff.caseManager = staff.caseManager;
                _staff.IsRosterable = staff.IsRosterable;
                _staff.EmailTimesheet = staff.EmailTimesheet;
                _staff.ExcludeFromConflictChecking = staff.ExcludeFromConflictChecking;
                _staff.PreferredName = staff.PreferredName;
                _staff.Cstda_DisabilityGroup = staff.Cstda_DisabilityGroup;
                _staff.Cstda_OtherDisabilities = staff.Cstda_OtherDisabilities;
                _staff.Cstda_Indiginous = staff.Cstda_Indiginous;
                _staff.CaldStatus = staff.CaldStatus;
                _staff.VisaStatus = staff.VisaStatus;


                if (staff.DOB != null)
                    _staff.DOB = staff.DOB;

                _staff = staff;

                await _context.SaveChangesAsync();
                return Ok(new { 
                    success = true
                });
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost, Route("opnote")]
        public async Task<IActionResult> PostOpNote(){
            return Ok();
        }

        [HttpDelete, Route("user/contact")]
        public async Task<IActionResult> DeleteAddress(){
            return Ok();
        }

        [HttpGet("is-accountno-unique/{accountNo}")]
        public async Task<IActionResult> GetIsAccountNoUnique(string accountNo){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using(SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) from Staff WHERE AccountNo = @accountNo",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@accountNo", accountNo);
                    await conn.OpenAsync();

                    int resultSet = 0;
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        if(await rd.ReadAsync())
                        {
                            resultSet = rd.GetInt32(0);
                        }
                        return Ok(resultSet == 0);
                    }
                }
            }
        }

        [HttpGet, Route("staffrecordview/{uname}")]
        public async Task<IActionResult> GetStaffRecordView(string uname)
        {
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using(SqlCommand cmd = new SqlCommand(@"SELECT StaffRecordView FROM UserInfo WHERE Name=@uname",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@uname", uname);
                    await conn.OpenAsync();

                    string recordView = string.Empty;
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync()){
                        while(await rd.ReadAsync())                        
                            recordView = GenSettings.Filter(rd[0]);
                        
                    }
                    return Ok(new {
                        viewString = recordView
                    });
                }
            }
        }


        
        [HttpPost("changeStaffCode/{oldCode}/{newCode}")]
        public async Task<IActionResult> ChangeStaffCode(string oldCode, string newCode)
        {
            try
            {
                var updateQueries = new string[]
                {
                    "UPDATE Roster SET [Carer Code] = @NewCode WHERE [Carer Code] = @OldCode",
                    "UPDATE Staff SET AccountNo = @NewCode WHERE AccountNo = @OldCode",
                    "UPDATE Recipients SET MainSupportWorker = @NewCode WHERE MainSupportWorker = @OldCode",
                    "UPDATE ServiceOverview SET ServiceStaff = @NewCode WHERE ServiceStaff = @OldCode",
                    "UPDATE HumanResources SET [Name] = @NewCode WHERE [Name] = @OldCode AND [GROUP] = 'EXCLUDEDSTAFF'",
                    "UPDATE UserInfo SET StaffCode = @NewCode WHERE StaffCode = @OldCode",
                    "UPDATE DataDomains SET [HACCCode] = @NewCode WHERE [HACCCode] = @OldCode"
                };

                await using var transaction = await _context.Database.BeginTransactionAsync();
                try
                {
                    foreach (var query in updateQueries)
                    {
                        await _context.Database.ExecuteSqlRawAsync(query,
                            new SqlParameter("@NewCode", newCode),
                            new SqlParameter("@OldCode", oldCode));
                    }
                    await transaction.CommitAsync();
                    return Ok("Staff code updated successfully.");
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    return BadRequest($"Error updating staff code: {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Unexpected error: {ex.Message}");
            }
        }

        [HttpDelete("deleteStaff/{accountNo}/{personId}")]
        public async Task<IActionResult> DeleteStaff(string accountNo, string personId)
        {
            try
            {
                var deleteQueries = new string[]
                {
                    "DELETE FROM Staff WHERE AccountNo = @AccountNo",
                    "DELETE FROM NamesAndAddresses WHERE PersonID = @PersonID",
                    "DELETE FROM PhoneFaxOther WHERE PersonID = @PersonID",
                    "DELETE FROM HumanResources WHERE PersonID = @PersonID"
                };

                await using var transaction = await _context.Database.BeginTransactionAsync();
                try
                {
                    foreach (var query in deleteQueries)
                    {
                        await _context.Database.ExecuteSqlRawAsync(query,
                            new SqlParameter("@AccountNo", accountNo),
                            new SqlParameter("@PersonID", personId));
                    }
                    await transaction.CommitAsync();
                    return Ok("Staff deleted successfully.");
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();
                    return BadRequest($"Error deleting staff: {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                return BadRequest($"Unexpected error: {ex.Message}");
            }
        }





    }
}
