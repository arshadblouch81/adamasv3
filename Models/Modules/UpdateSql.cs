
namespace Adamas.Models.Modules{
    public class UpdateSql {
      public string Sql { get; set; }
      public DataRecord Record { get; set ;}
    }

    public class  DataRecord {
        public int Id   { get; set; }
        public string Name { get; set; }
    }
}