import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Component, OnInit,OnDestroy, ViewChild } from '@angular/core'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { BsModalComponent } from 'ng2-bs3-modal';
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, GlobalService, ListService, achievementIndex } from '@services/index'

@Component({
    selector: '',
    templateUrl:'./plans.html'
})

export class IntakePlans implements OnInit, OnDestroy {
    user: any;
    dbAction: number;
    
    private plan$: Subscription;
    plans: Array<any>;
    loading: boolean;

    templateArr: Array<string>
    careDomainArr: Array<string>
    carePlanArr: Array<string>
    diciplineArr: Array<string>
    programsArr: Array<string>

    planGroup: FormGroup;
    constructor(
        private listS: ListService,
        private timeS: TimeSheetService,
        private sharedS: SharedService,
        private globalS: GlobalService,
        private router: Router,
        private formbuilder: FormBuilder
    ){
        this.plan$ = this.sharedS.changeEmitted$.subscribe(data => {
            if(this.globalS.isCurrentRoute(this.router, 'plans')){
                this.search();
            }
        });
    }
    
    ngOnDestroy(){
        this.plan$.unsubscribe();
    }

    ngOnInit(){
        this.resetGroup()
        this.reloadAll()
    }
    
    resetGroup(){
        this.planGroup = this.formbuilder.group({
            recordNumber: '',
            personID: '',
            id: '',
            planType: '',
            name: '',
            careDomain: '',
            discipline: '',
            program: '',
            startDate: '',
            signOffDate: '',
            reviewDate: '',
            reminder: ''
        })
    }

    reloadAll(){
        this.search()
        this.listDropDowns();
    }

    listDropDowns(){
        this.listS.gettemplatelist().subscribe(data => this.templateArr = data)

        this.listS.getcareplan().subscribe(data => this.carePlanArr = data)
        this.listS.getcaredomain().subscribe(data => this.careDomainArr = data)
        this.listS.getdiscipline().subscribe(data => this.diciplineArr = data)

        this.listS.getintakeprogram(this.user.uniqueID)
                    .subscribe(data => this.programsArr = data)      
    }

    search(){
        this.user = this.sharedS.getPicked();
        this.loading = true;
        this.timeS.getplans(this.user.uniqueID).subscribe(plans => {
            this.loading = false;
            this.plans = plans;
        });
    }

    planProcess(){
        
    }
}