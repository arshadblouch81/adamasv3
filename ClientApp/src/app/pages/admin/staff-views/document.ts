import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit,ViewChild } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, UploadService,PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DomSanitizer } from '@angular/platform-browser';
import { NzMessageService } from 'ng-zorro-antd/message';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';
import { ContextMenuComponent } from 'ngx-contextmenu';

interface NewDocument{
    file: string,
    newFile: string,
    path: string
}

@Component({
    styles: [`
    nz-table{
        margin-top:0px;
    }
    .limit-width{
        width: 5rem;
        overflow: hidden;
    }
    .exist{
        color:green;
    }
    .not-exist{
        color:red;
    }
    ul{
        list-style:none;
    }
    ul li {
        padding:5px;
        cursor:pointer;
    }
    ul li.active{
        background:#e6f1ff;
    }
    ul li:not(.active):hover{
        background:#e6f1ff;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    nz-table th{
        font-weight: 600;
        font-family: 'Segoe UI';
        font-size: 14px;
        border: 1px solid #f3f3f3;
    }
    tbody  tr:nth-child(odd) {
            background-color: #E9F7FF;
      }
      tbody  tr:nth-child(even) {
            background-color: #fff;
    }
    `],
    templateUrl: './document.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffDocumentAdmin implements OnInit, OnDestroy, AfterViewInit {
    
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
    private unsubscribe: Subject<void> = new Subject();
    public templates$: Observable<any>;
    
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    loading: boolean = false;
    addDocumentModal: boolean = false;
    current: number = 0;
    
    postLoading: boolean = false;
    selectedIndex: number;
    
    fileObject: NewDocument;
    showUpload: boolean = false;
    selectedRow  : number = 0;
    activeRowData:any;
    tocken: any;
    pdfTitle: string;
    selectedStaff: any;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    showDocProperties:boolean = false;
    dateFormat: string = 'dd/MM/yyyy';
    selectedItem:any;
    addOrEdit:boolean = false;
    showDocRename: boolean;
    programs: any;
    caredomains: any;
    disciplines: any;
    classifications: any;
    categories: any;
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private message: NzMessageService,
        private uploadS: UploadService,
        private printS: PrintService,
        private sanitizer: DomSanitizer,
    ) {
        cd.reattach();
        
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/staff/personal'])
                }
            }
        });
        
        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'document')) {
                this.search(data);
            }
        });
    }
    
    ngAfterViewInit(){
        this.showUpload = true;
    }
    
    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.user = this.sharedS.getPicked();
        if(this.user){
            this.search(this.user);
            this.buildForm();
            return;
        }
        this.router.navigate(['/admin/staff/personal'])
    }
    
    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
    
    buildForm() {
        this.inputForm = this.formBuilder.group({
            docID:0,
            title:'',            
            discipline :'',
            program :'',
            caredomain:'',
            classification:'',
            category:'',
            reminderDate: this.currentDate(),
            reminderText:'',
            notes:'' ,
            publishToApp:false           
        });
    }
    currentDate(){
        return new Date();
    }
    search(user: any = this.user) {
        this.cd.reattach();
        this.selectedRow=0;
        this.loading = true;
        this.selectedStaff = user;
        this.timeS.getdocuments(user.id).subscribe(data => {
            this.tableData = data;
            this.originalTableData = data;
            this.loading = false;
            this.postLoading= false;
            this.selectedRow = 0;
            this.cd.detectChanges();
        });
        
    }
    
    selectDocument(doc: any, index: number){
        this.selectedIndex = index;

        if(!doc.exists){
            this.current = 0;
            this.globalS.iToast('Info','The Selected Template is not longer valid - Please refer to your Traccs Administrator')
            return;
        }

        this.current = 1;

        // if(doc.exists){
        this.fileObject = {
            file: doc.name,
            newFile: `${doc.name}`,
            path: doc.template
        };

        // this.addDocumentModal=false;    
        // this.showDocProperties=true;
        // return;
        // }
        //this.globalS.eToast('Error','File not exists')            
    }
    selectedItemGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
    }
    trackByFn(index, item) {
        return item.id;
    }
    
    showAddModal() {
        this.addDocumentModal = true;
        this.addOrEdit=false;
        this.listDropDowns();
        this.templates$ = this.uploadS.getdocumenttemplate();
    }
    
    reload(reload: boolean){
        if(reload){
            this.search();
        }
    }
    
    showEditModal(data: any) {

        this.inputForm.reset();
        this.addOrEdit=true;

        console.log(data);

        this.inputForm.patchValue( {
            docID: data.docID,
            title:data.title,            
            discipline : data.discipline || 'VARIOUS',
            program : data.program || 'VARIOUS',
            caredomain:data.careDomain || 'VARIOUS',
            classification: data.classification || 'DEFAULT',
            category: data.category || 'FILE',
            reminderDate: data.alarmDate,
            reminderText: data.alarmText,
            // notes: data.notes  ,
            // publishToApp:data.publishToApp         
        });
        this.cd.detectChanges();
        this.showDocProperties=true;
    }
    
    downloadFile(index: any){
        var doc = this.tableData[this.selectedRow];
        var notifId = this.globalS.loadingMessage('Downloading Document');
        
        this.uploadS.download({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName:`${doc.filename}${doc.type}`,
            DocPath: doc.originalLocation
        }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {
            
            let data = window.URL.createObjectURL(blob);      
            let link = document.createElement('a');
            link.href = data;
            link.download = doc.filename;
            link.click();
            
            setTimeout(() =>
                {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                this.message.remove(notifId);
                
                this.globalS.sToast('Success','Download Complete');
            }, 100);
            
        }, err =>{
            console.log(err);
            this.globalS.eToast('Error', "Document can't be found");
        })
    }
    
    reportTab: any;
    viewFile(){
        var doc = this.tableData[this.selectedRow];
        
        this.reportTab = window.open("", "_blank");
        
        this.uploadS.getdocumentblob({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName: `${doc.filename}${doc.type}`,
            DocPath: doc.originalLocation
        }).subscribe(data => {
            this.openDocumentTab(data);
        }, (error: any) => {
            this.reportTab.close();
            this.globalS.eToast('Error','File not located')
        })
    }
    
    openFile(index: any){
        
    }
    
    openDocumentTab(data: any)
    {
        // this.fileDocumentName = data.fileName;
        this.reportTab.location.href = `${data.path}`;   
        this.reportTab.focus();
    }
    
    handleCancel(){
        this.addDocumentModal = false;
        this.showDocProperties=false;
    }
    
    delete() {
        
        let singleRow  = this.tableData[this.selectedRow];
        
        this.uploadS.delete(this.user.id,{ 
            id: singleRow.docID,
            filename: singleRow.title
        }).pipe(takeUntil(this.unsubscribe))
        .subscribe(data => {
            if(data){
                this.search();
                this.globalS.sToast('Success','File deleted')
                return;
            }
        })
    }
    
    save(){
        var inp = {
            User: this.globalS.decode().user,
            PersonID: this.user.id,
            OriginalFileName: this.fileObject.file,
            NewFileName: this.fileObject.newFile,
            Classification:"DEFAULT",
            discipline :  'VARIOUS',
            program    : 'VARIOUS',
            caredomain:'VARIOUS',
            category:"FILE",
            Path:  this.fileObject.path
        };
        this.uploadS.postdocumentstafftemplate(inp).subscribe(data => {
            this.globalS.sToast('Success','Document has been added');
            this.handleCancel();
            this.search();
        }, (err) =>{
            console.log(err);
            this.globalS.eToast('Error', err.error.message);
        });
    }
    
    pre(): void {
        this.current -= 1;
    }
    
    next(): void {
        this.current += 1;
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        // console.log("staff "+ permissoons.length );
        return permissoons.charAt(index-1);
    }
    //Mufeed 17 April 2024 
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = "";
        this.tryDoctype="";
    }           
    InitiatePrint(){
        
        var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid,strdate, endate;
        var head1, head2, head3, head4, head5,head6, head7, head8, head9, head10,head11;         
        //fQuery = " SELECT DOC_ID, Documents.[Title], [Status] as [St], Classification, Documents.Category ,[Created], UserInfo.Name As Author, [Modified], [Filename], DocumentType As [Type], OriginalLocation FROM Documents  LEFT JOIN UserInfo ON Documents.Author = UserInfo.Recnum  INNER JOIN Staff ON Documents.[PersonID] = Staff.[UniqueID] WHERE Staff.[AccountNo] = '"+this.user.code+"' AND DOCUMENTGROUP <> 'FORM' AND (Documents.[DeletedRecord] = 0 OR Documents.[DeletedRecord] Is NULL) ORDER BY Documents.[Modified] DESC   "        
        txtTitle = "Documents";       
        //Rptid =   "8WTerq14eNFSAnz0"  
        
        //console.log(fQuery)
            head1 = "title";
            head2 = "author";
            head3 = "status";
            head4 = "careDomain";
            head5 = "discipline";
            head6 = "program";
            head7 = "classification";
            head8 = "category";
            head9 = "modified";
            head10 = "filename";
            head11 = "created"; 
            

            Rptid =   "4HcwIWORPtT9HKXw"  
            const data = {
                        
                "template": { "_id": Rptid },                                
                "options": {
                    "reports": { "save": false },                
                    "sql": this.tableData,
                    "Criteria": lblcriteria,
                    "userid": this.tocken.user,
                    "txtTitle": txtTitle+ ' for '+ this.selectedStaff.code,                    
                    "head1":head1,
                    "head2":head2,
                    "head3":head3,
                    "head4":head4,
                    "head5":head5,
                    "head6":head6,
                    "head7":head7,
                    "head8":head8,
                    "head9":head9,
                    "head10":head10,
                    "head11":head11,
                    
                                                                    
                }
            }
        this.loading = true;
        this.drawerVisible = true;         
        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = txtTitle+".pdf"
            this.drawerVisible = true;                   
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            console.log(err);
            this.loading = false;
            this.drawerVisible = false;
            this.modalService.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });
        return;      
    }
    
    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];
    
    columnDictionary = [
        {
            key: 'Document Name',
            value: 'title'
        },{
            key: 'created By',
            value: 'author'
        },{
            key: 'Care Domain',
            value: 'careDomain'
        },{
            key: 'Dicipline',
            value: 'discipline'
        },{
            key: 'Program',
            value: 'program'
        },{
            key: 'class',
            value: 'classification'
        }
        ,{
            key: 'category',
            value: 'category'
        },
        {
            key:'Modified',
            value:'modified',
        },
        {
            key: 'Filename',
            value: 'filename'
        },{
            key: 'Created',
            value: 'Created'
        }]
        ;
        
        dragDestination = [       
            'Document Name',
            'created By',
            'Status',
            'Care Domain',
            'Dicipline',
            'Program',
            'Class',
            'Classification',
            'Category',
            'category',
            'Author',
            'Modified',
            'Filename',
            'Created',
        ];
        
        
        flattenObj = (obj, parent = null, res = {}) => {
            for (const key of Object.keys(obj)) {
                const propName = parent ? parent + '.' + key : key;
                if (typeof obj[key] === 'object') {
                    this.flattenObj(obj[key], propName, res);
                } else {
                    res[propName] = obj[key];
                }
            }
            return res;
        }
        
        searchColumnDictionary(data: Array<any>, tobeSearched: string){
            let index = data.findIndex(x => x.key == tobeSearched);        
            return data[index].value;
        }
        
        drop(event: CdkDragDrop<string[]>) {
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
            } else {
                if(!event.container.data.includes(event.item.data)){
                    copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
                }
            }
            this.generate();
        }
        
        generate(){
            const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
            
            console.log(dragColumns)
            var convertedObj = groupArray(this.originalTableData, dragColumns);
            console.log(convertedObj)
            
            var flatten = this.flatten(convertedObj, [], 0);
            
            if(dragColumns.length == 0){
                this.tableData = this.originalTableData;
            } else {
                this.tableData = flatten;
            }
        }
        
        flatten(obj: any, res: Array<any> = [], counter = null){
            for (const key of Object.keys(obj)) {
                const propName = key;
                if(typeof propName == 'string'){                   
                    res.push({key: propName, counter: counter});
                    counter++;
                }
                if (!Array.isArray(obj[key])) {
                    this.flatten(obj[key], res, counter);
                    counter--;
                } else {
                    res.push(obj[key]);
                    counter--;
                }
            }
            return res;
        }
        
        removeTodo(data: any){
            this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
            this.generate();
        }
        
        isArray(data: any){
            return Array.isArray(data);
        }
        
        isSome(data: any){
            if(data){
                return data.some(d => 'key' in d);
            }
            return true;        
        }
        showAcceptModal(data: any, index:number) {
            console.log(data)
            if (index==1){
                this.listDropDowns();
                this.showEditModal(data);
            }
            // else if (index==2){
            //     this.inputForm.patchValue( {
            //             title : data.title,
            //             docID: data.docID,
            //         });
            //     this.showDocRename=true;
            // }
            // else if (index==3){
            //     this.sendDocusignEnvelope(index);
            // }
        }
        
        // sendDocusignEnvelope(index: number) {
        //     throw new Error('Method not implemented.');
        // }

        listDropDowns() {

                    "Invalid column name 'T0100004652'."
                    let sql=`select distinct [Program]  FROM RecipientPrograms WHERE RecipientPrograms.PersonID = '${this.user.id}' ORDER BY [Program]`;
                    let sql2=` select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;
                    let sql3=`select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;
                    let sql4=` select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'FILECLASS' AND ISNULL(EndDate, '') = '' ORDER BY [Description] `;
                    let sql5=`select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DOCCAT' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;
                    forkJoin([
                        this.listS.getlist(sql),
                        this.listS.getlist(sql2),
                        this.listS.getlist(sql3),
                        this.listS.getlist(sql4),
                        this.listS.getlist(sql5)                
                    ]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                        this.programs = data[0].map(x=>x.program);
                        this.caredomains = data[1].map(x=>x.description);
                        this.disciplines= data[2].map(x=>x.description);
                        this.classifications= data[3].map(x=>x.description);
                        this.categories= data[4].map(x=>x.description);
                        this.caredomains.push('VARIOUS');
                        this.disciplines.push('VARIOUS');
                        this.programs.push('VARIOUS');
                        this.classifications.push('DEFAULT');
                        this.categories.push('FILE');
                        this.cd.detectChanges();
                        
                    });

                    setTimeout(() => {   },1000)
        }
        OnItemSelected(data:any, i:number){
            this.selectedRow=i;
            this.selectedItem=data;
        }
        OpenSelectedItem(data:any, i:number){
            this.selectedRow=i;
            this.selectedItem=data;
            this.downloadFile(i);
        }
        
        
    }