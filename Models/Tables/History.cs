using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Adamas.Models.Tables {
    public class History
    {
        [Key]
        public int RecordNumber { get; set; }
        public string PersonID { get; set;}
        public DateTime? DetailDate { get; set; }
        public string Detail { get; set; }

        public DateTime? AlarmDate { get; set; } = null;
        [Column("Acknowledged", TypeName = "bit")]
        [DefaultValue(false)]
        public Boolean Acknowledged { get; set; } = false;
        public string ExtraDetail1 { get; set; }
        public string ExtraDetail2 { get; set; }
        public string WhoCode { get; set; }

        [Column("DeletedRecord", TypeName = "bit")]
        [DefaultValue(false)]
        public Boolean DeletedRecord { get; set; }
        public string Creator { get; set; }
        public int? COID { get; set; }
        public int? BRID { get; set; }
        public int? DPID { get; set; }
        public string GroupID { get; set; }
        [Column("PrivateFlag", TypeName = "bit")]
        [DefaultValue(false)]
        public Boolean PrivateFlag { get; set; } = false;
        public string ReminderScope { get; set; }
        public string Program { get; set; }
        public string Discipline { get; set; }
        public string CareDomain { get; set; }
        public string? Restrictions { get; set; }
        public string AcknowledgedBy { get; set; }
        
        [Column("PublishToApp", TypeName = "bit")]
        [DefaultValue(false)]
        public Boolean? PublishToApp { get; set; }
        public string ReminderTo { get; set; }
        public Boolean? PublishToClientPortal { get; set; } 

    }
}