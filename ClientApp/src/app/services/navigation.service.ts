import { Injectable } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable()
export class NavigationService {
  private previousUrl: string | null = null;
  private currentUrl: string | null = null;
  private previousTabIndex: number | null = null;

  constructor(private router: Router) {
    this.currentUrl = this.router.url;
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      });
  }
  public resetPreviousTabIndex(): void {
    this.previousTabIndex = null;
  }

  public setPreviousTabIndex(index: number): void {
    this.previousTabIndex = index;
  }

  public getPreviousTabIndex(): number | null {
    return this.previousTabIndex;
  }

  public getPreviousUrl(): string | null {
    return this.previousUrl;
  }
}
