﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class ProgramStatusDto
    {
        public int? DocId { get; set; }
        public string PersonId { get; set; }
        public string Program { get; set; }
        public int? RecordNumber { get; set; }
    }
}
