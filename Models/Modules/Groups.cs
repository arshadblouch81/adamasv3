using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class Groups
    {

        public string PersonID { get; set; }
        public int? RecordNumber { get; set; } = 0;
        public string Name { get; set; }
        public  DateTime? Date1 { get; set; }
        public  DateTime? Date2 { get; set; }
        public string Notes { get; set; }
        public string Creator { get; set; }
        public  List<String> selectedConsent {get;set;}

         public bool MobileAlert { get; set; }
        
         public bool Recurring { get; set; }
    }
}