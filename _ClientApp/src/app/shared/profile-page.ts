import { forkJoin, Observable, Subject } from 'rxjs';

import { mergeMap, takeUntil } from 'rxjs/operators';
import { APP_BASE_HREF } from '@angular/common';
import { Component, OnInit, Input, forwardRef, ViewChild, OnDestroy, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { TimeSheetService, GlobalService, ClientService, StaffService, ListService, UploadService, months, days, gender, types, titles, caldStatuses, roles } from '../services/index';

import { RemoveFirstLast } from '../pipes/pipes';
import { BsModalComponent } from 'ng2-bs3-modal';

import { titles as pTitles } from '../services/global.service'

import * as _ from 'lodash';

import { HttpClient, HttpEventType, HttpParams } from '@angular/common/http'

export const PROFILEPAGE_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => ProfilePage),
};

const toast: any = {
    success: false,
    fail: false
};

const noop = () => {
};

@Component({
    selector: 'profile-page',
    templateUrl: './profile-page.html',
    styles: [
        `        
        h5{
            margin:10px 0;
        }
        .form-group{
            margin-bottom:10px;
        }
        .zindex-2000{
            z-index:2000;
            position:absolute;
            top: 6rem;
        }
        label{
            display:block;
            margin:0;
        }
        .photo-wrap{
            margin:10px 0;
            text-align:center;
        }
        .table{
            margin:0;
        }
        .bdate{
            margin:0 5px;
        }
        td select{
            height:1rem;
        }
      
        .profile-edit{
            position: absolute;
            top: 10px;
            right: 30px;
            z-index:10;
        }
        .profile-edit i{
            font-size: 20px;
            padding: 4px;
            border-radius: 50%;
        }
        .profile-edit i:hover{
            background:#d9e4ea !important;
            cursor:pointer !important;
        }
        select:not(.bdate){
            width:100%;
        }
        .sampl1{
            background: #fbfbfb;
            padding-top: 10px;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
            border-radius: 3px;
            margin-top: 10px;
            width: 100%;
            display:inline-block;
        }
        i.edit-main{
            float: right;
        }
        .ellipsis-overflow{
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }
        textarea.alerts{
            margin:10px 0;
        }
        clr-icon{
            float: left;
            margin: 3px 6px 0 0;
        }
        .email{
            position: absolute;
            margin-left: 1rem;
            bottom: 0;
            transform: translateY(-50%);
            cursor:pointer;
            color:#c1bcbc;
        }
        .email:hover{
            color:#717171;
        }
        .change-pic{
            position:absolute;
            left:12px;
        }
        input[type=file]{
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }
        .upload-pic{
            width: 10rem;
            margin: 15px auto;
            height: 35px;
            display: flex;
            align-items: center;
            justify-content: center;
            border: 1px solid #e7e7e7;
            cursor:pointer;
        }
        .upload-pic:hover{
            background: #e7e7e7;
        }

        .photoContainer{
            display: inline-block;
            position: relative;
            border-radius: 50%;
            overflow: hidden;
        }

        .photoContainer img{
            width: 4rem;
            border-radius: 50%;
            -webkit-box-shadow: 0 2px 9px -1px #777;
            -moz-box-shadow: 0 2px 9px -1px #777;
            box-shadow: 0 2px 9px -1px #777;
        }
        .photoContainer a{
            cursor:inherit;
        }

        .photoContainer.alert-show .prof-alert{
            transition: 0.1s;
            transition-timing-function: ease-out;
            
            transform: translateY(0);
            opacity: 1;
        }

        .prof-alert{
            position: absolute;
            color: #fff;
            bottom: 0;
            top: 1rem;
            padding-top:14px;
            right: 0;
            left: 0;
            background: #00000080;
            opacity:1;
            transform: translateY(130%);
            transition-timing-function: ease-in;
            transition: 0.2s;
            cursor:pointer;
        }
        .prof-alert i {
            display:block;
            font-size:18px;
        }
        `
    ],
    providers: [
        PROFILEPAGE_VALUE_ACCESSOR
    ]
})

export class ProfilePage implements OnInit, OnDestroy, ControlValueAccessor {
    @ViewChild('updateContactsAddressesModal', { static: false }) updateContactsAddressesModal: BsModalComponent;
    @ViewChild('caseStaffModal', { static: false }) caseStaffModal: BsModalComponent;

    private imgBlobData: any;

    src: string;
    location: Location;

    alertModalOpen: boolean = false;
    profilePictureOpen: boolean = false;
    alertShow: boolean = false;

    private unsubscribe$ = new Subject();
    changeTabView = new Subject<number>();

    tab: number = 1;

    titles: Array<string> = pTitles;

    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    private subSArr: Array<any> = [];

    deleteSelected: {
        data: any,
        option: string
    };

    @Input() AccountName: string = this.globalS.decode().nameid;
    @Input() Role: number = 2;  // 1-Admin 2-Recipient-Staff

    emailCoordinatorModal: boolean = false;
    updateProfileModal: boolean = false;
    updateWorkProfileModal: boolean = false;
    updateNotesModal: boolean = false;

    genderArr: Array<string> = gender
    typesArr: Array<string> = types
    titlesArr: Array<string> = titles
    caldStatuses: Array<string> = caldStatuses
    token: any;

    dropDowns: Dto.DropDowns;

    userForm: FormGroup;
    contactForm: FormGroup;
    addressForm: FormGroup;
    addressGroup: FormGroup;
    emailCoordinatorGroup: FormGroup;
    contactGroup: FormGroup;
    notesGroup: FormGroup;
    caseStaffGroup: FormGroup;

    contactIssueGroup: FormGroup;
    rosterAlertGroup: FormGroup;
    runsheetAlertGroup: FormGroup;

    years: Array<string> = [];
    months: Array<string> = [];
    days: Array<string> = [];

    addressType: Array<string> = [];
    contactType: Array<string> = [];
    casemanagers: Array<any> = [];
    rawcasemanagers: Array<any> = [];
    branches: Array<string> = [];

    tempContactArray: Array<any> = [];
    tempAddressArray: Array<any> = [];

    addresses: Array<any> = [];
    contacts: Array<any> = [];

    user: any = "";
    editModal: boolean = false;
    changePasswordModal: boolean = false;
    alertconsole: boolean = false;

    resultToast: any = toast;

    viewModal: Dto.ViewModal = {
        Title: 'Edit Contacts & Addresses',
        View: 1,
    };

    newContact: any = {
        Type: '',
        Detail: ''
    }

    newAddress: Dto.NamesAndAddresses = {
        Description: '',
        Suburb: '',
        Address1: ''
    };

    backgroundImage: string

    source: any;

    oldPass: string;
    newPass: string;
    reType: string;

    casestaffpopulateObj = {
        programs: [],
        coordinators: []
    }

    dbAction: number = 0;

    private innerValue: any = '';
    private role: string;

    constructor(
        private globalS: GlobalService,
        private listS: ListService,
        private clientS: ClientService,
        private staffS: StaffService,
        private formBuilder: FormBuilder,
        private timeS: TimeSheetService,
        private http: HttpClient,
        private uploadS: UploadService,
        @Inject(APP_BASE_HREF) private baseHref: string
    ) {

        this.addressForm = this.formBuilder.group({
            addresses: this.formBuilder.array([]),
        });

        this.contactForm = this.formBuilder.group({
            contacts: this.formBuilder.array([])
        });

        this.userForm = this.formBuilder.group({
            surnameOrg: [''],
            preferredName: [''],
            firstName: [''],
            middleNames: [''],
            gender: [''],
            year: [''],
            month: [''],
            day: [''],
            title: [''],
            uniqueID: '',

            file1: [''],
            file2: [''],
            subCategory: [''],
            branch: [''],

            serviceRegion: [''],
            casemanager: [''],

            caldStatus: [''],
            indigStatus: [''],
            primaryDisability: [''],
            note: [''],
            type: [''],
            jobCategory: [''],
            adminCategory: [''],
            team: [''],
            gridNo: [''],
            dLicense: [''],
            mvReg: [''],
            nReg: [''],
            isEmail: false,
            isRosterable: false,
            isCaseLoad: false,
            stf_Department: '',
            rating: ''
        });

        this.caseStaffGroup = this.formBuilder.group({
            coordinator: '',
            program: '',
            notes: '',
            name: '',
            recordNumber: null
        });


        this.contactIssueGroup = this.formBuilder.group({
            value: ''
        });

        this.rosterAlertGroup = this.formBuilder.group({
            value: ''
        });

        this.runsheetAlertGroup = this.formBuilder.group({
            value: ''
        });

        this.resetAddress();
        this.resetContacts();
        this.resetEmailCoordinator();
        this.resetNotes();
    }

    resetEmailCoordinator() {
        this.emailCoordinatorGroup = this.formBuilder.group({
            subject: new FormControl('', [Validators.required]),
            content: new FormControl('', [Validators.required])
        });
    }

    resetAddress() {
        this.addressGroup = this.formBuilder.group({
            description: new FormControl('', [Validators.required]),
            suburb: new FormControl('', [Validators.required]),
            address: new FormControl('', [Validators.required])
        })
    }

    resetContacts() {
        this.contactGroup = this.formBuilder.group({
            type: new FormControl('', [Validators.required]),
            detail: new FormControl('', [Validators.required])
        })
    }

    resetNotes() {
        this.notesGroup = this.formBuilder.group({
            notes: new FormControl('', [Validators.required]),
            caldStatus: new FormControl('', [Validators.required]),
            indigStatus: new FormControl('', [Validators.required]),
            primaryDisability: new FormControl('', [Validators.required]),
        })
    }

    ngOnInit() {
        this.months = months;
        this.years = this.globalS.year();
        this.days = days;
        this.token = this.globalS.decode();
        this.populateDropdowns();

        this.changeTabView.subscribe(data => this.tab = data)
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value != null && !_.isEqual(value, this.innerValue)) {
            this.innerValue = value;
            this.source = this.innerValue;
            this.pathForm(this.source);
            this.tab = 1;
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    patchTheseValuesInForm(user = this.user) {
        if (this.source.view === 'recipient') {
            this.userForm.patchValue({
                title: user.title,
                surnameOrg: user.surnameOrg,
                firstName: user.firstName,
                middleNames: user.middleNames,
                gender: this.globalS.searchOf(user.gender, this.genderArr, this.genderArr[2]),
                year: this.globalS.filterYear(user.dateOfBirth),
                month: this.globalS.searchOf(this.globalS.filterMonth(user.dateOfBirth), months, 'December'),
                day: this.globalS.filterDay(user.dateOfBirth),

                casemanager: user.recipient_Coordinator,
                file1: user.agencyIdReportingCode,
                branch: user.branch,
                file2: user.urNumber,
                subCategory: user.ubdMap,
                serviceRegion: user.agencyDefinedGroup
            });

            this.contactIssueGroup.patchValue({
                value: user.contactIssues
            })

            this.rosterAlertGroup.patchValue({
                value: user.notes
            })

            this.runsheetAlertGroup.patchValue({
                value: user.specialConsiderations
            })

        }

        if (this.source.view === 'staff') {
            this.userForm.patchValue({
                title: user.title,
                surnameOrg: user.lastName,
                firstName: user.firstName,
                middleNames: user.middleNames,
                gender: this.globalS.searchOf(user.gender, this.genderArr, this.genderArr[2]),
                year: this.globalS.filterYear(user.dob),
                month: this.globalS.searchOf(this.globalS.filterMonth(user.dob), months, 'December'),
                day: this.globalS.filterDay(user.dob),
                preferredName: user.preferredName,

                casemanager: user.pan_Manager,
                type: user.category,
                stf_Department: user.stf_Department,
                jobCategory: user.staffGroup,
                adminCategory: user.subCategory,
                team: user.staffTeam,
                serviceRegion: user.serviceRegion,
                gridNo: user.ubdMap,
                dLicense: user.dLicence,
                mvReg: user.vRegistration,
                nReg: user.nRegistration,
                rating: user.rating,

                caldStatus: user.caldStatus,
                indigStatus: user.cstda_Indiginous,
                primaryDisability: user.cstda_DisabilityGroup,
                note: user.contactIssues,

                isEmail: user.emailTimesheet,
                isRosterable: user.isRosterable,
                isCaseLoad: user.caseManager
            });

            this.notesGroup.patchValue({
                notes: user.contactIssues,
                caldStatus: user.caldStatus ? user.caldStatus.toUpperCase() : null,
                indigStatus: user.cstda_Indiginous ? user.cstda_Indiginous.toUpperCase() : null,
                primaryDisability: user.cstda_DisabilityGroup ? user.cstda_DisabilityGroup.toUpperCase() : null
            })

        }
    }

    refreshDynamicPicture(user: any): string {
        const { uniqueID, filePhoto } = user;

        if (this.globalS.isEmpty(filePhoto)) {
            return `assets/logo/profile.png?${this.globalS.randomString()}`
        }
        return `media/profile/${this.role}/${uniqueID}/profile.png?${this.globalS.randomString()}`;
    }

    pathForm(token: any = this.source) {

        this.role = this.source.view;

        if (this.source.view == 'recipient') {
            this.clientS.getprofile(token.code).pipe(
                mergeMap(data => {
                    this.user = data;
                    return this.getUserData(data.uniqueID);
                })).subscribe(data => {
                    this.backgroundImage = this.refreshDynamicPicture(this.user)

                    this.user.addresses = data[0];
                    this.user.contacts = data[1];
                    this.user.casestaff = data[2]

                    this.globalS.userProfile = this.user;

                    this.addresses = data[0];
                    this.contacts = data[1];

                    this.patchTheseValuesInForm(this.user)

                    this.addressForm.setControl('addresses', this.formBuilder.array(this.addressBuilder(data[0]) || []));
                    this.contactForm.setControl('contacts', this.formBuilder.array(this.contactBuilder(data[1]) || []));

                    this.tempContactArray = this.contactForm.get('contacts').value;
                });
        }

        if (this.source.view == 'staff') {
            this.staffS.getprofile(token.code).pipe(
                mergeMap(data => {
                    this.user = data;
                    return this.getUserData(data.uniqueID);
                })).subscribe(data => {
                    this.backgroundImage = this.refreshDynamicPicture(this.user)

                    this.user.addresses = data[0];
                    this.user.contacts = data[1];

                    this.globalS.userProfile = this.user;

                    this.addresses = data[0];
                    this.contacts = data[1];

                    this.patchTheseValuesInForm(this.user);
                    this.addressForm.setControl('addresses', this.formBuilder.array(this.addressBuilder(data[0]) || []));
                    this.contactForm.setControl('contacts', this.formBuilder.array(this.contactBuilder(data[1]) || []));
                    this.tempContactArray = this.contactForm.get('contacts').value;
                });
        }
    }

    getUserData(code: any) {
        return forkJoin([
            this.clientS.getaddress(code),
            this.clientS.getcontacts(code),
            this.timeS.getcasestaff(code)
        ]);
    }

    addressBuilder(arr: Array<any>): Array<FormGroup> {
        var groupArr: Array<FormGroup> = [];
        arr.forEach(x => {
            groupArr.push(
                new FormGroup({
                    description: new FormControl((new RemoveFirstLast().transform(x.description).trim())),
                    primaryAddress: new FormControl(x.primaryAddress),
                    address1: new FormControl(x.address1),
                    personID: new FormControl(x.personID),
                    suburb: new FormControl(x.suburb),
                    pcodesuburb: new FormControl(x.postCode + ' ' + x.suburb),
                    postcode: new FormControl(x.postCode),
                    recordNumber: new FormControl(x.recordNumber)
                })
            )
        });

        return groupArr;
    }

    contactBuilder(arr: Array<any>): Array<FormGroup> {
        var groupArr: Array<FormGroup> = [];
        arr.forEach(x => {
            groupArr.push(
                new FormGroup({
                    primaryPhone: new FormControl(x.primaryPhone),
                    type: new FormControl(new RemoveFirstLast().transform(x.type)),
                    detail: new FormControl(x.detail),
                    recordNumber: new FormControl(parseInt(x.recordNumber)),
                    personID: new FormControl(x.personID)
                })
            )
        });

        return groupArr;
    }

    populateDropdowns(): void {
        forkJoin([
            this.clientS.getcontacttype(),
            this.clientS.getaddresstype(),
            this.clientS.getmanagers(),
            this.listS.getlistbranches(),
            this.listS.getliststaffgroup(),
            this.listS.getliststaffadmin(),
            this.listS.getliststaffteam(),
            this.listS.getlistcasemanagers(),
            this.listS.getserviceregion(),
            this.listS.getlistdisabilities(),
            this.listS.getlistindigstatus()
        ]).pipe(
            takeUntil(this.unsubscribe$))
            .subscribe(data => {
                this.contactType = data[0].map(x => {
                    return (new RemoveFirstLast().transform(x.description)).trim();
                });
                this.addressType = data[1].map(x => {
                    return new RemoveFirstLast().transform(x.description)
                });

                this.rawcasemanagers = data[2];
                
                this.casemanagers = data[2].map(x => x.description);

                this.dropDowns = {
                    branchesArr: data[3],
                    jobCategoryArr: data[4],
                    adminCategoryArr: data[5],
                    teamArr: data[6],
                    managerArr: data[7],
                    serviceRegionArr: data[8],
                    disabilitiesArr: data[9],
                    indigenousArr: data[10]
                }
            })
    }

    edit() {
        this.editModal = true;
        this.viewModal = {
            Title: 'Edit Profile',
            View: 1,
        };

        this.populateDropdowns();
    }

    dateChange() {
        let userdata = this.userForm.value;

        if (userdata.month == 'January' ||
            userdata.month == 'March' ||
            userdata.month == 'May' ||
            userdata.month == 'July' ||
            userdata.month == 'August' ||
            userdata.month == 'October' ||
            userdata.month == 'December') {
            this.days = days;
            return false;
        }

        if (userdata.month == 'April' ||
            userdata.month == 'June' ||
            userdata.month == 'September' ||
            userdata.month == 'November') {
            this.days = this.globalS.thirty();
            return false;
        }
        if (userdata.month == 'February') {
            if (this.globalS.isLeapYear(userdata.year))
                this.days = this.globalS.twentynine();
            else
                this.days = this.globalS.twentyeight();

        }
    }

    addContact() {
        this.viewModal = {
            Title: 'Add New Contact',
            View: 2,
            Option: 'contact'
        }
    }

    addAddress() {
        this.viewModal = {
            Title: 'Add New Address',
            View: 2,
            Option: 'address'
        }
    }

    cancel() {
        if (this.viewModal.View == 1) this.updateContactsAddressesModal.close();
        if (this.viewModal.View == 2) {
            this.viewModal = {
                Title: 'Edit Profile',
                View: 1
            }
            this.clear();
            return;
        };
        this.editModal = false;
    }

    saveAddress() {
        if (this.addressGroup.valid) {
            let temp: Array<Dto.NamesAndAddresses> = [];
            let data = this.addressGroup.value;


            let pcode = /(\d+)/g.test(data.suburb) ? data.suburb.match(/(\d+)/g)[0] : "";
            let suburb = /(\D+)/g.test(data.suburb) ? data.suburb.match(/(\D+)/g)[0] : "";

            if (this.globalS.isEmpty(pcode) || this.globalS.isEmpty(suburb)) {
                this.globalS.eToast('Error', 'Postcode Wrong Format')
                return;
            }

            temp.push({
                Description: data.description,
                Address1: data.address,
                PostCode: pcode,
                Suburb: suburb,
                PersonID: this.user.uniqueID
            });

            this.clientS.addaddress(temp).subscribe(data => {
                if (data.success) {
                    this.globalS.sToast('Success', 'New Address inserted')
                    this.pathForm();
                    this.resetAddress();
                }
            })
            return
        }
        this.globalS.eToast('Error', 'Invalid inputs')
    }

    saveContact() {
        if (this.contactGroup.valid) {
            let temp: Array<Dto.PhoneFaxOther> = []
            let data = this.contactGroup.value
            temp.push({
                Type: data.type,
                Detail: data.detail,
                PersonID: this.user.uniqueID
            })

            this.clientS.addcontact(temp).subscribe(data => {
                console.log(data)
                if (data.success) {
                    this.globalS.sToast('Success', 'New Contact inserted')
                    this.pathForm();
                }
            })

            return
        }
        this.globalS.eToast('Error', 'Invalid inputs')
    }

    saveNotes() {
        const notes = this.notesGroup.value;
        let staff: Dto.Staffs = {
            accountNo: this.user.accountNo,
            uniqueID: this.user.uniqueID,
            caldStatus: notes.caldStatus,
            cstda_Indiginous: notes.indigStatus,
            cstda_DisabilityGroup: notes.primaryDisability,
            contactIssues: notes.notes
        }
        this.staffS.updatedisabilitystatus(staff)
            .subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Data Updated')
                    this.pathForm();
                }
            })
    }

    save() {
        if (this.viewModal.View === 1) {
            this.saveProfile();
        }

        if (this.viewModal.View === 2) {

            if (this.globalS.hasNoEmpty(this.newContact)) {
                let temp: Array<Dto.PhoneFaxOther> = [];
                temp.push({
                    Type: this.newContact.Type,
                    Detail: this.newContact.Detail,
                    PersonID: this.user.uniqueID
                })
                this.clientS.addcontact(temp).subscribe(data => {
                    if (data.success) {
                        this.viewModal.View = 1;
                        this.showAlert();
                        this.pathForm();
                    }
                })

            }


            if (this.globalS.hasNoEmpty(this.newAddress)) {
                let temp: Array<Dto.NamesAndAddresses> = [];
                let data = this.newAddress.Suburb;

                let pcode = /(\d+)/g.test(data) ? data.match(/(\d+)/g)[0] : "";
                let suburb = /(\D+)/g.test(data) ? data.match(/(\D+)/g)[0] : "";

                temp.push({
                    Description: this.newAddress.Description,
                    Address1: this.newAddress.Address1,
                    PostCode: pcode,
                    Suburb: suburb,
                    PersonID: this.user.uniqueID
                })

                this.clientS.addaddress(temp).subscribe(data => {
                    if (data.success) {
                        this.viewModal.View = 1;
                        this.showAlert();
                        this.pathForm();
                    }
                })

            }
            this.newContact = { Type: '', Detail: '' };
            this.newAddress = { Address1: '', Description: '', Suburb: '' };
        }

    }


    saveProfile(): void {
        var self = this;

        if (this.contactForm.dirty) {
            let data = this.contactForm.get('contacts').value as Array<Dto.PhoneFaxOther>;
            let temp: Array<Dto.PhoneFaxOther>;
            temp = _.differenceWith(data, this.tempContactArray, _.isEqual);
            this.subSArr.push(this.clientS.updateusercontact(temp));
        }

        if (this.addressForm.dirty) {

            let data = this.addressForm.get('addresses').value as Array<Dto.NamesAndAddresses>;

            data.forEach((address: any) => {

                let pcode = /(\d+)/g.test(address.pcodesuburb) ? address.pcodesuburb.match(/(\d+)/g)[0] : "";
                let suburb = /(\D+)/g.test(address.pcodesuburb) ? address.pcodesuburb.match(/(\D+)/g)[0] : ""
                address.suburb = suburb;
                address.postcode = pcode;

            });

            let temp: Array<Dto.NamesAndAddresses>;
            temp = _.differenceWith(data, this.tempAddressArray, _.isEqual);

            this.subSArr.push(this.clientS.updateuseraddress(temp));
        }

        if (this.userForm.dirty) {
            let data = this.userForm.value;
            let birthdate = this.formatDate(this.userForm);

            if (this.source.view == 'staff') {
                let user: Dto.Staffs = {
                    accountNo: this.source.code,
                    firstName: data.firstName,
                    middleNames: data.middleNames,
                    lastName: data.surnameOrg,
                    gender: data.gender,
                    title: data.title,
                    dob: birthdate,

                    rating: data.rating,
                    pan_Manager: data.casemanager,
                    category: data.type,
                    stf_Department: data.stf_Department,
                    staffGroup: data.jobCategory,
                    subCategory: data.adminCategory,
                    staffTeam: data.team,
                    serviceRegion: data.serviceRegion,
                    ubdMap: data.gridNo,
                    dLicence: data.dLicense,
                    vRegistration: data.mvReg,
                    nRegistration: data.nReg,
                    caseManager: data.isCaseLoad,
                    isRosterable: data.isRosterable,
                    emailTimesheet: data.isEmail,

                    preferredName: data.preferredName
                }
                this.subSArr.push(this.staffS.updateusername(user));
            }

            if (this.source.view == 'recipient') {
                let user: Dto.Recipients = {
                    accountNo: this.source.code,
                    firstName: data.firstName,
                    middleNames: data.middleNames,
                    surnameOrg: data.surnameOrg,
                    gender: data.gender,
                    title: data.title,
                    dateOfBirth: birthdate,

                    recipient_Coordinator: data.casemanager,
                    agencyIdReportingCode: data.file1,
                    urNumber: data.file2,
                    branch: data.branch,
                    agencyDefinedGroup: data.serviceRegion,
                    ubdMap: data.subCategory

                }

                this.subSArr.push(this.clientS.updateusername(user));
            }
        }
        this.process();
    }

    process(): void {
        forkJoin(this.subSArr).subscribe(
            data => {
                this.subSArr = [];
                let result = data.filter(x => x.success == false);
                if (result.length > 0)
                    this.resultToast.fail = true;
                else {
                    this.showAlert();
                    this.pathForm();
                }

                this.resetForms();
            }
        )
    }

    showAlert() {
        this.globalS.sToast('Success', 'Data Inserted')
        // this.resultToast.success = true;
        // setTimeout(() => {
        //     this.resultToast.success = false;
        //     this.resultToast.fail = false;
        // }, 2000);
    }

    delete() {
        let selected = this.deleteSelected;
        let value = this.deleteSelected.data.value;

        if (selected.option == 'contact') {
            this.clientS.deletecontact({
                Type: value.type,
                Detail: value.detail,
                RecordNumber: value.recordNumber,
                PersonID: value.personID
            }).subscribe(data => {
                if (data.success) {
                    this.globalS.sToast('Success', 'Contact deleted')
                    this.pathForm();
                }
            })
        }

        if (selected.option == 'address') {
            this.clientS.deleteaddress({
                RecordNumber: value.recordNumber,
                PersonID: value.personID
            }).subscribe(data => {
                if (data.success) {
                    this.globalS.sToast('Success', 'Address deleted')
                    this.pathForm();
                }
            })
        }
    }

    resetForms() {
        this.contactForm.reset();
        this.addressForm.reset();
        this.userForm.reset();
    }

    formatDate(data: any): string {
        let year = data.get('year').value;
        let month = this.months.indexOf(data.get('month').value) + 1;
        let day = data.get('day').value;

        if (year == null || day == null || month == 0)
            return null;

        return year + '/' + month + '/' + day + ' ' + '00' + ':' + '00' + ':' + '00';
    }

    pass() {
        this.changePasswordModal = true;
        this.clear();
    }

    changePass() {

        if ((this.globalS.isEmpty(this.reType) || this.globalS.isEmpty(this.newPass))
            || this.reType !== this.newPass) {
            this.globalS.eToast('Error', 'New Password did not match or is empty')
            return;
        }

        if (this.Role === 1) {
            this.timeS.updatepasswordadmin({
                Username: this.AccountName,
                Password: this.oldPass,
                NewPassword: this.newPass,
            }).subscribe(data => console.log(data))

            return;
        }

        if (this.globalS.isEmpty(this.oldPass) || this.globalS.isEmpty(this.newPass)) {
            this.globalS.eToast('Error', 'Both inputs are required')
            return;
        }

        var sample = {
            accountname: this.AccountName,
            oldpass: this.oldPass,
            newpass: this.newPass
        }
        //console.log(sample)
        this.timeS.updatepassword(
            {
                Username: this.AccountName,
                Password: this.oldPass,
                NewPassword: this.newPass,
            }
        ).subscribe(res => {
            this.globalS.sToast('Success', 'Password changed')
            this.changePasswordModal = false;
        }, (error: any) => {
            this.globalS.eToast('Error', error.error.message)
        })
    }

    clear() {
        this.oldPass = ""
        this.newPass = ""
        this.reType = ""

        this.newContact = {
            Type: '',
            Detail: ''
        }

        this.newAddress = {
            Description: '',
            Suburb: '',
            Address1: ''
        };


    }

    addressChange(state: boolean, data: any): void {
        this.addressForm.controls.addresses.value.forEach(e => {
            e.primaryAddress = false;
        });

        this.addressForm.controls.addresses.value.forEach(e => {
            if (e.recordNumber === data.recordNumber) {
                const address: Dto.NamesAndAddresses = {
                    RecordNumber: data.recordNumber,
                    PrimaryAddress: state,
                    PersonID: data.personID
                }
                this.timeS.updateprimaryaddress(address).subscribe(data => {
                    if (data) {
                        e.primaryAddress = state
                        this.pathForm();
                        this.globalS.sToast('Success', 'Primary Address Changed')
                    }
                })
            }
        });
    }

    contactChange(state: boolean, data: any): void {

        this.contactForm.controls.contacts.value.forEach(e => {
            e.primaryPhone = false;
        });

        this.contactForm.controls.contacts.value.forEach(e => {
            if (e.recordNumber === data.recordNumber) {
                let contacts: Dto.PhoneFaxOther = {
                    RecordNumber: data.recordNumber,
                    PrimaryPhone: state,
                    PersonID: data.personID
                }
                this.timeS.updateprimaryphone(contacts).subscribe(data => {
                    if (data) {
                        e.primaryPhone = state
                        this.pathForm();
                        this.globalS.sToast('Success', 'Primary Address Changed')
                    }
                })
            }
        });
    }

    editProfile() {

    }

    editWorkProfile(type: number) {
        // if 1 = staff ; 2 = recipient
        if (type === 1) {

        }

        if (type === 2) {

        }
    }


    /**
     *  Case Staff
     */
    updatecasestaff(staff: any) {
        console.log(staff)
        this.caseStaffModal.open();
        this.caseStaffGroup.patchValue({
            coordinator: staff.uniqueId,
            program: staff.program,
            notes: staff.notes,
            name: staff.accountNo,
            recordNumber: staff.recordNumber
        })

        console.log(this.caseStaffGroup.value)
    }

    deletecasestaff(staff: any) {
        this.timeS.deletecasestaff(staff.recordNumber)
            .subscribe(data => {
                if (data) {
                    this.pathForm();
                    this.globalS.sToast('Success', 'Case Staff Deleted')
                }
            })
    }

    populatecaseStaffModal() {
        this.caseStaffGroup.reset();
        this.listS.getactiveprograms()
            .subscribe(data => this.casestaffpopulateObj.programs = data)

        this.listS.getcoordinators()
            .subscribe(data => this.casestaffpopulateObj.coordinators = data)
    }

    saveCaseStaff() {
        if (this.dbAction == 0) {
            const { coordinator, notes, program } = this.caseStaffGroup.value;
            this.timeS.postcasestaff({
                personID: this.user.uniqueID,
                name: coordinator,
                address1: program,
                notes: notes
            }).subscribe(data => {
                if (data) {
                    this.pathForm();
                    this.globalS.sToast('Success', 'Case Staff Deleted')
                }
            })
        }

        if (this.dbAction == 1) {
            const { recordNumber, notes, program } = this.caseStaffGroup.value;
            this.timeS.updatecasestaff({
                recordNumber: recordNumber,
                address1: program,
                notes: notes
            }).subscribe(data => {
                if (data) {
                    this.pathForm();
                    this.globalS.sToast('Success', 'Case Staff Updated')
                }
            })
        }
    }
    /** */


    /**
     * Alerts and Contact Issues
     */


    saveCi() {

        const { sqlId } = this.user;

        this.timeS.updatealertsissues({
            sqlId: sqlId,
            issueType: 'ci',
            notes: this.contactIssueGroup.value.value
        }).subscribe(data => {
            if (data) {
                //this.pathForm();
                this.globalS.sToast('Success', 'Contact Issues Updated')
            }
        })
        this.contactIssueGroup.markAsPristine();
    }

    saveRoa() {
        const { sqlId } = this.user;
        this.timeS.updatealertsissues({
            sqlId: sqlId,
            issueType: 'roa',
            notes: this.rosterAlertGroup.value.value
        }).subscribe(data => {
            if (data) {
                //this.pathForm();
                this.globalS.sToast('Success', 'Roster Alert Updated')
            }
        })
        this.rosterAlertGroup.markAsPristine();
    }

    saveRua() {
        const { sqlId } = this.user;
        this.timeS.updatealertsissues({
            sqlId: sqlId,
            issueType: 'rua',
            notes: this.runsheetAlertGroup.value.value
        }).subscribe(data => {
            if (data) {
                //this.pathForm();
                this.globalS.sToast('Success', 'Runoff Alert Updated')
            }
        })
        this.runsheetAlertGroup.markAsPristine();
    }

    email() {
        var cm = this.rawcasemanagers.find(x => { return x.description == this.user.recipient_Coordinator });

        const { title, firstName, surnameOrg } = this.user;
        const { subject, content } = this.emailCoordinatorGroup.value;


        if (this.globalS.isEmpty(subject) || this.globalS.isEmpty(content))
            return this.globalS.eToast('Error', 'Subjet and Content is required');

        if (this.globalS.isEmpty(cm.detail)) {
            this.globalS.eToast('Error', 'No Coordinator email');
            return;
        }

        const emailData = {
            Subject: subject,
            Content: content,
            RecipientName: `${title} ${firstName} ${surnameOrg}`,
            CCAddresses: [],
            FromAddresses: [],
            ToAddresses: [{
                Name: cm.description,
                Address: cm.detail
            }]
        }

        this.clientS.postemailcoordinator(emailData).subscribe(data => console.log(data))
    }

    imgBLOB(imgBlob: FormData) {
        this.imgBlobData = imgBlob;
    }

    refreshProfilePic() {
        this.backgroundImage = this.refreshDynamicPicture(this.user)
    }

    changeProfile() {

        this.uploadS.uploadProfilePicture(this.imgBlobData)
            .subscribe(event => {
                if (event) {
                    this.globalS.sToast('Success', 'Profile picture updated')
                    this.refreshProfilePic();
                    this.profilePictureOpen = false;
                    return;
                }
                this.globalS.sToast('Error', 'Process not successful')
            })

        // const req = new HttpRequest('POST', `api/upload/profile`,   this.imgBlobData);

        // this.http.request(req).subscribe(event => {
        //     console.log(event);
        //     this.refreshProfilePic();
        //     this.profilePictureOpen = false;
        // });
    }

    profilePictureChange() {
        this.src = this.refreshDynamicPicture(this.user)
        this.profilePictureOpen = true;
    }

    onChangeUploadTemp(e) {

        e.preventDefault();

        var file = (e.target.files).length > 0
            ? e.target.files[0]
            : null;

        this.src = file;

        // var formData = new FormData();
        // formData.set('files', this.files)
        // // for (var file of this.files)
        // //     formData.append(file.name, file);

        // let params = new HttpParams();

        // const options = {
        //   params: params,
        //   reportProgress: true,
        // };

        // const req = new HttpRequest('POST', `api/upload/profile/temporary`,formData, options);
        // this.loadUrl = true;
        // this.http.request(req).subscribe(({ body }:any) => {
        //     if(body){
        //         this.src = `../../../../media/temporary/${body[0].fileName}`
        //         this.loadUrl = false;
        //     }
        // });
    }

    errorUrl(event: any) {
        this.backgroundImage = "assets/logo/profile.png"
    }


}
