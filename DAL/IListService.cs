using System.Threading.Tasks;
using Adamas.Models.Modules;
using System;
using Adamas.Models.DTO;
using AdamasV3.Models.Dto;

namespace Adamas.DAL
{
    public interface IListService
    {
        Task<QuoteMergeFields> GetRecipientQuoteMergeFields(string personID);
        Task<bool> DeleteDocumentTempFiles();
        Task<String> CheckQuoteDocument(QuoteHeaderDTO quote);
        Task<int> PostCaseStaff(CaseStaffDto details);
        Task<bool> DeleteCaseStaff(int recordNo);
    }
}