import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import format from 'date-fns/format';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { formatDate } from '@angular/common';
import { BillingService, TimeSheetService, ListService, GlobalService,  MenuService, PrintService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
    selector: 'app-hcp-claim-preparation',
    templateUrl: './hcpClaimPreparation.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
.btn1 {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}

`]
})

export class HCPClaimPreparation implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    
    loading: boolean = false;
    inputForm: FormGroup;
    userRole: string = "userrole";
    tocken: any;
    dateFormat: string = 'dd/MM/yyyy';
    
    processing: boolean;
    JSONData: any;
    tableData : Array<any> = [];

    
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private timeS: TimeSheetService,
        private printS:PrintService,
        private ModalS:NzModalService,
        private sanitizer: DomSanitizer,
    ) { }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.buildForm();
        // this.loadData();
        this.loading = false;
    }
    
    buildForm() {
        this.inputForm = this.formBuilder.group({
            receiptDate: new Date(),  
        });
    }

    handleCancel() {
        this.inputForm.reset();
        this.buildForm();
        this.isVisible = false;
        this.router.navigate(['/admin/hcp']);
    }

    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }
    convertFile(e) {
    }

    runProcess() {
        console.log('success');
    }
    
    print() {
        
        console.log('success');
    }
    generatePdf(){
      
        
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Claim Preparations",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Claim Preparations "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
}
