using System;

namespace Adamas.Models.Modules
{
    public class DboGetActivities
    {
        public string Recipient { get; set; }
        public string Program { get; set; }
        public bool ForceAll { get; set; } = false;
        public string MainGroup { get; set; }
        public string SubGroup { get; set; }
        public string ViewType { get; set; }
        public DateTime? Date { get; set; }
          public string StartTime { get; set; }
            public string EndTime { get; set; }
        public string Duration { get; set; }
        



        // EXECUTE [dbo].[GetActivities] 
        // @Recipient = '''ABBOTS MORGANICA''',
        // @Program = '''CSTDA ECI - 30501''',
        // @ForceAll = 0,
        // @MainGroup = '',
        // @SubGroup = '',
        // @ViewType		 = '',
        // @AllowedDays		= '',
        // @Duration		= ''

    }
}