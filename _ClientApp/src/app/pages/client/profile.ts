import { Component, OnInit } from '@angular/core';
import { GlobalService, } from '../../services/index';

const toast: any = {
    success: false,
    fail: false
};

@Component({
    templateUrl: './profile.html',
})

export class ProfileClient implements OnInit {

    private token: any;
    user: any = "";

    constructor(
        private globalS: GlobalService,
    ){  }

    ngOnInit(){
        this.token = this.globalS.decode()
        this.user = {
            code: this.token.code,
            view: 'recipient'
        };
    }

}
