﻿namespace AdamasV3.Helper
{
    public class Helper
    {
    }

    public static class Env
    {
        public static readonly string Development = "Development";
        public static readonly string Production = "Production";
        public static readonly string Staging = "Staging";
    }
}
