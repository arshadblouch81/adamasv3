import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef,HostBinding } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,functionalStatus,leaveTypes, ClientService, dateFormat, dataSetDropDowns } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';


@Component({
    selector: '',
    styles: [`
     nz-select {
            width: 100%;
            background-color: #cbe8f7 !important;
        }

     input{
        background-color: #cbe8f7;
    }

        input:focus {
            background-color: #85B9D5;
        }   

        nz-select:focus {
            background-color: #85B9D5;
        }  

        .row{
            margin:3px;
        }
    nz-tabset{
        margin-top:1rem;
    }
    .ant-divider-horizontal.ant-divider-with-text-center, .ant-divider-horizontal.ant-divider-with-text-left, .ant-divider-horizontal.ant-divider-with-text-right {
        margin:1px 0
    }
    nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
        height: 25px !important;
        font-size: 13px !important;
    }
    
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
        line-height: 24px;
        height: 25px;
        border-radius:15px 4px 0 0;
        margin:0 -10px 0 0;
    }
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
        background: #85B9D5;
        color: #fff;
    }
    :host ::ng-deep nz-select {
        width:100%;
        background-color: #cbe8f7 !important;
    }
  
    :host ::ng-deep .custom-background .ant-select-selector   {
      background-color: #cbe8f7 !important;
    }

    :host ::ng-deep  .ant-select-selection  {
      background-color: #cbe8f7 !important;
    }

    `],
    templateUrl: './chspdex.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class CHSPDEX implements OnInit, OnDestroy {
    @HostBinding('class.custom-background') applyCustomBackground = true;

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    dateFormat: string = dateFormat;
    loading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    functionalStatus = functionalStatus;
    dataSetDropDowns=dataSetDropDowns;
    tableData: Array<any> = [];
    referalPurposes: Array<any> = dataSetDropDowns.referalPurpose;
    referaltypes: Array<any> =["External", "Internal"]
    primaryReasons: Array<any> = dataSetDropDowns.primaryReasons;
    exitreasons: Array<any> = dataSetDropDowns.exitreasons;
    householdCompositions: Array<any> = dataSetDropDowns.householdCompositions;
    accomodations: Array<any> = dataSetDropDowns.accomodations;
    
    branches: Array<any> = [];

    private default: any = {
        recordNumber: '',
        personID: '',
        branch: null,
        notes: ''
    }

    carerRecipientList: any;
    carerRelationshipList: any;
    carerAvailabilityList: any;
    carerResidencyList: any;
    genderlist: any;
    countries: any;
    languages: any;
    indigniousStatus: any;
    livingArrangemnts: any;
    accomodationSetting: any;
    pensionAll: any;
    dvaCardStatus: any;
    referalSource: any;
    today:any;
    IncomeSource: any;
    IncomeFrequency: any;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/recipient/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'qccsmds')) {
                    this.user = data;
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
            this.today= new Date();
            this.user = this.sharedS.getPicked();
            this.search(this.user);
            this.getGeneralDataLists(this.user);
            this.buildForm();
            this.sharedS.emitSaveAll$.subscribe(data => {
                if (data.type=='Recipient' &&  data.tab=='Datasets'){
                    if (this.globalS.isCurrentRoute(this.router, 'chspdex')) 
                         this.save();
                }

              })
        }   
        tabFindIndexScope: number = 0;
        view(index: number){
            if(index == 2){
                this.getCarerDataLists();
            }
            this.tabFindIndexScope = index;
        }
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            this.inputForm = this.formBuilder.group({
                personID: '',
                recordNumber: 0,
                countryOfBirth:'',
                homeLanguage:'',
                cstdA_OtherDisabilities:'',
                dexReferralSource:'',
                dexIndiginousStatus:'',
                dexdvaCardholderStatus:'',
                dexAccomodation:'',
                referralPurpose:'',
                referralType:'',
                assistanceReason:'',
                assistanceReasons:'',
                exitReasonCode:'',
                hasCarer:'',
                hasDisabilities:'',
                firstArrivalYear:'',
                firstArrivalMonth:'',
                visaCategory:'',
                ancestry:'',
                xitReasonCode:'',
                isHomeless:'',
                householdComposition:'',
                mainIncomSource:'',
                incomFrequency:'',
                incomeAmount:'',
                ageAppropriateDevelopment :false ,
                communityParticipation   : false,
                education  :  false,
                familyFunctioning  : false,
                housing  :  false,
                material  :  false,
                mental  :  false,                
                moneyManagement  : false,
                personalSafety  :  false,
                physicalHealth  : false,
                unknown  :  false,
                intellectualLearning  :  false,
                psychiatric  :  false,
                sensory  :  false,
                physical  :  false,
                inadequatelyDescribed  :  false,
                infoConsent :  false,
                contactConsent :  false
        });
        }
        
        get_AssistanceReasons(){

            let bitStr = `${this.inputForm.value.ageAppropriateDevelopment ? '1' :'0'}${this.inputForm.value.communityParticipation ? '1' : '0' }${this.inputForm.value.education ? '1' : '0' }${this.inputForm.value.familyFunctioning ? '1' : '0'}${this.inputForm.value.housing ? '1' : '0'}${this.inputForm.value.material ? '1' : '0'}${this.inputForm.value.mental ? '1' : '0'}${this.inputForm.value.moneyManagement ? '1' : '0'}${this.inputForm.value.personalSafety ? '1' : '0'}${this.inputForm.value.physicalHealth ? '1' : '0'}${this.inputForm.value.unknown ? '1' : '0'}`;
            

            this.inputForm.patchValue({ assistanceReasons:bitStr});

        }
        set_AssistanceReasons(){

            let str=this.inputForm.value.assistanceReasons
            
            this.inputForm.patchValue ({
                ageAppropriateDevelopment  : str.charAt(0)=='1'? true : false,
                communityParticipation  : str.charAt(1)=='1'? true : false,
                education  : str.charAt(2)=='1'? true : false,
                familyFunctioning  : str.charAt(3)=='1'? true : false,
                housing  : str.charAt(4)=='1'? true : false,
                material  : str.charAt(5)=='1'? true : false,
                mental  : str.charAt(6)=='1'? true : false,                
                moneyManagement  : str.charAt(7)=='1'? true : false,
                personalSafety  : str.charAt(8)=='1'? true : false,
                physicalHealth  : str.charAt(9)=='1'? true : false,
                unknown  : str.charAt(10)=='1'? true : false
            });
        }

        get_cstda_OtherDisabilities(){

            let bitStr = `${this.inputForm.value.intellectualLearning ? '1' :'0'}${this.inputForm.value.psychiatric ? '1' : '0' }${this.inputForm.value.sensory ? '1' : '0' }${this.inputForm.value.physical ? '1' : '0'}${this.inputForm.value.inadequatelyDescribed ? '1' : '0'}`;

            this.inputForm.patchValue({cstdA_OtherDisabilities:bitStr});

        }
        set_cstda_OtherDisabilities(){

            let str=this.inputForm.value.cstdA_OtherDisabilities
            
            this.inputForm.patchValue ({
                intellectualLearning  : str.charAt(0)=='1'? true : false,
                psychiatric  : str.charAt(1)=='1'? true : false,
                sensory  : str.charAt(2)=='1'? true : false,
                physical  : str.charAt(3)=='1'? true : false,
                inadequatelyDescribed  : str.charAt(4)=='1'? true : false
               
            });
        }

        search(user: any = this.user) {
            this.timeS.getdatasetchsp(user.id)
            .subscribe(data => {
                this.inputForm.patchValue(data);    
                this.set_cstda_OtherDisabilities();  
                this.set_AssistanceReasons();      
        });
        this.listS.gethaccconsent(user.id)
        .subscribe(data => {
            this.inputForm.patchValue(data);    
               
    });
        

        }
        
        listDropDown(user: any = this.user) {
            this.branches = [];
            this.listS.getintakebranches(user.id)
            .subscribe(data => this.branches = data)

           
        }
        getGeneralDataLists(user: any = this.user){
            this.cd.reattach();
            this.loading = true;
            
            this.IncomeSource= dataSetDropDowns.IncomeSource;
            this.IncomeFrequency= dataSetDropDowns.IncomeFrquency;
            
            // this.listS.GetHaccSex()
            // .subscribe(data => {
            //     this.genderlist = data;
            // })
            
            // this.listS.GetLanguages()
            // .subscribe(data => {
            //     this.languages = data;
            // })
            return forkJoin([
                this.listS.GetHaccSex(),
                this.listS.GetLanguages(),
                this.listS.GetIndigniousStatus(),
                this.listS.GetLivingArrangments(),
                this.listS.GetAccomodationSetting(),
                this.listS.getpensionall(),
                this.listS.GetHACCVaCardStatus(),
                this.listS.GetHACCReferralSource(),
                this.listS.GetCountries(),

            ]).subscribe(x => {
                this.genderlist             = x[0];
                this.languages              = x[1];
                this.indigniousStatus       = x[2];
                this.livingArrangemnts      = x[3];
                this.accomodationSetting    = x[4];
                this.pensionAll             = x[5];
                this.dvaCardStatus          = x[6];
                this.referalSource          = x[7];
                this.countries              = x[8];
                this.loading = false;
                this.cd.detectChanges();
            });
        }
        getCarerDataLists(){
            return forkJoin([
                this.listS.GetCarerDataRecipientcarer(),
                this.listS.GetCarerDataRelationship(),
                this.listS.GetCarerDataAvailability(),
                this.listS.GetCarerDataResidency(),
            ]).subscribe(x => {
                this.carerRecipientList     = x[0];
                this.carerRelationshipList  = x[1];
                this.carerAvailabilityList  = x[2];
                this.carerResidencyList     = x[3];
            });
        }
        save() {
            
           
            
            this.get_cstda_OtherDisabilities();
            this.get_AssistanceReasons();
            this.inputForm.controls['personID'].setValue(this.user.id);
            
           
                this.timeS.updatedatasetchsp(this.inputForm.value,this.user.id)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Information saved Successfully');
                    //this.search();
                   
                });
            }

        handleCancel() {
            this.modalOpen = false;
            this.loading = false;
            this.inputForm.reset(this.default);
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        getYears(){
            let years : Array<any>=[];

            for (let i = 1900; i <= this.today.getFullYear(); i++) {
                years.push(""+i);
            }
            return years;
        }
        getMonths(){
            // let months : Array<any>=[
            //     {value:1,month:'January'},
            //     {value:2,month:'February'},
            //     {value:3,month:'March'},
            //     {value:4,month:'April'},
            //     {value:5,month:'May'},
            //     {value:6,month:'June'},
            //     {value:7,month:'July'},
            //     {value:8,month:'August'},
            //     {value:9,month:'September'},
            //     {value:10,month:'October'},
            //     {value:11,month:'November'},
            //     {value:12,month:'December'},
            // ];
            let months : Array<any>=["1","2","3","4","5","6","7","8","9","10","11","12"];
            return months;

        }
        showAddModal() {
            this.addOREdit = 1;
            this.listDropDown();
            this.modalOpen = true;
        }
        
        showEditModal(index: number) {
            this.addOREdit = 2;
            const { branch, recordNumber, notes } = this.tableData[index];
            this.inputForm.patchValue({
                recordNumber,
                branch,
                notes
            });
            
            this.modalOpen = true;
        }
        
        delete(index: number) {
            const { recordNumber } = this.tableData[index];
            this.timeS.deletebranches(recordNumber)
            .subscribe(data => {
                this.globalS.sToast('Success', 'Branch Deleted');
                this.search();
            })
        }
    }