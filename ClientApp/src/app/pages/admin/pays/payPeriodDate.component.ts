import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { takeUntil, timeout } from 'rxjs/operators';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { DatePipe } from '@angular/common'
import { formatDate } from '@angular/common';
import addYears from 'date-fns/addYears';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
  selector: 'app-billing',
  templateUrl: './payPeriodDate.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  },
  textarea{
    resize:none;
  },
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  .header{
      background-color: #85B9D5;
      border-color: #85B9D5;
      color: #fff;
      padding: 10px;
  }
  .header-dark-orange{
      line-height: 1;
      padding: 10px;
      background-color: #f18805;
      color: #fff;
      border-color: #f18805;
  }
  .header-orange{
      color: #fff;
      padding: 10px;
      line-height: 1;
      background-color: #ffba08;
      border-color: #ffba08;
  }
  .uns-button{
        font-size: 10pt; 
        text-align: center;
        background-color: #85B9D5;
        padding: 3px; 
        height: 1.75pc; 
        width: 5.5pc;
  } 
  .sav-button{
        font-size: 14pt;
        margin-top: 10px; 
        height: 2.4pc; 
        float: right;
        text-align: center;
        background-color: #85B9D5;
        padding: 3px; 
        width: 7pc;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: #85B9D5 !important;
    border-color: #000000 !important;
  }     
  `]
})
export class PayPeriodDateComponent implements OnInit {

  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  // dateFormat: string = 'dd/MM/yyyy';
  dateFormat: string = 'dd-MM-yyyy';
  title: string = "Set Pay Period End Date";
  //  
  token: any;
  check: boolean = false;
  userRole: string = "userrole";
  // whereString: string = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  lastMonthEndDate: any;
  private unsubscribe: Subject<void> = new Subject();
  date: moment.MomentInput;
  updatedRecords: any;
  payPeriodEndDate: any;
  tSheetFirstServiceDate: any;
  tSheetLastServiceDate: any;
  confirmModal?: NzModalRef;
  lockpayPeriodEndDate: any = true;
  locktSheetFirstServiceDate: any = true;
  locktSheetLastServiceDate: any = true;
  lockChangeButton: any = false;
  lockSaveButton: any = true;
  vPayPeriodEndDate: any;
  vStartDate: any;
  vEndDate: any;

  constructor(
    private router: Router,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private billingS: BillingService,
    private listS: ListService,
    private modal: NzModalService,
    public datepipe: DatePipe,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.token.role;
    this.buildForm();
    this.getDates();
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  resetModal() {
    this.current = 0;
    this.inputForm.reset();
  }
  handleCancel() {
    this.modalOpen = false;
    this.router.navigate(['/admin/pays']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      // payPeriodEndDate: '',
      // tSheetFirstServiceDate: '',
      // tSheetLastServiceDate: '',
      payPeriodEndDate: '',
      tSheetFirstServiceDate: '',
      tSheetLastServiceDate: '',

      //payPeriodEndDate: null,
      //tSheetFirstServiceDate: null,
      //tSheetLastServiceDate: null

      // payPeriodEndDate: startOfMonth(new Date()),
      // tSheetFirstServiceDate: startOfMonth(new Date()),
      // tSheetLastServiceDate: endOfMonth(new Date()),
    });
  }

  getDates() {
    this.billingS.getSysTableDates().subscribe(data => {
      this.payPeriodEndDate = data[0].payPeriodEndDate;
      this.tSheetFirstServiceDate = data[0].tSheetFirstServiceDate;
      this.tSheetLastServiceDate = data[0].tSheetLastServiceDate;

      
      // console.log("test1="+this.payPeriodEndDate)
      // console.log("test1="+this.tSheetFirstServiceDate)
      // console.log("test1="+this.tSheetLastServiceDate)
      
      // this.payPeriodEndDate = formatDate(this.payPeriodEndDate, 'dd-MM-yyyy hh:mm', 'en_US');

      // this.payPeriodEndDate = formatDate(this.payPeriodEndDate, 'dd-MM-yyyy', 'en_US');
      // this.tSheetFirstServiceDate = formatDate(this.tSheetFirstServiceDate, 'dd-MM-yyyy', 'en_US');
      // this.tSheetLastServiceDate = formatDate(this.tSheetLastServiceDate, 'dd-MM-yyyy', 'en_US');

      // console.log("test2="+this.payPeriodEndDate)
      // console.log("test2="+this.tSheetFirstServiceDate)
      // console.log("test2="+this.tSheetLastServiceDate)
      // console.log("test2="+this.payPeriodEndDate)
      // console.log("test2="+this.tSheetFirstServiceDate)
      // console.log("test2="+this.tSheetLastServiceDate)

      this.inputForm.patchValue({
        payPeriodEndDate: this.payPeriodEndDate,
        tSheetFirstServiceDate: this.tSheetFirstServiceDate,
        tSheetLastServiceDate: this.tSheetLastServiceDate
      })
    });
  }

  // getDates() {
  //   this.billingS.getSysTableDates().subscribe(data => {
  //     this.payPeriodEndDate = formatDate(data[0].payPeriodEndDate, 'dd-MM-yyyy', 'en_US');
  //     this.tSheetFirstServiceDate = formatDate(data[0].tSheetFirstServiceDate, 'dd-MM-yyyy', 'en_US');
  //     this.tSheetLastServiceDate = formatDate(data[0].tSheetLastServiceDate, 'dd-MM-yyyy', 'en_US');
  //     console.log(this.payPeriodEndDate)
  //     console.log(this.tSheetFirstServiceDate)
  //     console.log(this.tSheetLastServiceDate)
  //     this.inputForm.patchValue({
  //       payPeriodEndDate: this.payPeriodEndDate,
  //       tSheetFirstServiceDate: this.tSheetFirstServiceDate,
  //       tSheetLastServiceDate: this.tSheetLastServiceDate
  //     })
  //   });
  // }

  checkValidation() {
    // this.dtpEndDate = this.inputForm.get('dtpEndDate').value;
    // this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy-MM-dd hh:mm', 'en_US');
    //   this.closeRosterPeriod();
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Traccs User Message',
      nzContent: 'The Pay Period End Date is stamped against all roster/timesheet entries when you approve them in Timesheet Entry. Make sure you have finished all prior approvals and Timesheet Updates before changing the date. <br> Do you want to continue ..??',
      nzOnOk: () => {
        this.lockpayPeriodEndDate = false;
        this.locktSheetFirstServiceDate = false;
        this.locktSheetLastServiceDate = false;
        this.lockChangeButton = true;
        this.lockSaveButton = false;
      },
      nzOnCancel: () => {
        this.lockpayPeriodEndDate = true;
        this.locktSheetFirstServiceDate = true;
        this.locktSheetLastServiceDate = true;
        this.lockChangeButton = false;
        this.lockSaveButton = true;
      }
    });
  }

  // closeRosterPeriod() {
  //   this.billingS.closeRosterPeriod({
  //     Closedate: this.dtpEndDate,
  //   }).pipe(
  //     takeUntil(this.unsubscribe)).subscribe(data => {
  //       if (data) {
  //         this.updatedRecords = data[0].updatedRecords
  //         if (this.updatedRecords == 0) {
  //           this.globalS.iToast('Information', 'No Roster Entries are existing to Close Date.')
  //         } else {
  //           this.globalS.sToast('Success', 'Close Roster Date has been updated successfully on ' + this.updatedRecords + ' Records.')
  //         }
  //         this.ngOnInit();
  //         return false;
  //       }
  //     });
  // }
  checkedStatus() {
    this.vPayPeriodEndDate = formatDate(this.payPeriodEndDate, 'yyyy-MM-dd', 'en_US');
    this.vStartDate = formatDate(this.tSheetFirstServiceDate, 'yyyy-MM-dd', 'en_US');
    this.vEndDate = formatDate(this.tSheetLastServiceDate, 'yyyy-MM-dd', 'en_US');
  }

  closeRosterPeriod() {
    this.checkedStatus();
    // console.log(this.vPayPeriodEndDate);
    // console.log(this.vStartDate);
    // console.log(this.vEndDate);
    this.billingS.setPayPeriodDate({
      PayPeriodEndDate: this.vPayPeriodEndDate,
      StartDate: this.vStartDate,
      EndDate: this.vEndDate
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.eToast('Error', 'The Date(s) you have been entered is in an invalid format. Please re-enter in dd/mm/yyyy format !.')
          } else {
            this.globalS.sToast('Success', 'Respective Date(s) have been updated successfully.')
          }
          this.lockpayPeriodEndDate = true;
          this.locktSheetFirstServiceDate = true;
          this.locktSheetLastServiceDate = true;
          this.lockChangeButton = false;
          this.lockSaveButton = true;
          return false;
        }
      });
  }
}

