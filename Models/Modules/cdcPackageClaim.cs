using System;

namespace Adamas.Models.Modules
{
    public class cdcPackageClaim {
      public string ClaimDescription { get; set; }
      public string ClaimUser1 { get; set; }
      public string ClaimRecordNumber { get; set; }

    }
}