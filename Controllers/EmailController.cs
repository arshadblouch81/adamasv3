using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;

using System.Net.Mail;
using System.Net;

using MailKit;
using MailKit.Security;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

using AdamasV3.Models.Modules;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    public class EmailController : Controller, IEmailService
    {
        private readonly IEmailConfiguration _emailConfiguration;
        public EmailController(IEmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }
         [HttpPost, Route("email")]
        public async Task<EmailOutput> SendEmail([FromBody] MimeMessage message){
        return await Task.Run(() => Send(message));
        }

        [HttpPost, Route("sendemail")]
        public async Task<EmailOutput> SendEmailWithAttachment([FromBody] EmailMessage emailDto)
        {
            var message = new MimeMessage();
             message.From.Add(new MailboxAddress("", emailDto.FromAddress.Address));
        // Set To
        foreach (var to in emailDto.ToAddress.Select(x => x.Address))
        {
            message.To.Add(new MailboxAddress("", to));
        }

        if (emailDto.CCAddress != null)
        {
            // Set CC
            foreach (var to in emailDto.CCAddress.Select(x => x.Address))
            {
                message.Cc.Add(new MailboxAddress("", to));
            }
        }

        // Set Subject
        message.Subject = emailDto.Subject;

        // Set Body
        var bodyBuilder = new BodyBuilder { HtmlBody = emailDto.Body };

        // Add Attachments if any
        if (emailDto.Attachments != null)
        {
            foreach (var attachment in emailDto.Attachments)
            {

               var base64Data= attachment.ContentBase64;
                if (base64Data.Contains(","))
                {
                    base64Data = base64Data.Substring(base64Data.IndexOf(",") + 1); // Remove everything before the comma
                }
                var bytes = Convert.FromBase64String(base64Data);
                bodyBuilder.Attachments.Add(attachment.FileName, bytes);
            }
        }

        message.Body = bodyBuilder.ToMessageBody();
            return await Task.Run(() => Send(message));
        }

        [HttpPost, Route("test-email")]
        public async Task<IActionResult> TestEmail([FromBody] MailRequest mailRequest)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(_emailConfiguration.SmtpOriginName, _emailConfiguration.SmtpUsername));

                var toAddresses = mailRequest.To.Select(x => new MailboxAddress(x.Name, x.Email));

                message.To.AddRange(toAddresses);

                message.Subject = mailRequest.Subject;

                message.Body = new TextPart("plain")
                {
                    Text = mailRequest.Body
                };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    client.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, SecureSocketOptions.StartTls);

                    // Note: only needed if the SMTP server requires authentication
                    client.Authenticate(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                    client.Send(message);
                    client.Disconnect(true);
                }
            } catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(true);
        }

        private async Task<EmailOutput> Send(MimeMessage message, bool replyToAll = true)
        {
            try            
            {
               using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                {
                    //emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, SecureSocketOptions.StartTls);      

                    await emailClient.AuthenticateAsync(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                    await emailClient.SendAsync(message).ConfigureAwait(false);
                    await emailClient.DisconnectAsync(true).ConfigureAwait(false);
                    

                    return new EmailOutput()
                    {
                        ReplyTo = message.ReplyTo.ToString(),
                        CC = message.Cc.ToString(),
                        To = message.To.ToString(),
                        From = message.From.ToString(),
                        Status = new IsSuccessEmail()
                        {
                            IsSuccess = true
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return new EmailOutput()
                {
                    ReplyTo = message.ReplyTo.ToString(),
                    CC = message.Cc.ToString(),
                    To = message.To.ToString(),
                    From = message.From.ToString(),
                    Status = new IsSuccessEmail()
                    {
                        IsSuccess = false,
                        Log = ex.ToString()
                    }
                };
            }
        }

  private async Task<EmailOutput> SendEmail(MimeMessage message, bool replyToAll = true)
        {
            try            
            {
                
                using (var emailClient =  new System.Net.Mail.SmtpClient("smtp.office365.com"))
                {
                    emailClient.Port = 587; // Commonly used port for SMTP
                    emailClient.Credentials = new NetworkCredential("jobs@adamas.net.au", "W1nt3r$D@y");
                    emailClient.EnableSsl = true ;// Enable SSL for secure email sending

                    MailMessage mail = new MailMessage
                    {
                        From = new MailAddress("jobs@adamas.net.au"),
                        Subject = message.Subject,
                        Body =  message.Body.ToString(),
                    };

                    // Add the recipient
                   
                    mail.To.Add( message.To.ToString());
                    mail.To.Add(message.Cc.ToString());
                     mail.To.Add(message.ReplyTo.ToString());

                    // Send the email
                     emailClient.Send(mail);
                            
                    return new EmailOutput()
                    {
                        ReplyTo = message.ReplyTo.ToString(),
                        CC = message.Cc.ToString(),
                        To = message.To.ToString(),
                        From = message.From.ToString(),
                        Status = new IsSuccessEmail()
                        {
                            IsSuccess = true
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                return new EmailOutput()
                {
                    ReplyTo = message.ReplyTo.ToString(),
                    CC = message.Cc.ToString(),
                    To = message.To.ToString(),
                    From = message.From.ToString(),
                    Status = new IsSuccessEmail()
                    {
                        IsSuccess = false,
                        Log = ex.ToString()
                    }
                };
            }
        }
   [HttpPost, Route("pemail")]
        public async Task<EmailOutput> PowerShellEmail([FromBody] MimeMessage message){
        return await Task.Run(() => SendPEmail(message));
        }
    private async Task<EmailOutput> SendPEmail(MimeMessage message, bool replyToAll = true)
    {
        try
        {
            // Set up the SMTP client
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient("smtp.office365.com")
            {
                Port = 587, // Commonly used port for SMTP
                Credentials = new NetworkCredential("jobs@adamas.net.au", "W1nt3r$D@y"),
                EnableSsl = true // Enable SSL for secure email sending
            };

            // Create the email message
            MailMessage mail = new MailMessage
            {
                From = new MailAddress("jobs@adamas.net.au"),
                Subject = message.Subject,
                Body = "This is a test email sent programmatically using SMTP by Arshad.",
            };

            // Add the recipient
          
              mail.To.Add("arshadblouch81@gmail.com");

            // Send the email
            smtpClient.Send(mail);
           // Console.WriteLine("Email sent successfully.");
            return  new EmailOutput()
                    {
                        ReplyTo = message.ReplyTo.ToString(),
                        CC = message.Cc.ToString(),
                        To = message.To.ToString(),
                        From = message.From.ToString(),
                        Status = new IsSuccessEmail()
                        {
                            IsSuccess = true
                        }
                    };;
        }
        catch (Exception ex)
        {
           
            return new EmailOutput()
                {
                    ReplyTo = message.ReplyTo.ToString(),
                    CC = message.Cc.ToString(),
                    To = message.To.ToString(),
                    From = message.From.ToString(),
                    Status = new IsSuccessEmail()
                    {
                        IsSuccess = false,
                        Log = ex.ToString()
                    }
                };
        }
    }
    }

    public interface IEmailService
    {
        Task<EmailOutput> SendEmail(MimeMessage message);
    }

}
