using System;

namespace Adamas.Models.Tables {
    public class AuditHistory
    {
        public string Operator { get; set; }
        public DateTime? ActionDate { get; set; }
        public string ActionOn { get; set; }
        public string WhoWhatCode { get; set; }
        public string TraccsUser { get; set; }
        public string AuditDescription { get; set; }
    }
}    