using System.Collections;
namespace Adamas.Models.Modules
{
    public class CompetencyObject
    {
      public string Description { get; set; }
      public bool Selected { get; set; }
      public string Domain { get; set; }
    }
}