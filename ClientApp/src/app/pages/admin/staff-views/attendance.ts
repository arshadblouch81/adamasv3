import { Component, OnInit, OnDestroy, Input } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
    styles: [`
    ul{
        list-style:none;
    }
    
    div.divider-subs div{
        margin-top:2rem;
    }
    label{
        margin-bottom:15px !important;
    }
    .ant-checkbox-wrapper {
        margin-bottom:15px !important;
    }
    
    `],
    templateUrl: './attendance.html'
})


export class StaffAttendanceAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    
    checked: boolean = false;
    isDisabled: boolean = false;
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService
        ) {
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'staff-time-attendance')) {
                    this.search(data);
                }
            });        
        }
        
        ngOnInit(): void {
            this.user = this.sharedS.getPicked();
            if(this.user){
                this.search(this.user);
                this.buildForm();
                
                this.sharedS.emitSaveAll$.subscribe(data => {

                    if(data.type = 'Staff' && (data.tab = 'Roster & Mobile App')){
                        this.SaveAll_Changes(data);
                    }
                })
                
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        SaveAll_Changes(type:any){
            if (this.inputForm && this.inputForm.dirty) {
                this.save();
            }
        }
        search(user: any) {
            if (!user) this.router.navigate(['/admin/staff/personal']);
            this.timeS.getattendancestaff(user.id).subscribe(data => {
                this.patchData(data);
            })
        }
        
        patchData(data: any) {
            this.inputForm.patchValue({
                autoLogout: data.autoLogout,
                emailMessage: data.emailMessage,
                excludeShiftAlerts: data.excludeShiftAlerts,
                excludeFromTravelInterpretation:data.excludeFromTravelInterpretation,
                inAppMessage: data.inAppMessage,
                logDisplay: data.logDisplay,
                pin: data.pin,
                staffTimezoneOffset:data.staffTimezoneOffset,
                rosterPublish: data.rosterPublish,
                shiftChange: data.shiftChange,
                smsMessage: data.smsMessage
            });
        }
        
        
        buildForm() {
            this.inputForm = this.formBuilder.group({
                emailMessage: false,
                excludeShiftAlerts: false,
                excludeFromTravelInterpretation:false,
                inAppMessage: false,
                logDisplay: false,
                pin: [''],
                autoLogout: [''],
                staffTimezoneOffset:[''],
                rosterPublish: false,
                shiftChange: false,
                smsMessage: false,
                IsRosterable:false,
                emailTimesheet:false,
                excludeFromConflictChecking:false,
                excludeFromLogDisplay:false,
            });
        }
        
        onKeyPress(data: KeyboardEvent) {
            return this.globalS.acceptOnlyNumeric(data);
        }
        
        save() {
            const group = this.inputForm;
            this.timeS.updatetimeandattendance({
                AutoLogout: group.get('autoLogout').value,
                EmailMessage: group.get('emailMessage').value,
                ExcludeShiftAlerts: group.get('excludeShiftAlerts').value,
                ExcludeFromTravelinterpretation: group.get('excludeFromTravelInterpretation').value,
                InAppMessage: group.get('inAppMessage').value,
                LogDisplay: group.get('logDisplay').value,
                Pin: group.get('pin').value,
                StaffTimezoneOffset:group.get('staffTimezoneOffset').value,
                RosterPublish: group.get('rosterPublish').value,
                ShiftChange: group.get('shiftChange').value,
                SmsMessage: group.get('smsMessage').value,
                EmailTimesheet: group.get('emailTimesheet').value,
                ExcludeFromConflictChecking: group.get('excludeFromConflictChecking').value,
                ExcludeFromLogDisplay: group.get('excludeFromLogDisplay').value,
                Id: this.user.id
            }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Change successful');
                    this.inputForm.markAsPristine();
                    return;
                }
            });
        }
        
        canDeactivate() {
            
            if (this.inputForm && this.inputForm.dirty) {
                setTimeout(() => {
                    this.save();
                }, 2000);
            }
            // console.log("-----------------------");
            
            // if(this.getPermisson(79) == '1'){
            // if (this.inputForm && this.inputForm.dirty) {
            //         this.modalService.confirm({
            //             nzTitle: 'Save changes before exiting?',
            //             nzContent: '',
            //             nzOkText: 'Yes',
            //             nzOnOk: () => {
            //         this.save();
            //         },
            //         nzCancelText: 'No',
            //         nzOnCancel: () => {
                    
            //     }
            //     });
            //  }
            // }
            return true;
        }
        getPermisson(index:number){
            var permissoons = this.globalS.getStaffRecordView();
            // console.log("staff "+ permissoons.length );
            return permissoons.charAt(index-1);
        }
    }