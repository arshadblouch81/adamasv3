using System;

namespace Adamas.Models.Modules
{
    public class Suspension
    {
         public string RecordNumber { get; set; }
         public string PersonID { get; set; }         
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PositionID { get; set; }
        public string Notes { get; set; }
        public string NoteType { get; set; }
        
        public bool? PutMasterOnHold { get; set; }
        public bool? RecordFormalAbsence { get; set; }
        public bool? Record24Absence { get; set; }
        public bool? AllServices { get; set; }
        public string Cancellation { get; set; }
        public string Cancellationcode { get; set; }
        public string Category { get; set; }
        public string Reminder { get; set; }
        public DateTime? ReminderDate { get; set; }
        public string SelectedServices { get; set; }
        public string Email { get; set; }
         public string User { get; set; }
          public string WinUser { get; set; }
    
        public string AltProgram { get; set; }
        public string Activity { get; set; }
        public string PayType { get; set; }

        public bool? BillOriginalRate { get; set; }

        public bool? RetainOriginalRate { get; set; }
   
    }
}

  