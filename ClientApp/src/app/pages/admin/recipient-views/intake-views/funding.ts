import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService,fundingDropDowns, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject,EMPTY } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import * as moment from 'moment';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';

export interface TreeNodeInterface {
    id: string;
    date: string;
    description: string;
    usageDetails: string;
    amount: number;
    level?: number;
    expand?: boolean;
   
    children?: TreeNodeInterface[];
    parent?: TreeNodeInterface;
  }
  interface TreeNode {
    id: number;
    name: string;
    children?: TreeNode[];
  }

@Component({
    selector: '',
    styles: [`
    nz-select{
        width:100%
    }
    /* .ant-divider, .ant-divider-vertical{
         margin:4px ​0 !important;
     }
     nz-form-item.ant-form-item{
         margin:0 ​0 0 0 !important;
     }*/

    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top:10px;
           
    }
    .mrg-right{
        display:block;
        position:relative;
        text-align: right;
        right: 0;
        margin-bottom: 10px;
    }

    .tab{
        margin-top: -10px;
        margin-left: 11px;
    }
    .btn {
        background:#85B9D5;
        color:white;
        border: 1px solid #85B9D5;
    }
    nz-menu-item :ngdeep ul > nz-menu >li{
        font-size: 12px;
        font-weight: 400;
    }
    nzDropdownMenu{
        background:#85B9D5; 
    }
    .selected{
        background-color: #85B9D5;
        color: white;
    }
  

    `],
    templateUrl: './funding.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakeFunding implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    dateFormat: string = 'dd/MM/yyyy';
    user: any;
    loading: boolean = false;
    postLoading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    tableData: Array<any> = [];
    fundingData: Array<any> = [];
    fundingData2: Array<any> = [];
    fundingPayments: Array<any> = [];
    packageDetailForm: FormGroup;
    programsNames: any;
    supplements: FormGroup;
    programLevel: any;
    period: string[];
    levels: string[];
    cycles: string[];
    budgetEnforcement: string[];
    alerts: string[];
    DefPeriod: string[];
    expireUsing: string[];
    unitsArray: string[];
    dailyArry: string[];
    visibleRecurrent: boolean = false;
    isMenuOpen :boolean=false;
    packageTerm: string[];
    status: string[];
    type: string[];
    alertCycle: string[];
    fundingprioritylist: any;
    IS_CDC: boolean = true;
    selectedProgram: any;
    activeRowData:any;
    ViewPackage:Subject<any>=new Subject();
    ViewDischarge:Subject<any>=new Subject();
    selectedRowIndex:number;
    OptIn:boolean=true;
    formatterPercent = (value: number): string => `${value} %`;
    parserPercent = (value: string): string => value.replace(' %', '');
    formatterDollar = (value: number): string => `$ ${value}`;
    parserDollar = (value: string): string => value.replace('$ ', '');
    OptionsChecked:Array<boolean> = [false,false,false,false];
    OptionDate:any;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    confirmModal?: NzModalRef;
    showProgramList:boolean=false;
    showDateModal:boolean=false;
    showProgramModel:boolean=false;
    startDate:any = new Date();
    ProgramList:any;
    HighlightRow2:number;
    selectedNewProgram:any;
    originalList:any;
    txtSearch:any;
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef,
        private nzContextMenuService: NzContextMenuService
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/recipient/intake/branches'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'funding')) {
                    this.user = data;
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
          this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
            this.user = this.sharedS.getPicked();
            this.search(this.user);
           // this.buildForm();

          
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        
        getPermisson(index:number){
            var permissoons = this.globalS.getRecipientRecordView();
            return permissoons.charAt(index-1);
          }
          getStaffPermisson(index:number){
            var permissoons = this.globalS.getStaffRecordView();
            return permissoons.charAt(index-1);
          }
          confirm(view: number, data: any): void {
          
            if (view == 1) {
              data = this.activeRowData;
              this.delete(this.selectedRowIndex);
            }
    
        }

        
        showConfirm(): void {
          this.confirmModal = this.ModalS.confirm({
            nzTitle: "TRANSITION PROGRAM/PACKAGE",
            nzContent: ("<span>NB. New package/program and services MUST be already ACTIVE. TRANSITIONING will.... </span><br><br>" +
                          "<span>1. CHANGE the Program and Bill Rate for services in the Master Roster  <br>" +
                          "    AND in the Current Roster ON OR AFTER the Transition Date if they  " +
                          "    are approved against the new Program </span><br><br> " +
                          "<span>2. DELETE services in the Master that are NOT approved under the new  Program  </span> <br><br>" +
                          "Are you sure you want to continue?"),
            nzOnOk: () => {
              this.showProgramList=true;
              this.load_programList() ;
              this.cd.detectChanges();
              new Promise((resolve,reject) => {
                setTimeout(()=>{
                  resolve(1);               
                }, 100)
               
                
            }).then(()=>this.cd.detectChanges()).catch(()=>console.log('error'));
           
             
            },
            nzOnCancel: () => {
              
            },
          });
        }

        load_programList(){
         
        
          let sql =`SELECT DISTINCT rp.Program FROM RecipientPrograms rp WHERE IsNull(rp.ProgramStatus, '') = 'ACTIVE' AND PersonID = '${this.user.id}' AND rp.Program <> '${this.activeRowData.program}'`;
              this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.ProgramList = data.map(x=>x.program);
                this.originalList = this.ProgramList;
                this.cd.detectChanges();
              });
        }

        search(user: any = this.user) {
            this.cd.reattach();
            
            this.loading = true;
            this.timeS.getfunding(user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.loading = false;
                this.tableData = data;
                this.cd.markForCheck();
            });
            // this.dropDowns();

            // let input={
            //     code : user.code,
            //     program: 'HCP-L4-ABBOTS MORGANICA 17102011', //this.selectedProgram,
            //     startDate: '2024/02/02',
            //     endDate : '2024/05/31'
            // }
            // this.timeS.getfundingreport(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            //     this.loading = false;
            //     this.fundingData = this.convertToTree(data);
                
            //     data.forEach(item => {
            //         this.mapOfExpandedData[item.id] = this.convertTreeToList(item);
            //       });
                
            // });
            // this.timeS.getfundingPayments(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            //     this.loading = false;
            //     this.fundingPayments = data;
                
            // });

        }

     

            detectChanges(){
                this.cd.markForCheck();
                this.cd.detectChanges();
            }

         
            selectedItemGroup(data:any, i:number){
                this.selectedRowIndex=i;
                this.activeRowData=data;
              }
            showEditModal(index: number) {
                this.activeRowData = this.tableData[index];
                this.selectedRowIndex=index;
                this.addOREdit = 2;
                let recordNumber= this.activeRowData.recordNumber;

             
               
                this.ViewPackage.next(this.activeRowData);
              //  this.cd.detectChanges();
                

            }
            
            delete(data: any) {
                console.log(data);
                this.timeS.deleteprogramdetails(data.recordNumber).subscribe(data => {
                    this.globalS.sToast('Success', 'Package Deleted');
                    this.search();
                });
            }

            contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, data:any): void {
                this.activeRowData = data;
                this.selectedRowIndex = this.tableData.indexOf(data);
                this.nzContextMenuService.create($event, menu);
              }
            menuclick(event:any,i:number){
                if (i==1){
                  this.showConfirm();
        
                }
            }
            getPayout(){

            }

            handleCancel() {
                this.modalOpen = false;   
            }

            tabFindIndex: number = 0;
            tabFindChange(index: number){
                this.tabFindIndex = index;
                this.IS_CDC=true;
            }

            showAddModal() {
               // this.packageDetailForm = this.formBuilder.group(packageDefaultForm);                
                this.addOREdit = 1;
               this.modalOpen = true;
                this.ViewPackage.next(true);
            }

            domenticaChange(event: any){
                if(event.target.checked){
                    this.supplements.patchValue({
                        levelSupplement : this.programLevel,
                    });
                }else{
                    this.supplements.patchValue({
                        levelSupplement : '',
                    });
                }
            }
            packgChange(e){
            }
            recurrentChange(e){
                if(e.target.checked){
                    this.visibleRecurrent = true;
                }else{
                    this.visibleRecurrent = false;
                }
            }

            print(){
        
            }
            generatePdf(){
      
              var Title = " Fundings "
              const data = {
                  "template": { "_id": "6BoMc2ovxVVPExC6" },
                  "options": {
                      "reports": { "save": false },                        
                      "sql": this.tableData,                        
                      "userid": this.tocken.user,
                      "txtTitle":  Title+" for "+this.user.code,                      
                  }
              }
              this.loading = true;           
                          
              this.drawerVisible = true;
              this.printS.printControl(data).subscribe((blob: any) => {
                          this.pdfTitle = Title+" for "+this.user.code
                          this.drawerVisible = true;                   
                          let _blob: Blob = blob;
                          let fileURL = URL.createObjectURL(_blob);
                          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                          this.loading = false;
                          this.cd.detectChanges();                       
                      }, err => {
                          console.log(err);
                          this.loading = false;
                          this.ModalS.error({
                              nzTitle: 'TRACCS',
                              nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                              nzOnOk: () => {
                                  this.drawerVisible = false;
                              },
                          });
              });
            }
            handleCancelTop(): void {
            this.drawerVisible = false;
            this.loading = false;
            this.pdfTitle = ""
            this.tryDoctype = ""
            }
            close(){
                this.router.navigate(['/admin/recipient/personal']);
            }

            toggleMenu(){
              this.isMenuOpen =!this.isMenuOpen ;  
            }

            log(): void {
                console.log('click dropdown button');
              }
            
              showRows:boolean;
              showId:any;
              show_Rows(item:any){
                this.showRows=!this.showRows;
                this.showId= item.id

              }
              expandSet = new Set<number>();
  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }
            
              mapOfExpandedData: { [id: string]: TreeNodeInterface[] } = {};

              collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
                if (!$event) {
                  if (data.children) {
                    data.children.forEach(d => {
                      const target = array.find(a => a.id === d.id)!;
                      target.expand = false;
                      this.collapse(array, target, false);
                    });
                  } else {
                    return;
                  }
                }
              }
            
              convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {

                
                const stack: TreeNodeInterface[] = [];
                const array: TreeNodeInterface[] = [];
                const hashMap = {};
                stack.push({ ...root, level: 0, expand: false });
            
                while (stack.length !== 0) {
                  const node = stack.pop()!;
                  this.visitNode(node, hashMap, array);
                  if (node.children) {
                    for (let i = node.children.length - 1; i >= 0; i--) {
                      stack.push({ ...node.children[i], level: node.level! + 1, expand: false, parent: node });
                    }
                  }
                }
            
                return array;
              }
            
              visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
                if (!hashMap[node.id]) {
                  hashMap[node.id] = true;
                  array.push(node);
                }
              }
            
              
               convertToTree(flatList: any[]): TreeNode[] {
                const map = new Map<number, TreeNode>();
              
                // First, map each item to its corresponding node
                flatList.forEach(item => {
                  map.set(item.id, { ...item, children: [] });
                });
              
                // Then, connect children to their respective parent nodes
                let finallist = [];
                map.forEach(item => {
                  if (item.id !== null) {
                    const parent = map.get(item.id);
                    if (parent) {
                        var children = flatList.filter(x => x.id === item.id);
                        parent.children=children;

                      finallist.push(parent);
                    }

                  }
                });
              
                // Finally, find and return the root nodes
                return finallist; //flatList.filter(item => item.id === null).map(item => map.get(item.id)!);
              }
              
              allocateNewProgram(){
                
              }
              RecordClicked(event: any, value: any, i: number) {
            
                this.HighlightRow2 =i;
                this.selectedNewProgram=value;
                
              
            }
            
            onItemDbClick(sel:any , i:number) : void {
                this.HighlightRow2=i;
                this.selectedNewProgram=sel;
                this.showProgramList=false;
                this.showDateModal=true;
            }
            onTextChangeEvent(event:any){
                let value = this.txtSearch.toUpperCase();
                this.ProgramList=this.originalList.filter(element=>element.includes(value));
            }
            
            setDate(){
                this.showDateModal=false;
                this.load_programModel();
            }
            load_programModel(){
              let data ={
                  program:this.activeRowData.program,
                  personId: this.user.id,
                  accountNo : this.user.code,
                  startDate: this.startDate,
              }
              this.ViewDischarge.next(data);
            }
         

 }