using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class FilesizeLimit {
        public int Photo { get; set; }
        public int Video { get; set; }
    }

}