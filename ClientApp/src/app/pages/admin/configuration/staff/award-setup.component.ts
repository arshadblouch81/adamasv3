import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit,HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BillingService, GlobalService, ListService ,daytypes,NavigationService,timeSteps,categories,shiftTypes,MenuService, PrintService, TimeSheetService } from '@services/index';
import { SwitchService } from '@services/switch.service';
import { NzModalService } from 'ng-zorro-antd';
import { Subject,forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-award-setup',
  templateUrl: './award-setup.component.html',
  styles: [`
.section {
    margin-top: 15px;
}
    
.row {
    margin-top: 4px;
    display: flex;
    align-items: center;
}
    
.section-title {
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 10px;
}
    
.sub-section-title {
    font-size: 16px;
    font-weight: bold;
    margin-right: 10px;
}
    
.right-align {
    text-align: right;
}
    
.input-small {
    width: 60px;
    margin: 0 5px;
}
    
.select-small {
    width: 130px;
    margin-left: 5px;
}
    
label {
    font-weight: normal;
    margin-right: 5px;
}
    
input[type="text"] {
    width: 50px;
}
.content-tab{
    padding:0px 8% }
  .selecteditem{
        background-color: #e6f7ff;
    }
    .selecteditem td{
        background-color: #e6f7ff;
    }
     tr.highlight {
    background-color: #85B9D5 !important; /* Change to your preferred highlight color */
  }
    
    `]
  })
  export class AwardSetupComponent implements OnInit {
    
    tableData: Array<any>;
    loading: boolean = false;
    modalOpen: boolean = false;
    awardtimebandsallowncesshiftsmodal:boolean=false;
    current: number = 0;
    currentBreak:number = 0;
    inputForm: FormGroup;
    timebandsForm: FormGroup;
    postLoading: boolean = false;
    isUpdate: boolean = false;
    modalVariables:any;
    inputVariables:any;
    dateFormat: string ='dd/MM/yyyy';
    check : boolean = false;
    userRole:string="userrole";
    whereString :string="Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
    title:string = "Add Award Setup"
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    private unsubscribe: Subject<void> = new Subject();
     
    temp_title: any;
    awardListData: any;
    codeList: any;
    selectedRow  : number = 0;
    activeRowData: any;
    timebandsList: any;
    allowncesList: any;
    specialShiftList: any;
    selectedRowAllocance: number = 0;
    activeRowDataAllocance: any;
    selectedshifts: number = 0;
    activeRowDatashifts: any;
    submodalScreen: any;
    subModaltitle: string;
    addOrEdit:number=0;
    workStartHour: Array<any> = timeSteps
    shiftTypes:Array<any> = shiftTypes;
    daytypes: Array<any> =daytypes;
    categories:Array<any>= categories;
    travelPaytypeList: any;
    insertedAwardId:number = 32;
    programsList: any;
    activityList: any;
    allowancesForm: FormGroup;
    specialhiftsForm: FormGroup;
    searchTerm: string = '';
    currentIndex: number | null = null;
    breakdownList: any;
    currentAllowance: number | null = 0;
    ruleList: any;
  allinclusiveactivitesList: any;
  allinclusivecompetencyList: any;
  allexclusivecactivityList: any;
  allexclusivecompetencyList: any;
    
    
    constructor(
      private globalS: GlobalService,
      private cd: ChangeDetectorRef,
      private switchS:SwitchService,
      private timeS:TimeSheetService,
      private billingS:BillingService,
      private listS:ListService,
      private menuS:MenuService,
      private formBuilder: FormBuilder,
      private printS:PrintService,
      private sanitizer: DomSanitizer,
      private ModalS: NzModalService,
      private navigationService: NavigationService,
      private router: Router
    ){}
    
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.loadData();
      this.loading = false;
      this.cd.detectChanges();
    }
    loadTitle(){
      return this.title;
    }
    showAddModal() {
      this.title = "Add Award Setup"
      this.resetModal();

      this.inputForm.patchValue({
        payOvertimeForTeaBreaks :false,
      })
      this.modalOpen = true;
    }
    
    showAddSubMbodal(index: any) {
      
      this.submodalScreen = index;
     
      if(this.submodalScreen == 1){
        this.timebandsForm.reset();
        this.subModaltitle = "Add/Change Timebands";
      }
      if(this.submodalScreen == 2){
        this.allowancesForm.reset();
        this.subModaltitle = "Add/Change Allowances";
      }
      if(this.submodalScreen == 3){
        this.specialhiftsForm.reset;
        this.subModaltitle = "Add/Award Special Shift Detiails";
      }
      
      this.awardtimebandsallowncesshiftsmodal= true;
      
    }
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.current = 0;
      this.insertedAwardId = this.awardListData[index].recordno;

      this.menuS.getawardsetuptimebands(this.insertedAwardId).subscribe(data=>{
        this.timebandsList = data;
      });
      this.menuS.getawardsetupallocances(this.insertedAwardId).subscribe(data=>{
        this.allowncesList = data;
      });
      this.menuS.getawardsetupspecialshifts(this.insertedAwardId).subscribe(data=>{
        this.specialShiftList = data;
      });
      
      
      this.title = "Edit Award Setup"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      
      // Destructuring from the selected data object
      const { 
        code,
        end_date,
        description,
        category,
        level,
        notes,
        basE_PayType,
        week_MaxOrdHr,
        week_MaxOrdHrNotFound,
        fN_MaxOrdHr,
        w4_MaxOrdHours,
        week_MaxOrdDays,
        day_MaxOrdHr,
        fN_MaxOrdDays,
        fN_MaxOrdHrsDay,
        w4_MaxOrdDays,
        w4_MaxOrdHrsDay,
        ordHoursStartTime,
        ordHoursEndTime,
        minHoursPerDay,
        minHoursPerDayDisabled,
        minHoursPerService,
        breakTimeWorked,
        week_MinFreeDays,
        fN_FreeDays,
        w4_FreeDays,
        payOvertimeInsuficientMinDays,
        higherDuttieshour,
        payOvertimeNoMinBreak,
        minBetweenShiftBreak,
        minBreakBetweenSleepovers,
        minBreakPayType,
        day_SatOrdHr_PayType,
        day_SunOrdHr_PayType,
        day_PHOrdHr_PayType,
        overtimeLeeway,
        toilDefaultAccrualMinutes,
        toilDefaultAccrualPaytype,
        day_MaxOrdHrWD1stOTBreak,
        day_MaxOrdHrWD1stOTBreak_PayType,
        day_MaxOrdHrWD2ndOTBreak_PayType,
        day_MaxOrdHrSat1stOTBreak,
        day_MaxOrdHrSat1stOTBreak_PayType,
        day_MaxOrdHrSat2ndOTBreak_PayType,
        day_MaxOrdHrSun1stOTBreak,
        day_MaxOrdHrSun1stOTBreak_PayType,
        day_MaxOrdHrSun2ndOTBreak_PayType,
        day_MaxOrdHrPH1stOTBreak,
        day_MaxOrdHrPH1stOTBreak_PayType,
        day_MaxOrdHrPH2ndOTBreak_PayType,
        week_MaxOrdHrWD1stOTBreak,
        week_MaxOrdHrWD1stOTBreak_PayType,
        week_MaxOrdHrWD2ndOTBreak_PayType,
        fN_MaxOrdHrWD1stOTBreak,
        fN_MaxOrdHrWD1stOTBreak_PayType,
        fN_MaxOrdHrWD2ndOTBreak_PayType,
        disableBrokenShiftAllowance,
        brokenShiftAllowanceExtra_Threshold,
        brokenshiftLimit,
        brokenShiftAllowanceExtra_Threshold1,
        brokenShiftAllowance_PayType,
        unpaidMealBreakMin,
        unpaidMealBreakStartAt,
        unpaidMealBreak_PayType,
        teaBreakPaidTime,
        teaBreakStartAt,
        teaBreakPaidTimeNext,
        teaBreakStartAtNext,
        teaBreakPaidIncWkdHrs,
        payOvertimeForTeaBreaks,
        cancellationPayThreshold,
        makeUp_Pay_Threshold,
        makeUpLeadTime,
        noNoticeCancelPayType,
        shtNoticeCancelPayType,
        kmAllowanceType,
        BASE_PayType,
        recordno 
      } = this.awardListData[index] || {}; // Ensure it’s safe to access the object
      
      // Patching the form with all these values
      this.inputForm.patchValue({
        code,
        end_date,
        description,
        category,
        level,
        notes,
        basE_PayType,
        week_MaxOrdHr,
        week_MaxOrdHrNotFound,
        fN_MaxOrdHr,
        w4_MaxOrdHours,
        week_MaxOrdDays,
        day_MaxOrdHr,
        fN_MaxOrdDays,
        fN_MaxOrdHrsDay,
        w4_MaxOrdDays,
        w4_MaxOrdHrsDay,
        ordHoursStartTime,
        ordHoursEndTime,
        minHoursPerDay,
        minHoursPerDayDisabled,
        minHoursPerService,
        breakTimeWorked,
        week_MinFreeDays,
        fN_FreeDays,
        w4_FreeDays,
        payOvertimeInsuficientMinDays,
        higherDuttieshour,
        payOvertimeNoMinBreak,
        minBetweenShiftBreak,
        minBreakBetweenSleepovers,
        minBreakPayType,
        day_SatOrdHr_PayType,
        day_SunOrdHr_PayType,
        day_PHOrdHr_PayType,
        overtimeLeeway,
        toilDefaultAccrualMinutes,
        toilDefaultAccrualPaytype,
        day_MaxOrdHrWD1stOTBreak,
        day_MaxOrdHrWD1stOTBreak_PayType,
        day_MaxOrdHrWD2ndOTBreak_PayType,
        day_MaxOrdHrSat1stOTBreak,
        day_MaxOrdHrSat1stOTBreak_PayType,
        day_MaxOrdHrSat2ndOTBreak_PayType,
        day_MaxOrdHrSun1stOTBreak,
        day_MaxOrdHrSun1stOTBreak_PayType,
        day_MaxOrdHrSun2ndOTBreak_PayType,
        day_MaxOrdHrPH1stOTBreak,
        day_MaxOrdHrPH1stOTBreak_PayType,
        day_MaxOrdHrPH2ndOTBreak_PayType,
        week_MaxOrdHrWD1stOTBreak,
        week_MaxOrdHrWD1stOTBreak_PayType,
        week_MaxOrdHrWD2ndOTBreak_PayType,
        fN_MaxOrdHrWD1stOTBreak,
        fN_MaxOrdHrWD1stOTBreak_PayType,
        fN_MaxOrdHrWD2ndOTBreak_PayType,
        disableBrokenShiftAllowance,
        brokenShiftAllowanceExtra_Threshold,
        brokenshiftLimit,
        brokenShiftAllowanceExtra_Threshold1,
        brokenShiftAllowance_PayType,
        unpaidMealBreakMin,
        unpaidMealBreakStartAt,
        unpaidMealBreak_PayType,
        teaBreakPaidTime,
        teaBreakStartAt,
        teaBreakPaidTimeNext,
        teaBreakStartAtNext,
        teaBreakPaidIncWkdHrs,
        payOvertimeForTeaBreaks,
        cancellationPayThreshold,
        makeUp_Pay_Threshold,
        makeUpLeadTime,
        noNoticeCancelPayType,
        shtNoticeCancelPayType,
        kmAllowanceType,
        BASE_PayType,
        recordno
      });

      this.temp_title = name;

    }
    
    handleCancel() {
      this.current = 0;
      this.modalOpen = false;
    }
    handleCancelSubModal(){
      this.awardtimebandsallowncesshiftsmodal = false;
    }
    
    view(index: number){
      this.current = index;
    }
    viewBreakDown(index: number){
      this.currentBreak = index;
    }
    viewAllownTabs(index: number){
      this.currentAllowance = index;
    }
    loadData(){
      this.loading = true;
      this.menuS.getDataDomainByType("AWARDLEVEL",this.check).subscribe(data => {
        this.tableData = data;
        this.loading = false;
      });
      
      this.billingS.getAwardRuleList().subscribe(awardRuleList => {
        this.awardListData = awardRuleList;
        this.loading = false;
      });
      
      this.billingS.getTravelPaytype().subscribe(travelPaytypeList=>{
        this.travelPaytypeList =travelPaytypeList;
      })
      
      this.billingS.getCodeList().subscribe(data => {
        this.codeList = data;
      });
      
      this.listS.getcasestaffprograms().subscribe(data=>{
        this.programsList = data;
      })
      
      this.listS.getactivities().subscribe(data=>{
        this.activityList = data;
      })
      
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = "WHERE";
        this.loadData();
      }else{
        this.whereString = "Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
        this.loadData();
      }
    }
    selectedItemGroup(data:any, i:number){
      this.selectedRow=i;
      this.activeRowData=data;
    }
    
    
    trackByFn(index, item) {
      return item.id;
    }
    
    selectedItemallownces(data:any, i:number){
      this.selectedRowAllocance=i;
      this.activeRowDataAllocance=data;
    }
    
    
    trackByFnallownces(index, item) {
      return item.id;
    }
    
    
    selectedItemshifts(data:any, i:number){
      this.selectedshifts = i ;
      this.activeRowDataAllocance=data;
    }
    
    trackByFnshifts(index, item) {
      return item.id;
    }
    
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    }
    saveSubModal(index:any){ 
      
      if(index == 1 && this.addOrEdit ==0){//timeband check for save entry


        this.menuS.postawardsetuptimebands(this.timebandsForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {

        
          this.globalS.sToast('Success', 'TImeband Added Sucesfully!');
        
        });

      }
      if(index == 1 && this.addOrEdit == 1){

        this.menuS.updateawardsetuptimebands(this.timebandsForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        
          this.globalS.sToast('Success', 'TImeband Updated Sucesfully!');

          this.menuS.getawardsetuptimebands(this.insertedAwardId).subscribe(data=>{
            this.timebandsList = data;
          });
        
        });

      }
      if(index == 2 && this.addOrEdit ==0){

        this.menuS.postawardsetupallownces(this.allowancesForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        
          this.globalS.sToast('Success', 'Award Allowance Added Sucesfully!');

        });

      }
      if(index == 2 && this.addOrEdit == 1){

        this.menuS.updateawardsetupallocances(this.allowancesForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        
          this.globalS.sToast('Success', 'Award Allowance Updated Sucesfully!');

          this.menuS.getawardsetupallocances(this.insertedAwardId).subscribe(data=>{
            this.allowncesList = data;
          });
        
        });

      }
      if(index == 3 && this.addOrEdit ==0){

        this.menuS.postawardsetupspecialshifts(this.specialhiftsForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        
          this.globalS.sToast('Success', 'Special Shift Added Sucesfully!');

        });
      }
      if(index == 3 && this.addOrEdit == 1){

        this.menuS.updateawardsetupspecialshifts(this.specialhiftsForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        
          this.globalS.sToast('Success', 'Special Shift Updated Sucesfully!');

          this.menuS.getawardsetupspecialshifts(this.insertedAwardId).subscribe(data=>{
            this.specialShiftList = data;
          });
        
        });

      }
    }
    save() {
      this.postLoading = true;     
      


      

        var brokenshiftLimit = this.inputForm.get('brokenshiftLimit').value;
        this.inputForm.controls["brokenshiftLimit"].setValue(((brokenshiftLimit == null) ? 0 : brokenshiftLimit));

        
        var payOvertimeInsuficientMinDays = this.inputForm.get('payOvertimeInsuficientMinDays').value;
        this.inputForm.controls["payOvertimeInsuficientMinDays"].setValue(((payOvertimeInsuficientMinDays == null) ? 0 : payOvertimeInsuficientMinDays));
        



        if(!this.isUpdate){
          this.menuS.postawardpossetup(this.inputForm.value).pipe(
            takeUntil(this.unsubscribe))
            .subscribe(data => {
              this.loadData();
              this.globalS.sToast('Success', 'Data added');
                
            })
        }
        else
        {
            
          const group = this.inputForm;

          this.menuS.updateawardpossetup(this.inputForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.loadData();
            this.globalS.sToast('Success', 'Data updated');
          }, error => {
              // Handle error response here
          });


          this.postLoading = false;          
          this.isUpdate = false;
        }

      this.loadData();
      this.handleCancel();
      this.resetModal();
  }
  
  delete(data: any) {
    this.billingS.deleteAwardRuleSet(data.recordNo)
    .pipe(takeUntil(this.unsubscribe)).subscribe(dataDeleted => {
      if (dataDeleted) {
        this.billingS.postAuditHistory({
          Operator: this.tocken.user,
          actionDate: this.globalS.getCurrentDateTime(),
          auditDescription: 'DELETE AWARD ' + data.code,
          actionOn: 'AWARDPOS',
          whoWhatCode: data.recordNo, //inserted
          TraccsUser: this.tocken.user,
        }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.globalS.sToast('Success', 'Award Setup has been deleted successfully.');
        }
      );
      this.loadData();
      return;
    }
  });
}

buildForm() {
  this.inputForm = this.formBuilder.group({

    code: '',
    end_date:'',
    description:'',
    category:'',
    level:'',
    notes:'',

    basE_PayType:'',
    week_MaxOrdHr:'',
    week_MaxOrdHrNotFound:'',
    fN_MaxOrdHr:0,
    
    w4_MaxOrdHours:0,
    week_MaxOrdDays:'',
    day_MaxOrdHr:'',
    fN_MaxOrdDays:'',
    fN_MaxOrdHrsDay:'',
    w4_MaxOrdDays:'',
    w4_MaxOrdHrsDay:'',
    ordHoursStartTime:'',
    ordHoursEndTime:'',
    minHoursPerDay:'',
    minHoursPerDayDisabled:'',
    minHoursPerService:'',
    breakTimeWorked:false,
    week_MinFreeDays:'',
    fN_FreeDays:'',
    w4_FreeDays:'',
    payOvertimeInsuficientMinDays:0,
    higherDuttieshour:'',







    payOvertimeNoMinBreak:0,
    minBetweenShiftBreak:0,
    minBreakBetweenSleepovers:0,
    minBreakPayType:0,
    day_SatOrdHr_PayType:'',
    day_SunOrdHr_PayType:'',
    day_PHOrdHr_PayType:'',







    overtimeLeeway:0,
    
    toilDefaultAccrualMinutes:0,
    toilDefaultAccrualPaytype:'',
    
    day_MaxOrdHrWD1stOTBreak:0,
    day_MaxOrdHrWD1stOTBreak_PayType:'',
    


    day_MaxOrdHrWD2ndOTBreak_PayType:'',
    


    day_MaxOrdHrSat1stOTBreak:0,
    day_MaxOrdHrSat1stOTBreak_PayType:'',
    
    
    day_MaxOrdHrSat2ndOTBreak_PayType:'',
    


    day_MaxOrdHrSun1stOTBreak:0,
    day_MaxOrdHrSun1stOTBreak_PayType:'',
    
    

    day_MaxOrdHrSun2ndOTBreak_PayType:'',
    


    day_MaxOrdHrPH1stOTBreak:0,
    day_MaxOrdHrPH1stOTBreak_PayType:'',


    day_MaxOrdHrPH2ndOTBreak_PayType:'',



    week_MaxOrdHrWD1stOTBreak:0,
    week_MaxOrdHrWD1stOTBreak_PayType:'',


    week_MaxOrdHrWD2ndOTBreak_PayType:'',


    fN_MaxOrdHrWD1stOTBreak:0,
    fN_MaxOrdHrWD1stOTBreak_PayType:'',


    fN_MaxOrdHrWD2ndOTBreak_PayType:'',







    disableBrokenShiftAllowance:false,
    brokenShiftAllowanceExtra_Threshold:0,
    brokenshiftLimit :0,
    brokenShiftAllowanceExtra_Threshold1:'',
    brokenShiftAllowance_PayType:'',
    unpaidMealBreakMin:0,
    unpaidMealBreakStartAt:0,
    unpaidMealBreak_PayType:'',
    teaBreakPaidTime:0,
    teaBreakStartAt:0.0,
    teaBreakPaidTimeNext:0,
    teaBreakStartAtNext:0.0,
    teaBreakPaidIncWkdHrs:false,
    payOvertimeForTeaBreaks:false,
    cancellationPayThreshold:0,
    makeUp_Pay_Threshold:0,
    makeUpLeadTime:0,
    noNoticeCancelPayType:'',
    shtNoticeCancelPayType:'',
    kmAllowanceType:'',

    BASE_PayType:'',
    recordno:null,
  });
  
  this.timebandsForm = this.formBuilder.group({ 
    award: -1,
    startTime: '',
    endTime: '',
    payType: '',
    dayType: '',
    vRank: '',
    recordNo:0,
  });
  
  this.allowancesForm = this.formBuilder.group({
    
    awardID:-1,
    name:'',
    replaceActivitiesWithAllowance:false,
    default:false,
    notes:'',
    payType:'',
    serviceType:'',
    program:'',
    category1:'',
    recordNumber:0,
  })
  this.specialhiftsForm = this.formBuilder.group({
    
    awardID:-1,
    name:'',
    notes:'',
    activity:'',
    backToBackPayType:'',
    jobType:'',
    payBackToBack:false,
    recordNumber:0,
  })
  
  
  
}
handleOkTop() {
  this.generatePdf();
  this.tryDoctype = ""
  this.pdfTitle = ""
}
handleCancelTop(): void {
  this.drawerVisible = false;
  this.pdfTitle = ""
}
generatePdf(){
  
  this.drawerVisible = true;
  this.loading = true;
  var fQuery = "SELECT ROW_NUMBER() OVER(ORDER By Description) AS Field1,Description as Field2,CONVERT(varchar, [enddate],105) as Field3 from DataDomains "+this.whereString+" Domain='AWARDLEVEL'";
  
  const data = {
    "template": { "_id": "0RYYxAkMCftBE9jc" },
    "options": {
      "reports": { "save": false },
      "txtTitle": "Award Levels List",
      "sql": fQuery,
      "userid":this.tocken.user,
      "head1" : "Sr#",
      "head2" : "Name",
      "head3" : "End Date"
    }
  }
  this.printS.printControl(data).subscribe((blob: any) => { 
    let _blob: Blob = blob;
    let fileURL = URL.createObjectURL(_blob);
    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
    this.loading = false;
  }, err => {
    this.loading = false;
    this.ModalS.error({
      nzTitle: 'TRACCS',
      nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
      nzOnOk: () => {
        this.drawerVisible = false;
      },
    });
  });
  this.loading = true;
  this.tryDoctype = "";
  this.pdfTitle = "";
}
runProcess() {


  console.log("Test case work"); 


}

onKeyPress(data: KeyboardEvent) {
  return this.globalS.acceptOnlyNumeric(data);
}




showsubeditModal(index:number){
  
  this.submodalScreen = index;
  this.addOrEdit = 1;

    if(this.submodalScreen == 1){  
      this.subModaltitle = "Add/Change Timebands";
      const { 
        startTime,
        endTime,
        payType,
        dayType,
        higherpayrank,
        recordNo,
      } = this.timebandsList[this.selectedRow];
      
      if (this.timebandsForm) {
        this.timebandsForm.patchValue({
          recordNo:recordNo,
          award: this.insertedAwardId,
          startTime:startTime,
          endTime:endTime,
          payType: payType,
          dayType: dayType,
          vRank: higherpayrank
        });
      } else {
        console.error("timebandsForm is not initialized.");
      }
  }
  
  if(this.submodalScreen == 2){
    this.subModaltitle = "Add/Change Allowances";
        const { 
          name,
          payType,
          defaultProgram,
          category1,
          higherpayrank,
          autocreate,
          replaceActivitiesWithAllowance,
          notes,
          recordNumber
        } = this.allowncesList[this.selectedRowAllocance];
        
        if (this.allowancesForm) {
          
          this.allowancesForm.patchValue({
            awardID: this.insertedAwardId,
            name:name,
            default:autocreate,
            replaceActivitiesWithAllowance:replaceActivitiesWithAllowance,
            payType:payType,
            serviceType: higherpayrank,
            program: defaultProgram,
            category1: category1,
            notes:notes,
            recordNumber:recordNumber
          });

          
          this.menuS.getawardsetupallowancesrules(recordNumber).subscribe(data=>{
            this.ruleList = data;
          });
          

          
          this.menuS.getawardsetupallowancesinclusionactivity(recordNumber).subscribe({
            next: (data) => {
                this.allinclusiveactivitesList = data;
            }
          });
          
          this.menuS.getawardsetupallowancesinclusioncompetency(recordNumber).subscribe({
            next: (data) => {
                this.allinclusivecompetencyList = data;
            }
          });
          this.menuS.getawardsetupallowancesexclusionactivities(recordNumber).subscribe({
            next: (data) => {
                this.allexclusivecactivityList = data;
            }
          });
          this.menuS.getawardsetupallowancesexclusioncompetency(recordNumber).subscribe({
            next: (data) => {
                this.allexclusivecompetencyList = data;
            }
          });

          



        } else {
          console.error("AllowancesForm is not initialized.");
        }    
  }
  
  if(this.submodalScreen == 3){
    this.subModaltitle = "Add/Award Special Shift Detiails";
      const { 
        name,
        activity,
        jobType,
        payBackToBack,
        backToBackPayType,
        notes,
        recordNumber
      } = this.specialShiftList[this.selectedshifts];
      
      if (this.specialhiftsForm) {
        this.specialhiftsForm.patchValue({
          awardID: this.insertedAwardId,
          name:name,
          notes:notes,
          backToBackPayType:backToBackPayType,
          jobType:jobType,
          activity:activity,
          payBackToBack:payBackToBack,
          recordNumber:recordNumber
        });



        this.menuS.getawardsetupspecialshiftsbreakdown(recordNumber).subscribe(data=>{
          this.breakdownList = data;
        });




      } else {
        console.error("specialhiftsForm is not initialized.");
      }
  }
  
  this.awardtimebandsallowncesshiftsmodal= true;
}



showTimebandsAddModal(){
  
}
showTimebandsEditModal(){
  
}
timebandsdelete(){
  
}


showallowncesAddModal(){
  
}
showallowncesEditModal(){
  
}
allowncesdelete(){
  
}
showshiftsAddModal(){
  
}
showshiftsEditModal(){
  
}
shiftsdelete(){
  
}
goBack(): void {
  const previousUrl = this.navigationService.getPreviousUrl();
  const previousTabIndex = this.navigationService.getPreviousTabIndex();

  if (previousUrl) {
    this.router.navigate(['/admin/configuration'], {
      queryParams: { tab: previousTabIndex } // Pass tab index in query params
    });
  } else {
    this.router.navigate(['/admin/configuration'], {
      queryParams: { tab: previousTabIndex } // Pass tab index in query params
    });
  } 
}
@HostListener('document:keydown', ['$event'])
            handleKeyboardEvent(event: KeyboardEvent) {
              // Handle character input
              if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
                  this.searchTerm += event.key.toLowerCase();
                  this.scrollToRow();
              } 
              // Handle backspace
              else if (event.key === 'Backspace') {
                  // Only reset if there's something to remove
                  this.searchTerm = '';
                  if (this.searchTerm.length > 0) {
                      this.searchTerm = this.searchTerm.slice(0, -1);
                      this.scrollToRow();
                  }
              }
              // Handle other keys (optional)
              else if (event.key === 'Escape') {
                  // Reset search term on escape key
                  this.searchTerm = '';
                  this.scrollToRow();
              }
            }
        
            scrollToRow() {
                // Find the index of the first matching item
                const matchIndex = this.awardListData.findIndex(item => 
                    item.code.toLowerCase().startsWith(this.searchTerm)
                );
                // Scroll to the matching row if it exists
                if (matchIndex !== -1) {
                    const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
                    if (tableRow) {
                        tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                        this.highlightRow(tableRow);
                    }
                } else {
                }
            }
        
            highlightRow(row: HTMLElement) {
                row.classList.add('highlight');
                setTimeout(() => row.classList.remove('highlight'), 2000);
            }
}
