import { Component, forwardRef, ElementRef, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Subject } from 'rxjs';

import * as moment from 'moment';
const months: Array<string> = moment.months();

const noop = () => { };

export const DATEPICKER_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => DatePicker),
};

@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'datepicker',
    templateUrl:'./datepicker.html',
    styles:[
        `    
        input{
            width:100% !important;
            margin-bottom:0 !important;
        }
        .date-wrapper{
            position: relative;
            -webkit-tap-highlight-color:transparent;
            -webkit-user-select: none;       
            -moz-user-select: none; 
            -ms-user-select: none; 
        
            -o-user-select: none;
            user-select: none;
        }
        .date-wrapper > i{
            position: absolute;
            right: 0;
            bottom:4px;
        }
        .date-body{
            position: absolute;
            margin: 2px 0 0 0;
            width: 9rem;
            border: 1px solid #d0d0d0;
            padding: 2px;
            background: #ffffff;
            z-index: 5;
        }
        .control-switcher{
            width:50%;
        }
        .control-picker{
            display:inline-flex;
            width:50%;
        }
        .control-picker button{   
            color: #447fff;
            font-size: 18px;         
        }
        .control-picker button:hover{
            background:#eeeeff;
        }
        .dd-container button{
            width: 50%;
            height: 40px;
            color: #565656;
            margin: 0 !important;
        
        }
        .dd-container button:hover{
            background:#eeeeff;
        }
        .btn-clear{
            outline:none;
            border:0;
            background:inherit;
        }
        .year-controls{          
            float:right;
        }
        .year-controls > i:hover{
            background:#eeeeff;
            cursor:pointer;
        }
        .year-controls button:hover{
            background:#eeeeff;
        }
        .left{
            transform: rotate(-90deg);
        }
        .right{
            transform: rotate(90deg);
        }
        #days{
            width:100%;
            table-layout:fixed;
        }
        #days th{
            text-align:center;
        }
        #days td{
            text-align: center;
            line-height: 28px;
        }
        #days td:not(.disable):not(.today):hover{
            cursor:pointer;
            background:#eeeeff;
        }
        .disable{
            opacity:0.5;
        }
        .today:not(.disable){
            background: #6d94f5;
            border-radius: 50%;
            color: #fff;
        }
        td{
            font-size:11px;
        }
        .year-controls-y{
            float:none;
            text-align:center;
            margin: 8px 0;
        }
        .cancel{
            display: flex;
            border-top: 1px solid #ececec;
            cursor:pointer;
        }
        .cancel:hover{
            background:#f7f7f7e8;
        }
        .cancel:hover i{
            color: #777777;
        }
        .cancel i{
            margin: 0 auto;
            color:#adadad;
        }
        .clr-subtext{
            display:none;
        }
        .clr-error span.clr-subtext{
            display: block;
        }
        .clr-input-wrapper:hover > i{
            visibility: visible;
        }
        .clr-input-wrapper{
            position: relative;
            min-width: 4.5rem;
        }
        .clr-input-wrapper i{
            position: absolute;
            font-size: 17px;
            right: 3px;
            top: 3px;
            cursor:pointer;
            visibility:hidden;
        }
        .clr-input-wrapper i:hover{
            transform:scale(1.1);
        }
        `
    ],
    providers: [ DATEPICKER_VALUE_ACCESSOR ]
})

export class DatePicker implements ControlValueAccessor {

    @Input() DPrevDays: boolean = false;
    private dateNow: any;

    arrayDate: Array<any> = [];
    public emitDate = new Subject<any>();
    tempValue: any;

    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    show: boolean = false;
    date: any;
    window: number = 1;
    months: Array<string> = months;

    years: Array<number> = [];
    indexYear: number;
    //The internal data model
    private innerValue: any = '';

    invalidDate: boolean = false;

    constructor(
        private elem: ElementRef
    ){        
        this.emitDate.subscribe(data =>{
            //if(data){
                this.window = 1;
                this.generate(true);
            //}
        })
    }

    onClick(event: Event){
        if(!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }
   
    //get accessor
    get value(): any {
        return moment(this.innerValue).format('DD/MM/YYYY') == 'Invalid date' ? null : moment(this.innerValue).format('DD/MM/YYYY');
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    pick(category: string, pick:number){
        if(category == 'm') this.tempValue = moment(this.tempValue).month(pick);
        if(category == 'y') this.tempValue = moment(this.tempValue).year(pick);
        if(category == 'mi') this.tempValue = moment(this.tempValue).add(pick,'M');
        if(category == 'md') this.tempValue = moment(this.tempValue).subtract(pick,'M');        
        
        this.generate();
        this.window = 1;
    }

    pickDate(data:any){  
        if(!data.disable){
            this.value = moment(this.tempValue,['DD/MM/YYYY']).date(data.data);
            this.clearSelectedDate();
            data.selected = true;
        }
    }

    clearSelectedDate(){
       this.arrayDate.forEach(data => {
            data.forEach(a => {
                if(a.hasOwnProperty('selected')){
                    a.selected = false;
                }
            });            
        });
    }

    changeSelected(dateObject:any){
        var date = parseInt(moment(dateObject).format('DD'));
    }

    generate(origin: boolean = false){

        if(origin){
            this.tempValue = moment(this.value,['DD/MM/YYYY']);
        }

        if(moment(this.value).format('DD/MM/YYYY') == 'Invalid date' && origin ){
            this.tempValue = moment(this.value, ['DD/MM/YYYY']);
        }

        if(!this.value && origin){
            this.tempValue = moment();
        }        

        if(this.tempValue){
            let startOfDay = moment(this.tempValue, ['DD/MM/YYYY']).startOf('month').day();
            let endOfMonth = moment(this.tempValue, ['DD/MM/YYYY']).subtract(1,'M').daysInMonth();
            let noOfMonth = moment(this.tempValue, ['DD/MM/YYYY']).daysInMonth();

            let subDay = endOfMonth - startOfDay + 1;
            let arr: Array<any> = [];
            let finalArr: Array<any> = [];

            for(var a = subDay; a <= endOfMonth ; a++){
                arr.push({ disable: true, data: subDay});
                subDay++;
            }

            for(var a = 1; a <= noOfMonth; a++ ){                
                arr.push({ 
                    disable: this.daysDisabled(a), 
                    data: a, 
                    selected: this.isTodaysDate() == a 
                });
            }

            if(arr.length != 42){
                let diff = 42 - arr.length;
                for(var a = 1 ; a <= diff; a++){
                    arr.push({ disable: true, data: a});
                }
            }

            for(var a = 0; a < 6; a++){
               finalArr.push(arr.splice(0,7));
            }

            this.arrayDate = finalArr;
        }
    }
    
    daysDisabled(day: number): boolean {
        if(this.DPrevDays){
            this.dateNow = moment();            
            if( moment(this.dateNow).isAfter(moment(this.tempValue).date(day),'day')  )
                return true;
            return false;
        }
        return false;
    }

    isTodaysDate():number{       
        if(moment(this.value,['DD/MM/YYYY']).format('DD/MM/YYYY') === moment(this.tempValue,['DD/MM/YYYY']).format('DD/MM/YYYY')){
            return parseInt(moment(this.tempValue).format('DD'));
        }
        return 0;
    }

    loopYear(result: any){
        this.years = [];
        this.indexYear = result;
        for(var a = 0 ; a < 10 ; a++){
            this.years.push(result);    
            result++;
        }
    }

    populate(){
        let date = this.value;
     
        if(date == "" || date == null)
            date = this.tempValue;

        var year = parseInt(moment(date,['DD/MM/YYYY']).format('YYYY'));
        var result = (year -(year % 10));
        this.loopYear(result);
    }

    clearDate(){
        this.value = ""
        this.arrayDate.forEach(data => {
            data.forEach(a => {              
                a.selected = false                
            });            
        });
    }

    change(date: any){
        if(!moment(date,['DD/MM/YYYY']).isValid()){
            this.invalidDate = true;
            return;
        }

        if(date){
            this.value = moment(date,['DD/MM/YYYY']);
        }
        this.generate(true);
        this.invalidDate = false;
    }

    showDate(){
        console.log(this.elem.nativeElement.getBoundingClientRect())
    }
}
