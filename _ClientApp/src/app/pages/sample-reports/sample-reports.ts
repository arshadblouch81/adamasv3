import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'sample-reports',
  templateUrl: './sample-reports.html',
  styleUrls: ['sample-reports.css']
})
export class SampleReports {
    isExpanded = false;

    public reports: string[];
    public ListTitle: string;
  
    collapse() {
      this.isExpanded = false;
    }
  
    toggle() {
      this.isExpanded = !this.isExpanded;
    }
  
    constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
      if (navigator.language === 'ja') {
        this.ListTitle = 'JS Viewer  -  Angularサンプル';
      } else {
        this.ListTitle = 'JS Viewer - Angular Sample';
      }
  
      http.get<string[]>(baseUrl + 'api/report').subscribe(result => {
        this.reports = result;
      }, error => console.error(error));
    }
}
