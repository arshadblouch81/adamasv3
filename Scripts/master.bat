:: Call Develop Script
::PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& './master-script-csproj.ps1'"

set rootAdamasReleaseFolder="%~dp0..\bin\Release\net6.0"
set rootAdamasRootFolder="%~dp0..\"

D:
echo %rootAdamasReleaseFolder%

cd /d %rootAdamasReleaseFolder%

IF EXIST %rootAdamasReleaseFolder%	(
	echo "exist"
	rmdir /s /q %rootAdamasReleaseFolder%
) ELSE (
	echo "does not exist"
)

cd /d %rootAdamasRootFolder%
dotnet publish --self-contained true --configuration Release --output %rootAdamasReleaseFolder% AdamasV3.csproj

cd /d %rootAdamasReleaseFolder%
del jsreport.Binary.dll
del appsettings.Development.json
del appsettings.json

%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe -file "D:\Programming\Adamas\adamasv3\Scripts\version.ps1"

call git init
call git remote add origin https://github.com/maartri/adamas-production.git
call git add -A
call git commit -m "first comm"
call git push origin master -f

pause