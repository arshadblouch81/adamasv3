import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DayManager } from './dm-calendar';
import { DayManagerPopulate } from './dm-calendar.pipe';
import { OptionClickDirective } from './dm-calendar.directive'
import { ClarityModule } from '@clr/angular';

@NgModule({
    declarations:[
        DayManager,
        DayManagerPopulate,
        OptionClickDirective
    ],
    imports:[
        CommonModule,
        FormsModule,
        ClarityModule
    ],
    exports:[
        DayManager,
        ClarityModule
    ]
})

export class DayManagerModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: DayManagerModule
        }
    }
}