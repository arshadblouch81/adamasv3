namespace Adamas.Models.Modules{
    public class InputUser{
        public string Code { get; set; }
        public string Id { get; set; }
        public string Program { get; set; }
    }
}