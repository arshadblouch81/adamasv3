using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules
{
    public class TimeOverlap
    {
        public int RecordNo { get; set; }
        public string StartTime { get; set; }
    }

    public class OverlapsList {
        public IEnumerable<TimeOverlap> Overlaps { get; set;  }
    }
}