using System;

namespace Adamas.Models.Modules{
    public class RecipientDetails {
        public string AccountNo { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Branch { get; set; }
        public string Suburb { get; set; }
        public Contacts Contact { get; set; }
        public Address Address { get; set; }

    }


}