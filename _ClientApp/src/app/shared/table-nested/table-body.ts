import { Component, Input } from '@angular/core'

@Component({
    selector: 'table-body',
    template: `
        <div class="title" (click)="show = !show">
            <i class="material-icons" *ngIf="!show"> add </i> 
            <i class="material-icons" *ngIf="show"> remove </i>
            {{ title }}</div>

        <ng-content *ngIf="show"></ng-content>          
    `,
    styles:[`
        i {
            float: left;
            margin-right:10px;
            font-size: 14px;
            line-height: 1rem;
        }
        .title{
            cursor:pointer;
            background: #6b6b6b;
            color: #fff;
        }
        .title:hover{
            background: #7b7b7b;
        }
    `]
})

export class TableBody {
    @Input() title: string
    
    show: boolean = true
}
