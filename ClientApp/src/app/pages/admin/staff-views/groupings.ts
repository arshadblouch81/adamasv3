import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import { NzTabPosition } from 'ng-zorro-antd/tabs';

@Component({
    selector: 'app-your-component',
    templateUrl: './groupings.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styles: [`
      nz-form-item {
          margin: 0;
      }
      nz-select {
          width: 100%;
      }
      a:hover {
          text-decoration: underline;
      }
      nz-table {
          margin-top: 0px;
      }
      ::ng-deep .ant-tabs .ant-tabs-left-content {
          padding-left: 0px !important;
      } 
      .spinner {
          margin: 1rem auto;
          width: 1px;
      }  
       nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
       }
       tbody  tr:nth-child(odd) {
            background-color: #E9F7FF;
       }
       tbody  tr:nth-child(even) {
                background-color: #fff;
       }
        ::ng-deep .ant-tabs-tab {
            text-align:left !important
        }
            ::ng-deep .ant-tabs-nav{
                height:79vh;
            }
            ::ng-deep .ant-tabs-tab-active {
                background: #DFEBF4 !Important;
                color:black;
            }


    `]
  })

export class StaffGroupingsAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    position: NzTabPosition = 'left';
    loading: boolean = false;
    userdefined1: Array<any>;
    userdefined1List: Array<string>;
    userdefined2: Array<any>;
    userdefined2List: Array<any>;
    
    modalOpen: boolean = false;
    isLoading: boolean = false;
    whatView: number;
    
    inputForm: FormGroup;
    
    listArray: Array<any>;
    
    private editOrAdd: number;
    tocken: any;
    pdfTitle: string;
    selectedStaff: any;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    temp_title: any;
     
    selectedIndex = 0; 
    selectedRow  : number = 0;
    selectedRowp : number= 0;
    activeRowData:any;
    activeRowDatap:any;
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private printS:PrintService,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'groupings-preferences')) {
                    this.search(data);
                }
            });
        }
        
        selectedItemGroup(data:any, i:number){
            this.selectedRow=i;
            this.activeRowData=data;
        }
        selectedItemPreference(data:any, i:number){
            this.selectedRowp=i;
            this.activeRowDatap=data;
        }
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked()
            if(this.user){
                this.search(this.user);
                this.buildForm();
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            this.inputForm = this.formBuilder.group({
                list: [[], Validators.required],
                notes: [''],
                id: ['']
            });
        }
        
        get title() {
            const str = this.whatView == 1 ? 'Group' : 'Preference';
            const pro = this.editOrAdd == 1 ? 'Add' : 'Edit';
            return `${pro} ${str}`;
        }
        
        search(user: any) {
            this.cd.reattach();
            //if (!this.user) return;
            this.loading = true;        
            this.selectedStaff = user;
            forkJoin([
                this.timeS.getuserdefined1(user.id),
                this.timeS.getuserdefined2(user.id),
                this.listS.getlistuserdefined1(),
                this.listS.getlistuserdefined2()
            ]).subscribe(data => {
                this.loading = false;
                
                this.userdefined1 = data[0];
                this.userdefined2 = data[1];
                
                this.userdefined1List = data[2];
                this.userdefined2List = data[3];
                this.cd.detectChanges();
            });
        }
        
        showAddModal(view: number) {
            if (view == 1) this.listArray = this.userdefined1List;
            if (view == 2) this.listArray = this.userdefined2List;
            
            this.whatView = view;
            this.editOrAdd = 1;
            this.modalOpen = true;
        }
        
        showEditModal(view: number) {
            
            if (view == 1 ) {
                
                this.listArray = this.userdefined1List;
                const { group, notes, recordNumber } = this.userdefined1[this.selectedRow];
                
                this.inputForm.patchValue({
                    list: group,
                    notes: notes,
                    id: recordNumber
                });
                this.temp_title = group;
            }
            
            if (view == 2 ) {
                
                this.listArray = this.userdefined2List;
                const { preference, notes, recordNumber } = this.userdefined2[this.selectedRowp];
                this.inputForm.patchValue({
                    list: preference,
                    notes: notes,
                    id: recordNumber
                });
                this.temp_title = preference;
            }
            
            this.whatView = view;
            this.editOrAdd = 2;
            this.modalOpen = true;
        }
        
        handleCancel() {
            this.inputForm.reset();
            this.isLoading = false;
            this.modalOpen = false;
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        
        success() {
            this.search(this.sharedS.getPicked());
            this.isLoading = false;
            this.selectedRow = 0;
            this.selectedRowp = 0;
        }
        
        processBtn() {
            if (this.editOrAdd == 1) {
                this.save();
            }
            
            if (this.editOrAdd == 2) {
                this.edit();
            }
        }
        
        save() {
            for (const i in this.inputForm.controls) {
                this.inputForm.controls[i].markAsDirty();
                this.inputForm.controls[i].updateValueAndValidity();
            }
            
            if (!this.inputForm.valid)
            return;
            
            const { list, notes } = this.inputForm.value;
            const index = this.whatView;
            this.isLoading = true;
            let is_exist;
            let name        = this.inputForm.get('list').value.trim().toUpperCase();
            
            if((index == 1)){
                is_exist    = this.globalS.isNameMailingGroupExist(this.userdefined1,name);
            }else{
                is_exist    = this.globalS.isNameMailingPreferenceExist(this.userdefined2,name);
            }
            
            if(is_exist){
                this.globalS.iToast('Unsuccess', 'Already Exist');
                this.isLoading = false;
                return false;   
            }
            
            if (index == 1) {    
                
                this.timeS.postuserdefined1({
                    notes: notes,
                    // group: list,
                    personID: this.user.id,
                    groupList: [list] //passing single value as an array to post function 
                }).pipe(takeUntil(this.unsubscribe))
                .subscribe(data => {
                    if (data) {
                        this.handleCancel();
                        this.success();
                        this.globalS.sToast('Success', 'Data Added');
                    }
                })
            }
            
            if (index == 2) {
                this.timeS.postuserdefined2({
                    notes: notes,
                    // group: list,
                    personID: this.user.id,
                    groupList: [list] //passing single value as an array to post function 
                }).pipe(takeUntil(this.unsubscribe))
                .subscribe(data => {
                    if (data) {
                        this.handleCancel();
                        this.success();
                        this.globalS.sToast('Success', 'Data Added');
                    }
                });
            }
        }
        
        edit() {
            
            for (const i in this.inputForm.controls) {
                this.inputForm.controls[i].markAsDirty();
                this.inputForm.controls[i].updateValueAndValidity();
            }
            
            if (!this.inputForm.valid)
            return;
            
            const { list, notes, id } = this.inputForm.value;
            const index = this.whatView;
            this.isLoading = true;
            
            if (index == 1) {
                this.timeS.updateshareduserdefined({
                    group: list,
                    notes,
                    recordNumber: id
                }).pipe(
                    takeUntil(this.unsubscribe))
                    .subscribe(data => {
                        if (data) {
                            this.handleCancel();
                            this.success();
                            this.globalS.sToast('Success', 'Data Updated');
                        }
                    })
                }
                
                if (index == 2) {
                    this.timeS.updateshareduserdefined({
                        group: list,
                        notes,
                        recordNumber: id
                    }).pipe(
                        takeUntil(this.unsubscribe))
                        .subscribe(data => {
                            if (data) {
                                this.handleCancel();
                                this.success();
                                this.globalS.sToast('Success', 'Data Updated');
                            }
                        })
                    }
                }
                
                delete(view: any) {
                    let recordNum ;
                    if(view == 1){
                        recordNum = this.userdefined1[this.selectedRow].recordNumber;
                    }else{
                        recordNum = this.userdefined2[this.selectedRow].recordNumber;
                    }
                    this.timeS.deleteshareduserdefined(recordNum) 
                    .subscribe(data => {
                        if (data) {
                            this.handleCancel();
                            this.success();
                            this.globalS.sToast('Success', 'Data Deleted');
                        }
                    });
                }
                handleOkTop(view: number) {
                   
                    this.tryDoctype = ""
                    this.pdfTitle = ""
                }
                handleCancelTop(): void {
                    this.drawerVisible = false;
                    this.pdfTitle = ""
                    this.tryDoctype = ""
                }                
                getPermisson(index:number){
                    var permissoons = this.globalS.getStaffRecordView();
                    // console.log("staff "+ permissoons.length );
                    return permissoons.charAt(index-1);
                }

        //Mufeed 18 April 2024            
        InitiatePrint(view){           
            var lblcriteria,fQuery,txtTitle,Rptid,head1,head2;
            let DataArr; 
                        
            if(view == 1){ 
                DataArr =  this.userdefined2;                            
                txtTitle = "STAFF PREFERENCES ";
                head1 = "preference";
                head2 = "notes";
            
            }else{
                DataArr = this.userdefined1
                txtTitle = "User Defined Groups";
                head1 = "group";
                head2 = "notes";
            }

            Rptid =   "4HcwIWORPtT9HKXw"//"8WTerq14eNFSAnz0"  
    
            //console.log(fQuery)
            const data = {
                        
                "template": { "_id": Rptid },                                
                "options": {
                    "reports": { "save": false },                
                    "sql": DataArr,//"sql": fQuery,
                    "Criteria": lblcriteria,
                    "userid": this.tocken.user,
                    "txtTitle": txtTitle+ ' for '+ this.selectedStaff.code,                    
                    "head1":head1,
                    "head2":head2,
                                                                    
                }
            }
            this.loading = true;
            this.drawerVisible = true;         
                this.printS.printControl(data).subscribe((blob: any) => {
                this.pdfTitle = txtTitle+".pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.handleCancelTop();
                        },
                    });
                });
                return;      
    }
            }