
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

const docusign: string = "api/docusign";

@Injectable()
export class DocusignService {
   constructor(
      public http: HttpClient,    
      public auth: AuthService,
   
   ) { }

  private apiUrl = 'https://demo.docusign.net/restapi/v2.1/accounts/{accountId}/envelopes'; // Replace with your account ID
  private accessToken = 'YOUR_ACCESS_TOKEN'; // Replace with your access token

  

  sendEnvelope(data: any): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.accessToken}`,
      'Content-Type': 'application/json'
    });

    return this.http.post<any>(this.apiUrl, data, { headers });
  }

  login(): Observable<any> {
    return this.auth.get(`${docusign}/login` );
 }
  sendDocument(data:any): Observable<any> {
    return this.auth.post(`${docusign}/create`, data );
 }


}
