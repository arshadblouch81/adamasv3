import {  Component, OnInit } from '@angular/core'
import { ListService, states } from '@services/index'


import {forkJoin } from 'rxjs';

@Component({
    selector:'',
    templateUrl: './budget.html',
    styles:[`
    .budget-container{
        display:flex;
        justify-content: space-between;
        height:100%;
    }
    .budget-wrapper{
        flex: 1 1 3rem;
        padding: 0 10px;
        overflow: hidden;
    }
    .budget-wrapper-8{
        flex: 5 1 10rem;
        height: 100%;
        background: #f3f3f3;
        padding: 0 2rem;
        border: 1px solid #bebebe;
        overflow: hidden;
    }
    ul{
        list-style:none;
        max-height: 15rem;
        overflow-y: auto;
        margin-left:10px;
    }
    ul li{
        line-height:5px;
    }
    h4{
        margin-bottom: 0.5rem;
    }
    h4 span{
        border-bottom:2px solid #177dff;
        padding-bottom:5px;
    }
    div.budget-container div{
        font-size: 12px;
    }
    div.budget-wrap{   
    }
    .budget-options{
        margin-top:5rem;
    }
    li div label{
        font-weight:100;
    }
    `]
})

export class BudgetAdmin implements OnInit {
    states: Array<string> = states;
    criteriaList = {
        branches: [],
        managers: [],
        fundingRegions: [],
        serviceRegions: [],
        serviceProviderIds:[],
        agencyPrograms: [],
        serviceBudgetCodes: [],
        careDomains: [],
        disciplines: [],
        costCentres: [],
        environments: [],
        funders: [],
        funderServiceTypes: []
    };

    constructor(
        private listS: ListService
    ){

    }

    ngOnInit(){
        this.getAllCriteria();
    }

    getAllCriteria(){
        forkJoin([
            this.listS.getreportcriterialist({
                listType: 'BRANCHES',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'MANAGERS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'SERVICEREGIONS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'FUNDINGREGIONS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'PROVIDERIDS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'PROGRAMS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'BUDGETCODES',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'SERVICETYPES',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'CAREDOMAINS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'DISCPLINES',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'COSTCENTRES',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'ENVIRONMENTS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'FUNDERS',
                includeInactive: false
            }),
            this.listS.getreportcriterialist({
                listType: 'FUNDERSERVICETYPES',
                includeInactive: false
            })     
        ]).subscribe(data => {
            console.log(data);
            this.criteriaList = {
                branches: data[0],
                managers: data[1],
                serviceRegions: data[2],
                fundingRegions: data[3],
                serviceProviderIds: data[4],
                agencyPrograms: data[5],
                serviceBudgetCodes: data[6],
                careDomains: data[8],
                disciplines: data[9],
                costCentres: data[10],
                environments: data[11],
                funders: data[12],
                funderServiceTypes: data[13]
            };
        })
    }

}