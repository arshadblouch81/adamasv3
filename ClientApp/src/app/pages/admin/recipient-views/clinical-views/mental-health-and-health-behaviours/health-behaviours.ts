


import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, othersType, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { functionalStatus} from '@services/global.service'

@Component({
    
  styleUrls: ['./health-behaviours.css'],
  templateUrl: './health-behaviours.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ClinicalHealthBehaviours implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

  
    
    smoking:Array<any> = functionalStatus.smoking
    confusion:Array<any> = functionalStatus.confusion
    alcohol :Array<any> = functionalStatus.alcohol
    yesNo:Array<any> = functionalStatus.yesNo
    weightlost:Array<any> = functionalStatus.weightlost
    weight:Array<any> = functionalStatus.weight;
    fitness:Array<any> =functionalStatus.fitness;


    healthBehaviorForm: FormGroup;
    staffs: Array<string> = []

    dateFormat: string = dateFormat;

    occupationDefault: string;
    financialClassDefault: string;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'health-behaviours')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        this.user = this.sharedS.getPicked();
        this.buildForm();        
        this.populate();
        this.search(this.user);

       // let sql=`SELECT Description FROM DataDomains WHERE DOMAIN = 'FINANCIALCLASS'`;

        // this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //    this.lstFinancialClass = data.map(x => x.description);
        // });

        this.sharedS.emitSaveAll$.subscribe(data => {
            if (data.type=='Recipient' && data.tab =='Clinical Detail' && this.globalS.isCurrentRoute(this.router, 'health-behaviours')){
            
              this.save();
            }
          })

        
    
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    search(data: any){
        this.timeS.getHealthBehavior(data.id)
            .subscribe(data => {
                this.healthBehaviorForm.patchValue(data)               
                this.detectChanges();
            });
    }
    //FP2_WalkingDistance, FP3_Shopping, FP4_Medicine,FP5_Money , FP6_Walking, FP7_Bathing, FP8_Memory, FP9_Behaviour, FPA_Communication, FPA_Dressing, FPA_Eating, FPA_Toileting, FPA_GetUp


    buildForm(){
        this.healthBehaviorForm = this.formBuilder.group({
          recordNumber : 0,
          hbP_HealthChecks : null, 
          hbP_HealthChecks_Last: null, 
          hbP_HealthChecks_List: null, 
          hbP_Smoking: null, 
          hbP_Smoking_Quit: null,
          hbP_Alchohol: null, 
          hbP_Alchohol_NoDrinks: null, 
          hbP_Alchohol_BingeNo: null, 
          hbP_Malnutrition_LostWeight: null, 
          hbP_Malnutrition_PoorEating: null,
          hbP_Malnutrition_LostWeightAmount: null,  
          hbP_Malnutrition_Score: null,  
          hbP_Hydration_AdequateFluid: null,  
          hbP_Hydration_DecreasedFluid: null,  
          hbP_Weight: null,  
          hbP_PhysicalActivity: null,  
          hbP_PhysicalFitness: null,  
          hbP_Comments :null
            

        });
    }

    canDeactivate() {
        if (this.healthBehaviorForm && this.healthBehaviorForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Changes have been detected. Save Changes?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                    
                }
            });
        }
        return true;
    }

  

    populate(){
        // forkJoin([
        //     this.listS.getlistbranches(),
        //     this.listS.getlistcasemanagers(),
        //    // this.listS.getfinancialclass()
        // ]).subscribe(data => {
        //     this.branches = data[0];
        //     this.casemanagers = data[1];
        //  //   this.lstFinancialClass = data[2];
        //     this.detectChanges();
        // })

        // this.timeS.getstaff({
        //     User: this.globalS.decode().nameid,
        //     SearchString: '',
        //     IncludeInactive: false,
        //   }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     this.staffs = data.map(x => x.accountNo);
        //     this.detectChanges();
        //   });
    }

    save(){
        this.timeS.updateHealthBehavior(this.healthBehaviorForm.value, this.user.id).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
    }

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }


}