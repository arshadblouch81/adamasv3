using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class GetTimesheet {
        public string AccountNo { get; set; }
        public string User { get; set; } = "";
        public string personType { get; set; }
        public string s_status { get; set; } = "";
        public string startDate { get; set; } = "";
        public string endDate { get; set; } = "";
        public string order_by { get; set; } = "";
        public bool Include_Prev_Paid_Shift_Activity { get; set; } = false;
        public bool Include_Absence { get; set; } = false;

        public string prev_startDate { get; set; } = "";
        public string prev_endDate { get; set; } = "";

        public bool Master { get; set; } = false;

        //public List<JobStatus> JobStatuses { get; set; }
    }

    // public class JobStatus {
    //     public int RecordNumber { get; set; }
    //     public int JobNo { get; set; }
    // }
}