import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, recurringInt, recurringStr, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject, EMPTY } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';


import { Reminders } from '@modules/modules';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { isSameDay } from 'date-fns';

const defaultForm: any = {
    recordNumber: 0,
    personID: '',
    listOrder: '',
    followUpEmail:'',
    recurring: false,
    recurrInt: '',
    recurrStr: '',
    notes: '',
    reminderDate: null,
    dueDate: null,
    staffAlert: null
}


@Component({
    selector: '',
    templateUrl: './reminder.html',
    styles:[`
    h4{
        margin-top:10px;
    }
    nz-table th{
        font-weight: 600;
        font-family: 'Segoe UI';
        font-size: 14px;
        border: 1px solid #f3f3f3;
    }
    
    nz-table tr{
    
        font-family: 'Segoe UI';
        font-size: 14px;
        font-weight: 300;
    }
    label{
        font-family: 'Segoe UI';
        font-size: 16px;
        font-weight: 500;
        margin-top: 10px;
        }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ClinicalReminder implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    modalOpen: boolean = false;
    addOREdit: number;
    isLoading: boolean = false;
    lists: Array<any> = [];
    dateFormat: string = 'dd/MM/yyyy';
    
    dayInt = recurringInt;
    dayStr = recurringStr;
    activeRowData:any;
    selectedRowIndex:number;
    selectedReminders: any;
    
    private default: any = {
        recordNumber: '',
        personID: '',
        listOrder: '',
        followUpEmail: '',
        recurring: false,
        recurrInt: null,
        recurrStr: null,
        notes: '',
        reminderDate: null,
        dueDate: null,
        staffAlert: null
    }
    loading: boolean = false;
    ModalS: any;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    
    remindersList: any;
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private printS: PrintService,
        private cd: ChangeDetectorRef,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'reminders')) {
                    this.user = data;
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked();
            this.loading = false;
            if(this.user){
                this.search(this.user);
                this.buildForm();
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            
            this.inputForm = this.formBuilder.group({
                recordNumber: 0,
                personID: '',
                listOrder: '',
                followUpEmail: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
                recurring: false,
                recurrInt: '',
                recurrStr: '',
                notes: '',
                reminderDate: null,
                dueDate: null,
                staffAlert: ['', [Validators.required]]
            });
            
            this.inputForm.controls['recurrStr'].disable();
            this.inputForm.controls['recurrInt'].disable();
            
            this.inputForm.get('recurring').valueChanges.subscribe(data => {
                if(!data){
                    this.inputForm.controls['recurrInt'].setValue(null)
                    this.inputForm.controls['recurrStr'].setValue(null)
                    this.inputForm.controls['recurrStr'].disable()
                    this.inputForm.controls['recurrInt'].disable()
                } else {
                    this.inputForm.controls['recurrStr'].enable()
                    this.inputForm.controls['recurrInt'].enable()
                }
            })
        }
        
        resetForm(){
            this.inputForm.reset(defaultForm);
        }
        
        search(user: any = this.user) {
            this.cd.reattach();
            this.loading = true;
            this.listS.getclinicalreminder(user.id).subscribe(reminders => {
                this.loading = false;
                this.tableData = reminders;
                this.cd.markForCheck();
            })
        }
        populateDropDowns(){
            this.listS.getreminders(this.user.id).subscribe(data => {
                this.remindersList = data.map(x => {
                    return {
                        label: x,
                        value: x,
                        checked: false
                    }
                });
                this.cd.markForCheck();
            });
        }
        trackByFn(index, item) {
            return item.id;
        }
        logs(event: any) {
            this.selectedReminders = event;
        }
        process(){
            
            if(this.addOREdit == 1){
                if((this.addOREdit == 1 && this.selectedReminders === undefined) || (this.selectedReminders !== undefined && this.selectedReminders.length ===0) ){
                    this.globalS.sToast('Success', 'Please Select Atleast One Reminder ');
                    return
                }
                let data = {};
                let status = '';
                
                if(this.selectedReminders.length == 1){
                    data = {
                        PersonID:this.user.id,
                        Name:this.selectedReminders[0],
                        Notes:'',
                    };
                    status = "single"; 
                }else{
                    data = {
                        PersonID:this.user.id,
                        Name:'',
                        Notes:'',
                        selectedReminders:this.selectedReminders,
                    };
                    status = "multi"; 
                }
                
                this.timeS.postclinicalreminders(data,status).pipe(
                    takeUntil(this.unsubscribe))
                    .subscribe(insertedInd => {
                        console.log(insertedInd + "inserted ID");
                        this.inputForm.controls.staffAlert.setValue(this.selectedReminders[0]);
                        this.globalS.sToast('Success', 'Data added');
                        if(status == "single"){
                            this.inputForm.controls.recordNumber.setValue(insertedInd)
                            this.addOREdit = 0;
                            this.cd.detectChanges();
                        }else{
                            this.handleCancel();
                            this.cd.detectChanges();
                        }
                        
                    })
                }else{
                    const remGroup = this.inputForm.value;
                    const reminderDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(remGroup.reminderDate);
                    const dueDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(remGroup.dueDate);
                    
                    const reminder: Reminders = {
                        recordNumber: remGroup.recordNumber,
                        personID: this.user.id,
                        name: remGroup.staffAlert,
                        address1: remGroup.recurring ? remGroup.recurrInt : '',
                        address2: remGroup.recurring ? remGroup.recurrStr : '',
                        email: remGroup.followUpEmail,  
                        date1: reminderDate,
                        date2: dueDate,
                        state: remGroup.listOrder,
                        notes: remGroup.notes,
                        recurring: remGroup.recurring,
                        sameDate:false,
                        sameDay:false,
                        creator:"",
                    }
                    this.timeS.updateclinicalreminders(reminder).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                        this.globalS.sToast('Success', 'Data updated');
                        this.search();
                        this.handleCancel();
                    });
                }
            }
            
            
            save()
            {
                const remGroup = this.inputForm.value;
                const reminderDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(remGroup.reminderDate);
                const dueDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(remGroup.dueDate);
                
                const reminder: Reminders = {
                    recordNumber: remGroup.recordNumber,
                    personID: this.user.id,
                    name: remGroup.staffAlert,
                    address1: remGroup.recurring ? remGroup.recurrInt : '',
                    address2: remGroup.recurring ? remGroup.recurrStr : '',
                    email: remGroup.followUpEmail,  
                    date1: reminderDate,
                    date2: dueDate,
                    state: remGroup.listOrder,
                    notes: remGroup.notes,
                    recurring: remGroup.recurring,
                    sameDate:false,
                    sameDay:false,
                    creator:"",
                }
                
                if(this.addOREdit == 1){
                    this.timeS.postreminders(reminder).pipe(
                        takeUntil(this.unsubscribe))
                        .subscribe(data => {
                            this.globalS.sToast('Success', 'Data added');
                            this.search();
                            this.handleCancel();
                        })
                    }
                    if (this.addOREdit == 0) {
                        this.timeS.updatereminders(reminder).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                            this.globalS.sToast('Success', 'Data updated');
                            this.search();
                            this.handleCancel();
                        });
                    }
                }
                
                showAddModal() {
                    this.populateDropDowns()
                    this.modalOpen = true;
                    this.resetForm();
                    this.addOREdit = 1;
                }
                onItemclick(data: any, index: number) {
                    this.selectedReminders = data;
                    this.activeRowData = data;
                    this.selectedRowIndex = index;
                }
            
                
                showEditModal(data,index: any) {
                    this.addOREdit = 0;
                    this.activeRowData = data;
                    this.selectedRowIndex = index;
                    this.selectedReminders =data;

                    const { recordNumber, personID, alert, reminderDate, dueDate, address1, address2, recurring, state, email, notes} = data;
                    
                    this.inputForm.patchValue({
                        recordNumber: recordNumber,
                        personID: personID,
                        staffAlert: alert,
                        reminderDate: reminderDate,
                        dueDate: dueDate,
                        recurrInt: address1,
                        recurrStr: address2,
                        recurring: recurring,
                        listOrder: state,
                        followUpEmail: email,
                        notes: notes
                    });
                    
                    this.modalOpen = true;
                }
                
                delete(index: any) {
                    this.timeS.deleteclinicalreminders(index).pipe(
                        takeUntil(this.unsubscribe))
                        .subscribe(data => {
                            if (data) {
                                this.globalS.sToast('Success', 'Data delted');
                                this.handleCancel();
                                this.search();
                            }
                        })
                    }
                    
                    handleCancel() {        
                        this.inputForm.reset(this.default);
                        this.isLoading = false;
                        this.modalOpen = false;
                        this.addOREdit = 1;
                        this.search();
                    }
                    handleOkTop() {
                        this.generatePdf();
                        this.tryDoctype = ""
                        this.pdfTitle = ""
                    }
                    handleCancelTop(): void {
                        this.drawerVisible = false;
                        this.pdfTitle = ""
                    }                    
                    generatePdf(){
    
                        var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
                    
                        fQuery = " SELECT Name AS [Review],Name AS [Alert/Review], FORMAT(convert(datetime,Date1), 'dd/MM/yyyy')  as [Reminder Date], FORMAT(convert(datetime,Date2), 'dd/MM/yyyy')   as [Due Date], Notes FROM HumanResources WHERE PersonID = '"+this.user.id+"' AND [Type] = 'RECIPIENTALERT' AND IsNull(ReminderScope, '') <> 'CC' ORDER BY Name  ";
                        //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
                        txtTitle = "Reminders";       
                        Rptid =   "8WTerq14eNFSAnz0"  
                    
                        //console.log(fQuery)
                          const data = {
                                      
                              "template": { "_id": Rptid },                                
                              "options": {
                                  "reports": { "save": false },                
                                  "sql": fQuery,
                                  "Criteria": lblcriteria,
                                  "userid": this.tocken.user,
                                  "txtTitle": txtTitle,
                                  "Extra": this.user.code,
                                                                                  
                              }
                          }
                          this.loading = true;
                          this.drawerVisible = true;         
                              this.printS.printControl(data).subscribe((blob: any) => {
                              this.pdfTitle = txtTitle+".pdf"
                                  this.drawerVisible = true;                   
                                  let _blob: Blob = blob;
                                  let fileURL = URL.createObjectURL(_blob);
                                  this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                                  this.loading = false;
                                  this.cd.detectChanges();
                              }, err => {
                                  console.log(err);
                                  this.loading = false;
                                  this.modalService.error({
                                      nzTitle: 'TRACCS',
                                      nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                      nzOnOk: () => {
                                          this.drawerVisible = false;
                                      },
                                  });
                              });
                              return;      
                      }
                }