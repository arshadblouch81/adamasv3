import { Component, OnInit, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, forwardRef, Input, OnDestroy, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { mergeMap, takeUntil, concatMap, switchMap, distinctUntilChanged } from 'rxjs/operators';
import { TimeSheetService, GlobalService, view, ClientService, StaffService, ListService, UploadService, months, days, gender, types, titles, caldStatuses, roles, topMarginStyle,PrintService, DllService } from '@services/index';
import { forkJoin, Subscription, Observable, Subject, EMPTY, of } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import * as _ from 'lodash';

import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO'
import addMinutes from 'date-fns/addMinutes'
import isSameDay from 'date-fns/isSameDay'
import { isValid } from 'date-fns';

import { take } from 'rxjs/operators'

import { PROCESS } from '../../modules/modules';

import {  NzTreeComponent} from 'ng-zorro-antd/tree';
import { HttpClient, HttpParameterCodec } from '@angular/common/http';
import { NzFormatEmitEvent, NzTreeNode } from 'ng-zorro-antd/core';
import { resourceUsage } from 'process';
import { error } from 'console';

interface AddTimesheetModalInterface {
    index: number,
    name: string
}

export interface TreeNodeInterface {
  key: number;
  name?: string;
  shiftbookNo?: any;
  activity?: any;
  level?: number;
  expand?: boolean;
  address?: string;
  children?: TreeNodeInterface[];
  parent?: TreeNodeInterface;
}


interface CalculatedPay{
    KMAllowancesQty: number,
    AllowanceQty: number,
    WorkedHours : number,
    PaidAsHours : number,
    PaidAsServices: number,
    WorkedAttributableHours : number,
    PaidQty: number,
    PaidAmount : number,
    ProvidedHours: number,
    BilledAsHours : number,
    BilledAsServices: number,
    BilledQty : number,
    BilledAmount : number,
    HoursAndMinutes: string
}

interface TravelNode   {
    title: any,
    key: any,
    isLeaf: boolean,
    isExpanded: boolean,
    expanded: boolean,
    icon:string;
    children:Array<TravelNode>
}

@Component({    
    styles: [`
    .header{
            position: relative;
            top: 0;    
            left:0;
            right:0;
            margin:0 auto;
            width: 50%;
            height: auto;
            border:thin solid black;
            text-overflow:ellipsis;
            overflow:hidden;
            }

    .icon-style{
       
        border: 1px solid #214474;
        border-radius: 50%;       
        height:40px;
        width:40px;
        background: transparent;
        padding-top:7px;
        margin-right: 5px;
        text-align: center;
        font-size:18px;
      
    }
       ul{
            list-style:none;
            float:right;
            margin:0;
        }
        li{
            display: inline-block;
            margin-right: 10px;
            font-size: 12px;
            padding: 5px;
            cursor:pointer;
        }
        li:hover{
            color:#177dff;
        }
        li div{
            text-align: center;
            font-size: 17px;
            width:4rem;
        }
        .selected td{
            background: #d5ffca;
        }
        nz-step ::ngdeep .ant-steps-item-container .ant-steps-item-content{
            width: auto !important;
        }
        nz-select{
            width: 11.4rem;
        }
        .time-duration{
            font-weight: 500; 
            margin-top: 8px;
        }
        .calculations{
            
        }
      :host  nz-descriptions ::ngdeep .ant-descriptions-view tr td.ant-descriptions-item-label{
            padding: 4px 16px;
        }
        :host nz-descriptions div table tbody tr:last-child td:last-child {
            background: #e1ffdc;
        }
        :host nz-descriptions ::ngdeep div table tbody tr td{
            font-size:11px;
        }
        :host label ::ngdeep nz-checkbox{
            wrap:nowrap;
            text-overflow: ellipsis;
        }
        tr{
            pointer:cursor;
        }
        .notes{
            max-height: 3rem;
            overflow: hidden;            
        }
        .atay {
            font-size: 10px !important;
            white-space: break-spaces !important;
            height: 8rem !important;
            overflow: auto !important;
        }
        nz-alert ::ngdeep .ant-alert.ant-alert-no-icon{
            padding:4px 15px;
        }
        
        .disable-div{
            opacity: 0.96;
            pointer-events: none;
        }
        nz-tree {
        overflow: hidden;
        margin: 0 -24px;
        padding: 0 24px;
       
        font-family: ' Segoe UI Bold' , tahoma, arial;;
        
      }

    :host  nz-tree  nz-node {
        font-size: 18px;
      }

      .custom-node {
        cursor: pointer;
        line-height: 24px;
        margin-left: 4px;
        display: inline-block;
     
        font-family: ' Segoe UI Bold', tahoma, arial;
       
      }
      .active {
        background: #1890ff;
        color: #fff;
      }

      .file-name,
      .folder-name {
        margin-left: 4px;
      }

      .file-desc,
      .folder-desc {
        padding: 0 8px;
        display: inline-block;
        background: #87ceff;
        color: #ffffff;
        position: relative;
        left: 12px;
      }

      .disabled{
        cursor: not-allowed;
        
      }
      .spinner{
        margin:1rem auto;
        width:1px;
    } 
     
    `],
    templateUrl: './timesheet.html',
})


export class TimesheetAdmin implements OnInit, OnDestroy, AfterViewInit {

    private unsubscribe: Subject<void> = new Subject();
    private picked$: Subscription;

    loading: boolean = false;
    loadingTrvl: boolean = false;

    @ViewChild('treeView', { static: false }) nzTreeComponent!: NzTreeComponent;
    defaultCheckedKeys = ['10020'];
    defaultSelectedKeys = ['10010'];
    defaultExpandedKeys = ['100', '1001'];
    nodes: Array<any> = [];
    nodes2: Array<any> = [];
    activedNode:any;

    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
   

    // nodes: Array<any> = [
    //     {
    //            "key":"ABFLAT GISELBERT",
    //             "title":"ABFLAT GISELBERT",
    //             icon: 'calendar',
    //             expanded: true,
    //             "children":[
    //                {
    //                   "key":"2024/04/03",
    //                   "title":"2024/04/03 KM=0",
    //                   icon: 'usergroup-add',
    //                   expanded: true,
    //                   "children":[
    //                      {
    //                         "key":"08:15",
    //                         "title":"52 Eatonsville Road, WATERVIEW HEIGHTS, NSW, 2460 KM=0",
    //                         icon: 'credit-card',
    //                         "isLeaf":true
    //                      },
    //                      {
    //                         "key":"12:00",
    //                         "title":"564 Armidale Road, SOUTH GRAFTON, NSW, 2460 KM=0",
    //                         icon: 'credit-card',
    //                         "isLeaf":true
    //                      },
    //                      {
    //                         "key":"15:00",
    //                         "title":"564 Armidale Road, SOUTH GRAFTON, NSW, 2460 KM=0",
    //                         icon: 'credit-card',
    //                         "isLeaf":true
    //                      }
    //                   ]
    //                }
    //             ]
    //          }
    //   ];

    nodes_list : Array<TravelNode> =[];
    enableAutoCreate: boolean = false;
      nzClick(event: Event): void {
        console.log(event);
      }
    
      nzCheck(event: Event): void {
        console.log(event);
      }

    //   nzClick(event: NzFormatEmitEvent): void {
    //     console.log(event);
    //   }
    
    //   nzCheck(event: NzFormatEmitEvent): void {
    //     console.log(event);
    //   }
    
      // nzSelectedKeys change
      nzSelect(keys: string[]): void {
        console.log(keys, this.nzTreeComponent.getSelectedNodeList());
      }

 ViewTravelHistory:boolean;

    timesheets: Array<any> = [];
    timesheets_original: Array<any> = [];
    timesheetsGroup: Array<any> = [];   

    topMarginStyle = topMarginStyle;

    index: number = 0;
    resultMapData: Array<any> = [];

    currentDate: string;
    payPeriodEndDate: Date;
    payPeriod:any;
    unitsArr: Array<string> = ['HOUR', 'SERVICE'];

    activity_value: number;
    durationObject: any;

    // 0 = staff; 1 = recipient
    view: number = 0;
    viewType: any;
    Prev_startDate:any;
    Prev_endDate:any;
    prevPayPeriodModel:boolean;
    topBelow = { top: '20px' }

    whatProcess = PROCESS.ADD;
    tocken: any;

    parserPercent = (value: string) => value.replace(' %', '');
    parserDollar = (value: string) => value.replace('$ ', '');
    formatterDollar = (value: number) => `${value > -1 || !value ? `$ ${value}` : ''}`;
    formatterPercent = (value: number) => `${value > -1 || !value ? `% ${value}` : ''}`;

    overlapValue: any;
    dateFormat: string = 'dd/MM/yyyy'

    timesheetForm: FormGroup;
    modalTimesheetValues: Array<AddTimesheetModalInterface> = [
        {
            index: 1,
            name: 'ADMINISTRATION'
        },
        {
            index: 2,
            name: 'ALLOWANCE CHARGEABLE'
        },
        {
            index: 3,
            name: 'ALLOWANCE NON-CHARGEABLE'
        },
        {
            index: 4,
            name: 'CASE MANAGEMENT'
        },
        {
            index: 5,
            name: 'ITEM'
        },
        {
            index: 6,
            name: 'SLEEPOVER'
        },
        {
            index: 7,
            name: 'TRAVEL TIME'
        },
        {
            index: 8,
            name: 'SERVICE'
        },
    ];

    agencyDefinedGroup: string;

    today = new Date();

    defaultStartTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
    defaultEndTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);

    payTotal: CalculatedPay;
    selected: any = null;

    selectAll: boolean = false;
    overlapVisible: boolean = false;
    addTimesheetVisible: boolean = false;
    multipleRecipientShow: boolean = false;
    isTravelTimeChargeable: boolean = false;
    isSleepOver: boolean = false;
    searchStaff: boolean = false;
    reloadSearchList:Subject<any> = new Subject() ;

    defautValue :string;
    payUnits: any;
    token:any;
    rosterGroup: string;
    Object = Object;
    mapOfExpandedData: { [key: string]: TreeNodeInterface[] } = {};
    BookingNo:boolean;
    Program:boolean;
    Activity:boolean;
    DebtorCode: boolean;
    Recipient:boolean;
    BillRate:boolean;
    PayRate: boolean;
    BillQty: boolean;
    PayQty: boolean;
    PayRateTotal: boolean;
    ClaimedStart: boolean;
    ClaimedEnd: boolean;
    DecimalDuration: boolean;
    InvDate: boolean;
    InvRef: boolean;
    ShowInfoOnly:boolean;
    IncludeHours:boolean;
    IncludeShiftPrevPeriods:boolean;
    UseExtended:boolean;
    Master:boolean;
    Cycle:string;
    lstCycles: Array<any> = ['CYCLE 1','CYCLE 2','CYCLE 3','CYCLE 4','CYCLE 5','CYCLE 6','CYCLE 7','CYCLE 8','CYCLE 9','CYCLE 10'];
    StartDate:any   ='';
     EndDate:any='';
     Calc_KM:number;
     Calc_Time:number;
     lstTravelHistory:Array<any>=[];

    collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
        console.log(data)
        if ($event === false) {
            if (data.children) {
                data.children.forEach(d => {
                    const target = array.find(a => a.key === d.key)!;
                    target.expand = false;
                    this.collapse(array, target, false);
                });
            } else {
                return;
            }
        }
    }

    convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {
        const stack: TreeNodeInterface[] = [];
        const array: TreeNodeInterface[] = [];
        const hashMap = {};

        stack.push({ ...root, level: 0, expand: true });
        // console.log(stack);
        while (stack.length !== 0) {
            const node = stack.pop()!;
            this.visitNode(node, hashMap, array);
            if (node.children) {
                for (let i = node.children.length - 1; i >= 0; i--) {
                    stack.push({ ...node.children[i], level: node.level! + 1, expand: true, parent: node });
                }
            }
        }
        // console.log(array);
        return array;
    }

    visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
        if (!hashMap[node.key]) {
            hashMap[node.key] = true;
            array.push(node);
        }
    }

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private staffS:StaffService,
        private clientS: ClientService,
        private listS: ListService,
        private http:HttpClient,
        private printS:PrintService,
        private sanitizer: DomSanitizer,
        private dllService: DllService
    ) {
        cd.detach();

        this.currentDate = format(new Date(), 'MM-dd-yyyy');

  
    }

    ngOnInit(): void{
        this.buildForm(); 
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        
        
       
    }

   
    load_Travel_Address(AccountNo:any){

        let sql=`SELECT DISTINCT  AD.RecordNumber,ro.Date, ro.[Start Time] as startTime, re.UniqueID , ro.km, 
        CASE WHEN AD.Address1 <> '' THEN AD.Address1 ELSE '' END + CASE WHEN AD.Address2 <> '' THEN ' ' + AD.Address2 ELSE '' END + CASE WHEN AD.Suburb <> '' THEN ', ' + AD.Suburb ELSE '' END + CASE WHEN Left(AD.Postcode, 1) = '0' THEN ', NT, ' + AD.Postcode      WHEN Left(AD.Postcode, 2) = '26' THEN ', ACT, ' + AD.Postcode      WHEN Left(AD.Postcode, 2) = '29' THEN ', ACT, ' + AD.Postcode      WHEN Left(AD.Postcode, 1) = '2' THEN ', NSW, ' + AD.Postcode      WHEN Left(AD.Postcode, 1) = '3' THEN ', VIC, ' + AD.Postcode      WHEN Left(AD.Postcode, 1) = '4' THEN ', QLD, ' + AD.Postcode      WHEN Left(AD.Postcode, 1) = '5' THEN ', SA, ' + AD.Postcode      WHEN Left(AD.Postcode, 1) = '6' THEN ', WA, ' + AD.Postcode      WHEN Left(AD.Postcode, 1) = '7' THEN ', TAS, ' + AD.Postcode ELSE '' END AS Address, GoogleAddress
        FROM roster ro INNER JOIN Recipients re ON ro.[client code] = re.accountno 
        INNER JOIN ItemTypes ON ro.[Service Type] = ItemTypes.Title  
        INNER JOIN  NamesAndAddresses AD ON  PrimaryAddress = 1 AND  AD.PersonID=RE.UniqueID
        WHERE [Carer Code] = '${AccountNo}'  AND (Date BETWEEN '${format(new Date(this.payPeriod.start_Date),'yyyy/MM/dd')}' AND '${format(new Date(this.payPeriod.end_Date),'yyyy/MM/dd')}') 
        AND ro.Type IN (2, 3, 7, 8, 10, 11, 12) AND ISNULL(ItemTypes.ExcludeFromTravelCalc, 0) = 0  
        ORDER BY Date, [Start Time]`;

        this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
            this.lstTravelHistory = data;
            if(this.lstTravelHistory.length<=0) return;

            this.defaultCheckedKeys =  AccountNo;
            this.defaultExpandedKeys =  data[0].date;
            this.defaultSelectedKeys =  data[0].date;
          
            let node = <TravelNode>{} ;
            node.key =AccountNo
            node.title = AccountNo;
            node.isExpanded= true;
            node.expanded=true;
            node.icon='calendar';
            node.children = [];
            this.nodes.push(node);

            let prev_date:any='';
            let child = <TravelNode>{};

            this.lstTravelHistory.forEach(x=>{
               
                if (prev_date != x.date){
                   
                    if (child.children!=null){
                        this.nodes_list[0].children.push(child);                           
                    }
                    child = <TravelNode>{}; 
                    child.key = x.date;
                    child.title = x.date + ' ' + x.km + ' KM';
                    node.expanded=true;
                    node.icon='usergroup-add';
                    child.children = [];
                }
                let child2 = <TravelNode>{};             
                child2.key = x.startTime;
                child2.title = x.address + ' ' + x.km + ' KM';
                child2.isLeaf= true;
                node.expanded=true;
                node.icon='credit-card';
                child.children.push(child2);

                // if (this.nodes.filter(x=>x.key).length==0)
                //     this.nodes.push(child);
                // else
                //     this.nodes.filter(x=>x.key)[0].children.push(child);
                prev_date = x.date;
                
            });
          //  console.log(this.nodes_list)
           this.nodes = this.nodes_list //JSON.stringify(this.nodes_list);
           // console.log(this.nodes);

           
        });

          
    }

    activeNode(data: NzFormatEmitEvent): void {
        this.activedNode = data.node!;
      } 
    nzEvent(event: any): void {
        console.log(event);
       // this.loadNode();
        // load child async
        // if (event.eventName === 'expand') {
        //   const node = event.node;
        //   if (node?.getChildren().length === 0 && node?.isExpanded) {
        //     this.loadNode().then(data => {
        //       node.addChildren(data);
        //     });
        //   }
        // }
      }
        loadNode(): Promise<any[]> {
        return new Promise(resolve => {
          setTimeout(
            () =>
              resolve([
                this.timeS.loadTreavelData(this.selected.data, this.viewType).subscribe(data=>{
                    this.lstTravelHistory = data;
                    this.nodes=[];
                    if(this.lstTravelHistory.length<=0) return;
    
                    this.defaultCheckedKeys =  this.selected.data;
                    this.defaultExpandedKeys =  data[0].date;
                    this.defaultSelectedKeys =  data[0].date;
                  
                    let node = <TravelNode>{} ;
                    node.key =this.selected.data
                    node.title = this.selected.data;
                    node.isExpanded= true;
                    node.expanded=true;
                    node.icon='usergroup-add';
                    node.children = [];
                    this.nodes.push(node);
    
                    let prev_date:any='';
                    let child = <TravelNode>{};
    
                    this.lstTravelHistory.forEach(x=>{
               
                        if (prev_date != x.date){
                           
                            if (child.children!=null){                              
                                   this.nodes[0].children.push(child);                        
                            }
                            
                            child = <TravelNode>{}; 
                            child.key = x.date;
                            child.title = format(new Date(x.date),'dd/MM/yyyy') + ' ' + x.km + ' KM';
                            child.expanded=true;
                            child.icon='calendar';
                            child.children = [];
                        }
                        let child2 = <TravelNode>{};             
                        child2.key = x.address;
                        child2.title = x.address 
                        child2.isLeaf= true;
                        child2.expanded=true;
                        child2.icon='arrow-right';
                        child.children.push(child2);
                      
                        prev_date = x.date;
                        
                    });
                    if (child.children!=null){
                        this.nodes[0].children.push(child);                           
                    }
                })
              ]),
            1000
          );
        });
      }

      UpdateNodes(): Promise<any[]> {
        return new Promise(resolve => {
          setTimeout(
            () =>
              resolve([
                this.setDistances()
              ]),
            500
          );
        });
      }


       setDistances() {

        if (this.nodes.length<=0) return;     
        
        let lstnodes =this.nodes[0].children;
        let index=0;
        this.Calc_KM=0;
        this.Calc_Time=0;
        lstnodes.forEach(child=>{
        //let child =this.nodes[0].children[0]
        if (child.children.length>=2){
        let km:any ="0";
        let addresses ={
                Address1: child.children[0].title,
                Address2: child.children[1].title
             }

            // let addresses ={
            //     Address1: encodeURIComponent(child.children[0].title),
            //     Address2: encodeURIComponent(child.children[1].title)
            // }
            //`[{address1: '2/22 Fry Street, GRAFTON, NSW, 2460', address2: '52 Eatonsville Road, WATERVIEW HEIGHTS, NSW, 2460'}]`
            this.timeS.findDistance(addresses).subscribe(data=>{
            km= data.rows[0].elements[0].distance.text;
            if (km==null) km=0;
            this.Calc_KM += parseFloat(km.replace(' km',''));
            this.Calc_Time+= parseInt(km.replace(' mins',''));
           // child.title = format(new Date(child.date),'dd/MM/yyyy') + ' ' + km + ' KM';
            let title = format(new Date(child.key),'dd/MM/yyyy') + ' ' + km + ' KM';
            let node = this.nzTreeComponent.getTreeNodes();
            node[0].children.find(x=>x.key==child.key).title = title;
           
            this.nodes[0].children[index].title = title;
           if (this.Calc_KM>0)
                this.enableAutoCreate=true;            
            index += 1;
            error: (err: any) => {
                this.globalS.eToast("timesheet",err.toString)
            }
        });
           
      }
    });
}
    buildForm() {
        this.timesheetForm = this.formBuilder.group({
            recordNo: [''],
            date: [this.payPeriodEndDate, Validators.required],
            serviceType: ['', Validators.required],
            program: ['', Validators.required],
            serviceActivity: ['', Validators.required],
            payType: ['', Validators.required],
            analysisCode: [''],
            recipientCode:  [''],
            haccType: '',
            staffCode:  [''],
            debtor:  [''],
            isMultipleRecipient: false,
            isTravelTimeChargeable: false,
            sleepOverTime: '',
            time: this.formBuilder.group({
                startTime:  [''],
                endTime:  [''],
            }),
            pay: this.formBuilder.group({
                unit:  ['HOUR'],
                rate:  ['0'],
                quantity:  ['1'],
                position: ''
            }),
            bill: this.formBuilder.group({
                unit: ['HOUR'],
                rate: ['0'],
                quantity: ['1'],
                tax: '1'
            }),
        })

        this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
        this.fixStartTimeDefault();

        this.timesheetForm.get('sleepOverTime').valueChanges.pipe(
            takeUntil(this.unsubscribe)
        ).subscribe(d => {
            const { serviceType, sleepOverTime } = this.timesheetForm.value;
            if(serviceType === 'SLEEPOVER'){
                this.defaultEndTime = sleepOverTime;
            }
            this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
        });

        this.timesheetForm.get('time.startTime').valueChanges.pipe(
            takeUntil(this.unsubscribe)
        ).subscribe(d => {
            const { serviceType, sleepOverTime } = this.timesheetForm.value;
            if(serviceType === 'SLEEPOVER'){
                this.defaultEndTime = sleepOverTime;
            }
            this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
        });

        this.timesheetForm.get('isMultipleRecipient').valueChanges.pipe(
            takeUntil(this.unsubscribe),
            switchMap(d => {
                const { serviceType } = this.timesheetForm.value;
                return this.GETPROGRAMS(serviceType);
            })).subscribe(data => {
                console.log(data);
            });

        this.timesheetForm.get('payType').valueChanges.pipe(
            takeUntil(this.unsubscribe),
            switchMap(d => {
                if(!d) return EMPTY;
                return this.timeS.getpayunits(d);
            })
        ).subscribe(d => {
            this.timesheetForm.patchValue({
                pay: {
                    unit: d.unit,
                    rate: d.amount,
                    quantity: (this.durationObject.duration) ? 
                        (((this.durationObject.duration * 5) / 60)).toFixed(2) : 0
                }
            });
        });

        this.timesheetForm.get('time.endTime').valueChanges.pipe(
            takeUntil(this.unsubscribe)
        ).subscribe(d => {
            this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
        });


        this.timesheetForm.get('recipientCode').valueChanges.pipe(
            takeUntil(this.unsubscribe),
            switchMap(x => {            
                this.timesheetForm.patchValue({
                    debtor: x
                });
                return this.GETPROGRAMS(x)
            })
        ).subscribe((d: Array<any>) => {

            this.programsList = d.map(x => x.progName);
            console.log(this.programsList)

            if(this.whatProcess == PROCESS.UPDATE){
                setTimeout(() => {
                    this.timesheetForm.patchValue({
                        program: this.defaultProgram
                    });
                }, 0);
                console.log(this.timesheetForm.value)
            }         

            this.cd.markForCheck();
            this.cd.detectChanges();

            if(d && d.length == 1){
                this.timesheetForm.patchValue({
                    program: d[0].ProgName
                });
            }
            
        });

        this.timesheetForm.get('debtor').valueChanges.pipe(
            takeUntil(this.unsubscribe),
            switchMap(x => {
                if(this.selected.option == 0) return EMPTY;
                
                return this.GETPROGRAMS(x)
            })
        ).subscribe(d => {
            this.programsList = d;
        });

        this.timesheetForm.get('serviceType').valueChanges.pipe(
            takeUntil(this.unsubscribe),
            switchMap(x => {
                this.clearLowerLevelInputs();

                this.multipleRecipientShow = this.isServiceTypeMultipleRecipient(x);
                this.isTravelTimeChargeable = this.isTravelTimeChargeableProcess(x);
                this.isSleepOver = this.isSleepOverProcess(x);

                if (!x) return EMPTY;
                return forkJoin(
                    this.GETANALYSISCODE(),
                    this.GETPAYTYPE(x),
                    this.GETPROGRAMS(x)
                )
            })
        ).subscribe(d => {
            this.analysisCodeList = d[0];
            this.payTypeList = d[1];
            this.programsList = d[2].map(x => x.progName);

            if(this.viewType == 'Recipient'){
                this.timesheetForm.patchValue({
                    analysisCode: this.agencyDefinedGroup
                });
            }
        });

        this.timesheetForm.get('program').valueChanges.pipe(
            distinctUntilChanged(),
            switchMap(x => {
                if(!x) return EMPTY;
                this.serviceActivityList = [];
                this.timesheetForm.patchValue({
                    serviceActivity: null
                });
                return this.GETSERVICEACTIVITY(x)
            })
        ).subscribe((d: Array<any>) => {

            this.serviceActivityList = d.map(x => x.activity);
            console.log(d)
            console.log(this.serviceActivityList)
            if(this.whatProcess == PROCESS.UPDATE){
                setTimeout(() => {
                    this.timesheetForm.patchValue({
                        serviceActivity: this.defaultActivity
                    });
                }, 0);
            }

            if(d && d.length == 1){
                this.timesheetForm.patchValue({
                    serviceActivity: d[0]
                });
            }
        });

        this.timesheetForm.get('serviceActivity').valueChanges.pipe(
            distinctUntilChanged(),
            switchMap(x => {
                if (!x) {
                    this.rosterGroup = '';
                    return EMPTY;
                };
                return this.GETROSTERGROUP(x)
            })
        ).subscribe(d => {
            console.log(d);
            if (d.length > 1 || d.length == 0) return false;
            this.rosterGroup = (d[0].rosterGroup).toUpperCase();
            this.GET_ACTIVITY_VALUE((this.rosterGroup).trim());

            this.timesheetForm.patchValue({
                haccType: this.rosterGroup
            })
        });        
    }

    GET_ACTIVITY_VALUE(roster: string) {
        // ADMINISTRATION
        // ADMISSION
        // ALLOWANCE
        // CENTREBASED
        // GROUPACTIVITY
        // ITEM
        // ONEONONE
        // RECPTABSENCE
        // SALARY
        // SLEEPOVER
        // TRANSPORT
        // TRAVELTIME

        this.activity_value = 0;

        if (roster === 'ADMINISTRATION') {
            this.activity_value = 6;
        }

        if (roster === 'ADMISSION') {
            this.activity_value = 7;
        }

        if (roster === 'ALLOWANCE') {
            this.activity_value = 9;
        }
        
        if (roster === 'CENTREBASED') {
            this.activity_value = 11;
        }

        if (roster === 'GROUPACTIVITY') {
            this.activity_value = 12;
        }

        if (roster === 'ITEM') {
            this.activity_value = 14;
        }

        if (roster === 'ONEONONE') {
            this.activity_value = 2;
        }

        if (roster === 'RECPTABSENCE') {
            this.activity_value = 6;
        }

        if (roster === 'SALARY') {
            this.activity_value = 0;
        }

        if (roster === 'SLEEPOVER') {
            this.activity_value = 8;
        }

        if (roster === 'TRANSPORT') {
            this.activity_value = 10;
        }

        if (roster === 'TRAVEL TIME') {
            this.activity_value = 5;
        }
    }

    isEndSteps() {
        if (this.rosterGroup === 'ALLOWANCE') {
            return this.current >= 3;
        }
        else {
            return this.current >= 3;
        }
    }
    
    clearLowerLevelInputs() {

        this.timesheetForm.patchValue({
            recipientCode: null,
            debtor: null,
            program: null,
            serviceActivity: null,
            analysisCode: null,
            time: {
                startTime: '',
                endTime: '',
            },
            pay: {
                unit: 'HOUR',
                rate: '0',
                quantity: '1',
                position: ''
            },
            bill: {
                unit: 'HOUR',
                rate: '0',
                quantity: '1',
                tax: '0'
            },
        });
    }

    empty: any = [];
    temp: any;
    recurse(hehe: any, indexObj: any = null) {
        this.temp = "";
        for (var property in hehe) {            
            if (Object.prototype.toString.call(hehe[property]) == '[object Object]') {
                if (indexObj) {
                    let child = indexObj.child.push({
                        name: property,
                        child: hehe[property]
                    });
                    this.recurse(hehe[property], child)
                }

                if (indexObj == null) {
                    let child = this.empty.push({
                        name: property,
                        child: hehe[property]
                    })
                    this.recurse(hehe[property], child);
                }
            }
            if (Object.prototype.toString.call(hehe[property]) == '[object Array]') {
                return hehe[property]
            }
        }
    }   

    ngOnDestroy(): void{
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    ngAfterViewInit(): void{
        this.staffS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.payPeriodEndDate = parseISO(data.end_Date);
            this.payPeriod=data;
        });
        
        this.cd.reattach();
        this.cd.detectChanges();
    }

    trackByFn(index, item) {
        return item.id;
    }

    refresh(){
        this.picked(this.selected)
    }
    showInfoOnly_data(){
       
            if  (!this.ShowInfoOnly)               
                this.timesheets=this.timesheets_original.filter(x => x.infoOnly != true);
            else
                this.timesheets=this.timesheets_original;
        
    }  
    
    onStaffSearch(data:any){
        this.searchStaff=false;
        if (data ==null) return;  
      
        if (this.view==0) 
            this.defautValue  = data.accountno            
        else
            this.defautValue  = data.accountNo
        
        this.reloadSearchList.next({defautValue:this.defautValue, view:this.view}); 
        this.picked({data : this.defautValue, option: this.view});
    }
    
    picked(data: any) {
        this.view =data.option;

        if (!data.data) {
            this.timesheets = [];
            this.selected = null;
            return;
        }

        this.selected = data;
        this.Calc_KM=0;
        this.Calc_Time=0;
        this.viewType = this.whatType(data.option);
        this.loading = true;

        if (this.picked$) {
            this.picked$.unsubscribe();
        }

        if(this.viewType == 'Recipient'){
            this.clientS.getagencydefinedgroup(this.selected.data)
                .subscribe(data => {
                    this.agencyDefinedGroup = data.data;
                });
        }
        let PrevStartDate=''
        let PrevEndDate='';

        if (this.IncludeShiftPrevPeriods) {
            PrevStartDate=  format( this.Prev_startDate, 'yyyy/MM/dd') 
            PrevEndDate=format( this.Prev_endDate, 'yyyy/MM/dd') ;
        }

        if (!this.Master){
            this.StartDate='';
            this.EndDate='';
        }
        
       
        this.picked$ = this.timeS.gettimesheets({
            AccountNo: data.data,
            personType: this.viewType,
            startDate:this.StartDate,
            endDate:this.EndDate,
            Prev_startDate:PrevStartDate,
            Prev_endDate:PrevEndDate,
            Master: this.Master
        }).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {

                this.loading = false;

                this.timesheets = data.map(x => {
                    return {
                        shiftbookNo: x.shiftbookNo,
                        date: x.activityDate,
                        startTime: this.fixDateTime(x.activityDate, x.activity_Time.start_time),
                        endTime: this.fixDateTime(x.activityDate, x.activity_Time.end_Time),
                        duration: x.activity_Time.calculated_Duration,
                        durationNumber: x.activity_Time.duration,
                        recipient: x.recipientLocation,
                        program: x.program.title,
                        activity: x.activity.name,
                        paytype: x.payType.paytype,
                        payquant: x.pay.quantity,
                        payrate: x.pay.pay_Rate,
                        billquant: x.bill.quantity,
                        billrate: x.bill.bill_Rate,
                        approved: x.approved,
                        billto: x.billedTo.accountNo,
                        notes: x.note,
                        selected: false,

                        serviceType: x.roster_Type,
                        recipientCode: x.recipient_staff.accountNo,
                        debtor: x.billedTo.accountNo,
                        serviceActivity: x.activity.name,
                        serviceSetting: x.recipientLocation,
                        analysisCode: x.anal,

                        serviceTypePortal: x.serviceTypePortal,
                        payType: x.payType.paytype,
                        infoOnly : x.infoOnly,
                        status : x.status,
                        type : x.type

                    }
                });
               
               this.timesheets_original=this.timesheets;
               this.showInfoOnly_data();
              // this.showCalculatePay();
              this.getComputeTimeSheet();

                // this.loadNode().then(data => {  
                //     this.loadingTrvl=false;     
                //     this.UpdateNodes().then(data => {});
                    
                // });
               // this.load_Travel_Address(this.selected.data);
                // this.timesheetsGroup = this.nest(this.timesheets, ['activity']);
                 // console.log(JSON.stringify(this.timesheetsGroup));
                    
                // this.index = 0;
                
                 //this.resultMapData = this.recurseObjOuterLoop(this.timesheetsGroup);
                // this.resultMapData = this.listOfMapData
                // console.log(this.resultMapData)
                
                // this.resultMapData.forEach(item => {
                //     this.mapOfExpandedData[item.key] = this.convertTreeToList(item);
                //     console.log(this.convertTreeToList(item));
                // });
                
                // console.log(this.mapOfExpandedData)
                
            });
        
       // this.getCmputedPay(data).subscribe(x => this.computeHoursAndPay(x));
    
       
       
        this.selectAll = false;
    }
    loadMaster(){
        let s_StartDate='';
        let s_EndDate='';
     
        if (this.Master){
           
            switch (this.Cycle){
                case "CYCLE 1":  s_StartDate = "1900/01/01"; s_EndDate = "1900/01/28"; break;
                case "CYCLE 2":  s_StartDate = "1900/10/01"; s_EndDate = "1900/10/28"; break;
                case "CYCLE 3":  s_StartDate = "1901/04/01"; s_EndDate = "1901/04/28"; break;
                case "CYCLE 4":  s_StartDate = "1901/07/01"; s_EndDate = "1901/07/28"; break;
                case "CYCLE 5":  s_StartDate = "1902/09/01"; s_EndDate = "1902/09/28"; break;
                case "CYCLE 6":  s_StartDate = "1902/12/01"; s_EndDate = "1902/12/28"; break;
                case "CYCLE 7":  s_StartDate = "1903/06/01"; s_EndDate = "1903/06/28"; break;
                case "CYCLE 8":  s_StartDate = "1904/02/01"; s_EndDate = "1904/02/28"; break;
                case "CYCLE 9":  s_StartDate = "1904/08/01"; s_EndDate = "1904/08/28"; break;
                case "CYCLE 10": s_StartDate = "1905/05/01"; s_EndDate = "1905/05/28"; break;

            }
            this.StartDate=  s_StartDate;
            this.EndDate=s_EndDate;
        }  
            
        this.picked(this.selected)
    }
   getComputedPay(data: any = this.selected)  : Observable<any>{
       
        return this.timeS.getcomputetimesheet({
            AccountName: data.data,
            IsCarerCode: this.viewType == 'Staff' ? true : false,
            start_Date :  this.StartDate,
            end_Date : this.EndDate
        });
   }
   
    getComputeTimeSheet(){

        let compute={
            kmAllowancesQty: 0,
            allowanceQty: 0,
            workedHours : 0,
            paidAsHours : 0,
            paidAsServices: 0,
            workedAttributableHours : 0,
            paidQty: 0,
            paidAmount : 0,
            providedHours: 0,
            billedAsHours : 0,
            billedAsServices: 0,
            billedQty : 0,
            billedAmount : 0,
            hoursAndMinutes: ''
        }
        let filteredShifts = [];
        if (this.ShowInfoOnly && this.IncludeHours)
            filteredShifts =this.timesheets;
        else
             filteredShifts = this.timesheets.filter(x => x.approved == true && x.infoOnly == false);
         //&& x.status in [2,3] && x.type in [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14]

        filteredShifts.forEach(x => {
            compute.kmAllowancesQty += x.payquant;
            compute.allowanceQty += x.payquant;
            compute.workedHours += (x.durationNumber/12);
            compute.paidAsHours += (x.durationNumber/12);
            compute.paidAsServices += x.payquant;
            compute.workedAttributableHours += (x.durationNumber/12);
            compute.paidQty += x.payquant;
            compute.paidAmount += (x.payquant*x.payrate);
            compute.providedHours += (x.durationNumber/12);
            compute.billedAsHours += x.billquant;
            compute.billedAsServices += x.billquant;
            compute.billedQty += x.billquant;
            compute.billedAmount += (x.billquant*x.billrate);
            
        });

        this.computeHoursAndPay(compute);
    }

    computeHoursAndPay(compute: any): void{
        var hourMinStr;

        if (compute.workedHours && compute.workedHours > 0) {
            const hours = Math.floor(compute.workedHours * 60 / 60);
            const minutes = ('0' + compute.workedHours * 60 % 60).slice(-2);

            hourMinStr = `${hours}:${minutes}`
        }

        var _temp = {
            KMAllowancesQty: compute.kmAllowancesQty || 0,
            AllowanceQty: compute.allowanceQty || 0,
            WorkedHours: compute.workedHours || 0,
            PaidAsHours: compute.paidAsHours || 0,
            PaidAsServices: compute.paidAsServices || 0,
            WorkedAttributableHours: compute.workedAttributableHours || 0,
            PaidQty: compute.paidQty || 0,
            PaidAmount: compute.paidAmount || 0,
            ProvidedHours: compute.providedHours || 0,
            BilledAsHours: compute.billedAsHours || 0,
            BilledAsServices: compute.billedAsServices || 0,
            BilledQty: compute.billedQty || 0,
            BilledAmount: compute.billedAmount || 0,
            HoursAndMinutes: hourMinStr
        };

        this.payTotal = _temp;
    }

    fixDateTime(date: string, timedate: string) {
        var currentDate = parseISO(date);
        var currentTime = parseISO(timedate);

        var newDate = format(
            new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate(),
                currentTime.getHours(),
                currentTime.getMinutes(),
                currentTime.getSeconds()
            ), "yyyy-MM-dd'T'HH:mm:ss");
        
        return newDate;
    }

    recurseSubDirectories(data: any) {
        var arr: Array<TreeNodeInterface> = [];        
        this.index++;

        if (Object.prototype.toString.call(data) === '[object Array]') {
            arr.push({
                key: this.index,
                name: Object.keys(data)[0],
                children: data
            });
        }

        if (Object.prototype.toString.call(data) === '[object Object]') {
            for (var key in data) {
                arr.push({
                    key: this.index,
                    name: key,
                    children: this.recurseSubDirectories(data[key])
                });
            }
        }

        return arr;
    }

    recurseObjOuterLoop(data: any) {
        var out = [];        
        for (var key in data) {
            if (Object.prototype.toString.call(data[key]) === '[object Object]') {
                out.push({ key: this.index, name: key, children: { ...(this.recurseSubDirectories(data[key])) } });
                this.recurseObjOuterLoop(data[key]);
            }

            if (Object.prototype.toString.call(data[key]) === '[object Array]') {
                out.push({ key: this.index++, name: key, children: { ...data[key] } });
            }
        }
        return out;
    }

    nest = (seq: any[], keys: Array<string | ((obj: any) => string)>) => {
        if (!keys.length) {
            return seq;
        }
        const [first, ...rest] = keys;
        return _.mapValues(_.groupBy(seq, first), (value) => this.nest(value, rest));
    };

    clickme(){
   
    }

    openPayPeriod(){
        
        this.prevPayPeriodModel=this.IncludeShiftPrevPeriods;
    }
    processPrevPayPeriod(){
        this.prevPayPeriodModel=false;
        this.StartDate= format(this.Prev_startDate, 'yyyy-MM-dd');
        this.EndDate= format(this.Prev_endDate, 'yyyy-MM-dd');
        this.refresh();

    }
    confirm(index: number) {
       
        if (index == 0) {
            this.resetAddTimesheetModal();
           // if (this.viewType=='Staff' || this.view==0)
                this.searchStaff = true;
           
        }
        if (!this.selected && this.timesheets.length > 0) return;

        if (index == 1) {
            this.resetAddTimesheetModal();
            this.addTimesheetVisible = true;
            this.whatProcess = PROCESS.ADD
        }

        if (index == 2) {
            this.modalService.confirm({
                nzTitle: '<b>Do you want to delete highlighted shifts?</b>',
                nzContent: '<b></b>',
                nzOnOk: () => this.process(index)
            });
        }

        if (index == 3) {
            this.modalService.confirm({
                nzTitle: '<b>TRACCS</b>',
                nzContent: '<b>Do you want to delete all unapproved items?</b>',
                nzOnOk: () => this.process(index)
            });
        }

        if (index == 4) {
            this.overlapVisible = true;
            //this.OVERLAP_PROCESS(this.timesheets);           

            // this.modalService.confirm({
            //     nzTitle: '<b>Automatic Overlap Removal</b>',
            //     nzContent: `
            //         <div>This action will force any overlapping shifts to a later start time to remove the overlap. All entries must be either approved or unapproved. You cannot use this function if some entries are approved and others are not.</div>
            //         <div>If you want to force a timegap between overlapping shifts - select 5 minutes from the drop down or if a gap is not needed accept the defaultProgram of 0</div>
            //         <butto (click)="clickme()">Click</butto>
            //     `,
            //     nzOnOk: () => this.process(index)
            // });
        }

        if (index == 5) {
            this.modalService.confirm({
                nzTitle: '<b>Do you want to approve all items?</b>',
                nzContent: '<b></b>',
                nzOnOk: () => this.process(index)
            });
        }

        if (index == 6) {
            this.modalService.confirm({
                nzTitle: '<b>Do you want to unapprove all items?</b>',
                nzContent: '<b></b>',
                nzOnOk: () => this.process(index)
            });
        }

        if(index == 7){
            // console.log(this.timesheets.filter(x => x.selected).length);
            // return;
            if(this.timesheets.filter(x => x.selected).length > 1){

                this.modalService.warning({
                    nzTitle: 'Select Warning',
                    nzContent: 'You can only select one item on edit...'
                  });
            } 
            
            if(this.timesheets.filter(x => x.selected).length == 1){
                this.details(this.timesheets.shift())
            }
        }
        if(index == 8){
            this.generatePdf();
        }
    }

    handleCancel() {
        this.overlapVisible = false;
        this.overlapValue = null;
        this.addTimesheetVisible = false;
    }

    removeOverlap() {
        this.OVERLAP_PROCESS(this.timesheets, this.overlapValue);
    }

    selectAllChange(event: any) {
        this.cd.detach();
        this.timesheets.forEach(x => x.selected = event);
        this.cd.reattach();
        this.cd.detectChanges();
    }


    selectedTimesheet(event: any, data: any) {
        data.selected = event;
        // this.timesheets = [...this.timesheets, data]
        // this.timesheets = this.timesheets.filter(x => x.shiftbookNo == data.shiftbookNo);

        this.cd.detectChanges();
    }

    ifRosterGroupHasTimePayBills(rosterGroup: string) {
        return (
            rosterGroup === 'ADMINISTRATION' ||
            rosterGroup === 'ADMISSION' ||
            rosterGroup === 'CENTREBASED' ||
            rosterGroup === 'GROUPACTIVITY' ||
            rosterGroup === 'ITEM' ||
            rosterGroup === 'ONEONONE' ||
            rosterGroup === 'SLEEPOVER' ||
            rosterGroup === 'TRANSPORT' ||
            rosterGroup === 'TRAVELTIME'
        );
    }

    OVERLAP_PROCESS(rosters: Array<any>, overlapCounter: number) {

        let _unique = null;
        let endTime: Date;

        var cloneRosters = _.cloneDeep(rosters);

        cloneRosters.forEach(roster => {
            if (roster && roster.date != _unique) {
                _unique = roster.date;
                endTime = parseISO(roster.endTime);
            } else {
                if (overlapCounter == 0) {
                    roster.startTime = format(endTime, "yyyy-MM-dd'T'hh:mm:ss");
                    roster.endTime = format(addMinutes(parseISO(roster.startTime), roster.durationNumber * 5), "yyyy-MM-dd'T'hh:mm:ss");
                    endTime = parseISO(roster.endTime);
                }
                
                if (overlapCounter == 5){
                    roster.startTime = format(addMinutes(endTime, 5), "yyyy-MM-dd'T'hh:mm:ss");
                    roster.endTime = format(addMinutes(parseISO(roster.startTime), roster.durationNumber * 5), "yyyy-MM-dd'T'hh:mm:ss");
                    endTime = parseISO(roster.endTime);
                }
            }
        });

        var inputs = cloneRosters.map(x => {
            return {
                RecordNo: x.shiftbookNo,
                startTime: format(parseISO(x.startTime), "hh:mm")
            }
        })

        var ss = {
            OverLaps: inputs
        }

        this.timeS.updatetimeoverlap(ss).subscribe(data => {
            this.globalS.sToast('Success', 'Time Overlap Processed');
            this.picked(this.selected);
            this.handleCancel();
        });
    }
    
    showCalculatePay(){
       
        this.getComputedPay().pipe(takeUntil(this.unsubscribe)
            ).subscribe(data => {
                this.computeHoursAndPay(data);
            });           
    }

    checkBoxChange(event: any, timesheet: any){
        //this.getComputedPay();

        const tdate = parseISO(timesheet.date);

        this.timeS.getclosedate({ program: timesheet.program })
            .pipe(
                switchMap(x => {
                    if(x.closeDate == null){
                        return this.timeS.selectedApprove({
                            AccountNo: timesheet.shiftbookNo,
                            PersonType: this.GET_VIEW(),
                            Status: event
                        });
                    }
                    
                    const closeDate = parseISO(x.closeDate);

                    if(closeDate.toString() !== 'Invalid Date' && tdate.toString() !== 'Invalid Date' && (isSameDay(tdate, closeDate) || tdate > closeDate)){                    
                        return this.timeS.selectedApprove({
                            AccountNo: timesheet.shiftbookNo,
                            PersonType: this.GET_VIEW(),
                            Status: event
                        });                        
                    } else {
                        timesheet.approved = !event;
                        this.globalS.eToast('Error', `You cannot approve/unapprove entries for this program on that date - as the program is closed for action prior to ${ format(closeDate,'dd/MM/yyyy') }`)
                        return EMPTY;
                    }                    
                }),
                switchMap( x => this.getComputedPay())
            ).subscribe(data => {
                this.computeHoursAndPay(data);
            });           
    }

    GETBILLINGRATE(){
        
    }

    GET_VIEW(): string {
        return this.selected.option == 1 ? 'Recipient' : 'Staff'
    }

    process(index: number) {
        if (!this.selected && this.timesheets.length > 0) {
                     return;
        };

        if (index == 2) {
            const shiftArray = this.timesheets.filter(x => x.selected).map(x => x.shiftbookNo)

            if(shiftArray.length == 0){
                this.globalS.wToast('No Highlighted Item','Warning');
                return;
            }
            
            this.timeS.deleteshift(shiftArray)
                .subscribe(data => {
                    this.globalS.sToast('Success','Selected items are deleted');
                    this.picked(this.selected);                
                });
        }

        if (index == 3) {
            
            // const shiftArray = this.timesheets.filter(x => x.app).map(x => x.shiftbookNo)

            let input = {
                AccountNo: this.selected.data,
                PersonType: this.GET_VIEW(),
                Status: 1
            }

            this.timeS.deleteunapprovedall(input).subscribe(data => {
                this.globalS.sToast('Success', data.message);
                this.picked(this.selected);              
            });
        }

        if (index == 5) {
            this.timeS.approveAll({
                accountNo: this.selected.data,
                PersonType: this.GET_VIEW()
            }).subscribe(data => {
                this.globalS.sToast('Success', 'All items are approved');
                this.picked(this.selected);                
            });
        }

        if (index == 6) {
            this.timeS.unapproveAll({
                accountNo: this.selected.data,
                PersonType: this.GET_VIEW()
            }).subscribe(data => {
                this.globalS.sToast('Success', 'All items are unapproved');
                this.picked(this.selected);
            });
        }
    }

    isServiceTypeMultipleRecipient(type: string): boolean {
        return type === 'SERVICE';
    }

    isTravelTimeChargeableProcess(type: string): boolean {
        return type === 'TRAVEL TIME';
    }

    isSleepOverProcess(type: string): boolean {
        return type == 'SLEEPOVER';
    }


    whatType(data: number): string {
        return data == 0 ? 'Staff' : 'Recipient';
    }    
    duration: any;

    ngModelChangeStart(event): void{
        this.timesheetForm.patchValue({
            time: {
                startTime: event
            }
        })
    }

    ngModelChangeEnd(event): void {
        this.timesheetForm.patchValue({
            time: {
                endTime: event
            }
        })
    }

    GETPROGRAMS(type: string): Observable<any> {
        console.log(type);
        let sql;
        if (!type) return EMPTY;
        const { isMultipleRecipient } = this.timesheetForm.value;
        if (type === 'ADMINISTRATION' || type === 'ALLOWANCE NON-CHARGEABLE' || type === 'ITEM' || (type == 'SERVICE' && !isMultipleRecipient)) {
            sql = `SELECT Distinct [Name] AS ProgName FROM HumanResourceTypes WHERE [group] = 'PROGRAMS' AND ISNULL(UserYesNo3,0) = 0 AND (EndDate Is Null OR EndDate >=  '${this.currentDate}') ORDER BY [ProgName]`;
        } else {
            sql = `SELECT Distinct [Program] AS ProgName FROM RecipientPrograms 
                INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID 
                WHERE Recipients.AccountNo = '${type}' AND RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') ORDER BY [ProgName]`
        }
        if (!sql) return EMPTY;
        return this.listS.getlist(sql);
    }

    GETRECIPIENT(view: number): string {
        const { recipientCode, debtor, serviceType, isMultipleRecipient } = this.timesheetForm.value;
        if(view == 0){
            if(serviceType == 'SERVICE' && isMultipleRecipient) return '!MULTIPLE';
            if(this.globalS.isEmpty(recipientCode)) return '!INTERNAL';
            return recipientCode;
        }

        if(view == 1){
            return debtor;
        }
    }

    GETSERVICEACTIVITY(program: any): Observable<any> {

        const { serviceType, date, time } = this.timesheetForm.value;

        var _date = date;
        if (!program) return EMPTY;

        if (serviceType != 'ADMINISTRATION' && serviceType != 'ALLOWANCE NON-CHARGEABLE' && serviceType != 'ITEM'  || serviceType != 'SERVICE') {

            if(typeof date === 'string'){
                _date = parseISO(_date);
            }

            return this.listS.getserviceactivityall({
                program,
                recipient: this.GETRECIPIENT(this.selected.option),
                mainGroup: serviceType,
                viewType: this.viewType,
                date: format(_date, 'yyyy/MM/dd'),
                startTime: format(this.defaultStartTime,'hh:mm'),
                endTime: format(this.defaultEndTime,'hh:mm'),
                duration: this.durationObject?.duration
            });
        }
        else {
            let sql = `SELECT DISTINCT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid 
                WHERE SO.serviceprogram = '${ program}' AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] = 'ADMINISTRATION' AND processclassification = 'OUTPUT' AND ( ITM.enddate IS NULL OR ITM.enddate >= '${this.currentDate}' )) ORDER BY [service type]`;
            
            // let sql = `SELECT DISTINCT [Service Type] AS activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
            //     WHERE SO.ServiceProgram = '${ program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND 
            //     ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}')) ORDER BY [Service Type]`;
            return this.listS.getlist(sql);
        }
    }

    GETANALYSISCODE(): Observable<any>{
        return this.listS.getserviceregion();
    }

    GETROSTERGROUP(activity: string): Observable<any>{
        if (!activity) return EMPTY;
        return this.listS.getlist(`SELECT RosterGroup, Title FROM ItemTypes WHERE Title= '${activity}'`);
    }

    GETPAYTYPE(type: string): Observable<any> {
        // `SELECT TOP 1 RosterGroup, Title FROM  ItemTypes WHERE Title = '${type}'`
        let sql;
        if (!type) return EMPTY;
        if (type === 'ALLOWANCE CHARGEABLE' || type === 'ALLOWANCE NON-CHARGEABLE') {
            sql = `SELECT Recnum, Title FROM ItemTypes WHERE RosterGroup = 'ALLOWANCE ' 
                AND Status = 'NONATTRIBUTABLE' AND ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY TITLE`
        } else {
            sql = `SELECT Recnum, LTRIM(RIGHT(Title, LEN(Title) - 0)) AS Title
            FROM ItemTypes WHERE RosterGroup = 'SALARY'   AND Status = 'NONATTRIBUTABLE'   AND ProcessClassification = 'INPUT' AND Title BETWEEN '' 
            AND 'zzzzzzzzzz'AND (EndDate Is Null OR EndDate >= '${ this.currentDate }') ORDER BY TITLE`
        }
        return this.listS.getlist(sql);
    }

    // Add Timesheet
    current = 0;
    nextDisabled: boolean = false;
    programsList: Array<any> = [];
    serviceActivityList: Array<any>;
    payTypeList: Array<any> = [];
    analysisCodeList: Array<any> = []
    
    showRecipient(): boolean  {
        const { serviceType, isMultipleRecipient, isTravelTimeChargeable } = this.timesheetForm.value;
        // console.log(serviceType + '' + isTravelTimeChargeable)

        if(serviceType === 'TRAVEL TIME' && isTravelTimeChargeable){
            return true;
        }       


        if(((serviceType !== 'ADMINISTRATION' && serviceType !== 'ALLOWANCE NON-CHARGEABLE' && serviceType !=='TRAVEL TIME') && !isMultipleRecipient)){
            return true;
        }

        return false;
    }

    get showTime(): boolean {
        const { serviceType } = this.timesheetForm.value;
        if(serviceType === 'ALLOWANCE CHARGEABLE' || serviceType === 'ALLOWANCE NON-CHARGEABLE')
            return false;

        return true;
    }

    get showEndTime(): boolean{
        const { serviceType } = this.timesheetForm.value;
        if(serviceType === 'SLEEPOVER'){
            return false;
        }
        return true;
    }

    canProceed() {
        const { date, serviceType } = this.timesheetForm.value;

        if (this.current == 0) {
            if (!date || !serviceType) {
                this.nextDisabled = true;
            } else {
                this.nextDisabled = false;
            }
            return true;
        }

        if (this.current == 1) {
            return true;
        }

        if (this.current == 2) {
            return true;
        }

        if (this.current == 3) {
            return true;
        }
    }

    defaultOpenValue = new Date(0, 0, 0, 9, 0, 0);

    resetAddTimesheetModal() {
        this.current = 0;
        this.rosterGroup = '';

        this.timesheetForm.reset({
            date: this.payPeriodEndDate,
            serviceType: null,
            program: null,
            serviceActivity: null,
            payType: '',
            analysisCode: '',
            recipientCode: '',
            debtor: '',
            isMultipleRecipient: false,
            isTravelTimeChargeable: false,
            sleepOverTime: new Date(0, 0, 0, 9, 0, 0),
            time: this.formBuilder.group({
                startTime: '',
                endTime: '',
            }),
            pay: this.formBuilder.group({
                unit: 'HOUR',
                rate: '0',
                quantity: '1',
                position: ''
            }),
            bill: this.formBuilder.group({
                unit: 'HOUR',
                rate: 0,
                quantity: '1',
                tax: '1'
            }),
        });
        
        this.defaultStartTime = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
        this.defaultEndTime = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);        
        this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    }

    pre(): void {
        this.current -= 1;
    }

    next(): void {
        this.current += 1;

        if(this.whatProcess == PROCESS.UPDATE) return;

        if(this.current == 1 && this.selected.option == 1){
            this.timesheetForm.patchValue({
                debtor: this.selected.data
            });
        }

        if(this.current == 4){
            const { recipientCode, program, serviceActivity } = this.timesheetForm.value;

            if(!this.globalS.isEmpty(recipientCode) &&
                    !this.globalS.isEmpty(serviceActivity) &&
                        !this.globalS.isEmpty(program)){
                this.timeS.getbillingrate({
                    RecipientCode: recipientCode,
                    ActivityCode: serviceActivity,
                    Program: program
                }).subscribe(data => {
                    this.timesheetForm.patchValue({
                        bill: {
                            unit: data.unit,
                            rate: this.DEFAULT_NUMERIC(data.rate),
                            tax: this.DEFAULT_NUMERIC(data.tax)
                        }
                    });
                });
            }            
        }
    }

    DEFAULT_NUMERIC(data: any): number{
        if(!this.globalS.isEmpty(data) && !isNaN(data)){
            return data;
        }
        return 0;
    }

    get nextCondition() {
        // console.log(this.rosterGroup)
        if (this.current == 2 && !this.ifRosterGroupHasTimePayBills(this.rosterGroup)) {
            return false; 
        }
        if(this.current == 3 && this.rosterGroup == 'ADMINISTRATION'){
            return false;
        }
        return this.current < 4;
    }


    get showDone(){
        return this.current >= 4 || (this.rosterGroup == 'ADMINISTRATION' && this.current>=3);
    }

    get isFormValid(){
        return  this.timesheetForm.valid;
    }

    done(): void {
        this.fixStartTimeDefault();
        
        const tsheet = this.timesheetForm.value;
        let clientCode = this.FIX_CLIENTCODE_INPUT(tsheet);

        var durationObject = (this.globalS.computeTimeDATE_FNS(tsheet.time.startTime, tsheet.time.endTime));

        if(typeof tsheet.date === 'string'){
            tsheet.date = parseISO(tsheet.date);
        }
        let inputs = {
            anal: tsheet.analysisCode || "",
            billQty: parseInt(tsheet.bill.quantity || 0),
            billTo: clientCode,
            billUnit: tsheet.bill.unit || 0,
            blockNo: durationObject.blockNo,
            carerCode: this.selected.option == 0 ? this.selected.data : tsheet.staffCode,
            clientCode: this.selected.option == 0 ? clientCode : this.selected.data,
            costQty: parseInt(tsheet.pay.quantity || 0),
            costUnit: tsheet.pay.unit || 0,
            date: format(tsheet.date,'yyyy/MM/dd'),
            dayno: parseInt(format(tsheet.date, 'd')),
            duration: durationObject.duration,
            groupActivity: false,
            haccType: tsheet.haccType || "",
            monthNo: parseInt(format(tsheet.date, 'M')),
            program: tsheet.program,
            serviceDescription: tsheet.payType || "",
            serviceSetting:  "",
            serviceType: tsheet.serviceActivity || "",
            // serviceType: this.DETERMINE_SERVICE_TYPE_NUMBER(tsheet.serviceType),
            staffPosition:  "",
            startTime: format(tsheet.time.startTime,'HH:mm'),
            status: "1",
            taxPercent: parseInt(tsheet.bill.tax || 0),
            transferred: 0,
            // type: this.activity_value,
            type: this.DETERMINE_SERVICE_TYPE_NUMBER(tsheet.serviceType),
            unitBillRate: parseInt(tsheet.bill.rate || 0),
            unitPayRate: tsheet.pay.rate || 0,
            yearNo: parseInt(format(tsheet.date, 'yyyy')),
            serviceTypePortal: tsheet.serviceType,
            recordNo: tsheet.recordNo
        };

        if(!this.timesheetForm.valid){
            this.globalS.eToast('Error', 'All fields are required');
            return;
        }

        if(this.whatProcess == PROCESS.ADD){
            this.timeS.posttimesheet(inputs).subscribe(data => {
                this.globalS.sToast('Success', 'Timesheet has been added');
                this.addTimesheetVisible = false;
                this.picked(this.selected);
            });
        }   
        
        if(this.whatProcess == PROCESS.UPDATE){
            this.timeS.updatetimesheet(inputs).subscribe(data => {
                this.globalS.sToast('Success', 'Timesheet has been updated');
                this.addTimesheetVisible = false;
                this.picked(this.selected);
            });
        }
    }

    FIX_CLIENTCODE_INPUT(tgroup: any): string{
        if (tgroup.serviceType == 'ADMINISTRATION' || tgroup.serviceType == 'ALLOWANCE NON-CHARGEABLE' || tgroup.serviceType == 'ITEM') {
            return "!INTERNAL"
        }

        if (tgroup.serviceType == 'SERVICE' || tgroup.serviceType == 'TRAVEL TIME') {
            if (tgroup.isMultipleRecipient) {
                return "!MULTIPLE"
            }
            return tgroup.recipientCode;
        }

        return tgroup.recipientCode;
    }

    fixStartTimeDefault() {
        const { time } = this.timesheetForm.value;
        if (!time.startTime) {
            this.ngModelChangeStart(this.defaultStartTime)
        }

        if (!time.endTime) {
            this.ngModelChangeEnd(this.defaultEndTime)
        }
    }

    defaultProgram: any = null;
    defaultActivity: any = null;
    defaultCategory: any = null;

    details(index: any){

        this.whatProcess = PROCESS.UPDATE;
        console.log(index);
        const {
            activity, 
            serviceType, 
            analysisCode, 
            approved, 
            billquant,
            billrate, 
            billto,
            date, 
            debtor,
            duration, 
            durationNumber,
            serviceTypePortal,
            recipientCode,
            startTime,
            program,
            payType,
            shiftbookNo,
            endTime } = index;


        // this.timesheetForm.patchValue({
        //     serviceType: this.DETERMINE_SERVICE_TYPE(index),
        //     date: date,
        //     program: program,
        //     serviceActivity: activity,
        //     payType: payType,
        //     analysisCode: analysisCode,
        //     recordNo: shiftbookNo,            
            
        //     recipientCode: recipientCode,
        //     debtor: debtor
        // });

       this.defaultStartTime = parseISO(startTime);
        this.defaultEndTime = parseISO(endTime);
        this.current = 0;

        console.log(this.defaultEndTime)

        this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);

        setTimeout(() => {
            this.addTimesheetVisible = true;

            
            this.defaultProgram = program;
            this.defaultActivity = activity;
            this.defaultCategory = analysisCode;

            this.timesheetForm.patchValue({
                serviceType: this.DETERMINE_SERVICE_TYPE(index),
                date: date,
                program: program,
                serviceActivity: activity,
                payType: payType,
                analysisCode: analysisCode,
                recordNo: shiftbookNo,            
                
                recipientCode: recipientCode,
                debtor: debtor
            });
        }, 100);
    }

     // Add Timesheet
     DETERMINE_SERVICE_TYPE(index: any): any{
        console.log(index);
        const { serviceType, debtor } = index;

        // ALLOWANCE NON CHARGEABLE 
        if(serviceType == 9 && debtor == '!INTERNAL'){
            return 'ALLOWANCE NON-CHARGEABLE';
            return this.modalTimesheetValues[2];
        }

        // ALLOWANCE CHARGEANLE 
        if(serviceType == 9 && debtor != '!INTERNAL'){
            return 'ALLOWANCE CHARGEABLE';
        }

        // ADMINISTRATION
        if(serviceType == 6){            
            return 'ADMINISTRATION';
        }

        // CASE MANAGEMENT
        if(serviceType == 7){
            return 'CASE MANAGEMENT';
        }

        // ITEM
        if(serviceType == 15){
            return 'ITEM';
        }

        // SLEEPOVER
        if(serviceType == 8){

            return 'SLEEPOVER';
        }

        // TRAVEL TIME
        if(serviceType == 5){

            return 'TRAVEL TIME';
        }

        //SERVICE
        return 'SERVICE';

     }

     DETERMINE_SERVICE_TYPE_NUMBER(index: string): number{
        // ALLOWANCE NON CHARGEABLE 
        if(index == 'ALLOWANCE NON-CHARGEABLE' || index == 'ALLOWANCE CHARGEABLE'){
            return 9;
        }

        // ADMINISTRATION
        if(index == 'ADMINISTRATION'){            
            return 6;
        }

        // CASE MANAGEMENT
        if(index == 'CASE MANAGEMENT'){
            return 7;
        }

        // ITEM
        if(index == 'ITEM'){
            return 15;
        }

        // SLEEPOVER
        if(index == 'SLEEPOVER'){

            return 8;
        }

        // TRAVEL TIME
        if(index == 'TRAVEL TIME'){

            return 5;
        }

        //SERVICE
        return 2;
     }

     AwardInterpreter(){
        let AnyUnapprovedLines : boolean = false;
            //this.globalS.iToast('Information', 'Processing AwardInterpreter');
            this.timesheets.forEach(x => {
                if (x.approved == 0) { 
                    AnyUnapprovedLines = true;                    
                }
            });

            if (AnyUnapprovedLines){
                this.modalService.confirm({
                    nzTitle: 'Timesheet Unapproved Lines Warning',
                    nzContent: 'There are unapproved shifts on the timesheet. Award processing ignores unapproved lines. This can potentially result in incorrect interpretation and wages if the shifts are inadvertantly unapproved. Please confirm you intentionally wish to process award rates without approving ALL lines on the timesheet by clicking OK - otherwise click CANCEL?' ,
                    nzOnOk: () => this.CallAwardAPI(),
                    nzOnCancel: () => console.log('Award cancelled')
                });
                
            }else{
                this.CallAwardAPI();
            }
     }

     CallAwardAPI(){
        this.globalS.iToast('Information', 'Processing AwardInterpreter');
        let input_data = {
            User: "sysmgr",
            Password: "sysmgr",
             s_StaffCode : this.selected.data,
             WEDate:"18/04/2024"
        };

        this.dllService.postAwardInterpreter(input_data).subscribe(data => {
            if (data=='True')
                this.globalS.sToast('Success', 'Processed completed Successfully' );
            else
                this.globalS.eToast('Error', 'Processed not completed due to some error\n' + data);
            error => {
                this.globalS.eToast('Error', error);
            }
        });

        // this.dllService.postAwardInterpreter(input_data).subscribe(data => {
        //     if (data=='True')
        //         this.globalS.sToast('Success', 'AwardInterpreter Processed ' );
        //     else
        //         this.globalS.eToast('Error', 'Processed not completed due to some error\n' + data);
        //     error => {
        //         this.globalS.eToast('Error', error);
        //     }
        // });
     }
     print(){

     }

   
     CalculateTravel(){
        this.ViewTravelHistory=true;     
        //this.setDistances();
        this.nodes=[];
       this.loadingTrvl=true;
         this.nodes=[];
      // this.globalS.iToast('info', 'Calculating Travel Time');
     
    
            this.loadNode().then(data => {  
                this.loadingTrvl=false;     
                setTimeout(() =>{
                this.UpdateNodes().then(data => {
                    console.log(data);
                });

                },2000);  
                
                
            });
  
           // this.UpdateNodes().then(data => { this.loadingTrvl=false; });

//         let d_DlyDistance : Double, d_TtlDistance : number, s_DistanceText : String, d_DlyDuration : number, d_TtlDuration : number
//         let s_Duration : String, s_Address : String, c_Addresses : New Collection, v_Address : Variant, i_RowNumber : Long, oRow : JSRowData, s_Addresses(1000) : Variant
//         let s_Date : String, b_FromB:e : Boolean, b_ToB:e : Boolean, s_StartAddress : String, s_PreviousAddress : String
//         let i_HDRAdj : Integer, X : Integer, nodStaff : MSComctlLib.node, nodDate : MSComctlLib.node, nodAddress : MSComctlLib.node
//         let s_Provider : String
        
    
// 102     ClearTreeViewNodes tvw.hwnd
// 104     fr(2).BackColor = vbWhite: fr(3).BackColor = vbWhite
// 106     fr(2).Height = Me.Height * 0.9
// 108     tvw.Height = fr(3).Height - 60
// 110     shp(7).Height = fr(2).Height - 900
    
// 112     b_FromBase = chkx(0).Value = xtpChecked
// 114     b_ToBase = chkx(1) = xtpChecked
// 116     If b_FromBase Or b_ToBase Then s_StartAddress = GetStaffBranchAddress(txxtAccountNo)
//         s_Provider = DBLookup(TraccsDb, "TravelProvider", "Registration", , 1)
    
//         Dim s1 As String, rsM As New ADODB.Recordset, s_Records As String
    
//         With gexTimesheet
//             If .SelectedItems.Count > 1 Then
//                 Dim simTemp As JSSelectedItem
//                 .Redraw = False
//                 For Each simTemp In .SelectedItems
//                     If simTemp.RowType = jgexRowTypeRecord Then
//                         Set oRow = .GetRowData(simTemp.RowPosition)
//                         s_Records = IIf(s_Records <> "", s_Records & "," & oRow.Value(1), oRow.Value(1))
//                     End If
//                 Next
//                 s_Records = "(" & s_Records & ")"
//                 .Redraw = True
//             End If
//         End With
     
// 118     s1 = SetMapSQL(s_Records)
    
// 120     With rsM
// 122         .Open s1, TraccsDb, adOpenForwardOnly, adLockReadOnly
// 124         pb.Max = .RecordCount: pb.Value = 0
// 126         s_Date = NullToStr(!Date)
// 128         Set nodStaff = tvw.Nodes.Add(, , "nod_" & txxtAccountNo, txxtAccountNo, 1, 1)
// 130         Set nodDate = tvw.Nodes.Add("nod_" & txxtAccountNo, tvwChild, "nod_" & s_Date, Format$(s_Date, "dd/mm/yyyy"), 2, 2)
// 132         If b_FromBase Then
// 134             c_Addresses.Add s_StartAddress
// 136             s_Addresses(1) = s_Address
// 138             Set nodAddress = tvw.Nodes.Add("nod_" & s_Date, tvwChild, s_StartAddress, s_StartAddress)
// 140             i_HDRAdj = 1
//             End If
// 142         Do Until .EOF
// 144             If NullToStr(!Date) <> s_Date Then
// 146                 s_PreviousAddress = ""
// 148                 If b_ToBase Then
// 150                     c_Addresses.Add s_StartAddress
// 152                     Set nodAddress = tvw.Nodes.Add("nod_" & s_Date, tvwChild, NullToStr(i_RowNumber) & "E_" & s_StartAddress, s_StartAddress)
// 154                     Increment i_RowNumber
//                     End If
// 156                 d_DlyDistance = 0: d_DlyDuration = 0
// 158                 If Not GetDistance(s_Provider, c_Addresses, d_DlyDistance, d_DlyDuration, s_Status, b_AvoidTolls, b_AvoidHighways, b_AvoidFerries, "TIME", s_GoogleCustID, s_GooglePrivateKey) And s_Status <> "" Then trMessage s_Status
// 160                 Set c_Addresses = Nothing: Set c_Addresses = New Collection
// 162                 nodDate.Text = Format$(s_Date, "dd/mm/yyyy") & " : " & Round(d_DlyDistance / 1000, 1) & " KM"
// 164                 s_Date = NullToStr(!Date)
// 166                 d_TtlDistance = d_TtlDistance + d_DlyDistance: d_TtlDuration = d_TtlDuration + d_DlyDuration
// 168                 Set nodDate = tvw.Nodes.Add("nod_" & txxtAccountNo, tvwChild, "nod_" & s_Date, Format$(s_Date, "dd/mm/yyyy"), 2, 2)
// 170                 s_Address = GetMapAddress("~PRIMARY", NullToStr(!UniqueID), True)
// 172                 If b_ToBase Then
// 174                     c_Addresses.Add s_StartAddress
// 176                     Set nodAddress = tvw.Nodes.Add("nod_" & s_Date, tvwChild, NullToStr(i_RowNumber) & "S_" & s_StartAddress, s_StartAddress)
// 178                     Increment i_RowNumber
//                     End If
// 180                 If s_Address <> "NO ADDRESS ON FILE" And s_Address <> s_PreviousAddress Then
//                         c_Addresses.Add s_Address: s_Addresses(i_RowNumber + i_HDRAdj) = s_Address:
// 182                     Set nodAddress = tvw.Nodes.Add("nod_" & s_Date, tvwChild, NullToStr(i_RowNumber) & "_" & s_Address, s_Address)
// 184                     Increment i_RowNumber
//                     End If
//                 Else
// 186                 s_Address = GetMapAddress("~PRIMARY", NullToStr(!UniqueID), True)
// 188                 If s_Address <> "NO ADDRESS ON FILE" And s_Address <> s_PreviousAddress Then
//                         c_Addresses.Add s_Address
//                         s_Addresses(i_RowNumber + i_HDRAdj) = s_Address:
// 190                     Set nodAddress = tvw.Nodes.Add("nod_" & s_Date, tvwChild, NullToStr(i_RowNumber) & "_" & s_Address, s_Address)
// 192                     Increment i_RowNumber
//                     End If
//                 End If
// 194             pb.Value = pb.Value + 1
// 196             s_PreviousAddress = s_Address
// 198             .MoveNext
//             Loop
        
//             If b_ToBase Then
//                 c_Addresses.Add s_StartAddress
//                 Set nodAddress = tvw.Nodes.Add("nod_" & s_Date, tvwChild, NullToStr(i_RowNumber) & "E_" & s_StartAddress, s_StartAddress)
//                 d_DlyDistance = 0: d_DlyDuration = 0
//                 If Not GetDistance(s_Provider, c_Addresses, d_DlyDistance, d_DlyDuration, s_Status, b_AvoidTolls, b_AvoidHighways, b_AvoidFerries, "TIME", s_GoogleCustID, s_GooglePrivateKey) And s_Status <> "" Then trMessage s_Status
//             End If

// 200         If tvw.Nodes.Count > 0 Then
// 202             d_DlyDistance = 0: d_DlyDuration = 0
//                 If Not GetDistance(s_Provider, c_Addresses, d_DlyDistance, d_DlyDuration, s_Status, b_AvoidTolls, b_AvoidHighways, b_AvoidFerries, "TIME", s_GoogleCustID, s_GooglePrivateKey) And s_Status <> "" Then trMessage s_Status
// 204             nodDate.Text = Format$(s_Date, "dd/mm/yyyy") & " : " & Round(d_DlyDistance / 1000, 1) & " KM"
// 206             d_TtlDistance = d_TtlDistance + d_DlyDistance
// 208             d_TtlDuration = d_TtlDuration + d_DlyDuration
//             End If
    
//         End With
// 210     lbxl(8) = Round(d_TtlDistance / 1000, 1)
// 212     lbxl(9) = MinutesToTime(NullToLong(d_TtlDuration / 60), "hh:mm")
// 214     pb.Value = 0
//         Dim expAll As Integer
// 216     For expAll = 1 To tvw.Nodes.Count
// 218         If tvw.Nodes(expAll).Children Then
// 220           tvw.Nodes(expAll).Expanded = True
//             End If
//         Next

//         lbxl(10) = "Travel Summary as calculated by " & s_Provider
// 222     fr(6).Move 25, 25, fr(6).Width, gexTimesheet.Height + 1000
// 224     fr(7).Height = gexTimesheet.Height + 500
// 226     shp(9).Top = gexTimesheet.Height + 1000 - shp(9).Height
// 228     tvw.Height = gexTimesheet.Height + 500
// 230     fr(6).Visible = True
// 232     mbAutoCreate.Enabled = NullToStr(lbxl(8)) <> ""
    
//         'SET MAP DETAILS IF SELECTION IN PLACE
//         If gexTimesheet.SelectedItems.Count > 1 And s_Provider = "BING" Then
//             Dim s_Bing_Key As String
//             s_Bing_Key = DBLookup(TraccsDb, "ProviderKey", "Registration", , 1)
//             Dim wpCtr As Long: wpCtr = 0
//             Dim picURL As String
//             For Each v_Address In c_Addresses
//                 picURL = IIf(picURL <> "", picURL & "wp." & wpCtr & "=" & NullToStr(v_Address), "wp." & wpCtr & "=" & NullToStr(v_Address))
//             Next
//             Dim mapW As Integer: mapW = Me.Width
//             Dim mapH As Integer: mapH = Me.Height

//             If mapW > 900 Then
//                 mapW = 900
//             ElseIf mapW < 80 Then
//                 mapW = 80
//             End If

//             If mapH > 834 Then
//                 mapH = 834
//             ElseIf mapH < 80 Then
//                 mapH = 80
//             End If

//             Dim imgURL_Road As String: imgURL_Road = _
//                                          "http://dev.virtualearth.net/REST/v1/Imagery/Map/Road/Routes?" & _
//                                           picURL & _
//                                           "&ms=" & mapW & "," & mapH & "&key=" & s_Bing_Key

//             Dim imgURL_Aerial As String: imgURL_Aerial = _
//                                           "http://dev.virtualearth.net/REST/v1/Imagery/Map/Aerial/Routes?" & _
//                                           picURL & _
//                                           "&ms=" & mapW & "," & mapH & "&key=" & s_Bing_Key

//             Dim imgURL_AerialWithLabels As String: imgURL_AerialWithLabels = _
//                                           "http://dev.virtualearth.net/REST/v1/Imagery/Map/AerialWithLabels/Routes?" & _
//                                           picURL & _
//                                           "&ms=" & mapW & "," & mapH & "&key=" & s_Bing_Key
//             s_ImageURL = imgURL_Road

        
//         End If
    

     }
     AutoCreateTravelItem(){
       // this.globalS.iToast('info', 'AutoCreateTravelItem Travel Time');
        if (this.Calc_KM<=0) return;

        this.enableAutoCreate=false;

       let DocDetailConstraint:any;             
       let s_Program = ""   ; // 'GetDefaultKMProgram
       let s_ServiceType = ""; //'GetDefaultKMActivity
       let s_PayType = ""  ;   //'GetDefaultKMPayType
       let d_TotalAmount = this.Calc_KM;
       let s_Date = this.payPeriodEndDate;
       let s_Recordno = "";

       DocDetailConstraint=this.selected.data

   let  s_DEFSQL = `SELECT AA.paytype, AA.[SERVICE TYPE] as serviceType, AA.[program] 
                   FROM STAFF S 
                   INNER JOIN AWARDPOS AP ON S.AWARD = AP.CODE 
                   INNER JOIN AWARD_Allowances AA ON AP.RECORDNO = AA.AWARDID 
                   WHERE ACCOUNTNO = '${DocDetailConstraint}' 
                   AND AA.CATEGORY1 = 'N/C TRAVEL BETWEEN'  
                   AND AA.[DEFAULT] = 1`
        
                   this.listS.getlist(s_DEFSQL).subscribe(data => {
                          if(data.length > 0){
                            s_PayType = data[0].paytype;
                            s_Program = data[0].program;
                            s_ServiceType = data[0].serviceType;
                          }
                   });
    

    let inputs = {
        anal:  "",
        billQty: 0,
        billTo: "!INTERNAL",
        billUnit: "HOUR",
        blockNo: 0,
        carerCode: DocDetailConstraint,
        clientCode: "!INTERNAL",
        costQty:  this.Calc_KM,
        costUnit: "SERVICE",
        date: format(s_Date,'yyyy/MM/dd'),
        dayno: parseInt(format(s_Date, 'd')),
        duration: "0",
        groupActivity: false,
        haccType:  "",
        monthNo: parseInt(format(s_Date, 'M')),
        program: s_Program,
        serviceDescription: s_PayType,
        serviceSetting:  "",
        serviceType: s_PayType,      
        staffPosition:  "",
        startTime: "00:00",
        status: "2",
        taxPercent: 0,
        transferred: 0,
        // type: this.activity_value,
        type: 9,
        unitBillRate: 0,
        unitPayRate:  0,
        yearNo: parseInt(format(s_Date, 'yyyy')),
        serviceTypePortal: s_ServiceType,
        recordNo: 0
    };

    // if(!this.timesheetForm.valid){
    //     this.globalS.eToast('Error', 'All fields are required');
    //     return;
    // }

       this.timeS.posttimesheet(inputs).subscribe(data => {          
          
        s_Recordno=data[0];
            this.picked(this.selected);
            this.globalS.sToast('Success', 'Travel Item has been created');
           // AddAudit TBUserName, Now, x_Roster.AuditAction, "ROSTER", s_Recordno
          let sql =`INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) 
                    VALUES ('${this.token.user}', getDate(), 'Create Auto Treavel Item', 'Roster', ${s_Recordno}, '${this.token.user}') `;

                    this.listS.executeSql(sql).subscribe(d=>{
                        //this.globalS.sToast('Success', 'Travel Item has been created');
                    });
        });
     

    //  let sql =`Insert into Roster () values(s_Date, "00:00", "0", "!INTERNAL", CStr(DocDetailConstraint), s_Program, s_ServiceType, _
    //                           "0", "HOUR", "0", s_PayType, 0, "SERVICE", CStr(Round(d_TotalAmount, 2)), "", "9", "2", "", "", "", _
    //                           "", "", "", "", s_Status, s_Recordno``
    
     //If RedisplayForm(s_FormType, txxtAccountNo, s_Status) Then trMessage "Travel Item for " & DocDetailConstraint & " has been created on " & Format$(s_Date, "dd/mm/yyyy") & " for " & d_TotalAmount & "km"



     }

    createAPICall(callback: (response: any) => void) {
        // Make API call here
        // Example:
        // this.http.get('https://api.example.com/data').subscribe(response => {
        //   callback(response);
        // });

        // Replace the above example with your actual API call
    }
    handleOkTop() {
        //this.generatePdf();
        this.tryDoctype = ""
        this.pdfTitle = ""
    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
    }
    generatePdf(){
        
        var Title= "Time Sheet"
        
       
        this.loading = true;
        this.drawerVisible = true;
    //    const temp =  forkJoin([
     //       this.listS.getlist(SQL), 
    //        ]);    
    //        temp.subscribe(x => {                         
    //        this.rptData =  x[0]; 
            //console.log(this.timesheets)      
            
                const data = {
                    "template": { "_id": "45ZyOuXzuDwoCye6" },
                    "options": {
                        "reports": { "save": false },                        
                        "sql": this.timesheets,                        
                        "userid": this.tocken.user,
                        "txtTitle":  Title + ' for '+ this.selected.data,                      
                    }
                }              
                this.loading = true;
               
                        
                this.drawerVisible = true;
                this.printS.printControl(data).subscribe((blob: any) => {
                            this.pdfTitle = Title+".pdf"
                            this.drawerVisible = true;                   
                            let _blob: Blob = blob;
                            let fileURL = URL.createObjectURL(_blob);
                            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                            this.loading = false;
                            this.cd.detectChanges();
                        }, err => {
                            console.log(err);
                            this.loading = false;
                            this.modalService.error({
                                nzTitle: 'TRACCS',
                                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                nzOnOk: () => {
                                    this.drawerVisible = false;
                                },
                            });
                });                            
                return;        
        //    });         
    }
}
