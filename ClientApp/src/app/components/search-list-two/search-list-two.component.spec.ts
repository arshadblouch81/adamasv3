import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchListTwoComponent } from './search-list-two.component';

describe('SearchListTwoComponent', () => {
  let component: SearchListTwoComponent;
  let fixture: ComponentFixture<SearchListTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchListTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchListTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
