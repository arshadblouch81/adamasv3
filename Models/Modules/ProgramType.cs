namespace Adamas.Models.Modules{
    public class ProgramTypes
    {
        public int RecordNumber { get; set; }
        public string Group { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}