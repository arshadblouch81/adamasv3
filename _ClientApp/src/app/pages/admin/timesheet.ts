
import { takeUntil, mergeMap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import  { TimeSheetService, GlobalService, StaffService,ClientService, ListService, ROSTER_TYPE } from '../../services/index';
import { Subject, Subscription, Observable, EMPTY } from 'rxjs';
import { BsModalComponent, BsModalService } from 'ng2-bs3-modal';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'

import * as moment from 'moment';
import {ClrWizard} from "@clr/angular";

declare var $: any;

interface Views {
    default: boolean
    admin_act: boolean,
    charge: { all: boolean, allCha: boolean, allNonCha: boolean, chaItem: boolean },
    leave: boolean,
    unavail: boolean,
    travel: boolean,
    others: boolean,
    item: boolean
}

interface CalculatedPay{
    KMAllowancesQty: number,
    AllowanceQty: number,
    WorkedHours : number,
    PaidAsHours : number,
    PaidAsServices: number,
    WorkedAttributableHours : number,
    PaidQty: number,
    PaidAmount : number,
    ProvidedHours: number,
    BilledAsHours : number,
    BilledAsServices: number,
    BilledQty : number,
    BilledAmount : number,
    HoursAndMinutes: string
}

@Component({
    templateUrl:'./timesheet.html',
    styles:[`
    table thead tr th{
        background: rgb(23, 125, 255);
        color: #fff;
    }
    th{
        font-size:11px;
        font-weight:500;
        padding:5px 3px !important;
    }
    .timesheet-logo{
        background: url(assets/logo/timesheet.png) center/contain no-repeat;
        height: 10rem;
        position: relative;
        width: 7rem;
        margin: 0 auto;
        display: flex;
        align-items: flex-end;
    }
    .search{
        width: 20rem;
        margin: 10px auto;
    }
    td span{
        font-size:10px;
    }
    .option{
        text-align:center;
    }
    i{
        color:#969696;
        cursor:pointer;
    }
    i:hover{
        color:#676363 !important;
    }
    ul{
        margin-top:22px;
    }
    li{
        display: inline-block;
        width: 16.66%;
        text-align: center;
    }
    clr-checkbox-wrapper {
        position: relative;
        float: left;
        margin-left: 1rem;

    }
    .timesheet-list{
        cursor:pointer;
    }
    .timesheet-list:hover:not(.active){
        background:#ebe7e7;
    }
    .selected{
        background:#f2ff87 !important;
    }
    .notice{
        padding: 0.5rem;
        margin-bottom: 1rem;
        text-align: justify;
        font-size: 12px;
        border: 1px solid #d0d0d0;
    }
    .select-notice{
        width: 5rem;
        margin: 0 auto;
    }

    recipient-popup{
        flex: 1;
    }



    
div.table-scroll {
    width: 95%;
    margin: 0 auto;
    margin-top: 1rem;
    max-height: 46vh;
    overflow-y: scroll;
    position: relative;
    border-bottom: 1px solid #c1c1c1;
  }
  
  div.table-scroll table {
    position: relative;
    border-collapse: collapse;
    margin-top:0;
  }
  
  div.table-scroll td, th {
    padding: 0.25em;
  }
  
  div.table-scroll thead th {
    position: -webkit-sticky; 
    position: sticky;
    top: -1px;
    background: #4571ed;
    color: #FFF;
    z-index:1;
  }
  
  div.table-scroll thead th:first-child {
    left: 0;
    z-index: 1;
  }
  
  div.table-scroll tbody th {
    position: -webkit-sticky; 
    position: sticky;
    left: 0;
    background: #FFF;
    border-right: 1px solid #CCC;
  }

  .list-select{
    cursor:pointer;
  }
  .list-select tr:hover:not(.recselected){
      background:#f5f5f5;
  }
  .form-inline label{
      flex:5;
  }
  .total-pay{
    display: flex;
    justify-content: space-around;
    margin-top: 1rem;
    font-size: 11px;
    color: #177dff;
    background: #e4f0ff;
    padding: 1rem 0;
    border: 1px solid #c3c3c3;
  }

  .total-pay label{
    font-size: 15px;
  }
  .total-pay span{
    font-size: 13px;
    color: #716363;
    font-weight: bold;
    text-align:right;
  }

    `]
})

export class TimeSheetAdmin implements OnInit, OnDestroy{

    private subscriptions: Subscription = new Subscription();

    @ViewChild('shared', { static: false }) shared: BsModalComponent;
    @ViewChild('recipientSearch', { static: true }) recipientSearch: BsModalComponent;
    @ViewChild("timesheetModal", { static: true }) timesheetModal: ClrWizard;

    private unsubsribe$ = new Subject()
    loading: boolean = false;
    timesheetOpen: boolean = false;
    recipientModal: boolean = false;
    timesheets: Array<any> = null;

    ROSTER_TYPE = ROSTER_TYPE;

    selectedRow: number;

    view: Dto.ViewTimesheet;

    selected: any = {
        option: '',
        data: ''
    };

    views: Views = {
        default: false,
        admin_act: false,
        unavail: false,
        travel: false,
        others: false,
        leave: false,
        charge: { all: false, allCha: false, allNonCha: false, chaItem: false },
        item: false
    };

    dataDomain: any;
    add_activity_value: number;
    add_recipientLocation: string;

    addForm: FormGroup;
    serviceTypeList: Array<any> = [];
    serviceList: Array<any> = [];
    analysisCodeList: Array<any> = [];
    serviceSettingList: Array<any> = [];
    payTypeList: Array<any> = [];

    unitList: Array<string> = ['HOUR','SERVICE']


    List: Array<string> = []

    overlapInterval: number = 0;
    recipientSearchList: Array<any> = []

    payTotal: CalculatedPay;

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private staffS: StaffService,
        private clientS: ClientService,
        private listS: ListService,
        private formBuilder: FormBuilder,
        private modalService: BsModalService
    ){

        
    }

    ngOnInit(){
        this.resetGroup();
        this.valueChanges();
    }


    clearGroup(isFullReload = false){

        if(isFullReload){
            this.addForm.patchValue({
                serviceType: ''
            })
        }

        this.addForm.patchValue({
            date: '',
            recipientCode: '',
            debtor: '',
            program: '',
            service: '',
            serviceSetting: '',
            payType: '',
            analysisCode: '',
            isMultiple: false,
            isChargeable: false,
            time: {
                startTime: '08:00',
                endTime: '09:00',
            },
            pay: {
                unit: '',
                rate: '',
                quantity: '',
                position: ''
            },
            bill: {
                unit: '',
                rate: '',
                quantity: '',
                tax: ''
            }
        })

        this.timesheetModal.reset();      
    }


    resetGroup(){
        this.addForm = this.formBuilder.group({
            serviceType: '',
            date: '',
            recipientCode: '',
            debtor: '',
            program: '',
            service: '',
            serviceSetting: '',
            payType: '',
            analysisCode: '',
            isMultiple: false,
            isChargeable: false,
            time: new FormGroup({
                startTime: new FormControl('08:00'),
                endTime: new FormControl('09:00'),
            }),
            pay: new FormGroup({
                unit: new FormControl(''),
                rate: new FormControl(''),
                quantity: new FormControl(''),
                position: new FormControl('')
            }),
            bill: new FormGroup({
                unit: new FormControl(''),
                rate: new FormControl(''),
                quantity: new FormControl(''),
                tax: new FormControl('')
            })
        });

    }

    valueChanges(){

        this.subscriptions.add(
            this.addForm.get('recipientCode').valueChanges.pipe(
                mergeMap(x => {
                    if(!x) return EMPTY;

                    this.addForm.patchValue({
                        debtor: x
                    });
                    return this.getprograms(x);
                })
            ).subscribe(data => {               
                this.programsList = data.map(x => {
                    return x.ProgName
                });           
            })
        );
       
        this.subscriptions.add(
            this.addForm.get('program').valueChanges.pipe(
                mergeMap(x => {
                    if(!x) return EMPTY;

                    const { serviceType, recipientCode } = this.addForm.value;    
                    this.addForm.get('service').reset();
    
                    return this.getServiceActivityItem(serviceType, recipientCode, x);
                })
            ).subscribe(data => {    
                this.serviceList = data.map(x => {  return x.Activity   });            
            })    
        )

        this.subscriptions.add(
            this.addForm.get('serviceType').valueChanges.subscribe(type => {
                if(!type) return EMPTY;
                
                this.resetView();
                this.clearGroup();
    
                switch (type) {
                    case "ONEONONE":
                        this.add_activity_value = 2;
                        this.dataDomain = "ONEONONE";
                        this.views.others = true;
                        break;
                    case "BROKERED SERVICE":
                        this.add_activity_value = 3;
                        this.dataDomain = "BROKERED SERVICE";
                        this.views.default = true;
                        break;
                    case "LEAVE ABSENCE":
                        this.add_activity_value = 4;
                        this.dataDomain = "LEAVE ABSENCE";
                        this.views.leave = true;
                        break;
                    case "TRAVEL TIME":
                        this.add_activity_value = 5;
                        this.dataDomain = "TRAVEL TIME";
                        this.views.others = true;
                        break;
                    case "ADMIN ACTIVITY":
                        this.add_activity_value = 6;
                        this.dataDomain = "ADMIN ACTIVITY";
                        this.add_recipientLocation = "!INTERNAL";
                        this.views.admin_act = true;
                        break;
                    case "ADMISSION SERVICE":
                        this.add_activity_value = 7;
                        this.dataDomain = "ADMISSION SERVICE";
                        this.views.others = true;
                        break;
                    case "SLEEPOVER":
                        this.add_activity_value = 8;
                        this.dataDomain = "SLEEPOVER";
                        this.views.others = true;
                        break;
                    case "ALLOWANCE":
                        this.add_activity_value = 9;
                        this.dataDomain = "ALLOWANCES";
                        this.views.charge.all = true;
                        break;
                    case "TRANSPORT":
                        this.add_activity_value = 10;
                        this.dataDomain = "VEHICLES"
                        this.views.default = true;
                        break;
                    case "CENTRE BASED ACTIVITY":
                        this.add_activity_value = 11;
                        this.dataDomain = "CENTRE BASED ACTIVITY";
                        this.views.default = true;
                        break;
                    case "GROUP ACTIVITY":
                        this.add_activity_value = 12;
                        this.dataDomain = "ACTIVITY GROUPS";
                        this.views.default = true;
                        break;
                    case "UNAVAILABILITY":
                        this.add_activity_value = 13;
                        this.dataDomain = "UNAVAILABILITY";
                        this.views.unavail = true;
                        break;
                    case "ITEM":
                        this.add_activity_value = 14;
                        this.dataDomain = "ITEM";
                        this.views.item = true;
                        break;
                    case "ALLOWANCE NON-CHARGEABLE":
                        this.add_activity_value = 9;
                        this.add_recipientLocation = "!INTERNAL";
                        this.dataDomain = "ALLOWANCES";
                        this.views.charge.allNonCha = true;
                        break;
                    case "ALLOWANCE CHARGEABLE":
                        this.add_activity_value = 9;
                        this.dataDomain = "ALLOWANCES";
                        this.views.charge.allCha = true;
                        break;
                    case "CHARGEABLE ITEM":
                        this.add_activity_value = 9;
                        this.dataDomain = "CHARGEABLE ITEM";
                        this.views.charge.chaItem = true;
                    default:
                        this.dataDomain = event;
                        this.add_activity_value = 0;
                        this.views.others = true;
                }
    
                this.getpaytype(type);
                this.getanaylsiscode();
    
                this.programsList = []
    
                // Populates Program Dropdown
                if(type === "ADMIN ACTIVITY" || type === "ALLOWANCE NON-CHARGEABLE" || type === "CENTRE BASED ACTIVITY" || type === "GROUP ACTIVITY" || type === "ITEM" || type === "TRANSPORT" ){
                    this.listS.getlist(`SELECT DISTINCT UPPER([Name]) AS Program, type FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= GETDATE())`)
                        .subscribe(data => {
                            this.programsList = data.map(x => {
                                return x.Program
                            });
                        });
                }
    
                
            })
        )
        
    }

    computeDuration(start: any, end: any){
        console.log(this.globalS.computeTime(start,end))
    }

    resetView(){
        this.views = {
            default: false,
            admin_act: false,
            unavail: false,
            travel: false,
            others: false,
            leave: false,
            charge: { all: false, allCha: false, allNonCha: false, chaItem: false },
            item: false
        };
    }
    

    picked(data: any){
        
        this.selected = data;

        if(data.data == ''){
            return;
        }

        let whatType = this.whatType(this.selected.option);

        this.loading = true;

        this.timeS.gettimesheets({
            AccountNo: data.data,
            personType: whatType
        }).pipe(takeUntil(this.unsubsribe$))
            .subscribe(data => {
            this.loading = false;
            this.timesheets = data.map(x =>{
                return {
                    shiftbookNo: x.shiftbookNo,
                    date: x.activityDate,
                    startTime: x.activity_Time.start_time,
                    endTime: x.activity_Time.end_Time,
                    duration: x.activity_Time.calculated_Duration,
                    recipient: x.recipientLocation,
                    program: x.program.title,
                    activity: x.activity.name,
                    paytype: x.payType.paytype,
                    payquant: x.pay.quantity,
                    payrate: x.pay.pay_Rate,
                    billquant: x.bill.quantity,
                    billrate: x.bill.bill_Rate,
                    approved: x.approved,
                    billto: x.billedTo.accountNo,
                    notes: x.note,
                    selected: false,

                    serviceType: x.roster_Type,
                    recipientCode: x.recipient_staff.accountNo,
                    debtor: x.billedTo.accountNo,
                    serviceActivity: x.activity.name,
                    serviceSetting: x.recipientLocation,
                    payType: x.payType.paytype,
                    analysisCode: x.anal

                }
            });
        });
        
        this.timeS.getcomputetimesheet({
            AccountName: data.data,
            IsCarerCode: whatType == 'Staff' ? true : false
        }).subscribe(compute => {
                var hourMinStr;

                if(compute.workedHours && compute.workedHours > 0){
                    const hours = Math.floor(compute.workedHours*60/60);
                    const minutes = ('0'+compute.workedHours * 60 % 60).slice(-2);

                    hourMinStr = `${hours}:${minutes}`
                }

                var _temp = {
                        KMAllowancesQty: compute.kmAllowancesQty || 0,
                        AllowanceQty: compute.allowanceQty || 0,
                        WorkedHours : compute.workedHours || 0,
                        PaidAsHours : compute.paidAsHours || 0,
                        PaidAsServices: compute.paidAsServices || 0,
                        WorkedAttributableHours : compute.workedAttributableHours || 0,
                        PaidQty: compute.paidQty || 0,
                        PaidAmount : compute.paidAmount || 0,
                        ProvidedHours: compute.providedHours || 0,
                        BilledAsHours : compute.billedAsHours || 0,
                        BilledAsServices: compute.billedAsServices || 0,
                        BilledQty : compute.billedQty || 0,
                        BilledAmount : compute.billedAmount || 0,
                        HoursAndMinutes: hourMinStr
                };
              
                this.payTotal = _temp; 
            })
    }

    getservicetype(){
        const type = this.whatType(this.selected.option)
        this.timeS.getservicetype(type)
            .subscribe(data => this.serviceTypeList = data)
    }

    getprograms(rcode: string): Observable<any>{
        if(this.globalS.isEmpty(rcode)) return;

        return this.listS.getlist(`SELECT Distinct [Program] AS ProgName FROM RecipientPrograms INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID
            WHERE Recipients.AccountNo = '${ rcode }' AND RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') 
            ORDER BY [ProgName]`);
    }

    getanaylsiscode(){
        this.listS.getserviceregion()
            .subscribe(analysis => this.analysisCodeList = analysis);
    }

    // Populates Pay Type Dropdown
    getpaytype(type: string){

        var sqlStmt = "";
        var _date = moment().format('MM-DD-YYYY');
        
        if(this.add_activity_value == 9){
            sqlStmt = `SELECT Title AS Activity FROM ItemTypes WHERE RosterGroup = 'ALLOWANCE ' AND Status = 'NONATTRIBUTABLE' AND ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= '${_date}')  AND AUTOAPPROVE = 1 ORDER BY TITLE`
        } else {
            sqlStmt = `SELECT Title AS Activity FROM ItemTypes WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' AND ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= '${_date}')  AND AUTOAPPROVE = 1 ORDER BY TITLE`
        }
        console.log(sqlStmt);

        if(!sqlStmt) return;

        this.listS.getlist(sqlStmt).subscribe(paytypes => {
            this.payTypeList = paytypes.map(x => {
                return x.Activity
            });
        })
    }

    whatType(data: number): string {
        return data > 0 ? 'Staff' : 'Recipient';
    }

    ngOnDestroy(): void{
        this.unsubsribe$.next();
        this.unsubsribe$.complete();
    }


    selectedTimesheet(index){
        index.selected = !(index.selected);
    }

    delete(){
        let selectedlist = [this.selectedTimesheet]
        selectedlist.filter(x => !x == true);
    }


    whatView(viewNo: number){

        if(viewNo == 6){
            this.clearGroup(true);
            this.getservicetype();

            this.view = {
                view: viewNo,
                header: '',
                body: ''
            }
            this.timesheetOpen = true;
            return;
        }

        if(!(this.selected.data)){
            this.view = {
                view: -1,
                header: 'Error',
                body: 'NO USER SELECTED'
            }
            this.shared.open();
            return;
        }


        // Delete Unapproved
        if(viewNo == 1){
            this.view = {
                view: viewNo,
                header: 'Delete Unapproved',
                body: 'Are you sure to delete all unapproved items?'
            }
        }

        // Approve all
        if(viewNo == 2){
            this.view = {
                view: viewNo,
                header: 'Approve All',
                body: 'Are you sure to approve all items?'
            }
        }

        // Unapprove All
        if(viewNo == 3){
            this.view = {
                view: viewNo,
                header: 'Unapprove All',
                body: 'Are you sure to unapprove all items?'
            }
        }

        // Delete Highlighted Shifts
        if(viewNo == 4){
            this.view = {
                view: viewNo,
                header: 'Delete Highlighted Shifts',
                body: 'Are you sure to delete highlighted shifts?'
            }
        }

        // Remove Shift Overlap
        if(viewNo == 5){
            this.view = {
                view: viewNo,
                header: 'Automatic Overlap Removal',
                body: ''
            }
        }

        this.shared.open();
    }

    sharedProcess(){
        const { view } = this.view;

        if(view == 1){

        }

        if(view == 2){
            this.timeS.approveAll({
                accountNo: this.selected.data
            }).subscribe(data => {
                if(data){
                    this.globalS.sToast('Success','All items are approved');
                    this.picked(this.selected)
                }
            })
        }

        if(view == 3){
            this.timeS.unapproveAll({
                accountNo: this.selected.data
            }).subscribe(data => {
                if(data){
                    this.globalS.sToast('Success','All items are unapproved');
                    this.picked(this.selected)
                }
            })
        }

        if(view == 4){
            const shiftArray = this.timesheets.filter(x => x.selected).map(x => x.shiftbookNo)

            if(shiftArray.length == 0){
                this.globalS.wToast('No Highlighted Item','Warning');
                return;
            }

            this.timeS.deleteshift(shiftArray)
                .subscribe(data => {
                    if(data){
                        this.globalS.sToast('Success','Selected items are deleted');
                        this.picked(this.selected)
                    }
                })
        }

        if(view == 5){
            this.timeS.removeShiftOverlap({
                AccountNo: this.selected.data,
                personType: this.whatType(this.selected.option)
            }, 5).subscribe(data => console.log(data))
        }


    }


    checkBoxChange(event: boolean, timesheet: any){

        this.timeS.selectedApprove({
            AccountNo: timesheet.shiftbookNo,
            Status: event
        }).subscribe(data => {
            if(data){
                timesheet.approved = event;
                this.globalS.sToast('Success','Selected item');
            }
        })
    }

    populateRecipientSearch(){
        this.recipientSearchList = []
        this.listS.getrecipientsearch({
            AccountNo: '',
            RowNo: this.counterRecipient
        }).subscribe(data => this.recipientSearchList = data)
    }



    recDismiss(){
        // this.recipientSearch.close();
        setTimeout(() => {
            this.timesheetOpen = true;
        }, 100);
    }

    recClose(){
        setTimeout(() => {
            this.timesheetOpen = true;
        }, 100);
    }

    counterRecipient: number = 0;
    disableSearchOnScroll: boolean = false;
    selectedRecipient: any;
    programsList: Array<any> = []

    onScroll(){
        if (this.disableSearchOnScroll) return;

        this.listS.getrecipientsearch({
            AccountNo: '',
            RowNo: this.counterRecipient+=200
        }).subscribe(data => {
            data.forEach(e => {
                this.recipientSearchList.push(e);
            })
        })
    }

    openRecipientModal(){
        this.recipientModal = true;
        this.populateRecipientSearch();
    }

    searchRecipient(data: any){
        this.recipientSearchList = []

        if(this.globalS.isEmpty(data))  {
            this.disableSearchOnScroll = false;
        } else{
            this.disableSearchOnScroll = true;
        }              

        this.listS.getrecipientsearch({
            AccountNo: data,
            RowNo: 0
        }).subscribe(data => this.recipientSearchList = data)
    }

    saveRecipient(){
        this.addForm.patchValue({
            recipientCode: this.selectedRecipient.accountNo,
            debtor: this.selectedRecipient.accountNo
        });


        this.listS.getlist(`
        SELECT Distinct [Program] AS ProgName FROM RecipientPrograms INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID
        WHERE Recipients.AccountNo = '${ this.addForm.get('recipientCode').value }' AND RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') 
        ORDER BY [ProgName]`)
            .subscribe(data =>  {
                this.programsList = data.map(x => {
                    return x.ProgName
                });
            })
    }


    getServiceActivityItem(serviceType: string, recipientCode: string, program: string): Observable<any>{
     
        var sqlStatement = "";
        var _date = moment().format('MM-DD-YYYY');

        if(serviceType === 'ADMIN ACTIVITY'){

            if (program != '!INTERNAL' && program != '!TEST' && program != '!TEST_CONTINGENCY') {                
                sqlStatement = `SELECT DISTINCT [service type] AS Activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid 
                WHERE SO.serviceprogram = '${program}' AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] = 'ADMINISTRATION' AND processclassification = 'OUTPUT' AND ( ITM.enddate IS NULL OR ITM.enddate >= '${_date}' )) ORDER BY [service type]`          
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMINISTRATION' AND ProcessClassification = 'OUTPUT' 
                AND (ITM.EndDate Is Null OR ITM.EndDate >= '${_date}')) ORDER BY [Service Type]`   
            }     
        }

        if(serviceType === 'ADMISSION SERVICE'){

            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE SO.ServiceProgram = '${ program }' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMISSION' AND
                 ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${_date}')) ORDER BY [Service Type]`;
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMISSION' AND ProcessClassification = 'OUTPUT' AND 
                (ITM.EndDate Is Null OR ITM.EndDate >= '${_date}')) ORDER BY [Service Type]`
            }                  
        }


        if(serviceType === 'ALLOWANCE CHARGEABLE'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN Recipients ON SO.PersonID = Recipients.UniqueID WHERE  [Recipients].[AccountNo] = '${recipientCode}' 
                AND [SO].[ServiceProgram] = '${ program }' AND [SO].[ServiceStatus] = 'ACTIVE' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] 
                    AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019') AND ((ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) OR 
                    (ISNULL(ITM.MinDurtn, 0) <= 0 AND IsNull(ITM.MaxDurtn, 0) >= 0))) ORDER BY [Service Type]`
            } else {
                sqlStatement =  `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN Recipients ON SO.PersonID = Recipients.UniqueID WHERE  
                [Recipients].[AccountNo] = '${recipientCode}' AND [SO].[ServiceStatus] = 'ACTIVE' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] 
                    AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019') AND ((ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) 
                    OR (ISNULL(ITM.MinDurtn, 0) <= 0 AND IsNull(ITM.MaxDurtn, 0) >= 0))) ORDER BY [Service Type]`
            }
        }


        if(serviceType === 'ALLOWANCE NON-CHARGEABLE'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE SO.ServiceProgram = '${program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ProcessClassification = 'OUTPUT' AND 
                (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            } else {
                sqlStatement =  `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) 
                ORDER BY [Service Type]`
            }
        }

        if(serviceType === 'CENTRE BASED ACTIVITY'){
            if (program != '!INTERNAL' && program != '!TEST' && program != '!TEST_CONTINGENCY'){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE SO.ServiceProgram = '${program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'CENTREBASED' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'CENTREBASED' AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            }
        }

        if(serviceType === 'GROUP ACTIVITY'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE
                 SO.ServiceProgram = '${program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'GROUPACTIVITY' AND 
                 ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'GROUPACTIVITY' AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            }
        }

        if(serviceType === 'ITEM'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE SO.ServiceProgram = '${program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ITEM' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ITEM' AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            }
        }

        if(serviceType === 'ONEONONE'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] FROM ServiceOverview SO INNER JOIN Recipients ON SO.PersonID = Recipients.UniqueID WHERE  [Recipients].[AccountNo] = '${recipientCode}' 
                AND [SO].[ServiceStatus] = 'ACTIVE' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ONEONONE' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019') AND ((ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) 
                OR (ISNULL(ITM.MinDurtn, 0) <= 0 AND IsNull(ITM.MaxDurtn, 0) >= 0))) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] FROM ServiceOverview SO INNER JOIN Recipients ON SO.PersonID = Recipients.UniqueID WHERE  [Recipients].[AccountNo] = '${recipientCode}' 
                AND [SO].[ServiceStatus] = 'ACTIVE' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ONEONONE' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019') AND ((ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) 
                OR (ISNULL(ITM.MinDurtn, 0) <= 0 AND IsNull(ITM.MaxDurtn, 0) >= 0))) ORDER BY [Service Type]`
            }
        }

        if(serviceType === 'SLEEPOVER'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] FROM ServiceOverview SO INNER JOIN Recipients ON SO.PersonID = Recipients.UniqueID WHERE  [Recipients].[AccountNo] = '${recipientCode}' 
                AND [SO].[ServiceProgram] = '${program}' AND [SO].[ServiceStatus] = 'ACTIVE' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] 
                    AND ITM.[RosterGroup] = 'SLEEPOVER' AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019') AND ((ISNULL(ITM.MinDurtn, 0) = 0 
                    AND ISNULL(ITM.MAXDurtn, 0) = 0) OR (ISNULL(ITM.MinDurtn, 0) <= 0 AND IsNull(ITM.MaxDurtn, 0) >= 0))) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] FROM ServiceOverview SO INNER JOIN Recipients ON SO.PersonID = Recipients.UniqueID WHERE  [Recipients].[AccountNo] = '${recipientCode}' 
                AND [SO].[ServiceStatus] = 'ACTIVE' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'SLEEPOVER' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019') AND ((ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) 
                OR (ISNULL(ITM.MinDurtn, 0) <= 0 AND IsNull(ITM.MaxDurtn, 0) >= 0))) ORDER BY [Service Type]`
            }
        }


        if(serviceType === 'TRANSPORT'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE SO.ServiceProgram = '${program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'TRANSPORT' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'TRANSPORT' AND ProcessClassification = 'OUTPUT' 
                AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            }
        }

        if(serviceType === 'TRAVEL TIME'){
            if(program){
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE SO.ServiceProgram = '${program}' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'TRAVELTIME' 
                AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            } else {
                sqlStatement = `SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID 
                WHERE EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'TRAVELTIME' AND ProcessClassification = 'OUTPUT' 
                AND (ITM.EndDate Is Null OR ITM.EndDate >= '09-24-2019')) ORDER BY [Service Type]`
            }
        }

        if(!sqlStatement) return;

        return this.listS.getlist(sqlStatement);
    }

    addTimesheet(){

        const {
            analysisCode,
            payType,
            program,
            service,
            time,
            date,
            pay,
            bill,
            debtor
        } = this.addForm.value;

        if(!date){
            this.globalS.eToast('Error', 'Date is required')
            return;
        }

        
        var durationObject = this.globalS.computeTime(time.startTime, time.endTime);

        let inputs = {
            anal: analysisCode,
            billQty: bill.quantity,
            billTo: debtor,
            billUnit: bill.unit,
            blockNo: durationObject.blockNo,
            carerCode: this.selected.data,
            clientCode: "12321",
            costQty: pay.quantity,
            costUnit: pay.unit,
            date: moment(date).format('YYYY/MM/DD'),
            dateEntered: null,
            dateLastMod: null,
            dayno: moment(date).format('DD'),
            duration: durationObject.duration,
            groupActivity: false,
            haccType: null,
            monthNo: moment(date).format('MM'),
            program: program,
            serviceDescription: payType,
            serviceSetting: null,
            serviceType: service,
            staffPosition: null,
            startTime: time.startTime,
            status: "1",
            taxPercent: bill.tax,
            transferred: 0,
            type: this.add_activity_value,
            uniqueID: null,
            unitBillRate: bill.rate,
            unitPayRate: pay.rate,
            yearNo: moment(date).format('YYYY')
        };

        console.log(inputs);

        this.timeS.posttimesheet(inputs).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Timesheet has been added');
                this.picked(this.selected)
                this.clearGroup(true);
            }
        });

    }

    details(index: number){
        //this.subscriptions.unsubscribe();
        this.whatView(6);

        const {
            serviceType,
            date,
            program,
            serviceActivity,
            paytype,
            analysisCode
        } = this.timesheets[index];
  

        console.log(this.timesheets[index])

        this.addForm.get('service').reset();
        this.getServiceActivityItem(serviceType, this.selected.data , program);

     

            
        setTimeout(() => {
            this.addForm.patchValue({
                serviceType: this.ROSTER_TYPE[parseInt(serviceType)],
                date: moment(date),
                program: program,
                service: serviceActivity,
                payType: paytype,
                analysisCode: analysisCode
            })
            console.log(this.addForm.value)
            

            this.timesheetOpen = true;
        }, (500));
            

        // console.log(this.timesheets[index])
    }

    

}