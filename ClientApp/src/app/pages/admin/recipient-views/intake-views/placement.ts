import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { RECIPIENT_OPTION, ModalVariables, ProcedureRoster, UserToken, CallAssessmentProcedure, Consents } from '@modules/modules';

import format from 'date-fns/format';

@Component({
    selector: '',
    templateUrl: './placement.html',
    styles:[`
    h4{
        margin-top:10px;
    }
    
    .selected{
        background-color: #85B9D5;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }  
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakePlacement implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;

    placementOpen: boolean = false;
    placementGroup: FormGroup;
    placements: Array<any> = ['PERMANENT', 'PART TIME', 'PERMANENT RESIDENT'];
    selectedplacements: Array<any> = [];
    placementData: Array<any> = [];

    addOREdit: number;

    lists: Array<any>;

    dateFormat: string = dateFormat;
    selectedplacement: any;
    activeRowData:any;
    selectedRowIndex:number;
    ViewAllocateResourceModal:boolean=false;
    serviceActivityList:Array<any>=[];
    originalList:Array<any>=[];
    txtSearch:any;
    HighlightRow2:number;
    
  
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/intake/branches'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'placements')) {
                console.log('sasd')
                this.user = data;
                this.search(data);
            }
        });

       
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();        
        this.buildForm();
        this.search(this.user);
     
      
    }

    buildForm(){
        let today = new Date();
        this.placementGroup = this.formBuilder.group({
            recordNumber: 0,
            
            personID: [''],               
            creator : null,     
            type : null,               
            name : null,
            startDate : today ,
            endDate : today ,                        
            referral : [false],
            atc : [false],
            notes : null,
            
         })

      
    }

    trackByFn(index, item) {
        return item.id;
    }

    resetAll(){
        this.search();
        this.selectedplacement = {};
    }

    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(data: any): void {
        let records = this.selectedplacements.map(x=>x.recordNumber).join(',').toString();
          this.deleteplacement(records);
        
  
    }
    

    showAddModal() {
      
        this.addOREdit = 0;
        this.placementOpen=true;
      
    
    }

    
    showEditModal(data: any){

        this.placementOpen = true;
        this.addOREdit = 1;
        this.activeRowData = data;
        let placement = data;

        this.placementGroup.patchValue({
            
         
                personID : this.user.id,      
                creator : this.tocken.user,     
                type : placement.type,               
                name : placement.carer,
                startDate :  placement.startDate ? (new Date(placement.startDate)) : null, 
                endDate : placement.endDate ? (new Date(placement.endDate)) : null ,                        
                referral : placement.referral,
                atc : placement.atc,
                notes : placement.notes,
                recordNumber : placement.recordNumber,
                
            
        });
    
        // this.placementGroup.controls['placement'].disable();

    }

    deleteplacement(data: any){
        this.timeS.deleteplacements(data)
                    .subscribe(data => {
                        if(data){
                            this.resetAll();
                            this.globalS.sToast('Success','placement Deleted')
                        }
                    })
    }

    search(user: any = this.user){
        this.cd.reattach();
        this.loading = true;

       
        this.timeS.getplacements(user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        
            this.loading = false;
            this.placementData = data;
           
            this.cd.markForCheck();
        })        
    }
    save() {

        let placement = this.placementGroup.value;
     
      
            let inputs={
                personID : this.user.id,      
                creator : this.tocken.user,     
                type : placement.type,               
                name : placement.name,
                date1 :   format(new Date(placement.startDate),'yyyy-MM-dd') , 
                date2 :  format(new Date(placement.endDate),'yyyy-MM-dd')  ,                       
                referral : placement.referral,
                atc : placement.atc,
                notes : placement.notes,
                recordNumber : placement.recordNumber,
                
            };
    
          if (  this.addOREdit == 0) {
            this.timeS.postplacements(inputs).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    
                    this.success();
                    this.globalS.sToast('Success', 'Data Added');

                    
                }
            });}
            else{
                this.timeS.updateplacements(inputs).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        
                        this.success();
                        this.globalS.sToast('Success', 'Record Updated');
    
                        
                    }
                });
            }
    }
    success() {
        this.search(this.sharedS.getPicked());
        this.placementGroup.reset();
        this.placementOpen = false;
        this.loading = false;
      }
    ngOnDestroy(): void {

    }

    handleCancel(){
        this.placementOpen = false;
    }

    handleOk(){

    }
    RecordClicked(event: any, value: any, i: number, main:boolean) {

        this.activeRowData=value;  
        if (main)
            this.selectedRowIndex =i;
        else
        this.selectedRowIndex =-1;
       
       //console.log(this.selectedplacements.includes(value))
        //let tindex = this.selected.indexOf(value.recordNo);
    
    
        if (event.ctrlKey || event.shiftKey) {
         
            if (!this.selectedplacements.includes(value))
                this.selectedplacements.push(value);
            //this.selected.push(value.recordNo)
          
    
        } else {
          
          this.selectedplacements = [];
         // this.selected = [];
    
          if (this.selectedplacements.length <= 0) {
            this.selectedplacements.push(value)
            
          }
        }
      }
   
     onItemDbClick(sel:any , i:number) : void {
     
         this.HighlightRow2=i;
         this.selectedplacement=sel;
         if (!this.selectedplacements.includes(sel)) { 
            this.selectedplacements.push(sel);
        }

         this.save();
     
     }
    onTextChangeEvent(event:any){
        // console.log(this.txtSearch);
         let value = this.txtSearch.toUpperCase();
         //console.log(this.serviceActivityList[0].description.includes(value));
         this.serviceActivityList=this.originalList.filter(element=>element.description.includes(value));
     }
    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
        this.selectedplacement=data.placement ;
      }
    print(){
        
    }
    generatePdf(){
      
        
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.placementData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Placements",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Placements "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}

 