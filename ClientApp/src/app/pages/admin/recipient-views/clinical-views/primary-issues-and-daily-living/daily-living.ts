
import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, othersType, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { functionalStatus} from '@services/global.service'

@Component({
    styles: [`
        nz-select {
            width: 100%;
            background-color: #cbe8f7 !important;
        }

     input{
        background-color: #cbe8f7;
    }

        input:focus {
            background-color: #85B9D5;
        }   

        nz-select:focus {
            background-color: #85B9D5;
        }  

        .row{
            margin:5px;
        }

        .input{
            background-color: #cbe8f7;
            border: 1px solid #cbe8f7;
        }
        .selected{
            background-color: #85B9D5;
            border: 1px solid #85B9D5;
        }
    

        ul{
            list-style:none;
        }

        div.divider-subs div{
            margin-top:2rem;
        }

        .layer2 > span{
            width:13rem;
            text-align:right;
        }
        .layer2 > input{
            width:4rem;
        }
        .layer2 {
            margin-bottom:5px;
        }
        .layer2 > *{
            display:inline-block;
            margin-right:5px;
            font-size:11px;
        }
        nz-select{
            width:12rem;
        }

         /* Set background color for the dropdown options */
         :host ::ng-deep .nz-option {
        background-color: #cbe8f7 !important; /* Change to your desired background color */
        }

        :host ::ng-deep  .ant-select-selection   {
            background-color: #cbe8f7 !important;
            }

            .heading-div{
                margin-left:5px;
                  margin-top :20px; margin-bottom :20px; font-size:15px; font-weight:bold; font-family:'Segoe UI';
            }
        
    `],
    templateUrl:'./daily-living.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ClinicalDailyLiving implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

  
    housework : Array<string> = functionalStatus.housework      
    transport: Array<string> =  functionalStatus.transport;
    shopping:Array<any> = functionalStatus.shopping
    medicines:Array<any> = functionalStatus.medication
    money :Array<any> = functionalStatus.money
    walk:Array<any> = functionalStatus.walk
    bathShower:Array<any> = functionalStatus.bathShower
    confusion:Array<any> = functionalStatus.confusion
    behaviouralProblems:Array<any> = functionalStatus.behaviouralProblems
    communication:Array<any> = functionalStatus.communication
    dressing:Array<any> = functionalStatus.dressing
    eating:Array<any> = functionalStatus.eating
    toileting:Array<any> = functionalStatus.toileting
    gettingUp:  Array<any> = functionalStatus.gettingUp

    dailyLivingForm: FormGroup;
    staffs: Array<string> = []

    dateFormat: string = dateFormat;

    occupationDefault: string;
    financialClassDefault: string;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'daily-living')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        this.user = this.sharedS.getPicked();
        this.buildForm();        
        this.populate();
        this.search(this.user);

       // let sql=`SELECT Description FROM DataDomains WHERE DOMAIN = 'FINANCIALCLASS'`;

        // this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //    this.lstFinancialClass = data.map(x => x.description);
        // });

        this.sharedS.emitSaveAll$.subscribe(data => {
            if (data.type=='Recipient' && data.tab =='Clinical Detail' && this.globalS.isCurrentRoute(this.router, 'daily-living')){
            
              this.save();
            }
          })

        
    
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    search(data: any){
        this.timeS.getonidailyliving(data.id)
            .subscribe(data => {
                this.dailyLivingForm.patchValue(data)               
                this.detectChanges();
            });
    }
    //FP2_WalkingDistance, FP3_Shopping, FP4_Medicine,FP5_Money , FP6_Walking, FP7_Bathing, FP8_Memory, FP9_Behaviour, FPA_Communication, FPA_Dressing, FPA_Eating, FPA_Toileting, FPA_GetUp


    buildForm(){
        this.dailyLivingForm = this.formBuilder.group({
          fP1_Housework:null,
          fP2_WalkingDistance: null,
          fP3_Shopping: null,
          fP4_Medicine: null,
          fP5_Money: null,
          fP6_Walking: null,
          fP7_Bathing: null,
          fP8_Memory: null,
          fP9_Behaviour: null,
          fpA_Communication: null,
          fpA_Dressing: null,
          fpA_Eating: null,
          fpA_Toileting: null,
          fpA_GetUp: null     
            

        });
    }

    canDeactivate() {
        if (this.dailyLivingForm && this.dailyLivingForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Changes have been detected. Save Changes?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                    
                }
            });
        }
        return true;
    }

  

    populate(){
        // forkJoin([
        //     this.listS.getlistbranches(),
        //     this.listS.getlistcasemanagers(),
        //    // this.listS.getfinancialclass()
        // ]).subscribe(data => {
        //     this.branches = data[0];
        //     this.casemanagers = data[1];
        //  //   this.lstFinancialClass = data[2];
        //     this.detectChanges();
        // })

        // this.timeS.getstaff({
        //     User: this.globalS.decode().nameid,
        //     SearchString: '',
        //     IncludeInactive: false,
        //   }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     this.staffs = data.map(x => x.accountNo);
        //     this.detectChanges();
        //   });
    }

    save(){
        this.timeS.updatedailyliving(this.dailyLivingForm.value, this.user.id)
            .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
    }

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }


}