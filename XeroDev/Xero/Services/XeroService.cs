﻿using Microsoft.Extensions.Caching.Memory;
using Xero.Interfaces;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System;
using Xero.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xero.NetStandard.OAuth2.Api;
using Xero.NetStandard.OAuth2.Client;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Model.PayrollAu;

namespace Xero.Services
{
    public class XeroService : IXeroService 
    {
        private static string CLIENT_ID = "66AFB5EBEE304BE985F69083FAEEBD08";
        private static string SCOPES = "offline_access openid profile email accounting.transactions accounting.settings accounting.contacts payroll.timesheets payroll.timesheets.read";
        private static string CODE_VERIFIER = "3kP9mLPhY-zJ1EhvKb-0Y81rGQQu9cpyHh__Fgei8h0";


        public const string ConnectionsUrl = "https://api.xero.com/connections";

        public const string BASE_V1 = "https://api.xero.com/api.xro/1.0";
        public const string BASE_V2 = "https://api.xero.com/api.xro/2.0";

        public const string REDIRECT_URI = "https://localhost:44393/api/xero/callback";

        private static string STATE = "123456789";

        public const string AuthorisationUrl = "https://login.xero.com/identity/connect/authorize";

        public static DateTime CurrentDateTime = DateTime.Now;

        private string id_token, access_token, refresh_token;

        private string xeroTenantId = "6889e4b6-7313-446b-8cc8-8c2056d83568";

        private IMemoryCache _cache;
        public XeroService(
            IMemoryCache cache)
        {
            _cache = cache;

            _cache.TryGetValue("id_token", out id_token);
            _cache.TryGetValue("access_token", out access_token);
            _cache.TryGetValue("refresh_token", out refresh_token);

        }

        public async Task<List<Tenant>> GetTenants()
        {
            string id_token, access_token, refresh_token;

            _cache.TryGetValue("id_token", out id_token);
            _cache.TryGetValue("access_token", out access_token);
            _cache.TryGetValue("refresh_token", out refresh_token);

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);

            var response = await client.GetAsync(ConnectionsUrl);
            var content = await response.Content.ReadAsStringAsync();

            //fill the dropdown box based on the results 
            var tenants = JsonConvert.DeserializeObject<List<Tenant>>(content);

            return tenants;
        }

        public async Task<PayItems> GetPayItems()
        {
            var where = "Status=='ACTIVE'";
            var order = "EmailAddress%20DESC";
            var page = 56;

            var apiInstance = new PayrollAuApi();

            var result = await apiInstance.GetPayItemsAsync(access_token, xeroTenantId, null, where, order, page);

            //var client = new HttpClient();
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", access_token);
            //client.DefaultRequestHeaders.Add("xero-tenant-id", "6889e4b6-7313-446b-8cc8-8c2056d83568");

            //var PAYITEMS = "https://api.xero.com/api.xro/2.0/PayItems";

            //var response = await client.GetAsync(PAYITEMS);
            //var content = await response.Content.ReadAsStringAsync();

            //fill the dropdown box based on the results 
            //var tenants = JsonConvert.DeserializeObject<List<Tenant>>(content);

            return result;
        }

        public async Task<Employees> GetEmployees()
        {
            var where = "Status==\"ACTIVE\"";
            var order = "LastName ASC";

            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.GetEmployeesAsync(access_token, xeroTenantId, null, where, order);
            return result;
        }
        
        public async Task<Employees> GetEmployee(string employeeID)
        {
            var apiInstance = new PayrollAuApi();
            var _employeeID = Guid.Parse(employeeID);

            var result = await apiInstance.GetEmployeeAsync(access_token, xeroTenantId, _employeeID);
            return result;
        }

        public async Task<TimesheetObject> GetTimesheet(string employeeID)
        {
            var apiInstance = new PayrollAuApi();
            var timesheetID = Guid.Parse(employeeID);

            var result = await apiInstance.GetTimesheetAsync(access_token, xeroTenantId, timesheetID);
            return result;
        }

        public async Task<Timesheets> GetTimesheets()
        {
            var where = "Status==\"ACTIVE\"";
            var ifModifiedSince = DateTime.Parse("2020-02-06T12:17:43.202-08:00");

            var page = 56;

            var apiInstance = new PayrollAuApi();

            var result = await apiInstance.GetTimesheetsAsync(access_token, xeroTenantId, ifModifiedSince, where, "", page);

            return result;
        }

        public async Task<Timesheets> PostTimesheet(List<Timesheet> timesheet)
        {
            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.CreateTimesheetAsync(access_token, xeroTenantId, timesheet);
            return result;
        }

        public async Task<PayrollCalendars> PostPayrollCalendar(List<PayrollCalendar> payrollCalendar)
        {
            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.CreatePayrollCalendarAsync(access_token, xeroTenantId, payrollCalendar);
            return result;
        }

        public async Task<PayrollCalendars> GetPayrollCalendars()
        {
            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.GetPayrollCalendarsAsync(access_token, xeroTenantId, null, null, null);
            return result;
        }

        public async Task<PayRuns> PostPayRun(List<PayRun> payRuns)
        {
            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.CreatePayRunAsync(access_token, xeroTenantId, payRuns);
            return result;
        }

        public async Task<PayRuns> GetPayruns()
        {
            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.GetPayRunsAsync(access_token, xeroTenantId, null, null, null);
            return result;
        }



    }
}
