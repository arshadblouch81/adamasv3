import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef,Injectable,OnChanges, SimpleChanges, } from '@angular/core'
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { GlobalService, ListService, TimeSheetService, ShareService, fundingDropDowns, leaveTypes, ClientService, Quotesdropdowns, NeedRisks } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject, EMPTY } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';


@Component({
    selector: "needsrisks-component",
    templateUrl: "./needsrisks.html",
    styleUrls: ['./needsrisks.css']
})

export class NeedsRisksComponent implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    @Input() ViewNeedsRisk:Subject<any>=new Subject();

    token: any;
    NeedsRisksForm: FormGroup;
    showRows: Array<boolean> = [false, false, false, false, false, false, false, false, false, false, false, false];
    Genders: Array<any> = Quotesdropdowns.Genders;
    Identities: Array<any> = [];
    MaritalStatuses: Array<any> = [];
    Religions: Array<any> = [];
    borns: Array<any> = Quotesdropdowns.borns;
    arrivals: Array<number> = []
    citizenships: Array<any>  = Quotesdropdowns.citizenships;
    languages: Array<any> = [];
    languagesfluency: Array<any> = Quotesdropdowns.languagesfluency;
    languagesLiteracy: Array<any> = Quotesdropdowns.languagesLiteracy;
    walking: Array<any> = NeedRisks.walking;
    shopping: Array<any> = NeedRisks.shopping;
    housework: Array<any> = NeedRisks.housework;
    medication: Array<any> = NeedRisks.medication;
    transport: Array<any> = NeedRisks.transport;
    money:Array<any> = NeedRisks.money;
    bathShower: Array<any> = NeedRisks.bathShower;
    confusion: Array<any> = NeedRisks.confusion;
    behaviouralProblems: Array<any> = NeedRisks.behaviouralProblems;
    communication: Array<any> = NeedRisks.communication;
    dressing: Array<any> = NeedRisks.dressing;
    eating: Array<any> = NeedRisks.eating;
    toileting: Array<any> = NeedRisks.toileting;
    gettingUp: Array<any> = NeedRisks.gettingUp;
    grooming: Array<any> = NeedRisks.grooming;
    risks: Array<any> = NeedRisks.risks;
    likelihood: Array<any> = NeedRisks.likelihood;
    consequences: Array<any> = NeedRisks.consequences;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef
    ) {
       // cd.detach();

        // this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     if (data instanceof NavigationEnd) {
        //         if (!this.sharedS.getPicked()) {
        //             this.router.navigate(['/admin/recipient/personal'])
        //         }
        //     }
        // });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'needs')) {
                this.user = data;

            }
        });

       
    }

    ngOnInit(): void {
        this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
       // this.user = this.sharedS.getPicked();

        this.buildForm();
        this.listDropDowns();
        this.ViewNeedsRisk.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.user=data;
           
           

        });

    }
    ngOnChanges(changes: SimpleChanges) {
       
        for (let property in changes) {
        
          }
         // this.cd.detectChanges();

}

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }


    listDropDowns() {
        let currentYear = new Date().getFullYear();
        for (let i = 1920; i <= currentYear; i++) {
            this.arrivals.push(i);
        }

        forkJoin([
            this.listS.getgendersIdenties(),
            this.listS.getreligions(),
            this.listS.GetLanguages(),
        ]).subscribe(data => {
            this.Identities = data[0];
            this.Religions = data[1];
            this.languages = data[2];
            this.cd.detectChanges();

        });
    }
    buildForm() {
        let today = new Date();
        this.NeedsRisksForm = this.formBuilder.group({
            name: '',
            address: '',
            genderOfBirth: '',
            identity: '',
            religion: '',
            ausBirth: '',
            arrivalYear: [1990],
            citizenship:'',
            primaryLanguage:'',
            primaryLanguageFluency:'',
            primaryLanguageLiteracy:'',
            englishFluency:'',
            englishLiteracy :'',
            transport:'',
            transportRisk:'',
            transportLikelihood:'',
            transportConsequences:'',
            shopping:'',
            shoppingRisk:'',
            shoppingLikelihood:'',
            shoppingConsequences:'',
            medication:'',
            medicationRisk:'',
            medicationLikelihood:'',
            medicationConsequences:'',
            money:'',
            moneyRisk:'',
            moneyLikelihood:'',
            moneyConsequences:'',
            walking:'',
            walkingRisk:'',
            walkingLikelihood:'',
            walkingConsequences:'',
            bathShower:'',
            bathShowerRisk:'',
            bathShowerLikelihood:'',
            bathShowerConsequences:'',
            moneyconfusion:'',
            moneyconfusionRisk:'',
            moneyconfusionLikelihood:'',
            moneyconfusionConsequences:'',
            behaviouralProblem:'',
            behaviouralProblemRisk:'',
            behaviouralProblemLikelihood:'',
            behaviouralProblemConsequences:'',
            communicate:'',
            communicateRisk:'',
            communicateLikelihood:'',
            communicateConsequences:'',
            dressing:'',
            dressingRisk:'',
            dressingLikelihood:'',
            dressingConsequences:'',
            eating:'',
            eatingRisk:'',
            eatingLikelihood:'',
            eatingConsequences:'',
            toileting:'',
            toiletingRisk:'',
            toiletingLikelihood:'',
            toiletingConsequences:'',
            gettingUp:'',
            gettingUpRisk:'',
            gettingUpLikelihood:'',
            gettingUpConsequences:'',
            grooming:'',
            groomingRisk:'',
            groomingLikelihood:'',
            groomingConsequences:'',
                

            


        });
    }


    show_Rows(index: number) {
        this.showRows[index] = !this.showRows[index];
        this.cd.detectChanges();
    }

}