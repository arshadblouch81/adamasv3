using Microsoft.AspNetCore.Http;


namespace Adamas.Models.Modules
{
    public class BlobFile
    {
        public string MediaDisplay { get; set; }
        public string MediaText { get; set; }
        public string Media { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Program { get; set; }
        public string Item { get; set; }
        public string ClientGroup { get; set; }
        public string Type { get; set; }
        public string Target { get; set; }
        public IFormFile FileBlob { get; set; }
    }
}
