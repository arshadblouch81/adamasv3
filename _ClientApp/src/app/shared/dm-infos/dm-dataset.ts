import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { TimeSheetService } from '@services/index'

interface DataSet {
    program: string,
    funding: string,
    traccsActivityCode: string,
    dataSetActivity: string,
    dataSetGroup: string,
    activityCode: string,
    carer: string,
    serviceOutlet: string
}
@Component({
    selector: 'dm-dataset',
    templateUrl: './dm-dataset.html',
    styles: [`
        .dataset-group > div{
            display:flex;
        }
        .dataset-group > div:nth-child(even){
            background: #f7f7f7
        }
        .dataset-group > div > div:first-child{
            flex: 2;
            text-align: right;            
        }
        .dataset-group > div > div:last-child{
            display: flex;
            align-items: center;
            flex: 3;
            padding: 0 1rem;
        }
    `],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DmDataSetComponent),
            multi: true
        }
    ]
})

export class DmDataSetComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    result: DataSet

    constructor(
        private timeS: TimeSheetService
    ){ }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        this.timeS.getdataset(user.recordno)
            .subscribe(data => {
                this.result = {
                    program: user.rProgram,
                    funding: '',
                    traccsActivityCode: data.title,
                    dataSetActivity: data.it_DataSet,
                    dataSetGroup: data.datasetGroup,
                    activityCode: data.haccType,
                    carer: user.Recipient,
                    serviceOutlet: ''
                }
            })
    }
}