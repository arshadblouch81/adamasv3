using System;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Text;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using Microsoft.Extensions.Caching.Memory;
using System.Threading;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Configuration;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;


using Adamas.Controllers;

namespace Adamas.DAL {

    public class CacheService: ICacheService{
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly IMemoryCache _cache;
        private IConfiguration configuration { get; }
        private Dictionary<string, string> loggedInTokens;
        private int NUM_OF_USERS;
        public CacheService(
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            IMemoryCache cache,
            IConfiguration _configuration
            )
        {    
             _jwtOptions = jwtOptions.Value;
            _conn = conn;
            NUM_OF_USERS = 0;
            loggedInTokens = new Dictionary<string, string>();
            _cache = cache;   
            configuration = _configuration;    
        }
        public dynamic GetTokenValue(string token){
            return new {
                data = _cache.Get(token),
                users = NUM_OF_USERS,
                userlist = loggedInTokens
            };
        }

        public async Task<List<dynamic>> GetListOfLoggedUsers(){
            List<dynamic> list = new List<dynamic>();

            return await Task.Run(() => {

                foreach(KeyValuePair<string,string> user in loggedInTokens){
                    list.Add(new {
                        Key = user.Key,
                        Value = user.Value
                    });
                }
                return list;
            });
        }

        // public List<string> GetListTokens(){
        //     return loggedInTokens;
        // }

        public bool RemoveToken(string token){
            _cache.Remove(token);
            return true;
        }

        public async Task<CacheResult> AppLoginAsync(string token){
                
            string cacheValue;

            var uid = GetUniqueID(token);

            if(_cache.TryGetValue(uid, out cacheValue)){
                _cache.Remove(uid);
                loggedInTokens.Remove(uid);
            }

            int expiryTime = Convert.ToInt32(configuration["JwtIssuerOptions:ExpireTime"]);

            var expirationToken = new CancellationChangeToken(new CancellationTokenSource(TimeSpan.FromMinutes(expiryTime + .01)).Token);

            var cacheEntry = await _cache.GetOrCreateAsync(uid, entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(expiryTime);
                entry.RegisterPostEvictionCallback(callback: EvictionCallback, state: this);
                entry.AddExpirationToken(expirationToken);

                
                this.NUM_OF_USERS++;

                loggedInTokens.Add(
                    uid.ToString(), 
                    token.ToString()
                );
        

                return Task.FromResult(token);
            }); 
       
            return new CacheResult{ Result = true };
        }

        private async Task DeleteToken(string token){
            await Task.Delay(10000);
        }

        public async Task<Boolean> AppLogoutAsync(string uid){
            
            string token = string.Empty;

            if(_cache.TryGetValue(uid, out token)){
                _cache.Remove(uid);
                loggedInTokens.Remove(uid);
            }
            
            return true;
        }

        private async Task<Boolean> IsLoggedIn(string token){
            bool isFound = false;
            var _token = GetUniqueID(token);
            
            var keys = await ListOfTokens();
            foreach(string key in keys){
                var keyToken = GetUniqueID(key);
                if(keyToken == _token){
                    isFound = true;
                    break;
                }
            }
            return isFound;
        }

        private static string GetUniqueID(string token){
            var _token = new JwtSecurityTokenHandler().ReadToken(token) as JwtSecurityToken;
            return ((List<Claim>)_token.Claims).Find(x => x.Type == "uniqueID").Value;
        }

        private async Task<List<string>> ListOfTokens(){
            return await Task.Run(() => _cache.Get<List<string>>("tokens"));
        }

        private async Task<Boolean> IsTokenPresent(string token){
            bool isFound = false;
            var keys = await ListOfTokens();

            foreach(string key in keys){
                if(key == token){
                    isFound = true;
                    break;
                }              
            }            
            return isFound;
        }

        private void EvictionCallback(object key, object value,
            EvictionReason reason, object state)
        {
            string cacheValue;
            if(_cache.TryGetValue(key, out cacheValue)){
                if(string.Compare(value.ToString(), cacheValue) == 0){
                    _cache.Remove(key);
                    loggedInTokens.Remove(key.ToString());
                    this.NUM_OF_USERS--;
                }              
            } else {
                loggedInTokens.Remove(key.ToString());
                this.NUM_OF_USERS--;
            }
        }
        
    } 

    public class CacheResult {
        public bool Result { get; set ;}
        public string Message { get; set; }
    }

}