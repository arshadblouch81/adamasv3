
import {takeUntil, switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import  { TimeSheetService, GlobalService } from '../../services/index';

import { Subject } from 'rxjs';
import * as moment from 'moment';

@Component({
    selector: 'history-client',
    templateUrl: './history.html',
    styles:[
        `
        

        @media (max-width: 1000px)  {        


            .web-first{
                display:none;
            }
            .mobile-first{
                display:block;
            }

            .stackview {
                display: inline-block;
                width: 100%;          
            }        
            

        }

        @media (min-width:1000px) { 
            .web-first{
                display:block;
            }
            .mobile-first{
                display:none;
            }

        }

        
        table thead tr th{
            background: linear-gradient(to right, rgb(29, 166, 234), rgb(26, 172, 236));
            color: #fff;
            font-size: 12px;
            font-weight: normal;
        }
        td:nth-child(8),td:nth-child(9) {
            background:#e8ffe4;
        }
        td:nth-child(10){
            background:#cfffc6;
        }
        clr-checkbox {
            margin:0;
        }
        clr-checkbox >>> label::before{
            background:#fff !important;
            border:1px solid #757575 !important;
        }

        clr-checkbox >>> label::after{
            border-left: 2px solid #2b62ff !important;
            border-bottom: 2px solid #2b62ff !important;
        }

        tbody tr:hover{
            background:#f1f1f1;
        }
        .loading{
            text-align:center;
            margin-top:1rem;
        }
        .no-margin{
            margin:0;
        }
        clr-checkbox-wrapper >>> label{
            display: inherit !important;
        }
        table thead tr th{
            font-size:11px;
        }
        `
    ]
})

export class HistoryClient implements OnInit, OnDestroy{

    @Input() id;

    private accountNo: string;
    private unsubscribe$ = new Subject()

    dateStream = new Subject<any>();

    loading: boolean;
    timesheets: Array<any>;
    sDate: any;
    eDate:any;

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService
    ){       
        this.dateStream.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((x: any) => this.getTimeSheet({ 
                AccountNo: this.accountNo, 
                personType: 'Recipient',
                startDate: moment(this.sDate).format('YYYY/MM/DD'),
                endDate:  moment(this.eDate).format('YYYY/MM/DD')
            })),
            takeUntil(this.unsubscribe$),)
            .subscribe(data => {
                this.loading = false;
                this.timesheets = data;
            })
            
    }

    ngOnInit(){
        this.accountNo = this.id || this.globalS.decode().code;
        this.sDate = moment().startOf('month');
        this.eDate = moment().endOf('month');
        this.dateStream.next();
    }

    ngOnDestroy(){
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    getTimeSheet(data: Dto.GetTimesheet){
        this.loading = true;
        this.timesheets = [];
        return this.timeS.gettimesheets({
            AccountNo: data.AccountNo,
            personType: data.personType,
            startDate: data.startDate,
            endDate: data.endDate
        })
    }
}
