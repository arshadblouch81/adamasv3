import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService,dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import format from 'date-fns/format';


@Component({
    selector: '',
    templateUrl: './groups.html',
    styles:[ `
         nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakeGroups implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;

    definedOpen: boolean = false;   
    preferenceOpen: boolean = false;

    addOREdit: number;
    token:any;

    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    groupTypes: Array<any>;
    //groupPreferences: Array<any>;

    userGroupForm: FormGroup;
    //preferenceForm: FormGroup;
    dateFormat: string = dateFormat;
    dropDowns: {
        userGroups: Array<string>,
        preferences: Array<string>
    }
    selectedGroups:Array<any>=[];
    //selectedPrefernces: any;
    activeRowData:any;
    selectedRowIndex:number;
    HighlightRow2:number;
    selectedGroup:any
    serviceActivityList:Array<any>=[];
    txtSearch:string;
    originalList:Array<any>=[];
    ViewAllocateResourceModal:boolean=false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'groups')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
       
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();
        this.listDropDowns();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm( data: any): void {
        let records =this.selectedGroups.map(x=>x.recordNumber).join(',').toString();
       this.deleteUserGroup(records);
    }

search(user: any = this.user) {
        this.cd.reattach();
        this.loading = true;
        forkJoin([
            this.timeS.getgrouptypes(user.id),
            this.timeS.getgroupalerts(user.id)
        ]).subscribe(data => {
            this.loading = false;
            this.groupTypes = data[0];
            this.serviceActivityList = data[1].map(x => x.description);
            this.originalList = data[1].map(x => x.description);
            this.loading = false;
            this.cd.markForCheck();
        });
    }

    buildForm() {
        this.userGroupForm = this.formBuilder.group({
            group: new FormControl('', [Validators.required]),
            notes: new FormControl(''),
            personID: new FormControl(''),
            recordNumber: new FormControl(0),
            recurring: new FormControl(false),
            mobileAlert: new FormControl(false),
            date1: new FormControl(null),
            date2: new FormControl(null),
            email: new FormControl(''),
            selectedGroup:new FormControl(''),
         })
 
    }

    save() {

         let inputs = {
            personID : this.user.id,               
            selectedConsent : this.selectedGroups,
            creator : this.token.user,          
            notes : '',
            Date1 :null ,
            Date2 :null,
           
        };

    
    this.ViewAllocateResourceModal=false;
    this.timeS.postgroups(inputs).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {            
            //this.success();
            
            this.showEditModal(data);
          
            this.globalS.sToast('Success', 'Data Added');
            this.search(this.sharedS.getPicked());            
            this.loading = false;
            
                        
          }
    });

    }
    success() {
        this.search(this.sharedS.getPicked());
        this.loading = false;
      }

    delete(index: number) {

    }

    handleCancel(view: number) {
        
        this.addOREdit = 1;       
        this.selectedGroups = []
        this.definedOpen = false;
        // this.userGroupForm.reset();
        
    }

    reloadAll(){
        this.search()
        this.listDropDowns()
    }
    logs(event: any) {
        this.selectedGroups = event;
    }
   
    listDropDowns(){
        forkJoin([
            this.listS.getusergroup(this.user.id),
            this.listS.getrecipientpreference(this.user.id)
        ]).subscribe(data => {
            this.loading = false;
            let usergroup = data[0].map(x => {
                return {
                    label: x,
                    value: x,
                    checked: false
                }
            });
           let preference = data[1].map(x => {
                return {
                    label: x,
                    value: x,
                    checked: false
                }
            });
            this.dropDowns = {
                userGroups: usergroup,
                preferences: preference
            }
        });
    }

    showAddModal(view: number) {
        this.addOREdit = 1;
        this.ViewAllocateResourceModal = true;
    }

    showEditModal(data: any){
        
        this.addOREdit = 2;
        this.definedOpen = true;
        this.userGroupForm.patchValue({
            group: data.name,
            notes: data.notes,
            recurring: data.recurring, 
            mobileAlert: data.mobileAlert,               
            date1: data.date1,
            date2: data.date2,
            email: data.email,
            recordNumber: data.recordNumber,
           
        })
    }
    deleteUserGroup(data: any){
        this.timeS.deletegroups(data).subscribe(data =>{
            if(data){
                this.reloadAll()
                this.globalS.sToast('Success','Data Deleted')
            }
        })
    }
    userGroupProcess(){

        this.userGroupForm.controls['personID'].setValue(this.user.id)
        this.userGroupForm.controls['selectedGroup'].setValue(this.selectedGroups)
        
       

       
        let data= this.userGroupForm.value;
        let userGroup = {
            name: data.group,
            notes: data.notes,
            recurring: data.recurring, 
            mobileAlert: data.mobileAlert,               
            date1: format(new Date(data.date1),'yyyy-MM-dd'),
            date2: format(new Date(data.date2),'yyyy-MM-dd'),
            email: '',
            recordNumber: data.recordNumber,
            personID:this.user.id
        }

            this.timeS.updategroups(userGroup)
                .subscribe(d => {
                    if(d){
                        
                        this.globalS.sToast('Success','Data Updated')
                        this.reloadAll()
                        this.handleCancel(1);
                    }
                })
        
    }

    handleOk(){

    }
   
    RecordClicked(event: any, value: any, i: number, main:boolean) {

        this.activeRowData=value;
        if (main)
            this.selectedRowIndex =i;
        else{
            this.selectedRowIndex = -1;
            this.HighlightRow2=i;
        }

        this.selectedGroup=value;  
       //console.log(this.selectedGroups.includes(value))
        //let tindex = this.selected.indexOf(value.recordNo);
       
    
        if (event.ctrlKey || event.shiftKey) {
         
            if (!this.selectedGroups.includes(value)) { 
                this.selectedGroups.push(value);
            }
            //this.selected.push(value.recordNo)
          
    
        } else {
          
          this.selectedGroups = [];
         // this.selected = [];
    
          if (this.selectedGroups.length <= 0) {
            this.selectedGroups.push(value)
            
          }
        }
      }
   
     onItemDbClick(sel:any , i:number) : void {
         this.selectedGroup=sel;
         if (!this.selectedGroups.includes(sel)) { 
            this.selectedGroups.push(sel);
        }

         this.save();
     
     }
    onTextChangeEvent(event:any){
        // console.log(this.txtSearch);
         let value = this.txtSearch.toUpperCase();
         //console.log(this.serviceActivityList[0].description.includes(value));
         this.serviceActivityList=this.originalList.filter(element=>element.description.includes(value));
     }

    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }
      print(){
        
      }
      generatePdf(){
      
        var Title = " Groups "
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.groupTypes,                        
                "userid": this.token.user,
                "txtTitle":  Title+" for "+this.user.code,                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = Title+" for "+this.user.code
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false; 
                    this.cd.detectChanges();                      
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
      }
      close(){
          this.router.navigate(['/admin/recipient/personal']);
      }
    
}