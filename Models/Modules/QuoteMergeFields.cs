
using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Adamas.Models.Modules{
    public class QuoteMergeFields {        
        public string Id { get; set; }
        [Column("UR Number")]
        public string URNumber { get; set; }
        [Column("Account#")]
        public string AccountNo { get; set; }
        public string Title { get; set; }
        [Column("surname/organisation")]
        public string SurnameOrg { get; set; }
        [Column("First Name")]
        public string FirstName { get; set; }
        [Column("Other Names")]
        public string OtherNames { get; set; }
        [Column("Preferred Name")]
        public string PreferredName { get; set; }
        
        public string FullNameSurnameFirst { get; set; }
        public string FullNameSurnameLast { get; set; }
        public string Gender { get; set; }
        [Column("D.O.B.")]
        public DateTime? DateOfBirth { get; set; }
        [Column("ndisnumber")]
        public string NDISNo { get; set; }
        public string OHSProfile { get; set; }
        public string Notes { get; set; }
        public string Type { get; set; }
        public string Branch { get; set; }
        [Column("Primary Coord")]
        public string PrimaryCoord { get; set; }
        public string Category { get; set; }
        public string ONIRating { get; set; }
        [Column("Activation Date")]
        public DateTime? ActivationDate { get; set; }
        [Column("Deactivation Date")]
        public DateTime? DeactivationDate { get; set; }
        public string PrimaryStreetAddress { get; set; }
        public string PrimarySuburb { get; set; }
        public string PrimaryState { get; set; }
        public string PrimaryPostcode { get; set; }
        public string UsualStreetAddress { get; set; }
        public string UsualSuburb { get; set; }
        public string UsualState { get; set; }
        public string UsualPostcode { get; set; }
        public string ContactAddress { get; set; }
        public string ContactSuburb { get; set; }
        public string ContactState { get; set; }
        public string ContactPostcode { get; set; }
        public string PrimaryPhone { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}