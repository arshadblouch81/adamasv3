﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class LeaveApplicationDto
    {
        public int RecordNumber { get; set; }
        public string LeaveType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notes { get; set; }
        public DateTime? ReminderDate { get; set; }
        public DateTime? RequestedDate { get; set; }
        public bool? Approved { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public bool? EquipmentCode { get; set; }
        public bool? MakeUnavailable { get; set; }
        public bool? UnallocUnapproved { get; set; }
    }
}
