import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import { GlobalService, SettingsService, LoginService, ClientService, TimerService } from '@services/index';

import { ProfileInterface } from '@modules/modules';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

import {
    Router
  } from '@angular/router';
  
declare var Dto: any;

@Component({
    styles: [`
  .side-button{
        position: fixed;
        right: 5px;
        background: #ff3737;
        color: #fff;
        width: 35px;
        height: 36px;
        cursor: pointer;
        text-align: center;
        z-index: 10;
        top: 10rem;
    }
    .side-button:hover{
        background:#ff3737db;
    }
    .side-button i{
        font-size: 18px;
        margin-top: 7px;
    }
    `],
    templateUrl: './profile.html'
})


export class ProfileClient implements OnInit, OnDestroy {
    user: ProfileInterface;
    token: any;

    visible: boolean = false;
    passwordResetVisible: boolean = false;

    _settings: SettingsService;
    personID: any;

    changePasswordForm: FormGroup;

    newReferralModal: boolean = false;

    constructor(
        private globalS: GlobalService,
        private settings: SettingsService,
        private fb: FormBuilder,
        private clientS: ClientService,
        private router: Router,
        private timerS: TimerService
    ) {

    }

    ngOnInit() {
        this._settings = this.settings;
        this.token = this.globalS.decode();

        this.BUILD_FORM();

        this.personID = this.token.code;
        
        if(this.globalS.pickedMember){
            var pickedUser: any = this.globalS.pickedMember;

            this.user = {
                name: pickedUser.accountNo,
                id: pickedUser.uniqueID,
                view: 'recipient'
            }
        } else {
            this.user = {
                name: this.token.code,
                view: 'recipient',
                id: this.token.uniqueID
            }
        }
    }

    BUILD_FORM(){
        this.changePasswordForm = this.fb.group({
            newPassword: null,
            newPasswordConfirm: null
        })
    }

    ngOnDestroy() {

    }

    showNotices() {
        this.visible = true;
    }

    changePass(){
        this.newReferralModal = !this.newReferralModal;
    }

    openReferModal(newAccountNo: any){

        const { newPassword, newPasswordConfirm } = this.changePasswordForm.getRawValue();

        if(newPassword !== newPasswordConfirm){
            this.globalS.eToast('Error','Passwords do not match!');
            return;
        }

        this.clientS.changepassword({
            Password: newPassword,
            PasswordConfirmation: newPasswordConfirm,
            NewAccountNo: newAccountNo?.code
        }).subscribe(data => {
            this.globalS.sToast('Success','Password Updated');
            this.router.navigate(['']);
        })
    }
}