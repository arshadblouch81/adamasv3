
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class MTAJobInput
    {
    public string Recordno { get; set; }
    public bool cancel { get; set; }

    public string timeStamp { get; set; }
    public string latitude { get; set; }
    public string longitude { get; set; }
    
    public string location { get; set; }

    }
}