﻿namespace AdamasV3.Models.Dto
{
    public class ViewClientPortalDto
    {
        public bool ShowDocuments { get; set; }
        public bool ShowOPNote { get; set; }
        public bool ShowCaseNote { get; set; }
        public bool ShowServiceNote { get; set; }
        public bool ShowIncidentNote { get; set; }
        public bool ShowClinicalNote { get; set; }
        public bool ShowNotesTab { get; set;  }

    }
}
