import { Component, ContentChildren, QueryList, AfterContentInit, Input, Output, EventEmitter } from '@angular/core'
import { TableMarkColumn  } from "./tablemark-column";

@Component({
    selector: 'tablemark',
    template:`
        <table class="table table-compact">
  
               <ng-content></ng-content>
       
        </table>
    `,
    styles:[
        `
        td:last-child{
            background:#d6d6d6;
            width:3rem;
        }
        th:last-child{
            background:#d6d6d6;
        }
        i{
            font-size:19px;
            color:#5f5d5d;
            margin:0 4px;
        }
        i:hover{
            color:#353232;
            cursor:pointer;
        }
        `
    ]
})

export class TableMark implements AfterContentInit {
    @ContentChildren(TableMarkColumn) columns: QueryList<TableMarkColumn>;
    @Input() sourceData: any;
    @Output() selected = new EventEmitter<any>();

    constructor(){
        
    }

    ngAfterContentInit(){
        console.log(this.columns);
    }
}