using System;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Text;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using Microsoft.Extensions.Caching.Memory;
using System.Threading;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Configuration;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;

using Adamas.Controllers;

namespace Adamas.DAL {

    public class ClientService: IClientService
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        
        public ClientService(
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            DatabaseContext context,
            IGeneralSetting setting
            )
        {    
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _conn = conn;
            GenSettings = setting;
        }

        public async Task<string> GetPrimaryManager(string accountNo)
        {
            using (var conn = _conn.GetConnection())
            {

                using (SqlCommand cmd = new SqlCommand(@"SELECT dbo.getCoordinator_Email(@accountNo,'RECIPIENT') AS Email", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@accountNo", accountNo);

                    SqlDataReader rd = cmd.ExecuteReader();

                    if (await rd.ReadAsync()){
                        return GenSettings.Filter(rd["Email"]);
                    }

                    return null;
                }
            }

        }


    }
}