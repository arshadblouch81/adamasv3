import { Directive, ContentChildren, QueryList, AfterContentInit, ElementRef, HostListener } from '@angular/core'
import { MovableDirective } from './movable.directive';

interface Boundaries{
    minX: number,
    maxX: number,
    minY: number,
    maxY: number
}

@Directive({
    selector: '[containerDraggable]'
})

export class ContainerDraggableDirective implements AfterContentInit {
    
    @ContentChildren(MovableDirective) movables: QueryList<MovableDirective>
    private boundaries: Boundaries;

    constructor(
        private element: ElementRef
    ){ }

    private notHighlight: boolean = true



    // @HostListener('mouseover',['$event'])
    // onMouseEnter(event: MouseEvent){

    //     setTimeout(() => {
    //         this.element.nativeElement.style.backgroundColor = '#e8e8e8'   
    //     });
    

    // }

    // @HostListener('mouseleave',['$event'])
    // onMouseLeave(event: MouseEvent){
    //     setTimeout(() => {
    //         this.element.nativeElement.style.backgroundColor = null   
    //     });
    // }

    ngAfterContentInit(){
        this.movables.forEach(movable => {            
            movable.dragStart.subscribe(() => this.measureBoundaries(movable))
            movable.dragMove.subscribe(() => this.maintainBoundaries(movable))
        })
    }


    private measureBoundaries(movable: MovableDirective){
        console.log('hasha')
        this.notHighlight = false
        // const viewRect: ClientRect = this.element.nativeElement.getBoundingClientRect()
        // const movableClientRect: ClientRect = movable.element.nativeElement.getBoundingClientRect()

        // this.boundaries = {
        //     minX: viewRect.left - movableClientRect.left + movable.position.x,
        //     maxX: viewRect.right - movableClientRect.right + movable.position.x,
        //     minY: viewRect.top - movableClientRect.top + movable.position.y,
        //     maxY: viewRect.bottom - movableClientRect.bottom + movable.position.y
        // }
        // console.log(this.boundaries)
    }

    private maintainBoundaries(movable: MovableDirective){
        // movable.position.x = Math.max(this.boundaries.minX, movable.position.x);
        // movable.position.x = Math.min(this.boundaries.maxX, movable.position.x);
        // movable.position.y = Math.max(this.boundaries.minY, movable.position.y);
        // movable.position.y = Math.min(this.boundaries.maxY, movable.position.y);
    }
}