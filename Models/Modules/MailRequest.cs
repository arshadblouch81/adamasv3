﻿using MimeKit;
using System.Collections.Generic;

namespace AdamasV3.Models.Modules
{
    public class MailRequest
    {
        public List<Mail> To { get; set; }
        public string Subject { get; set; } 
        public string Body { get; set; }
    }

    public class Mail
    {
        public string Name { get; set; }    
        public string Email { get; set; }
    }
}
