
import {forkJoin,  Observable ,  merge ,  Subject, Subscriber, Subscription } from 'rxjs';

import { mergeMap, debounceTime, distinctUntilChanged, takeUntil, switchMap} from 'rxjs/operators';
import { Component, OnInit, AfterContentInit, ViewChild, OnDestroy } from '@angular/core'
import { DatePipe } from '@angular/common'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { DomSanitizer } from '@angular/platform-browser';

import { LoginService, LandingService, ClientService, TimeSheetService, GlobalService, UploadService, StaffService, ListService, statuses, contactGroups, recurringInt,recurringStr, gender, types } from '../../services/index'
import * as moment from 'moment';
import { RemoveFirstLast } from '../../pipes/pipes';
import { BsModalComponent } from 'ng2-bs3-modal';
import { ClrWizard } from "@clr/angular";

import * as _ from 'lodash';

interface Notes {
    miscellaneous: string,
    loans: any[]
}

@Component({
    templateUrl:'./staff.html',
    styles:[
        `
        .search{
            width: 20rem;
            margin: 10px auto;
        }
        .control-tables{
            list-style:none;
            margin: 0.5rem 0;
            display: flex;        
        }
        .control-tables li{
            float:left;
            padding:5px;
            margin-right:10px;
            text-align:center;
        }
        .control-tables li:hover{
            color:#457094;
            cursor:pointer;
        }
        .control-tables li span{
            display: block;
            font-size: 12px;
            line-height: 5px;
        }        
        td i:first-child{
            margin:0 5px 0 0;
        }
        ul.staff-controls li{
            display:inline;
        }
        select{
            display:block;
            width:100%;
        }
        .work-hours tbody tr td input{
            width:20px;
        }
        .contracted-hours tbody tr td{
            width:20px;
        }
        .contracted-hours tbody tr:nth-child(1){
            background:#dbe5ec;
        }
        .contracted-hours tbody tr:nth-child(2){
            background:#f3f3f3;
        }
        .contracted-hours tbody tr td:first-child{
            font-weight:500;
        }
        .contracted-hours tbody tr td input{
            min-width:6.6%;
        }
        .contracted-hours tbody tr:last-child td{
            padding: 0 5px;
        }
        ul.pad{
            list-style:none;
        }
        .disableOverFlow bs-modal-body >>> .modal-body{
            padding: 0 .125rem;
            display: flex;
            max-height: 70vh;
            overflow-y: inherit;
            overflow-x: inherit;
        } 
        button.import{
            margin-left: 5px;
            background: linear-gradient(rgb(118, 125, 165), rgb(109, 165, 214));
            border: 0;
            border-radius: 3px;
            color: #fff;
            padding-right: 14px;
        }
        button.import:hover{
            background:linear-gradient(rgb(118, 125, 165), rgb(85, 146, 199));
        }
        i.upload{
            font-size: 20px;
            float: left;
            margin-top: 1px;
            margin-right: 6px;
        }
        .card-header button{
            float:right;
        }
        .input-align-mid input{
            text-align:center;
        }
        table td{
            padding:5px 0;
        }
        .address-container{
            display: inline-block;
        }
        .address-container:nth-child(2){
            padding-top:1rem;
        }
        ul{
            list-style:none;
        }
        .two-tarea-height{
            height:2rem;
        }
        .unavail-list{
            margin-left:15px;
        }
        .unavail-list > label{
            font-size:12px;
        }
        .document-container li{
            padding: 5px 10px;
            cursor:pointer;
            color:red;
        }
        .document-container li:hover{
            background:#dfedff;
        }
        .exists{
            color:green;
        }
        `
    ]
})

export class StaffAdmin implements OnInit, AfterContentInit, OnDestroy {
    private unsubscribe$ = new Subject();
    
    @ViewChild('updateStaffModal', { static: true }) updateStaffModal: BsModalComponent;
    @ViewChild('addpositionModal', { static: true }) addpositionModal: BsModalComponent;
    @ViewChild('updateStaffCompetenciesModal', { static: true }) updateStaffCompetenciesModal: BsModalComponent;    
    @ViewChild('addhrnoteModal', { static: true }) addhrnoteModal: BsModalComponent;
    @ViewChild('updatehrnoteModal', { static: true }) updatehrnoteModal: BsModalComponent;
    @ViewChild('addopnoteModal', { static: true }) addopnoteModal: BsModalComponent;
    @ViewChild('importModal', { static: true }) importModal: BsModalComponent;
    @ViewChild('UserDefinedGroup1Modal', { static: true }) UserDefinedGroup1Modal: BsModalComponent;
    @ViewChild('UserDefinedGroup2Modal', { static: true }) UserDefinedGroup2Modal: BsModalComponent;
    @ViewChild('addRemindersModal', { static: true }) addRemindersModal: BsModalComponent;
    @ViewChild('uploadDocModal', { static: true }) uploadDocModal: BsModalComponent;
    @ViewChild('leaveApplication', { static: true }) leaveApplication: BsModalComponent;
    @ViewChild('preview', { static: true }) preview: BsModalComponent;
    
    @ViewChild("addcontactskinModal", { static: true }) addcontactskinModal: ClrWizard;
    @ViewChild("documentModal", { static: true }) documentModal: ClrWizard;
    @ViewChild("addCompetenciesModal", { static: true }) addCompetenciesModal: ClrWizard;
    @ViewChild("newStaffModal", { static: true }) newStaffModal: ClrWizard;
    @ViewChild("addIncidentModal", { static: true }) addIncidentModal: ClrWizard;

    terminateModal: boolean = false;

    newStaffOpen: boolean = false;
    newIncidentOpen: boolean = false;
    loading:boolean = false;
    loadResult: boolean = false;
    miscReadOnly: boolean = true
    addContactOpen: boolean = false;
    addCompetenciesOpen: boolean = false;
    documentOpen: boolean = false;
    fp_hide: boolean = false;
    isLeaveSaveBtnDisable: boolean = false;
    
    selected: any;

    dropDowns: Dto.DropDowns;

    user:any;
    tableData: Array<any> = null;
    statuses: Array<string> = statuses;
    contactGroups: Array<string> = contactGroups;
    categoryOP: Array<string>  = [];
    list: Array<any>;
    competencies: Array<string>;
    categoryHR: Array<string>;
    listpositions: Array<string>;
    userdefined1: Array<any>;
    userdefined1List: Array<string>;
    userdefined2: Array<any>;
    userdefined2List: Array<any>;
    remindersList:Array<string>;
    recurringIntArray: Array<number> = recurringInt;
    recurringStrArray: Array<string> = recurringStr;
    awardsArr: Array<string>;
    staffsArr: Array<string>;

    tab:number = 1;
    changeTab = new Subject<number>();
    dataSub$ = new Subscription();
    //dataObservable = 

    payDetails: any;
    selectedRow: any;

    notes: Notes;

    attendanceGroup: FormGroup;
    kindetailsGroup: FormGroup;
    contactFormGroup: FormGroup;
    competencyGroup: FormGroup;
    hrGroup: FormGroup;
    opGroup: FormGroup;
    positionGroup: FormGroup;
    user1Group: FormGroup;
    user2Group: FormGroup;
    remindersGroup: FormGroup;
    leaveGroup: FormGroup;
    payGroup: FormGroup;
    incidentGroup: FormGroup;
    terminateGroup: FormGroup

    newStaffGroup: FormGroup;

    dbAction: number;
    token: any;

    genderArr: Array<string> = gender;
    typesArr: Array<string> = types

    addressType: Array<string> = [];
    contactType: Array<string> = [];

    leave_dd: any = {
        paycode: [],
        programs: [],
        activitycode: [],
        leaveActivityCodes: [],
        leaveBalances: []
    }

    incident_dd: any = {
        activities: [],
        programs: [],
        incidentType: [],
        staffs: [],
        locations:[],
        locationCategories: []
    }

    fileObject: {
        file: '',
        newFile: ''
    }

    loadStaff: boolean = true;
    public templates$: Observable<any>;
        
    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private listS: ListService,
        private staffS: StaffService,
        private uploadS: UploadService,
        private clientS: ClientService,
        private logS: LoginService,
        private formBuilder: FormBuilder,
        public sanitizer: DomSanitizer
    ){

        this.changeTab.pipe(
                debounceTime(100),
                //distinctUntilChanged()
            ).subscribe(data => {
               
            this.dataSub$.unsubscribe();            
            this.loadResult = false;
            this.loading = true;
            const account = this.user.code;
            const id = this.user.id ;

            this.clear();

            switch(data){   
                case 1:                      
                    this.tableData = [];
                    this.loading = false;
                    break;
                case 2:
                    this.dataSub$ = this.timeS.getcontactskinstaff(account)
                        .subscribe(data => {
                            this.tableData = data;
                            this.selected = data.length > 0 ? data[0] : '';
                            
                            this.loadResult = true;
                            this.loading = false;
                        });
                    break;
                case 3: 
                    this.payGroup.reset();

                    this.dataSub$ = this.timeS.getpaydetails(id)
                                        .subscribe(data => {                 
                                            this.payGroup.patchValue(data)
                                        })

                    this.dataSub$ = this.listS.getawards()
                                        .subscribe(data => this.awardsArr = data)


                    this.dataSub$ =this.timeS.getstaff({
                                        User: this.globalS.decode().nameid,
                                        SearchString: ''
                                    }).subscribe(data => {
                                        this.staffsArr = data;
                                    }); 

                    this.loadResult = true;
                    this.loading = false;
                    break;
                case 4: 
                    this.dataSub$ = forkJoin([
                        this.timeS.getnotesmiscellaneous(id),
                        this.timeS.getnotesloans(id)
                    ]).subscribe(data =>{
                        this.loading = false;
                        this.notes = {
                            miscellaneous: this.globalS.rt2filter(data[0].data),
                            loans: data[1]
                        }
                    })
                    this.loadResult = true;
                    break;
                case 5:
                    this.dataSub$ = this.timeS.getleaveapplication(account).subscribe(data => {
                        this.tableData = data;
                        this.loading = false;                    
                    });
                    this.loadResult = true;
                    break;
                case 6: 
                    this.dataSub$ = this.listS.getlistreminders().subscribe(data => this.remindersList = data)
                    this.dataSub$ = this.timeS.getreminders(account).subscribe(data => {
                        this.tableData = data;   

                        this.loadResult = true;
                        this.loading = false;
                    });
                    break;
                case 7:
                    this.dataSub$ = this.timeS.getopnotes(account).subscribe(data => {
                        this.tableData = data;
                        this.loading = false;
                    })
                    this.dataSub$ = this.listS.getlistop().subscribe(data => this.categoryOP = data)
                    this.loadResult = true;
                    break;
                case 8:
                    this.dataSub$ = this.timeS.gethrnotes(account).subscribe(data => {
                        this.tableData = data;
                        this.loading = false;
                    });
                    this.listS.getlisthr().subscribe(data => this.categoryHR = data)
                    this.loadResult = true;
                    break;
                case 9:
                    this.dataSub$ = this.timeS.getcompetenciesall()
                        .subscribe(data => this.competencies = data)

                        this.dataSub$ = this.timeS.getcompetencies(account).subscribe(data => {
                            this.tableData = data;
                            
                            this.loadResult = true;
                            this.loading = false;
                        });
                    break;
                case 10:
                    this.dataSub$ = this.timeS.gettraining(account).subscribe(data => {
                        this.tableData = data;
                        
                        this.loadResult = true;
                        this.loading = false;
                    });
                    break;
                case 11:
                    this.dataSub$ = this.timeS.getincidents(account).subscribe(data => {
                        this.tableData = data;
                        
                        this.loadResult = true;
                        this.loading = false;
                    });
                    break;
                case 12:
                    this.dataSub$ = this.timeS.getdocuments(account).subscribe(data =>{
                        this.tableData = data;

                        this.loadResult = true;
                        this.loading = false;
                    });

                    break;
                case 13:
                    break;
                case 14:
                    this.dataSub$ = this.timeS.getattendancestaff(id).subscribe(data =>{
                        this.attendanceGroup.patchValue({
                            autoLogout: data.autoLogout,
                            emailMessage: data.emailMessage,
                            excludeShiftAlerts: data.excludeShiftAlerts,
                            inAppMessage: data.inAppMessage,
                            logDisplay: data.logDisplay,
                            pin: data.pin,
                            rosterPublish: data.rosterPublish,
                            shiftChange: data.shiftChange,
                            smsMessage: data.smsMessage
                        },{
                            emitEvent:false
                        });

                        this.loading = false;
                        this.loadResult = true;
                    })
                    break;
                case 15: 
                    this.dataSub$ = this.timeS.getstaffpositions(this.user.id).subscribe(data =>{
                        this.tableData = data;
                    })

                    this.dataSub$ = this.listS.getlistpositions().subscribe(data => this.listpositions = data)

                    this.loading = false;
                    this.loadResult = true;
                    break;
                case 16:

                    this.dataSub$ = forkJoin([
                        this.timeS.getuserdefined1(this.user.id),
                        this.timeS.getuserdefined2(this.user.id),
                        this.listS.getlistuserdefined1(),
                        this.listS.getlistuserdefined2()
                    ]).subscribe(data => {
                        this.userdefined1 = data[0];
                        this.userdefined2 = data[1];

                        this.userdefined1List = data[2];
                        this.userdefined2List = data[3];

                        
                        this.loading = false;
                        this.loadResult = true;
                    });

                    
                    break;
            }
            this.tab = data;
        })      

        this.resetGroups();
    }

    staffRecordView: string

    ngOnInit(){

        this.token = this.globalS.decode();

        if(!this.token){
            var uname = prompt("Please enter your name", "");
            var passwd = prompt("Please enter your password", "");            
    
            let user: Dto.ApplicationUser = {
                Username: uname,
                Password: passwd
            }
    
            this.logS.login(user).pipe(
                takeUntil(this.unsubscribe$),
                switchMap(x => {
                    this.globalS.token = x.access_token;
                    this.token = this.globalS.decode();
                    return this.staffS.getstaffrecordview(this.token.user)
                })
            ).subscribe(data => {  
                this.loadStaff = false;
                this.staffRecordView = data.viewString;               
            });
    
            return;
        } else {
            this.staffS.getstaffrecordview(this.token.user)
                .subscribe(data => {
                    this.loadStaff = false;
                    this.staffRecordView = data.viewString;
                })
        }

       
    }

    generateAccount(){        
        var _account = this.newStaffGroup.get('surnameOrg').value + ' ' + 
                (this.newStaffGroup.get('firstName').value.length > 0 ? this.newStaffGroup.get('firstName').value[0] : '');
        
        return _account || '';
    }

    resetModal(){
        this.newStaffModal.reset();
    }

    newStaffReset(){

        this.newStaffGroup = this.formBuilder.group({
            type: '',
            accountNo: '',
            individualOrg: 'Individual',
            surnameOrg: '',
            firstName: '',
            gender: '',    
            birthDate: '',
            commencementDate: '',
            branch:'',
            jobCategory: '',
            manager: '',
            addressForm: this.formBuilder.array([]),
            contactForm: this.formBuilder.array([]),
            confirmation: new FormControl('false')
        })



        this.newStaffGroup.get('surnameOrg').valueChanges
        .pipe(
            distinctUntilChanged(),
            debounceTime(500)
        ).subscribe(data => {

        this.newStaffGroup.patchValue({ 
            accountNo: this.generateAccount()
        });

      

        })

        this.newStaffGroup.get('firstName').valueChanges.pipe(
            distinctUntilChanged(),
            debounceTime(500)
        ).subscribe(data => {
            this.newStaffGroup.patchValue({ 
                accountNo: this.generateAccount()
            })
        })

        this.newStaffGroup.get('accountNo').valueChanges.pipe(           
            mergeMap(x => {
                var data = x || '';
                return this.staffS.isAccountNoUnique(data)
            })
        ).subscribe(data => {
            if(data){              
                this.newStaffGroup.patchValue({ 
                    confirmation: true
                })  
                
            } else {
                this.newStaffGroup.patchValue({ 
                    confirmation: false
                })                
            }
        })

    }

    resetGroups(){      

        this.newStaffReset();

        this.payGroup = this.formBuilder.group({
            accountNo: "",
            award: "",
            awardLevel: "",
            cH_1_1: "",
            cH_1_2: "",
            cH_1_3: "",
            cH_1_4: "",
            cH_1_5: "",
            cH_1_6: "",
            cH_1_7: "",
            cH_2_1: "",
            cH_2_2: "",
            cH_2_3: "",
            cH_2_4: "",
            cH_2_5: "",
            cH_2_6: "",
            cH_2_7: "",
            caldStatus: "",
            caseManager: false,
            category: "",
            commencementDate: "",
            contactIssues: "",
            cstda_DisabilityGroup: "",
            cstda_Indiginous: "",
            cstda_OtherDisabilities: null,
            dLicence: "",
            dob: "",
            emailTimesheet: false,
            employeeOf: "",
            excludeClientAdminFromPay: false,
            excludeFromPayExport: false,
            filePhoto: "",
            firstName: "",
            gender: "",
            hrS_DAILY_MAX: "",
            hrS_DAILY_MIN: "",
            hrS_FNIGHTLY_MAX: "",
            hrS_FNIGHTLY_MIN: "",
            hrS_WEEKLY_MAX: "",
            hrS_WEEKLY_MIN: "",
            includeLaundry: false,
            includeUniform: false,
            isRosterable: false,
            jobFTE: null,
            jobStatus: "",
            jobTitle: "",
            jobWeighting: null,
            lastName: "",
            middleNames: "",
            nRegistration: "",
            pan_Manager: "",
            payGroup: "",
            preferredName: "",
            publicHolidayRegion: "",
            rating: "",
            serviceRegion: "",
            sqlid: null,
            stF_CODE: "",
            staffGroup: "",
            staffTeam: "",
            stf_Code: "",
            stf_Department: "",
            subCategory: "",
            superFund: "",
            superPercent: "",
            terminationDate: null,
            title: "",
            ubdMap: "",
            uniqueID: "",
            vRegistration: "",
        })

        this.attendanceGroup = this.formBuilder.group({
            autoLogout: [''],
            emailMessage: false,
            excludeShiftAlerts: false,
            inAppMessage: false,
            logDisplay: false,
            pin: [''],
            rosterPublish: false,
            shiftChange: false,
            smsMessage: false
        });

        this.kindetailsGroup = this.formBuilder.group({
            listOrder: [''],
            type: [''],
            name: [''],
            email: [''],
            address1: [''],
            address2: [''],
            suburbcode: [''],
            suburb: [''],
            postcode: [''],
            phone1: [''],
            phone2: [''],
            mobile: [''],
            fax: [''],
            notes: [''],
            oni1: false,
            oni2: false,
            recordNumber: null
        });

        this.contactFormGroup = this.formBuilder.group({
            group: [''],
            listOrder: [''],
            type: [''],
            name: [''],
            email: [''],
            address1: [''],
            address2: [''],
            suburbcode: [''],
            suburb: [''],
            postcode: [''],
            phone1: [''],
            phone2: [''],
            mobile: [''],
            fax: [''],
            notes: [''],
            oni1: false,
            oni2: false,
            ecode: [''],
            creator: [''],
            recordNumber: null
        })

        this.competencyGroup = this.formBuilder.group({
            recordNumber: null,
            expiryDate:null,
            reminderDate: null,
            competency: '',
            certReg:'',
            mandatory: false,
            notes:''
        })

        this.hrGroup = this.formBuilder.group({
            notes: '',
            isPrivate: false,
            alarmDate: null,
            whocode: '',
            recordNumber: null,
            category: ''
        })

        this.opGroup = this.formBuilder.group({
            notes: '',
            isPrivate: false,
            alarmDate: null,
            whocode: '',
            recordNumber: null,
            category: ''
        })

        this.positionGroup = this.formBuilder.group({
            personID: '',
            position: '',
            startDate: null,
            endDate: null,
            positionID:'',
            notes: '',
            recordNumber: ''
        })

        this.user1Group = this.formBuilder.group({
            group: '',
            notes: '',
            recordNumber: ''
        })

        this.user2Group = this.formBuilder.group({
            preferences:'',
            notes:'',
            recordNumber: ''
        })

        this.remindersGroup = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            listOrder: '',
            followUpEmail: '',
            recurring: false,
            recurrInt: null,
            recurrStr: '',
            notes: '',
            reminderDate: null,
            dueDate: null,
            staffAlert: ''
        })

        this.leaveGroup = this.formBuilder.group({
            user:'',
            staffcode: '',

            fromDate: '',
            toDate: '',
            
            makeUnavailable: true,
            unallocAdmin: false,
            unallocUnapproved: true,
            unallocMaster: false,
           
            explanation: '',
            activityCode: '',
            payCode: '',
            program: '',
        })

        this.incidentGroup = this.formBuilder.group({
            personId: '',
            service: '',
            program: '',
            type: '',
            perpSpecify: '',
            location: '',
            setting: '',
            locationNotes: '',

            dateReported: '',
            date: '',
            time: new FormControl('06:00'),

            staffs: this.formBuilder.array([]),

            duration: '',
            reportedBy: '',
            severity: '',
            currentAssignee: '',
            shortDesc: '',
            fullDesc: '',
            triggerShort: '',
            triggers: '',
            initialNotes:'',
            ongoingNotes: '',
        })

        this.terminateGroup = this.formBuilder.group({
            terminateDate: ''
        });


        this.recurringChangeEvent(false);
        this.newStaffReset();

        this.leaveGroup.valueChanges.pipe(debounceTime(200)).subscribe(e =>{
            console.log(e)
            const { activityCode, payCode, fromDate, toDate } = this.leaveGroup.value;

            if(moment(toDate).isBefore(moment(fromDate))){
                this.globalS.eToast('Error','Date Invalid');
                this.isLeaveSaveBtnDisable = true;
                return;
            }
    
            if( activityCode && payCode ){
                this.isLeaveSaveBtnDisable = false;
                return;
            }    
            
            this.isLeaveSaveBtnDisable = true;
        })

    }

    populateDropdowns(): void{
        forkJoin([
            this.listS.getlistbranches(),
            this.listS.getliststaffgroup(),
            this.listS.getlistcasemanagers(),
            this.clientS.getcontacttype(),
            this.clientS.getaddresstype(),
        ]).subscribe(data => {
            this.dropDowns = {
                branchesArr: data[0],
                jobCategoryArr: data[1],
                managerArr: data[2]
            }

            this.contactType = data[3].map(x => {
                return (new RemoveFirstLast().transform(x.description)).trim();
            });

            this.addressType = data[4].map(x => {
                return new RemoveFirstLast().transform(x.description)
            });
        })

        this.newStaffGroup.setControl('addressForm', this.formBuilder.array( this.addressBuilder() || []))
        this.newStaffGroup.setControl('contactForm', this.formBuilder.array( this.contactBuilder() || []))
    }

    staffBuilder(data: any): Array<FormGroup> {

        var groupArr: Array<FormGroup> = [];
        groupArr = data.map(x => {
            return new FormGroup({
                selected: new FormControl(x.selected),
                accountNo: new FormControl(x.accountNo)
            })
        })
        return groupArr;

    }

    addressBuilder(): Array<FormGroup>{
        var groupArr: Array<FormGroup> = [];

        groupArr =[ new FormGroup({
            description: new FormControl('RESIDENTIAL'),
            address: new FormControl(''),
            pcode: new FormControl('')
        }),new FormGroup({
            description: new FormControl('POSTAL'),
            address: new FormControl(''),
            pcode: new FormControl('')
        })]
        return groupArr;
    }

    contactBuilder(): Array<FormGroup>{
        var groupArr: Array<FormGroup> = [];
            groupArr =[ new FormGroup({
                type: new FormControl('HOME'),
                detail: new FormControl('')
            }),new FormGroup({
                type: new FormControl('MOBILE'),
                detail: new FormControl('')
            })]

        return groupArr;
    }

    get typeValue(): string {
        return this.newStaffGroup.get('type').value;
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }    

    ngAfterContentInit(){

       
        // this.attendanceGroup.get('emailMessage').valueChanges.subscribe(data => console.log(data))

        // merge(    
        //     this.attendanceGroup.get('emailMessage').valueChanges,
        //     this.attendanceGroup.get('excludeShiftAlerts').valueChanges,     
        //     this.attendanceGroup.get('inAppMessage').valueChanges,
        //     this.attendanceGroup.get('logDisplay').valueChanges, 
        //     this.attendanceGroup.get('rosterPublish').valueChanges,
        //     this.attendanceGroup.get('shiftChange').valueChanges,
        //     this.attendanceGroup.get('smsMessage').valueChanges,   
        // ).pipe(takeUntil(this.unsubscribe$))
        // .subscribe(() => {
        //     this.updateCheckBoxTimeAndAttendance();
        // })
    }

    updateCheckBoxTimeAndAttendance(){
        const group = this.attendanceGroup;
        this.timeS.updatetimeandattendance({
            AutoLogout: group.get('autoLogout').value,
            EmailMessage: group.get('emailMessage').value,
            ExcludeShiftAlerts: group.get('excludeShiftAlerts').value,
            InAppMessage: group.get('inAppMessage').value,
            LogDisplay: group.get('logDisplay').value,
            Pin: group.get('pin').value,
            RosterPublish: group.get('rosterPublish').value,
            ShiftChange: group.get('shiftChange').value,
            SmsMessage: group.get('smsMessage').value,
            Id: this.user.id
        }).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success','Change successful');
                this.attendanceGroup.markAsPristine();
                return;
            }
            this.globalS.eToast('Error','Changes not saved')
        })
    }

    onEnter(){
        return false;
        //this.updateCheckBoxTimeAndAttendance();
    }

    onKeyPress(data: KeyboardEvent) {
        if (data.key.length == 1 && /^[a-z.,]$/i.test(data.key)) {
            return false;
        }
        return true;
    }

    /**
     * Contacts
     */

    addcontactskin(){
        if(this.contactFormGroup.controls['suburbcode'].dirty){
            var rs = this.contactFormGroup.get('suburbcode').value;
            let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0].trim() : "";
            let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0].trim() : "";

            if(pcode !== ""){   
                this.contactFormGroup.controls["postcode"].setValue(pcode);
                this.contactFormGroup.controls["suburb"].setValue(suburb);
            }
        }

        if(this.contactFormGroup.get('oni1').value){
            this.contactFormGroup.controls['ecode'].setValue('PERSON1')
        } else if (this.contactFormGroup.get('oni2').value){
            this.contactFormGroup.controls['ecode'].setValue('PERSON2')
        }       

        this.timeS.postcontactskinstaffdetails(
            this.contactFormGroup.value,
            this.user.id
        ).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success','Contact Inserted');
                this.changeTab.next(2);
                return;
            }
            this.globalS.eToast('Error','Process Not Completed')
        })
        this.addcontactskinreset();
    }

    addcontactskinreset(): void {
        this.addcontactskinModal.reset();
        this.resetGroups();
    }

    deletestaffkin(staff: any){
        this.timeS.deletecontactskin(staff.recordNumber).pipe(
            takeUntil(this.unsubscribe$))
            .subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success','Contact Deleted');
                    this.changeTab.next(2);
                    return;
                }
                this.globalS.eToast('Error','Process Not Completed')
            })
    }

    updateStaffModalOpen(data: any){
        this.updateStaffModal.open();
        this.timeS.getcontactskinstaffdetails(data.recordNumber).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data => {
            this.kindetailsGroup.patchValue({
                address1: data.address1,
                address2: data.address2,
                name: data.contactName,
                type: data.contactType,
                email: data.email,
                fax: data.fax,
                mobile: data.mobile,
                notes: data.notes,
                phone1: data.phone1,
                phone2: data.phone2,
                suburbcode: (data.postcode).trim()+ ' ' + (data.suburb).trim(),
                suburb: data.suburb,
                postcode: data.postcode,
                listOrder: data.state,
                oni1: (data.equipmentCode || '').toUpperCase() == 'PERSON1',
                oni2: (data.equipmentCode || '').toUpperCase() == 'PERSON2',
                recordNumber: data.recordNumber
            })        
        })
    }
    
    updatestaffkin(){
        if(this.kindetailsGroup.dirty){
            var rs = this.kindetailsGroup.get('suburbcode').value;

            let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0] : "";
            let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0] : "";

            // Get Postcode and Suburb Values
            this.contactFormGroup.controls["postcode"].setValue(pcode);
            this.contactFormGroup.controls["suburb"].setValue(suburb);

            this.timeS.updatecontactskinstaffdetails( this.kindetailsGroup.value , this.kindetailsGroup.value.recordNumber).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data => {
                    if(data){
                        this.globalS.sToast('Success','Changes saved');
                        this.updateStaffModal.close();
                        this.changeTab.next(2);
                        return;
                    }
                    this.globalS.eToast('Error Update','Changes not saved')
                });                
            return;
        }

            
    }

    /** */


    /**
     *  Competencies
     */

    updateStaffCompetenciesModalOpen(data: any){
        this.updateStaffCompetenciesModal.open()
        this.competencyGroup.patchValue(data)
    }

    addCompetenciesOpenChange(){
        this.addCompetenciesOpen = true;
        this.addcompetenciesreset();
    }

    updatecompetencies(){
        if(this.competencyGroup.dirty){
            const competency = this.competencyGroup.value;
            this.timeS.updatecompetency(competency, competency.recordNumber).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                if(data){
                    this.globalS.sToast('Success','Changes saved');
                    this.updateStaffCompetenciesModal.close();
                    this.changeTab.next(9);
                    return;
                }
                this.globalS.eToast('Error Update','Changes not saved')
            })
        }
    }

    addcompetencies(){
        this.timeS.postcompetencies(this.competencyGroup.value, this.user.id).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success','Competency saved');
                this.changeTab.next(9);
                return;
            }
            this.globalS.eToast('Error','Changes not saved')
        })
        this.addcompetenciesreset();
    }

    addcompetenciesreset(){
        this.addCompetenciesModal.reset();
        this.competencyGroup.reset({
            recordNumber: '',
            expiryDate: null,
            reminderDate:null,
            competency:'',
            certReg:'',
            mandatory: false,
            notes:''
        });
    }

    deleteCompetency(data: any){
        this.timeS.deletecompetency(data.recordNumber).pipe(takeUntil(this.unsubscribe$)).subscribe(data =>{
            if (data) {
                this.globalS.sToast('Success','Competency Deleted');
                this.changeTab.next(9);
                return;
            }
            this.globalS.eToast('Error','Process Not Completed')
        })
    }

    /** */

    /**
     *  HR Notes
     */

    deletehrnote(data: any){
        this.timeS.deletehrnotes(data.recordNumber).pipe(takeUntil(this.unsubscribe$)).subscribe(data =>{
            if (data) {
                this.globalS.sToast('Success','Note Deleted');
                this.changeTab.next(8);
                return;
            }
            this.globalS.eToast('Error','Process Not Completed')
        })
    }

    hrNoteProcess(){

        const { alarmDate } = this.hrGroup.value;

        this.hrGroup.patchValue({
            alarmDate: moment(alarmDate).format()
        })

        if(this.dbAction == 0){
            this.hrGroup.controls["whocode"].setValue(this.user.code);
            this.timeS.posthrnotes(this.hrGroup.value, this.user.id).pipe(
                takeUntil(this.unsubscribe$)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success','Note saved');
                        this.changeTab.next(8);
                        return;
                    }
                    this.globalS.eToast('Error','Changes not saved')
                })
            this.addhrnoteModal.close();
        }

        if(this.dbAction == 1){
            
            this.timeS.updatehrnotes(this.hrGroup.value, this.hrGroup.value.recordNumber).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data =>{
                if (data) {
                    this.globalS.sToast('Success','Note updated');
                    this.changeTab.next(8);
                    return;
                }
                this.globalS.eToast('Error','Changes not saved')
            })
        }
    }

    updatehrmodal(data: any){
        this.addhrnoteModal.open();
        this.hrGroup.patchValue({
            alarmDate: data.alarmDate,
            category: data.category,
            isPrivate: data.isPrivate,
            recordNumber: data.recordNumber,
            notes: data.detail
        })
    }

    /** */


    /**
     *  OP Notes
     */

    opNoteProcess(){
        if(this.dbAction == 0){
            this.opGroup.controls["whocode"].setValue(this.user.code);        
            this.timeS.postopnote(this.opGroup.value, this.user.id).pipe(
                takeUntil(this.unsubscribe$)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success','Note saved');
                        this.changeTab.next(7);
                        return;
                    }
                    this.globalS.eToast('Error','Changes not saved')
                })
        }

        if(this.dbAction == 1){
            this.opGroup.controls["alarmDate"].setValue(moment(this.opGroup.value.alarmDate).format())
            this.timeS.updateopnote(this.opGroup.value, this.opGroup.value.recordNumber).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data =>{
                if (data) {
                    this.globalS.sToast('Success','Note updated');
                    this.changeTab.next(7);
                    return;
                }
                this.globalS.eToast('Error','Changes not saved')
            })
        }
    }


    updateopnote(data:any){
        this.addopnoteModal.open();

        this.opGroup.patchValue({
            notes: data.detail,
            category: data.category,
            isPrivate: data.isPrivate,
            alarmDate: data.alarmDate,
            recordNumber: data.recordNumber
        })

    }

    deleteopnote(id: number){
        this.timeS.deleteopnote(id).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success','Note Deleted');
                    this.changeTab.next(7);
                    return;
                }
                this.globalS.eToast('Error','Process Not Completed')
            })
    }
    
    /** */

    /**
     *  Position
     */

    positionProcess(){
        if(this.dbAction == 0){
            this.positionGroup.controls["personID"].setValue(this.user.id);
            this.timeS.poststaffpositions(this.positionGroup.value).pipe(
                        takeUntil(this.unsubscribe$)).subscribe(data => {
                            if (data) {
                                this.globalS.sToast('Success','Position Added');
                                this.changeTab.next(15);
                                return;
                            }
                            this.globalS.eToast('Error','Process Not Completed')
                        })
        }

        if (this.dbAction == 1) {

            this.timeS.updatestaffpositions(this.positionGroup.value, this.positionGroup.value.recordNumber).pipe(
                takeUntil(this.unsubscribe$)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success','Position Updated');
                        this.changeTab.next(15);
                        return;
                    }
                    this.globalS.eToast('Error','Process Not Completed')
                })
        }
    }

    updatePosition(data: any){
        this.resetGroups();
        this.addpositionModal.open();

        this.positionGroup.patchValue({
            recordNumber: data.recordNumber,
            personID: data.personID,
            position: data.position,
            startDate: data.startDate,
            endDate: data.endDate,
            positionID:data.positionID,
            notes: data.notes
        })
        
    }

    deletePosition({ recordNumber}){        
        this.timeS.deletestaffpositions(recordNumber).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            if (data) {
                this.globalS.sToast('Success','Position Deleted');
                this.changeTab.next(15);
                return;
            }
            this.globalS.eToast('Error','Process Not Completed')
        })
    }
    /** */

    picked(data: any){
        this.user = {
            code: data.accountNo,
            id: data.uniqueID,
            view: 'staff',
            sysmgr: true
        }
        console.log(this.user)
        this.changeTab.next(this.tab);
    }

    getUserData(code: any){
        return forkJoin([
            this.staffS.getaddress(code),
            this.staffS.getcontacts(code)
        ]);
    }

    editLeave(data: any){

        const currentDate = moment();                
        const startDate = moment(data.startDate);
        const endDate = moment(data.endDate);

        if((startDate.diff(currentDate, 'days',true) > 0 && endDate.diff(currentDate, 'days',true) > 0 ) 
                || (startDate.diff(currentDate, 'days',true) < 0 && endDate.diff(currentDate, 'days',true) > 0)) {   
            this.leaveApplication.open();

            this.leaveGroup.patchValue({
                fromDate: data.startDate,
                toDate: data.endDate,
                makeUnavailable: data.makeUnavailable,
                unallocAdmin: data.unallocAdmin,
                unallocUnapproved: data.unallocUnapproved,
                unallocMaster: data.unallocMaster,
                explanation: data.leaveType,
                activityCode: data.address2,
                payCode: data.address1,
                program: data.suburb
            });

            if(data.suburb !=''){
                this.fp_hide = true;
            } else{
                this.fp_hide = false;
            }

            return;
        }

        this.globalS.eToast('Error','Either both dates are forthcoming or just the end date')

        
    }

    deleteLeave(data: any){

        const currentDate = moment();
        const startDate = moment(data.startDate);
        const endDate = moment(data.endDate);

        if(startDate.diff(currentDate, 'days') > 0 && endDate.diff(currentDate, 'days') > 0){
            this.timeS.deleteleaveapplication(data.recordNumber)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(data => {
                if (data) {
                    this.changeTab.next(this.tab)
                    this.globalS.sToast('Success','Position Deleted');
                }
            });
            return;
        }

        this.globalS.eToast('Error','Cannot delete prior dates');
    }

    clear(){
        this.tableData = null;
        this.notes = null;
    }


    /**
     *     CRUD Operations 
     */

    notesControl(res: any){
        console.log(res);
    }

    updateMiscNote(res: any){
        if(this.globalS.isEmpty(this.notes.miscellaneous.trim())){
            this.globalS.eToast('String Empty', 'Note required')
            return false;
        }

        this.timeS.updatemiscellaneous({ 
            Note: this.notes.miscellaneous.trim(), 
            UniqueId: this.user.id
        }).pipe(  takeUntil(this.unsubscribe$))
            .subscribe(res => {
            if(res){
                this.globalS.sToast('Success','Note Updated')
            } else {
                this.globalS.eToast('Error','An error occured')
            }
        })
    }

    oniProcess(data: any, source: string, group: FormGroup){
        group.patchValue({
            oni1: (data && source == 'oni1') ? true : false,
            oni2: (data && source == 'oni2') ? true : false
        });
    }

    reloadTab(pageNo: number){
        this.changeTab.next(pageNo);
    }

    downloadFile(doc: any){

        this.uploadS.download({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).pipe(takeUntil(this.unsubscribe$)).subscribe(blob => {

            let data = window.URL.createObjectURL(blob);      
            let link = document.createElement('a');
            link.href = data;
            link.download = doc.filename;

            link.click();
            setTimeout(() =>
            {
            // For Firefox it is necessary to delay revoking the ObjectURL
            window.URL.revokeObjectURL(data);
            }, 100);
        })
    }

    deleteFile(data: any){
        this.uploadS.delete(this.token.uniqueID,{ 
            id: data.docID,
            filename: data.filename
        }).pipe(  takeUntil(this.unsubscribe$))
            .subscribe(data => {
            if(data){
                this.globalS.sToast('Success','File deleted')
                return;
            }
        })
        //this.uploadS.delete({ 
        //    id: data.docID,
        //    filename: data.filename
        //}).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        //    if(data){
        //        this.reloadTab(12);
        //        this.globalS.sToast('Success','File deleted')
        //        return;
        //    }
        //})
    }

    openEditModalUserDefined1(data: any){
        this.UserDefinedGroup1Modal.open();
        this.user1Group.patchValue({
            recordNumber:  data.recordNumber,
            group: data.group,
            notes: data.notes
        });

    }

    openEditModalUserDefined2({ notes, preference, recordNumber}: any){
        this.UserDefinedGroup2Modal.open();
        this.user2Group.patchValue({
            notes: notes,
            preferences: preference,
            recordNumber: recordNumber
        })
    }

    userDefinedSave(){
        const { group, notes, recordNumber } = this.user1Group.value
        if(this.dbAction == 0){
            this.timeS.postuserdefined1({
                notes: notes,
                group: group,
                personID: this.user.id
            }).pipe(  takeUntil(this.unsubscribe$))
                .subscribe(data => {
                if(data){
                    this.reloadTab(16);
                    this.globalS.sToast('Success','Group Added')
                }
            })
        }

        if(this.dbAction == 1){           
            this.timeS.updateshareduserdefined({ group, notes, recordNumber}).pipe(
                        takeUntil(this.unsubscribe$))
                        .subscribe(data => {
                            if(data){
                                this.reloadTab(16);
                                this.globalS.sToast('Success','Group Deleted')
                            }
                        })
        }       
       
    }

    userDefined2Save(){
        const { preferences, notes, recordNumber } = this.user2Group.value

        if(this.dbAction == 0){
            this.timeS.postuserdefined2({
                notes: notes,
                group: preferences,
                personID: this.user.id
            }).pipe(  takeUntil(this.unsubscribe$))
                .subscribe(data => {
                if(data){
                    this.reloadTab(16);
                    this.globalS.sToast('Success','Group Added')
                }
            })
        }

        if(this.dbAction == 1){           
            this.timeS.updateshareduserdefined({ group: preferences, notes, recordNumber}).pipe(
                    takeUntil(this.unsubscribe$))
                        .subscribe(data => {
                            if(data){
                                this.reloadTab(16);
                                this.globalS.sToast('Success','Group Deleted')
                            }
                        })
        }       
    }

    deleteSharedUserDefined({ recordNumber }: any){
        this.timeS.deleteshareduserdefined(recordNumber).subscribe(data => {
            if(data){
                this.reloadTab(16);
                this.globalS.sToast('Success','Group Deleted')
            }
        })        
    }

    /** Reminders */

    recurringChangeEvent(event: any){
        if(!event){
            this.remindersGroup.controls['recurrInt'].setValue('')
            this.remindersGroup.controls['recurrStr'].setValue('')
            this.remindersGroup.controls['recurrStr'].disable()
            this.remindersGroup.controls['recurrInt'].disable()
        } else {
            this.remindersGroup.controls['recurrStr'].enable()
            this.remindersGroup.controls['recurrInt'].enable()
        }
    }

    reminderProcess(){
        const remGroup = this.remindersGroup.value;
  
        const reminder: Dto.Reminders = {
            recordNumber: remGroup.recordNumber,
            personID: this.user.id,
            name: remGroup.staffAlert,
            address1: remGroup.recurring ? remGroup.recurrInt : '',
            address2: remGroup.recurring ? remGroup.recurrStr : '',
            email: remGroup.followUpEmail,
            date1: GlobalService.filterDate(remGroup.reminderDate),
            date2: GlobalService.filterDate(remGroup.dueDate),
            state: remGroup.listOrder,
            notes: remGroup.notes,
            recurring: remGroup.recurring
        }
        if(this.dbAction == 0){
            this.timeS.postreminders(reminder).pipe(
                        takeUntil(this.unsubscribe$))
                        .subscribe(data => {
                            if(data){
                                this.reloadTab(6)
                                this.globalS.sToast('Success','Reminder Added')
                            }
                        })
        }

        if(this.dbAction == 1){    
            this.timeS.updatereminders(reminder).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                if(data){
                    this.reloadTab(6)
                    this.globalS.sToast('Success','Reminder Added')
                }
            })
        }
    }

    deletereminder({recordNumber}:any){
        this.timeS.deletereminders(recordNumber).pipe(
            takeUntil(this.unsubscribe$))
            .subscribe(data => {
                if(data){
                    this.reloadTab(6)
                    this.globalS.sToast('Success','Reminder Dleted')
                }
            })
    }

    updateReminderModal({ recordNumber, personID, alert, reminderDate, dueDate, address1, address2, recurring, state, email, notes} : any){

        this.addRemindersModal.open();
        this.remindersGroup.patchValue({
            recordNumber: recordNumber,
            personID: personID,
            staffAlert: alert,
            reminderDate: reminderDate,
            dueDate: dueDate,
            recurrInt: address1,
            recurrStr: address2,
            recurring: recurring,
            listOrder: state,
            followUpEmail: email,
            notes: notes
        })

        this.recurringChangeEvent(this.remindersGroup.value.recurring);
    }

    /** */

    /** Pay */

    refreshPayGroup(data: any): void{
        this.globalS.sToast('Success', 'Data Updated')
        this.payGroup.reset()
        this.payGroup.patchValue(data)
    }

    updateCommencement(){
        const { commencementDate, terminationDate, publicHolidayRegion } = this.payGroup.value;

        this.payGroup.patchValue({
            commencementDate: moment(commencementDate).format(),
            terminationDate: moment(terminationDate).format(),
        });

        this.timeS.updatecommencement({
            commencementDate: commencementDate,
            terminationDate: terminationDate,
            publicHolidayRegion: publicHolidayRegion,
            UniqueID: this.user.id
        }).pipe(
                    takeUntil(this.unsubscribe$))
                    .subscribe(data =>{
                        if(data.result){
                            this.refreshPayGroup(data.data)
                        }
                    })
    }

    updatePositionDetails(){
        this.timeS.updateposition(this.payGroup.value).pipe(
                    takeUntil(this.unsubscribe$))
                    .subscribe(data => {
                        if(data.result){
                            this.refreshPayGroup(data.data)
                        }
                    })
    }

    updatePayroll(){
        this.timeS.updatepayroll(this.payGroup.value).pipe(
                    takeUntil(this.unsubscribe$))
                    .subscribe(data => {
                        if(data.result){
                            this.refreshPayGroup(data.data)
                        }
                    })
    }

    updateWorkHours(){
        this.timeS.updateworkhours(this.payGroup.value).pipe(
                    takeUntil(this.unsubscribe$))
                    .subscribe(data => {
                        if(data.result){
                            this.refreshPayGroup(data.data)
                        }
                    })
    }

    /** */

    saveNewStaffProfile(){
       

        const {
            type,               //category
            surnameOrg,         //lastname
            firstName,          //firstname
            gender,             //gender
            jobCategory,        //job category
            manager,            //manager
            branch,             //branch
            commencementDate,   //commencement date
            birthDate,          //birthdate
            accountNo
        } = this.newStaffGroup.value;



        var addressList = (this.newStaffGroup.value.addressForm).map(x => {
            let pcode = /(\d+)/g.test(x.pcode) ? x.pcode.match(/(\d+)/g)[0] : "";
            let suburb = /(\D+)/g.test(x.pcode) ? x.pcode.match(/(\D+)/g)[0] : "";

            if(!_.isEmpty(x.description) && !_.isEmpty(x.address) && !_.isEmpty(suburb) && !_.isEmpty(pcode)){
                return {
                    personID: '',
                    description: x.description,
                    address1: x.address,
                    suburb: suburb,
                    postcode: pcode
                }
            }
        }).filter(x => x)
        
        var contactList = (this.newStaffGroup.value.contactForm).map(x => {
            if( !_.isEmpty(x.detail) && !_.isEmpty(x.type) )
            {
                return {
                    detail: x.detail,
                    type: x.type,
                    personID: ''
                }
            }
        }).filter(x => x);


        this.staffS.poststaffprofile({
            Staff: {
                accountNo: accountNo,
                firstName: firstName,
                lastName: surnameOrg,
                gender: gender,
                dob: birthDate ? moment(birthDate).format() : null,
                category: type,
    
                commencementDate: commencementDate ? moment(commencementDate).format() : null,
                stf_Department: branch,
                staffGroup: jobCategory,
                pan_Manager: manager
            },
            NamesAndAddresses: addressList,
            PhoneFaxOther: contactList
        }).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Staff Added')
            }
        });
    }

    /**
     *  Incidents
     */

    addIncidentOpenChange(){
        this.loadIncidentInfos()
            .subscribe(data => {     
                this.incident_dd = {
                    activities: data[0],
                    programs: data[1],
                    incidentType: data[2],
                    staffs: data[3].map(x => {
                        return {
                            selected: false,
                            accountNo: x.accountNo
                        }
                    }),                
                    locationCategories: data[4],
                    location: data[5]
                }
            });

        this.addincidentreset();
        this.newIncidentOpen = true;       
    }

    addincidentreset(){
        this.addIncidentModal.reset();
        this.resetGroups();
    }


    loadIncidentInfos(): Observable<any>{

        let momentDate = this.globalS.getStartEndCurrentMonth()
    
        let dates = {
            StartDate: momentDate.start.format('MM-DD-YYYY'),
            EndDate: momentDate.end.format('MM-DD-YYYY')
        };

        return forkJoin([
            this.listS.getactivities(),
            this.listS.getprograms(dates),
            this.listS.getwizardnote('INCIDENT TYPE'),
            this.timeS.getstaff({ User: this.globalS.decode().nameid, SearchString: '' }),
            this.listS.getimlocation(),
            this.listS.getcstdaoutlets()
        ])        
    }

    /** 
     *  Documents
     */

    documentModalOpen(){
        this.documentModal.reset();
        this.templates$ =  this.uploadS.getdocumenttemplate();
        this.documentOpen = true;
    }


    selectDocument(doc: any){
        if(doc.exists){
            this.fileObject = {
                file: doc.name,
                newFile: doc.name
            };          
            this.documentModal.next();
            return;
        }
        this.globalS.eToast('Error','File not exists')            
    }

    postDocument(){
        this.uploadS.postdocumenttemplate({
            PersonID: this.user.id,
            OriginalFileName: this.fileObject.file,
            NewFileName: this.fileObject.newFile
        }).subscribe(data => {
            if(data){
                this.changeTab.next(12);
                this.globalS.sToast('Success','Document has been added');
            }
        })
    }

    deleteDocument(data: any){
        this.uploadS.delete(this.user.id,{ 
            id: data.docID,
            filename: data.title
        }).pipe(  takeUntil(this.unsubscribe$))
            .subscribe(data => {
            if(data){
                this.changeTab.next(12);
                this.globalS.sToast('Success','File deleted')
                return;
            }
        })
    }

    urlFile: string = "";
    fileDocumentName: string = "";
    loadFile: boolean = false;


    reportTab: any;
    previewDocument(doc: any){

        this.reportTab = window.open("", "_blank");

        this.uploadS.getdocumentblob({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).subscribe(data => {
          this.openDocumentTab(data);
        }, (error: any) => {
            this.reportTab.close();
            this.globalS.eToast('Error','File not located')
        })
    }

    uploadDocumentModal(data: any){
        console.log(data)
        this.uploadDocModal.open();
    }

    openDocumentTab(data: any)
    {
        this.fileDocumentName = data.fileName;
        this.reportTab.location.href = `${data.path}`;   
        this.reportTab.focus();
    }

    /** */
    
    newIncident(){

        let staffs = (this.incident_dd.staffs).map(x => {
            if(x.selected)
               return x;
           }).filter(x => x)

        this.incidentGroup.setControl('staffs', this.formBuilder.array(this.staffBuilder(staffs) || []))

        this.incidentGroup.patchValue({
            personId: this.user.id,
            date: this.globalS.dateInput(this.incidentGroup.get('date').value),
            dateReported: this.globalS.dateInput(this.incidentGroup.get('dateReported').value)
        })
   
        this.staffS.postincidentapplication(this.incidentGroup.value)
            .subscribe(data => {
                if(data){
                    this.globalS.sToast('Succes','Incident Added');
                    this.changeTab.next(this.tab);
                }
            })
    }

    updateIncident(incident: any){

        
        forkJoin([
            this.loadIncidentInfos(),
            this.timeS.getincidentdetails(this.user.code, incident.recordNumber)
        ]).subscribe(data => {
    
            this.addincidentreset();
            this.newIncidentOpen = true;  

            const { 
                currentAssignee,
                date,
                dateReported,
                duration,
                fullDesc,
                initialNotes,
                location,
                locationNotes,
                ongoingNotes,
                perpSpecify,
                personId,
                program,
                recordNo,
                reportedBy,
                service,
                setting,
                severity,
                shortDesc,
                time,
                triggerShort,
                triggers,
                type,
                staffs
                 } = data[1]


            this.incident_dd = {
                activities: data[0][0],
                programs: data[0][1],
                incidentType: data[0][2],
                staffs: this.initializeStaffList(data[0][3], staffs),  
                locationCategories: data[0][4],
                location: data[0][5]
            }

        

            setTimeout(() => {
                this.incidentGroup.patchValue({
                    service: service,
                    personId: personId,
                    program: program,
                    type: type,
                    perpSpecify: perpSpecify,
                    location: location,
                    setting: setting,
                    locationNotes: locationNotes,
                    dateReported: dateReported,
                    date: date,
                    time: time,

                    duration: duration,
                    reportedBy: reportedBy,
                    severity: severity,
                    currentAssignee: currentAssignee,
                    shortDesc: shortDesc,
                    fullDesc: fullDesc,
                    triggerShort: triggerShort,
                    triggers: triggers,
                    initialNotes: initialNotes,
                    ongoingNotes: ongoingNotes
                })
    
            }, 600);
            
   
        }) 
    }

    initializeStaffList(staffList: Array<any>, staffs: Array<any>): Array<any>{
        var count = 0;

        return staffList.map(x => {
            if( staffs.length > count){
                var found = null;
                for(var i = 0; i < staffs.length;  i++){
                    if(x.accountNo == staffs[i].accountNo){
                        count++;
                        found = {
                            selected: true,
                            accountNo: x.accountNo
                        }
                        break;
                    }
                }

                if(found && found.selected)
                    return found;                
            }  
            return {
                selected: false,
                accountNo: x.accountNo
            }
        })
    }

    /** */

    leaveAppProcess(){
        const { fromDate, toDate, program } = this.leaveGroup.value;       
        
        if(this.dbAction == 0){
            this.leaveGroup.patchValue({
                fromDate: moment(fromDate).format('YYYY/MM/DD'),
                toDate: moment(toDate).format('YYYY/MM/DD'),
                program: this.fp_hide ? program : '',
                staffcode: this.user.code,
                user: this.token.user
            });
            const data = this.leaveGroup.value;

            console.log(data);
    
            // this.timeS.postleaveentry(data)
            //     .subscribe(data => {
            //         if(data){
            //             this.changeTab.next(this.tab);
            //             this.globalS.sToast('Success','Leave process');
            //             this.leaveApplication.close();
            //         }
            //     }, error =>{
            //         this.globalS.eToast('Error',`${error.error.message}`)
            //     })
        }

        if(this.dbAction == 1){
            this.leaveGroup.patchValue({
                fromDate: moment(fromDate).format('YYYY/MM/DD'),
                toDate: moment(toDate).format('YYYY/MM/DD'),
                program: this.fp_hide ? program : '',
                staffcode: this.user.code,
                user: this.token.user
            });
            const data = this.leaveGroup.value;
    
            this.timeS.putleaveentry(data)
                .subscribe(data => {
                    if(data){
                        this.changeTab.next(this.tab);
                        this.globalS.sToast('Success','Leave process');
                        this.leaveApplication.close();
                    }
                }, error =>{
                    this.globalS.eToast('Error',`${error.error.message}`)
                })
        }
    }

    checkBoxChange($event: any,index: number){
        console.log(this.tableData[index]);

        const { recordNumber, approved } = this.tableData[index]; 
       
        this.timeS.putleaveapproved({
            recordNumber: recordNumber,
            approve: !approved
        }).subscribe(data => {
            if(data){
                this.globalS.sToast('Success','Approve Changed');
            }
        }, error =>{
            $event.preventDefault();
        })
        

        
       
    }

    loadLeaveInfos(){
        
        let dates = {
            StartDate: '08-01-2019',
            EndDate: '08-30-2019',
            
        };

        forkJoin([
            this.listS.getpaycode(dates),
            this.listS.getprograms(dates),
            this.listS.getleaveactivitycodes(dates),
            this.listS.getleavebalances(this.user.id)
        ]).subscribe(data => {            
            this.leave_dd = {
                paycode: data[0],
                programs: data[1],
                leaveActivityCodes: data[2],
                leaveBalances: data[3]
            }
           
        })
    }

    // buildLeaveActivity(activities: Array<any>): Array<FormGroup>{        
    //     var groupArr: Array<FormGroup> = [];        
    //     groupArr = activities.map(activity => {
    //         return new FormGroup({
    //             selected: new FormControl(false),
    //             title: new FormControl(activity)
    //         });
    //     });
    //     return groupArr;
    // }

    terminateProcess(){
        const { code, id } = this.user;

        if(!(this.terminateGroup.get('terminateDate').value)){
            this.globalS.wToast('Termination Date is required','Warning');
            return;
        }
        
        this.timeS.posttermination({
            TerminationDate: moment(this.terminateGroup.get('terminateDate').value).format(),
            AccountNo: code,
            PersonID: id
        }).subscribe(data => {
            if(data)
                this.globalS.sToast('Success','Staff Terminated');
        })
    }

    leaveBalanceList: Array<any> = []

    terminateModalOpen(){
        this.terminateModal = true;
        this.listS.getleavebalances(this.user.id)
            .subscribe(data => this.leaveBalanceList = data)
    }

}
