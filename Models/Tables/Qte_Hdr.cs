using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class Qte_Hdr
    {
        [Key]
        public int RecordNumber { get; set; }
        public DateTime? Date { get; set; }

        [Column("Doc#")]
        public string DocId { get; set; }
        public int? ClientId { get; set; }
        public int? ProgramId { get; set; }
        public string Narration { get; set; }
        public int? COID { get; set; }
        public int? BRID { get; set; }
        public int? DPID { get; set; }
        public int? CPID { get; set; }
        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }
        public string Contingency { get; set; }
        public string Basis { get; set; }
        public string Contribution { get; set; }
        public string ContingencyBuildAmount { get; set; }
        public string AdminAmount_Perc { get; set;}
        public double? DaysCalc { get; set; }
        public double? Budget { get; set; }
        public string QuoteBase { get; set; }
        public string IncomeTestedFee { get; set; }
        public string GovtContribution { get; set; }
        public string PackageSupplements { get; set; }
        public string DailyCDCRate { get; set; }
        public string AgreedTopUp { get; set; }
        public string BalanceAtQuote { get; set; }
        public string CLAssessedIncomeTestedFee { get; set; }
        
        [Column("FeesAccepted", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? FeesAccepted { get; set; }
        public string BasePension { get; set; }
        public string DailyBasicCareFee { get; set; }
        public string DailyIncomeTestedFee { get; set; }
        public string DailyAgreedTopUp { get; set; }
        public string HardshipSupplement { get; set; }
        public string QuoteView { get; set; }

        [Column("XDeletedRecord", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? XDeletedRecord { get; set; }
        public DateTime? XEndDate { get; set; } 

       
        
    }
}