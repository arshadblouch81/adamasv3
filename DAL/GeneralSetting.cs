using System;
using System.Data;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Text;

using Adamas.Models;
using Adamas.Models.Dto;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using ProgramEntities = Adamas.Models.Modules.Program;
using System.Globalization;

using Adamas.Controllers;
using NPOI.SS.Formula.Functions;
using Microsoft.AspNetCore.Mvc;

namespace Adamas.DAL
{
    static class UserType
    {
        public const string Recipient = "recipient";
        public const string Staff = "staff";
    }

    public class GeneralSetting : IGeneralSetting
    {
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private IConfiguration _configuration;


        public GeneralSetting(
            ISqlDbConnection conn,
            DatabaseContext context,
            IConfiguration configuration)

        {
            _context = context;
            _conn = conn;
            _configuration = configuration;
        }

        // public static T Convert<T>(this string input){
        //     try
        //     {
        //         var converter = TypeDescriptor.GetConverter(typeof(T));
        //         if(converter != null)
        //         {
        //             // Cast ConvertFromString(string text) : object to (T)
        //             return (T)converter.ConvertFromString(input);
        //         }
        //         return default(T);
        //     }
        //     catch (NotSupportedException)
        //     {
        //         return default(T);
        //     }
        // }

        public async Task<Boolean> InsertAuditAsync(AuditDTO audit, SqlConnection conn, SqlTransaction transaction = null)
        {
     
            using (SqlCommand cmd = new SqlCommand(@"INSERT INTO Audit(Operator, ActionDate, ActionOn, WhoWhatCode, TraccsUser, AuditDescription) 
                    VALUES(@operator,@actionDate,@actionOn,@whowhatcode,@traccsuser,@description)", conn, transaction))
            {
                cmd.Parameters.AddWithValue("@operator", audit.Operator);
                cmd.Parameters.AddWithValue("@actionDate", audit.ActionDate.HasValue ? audit.ActionDate : null);
                cmd.Parameters.AddWithValue("@actionOn", audit.ActionOn);
                cmd.Parameters.AddWithValue("@whowhatcode", audit.WhoWhatCode);
                cmd.Parameters.AddWithValue("@traccsuser", audit.TraccsUser);
                cmd.Parameters.AddWithValue("@description", audit.AuditDescription);
                await cmd.ExecuteNonQueryAsync();
            }
            
            return true;
        }

        public async Task<Boolean> UpdateSysTable(string user, int counter)
        {
            using (var conn = _context.GetConnection() as SqlConnection)
            {
                await conn.OpenAsync();
                string query = string.Empty;

                if (user == UserType.Recipient) query = "UPDATE SysTable SET CustomerID = @counter";
                if (user == UserType.Staff) query = "UPDATE SysTable SET CarerID = @counter";

                using (SqlCommand cmd = new SqlCommand(query, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@counter", counter);
                    await cmd.ExecuteNonQueryAsync();

                    return await cmd.ExecuteNonQueryAsync() > 0;
                }
            }
        }

        public string DetermineMainGroup(string rosterGroup, string mainGroup) {
            string result = string.Empty;
            if (rosterGroup == "ITEM")
            {
                result = "ITEM";
            }
            else if (rosterGroup == "ADMISSION")
            {
                if (mainGroup == "PACKAGE ADMIN")
                {
                    result = "ADMIN";
                }

                if (mainGroup == "CASE MANAGEMENT")
                {
                    result = "CASE MANAGEMENT";
                }
            }
            else
            {
                result = "SERVICE";
            }
            return result;
        }


        public async Task<String> AllowedDaysString(DateTime? date){


            if(!date.HasValue) return "00000000";

            DateTime dateTime;
            var realDates = await (from holiday in _context.PublicHoliday 
                                    where DateTime.TryParse(holiday.Date, out dateTime)
                                select DateTime.Parse(holiday.Date)).ToListAsync();

            var isPublicHoliday = (from realDate in realDates 
                                where date.HasValue && realDate.Month == date.Value.Month && realDate.Day == date.Value.Day
                                    select realDate).ToList();

            if(isPublicHoliday.Count > 0)  return "00000001";
            
            var dayWeek = date.Value.DayOfWeek;

            switch(dayWeek)
            {
                case (DayOfWeek.Monday):
                    return "10000000";
                case (DayOfWeek.Tuesday):
                    return "01000000";
                case (DayOfWeek.Wednesday):
                    return "00100000";
                case (DayOfWeek.Thursday):
                    return "00010000";
                case (DayOfWeek.Friday):
                    return "00001000";                    
                case (DayOfWeek.Saturday):
                    return "00000100";
                case (DayOfWeek.Sunday):
                    return "00000010";
                default: 
                    return "00000001";
            }
        }



        public async Task<string> GetDateMaskValue(string date){
            
            StringBuilder defaultString= new StringBuilder("00000000");

            if(await IsHolidateAsync(date))
            {
                defaultString[7] = '1';
            } 
            else{
                var _final = DateBreakDownRelativeCurrentYear(date);

                switch(_final.Value.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        defaultString[0] = '1';
                        break;
                    case DayOfWeek.Tuesday:
                        defaultString[1] = '1';
                        break;
                    case DayOfWeek.Wednesday:
                        defaultString[2] = '1';
                        break;
                    case DayOfWeek.Thursday:
                        defaultString[3] = '1';
                        break;
                    case DayOfWeek.Friday:
                        defaultString[4] = '1';
                        break;
                    case DayOfWeek.Saturday:
                        defaultString[5] = '1';
                        break;
                    case DayOfWeek.Sunday:
                        defaultString[6] = '1';
                        break;
                }
            }

            return defaultString.ToString();
        }

        private DateTime? DateBreakDownRelativeCurrentYear(string date){
            var validDate = TryDateParse(date);

            if (validDate == null) {
                return null;
            } else{

                DateTime currentDate = DateTime.Now;

                int currYear = currentDate.Year;
                int currMonth = validDate.Value.Month;
                int currDay = validDate.Value.Day;

                return new DateTime(currYear, currMonth, currDay);
            }
        }

        public async Task<Boolean> IsHolidateAsync(string date){

            var validDate = TryDateParse(date);
            if(validDate == null) return false;

            using(var conn = _context.Database.GetDbConnection())
            {
                List<string> holidayList = new List<string>();
                using(SqlCommand cmd = new SqlCommand(@"SELECT CONCAT(MONTH([Date]),'/',DAY([Date])) AS DayMonth FROM PUBLIC_HOLIDAYS WHERE [Date] IS NOT NULL AND COALESCE([Date], '') <> '';",(SqlConnection) conn))
                {
                    await conn.OpenAsync();
                    using(var rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            holidayList.Add(rd[0].ToString());
                        }
                    }
                }

                return holidayList.Any(x =>
                                x == (string.Concat(validDate.Value.Month.ToString(), "/", validDate.Value.Day.ToString())
                            ));
            }
        }

        public async Task<bool> IfRecipientAccountNameExist(string accountName){
            var recipientFound = await (from recipient in _context.Recipients where recipient.AccountNo == accountName select recipient).FirstOrDefaultAsync();
            return recipientFound != null;
        }

        public async Task<RegistrationCounter> GetAccountNameGenerator(string user)
        {
            using (var conn = _context.GetConnection() as SqlConnection)
            {
                await conn.OpenAsync();

                string finalCarerId = string.Empty;
                RegistrationCounter rc = new RegistrationCounter();

                using (SqlCommand cmd = new SqlCommand(@"SELECT SYSTABLE.CustomerID, SYSTABLE.CARERID, REGISTRATION.PRODNAME FROM Registration CROSS JOIN SysTable", (SqlConnection)conn))
                {
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (await rd.ReadAsync())
                        {
                            rc.CustomerId = Convert.ToInt32(Filter(rd[0])) + 1;
                            rc.CarerId = Convert.ToInt32(Filter(rd[1])) + 1;
                            rc.ProdName = Filter(rd[2]);
                        }
                    }
                }

                if (user == UserType.Staff)
                {
                    int countRow = 0;
                    bool ifNotFound = true;

                    while (ifNotFound)
                    {
                        finalCarerId = String.Concat("S", rc.ProdName, rc.CarerId.ToString().PadLeft(8, '0'));
                        using (SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) FROM Staff WHERE [UniqueID] = @uniqueID", (SqlConnection)conn))
                        {

                            cmd.Parameters.AddWithValue("@uniqueID", finalCarerId);
                            using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync())
                                    countRow = Filter(rd[0]);

                                if (countRow == 0)
                                {
                                    ifNotFound = false;
                                    break;
                                }
                                else
                                {
                                    rc.CarerId = rc.CarerId + 1;
                                }
                            }
                        }
                    }
                    rc.UniqueId = String.Concat("S", rc.ProdName, rc.CarerId.ToString().PadLeft(8, '0'));
                }

                if (user == UserType.Recipient)
                {
                    int countRow = 0;
                    bool ifNotFound = true;

                    while (ifNotFound)
                    {
                        finalCarerId = String.Concat("T", rc.ProdName, rc.CustomerId.ToString().PadLeft(8, '0'));
                        using (SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) FROM Recipients WHERE [UniqueID] = @uniqueID", (SqlConnection)conn))
                        {
                            cmd.Parameters.AddWithValue("@uniqueID", finalCarerId);
                            using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync())
                                    countRow = Filter(rd[0]);

                                if (countRow == 0)
                                {
                                    ifNotFound = false;
                                    break;
                                }
                                else
                                {
                                    rc.CustomerId = rc.CustomerId + 1;
                                }
                            }
                        }
                    }
                    rc.UniqueId = String.Concat("T", rc.ProdName, rc.CustomerId.ToString().PadLeft(8, '0'));
                }

                return rc;
            }
        }


        public string DBLookup(string Field, string table, string where, ref string s_status)
        {
            string strValue = "", SqlStmt = "";
            using (var conn = _conn.GetConnection() as SqlConnection)
            {
                if (string.IsNullOrEmpty(@where))
                    SqlStmt = "SELECT " + Field + " FROM " + table + "";
                else
                    SqlStmt = "SELECT " + Field + " FROM " + table + " WHERE " + @where;

                using (SqlCommand cmd = new SqlCommand(SqlStmt, (SqlConnection)conn))
                {
                    conn.Open();
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        strValue = NullToStr(rd.GetValue(0).ToString());
                    }
                    return strValue;
                }
            }
        }

        public async Task<ComputeTimesheet> GetComputeTimesheet(ComputeTimeInput input)
        {
            try
            {
                using (var conn = _conn.GetConnection() as SqlConnection)
                {
                    string isCarerOrClient = "";

                    if (input.IsCarerCode)
                    {
                        isCarerOrClient = "[Carer Code]";
                    }
                    else
                    {
                        isCarerOrClient = "[Client Code]";
                    }
                    string sql = @"SELECT Sum(IsNull(KMAllowanceQty, 0)) AS KMAllowancesQty, Sum(IsNull(AllowanceQty, 0)) AS AllowanceQty, Round(Sum(IsNull(WorkedHours, 0)), 2) AS WorkedHours, Round(Sum(IsNull(PaidAsHours, 0)), 2) AS PaidAsHours, Round(Sum(IsNull(PaidAsServices, 0)), 2) AS PaidAsServices, Round(Sum(IsNull(WorkedAttributableHours, 0)), 2) AS WorkedAttributableHours, Round(Sum(IsNull(PaidQty, 0)), 2) AS PaidQty, Sum(isnull(PaidAmount, 0)) AS PaidAmount, Round(Sum(IsNull(ProvidedHours, 0)), 2) AS ProvidedHours, Round(Sum(IsNull(BilledAsHours, 0)), 2) AS BilledAsHours, Round(Sum(IsNull(BilledAsServices, 0)), 2) AS BilledAsServices, Round(Sum(IsNull(BilledQty, 0)), 2) AS BilledQty, Round(Sum(IsNull(BilledAmount, 0)), 2) AS BilledAmount FROM ( SELECT RecordNo, [Type], CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Carer Code] > '!z' AND [Type] = 9 AND ISNULL(PAY.MinorGroup, 'WORKED HOURS') NOT IN ('N/C TRAVEL BETWEEN', 'CHG TRAVEL BETWEEN', 'N/C TRAVEL WITHIN', 'CHG TRAVEL WITHIN') THEN Round(isnull([CostQty], 0), 1) END AS AllowanceQty, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Carer Code] > '!z' AND [Type] = 9 AND ISNULL(PAY.MinorGroup, 'WORKED HOURS') IN ('N/C TRAVEL BETWEEN', 'CHG TRAVEL BETWEEN', 'N/C TRAVEL WITHIN', 'CHG TRAVEL WITHIN') THEN Round(isnull([CostQty], 0), 1) END AS KMAllowanceQty, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Carer Code] > '!z' AND [Type] <> 9 THEN (r.[Duration] * 5) / 60 ELSE 0 END AS WorkedHours, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Carer Code] > '!z' AND [Type] <> 9 AND CostUnit = 'HOUR' AND ISNULL(PAY.ExcludeFromPayExport,0) = 0 THEN isnull([CostQty], 0) ELSE 0 END AS PaidAsHours, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Carer Code] > '!z' AND [Type] <> 9 AND CostUnit = 'SERVICE' AND ISNULL(PAY.ExcludeFromPayExport,0) = 0 THEN isnull([CostQty], 0) ELSE 0 END AS PaidAsServices, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Carer Code] > '!z' AND [Type] <> 9 THEN isnull([CostQty], 0) ELSE 0 END AS PaidQty, ROUND(ISNULL([CostQty], 0) * ISNULL([Unit Pay Rate], 0), 2) AS PaidAmount, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Client Code] > '!z' AND [Type] <> 9 THEN (r.[Duration] * 5) / 60 ELSE 0 END AS ProvidedHours, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Client Code] > '!z' AND [Type] <> 9 THEN (r.[Duration] * 5) / 60 ELSE 0 END AS WorkedAttributableHours, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Client Code] > '!z' AND [Type] <> 9 AND BillUnit = 'HOUR' THEN (r.[Duration] * 5) / 60 ELSE 0 END AS BilledAsHours, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Client Code] > '!z' AND [Type] <> 9 AND BillUnit = 'SERVICE' THEN isnull([BillQty], 0) ELSE 0 END AS BilledAsServices, CASE WHEN IsNull(IT.InfoOnly, 0) = 0 AND r.[Client Code] > '!z' AND [Type] <> 9 THEN isnull([BillQty], 0) ELSE 0 END AS BilledQty, ROUND(ISNULL([BillQty], 0) * ISNULL([Unit Bill Rate], 0), 2) AS BilledAmount FROM Roster r INNER JOIN ItemTypes IT ON r.[Service Type] = IT.Title AND IT.ProcessClassification = 'OUTPUT' INNER JOIN ItemTypes PAY ON r.[Service Description] = PAY.Title AND PAY.ProcessClassification = 'INPUT' 
                    WHERE " + isCarerOrClient + "= @accountName AND (IT.InfoOnly = 0 OR IT.InfoOnly IS Null) AND (r.[status] IN (2,3)) AND (Date BETWEEN @startDate AND @endDate) AND r.Type IN (2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14) ) t";
                    using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                    {
                        PayPeriod periodDate = await GetPayPeriod();
                        await conn.OpenAsync();

                    if (input.Start_Date == null || input.End_Date == null || input.Start_Date == "" || input.End_Date == ""){
                        input.Start_Date =(periodDate.Start_Date).ToString("yyyy/MM/dd");
                        input.End_Date = (periodDate.End_Date).ToString("yyyy/MM/dd");
                    }

                        cmd.Parameters.AddWithValue("@startDate", input.Start_Date );
                        cmd.Parameters.AddWithValue("@endDate",  input.End_Date );
                        cmd.Parameters.AddWithValue("@accountName", input.AccountName);

                        using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                        {
                            ComputeTimesheet tsheet = new ComputeTimesheet();
                            if (await rd.ReadAsync())
                            {
                                tsheet.KMAllowancesQty = Filter(rd["KMAllowancesQty"]);
                                tsheet.AllowanceQty = Filter(rd["AllowanceQty"]);
                                tsheet.WorkedHours = Filter(rd["WorkedHours"]);
                                tsheet.PaidAsHours = Filter(rd["PaidAsHours"]);
                                tsheet.PaidAsServices = Filter(rd["PaidAsServices"]);
                                tsheet.WorkedAttributableHours = Filter(rd["WorkedAttributableHours"]);
                                tsheet.PaidQty = Filter(rd["PaidQty"]);
                                tsheet.PaidAmount = Filter(rd["PaidAmount"]);
                                tsheet.ProvidedHours = Filter(rd["ProvidedHours"]);
                                tsheet.BilledAsHours = Filter(rd["BilledAsHours"]);
                                tsheet.BilledAsServices = Filter(rd["BilledAsServices"]);
                                tsheet.BilledQty = Filter(rd["BilledQty"]);
                                tsheet.BilledAmount = Filter(rd["BilledAmount"]);
                            }

                            return tsheet;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var sd = ex;
                throw;
            }
        }

        public async Task<PayPeriod> GetPayPeriod()
        {
            PayPeriod pay_period = new PayPeriod();
            using (var conn = _conn.GetConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select top 1 PayPeriodEndDate,s.TSheetFirstServiceDate,ISNULL(DefaultPayPeriod,14) as DefaultPayPeriod,ISNULL(DefaultPayStartDay,'Monday') 
                                                            as DefaultPayStartDay,tsheetlastservicedate From Systable s, Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    DateTime? tsheetlastservicedate = null;
                    DateTime? payPeriodEndDate = null;
                    DateTime? tsheetFirstServiceDate = null;

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            payPeriodEndDate = Convert.ToDateTime(rd["PayPeriodEndDate"]);

                            if (payPeriodEndDate.HasValue)
                                pay_period.PayPeriodEndDate = Convert.ToDateTime(payPeriodEndDate);

                            tsheetFirstServiceDate = Convert.ToDateTime(rd["TSheetFirstServiceDate"]);

                            if (!tsheetFirstServiceDate.HasValue)
                                pay_period.Start_Date = DateTime.Today;
                            else
                                pay_period.Start_Date = Convert.ToDateTime(tsheetFirstServiceDate);


                            tsheetlastservicedate = Convert.ToDateTime(rd["tsheetlastservicedate"]);

                            if (!tsheetlastservicedate.HasValue)
                                pay_period.End_Date = Convert.ToDateTime(rd["PayPeriodEndDate"]);
                            else
                                pay_period.End_Date = Convert.ToDateTime(tsheetlastservicedate);

                            pay_period.PayPeriod_Length = Convert.ToInt32(rd["DefaultPayPeriod"]);
                            pay_period.StartDay = rd["DefaultPayStartDay"] as string;
                        }
                    }
                }
            }
            return pay_period;
        }

        public async Task<List<TimeSheetRosterDto>> GetTimeSheet(GetTimesheet ts)
        {
            try
            {
                string AccountNo = ts.AccountNo;
                string personType = ts.personType;
                string s_status = ts.s_status;
                string startDate = ts.startDate;
                string endDate = ts.endDate;
                string order_by = ts.order_by;
                bool Include_Prev_Paid_Shift_Activity = ts.Include_Prev_Paid_Shift_Activity;
                bool Include_Absence = ts.Include_Absence;

                string accountNo = AccountNo;
                PayPeriod pay_prd = await GetPayPeriod(s_status);

                 string Person_Criteria = "";
                string Criteria_Prev_paid = "";
                string absence_Criteria = "";
                string s_FieldList = "";
                string s_claimed = "";
                string prev_date_Criteria = "";
                

                if (String.IsNullOrEmpty(startDate) && String.IsNullOrEmpty(endDate) && !ts.Master)
                {
                    startDate = pay_prd.Start_Date.Date.ToString();
                    endDate = pay_prd.End_Date.Date.ToString();
                }

                if (!(String.IsNullOrEmpty(ts.prev_startDate) && String.IsNullOrEmpty(ts.prev_endDate)) && !ts.Master)
                {
                    prev_date_Criteria = " OR (Date >= '" + GetSqlDate(ts.prev_startDate) + "' And Date <= '" + GetSqlDate(ts.prev_endDate) + "')";
                }


                List<TimeSheetRosterDto> TimeSheet_List = new List<TimeSheetRosterDto>();
               

                if (personType == "Staff")
                {
                    Person_Criteria = " and [Carer Code] = '" + accountNo + "'";
                    if (Include_Prev_Paid_Shift_Activity == false)
                    {
                        Criteria_Prev_paid = " And ([Roster].[Status] IN(1,2,3)) ";
                    }

                }
                else
                {
                    Person_Criteria = " and [client Code] = '" + accountNo + "'";
                }

                if (string.IsNullOrEmpty(order_by))
                {
                    order_by = " ORDER BY roster.Status, convert(datetime,Date), [Start Time]";
                }
                else
                {
                    order_by = " order by " + order_by;
                }
                if (Include_Absence == true & personType == "Staff")
                {
                    absence_Criteria = " AND Type in (2, 3,4, 5, 6, 7, 8, 9, 10, 11, 12, 15) ";
                }
                else
                {
                    absence_Criteria = " AND Type in (2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 15) ";
                }
                if (Include_Prev_Paid_Shift_Activity == true)
                {
                    if (personType == "Staff")
                    {
                        absence_Criteria = "  AND Type > 1 AND Type < 13";
                    }
                }

                s_FieldList = ",CASE " + "WHEN [Type] = 9 THEN 'N/A' " + "WHEN [Start Time] = '00:00' AND Duration = 288 THEN '24:00' " + "ELSE Convert(nvarchar(5), DateAdd(Minute, ([Duration] * 5), replace([start time],'.','')), 114) " + "END AS [End_Dur], " + "(([Duration] * 5) / 60) as DecDur ";

                s_claimed = ",ClaimedStart,ClaimedEnd,ClaimedDate,isnull(ClaimedBy,0) as ClaimedBy,editer,isnull([Date Last Mod],'') as [Date Last Mod] " + ",isnull(StartKM,0) as Start_KM,isnull(EndKM,0) as End_KM,isnull(KM,0) as KM";

                pay_prd.Start_Date = Convert.ToDateTime(startDate);
                pay_prd.End_Date = Convert.ToDateTime(endDate);

                string queryString = @"SELECT  RecordNo, Date,[DayNo],[MonthNo],[YearNo],[Client Code],[Duration], [Carer code],isnull(convert(varchar(max),Notes),'-') as Notes, CASE WHEN [Type] = 9 THEN 'N/A' ELSE replace([Start Time],'.','') END AS [Start time], CASE WHEN [Type] = 9 THEN '00:00' WHEN [Start Time] = '00:00' AND Duration = 288 THEN " + " '24:00' ELSE Convert(nvarchar(5), DateAdd(Minute, ([Duration] * 5), '00:00'), 114) END AS [Duratn], [Program], CASE WHEN [Client Code] = '!INTERNAL' THEN 'ADMINISTRATION' WHEN " + " [Client Code] = '!MULTIPLE' THEN [ServiceSetting] ELSE [Client Code] END AS [Recipient or Loctn], CASE WHEN [Carer Code] = '!INTERNAL' THEN 'ADMINISTRATION' WHEN [Carer Code] = " + "'!MULTIPLE' THEN [ServiceSetting] ELSE [Carer Code] END AS [Staff or Loctn], [Service Type] AS Activity, CASE WHEN [Carer Code] = '!INTERNAL' THEN 'N/A' WHEN [Carer Code] = " + "'!MULTIPLE' THEN 'N/A' ELSE [Service Description] END AS [Pay Type], CASE WHEN [Carer Code] = '!INTERNAL' THEN 0 WHEN [Carer Code] = '!MULTIPLE' THEN 0 WHEN [Type] = 9 THEN " + " [CostQty] WHEN CONVERT(Integer, (Duration * 5 / 60) * 100) = Convert(Integer, CostQty * 100) THEN (Duration * 5 / 60) ELSE [CostQty] END AS [Pay Qty], CASE WHEN [Carer Code] = " + "'!INTERNAL' THEN 0 WHEN [Carer Code] = '!MULTIPLE' THEN 0 ELSE [Unit Pay Rate] END AS [Pay Rate], CASE WHEN [Client Code] = '!INTERNAL' THEN 0 WHEN [Client Code] = '!MULTIPLE' " + " THEN 0 ELSE [BillQty] END AS [Bill Qty], CASE WHEN [Client Code] = '!INTERNAL' THEN 0 WHEN [Client Code] = '!MULTIPLE' THEN 0 ELSE isnull([Unit Bill Rate],0) END AS [Bill Rate], CASE " + " WHEN Roster.[Status] = 1 THEN 0 ELSE 1 END AS [DStatus], Notes, Type, InfoOnly, Roster.Status, '' as App, BillTo As [Bill To]" + ",isnull(BlockNo,0) as BlockNo,duration,Anal,serviceSetting,BillUnit,Transferred,roster.[TaxPercent],[CostUnit]" + s_FieldList + s_claimed + " FROM roster inner join ItemTypes on Roster.[Service Type] = ItemTypes.Title " + " where ((Date >= '" + GetSqlDate(pay_prd.Start_Date) + "' And Date <= '" + GetSqlDate(pay_prd.End_Date) + "') " + prev_date_Criteria + ")  " + Person_Criteria + absence_Criteria + Criteria_Prev_paid + order_by;

               // Console.WriteLine(queryString);

                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    using (SqlCommand cmd = new SqlCommand(queryString, (SqlConnection)conn))
                    {

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();

                        while (await rd.ReadAsync())
                        {
                            TimeSheetRosterDto time_sht = new TimeSheetRosterDto();
                            time_sht.recipient = new Person();

                            time_sht.recipient.AccountNo = (rd["Client Code"].ToString()).Trim();
                            time_sht.Carer_Code = (rd["Carer code"].ToString()).Trim();
                            time_sht.Client_Code = (rd["Client Code"].ToString()).Trim();
                            time_sht.RecipientLocation = rd["Recipient or Loctn"].ToString();
                            time_sht.ShiftbookNo = Convert.ToString(rd["RecordNo"]);
                            time_sht.RecordNo = Convert.ToString(rd["RecordNo"]);

                            time_sht.ActivityDate = Convert.ToDateTime(rd["Date"].ToString());
                            time_sht.Date_Value = rd["Date"].ToString();
                            time_sht.BlockNo = (float)rd["BlockNo"];
                            time_sht.DayNo =rd["DayNo"].ToString();
                            time_sht.MonthNo = rd["MonthNo"].ToString();
                            time_sht.YearNo = rd["YearNo"].ToString();
                            time_sht.Duration = rd["Duration"].ToString();
                            time_sht.InfoOnly = Convert.ToBoolean(rd["InfoOnly"]) ;// rd["InfoOnly"].toBoolean();
                            time_sht.Status = rd["status"].ToString();
                            time_sht.Type = rd["Type"].ToString();
                           
                            time_sht.Activity_Time = new TimeDuration();

                            string stat_time = rd["Start time"].ToString();

                            if (stat_time != "" && stat_time != "N/A")
                            {
                                time_sht.Activity_Time.Start_time = Convert.ToDateTime(stat_time);
                                time_sht.Activity_Time.Time_Value = stat_time;

                                time_sht.Activity_Time.Duration = (float)rd["duration"];
                                time_sht.Activity_Time.Calculate_EndTime();
                                time_sht.Activity_Time.getDuration();
                            }
                            else
                            {
                                time_sht.Activity_Time.Duration = 0;
                            }

                            time_sht.program = new ProgramEntities();
                            time_sht.program.Title = rd["program"].ToString();

                            time_sht.activity = new Activity();
                            time_sht.activity.Name = rd["Activity"].ToString();
                            time_sht.activity.InfoOnly = Filter(rd["InfoOnly"]);

                            time_sht.Recipient_staff = new Person();
                            time_sht.Recipient_staff.AccountNo = rd["Recipient or Loctn"].ToString();

                            time_sht.payType = new PayType();

                            time_sht.payType.paytype = rd["Pay Type"].ToString();
                            time_sht.pay = new Pay();

                            string qty = NullToStr(rd["Pay Qty"].ToString(), "0");
                            if (qty == "") qty = "0";
                            time_sht.pay.Quantity = Convert.ToDouble(qty);

                            string Pay_rate = NullToStr(rd["Pay Rate"].ToString(), "0");
                            if (Pay_rate == "") Pay_rate = "0";
                            time_sht.pay.Pay_Rate = Convert.ToDouble(Pay_rate);


                            string cous_unit = (rd["CostUnit"].ToString()).ToLower();
                            if (cous_unit == "hour")
                            {
                                time_sht.pay.Pay_Unit = "Hour";
                            }
                            else
                            {
                                time_sht.pay.Pay_Unit = "Service";
                            }

                            time_sht.Bill = new TimeSheetBill();

                            string BillQty = NullToStr(rd["Bill Qty"].ToString(), "0");
                            time_sht.Bill.Quantity = Convert.ToDouble(BillQty);

                            string BillRate = NullToStr(rd["Bill Rate"].ToString(), "0");
                            time_sht.Bill.Bill_Rate = Convert.ToDouble(BillRate);
                            string TaxPercent = "0";

                            TaxPercent = NullToStr(rd["TaxPercent"].ToString(), "0");
                            if (TaxPercent == "")
                                TaxPercent = "0";

                            time_sht.Bill.Tax = Convert.ToDouble(TaxPercent);

                            if (personType == "Staff")
                            {
                                time_sht.Approved = ((Convert.ToInt32(NullToStr(rd["status"].ToString(), "0")) >= 2) ? true : false);
                            }
                            else
                            {
                                time_sht.Approved = (Convert.ToInt32(rd["Status"]) > 1 ? true : false);
                            }
                            if (NullToStr((rd["BillUnit"].ToString()).ToLower()) == "hour")
                            {
                                time_sht.Bill.Pay_Unit = "Hour";
                            }
                            else
                            {
                                time_sht.Bill.Pay_Unit = "Service";
                            }

                            time_sht.BilledTo = new Person();
                            time_sht.BilledTo.AccountNo = rd["Bill To"].ToString();


                            time_sht.Note = rd["Notes"].ToString();
                            time_sht.KM = Filter(rd["KM"]);
                            time_sht.Start_KM = Filter(rd["Start_KM"]);
                            time_sht.End_KM = Filter(rd["End_KM"]);
                            time_sht.ServiceSetting = rd["serviceSetting"].ToString();

                            time_sht.Anal = rd["Anal"].ToString();
                            // time_sht.ServiceTypePortal = rd["ServiceTypePortal"].ToString();

                            string type = rd["type"].ToString();
                            if (type == null)
                            {
                                time_sht.Roster_Type = 0;

                            }
                            else
                            {

                                time_sht.Roster_Type = Convert.ToInt32(type);
                            }

                            if (rd["Status"] == null)
                            {
                                time_sht.Roster_Status = "0";
                            }
                            else
                            {
                                time_sht.Roster_Status = rd["Status"].ToString();
                            }


                            time_sht.End_Dur = NullToStr(rd["End_Dur"]);

                            string DecDur = NullToStr(rd["DecDur"].ToString());
                            time_sht.DecDur = DecDur;

                            time_sht.Editer = rd["Editer"].ToString().Trim();

                            try
                            {
                                if (rd["Date Last Mod"] != null)
                                {
                                    time_sht.Last_Modify_Date = Convert.ToDateTime(rd["Date Last Mod"]);
                                }
                            }
                            catch
                            {
                                return null;
                            }

                            if (Convert.ToInt32(rd["ClaimedBy"]) != 0)
                            {
                                time_sht.Claimed_Start = rd["ClaimedStart"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rd["ClaimedStart"]);
                                time_sht.Claimed_End = rd["ClaimedEnd"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rd["ClaimedEnd"]);
                                time_sht.ClaimDate = rd["ClaimedDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(rd["ClaimedDate"]);
                                time_sht.Claimedby = rd["ClaimedBy"].ToString();

                                time_sht.Submitted = true;
                            }
                            TimeSheet_List.Add(time_sht);
                        }
                        return TimeSheet_List;
                    }
                }
            } catch(Exception ex)
            {
                throw ex;
            }
        }


        public double GetTimeGap(TimeSheetRosterDto firstDate, TimeSheetRosterDto secondDate)
        {
            DateTime fTime = default(DateTime);
            DateTime eTime = default(DateTime);

            fTime = Convert.ToDateTime(firstDate.Activity_Time.End_Time);
            eTime = Convert.ToDateTime(secondDate.Activity_Time.Start_time);

            double gap = 0;

            if (firstDate.ActivityDate == secondDate.ActivityDate && fTime > eTime)
            {
                gap = eTime.Subtract(fTime).Minutes;
            }
            else if (eTime.Subtract(fTime).Days > 0)
            {
                var dayOverlap = eTime.Subtract(fTime).Days;
                fTime = fTime.AddDays(dayOverlap);
                gap = eTime.Subtract(fTime).Minutes;
            }
            return Math.Abs(gap);
        }

        public string ComputeBookingLeadTime(Object obj, string defaultValue = "")
        {
            int BookLeadTime = Convert.ToInt32(NullToStr(obj, defaultValue));
            string IsDayOrHour = this._configuration.GetValue<string>("BookingLeadTime") ?? "day";

            if (IsDayOrHour == "hour")
            {
                BookLeadTime = BookLeadTime / 24;
            }
            return BookLeadTime.ToString();
        }

        public string NullToStr(Object obj, string default_value = "")
        {
            string val = default_value;
            if (obj == null || string.IsNullOrEmpty(obj.ToString()) || object.ReferenceEquals(null, obj))
                return val;
            else
                return obj.ToString();
        }

        public string GetDayMask(bool b_SDay, string s_EarliestDate)
        {
            string s_status = "";
            DateTime d;
            DateTime.TryParse(s_EarliestDate, out d);

            // Warning!!! Optional parameters not supported
            string s_Date;
            bool b_PublicHoliday;
            string s_Weekday;
            string[] ip = new string[8];
            string DayMask = "";
            for (int i = 0; i <= 7; i++)
            {
                ip[i] = "0";
            }

            s_Date = d.ToString("yyyy/MM/dd");
            string res = DBLookup("DATE", "PUBLIC_HOLIDAYS", "[DATE]='" + s_Date + "'", ref s_status);

            if (res != "")
                // b_PublicHoliday = Convert.ToBoolean(res);
                b_PublicHoliday = true;
            else
                b_PublicHoliday = false;

            if (b_PublicHoliday)
            {
                ip[7] = "1";
            }
            else
            {
                s_Weekday = d.DayOfWeek.ToString().ToUpper();
                // GetWeekday(s_Date).ToUpper();
                switch (s_Weekday)
                {
                    case "MONDAY":
                        ip[0] = "1";
                        break;
                    case "TUESDAY":
                        ip[1] = "1";
                        break;
                    case "WEDNESDAY":
                        ip[2] = "1";
                        break;
                    case "THURSDAY":
                        ip[3] = "1";
                        break;
                    case "FRIDAY":
                        ip[4] = "1";
                        break;
                    case "SATURDAY":
                        ip[5] = "1";
                        break;
                    case "SUNDAY":
                        ip[6] = "1";
                        break;
                }
            }

            for (int i = 0; i <= 7; i++)
            {
                DayMask = (DayMask + ip[i]);
            }
            return DayMask;
        }

        public string CalculateEndTime(string StartTime, double DurationInMinutes, string s_Format = "")
        {
            string functionReturnValue = "";
            DateTime strDate = default(DateTime);

            if (IsDate(StartTime))
            {
                DateTime StartTime2 = Convert.ToDateTime(StartTime);

                if (!string.IsNullOrEmpty(s_Format))
                {
                    strDate = StartTime2.AddMinutes(DurationInMinutes);
                }

                functionReturnValue = strDate.TimeOfDay.ToString();

                if (functionReturnValue == "00:00")
                    functionReturnValue = "24:00";
            }

            return functionReturnValue;
        }

        public bool IsDate(Object value)
        {
            try
            {
                string strDate = value.ToString();
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }


        public bool HasColumn(SqlDataReader rd, string colName)
        {
            for (int a = 0; a < rd.FieldCount; a++)
            {
                if (rd.GetName(a).Equals(colName, StringComparison.InvariantCultureIgnoreCase)) return true;
            }
            return false;
        }

        public string GetSqlDate(string dt)
        {
            dt = dt.Substring(0, 10);
            DateTime dat = Convert.ToDateTime(dt);
            return dat.Date.Year.ToString("0000") + "/" + dat.Date.Month.ToString("00") + "/" + dat.Date.Day.ToString("00");
        }

        public bool Null2Bool(string data)
        {
            return data == "1";
        }

        public int GetBlocks(string StrTime)
        {
            int Part1 = 0;
            int Part2 = 0;
            int Part3 = 0;
            int Part4 = 0;

            string[] timeParts = null;
            //Breakdown start time and calculate startblock
            timeParts = StrTime.Split(':');
            Part1 = Convert.ToInt32(timeParts[0]);
            Part2 = Convert.ToInt32(timeParts[1]);
            Part3 = Part1 * 12;
            Part4 = Part2 / 5;
            return (Part3 + Part4);
        }

        public dynamic Filter(object input)
        {

            var type = input.GetType();
            if (type == typeof(Int16)) return (Int16?)(input == DBNull.Value ? null : input);
            if (type == typeof(Int32)) return (int?)(input == DBNull.Value ? null : input);
            if (type == typeof(DateTime)) return (DateTime?)(input == DBNull.Value ? null : input);
            if (type == typeof(string)) return (string)(input == DBNull.Value ? "" : input);
            if (type == typeof(bool)) return (bool?)(input == DBNull.Value ? false : input);
            if (input == DBNull.Value) return null;
            return input;
        }

        public dynamic FilterInput(object input)
        {
            if (input == null) return DBNull.Value;

            var type = input.GetType();

            if (type == typeof(Int16)) return Convert.ToInt16(input);
            if (type == typeof(Int32)) return Convert.ToInt32(input);
            if (type == typeof(DateTime)) return Convert.ToDateTime(input);
            if (type == typeof(string)) return input.ToString();
            if (type == typeof(bool)) return Convert.ToBoolean(input);

            return DBNull.Value;
        }

        public int TypeNumber(string RosterGroup)
        {
            string rosterUpperCase = RosterGroup.ToUpper();
            int rosterNumber = 1;

            switch (rosterUpperCase)
            {
                case "BOOKING":
                    rosterNumber = 1;
                    break;
                case "ONEONONE":
                    rosterNumber = 2;
                    break;
                case "BROKERAGE":
                    rosterNumber = 3;
                    break;
                case "LEAVEOFABSENCE":
                    rosterNumber = 4;
                    break;
                case "TRAVELTIME":
                    rosterNumber = 5;
                    break;
                case "ADMINISTRATION":
                    rosterNumber = 6;
                    break;
                case "ADMISSION":
                    rosterNumber = 7;
                    break;
                case "SLEEPOVER":
                    rosterNumber = 8;
                    break;
                case "ALLOWANCES":
                    rosterNumber = 9;
                    break;
                case "TRANSPORT":
                    rosterNumber = 10;
                    break;
                case "CENTREBASEDACTIVITY":
                    rosterNumber = 11;
                    break;
                case "GROUPACTIVITY":
                    rosterNumber = 12;
                    break;
                case "UNAVAILABILITY":
                    rosterNumber = 13;
                    break;
                case "ITEM":
                    rosterNumber = 14;
                    break;
            }
            return rosterNumber;
        }

        public async Task<string> PublishedDate()
        {
            string date = string.Empty;
            using (var conn = _conn.GetConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP 1 newdate 
                    FROM   (SELECT rsc_cycle, 
                                RIGHT(rsc_destinationend, 4) + '/' 
                                + Substring(rsc_destinationend, 4, 2) + '/' 
                                + LEFT(rsc_destinationend, 2) AS NewDate 
                            FROM   rostercopy) t 
                    WHERE  Isnull(newdate, '') <> '' 
                        AND CONVERT(VARCHAR, rsc_cycle) IN ( 'ALL', 'CYCLE 1' ) 
                    ORDER  BY newdate DESC ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        if (await rd.ReadAsync())
                        {
                            date = Filter(rd[0]);
                        }
                    }
                }
            }
            return date;
        }

        public bool SendEmail()
        {
            return true;
        }

        public async Task<string> getValue(string sqlString, List<KeyValuePair<string, string>> parameters = null)
        {
            string str_value = "";
            using (var conn = _conn.GetConnection() as SqlConnection)
            {
                var cmdString = new StringBuilder(sqlString);

                if (parameters != null)
                {
                    int len = 0;
                    cmdString.Append(" WHERE ");
                    foreach (KeyValuePair<string, string> parameter in parameters)
                    {
                        if (len != 0)
                        {
                            cmdString.Append("AND");
                        }

                        cmdString.AppendFormat(" [{0}]= @{1} ", parameter.Key, len);
                        len++;
                    }
                }

                using (SqlCommand cmd = new SqlCommand(cmdString.ToString(), (SqlConnection)conn))
                {
                    if (parameters != null)
                    {
                        int cmdlen = 0;
                        foreach (KeyValuePair<string, string> parameter in parameters)
                        {
                            cmd.Parameters.AddWithValue("@" + cmdlen, parameter.Value);
                            cmdlen++;
                        }
                    }
                    await conn.OpenAsync();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        if (await rd.ReadAsync())
                        {
                            str_value = Convert.ToString(rd.GetValue(0));
                        }
                    }
                }
            }
            return str_value;
        }

        public DateTime? TryDateParse(string text) =>
            DateTime.TryParse(text, out var date) ? date : (DateTime?)null;

        public int getDayNoOfWeek(DayOfWeek weekday)
        {
            switch (weekday)
            {
                case DayOfWeek.Monday: return 1;
                case DayOfWeek.Tuesday: return 2;
                case DayOfWeek.Wednesday: return 3;
                case DayOfWeek.Thursday: return 4;
                case DayOfWeek.Friday: return 5;
                case DayOfWeek.Saturday: return 6;
                case DayOfWeek.Sunday: return 7;
                default: return 0;
            }
        }

        public string GetSqlDate(System.DateTime dat)
        {
            return dat.Date.Year.ToString("0000") + "/" + dat.Date.Month.ToString("00") + "/" + dat.Date.Day.ToString("00");
        }

        public async Task<PayPeriod> GetPayPeriod(string s_status)
        {
            PayPeriod pay_period = new PayPeriod();
            using (var conn = _conn.GetConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select top 1 PayPeriodEndDate,s.TSheetFirstServiceDate,ISNULL(DefaultPayPeriod,14) as DefaultPayPeriod,ISNULL(DefaultPayStartDay,'Monday') as DefaultPayStartDay,tsheetlastservicedate From Systable s, Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        DateTime? tsheetlastservicedate = null;
                        DateTime? payPeriodEndDate = null;
                        DateTime? tsheetFirstServiceDate = null;

                        if (await rd.ReadAsync())
                        {
                            payPeriodEndDate = Convert.ToDateTime(rd["PayPeriodEndDate"]);

                            if (payPeriodEndDate.HasValue)
                                pay_period.PayPeriodEndDate = Convert.ToDateTime(payPeriodEndDate);

                            tsheetFirstServiceDate = Convert.ToDateTime(rd["TSheetFirstServiceDate"]);

                            if (!tsheetFirstServiceDate.HasValue)
                                pay_period.Start_Date = DateTime.Today;
                            else
                                pay_period.Start_Date = Convert.ToDateTime(tsheetFirstServiceDate);


                            tsheetlastservicedate = Convert.ToDateTime(rd["tsheetlastservicedate"]);

                            if (!tsheetlastservicedate.HasValue)
                                pay_period.End_Date = Convert.ToDateTime(rd["PayPeriodEndDate"]);
                            else
                                pay_period.End_Date = Convert.ToDateTime(tsheetlastservicedate);


                            pay_period.PayPeriod_Length = Convert.ToInt32(rd["DefaultPayPeriod"]);
                            pay_period.StartDay = rd["DefaultPayStartDay"] as string;
                        }
                    }
                }
            }
            return pay_period;
        }

        public string GetFilteredRecipient_Criteria(string operatorid, string s_SQLStmt)
        {

            string s_SQL = "";
            string s_ProgramFilter = UserFilter.s_ProgramFilter;
            string s_CategoryFilter = UserFilter.s_ServiceRegionFilter;

            GetUserFilters(operatorid);

            if (!string.IsNullOrEmpty(UserFilter.s_BranchFilter))
                s_SQL = " AND " + UserFilter.s_BranchFilter;

            if (!string.IsNullOrEmpty(s_ProgramFilter))
            {
                if (s_SQLStmt.IndexOf("INNER JOIN RECIPIENTPROGRAMS", StringComparison.CurrentCultureIgnoreCase) > 0)
                    s_SQL = s_SQL + " AND " + s_ProgramFilter;
                if (s_SQLStmt.IndexOf("INNER JOIN ROSTER", StringComparison.CurrentCultureIgnoreCase) > 0)
                    s_SQL = s_SQL + " AND " + s_ProgramFilter.Replace("RecipientPrograms", "ROSTER");
                if (s_SQLStmt.IndexOf("INNER JOIN ServiceOverView", StringComparison.CurrentCultureIgnoreCase) > 0)
                    s_SQL = s_SQL + " AND " + s_ProgramFilter.Replace("RecipientPrograms.Program", "ServiceOverView.ServiceProgram");
            }

            if (!string.IsNullOrEmpty(s_CategoryFilter))
                s_SQL = s_SQL + " AND " + s_CategoryFilter;

            if (!string.IsNullOrEmpty(UserFilter.s_CoOrdinatorFilter))
                s_SQL = s_SQL + " AND " + UserFilter.s_CoOrdinatorFilter;

            if (!string.IsNullOrEmpty(UserFilter.s_StaffCategoryFilter) & s_SQLStmt.IndexOf("INNER JOIN STAFF", StringComparison.CurrentCultureIgnoreCase) > 0)
            {
                s_SQL = s_SQL + " AND " + UserFilter.s_StaffCategoryFilter;
            }

            return s_SQL;
        }

        public string GetFilteredStaff_Criteria(string operatorid)
        {
            string s_Sriteria = "";
            string s_StaffCategoryFilter = "";
            string s_BranchFilter = "";

            GetUserFilters(operatorid);
            s_Sriteria = " AccountNo > '!z' AND (CommencementDate is not null) and (TerminationDate is null)";

            s_BranchFilter = UserFilter.s_BranchFilter;
            s_StaffCategoryFilter = UserFilter.s_StaffCategoryFilter;

            if (s_BranchFilter != "")
            {
                s_BranchFilter = s_BranchFilter.Replace("Recipients.Branch", "Staff.STF_DEPARTMENT");
                s_Sriteria = s_Sriteria + " AND " + s_BranchFilter;
            }

            if (s_StaffCategoryFilter != "") s_Sriteria = s_Sriteria + " AND " + s_StaffCategoryFilter;
            return s_Sriteria;
        }

        public string CalculateEndTime(DateTimeObject obj)
        {
            return Convert.ToDateTime(obj.DateStr + " " + obj.StartTime).AddMinutes(5 * obj.Duration).ToString("HH:mm");
        }


        public void GetUserFilters(string operatorid)
        {
            using (var conn = _conn.GetConnection())
            {

                using (SqlCommand cmd = new SqlCommand(@"SELECT * FROM UserInfo WHERE Name = '" + operatorid + "'", (SqlConnection)conn))
                {
                    conn.Open();
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.Read())
                    {
                        if (rd["ViewFilter"].ToString() != "") UserFilter.s_ProgramFilter = rd["ViewFilter"].ToString();
                        if (rd["ViewFilterCategory"].ToString() != "") UserFilter.s_ServiceRegionFilter = rd["ViewFilterCategory"].ToString();
                        if (rd["ViewFilterCoOrd"].ToString() != "") UserFilter.s_CoOrdinatorFilter = rd["ViewFilterCoOrd"].ToString();
                        if (rd["ViewFilterBranches"].ToString() != "") UserFilter.s_BranchFilter = rd["ViewFilterBranches"].ToString();
                        if (rd["ViewFilterReminders"].ToString() != "") UserFilter.s_ReminderFilters = rd["ViewFilterReminders"].ToString();
                        if (rd["ViewFilterStaffCategory"].ToString() != "") UserFilter.s_StaffCategoryFilter = rd["ViewFilterStaffCategory"].ToString();
                    }
                }
            }

            //  If N2S(!ViewFilterCategory) <> "" Then s_ServiceRegionFilter = !ViewFilterCategory
            // If N2S(!ViewFilterCoOrd) <> "" Then s_CoOrdinatorFilter = !ViewFilterCoOrd
            // If N2S(!ViewFilterBranches) <> "" Then s_BranchFilter = !ViewFilterBranches
            //If N2S(!ViewFilterReminders) <> "" Then s_ReminderFilter = !ViewFilterReminders
            //If N2S(!ViewFilterStaffCategory) <> "" Then s_StaffCategoryFilter = !ViewFilterStaffCategory            
        }

      
       
        Task<bool> IGeneralSetting.ErrorLog(string ModuleName, string ModuleFunction, string Error, string user)
        {
             string sql = @" if not exists( select * from sys.objects where name='ErrorLog')
                         create table ErrorLog (Id int identity(1,1),logDate dateTime,ModuleName varchar(100),ModuleFunction varchar(100), Error varchar(MAX), [User] varchar(100));
                         INSERT INTO ErrorLog (logDate,ModuleName,ModuleFunction, Error, [User]) VALUES (getDate(),@ModuleName,@ModuleFunction, @Error, @User)";

            using (var conn = _conn.GetConnection())
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ModuleName", ModuleName);
                    cmd.Parameters.AddWithValue("@ModuleFunction", ModuleFunction);
                    cmd.Parameters.AddWithValue("@Error", Error);
                    cmd.Parameters.AddWithValue("@User", user);
                    cmd.ExecuteNonQuery();
                }
            }
            
            return Task.FromResult(true);
        }
    }

    public class CompareDateTimes
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}