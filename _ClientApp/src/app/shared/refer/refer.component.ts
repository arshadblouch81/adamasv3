import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'

import { Subject } from 'rxjs';
import { mergeMap, takeUntil, debounceTime, distinctUntilChanged, map, delay } from 'rxjs/operators';
import * as moment from 'moment';
import { ClrWizard } from "@clr/angular";
import { SqlWizardService } from '@services/sqlwizard.service'
import { TimeSheetService, GlobalService, ClientService, ListService, statuses, contactGroups, recurringInt, recurringStr } from '@services/index';

import format from 'date-fns/format';

const NOTE_TYPE: Array<string> = ['CASENOTE', 'OPNOTE', 'CLINNOTE'];

@Component({
  selector: 'app-refer',
  templateUrl: './refer.component.html',
  styleUrls: ['./refer.component.css']
})
  
export class ReferComponent implements OnInit, OnDestroy ,OnChanges {
  private unsubscribe$ = new Subject<void>();
  @Input() open: any;
  @Input() user: any;
  @Input() referTab: number = 1;

  @ViewChild("wizard", { static: false }) wizard: ClrWizard;

  referralGroup: FormGroup;
  programs: Array<any> = [];

  // Check if it is HCP, NDIA, DEX or OTHER
  referralType: number;

  packageCode: string;
  errorPackageCode: boolean = null;

  programTicked: boolean = false;
  showPackageTemplate: boolean = false;
  loadIfPackageExist: boolean = false;

  whatOptionVar: any = {
  reminderToArr: []
  }

  emailToArray: Array<any> = [];
  

  programsChecked: Array<any>;
  referralVariables: any;
  
  noteNumber: number = 0;
  noteArray: Array<string> = [];

  constructor(
    private listS: ListService,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private sqlWizS: SqlWizardService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  ngOnDestroy(): void{
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.wizard.reset();
        this.buildForm();
        this.open = true;
      }
    }
  }

  buildForm(): void{
    const currentDate = format(new Date(),'MM/dd/yyyy');
    const time = format(new Date(), 'HH:mm');

    this.referralGroup = this.formBuilder.group({
      date: currentDate,
      notes: '',
      noteType: 'CASENOTE',
      noteCategory: '',
      time: time,
      timeSpent: '00:15',
      publishToApp: false,
      referralSource: '',
      referralCode: '',
      referralType: '',
      reminderDate: '',
      reminderTo: '',
      notifTo: '',

      quantity: '',
      quantityTitle: '',
      charge: '',
      gst: '',
      suppRefNo: '',
      suppDate: '',
      dateOfDeath: ''
    });

    this.referralType = null; 
    this.loadIfPackageExist = null;
    this.programs = [];
    this.programsChecked = [];
    this.showPackageTemplate = null;
  }

  mutateToCheckboxes(list: Array<any>): Array<any> {
    return list.map((x, index, arr) => {
      return {
        program: typeof x === 'object' ? x.name : x,
        level: typeof x === 'object' ? x.level : '',
        status: false,
        disabled: false,
        index: index,
        selected: ''
      }
    });
  }

  accept() {
    
  }

  changeReferralType(data: any) {
    this.packageCode = null;
    this.showPackageTemplate = null;

    console.log(this.user)
    if (data == 1) {
      this.listS.getndiaprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data);
      });
    }
    if (data == 2) {
      this.listS.gethcpprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data);
      });
    }
    if (data == 3) {
      this.listS.getdexprograms(this.user.id).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data)
      });
    }
    if (data == 4) {      
      this.listS.getotherprograms(this.user.id).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data);
      });
    }
  }

  checkRadioButtonManual(data: any, index: number) {
    this.changeDetectorRef.detach();
    this.programs.forEach(x => x.status = false);
    
    this.changeDetectorRef.reattach();
    let program = this.programs[index];
    program.status = true;
  }

  programCheckedChange(data: any, index: number) {
    this.errorPackageCode = null;

    if (this.IsNDIAorHCP()) {
      this.checkRadioButtonManual(data, index);
    }
    

    if (this.referralType == 1 || this.referralType == 2) {
      let program = this.programs.filter(x => x.status);

      const input = {
        AccountNo: this.user.code,
        Template: program[0].program,
        Type: this.referralType
      }

      this.listS.getmarkreferralid(input).subscribe(data => {
        this.packageCode = data.result;
        this.showPackageTemplate = true;
      });

    }
 
    this.programTicked = true;
  }

  doCustomClick(buttonType: string): void {
    if ("custom-next" === buttonType) {
      
      // Email Notif
      if (this.user.email) {
        this.emailToArray.push(this.user.email);
        this.referralGroup.controls["notifTo"].setValue(this.user.email);
      }

      if (this.IsNDIAorHCP()) {
        this.loadIfPackageExist = true;
        this.checkPackageCodeIfExistObs(this.packageCode).pipe(delay(2000)).subscribe(data => {
          this.loadIfPackageExist = false;
          if (data[0].Count == 0) {
            this.getDetails();
            this.errorPackageCode = false;
            this.wizard.next();
          } else {
            this.errorPackageCode = true;
          }
        });

      
        
      } else {
        this.getDetails();
        this.wizard.next();
      }
    }
  }

  checkPackageCodeIfExistObs(packageCode: string) {    
    return this.listS.getlist(`SELECT COUNT(Name) AS Count FROM HumanResourceTypes WHERE [Name] = '${packageCode}'`)
  }

  getDetails() {
    this.referralVariables = {
      referralSourceArr: [],
      referralCodeArr: [],
      referralTypeArr: []
    }

    this.GET_REFERRAL_TYPE();
    this.GET_REFERRAL_CODE();
    this.GET_REFERRAL_SOURCE();
  }

  GET_REFERRAL_SOURCE() {
    this.listS.getwizardreferralsource('default').subscribe(data => {
      this.referralVariables.referralSourceArr = data;

      if (data && data.length > 1) {
        this.referralGroup.patchValue({
          referralSource: this.DEFAULT_REFERRAL_SOURCE()
        });
      }
    });
  }

  DEFAULT_REFERRAL_SOURCE(): string {
    if (this.referralType == 1) return 'NDIA';
    if (this.referralType == 2) return 'My Aged Care Gateway';
    if (this.referralType == 3) return 'My Aged Care Gateway';
    if (this.referralType == 4) return 'Other';
  }

  GET_REFERRAL_CODE() {
    this.sqlWizS.GetReferralCode(this.programsChecked).subscribe(data => {
      this.referralVariables.referralCodeArr = data;

      // if (data && data.length > 1) {
      //   this.referralGroup.patchValue({
      //     referralSource: 'My Aged Care Gateway'
      //   });
      // }
    })
  }

  GET_CASE_NOTE_CATEGORY() {
    this.noteArray = ['REFERRAL-IN'];
    this.referralGroup.patchValue({
      noteCategory: 'REFERRAL-IN'
    })
    // this.listS.getcasenotecategory(0).subscribe(data => {
    //   this.noteArray = data;
    //   console.log(data);
    // });
  }

  GET_REFERRAL_TYPE() {
    var date = format(new Date(), 'MM-dd-yyyy');

    if(this.referralType == 1 || this.referralType == 2){
      this.listS.getlist(`SELECT [Title] As Activity FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ADMISSION') AND [MinorGroup] IN ('REFERRAL-IN') AND (EndDate Is Null OR EndDate >= '${date}') ORDER BY [Title]`)
        .subscribe(data => {

          this.referralVariables.referralTypeArr = data;
          if (data && data.length == 1) {
            this.referralGroup.patchValue({
              referralType: data[0].Activity
            });
          }

        });
    } else {
      this.getCommaSeparatedStringProgram();
      this.listS.getlist(`SELECT DISTINCT([service type]) AS Activity 
          FROM   serviceoverview SO 
                INNER JOIN humanresourcetypes HRT 
                        ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid 
          WHERE  SO.serviceprogram IN (${ this.getCommaSeparatedStringProgram()})
                AND EXISTS (SELECT title 
                            FROM   itemtypes ITM 
                            WHERE  title = SO.[service type] 
                                    AND ITM.[rostergroup] IN ( 'ADMISSION' ) 
                                    AND ITM.[minorgroup] IN ( 'REFERRAL-IN' ) 
                                    AND ( ITM.enddate IS NULL 
                                          OR ITM.enddate >= '04-03-2020' )) 
          ORDER  BY [service type];
          `).subscribe(data => {
              this.referralVariables.referralTypeArr = data;
            
              if (data && data.length == 1) {
                this.referralGroup.patchValue({
                  referralType: data[0].Activity
                });
              }
          })
    }
  }

  getCommaSeparatedStringProgram(): string {
    return this.programs.filter(x => x.status).map(x => `'${x.program}'`).join(',');
  }

  noteChangeEvent(index: number) {
    // if (this.referralGroup.value.notes != '' && this.showThisOnTab_A()) {
    //   this.referralGroup.patchValue({ noteCategory: '' })
    //   this.listS.getcasenotecategory(index).subscribe(data => this.noteArray = data);
    //   return;
    // }
    // this.referralGroup.patchValue({ noteCategory: this.whatOptionVar.wizardTitle })
  }

  nextCustomClick(buttonType: string) {
    if ("custom-next" === buttonType) {
      this.buildNotes();
      this.GET_CASE_NOTE_CATEGORY();
      this.wizard.next();
    }
  }

  buildNotes(): void {

    let _programInit = null;

    if (this.referralType == 1) {
      _programInit = 'NDIA REFERRAL';
    }

    if (this.referralType == 2) {
      _programInit = 'HOME CARE PACKAGE REFERRAL';
    }

    if (this.referralType > 2) {
      _programInit = this.programs.filter(x => x.status).map(x => x.program).join(',');
    }
    const note =
      `REFERRAL-IN: ${this.programs.filter(x => x.status).length > 1 ? 'VARIOUS -' + `${this.user.code}` : ''} 
      ${_programInit} / INQUIRY FROM: ${this.user.code}
      PHONE:  ${this.user.contact || ''}
      ADDRESS: ${this.user.address || ''}

      NOTES:     `
    this.referralGroup.patchValue({
      notes: note
    })
  }


  finishClick(buttonType: string) {
    if ("finish" === buttonType) {
        console.log('finish')
    }
  }

  doCancel(): void {
    this.wizard.close();
  }

  goBack(): void {
    this.wizard.previous();
  }

  IsNDIAorHCP(): boolean {
    return this.referralType == 1 || this.referralType == 2;
  }

  Type(): string {

    if (this.referralType == 1)
      return 'NDIA';
    
    if (this.referralType == 2)
      return 'DOHA';
  }

  Level() {
    return 'LEVEL 1'
  }

  onCommit() {

    var referralGroup = this.referralGroup.value;

    const blockNoTime = Math.floor(this.globalS.getMinutes(referralGroup.time) / 5)
    const timeInMinutes = this.globalS.getMinutes(referralGroup.timeSpent)
    const timePercentage = (Math.floor(timeInMinutes / 60 * 100) / 100).toString()
    const user = this.globalS.decode();

    const defaultValues = {
        // '' is the default
        billDesc: '',

        // 7 for everything except 14 for item
        type: 7,

        // 2 is the default,
        rStatus: '2',

        // HOUR is the default     
        billUnit: 'HOUR',

        // 0 is the default  
        billType: '0',

        // '' is the default
        payType: '',

        // '' is the default
        payRate: '',

        // '' is the default
        payUnit: '',

        // '' is the default
        apInvoiceDate: '',

        // '' is the default
        apInvoiceNumber: '',

        // 0 is default
        groupActivity: '0',

        // '' is the default
        serviceSetting: ''
    }
    var finalRoster: Array<Dto.ProcedureRoster> = [];

    const checkedPrograms = this.programs.filter(x => x.status);

    checkedPrograms.forEach(x => {

      let program: Dto.ProcedureRoster = {
        clientCode: this.user.code,
        carerCode: user.code,
        serviceType: referralGroup.referralType,
        date: moment(referralGroup.date).format('YYYY/MM/DD'),
        time: referralGroup.time,
        creator: user.user,
        editer: user.user,
        billUnit: defaultValues.billUnit,
        billDesc: defaultValues.billDesc,
        agencyDefinedGroup: this.user.agencyDefinedGroup,
        referralCode: referralGroup.referralCode,
        timePercent: timePercentage,
        Notes: '',
        type: defaultValues.type,
        duration: timeInMinutes / 5,
        blockNo: blockNoTime,
        reasonType: this.IsNDIAorHCP() ? '' : '',
        tabType: 'REFERRAL-IN',
        program: this.IsNDIAorHCP() ? this.packageCode : x.program,
        packageStatus: 'REFERRAL'
      }

      finalRoster.push(program);
    });

    

    const data: Dto.CallProcedure = {
      isNDIAHCP: this.IsNDIAorHCP(),
      oldPackage: this.IsNDIAorHCP() ? this.packageCode : '',
      newPackage: this.IsNDIAorHCP() ? this.packageCode : '',
      level: this.IsNDIAorHCP() ? this.Level() : '',
      type: this.IsNDIAorHCP() ? this.Type() : '',
      roster: finalRoster,
      staffNote: {
        personId: this.user.id,
        program: checkedPrograms.length > 1 ? 'VARIOUS' : checkedPrograms[0].program,
        //this.globalS.isVarious(this.checkedPrograms)

        detailDate: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'),
        extraDetail1: NOTE_TYPE[this.noteNumber],
        extraDetail2: 'REFERRAL-IN',
        whoCode: this.user.code,
        publishToApp: referralGroup.publishToApp ? 1 : 0,
        creator: user.user,
        note: referralGroup.notes,
        alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
        reminderTo: referralGroup.reminderTo
      }
    }

    console.log(data);

    this.listS.postreferralin(data).subscribe(data => {
      this.globalS.sToast('Success', 'Data is processed');
      this.wizard.close();
    });

  }

}
