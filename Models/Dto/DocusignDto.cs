﻿namespace AdamasV3.Models.Dto
{
    public class DocusignDto
    {
        public string EnvelopeId { get; set; }
        public string DocusignEmbeddedUrl { get; set; }
    }
}
