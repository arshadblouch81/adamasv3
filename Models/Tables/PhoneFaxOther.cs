using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Adamas.Models.Tables {
    public class PhoneFaxOther
    {
        [Key]
        public int RecordNumber { get; set; }
        public string Type { get; set; }
        public string Detail { get; set; }
        public string PersonID { get; set; }
        public bool PrimaryPhone { get; set; }
    }
}