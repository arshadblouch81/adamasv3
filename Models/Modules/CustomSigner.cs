
using DocuSign.eSign.Model;

namespace Adamas.Models.Modules{
    public class CustomSigner 
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int RecipientId { get; set; }
        public string RoutingOrder { get ;set; }
        public string Company { get; set; }
        public string Title { get; set; }
    }
}