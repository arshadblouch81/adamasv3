import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import {Router,RouterLink} from '@angular/router'

import { GlobalService } from '@services/index';

interface SPECIAL_ROUTE_GUARDS {
  mmAccounting: number,
  mmAnalyseBudget: boolean,
  mmAtAGlance: boolean,
  mmBilling: number,
  mmOtherDS: number,
  mmPriceUpdates: number,
  mmPublishPrintRosters: number,
  mmTimesheetProcessing: number,
  mmdexUploads: number,
  mmhacc: number,
  mmndia: number,
  accessCDC: boolean,
  system?: number
}

@Component({
    styles: [`
      .landing-container{
        display: grid;
        grid-template-columns: minmax(min-content, 1fr) minmax(min-content, 1fr) minmax(min-content, 1fr) ;
        grid-gap: 1rem;
        width: 50%;
        margin: 0 auto;
      }
      figure{
        min-width:4rem;
        background-color:#85B9D5;
        color:#fff;
        border-radius: 3px;
        padding: 20px;
        font-size: 30px;
        text-align: center;
        margin:0;
        position: relative;
        height: 8rem;
        cursor:pointer;
      }
      figure:hover{
        background-color: #2466b9;
        color:#fff;
        -webkit-transition: background-color 300ms linear;
        -ms-transition: background-color 300ms linear;
        transition: background-color 300ms linear;
      }
      figure i{
        font-size: 2.7rem;
      }
      figcaption{
        font-size: 1.12rem;
        position: absolute;
        bottom: 1rem;
        left: 0;
        right: 0;
      }
    `],
    templateUrl: './landing.html'
})


export class LandingAdmin implements OnInit, OnDestroy {

    SPECIAL_ROUTE_GUARDS: SPECIAL_ROUTE_GUARDS;
  
    constructor(
      private router: Router,
      private globalS: GlobalService
      ) {

    }

    ngOnInit(): void {
      const settings: any = this.globalS.settings;

      this.SPECIAL_ROUTE_GUARDS = {
          mmAccounting: settings.mmAccounting,
          mmAnalyseBudget: settings.mmAnalyseBudget,
          mmAtAGlance: settings.mmAtAGlance,
          mmBilling: settings.mmBilling,
          mmOtherDS: settings.mmOtherDS,
          mmPriceUpdates: settings.mmPriceUpdates,
          mmPublishPrintRosters: settings.mmPublishPrintRosters,
          mmTimesheetProcessing: settings.mmTimesheetProcessing,
          mmdexUploads: settings.mmdexUploads,
          mmhacc: settings.mmhacc,
          mmndia: settings.mmndia,
          accessCDC: settings.accessCDC,
          system: settings.system
      }

     // console.log(this.SPECIAL_ROUTE_GUARDS)
    }

    ngOnDestroy(): void {

    }

    
}