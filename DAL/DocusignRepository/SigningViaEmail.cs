﻿namespace AdamasV3.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Adamas.Models.Modules;
    using AdamasV3.Models.Dto;
    using DocuSign.eSign.Api;
    using DocuSign.eSign.Client;
    using DocuSign.eSign.Model;
    using Org.BouncyCastle.Crypto;
    using Document = DocuSign.eSign.Model.Document;
    public static class SigningViaEmail
    {
        public static DocusignDto SendEnvelopeViaEmail(List<CustomSigner> _signers, List<Document> _documents, string envStatus, string basePath, string accessToken, string accountId, string contentRootPath)
        {

            EnvelopeDefinition env = MakeEnvelope(_signers, _documents, envStatus, contentRootPath);
            var docuSignClient = new DocuSignClient(basePath);
            docuSignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + accessToken);

            EnvelopesApi envelopesApi = new EnvelopesApi(docuSignClient);
            EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, env);


            DocusignDto dsignDto = new DocusignDto()
            {
                DocusignEmbeddedUrl = "",
                EnvelopeId = results.EnvelopeId
            };

            return dsignDto;
        }

        public static EnvelopeDefinition MakeEnvelope(List<CustomSigner> _signers, List<Document> _documents, string envStatus, string contentRootPath)
        {

            //ds-snippet-start:eSign2Step2
            EnvelopeDefinition env = new EnvelopeDefinition();
            env.EmailSubject = "Please sign this document set";

            var signers = GetSigners(_signers);
            var documents = GetDocuments(_documents, contentRootPath);


            // The order in the docs array determines the order in the envelope
            env.Documents = documents;

            int signerCounter = 1;
            foreach (var signer in signers)
            {
                signer.Tabs = new Tabs
                {
                    SignHereTabs = new List<SignHere> {
                        new SignHere(){
                            AnchorString    = $@"\s{signerCounter}\",
                            Name            = signer.Name
                        }
                    },
                    InitialHereTabs = new List<InitialHere> {
                        new InitialHere(){
                            AnchorString    = $@"\i{signerCounter}\",
                        }
                    },
                    FullNameTabs = new List<FullName> {
                        new FullName(){
                            AnchorString    = $@"\n{signerCounter}\",
                            Name            = signer.Name
                        }
                    },
                    CompanyTabs = new List<Company> {
                        new Company(){
                            AnchorString    = $@"\co{signerCounter}\",
                            Name            = _signers[signerCounter - 1].Company,
                            Value           = _signers[signerCounter - 1].Company,
                            OriginalValue   = _signers[signerCounter - 1].Company,
                            Required        = "false"
                        }
                    },
                    TitleTabs = new List<Title> {
                        new Title(){
                            AnchorString    = $@"\t{signerCounter}\",
                            Name            = _signers[signerCounter - 1].Title,
                            Value           = _signers[signerCounter - 1].Title,
                            OriginalValue   = _signers[signerCounter - 1].Title,
                            Required        = "false"
                        }
                    },
                    DateSignedTabs = new List<DateSigned>{
                        new DateSigned(){
                            AnchorString    = $@"\d{signerCounter}\",
                            Name            = "",
                            Value           = "",
                            TemplateLocked  = "true"
                        }
                    }
                };
                signerCounter++;
            }

            // Add the recipients to the envelope object
            Recipients recipients = new Recipients
            {
                Signers = signers
            };
            env.Recipients = recipients;

            // Request that the envelope be sent by setting |status| to "sent".
            // To request that the envelope be created as a draft, set to "created"
            env.Status = envStatus;

            return env;
        }

        public static List<Signer> GetSigners(List<CustomSigner> _signers)
        {
            List<Signer> signers = new List<Signer>();
            foreach (var signer in _signers)
            {
                signers.Add(new Signer()
                {
                    Email           = signer.Email,
                    Name            = signer.Name,
                    RecipientId     = signer.RecipientId.ToString(),
                    RoutingOrder    = signer.RoutingOrder
                });
            }
            return signers;
        }

        public static List<Document> GetDocuments(List<Document> _documents, string contentRootPath)
        {
            List<Document> documents = new List<Document>();
            foreach (var document in _documents)
            {
                documents.Add(new Document()
                {
                    DocumentBase64  = Convert.ToBase64String(System.IO.File.ReadAllBytes(System.IO.Path.Combine(contentRootPath, "document", document.DocumentBase64))),
                    Name            = document.Name,
                    FileExtension   = document.FileExtension,
                    DocumentId      = document.DocumentId
                });
            }
            return documents;
        }

    }
}
