using System;
using System.Globalization;

namespace Adamas.Models.Modules{
    public class ProcedureRosterNew {

        public string ClientCode { get; set; }
        public string CarerCode { get; set; }
        public string ServiceType { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Creator { get; set; }
        public string Editer { get; set; }
        public string BillUnit { get; set; }
        public string AgencyDefinedGroup { get; set; }
        public string ReferralCode { get; set; }
        public string TimePercent { get; set; }
        public string Notes { get; set; }
        public int? Type { get; set; }
        public decimal? Duration { get; set; }
        public int? BlockNo  { get; set; }
        public string ReasonType { get; set; }
        public string TabType { get; set; }     
        public string Program { get; set; }  
        public string BillDesc { get; set; }
        public string PackageStatus { get; set; }
        
        // public string BillTo { get; set; }
        // public string CostUnit { get; set; }
        // public string HaccType { get; set; }
        // public Decimal? UnitPayRate { get; set; }
        // public Decimal? UnitBillRate { get; set; }  
        // public Decimal? TaxPercent { get; set; }
        // public string APIInvoiceDate { get; set; }
        // public string APIInvoiceNumber { get; set; }
       
    }
}