using Adamas.Models.Tables;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class StaffProfileInput {

        public Adamas.Models.Tables.Staff Staff { get; set; }
        public List<NamesAndAddresses> NamesAndAddresses { get; set; }
        public List<PhoneFaxOther> PhoneFaxOther { get; set; }

    }
}