import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';


import
{
  ChildSafety,
  DatasetQccsmda,
  DSS,
  MentalHlth,
  QCSSHAC,
  CHSPDEX
} from './index';



const routes: Routes = [
    {
        path: '',
        redirectTo: 'chspdex',
        pathMatch: 'full'
    },
    {
      path: 'qcsshac',
      component: QCSSHAC
  },
  {
    path: 'chspdex',
    component: CHSPDEX
},
    {
        path: 'qccsmds',
        component: DatasetQccsmda
    },
    {
        path:'childsafety',
        component: ChildSafety,
    },
    {
        path:'dss',
        component:DSS,
    },
    {
        path:'mentalhlth',
        component:MentalHlth,
    }
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule
    
  ],
  declarations: [
   
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class DatasetAdminModule {}

