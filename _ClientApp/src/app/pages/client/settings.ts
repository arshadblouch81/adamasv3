
import { takeUntil, mergeMap, distinctUntilChanged } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs'

import { TimeSheetService, GlobalService, StaffService, ClientService } from '../../services/index';
import * as moment from 'moment';
import * as _ from 'lodash';

interface Settings {
    key: string,
    value: any
}
@Component({
    selector: 'settings',
    templateUrl: './settings.html',
    styles: [`
        .settings{
            display:flex;
            height:1.5rem;
            align-items: center;
            padding:0 10px;
        }
        .settings:nth-child(even){
            background:#f3f3f3
        }
        .settings > div:first-child{
            font-weight: 500;
            font-size: 13px;
        }
        .settings > div{
            flex:1;
        }
        .check{
            color:green;
        }
        .exit{
            color:red;
        }
       
    `]
})

export class SettingsClient implements OnInit, OnDestroy {
    settings: Array<Settings> = [];

    constructor(
        private globalS: GlobalService
    ) {

    }

    ngOnInit() {
        for (let [key, val] of Object.entries(this.globalS.userSettings)) {
            const data: Settings = {
                key: key,
                value: val
            };
            this.settings.push(data);
        }
    }

    ngOnDestroy() {

    }

    show(value): number {
        if (value === "True" || value === 1 || value === "1")
            return 1;
        if (value === "False" || value === 0 || value === "0" || value === null || value === "")
            return 0;
        return -1;
    }

}
