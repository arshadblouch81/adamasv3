using System;

namespace Adamas.Models.Modules
{
    public class PayPeriodDate {
      public DateTime PayPeriodEndDate { get; set; }
      public DateTime StartDate { get; set; }
      public DateTime EndDate { get; set; }
    }
}