using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class SqlVariable {
        public string TableName { get; set; }
        public string Columns { get; set; }
        public string ColumnValues { get ;set ;}
        public string  SetClause { get; set; }
        public string WhereClause { get; set; }
        // public List<Dictionary<string,string>> Columns { get; set; }
        // public Record Record { get; set ; }
    }

    // public class Record {
    //     public int Id   { get; set; }
    //     public string Name { get; set; }
    // }
}