import { Component, OnInit, Input, forwardRef, ViewChild, OnDestroy, Inject, ChangeDetectionStrategy, ChangeDetectorRef, HostListener, EventEmitter, Output, ComponentRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';


import { TimeSheetService, GlobalService, view, ClientService, ShareService , StaffService, ListService, UploadService, months, days, gender, gender_cald,visaStatus, types, titles, caldStatuses, roles, SettingsService, DllService } from '@services/index';
import * as _ from 'lodash';
import { mergeMap, takeUntil, concatMap, switchMap, map, debounceTime } from 'rxjs/operators';
import { forkJoin, Observable, EMPTY, Subscription, Subject } from 'rxjs';

import { NzMessageService } from 'ng-zorro-antd/message';
import { RemoveFirstLast } from '@pipes/pipes';

import { ProfileInterface, PhoneFaxOther, NamesAndAddresses, Staffs, DropDowns, Recipients  } from '@modules/modules';

import { NzModalService } from 'ng-zorro-antd/modal';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import format from 'date-fns/format';

declare var Dto: any;

const noop = () => {
};

const PROFILEPAGE_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  multi: true,
  useExisting: forwardRef(() => ProfileComponent),
};
class Address {
  postcode: string;
  address: string;
  suburb: string;
  state: string;
  
  constructor(postcode: string, address: string, suburb: string, state: string) {
    this.suburb = suburb.trim();
    this.address = address;
    this.postcode = postcode;
    this.state = state;
  }
  
  getAddress() {
    var _address = `${this.address} ${this.suburb} ${this.postcode}`;
    return (_address.split(' ').join('+')).split('/').join('%2F');
  }
}
@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [
    PROFILEPAGE_VALUE_ACCESSOR
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ProfileComponent implements OnInit, OnDestroy, ControlValueAccessor {
  
  innerWidth: any;
  
  @Input() isAdmin: boolean = false;
  selectedRow: number = 0;
  activeRowData: any;
  selectedAdrRow: number = 0;
  activeAdrRowData: any;
  private unsubscribe: Subject<void> = new Subject();
  postcodesArray: any;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = this.globalS.ADJUST_DRAWER_WIDTH(window.innerWidth);;
  }
  
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  
  typeSub$: Subscription;
  
  
  coordinator$: Observable<any>;
  programs$: Observable<any>;
  
  showUpload: boolean = false;
  innerValue: ProfileInterface;
  
  current: number = 0;
  
  profileStaffModal: boolean = false;
  profileStaffOptionsModal: boolean = false;
  profileStaffPreferredModal: boolean = false;
  addCaseStaffModal: boolean = false;
  
  tabIndex: number = 0;
  
  profileRecipientOptionsModal: boolean = false;
  
  editModalOpen: boolean = false;
  user: any;
  
  window: number;
  view = view;
  
  selectedValue: any
  
  contactsArray: Array<any> = [];
  addressArray: Array<any> = [];
  caseStaffArray: Array<any> = [];
  activePrograms: Array<any> = [];
  years: Array<string> = [];
  months: Array<string> = [];
  days: Array<string> = [];
  
  casemanagers: Array<any> = [];
  subscriptionArray: Array<Observable<any>> = []
  
  contactForm: FormGroup;
  addressForm: FormGroup;
  userForm: FormGroup;
  emailCoordinatorGroup: FormGroup;
  contactIssueGroup: FormGroup;
  caseStaffGroup: FormGroup;
  frmPastName: FormGroup;
  frmOtherName: FormGroup;
  
  loading: boolean = false;
  loading2: boolean = false;
  editProfileDrawer: boolean = false;
  addAddressDrawer: boolean = false;
  addContactDrawer: boolean = false;
  changeProfilePictureModal: boolean = false;
  ViewPastName:boolean;
  ViewOtherName:boolean;
  
  titles: Array<string> = titles;
  genderArr: Array<string> = gender;
  genderArr_cald: Array<string> = gender_cald;
  visaStatus:Array<string> = visaStatus;
  typesArr: Array<string> = types;
  caldStatuses: Array<string> = caldStatuses
  
  showMailManagerBtn: boolean = false;
  emailManagerOpen: boolean = false;
  emailManagerNoEmailShowNotif: boolean = false;
  
  caseManagerDetails: any;
  dropDowns: DropDowns;
  
  _settings: SettingsService;
  
  imgSrc: any;
  defaultURL: any;
  
  showAvatar: boolean =true;
  staffrecordview: string; 
  token: any;
  
  topBelow = { top: '20px' }
  
  recipientStatus: string;
  recipientType: any;
  personID:string;
  currentview:string;
  dateFormat: string = 'dd/MM/yyyy';
  AliasName:any;
  viewAliasForm:boolean;
  lstMobility:Array<any>=['HOIST','BUS','WCH','WWW'];
  lstcarerStatus:Array<any>=['Has A Carer','Has No Carer','N/A-No Carer Required','N/A-Paid Carer','Not Stated Described','Non-Resident Carer'];
  lstRecipient:Array<any>=[];
  lstPastNames:Array<any>=[];
  lstAliases:Array<any>=[];
  lstGroup: Array<any>=['RECIPIENTPASTNAME','RECIPIENTALIAS']
  alert_disabled:boolean=true;
  otherDisabilityCheckBoxString: string = '00000000000000000000000000000000000000000000000000000000000000';
  ViewBudgetModel:boolean;
  ViewGrpah:Subject<any>=new Subject();
  budgetData:Array<any>=[];
  ViewPackage:Subject<boolean>=new Subject();
  budgetStatus:any;
  redBudget:boolean;
  yellowBudget:boolean;
  orangeBudget:boolean=true;
  greenBudget:boolean=true;
  budgetConsumed:number;
  
  
  constructor(
    private globalS: GlobalService,
    private clientS: ClientService,
    private staffS: StaffService,
    private timeS: TimeSheetService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private message: NzMessageService,
    private settings: SettingsService,
    private cd: ChangeDetectorRef,
    private uploadS: UploadService,
    private shareS: ShareService,
    private ds: DomSanitizer,
    private sharedS: ShareService,
    private nzContextMenuService: NzContextMenuService,
    private dll:DllService
   
  ) {
    
    this.sharedS.emitProfileStatus$.subscribe(data => {
      this.recipientType = data.type == null || data.type.trim() == "" ? null : data.type; 
      this.personID= data.uniqueID;
      

      if (data.admissionDate != null) {
        if(data.admissionDate != null && data.dischargeDate == null){
          this.recipientStatus = 'active';
        } else {
          this.recipientStatus ='inactive';
        }
      }
          
      if (data.commencementDate != null) {  // Check if CommencementDate is not null or undefined
        if (data.TerminationDate == null) {  // Check if TerminationDate is null
            this.recipientStatus = 'active';  
        } else {
            this.recipientStatus = 'inactive';
        }
      }
      
    });
    
    this.sharedS.emitSaveAll$.subscribe(data => {
      if (data.type=='Recipient' && data.tab=='Personal')
        this.SaveAll_Changes(data);
      else  if (data.type=='Staff' && data.tab=='Personal')
        this.SaveAll_Staff(data);
    })
    this.POPULATE_ADDRESS();
  }

  showBudget(user:any){
    this.greenBudget=true;
    // this.timeS.getRecipientBudgetStatus(this.personID).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //   if (data.length>0) {
    //   this.budgetStatus=data[0];
     
    // }
    // });
  
    if(user.id==null || user.id==''){
      return;
    }

  
    let input ={
      User: this.token.user,
      Password : 'sysmgr', //this.globalS.decode().password,
      PersonId: user.id,
      AccountNo: user. accountNo
    }
   

    this.dll.CheckFundingAlerts(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.greenBudget=false;
      this.orangeBudget=false;
      this.redBudget=false;
      this.yellowBudget=false;

      if (data=='OVER'){
        this.redBudget=true;           
      }else if (data=='UNDER'){            
        this.orangeBudget=true;            
      }else if (data=='OK' ){          
        this.greenBudget=true;
       } else{
        this.yellowBudget=true;
      
        
      }
 });

     
  }
  
  ngOnInit() {
    const { role, user } = this.globalS.decode();
    
    this.innerWidth = this.globalS.ADJUST_DRAWER_WIDTH(window.innerWidth);
    
    if(user && role == 'ADMIN USER'){
      this.listS.getstaffrecordview(user).subscribe(data => {
        if(data !== null){
          this.staffrecordview = data.staffRecordView;
        }
      });
    }
    
    
    this._settings = this.settings;
    
    if (role == roles.client) {
      this.showMailManagerBtn = true;
    }
    
    this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.pickedMember) : this.globalS.decode();
    
    
    this.buildForms();
    this.POPULATE_DATE_DROPDOWNS();
    this.POPULATE_OTHER_DROPDOWNS();
    
    this.POPULATE_CONTACTS();
    //temporarily added code
    let sql=`SELECT AccountNo FROM Recipients WHERE AccountNo <> '!INTERNAL' ORDER BY AccountNo`;
    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.lstRecipient = data;
    });

    
      
  }
  
  ngOnAfterViewInit(){
    
  }

  ngOnDestroy(): void{
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
  
  buildForms(): void {
    
    this.contactIssueGroup = this.formBuilder.group({
      contactIssues: '',
      rosterAlerts: '',
      runsheetAlerts: '',
      klm: ''
    });
    
    this.caseStaffGroup = this.formBuilder.group({
      coordinator: null,
      program: null,
      notes: null
    });
    
    this.contactForm = this.formBuilder.group({
      id: [''],
      type: ['', [Validators.required]],
      details: [''],
      personId: [''],
      primaryPhone: [false],
      privatecontact:[false]
    });
    
    this.addressForm = this.formBuilder.group({
      id: [''],
      description: [null, [Validators.required]],
      address1: [null, [Validators.required]],
      address2: [null, [Validators.required]],
      pcodesuburb: [null],
      gridRef:[''],
      googleAddress:[''],
      latitude:[''],
      longitude:[''],
      personId: [''],
      primaryAddress: [false]
    });
    
    
    this.frmPastName = this.formBuilder.group({
      id: [''],
      group: [null, [Validators.required]],
      pastName: [null, [Validators.required]],
      notes: [null],
      
    });
    this.frmOtherName = this.formBuilder.group({
      id: [''],
      group: [null, [Validators.required]],
      Name: [null, [Validators.required]],
      Address1: [null],
      Address2: [null],
      Suburb: [null],
      Postcode: [null],
      Phone1: [null],
      Phone2: [null],
      Mobile: [null],
      Fax: [null],
      Email: [null],
      ListOrder: [null],
      Person1: [null],
      Person2: [null],
      notes: [null]});
      
      
      
      this.userForm = this.formBuilder.group({
        recordId:[''],
        surnameOrg: [''],
        preferredName: [''],
        firstName: [''],
        middleNames: [''],
        gender: [''],
        gender_cald: [''],
        dateOfBirth :[''],
        year: [''],
        month: [''],
        day: [''],
        dob:[''],
        title: [''],
        uniqueID: '',
        
        file1: [''],
        file2: [''],
        subCategory: [''],
        branch: [''],
        
        serviceRegion: [''],
        casemanager: [''],
        
        caldStatus: [''],
        indigStatus: [''],
        visaStatus:[''],
        primaryDisability: [''],
        cstda_OtherDisabilities:[''],
        note: [''],
        type: [''],
        jobCategory: [''],
        adminCategory: [''],
        team: [''],
        gridNo: [''],
        dLicense: [''],
        mvReg: [''],
        nReg: [''],
        isEmail: false,
        isRosterable: false,
        isCaseLoad: false,
        excludeFromConflictChecking:false,
        stf_Department: '',
        rating: '',
        mobility: [''],
        carerStatus: [''],
        urNumber: [''],
        ndisNumber: [''],
        alias: [''],
      });
      
      this.resetEmailCoordinator();
      
      // this.contactForm.get('type').valueChanges.pipe(debounceTime(100)).subscribe(x => this.contactForm.patchValue({ details: null }))
      
    }
   
    patchTheseValuesInForm(user = this.user) {
      
      this.sharedS.setRecipientData(user);

      if (this.innerValue.view === view.recipient) {
        this.userForm.patchValue({
          title: user.title,
          surnameOrg: user.surnameOrg,
          firstName: user.firstName,
          middleNames: user.middleNames,
          dateOfBirth : user.dateOfBirth,
          gender: this.globalS.searchOf(user.gender, this.genderArr, this.genderArr[2]),       
          year: this.globalS.filterYear(user.dateOfBirth),
          month: this.globalS.searchOf(this.globalS.filterMonth(user.dateOfBirth), months, 'December'),
          day: this.globalS.filterDay(user.dateOfBirth),
          casemanager: user.recipient_Coordinator,
          file1: user.agencyIdReportingCode,
          branch: user.branch,
          file2: user.urNumber,
          subCategory: user.ubdMap,
          serviceRegion: user.agencyDefinedGroup,
          uniqueID: user.uniqueID,
          preferredName: user.preferredName,
          mobility: user.mobility,
          carerStatus: user.carerStatus,          
          ndisNumber: user.ndisNumber,
          urNumber: user.urNumber,
          alias :user.alias
        });
        
        
        this.contactIssueGroup.patchValue({
          contactIssues: user.contactIssues,
          rosterAlerts: user.notes,
          runsheetAlerts: user.specialConsiderations,
          klm: user.kLMArrangements
        })
        
        
        // this.contactIssueGroup.patchValue({
        //     value: user.contactIssues
        // })
        
        // this.rosterAlertGroup.patchValue({
        //     value: user.notes
        // })
        
        // this.runsheetAlertGroup.patchValue({
        //     value: user.specialConsiderations
        // })
        
      }
      
      if (this.innerValue.view === view.staff) {
        
        // Handling the patching logic
        if (titles.includes(user.title)) {
          // If the user title exists in the titles list, patch it
          this.userForm.patchValue({
              title: user.title
          });
        } else {
          // If the title is not valid, reset the title form control to null
          this.userForm.get('title').reset()
        }

        this.userForm.patchValue({
          surnameOrg: user.lastName,
          firstName: user.firstName,
          middleNames: user.middleNames,
          gender: this.globalS.searchOf(user.gender, this.genderArr, this.genderArr[2]),
          year: this.globalS.filterYear(user.dob),
          month: this.globalS.searchOf(this.globalS.filterMonth(user.dob), months, 'December'),
          day: this.globalS.filterDay(user.dob),
          dob:user.dob,
          preferredName: user.preferredName,
          recordId:user.uniqueID,
          casemanager: user.pan_Manager,
          type: user.category,
          stf_Department: user.stf_Department,
          jobCategory: user.staffGroup,
          adminCategory: user.subCategory,
          team: user.staffTeam,
          serviceRegion: user.serviceRegion,
          gridNo: user.ubdMap,
          dLicense: user.dLicence,
          mvReg: user.vRegistration,
          nReg: user.nRegistration,
          rating: user.rating,
          note: user.contactIssues,
          isEmail: user.emailTimesheet,
          excludeFromConflictChecking:user.excludeFromConflictChecking,
          isRosterable: user.isRosterable,
          caldStatus: user.caldStatus ? user.caldStatus : null,
          indigStatus: user.cstda_Indiginous ? user.cstda_Indiginous : null,
          visaStatus: user.visaStatus ? user.visaStatus : null,
          primaryDisability: user.cstda_DisabilityGroup ? user.cstda_DisabilityGroup : null,
          isCaseLoad: user.caseManager
        });
        
        this.otherDisabilityCheckBoxString = (user.cstda_OtherDisabilities == null) ? this.otherDisabilityCheckBoxString : user.cstda_OtherDisabilities;
        this.globalS.var1 = this.userForm.value.recordId;      
        this.cd.markForCheck();
        this.cd.detectChanges();
        
        this.contactIssueGroup.patchValue({
          contactIssues: user.contactIssues,
        })
        
      }
    }
    
    
    resetEmailCoordinator() {
      this.emailCoordinatorGroup = this.formBuilder.group({
        subject: new FormControl('', [Validators.required]),
        content: new FormControl('', [Validators.required])
      });
    }
    
    emailManager() {
      this.emailCoordinatorGroup.reset();      
      this.emailManagerOpen = true;
      
      this.caseManagerDetails = this.casemanagers.find(x => { return x.description == this.user.recipient_Coordinator });
      this.emailManagerNoEmailShowNotif = this.globalS.isEmpty(this.caseManagerDetails) ? false : true; 
    }
    
    detectChanges(): void{
      this.cd.markForCheck();
      this.cd.detectChanges();
    }
    
    populateCaseStaff(id: any = this.innerValue.id): void{
      this.listS.getcasestaffpopulate(id).subscribe(data =>{
        this.caseStaffArray = data;
        this.detectChanges();
      });
    }
    isOrganization:boolean=false;
    pathForm(token: ProfileInterface) {
      this.currentview = token.view;
      this.populateCaseStaff(token.id);
      
      if (this.globalS.isEmpty(token))
        return;
      
      if (token.view == view.recipient) {
        this.clientS.getprofile(token.name).pipe(
          concatMap(data => {
            this.user = data;
            this.isOrganization=this.user.companyFlag==true;
           

            this.shareS.emitRecipientStatus(this.user);
            
            this.patchTheseValuesInForm(data);
            this.showBudget(this.user);
            this.clientS.getCaldGender(data.uniqueID).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
              this.userForm.patchValue({               
                gender_cald: data.caL_IdentifiesAs
              });
            })
            return this.getUserData(data.uniqueID);
          }),
          concatMap(data => {
            // this.backgroundImage = this.refreshDynamicPicture(this.user);
            
            this.user.addresses = this.addressBuilder(data[0]);
            this.user.contacts = this.contactBuilder(data[1]);
            this.user.casestaff = data[2];
            
            var arr111 = [];
            
            this.activePrograms = (data[3] == null) ? arr111 : data[3];
           
           
            this.detectChanges();
            
            // this.globalS.userProfile = this.user;
            // this.addresses = data[0];
            // this.contacts = data[1];
            
            // this.patchTheseValuesInForm(this.user)
            
            // this.addressForm.setControl('addresses', this.formBuilder.array(this.addressBuilder(data[0]) || []));
            // this.contactForm.setControl('contacts', this.formBuilder.array(this.contactBuilder(data[1]) || []));
            
            // this.tempContactArray = this.contactForm.get('contacts').value;
            if(!this.user.filePhoto){
              this.imgSrc = null;
              this.showAvatar = true;
              this.src = null;
              this.detectChanges();
              return EMPTY;
            }
            return this.staffS.getimages({ directory: this.user.filePhoto })
          })
        ).subscribe(blob => {
          if (blob.isValid) {
            this.showAvatar = false;
            this.defaultURL = 'data:image/png;base64,' + blob.image;
            
            this.imgSrc = this.ds.bypassSecurityTrustUrl(this.defaultURL);
            this.src = this.defaultURL;
            
          } else {
            this.imgSrc = null;
            this.showAvatar = true;
            this.src = null;
          }
          
          this.detectChanges();
        });     
      }
      
      if (token.view == view.staff) {
        
        this.staffS.getprofile(token.name).pipe(
          concatMap(data => {
            if (!data) return EMPTY;
            this.user = data;
            this.user.rating = data.rating ? data.rating.split('*').length - 1 : 0;
            this.patchTheseValuesInForm(data);
            return this.getUserData(data.uniqueID);
          }),
          concatMap(data => {
            
            this.shareS.emitRecipientStatus(this.user);
            //this.src = this.imgSrc;
            // this.backgroundImage = this.refreshDynamicPicture(this.user)
            
            this.user.addresses = this.addressBuilder(data[0]);
            this.user.contacts = this.contactBuilder(data[1]);
            
            this.cd.markForCheck();
            this.cd.detectChanges();
            
            // this.globalS.userProfile = this.user;
            
            // this.addresses = data[0];
            // this.contacts = data[1];
            
            // this.patchTheseValuesInForm(this.user);
            // this.addressForm.setControl('addresses', this.formBuilder.array(this.addressBuilder(data[0]) || []));
            // this.contactForm.setControl('contacts', this.formBuilder.array(this.contactBuilder(data[1]) || []));
            // this.tempContactArray = this.contactForm.get('contacts').value;
            if(!this.user.filePhoto){
              this.imgSrc = null;
              return EMPTY;
            }
            return this.staffS.getimages({ directory: this.user.filePhoto })
          })
        ).subscribe(blob => {
          
          if (blob.isValid) {
            this.showAvatar = false;
            this.defaultURL = 'data:image/png;base64,' + blob.image;
            
            this.imgSrc = this.ds.bypassSecurityTrustUrl(this.defaultURL);
            this.src = this.defaultURL;
            
          } else {
            this.showAvatar = true;
            this.src = null;
            this.imgSrc = null;
          }
          
          this.cd.markForCheck();
          this.cd.detectChanges();
        });
        
      }
      
    }
    
    addressBuilder(data: Array<any>): Array<any> {
      data.forEach(e => {
        e.description = new RemoveFirstLast().transform(e.description).trim()
      });
      return data;
    }
    
    contactBuilder(data: Array<any>): Array<any>{
      data.forEach(e => {
        e.type = new RemoveFirstLast().transform(e.type).trim()
      });
      return data;
    }
    
    getUserData(code: any) {
      return forkJoin([
        this.clientS.getaddress(code),
        this.clientS.getcontacts(code),
        this.timeS.getcasestaff(code),
        this.listS.getProfileActiveprogram(code)
      ]);
    }
    
    cancel(v: number): void {
      
    }
    
    confirm(view: number, data: any): void {
      
      if (view == 1) {
        data = this.activeAdrRowData;
        this.clientS.deleteaddress({
          RecordNumber: data.recordNumber,
          PersonID: data.personID
        }).subscribe(data => {
          if (data.success) {
            this.globalS.sToast('Success', 'Address deleted')
            this.pathForm(this.innerValue);
          }
        })
      }
      
      if (view == 2) {
        data = this.activeRowData;
        this.clientS.deletecontact({
          RecordNumber: data.recordNumber,
          PersonID: data.personID
        }).subscribe(data => {
          if (data.success) {
            this.globalS.sToast('Success', 'Contact deleted')
            this.pathForm(this.innerValue);
          }
        })
      }
      
    }
    
    tabChange(index: number) {
      
      // if (index == 0) {
      //   this.contactIssueGroup.patchValue({
      //     contactIssues: this.user.contactIssues
      //   })
      // }
      
      // if (index == 1) {
        
      //   this.contactIssueGroup.patchValue({
      //     rosterAlerts: this.user.notes
      //   })
      // }
      
      // if (index == 2) {
      //   this.contactIssueGroup.patchValue({
      //     runsheetAlerts: this.user.specialConsiderations
      //   })
      // }

      this.saveAllNotes();
    }
    
    
    
    next(){
      this.current++;
    }
    
    pre(){
      this.current--;
    }
    
    addCaseStaff(){
      const { coordinator, notes, program } = this.caseStaffGroup.value;
      
      this.listS.postcasestaff({
        personId: this.innerValue.id,
        notes: notes,
        caseManagerId: coordinator,
        program: program
      }).subscribe(data => {
        this.globalS.sToast('Success', 'Case Staff is added');
        this.populateCaseStaff();
        this.addCaseStaffModal = false;
        
        this.detectChanges();
      });
    }
    
    handleCancel(): void {
      // this.typeSub$.unsubscribe();
      
      this.addressForm.reset();
      this.contactForm.reset();
      
      this.selectedRow = 0;
      this.selectedAdrRow = 0;
      this.editModalOpen = false;
      this.profileStaffModal = false;
      this.profileStaffOptionsModal = false;
      this.profileStaffPreferredModal = false;
      this.profileRecipientOptionsModal = false;
      this.changeProfilePictureModal = false;
      this.loadPhoto = false;
      this.addCaseStaffModal = false;
      this.src = '';
      
      
    }
    
    formatDate(data: any): string {
      let year = data.get('year').value;
      let month = this.months.indexOf(data.get('month').value) + 1;
      let day = data.get('day').value;
      
      if (year == null || day == null || month == 0)
        return null;
      
      return year + '/' + month + '/' + day + ' ' + '00' + ':' + '00' + ':' + '00';
    }
    
    formatContact(contactForm: FormGroup): Array<PhoneFaxOther> {
      let temp: Array<PhoneFaxOther> = [];
      
      const { id, type, details, personId, primaryPhone,privatecontact } = contactForm.value;
      
      let pf: PhoneFaxOther = {
        RecordNumber: id,
        Type: type,
        Detail: details,
        PersonID: personId,
        PrimaryPhone: primaryPhone,
        Private:privatecontact
      }
      
      
      temp.push(pf);
      return temp;
    }
    
    convertRatingToString(rating: number): string {
      if (!rating) rating = 0;
      var ratingStr = "";
      for (var a = 0; a < rating; a++){
        ratingStr += '*';
      }
      return ratingStr;
    }
    
    SaveAll_Changes(type:any){
      this.saveAllNotes();
      this.saveProfile();
      this.saveCald_Gender();
    }
    SaveAll_Staff(type:any){
      this.savePreferredNotes();
      this.saveProfile(); 
    }
    
    saveNotes() {
      const notes = this.userForm.value;
      let staff: Staffs = {
        accountNo: this.user.accountNo,
        uniqueID: this.user.uniqueID,
        caldStatus: notes.caldStatus,
        visaStatus:this.user.visaStatus,
        cstda_Indiginous: notes.indigStatus,
        cstda_DisabilityGroup: notes.primaryDisability,
        cstda_OtherDisabilities:this.otherDisabilityCheckBoxString,
        contactIssues: notes.note
      }
      this.subscriptionArray.push(this.staffS.updatedisabilitystatus(staff));
      this.processSubscriptions();
    }
    
    saveProfile() {
      
      if (this.userForm.dirty) {
        let data = this.userForm.value;
       
        
        if (this.innerValue.view == 'staff') {
          let birthdate = format(new Date(data.dob),'yyyy/MM/dd');
          let user: Staffs = {
            accountNo: this.innerValue.name,
            firstName: data.firstName,
            middleNames: data.middleNames,
            lastName: data.surnameOrg,
            gender: data.gender,
            title: data.title,
            dob: birthdate,
            rating: this.convertRatingToString(data.rating),
            pan_Manager: data.casemanager,
            category: data.type,
            stf_Department: data.stf_Department,
            staffGroup: data.jobCategory,
            subCategory: data.adminCategory,
            staffTeam: data.team,
            serviceRegion: data.serviceRegion,
            ubdMap: data.gridNo,
            dLicence: data.dLicense,
            vRegistration: data.mvReg,
            nRegistration: data.nReg,
            caseManager: data.isCaseLoad,
            isRosterable: data.isRosterable,
            caldStatus: data.caldStatus ? data.caldStatus : null,
            visaStatus:data.visaStatus ? data.visaStatus : null,
            cstda_Indiginous: data.indigStatus ? data.indigStatus : null,
            cstda_DisabilityGroup:data.primaryDisability?data.primaryDisability:null,
            cstda_OtherDisabilities: data.cstda_OtherDisabilities ? data.cstda_OtherDisabilities : this.otherDisabilityCheckBoxString,
            emailTimesheet: data.isEmail,
            excludeFromConflictChecking:data.excludeFromConflictChecking,
            preferredName: data.preferredName,
            alias :data.Alias
          }
          
          this.subscriptionArray.push(this.staffS.updateusername(user));
        }
        
        if (this.innerValue.view == 'recipient') {
          let birthdate = format(new Date(data.dateOfBirth),'yyyy/MM/dd');

          let user: Recipients = {
            accountNo: this.innerValue.name,
            firstName: data.firstName,
            middleNames: data.middleNames,
            surnameOrg: data.surnameOrg,
            gender: data.gender,
            title: data.title,
            dateOfBirth: birthdate,
            recipient_Coordinator: data.casemanager,
            agencyIdReportingCode: data.file1,
           
            branch: data.branch,
            agencyDefinedGroup: data.serviceRegion,
            ubdMap: data.subCategory,
            preferredName: data.preferredName,
            mobility:data.mobility,
            carerStatus : data.carerStatus,
            ndisNumber :data.ndisNumber,
            urNumber : data.urNumber,
            alias :data.alias
          }
          
          this.subscriptionArray.push(this.clientS.updateusername(user));
        }
        
        this.processSubscriptions();
      }
    }
    saveCald_Gender(){
      
      let data = {
        id : this.user.uniqueID,
        gender: this.userForm.value.gender_cald
    }

    this.clientS.postCaldGender(data).pipe(takeUntil(this.unsubscribe)).subscribe(data => {})
  }
    formatAddress(addressForm: FormGroup): Array<NamesAndAddresses> {
      const { pcodesuburb, address1,address2, description, id, personId, primaryAddress } = addressForm.value;
      
      let pcode = /(\d+)/g.test(pcodesuburb) ? pcodesuburb.match(/(\d+)/g)[0].trim() : "";
      let suburb = /(\D+)/g.test(pcodesuburb) ? pcodesuburb.match(/(\D+)/g)[0].trim() : ""
      let state = suburb && suburb.split(',').length > 1 ? suburb.split(',')[1].trim() : '';
      
      let temp: Array<NamesAndAddresses> = [];
      
      let na: NamesAndAddresses = {
        Suburb: suburb && suburb.split(',').length > 0 ? suburb.split(',')[0] : suburb,
        PostCode: pcode,
        Stat: state,
        Description: description,
        Address1: address1,
        Address2: address2,
        RecordNumber: id,
        PersonID: personId,
        PrimaryAddress: primaryAddress
      }
      
      temp.push(na);
      return temp;
    }
    
    editProfile(): void {
      let data = this.userForm.value;
      let birthdate = this.formatDate(this.userForm);
      
      if (this.innerValue.view == 'staff') {
        let user: Staffs = {
          accountNo: this.innerValue.name,
          firstName: data.firstName,
          middleNames: data.middleNames,
          lastName: data.surnameOrg,
          visaStatus:data.visaStatus, 
          gender: data.gender,
          title: data.title,
          dob: birthdate,
          rating: data.rating,
          pan_Manager: data.casemanager,
          category: data.type,
          stf_Department: data.stf_Department,
          staffGroup: data.jobCategory,
          subCategory: data.adminCategory,
          staffTeam: data.team,
          serviceRegion: data.serviceRegion,
          ubdMap: data.gridNo,
          dLicence: data.dLicense,
          vRegistration: data.mvReg,
          nRegistration: data.nReg,
          caseManager: data.isCaseLoad,
          isRosterable: data.isRosterable,
          emailTimesheet: data.isEmail,
          excludeFromConflictChecking:data.excludeFromConflictChecking,
          preferredName: data.preferredName
        }
        this.subscriptionArray.push(this.staffS.updateusername(user));
      }
      
      if (this.innerValue.view == 'recipient') {
        let user: Recipients = {
          accountNo: this.innerValue.name,
          firstName: data.firstName,
          middleNames: data.middleNames,
          surnameOrg: data.surnameOrg,
          gender: data.gender,
          title: data.title,
          dateOfBirth: birthdate,
         
          recipient_Coordinator: data.casemanager,
          agencyIdReportingCode: data.file1,
          urNumber: data.urNumber,
          branch: data.branch,
          agencyDefinedGroup: data.serviceRegion,
          ubdMap: data.subCategory,
          mobility:data.mobility,
          carerStatus : data.carerStatus,
          ndisNumber :data.ndisNumber
        }
        this.subscriptionArray.push(this.clientS.updateusername(user));
      }
      this.processSubscriptions();
    }
    
    processSubscriptions() {
      
      this.loading = true;
      
      forkJoin(this.subscriptionArray).subscribe(
        data => {
          this.subscriptionArray = [];
          this.globalS.sToast('Success', 'Profile Updated!');
          this.pathForm(this.innerValue);
          this.handleCancel();
          this.closeDrawer();
          // let result = data.filter(x => x.success == false);
          // if (result.length > 0)
            //     this.resultToast.fail = true;
          // else {
          //     this.showAlert();
          //     this.pathForm();
          // }
          
          // this.resetForms();
        }, err => {
          this.loading = false
        }, () => {
          
          this.loading = false;
        }
      )
    }
    
    
    
    edit(): void {
      
      
      if (this.window == 1) {
        this.loading = true;
        this.clientS.updateuseraddress(this.formatAddress(this.addressForm))
        .subscribe(data => {
          if (data.success) {
            this.globalS.sToast('Success', 'Address Updated!');
            this.pathForm(this.innerValue);
          }
        }, (err) => {
          
        }, () => {
          // this.typeSub$.unsubscribe();
          this.loading = false;
          this.editModalOpen = false;
          this.handleCancel();
        })
      }
      
      if (this.window == 2) {
        this.clientS.updateusercontact(this.formatContact(this.contactForm)).subscribe(data => {
          if (data.success) {
            this.globalS.sToast('Success', 'Contact Updated!');
            this.pathForm(this.innerValue);
          }
        }, (err) => {
          
        }, () => {
          // this.typeSub$.unsubscribe();
          this.loading = false;
          this.handleCancel();
        });
      }
      
    }
    
    editProfileOpen() {
      this.editProfileDrawer = true;
    }
    
    addAddressOpen() {
      this.addAddressDrawer = true;
      this.selectedAdrRow = 0;
      this.addressForm.reset({
        description: null,
        address: null,
        pcodesuburb: null,
      });
      // this.POPULATE_ADDRESS();
    }
    
    addContactOpen() {
      this.addContactDrawer = true;
      this.selectedRow = 0;
      this.contactForm.markAsPristine();
      
      this.contactForm.reset({
        id: '',
        type: null,
        details: null,
        personId: '',
        primaryPhone: false,
        privatecontact:false,
      });
      this.POPULATE_CONTACTS();
    }
    
    addContact() {
      
      
      for (const i in this.contactForm.controls) {
        this.contactForm.controls[i].markAsDirty();
        this.contactForm.controls[i].updateValueAndValidity();
      }
      
      if (!this.contactForm.valid)
        return;
      
      
      this.contactForm.patchValue({
        personId: this.user.uniqueID,
        id: -1
      });
      
      
      this.subscriptionArray.push(this.clientS.addcontact(this.formatContact(this.contactForm)));
      this.processSubscriptions();
    }
    
    addAddress(): void {
      
      for (const i in this.addressForm.controls) {
        this.addressForm.controls[i].markAsDirty();
        this.addressForm.controls[i].updateValueAndValidity();
      }
      
      if(this.globalS.isEmpty(this.addressForm.value.pcodesuburb)){
        this.addressForm.controls.pcodesuburb.setErrors({'incorrect': true});
      }
      
      if (!this.addressForm.valid && this.globalS.isEmpty(this.addressForm.value.pcodesuburb)){
        this.globalS.wToast('Warning','All items are required');      
        return;
      }
      
      
      this.addressForm.patchValue({
        personId: this.user.uniqueID,
        id: -1,
        primaryAddress: false
      });
      
      this.subscriptionArray.push(this.clientS.addaddress(this.formatAddress(this.addressForm)));
      this.processSubscriptions();
      
    }
    
    editAddressOpen(add: any): void {
      
      let address = this.activeAdrRowData

      address = this.user.addresses[this.selectedAdrRow];

      this.editModalOpen = true;
      
      this.window = 1;

      this.addressForm.reset();

      this.addressForm.patchValue({
        id: address.recordNumber,
        description: address.description,
        address: address.address1,
        pcodesuburb: `${address.postCode} ${address.suburb ? address.suburb.trim() : ''} ${address.stat ? `, ${address.stat}` : ''}`,
        personId: address.personID,
        primaryAddress: address.primaryAddress
      });
      
      // this.POPULATE_ADDRESS();
    }
    
    editContactOpen(cnt: any): void {
      
      this.editModalOpen = true;
      this.window = 2;
      let contact;
      
      contact = this.user.contacts[this.selectedRow];
      
      this.contactForm.patchValue({
        id: contact.recordNumber,
        type: contact.type,
        details: contact.detail,
        personId: contact.personID,
        primaryPhone: contact.primaryPhone,
        privatecontact:contact.private
      });
      
      this.detectChanges();           
      
      this.typeSub$ = this.contactForm.get('type').valueChanges
      .subscribe(x => this.contactForm.patchValue({ details: null }));
      
    }
    
    POPULATE_ADDRESS(): void {
      this.clientS.getaddresstype()
      .subscribe(data => {
        this.addressArray = data.map(x => {
          return (new RemoveFirstLast().transform(x.description)).trim().toUpperCase();
        });
      })
      this.listS.getpostcodes()
      .subscribe(data=> {
        this.postcodesArray = data.map(x=>{
          return (new RemoveFirstLast().transform(x)).trim().toUpperCase();
        })
      })

    }
    
    POPULATE_CONTACTS(): void {
      this.clientS.getcontacttype().subscribe(data => {
        this.contactsArray = data.map(x => {
          return (new RemoveFirstLast().transform(x.description)).trim().toUpperCase();
        });
      })
    }
    
    //From ControlValueAccessor interface
    writeValue(value: any) {
      if (value != null && !_.isEqual(value, this.innerValue)) {
        this.innerValue = value;
        this.pathForm(this.innerValue);
        // this.tab = 1;
      }
    }
    
    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
      this.onChangeCallback = fn;
    }
    
    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
      this.onTouchedCallback = fn;
    }
    
    POPULATE_OTHER_DROPDOWNS() {
      forkJoin([
        this.listS.getlistbranches(),
        this.listS.getliststaffgroup(),
        this.listS.getliststaffadmin(),
        this.listS.getliststaffteam(),
        this.listS.getlistcasemanagers(),
        this.listS.getserviceregion(),
        this.listS.getlistdisabilities(),
        this.listS.getlistindigstatus(),
        
      ]).subscribe(data=> {
        this.dropDowns = {
          branchesArr: data[0],
          jobCategoryArr: data[1],
          adminCategoryArr: data[2],
          teamArr: data[3],
          managerArr: data[4],
          serviceRegionArr: data[5],
          disabilitiesArr: data[6],
          indigenousArr: data[7],
        }
      });
      
    }
    log(value: string[]): void {
    }
    POPULATE_CASE_STAFF(){
      this.current = 0;
      this.caseStaffGroup.reset();
      
      this.coordinator$ = this.listS.getcasestafflist(this.innerValue.id);
      // this.programs$ = this.listS.getcasestaffprograms();
      this.programs$ = this.timeS.getprogrampackages(this.user.uniqueID);
      
    }
    
    POPULATE_DATE_DROPDOWNS() {
      this.months = months;
      this.years = this.globalS.year();
      this.days = days;
      
      forkJoin([
        this.clientS.getmanagers(),
        this.listS.getlistcasemanagers()
      ]).subscribe(data => {
        this.casemanagers = data[0];
      });
    }
    
    closeDrawer() {
      this.editProfileDrawer = false;
      this.addAddressDrawer = false;
      this.addContactDrawer = false;
    }
    
    messageFinal: any;
    
    sendMail() {
      for (const i in this.emailCoordinatorGroup.controls) {
        this.emailCoordinatorGroup.controls[i].markAsDirty();
        this.emailCoordinatorGroup.controls[i].updateValueAndValidity();
      }
      
      if (!this.emailCoordinatorGroup.valid)
        return;
      
      const { title, firstName, surnameOrg, accountNo } = this.user;
      const { subject, content } = this.emailCoordinatorGroup.value;
      const { detail, description } = this.caseManagerDetails;
      
      const emailData = {
        Subject: subject,
        Content: content,
        RecipientName: `${title} ${firstName} ${surnameOrg}(${accountNo})`,
        CCAddresses: [],
        FromAddresses: [],
        ToAddresses: [{
          Name: description,
          Address: ''
        }]
      }
      
      if(!emailData?.ToAddresses[0].Address){
        this.message.error('Your coordinator doesnt have an email').onClose!;
        return;
      }
      
      this.message.loading('Processing', { nzDuration: 2000 }).onClose!.pipe(       
        switchMap(() => {
          this.messageFinal = this.message.loading('Sending Email', { nzDuration: 0 }).messageId;
          return this.clientS.postemailcoordinator(emailData)
        })
      ).subscribe((x) => {      
        this.message.remove(this.messageFinal);          
        this.emailManagerOpen = false;
        if(x) {
          return this.message.success('Email Sent', { nzDuration: 1000 }).onClose!
        } else {
          return this.message.error('An error occured!').onClose!
        }
      }, error =>{
        this.message.remove(this.messageFinal);
        this.emailManagerOpen = false;
      });
    }
    
    saveAllNotes() {
      const { sqlId, uniqueID } = this.user;
      const { contactIssues,  rosterAlerts, runsheetAlerts,klm  } = this.contactIssueGroup.value;
      this.alert_disabled=true;
      
      this.timeS.updatecontactrosterrunsheet({
        contactIssues,
        rosterAlerts,
        runsheetAlerts,
        uniqueId: uniqueID,
        KLMArrangements:klm
      }).subscribe(data => {
        //this.globalS.sToast('Success', 'Contact Issues Updated');
        
      });  
    }
    savePreferredNotes() {
      const {uniqueID } = this.user;
      const {contactIssues} = this.contactIssueGroup.value;

      this.alert_disabled=true;
      
      this.timeS.updatecontactrosterrunsheetstaff({
        contactIssues,
        uniqueId: uniqueID
      }).subscribe(data => {
        //this.globalS.sToast('Success', 'Contact Issues Updated');
      }); 
    }



    saveAlerts() {
      const { sqlId, uniqueID } = this.user;
      const { contactIssues,  rosterAlerts, runsheetAlerts  } = this.contactIssueGroup.value;
      this.alert_disabled=true;
      
      // this.timeS.updatecontactrosterrunsheet({
      //   contactIssues,
      //   rosterAlerts,
      //   runsheetAlerts,
      //   uniqueId: uniqueID
      // }).subscribe(data => {
      //   //this.globalS.sToast('Success', 'Contact Issues Updated');
        
      // });  
      
      
      if (this.tabIndex == 0) {
        
        if(this.getPermisson(56) == '1'){
          this.timeS.updatealertsissues({
            sqlId: sqlId,
            issueType: 'ci',
            notes: this.contactIssueGroup.value.contactIssues
          }).subscribe(data => {
            //this.globalS.sToast('Success', 'Contact Issues Updated');
          });
        }else{
          this.globalS.iToast('info', 'You Dont have the permission to update please Contact Administrator');
        }
        this.contactIssueGroup.markAsPristine();
      }
      
      if (this.tabIndex == 1) {
        if(this.getPermisson(54) == '1'){
          this.timeS.updatealertsissues({
            sqlId: sqlId,
            issueType: 'roa',
            notes: this.contactIssueGroup.value.value
          }).subscribe(data => {
            this.globalS.sToast('Success', 'Roster Alert Updated');        
          })
        }else{
          this.globalS.iToast('info', 'You Dont have the permission to update please Contact Administrator');
        }
        this.contactIssueGroup.markAsPristine();
      }
      
      if (this.tabIndex == 2) {
        if(this.getPermisson(55) == '1'){
          this.timeS.updatealertsissues({
            sqlId: sqlId,
            issueType: 'rua',
            notes: this.contactIssueGroup.value.value
          }).subscribe(data => {
            this.globalS.sToast('Success', 'Runoff Alert Updated');
          });
        }else{
          this.globalS.iToast('info', 'You Dont have the permission to update please Contact Administrator');
        }
        this.contactIssueGroup.markAsPristine();
      }
      
    }
    
    changeProfilePicture() {
      this.changeProfilePictureModal = true;
      this.src = this.defaultURL;
    }
    
    errorUrl(event: any) {
      this.imgSrc = '';
      this.showAvatar = true;
    }
    
    src: any;
    uploadChange(e: Event | any) {
      e.preventDefault();
      var fileLen = e.target.files.length;
      var file = e.target.files;
      
      if (fileLen == 0) {
        this.globalS.wToast('Warning', 'Select a picture');
        return;
      }
      
      if (fileLen > 1) {
        this.globalS.wToast('Warning', 'File limit exceeded');
        return;
      }
      
      this.src = file[0];
    }
    
    private imgBlobData: any;
    
    imgBLOB(file: any) {
      this.imgBlobData = file;
    }
    
    loadPhoto: boolean = false;
    uploadPicture() {
      
      this.loadPhoto = true;
      
      
      var uploadParams = this.imgBlobData.formData
      
      this.uploadS.uploadProfilePicture(uploadParams)
      .subscribe(blob => {
        this.defaultURL = 'data:image/png;base64,' + blob.image;
        
        this.imgSrc = this.ds.bypassSecurityTrustUrl(this.defaultURL);
        this.globalS.sToast('Success', 'Profile picture updated');
        this.showAvatar = false;
        
        this.handleCancel();
      });
    }
    
    isEmail(data: any){
      return data == "EMAIL" || data == "EMAIL-SMS"
    }
    
    isMobile(data: any){
      return data == 'MOBILE';
    }
    
    isPhoneFax(data: any){
      return data == 'FAX' || data == 'HOME' || data == 'WORK';
    }
    
    toMap(address: any){
      var adds = [];
      var addr = new Address(address.postCode, address.address1, address.suburb, address.stat);
      var addres = adds.concat('/',addr.getAddress());
      window.open(`https://www.google.com/maps/dir${addres.join('')}`,'_blank');
      return false;
    }
    
    
    deleteStaff(data){
      this.listS.deletecasestaff(data.recordNumber).subscribe(data => {
        this.populateCaseStaff();
        this.globalS.sToast('Success','Case Staff deleted');
      })
    }
    getPermisson(index:number){
      var permissoons = this.globalS.getRecipientRecordView();
      return permissoons.charAt(index-1);
    }
    getStaffPermisson(index:number){
      var permissoons = this.globalS.getStaffRecordView();
      return permissoons.charAt(index-1);
    }
    selectedItemGroup(data:any, i:number){
      this.selectedRow=i;
      this.activeRowData=data;
    }
    selectedaddress(data:any,i:number){
      this.selectedAdrRow=i;
      this.activeAdrRowData=data;
    }
    trackByFn(index, item) {
      return item.id;
    }
    
    
    
    loadAliasForm(){
      
      
      
      //this.viewAliasForm=true;
      
      this.loading2=true;
      let sql1=`  SELECT RecordNumber, Name AS PastName, [group],notes FROM HumanResources WHERE PersonID = '${this.user.uniqueID}' AND [Type] = 'RECIPIENTPASTNAME' ORDER BY Name`
      let sql2=` SELECT  * FROM HumanResources WHERE PersonID = '${this.user.uniqueID}' AND [Type] = 'RECIPIENTALIAS' ORDER BY Name`
      
    
          forkJoin([
            this.listS.getlist(sql1),
            this.listS.getlist(sql2)
            
          ])
          .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.lstPastNames = data[0];
            this.lstAliases = data[1];              
            this.loading2=false;
          
            this.viewAliasForm=true;
            this.cd.detectChanges();
            error=>{
              this.loading2=false;
              this.viewAliasForm=true;
              this.cd.detectChanges();
            }
            });
      setTimeout(() => {
      }, 500);
        
        
        //initial load
        
        
      }
      
      savePastName(){
        let sql='';
        let val= this.frmPastName.value;
        if (val.id>0){
          sql=` Update  HumanResources set [Name] = N'${val.pastName}',Notes=N'${val.notes}',Creator=N'${this.token.user}' 
          WHERE RecordNumber=${val.id}` ;
          
        }else{
          sql=` INSERT INTO HumanResources (PersonID,[Group],[Type],[Name],Notes,Creator) 
          VALUES (N'${this.user.uniqueID}',N'${val.group}','RECIPIENTPASTNAME',N'${val.pastName}',N'${val.notes}',N'${this.token.user}')
          `
        }
        this.listS.executeSql(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.frmPastName.reset();
          this.ViewPastName=false;
          
          this.loadAliasForm();
          error=>{this.ViewPastName=false;}
        });
        
        
      }
      selectedNameGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
      }
      selectedEditNameGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
        this.editPastName();
      }
      editPastName(){
        this.ViewPastName=true;
        this.frmPastName.patchValue({
          id: this.activeRowData.recordNumber,
          pastName: this.activeRowData.pastName,
          notes: this.activeRowData.notes,
          group: this.activeRowData.group
        });
      }
      
      saveOtherName(){
        let sql='';
        let val= this.frmOtherName.value;
        if (val.id>0){
          sql=` Update  HumanResources set [Name] = N'${val.Name}',Address1=N'${val.Address1}',Address2=N'${val.Address2}' ,
          [Suburb] = N'${val.Suburb}',[Postcode] = N'${val.Postcode}' , [Phone1]=N'${val.Phone1}', [Phone2]=N'${val.Phone2}',
          [Mobile]=N'${val.Mobile}', [Fax]=N'${val.Fax}', [Email]=N'${val.Email}',
          [Notes]=N'${val.notes}',Creator=N'${this.token.user}' 
          WHERE RecordNumber=${val.id}` ;
          
        }else{
          sql=`insert into HumanResources(PersonID,[Group],[Type],[Name],Address1,Address2,Suburb,Postcode,[Phone1],[Phone2],[Mobile],[Fax],[Email],Notes,ReminderProcessed,Creator)
          VALUES (N'${this.user.uniqueID}',N'${val.group}','RECIPIENTALIAS',N'${val.Name}',N'${val.Address1}',N'${val.Address2}'
          ,N'${val.Suburb}' ,N'${val.Postcode}' ,N'${val.Phone1}',N'${val.Phone2}',N'${val.Mobile}' ,N'${val.Fax}',N'${val.Email}',N'${val.notes}',0,N'${this.token.user}')
          `
        }
        this.listS.executeSql(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.frmOtherName.reset();
          this.ViewOtherName=false;
          
          this.loadAliasForm();
          error=>{this.ViewOtherName=false;}
        });
        
        
      }
      editOtherName(){
        this.ViewOtherName=true;
        this.frmOtherName.patchValue({
          id: this.activeRowData.recordNumber,
          Name: this.activeRowData.name,     
          group: this.activeRowData.group,
          Address1: this.activeRowData.address1,
          Address2: this.activeRowData.address2,
          Suburb: this.activeRowData.suburb,
          Postcode: this.activeRowData.postcode,
          Phone1: this.activeRowData.phone1,
          Phone2: this.activeRowData.phone2,
          Mobile: this.activeRowData.mobile,
          Fax: this.activeRowData.fax,
          Email: this.activeRowData.email,
          notes: this.activeRowData.notes
        });
      }
      confirmName(view: number, data: any): void {
        
        let sql='';
        
        sql=`DELETE FROM HumanResources WHERE RecordNumber=${this.activeRowData.recordNumber}`;
        
        
        this.listS.executeSql(sql).subscribe(data => {
          
          this.globalS.sToast('Success', 'Address deleted')
          
          this.loadAliasForm();
          
        })
      }
      otherDisabilitychangeCheckbox(index: number, val: boolean){
        let temp = Object.assign([], this.otherDisabilityCheckBoxString);
        temp.splice(index,1, val ? '1' : '0')
        this.otherDisabilityCheckBoxString = temp.join('')
      }
      openBudgetModel(){        
        this.ViewBudgetModel = true;
        this.loading2=true;
          this.timeS.getRecipientBudget(this.user.uniqueID).subscribe(data => {
            this.budgetData = data;
            this.ViewBudgetModel = true;
            this.loading2=false;
            this.cd.detectChanges()
  
          });
          //User=" + values.User + "&Password=" + values.Password + "&PersonId=" + values.PersonId + "&AccountNo="
         
        
       

       
      }
    

      
     
      contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, data:any): void {
        this.activeRowData = data;
       // this.selectedRowIndex = this.tableData.indexOf(data);
        this.nzContextMenuService.create($event, menu);
      }
      menuclick(event:any,index:number){
        if (index==1){
         // this.router.navigate([`/recipient/intake/branches`]);

         let data = { program: this.budgetData[0].program, open: true};
          
          this.ViewGrpah.next(data);
        }
        if (index==2){
         // this.router.navigate([`/admin/recipient/package`]);
          this.ViewPackage.next(this.activeRowData);
        }
      }
    


      
    }