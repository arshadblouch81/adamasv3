using System;
using System.Globalization;

namespace Adamas.Models.Modules{
    public class PoDocument {

        public string Program { get; set; }
        public string Creator { get; set; }
        public string Filename { get; set; }
        public string Title { get; set; }
        
        public string PersonID { get; set; }
        public string FileClass { get; set; }
        public string Category { get; set; }
        public string DocumentType { get; set; }        
        public string OriginalLocation { get; set; }
        public string Editer { get; set; }
        public string Paramaters { get; set; }
        public string DocumentGroup { get; set; }
        public string SubCat { get; set; }        
        public string SubID { get; set; }
        public string D_ID { get; set; }
        public string AlarmDate { get; set; }
        public string AlarmText { get; set; }
    }
}