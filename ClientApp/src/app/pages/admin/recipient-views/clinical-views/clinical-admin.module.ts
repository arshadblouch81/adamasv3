import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';

import {
  PhysicalHealth,
  ClinicalDiagnose,
  ClinicalProcedure,
  ClinicalMedication,
  ClinicalReminder,
  ClinicalAlert,
  ClinicalNote,
  ClinicalHealthConditions,
  ClinicalNursingDiagnose,
  ClinicalVaccinations,
  ClinicalHistoryComponent,
  ClinicalPrimaryIssues,
  ClinicalActionPlans,
  ClinicalCareerStatus,
  ClinicalDailyLiving,
  ClinicalCurrentServices,
  ClinicalMedicalDiagnose,
  ClinicalOtherIssues, ClinicalMentalHealth, ClinicalHealthBehaviours
} from './index';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'history',
    pathMatch: 'full'
  },
  {
    path: 'physical-health',
    component: PhysicalHealth
  },
  {
    path: 'diagnose',
    component: ClinicalDiagnose
  },
  {
    path: 'nursing-diagnose',
    component: ClinicalNursingDiagnose
  },
  {
    path: 'medical-diagnose',
    component: ClinicalMedicalDiagnose,
  },
  {
    path: 'procedure',
    component: ClinicalProcedure
  },
  {
    path: 'medication',
    component: ClinicalMedication
  },
  {
    path: 'reminder',
    component: ClinicalReminder
  },
  {
    path: 'alert',
    component: ClinicalAlert,
  },
  {
    path: 'note',
    component: ClinicalNote,
  },
  {
    path: 'health-conditions',
    component: ClinicalHealthConditions,
  },
  {
    path: 'vaccinations',
    component: ClinicalVaccinations,
  },
  {
    path: 'history',
    component: ClinicalHistoryComponent,
  },
  {
    path: 'primary-issues',
    component: ClinicalPrimaryIssues,
  },
  {
    path: 'other-issues',
    component: ClinicalOtherIssues,
  },
  {
    path: 'current-services',
    component: ClinicalCurrentServices,
  },
  {
    path: 'daily-living',
    component: ClinicalDailyLiving,
  },
  {
    path: 'career-status',
    component: ClinicalCareerStatus,
  },
  {
    path: 'action-plans',
    component: ClinicalActionPlans,
  },
  {
    path: 'mental-health',
    component: ClinicalMentalHealth,
  },
  {
    path: 'health-behaviours',
    component: ClinicalHealthBehaviours,
  }
  
  
];

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,

  ],
  declarations: [

  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class ClinicalAdminModule {}

