namespace Adamas.Models.Modules{
    public class DayManager {
        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string DmType { get; set; }
    }
}