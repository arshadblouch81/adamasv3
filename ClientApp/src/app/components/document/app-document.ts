import { Component, OnChanges, OnInit, OnDestroy, SimpleChanges, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService, ListService, UploadService } from '@services/index';
import { ShareService } from '@services/share.service';
import { TimeSheetService } from '@services/timesheet.service';
import format from 'date-fns/format';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface NewDocument{
    file: string,
    newFile: string,
    path: string
}

@Component({
  selector: 'app-document',
  templateUrl: './app-document.html',
  styleUrls: ['./app-document.css']
})
export class AppDocument implements OnInit, OnChanges, OnDestroy {

  @Input() open: any;
  @Input() user: any;
  @Output() callback= new EventEmitter<any>();
  @Input() viewData:Subject<any>=new Subject();

  document: any;
  isVisible: boolean = false;
  index: number = 0;
  addOrEdit: boolean = false;
  dateRange: any;

  selectedValue: any;
  showDocProperties:boolean = false;
  dateFormat: string = 'dd/MM/yyyy';
  categories:Array<any> = [];
  classifications:Array<any> = [];
  caredomains:Array<any> = [];
  programs:Array<any> = [];
  disciplines:Array<any> = [];
  topBelow = { top: '40px' }
  inputForm: FormGroup;
  tocken:any;
  postLoading:boolean;
  private unsubscribe: Subject<void> = new Subject();
  fileObject: NewDocument;
  constructor( private timeS: TimeSheetService,
    private sharedS: ShareService,
    private listS: ListService,
    private router: Router,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private uploadS: UploadService
)
     { }

  ngOnInit(): void {
    this.buildForm();
    this.tocken=this.globalS.decode();
    this.listDropDowns();
    this.viewData.subscribe(data => {
       if (data==null) return;
       this.isVisible=true;
        this.document=data;      
        this.showEditModal(data);
      
        this.cd.detectChanges();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      console.log(property)
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
          this.isVisible = true;
         
      }
    }
  }

  ngOnDestroy(){
      
  }


  handleCancel(){
    this.isVisible = false;
  }

  handleOk(){
    
  }

  view(index: any){
    this.index = index;
  }

  buildForm() {
    this.inputForm = this.formBuilder.group({
        docID:0,
        title:'',            
        discipline :'',
        program :'',
        caredomain:'',
        classification:'',
        category:'',
        reminderDate: this.currentDate(),
        reminderText:'',
        notes:'' ,
        publishToApp:false           
    });
}
currentDate(){
    return new Date();
}

 
listDropDowns() {
    
    if (this.user==null)return;

let sql=`select distinct [Program]  FROM RecipientPrograms WHERE RecipientPrograms.PersonID = '${this.user.id}' ORDER BY [Program]`;


let sql2=` select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;


let sql3=`select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;

let sql4=` select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'FILECLASS' AND ISNULL(EndDate, '') = '' ORDER BY [Description] `;

let sql5=`select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DOCCAT' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;


    forkJoin([
        this.listS.getlist(sql),
        this.listS.getlist(sql2),
        this.listS.getlist(sql3),
        this.listS.getlist(sql4),
        this.listS.getlist(sql5)                
    ]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.programs = data[0].map(x=>x.program);
        this.caredomains = data[1].map(x=>x.description);
        this.disciplines= data[2].map(x=>x.description);
        this.classifications= data[3].map(x=>x.description);
        this.categories= data[4].map(x=>x.description);

        this.caredomains.push('VARIOUS');
        this.cd.detectChanges();
        
    });

    setTimeout(() => {   },1000)


}
save(){

    if (this.addOrEdit==true){
        this.update_document();
        return;
    }
    let  input = this.inputForm.value;

    var inp = {
       
        PersonID: this.user.id,
        OriginalFileName: this.fileObject.file,
        NewFileName: this.fileObject.newFile,
        Path:  this.fileObject.path,
        Title: input.title,
        Program: input.program,
        Discipline:  input.discipline,
        CareDomain:  input.caredomain,
        Classification:  input.classification,
        Category:  input.category,
        ReminderDate:  format(new Date(input.reminderDate),'yyyy/MM/dd'), 
        ReminderText:  input.reminderText,
        PublishToApp:  input.publishToApp,
        User:  this.tocken.user,
        Notes:  input.notes,
        DocID:0
    };
    this.postLoading = true;
    this.uploadS.postdocumentstafftemplate(inp).subscribe(data => {
        this.globalS.sToast('Success','Document has been added');
       
        this.callback.emit(input);
        this.handleCancel();
        
    }, (err) =>{
        this.postLoading = false;
        this.globalS.eToast('Error', err.error.message);
    });
}

update_document(){
   
  

   let  input = this.inputForm.value;
    var inp = {
       
        PersonID: this.user.id,
        DocID: input.docID,
        Title: input.title,
        Program: input.program,
        Discipline:  input.discipline,
        CareDomain:  input.caredomain,
        Classification:  input.classification,
        Category:  input.category,
        ReminderDate:  format(new Date(input.reminderDate),'yyyy/MM/dd'), 
        ReminderText:  input.reminderText,
        PublishToApp:  input.publishToApp,
        User:  this.tocken.user,
        Notes:  input.notes

    };

    this.timeS.updateDocument(inp).subscribe(data => {
     
        this.globalS.sToast('Success','Document has been updated');
        this.callback.emit(input);
        this.handleCancel();
        this.cd.detectChanges();
       
    }, (err) =>{
        
        this.globalS.eToast('Error', err.error.message);
    });


}


  showEditModal(data: any) {

    this.inputForm.reset();   
    this.addOrEdit=true;
   
  this.inputForm.patchValue( {
        docID: data.docID,
        title:data.title,            
        discipline : data.discipline,
        program : data.program,
        caredomain:data.careDomain || 'VARIOUS',
        classification: data.classification,
        category: data.category,
        reminderDate: data.alarmDate,
        reminderText: data.alarmText,
        notes: data.notes  ,
        publishToApp:data.publishToApp         
    });
    this.cd.markForCheck();

    this.showDocProperties=true;

    
}
}
