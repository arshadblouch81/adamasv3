using System;
using Adamas.Models.Modules;
namespace Adamas.Models.Modules{
    public class TimeSheetRosterDto{
        public string ShiftbookNo { get; set; }
        public DateTime ActivityDate { get; set; }
        public TimeDuration Activity_Time { get; set; }
        public string Date_Value { get; set; }
        public string DayNo { get; set; }
        public string MonthNo { get; set; }
        public string YearNo { get; set; }
        public Program program { get; set; }
        public string Carer_Code { get; set; }
        public string Client_Code { get; set; }
        public Person recipient { get; set; }
        public Activity activity { get; set; }
        public PayType payType { get; set; }
        public Pay pay { get; set; }
        public TimeSheetBill Bill { get; set; }
        public Boolean Approved { get; set; }
        public Person BilledTo { get; set; }
        public string Anal { get; set; }
        public string Note { get; set; }
        public string ServiceSetting { get; set; }
        public float BlockNo { get; set; }
        public int Roster_Type { get; set; }
        public string  Roster_Status { get; set; }
        public string Invoice_Number { get; set; }
        public string TimeSheet_Number { get; set; }
        public string End_Dur { get; set; }
        public string DecDur { get; set; }
         public string Duration { get; set; }
        public Person Recipient_staff { get; set; }
        public string group_Activity { get; set; }
        public DateTime Last_Modify_Date { get; set; }
        public bool? InfoOnly { get; set; }
        public string Editer { get; set; }
        public int Pay_Quantity { get; set; }
        public double Pay_Rate { get; set; }
        public int Bill_Quantity { get; set; }
        public double Bill_Rate { get; set; }
        public string RecipientLocation { get; set; }
        public string ShiftName { get; set; }        

        public double Get_Pay_Amount()
        {
            return Pay_Rate * Pay_Quantity;
        }

        public double Get_Bill_Amount()
        {
            return Bill_Rate * Bill_Quantity;
        }
        public DateTime ClaimDate { get; set; }
        public string Claimedby { get; set; }
        public DateTime Claimed_Start { get; set; }
        public DateTime Claimed_End { get; set; }
        public bool Submitted { get; set; }
        public double Start_KM { get; set; }      
        public double End_KM { get; set; }
        public double KM { get; set; }
        public string VariationReason { get; set; }
        public string ServiceTypePortal { get; set; }
         public string RecordNo { get; set; }
          public string? Status { get; set; }
           public string? Type { get; set; }
    }
}