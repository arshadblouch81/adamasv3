using System;

namespace Adamas.Models.Modules
{
    public class TravelClaimGap
    {
        public string BatchNumber { get; set; }
        public string TravelToFitGap { get; set; } 
        public string SeparteClaim { get; set; } 

    }
}