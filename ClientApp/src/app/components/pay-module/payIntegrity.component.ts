import { ChangeDetectorRef, Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService,PrintService, MenuService } from '@services/index';
import { takeUntil, timeout } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { FormsModule } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
    selector: 'app-pay-integrity-check',
    templateUrl: './payIntegrity.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
.btn1 {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}
  `]
})
export class PayIntegrityComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;

    branchList: Array<any>;
    programList: Array<any>;
    categoriesList: Array<any>;
    payIntegrityList: Array<any>;
    tableData: Array<any>;
    PayPeriodLength: number;
    PayPeriodEndDate: any;
    loading: boolean = false;
    allchecked: boolean = false;
    modalOpen: boolean = false;
    billingType: Array<any>;
    AccountPackage: Array<any>;
    invType: Array<any>;
    current: number = 0;
    inputForm: FormGroup;
    modalVariables: any;
    dateFormat: string = 'dd/MM/yyyy';
    inputVariables: any;
    postLoading: boolean = false;
    chkMaster: boolean;
    isUpdate: boolean = false;
    // 
    token: any;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean = false;
    check: boolean = false;
    temp_title: any;
    settingForm: FormGroup;
    startDate: any; 
    endDate: any;
    private unsubscribe: Subject<void> = new Subject();
    id: string;
    btnid: any;
    btnid1: any;

    

    constructor(
        private cd: ChangeDetectorRef,
        private router: Router,
        private globalS: GlobalService,
        private listS: ListService,
        private formBuilder: FormBuilder,
        private menuS: MenuService,
        private timesheetS: TimeSheetService,
        private billingS: BillingService,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
    ) { }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.buildForm();
        this.loadData();
        this.loading = false;
        this.modalOpen = true;
    }
    handleCancel() {
        // this.inputForm.reset();
        this.postLoading = false;
        this.modalOpen = false;
        this.isVisible = false;
        this.tableData = null;
        this.payIntegrityList = null;
        this.router.navigate(['/admin/pays']);
    }
    buildForm() {
        this.inputForm = this.formBuilder.group({
            chkMaster: false,
            startDate: '01/01/2021',
            endDate: '01/01/2021',
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    loadData() {
        this.loading = true;
        this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
            if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
                this.endDate = dataPayPeriodEndDate[0].payPeriodEndDate;
                this.inputForm.patchValue({
                    endDate: this.endDate,
                })
            }
            else {
                this.inputForm.patchValue({
                    endDate: new Date()
                });
                this.endDate = new Date;
            }
            this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
                if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
                    this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
                }
                else {
                    this.PayPeriodLength = 14
                }
                var firstDate = new Date(this.endDate);
                firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
                this.startDate = formatDate(firstDate, 'yyyy/MM/dd', 'en_US');
                this.inputForm.patchValue({
                    startDate: this.startDate,
                });
            });
        });
        this.loading = false;
    }

    loadPayIntegrity() {
        this.loading = true;
        this.chkMaster = this.inputForm.get('chkMaster').value;
        if (this.chkMaster == true) {
            this.startDate = '1900/01/01';
            this.endDate = '1900/01/28';
        }
        else {
            this.startDate = this.inputForm.get('startDate').value;
            this.endDate = this.inputForm.get('endDate').value;
        }
        this.startDate = formatDate(this.startDate, 'yyyy/MM/dd', 'en_US');
        this.endDate = formatDate(this.endDate, 'yyyy/MM/dd', 'en_US');

        this.billingS.getPayIntegrityList({
            StartDate: this.startDate,
            EndDate: this.endDate
        }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.payIntegrityList = data;
            this.tableData = data;
        });
        this.loading = false;
    };
    generatePdf(){
      
        console.log("called")
              
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Pay Integrity Check",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Pay Integrity Check"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
}


