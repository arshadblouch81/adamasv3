
namespace Adamas.Models.Modules{
    public class GetPackage {
        public string Code { get; set; }
        public string PCode { get; set; }
        public string Date { get; set; }
    }
}