﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

using System.Xml.Serialization;
using DocuSign.eSign.Model;
using Org.BouncyCastle.Ocsp;
using System.Xml;
using Adamas.Models.Modules;
using eg_03_csharp_auth_code_grant_core.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using Xero.NetStandard.OAuth2.Model.Identity;

using AdamasV3.DAL;
using Microsoft.Extensions.Configuration;

namespace AdamasV3.Controllers
{
    [Route("[controller]")]
    public class DocusignWebhookController : ControllerBase
    {
        private IConfiguration Configuration;
        private readonly IDocusignService docService;
        private readonly ILogService log;
        public DocusignWebhookController(
            IDocusignService _docService,
            IConfiguration _configuration,
            ILogService log
            )
        {
            docService = _docService;
            Configuration = _configuration;
            this.log = log;
        }



        [HttpPost("connect")]
        public async Task ConnectWebHook()
        {

            try
            {
                //docService.RefreshJWT();
                //var sa = await docService.GetDocumentDetails(45808);
                //DocusignService.CopyMTADocument(filesDirectory.Filename, filesDirectory.OriginalLocation);

                Stream stream = Request.Body;

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(stream);

                var mgr = new XmlNamespaceManager(xmldoc.NameTable);
                mgr.AddNamespace("a", "http://www.docusign.net/API/3.0");

                XmlNode envelopeStatus = xmldoc.SelectSingleNode("//a:EnvelopeStatus", mgr);
                XmlNode envelopeId = envelopeStatus.SelectSingleNode("//a:EnvelopeID", mgr);
                XmlNode status = envelopeStatus.SelectSingleNode("./a:Status", mgr);

                string path = Directory.GetCurrentDirectory();

                if (status.InnerText == "Completed")
                {
                    XmlNode docs = xmldoc.SelectSingleNode("//a:DocumentPDFs", mgr);
                    foreach (XmlNode doc in docs.ChildNodes)
                    {
                        string documentName = doc.ChildNodes[0].InnerText;      // pdf.SelectSingleNode("//a:Name", mgr).InnerText;
                        string documentId = doc.ChildNodes[2].InnerText;        // pdf.SelectSingleNode("//a:DocumentID", mgr).InnerText;
                        string byteStr = doc.ChildNodes[1].InnerText;           // pdf.SelectSingleNode("//a:PDFBytes", mgr).InnerText;

                        int orderNumber = 0;
                        bool isNumericalValue = int.TryParse(documentId, out orderNumber);
                        var tbdFile = await docService.GetDocumentDetails(orderNumber);

                        if (isNumericalValue && tbdFile != null)
                        {
                            tbdFile = await docService.GetDocumentDetails(orderNumber);
                            await this.docService.UpdateDocumentStatus(envelopeId.InnerText, status.InnerText,"", orderNumber);
                        }
                        else
                        {
                            throw new Exception("Document number does not exist");
                        }

                        EnvelopeDocItem item = new EnvelopeDocItem()
                        {
                            Name = Path.GetFileNameWithoutExtension(tbdFile.Filename),
                            Type = "content",
                            DocumentId = documentId
                        };

                        List<EnvelopeDocItem> itemList = new List<EnvelopeDocItem>() { item };

                        //Download Completed File
                        var fs = docService.DownloadCompletedDocument(envelopeId.InnerText, documentId);                        
                        var fsResult = File(fs, MimeAndFile.GetMimeType(itemList, documentId), MimeAndFile.GetDocument(itemList, documentId));

                        var downloadFileName = fsResult.FileDownloadName;

                        using (var fileStream = System.IO.File.Create(Path.Combine(path, "WebHookFile", downloadFileName)))
                        {
                            fsResult.FileStream.Seek(0, SeekOrigin.Begin);
                            fsResult.FileStream.CopyTo(fileStream);
                        }
                        //var downloadFileName = MimeAndFile.GetDocument(itemList, documentId);

                        var WebHookFilePath = (Configuration.GetSection("RemoteDownloadDirectory:WebHookPath")?.Value == null)
                                                ? Path.Combine(path, "WebHookFile") : Configuration.GetSection("RemoteDownloadDirectory:WebHookPath")?.Value;

                        await docService.TestCopyPasteProcedure(
                                    downloadFileName,
                                    WebHookFilePath,
                                    tbdFile.OriginalLocation
                                    );

                        await docService.UpdateDocumentStatus(envelopeId.InnerText, "Completed", downloadFileName, orderNumber);
                    }
                }

                if (status.InnerText == "Delivered")
                {
                    XmlNode docs = xmldoc.SelectSingleNode("//a:DocumentPDFs", mgr);
                    foreach (XmlNode doc in docs.ChildNodes)
                    {
                        string documentName = doc.ChildNodes[0].InnerText;      // pdf.SelectSingleNode("//a:Name", mgr).InnerText;
                        string documentId = doc.ChildNodes[2].InnerText;        // pdf.SelectSingleNode("//a:DocumentID", mgr).InnerText;
                        string byteStr = doc.ChildNodes[1].InnerText;           // pdf.SelectSingleNode("//a:PDFBytes", mgr).InnerText;

                        int orderNumber = 0;
                        bool isNumericalValue = int.TryParse(documentId, out orderNumber);

                        if (isNumericalValue)
                        {
                            await this.docService.UpdateDocumentStatus(envelopeId.InnerText, status.InnerText, "", orderNumber);
                        }
                        else
                        {
                            throw new Exception("Not a number");
                        }
                    }
                }

                //if (status.InnerText == "Sent")
                //{
                //    await docService.UpdateDocumentStatus(envelopeId.InnerText, "Completed", downloadFileName, orderNumber);
                //}
            } catch(Exception ex)
            {
                await this.log.InsertError(ex);
                throw new Exception(ex.ToString());
            }

        }

    }
}
