using System.ComponentModel.DataAnnotations;

namespace Adamas.Models.Modules{
    public class ApplicationUser {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string PasswordHandler { get; set; }
        public bool Bypass { get; set; } = false;
        public string BrowserName { get; set; }
    }
}