
import {debounceTime, distinctUntilChanged, takeUntil} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, Input } from '@angular/core';


import { StaffService,GlobalService } from '../../services/index';
import * as moment from 'moment';

import { Subject } from 'rxjs'

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

import { FullCalendarComponent } from '@fullcalendar/angular';

import * as $ from 'jquery';

@Component({
    templateUrl:'./rosters.html',
    styles: [`
        div.calendar-title{
            display:flex;
            text-transform:uppercase;
            color:rgb(23, 125, 255);
            width: 25rem;
            margin: 0 auto;
        }

        div.calendar-title div{
            font-size:1rem;
        }
        div.calendar-title div{
            align-items: center;
            display: flex;
            justify-content: center;
        }
        div.calendar-title i{
            font-size:1.5rem;
            cursor:pointer;
        }
        div.calendar-title i:hover{
            transform:scale(1.1)
        }
        i.right{
            text-align:right;
        }
        button{
            border: 0;
            background: none;
            outline:none;
        }
        .loading{
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: #177dff0d;
            z-index: 3;
            margin-top: 21px;
        }
        .calendar{
            height:75vh;
            position:relative;
        }
        .spinner-wrap{
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
        }
        .date-title{
            flex:2;
        }
        .hover{
            background-color:red;
        }
    `]
})

export class RostersAdmin implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('calendar', { static: false }) calendarComponent: FullCalendarComponent;

    loading:boolean = false
    basic: boolean = false;
   
    events: Array<any>;

    calComponent: any;

    calendarPlugins = [dayGridPlugin,timeGridPlugin,interactionPlugin]; // important!

    private unsubscribe$ = new Subject()
    private upORdown = new Subject<boolean>()

    date:any;
    recipient: string = '';
    source: any;

    public dateStream = new Subject<any>();
    public recipientStream = new Subject<string>();

    options: any;
    data: any;

    private rosters: Array<any>;

    constructor(
        private staffS: StaffService,
        private globalS: GlobalService
    ){

        this.dateStream.pipe(
            distinctUntilChanged(),          
            takeUntil(this.unsubscribe$),)
            .subscribe(data =>{
                this.date = data;
                this.searchRoster(this.date);
            })

        this.recipientStream.pipe(
            distinctUntilChanged(),
            debounceTime(500),
            takeUntil(this.unsubscribe$),)
            .subscribe(data =>{
                this.loading = true;
                this.recipient = data;
                this.searchRoster(this.date);
            }) 

        this.upORdown.pipe(debounceTime(300))
            .subscribe(data => {
                this.loading = true;                         
                this.searchRoster(this.date);          
            })
        
        // this.onElementHeightChange(document.body, function(){
        //     console.log('Body height changed');
        // });
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    ngOnInit(){ 
        this.date = moment();
    }

    ngAfterViewInit(){
        this.searchRoster(this.date);   
    }

    searchRoster(date: any): void{
        this.staffS.getroster({
            RosterType: 'PORTAL CLIENT',
            //AccountNo: 'ABBERTON B',
            AccountNo: this.recipient,
            StartDate: moment(date).startOf('month').format('YYYY/MM/DD'),
            EndDate: moment(date).endOf('month').format('YYYY/MM/DD')
        }).pipe(takeUntil(this.unsubscribe$)).subscribe(roster => {

            this.rosters = roster;
           
                this.options = null;

                var events = roster.map(x => {
                    return {
                        id: x.recordNo,
                        title: `<b class="title" data-toggle="tooltip" data-placement="top" title="${ x.serviceType }">${ x.carerCode }</b>`,
                        start: `${ moment(x.shift_Start).format("YYYY-MM-DD HH:mm:00") }`,
                        end: `${ this.detectMidNight(x) }`
                    }
                })

                var time = events.map(x => x.start);
                var timeStart = moment(this.globalS.getEarliestTime(time)).subtract(2,'h').format('hh:mm:ss');

                if(timeStart != null){
                    this.options = {
                        show: true,
                        scrollTime:  timeStart,
                        events: events
                    }
                }                 
                else {
                    this.options = {
                        show: true,
                        scrollTime:  '00:00:00',
                        events: events
                    }
                }
                
                this.loading = false;

                console.log(JSON.stringify(this.options.events))
            
        })
    }

    eventRender(e: any){
        e.el.querySelectorAll('.fc-title')[0].innerHTML = e.el.querySelectorAll('.fc-title')[0].innerText;
    }

    detectMidNight(data: any){
        if(Date.parse(data.shift_Start) >= Date.parse(data.shift_End)){
            return moment(data.shift_End).format("YYYY-MM-DD 24:00:00");
        }
        return moment(data.shift_End).format("YYYY-MM-DD HH:mm:00");
    }

    // onElementHeightChange(elm, callback){
    //     var lastHeight = elm.clientHeight, newHeight;
    //     (function run(){
    //         newHeight = elm.clientHeight;
    //         if( lastHeight != newHeight )
    //             callback();
    //         lastHeight = newHeight;
    
    //         if( elm.onElementHeightChangeTimer )
    //             clearTimeout(elm.onElementHeightChangeTimer);
    
    //         elm.onElementHeightChangeTimer = setTimeout(run, 200);
    //     })();
    // }

    pick(data: any){
        this.recipientStream.next(data);
    }

    next(){
        this.date = moment(this.date).add('month', 1);
        var calendar = this.calendarComponent.getApi(); 
        calendar.next();
        this.upORdown.next(true);
    }

    prev(){
        this.date = moment(this.date).subtract('month', 1);
        var calendar = this.calendarComponent.getApi(); 
        calendar.prev();
        this.upORdown.next(false);
    }

    handleDateClick({ event }){
        this.basic = !this.basic;
        this.data = this.search(this.rosters, 'recordNo', event.id);
    }

    eventMouseEnter(event){     
        $(event.jsEvent.target).closest('a').css({'cursor':'pointer','background-color':'#4396e8'})
    }

    eventMouseLeave(event){
        $(event.jsEvent.target).closest('a').css({'cursor':'pointer','background-color':'#3788d8'})
    }

    search(arr: Array<any>, key: string, name: any): any{
        return arr.find(o => o[key] === name);
    }

    eventDragStart(event){
        console.log(event)
    }

    eventDrop(event){
        console.log(event.event)
        console.log(event.oldEvent)
    } 


}