namespace Adamas.Models.Modules{
    public class UnallocateStaff {
        public string RecordNo { get; set; }
        public string CarerCode { get; set; }
        public string ServiceType { get; set; }
        public string ServiceDescription { get; set; }
        public string Program { get; set; }
        public string Branch { get; set; }
        public string User { get; set; }
        public bool? Reallocate { get; set; }
        public bool? AddLeave { get; set; }
        public bool? UnavailableForBalanceDays { get; set; }
        public bool? UnallocateAdminActivities { get; set; }
        public bool? UnallocateMaster { get; set; }
        public bool? UnallocateAny { get; set; }
        public int ServiceDuration { get; set; }
        public bool? AcceptProgram  { get; set; }
    }
}