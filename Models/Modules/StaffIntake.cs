using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class StaffIntake
    {
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string Name { get; set; }
        public int? StaffCategory { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Creator { get; set; }
        public string Notes { get; set; }
        public  List<String> selectedStaff {get;set;}
    }
}
