import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, Subject } from 'rxjs';

import { SharedService } from '@shared/sharedService/shared-service';
import { TimeSheetService, GlobalService, ListService, fundingDropDowns } from '@services/index';

import { SplitArrayPipe } from '@pipes/pipes';

interface FundingDropDowns {
    programs?: Array<any>,
    purposes?: Array<any>,
    priorities?: Array<any>,
    types?: Array<any>,
    statuses?: Array<any>,
    expireUsing?: Array<any>,
    costTypes?: Array<any>,
    perUnit?: Array<any>,
    periods?: Array<any>,
    length?: Array<any>,
    alerts?: Array<any>,
    homeCare?: Array<any>,
    packageTerm?: Array<any>,
}

@Component({
    selector: '',
    templateUrl: './funding.html',
    styles: [`
        bs-modal >>> div.modal-dialog{
            width:45rem;
        }
        bs-modal.disable-overflow >>> .modal-body{
            overflow-y: inherit;
            overflow-x: inherit;
            max-height:inherit;
        }
        .fund-wrap{
            padding: 8px;
            border: 1px solid #dedede;
            border-radius: 3px;
        }
        .form-group{
            margin-bottom:10px;
        }
        .inline-group{
            display:flex;
            margin-bottom:10px;
        }
        .inline-group label{
            margin:0 10px 0 0;
        }
        .inline-group * {
            flex:1;
        }
        .equal-width{
            display:flex;
        }
        .equal-width *{
            flex: 1;
        }
        .group-wrap{
            margin-bottom:12px;
        }
        ul{
            list-style:none;
        }
        .clr-checkbox-wrapper input[type=checkbox]+label.disabled::before{
            cursor: not-allowed;
            background: #d2d2d2;
        }
        clr-checkbox-container{
            padding-left:20px;
        }
        .wrap-scroll{
            height:17rem;
            overflow-x: auto;
        }
        .one-line{
            display: flex;
            justify-content: space-between;
        }
        .one-line > div{
            margin-right: 20px;
            flex-basis:auto;
            flex-grow: 1;
        }
        .line{
            width: 100%;
            border: 1px solid #ececec;
            display: inline-block;
        }
        hr {
            box-sizing: content-box;
            height: 0;
            display: inline-block;
            width: 100%;
            margin:5px;
            border-top: 1px solid #f3f3f3;
        }
        .grid-wrapper{
            display: grid;
            grid-template-columns: 1fr 3fr 3fr;

        }
        .grid-row{
            display: grid;
            grid-template-rows: 1fr;
        }
        .grid-wrapper-two-eight{
            display: grid;
            grid-template-columns: 1fr 3fr;
        }

    `]
})

export class IntakeFunding implements OnInit, OnDestroy {

    changeTabView = new Subject<number>();
    changeDocView = new Subject<number>();

    openModal: boolean = false;

    tab: number = 1;

    // 0 for add 1 for update
    whatProcess: number;

    checkBoxArrays: Array<any> = [];
    showCheckbox: boolean = false;

    private funding$: Subscription;
    funding: Array<any>;
    loading: boolean;

    packageDetailsGroup: FormGroup;

    selectedFunding: any;
    selectedRecipient: any;
    carePlanArray: Array<any>;

    packageSupplement: string;

    dropDowns: FundingDropDowns = {
        types: fundingDropDowns.type,
        statuses: fundingDropDowns.status,
        expireUsing: fundingDropDowns.expireUsing,
        packageTerm: fundingDropDowns.packageTerm,
        homeCare: fundingDropDowns.homeCare,
        perUnit: fundingDropDowns.perUnit,
        periods: fundingDropDowns.period,
        length: fundingDropDowns.length,
        alerts: fundingDropDowns.alerts,
        costTypes: fundingDropDowns.costType
    };


    constructor(
        private timeS: TimeSheetService,
        private listS: ListService,
        private sharedS: SharedService,
        private globalS: GlobalService,
        private router: Router,
        private formBuilder: FormBuilder
    ) {
        this.funding$ = this.sharedS.changeEmitted$.subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'funding')) {
                this.search();
            }
        })
    }

    ngOnInit() {

        this.search();
        this.initCheckBoxes();

        this.changeTabView.subscribe(tabNo => {
            const funding = this.selectedFunding;
            switch (tabNo) {
                case 1:
                    this.timeS.getprogramdetails(funding.recordNumber)
                        .subscribe(data => {
                            this.packageDetailsGroup.patchValue(data);
                        });
                    this.populateList();
                    break;
                case 2:
                    this.timeS.getintakefees(funding.recordNumber)
                        .subscribe(data => {
                            this.redrawCheckboxValue(data.packageSupplements);
                        })
                    break;
                case 3:
                    this.changeDocView.next(1)
                    break;
                case 4:
                    this.timeS.getintakecredits({
                        clientCode: this.selectedRecipient.accountNo,
                        program: funding.program,
                        startDate: '2018/05/01',
                        endDate: '2019/02/28'
                    }).subscribe(data => this.carePlanArray = data)
                    break;
            }
            this.tab = tabNo;
        });



        this.changeDocView.subscribe(tabNo => {
            const funding = this.selectedFunding;
            this.carePlanArray = [];
            switch (tabNo) {
                case 1:
                    this.timeS.getintakedocuments({
                        personId: funding.personID,
                        documentType: 'CP_QUOTE'
                    }).subscribe(data => this.carePlanArray = data)
                    break;
                case 2:
                    this.timeS.getintakedocuments({
                        personId: funding.personID,
                        documentType: 'CAREPLAN'
                    }).subscribe(data => this.carePlanArray = data)
                    break;
                case 3:
                    this.timeS.getintakedocuments({
                        personId: funding.personID,
                        documentType: 'DOCUMENT'
                    }).subscribe(data => console.log(data))
                    break;
            }
        })

        this.loadForms();
    }

    addOpenProcess() {

        this.whatProcess = 0;
        this.tab = 1;
        this.openModal = true;

        this.loadForms();
        this.populateList();
    }

    get isAuthorized() {
        return this.globalS.isRecipients999()
    }

    ngOnDestroy() {
        this.funding$.unsubscribe();
    }

    unCheckORCheck(checkBoxArr: Array<any>, checkBoxEvent: boolean): void {
        if (checkBoxArr.length > 0) {
            for (const a of checkBoxArr) {
                a.checked = false;
                a.disabled = checkBoxEvent === true ? checkBoxEvent : false;
            }
        }
    }

    mainCheckBox(event: any, index: number) {
        if (index === 0) {
            const checkboxes = new SplitArrayPipe().transform(this.checkBoxArrays, 1, 5);
            this.unCheckORCheck(checkboxes, !event);
        }

        if (index == 6) {
            const checkboxes = new SplitArrayPipe().transform(this.checkBoxArrays, 7, 9);
            this.unCheckORCheck(checkboxes, !event);
        }

        if (index == 10) {
            const checkboxes = new SplitArrayPipe().transform(this.checkBoxArrays, 11, 18);
            this.unCheckORCheck(checkboxes, !event);
        }
    }

    initCheckBoxes(): void {
        // Value is 18 since PackageSupplement value is only up to 18 length 
        const len = 18;
        for (var a = 0; a < len; a++) {
            this.checkBoxArrays.push({
                index: a,
                checked: false,
                disabled: false
            });
        }

    }

    loadForms() {
        var data: Dto.RecipientPrograms = {
            recordNumber: 0,
            personID: null,
            program: null,
            quantity: null,
            itemUnit: null,
            perUnit: null,
            timeUnit: null,
            period: null,
            packageType: null,
            aP_BasedOn: null,
            aP_CostType: null,
            aP_PerUnit: null,
            aP_Period: null,
            aP_YellowAmtPerc: null,
            aP_OrangeAmtPerc: null,
            aP_RedAmtPerc: null,
            aP_YellowQty: null,
            aP_OrangeQty: null,
            aP_RedQty: null,
            adminAmount_Perc: null,
            packageLevel: null,
            packageTermType: null,
            priority: null,
            packageSupplements: null,
            hardShipSupplement: null,
            programSummary: null,
            contingency: null,
            incomeTestedFee: null,
            clientCont: null,
            agreedTopUp: null,
            dailyIncomeTestedFee: null,
            dailyBasicCareFee: null,
            dailyAgreedTopUp: null,
            commonwealthCont: null,
            contingency_Start: null,
            contingency_BuildCycle: null,
            lastReferrer: null,
            programStatus: null,
            billing: null,
            expireUsing: null,
            percentage: null,
            expiryDate: null,
            startDate: null,
            deletedRecord: false,
            cappedAt: 0,
            capped: false,
            autoRenew: false,
            RolloverRemainder: false,
            reminderProcessed: false,
            renewalProcessed: false,
            reminderDate: null,
            reminderLeadTime: 0,
            deactivateOnExpiry: false,
            autoReceipt: false,
            autoBill: false,
            used: 0,
            remaining: 0,
            totalAllocation: 0
        };

        this.packageDetailsGroup = this.formBuilder.group(data)
    }

    search() {
        const recipient = this.sharedS.getPicked();
        this.selectedRecipient = recipient;

        this.loading = true;
        this.timeS.getfunding(recipient.uniqueID).subscribe(funding => {
            this.loading = false;
            this.funding = funding
        });
    }

    redrawCheckboxValue(packageSupplements: string) {
        var count = 0;
        this.packageSupplement = packageSupplements;

        for (let supp of this.checkBoxArrays) {
            supp.checked = packageSupplements[count] == '1' ? true : false
            count++;
        }

        this.redrawCheckboxDisable(1, 5, this.checkBoxArrays[0].checked);
        this.redrawCheckboxDisable(7, 9, this.checkBoxArrays[6].checked);
        this.redrawCheckboxDisable(11, 18, this.checkBoxArrays[10].checked);

        this.showCheckbox = true;
    }


    redrawCheckboxDisable(min, max, event) {
        const chkboxes = new SplitArrayPipe().transform(this.checkBoxArrays, min, max);
        for (let box of chkboxes) {
            box.disabled = !event
        }
    }

    calculateString(): string {
        this.packageSupplement.substring(18);
        var initValue = "";

        for (let supp of this.checkBoxArrays) {
            const isChecked = supp.checked ? '1' : '0';
            initValue = initValue + isChecked;
        }

        return initValue + this.packageSupplement.substring(18);
    }

    updateDetails(data: any) {
        this.whatProcess = 1;
        this.loadForms();

        this.selectedFunding = data;
        this.changeTabView.next(1);
        this.openModal = true;
    }

    populateList(): void {
        this.listS.getfundingpackagepurposelist().subscribe(data => this.dropDowns.purposes = data);
        this.listS.getfundingprioritylist().subscribe(data => this.dropDowns.priorities = data);
        this.listS.getfundingpackagelist(this.selectedRecipient.uniqueID).subscribe(data => this.dropDowns.programs = data);
    }

    change(min: any, max: any) {
        const chkboxes = new SplitArrayPipe().transform(this.checkBoxArrays, min, max);
        for (let box of chkboxes) {
            box.checked = false
        }
    }

    save() {

        if (this.tab === 1) {
            if (this.whatProcess == 0) {
                let data = this.packageDetailsGroup.value;
                data.personID = this.selectedRecipient.uniqueID;
                data.user = this.globalS.decode().user;
                this.timeS.postfunding(data).subscribe(data => {
                    if (data)
                        this.globalS.sToast('Success', 'New Funding Added');
                })
            }
            if (this.whatProcess == 1) {
                this.timeS.updateprogramdetails(this.packageDetailsGroup.value)
                    .subscribe(data => console.log(data))
            }
        }

        if (this.tab === 2) {
            this.timeS.updatepackagesupplement({
                PackageSupplement: this.calculateString(),
                RecordNo: this.selectedFunding.recordNumber
            }).subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Changes Updated');
                }
            });
        }
    }

}
