import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
// import { ListService, MenuService } from '@services/index';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, DllService } from '@services/index';
import { debounceTime, timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';

import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-print-email-invoices',
  templateUrl: './printEmailInvoices.component.html',
  styleUrls: ['./printEmailInvoices.component.css'],
})

// .ant-divider, .ant-divider-vertical {
//     position: relative;
//     top: 10px;
// }

export class PrintEmailInvoicesComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;
  
    isVisible: boolean = false;
    operatorID: any;
    modalOpen: boolean = false;
    current: number = 0;
    inputForm: FormGroup;
    loading: boolean = false;
    token: any;
    tocken: any;
    check: boolean = false;
    userRole: string = "userrole";
    id: string;
    private unsubscribe: Subject<void> = new Subject();
    dateFormat: string = 'dd/MM/yyyy';
    title: string = "Print / Email Invoices";
    tableData: Array<any>;
    updatedRecords: any;
    
    endDate: any;
    startDate: any;
    chkAllDates: boolean;
    currentDateTime: any;
    PayPeriodLength: number;
    inputValue?: string;
    displayInvDropList: Array<any>;
    invociesList: Array<any>;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    private Dlls: DllService
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/billing']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      chkAllDates: false,
      inputValue: '',
      displayInvDropList: 'Display Invoices to be BOTH Printed OR Emailed',
      startDate: '01/01/2021',
      endDate: '31/01/2021',
      rdoFilterType: "rdoNoFilter",
    });
    var now = new Date();
    
    //load Payperiod End Date
    // this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
    //   if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
    //     this.endDate = dataPayPeriodEndDate[0].payPeriodEndDate;
    //     this.inputForm.patchValue({
    //       endDate: this.endDate,
    //     })
    //   }
    //   else {
    //     this.inputForm.patchValue({
    //       endDate: new Date()
    //     });
    //     this.endDate = new Date;
    //   }
    //   this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
    //     if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
    //       this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
    //     }
    //     else {
    //       this.PayPeriodLength = 14
    //     }
    //     var firstDate = new Date(this.endDate);
    //     firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
    //     this.startDate = formatDate(firstDate, 'yyyy/MM/dd', 'en_US');
    //     this.inputForm.patchValue({
    //       startDate: this.startDate,
    //     });
    //   });
    // });
    
    // if (now.getMonth() == 11) {
    //     this.startDate = new Date(now.getFullYear() + 1, 0, 1);
    //     this.endDate = new Date(now.getFullYear() + 1, 5, 1);
    // } else {
    //     this.startDate = new Date(now.getFullYear(), now.getMonth() + 1, 1);
    //     this.endDate = new Date(now.getFullYear(), now.getMonth() + 6, 1);
    // }
    // this.startDate = formatDate(this.startDate, 'MM/dd/yyyy', 'en_US');
    // this.endDate = formatDate(this.endDate, 'MM/dd/yyyy', 'en_US');
    // this.inputForm.patchValue({
    //     startDate: this.startDate,
    //     endDate: this.endDate,
    // });

    // let dataPass = {
    //     CurrentDate: formatDate(this.globalS.getCurrentDateTime(), 'MM-dd-yyyy', 'en_US')
    //   }

    this.displayInvDropList = ['Display Invoices to be BOTH Printed OR Emailed', 'Display ONLY Invoices Flagged For Emailing', 'Display ONLY Invoices Flagged For Printing'];

    // Load the current month start and end date
    this.startDate = new Date(now.getFullYear(), now.getMonth(), 1);
    this.endDate = endOfMonth(new Date());
    this.startDate = formatDate(this.startDate, 'MM/dd/yyyy', 'en_US');
    this.endDate = formatDate(this.endDate, 'MM/dd/yyyy', 'en_US');
    this.inputForm.patchValue({
      startDate: this.startDate,
      endDate: this.endDate,
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  runProcess() {
    this.loading = true;
    this.operatorID = this.token.nameid;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');

    const modalVal = this.inputForm;
    let _currentDate = formatDate(this.globalS.getCurrentDateTime(), 'yyyy-MM-dd hh:mm', 'en_US')
    let _startDate = formatDate(modalVal.get('startDate').value, 'yyyy/MM/dd', 'en_US')
    let _endDate = formatDate(modalVal.get('endDate').value, 'yyyy/MM/dd', 'en_US')

    this.billingS.getPrintEmailInvoicesList({
        StartDate: _startDate,
        EndDate: _endDate
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.invociesList = data;
        this.tableData = data;
    });
    // this.billingS.postDebtorBilling({
    //   OperatorID: this.operatorID,
    //   CurrentDateTime: this.currentDateTime
    // }).pipe(
    //   takeUntil(this.unsubscribe)).subscribe(data => {
    //     if (data) {
    //       this.updatedRecords = data[0].updatedRecords
    //       if (this.updatedRecords == 0) {
    //         this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
    //       } else {
    //         this.globalS.sToast('Success', this.updatedRecords + ' - Debtor Records Updated')
    //         this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Debtor Run is: ' + '')
    //       }
    //       this.loading = false;
    //       this.ngOnInit();
    //       return false;
    //     }
    //   });
    this.loading = false;
  }



  startProcess() {
    this.loading = true;
    this.billingS.getBatchRecord(null).subscribe(data => {
      this.validateModalFields();
    });
  }

  validateModalFields(): void {
    this.operatorID = this.token.nameid;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');
    this.startDate = this.inputForm.get('startDate').value;
    this.endDate = this.inputForm.get('endDate').value;
    this.startDate = formatDate(this.startDate, 'yyyy/MM/dd', 'en_US');
    this.endDate = formatDate(this.endDate, 'yyyy/MM/dd', 'en_US');

      this.globalS.eToast('Error', 'Please select atleast one Category to proceed')
  }

  processExecution() {
    const modalVal = this.inputForm;

    let _AccountPackage = modalVal.get('AccountPackage')?.value ?? '';
    let _myOB = modalVal.get('myOB')?.value ?? '';
    let _invLineType = modalVal.get('invLineType')?.value ?? '';
    let _billingType = modalVal.get('billingType')?.value ?? '';

    const params = {
      User: this.token.nameid,
      Password: '', // Ignored by Mansoor
      // WindowsUser: this.loggedUserName,
      b_AllDates: modalVal.get('chkAllDates').value,
      b_Award: modalVal.get('chkAwardInterExport').value,
      b_Export: modalVal.get('chkCreateExportFile').value,
      b_ExportZeroValueLines: modalVal.get('chkExportZeroValueLines').value,
      b_GLCodesRequired: '', // Ignored by Mansoor
      b_IncludeADMINPaytype: modalVal.get('chkIncludeADMINPaytype').value,
      b_IncludeRecipientAdmin: modalVal.get('chkIncludeRecipientAdmin').value,
      b_IncUnapproved: '', // Ignored by Mansoor
      b_Option1: modalVal.get('chkIncActivityCode').value,
      b_Option2: modalVal.get('chkUseTraccsCostAcc').value,
      b_Option3: modalVal.get('chkActCostAcc').value,
      b_Option4: modalVal.get('chkStaffNumberPosID').value,
      b_Option5: modalVal.get('chkPrefixGL').value,
      b_PayBrokerage: modalVal.get('chkPayBrokerage').value,
      b_UpdateCompletion: '', // Ignored by Mansoor
      ProgressBar_Max: '',
      ProgressBar_Text: '',
      ProgressBar_Value: '',
      dt_ProcessStartTime: '',
      OperatorID: this.operatorID,
      s_ABMDefaultAccount: modalVal.get('inputDefaultAccount').value,
      s_ABMDefaultActivity: modalVal.get('inputDefaultActivity').value,
      s_ABMDefaultProject: modalVal.get('inputDefaultProject').value,
      s_EndDate: formatDate(modalVal.get('endDate').value, 'yyyy/MM/dd', 'en_US'),
      s_EndOfInvoiceString: '', // Ignored by Mansoor
      s_ExportLocation: modalVal.get('selectedDirectoryPath').value,
      s_Interface: modalVal.get('selectedPackage').value,
      s_Option1: modalVal.get('normalPayType')?.value ?? '',
      s_Option2: modalVal.get('superannuationType')?.value ?? '',
      s_Option3: modalVal.get('allowanceType')?.value ?? '',
      s_StartDate: formatDate(modalVal.get('startDate').value, 'yyyy/MM/dd', 'en_US'),
    };

    this.Dlls.PayBillUpdate({ //this should be for the billing update as when dll will be complete
      User: params.User,
      Password: params.Password,
      b_AllDates: params.b_AllDates,
      b_Award: params.b_Award,
      b_Export: params.b_Export,
      b_ExportZeroValueLines: params.b_ExportZeroValueLines,
      b_GLCodesRequired: params.b_GLCodesRequired,
      b_IncludeADMINPaytype: params.b_IncludeADMINPaytype,
      b_IncludeRecipientAdmin: params.b_IncludeRecipientAdmin,
      b_IncUnapproved: params.b_IncUnapproved,
      b_Option1: params.b_Option1,
      b_Option2: params.b_Option2,
      b_Option3: params.b_Option3,
      b_Option4: params.b_Option4,
      b_Option5: params.b_Option5,
      b_PayBrokerage: params.b_PayBrokerage,
      b_UpdateCompletion: params.b_UpdateCompletion,
      ProgressBar_Max: params.ProgressBar_Max,
      ProgressBar_Text: params.ProgressBar_Text,
      ProgressBar_Value: params.ProgressBar_Value,
      dt_ProcessStartTime: params.dt_ProcessStartTime,
      OperatorID: params.OperatorID,
      s_ABMDefaultAccount: params.s_ABMDefaultAccount,
      s_ABMDefaultActivity: params.s_ABMDefaultActivity,
      s_ABMDefaultProject: params.s_ABMDefaultProject,
      s_EndDate: params.s_EndDate,
      s_EndOfInvoiceString: params.s_EndOfInvoiceString,
      s_ExportLocation: params.s_ExportLocation,
      s_Interface: params.s_Interface,
      s_Option1: params.s_Option1,
      s_Option2: params.s_Option2,
      s_Option3: params.s_Option3,
      s_StartDate: params.s_StartDate,
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      console.log(data)
      if (data == 'True') {
        this.updatedRecords = data[0].updatedRecords
        if (this.updatedRecords == 0) {
          this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
        } else {
          this.globalS.sToast('Success', this.updatedRecords + ' - Payroll Records Updated')
          this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Payroll Run is: ')
        }
        this.ngOnInit();
        return false;
      }
    });
  }
  

  processExecutionTest() {
    this.billingS.postDebtorBilling({
      InvoiceNumber: 'N/A',
      StartDate: this.startDate,
      EndDate: this.endDate,
      OperatorID: this.operatorID,
      CurrentDateTime: this.currentDateTime
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
          } else {
            this.globalS.sToast('Success', this.updatedRecords + ' - Debtor Records Updated')
            this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Debtor Run is: ')
          }
          // this.postLoading = false;
          this.ngOnInit();
          return false;
        }
      });
  }
  
  async selectSavePath() {
    const modalVal = this.inputForm;
    var packageName = modalVal.get('selectedPackage').value;

    try {

      let s_Location: string = '';
      let suggestedName: string = '';
      let mimeType: string = '';
      let types: { description: string, accept: { [key: string]: string[] } }[] = [];
      let selectedDirectoryPath: string = '';

      //these should need to be udpate as per the same as defined for Debtor update module
      switch (packageName) {
        case 'AURION':
        case 'AURION (AWARD)':
          suggestedName = 'AURION.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'GREENTREE':
          suggestedName = 'GREENTREE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CHRIS21':
          suggestedName = 'PAYROLL.DAT';
          mimeType = 'application/dat';
          types = [
            {
              description: 'DAT Files',
              accept: { 'application/dat': ['.dat'] },
            },
          ];
          break;

        case 'MYOB':
          suggestedName = 'MYOB.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'QUICKBOOKS':
          suggestedName = 'QUICKBOOK.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'WAGE EASY PAYROLL':
          suggestedName = 'WAGE.WTC';
          mimeType = 'application/octet-stream';
          types = [
            {
              description: 'WTC Files',
              accept: { 'application/octet-stream': ['.wtc'] },
            },
          ];
          break;

        case 'HR3':
          suggestedName = 'HR3.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ABM':
          suggestedName = 'ABMSUPPLIER.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'QUICKEN PREMIER':
          suggestedName = 'QUICKEN.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'ATTACHE':
        case 'ATTACHE SUPPLIER':
          suggestedName = 'PAYTSHT.INP';
          mimeType = 'application/inp';
          types = [
            {
              description: 'INP Files',
              accept: { 'application/inp': ['.inp'] },
            },
          ];
          break;

        case 'CIM':
          s_Location = getOutputFolder('PayExportFolder', 'Force_PayExportFolder', 'Select the Payroll Export Folder', this.operatorID);
          break;

        case 'MICROPAY':
          suggestedName = 'MicroPayPayrollPath' + '\\TRACCS.MIF';
          mimeType = 'application/mif';
          types = [
            {
              description: 'MIF Files',
              accept: { 'application/mif': ['.mif'] },
            },
          ];
          break;

        case 'PAYGLOBAL':
          suggestedName = 'PAYGLOBAL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'EASYTIME':
          suggestedName = 'EASYTIME.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'NELLER-PRECEDA':
          suggestedName = 'PAY34T.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ALESCO':
          suggestedName = 'AL-PAY.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'LATTICE/CONSISTO':
          suggestedName = 'CONSISTO.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ROSTERLIVE':
          suggestedName = 'ROSTERLIVE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CLOUD PAYROLL':
          suggestedName = 'CLOUDPAYROLL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CONNX':
          suggestedName = 'CONNXPAYROLL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CARE SYSTEMS':
          suggestedName = 'CARESYSTEMS.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        default:
          break;
      }

      function getOutputFolder(folder: string, forceFolder: string, dialogTitle: string, operatorId: string): string {
        return `/output/folder/path/${folder}`;
      }

      const handle = await (window as any).showSaveFilePicker({
        suggestedName: suggestedName,
        types: types,
      });

      const filePath = handle.name;
      const dirHandle = await (window as any).showDirectoryPicker();
      const dirName = filePath;
      const fileHandle = await dirHandle.getFileHandle(dirName, { create: true });

      // console.log(`File path in: ${dirHandle.name}/${dirName}`);

      this.inputForm.patchValue({
        // selectedDirectoryPath: `C:\\Users\\${this.loggedUserName}\\Documents\\${dirHandle.name}\\${dirName}`
      })

    } catch (error) {
      // console.error('Failed to get path:', error);
      this.globalS.iToast('Information', 'Directory Path not selected.');
    }
  }
  selectedDirectoryPath: any;

}

