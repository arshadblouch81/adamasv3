namespace Adamas.Models.Modules{
    public class EmailOutput
    {
      public string ReplyTo { get; set; }
      public string To { get; set; }
      public string From { get; set; }
      public string CC { get; set; }
      public IsSuccessEmail Status { get; set; }
    }

    public class IsSuccessEmail  {
        public string Log { get; set; } = "";
        public bool IsSuccess { get; set; }
    }
}