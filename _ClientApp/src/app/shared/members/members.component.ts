import { Component, OnInit } from '@angular/core';
import { MemberService, GlobalService } from '@services/index';

import { Router } from '@angular/router';
import { distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Subject } from 'rxjs'

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members: Array<any>;
  membersTemp: Array<any>;
  loading:boolean = false;
  isActive: boolean = false;

  search: string;
  textChanges = new Subject<string>();

  constructor(
    private memberS: MemberService,
    private globalS: GlobalService,
    private router: Router
  ) { 


    this.textChanges
    .pipe(
        debounceTime(500),
        distinctUntilChanged()
    ).subscribe(data => {
        this.members = this.membersTemp.filter(x => {
            if((x.accountNo).indexOf(data.toUpperCase()) > -1){
                return x;
            }
        })      
    });

  }

  ngOnInit() {
      this.getmembers(this.isActive);
  }

  getmembers(isActive: boolean){
    this.loading = true;
    this.members = null;
    
    this.memberS.getmembers({
      PersonId: this.globalS.decode().code,
      isActive: isActive
    }).subscribe(data => {
      this.membersTemp = data;
      this.members = this.membersTemp;

      this.loading = false;
    });
  }

  isActiveChanges(event: boolean){
    this.isActive = event;
    this.getmembers(this.isActive);
  }


  toProfile(member: any){
    this.globalS.member = JSON.stringify(member);
    this.router.navigate([`client/members/${member.uniqueID}`])
  }

}
