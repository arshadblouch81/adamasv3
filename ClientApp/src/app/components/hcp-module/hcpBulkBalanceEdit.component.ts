import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { formatDate, formatNumber } from '@angular/common';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { start } from 'repl';

@Component({
    selector: 'app-hcp-bulk-balance-edit',
    templateUrl: './hcpBulkBalanceEdit.component.html',
    styleUrls: ['./hcpFullScreenModal.component.css'],
})

export class HCPBulkBalanceEditComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    modalOpen: boolean = false;

    listData: Array<any>;
    loading: boolean = false;
    inputForm: FormGroup;
    check: boolean = false;
    userRole: string = "userrole";
    tocken: any;
    private unsubscribe: Subject<void> = new Subject();
    confirmModal?: NzModalRef;
    endDate: any;
    startDate: any;

    drawerVisible: boolean = false;
    tryDoctype: any;
    pdfTitle: string;

    editId: number | null = null;
    previousValues: { [key: number]: any } = {};
    auditDescriptionDetail: any;

    constructor(
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private ModalS: NzModalService,
        private modal: NzModalService,
        private cd: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private printS: PrintService,
    ) { }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.loadData();
        this.loading = false;
        this.modalOpen = false;
    }

    handleCancel() {
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/hcp']);
    }

    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    resetModal() {
        this.inputForm.reset();
        this.loading = false;
    }

    loadData() {
        this.loading = true;
        this.billingS.getPeriodEndDate().subscribe(dataEndDate => {
            if (dataEndDate[0].periodEndDate != "") {
                this.endDate = dataEndDate[0].periodEndDate;

                if (typeof this.endDate === 'string') {
                    this.endDate = new Date(this.endDate);
                } else if (this.endDate instanceof Date) {
                    this.endDate = this.endDate;
                } else {
                    throw new Error('Invalid endDate provided');
                }

                this.startDate = startOfMonth(this.endDate)
                this.startDate = formatDate(this.startDate, 'yyyy-MM-dd', 'en_US');
                this.endDate = formatDate(this.endDate, 'yyyy-MM-dd', 'en_US');

                let dataPass = {
                    DateStart: this.startDate,
                    DateEnd: this.endDate,
                    IsWhere: null
                }

                this.billingS.getHcpBulkBalances(dataPass).subscribe(getListDetail => {
                    this.listData = getListDetail;
                });
            }
        });
        this.loading = false;
    }

    startEdit(i: number): void {
        this.editId = i;
        this.previousValues[i] = { ...this.listData[i] }; // Store a copy of the current values
    }

    saveEdit(i: number): void {
        const previousRow = this.previousValues[i];
        const currentRow = this.listData[i];

        if (currentRow.balance != previousRow.balance) {
            this.auditDescriptionDetail = 'Balance changed from ' + previousRow.balance + ' to ' + currentRow.balance + ', Package = ' + currentRow.name + ', Date = ' + formatDate(currentRow.date, 'dd/MM/yyyy', 'en_US');
            this.updateAuditRecord(currentRow);
        }

        if (currentRow.cWealthHeldByProvider != previousRow.cWealthHeldByProvider) {
            this.auditDescriptionDetail = 'CW held by provider changed from ' + previousRow.cWealthHeldByProvider + ' to ' + currentRow.cWealthHeldByProvider + ', Package = ' + currentRow.name + ', Date = ' + formatDate(currentRow.date, 'dd/MM/yyyy', 'en_US');
            this.updateAuditRecord(currentRow);
        }

        if (currentRow.cWealthHeldByCWealth != previousRow.cWealthHeldByCWealth) {
            this.auditDescriptionDetail = 'CW held by client changed from ' + previousRow.cWealthHeldByCWealth + ' to ' + currentRow.cWealthHeldByCWealth + ', Package = ' + currentRow.name + ', Date = ' + formatDate(currentRow.date, 'dd/MM/yyyy', 'en_US');
            this.updateAuditRecord(currentRow);
        }

        if (currentRow.clientHeldByProvider != previousRow.clientHeldByProvider) {
            this.auditDescriptionDetail = 'Balance held by client changed from ' + previousRow.clientHeldByProvider + ' to ' + currentRow.clientHeldByProvider + ', Package = ' + currentRow.name + ', Date = ' + formatDate(currentRow.date, 'dd/MM/yyyy', 'en_US');
            this.updateAuditRecord(currentRow);
        }

        if ((currentRow.cWealthHeldByProvider + currentRow.cWealthHeldByCWealth + currentRow.clientHeldByProvider != currentRow.checkTotal) || (currentRow.balance - currentRow.checkTotal != currentRow.variance)) {
            this.confirmModal = this.modal.confirm({
                nzTitle: 'Out of Balance',
                nzContent: 'This will create an out of balance condition, please resolve before existing sheet',
                nzOnOk: () => {
                    // this.globalS.iToast('Information', 'OK Button Selected.')
                },
                nzOnCancel: () => {
                    // this.globalS.sToast('Success', 'CANCEL Button Selected.')
                }
            });
        } else {
            this.stopEdit(i);
            this.billingS.updatePackageBalance({
                value1: currentRow.balance,
                value2: currentRow.cWealthHeldByProvider,
                value3: currentRow.cWealthHeldByCWealth,
                value4: currentRow.clientHeldByProvider,
                recordNumber: currentRow.recordNumber,
            }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.globalS.sToast('Success', 'Values are updated successfully.');
            });
            this.loadData();
        }
    }

    cancelEdit(i: number): void {
        this.listData[i] = { ...this.previousValues[i] }; // Revert changes
        this.stopEdit(i);
        this.loadData();
    }

    stopEdit(i: number): void {
        this.editId = null;
    }

    updateAuditRecord(data: any): void {
        this.billingS.postAuditHistory({
            Operator: this.tocken.user,
            actionDate: this.globalS.getCurrentDateTime(),
            auditDescription: this.auditDescriptionDetail,
            actionOn: 'PACKAGEBALANCE',
            whoWhatCode: data.accountNumber,
            TraccsUser: this.tocken.user,
        }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            // this.globalS.sToast('Success','Values are updated successfully.');
        });
    }

    //Mufeed 25/4/24
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
    }
    HCPBalance() {


        const data = {
            "template": { "_id": "C126Vu5PE5lFsUWC" },
            "options": {
                "reports": { "save": false },
                "userid": this.tocken.user,
                "SDate": this.startDate,
                "EDate": this.endDate

            }
        }
        var Title = "HCP Balance"
        this.loading = true;
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = Title + ".pdf"
            this.drawerVisible = true;
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            console.log(err);
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });
        this.tryDoctype = "";
        return;

    }



}
