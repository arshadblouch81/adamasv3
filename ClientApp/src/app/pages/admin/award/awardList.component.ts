import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';

@Component({
  selector: 'app-award-list',
  templateUrl: './awardList.component.html',
  styles: [`
`]
})

export class AwardListComponent implements OnInit {

  isVisible: boolean = true;
  awardListData: Array<any>;
  loading: boolean = false;
  inputForm: FormGroup;
  check: boolean = false;
  userRole: string = "userrole";
  title: string = "Add or Change Award Rule Sets"
  tocken: any;
  columnstoSee: any = true;
  bodystyle: object;
  private unsubscribe: Subject<void> = new Subject();
  tmpValue: any;
  confirmModal?: NzModalRef;

  constructor(
    private globalS: GlobalService,
    private timeS: TimeSheetService,
    private menuS: MenuService,
    private formBuilder: FormBuilder,
    private router: Router,
    private billingS: BillingService,
    private modal: NzModalService,
    private host: ElementRef<Element> = inject(ElementRef)
  ) { }

  enterFullscreen() {
    this.host.nativeElement.requestFullscreen();
  }
  exitFullscreen() {
    document.exitFullscreen();
  }

  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    this.loadData();
    this.loading = false;
  }


  loadTitle() {
    return this.title;
  }

  handleCancel() {
    this.router.navigate(['/admin/configuration']);
  }

  buildForm() {
    this.inputForm = this.formBuilder.group({
      code: '',
      description: '',
      category: '',
      recordNumber: null,
    });
  }

  loadData() {
    this.loading = true;
    this.billingS.getAwardRuleList().subscribe(awardRuleList => {
      this.awardListData = awardRuleList;
      this.loading = false;
    });
  }

  delete(data: any) {
    const group = this.inputForm;
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Do you want to Delete?',
      nzContent: 'Are you sure you want to delete the Award Rule set "' + data.code + '" ?',
      nzOnOk: () => {
        this.tmpValue = data.code;
        this.billingS.deleteAwardRuleSet(data.recordNo)
        .pipe(takeUntil(this.unsubscribe)).subscribe(dataDeleted => {
        if (dataDeleted) {
          this.billingS.postAuditHistory({
            Operator: this.tocken.user,
            actionDate: this.globalS.getCurrentDateTime(),
            auditDescription: 'DELETE AWARD '+data.code,
            actionOn: 'AWARDPOS',
            whoWhatCode: data.recordNo, //inserted
            TraccsUser: this.tocken.user,
          }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.globalS.sToast('Success','Award Rule set "' + this.tmpValue + '" has been deleted successfully.');
          }
          );
          this.loadData();
          return;
        }
      });
      },
      nzOnCancel: () => {
        // this.globalS.sToast('Success', 'CANCEL Button Selected.')
      }
    });
    this.tmpValue = "";
  }

  showEditModal(index: any) {
    console.log("value is " + index)

    // this.title = "Edit Award Levels"
    // this.isUpdate = true;
    // this.current = 0;
    // this.modalOpen = true;
    // const { 
    //   name,
    //   end_date,
    //   recordNumber,
    // } = this.tableData[index];
    // this.inputForm.patchValue({
    //   name: name,
    //   end_date:end_date,
    //   recordNumber:recordNumber,
    // });
    // this.temp_title = name;
  }
}
