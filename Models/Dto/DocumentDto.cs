﻿using System;

namespace AdamasV3.Models.Dto
{
    public class DocumentDto
    {
        public int DocID { get; set; }  
        public string DocumentGroup { get; set; }
        public string Title { get; set; }
        public string Created { get; set; }
        public string Status { get; set; }
        public string CareDomain { get; set; }
        public string Discipline { get; set; }
        public string Program { get; set; }
        public string Classification { get; set; }
        public string Type { get; set; }
        public string OriginalLocation { get; set; }
        public string Category { get; set; }
        public string Modified { get; set; }
        public string Filename { get; set; }
        public DateTime? DCreated { get; set; }

        public DateTime? AlarmDate { get; set; }
        public string AlarmText { get; set; }
        public string Notes { get; set; }
        
    }
}
