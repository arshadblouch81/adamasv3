import { Injectable, Injector, ErrorHandler } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginService, GlobalService, TokenRefreshValidation } from '@services/index'

import { Observable, Subscription, interval, Subject, timer} from 'rxjs';
import { takeUntil, repeatWhen, map, startWith, switchMap  } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
})
export class TimerService {

    private reset$ = new Subject();
    timer$: Observable<any>;
    subscription: Subscription;

    timerInterval: number = 1000;
    mouseVal: boolean = false;
    sessionTime:number=0;
    private resetActivity$ = new Subject();
    checkActivity$: Observable<any>;
    checkActivitySubscription: Subscription;

    constructor(
        private jwtHelper: JwtHelperService,
        private loginS: LoginService,
        private globalS: GlobalService
    ) { 
        
        this.timer$ = this.reset$.pipe(
            startWith(0),
            switchMap(() => timer(0, this.getInterval))
        );

    this.checkActivity$ = this.resetActivity$.pipe(
            startWith(0),
            switchMap(() => timer(0, this.getInterval - 10000))
        );
    }
    set setMouseMoving(val: boolean){
       
        if (val==true){
          
           this.restTimer();
           
        }
        this.mouseVal = val;
    }

    get isMouseMoving(){
       
        return this.mouseVal;
    }

    set setInterval(val: number){
        this.timerInterval = val;
        this.sessionTime=val;
    }

    get getInterval(): number{
        return this.timerInterval ?? 1000;
    }

    restTimer(){

        if (this.subscription!=null) 
            this.subscription.unsubscribe();
        
        this.timer$ = this.reset$.pipe(
            startWith(0),
            switchMap(() => timer(0, this.getInterval))
        );
        this.startTimer(); 
    }

    startActivityTimer(){
        this.checkActivitySubscription = this.checkActivity$.subscribe((i) => { 
            if(this.isMouseMoving && i > 0){
                console.log('refresh token')
                this.refreshToken();
            }
        });  
    }

    refreshActivityTimer(): void{
        this.resetActivity$.next(0);
    }

    refreshToken(){
        if(this.globalS.access){
            var token: any = JSON.parse(this.globalS.access);
            var toBeRefreshed: TokenRefreshValidation = { AccessToken: token.access_token, RefreshToken: token.refresh_token }
            this.loginS.refreshToken(toBeRefreshed)
                .subscribe(data => {
                    this.globalS.token = data.access_token;
                    this.globalS.access = data;
                })
        }        
    }

    startTimer(){
        this.subscription = this.timer$.subscribe((i) => { 
            if(i > 0){     
                console.log('check token validity')           
                this.timerFire();                
            }
        });        
    }

    timerFire(): void{
        if(this.jwtHelper.isTokenExpired()) {
            console.log('logout')
            this.refreshTimer();
            this.logout();
        }
    }

    refreshTimer(): void {
        this.reset$.next(void 0);
    }

    logout(): void{
        this.stopTimer();
        const token = this.globalS.decode();
        if(token){
            this.loginS.logout(token.uniqueID)
                .subscribe(data => {
                    this.globalS.logout();
                });
        }
    }

    stopTimer(): void{
        if(this.subscription){
            this.subscription.unsubscribe();
        }

        if(this.checkActivitySubscription){
            this.checkActivitySubscription.unsubscribe();
        }
    }

    
}