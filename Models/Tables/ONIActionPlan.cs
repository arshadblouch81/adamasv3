using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DocuSign.eSign.Model;

namespace Adamas.Models.Tables
{
    public class ONIActionPlan
    {
        [Key]
        public int RecordNumber { get; set; }
        public string HealthProfessional { get; set; }
        public string For { get; set; }
        public string Consent { get; set; }
        public string PersonID { get; set; }
        public string Referral { get; set; }
        public string Transport { get; set; }
        public string Feedback { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? Review { get; set; }
    }
}
