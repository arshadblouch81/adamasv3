using Microsoft.Data.SqlClient;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using Adamas.Models.Modules;
using Adamas.Models.Dto;

namespace Adamas.DAL {
    public interface IGeneralSetting{
        string DBLookup(string Field, string table, string where, ref string s_status);
        string ComputeBookingLeadTime(Object obj, string defaultValue = "");
        string NullToStr(object obj, string default_value = "");
        string DetermineMainGroup(string rosterGroup, string mainGroup);
        string GetDayMask(bool b_SDay, string s_EarliestDate);
        bool HasColumn(SqlDataReader rd, string colName);
        string GetSqlDate(string dt);
        int GetBlocks(string StrTime);
        dynamic Filter(object input);
        dynamic FilterInput(object input);
        int TypeNumber(string RosterGroup);
        Task<bool> IfRecipientAccountNameExist(string accountName);
        Task<string> getValue(string sqlString, List<KeyValuePair<string,string>> parameters = null);
        int getDayNoOfWeek(DayOfWeek weekday);
        string GetSqlDate(System.DateTime dat);
        Task<PayPeriod> GetPayPeriod(string s_status);
        string GetFilteredRecipient_Criteria(string operatorid, string s_SQLStmt);
        string GetFilteredStaff_Criteria(string operatorid);
        string CalculateEndTime(DateTimeObject obj);
        void GetUserFilters(string operatorid);
        bool Null2Bool(string data);
        Task<List<TimeSheetRosterDto>> GetTimeSheet(GetTimesheet ts);
        double GetTimeGap(TimeSheetRosterDto firstDate, TimeSheetRosterDto secondDate);
        bool IsDate(Object value);
        string CalculateEndTime(string StartTime, double DurationInMinutes, string s_Format = "");
        Task<PayPeriod> GetPayPeriod();
        Task<ComputeTimesheet> GetComputeTimesheet(ComputeTimeInput input);
           
        Task<string> PublishedDate();
        Task<RegistrationCounter> GetAccountNameGenerator(string user);
        Task<Boolean> UpdateSysTable(string user, int counter);
        DateTime? TryDateParse(string text);
        Task<Boolean> IsHolidateAsync(string date);
        Task<string> GetDateMaskValue(string date);
        Task<String> AllowedDaysString(DateTime? date);
        Task<Boolean> InsertAuditAsync(AuditDTO audit, SqlConnection conn, SqlTransaction transaction = null);

        Task<bool> ErrorLog(string ModuleName, string ModuleFunction, string Error, string user );
    }
}