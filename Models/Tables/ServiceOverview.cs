using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class ServiceOverview
    {
        [Key]
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; }

        [Column("Service Type")]
        public string ServiceType { get; set; }
        public Double? Frequency { get; set; }
        public string Period { get; set; }
        public Single? Duration { get; set; }
        public string UnitType { get; set; }
        [Column("Unit Bill Rate")]
        public double? UnitBillRate { get; set; }
        [Column("Activity BreakDown")]
        public string ActivityBreakDown { get; set; }
        public string  ServiceProgram { get; set; }
        public string ServiceBiller { get; set; }
        public string BudgetType { get; set; }
        public double? BudgetAmount { get; set; }
        public string BudgetLimitType { get; set; }
        public string BudgetStartDate { get; set; }
        public string ServiceStatus { get; set; }
        public bool? ForceSpecialPrice { get; set; }
        public string TaxRate { get; set; }
        public bool? ExcludeFromNDIAPriceUpdates { get; set; }
        public bool? AutoInsertNotes { get; set; }
        

    }
}