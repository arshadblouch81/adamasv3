import { NgModule, ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'

import { Table } from './table'
import { TableBody } from './table-body'

@NgModule({
    declarations: [
        Table,
        TableBody
    ],
    imports: [
        CommonModule
    ],
    exports: [
        Table,
        TableBody
    ]
})

export class TableModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: TableModule
        }
    }
}