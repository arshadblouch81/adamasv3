using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Adamas.Models.Modules{
    public class IdGenerator{
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;

        public IdGenerator(
            DatabaseContext context,
            IGeneralSetting setting
        ){
            _context = context;
            GenSettings = setting;
        }

        public IdGeneratorOutput Generate(){
            int _carerId = 0;
            string _prodId = string.Empty;
            string finalCarerId = string.Empty;
            
            try
            {
                using(var conn = _context.Database.GetDbConnection() as SqlConnection){
                    conn.OpenAsync();

                    using(SqlCommand cmd = new SqlCommand(@"SELECT SYSTABLE.CARERID, REGISTRATION.PRODNAME FROM Registration CROSS JOIN SysTable;", (SqlConnection) conn))
                    {
                        using(SqlDataReader rd = cmd.ExecuteReader()){

                            while(rd.Read()){
                                 _carerId = Convert.ToInt32(GenSettings.Filter(rd[0])) + 1;
                                _prodId = GenSettings.Filter(rd[0]);
                            }                                       
                        }           
                        finalCarerId = String.Concat("S", _prodId, _carerId.ToString().PadLeft(8,'0'));
                    }

                    // using(SqlCommand cmd = new SqlCommand(@"UPDATE SysTable SET CarerID = @carerId WHERE SQLID = 1", (SqlConnection) conn)){
                    //     cmd.Parameters.AddWithValue("@carerId", _carerId);
                    //     await cmd.ExecuteNonQueryAsync();
                    // }
                }
                return new IdGeneratorOutput() {
                    UniqueId = finalCarerId,
                    NewCarerId = _carerId
                };
            } catch(Exception ex)
            {
                return null;
            }
        }
    }


    public class IdGeneratorOutput {
        public string UniqueId { get; set; }
        public int NewCarerId { get; set; }
    }
}