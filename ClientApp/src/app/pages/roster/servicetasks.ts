import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit,Input,Output,HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService, ListService, MenuService, PrintService, TimeSheetService } from '@services/index';
import { SwitchService } from '@services/switch.service';
import { NzModalService } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'service-tasks',
  templateUrl: './servicetasks.html',
  styles: [`
  td {
    margin:-2px;
    padding:0px;
  }
  .selected{
        background-color: #85B9D5;
        color: white;
    }
  .heading{
    font-size: 16px;
    font-family: 'Segoe UI';
    color: #665d5d;
    font-weight: bold;
    margin: 5px;
    
}
.line{
    border-bottom: 1px solid #ccc;
    margin: 0px;
    padding: 0px;
    width: 100%;
    margin-top: 5px;
    margin-bottom: 5px;
}
  `]
})
export class ServiceTasks implements OnInit {
  
  @Input() loadTasks: Subject<any>;

  listTasks: Array<any>=[];
  selectedTasks: Array<any>=[];

  ViewServiceTaskList:boolean;
  HighlightRow:number;
  NewTask:any;
  tableData: Array<any>;

  loading: boolean = false;
  recordNo:string;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  modalVariables:any;
  inputVariables:any;
  title:string = "Add Tasks"
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;   
  dateFormat: string ='dd/MM/yyyy';
  check : boolean = false;
  userRole:string="userrole";
  whereString :string="Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  private unsubscribe: Subject<void> = new Subject();
   
  temp_title: any;
  searchTerm: string = '';
  currentIndex: number | null = null;
  selectedAllocatedTasks: Array<any>=[];
  activeRow:any;
  selectedIndex:number;

  constructor(
    private globalS: GlobalService,
    private cd: ChangeDetectorRef,
    private switchS:SwitchService,
    private listS:ListService,
    private timeS:TimeSheetService,
    private printS:PrintService,
    private menuS:MenuService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    ){}
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      //this.buildForm();
     
      this.loadTasks.subscribe(d=>{
        this.loadSericeTasks(d);
    })    
    
      this.cd.detectChanges();
    }
    loadTitle(){
      return this.title;
    }
    showAddModal() {
      this.selectedTasks=[];
      this.loadTaskList();
      this.title = "Tasks List"
     // this.resetModal();
      
    }
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      // debugger;
      this.title = "Edit Tasks"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      const { 
        name,
        end_date,
        recordNumber,
      } = this.tableData[index];
      this.inputForm.patchValue({
        name: name,
        end_date:end_date,
        recordNumber:recordNumber,
      });
      this.temp_title = name;
    }
    
    handleCancel() {
      this.modalOpen = false;
    }
  
    loadSericeTasks(recordNo:string){
      
      this.recordNo=recordNo;
      this.timeS.getRosterTasks(this.recordNo).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.tableData = data;
        this.cd.detectChanges();
      });
    }

    loadTaskList(){

      let selectedTasks =this.tableData.map(x=>x.taskNotes).join("','").toString();
      let sql= `SELECT RecordNumber,Description, 0 as selected FROM DATADOMAINS WHERE Domain = 'TASK' and [Description] not in ('${selectedTasks}') ORDER BY Description`;

      this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(d=>{
        this.listTasks=d;
        this.ViewServiceTaskList = true;
      })
    }
   RecordClicked(event: any, value: any, i: number, mian:boolean) {

        this.activeRow = value;
        if (mian)
             this.selectedIndex =i;
        else
            this.selectedIndex =-1;
      
        if (event.ctrlKey || event.shiftKey) {
            if (!this.selectedAllocatedTasks.includes(value))
                 this.selectedAllocatedTasks.push(value);
            //this.selected.push(value.recordNo)
          
    
        } else {
          
          this.selectedAllocatedTasks = [];
         // this.selected = [];
    
          if (this.selectedAllocatedTasks.length <= 0) {
            this.selectedAllocatedTasks.push(value)
            
          }
        }
      }
   
    

deleteSelectedRecords(){

        //   setTimeout(() => {
        //   this.selectedAllocatedTasks.forEach(v=>{
        //     this.delete(v);
        //   });
        // },1000);
        let selectedTasks =this.selectedAllocatedTasks.map(x=>x.recordNo).join(",").toString();

        this.timeS.deleteRosterTasks(selectedTasks).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.loadSericeTasks(this.recordNo);
        });
        
 }


       
        handleOkTop() {
         // this.generatePdf();
          this.tryDoctype = ""
          this.pdfTitle = ""
        }
        handleCancelTop(): void {
          this.drawerVisible = false;
          this.pdfTitle = ""
        }
        generatePdf(){
          var Title = "Service Tasks List";
         
          this.loading = true;
          this.drawerVisible = true;
          /*const temp =  forkJoin([
              this.listS.getlist(SQL), 
              ]);   */ 
             // temp.subscribe(x => {                         
              //this.rptData =  x[0]; 
              //console.log(this.rptData)      
              
                  const data = {
                      "template": { "_id": "6BoMc2ovxVVPExC6" },
                      "options": {
                          "reports": { "save": false },                        
                          "sql": this.tableData,//this.rptData,                        
                          "userid": this.tocken.user,
                          "txtTitle":  Title,                      
                      }
                  }              
                  this.loading = true;
                 
                          
                  this.drawerVisible = true;
                  this.printS.printControl(data).subscribe((blob: any) => {
                              this.pdfTitle = Title+".pdf"
                              this.drawerVisible = true;                   
                              let _blob: Blob = blob;
                              let fileURL = URL.createObjectURL(_blob);
                              this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                              this.loading = false;
                              this.cd.detectChanges();
                          }, err => {
                              console.log(err);
                              this.loading = false;
                              this.ModalS.error({
                                  nzTitle: 'TRACCS',
                                  nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                  nzOnOk: () => {
                                      this.drawerVisible = false;
                                  },
                              });
                  });                            
                  return;        
            //  });         
      }

    onItemSelected(sel: any, i:number): void {
          console.log(sel)  
          this.HighlightRow=i;   
          this.NewTask=sel;   
          sel.selected=sel.selected=="1" ? "0" : "1";
          let d=this.selectedTasks.indexOf(sel)

          if (d<0 && sel.selected=="1")
            this.selectedTasks.push(sel)
          else if (d>=0 && sel.selected=="0"){           
              this.selectedTasks.splice(d,1)
          }
    }
   
    onItemDbClick(sel: any, i:number) : void {      
      this.HighlightRow=i;         
      this.ViewServiceTaskList=false;   
      this.NewTask=sel;
      sel.selected=sel.selected=="1" ? "0" : "1";
      let d=this.selectedTasks.indexOf(sel)

          if (d<0 && sel.selected=="1")
            this.selectedTasks.push(sel)
          else if (d>=0 && sel.selected=="0"){           
              this.selectedTasks.splice(d,1)
          }
                  
            this.SaveSelected();
        
          
    }
  
    SaveSelected(){

      let newTasksList:any =this.selectedTasks.map(x=>{

            return {
                  RosterID: this.recordNo,
                  TaskID : x.recordNumber,
                  TaskDetail : x.TaskDetail,
                  TaskNotes : x.description,
                  TaskOrder : 1,
                  TaskComplete : 0,
                  XDeletedRecord : 0
            }
                  
      });

      this.timeS.saveRosterTasks(newTasksList).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.loadSericeTasks(this.recordNo);
        this.ViewServiceTaskList=false;
      });


      // setTimeout(() => {
      // this.selectedTasks.forEach(v=>{
      //   this.save(v)
      //   });
      // },1000);

      // this.ViewServiceTaskList=false;   
      // this.loadSericeTasks(this.recordNo);
      
    }
    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
      // Handle character input
      if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
          this.searchTerm += event.key.toLowerCase();
          this.scrollToRow();
      } 
      // Handle backspace
      else if (event.key === 'Backspace') {
          // Only reset if there's something to remove
          this.searchTerm = '';
          if (this.searchTerm.length > 0) {
              this.searchTerm = this.searchTerm.slice(0, -1);
              this.scrollToRow();
          }
      }
      // Handle other keys (optional)
      else if (event.key === 'Escape') {
          // Reset search term on escape key
          this.searchTerm = '';
          this.scrollToRow();
      }
    }

    scrollToRow() {
        // Find the index of the first matching item
        const matchIndex = this.tableData.findIndex(item => 
            item.task.toLowerCase().startsWith(this.searchTerm)
        );
        // Scroll to the matching row if it exists
        if (matchIndex !== -1) {
            const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
            if (tableRow) {
                tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                this.highlightRow(tableRow);
            }
        } else {
        }
    }

    highlightRow(row: HTMLElement) {
        row.classList.add('highlight');
        setTimeout(() => row.classList.remove('highlight'), 2000);
    }

}
      