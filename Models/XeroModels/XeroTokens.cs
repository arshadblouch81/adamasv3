using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Adamas.Models.XeroModels {
    public class XeroTokens
    {
        public string Id_Token { get; set; }
        public string Access_Token { get; set; }
        public string Refresh_Token { get; set; }
    }
}