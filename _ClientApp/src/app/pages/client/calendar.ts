
import { takeUntil } from 'rxjs/operators';
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit,
  OnDestroy,
  Input,
  OnChanges
} from '@angular/core';

import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarDayViewBeforeRenderEvent
} from 'angular-calendar';

import * as moment from 'moment';

import { BsModalComponent } from 'ng2-bs3-modal';

const colors: any = {
  red: {
    primary: '#ad2121'
  },
  blue: {
    primary: '#1e90ff'
  },
  yellow: {
    primary: '#e3bc08'
  },
  purple: {
    primary: '#5f00a4'
  },
  brown: {
    primary: '#9b3400'
  },
  blue2: {
    primary: '#25918d'
  },
  green: {
    primary: '#2ba316'
  }
};


import { TimeSheetService, GlobalService, StaffService, ClientService } from '../../services/index';


interface RosterEvent {
  billRate: string,
  carer: string,
  client: string,
  endTime: string,
  notes: string,
  recordNo: string,
  service: string,
  startTime: string,
  status: string,
  type: string
}


@Component({
  selector: 'calendar',
  templateUrl: './calendar.html',
  styles: [`
    h3{
      margin:0;
    }
    .active{
      background:#0c5175;
    }
    .time-segment{
      float: right;
      margin: 1rem;
      width: 6rem;
    }
  `]
})

export class CalendarClient implements OnInit, OnDestroy {

  @Input() accountNo;
  @ViewChild('modalContent', { static: false }) modalContent: TemplateRef<any>;
  @ViewChild('roster', { static: false }) roster: BsModalComponent;

  private unsubscribe$ = new Subject();

  view: string = 'month';

  viewDate: Date = new Date();

  toggleAdminFees: boolean = false;

  timeArray: Array<number> = [5, 10, 15, 30];

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="material-icons icons"> create </i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="material-icons icons"> clear </i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();
  rosterData: RosterEvent;

  events: CalendarEvent[];
  temporaryEvents: CalendarEvent[];

  activeDayIsOpen: boolean = false;

  hourSegments: number;
  minuteInterval: number;

  constructor(
    private timeS: TimeSheetService,
    private globalS: GlobalService,
    private staffS: StaffService,
    private clientS: ClientService,
    private modal: NgbModal
  ) {

  }

  ngOnInit() {
    this.search(
      moment().startOf('month').format('YYYY/MM/DD'),
      moment().endOf('month').format('YYYY/MM/DD')
    )
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  filterDate(datalist: Array<any>, isToggleAdmin: boolean = false, initialLoad: boolean = false) {
    this.events = datalist.map(x => {
      let totalColors = Object.keys(colors);

      let sTime = moment(x.shift_Start).format('HH:mm');
      let eTime = moment(x.shift_End).format('HH:mm');

      return {
        start: new Date(x.shift_Start),
        end: new Date(x.shift_End),
        title: `${x.serviceType} |  ${x.carerCode} | ${sTime} - ${eTime}`,
        //actions: this.actions,
        color: colors[totalColors[this.designateColor(x.status, x.type)]],
        data: {
          client: x.clientCode,
          service: x.serviceType,
          carer: x.carerCode,
          startTime: sTime,
          endTime: eTime,
          notes: x.notes,
          recordNo: x.recordNo,
          type: x.type,
          billUnit: x.billUnit,
          billRate: x.billRate,
          status: x.status
        }
      }
    }).filter(x => {
      var type = !isToggleAdmin ? "14" : ""
      if (x && x.data.type !== type)
        return x;
    });
    this.refresh.next();
  }

  designateColor(_status: string, _type: string): number {
    const type = parseInt(_type);
    const status = parseInt(_status);

    if (type == 1) {
      return 0;
    }

    if (type > 1) {
      if (status == 1) return 6;
      if (status == 2) return 3;
      if (status == 3 || status == 4) return 4;
    }

  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  search(startDate: any, endDate: any) {

    this.staffS.getroster({
      RosterType: 'PORTAL CLIENT',
      AccountNo: this.accountNo || this.globalS.decode().code,
      StartDate: startDate,
      EndDate: endDate
    }).pipe(
      takeUntil(this.unsubscribe$))
      .subscribe(data => {
        this.temporaryEvents = data;
        this.filterDate(this.temporaryEvents, null, true);
      });
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  title: string;
  handleEvent(action: string, event: any): void {
    const roster: RosterEvent = event.data;
    this.rosterData = roster;

    const type = parseInt(this.rosterData.type);
    const status = parseInt(this.rosterData.status);

    if (type == 1) {
      this.title = "UNASSIGNED BOOKING";
    }

    if (type > 1) {
      if (status == 1) this.title = "ASSIGNED BOOKING";
      if (status == 2) this.title = "FINALISED SERVICE";
      if (status == 3 || status == 4) this.title = `BILLED ${this.rosterData.recordNo}`;
    }

    this.roster.open();

    //this.clientS.getprimaryaddressnyname(roster.client).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
    //  this.addresses = data;
    //});

    // this.modalData = { event, action };
    // this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events.push({
      title: 'New event',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    });
    this.refresh.next();
  }

  toggle(toggle: boolean) {
    this.filterDate(this.temporaryEvents, toggle);
  }

  dateChanged(event: any, view: string = "") {
    // if(view === 'day' && !this.isDateValid(event)){
    //   return false;
    // }

    if (this.view === 'month') {

      const startDate = moment(event).startOf('month');
      const endDate = moment(event).endOf('month');

      this.search(
        moment(startDate).format('YYYY/MM/DD'),
        moment(endDate).format('YYYY/MM/DD')
      );
    }

    if (this.view === 'week') {

      const startDate = moment(event).startOf('week');
      const endDate = moment(event).endOf('week');

      this.search(
        moment(startDate).format('YYYY/MM/DD'),
        moment(endDate).format('YYYY/MM/DD')
      );
    }

    if (this.view === 'day') {
      const startDate = event;
      this.minuteInterval = 5;
      this.hourSegments = 12;
      // this.search(
      //   moment(startDate).format('YYYY/MM/DD'), 
      //   moment(startDate).format('YYYY/MM/DD')
      // );
    }
  }


  timeSegChange(num: number) {

    if (num == 5) {
      this.hourSegments = 12
    }

    if (num == 10) {
      this.hourSegments = 6
    }

    if (num == 15) {
      this.hourSegments = 4
    }

    if (num == 30) {
      this.hourSegments = 2
    }

    setTimeout(() => {
      this.refresh.next();
    }, 10);
  }


}
