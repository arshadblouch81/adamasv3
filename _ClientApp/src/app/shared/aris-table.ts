import { Component, Input, OnInit, forwardRef,AfterViewInit, OnChanges,SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, FormArray , Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor  } from '@angular/forms';

export const ARISTABLE_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => ArisTable),
};

@Component({
    selector: 'aris-table',
    templateUrl: './aris-table.html',
    providers:[ARISTABLE_VALUE_ACCESSOR],
    styles:[
        `
        thead tr{
            font-weight:600;
        }
        td{
            font-size:11px;
        }
        `
    ]
})

export class ArisTable implements OnChanges {
    @Input() source: any;
    @Input() columns: Array<string> = [];

    valid: boolean = true;
    keys: Array<any>;

    constructor(){ 

    }

    ngOnChanges(changes: SimpleChanges){
        for(let property in changes){
            if(property == 'source' && changes[property].currentValue != null)
                this.initTable(changes[property].currentValue);  
            if(property == 'columns')
                this.columns = changes[property].currentValue;
        }
    }

    initTable(data: any){
        this.source = data;
    }
}