import { Component, OnInit, ElementRef, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  host:{
    '(document:click)': 'onClick($event)'
  },
  selector: 'dropdown-report',
  templateUrl: './dropdown-report.component.html',
  styleUrls: ['./dropdown-report.component.css'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownReportComponent),
      multi: true      
    }
  ]
})
export class DropdownReportComponent implements OnInit, ControlValueAccessor {

  @Input() SQLSTATEMENT: string;
  @Input() SQLSTATICLIST: Array<any> = [];

  lists: Array<any> = [];
  isHide: boolean = false;
  all: boolean = true;
  spillValue: number;

  value: any = '';
  spliced: Array<any> = []

  private onChange: Function
  private onTouch: Function;

  constructor(
    private elem: ElementRef
  ) { }

  ngOnInit() {
    this.search();
  }

  onClick(event: Event){
    if(!this.elem.nativeElement.contains(event.target))
        this.isHide = false;
  }

  search(){
    if(this.SQLSTATICLIST.length > 0){
      this.lists = this.SQLSTATICLIST.map(x => {
        return {
          selected: false,
          name: x
        }
      });
    }
  }

  defaultValues(lists: string){
    var splits = lists.split(',');
    if(splits.length > 0){
        this.loopValues(this.lists, splits)
    }
  }

  loopValues(lists: Array<any>, defaults: Array<string>){
    var i = 0, j =0, a = lists, b = defaults;
    var common: Array<string> = [];

    while (i < a.length && j < b.length) {     
      while (a[i].name < b[j]) {                  
          ++i;                               
      }
      while (b[j] < a[i].name) {                  
          ++j;                               
      }
      if (a[i].name === b[j]) {                
          a[i].selected = true;
          common.push(b[j])
          ++i;                               
          ++j;
      }
    }

    this.onCheckBoxChange()
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  showme: boolean = false;
  onCheckBoxChange(){
    this.all = false;

    setTimeout(() => {     
      var len = 0;

      var selected = this.lists.map(x => {
        if(x.selected){ return x.name }        
      }).filter(x => x);

      var filtered = selected.map(x => {
          len = len + x.length;
          if(len < 45) {
             return x
          }        
      }).filter(x => x);

      if(len > 45)  {
        this.showme = true;
        this.spillValue = selected.length - filtered.length;
      } else {
        this.showme = false;
      }

      this.spliced = filtered 

      var joinedResult = selected.join(",");
      this.onChange(joinedResult);

    }, (100));
    
  }

  writeValue(value: any): void { 
    if(!value || value == 'ALL'){
      this.allCheckBoxChange(true);
      this.value = 'ALL';
    } else {
      this.value = value;
      this.defaultValues(this.value);   
    }
  }

  allCheckBoxChange(event: boolean){
    this.all = event;
    this.uncheckList();
    this.spliced = []
  }


  uncheckList(){
    var len = this.lists.length;
    for(var a = 0 ; a < len ; a++){
      this.lists[a].selected = false;
    }
  }
}
