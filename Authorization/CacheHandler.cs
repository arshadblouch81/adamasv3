using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

using Adamas.DAL;

namespace Adamas.Authorization{
    public class CacheHandler : AuthorizationHandler<CacheRequirement>
    {
        private ICacheService cache;
        private IHttpContextAccessor _httpContextAccessor = null;

        public CacheHandler(
            ICacheService _cache,
            IHttpContextAccessor httpContextAccessor
        )
        {
            cache = _cache;
            this._httpContextAccessor = httpContextAccessor;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context, 
            CacheRequirement requirement)
        {
            HttpContext httpContext = _httpContextAccessor.HttpContext;

            string authString = httpContext.Request.Headers["Authorization"].ToString();            

            if (!context.User.HasClaim(c => c.Type ==  "uniqueID"))
            {
                return Task.CompletedTask;
            }
    
            var id = Convert.ToString(context.User.FindFirst(c => c.Type == "uniqueID").Value);
            var token = cache.GetTokenValue(id).data;


            if( token != null && httpContext.Request.Headers["Authorization"].ToString().Substring("Bearer".Length).Trim() == token){
                context.Succeed(requirement);
            } else {
                context.Fail();
            }

            if(context.HasFailed){
                var filterContext = _httpContextAccessor.HttpContext;
                var Response = filterContext.Response;
                var message = Encoding.UTF8.GetBytes("Error Unauthorized");

                Response.OnStarting(async () => {
                    Response.StatusCode = 403;
                    await Response.Body.WriteAsync(message,0, message.Length);
                });
            }

            return Task.CompletedTask;
        }
    }
}