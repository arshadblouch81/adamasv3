import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';


@Component({
    selector: '',
    styles: [`
        nz-select{
            width:100%
        }
        nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin: 10px;
    }

    .selected{
        background-color: #85B9D5;
        color: white;
    }

    `],
    templateUrl: './others.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class OthersContact implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    
    isDisabled: boolean = false;

    visible: boolean = false;
    group: any ='ASSOCIATEDPARTY';

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
    ) {
        // this.cd.detectChanges();
        
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'others-contact')) {
                this.user = {...data};
                this.cd.markForCheck();
            }
        });
        
    }

    ngAfterContentInit(){
        this.visible = true;
    }

    ngOnInit(): void {
        this.user = Object.assign({}, this.sharedS.getPicked());        
        this.cd.markForCheck();
    }

    ngOnDestroy(): void {
        this.visible = false;
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}