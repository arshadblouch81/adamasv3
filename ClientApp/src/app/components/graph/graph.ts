import { AfterViewInit, Component, Input, OnInit , ChangeDetectorRef} from "@angular/core";
import { GlobalService, ListService, TimeSheetService, ShareService,fundingDropDowns, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject,EMPTY } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

//import {Chart} from 'chart.js';
import { Chart, registerables } from 'chart.js';

@Component({
    selector: 'graph-view',
    templateUrl: './graph.html',
    styleUrls: ['./graph.css']
   
    
  })
  export class GraphComponent implements OnInit, AfterViewInit {
    private unsubscribe: Subject<void> = new Subject();
    dateFormat: string = 'dd/MM/yyyy';
    user: any;
    loading: boolean = false;
    @Input() ViewGrpah:Subject<any>=new Subject();
    openModel:boolean=false;
    topBelow = { top: '20px' }
    labels:Array<any> =[];
    funds :Array<any>=[];
    funds_ou :Array<any> = [];
    acb :Array<any> = [];
    myChart: any;
    title:string;
	
	chartOptions = {
		theme: "light2",
		animationEnabled: true,
		zoomEnabled: true,
		title: {
			text: "Market Capitalization of ACME Corp"
		},
		axisY: {
			labelFormatter: (e: any) => {
				var suffixes = ["", "K", "M", "B", "T"];
 
				var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
				if(order > suffixes.length - 1)
					order = suffixes.length - 1;
 
				var suffix = suffixes[order];
				return "$" + (e.value / Math.pow(1000, order)) + suffix;
			}
		},
		data: [{
			type: "line",
			xValueFormatString: "YYYY",
			yValueFormatString: "$#,###.##",
			dataPoints: [
			  { x: new Date(1980, 0, 1), y: 2500582120 },
			  { x: new Date(1981, 0, 1), y: 2318922620 },
			  { x: new Date(1982, 0, 1), y: 2682595570 },
			  { x: new Date(1983, 0, 1), y: 3319952630 },
			  { x: new Date(1984, 0, 1), y: 3220180980 },
			  { x: new Date(1985, 0, 1), y: 4627024630 },
			  { x: new Date(1986, 0, 1), y: 6317198860 },
			  { x: new Date(1987, 0, 1), y: 7653429640 },
			  { x: new Date(1988, 0, 1), y: 9314027340 },
			  { x: new Date(1989, 0, 1), y: 11377814830 },
			  { x: new Date(1990, 0, 1), y: 9379751620 },
			  { x: new Date(1991, 0, 1), y: 11185055410 },
			  { x: new Date(1992, 0, 1), y: 10705343270 },
			  { x: new Date(1993, 0, 1), y: 13764161445.9 },
			  { x: new Date(1994, 0, 1), y: 14470193647.6 },
			  { x: new Date(1995, 0, 1), y: 17087721440.6 },
			  { x: new Date(1996, 0, 1), y: 19594314507.7 },
			  { x: new Date(1997, 0, 1), y: 21708247148.4 },
			  { x: new Date(1998, 0, 1), y: 25445271790 },
			  { x: new Date(1999, 0, 1), y: 33492125981.9 },
			  { x: new Date(2000, 0, 1), y: 30963463195.2 },
			  { x: new Date(2001, 0, 1), y: 26815924144.7 },
			  { x: new Date(2002, 0, 1), y: 22770427533.4 },
			  { x: new Date(2003, 0, 1), y: 31253989239.5 },
			  { x: new Date(2004, 0, 1), y: 36677497452.5 },
			  { x: new Date(2005, 0, 1), y: 40439926591.3 },
			  { x: new Date(2006, 0, 1), y: 49993998569.1 },
			  { x: new Date(2007, 0, 1), y: 60305010382.7 },
			  { x: new Date(2008, 0, 1), y: 32271465666.7 },
			  { x: new Date(2009, 0, 1), y: 43959427666.5 },
			  { x: new Date(2010, 0, 1), y: 50941861580.9 },
			  { x: new Date(2011, 0, 1), y: 43956921719.4 },
			  { x: new Date(2012, 0, 1), y: 50655765599.9 },
			  { x: new Date(2013, 0, 1), y: 59629932862.7 },
			  { x: new Date(2014, 0, 1), y: 62837256171.1 },
			  { x: new Date(2015, 0, 1), y: 61894377981.9 },
			  { x: new Date(2016, 0, 1), y: 64998472607.9 },
			  { x: new Date(2017, 0, 1), y: 75233321687.8 },
			  { x: new Date(2018, 0, 1), y: 68650476424.8 }
			]
		}]
	}

    
    dataPoints:Array<any>= [
        { x: new Date(1980, 0, 1), y: 2500582120 },
        { x: new Date(1981, 0, 1), y: 2318922620 },
        { x: new Date(1982, 0, 1), y: 2682595570 },
        { x: new Date(1983, 0, 1), y: 3319952630 },
        { x: new Date(1984, 0, 1), y: 3220180980 },
        { x: new Date(1985, 0, 1), y: 4627024630 },
        { x: new Date(1986, 0, 1), y: 6317198860 },
        { x: new Date(1987, 0, 1), y: 7653429640 },
        { x: new Date(1988, 0, 1), y: 9314027340 },
        { x: new Date(1989, 0, 1), y: 11377814830 },
        { x: new Date(1990, 0, 1), y: 9379751620 },
        { x: new Date(1991, 0, 1), y: 11185055410 },
        { x: new Date(1992, 0, 1), y: 10705343270 },
        { x: new Date(1993, 0, 1), y: 13764161445.9 },
        { x: new Date(1994, 0, 1), y: 14470193647.6 },
        { x: new Date(1995, 0, 1), y: 17087721440.6 },
        { x: new Date(1996, 0, 1), y: 19594314507.7 },
        { x: new Date(1997, 0, 1), y: 21708247148.4 },
        { x: new Date(1998, 0, 1), y: 25445271790 },
        { x: new Date(1999, 0, 1), y: 33492125981.9 },
        { x: new Date(2000, 0, 1), y: 30963463195.2 },
        { x: new Date(2001, 0, 1), y: 26815924144.7 },
        { x: new Date(2002, 0, 1), y: 22770427533.4 },
        { x: new Date(2003, 0, 1), y: 31253989239.5 },
        { x: new Date(2004, 0, 1), y: 36677497452.5 },
        { x: new Date(2005, 0, 1), y: 40439926591.3 },
        { x: new Date(2006, 0, 1), y: 49993998569.1 },
        { x: new Date(2007, 0, 1), y: 60305010382.7 },
        { x: new Date(2008, 0, 1), y: 32271465666.7 },
        { x: new Date(2009, 0, 1), y: 43959427666.5 },
        { x: new Date(2010, 0, 1), y: 50941861580.9 },
        { x: new Date(2011, 0, 1), y: 43956921719.4 },
        { x: new Date(2012, 0, 1), y: 50655765599.9 },
        { x: new Date(2013, 0, 1), y: 59629932862.7 },
        { x: new Date(2014, 0, 1), y: 62837256171.1 },
        { x: new Date(2015, 0, 1), y: 61894377981.9 },
        { x: new Date(2016, 0, 1), y: 64998472607.9 },
        { x: new Date(2017, 0, 1), y: 75233321687.8 },
        { x: new Date(2018, 0, 1), y: 68650476424.8 }
      ];
    selectedProgram:any;
    budgetGrpahData:Array<any>=[];  
    startDate:any;
    endDate:any;
    constructor( private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cdr: ChangeDetectorRef
      ) { }

    ngOnInit(): void {
        this.user = this.sharedS.getPicked();
        Chart.register(...registerables);
        this.selectedProgram='HCP-L4-ABBOTS MORGANICA 17102011';
      //  this.search(this.user);
       // this.buildForm();
      
        this.ViewGrpah.subscribe(res=>{
            this.openModel=res.open;
            this.selectedProgram=res.program;
            this.title= this.user.code + ' (' + res.program + ')';
            this.loadChart();
           
        });

          
    }
    
    ngAfterViewInit(){
       
    }
    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
        if (this.myChart) {
            this.myChart.destroy();
    }
}
    
    
    createChart(){
  
        if (this.myChart) {
            this.myChart.destroy();
        }

        const ctx = document.getElementById('chartContainer') as HTMLCanvasElement;

        let d:any = this.budgetGrpahData[0];

        let labels:Array<any> = [];
        let funds :Array<any> = [d.f1,d.f2, d.f3, d.f4, d.f5, d.f6, d.f7, d.f8, d.f9, d.f10,d.f11,d.f12, d.f13, d.f14, d.f15, d.f16, d.f17, d.f18, d.f19, d.f20,d.f21,d.f22, d.f23, d.f24, d.f25, d.f26];
        let funds_ou :Array<any> = [d.f1ou,d.f2ou, d.f3ou, d.f4ou, d.f5ou, d.f6ou, d.f7ou, d.f8ou, d.f9ou, d.f10ou,d.f11ou,d.f12ou, d.f13ou, d.f14ou, d.f15ou, d.f16ou, d.f17ou, d.f18ou, d.f19ou, d.f20ou,d.f21ou,d.f22ou, d.f23ou, d.f24ou, d.f25ou, d.f26ou];
        let acb :Array<any> = [d.acB1, d.acB2, d.acB3, d.acB4, d.acB5,d.acB6, d.acB7, d.acB8,d.acB9,d.acB10,d.acB11, d.acB12, d.acB13, d.acB14, d.acB15,d.acB16, d.acB17, d.acB18,d.acB19,d.acB20,d.acB21, d.acB22, d.acB23, d.acB24, d.acB25,d.acB26];

         
        let today= new Date();

        let date = new Date ( today.getFullYear()-1 + '/06/30')
        for (let i = 0; i < 26; i++) {
            date.setDate(date.getDate() + 14);
            labels.push(date.toLocaleDateString());
        }
        let datasets:Array<any> = [
           // {label : 'Fund Allotted', data : funds, backgroundColor: 'blue', fill: false,  markerType: 'triangle', borderColor: 'rgba(75,192,192,1)', tension: 0.1},
           // {label : 'Funds Consumed', data : funds_ou, backgroundColor: 'limegreen', fill: false,  markerType: 'triangle', borderColor: 'rgba(75,192,192,1)', tension: 0.1},
            {label : 'Funds over', data : acb, backgroundColor: 'red', fill: false,  markerType: "triangle", borderColor: 'rgba(75,192,192,1)', tension: 0.1},
        ];
            
    
        this.funds=funds;
        this.labels=labels;
        this.funds_ou=funds_ou;
        this.acb=acb;

        this.myChart = new Chart(ctx, {
          type: 'line', //this denotes tha type of chart
          
          data: {
            // values on X-Axis
            labels: labels, 
            datasets: datasets,
           
           
          },
          options: {
               aspectRatio:1.5,
              
                scales: {
                  y: {
                    beginAtZero: true
                  }
                }
              }
          
        });
        this.cdr.detectChanges();
        console.log(this.myChart.data)

      }

      createChart_old(){
  
        if (this.myChart) {
            this.myChart.destroy();
        }

        const ctx = document.getElementById('chartContainer') as HTMLCanvasElement;

        this.myChart = new Chart(ctx, {
          type: 'line', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: ['2022-05-10', '2022-05-11', '2022-05-12','2022-05-13',
                                     '2022-05-14', '2022-05-15', '2022-05-16','2022-05-17', ], 
               datasets: [
              {
                label: "Sales",
                data: ['467','576', '572', '79', '92',
                                     '574', '573', '576'],
                backgroundColor: 'blue'
              },
              {
                label: "Profit",
                data: ['542', '542', '536', '327', '17',
                                         '0.00', '538', '541'],
                backgroundColor: 'limegreen'
              }  
            ]
          },
          options: {
            aspectRatio:2.5
          }
          
        });
      }
      loadChart(){
       
        this.timeS.getRecipientBudgetGrpah(this.user.id, this.selectedProgram).subscribe(data => {
            this.budgetGrpahData = data;
           
            this.createChart();
           
  
          });
        
      }

  }