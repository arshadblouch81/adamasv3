import { Component, forwardRef } from '@angular/core'

import { ClientService } from '@services/index'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'
import { RemoveFirstLast } from '@pipes/pipes';

@Component({
    selector: 'recipient-details',
    templateUrl: './recipient-details.html',
    styles:[`
        .flex-box{
            display: flex;
            font-size:12px;
            max-width: 590px;
            margin: 0.7rem 0 0 0;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
        }
        .flex-box .icon{
            display: flex;
            background: #597b98;
            color: #fff;
            padding: 5px 0;
            vertical-align: middle;
            flex-direction: column;
            flex: 0 0 15%;
            text-align: center;
            justify-content: center;
        }
        .flex-box .icon i{
            font-size:1rem;
        }
        .flex-box .content{
            display: flex;
            flex: 0 0 85%;
        }
        ul {
            list-style:none;
            width: 100%;
            line-height: 1.5;
        }
        ul li{            
            border-bottom: 1px solid #cacaca;
            position:relative;
        }
        ul li:last-child{
            border: none;
        }
        ul li > div{
            padding: 5px 10px;
        }
        .primary{
            position: absolute;
            top: 20px;
            right: 0;
        }
        .type span{
            padding: 0px 4px;
            border-radius: 3px;
            color: #fff;
            background: linear-gradient(rgb(178, 204, 226), rgb(57, 91, 119));
            font-weight: 600;
        }
        .note-area{
            position:relative;            
            width:100%;
        }
        .note-area textarea{
            border:0;
            outline:none;
            box-shadow: none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
            height: 100%;
            min-height: 5rem;
            overflow-y: auto;
        }
        .note-area ul{
            overflow-y: auto;
            margin-top:1rem;
        }
        .note-area ul li{
            padding-left:1rem;
        }
        .note-area label{
            position: absolute;
            top: -14px;
            left: 3px;        
            padding: 0 10px;
            background: #fff;
            z-index:1;
        }
        `
    ],
     providers: [
         {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RecipientDetailsComponent),
            multi: true
         }
     ]
})

export class RecipientDetailsComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    show: boolean = true;
    user: any

    results: any = {
       addresses: [],
       contacts: [],
       alerts: '',
       serviceTaskList: []
    }

    constructor(
        private clientS: ClientService
    ){ }

    writeValue(value: any){
        if(value != null){
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
       
        this.clientS.getaddress(user.code)
            .subscribe(data => {
                this.results.addresses = data.map(x =>  {
                    return {
                        address1: x.address1,
                        address2: x.address2,
                        description: new RemoveFirstLast().transform(x.description).trim(),
                        postCode: x.postCode,
                        suburb: x.suburb,
                        primaryAddress: x.primaryAddress
                    }
                })
            })

        this.clientS.getcontacts(user.code)
            .subscribe(data => {
                this.results.contacts = data.map(x => {
                    return {
                        detail: x.detail,
                        primaryPhone: x.primaryPhone,
                        type: new RemoveFirstLast().transform(x.type).trim(),
                    }
                })
            })
        this.clientS.getservicetasklist(user.code)
            .subscribe(data => this.results.serviceTaskList = data)

        this.clientS.getalerts(user.code)
            .subscribe(data => this.results.alerts = data)
    }
}