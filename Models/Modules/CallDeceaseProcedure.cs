
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class CallDeceaseProcedure {
        public List<ProcedureRoster> Roster { get; set; }
        public ProcedureClientStaffNote Note { get; set; }
        public string DateOfDeath { get; set; }
    }
}
