

import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, othersType, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { functionalStatus, oni} from '@services/global.service'

@Component({   
    templateUrl:'./physicalhealth.html',
    styleUrls: ['physicalhealth.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class PhysicalHealth implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

    generalCondition:any= oni.generalCondition;
    level:any= oni.level;
    level2:any= oni.level2;
    confusion:any = functionalStatus.confusion
   
    leakageFrequency:any = oni.leakageFrequency
    LeakageQuantity:any = oni.LeakageQuantity
    faecalFrequency:any = oni.faecalFrequency
    regularity:any = oni.regularity
    reliability:any = oni.reliability
    willingness:any = oni.willingness

    carerAvailabilityList: any;
    carerResidencyList: any;
    ClientCarerNeeded :any;
    SustainabilityList:any;
    recordNumber: number = 0;

    physicalHealthForm: FormGroup;
 
    dateFormat: string = dateFormat;

    occupationDefault: string;
    financialClassDefault: string;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'physical-health')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        this.user = this.sharedS.getPicked();
        this.buildForm();        
       // this.populate();
       

       // let sql=`SELECT Description FROM DataDomains WHERE DOMAIN = 'FINANCIALCLASS'`;

        // this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //    this.lstFinancialClass = data.map(x => x.description);
        // });
       
        this.sharedS.emitSaveAll$.subscribe(data => {
         
            if (data.type=='Recipient' && data.tab =='Clinical Detail' && this.globalS.isCurrentRoute(this.router, 'physical-health')){
            
              this.save();
            }
          })

        this.getCarerDataLists();
    
    }
  getCarerDataLists(){
            return forkJoin([            
                this.listS.GetCarerDataAvailability(),
                this.listS.GetCarerDataResidency(),
                this.listS.GetCarerSustainability(),
            ]).subscribe(x => {
               
                this.carerAvailabilityList  = x[0];
                this.carerResidencyList     = x[1];
                this.SustainabilityList     = x[2];
                this.search(this.user);
            });
        }
    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    search(data: any){
        this.timeS.getOniPhysicalhealth(data.id)
            .subscribe(data => {
                this.recordNumber = data.recordNumber;
                this.physicalHealthForm.patchValue(data)               
                this.detectChanges();
            });
    }
    //  c_General_NeedForCarer,CarerAvailability, c_Issues_Sustainability, CarerResidency, CarerRelationship, CarerAvailability, DatasetCarer, , 
    //c_Support_Help, c_Support_Allowance, c_Support_Information, c_Support_NeedTraining, c_Threats_Emotional, c_Threats_Physical, 
    //c_Threats_Physical_Slow, c_Threats_Unrelated, c_Threats_ConsumerNeeds, c_Threats_ConsumerOther, c_Issues_Comments 
    


    buildForm(){
        this.physicalHealthForm = this.formBuilder.group({
           recordNumber: 0,
           hC_Overall_NotApplicable: false, 
           hC_Overall_General:'', 
           hC_Overall_Pain:'', 
           hC_Overall_Interfere:'', 
           hC_Vision_Reading:'',
           hC_Vision_Long:'', 
           hC_Hearing:'', 
           hC_Oral_NotApplicable: false, 
           hC_Oral:'', 
           hC_OralComments:'',
           hC_Speech_NotApplicable: false,  
           hC_Speech:'',  
           hC_SpeechComments:'',  
           hC_Falls_NotApplicable: false,  
           hC_Falls:'',  
           hC_FallsNumber:'',  
           hC_FallsComments:'',  
           hC_Feet_NotApplicable: false,  
           hC_Feet:'',  
           hC_FeetComments:'',
           hC_Vaccinations_NotApplicable: false,  
           hC_Vac_Influenza:'',  
           hC_Vac_Influenza_Date:'',  
           hC_Vac_Pneumo:'',  
           hC_Vac_Pneumo_Date:'',  
           hC_Vac_Tetanus:'',  
           hC_Vac_Tetanus_Date:'',  
           hC_Vac_Other:'',  
           hC_Vac_Other_Date:'',  
           hC_Driving_NotApplicable: false,  
           hC_Driving:'',  
           hC_FitToDrive:'',  
           hC_DrivingComments:'',  
           hC_Continence_NotApplicable: false,  
           hC_Continence_Urine:'',  
           hC_Continence_Urine_Frequency:'',  
           hC_Continence_Urine_Score:0,  
           hC_Continence_Faecal:'',  
           hC_ContinenceComments:'',
           hC_HeightWeightApplicable:'',  
           hC_Weight:'',  
           hC_Height:'',  
           hC_BMI:'',  
           hC_BPPulseApplicable:'',  
           hC_BP_Systolic:'', 
           hC_BP_Diastolic:'',  
           hC_PulseRate: 0,  
           hC_Pulse:'',  
           hC_PHCheck:'',
           hC_Med_TakeOwn:'',  
           hC_Med_TakeOwn_Score:0,  
           hC_Med_Willing:'',  
           hC_Med_Willing_Score:0,  
           hC_Med_Coop:'',  
           hC_Med_Coop_Score:0,  
           hC_Med_Webster:'',  
           hC_Med_Review:'',  
           hC_MedComments  : ''  

        });
    }

    canDeactivate() {
        if (this.physicalHealthForm && this.physicalHealthForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Changes have been detected. Save Changes?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                    
                }
            });
        }
        return true;
    }

  



    save(){

        if (this.recordNumber == 0){ return; }

        this.physicalHealthForm.patchValue({recordNumber:this.recordNumber})

        this.timeS.updateOniPhysicalhealth(this.physicalHealthForm.value, this.user.id)
            .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
    }

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }


}