namespace Adamas.Models.Modules{
    public class PrimaryAddress {
        public string RecordNo { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Description { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
    }
}