using System;

namespace Adamas.Models.Modules
{
    public class BatchGroup
    {
        public string Group { get; set; }
        public string OperatorID { get; set; }
        public DateTime? BatchDate { get; set; }
        public string BatchNumber { get; set; }
        public string BatchDetail { get; set; }
        public string BatchType { get; set; }
        public Nullable<DateTime> Date1 { get; set; }
        public Nullable<DateTime> Date2 { get; set; }
    }
}