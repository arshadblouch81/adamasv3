namespace Adamas.Models.Modules{
    public class InputFilter{
        public string User { get; set; }
        public string SearchString { get; set; }
        public bool IncludeActive { get; set; }
        public string Status { get; set; }
    }
}