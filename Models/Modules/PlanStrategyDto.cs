﻿namespace AdamasV3.Models.Modules
{
    public class PlanStrategyDto
    {
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string Detail { get; set; }
        public string Outcome { get; set; }
        public string StrategyId { get; set; }        
    }
}
