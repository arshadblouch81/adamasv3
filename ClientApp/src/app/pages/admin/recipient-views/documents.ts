import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, ViewChild } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, UploadService,PrintService, DocusignService, ClientService, EmailService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DomSanitizer } from '@angular/platform-browser';
import { NzMessageService } from 'ng-zorro-antd/message';

import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

import { ContextMenuComponent } from 'ngx-contextmenu';
import { da } from 'date-fns/locale';
import format from 'date-fns/format';
import { EmailMessage,EmailAddress, MimeMessage, CoordinatorEmail } from '@modules/modules';
import { Address } from 'cluster';


interface NewDocument{
    file: string,
    newFile: string,
    path: string
}

const FILTERS: Array<string> = [
    'CARE DOMAIN',
    'CREATOR',
    'DISCIPLINE',
    'DOCUMENT CATEGORY',
    'FILE CLASSIFICATION',
    'PROGRAMS'
 ]


@Component({
    styles: [`
        nz-table{
            margin-top:20px;
        }
        button{
            margin-right:2px;
        }
        .limit-width{
            width: 5rem;
            overflow: hidden;
        }
        .exist{
            color:green;
        }
        .not-exist{
            color:red;
        }
        ul{
            list-style:none;
        }
        ul li.active{
            background:#e6f1ff;
        }
        ul li:not(.active):hover{
            background:#e6f1ff;
        }
        .spinner{
            margin:1rem auto;
            width:1px;
        }
        .selected{
        background-color: #85B9D5;
        color: white;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
}
.example-container{
    width: 100%;
    height: 5vh;
}

td{
    white-space: nowrap;
    text-overflow: ellipsis;
  
   
}
    `],
    templateUrl: './documents.html',
    // changeDetection: ChangeDetectionStrategy.OnPush
})


export class RecipientDocumentsAdmin implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;

    private unsubscribe: Subject<void> = new Subject();
    public templates$: Array<any>;
    public originalList$: Array<any>;
    user: any;
    showDocRename:boolean = false;
    inputForm: FormGroup;
    tableData: Array<any>;

    loading: boolean = false;
    addDocumentModal: boolean = false;
    addOrEdit:boolean = false;
    current: number = 0;

    postLoading: boolean = false;
    selectedIndex: number;

    fileObject: NewDocument;
    showUpload: boolean = false;

    filters: any;
    FILTERS = FILTERS;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    RptDisplay: any;
    RecipientData:any;
    selectedRow:number;
    selectedItem:any;
    showDocProperties:boolean = false;
    dateFormat: string = 'dd/MM/yyyy';
    categories:Array<any> = [];
    classifications:Array<any> = [];
    caredomains:Array<any> = [];
    programs:Array<any> = [];
    disciplines:Array<any> = [];
    topBelow = { top: '40px' }
   
    recipientCorrdinator:any;
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private message: NzMessageService,
        private uploadS: UploadService,
        private printS:PrintService,
        private sanitizer: DomSanitizer,
        private docusign :DocusignService,
        private clientS: ClientService,
        private emailS:EmailService
       
    ) {
        // cd.reattach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'documents')) {
                this.user = data;
                this.RecipientData= this.sharedS.getRecipientData();
                this.search(data);
               
            }
        });
    }

    ngAfterViewInit(){
        this.showUpload = true;
    }
  
    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.user = this.sharedS.getPicked();
     
       // console.log(this.user)
        if(this.user){
            this.search(this.user);
            this.buildForm();
            
        }  
        
        if (this.RecipientData==null)
            this.RecipientData= this.sharedS.getRecipientData();

        let email:CoordinatorEmail={
            IsRecipient:true,
            AccountName : this.user.code

        }
      
        this.timeS.getcoordinatoremail(email).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.recipientCorrdinator=data;
        })
      //  this.listDropDowns();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            docID:0,
            title:'',            
            discipline :'',
            program :'',
            caredomain:'',
            classification:'',
            category:'',
            reminderDate: this.currentDate(),
            reminderText:'',
            notes:'' ,
            publishToApp:false           
        });
    }

    currentDate(){
        return new Date();
    }

    OnItemSelected(data:any, i:number){
        this.selectedRow=i;
        this.selectedItem=data;
    }
    OpenSelectedItem(data:any, i:number){
        this.selectedRow=i;
        this.selectedItem=data;

        this.downloadFile(i);
            
    }
    search(user: any = this.user, filters: any = null) {

        this.cd.reattach();

        this.loading = true;
        this.timeS.getdocumentsrecipients(user.id, filters).subscribe(data => {
            console.log(data)
            this.tableData = data;
            this.originalTableData = data;


            this.loading = false;
            this.cd.detectChanges();
        });

       

    }

    selectDocument(doc: any, index: number){
        this.selectedIndex = index;
        // if(doc.exists){
            this.fileObject = {
                file: doc.name,
                newFile: `${doc.name}`,
                path: doc.template
            };

            this.inputForm.reset();
            this.inputForm.patchValue({
                title: doc.name
            });
           
            this.addDocumentModal=false;
            
            this.showDocProperties=true;
            return;
        // }
        //this.globalS.eToast('Error','File not exists')            
    }

    trackByFn(index, item) {
        return item.id;
    }
    close(index: number){
        this.router.navigate(['/admin/recipient/personal'])
    }
    showAddModal() {
        this.addDocumentModal = true;
        this.addOrEdit=false;
        this.current = 0;
        this.selectedIndex = null;
        this.loading = true;
        this.uploadS.getdocumenttemplate().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.templates$  = data;
            this.originalList$ = data;
            this.loading = false;
            this.cd.detectChanges();
        });
      
        

        this.listDropDowns();
    }

    reload(reload: boolean){
        if(reload){
            this.search();
        }
    }

    showEditModal(data: any) {

        this.inputForm.reset();

        this.addOrEdit=true;
       
      this.inputForm.patchValue( {
            docID: data.docID,
            title:data.title,            
            discipline : data.discipline,
            program : data.program,
            caredomain:data.careDomain || 'VARIOUS',
            classification: data.classification,
            category: data.category,
            reminderDate: data.alarmDate,
            reminderText: data.alarmText,
            notes: data.notes  ,
            publishToApp:data.publishToApp         
        });
        this.cd.detectChanges();
        this.showDocProperties=true;
    }

    downloadFile(index: any){
        var doc = this.tableData[index];
        var notifId = this.globalS.loadingMessage('Downloading Document');
        
        this.uploadS.download({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {

            let data = window.URL.createObjectURL(blob);      
            let link = document.createElement('a');
            link.href = data;
            link.download = doc.filename;
            link.click();

            setTimeout(() =>
            {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
                this.message.remove(notifId);

                this.globalS.sToast('Success','Download Complete');
            }, 100);

        }, err =>{
            this.message.remove(notifId);
            this.globalS.eToast('Error', "Document can't be found");
        })
    }

    reportTab: any;
    viewFile(index: any){
        var doc = this.tableData[index];

        this.reportTab = window.open("", "_blank");

        this.uploadS.getdocumentblob({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).subscribe(data => {
          this.openDocumentTab(data);
        }, (error: any) => {
            this.reportTab.close();
            this.globalS.eToast('Info','The file you are trying to open no longer exists in the orignal location')
        })
    }

    openDocumentTab(data: any)
    {
        // this.fileDocumentName = data.fileName;
        this.reportTab.location.href = `${data.path}`;   
        this.reportTab.focus();
    }

    handleCancel(){
        this.addDocumentModal = false;
        this.postLoading = false;
        this.showDocProperties=false;
        this.showDocRename=false;
    }
    
    delete(index: any) {
        var data = this.tableData[index];
        this.uploadS.delete(this.user.id,{ 
            id: data.docID,
            filename: data.title
        }).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
            if(data){
                this.search();
                this.globalS.sToast('Success','File deleted')
                return;
            }
        })
    }

    save(){

        if (this.addOrEdit==true){
            this.update_document();
            return;
        }
        let  input = this.inputForm.value;

        var inp = {
           
            PersonID: this.user.id,
            OriginalFileName: this.fileObject.file,
            NewFileName: this.fileObject.newFile,
            Path:  this.fileObject.path,
            Title: input.title,
            Program: input.program,
            Discipline:  input.discipline,
            CareDomain:  input.caredomain,
            Classification:  input.classification,
            Category:  input.category,
            ReminderDate:  format(new Date(input.reminderDate),'yyyy/MM/dd'), 
            ReminderText:  input.reminderText,
            PublishToApp:  input.publishToApp,
            User:  this.tocken.user,
            Notes:  input.notes
        };
        this.postLoading = true;
        this.uploadS.postdocumentstafftemplate(inp).subscribe(data => {
            this.globalS.sToast('Success','Document has been added');
            this.handleCancel();
            this.search();
        }, (err) =>{
            this.postLoading = false;
            this.globalS.eToast('Error', err.error.message);
        });
    }

    update_document(){
       
      

       let  input = this.inputForm.value;
        var inp = {
           
            PersonID: this.user.id,
            DocID: input.docID,
            Title: input.title,
            Program: input.program,
            Discipline:  input.discipline,
            CareDomain:  input.caredomain,
            Classification:  input.classification,
            Category:  input.category,
            ReminderDate:  format(new Date(input.reminderDate),'yyyy/MM/dd'), 
            ReminderText:  input.reminderText,
            PublishToApp:  input.publishToApp,
            User:  this.tocken.user,
            Notes:  input.notes

        };

        this.timeS.updateDocument(inp).subscribe(data => {
            this.globalS.sToast('Success','Document has been updated');
            this.handleCancel();
            this.search();
        }, (err) =>{
            this.postLoading = false;
            this.globalS.eToast('Error', err.error.message);
        });
    

    }
    rename_document(){
       

        let  input = this.inputForm.value;
        let sql = `UPDATE Documents SET Title = '${input.title}' WHERE DOC_ID = ${input.docID}`;
 
         this.listS.executeSql(sql).subscribe(data => {
             this.globalS.sToast('Success','Document has been renamed');
             this.handleCancel();
             this.search();
         }, (err) =>{
             this.postLoading = false;
             this.globalS.eToast('Error', err.error.message);
         });
     
 
     }
    pre(): void {
        this.current -= 1;
    }

    next(): void {
        this.current += 1;
    }

    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Title',
        value: 'title'
    },{
        key: 'Created',
        value: 'created'
    },{
        key: 'Status',
        value: 'status'
    },{
        key: 'Care Domain',
        value: 'careDomain'
    }
    ,{
        key: 'Discipline',
        value: 'discipline'
    }
    ,{
        key: 'Program',
        value: 'program'
    },{
        key: 'Classification',
        value: 'classification'
    },{
        key: 'Category',
        value: 'category'
    },{
        key: 'Created',
        value: 'created'
    },{
        key: 'Author',
        value: 'author'
    },{
        key: 'Modified',
        value: 'modified'
    },{
        key: 'Filename',
        value: 'filename'
    },{
        key:'Type',
        value:'type'
    },{
        key:'Original Location',
        value:'originalLocation'
    }];
    
    
    

    dragDestination = [
        'Title',
        'Created',
        'Status',
        'Care Domain',
        'Discipline',
        'Program',
        'Classification',
        'Category',
        'Created',
        'Author',
        'Modified',
        'Filename',
        'Type',
        'Original Location',
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);
        console.log(flatten)
        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }

    filterChange(filters: any){
        this.RptDisplay = filters.display;
        this.search(this.user, filters); 
    }
    
    showAcceptModal(data: any, index:number) {
        console.log(data)
        if (index==1){
            this.listDropDowns();
            this.showEditModal(data);
        }
        else if (index==2){
            this.inputForm.patchValue( {
                    title : data.title,
                    docID: data.docID,
                });
            this.showDocRename=true;
        }
        else if (index==3){
            this.sendDocusignEnvelope(index);
        }
        else if (index==4){
            this.sendEmail(data);
        }
    }

    sendEmail(doc:any){

      
     
        var notifId = this.globalS.loadingMessage('Attaching Document with Email');
        let base64String :any;
        let contentType:any;
        this.uploadS.download({
            PersonID: this.user.id,
            Extension: doc.type,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {

            this.convertBlobToBase64(blob).then((base64) => {
                base64String= base64;
                contentType=base64String.substring(0,base64String.indexOf(";") );
                base64String = base64String.substring(base64String.indexOf(",") + 1);
        
              this.globalS.clearAMessage(notifId);

       let  FromAddress : EmailAddress = 
            {
                Name: "Arshad Adamas",
                Address: "jobs@adamas.net.au"
            };
        

    let  ToAddress : Array<EmailAddress> = [
            {
            Name: this.RecipientData.recipient_Coordinator,
            Address: this.recipientCorrdinator.coordEmail
            }
        ];

        let  CcAddress : Array<EmailAddress>= [
            {
            Name: "Arshad Adamas",
            Address: "arshad@adamas.net.au"
            }
         ];

        let message : EmailMessage={
            FromAddress : FromAddress,
            ToAddress : ToAddress,   
            CCAddress  : CcAddress,       
            Subject: "Adamas Email",
            Body: "<p>This is a test email to send Document from Traccs</p>",
            Attachments: [
                {
                    "fileName": doc.filename + doc.type,
                    "contentBase64": base64String,
                    "contentType": contentType
                }]
            
        };
       
         this.globalS.loadingMessage('Sending Email');
         console.log (JSON.stringify(message));
                    
            this.emailS.sendEMailWithAttachments(message).pipe(takeUntil(this.unsubscribe)).subscribe({
                next: (value) => {
                    // Handle next value if needed
                },
                complete: () => {
                    this.globalS.sToast('Success', 'Email sent');
                    this.globalS.clearMessage();
                },
                error: (err) => {
                    this.globalS.eToast('Error', err.error.message);
                    this.globalS.clearMessage();
                }
            });

        }).catch((error) => {
            console.error('Error:', error);
            this.globalS.clearMessage();
          });
            
        error: (err) => {
            this.globalS.eToast('Error', err.error.message);
            this.globalS.clearMessage();
        }
    });

    }

    convertBlobToBase64(blob: Blob): Promise<string> {
        return new Promise((resolve, reject) => {
          const reader = new FileReader();
      
          // Listen for the load event to read the blob as a Base64 string
          reader.onloadend = () => {
            if (reader.result) {
              resolve(reader.result.toString());
            } else {
              reject('Failed to convert Blob to Base64');
            }
          };
      
          // Listen for any errors
          reader.onerror = (error) => reject(error);
      
          // Read the blob as a data URL
          reader.readAsDataURL(blob);
        });
      }

      
    refresh(){
        this.search(this.user); 
    }
    handleOkTop(){
        
    }
//Mufeed 9 April 2024
handleCancelTop(){
    this.drawerVisible = false;
    this.pdfTitle = "";
    this.tryDoctype="";
  }
  
  InitiatePrint(){

    var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
    
    
    fQuery = " SELECT TOP 20  DOC_ID,Title AS [Document Name], [Created], IsNull([Status], '') AS [St], IsNull(CareDomain, 'VARIOUS') AS [Care Domain], IsNull(Discipline, 'VARIOUS') AS [Discipline], IsNull(Program, 'VARIOUS') AS [Program], IsNull([Classification], '') AS [Class.], IsNull([Category], '') AS [Cat.], [Modified], Filename, dCreated FROM (SELECT DOC_ID, DOCUMENTGROUP, CONVERT(Varchar(10),D.Created, 103) + ' ' + (SELECT [Name] FROM UserInfo WHERE Recnum = D.Author) AS Created, CONVERT(Varchar(10),D.Modified, 103) + ' ' + (SELECT [Name] FROM UserInfo WHERE Recnum = D.Typist) AS Modified, D.Created AS dCreated, D.Doc# AS CareplanID, (SELECT [Name] FROM HumanResourceTypes WHERE RecordNumber = D.Department AND [Group] = 'PROGRAMS') AS Program, (SELECT [Description] FROM DataDomains WHERE RecordNumber = D.DPID) AS Discipline, (SELECT [Description] FROM DataDomains WHERE RecordNumber = D.CareDomain) AS CareDomain, D.Title, Filename, Classification, Category, D.Status, D.DocStartDate, D.DocEndDate, D.AlarmDate, D.AlarmText FROM Documents D WHERE PersonID = '"+this.user.id+"' AND DOCUMENTGROUP = 'DOCUMENT' AND (DeletedRecord = 0 OR DeletedRecord Is NULL)) D1  ORDER BY [dCreated] DESC ";
    //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
    txtTitle = "Documents ";       
    Rptid =   "8WTerq14eNFSAnz0"  

    //console.log(fQuery)
      const data = {
                  
          "template": { "_id": Rptid },                                
          "options": {
              "reports": { "save": false },                
              "sql": fQuery,
              "Criteria": lblcriteria,
              "userid": this.tocken.user,
              "txtTitle": txtTitle,
              "Extra": this.user.code,
                                                              
          }
      }
      this.loading = true;
      this.drawerVisible = true;         
          this.printS.printControl(data).subscribe((blob: any) => {
          this.pdfTitle = txtTitle+".pdf"
              this.drawerVisible = true;                   
              let _blob: Blob = blob;
              let fileURL = URL.createObjectURL(_blob);
              this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
              this.loading = false;
              this.cd.detectChanges();
          }, err => {
              console.log(err);
              this.loading = false;
              this.modalService.error({
                  nzTitle: 'TRACCS',
                  nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                  nzOnOk: () => {
                      this.drawerVisible = false;
                  },
              });
          });
          return;      
  }

  readFile(filePath: string) {
   // const fs = (); // require('fs');
//     const fs = require('filestream/read');
//     fs.readFile(filePath, (err, data) => {
//      if (err) {
//        return ({ message: 'File not found' });
//      }
//      return ({ base64: data.toString('base64') });
//    });

   
}
  sendDocusignEnvelope(index:any) {
    var doc = this.tableData[index];
    var notifId = this.globalS.loadingMessage('Sending Document for Docusign');

    let filename = doc.filename;
    let docID = doc.docID;
    
    this.uploadS.download({
        PersonID: this.user.id,
        Extension: doc.type,
        FileName: doc.filename ,
        DocPath: doc.originalLocation
    }).pipe(takeUntil(this.unsubscribe)).subscribe(blob => {
    })



        this.clientS.getcontacts(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(contacts => {

            let primaryContact = contacts.find((x: any) => x.primaryPhone == 1);
            if (primaryContact==null)
                primaryContact = contacts[0];

    const envelopeData = {
      emailSubject: 'Please sign this document',
      emailBlurb: 'Here is the document you need to sign.',
      status: 'sent',
      
        signers: [
          {
            email: this.recipientCorrdinator.coordEmail,
            name: this.user.code,                  
            company : 'Farnorth',
            title : 'MS',
            recipientId: '1',
            routingOrder: '1',
            tabs: {
              signHereTabs: [
                {
                  xPosition: '100',
                  yPosition: '150',
                  documentId: '1',
                  pageNumber: '1'
                }
              ]
            }
          }
        ]
      ,
      documents: [
        {
          documentId: docID,
          name: doc.title,
          documentBase64: filename,
          fileExtension: doc.type,
          remoteUrl: doc.originalLocation 
        }
      ],
      startingView : 'tagging',
      DOC_ID: docID,
      ccRecipients: [ {
        "email":  this.recipientCorrdinator.coordEmail,
         "Address": this.recipientCorrdinator.coordEmail,
        "name": this.RecipientData.recipient_Coordinator,
        "recipientId": "2",
        "routingOrder": "2"
      }, {
        "email": "arshad@adamas.net.au",
        "name": "Arshad Adamas",
        "recipientId": "3",
        "routingOrder": "3"
      }]

    };
    
    this.docusign.sendDocument(envelopeData).pipe(takeUntil(this.unsubscribe)).subscribe({

        next: (value) => {
            // Handle next value if needed
        },
        complete: () => {
            this.globalS.sToast('Success', 'Document sent');
            this.globalS.clearMessage();
        },
        error: (err) => {
            this.globalS.eToast('Error', err.error.message);
            this.globalS.clearMessage();
        }
   
        });
     
    
});


  }

  async convertToBase64(file: File) {
    let base64String :any;
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
     base64String = reader.result?.toString().split(',')[1]; // Remove the prefix if needed
      return (base64String);
    };
    reader.onerror = (error) => {
      console.log('Error: ', error);
    };

   
  }

  listDropDowns() {
            "Invalid column name 'T0100004652'."
    let sql=`select distinct [Program]  FROM RecipientPrograms WHERE RecipientPrograms.PersonID = '${this.user.id}' ORDER BY [Program]`;

    
    let sql2=` select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;
   

    let sql3=`select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;

    let sql4=` select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'FILECLASS' AND ISNULL(EndDate, '') = '' ORDER BY [Description] `;

    let sql5=`select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DOCCAT' AND ISNULL(EndDate, '') = '' ORDER BY [Description]`;
     
   
            forkJoin([
                this.listS.getlist(sql),
                this.listS.getlist(sql2),
                this.listS.getlist(sql3),
                this.listS.getlist(sql4),
                this.listS.getlist(sql5)                
            ]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.programs = data[0].map(x=>x.program);
                this.caredomains = data[1].map(x=>x.description);
                this.disciplines= data[2].map(x=>x.description);
                this.classifications= data[3].map(x=>x.description);
                this.categories= data[4].map(x=>x.description);

                this.caredomains.push('VARIOUS');
                this.cd.detectChanges();
                
            });

            setTimeout(() => {   },1000)

    
  }

  txtSearch:string;

 onTextChangeEvent(event:any, listTye:any){
    // console.log(this.txtSearch);
     let value = this.txtSearch.toUpperCase();
 
     this.templates$=this.originalList$.filter(element=>element.name.includes(value));



 }

}
