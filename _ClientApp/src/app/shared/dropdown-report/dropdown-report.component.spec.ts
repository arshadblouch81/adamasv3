import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownReportComponent } from './dropdown-report.component';

describe('DropdownReportComponent', () => {
  let component: DropdownReportComponent;
  let fixture: ComponentFixture<DropdownReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
