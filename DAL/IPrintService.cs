﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;

using Adamas.Models;
using Adamas.Models.Modules;

namespace Adamas.DAL
{
    public interface IPrintService
    {
        Task<(MemoryStream, string, string)?> PrintQuotes(string sourcePath);
        Task<(MemoryStream, string, string)?> PrintQuotes(DocumentQuoteInputDto input);
        Task<(MemoryStream, string, string)?> PrintNotes();
    }
}
