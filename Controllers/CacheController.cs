using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using Adamas.DAL;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class CacheController : Controller
    {
        private ICacheService cache;
        public CacheController(
            ICacheService _cache
        ){
            cache = _cache;
        }        

        public async Task<IActionResult> GetCacheList()
        {
            return Ok(await cache.GetListOfLoggedUsers());
        }

        [HttpGet("remove/{token}")]
        public async Task<IActionResult> RemoveToken(string token)
        {
            return Ok(cache.RemoveToken(token));
        }
    }
}
