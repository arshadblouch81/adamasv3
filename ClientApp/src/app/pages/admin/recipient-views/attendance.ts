import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, attendance, appverification,appverification2 } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
@Component({
    styles: [`
        nz-select {
            width: 100%;
            background-color: #cbe8f7 !important;
        }

     input{
        background-color: #cbe8f7;
    }

        input:focus {
            background-color: #85B9D5;
        }   

        nz-select:focus {
            background-color: #85B9D5;
        }  

        .row{
            margin:3px;
        }

        .input{
            background-color: #cbe8f7;
            border: 1px solid #cbe8f7;
        }
        .selected{
            background-color: #85B9D5;
            border: 1px solid #85B9D5;
        }
        ul{
            list-style:none;
        }

        div.divider-subs div{
            margin-top:2rem;
        }
        .layer > span:first-child{
            width:8rem;
        }
        .layer > *{
            display:inline-block;
            margin-right:5px;
            font-size:14px;
            font-family: 'segoe ui';
        }
        .layer > input{
            width: 4rem;
        }
        .compartment > div{
            margin:5px;
            padding:10px;
        }
        .layer2 > span{
            width:13rem;
        }
        .layer2 > input{
            width:4rem;
        }
        .layer2 {
            margin-bottom:5px;
        }
        .layer2 > *{
            display:inline-block;
            margin-right:5px;
            font-size:11px;
        }
        label{
            font-size:14px;
            font-family: 'segoe ui';
        }

        /* Set background color for the dropdown menu */
        :host ::ng-deep  .nz-dropdown-menu {
        background-color: #cbe8f7 !important; /* Change to your desired background color */
        }

        /* Set background color for the dropdown options */
        :host ::ng-deep .nz-option {
        background-color: #cbe8f7 !important; /* Change to your desired background color */
        }

        :host ::ng-deep  .ant-select-selection   {
            background-color: #cbe8f7 !important;
            }
    

      
    `],
    templateUrl: './attendance.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class RecipientAttendanceAdmin implements OnInit, OnDestroy {
    attendanceForm: FormGroup;

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;

    tableData: Array<any>;

    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;
    MTAMethod:any;
    lists: Array<string> = attendance;
    lstapp: Array<any> = appverification;
    managers: Array<string> = [];
    dropdownStyle = {
        'background-color': '#cbe8f7 !important',  // Background color
        'color': '#333',                 // Text color
        'border': '1px solid #ccc'       // Border
        // Add more styles as needed
    };
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private cd: ChangeDetectorRef,
        private modalService: NzModalService,
        private formBuilder: FormBuilder
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'attendance')) {
                this.search(data);
            }
        });


        this.sharedS.emitSaveAll$.subscribe(data => {
            if (data.type == 'Recipient' && data.tab == 'Roster/Mobile App')
                this.save();
        })
    }

    ngOnInit(): void {
        this.buildForm();
        this.user = this.sharedS.getPicked();
       
        this.search(this.user);
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    buildForm() {
        this.attendanceForm = this.formBuilder.group({
            mtaMethod: null,
            allowRegisterSignature: null,
            daelibsid: null,
            paN_TAEarlyFinishTHEmail: null,
            paN_TAEarlyFinishTHSMS: null,
            paN_TAEarlyFinishTHWho: null,
            paN_TALateStartTHEmail: null,
            paN_TALateStartTHSMS: null,
            paN_TALateStartTHWho: null,
            paN_TANoWorkTHEmail: null,
            paN_TANoWorkTHSMS: null,
            paN_TANoWorkTHWho: null,
            paN_TAOverstayTHEmail: null,
            paN_TAOverstayTHSMS: null,
            paN_TAOverstayTHWho: null,
            paN_TAUnderstayTHEmail: null,
            paN_TAUnderstayTHSMS: null,
            paN_TAUnderstayTHWho: null,
            panEarlyFinishTH: null,
            panLateStartTH: null,
            panNoGoTH: null,
            panNoShowTH: null,
            panNoWorkTH: null,
            panOverStayTH: null,
            panUnderStayTH: null,
            pan_Manager: null,
            pan_Status: null,
            paN_TAEarlyStartTHEmail: null,

            panearlystartth: null,
            pinCode: null,
            recipienT_PARENT_SITE: null,
            timeZoneOffset: null,
            paN_TAEarlyStartTHWho: null,

            panlatefinishth: null,
            paN_TALateFinishTHEmail: null,
            paN_TALateFinishTHWho: null,
            smsServiceReminders:null
        });
    }

    search(data: any) {
        this.timeS.getattendance(data.id)
            .subscribe(data => {
                this.attendanceForm.patchValue(data);
              
                // this.attendanceForm.patchValue({mtaMethod: {code: data.mtaMethod, value:this.globalS.getMTAMethod(data.mtaMethod)}});

                this.attendanceForm.get('mtaMethod')?.setValue(this.globalS.getMTAMethod(data.mtaMethod));
                

                this.detectChanges();
            });
            this.listS.getportalmanagers().subscribe(data => {
                this.managers = data;
                this.detectChanges();
                
            });
      
    }

    detectChanges() {
        this.cd.markForCheck();
        this.cd.detectChanges();
    }

    canDeactivate() {
        if (this.attendanceForm && this.attendanceForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Save changes before exiting?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {

                }
            });
        }

        return true;
    }

    save() {
        this.attendanceForm.patchValue({mtaMethod: this.globalS.getMTAMethodCode(this.attendanceForm.value.mtaMethod)});
      // this.attendanceForm.get('mtaMethod')?.setValue(this.globalS.getMTAMethodCode(this.attendanceForm.value.mtaMethod));

        this.timeS.updateattendance(this.attendanceForm.value, this.user.id)
            .subscribe(data => {
                this.attendanceForm.markAsPristine();
                this.globalS.sToast('Success', 'Data saved successfully')
            });
    }


    
}