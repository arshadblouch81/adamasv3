import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators, FormArray } from '@angular/forms'
import { CurrencyPipe } from '@angular/common'

import { TimeSheetService, ListService, fundingDropDowns } from '@services/index';
import { switchMap } from 'rxjs/operators';
@Component({
  selector: 'document-edit-quote',
  templateUrl: './document-edit-quote.component.html',
  styleUrls: ['./document-edit-quote.component.css']
})
export class DocumentEditQuoteComponent implements OnInit, OnChanges {

  @Input() open: boolean = false;
  @Input() program: string;
  @Input() data: boolean = false;

  quoteDetailsGroup: FormGroup;
  isAnUpdate: boolean = false;

  itemCodeList: Array<string> = []
  cycles: Array<string> = fundingDropDowns.cycle;

  constructor(
    private timeS: TimeSheetService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private cp: CurrencyPipe
  ) { 

  }

  ngOnInit() {
    this.reset();
  }

  ngOnChanges(changes: SimpleChanges){
    for(let property in changes){
        if(property == 'open' && !changes[property].firstChange 
            && changes[property].currentValue != null){
            this.open = true;
            this.populate(this.data);
        }
    }
  }

  populate(data: any){
    console.log(this.program);
    if(data && this.program){
      this.isAnUpdate = true;
      this.timeS.getquotedetails(data.RecordNumber).subscribe(detail => {
        this.quoteDetailsGroup.patchValue({
          frequency: this.filterFrequency(detail),
          rate: this.cp.transform(detail.Rate,'USD', true,'1.2-2'),
          lengthInWeeks: detail.LengthInWeeks,
          quoteQuantity: detail.QuoteQty,
          chargeType: this.sortChargeType(detail.RosterGroup),
          itemCode: detail.Title,
          displayText: detail.DisplayText,
          quantity: detail.Qty,
          budgetPercentage: detail.BudgetPerc,
          billUnit: detail.BillUnit,
          total: this.cp.transform((detail.Rate*detail.QuoteQty),'USD',true, '1.2-2')
        });

        this.mouldTimeSlots(detail.Roster, this.noOfLoops(detail.Frequency));        
      })
      return;
    } else {
      this.isAnUpdate = true;
      this.reset();
    }    
  }

  sortChargeType(type: string): number{
    const typeUpper = type.toUpperCase();

    if(typeUpper === "GROUPACTIVITY" || typeUpper === "ONEONONE" || typeUpper === "TRANSPORT")
      return 1;
    if(typeUpper === "ITEM")
      return 2;
    if(typeUpper === "ADMISSION")
      return 3;
    if(typeUpper === "GROUPACTIVITY")
      return 4;
  }

  pad(n){
    return (n < 10) ? ("0" + n) : n;
  }

  filterFrequency(detail: any): string{
    if(!detail.Roster)  return 'DAILY';
    return detail.Frequency;
  }

  clearTimeSlot(){
    (this.quoteDetailsGroup.get('timeSlots') as FormArray).clear();
  }

  mouldTimeSlots(rosters: string, loopIndex: number = 4){
    this.clearTimeSlot();
    var timeSlots = rosters ? rosters.split('||') : false;

    if(timeSlots && timeSlots.length >= 28){      
      let index = 0;
      let slot = this.quoteDetailsGroup.get('timeSlots') as FormArray;
      for(var a = 0 ; a < loopIndex; a++){
        slot.push(this.createTimeSlot(timeSlots.slice(index,index+7)));
        index+=7;
      }
    }

  }

  reset(){

    this.quoteDetailsGroup = new FormGroup({
      timeSlots: new FormArray([]),
      rosterGroup: new FormControl('NONE'),
      frequency: new FormControl('DAILY'),
      rate: new FormControl(''),
      lengthInWeeks: new FormControl(''),
      quoteQuantity: new FormControl(''),
      chargeType: new FormControl(''),
      itemCode: new FormControl(''),
      strategy: new FormControl(''),
      displayText: new FormControl(''),
      quantity: new FormControl(''),
      budgetPercentage: new FormControl(''),
      billUnit: new FormControl(''),
      total: new FormControl(''),

      timeSlotsStr: new FormControl('')
    });
    
    this.quoteDetailsGroup.get('frequency').valueChanges
    .subscribe(data => {
      this.clearTimeSlot();
      this.loopRoster(this.noOfLoops(data));
    });

    this.quoteDetailsGroup.get('chargeType').valueChanges
    .pipe(switchMap(x => {
      return this.listS.getchargetype({
        program: this.program,
        index: x
      })
    })).subscribe(data => {
      this.itemCodeList = data;
    });

  }

  noOfLoops(data): number{
    if(data == 'WEEKLY'){
      return 1;
    } else if(data == 'FORTNIGHTLY'){
      return 2;
    } else if(data == 'MONTHLY'){
      return 4;
    } else {
      return 0;
    }
  }

  loopRoster(noOfLoop: number){
    let index = 0;
    while( index < noOfLoop){
      this.addTimeSlot();
      index ++;
    }
  }


  addTimeSlot(){
    const slot = this.quoteDetailsGroup.get('timeSlots') as FormArray;
    slot.push(this.createTimeSlot());
  }

  createTimeSlot(data: Array<any> = null): FormGroup {    
    return this.formBuilder.group({
      monday: new FormGroup({
        time: new FormControl( data ? data[0].split('|')[0] : '9:00'),
        quantity: new FormControl( data ? Math.trunc(data[0].split('|')[1]) : 0)
      }),
      tuesday: new FormGroup({
        time: new FormControl(data ? data[1].split('|')[0] : '9:00'),
        quantity: new FormControl( data ? Math.trunc(data[1].split('|')[1]) : 0)
      }),
      wednesday: new FormGroup({
        time: new FormControl(data ? data[2].split('|')[0] : '9:00'),
        quantity: new FormControl(data ? Math.trunc(data[2].split('|')[1]) : 0)
      }),
      thursday: new FormGroup({
        time: new FormControl(data ? data[3].split('|')[0] : '9:00'),
        quantity: new FormControl(data ? Math.trunc(data[3].split('|')[1]) : 0)
      }),
      friday: new FormGroup({
        time: new FormControl(data ? data[4].split('|')[0] : '9:00'),
        quantity: new FormControl(data ? Math.trunc(data[4].split('|')[1]) : 0)
      }),
      saturday: new FormGroup({
        time: new FormControl(data ? data[5].split('|')[0] : '9:00'),
        quantity: new FormControl(data ? Math.trunc(data[5].split('|')[1]) : 0)
      }),
      sunday: new FormGroup({
        time: new FormControl(data ? data[6].split('|')[0] : '9:00'),
        quantity: new FormControl(data ? Math.trunc(data[6].split('|')[1]) : 0)
      })
    });
  }

  save(){
    
    //this.generateTimeSlotFormat(this.quoteDetailsGroup.get('timeSlots').value);
    this.quoteDetailsGroup.patchValue({
      timeSlotsStr: this.generateTimeSlotFormat(this.quoteDetailsGroup.get('timeSlots').value)
    })

    console.log(this.quoteDetailsGroup.value);

  }

  generateTimeSlotFormat(timeslots: Array<any>): string{
    let slotString = '';

    for(var index = 0; index < 4; index++){
      var x = timeslots[index];

      slotString = slotString + (typeof(x) !== 'undefined' ? x.monday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.monday.quantity)).toFixed(2)) : "00.00") + '||';

      slotString = slotString + (typeof(x) !== 'undefined' ? x.tuesday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.tuesday.quantity)).toFixed(2)) : "00.00") + '||';

      slotString = slotString + (typeof(x) !== 'undefined' ? x.wednesday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.wednesday.quantity)).toFixed(2)) : "00.00") + '||';

      slotString = slotString + (typeof(x) !== 'undefined' ? x.thursday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.thursday.quantity)).toFixed(2)) : "00.00") + '||';

      slotString = slotString + (typeof(x) !== 'undefined' ? x.friday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.friday.quantity)).toFixed(2)) : "00.00") + '||';

      slotString = slotString + (typeof(x) !== 'undefined' ? x.saturday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.saturday.quantity)).toFixed(2)) : "00.00") + '||';

      slotString = slotString + (typeof(x) !== 'undefined' ? x.sunday.time : "09:00") + '|';
      slotString = slotString + (typeof(x) !== 'undefined' ? this.pad(parseFloat((x.sunday.quantity)).toFixed(2)) : "00.00") + '||';
    }
    
    console.log(slotString);
    return slotString;
  }

}
