import { Component, Input, SimpleChanges } from '@angular/core'

@Component({
    selector:'intake-no-data',
    templateUrl: './intake-details-no-data.html'
})

export class IntakeNoDataComponent {
    @Input() data: number = null;
    @Input() message: string = 'No Data';
    @Input() loading: boolean;

    ngOnChanges(changes: SimpleChanges){
        for(let property in changes){
            if(property == 'data' && (changes[property].currentValue != null )){  
                return;
            } 
            if(property == 'loading' && (changes[property].currentValue != null )){  
                this.loading = changes[property].currentValue;
            } 
        }
    }
}