using System;

namespace Adamas.Models.Tables {
    public class InsertRecord
    {
        public string Operator { get; set; }
        public DateTime? actionDate { get; set; }
        public string actionStartDate { get; set; }
        public string actionEndDate { get; set; }
        public string otherDate { get; set; }
        public string actionOn { get; set; }
        public string actionCode { get; set; }
        public string actionUser { get; set; }
        public string actionDescription { get; set; }
    }
}    