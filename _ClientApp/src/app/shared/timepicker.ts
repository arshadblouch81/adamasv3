import { Component, forwardRef, OnInit, ElementRef, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import * as moment from 'moment';

const noop = () => {
};

export const TIMEPICKER_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => TimePicker),
};


@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'timepicker',
    templateUrl:'./timepicker.html',
    styles:[
        `
        .time-container{
            position:relative;
            max-width: 4.5rem;
        }
        .time-container:hover > .timer{
            visibility: visible;
        }
        input[type=text]{
            -webkit-appearance: none;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            margin: 0;
            padding: 0;
            border: none;
            border-radius: 0;
            box-shadow: none;
            background: none;
            height: 1rem;
            color: #000;
            display: inline-block;
            min-width: 2.5rem;
            width:100%;
            border-bottom: 1px solid #9a9a9a;
            padding: 0 .25rem;
        }
        p{
            margin:0;
        }
        .colon{
            display:inline-block;
        }
        .colon .title{
            float: left;
            margin: 18px 0;
        }
        p.title{
            font-size:18px;
        }
        .spin-down{
            transform: rotate(180deg)
        }
        .form-month{
            display:inline-block;
            margin:0 8px;
        }
        .month-wrapper{
            background:#fff;
            max-width: 10rem;
            min-width: 5rem;
            position: absolute;
            text-align: center;
            border: 1px solid #dfdfdf;
            z-index:5;
        }
        button{
            border: 0;
            background: inherit;
        }
        .timer{
            position: absolute;
            font-size: 18px;
            right: 0;
            bottom: 4px;
            visibility: hidden;
            cursor:pointer;
        }
        .timer:hover{
            transform: scale(1.1);
        }
        `
    ],
    providers: [ TIMEPICKER_VALUE_ACCESSOR ]
})

export class TimePicker implements OnInit, ControlValueAccessor {
    @Input() interval: number = 1;

    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_:any) => void = noop;

    show: boolean = false;
    date: any;

    //The internal data model
    private innerValue: any = '';

    constructor(
        private elem: ElementRef
    ){
        
    }

    ngOnInit(){

    }

    onClick(event: Event){
        if(!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }
   
    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue && v != 'Invalid date') {            
            this.innerValue = moment(v,['YYYY-MM-DD HH:mm']).format();
            this.onChangeCallback(moment(this.innerValue).format('HH:mm'));
        }
    }
    

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== null) {
            const momentTime = moment(value,['YYYY-MM-DD HH:mm','HH:mm']);
            this.value = momentTime.format('YYYY-MM-DD HH:mm');
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    changeHour(data:string){
        if(data == 'i')
            this.formatValue(moment(this.value).add(1, 'h'));      
        if (data == 'd')
            this.formatValue(moment(this.value).subtract(1, 'h'));     
    }

    changeMinute(data:string){
        if(data == 'i')
            this.formatValue(moment(this.value).add(this.interval, 'm'));      
        if (data == 'd')
            this.formatValue(moment(this.value).subtract(this.interval, 'm'));     
    }

    formatValue(date: any){
        this.writeValue(date);
    }

}