import { Component, forwardRef, OnInit, ElementRef } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import * as moment from 'moment';

const noop = () => {
};

export const MONTHPICKER_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => MonthPicker),
};


@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'monthpicker',
    templateUrl:'./monthpicker.html',
    styles:[
        `
    .month-container{
        position:relative;
        min-width:6rem;
    }
    i{
        position:absolute;
        right:0;
        bottom:4px;
    }
    input[type=text]{
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        -o-appearance: none;
        margin: 0;
        padding: 0;
        border: none;
        border-radius: 0;
        box-shadow: none;
        background: none;
        height: 1rem;
        color: #000;
        display: inline-block;
        min-width: 6rem;
        border-bottom: 1px solid #9a9a9a;
        padding: 0 .25rem;
    }
    .spin-down{
        transform: rotate(180deg)
    }
    .form-month{
        display:inline-block;
        margin:0 8px;
    }
    p{
        margin:0;
    }
    p.title{
        font-size:18px;
    }
    .month-wrapper{
        background:#fff;
        max-width: 10rem;
        min-width: 5rem;
        position: absolute;
        text-align: center;
        border: 1px solid #dfdfdf;
        z-index:5;
    }
    button{
        border: 0;
        background: inherit;
    }
        `
    ],
    providers: [ MONTHPICKER_VALUE_ACCESSOR ]
})

export class MonthPicker implements OnInit, ControlValueAccessor {
    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    show: boolean = false;
    date: any;

    //The internal data model
    private innerValue: any = '';

    constructor(
        private elem: ElementRef
    ){
        
    }

    ngOnInit(){
        //this.date = moment();
        //this.value = this.date;
    }

    onClick(event: Event){
        if(!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }

    changeYear(data:string): void{
        if(data == 'i')
            this.formatValue(moment(this.value).add(1,'y'));
        if(data == 'd')
            this.formatValue(moment(this.value).subtract(1,'y'))
    }

    changeMonth(data:string): void{
        if(data == 'i')
            this.formatValue(moment(this.value).add(1, 'M'));      
        if (data == 'd')
            this.formatValue(moment(this.value).subtract(1, 'M'));        
    }

    formatValue(date: any){       
        this.writeValue(date);
        this.onChangeCallback(date);
    }

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
}