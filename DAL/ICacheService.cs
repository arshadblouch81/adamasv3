using Microsoft.Data.SqlClient;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using Adamas.Models.Modules;

namespace Adamas.DAL {
    public interface ICacheService
    {
        Task<CacheResult> AppLoginAsync(string token);
        Task<Boolean> AppLogoutAsync(string token);
        bool RemoveToken(string token);
        dynamic GetTokenValue(string token);
        Task<List<dynamic>> GetListOfLoggedUsers();
    }
}