import { Injectable } from '@angular/core'
import { AuthService } from './auth.service';

import { Observable } from 'rxjs';
import { EmailMessage,MimeMessage  } from '@modules/modules';

const email: string = "api/email"
@Injectable()
export class EmailService {

    constructor(
        private auth: AuthService
    ){  }

    sendMail(message: any = null): Observable<any>{
        return this.auth.post(`${email}/email`, message);
    }

    sendEMailWithAttachments(message: any = null): Observable<any>{
        return this.auth.post(`${email}/sendemail`, message);
    }
   

    testMail(request: any): Observable<any>{
        return this.auth.post(`${email}/test-email`, request);
    }
    sendPMail(message: any = null): Observable<any>{
        return this.auth.post(`${email}/pemail`, message);
    }
    
}