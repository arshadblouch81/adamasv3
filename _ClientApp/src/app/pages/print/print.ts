import { Component, OnInit, ViewChild, Input, ElementRef } from '@angular/core'
import { ClrWizard } from '@clr/angular';

import  { AuthService, TimeSheetService, ScriptService } from '../../services/index';
import { FormBuilder, FormGroup, FormControl,Validators } from '@angular/forms'

import Cropper from "cropperjs";

declare let pdfMake: any ;
declare let pdfFonts: any ;

declare var $:any;
@Component({
    templateUrl: './print.html',
    styles:[`

    `]
})

export class PrintComponent implements OnInit {
    src: string = "../../../assets/logo/profile.png";
    hello:any = '';
    
    public constructor(
      private timeS: TimeSheetService,
      private scriptS: ScriptService) {
        this.scriptS.load('pdfMake','vfsFonts');
        
    }

    public ngOnInit() {

      // this.timeS.getPDFReport({
      //   SqlStmt: "sadas",
      //   reportPath: "asdasd",
      //   reportName: "asdasd",
      //   parameters: "asdasdasd"
      // }).subscribe(data => {
        
      // });      
    }

    generatePdf(){
      //pdfMake.vfs = pdfFonts.pdfMake.vfs;
      const documentDefinition = { 
        content: [
          {
            text: 'This is a header (whole paragraph uses the same header style',
            style: 'header'
          }        
        ],
        styles: {
          header: {
            fontSize: 18,
            bold: true
          },
          bigger: {
            fontSize: 15,
            italics: true
          }
        }

       };
      pdfMake.createPdf(documentDefinition).open();
        // var random = faker.random.number(100); 
        // console.log(random)
    }





}
