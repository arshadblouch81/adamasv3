using System;
namespace Adamas.Models.Modules {
    public class Pay {
        public String Pay_Unit { get; set; }
        public double Pay_Rate { get; set; }
        public double Quantity { get; set; }    


        public double Get_Amount()
        {
            return Pay_Rate * Quantity;
        }
    }
}