import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { Subject } from 'rxjs';
import { formatDate } from '@angular/common';
import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
  selector: 'app-billing',
  templateUrl: './workHours.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  },
  textarea{
    resize:none;
  },
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  `]
})
export class WorkHoursComponent implements OnInit {

  unapprovedWorkHoursList: Array<any>;
  tableData: Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  dateFormat: string = 'dd/MM/yyyy';
  postLoading: boolean = false;
  title: string = "Unapproved Shifts";
  token: any;
  tocken: any;
  userRole: string = "userrole";
  dtpStartDate: any;
  dtpEndDate: any;
  RecordNumber: any;
  Recipient: any;
  Date: any;
  printLoad: boolean = false;
  StartTime: any;
  ServiceType: any;
  Staff: any;
  private unsubscribe: Subject<void> = new Subject();

  constructor(
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private billingS: BillingService,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.buildForm();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.loadBatchHistory();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  resetModal() {
  }
  handleCancel() {
    this.modalOpen = false;
    this.router.navigate(['/admin/pays']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
        dtpStartDate: this.dtpStartDate = startOfMonth(new Date()),
        dtpEndDate: this.dtpEndDate = endOfMonth(new Date())
    });
  }

  loadBatchHistory() {
    this.loading = true;
    this.dtpStartDate = this.inputForm.get('dtpStartDate').value;
    this.dtpEndDate = this.inputForm.get('dtpEndDate').value;

    this.dtpStartDate = formatDate(this.dtpStartDate, 'yyyy/MM/dd','en_US');
    this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy/MM/dd','en_US');

    let dataPass = {
        DateStart: this.dtpStartDate,
        DateEnd: this.dtpEndDate,
        IsWhere: null
    }
    
    this.billingS.getUnapprovedWorkHours(dataPass).subscribe(data => {
          this.unapprovedWorkHoursList = data;
          this.tableData = data;
          this.loading = false;
    });
  }

//   loadBatchHistory() {
//     let sql = "Select pay_bill_batch.RecordNumber, pay_bill_batch.OperatorID, pay_bill_batch.BatchDate, pay_bill_batch.BatchNumber, pay_bill_batch.BatchDetail, pay_bill_batch.BatchType, pay_bill_batch.CDCBilled, pay_bill_batch.Date1, pay_bill_batch.Date2, pay_bill_batch.BillBatch# as BillBatch, pay_bill_batch.xDeletedRecord, pay_bill_batch.xEndDate FROM pay_bill_batch INNER JOIN batch_record on batchnumber = batch_record.BCH_NUM WHERE batch_record.bch_type in ('B','S') ORDER BY convert(int, batchnumber) DESC";
//     this.loading = true;
//     this.listS.getlist(sql).subscribe(data => {
//       this.unapprovedWorkHoursList = data;
//       this.tableData = data;
//       this.loading = false;
//     });
//   }
}

