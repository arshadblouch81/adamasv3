import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpParams } from '@angular/common/http';
import { URL } from '@constants/constant';

import { WorkerInput,  GetPackage, ApproveService, QualifiedStaff, SuburbIn, ProgramActive, AddBooking, RecordIncident, NamesAndAddresses, PhoneFaxOther, Recipients} from '@modules/modules';

const dll: string = `api/dll`;
const global: string = `api/global`

declare var Dto: any;
@Injectable()
export class DllService {
    constructor(
        private auth: AuthService
    ) { }

    postAwardInterpreter(data: any): Observable<any> {

        return this.auth.post(`${dll}/award/interpreter`, data)
    
       // const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
       // let serviceURL=AppConfiguration.Setting().Application.ServiceUrl;
        
    
       //return this.http.get(`http://20.213.152.181:4040/dll.asmx/InterpretAwards?User=sysmgr&Password=sysmgr&s_StaffCode=MELLOW MARSHA&WEDate=18/06/2023&b_IncludeAbsences=True`);
      
       //  return this.http.post('http://20.213.152.181:4040/dll.asmx/InterpretAwards?User=sysmgr&Password=sysmgr&s_StaffCode=MELLOW MARSHA&WEDate=18/06/2023&b_IncludeAbsences=True'
        // , id, { headers: headers || headers, responseType: 'text' });
    
       
    }
    createcopyroster(data: any): Observable<any> {

        return this.auth.get(`${dll}/roster/createcopyroster`, data)

    }

    CheckFundingAlerts(data: any): Observable<any> {

        return this.auth.get(`${dll}/funding/alerts`, data)

    }

    HCPBalanceExport(data: any): Observable<any> {

        return this.auth.get(`${dll}/hcp/balance/export`, data)

    }

    NDIAClaimUpload(data: any): Observable<any> {

        return this.auth.get(`${dll}/ndia/claim/upload`, data)

    }

    TravelInterpreter(data: any): Observable<any> {

        return this.auth.get(`${dll}/travel/interpreter`, data)

    }   

    PayBillUpdate(data: any): Observable<any> {

        return this.auth.get(`${dll}/PayBillUpdate`, data)

    }   

}