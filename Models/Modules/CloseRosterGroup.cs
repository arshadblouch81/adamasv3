using System;

namespace Adamas.Models.Modules
{
    public class CloseRosterGroup {
      public DateTime CloseDate { get; set; }
      public string Programs { get; set; }
      public string Fundings { get; set; }
    }
}