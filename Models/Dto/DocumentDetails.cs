﻿namespace AdamasV3.Models.Dto
{
    public class DocumentDetails
    {
        public string Filename { get; set; }
        public string OriginalLocation { get; set; }
    }
}
