import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';

import {
  RouteGuard,
  AdminStaffRouteGuard,
  CanDeactivateGuard,
  LoginGuard,
  AdminRouteGuard,
  ByPassGuard
} from '@services/index'

import {
  RecipientCasenoteAdmin,
  RecipientContactsAdmin,
  RecipientHistoryAdmin,
  RecipientIncidentAdmin,
  RecipientIntakeAdmin,
  RecipientClinicalAdmin,
  RecipientDatasetAdmin,
  RecipientOpnoteAdmin,
  RecipientPensionAdmin,
  RecipientPermrosterAdmin,
  RecipientPersonalAdmin,
  RecipientQuotesAdmin,
  RecipientRemindersAdmin,
  RecipientFormsAdmin,
  RecipientDocumentsAdmin,
  RecipientAttendanceAdmin,
  RecipientOthersAdmin,
  RecipientAccountingAdmin,
  RecipientShiftReportsAdmin,
  RecipientSuspensionAdmin,
  RecipientLoans,
  ContactsAdmin,
  
  
} from './index'

const routes: Routes = [
  {
    path: 'personal',
    component: RecipientPersonalAdmin,
    canDeactivate: [CanDeactivateGuard]
  },
  // {
  //   path: 'contacts',
  //   component: RecipientContactsAdmin
  // },
  {
    path: 'reminders',
    component: RecipientRemindersAdmin
  },
  {
    path: 'documents',
    component: RecipientDocumentsAdmin
  },
  {
    path: 'attendance',
    component: RecipientAttendanceAdmin,
    canDeactivate: [CanDeactivateGuard]
  },
  // {
  //   path: 'accounting',
  //   component: RecipientAccountingAdmin
  // },
  {
    path: 'others',
    component: RecipientOthersAdmin,
    canDeactivate: [CanDeactivateGuard]
  },
  {
  path: 'loans',
  component: RecipientLoans,
 },
  {
    path: 'opnote',
    component: RecipientOpnoteAdmin
  },
  {
    path: 'casenote',
    component: RecipientCasenoteAdmin
  },
  {
    path: 'incidents',
    component: RecipientIncidentAdmin
  },
  {
    path: 'perm-roster',
    component: RecipientPermrosterAdmin
  },
  {
    path: 'history',
    component: RecipientHistoryAdmin
  },
  {
    path: 'insurance-pension',
    component: RecipientPensionAdmin
  },
  {
    path: 'quotes',
    component: RecipientQuotesAdmin
  },
  
  {
    path: 'media',
    component: MediaList,
  },
  {
    path: 'shift-reports',
    component: RecipientShiftReportsAdmin   
  },
  {
    path: 'suspensions',
    component: RecipientSuspensionAdmin,
  },
  {
    path: 'intake',
    component: RecipientIntakeAdmin,
    children: [
      {
        path: '',
        loadChildren: () => import('./intake-views/intake-admin.module').then(m => m.IntakeAdminModule)
      }
    ]
  },
  {
    path: 'clinical',
    component: RecipientClinicalAdmin,
    children: [
      {
        path: '',
        loadChildren: () => import('./clinical-views/clinical-admin.module').then(m => m.ClinicalAdminModule)
      }
    ]
  },
  {
    path: 'dataset',
    component: RecipientDatasetAdmin,
    children: [
      {
        path: '',
        loadChildren: () => import('./dataset-views/dataset-admin.module').then(m => m.DatasetAdminModule)
      }
    ]
  },
  {
    path: 'accounting',
    component: RecipientAccountingAdmin,
    children: [
      {
        path: '',
        loadChildren: () => import('./accounting-views/accounting.admin.module').then(m => m.AccountingAdminModule)
      }
    ]
  },
  {
    path: 'contacts',
    component: RecipientContactsAdmin,
    children: [
      {
        path: '',
        loadChildren: () => import('./contacts-view/contact-admin.module').then(m => m.ContactAdminModule)
      }
    ]
  },
  
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule
    
  ],
  declarations: [
   
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class RecipientsAdminModule {}

