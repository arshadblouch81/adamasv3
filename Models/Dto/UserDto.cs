﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class UserDto
    {
        public string User { get; set; }
        public string UniqueId { get; set; }
        public string StaffCode { get; set; }
        public int Recnum { get; set; }

    }
}
