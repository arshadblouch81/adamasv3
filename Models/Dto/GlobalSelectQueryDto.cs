﻿namespace AdamasV3.Models.Dto
{
    public class GlobalSelectQueryDto
    {
        public string Query { get; set; }
    }
}
