namespace Adamas.Models.Modules{
    public class TravelClaim{
        public string RecordNo { get; set; }
        public string User { get; set; } = "";
        public string Distance { get; set; }
        public string TravelType { get; set; }
        public string ChargeType { get; set; }
        public string StartKm { get; set; }
        public string EndKm { get; set; }
        public string Notes { get; set; }
    }
}