using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;


namespace Adamas.Models.Tables{
    
    public class Registration {        
        [Key]
        public int SqlId { get; set; }
        public string SharedDocumentPath { get; set; }
        public string BrandingLogoSmall { get; set; }
        public string BrandingLogoBig { get; set; }
        public string Adamas1 { get; set; }
        public string Adamas2 { get; set; }
        public string Adamas3 { get; set; }
        public string Adamas4 { get; set; }
        public string Adamas5 { get; set; }
        public string Adamas6 { get; set; }
        public string Adamas7 { get; set; }
        public string Adamas8 { get; set; }
        public string Adamas9 { get; set; }
        public bool PopulateIntakeEmail { get; set; }
        public string ClientPortalManagerMethod{get;set;}
        public string coName { get; set; }
        public string coPhone2 { get; set; }
        public string ACN { get; set; }
        public string coPhone1 { get; set; }
        public string coFax { get; set; }
        public string coMobile { get; set; }
        public string coAddress1 { get; set; }
        public string coEmail { get; set; }
        public string coAddress2 { get; set; }
        public string coAddress3 { get; set; }
        public string coSuburb { get; set; }
        public string coPostCode { get; set; }
        public string prodDesc { get; set; }
        public int DOHTransportProviderID { get; set; }
        public string logoFile { get; set; }


        public string bankName { get; set; }
        public string bpaybillercode { get; set; }
        public string bankBSB { get; set; }
        public string bankAccountName { get; set; }
        public string bankAccountNumber { get; set; }
        public string CSTDAContact { get; set; }
        public string prodVer {get;set;}
        public bool DoesWADSC { get; set; }
        public string reg_contact_position {get;set;}
        public string CSTDAEmail {get;set;}
        public string DSCUserName {get;set;}
        public string DSCPassword {get;set;}


        public string SQLVersion {get;set;}
        public string ODBCTimeout {get;set;}
        public bool auditTriggers {get;set;}
        public int SessionCheck {get;set;}
        
        public bool brokerageRequestModule {get;set;}
        public int TRACCS_MAX_SESSIONS {get;set;}
        public int RECIPIENT_MAX_SESSIONS {get;set;}
        public int STAFF_MAX_SESSIONS {get;set;}
        public int ROSTERS_MAX_SESSIONS {get;set;}
        public int DAYMANAGER_MAX_SESSIONS {get;set;}
        public int TIMESHEET_MAX_SESSIONS {get;set;}
        public int REPORTS_MAX_SESSIONS {get;set;}
        public bool AllowCDCMultiClaim {get;set;}


        public bool ForceRecipientOptions {get;set;}
        public string AccountWidth {get;set;}
        public string AccountingIDgeneration {get;set;}
        public string LegacyCareplanLabel {get;set;}
        public string WizardDefaultNote {get;set;}
        public bool AutoExportCaseNotes {get;set;}
        public string AutoExportCaseNotesFolder {get;set;}
        public bool UppercaseSurname{get;set;}
        public bool AutoGenerateFileNumber{get;set;}
        public bool DisplayAllRecipients{get;set;}
        public bool ONIArchive{get;set;}
        public bool ForceCaseNote1{get;set;}
        public bool ForceSuspendReminder{get;set;}
        public bool AutoExportONI{get;set;}
        public bool PrintONIOnReferral{get;set;}
        public string CustomPkgBudgetFormat{get;set;}
        public string CustomPkgStatementFormat{get;set;}
        public bool DisplaySaveWarningInRecipients{get;set;}
        public bool CaseNotesWarning{get;set;}
        public bool CaseNotesSecurityCheck{get;set;}
        public bool StoreNameAsCreator{get;set;}
        public bool IncludeReferralsInActive{get;set;}
        public bool IncludeWaitingListInActive{get;set;}
        public bool RestrictReferrals{get;set;}

        public string IMStyleRecip{get;set;}
        public string IMStyleStaff{get;set;}

        public bool EnforceActivityLimits{get;set;}
        public string RosterAwardEnforcement{get;set;}
        public string IncrementalRosterCreation{get;set;}
        public bool IncrementalCompetencyAction{get;set;}
        public bool UseNewDayManager{get;set;}
        public string MealLabelAgencyName{get;set;}
        public bool DotPointXtraInfo{get;set;}
        public string nmCycle1{get;set;}
        public string nmCycle2{get;set;}
        public string nmCycle3{get;set;}
        public string nmCycle4{get;set;}
        public string nmCycle5{get;set;}
        public string nmCycle6{get;set;}
        public string nmCycle7{get;set;}
        public string nmCycle8{get;set;}
        public string nmCycle9{get;set;}
        public string nmCycle10{get;set;}
        public string RosterEmail{get;set;}

        public string DefaultPayPeriod{get;set;}
        public string DefaultPayStartDay{get;set;}
        public string CompetencyEnforcement{get;set;}
        public bool payPrimacy{get;set;}
        public bool KeepPayOnAllocate{get;set;}
        public bool AutoAutoApproveRecipientAdmin{get;set;}
        public bool DisableDayManagerNotes{get;set;}
        public bool UseAwards{get;set;}
        public int UsePositions{get;set;}
        public bool NewTimesheet{get;set;}
        public bool TimesheetIsRestricted{get;set;}
        public bool googletravel{get;set;}
        public string TravelProvider{get;set;}
        public string ProviderKey{get;set;}

        public string BillingMethod{get;set;}
        public string CustomInvoiceFormat{get;set;}
        public bool InclAgedSumInvoice{get;set;}
        public string Bill_DirectDebit{get;set;}
        public string Bill_OnSiteDetails{get;set;}
        public string Bill_MailToDetails{get;set;}
        public string Bill_ByPhoneDetails{get;set;}
        public string EmailInvoiceSubjectSetting{get;set;}
        public string EmailInvoiceSubjectText{get;set;}


        public int ShortNoticeThreshold{get;set;}
        public int NoNoticeThreshold{get;set;}
        public int MinimumCancellationLeadTime{get;set;}
        public int BookingLeadTime{get;set;}
        public string CancellationFeeText{get;set;}

        public string POP3Server{get;set;}
        public string POP3User{get;set;}
        public string POP3Password{get;set;}
        public string SMTPServer{get;set;}
        public string SMTPUser{get;set;}
        public string SMTPPassword{get;set;}
        public string IMEmailAddress{get;set;}
        public string IMEmailDisplay{get;set;}
        public string HelpDeskEmail{get;set;}
        public string DefaultInvoiceEmailSubject{get;set;}
        public string smsPassword{get;set;}
        public string eMailMode{get;set;}
        public string OutlookPath{get;set;}


        public int MinimumInternetSpeedForOnline{get;set;}
        public double MobileGEOCodeLimit{get;set;}
        public int StaffLocationUpdateInterval{get;set;}
        public string MobilUserURL{get;set;}
        public string WebKioskUserURL{get;set;}
        public bool LockWebkiosk{get;set;}
        public string TA_T1{get;set;}
        public string TA_T2{get;set;}
        public string TA_T3{get;set;}
        public bool ShowClientPhoneInApp{get;set;}
        public string TA_TRAVELDEFAULT{get;set;}
        public bool TA_ForceNoteOnForcedLogOnOff{get;set;}
        public string DefaultAppNoteCategory{get;set;}
        public bool TA_ApproveWithoutCorrection{get;set;}

        public  int NDIATravel{get;set;}
        public string NDIACancelFeePercent{get;set;}

        public string Pension{get;set;}
        public string PensionCoupleEach{get;set;}
        public string PensionCoupleCombined{get;set;}
        public string PensionCoupleEachIll{get;set;}
        public string BasicFeePercent{get;set;}
        public bool UseMedicareImport{get;set;}
        public bool AllowChangeIncomeTestedFee{get;set;}


        public bool StaffLeaveEmailOverrides{get;set;}
        public string StaffLeaveEmail{get;set;}
        public bool ClientNoteEmailOverrides{get;set;}
        public string ClientNoteEmail{get;set;}


        public string DocusignServerPort{get;set;}
        public string DocusignServerRef{get;set;}

        public string POStdDisclaimer{get;set;}

        public bool NDIAAutoTravelClaim{get;set;}

        public string MTA_ErrorLogFile {get;set;}

        public bool appUsesSMTP{get;set;}

         // Threshold Parameters
    public string PAN_TANoShowTH { get; set; }
    public string PAN_TANoGoResend { get; set; }
    public string PAN_TANoShowResend { get; set; }    
    public string PAN_TAEarlyStartTH { get; set; }
    public string PAN_TALateStartTH { get; set; }
    public string PAN_TAEarlyFinishTH { get; set; }
    public string PAN_TALateFinishTH { get; set; }
    public string PAN_TAOverstayTH { get; set; }
    public string PAN_TAUnderstayTH { get; set; }
    public string PAN_TANoWorkTH { get; set; }

    // Notification Parameters for Early Start
    public bool? PAN_TAEarlyStartTHEmail { get; set; }
    public bool? PAN_TAEarlyStartTHSMS { get; set; }
    public string PAN_TAEarlyStartTHWho { get; set; }

    // Notification Parameters for Early Finish
    public bool? PAN_TAEarlyFinishTHEmail { get; set; }
    public bool? PAN_TAEarlyFinishTHSMS { get; set; }
    public string PAN_TAEarlyFinishTHWho { get; set; }

    // Notification Parameters for Late Start
    public bool? PAN_TALateStartTHEmail { get; set; }
    public bool? PAN_TALateFinishTHSMS { get; set; }
    public string PAN_TALateStartTHWho { get; set; }


    // Notification Parameters for Overstay
    public bool? PAN_TAOverstayTHEmail { get; set; }
    public bool? PAN_TAOverstayTHSMS { get; set; }
    public string PAN_TAOverstayTHWho { get; set; }

    // Notification Parameters for Understay
    public bool? PAN_TAUnderstayTHEmail { get; set; }
    public bool? PAN_TAUnderstayTHSMS { get; set; }
    public string PAN_TAUnderstayTHWho { get; set; }

    // Notification Parameters for No Work
    public bool? PAN_TANoWorkTHEmail { get; set; }
    public bool? PAN_TANoWorkTHSMS { get; set; }
    public string PAN_TANoWorkTHWho { get; set; }

    // Miscellaneous Parameters
    public string PanLogFileLocation { get; set; }
    public string PAN_DMRefreshInterval { get; set; }

    }

}