﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class ProgramLevelDto
    {
        public bool? IsCDC { get; set; }
        public string Level { get; set; }
        public string User2 { get; set; }
        public string Quantity { get; set; }
        public string TimeUnit { get; set; }
        public double DefaultDailyFee { get; set; }
        public string Type { get; set; }
        public string ExpireUsing { get; set; }
        public int RecordNumber { get; set; }

    }
}
