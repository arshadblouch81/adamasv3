using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Adamas.Models.Tables {
    public class IM_Master
    {
        [Key]
        public int RecordNo { get; set; }
        public string PersonId { get; set; }
        public string Type { get; set; }
        public string Service { get; set; }
        public string Severity { get; set; }
        [Column("Date", TypeName="DateTime2")] 
        public DateTime? Date { get; set; }
        [Column("Time", TypeName="DateTime2")] 
        public DateTime? Time { get; set; }
        public int? Duration { get; set; }
        public string Location { get; set; }
        public string LocationNotes { get; set; }
        public string ReportedBy { get; set; }
        [Column("DateReported", TypeName="DateTime2")] 
        public DateTime? DateReported { get; set; }
        public string CurrentAssignee { get; set; }
        public bool? Reported { get; set; }
        public string ShortDesc { get; set; }
        public string FullDesc { get; set; }
        public string Triggers { get; set; }
        public string InitialAction { get; set; }
        [Column("InitialFupBy", TypeName="DateTime2")] 
        public DateTime? InitialFupBy { get; set; }
        public bool? Completed { get; set; }
        public string OngoingAction { get; set; }
        public string OngoingNotes { get; set; }
        public string Background { get; set; }
        public bool? DOPWithDisability { get; set; }
        public string Abuse { get; set; }
        public string SeriousRisks { get; set; }
        public string Complaints { get; set; }
        public string Perpetrator { get; set; }
        public string PerpSpecify { get; set; }
        public string Notify { get; set; }
        public string NoNotifyReason { get; set; }
        public string Notes { get; set; }
        public string Setting { get; set; }
        public string Program { get; set; }
        public string Status { get; set; }
        public string DSCServiceType { get; set; }
        public string TriggerShort { get; set; }
        public int? Incident_Level { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
        public string Position { get; set; }
        public string Phone { get; set; }
        public DateTime? Verbal_Date { get; set; }
        public string Verbal_Time { get; set; }
        public string By_Whome { get; set; }
        public string To_Whome { get; set; }
        public string BriefSummary { get; set; }
        public string ReleventBackground { get; set; }
        public string SummaryofAction { get; set; }
        public string SummaryOfOtherAction { get; set; }
        public string DSQ_fund { get; set; }
        public string DSQ_Fund_ServiceName { get; set; }
        public string DSQ_Fund_Service_Address { get; set; }
        public string DSQ_Fund_Service_Phone { get; set; }
        public string DSQ_Fund_Service_Contact_Person { get; set; }
        public string DSQ_Fund_Service_Type { get; set; }
        public string SubjectName { get; set; }
        public string SubjectDOB { get; set; }
        public string SubjectAge { get; set; }
        public string SubjectGender { get; set; }
        public string SubjectAssociation { get; set; }
        public string SubjectType { get; set; }
        public string SubjectTypeOther { get; set; }
        public string ResidentSubject { get; set; }
        public string ResidenceSubjectOther { get; set; }
        public string PrimaryDisability { get; set; }
        public string PrimaryDisablityOther { get; set; }
        public string YoungCommission { get; set; }
        public string ChildProtection { get; set; }
        public string OtherPartyName { get; set; }
        public string OtherPartyDOB { get; set; }
        public string OtherPartyAge { get; set; }
        public string OtherPartyGender { get; set; }
        public string OtherPartyAssociation { get; set; }
        public string OtherPartyType { get; set; }
        public string OtherPartyTypeOther { get; set; }
        public string ResidentOtherParty { get; set; }
        public string ResidenceOtherPartyOther { get; set; }
        public string PrimaryDisabilityOtherParty { get; set; }
        public string PrimaryDisabilityOtherPartyOther { get; set; }
        public string Death { get; set; }
        public string Injury { get; set; }
        public string TypeOther { get; set; }
        public string Occured { get; set; }
        public string OccuredOther { get; set; }
        public string EstimatedTime { get; set; }
        public string EstimatedTimeOther { get; set; }
        public string Policy { get; set; }
        public string Charge { get; set; }
        public string Refered { get; set; }
        public string Family { get; set; }
        public string Manager { get; set; }
        public string IncidentTypeOther { get; set; }
        public string Mobile { get ; set; }
        public string OfficeUse { get; set; }
        public string FollowupContacted { get; set; }
        public string FollowupContactedOther { get; set; }
        public string SubjectMood { get; set; }
        public string InitialNotes { get; set; }
        

    }
}