namespace Adamas.Models.Modules{
    public class RegistrationCounter {
        public int CustomerId { get; set; }
        public int CarerId { get; set; }
        public string ProdName { get; set; }
        public string UniqueId { get; set; }
    }
}