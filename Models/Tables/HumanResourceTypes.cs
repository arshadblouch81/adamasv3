using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class HumanResourceTypes
    {
        [Key]
        public int? RecordNumber { get; set; }
        public string Group { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public string Phone1 { get; set; }
        public string Notes { get; set; }
        public string Email{ get; set; }
        public DateTime? CloseDate { get; set; }
        public string User3 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public string Mobile { get; set; }
        public string HRT_DATASET { get; set; }
        public bool? GST { get; set; }
        public int? GSTRate { get; set; }
        public string BudgetAmount { get; set; }
        public string BudgetPeriod { get; set; }
        public string BUDGET_1 { get; set; }
        public string BUDGET_2 { get; set; }
        public string BUDGET_3 { get; set; }
        public string BUDGET_4 { get; set; }
        public string BUDGET_5 { get; set; }
        public int? COID { get; set; }
        public int? BRID { get; set; }
        public int? DPID { get; set; }
        public DateTime? EndDate { get; set; }
        public string USER1 { get; set; }
        public string User2 { get; set; }
        public bool? UserYesNo1 { get; set; }
        public bool? UserYesNo2 { get; set; }
        public string User4 { get; set; }
        public string User5 { get; set; }
        public bool? UserYesNo3 { get; set; }
        public bool? UserYesNo4 { get; set; }
        public bool? UserYesNo5 { get; set; }
        public string User6 { get; set; }
        public string User7 { get; set; }
        public string User8 { get; set; }
        public string User9 { get; set; }
        public string User10 { get; set; }
        public string User11 { get; set; }
        public string User12 { get; set; }
        public string P_Def_Alert_Type { get; set; }
        public string P_Def_Alert_BaseOn { get; set; }
        public string P_Def_Alert_Period { get; set; }
        public string P_Def_Alert_Allowed { get; set; }
        public string P_Def_Alert_Yellow { get; set; }
        public string P_Def_Alert_Orange { get; set; }
        public string P_Def_Alert_Red { get; set; }
        public string P_Def_Expire_Amount { get; set; }
        public string P_Def_Expire_CostType { get; set; }
        public string P_Def_Expire_Unit { get; set; }
        public string P_Def_Expire_Period { get; set; }
        public string P_Def_Expire_Length { get; set; }
        public string P_Def_Fee_BasicCare { get; set; }
        public string P_Def_Fee_IncomeTested { get; set; }
        public string P_Def_Fee_TopUp { get; set; }
        public string P_Def_Contingency_PercAmt { get; set; }
        public string P_Def_Admin_AdminType { get; set; }
        public string P_Def_Admin_Admin_PercAmt { get; set; }
        public string P_Def_Admin_CMType { get; set; }
        public string P_Def_Admin_CM_PercAmt { get; set; }
        public string P_Def_StdDisclaimer { get; set; }
        public string P_Def_Admin_AdminFrequency { get; set; }
        public string P_Def_Admin_CMFrequency { get; set; }
        public string P_Def_Admin_AdminDay { get; set; }
        public string P_Def_Admin_CMDay { get; set; }
        public string P_Def_Expire_Using { get; set; }
        public string P_Def_Contingency_Max { get; set; }
        public bool? P_Def_QueryAutoDeleteAdmin { get; set; }
        public bool? P_Def_IncludeIncomeTestedFeeInAdmin { get; set; }
        public string DefaultCHGTravelWithinActivity { get; set; }
        public string DefaultCHGTravelWithinPayType { get; set; }
        public string DefaultNCTravelWithinProgram { get; set; }
        public string DefaultNCTravelWithinActivity { get; set; }
        public string DefaultNCTravelWithinPayType { get; set; }
        public string DefaultCHGTravelBetweenActivity { get; set; }
        public string DefaultCHGTravelBetweenPayType { get; set; }
        public string DefaultNCTravelBetweenProgram { get; set; }
        public string DefaultNCTravelBetweenActivity { get; set; }
        public string DefaultNCTravelBetweenPayType { get; set; }
        public bool? P_Def_IncludeClientFeesInCont { get; set; }
        public bool? P_Def_IncludeBasicCareFeeInAdmin { get; set; }
        public bool? P_Def_IncludeTopUpFeeInAdmin { get; set; }
        public string User13 { get; set; }
        public int? NoNoticeLeadTime { get; set; }
        public int? ShortNoticeLeadTime { get; set; }
        public string NoNoticeLeaveActivity { get; set; }
        public string ShortNoticeLeaveActivity { get; set; }
        public string WithNoticeLeaveActivity { get; set; }
        public bool? UserYesNo6 { get; set; }
        public string DefaultNoNoticeCancel { get; set; }
        public string DefaultNoNoticeBillProgram { get; set; }
        public string DefaultNoNoticePayProgram { get; set; }
        public string DefaultNoNoticePaytype { get; set; }
        public string DefaultShortNoticeCancel { get; set; }
        public string DefaultShortNoticeBillProgram { get; set; }
        public string DefaultShortNoticePayProgram { get; set; }
        public string DefaultShortNoticePaytype { get; set; }
        public string DefaultWithNoticeCancel { get; set; }
        public int? NoNoticeCancelRate { get; set; }
        public int? ShortNoticeCancelRate { get; set; }
        public string DefaultWithNoticeProgram { get; set; }
        public string CDCStatementText1 { get; set; }
        public string DefaultCHGTravelWithinProgram { get; set; }
        public string DefaultCHGTravelBetweenProgram { get; set; }
        public string BudgetEnforcement { get; set; }
        public string BudgetRosterEnforcement { get; set; }
        public bool? xDeletedRecord { get; set; }
        public bool? UserYesNo7 { get; set; }

    }
}