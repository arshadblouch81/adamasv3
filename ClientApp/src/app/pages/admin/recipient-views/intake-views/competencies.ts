import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService,dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';


@Component({
    selector: '',
    templateUrl: './competencies.html',
    styles: [`
     .selected{
        background-color: #85B9D5;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    } 
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakeCompetencies implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;

    definedOpen: boolean = false;   
    competencyOpen: boolean = false;

    addOREdit: number;

    groupTypes: Array<any>;
    lstCompetencies: Array<any>;

    userGroupForm: FormGroup;
    competencyForm: FormGroup;
    dateFormat: string = dateFormat;
    dropDowns: {
        userGroups: Array<string>,
        competencys: Array<string>
    }
   
    selectedcompetencies: Array<any>=[];
    activeRowData:any;
    selectedRowIndex:number;
    serviceActivityList: Array<any>=[];
    originalList: Array<any>=[];
    txtSearch:string;
    ViewAllocateResourceModal:boolean=false;
    selectedCompetency:any;
    HighlightRow2: number;
    token:any;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/intake/branches'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'competencies')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();
        this.listDropDowns();
        
      
       
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(data: any): void {
      
        let records =this.selectedcompetencies.map(x=>x.recordNumber).join(',').toString();

          this.deletecompetency(records);
        
  
    }
    search(user: any = this.user) {

      
        this.cd.reattach();
        this.loading = true;
        forkJoin([
           
            this.timeS.getspecificcompetencies(user.id),
            this.timeS.getrecipientcompetencies(this.user.id)
        ]).subscribe(data => {
            this.loading = false;
          
            this.lstCompetencies = data[0];
            this.serviceActivityList=data[1];
            this.originalList=data[1];
            this.cd.markForCheck();
        });
    }

    buildForm() {
   
         
         this.competencyForm = this.formBuilder.group({
            competencyValue: new FormControl('', [Validators.required]),
            notes: new FormControl(''),
            personID: new FormControl(''),
            recordNumber: new FormControl(0),
            mandatory: new FormControl(false),
            certReg : new FormControl(''),
            selectedcompetencies:new FormControl('')
         })
    }
    
    RecordClicked(event: any, value: any, i: number, mian:boolean) {

        this.activeRowData = value;
        if (mian)
             this.selectedRowIndex =i;
        else
            this.selectedRowIndex =-1;
      
         
       //console.log(this.selectedcompetencies.includes(value))
       
    
        if (event.ctrlKey || event.shiftKey) {
            if (!this.selectedcompetencies.includes(value))
                 this.selectedcompetencies.push(value);
            //this.selected.push(value.recordNo)
          
    
        } else {
          
          this.selectedcompetencies = [];
         // this.selected = [];
    
          if (this.selectedcompetencies.length <= 0) {
            this.selectedcompetencies.push(value)
            
          }
        }
      }
   

    onItemSelected(sel: any, i:number): void {
        this.selectedCompetency=sel;    
         this.HighlightRow2=i;
     
     }
     
     onItemDbClick(sel:any , i:number) : void {
     
         this.HighlightRow2=i;
         this.selectedCompetency=sel;
         this.save();
     
     }
     success() {
        this.search(this.sharedS.getPicked());
        this.loading = false;
      }
    save() {

        let input={
            personID:this.user.id,
            selectedcompetency:this.selectedcompetencies,
            mandatory:false,
            notes:'',
            Creator : this.token.user
        }
        this.ViewAllocateResourceModal=false;
        this.timeS.postintakecompetencies(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
                
                this.success();
                this.globalS.sToast('Success', 'Data Added');

                
              }
        });
    }


    delete(index: number) {

    }

    handleCancel(view: number) {
        
        this.addOREdit = 1;
      
            this.selectedcompetencies = [];
            this.competencyOpen = false;
            // this.competencyForm.reset();
        
    }

    reloadAll(){
        this.search()
        this.listDropDowns()
    }
   
    listDropDowns(){
        forkJoin([
            this.listS.getusergroup(this.user.id),
            this.timeS.getspecificcompetencies(this.user.id)
        ]).subscribe(data => {
            this.loading = false;
            let usergroup = data[0].map(x => {
                return {
                    label: x,
                    value: x,
                    checked: false
                }
            });
           let competency = data[1].map(x => {
                return {
                    label: x,
                    value: x,
                    checked: false
                }
            });
            this.dropDowns = {
                userGroups: usergroup,
                competencys: competency
            }
        });
    }

    showAddModal(view: number) {
     
      
        this.addOREdit = 1;
        this.ViewAllocateResourceModal=true; 
    }

 

    competencyProcess(){
        this.competencyForm.controls['personID'].setValue(this.user.id)
        let competencys = this.competencyForm.value;

            this.timeS.updateintakecompetency(competencys)
                .subscribe(data => {
                    if(data){
                        this.reloadAll()
                        this.globalS.sToast('Success','Data Updated')
                        this.handleCancel(2);
                    }
                })
        
    }

    showEditModal(data: any){
        this.addOREdit = 2;
        this.competencyOpen = true;

        this.competencyForm.patchValue({
            competencyValue: data.competency,
            notes: data.notes,
            mandatory : data.mandatory,
            recordNumber: data.recordNumber
        })
    }

    handleOk(){

    }

    deletecompetency(data: any){
        this.timeS.deleterecipientpreference(data)
                    .subscribe(data =>{
                        if(data){
                            this.reloadAll()
                            this.globalS.sToast('Success','Data Deleted')
                        }
                    })
    }

    onTextChangeEvent(event:any){
        // console.log(this.txtSearch);
         let value = this.txtSearch.toUpperCase();
         //console.log(this.serviceActivityList[0].description.includes(value));
         this.serviceActivityList=this.originalList.filter(element=>element.description.includes(value));
     }


    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }

    print(){
        
    }
    generatePdf(){
      
        var Title = " Competencies " ;
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.lstCompetencies,                        
                "userid": this.token.user,
                "txtTitle":  Title+" for "+this.user.code,                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = Title+" for "+this.user.code, 
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
    
}