﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Modules
{
    public class Appsettings
    {
        public Logging Logging { get; set; }
        public string AllowedHosts { get; set; }
        public JwtIssuerOptions JwtIssuerOptions { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }
        public DocumentPaths DocumentPaths { get; set; }
        public string BookingLeadTime { get; set; }
        public FilesizeLimit FilesizeLimit { get; set; }
        public ProfilePhotos ProfilePhotos { get; set; }
        public JSReportConfiguration JSReportConfiguration { get; set; }

    }

    public class JSReportConfiguration
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string JSReportUrl { get; set; }
        public string ConnectionString { get; set; }
    }

    public class ProfilePhotos
    {
        public string Directory { get; set; }
        public string MaxFileSize { get; set; }
    }

    public class FilesizeLimit
    { 
        public int Photo { get; set; }
        public int Video { get; set; }
    }


    public class DocumentPaths 
    { 
        public string GViewPath { get; set; }
        public string DocumentPreviewPath { get; set; }
        public string Embedded { get; set; }
        public string StaffPath { get; set; }
        public string RecipientPath { get; set; }
    }

    public class Logging
    {
        public LogLevel LogLevel { get; set; }
    }

    public class LogLevel { 
        public string Default { get; set; }
    }

    public class JwtIssuerOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Key { get; set; }
        public int ExpireTime { get; set; }
        public EmailConfiguration EmailConfiguration { get; set; }
        
    }

    public class ConnectionStrings
    {
        public string Production { get; set; }
    }
    public class EmailConfiguration
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public string SmtpOriginEmail { get; set; }
        public string SmtpOriginName { get; set; }
        public bool SmtpEnableSampleMail { get; set; }
        public string SmtpCCEmail { get; set; }
        public string SmtpReplyTo { get; set; }
        public string SmtpRecipientEmail { get; set; }
    }
}
