using System;

namespace Adamas.Models.Modules{
    public class CopyDocument {
       public string SourcePath { get; set; }
       public string DestinationPath { get; set; }
       public string SourceFileName { get; set; }
       public string DestinationFileName { get; set; }
    }
}