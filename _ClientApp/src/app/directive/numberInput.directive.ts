import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[ExcludeNumber]'
})

export class NumberInputDirective {

    lastValue: string;

    constructor(private el: ElementRef) { }
    @Input() ExcludeNumber: boolean;

    @HostListener('input', ['$event']) onInputEvent($event) {
        var start = $event.target.selectionStart;
        var end = $event.target.selectionEnd;
        $event.target.value = $event.target.value.toUpperCase();
        $event.target.setSelectionRange(start, end);
        $event.preventDefault();

        if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
            this.lastValue = this.el.nativeElement.value = $event.target.value;
            // Propagation
            const evt = new Event('HTMLEvents');
            evt.initEvent('input', false, true);
            event.target.dispatchEvent(evt);
        }    
    }

    @HostListener('keypress',['$event']) onKeyPress($event){
        let e = $event;
        if (this.ExcludeNumber)   {
        //console.log(e)
        e.target.value = e.target.value.toUpperCase();

        // Ensure that it is a number and stop the keypress
        if (!e.key.match(/^[a-zA-Z ]*$/)) {
            return false;
        }        

        if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+C
            (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+V
            (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
            // Allow: Ctrl+X
            (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) 
            {
            // let it happen, don't do anything
                return e;
            }
    
        }
    }
}

@Directive({
    selector: '[AccountGenerate]'
})
  
export class AccountGenerateDirective {

    lastValue: string;

    constructor(private el: ElementRef) { }
    @Input() AccountGenerate: string;

    @HostListener('input', ['$event']) onInputEvent($event) {
        var start = $event.target.selectionStart;
        var end = $event.target.selectionEnd;
        $event.target.value = $event.target.value.toUpperCase();
        $event.target.setSelectionRange(start, end);
        $event.preventDefault();

        if (!this.lastValue || (this.lastValue && $event.target.value.length > 0 && this.lastValue !== $event.target.value)) {
            this.lastValue = this.el.nativeElement.value = $event.target.value;
            // Propagation
            const evt = new Event('HTMLEvents');
            evt.initEvent('input', false, true);
            event.target.dispatchEvent(evt);
        }    
        console.log(this.AccountGenerate)
    }
    
}