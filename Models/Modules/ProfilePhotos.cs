namespace Adamas.Models.Modules
{
    public class ProfilePhotos
    {
        public string Directory { get; set; }
        public int MaxFileSize { get; set; }
    }
}