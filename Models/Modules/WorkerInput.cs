namespace Adamas.Models.Modules{
    public class WorkerInput{
        public string ClientCode { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}