using System;

namespace Adamas.Models.Modules
{
    public class Diagnosis
    {
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string Description { get; set; }
        public string ICDCode {get;set;}
        public string Code {get;set;}
    }
}
