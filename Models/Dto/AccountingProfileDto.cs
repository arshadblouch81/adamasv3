﻿using System;

namespace AdamasV3.Models.Dto
{
    public class AccountingProfileDto
    {
        public string AccountingIdentifier { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public string AdmittedBy { get; set; }
        public string BPayRef { get; set; }
        public string BillProfile { get; set; }
        public string BillTo { get; set; }
        public string BillingCycle { get; set; }
        public string BillingMethod { get; set; }
        public string Branch { get; set; }
        public bool? CappedBill { get; set; }
        public bool? CareplanChange { get; set; }
        public bool? CompanyFlag { get; set; }
        public string CreditCardCCV { get; set; }
        public DateTime? CreditCardExpiry { get; set; }

        public string CreditCardName { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardType { get; set; }
        public string CreditCardTypeOther { get; set; }
        public bool? DirectDebit { get; set; }  
        public DateTime? DischargeDate { get; set; }
        public string DischargeBy { get; set; }
        public double? DonationAmount { get; set; }
        public bool? DvaCoBiller { get; set; }
        public bool? EmailInvoice { get; set; }
        public bool? EmailStatement { get; set; }

        public bool? ExcludeFromRosterCopy { get; set; }
        public bool? Fdp { get; set; }
        public string FinancialClass { get; set; }
        public bool? HideTransportFare { get; set; }
        public string InterpreterRequired { get; set; }
        public string MainSupportWorker { get; set; }

        public string NDISNumber { get; set; }
        public string Notes { get; set; }
        public string Occupation { get; set; }
        public string OhsProfile { get; set; }

        public string OrderNo { get; set; }
        public double? PercentageRate { get; set; }

        public bool? PrintInvoice { get; set; }
        public bool? PrintStatement { get; set; }
        public string Recipient_Coordinator { get; set; }

        public bool? Recipient_Split_Bill { get; set; }
        public double? ReportingId { get; set; }

        public int? Terms { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string UniqueID { get; set; }
        public DateTime? Whs { get; set; }
        public string DischargedBy { get; set; }
 
        

    }
}
