import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component,ViewChild, OnInit,HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { GlobalService, ListService,dataSetDropDowns,datasetTypeDropDowns,MenuService,PrintService,timeSteps, NavigationService } from '@services/index';
import { SwitchService } from '@services/switch.service';
import { NzModalService } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recipient-absence',
  templateUrl: './recipient-absence.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  }
  textarea{
    resize:none;
  }
  nz-tabset{
    margin-top:1rem;
  }
  .ant-divider-horizontal.ant-divider-with-text-center, .ant-divider-horizontal.ant-divider-with-text-left, .ant-divider-horizontal.ant-divider-with-text-right {
    margin:1px 0
  }
  nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
    height: 25px !important;
    font-size: 13px !important;
  }
  
  nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
    line-height: 24px;
    height: 25px;
    border-radius:15px 4px 0 0;
    margin:0 -10px 0 0;
  }
  nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
    background: #85B9D5;
    color: #fff;
  }
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  nz-select{
    min-width:100%;
  }
  .ant-modal-content .ant-modal-header .ant-modal-title .ng-star-inserted{
    width: 40% !important;
    margin: auto !important;
    background: #85b9d5 !important;
    text-align: center !important;
    color: white !important;
    border-radius: 5px !important;
    padding: 5px !important;
  }
  .ant-modal-header {
    padding: 4px 24px !important;
  }
  legend + * {
    -webkit-margin-top-collapse: separate;
    margin-top: 10px;
  }
  .ant-tabs-bar{
    margin:0px
  } 
  #main-wrapper{
    border:1px solid #85B9D5;padding:10px 0px;min-height:28rem;
  }
  #mta-btn-group{
  margin-left: 12px !important;margin-right: 12px;padding:10px;
  }
  #mta-btn-group .ant-tabs-bar {
    margin: 0px;
    border: 0px;
  }
  #mta-btn-group nz-tabset[_ngcontent-gwp-c604] {
    margin-top: 0px;
  }
  .redColor{
    color:red
  }
  .whiteColor{
    color:rgba(0, 0, 0, 0);
  }
    tr.highlight {
    background-color: #85B9D5 !important; /* Change to your preferred highlight color */
  }
  `]
  })
  export class RecipientAbsenceComponent implements OnInit {
    
    tableData: Array<any>;//load Main Listing
    branches:Array<any>;
    listStaff:Array<any>;
    selectedStaff:Array<any>;
    paytypes:Array<any>;
    checkedList:string[];//competency 
    competencyList:Array<any>//list competency;
    programz:Array<any>;//populate dropdown
    mtaAlerts:Array<any>;//populate dropdown
    addressTypes:Array<any>;//populate dropdown
    contactTypes:Array<any>;//populate dropdown
    subgroups:Array<any>;//populate dropdown
    status:Array<any>;//populate dropdown
    units:Array<any>;//populate dropdown
    
    mainGroupList: { RECPTABSENCE: string;};
    subGroupList:Array<any>;//populate dropdown
    budgetGroupList:Array<any>;//populate dropdown
    lifeCycleList:Array<any>;//populate dropdown
    diciplineList:Array<any>;//populate dropdown
    ServiceData:Array<any>;
    items:Array<any>;
    jurisdiction:Array<any>;
    loading: boolean = false;
    modalOpen: boolean = false;
    staffApproved: boolean = false;
    staffUnApproved: boolean = false;
    competencymodal: boolean = false;
    check : boolean = false;
    current: number = 0;
    current_mta:number = 0;
    checkedflag:boolean = true;
    dateFormat: string = 'dd/MM/yyyy';
    inputForm: FormGroup;
    modalVariables:any;
    inputVariables:any; 
    postLoading: boolean = false;
    isUpdate: boolean = false;
    title:string = "Add New Recipient Absences";
    tocken: any;
    userRole:string="userrole";
    whereString :string="Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    private unsubscribe: Subject<void> = new Subject();
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
     
    budgetUomList: string[];
    ndiaList: string[];
    datasetList: string[];
    shiftTypes: string[];
    mobileLogModes: { "0": string; "3": string; "1": string; "2": string; };
    timesteps: string[];
    ndiaItems: any;
    emptyList: any[];
    selectedPrograms: any;
    selectedCompetencies: any;
    competencyForm: FormGroup;
    parent_person_id: any;
    addOrEdit: number = 0;
    isNewRecord: any;
    dataSetDropDowns: { CACP: string[]; CTP: string[]; DEX: string[]; DFC: string[]; DVA: any[]; HACC: string[]; HAS: string[]; QCSS: string[]; ICTD: string[]; NDIS: any[]; NRCP: string[]; NRCPSAR: string[]; OTHER: string[]; };
    datasetTypeDropDowns: { CACP: string[]; CTP: string[]; DEX: string[]; DFC: string[]; DVA: any[]; HACC: string[]; HAS: string[]; QCSS: string[]; ICTD: string[]; NDIS: any[]; NRCP: string[]; NRCPSAR: string[]; OTHER: string[]; };
    dataset_group: any[];
    dataset_type:any;
    ndiaItemss: any;
    travelandAlernatelist: any;
    checkList: any;
    chkListForm: any;
    insertOne: number;
    selectedChklst: any;
    chklstmodal: boolean;
    chkList: any;
    activityCode: string;
    operation: number;
    selectedClonedRecord: any;
    clonemodalOpen:boolean = false;
    mergeHistorymodalOpen:boolean=false;
    activeRowData:any;
    selectedRowIndex:number;
    mergeactivitycodesList: Array<any>=[];
    originalList: Array<any>=[];
    txtSearch:string;
    selectedmergecodes: Array<any>=[];
    selectedCode: any;
    HighlightRow2: number;
    mergestatus:boolean=false;
    selectedRows: any[] = [];
    searchTerm: string = '';
    currentIndex: number | null = null;


    constructor(
      private globalS: GlobalService,
      private cd: ChangeDetectorRef,
      private switchS:SwitchService,
      private printS:PrintService,
      private listS:ListService,
      private menuS:MenuService,
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private fb: FormBuilder,
      private navigationService: NavigationService,
      private router: Router,
      private sanitizer: DomSanitizer,
      private ModalS: NzModalService,
    ){}
    
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole             = this.tocken.role;
      this.checkedList          = new Array<string>();
      this.dataSetDropDowns     = dataSetDropDowns;
      this.datasetTypeDropDowns = datasetTypeDropDowns;
      this.loadData();
      this.buildForm();
      this.populateDropdowns();
      this.loading = false;
      this.cd.detectChanges();
    }
    
    showAddModal() {
      this.title = "Add New Recipient Absences"
      this.resetModal();
      this.inputForm.patchValue({
        minorGroup   :'NOT APPLICABLE',
        status       :'ATTRIBUTABLE',
        unit         :'HOUR',
        processClassification:'OUTPUT',
        amount:'0.0000',
        minChargeRate:'0.0000',
        autoActivityNotes:false,
        autoRecipientDetails:false,
        CSTDAUse:false,
        HACCUse:false,
        excludeFromRecipSummarySheet:false,
        NRCPUse:false,
        DeletedRecord:false,
        payAsRostered:false,
        Discountable:false,
        OnSpecial:false,
        ExcludeFromMinHoursCalculation:false,
        minDurtn :0,
        maxDurtn :0,
        fixedTime:0,
        price2:0.0,
        price3:0.0,
        price4:0.0,
        price5:0.0,
        price6:0.0,
      });
      
      this.modalOpen = true;
    }
    logs(value: string[]): void {
      this.selectedCompetencies 
    }
    log(event: any) {
      this.selectedPrograms = event;
    }
    chklog(event: any) {
      this.selectedChklst = event;
    }
    clearPrograms(){
      this.programz.forEach(x => {
        x.checked = false
      });
      this.selectedPrograms = [];
    }
    selectProgram(){
      this.programz.forEach(x => {
        x.checked = true;
        this.selectedPrograms = x.name;
      });
    }
    editCompetencyModal(data:any){
      this.addOrEdit = 1;
      this.competencyForm.patchValue({
        competencyValue : data.competency,
        mandatory : data.mandatory,
        recordNumber:data.recordNumber,
      })
      this.competencymodal = true;
    }
    deleteCompetency(data:any){
      this.loading = true;
      this.menuS.deleteconfigurationservicescompetency(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadCompetency();
          return;
        }
      });
    }
    loadTitle()
    {
      return this.title;
    }
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.title = "Edit Recipient Absences"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      this.inputForm.patchValue(this.tableData[index-1]); 
      this.inputForm.patchValue({
        unit:this.tableData[index-1].billUnit,
        amount:this.tableData[index-1].billAmount,
        ndiaTravel: ((this.tableData[index-1].ndiaTravel == 0) ? false : true),
        tA_LOGINMODE: ((this.tableData[index-1].tA_LOGINMODE == 0) ? false : true),
      })
      this.parent_person_id = this.tableData[index-1].recnum; //set person id for programs and competencies and checklist
    }
    handleCancel() {
      this.current = 0;
      this.resetModal();
      this.modalOpen = false;
    }
    handleCompCancel() {
      this.competencymodal = false;
    }
    handleAprfCancel(){
      this.staffApproved = false;
    }
    handleUnAprfCancel(){
      this.staffUnApproved = false;
    }
    showCompetencyModal(){
      this.addOrEdit = 0;
      this.competencymodal = true;
      this.clearCompetency();
    }
    handleCompetencyCancel(){
      this.addOrEdit = 0;
      this.competencymodal = false;
    }
    pre(): void {
      this.current -= 1;
    }
    
    trueString(data: any): string{
      return data ? '1': '0';
    }
    
    isChecked(data: string): boolean{
      return '1' == data ? true : false;
    }
    onCheckboxChange(option, event) {
      if(event.target.checked){
        console.log(option);
        this.checkedList.push(option.name);
      } else {
        this.checkedList = this.checkedList.filter(m=>m!= option.name)
      }
    }
    onIndexChange(index: number): void {
      this.current = index;
      if(index == 7){
        this.loadCompetency();
      }
      if(index == 8){
        this.loadChecklist();
      }
    }
    view(index: number){
      if(index == 7){
        this.loadCompetency();
      }
      if(index == 8){
        this.loadChecklist();
      }
      this.current = index;
    }
    viewMTA(index: number){
      this.current_mta = index;
    }
    next(): void {
      this.current += 1;
    }
    save() {
      this.loading = true;
      if(!this.isUpdate){


        var ndiaTravel = this.inputForm.get('ndiaTravel').value;
        this.inputForm.controls["ndiaTravel"].setValue(((ndiaTravel == false) ? 0 : 1));

        var tA_LOGINMODE = this.inputForm.get('tA_LOGINMODE').value;
        this.inputForm.controls["tA_LOGINMODE"].setValue(((tA_LOGINMODE == false) ? 0 : -1));


        var recnum = this.inputForm.get('recnum').value;
        this.inputForm.controls["recnum"].setValue(((recnum == null) ? 0 : this.inputForm.get('recnum').value));
        
        // Extract values from the form
        let name        = this.inputForm.get('title').value.trim().toUpperCase();
        
        this.inputForm.controls["title"].setValue(name);

        this.menuS.postRecipientAbsenses(this.inputForm.value)
        .subscribe(data => {
          this.globalS.sToast('Success', 'Added Succesfully');
          this.loading = false;
          this.handleCancel();
          this.loadData()
        });
      }else{var ndiaTravel = this.inputForm.get('ndiaTravel').value;
        this.inputForm.controls["ndiaTravel"].setValue(((ndiaTravel == false) ? 0 : 1));

        var tA_LOGINMODE = this.inputForm.get('tA_LOGINMODE').value;
        this.inputForm.controls["tA_LOGINMODE"].setValue(((tA_LOGINMODE == false) ? 0 : -1));

        this.inputForm.controls["CSTDAUse"].setValue(false);
        this.inputForm.controls["HACCUse"].setValue(false);
        this.inputForm.controls["NRCPUse"].setValue(false);


        this.menuS.updateRecipientAbsenses(this.inputForm.value)
        .subscribe(data => {
          this.globalS.sToast('success','Updated Successfuly');
          this.loading = false;
          this.handleCancel();
          this.loadData();
          
        });
      }
    }
    loadChecklist(){
      this.menuS.getconfigurationserviceschecklist(this.parent_person_id).subscribe(data => {
        this.checkList = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    showChkLstModal(){
      if(this.globalS.isEmpty(this.inputForm.get('title').value))
        {
        this.globalS.iToast('Info','can not create this record beacuse there are blank entries');  
        return;
      }
      this.addOrEdit = 0;
      this.chklstmodal = true;
      this.clearChkList();
    }
    saveCheckList(){
      this.postLoading = true;
      const group = this.chkListForm.value;
      this.insertOne = 0;
      if(this.addOrEdit == 0){
        if(!this.isUpdate){
          if(!this.isNewRecord){
            this.save();
          }
        }
        var checklists = this.selectedChklst;
        checklists.forEach( (element) => {
          this.menuS.postconfigurationserviceschecklist({
            competencyValue:element,
            personID:this.parent_person_id,
          }).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
              this.insertOne = 1;
            })
          });
          
          this.globalS.sToast('Success', 'CheckList Added');
          this.postLoading = false;
          this.loadChecklist();
          this.handleChkLstCancel();
          return false;
        }
        else
        {
          this.menuS.updateconfigurationserviceschecklist({
            competencyValue:group.chkListValue,
            recordNumber:group.recordNumber,
          }).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
              if(data){
                this.globalS.sToast('Success','CheckList Updated')
                this.postLoading = false;
                this.loadChecklist();
                this.handleChkLstCancel();
                return false;
              }
            });
          }
        }
        handleChkLstCancel(){
          this.addOrEdit = 0;
          this.chklstmodal = false;
        }
        clearChkList(){
          this.chkList.forEach(x => {
            x.checked = false
          });
          this.selectedChklst = [];
        }
        editChecklistModal(data:any){
          this.addOrEdit = 1;
          this.chkListForm.patchValue({
            chkListValue : data.checklist,
            recordNumber:data.recordNumber,
          })
          this.chklstmodal = true;
        }
        deleteChecklist(data:any){
          this.loading = true;
          this.menuS.deleteconfigurationserviceschecklist(data.recordNumber)
          .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
              this.globalS.sToast('Success', 'Data Deleted!');
              this.loadChecklist();
              return;
            }
          });
        }
        saveCompetency(){
          this.postLoading = true;
          const group = this.competencyForm.value;
          let insertOne = false;
          if(this.addOrEdit == 0){
            if(!this.isUpdate){
              if(!this.isNewRecord){
                this.save();
              }
            }
            var checkedcompetency = this.selectedCompetencies;
            checkedcompetency.forEach( (element) => {
              this.menuS.postconfigurationservicescompetency({
                competencyValue:element,
                notes:this.competencyForm.value.notes,
                personID:this.parent_person_id,
              }).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                  if(data)
                    {
                    insertOne = true;
                  }
                })
                this.globalS.sToast('Success', 'Competency Added');
                this.loadCompetency();
                this.postLoading = false;
                this.handleCompCancel();
              });
            }
            else
            {
              this.menuS.updateconfigurationservicescompetency({
                competencyValue:group.competencyValue,
                mandatory:group.mandatory,
                recordNumber:group.recordNumber,
              }).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                  if(data){
                    this.globalS.sToast('Success','Competency Updated')
                    this.postLoading = false;
                    this.loadCompetency();
                    this.handleCompCancel();
                    return false;
                  }
                });
              }
            }
            clearCompetency(){
              this.competencyList.forEach(x => {
                x.checked = false
              });
              this.selectedCompetencies = [];
            }
            loadData(){
              this.loading = true;
              this.menuS.GetlistRecipientAbsenses(this.check).subscribe(data => {
                this.tableData = data;
                this.loading = false;
                this.cd.detectChanges();
              });
            }
            loadCompetency(){
              this.menuS.getconfigurationservicescompetency(this.parent_person_id).subscribe(data => {
                this.checkedList = data;
                this.loading = false;
                this.cd.detectChanges();
              });
            }
            fetchAll(e){
              if(e.target.checked){
                this.whereString = "WHERE ProcessClassification <> 'INPUT' ";
                this.loadData();
              }else{
                this.whereString = "WHERE ProcessClassification <> 'INPUT' AND ISNULL(DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE())";
                this.loadData();
              }
            }
            clearStaff(){
              this.listStaff.forEach(x => {
                x.checked = false
              });
              
              this.selectedStaff = [];
            }
            populateDropdowns(): void {
              
              this.emptyList      = [];
              this.mainGroupList  = {"RECPTABSENCE":"RECIPIENT ABSENCE"};
              this.subGroupList   = ['FULL DAY-RESPITE','FULL DAY-HOSPITAL','FULL DAY-TRANSITION','FULL DAY-SOCIAL LEAVE','OTHER','ALTERNATIVE','ALTERNATIVE'];
              this.status         = ['ATTRIBUTABLE','NONATTRIBUTABLE'];
              this.units          = ['HOUR','SERVICE'];
              this.budgetUomList  = ['EACH/SERVICE','HOURS','PLACE','DOLLARS'];
              this.ndiaList       = ['DIRECT SERVICE','PACKAGE ADMIN','CASE MANAGEMENT','GOODS/EQUIPMENT'];
              this.datasetList    = ['CACP','CTP','DEX','DFC','DVA','HACC','HAS','ICTD','NDIS','NRCP','NRCP-SAR','OTHER','QCSS'];
              this.shiftTypes     = ['EXCURSION','MEAL BREAK','SLEEPOVER','TEA BREAK'];
              this.mobileLogModes = {
                "0":'BUTTONS',
                "3":'PIN CODE',
                "1":'QRCODE',
                "2":'SIGNATURE',
              };
              
              let todayDate       = this.globalS.curreentDate();
              
              this.listS.GettravelandAlternateCode().subscribe(data => {
                this.travelandAlernatelist = data;
              })
              this.listS.getndiaitemss().subscribe(data=>{
                this.ndiaItemss = data;
              })
              this.listS.getMergeCodes(4,this.globalS.curreentDate()).subscribe(
                data => {
                  this.mergeactivitycodesList = data;
                  this.originalList=data;
                },
                error => {
                  console.error('Error loading activities', error);
                }
              );
              
              let sql ="SELECT distinct Description from DataDomains Where  Domain = 'LIFECYCLEEVENTS'";
              this.loading = true;
              this.listS.getlist(sql).subscribe(data => {
                this.lifeCycleList = data;
              });
              
              let sql3 ="SELECT distinct Description from DataDomains Where  Domain = 'ADDRESSTYPE'";
              this.loading = true;
              this.listS.getlist(sql3).subscribe(data => {
                this.addressTypes = data;
              });
              let sql4 ="SELECT distinct Description from DataDomains Where  Domain = 'CONTACTTYPE'";
              this.loading = true;
              this.listS.getlist(sql4).subscribe(data => {
                this.contactTypes = data;
              });
              let sql1 ="SELECT distinct Description from DataDomains Where  Domain = 'BUDGETGROUP'";
              this.loading = true;
              this.listS.getlist(sql1).subscribe(data => {
                this.budgetGroupList = data;
              });
              
              let sql2 ="SELECT distinct Description from DataDomains Where  Domain = 'DISCIPLINE'";
              this.loading = true;
              this.listS.getlist(sql2).subscribe(data => {
                this.diciplineList = data;
              });
              
              let comp = "SELECT distinct Description as name from DataDomains Where  Domain = 'STAFFATTRIBUTE' ORDER BY Description";
              this.listS.getlist(comp).subscribe(data => {
                this.competencyList = data;
                this.loading = false;
              });  
              let chk = "SELECT distinct Description as name from DataDomains Where  Domain = 'CHECKLIST' AND ((EndDate IS NULL) OR (EndDate > Getdate())) ORDER BY Description";
              this.listS.getlist(chk).subscribe(data => {
                this.chkList = data;
                this.loading = false;
              });
              let prog = "select distinct Name from HumanResourceTypes WHERE [GROUP]= 'PROGRAMS' AND ((EndDate IS NULL) OR (EndDate > Getdate()))";
              this.listS.getlist(prog).subscribe(data => {
                this.programz = data;
              });
              this.timesteps = timeSteps;
              this.listS.getndiaitems().subscribe(data => {
                this.ndiaItems = data;
              })
              this.mtaAlerts = ['NO ALERT','STAFF CASE MANAGER','RECIPIENT CASE MANAGER','BRANCH ROSTER EMAIL'];
              this.paytypes  = ['SALARY','ALLOWANCE'];
              this.subgroups  = ['NOT APPLICABLE','WORKED HOURS','PAID LEAVE','UNPAID LEAVE','N/C TRAVVEL BETWEEN','CHG TRAVVEL BETWEEN','N/C TRAVVEL WITHIN','CHG TRAVVEL WITHIN','OTHER ALLOWANCE'];
            }
            delete(data: any) {
              this.postLoading = true;     
              const group = this.inputForm;
              this.menuS.deleteActivityServiceslist(data.recnum)
              .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                  this.globalS.sToast('Success', 'Data Deleted!');
                  this.loadData();
                  return;
                }
              });
            }
            buildForm() {
              this.inputForm = this.formBuilder.group({
                ALT_NDIANonLabTravelKmActivity:'',
                ALT_AppKmWithinActivity:'',
                ExcludeFromAppLogging:false,
                dataSet:'',
                datasetGroup:'',
                haccType:'',
                title:'',
                billText:'',
                processClassification:'OUTPUT',
                rosterGroup:'',
                minorGroup:'',
                status:'',
                amount:0.0,
                minChargeRate:'',
                lifecycle:'',
                unit:'',
                budgetGroup:'',
                iT_Dataset:'',
                colorCode:'',
                autoApprove:false,
                excludeFromAutoLeave:false,
                infoOnly:false,
                groupMapping:'',
                NDIA_ID:'',
                ndiA_ID:'',
                accountingIdentifier:'',
                glRevenue:'',
                job:'',
                glCost:'',
                unitCostUOM:'',
                unitCost:'',
                price2:0.0,
                price3:0.0,
                price4:0.0,
                price5:0.0,
                price6:0.0,
                excludeFromPayExport:false,
                excludeFromUsageStatements:false,
                endDate:'',
                excludeFromConflicts:false,
                noMonday   : false,//day1
                noTuesday  : false,//day2
                noWednesday: false,//day3
                noThursday : false,//day4
                noFriday   : false,//day5
                noSaturday : false,//day6
                noSunday   : false,//day7
                noPubHol   : false,//day0
                startTimeLimit:'',
                endTimeLimit:'',
                maxDurtn:0,
                minDurtn:0,
                fixedTime:0,
                noChangeDate:false,
                noChangeTime:false,
                timeChangeLimit:0,
                defaultAddress:'',
                defaultPhone:'',
                autoActivityNotes:false,
                autoRecipientDetails:false,
                jobSheetPrompt:false,
                activityNotes:'',
                excludeFromHigherPayCalculation:false,
                noOvertimeAccumulation:false,
                payAsRostered:false,
                excludeFromTimebands:false,
                excludeFromInterpretation:false,
                jobType:'',
                mtacode:'',
                tA_LOGINMODE:'',
                excludeFromClientPortalDisplay: false,
                excludeFromTravelCalc: false,
                tA_EXCLUDEGEOLOCATION:false,
                appExclude1:false,
                taexclude1:false,
                taEarlyStartTHEmail:false,
                taLateStartTHEmail:false,
                taEarlyStartTH:'',
                taLateStartTH:'',
                taEarlyStartTHWho:'',
                taLateStartTHWho:'',
                taNoGoResend:'',
                taNoShowResend:'',
                taEarlyFinishTHEmail:false,
                taLateFinishTHEmail:false,
                taEarlyFinishTH:'',
                taLateFinishTH:'',
                taLateFinishTHWho:'',
                taEarlyFinishTHWho:'',
                taOverstayTHEmail:false,
                taUnderstayTHEmail:false,
                taNoWorkTHEmail:false,
                taOverstayTH:'',
                taUnderstayTH:'',
                taNoWorkTH:'',
                taUnderstayTHWho:'',
                taOverstayTHWho:'',
                taNoWorkTHWho:'',
                HACCUse:false,
                CSTDAUse:false,
                NRCPUse:false,
                ndiaClaimType:"",
                ndiaPriceType:"",
                ndiaTravel:false,
                DeletedRecord:false,
                excludeFromRecipSummarySheet:false,
                ExcludeFromMinHoursCalculation:false,
                OnSpecial:false,
                Discountable:false,
                ndiA_LEVEL2:'',
                ndiA_LEVEL3:'',
                ndiA_LEVEL4:'',
                ALT_PHActivityCode:'',
                PHAction:'',
                recnum:0,
              });
              this.competencyForm = this.formBuilder.group({
                competencyValue: '',
                mandatory: false,
                notes: '',
                personID: this.parent_person_id,
                recordNumber: 0
              });
              this.chkListForm = this.formBuilder.group({
                chkListValue: '',
                mandatory: false,
                notes: '',
                personID: this.parent_person_id,
                recordNumber: 0
              })
              // Subscribe to changes in iT_Dataset
              
              this.inputForm.get('iT_Dataset').valueChanges.subscribe(x => {
                this.dataset_group = [];
                this.dataset_type = [];
                
                
                // Check if the selected value is 'NRCP-SAR' and set dataset_group accordingly

                if (x === 'NRCP-SAR') {
                    this.dataset_group = this.dataSetDropDowns['NRCPSAR'];
                } else {
                    this.dataset_group = this.dataSetDropDowns[x];
                }

                // Check if the value of iT_Dataset is "OTHER", "CACP", or "DFC"
                if (['OTHER', 'CACP', 'DFC'].includes(x)) {
                    // Set dataset_type to the value of the "title" form control
                    this.dataset_type.push(this.inputForm.get('title').value);
                } else {
                    // Otherwise, use determineTypeList function to set the appropriate dropdown value
                    const groupField = this.inputForm.get('datasetGroup').value;
                    const prodDesc = 'NSW';
                    this.dataset_type = this.determineTypeList(x, groupField, prodDesc);
                }

              });

              // Subscribe to changes in datasetGroup
              this.inputForm.get('datasetGroup').valueChanges.subscribe(groupField => {
                const iT_Dataset = this.inputForm.get('iT_Dataset').value;
                const prodDesc = 'NSW';
                // Ensure dataset_type is updated when datasetGroup changes
                this.dataset_type = this.determineTypeList(iT_Dataset, groupField, prodDesc);
              });


            
            }
          
          // Helper function (from the previous code)
          determineTypeList(datasetField, groupField, prodDesc) {
            const typeList = [];
            
            switch (datasetField) {
                case "OTHER":
                case "CACP":
                case "DFC":
                    typeList.push(this.inputForm.get('title').value);
                    break;
                case "HACC":
                case "CTP":
                case "ICTD":
                case "QCSS":
                    if (groupField === "TRANSPORT" || groupField === "CARER TRANSPORT") {
                        if (prodDesc === "NSW") {
                            typeList.push(
                                "IPB-Individual Private/STA Bus",
                                "IAB-Individual Service Bus",
                                "IOB-Individual Other Bus",
                                "IAC-Individual Service Car",
                                "IOC-Individual Other Car",
                                "ITX-Individual Taxi",
                                "GPB-Group Private/STA Bus",
                                "GAB-Group Service Bus",
                                "GOB-Group Other Bus",
                                "GAC-Group Service Car",
                                "GOC-Group Other Car",
                                "GTX-Group Taxi"
                            );
                        } else {
                            typeList.push(groupField);
                        }
                    } else if (groupField === "ALLIED HEALTH CARE") {
                        if (prodDesc === "NSW") {
                            typeList.push(
                                "1-Dietician/Nutritionist Intervention",
                                "2-Occupational Therapy Intervention",
                                "3-Physiotherapy Intervention",
                                "4-Podiatry Intervention",
                                "5-Speech Therapy Intervention",
                                "98-Other Intervention",
                                "99-Not Specified Intervention"
                            );
                        } else {
                            typeList.push(groupField);
                        }
                    } else {
                        typeList.push(groupField);
                    }
                    break;
        
                case "DEX":
                    switch (groupField) {
                        case "Assistance with Care and Housing":
                            typeList.push(
                                "Advocacy - Financial and Legal",
                                "Assessment - Referrals",
                                "Hoarding and Squalor",
                                "Utility Bill Assistance"
                            );
                            break;
                        case "Centre-Based Respite":
                            typeList.push(
                                "Centre-Based Day Respite",
                                "Community Access - Group",
                                "Residential Day Respite"
                            );
                            break;
                        case "Carers, Disability and Mental Health":
                            typeList.push(
                                "Intake/assessment",
                                "Information/advice/referral",
                                "Education and Skills training",
                                "Child/Youth focussed groups",
                                "Counselling",
                                "Advocacy/Support",
                                "Community Capacity building",
                                "Outreach",
                                "Mentoring/Peer Support",
                                "Family Capacity Building",
                                "Facilitate Employment Pathways",
                                "NDIS access",
                                "NDIS eligible",
                                "NDIS ineligible",
                                "Respite",
                                "Carer Support",
                                "Brokered respite",
                                "Brokered carer support",
                                "Community Engagement",
                                "Community sector coordination",
                                "Community sector planning",
                                "Indigenous community engagement",
                                "Indigenous community participation"
                            );
                            break;
                        case "Cottage Respite":
                            typeList.push("Overnight Community Respite");
                            break;
                        case "Emergency Relief Funds":
                            typeList.push(
                                "Food Parcels & Food Vouchers",
                                "Material Goods",
                                "Health Care Assistance",
                                "Transport Assistance",
                                "Utility Bill Assistance",
                                "Rent and Mortgage",
                                "Accommodation assistance"
                            );
                            break;
                        case "Flexible Respite":
                            typeList.push(
                                "Community Access - Individual Respite",
                                "Host Family Day Respite",
                                "Host Family Overnight Respite",
                                "In-Home Day Respite",
                                "In-Home Overnight Respite",
                                "Mobile Respite",
                                "Other Planned Respite"
                            );
                            break;
                        case "Allied Health and Therapy Services":
                            typeList.push(
                                "ATSI Health Worker",
                                "Dietitian or Nutritionist",
                                "Diversional Therapy",
                                "Exercise Physiologist",
                                "Occupational Therapy",
                                "Ongoing Allied Health and Therapy Services",
                                "Other Allied Health and Therapy Services",
                                "Hydrotherapy",
                                "Physiotherapy",
                                "Podiatry",
                                "Psychologist",
                                "Restorative Care Services",
                                "Social Work",
                                "Speech Pathology"
                            );
                            break;
                        case "Domestic Assistance":
                            typeList.push("General House Cleaning", "Linen Services", "Unaccompanied Shopping");
                            break;
                        case "Goods, Equipment and Assistive Technology":
                            typeList.push(
                                "Car Modifications",
                                "Communication Aids",
                                "Medical Care Aids",
                                "Other Goods And Equipment",
                                "Reading Aids",
                                "Self-Care Aids",
                                "Support and Mobility Aids"
                            );
                            break;
                        case "Home Maintenance":
                            typeList.push(
                                "Garden Maintenance",
                                "Major Home Maintenance and Repairs",
                                "Minor Home Maintenance and Repairs"
                            );
                            break;
                        case "Home Modifications":
                            typeList.push("Home Modifications");
                            break;
                        case "Meals":
                            typeList.push("Meals at Centre", "Meals at Home");
                            break;
                        case "Nursing":
                            typeList.push("Nursing");
                            break;
                        case "Other Food Services":
                            typeList.push(
                                "Food Advice, Lessons, Training and Food Safety",
                                "Food Preparation in the Home"
                            );
                            break;
                        case "Personal Care":
                            typeList.push(
                                "Assistance with Client Self-Administration of Medicine",
                                "Assistance with Self-Care"
                            );
                            break;
                        case "Social Support Group":
                            typeList.push("Social Support Group");
                            break;
                        case "Social Support Individual":
                            typeList.push("Accompanied Activities", "Telephone/Web Contact", "Visiting");
                            break;
                        case "Specialised Support Services":
                            typeList.push(
                                "Client Advocacy",
                                "Continence Advisory Service",
                                "Dementia Advisory Service",
                                "Hearing Services",
                                "Other Support Services",
                                "Vision Services",
                                "Carer Support"
                            );
                            break;
                        case "Transport":
                            typeList.push("Direct Transport", "Indirect Transport");
                            break;
                        default:
                            typeList.push(groupField);
                    }
                    break;
        
                case "CSTDA":
                    switch (prodDesc) {
                        case "WA":
                            switch (groupField.toUpperCase()) {
                                case "ACCOMMODATION SUPPORT":
                                    typeList.push(
                                        "1.01-Large residential/institution (>20 people) - 24 hour care",
                                        "1.02-Small residential/institution (7-20 people) - 24 hour care",
                                        "1.03-Hostels - generally not 24 hour care",
                                        "1.041-Group homes (<7 people): group home",
                                        "1.042-Group homes (<7 people): shared arrangements",
                                        "1.05-Attendant care/personal care",
                                        "1.061-In-home accomm support (not living in family home -ASF)",
                                        "1.062-In-home accomm support (living in family home - IFS)",
                                        "1.063-In-home accomm support (community living)",
                                        "1.07-Alternative family placement",
                                        "1.08-Other accommodation support"
                                    );
                                    break;
                                case "COMMUNITY SUPPORT":
                                    typeList.push(
                                        "2.011-Comprehensive School Age Therapy",
                                        "2.012-Comprehensive School Age Therapy (place based funding)",
                                        "2.013-Comprehensive Adult Therapy",
                                        "2.014-Equipment / Technology Consultancy (not CAEP)",
                                        "2.021-Early childhood intervention",
                                        "2.022-Early childhood intervention - Autism only",
                                        "2.03-Behaviour/specialist intervention",
                                        "2.041-Counselling - individual/family/group (not OOHC)",
                                        "2.042-Counselling - individual/family/group (OOHC only)",
                                        "2.05-Regional resource and support teams",
                                        "2.06-Case management, local coordination and development",
                                        "2.07-Other community support"
                                    );
                                    break;
                                case "COMMUNITY ACCESS":
                                    typeList.push(
                                        "3.01-Learning and life skills development (day programs)",
                                        "3.02-Recreation/holiday programs",
                                        "3.03-Other community access"
                                    );
                                    break;
                                case "RESPITE":
                                    typeList.push(
                                        "4.011-Own home respite (not OOHC)",
                                        "4.012-Own home respite (OOHC only)",
                                        "4.02-Centre based respite/respite homes",
                                        "4.03-Host family respite/peer support respite",
                                        "4.041-Other flexible respite (not OOHC)",
                                        "4.042-Other flexible respite (OOHC only)",
                                        "4.05-Other respite"
                                    );
                                    break;
                                default:
                                    typeList.push(groupField);
                            }
                            break;
        
                        case "QLD":
                            switch (groupField.toUpperCase()) {
                                case "ACCOMMODATION SUPPORT":
                                    typeList.push(
                                        "1.04-Group homes (<7 people)",
                                        "1.05-Attendant care/personal care",
                                        "1.06-In-home accommodation support",
                                        "1.07-Alternative family placement",
                                        "1.08-Other accommodation support"
                                    );
                                    break;
                                case "COMMUNITY SUPPORT":
                                    typeList.push(
                                        "2.01-Therapy support for individuals",
                                        "2.02-Early childhood intervention",
                                        "2.03-Behaviour/specialist intervention",
                                        "2.04-Counselling (individual/family/group)",
                                        "2.05-Regional resource and support teams",
                                        "2.06-Case management, local coordination and development",
                                        "2.07-Other community support"
                                    );
                                    break;
                                case "COMMUNITY ACCESS":
                                    typeList.push(
                                        "3.01-Learning and life skills development",
                                        "3.02-Recreation/holiday programs",
                                        "3.03-Other community access"
                                    );
                                    break;
                                case "RESPITE":
                                    typeList.push(
                                        "4.01-Own home respite",
                                        "4.02-Centre based respite/respite homes",
                                        "4.03-Host family/peer support respite",
                                        "4.04-Other flexible respite",
                                        "4.05-Other respite"
                                    );
                                    break;
                                default:
                                    typeList.push(groupField);
                            }
                            break;
        
                        default:
                            typeList.push(groupField);
                    }
                    break;
        
                case "HAS":
                    switch (groupField) {
                        case "HOME MAINTENANCE":
                            typeList.push(
                                "Hand-held shower device",
                                "Handyperson work",
                                "Lawns",
                                "Minor Modifications",
                                "Qualified Trade Work",
                                "Security Assessment",
                                "Smoke Alarms",
                                "Security Work",
                                "Yard and outside"
                            );
                            break;
                        case "GENERAL SERVICES":
                            typeList.push(
                                "First Interview",
                                "Information & Referral",
                                "Help Employing Trade",
                                "Standard General Ins"
                            );
                            break;
                        default:
                            typeList.push(groupField);
                    }
                    break;
        
                case "NRCP":
                    typeList.push(
                        "Commonwealth approved aged care homes residential respite",
                        "State/Territory funded disability care homes residential respite",
                        "Community residential respite",
                        "Other residential respite",
                        "Community respite",
                        "In-home respite",
                        "Individualised respite",
                        "Indirect respite"
                    );
                    break;
        
                case "NRCP-SAR":
                    typeList.push(
                        "Day Care - Centre based",
                        "Daytime Respite - In home",
                        "Overnight Respite - In home",
                        "Community Access - Individual",
                        "Community Access - Group",
                        "Host Family - Overnight",
                        "Host Family - Day",
                        "Overnight Community Respite Houses",
                        "Overnight - Other",
                        "Referral to Other Respite Services",
                        "Referral to Non Respite Services"
                    );
                    break;
        
                case "VHC":
                    typeList.push("DA", "PC", "HG", "HT", "RI", "RE", "SA");
                    break;
        
                default:
                    typeList.push(groupField);
            }
        
            return typeList;
          }
            handleOkTop() {
              this.generatePdf();
              this.tryDoctype = ""
              this.pdfTitle = ""
            }
            handleCancelTop(): void {
              this.drawerVisible = false;
              this.pdfTitle = ""
            }
            generatePdf(){
              this.drawerVisible = true;
              
              this.loading = true;
              
              var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY [Title]) AS Field1,[Title] As [Field2], CASE WHEN RosterGroup = 'ONEONONE' THEN 'ONE ON ONE' WHEN RosterGroup = 'CENTREBASED' THEN 'CENTER BASED ACTIVITY' WHEN RosterGroup = 'GROUPACTIVITY' THEN 'GROUP ACTIVITY' WHEN RosterGroup = 'TRANSPORT' THEN 'TRANSPORT' WHEN RosterGroup = 'SLEEPOVER' THEN 'SLEEPOVER' WHEN RosterGroup = 'TRAVELTIME' THEN 'TRAVEL TIME' WHEN RosterGroup = 'ADMISSION' THEN 'RECIPIENT ADMINISTRATION' WHEN RosterGroup = 'RECPTABSENCE' THEN 'RECIPIENT ABSENCE' WHEN RosterGroup = 'ADMINISTRATION' THEN 'STAFF ADMINISTRATION' ELSE RosterGroup END As [Field3],[MinorGroup] As [Field4],[HACCType] As [Field5],[DatasetGroup] As [Field6],  [NDIA_ID] As [Field7],[Amount] As [Field8],[Unit] As [Field9] FROM ItemTypes WHERE ProcessClassification <> 'INPUT' AND (EndDate Is Null OR EndDate >= '12-22-2020')  AND (RosterGroup IN ('ITEM')) ORDER BY Title";
              
              const data = {
                "template": { "_id": "0RYYxAkMCftBE9jc" },
                "options": {
                  "reports": { "save": false },
                  "txtTitle": "Recipient Absences List",
                  "sql": fQuery,
                  "userid":this.tocken.user,
                  "head1" : "Sr#",
                  "head2" : "Title",
                  "head3" : "Roaster Group",
                  "head4" : "Sub Group",
                  "head5" : "DataSet Code",
                  "head6" : "DataSet Group",
                  "head7" : "NDIA ID",
                  "head8" : "Bill Amount",
                  "head9" : "Bill Unit",
                }
              }
              this.printS.printControl(data).subscribe((blob: any) => { 
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.pdfTitle = "Recipient Absences.pdf";
                this.cd.detectChanges();
              }, err => {
                this.loading = false;
                this.ModalS.error({
                  nzTitle: 'TRACCS',
                  nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                  nzOnOk: () => {
                    this.drawerVisible = false;
                  },
                });
              });
              this.loading = true;
              this.tryDoctype = "";
              
            }
            
            clonehandleCancel(){
              this.clonemodalOpen = false;
            }
            showActivityMOdal(item: any,operation:number){
              this.clonemodalOpen = true;
              this.operation = operation;
              this.selectedClonedRecord = item;
            }
            
            clone(){
              if(this.operation == 1){
                this.menuS.postActivityClone(this.selectedClonedRecord.recnum,this.activityCode).subscribe(
                  response => {
                    this.loading = false;
                    this.globalS.sToast('Success', 'cloned successfully!');
                    this.loadData();
                    this.clonemodalOpen = false;
                    this.activityCode   = '';
                    
                  },
                  error => {
                    this.loading = false;
                    
                    this.cd.detectChanges();
                    this.globalS.eToast('ERROR', 'SomeThing Went Wring Try Again!');
                  }
                );
              }
            }
            
            
            // fileName: string = 'TraccsProgramPackagesExport.csv'; // Default file name
            
            // export(): void {
            //   if (this.fileInput) {
            //     this.fileInput.nativeElement.setAttribute('nwsaveas', 'TraccsProgramPackagesExport.csv');
            //     this.fileInput.nativeElement.click();
            //   }
            // }
            
            // onFileSelected(event: Event): void {
            //   const input = event.target as HTMLInputElement;
            //   if (input.files && input.files.length > 0) {
            //     const file = input.files[0];
            //     const csvContent = this.generateCSVContent();
            //     const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
            
            //     let fullFileName = file.name.trim(); // Use the selected file's name
            
            //     // Save the file with the selected filename
            //     saveAs(blob, fullFileName);
            //   }
            // }
            
            // generateCSVContent(): string {
            //   const data = [
            //     ['Name', 'Age', 'Country'],
            //     ['John Doe', '30', 'USA'],
            //     ['Jane Smith', '25', 'Canada'],
            //     ['Sam Brown', '22', 'UK']
            //   ];
            //   return data.map(row => row.join(',')).join('\n');
            // }
            
            fileName: string = 'TraccsRecipientAbsenceExport.csv'; // Default file name
            
            export(): void {
              const csvContent = this.generateCSVContent();
              const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
              
              // Create a temporary anchor element
              const a = document.createElement('a');
              a.href = URL.createObjectURL(blob);
              a.download = this.fileName;
              
              // Append anchor to body to trigger download
              document.body.appendChild(a);
              
              // Programmatically trigger a click on the anchor element
              a.click();
              
              // Clean up: remove anchor and revoke object URL
              document.body.removeChild(a);
              URL.revokeObjectURL(a.href);
            }
            
            generateCSVContent(): string {
              
              
              const data = this.convertResponseToOutput(this.tableData);
              return data.map(row => row.join(',')).join('\n');
              
            }
            convertResponseToOutput(response: any[]): any[] {
              
              
              
              const headers = ['RecNum','Title', 'Roster Group', 'Sub Group', 'DataSet', 'DataSet Code', 'Outlet ID', 'DataSet Group', 'NDIA ID', 'Accounting Code', 'Bill Amount', 'Bill Unit'];
              
              // Map each record to an array of values in the specified order
              const dataRows = response.map(record => [
                record.recnum,
                record.title,
                record.rosterGroupD,
                record.minorGroup,
                record.dataSet,
                record.datasetCode,
                record.outletID,
                record.datasetGroup,
                record.ndiaid,
                record.accountingCode,
                record.billAmount,
                record.billUnit,
              ]);
              
              // Return an array that starts with headers followed by data rows
              return [headers, ...dataRows];
              
            }
            selectCode(){
              this.mergeHistorymodalOpen = true;
            }

            onTextChangeEvent(event:any){
              let value = this.txtSearch.toUpperCase();
              this.mergeactivitycodesList=this.originalList.filter(element=>element.title.includes(value));
            }
            
            selectedItemGroup(data:any, i:number){
              this.selectedRowIndex=i;
              this.activeRowData=data;
            }
            
            RecordClicked(event: any, value: any, i: number) {
              
              this.selectedRowIndex =i;
              
              if (event.ctrlKey || event.shiftKey) {
                this.selectedmergecodes.push(value)
              } else {
                this.selectedmergecodes = [];
                if (this.selectedmergecodes.length <= 0) {
                  this.selectedmergecodes.push(value)
                }
               }
            }
            onItemSelected(sel: any, i:number): void {
              this.selectedCode=sel;    
              this.HighlightRow2=i;
            }
            onItemDbClick(sel:any , i:number) : void {
              this.HighlightRow2=i;
              this.selectedCode=sel;
              this.mergehistories();
            }
            
            mergehistories(){
              
              const audit = {
                Operator: this.tocken.user,
                ActionDate: this.globalS.getCurrentDateTime(),
                AuditDescription: '',
                ActionOn: 'ITEMTYPES',
                WhoWhatCode: '',
                TraccsUser: this.tocken.user,
              };
              
              let newServiceType: string;  // Declare newServiceType outside the if block
              if (this.selectedmergecodes.length > 0) {
                newServiceType = this.selectedmergecodes[0].title;
              }
              
              
              const existingServiceTypes = Array.from(new Set(this.selectedRows.map(row => row.title)));
              
              this.ModalS.confirm({
                nzTitle: 'Do You Want to Merge?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                  this.menuS.mergeHistories(newServiceType,{
                    audit:audit,
                    existingServiceTypes:existingServiceTypes
                  }).subscribe(response => {
                      console.log('Merge successful', response);
                      this.mergeHistorymodalOpen = false;
                      this.loadData();  // Assuming this method reloads data after canceling
                    }, error => {
                      console.error('Merge failed', error);
                    });
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                  this.mergeHistorymodalOpen = false;
                  this.loadData();  // Assuming this method reloads data after canceling
                }
              });  
            }


            isSelected(data: any): boolean {
              return this.selectedRows.includes(data);
            }
            
            onRowClick(event: MouseEvent, data: any): void {
              if (event.ctrlKey) {
                if (this.isSelected(data)) {
                  this.selectedRows = this.selectedRows.filter(row => row !== data);
                } else {
                  this.selectedRows.push(data);
                }
              } else {
                this.selectedRows = [data];
              }
              this.mergestatus = this.selectedRows.length > 1 ? true : false;
            }
            
            goBack(): void {
              const previousUrl = this.navigationService.getPreviousUrl();
              const previousTabIndex = this.navigationService.getPreviousTabIndex();
            
              if (previousUrl) {
                this.router.navigate(['/admin/configuration'], {
                  queryParams: { tab: previousTabIndex } // Pass tab index in query params
                });
              } else {
                this.router.navigate(['/admin/configuration'], {
                  queryParams: { tab: previousTabIndex } // Pass tab index in query params
                });
              }
            }
            @HostListener('document:keydown', ['$event'])
            handleKeyboardEvent(event: KeyboardEvent) {
              // Handle character input
              if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
                  this.searchTerm += event.key.toLowerCase();
                  this.scrollToRow();
              } 
              // Handle backspace
              else if (event.key === 'Backspace') {
                  // Only reset if there's something to remove
                  this.searchTerm = '';
                  if (this.searchTerm.length > 0) {
                      this.searchTerm = this.searchTerm.slice(0, -1);
                      this.scrollToRow();
                  }
              }
              // Handle other keys (optional)
              else if (event.key === 'Escape') {
                  // Reset search term on escape key
                  this.searchTerm = '';
                  this.scrollToRow();
              }
            }
        
            scrollToRow() {
                // Find the index of the first matching item
                const matchIndex = this.tableData.findIndex(item => 
                    item.title.toLowerCase().startsWith(this.searchTerm)
                );
                // Scroll to the matching row if it exists
                if (matchIndex !== -1) {
                    const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
                    if (tableRow) {
                        tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                        this.highlightRow(tableRow);
                    }
                } else {
                }
            }
        
            highlightRow(row: HTMLElement) {
                row.classList.add('highlight');
                setTimeout(() => row.classList.remove('highlight'), 2000);
            }
            
          }

