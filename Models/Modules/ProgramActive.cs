
namespace Adamas.Models.Modules{
    public class ProgramActive {
        public string Code { get; set; }
        public bool IsActive { get; set; }
    }
}