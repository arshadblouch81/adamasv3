﻿using System;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Data;
using System.Data.Common;
//using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;

using ProgramEntities = Adamas.Models.Modules.Program;
using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using System.Collections;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using System.Drawing.Imaging;
using AdamasV3.Models.Dto;
using Microsoft.Data.SqlClient;


namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class MapsController : Controller
    {
        private readonly IHttpClientFactory _clientFactory;

        private readonly IConfiguration _config;

        public MapsController(
            IConfiguration config,
            IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            _config = config;
        }

        [HttpGet("GetMapDistance")]
        public async Task<IActionResult> GetMapDistance()
        {
            var url = $"https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Lahore&origins=Faisalabad&units=imperial&key={this._config["GoogleConfiguration:MapApiKey"]}";
            var request = new HttpRequestMessage(HttpMethod.Get, url);


            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var serializer = new JsonSerializer();

                var ms = await response.Content.ReadAsStreamAsync();

                using (var sr = new StreamReader(ms))
                using (var jsonTextReader = new JsonTextReader(sr))
                {
                    return Ok(serializer.Deserialize(jsonTextReader));
                }
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpGet("GetDistanceMatrix")]
        public async Task<IActionResult> GetDistanceMatrix(MapDetailsDto map)
        {
            try
            {
                // var url = $"maps.googleapis.com/maps/api/distancematrix/json?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false&key={this._config["GoogleConfiguration:MapApiKey"]}";
                // var http = "https://";

                var url = "";
                var http = "";

                if (map.TravelProvider == "GOOGLE") //CASE GOOGLE
                {
                    if (map.GoogleCustID == "KEY")
                    {
                        http = "https://";
                        url = $"maps.googleapis.com/maps/api/distancematrix/json?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false&key={map.MapApiKey}";
                        // url = $"maps.googleapis.com/maps/api/distancematrix/json?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false&key={this._config["GoogleConfiguration:MapApiKey"]}";
                    }
                    else
                    {
                        // http = "http://";
                        // url = $"maps.googleapis.com/maps/api/distancematrix/json?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false&client={map.GoogleCustID}&signature=";
                        http = "https://";
                        url = $"maps.googleapis.com/maps/api/distancematrix/json?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false&key={map.MapApiKey}";
                        // url = $"maps.googleapis.com/maps/api/distancematrix/json?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false&key={this._config["GoogleConfiguration:MapApiKey"]}";
                    }
                } else { //CASE BING
                        http = "http://";
                        url = $"maps.googleapis.com/maps/api/distancematrix/xml?origins={map.OriginDestination}&destinations={map.FinalDestination}&mode=driving&sensor=false";
                }

                var request = new HttpRequestMessage(HttpMethod.Get, String.Concat(http, url));

                var client = _clientFactory.CreateClient();

                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    var serializer = new JsonSerializer();
                    var ms = await response.Content.ReadAsStreamAsync();

                    using (var sr = new StreamReader(ms))
                    using (var jsonTextReader = new JsonTextReader(sr))
                    {
                        return Ok(serializer.Deserialize(jsonTextReader));
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
