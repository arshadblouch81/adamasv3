using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using eg_03_csharp_auth_code_grant_core.Models;
using Microsoft.AspNetCore.Authorization;
using eg_03_csharp_auth_code_grant_core.Common;
using System.Linq;

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Adamas.Models;

namespace eg_03_csharp_auth_code_grant_core.Controllers
{
    public class HomeController : Controller
    {
        private readonly DatabaseContext _context;
        public HomeController(DatabaseContext context)
        {
            _context = context;
        }

        [Route("/dsReturn")]
        public IActionResult DsReturn(string state, string @event, string envelopeId)
        {
            // var dbDoc = (from docs in _context.Documents
            //     where docs.DocusignEnvelopeId  == envelopeId select docs).FirstOrDefault();

            // dbDoc.DocusignStatus = @event;
            // _context.SaveChanges();
            
            // ViewBag.title = "Return from DocuSign";
            // ViewBag._event = @event;
            // ViewBag.state = state;
            // ViewBag.envelopeId = envelopeId;

            return View();
        }

    }

    public static class SessionExtensions
    {
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
