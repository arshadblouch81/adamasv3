using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using Adamas.DAL;
using Microsoft.Data.SqlClient;
using Microsoft.AspNetCore.Http;
using Adamas.Models.Modules;
using System.IO;

namespace Repositories
{
    public interface IUploadRepository
    {
        Task<Boolean> InsertFileToDatabase(string PERSONID, IFormFile file, string user = null);
        Task<Boolean> InsertFileToFileSystemAsync(string PERSONID, IFormFile file, string directory);
        Task<Boolean> DeleteFileToDatabase(string PERSONID, FileForm file);
        Task<Boolean> DeleteFileToFileSystemAsync(FileForm file);
        Task<(MemoryStream, string)> DownloadFile(GetDocument doc);
        Task<String> GetUploadDownloadDirectoryAsync(string user = null);
        Task<List<dynamic>> GetFileFromDatabase(string name, string view, string user);
        Task<string> GetFileDirectoryAsync(string devPath, string prodPath = null);
        Task<String> GetRecipientDocFolderViaUserInfo(string uname);
        string FilterFileNameAnomalies(string fileName);
        Task<Boolean> InsertDocuments(string personID, string user, IFormFileCollection files);
        Task<String> GetUploadRecipientDirectory(string personid);
        Task<String> InsertFileToFileSystemAsyncReturnFileNameString(string PERSONID, IFormFile file, string directory);
        Task<Boolean> DeleteFileByNameToFileSystemAsync(string file, string directory);
        Task<(MemoryStream, string)> DownloadFileInDocumentDirectory(GetDocument doc, string pathString);
        Task<Boolean> InsertIncidentDocument(string PERSONID, GetDocument doc, IFormFile file);
        Task<string> GetSharedDocumentRegistration();
        Task<(MemoryStream, string, string)?> DownloadFileInDocumentDirectory(string docFilePath, string folderName = "");
        Task<(MemoryStream, string)?> DownloadFileFromMemory(byte[] fileByteArr, string mimeType);
        Task<Boolean> DeleteFileByNameToFileSystemAsync(string filePath);
        Task<String> GetUploadRecipientDirectoryByName(string name);
        MemoryStream DownloadDocumentPortalClient(CopyAndPasteFile doc);
        Task<bool> UploadDocumentPortalClient(CopyAndPasteFile doc, IFormFile file);
    }
}