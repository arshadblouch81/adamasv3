import { Component, OnInit } from '@angular/core';

import  { ClientService, GlobalService, TimeSheetService, ReportService } from '../../services/index';
import * as moment from 'moment';
@Component({
    templateUrl:'./attendance.html'
})

// comment

export class AttendanceAdmin implements OnInit {
    source: any = null;

    constructor(
        private clientS: ClientService,
        private reportS: ReportService
    ){

    }

    ngOnInit(){
        this.reportS.getreferrallist().subscribe(data => {
            this.source = data.map(data => {
                return {
                    Name: data.surname + ' ' + data.firstname,
                    ActivationDate: moment(data.activationDate).format('DD/MM/YYYY'),
                    Oni: data.oniRating,
                    Phone: data.phone,
                    Address: data.address,
                    LastService: data.lastService
                }
            });
        })

        // let data: Dto.GetPackage = { 
        //     Code: 'TRUMP D', 
        //     PCode: 'L2 - TRUMP D', 
        //     Date: '2018-07-31'
        // }

        // this.clientS.getpackages(data)
        // .subscribe(data => {
        //     this.source = data.list;
        // });        
    }

    toPrint(){
        
    }
}