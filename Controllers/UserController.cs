﻿using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

using AdamasV3.Models.Dto;
using Microsoft.AspNetCore.Authorization;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly DatabaseContext _context;
        private readonly ILoginService _loginS;
        public UserController(
            DatabaseContext context,
            ILoginService loginS
            ) {
            _context = context;
            _loginS = loginS;
        }

        [HttpGet("GetClientPortalView")]
        public async Task<IActionResult> GetClientPortalView()
        {
            var currentUser = await _loginS.GetCurrentUser();

            var user = _context.UserInfo.Where(x => x.Name == currentUser.User).Select(x => new UserInfo() { 
                ViewClientPortalDocuments = x.ViewClientPortalDocuments,
                ClientPortalViewNotes = x.ClientPortalViewNotes
            }).FirstOrDefault();

            var vcpDocuments = (user.ViewClientPortalDocuments == null || user.ViewClientPortalDocuments == false) ? false : true;
            var cpvNotes = user.ClientPortalViewNotes == null ? "00000" : user.ClientPortalViewNotes;


            return Ok(new ViewClientPortalDto {
                ShowDocuments       = vcpDocuments,
                ShowOPNote          = cpvNotes.Substring(0, 1) == "1" ? true : false,
                ShowCaseNote        = cpvNotes.Substring(1, 1) == "1" ? true : false,
                ShowServiceNote     = cpvNotes.Substring(2, 1) == "1" ? true : false,
                ShowIncidentNote    = cpvNotes.Substring(3, 1) == "1" ? true : false,
                ShowClinicalNote    = cpvNotes.Substring(4, 1) == "1" ? true : false,
                ShowNotesTab        = cpvNotes.Substring(0, 1) == "1" || cpvNotes.Substring(1, 1) == "1" ? true : false
            });
        }
      
    }
}
