import { Component, Input, Output, EventEmitter, SimpleChanges,AfterViewInit,ChangeDetectorRef } from '@angular/core'

@Component({
    selector: 'paginate',
    templateUrl: './paginate.html',
    styles: [
        `
        .paginate-container{
            display:flex;
            padding:0 10px;
            align-items:center;
        }
        .paginate-pageNo{
            flex: 1;
            height:40px;
            line-height:40px;
        }
        .paginate-controls{
            flex: 2;
            text-align:right;
        }
        ul{
            list-style:none;
        }
        ul li{
            display: inline-block;
            vertical-align: middle;
            text-align: center;
            height: 25px;
            margin: 0 5px;
            width: 20px;
            font-weight: 500;
            cursor:pointer;
        }
        ul li a{
            line-height:26px;
        }
        .active{
            background: #e2e2e2;
            border-radius: 2px;
        }
        :focus {
            outline: none !important;
        }
        .noSelect {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        `
    ]
})

export class PaginateComponent implements  AfterViewInit{
    @Input() list: Array<any>
    @Input() pageNumber: number = 10
    @Output() outList = new EventEmitter()

    total: number
    currentPage: number = 1
    lastPage:number
    pagesArr: Array<number> = []

    params: {
        min: number,
        max: number
    }

    constructor(
        private cd: ChangeDetectorRef
    ){ }

    ngAfterViewInit(){
        this.cd.detectChanges()
    }

    ngOnChanges(changes: SimpleChanges){
        for(let property in changes){
            if(property == 'list' && changes[property].currentValue != null)
                this.calculate(changes[property].currentValue)
                this.emit(changes[property].currentValue)
        }
    }

    calculate(data: any){
        // calculate # of pages
        this.total = data && data.length > 0 ? data.length : 0

        // minus 1 since start page starts with 0
        this.lastPage = Math.ceil(this.total / this.pageNumber);
      
        this.calculatePages(this.currentPage);                          
    }

    calculatePages(pageNo: number){
        this.currentPage = pageNo;
        this.pagesArr = [];
       
        this.params = this.isMinMax(pageNo);

        while(this.params.min <= this.params.max) 
            this.pagesArr.push(this.params.min++);

        this.emit(this.list,this.currentPage);
    }


    isMinMax(currPage: number){  
      if(this.lastPage < 3){
        return { min: 1, max: this.lastPage }      
      }    
      if(currPage == 1)
        return { min: 1, max: currPage + 2 }      

      if(currPage == this.lastPage)
        return { min: this.lastPage - 2, max: this.lastPage }      

      return { min: currPage - 1, max: currPage + 1}
    }

  
    emit(data: Array<any> = this.list, page: number = 0){    

        if(!data || data.length == 0)  {
            this.outList.next([])
            return;
        }

        setTimeout(() => {
            var emit;
            if(page == 0){
                data = data.slice(0,this.pageNumber);
            }

            emit = data.slice((page -1) * this.pageNumber, (page*this.pageNumber+this.pageNumber))
            this.outList.next(emit);    
        });
    }    

    fPage(){
        this.calculatePages(1); 
    }

    lPage(){
        this.calculatePages(this.lastPage);        
    }

    currPage(pageNo: number){  
        this.calculatePages(pageNo);       
    }
    
    dec(){
        if(this.currentPage == 1) return;
        this.calculatePages(this.currentPage-1)
    }

    inc(){
        if(this.currentPage == this.lastPage) return;
        this.calculatePages(this.currentPage+1)
    }


}