namespace Adamas.Models.Interfaces{
    public interface IJwtHandler {
        JwtIssuerOptions Create(string username, string password);
    }
}