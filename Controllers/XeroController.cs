using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Adamas.XeroUtilities;
using System.Text.Json;
using System.Text;
using Microsoft.Extensions.Options;

using Adamas.Models.XeroModels;
using System.Security.Cryptography;

using Xero.NetStandard.OAuth2.Client;
using Xero.NetStandard.OAuth2.Config;

using System.Net.Http;
using Microsoft.Extensions.Configuration;
//using static Org.BouncyCastle.Math.EC.ECCurve;
using AdamasV3.DAL;
using AdamasV3.Models.Modules;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using System.IO;
using Adamas.Models;
using Microsoft.EntityFrameworkCore;
//using Xero.Api.Core.Model;
using Xero.NetStandard.OAuth2.Model.PayrollAu;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class XeroController : Controller
    {

        private static string CLIENT_ID         = string.Empty;
        private static string SCOPES            = string.Empty;
        private static string CODE_VERIFIER     = string.Empty;
        private static string APPLICATION_NAME  = string.Empty;
        private static string REDIRECT_URI      = string.Empty;
        private static string STATE             = string.Empty;
        public static string AuthorisationUrl   = string.Empty;

        private readonly IOptions<XeroConfigurationPKCE> _xeroConfigPKCE;
        private readonly IOptions<XeroConfiguration> _xeroConfig;
        private readonly IHttpClientFactory httpClientFactory;
        private readonly IConfiguration Configuration;
        private IXeroService xeroService;
        private IMemoryCache _cache;
        private readonly DynamicDatabaseContext context;
        public XeroController(
            IOptions<XeroConfigurationPKCE> xeroConfigPKCE,
            IOptions<XeroConfiguration> xeroConfig,
            IHttpClientFactory httpClientFactory,
            IConfiguration configuration,
            IXeroService _xeroService,
            IMemoryCache cache,
            DynamicDatabaseContext _context
        )
        {
            _xeroConfigPKCE = xeroConfigPKCE;
            _xeroConfig = xeroConfig;
            xeroService = _xeroService;
            _cache = cache;
            context = _context;

            this.httpClientFactory = httpClientFactory;

            Configuration = configuration;

            CLIENT_ID           = Configuration["XeroConfiguration:ClientId"].ToString();
            SCOPES              = Configuration["XeroConfiguration:Scopes"].ToString();
            CODE_VERIFIER       = Configuration["XeroConfiguration:CodeVerifier"].ToString();
            APPLICATION_NAME    = Configuration["XeroConfiguration:ApplicationName"].ToString();
            REDIRECT_URI        = Configuration["XeroConfiguration:REDIRECT_URI"].ToString();
            STATE               = Configuration["XeroConfiguration:State"].ToString();
            AuthorisationUrl    = Configuration["XeroConfiguration:AuthorisationUrl"].ToString();
        }


        [HttpGet]
        public IActionResult GetUrl()
        {
            var clientId = CLIENT_ID;
            var scopes = Uri.EscapeUriString(SCOPES);
            var redirectUri = REDIRECT_URI;
            var state = STATE;

            string codeChallenge;
            using (var sha256 = SHA256.Create())
            {
                var challengeBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(CODE_VERIFIER));
                codeChallenge = Convert.ToBase64String(challengeBytes)
                    .TrimEnd('=')
                    .Replace('+', '-')
                    .Replace('/', '_');
            }
            var authLink = $"{AuthorisationUrl}?response_type=code&client_id={clientId}&redirect_uri={redirectUri}&scope={scopes}&state={state}&code_challenge={codeChallenge}&code_challenge_method=S256";
            return Redirect(authLink);
        }

        [HttpGet("callback")]
        public async Task<IActionResult> GetCallback()
        {
            string code = HttpContext.Request.Query["code"].ToString();
            string scope = HttpContext.Request.Query["scope"].ToString();
            string state = HttpContext.Request.Query["state"].ToString();

            const string url = "https://identity.xero.com/connect/token";

            var client = new HttpClient();

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
                new KeyValuePair<string, string>("client_id", CLIENT_ID),
                new KeyValuePair<string, string>("code", code),
                new KeyValuePair<string, string>("redirect_uri", REDIRECT_URI),
                new KeyValuePair<string, string>("code_verifier", CODE_VERIFIER),
            });

            var response = await client.PostAsync(url, formContent);

            //read the response and populate the boxes for each token
            //could also parse the expiry here if required
            var content = await response.Content.ReadAsStringAsync();
            var tokens = JObject.Parse(content);

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromDays(1));

            string id_token = tokens["id_token"]?.ToString();
            _cache.Set("id_token", id_token, cacheEntryOptions);

            string access_token = tokens["access_token"]?.ToString();
            _cache.Set("access_token", access_token, cacheEntryOptions);


            string refresh_token = tokens["refresh_token"]?.ToString();
            _cache.Set("refresh_token", refresh_token, cacheEntryOptions);

            var token = new Tokens()
            {
                IdToken = id_token,
                AccessToken = access_token,
                RefreshToken = refresh_token
            };

            var serializeJSON = Newtonsoft.Json.JsonConvert.SerializeObject(token);


            var logPath = Environment.CurrentDirectory;
            var OutputFilePath = Path.Combine(logPath, "Output", "token.json");

            using (var writer = System.IO.File.CreateText(OutputFilePath))
            {
                writer.WriteLine(serializeJSON);
            }

            return Ok(token);
        }


        [HttpGet("refresh-tokens")]
        //[Authorize]
        public async Task<IActionResult> GetRefreshTokens()
        {
            return Ok(await xeroService.GetRefreshTokens());
        }


        [HttpGet("PostEarningsRate/{dbName}")]
        public async Task<IActionResult> PostEarningsRate(string dbName)
        {
            var strDbConn = Configuration.GetValue<string>("XeroConfiguration:DBConnection");
            await xeroService.ChangeDbConnection(dbName, strDbConn);

            // Get RefreshTokens
            var token = await xeroService.GetRefreshTokens();
            // Get Xero EarningsRates
            var payItems = await xeroService.GetPayItems();
            return Ok(await xeroService.PostAllEarningsRateIds(payItems._PayItems.EarningsRates));
        }

        [HttpGet("PostXeroEmployeeId/{dbName}")]
        public async Task<IActionResult> PostXeroEmployeeId(string dbName)
        {
            var strDbConn = Configuration.GetValue<string>("XeroConfiguration:DBConnection");
            await xeroService.ChangeDbConnection(dbName, strDbConn);

            // Get RefreshTokens
            var token = await xeroService.GetRefreshTokens();
            // Get Xero EarningsRates
            var employees = await xeroService.GetEmployees();
            return Ok(await xeroService.PostXeroEmployeeId(employees));
        }

        [HttpPost("timesheets/{dbName}")]
        public async Task<IActionResult> PostTimesheets([FromBody] TimesheetXeroDto timesheet, string dbName)
        {
            var strDbConn = Configuration.GetValue<string>("XeroConfiguration:DBConnection");
            await xeroService.ChangeDbConnection(dbName, strDbConn);


            return Ok(await xeroService.PostTimesheet(timesheet.Timesheets));
        }

        [HttpGet("payroll-calendar")]
        public async Task<IActionResult> GetPayrollCalendars()
        {
            return Ok(await xeroService.GetPayrollCalendars());
        }

        [HttpGet("employees")]
        public async Task<IActionResult> GetEmployees()
        {
            var token = await xeroService.GetRefreshTokens();
            return Ok(await xeroService.GetEmployees());
        }

        [HttpGet("payitems")]
        public async Task<IActionResult> GetPayItems()
        {
            return Ok(await xeroService.GetPayItems());
        }

        [HttpGet("getTimeSheets")]
        public async Task<IActionResult> getTimeSheets()
        {
            return Ok(await xeroService.GetTimeSheets());
        }

        [HttpGet("connstring/{dbName}")]
        public async Task<IActionResult> GetConnectionString(string dbName)
        {
            //var strDbConn = Configuration.GetValue<string>("XeroConfiguration:DBConnection");
            //await xeroService.ChangeDbConnection(dbName, strDbConn);
            return Ok(context.Database.GetConnectionString());
        }

        [HttpGet("staff/{dbName}")]
        public async Task<IActionResult> GetStaffCount(string dbName)
        {
            //var strDbConn = Configuration.GetValue<string>("XeroConfiguration:DBConnection");
            //await xeroService.ChangeDbConnection(dbName, strDbConn);
            return Ok(await xeroService.GetStaffCount());
        }

    [HttpPost("{CreateEmployees}")]
        public async Task<IActionResult> CreateEmployees([FromBody] List<Employee>  employees)
        {
            //var strDbConn = Configuration.GetValue<string>("XeroConfiguration:DBConnection");
            //await xeroService.ChangeDbConnection(dbName, strDbConn);
            return Ok(await xeroService.CreateEmployees(employees));
        }


    [HttpPost("{CreatePayItems}")]
        public async Task<IActionResult> CreatePayItems([FromBody] List<PayItem>  payitems)
        {
            
            return Ok(await xeroService.CreatePayItems(payitems));
        }

         [HttpGet("TrackingCategories")]
        public async Task<IActionResult> getTrackingCategories()
        {
            return Ok(await xeroService.GetTrackingCategories());
        }



    }

   
    
}
