using System;

namespace Adamas.Models.Modules
{
    public class Loans
    {
        public string Item { get; set; }
        public string Notes { get; set; }
        public string Program { get; set; }
        public string Type { get; set; }
        public DateTime? Collected { get; set; }
        public DateTime? DateInstalled { get; set; }
        public DateTime? Loaned { get; set; }
        public string EquipmentCode { get; set; }
        public string SerialNumber { get; set; }
        public string LockBoxCode { get; set; }
        public string LockBoxLocation { get; set; }
        public string PurchaseAmount { get; set; }
        public string PurchaseDate { get; set; }
        public string RecordNumber { get; set; }

    }
}