
namespace Adamas.Models.Modules{
    public class UnfilteredCompetencies {
      public string Group { get; set; }
      public string Description { get; set; }
      public string PersonId { get; set; }
      public string Domain { get; set; }
      public bool Status { get; set; }
    }

    public class RecipientPreferences {
      public int RecordNumber { get; set; }
      public string Preference { get; set; }
      public string Notes { get; set; }
    }
}