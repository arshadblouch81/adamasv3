import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators, FormArray } from '@angular/forms'

import { mergeMap, debounceTime, distinctUntilChanged, takeUntil, switchMap, flatMap} from 'rxjs/operators';
import { ClrWizard } from "@clr/angular";
import * as moment from 'moment';
import { RemoveFirstLast } from '@pipes/pipes';
import { TimeSheetService, GlobalService, ClientService, StaffService, ListService, UploadService, months, days, gender,types, titles,caldStatuses, roles } from '@services/index';
import {Observable,of, from } from 'rxjs';

@Component({
  selector: 'add-referral',
  templateUrl: './add-referral.component.html',
  styleUrls: ['./add-referral.component.css']
})
export class AddReferralComponent implements OnInit {

    @Input() open: boolean = false;
    @Input() type: string = 'referral';

    @Output() openRefer = new EventEmitter();

    @ViewChild("wizard", {static: false}) wizard: ClrWizard;

    referralGroup: FormGroup;

    genderArr: Array<string> = gender
    typesArr: Array<string> = types
    titlesArr: Array<string> = titles
    addressType: Array<string> = []
    contactType: Array<string> = []


    loading: boolean;

    errorStr: string;

    branches: Array<string>;
    managers: Array<string>;
    agencies: Array<string>;

    private addresses: FormArray;
    private contacts: FormArray;

    constructor(
      private clientS: ClientService,
      private formBuilder: FormBuilder,
      private globalS: GlobalService,
      private listS: ListService
    ) {
      this.resetGroup();

    }

    ngOnInit() {
      
    }

    ngOnChanges(changes: SimpleChanges){
      for(let property in changes){
          if(property == 'open' && !changes[property].firstChange && changes[property].currentValue != null){
              this.open = true;
              this.wizard.reset();
              this.resetGroup();
          }
      }
    }

    resetGroup(){

          this.referralGroup = new FormGroup({
            gender: new FormControl(''),
            dob: new FormControl(''),
            title: new FormControl(''),
            lastname: new FormControl(''),
            firstname: new FormControl(''),
            middlename: new FormControl(''),
            tempaccountName: new FormControl(''),
            appendName: new FormControl(''),
            accountNo: new FormControl(''),
            organisation: new FormControl(''),
            type: new FormControl(0),
  
            addresses: new FormArray([this.createAddress()]),
            contacts: new FormArray([this.createContact()]),
  
            branch: new FormControl(''),
            agencyDefinedGroup: new FormControl(''),
            recipientCoordinator: new FormControl(''),
            referral: new FormControl(''),
  
            confirmation: new FormControl(null)
          });
  
  
          this.populateDropdowns();

          this.referralGroup.get('organisation').valueChanges
            .pipe( distinctUntilChanged(),debounceTime(200), mergeMap(x => {
              if(x) return this.clientS.isAccountNoUnique(x)
              else return of(-1)
            }))
            .subscribe(data => {
              this.processOutput(data);
          });
  
          this.referralGroup.get('appendName').valueChanges
              .pipe(debounceTime(1200))
              .subscribe(data => {
                this.referralGroup.patchValue({
                  tempaccountName:  this.appendAccount(),
                  confirmation: null
                });
              });
  
          this.referralGroup.get('firstname').valueChanges
              .pipe(debounceTime(200))
              .subscribe(data => {
                this.referralGroup.patchValue({
                  tempaccountName: this.generateAccount(),
                  appendName: '',
                  confirmation: null
                });
              });
  
          this.referralGroup.get('lastname').valueChanges
              .pipe(debounceTime(200))
              .subscribe(data => {
                this.referralGroup.patchValue({
                  tempaccountName: this.generateAccount(),
                  appendName: '',
                  confirmation: null
                });
              });

              
          this.referralGroup.get('title')
            .valueChanges
            .subscribe((data: string) => {
              const title = data.toUpperCase();
              if(title == 'MR'){
                this.referralGroup.patchValue({ gender: 'MALE'})
              } else if(title == 'MS' || title == 'MRS'){
                this.referralGroup.patchValue({ gender: 'FEMALE'})
              } else {
                this.referralGroup.patchValue({ gender: ''})
              }              
          });

    }

    contactTypeChange(index: number){
      var contact = this.referralGroup.get('contacts') as FormArray;
      contact.controls[index].get('contact').reset();
    }

    resetappendName(): void{
      this.referralGroup.get('appendName').disable;
      this.referralGroup.patchValue({
        appendName: ''
      });
      this.referralGroup.get('appendName').enable;
    }

    processOutput(data: number, isStrIncluded: boolean = false): void{
      let obj = null;
      if(data == 1){
        obj = {
          accountNo: !isStrIncluded ? this.generateAccount() : this.appendAccount(),
          confirmation: true
        }
      }

      if(data == 0){
        obj = {
          accountNo: this.generateAccount(),
          tempaccountName: this.generateAccount(),
          confirmation: false
        }
        this.errorStr = 'Account is taken';
      }

      //
      if(data == -1){
        obj = {
          accountNo: this.generateAccount(),
          confirmation: false
        }
        this.errorStr = this.type == 'referral' ? 'Lastname and Firstname are required' : 'Organisation Name required';
      }
      this.referralGroup.patchValue(obj);
      this.loading = false;
    }

    _keydown(event: KeyboardEvent) {
      // numbers only /[0-9\+\-\ ]/
      if(!event.key.match(/^[a-zA-Z ]*$/)) return false;
    }

    isNamesComplete(): boolean{
      return !this.globalS.isEmpty(this.referralGroup.get('lastname').value) &&
              !this.globalS.isEmpty(this.referralGroup.get('firstname').value);
    }

    isNameDataValidated(): boolean {
      if(!this.globalS.isEmpty(this.referralGroup.get('lastname').value) &&
          !this.globalS.isEmpty(this.referralGroup.get('firstname').value)){
        return true;
      }
      return false;
    }

    generateAccount(): string{
      const fname = (this.referralGroup.get('firstname').value).trim();
      const lname = (this.referralGroup.get('lastname').value).trim();
      const birthdate = this.referralGroup.get('dob').value ? moment(this.referralGroup.get('dob').value).format('YYYYMMDD') : '';
      const gender = this.referralGroup.get('gender').value ? '('+(this.referralGroup.get('gender').value).trim()[0] + ')' : '';

      var _account = this.type === 'referral' ? lname + ' ' + fname + gender + ' ' +birthdate : (this.referralGroup.get('organisation').value).trim();
      return _account.toUpperCase() || '';
    }

    appendAccount(): string{
      var tName = (this.referralGroup.get('tempaccountName').value).trim();
      var aName = (this.referralGroup.get('appendName').value).trim();

      return (tName + aName.toUpperCase()) || '';
    }

    populateDropdowns(): void{
      this.clientS.getcontacttype().subscribe(data => {
        this.contactType = data.map(x => {
          return (new RemoveFirstLast().transform(x.description)).trim();
        });
      })
      this.clientS.getaddresstype().subscribe(data => {
        this.addressType = data.map(x => {
          return new RemoveFirstLast().transform(x.description)
        });
      })
      this.listS.getlistbranches().subscribe(data => this.branches = data);
      this.clientS.getmanagers().subscribe(data => this.managers = data);
      this.listS.getserviceregion().subscribe(data => this.agencies = data);
    }

    add(){
      this.referralGroup.controls["dob"].setValue(this.referralGroup.value.dob ? moment(this.referralGroup.value.dob).format() : '')
      var manager = (this.managers[this.referralGroup.get('recipientCoordinator').value] as any);
      this.referralGroup.controls["recipientCoordinator"].setValue(manager.description);

      console.log(this.referralGroup.value);
      this.clientS.postprofile(this.referralGroup.value)
          .subscribe(data => {
            if (data) {
              data.email = manager.detail;
              this.openRefer.emit(data);
              this.globalS.sToast('Success','Recipient Added')
            }
        });
    }

    addAddress(): void{
      this.addresses = this.referralGroup.get('addresses') as FormArray;
      this.addresses.push(this.createAddress());
    }

    deleteAdd(i: number): void{
      this.addresses = this.referralGroup.get('addresses') as FormArray;
      this.addresses.removeAt(i);
    }

    createAddress(): FormGroup {
      return this.formBuilder.group({
        address1: new FormControl(''),
        type: new FormControl(''),
        suburb: new FormControl('')
      });
    }

    addContact(): void{
      this.contacts = this.referralGroup.get('contacts') as FormArray;
      this.contacts.push(this.createContact());
    }

    deleteCont(i: number): void{
      this.contacts = this.referralGroup.get('contacts') as FormArray;
      this.contacts.removeAt(i);
    }

    createContact(): FormGroup{    
      return this.formBuilder.group({
        contacttype: new FormControl('MOBILE'),
        contact: new FormControl('')
      });
    }

    verify(){

      if(!this.isNameDataValidated()){
        this.processOutput(-1);
        return;
      }

      const account = this.referralGroup.get('tempaccountName').value;

      this.clientS.isAccountNoUnique(account)
          .subscribe(data => {
            this.processOutput(data);
          });
    }

    isPhoneNumber(fg: FormGroup): boolean {
      const type = fg.value.contacttype;
      return type === 'FAX' || type === 'HOME' || type === 'MOBILE' || type === 'PHONE' || type === 'WORK MOBILE' || type === 'WORK';
    }


}
