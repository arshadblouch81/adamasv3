import { NgModule, ModuleWithProviders } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { RosterTimeline } from './roster'
import { RosterDateTimeDirective } from './roster.directive'
import { RosterPipe } from './roster.pipe'
import { ClarityModule } from '@clr/angular'

import { DraggableModule } from '../draggable/draggable.module';

@NgModule({
    declarations:[
        RosterTimeline,
        RosterDateTimeDirective,
        RosterPipe
    ],
    imports:[
        CommonModule,
        FormsModule,
        ClarityModule,
        DraggableModule
    ],
    exports:[
        RosterTimeline,
        RosterPipe,
        ClarityModule,
        RosterDateTimeDirective
    ]
})

export class RosterTimelineModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: RosterTimelineModule            
        }
    }
}

