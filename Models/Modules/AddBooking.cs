using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules
{
    public class AddBooking
    {
        public bool BookType { get; set; }
        public string StaffCode { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public Services Service { get; set; }
        public string ClientCode { get; set; }
        public string Duration { get; set; }
        public string Username { get; set; }
        public bool AnyProvider { get; set; }
        public string BookingType { get; set; }
        public string Notes { get; set; }
        public List<PermanentBooking> PermanentBookings { get; set; }
        public List<PermanentBooking> RealDateBookings { get; set; }
        public string Summary { get; set; }
        public string RecipientPersonId { get; set; }
        public string ManagerPersonId { get; set; }
    }

    public class PermanentBooking{
        public DateTime Time { get; set; }
        public double Quantity { get; set; }
        public int Week { get; set; }  
    }
}