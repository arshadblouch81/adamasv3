using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;


using Syncfusion.DocIO.DLS;
using System.IO;
using Syncfusion.DocIO;
using System.Text.RegularExpressions;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class WordController : Controller
    {

        public WordController(){

        }

        [HttpGet("sample")]
        public async Task<IActionResult> GetSample(){

            //Application application = new Application();

            //Document doc = application.Documents.Open(@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan.docx");

            //object o = "";
            //object oFalse = false;
            //object oTrue = true;

            // foreach (Word.Range range in doc.StoryRanges)
            // {

            //     Word.Find find = range.Find;

            //     Word.Find find1 = range.Find;
            //     object findText1 = "<<FullNameSurnameLast>>]";
            //     object replacText1 = "Mark Aris M. Trinidad";
            //     object replace1 = Word.WdReplace.wdReplaceAll;
            //     object findWrap1 = Word.WdFindWrap.wdFindContinue;
            //     find.Execute(
            //         ref findText1,      // 1
            //         ref o, 
            //         ref o, 
            //         ref o, 
            //         ref o, 
            //         ref o,
            //         ref o, 
            //         ref findWrap1,      //8
            //         ref o, 
            //         ref replacText1,    //10
            //         ref replace1,       //11
            //         ref o, 
            //         ref o, 
            //         ref o, 
            //         ref o);

            //     Marshal.FinalReleaseComObject(find1);
            //     Marshal.FinalReleaseComObject(range);
            // }

            // var path_YourDocsName = @"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Merge Careplan1.docx";
            // doc.SaveAs(path_YourDocsName);
            // doc.Close();
            // application.Quit(ref o, ref o, ref o);

            // if (doc != null) Marshal.FinalReleaseComObject(doc);

            object missing = "";

           try
           {
                //Creates new Word document instance for word processing.
                using (WordDocument document = new WordDocument())
                {

                    Stream docStream = System.IO.File.OpenRead(Path.GetFullPath(@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan.docx"));
                    document.Open(docStream, FormatType.Docx);
                    docStream.Dispose();
                    //Finds all occurrences of a misspelled word and replaces with properly spelled word.

                    IWSection section = document.AddSection();
                    //Adds a new paragraph into Word document and appends text into paragraph
                    IWTextRange textRange = section.AddParagraph().AppendText("Price Details");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    section.AddParagraph();
                    //Adds a new table into Word document
                    IWTable table = section.AddTable();
                    //Specifies the total number of rows & columns
                    table.ResetCells(3, 2);
                    //Accesses the instance of the cell (first row, first cell) and adds the content into cell
                    textRange = table[0, 0].AddParagraph().AppendText("Item");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    //Accesses the instance of the cell (first row, second cell) and adds the content into cell
                    textRange = table[0, 1].AddParagraph().AppendText("Price($)");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 12;
                    textRange.CharacterFormat.Bold = true;
                    //Accesses the instance of the cell (second row, first cell) and adds the content into cell
                    textRange = table[1, 0].AddParagraph().AppendText("Apple");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 10;
                    //Accesses the instance of the cell (second row, second cell) and adds the content into cell
                    textRange = table[1, 1].AddParagraph().AppendText("50");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 10;
                    //Accesses the instance of the cell (third row, first cell) and adds the content into cell
                    textRange = table[2, 0].AddParagraph().AppendText("Orange");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 10;
                    //Accesses the instance of the cell (third row, second cell) and adds the content into cell
                    textRange = table[2, 1].AddParagraph().AppendText("30");
                    textRange.CharacterFormat.FontName = "Arial";
                    textRange.CharacterFormat.FontSize = 10;


                    // document.Replace("<<FullNameSurnameLast>>", "Mark Aris Trinidad", true, true);
                    // document.Replace("<<HomePhone>>", "0932342123", true, true);

                    // document.Replace("<<MobilePhone>>", "092832424", true, true);
                    // document.Replace("<<Email>>", "maartri@gmail.com", true, true);
                    // document.Replace("<<UsualStreetAddress>>", "ILigan City", true, true);
                    // document.Replace("<<UsualSuburb>>", "Lanao del Norte", true, true);
                    // document.Replace("<<UsualPostcode>>", "600", true, true);
                    
                    //Saves the resultant file in the given path.
                    docStream = System.IO.File.Create(Path.GetFullPath(@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan_Copy.docx"));
                    document.Save(docStream, FormatType.Docx);
                    docStream.Dispose();

                  
                    //Stream docStream = System.IO.File.OpenRead(Path.GetFullPath(@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan.docx"));
                    //document.Open(docStream, FormatType.Docx);
                    //docStream.Dispose();
                    
                    //TextSelection[] textSelections = document.FindAll(new Regex(@"\[(.*)\]"));
                    //for (int i = 0; i < textSelections.Length; i++)
                    //{
    
                    //    docStream = System.IO.File.OpenRead(Path.GetFullPath(@"../../../" + textSelections[i].SelectedText.TrimStart('[').TrimEnd(']') + ".docx"));
                    //    WordDocument subDocument = new WordDocument(docStream, FormatType.Docx);
                    //    docStream.Dispose();
                    //    document.Replace(textSelections[i].SelectedText, subDocument, true, true);
                    //    subDocument.Dispose();
                    //}
                    
                    //docStream = System.IO.File.Create(Path.GetFullPath(@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan1.docx"));
                    //document.Save(docStream, FormatType.Docx);
                    //docStream.Dispose();
                }

           } catch(Exception ex)
           {
               throw ex;

           }

            return Ok(true);
        }

    }
}
