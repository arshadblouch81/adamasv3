import { Injectable, Renderer2, RendererFactory2,HostListener  } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableNavigationService {
  private renderer: Renderer2;
  public searchTerm: string = '';

  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  // Initialize the listener and store the component’s data
  initializeKeyboardListener(tableData: any[]) {
    this.renderer.listen('document', 'keydown', (event: KeyboardEvent) => {
      this.handleKeyboardEvent(event, tableData);
    });
  }

  // Pass tableData as an argument
  private handleKeyboardEvent(event: KeyboardEvent, tableData: any[]) {
    if (event.key.length === 1 && /^[a-zA-Z0-9]$/.test(event.key)) {
      this.searchTerm += event.key.toLowerCase();
      this.scrollToRow(tableData);
    } else if (event.key === 'Backspace') {
      this.searchTerm = '';
      if (this.searchTerm.length > 0) {
        this.searchTerm = this.searchTerm.slice(0, -1);
        this.scrollToRow(tableData);
      }
    } else if (event.key === 'Escape') {
      this.searchTerm = '';
      this.scrollToRow(tableData);
    }
  }

  scrollToRow(tableData: any[]) {

    if (!tableData || tableData.length === 0) {
      console.warn('Table data is undefined or empty');
      // return;
    }


  console.log('Search Term:', this.searchTerm); // Log the search term
  console.log('Table Data:', tableData); // Log the table data

    const matchIndex = tableData.findIndex(item =>
      item.name.toLowerCase().startsWith(this.searchTerm)
    );
    if (matchIndex !== -1) {
      const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
      if (tableRow) {
        tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
        this.highlightRow(tableRow);
      }
    }
  }

  private highlightRow(row: HTMLElement) {
    row.classList.add('highlight');
    setTimeout(() => row.classList.remove('highlight'), 2000);
  }
}
