import { Component, OnInit, OnDestroy, Output, Input ,ViewChild, AfterViewInit, EventEmitter, Renderer2, ChangeDetectorRef, ViewEncapsulation} from '@angular/core'
import { ComponentFactoryResolver, ComponentRef, Injector,  ViewContainerRef, Inject, Injectable} from '@angular/core'

import { GlobalService, ClientService, TimeSheetService,ShareService, ListService, PrintService } from '@services/index';
import { forkJoin,  Subject ,  Observable } from 'rxjs';
import {ShiftDetail} from '../../roster/shiftdetail'
import {DMRoster} from '../../roster/dm-roster'
import { NzModalService } from 'ng-zorro-antd/modal';
import { BrowserModule,DomSanitizer } from '@angular/platform-browser';
import { FormGroup,FormBuilder,Validators, FormArray, FormControl } from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import parseISO from 'date-fns/parseISO'
import { addDays, format, formatDistance, formatRelative, nextDay, subDays } from 'date-fns'
import * as moment from 'moment';
import { CancelShift, RecipientExternal, RecipientSearch, StaffExternal, StaffSearch, UnallocateStaff } from '../../roster';
import { toNumber, uniqueId } from 'lodash';
import { DOCUMENT } from '@angular/common';
import { error } from 'console';
import {RECIPIENT_OPTION, User} from '../../../modules/modules';
import {Router, ActivatedRoute} from '@angular/router';


class Address {
    postcode: string;
    address: string;
    suburb: string;
    state: string;

    constructor(postcode: string, address: string, suburb: string, state: string) {
        this.suburb = suburb.trim();
        this.address = address;
        this.postcode = postcode;
        this.state = state;
    }

    getAddress() {
        var _address = `${this.address} ${this.suburb} ${this.postcode}`;
        return (_address.split(' ').join('+')).split('/').join('%2F');
    }
}



  
@Component({
    selector: 'daymanager',    
    styleUrls: ['./daymanager.css'],
    templateUrl: './daymanager.html',
    
})
//

export class DayManagerAdmin implements OnInit,AfterViewInit, OnDestroy {
    date: any = new Date();
    refreshCalander:boolean;
    serviceType:string;
    dateFormat:string="dd/MM/YYYY"
    dayView: number = 7;
    dayViewValue:number=7;
    dayViewArr: Array<number> = [5, 7, 10, 14];
    //reload: boolean = false;
    reload:Subject<boolean> = new Subject();
    loadingRoster :Subject<any> = new Subject() ;
    applyFilter: Subject<any>= new Subject();
    dmOptions: Subject<any>= new Subject();
    loadCancelShift :Subject<any> = new Subject() ;
    loadUnallocateStaff :Subject<any> = new Subject() ;
    AILevel:number;
    isDropdownVisible:boolean=false;
    @Input() loadMain:Subject<any>= new Subject() ;
    @Output() payperiodEmiter :EventEmitter<any> = new EventEmitter() ;

    loadAIData:Subject<any>= new Subject() ;

    AllocateStaff:boolean;

    CustomFilters: Array<any> = [];
    toBePasted: Array<any>=[];
    lstStartWith: Array<any>=[];
    branchesList = [];
    activityList = [];
    programsList = [];
    fundingList:Array<any>=[];
    teamList:Array<any>=[];
    
    lstMakeUps:Array<any>=[];
    selectedBranches:Array<any>=[];
    selectedPrograms:Array<any>=[];
    selectedActivities:Array<any>=[];
    selectedFundings:Array<any>=[];
    selectedTeams:Array<any>=[];
    addInExisting:boolean=true;
    viewMakeUps:boolean;
    nzFilterPlaceHolder:string="Input to Search";
    showSimpleFilterInput:boolean;
    //@ViewChild(ShiftDetail) detail!:ShiftDetail;
    //@ViewChild(DMRoster) dmroster!:DMRoster;
    @ViewChild('dynamicContainer', {read: ViewContainerRef})
    dynamicContainer: ViewContainerRef;
    private _dynamicInstance: ComponentRef<ShiftDetail>;

    @ViewChild('dynamicRoster', {read: ViewContainerRef})
    dynamicRosterContainer: ViewContainerRef;
    private _dynamicRoster: ComponentRef<DMRoster>;

    @ViewChild('dynamicContainerRecipientexternal', {read: ViewContainerRef})
    dynamicContainerRecipientexternal: ViewContainerRef;
    private _dynamicContainerRecipientexternal: ComponentRef<RecipientExternal>;

    @ViewChild('dynamicContainerStaffExternal', {read: ViewContainerRef})
    dynamicContainerStaffExternal: ViewContainerRef;
    private _dynamicContainerStaffExternal: ComponentRef<StaffExternal>;

    @ViewChild('dynamicContainerStaffSearch', {read: ViewContainerRef})
    dynamicContainerStaffSearch: ViewContainerRef;
    private _dynamicContainerStaffSearch: ComponentRef<StaffSearch>;

    @ViewChild('dynamicContainerRecipientSearch', {read: ViewContainerRef})
    dynamicContainerRecipientSearch: ViewContainerRef;
    private _ddynamicContainerRecipientSearch: ComponentRef<RecipientSearch>;
    
    @ViewChild('dynamicCancelShift', {read: ViewContainerRef})
    dynamicCancelShift: ViewContainerRef;
    private _dynamicCancelShift: ComponentRef<CancelShift>;

    @ViewChild('dynamicUnallocateStaff', {read: ViewContainerRef})
    dynamicUnallocateStaff: ViewContainerRef;
    private _dynamicUnallocateStaff: ComponentRef<UnallocateStaff>;

    
    
    optionsModal: boolean = false;
    displayOption:boolean=true;
    pastePosition:any;
    recipientDetailsModal: boolean = false;
    changeModalView = new Subject<number>();
    OperationView= new Subject<number>();
    AllocateView= new Subject<number>();
    ViewNudge= new Subject<number>();
    ViewDMStatus = new Subject<number>();
    ApproveView = new Subject<number>();
    ViewGroupShift = new Subject<number>();
    resourceType:string;
    txtSearch:string;
    openSearchStaffModal:boolean;
    openSearchRecipientModal:boolean;
    Spiner_loading:boolean;
    ViewDMStatusModal:boolean;
    mnuMarkInProgress:boolean;
    mnuGroupShift:boolean;
    dmStatusType:number;
    ProgressNote:any;
    showGroupShiftRecipient:boolean;
    showGroupShiftAttendees:boolean
    RecipientList:Array<any>=[];
    dmManager:Array<any>=[];
    setOfCheckedId = new Set<any>();
    checked = false;    
    indeterminate = false;
    viewAIAssistant:boolean;
    viewStaffAvailability:boolean;
    viewVehicleAvailability:boolean;
    showPublicHoliday:boolean;
    lstHolidays:Array<any>=[];

    drawerVisible:boolean;
    pdfTitle:string;
    tryDoctype:any;
    ViewCancelShiftModal:boolean;
    ViewCancelShiftOption:boolean;

    booking:{
        recipientCode: 'TT',
        userName:'sysmgr',
        date:'2022/01/01',
        startTime:'07:00',
        endTime:'17:00',
        endLimit:'20:00'
      };
    bookingData = new Subject<any>();
    bookingDataRecipient = new Subject<any>();
    ViewChangeDayTimeModal:boolean;
    AddRosterModel:boolean;
    ViewServiceNoteModal:boolean    
    Person:any={id:'0',code:'',personType:'Recipient', noteType:'SVCNOTE'};
    loadingNote:Subject<any>=new Subject();
    loading:boolean;
    operation:string;
    CaseLoadValue:string;
    ViewCaseLoadModal:boolean
    ViewAllocateResourceModal:boolean;
    ViewAllocateResourceQtyModal:boolean;
    ResourceValue:number;
    InputMode:string='decimal';
    
    size: any = 'large';
    showMenu:boolean;
    LimitTo:string;
    startWith:string;
    

    parserPercent = (value: string) => value.replace(' %', '');
    parserDollar = (value: string) => value.replace('$ ', '');
    parserValue = (value: string) => value;
    formatterDollar = (value: number) => `${value > -1 || !value ? `$ ${value}` : ''}`;
   
    info = {StaffCode:'', ViewType:'',IsMaster:false,date:''}; 
    selectedOption:any;
    rosters:any;
    new_date:any;
    selectedStaff:string='';
    txtAlertSubject:String;
    txtAlertMessage:String;
    show_alert:Boolean
    Error_Msg:String;
    breachRoster:Boolean;
    record_being_pasted:any;
    AllocateStaffModal:boolean;
    OpenAllocateStaffComponent:boolean;
    selectedCarer:string;
    selectedRecipient:string;
    NudgeValue=0;
    NudgeStatus:string='Up';
    ViewNudgeModal:boolean;
    serviceActivityList: Array<any>;
    originalList: Array<any>;
    ServiceSetting:any;
    HighlightRow2:number
    isFirstLoad:boolean
    _highlighted: Array<any> = [];
    private address: Array<any> = [];

    ViewRecipientDetails = new Subject<number>();
    ViewDetails= new Subject<number>();
    ViewChangeDayTime= new Subject<number>();
    DateTimeForm:FormGroup;
    durationObject: any;
    today = new Date();
    defaultStartTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
    defaultEndTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);
    selectedPersonType:any ;
  
    @Output() dmTypeSelected = new EventEmitter();
    personTypeList:Array<any>=[];
    
    themes : Array<any> = ['CLASSIC','DARK','LIGHT'];
    Theme:string='CLASSIC';
    user:any;
    token:any;
    Reportuser : any;
    master:boolean=false;
    deleted:boolean=false;
    viewType:string='Staff'
    selected_data:any;
    recipientexternal:boolean;
    staffexternal:boolean;
    ViewAdditionalModal:boolean;
    notes:string="";
    HideW1W2:boolean;
    IncludeDuration:boolean;
    AutoPrviewNote:boolean;
    CustomFilter:boolean;
    ViewExistingFilter:boolean;
    SortOrder:string;
    lstGroup:Array<any>=[];
    viewQuickFilter:boolean;
    AllPrograms:boolean=true;
    AllBranches:boolean=true;
    AllActivities:boolean=true;
    AllTeams:boolean=true;
    AllFundings:boolean=true;
    OpenSearchOfStaff:boolean;
    OpenSearchOfRecipient:boolean;
    PayPeriodEndDate:string;
    PayPeriodEndDateValue:string;
    PayPeriod:any;
   

    workflowVisible: boolean = false;
    searchRecord:string;
    style1 = "{'background-color':master === true ? '#F18805' : '#b7b5b5', margin:'5px', padding-left: '2px', padding-top: '4px', font-size: '15px', color: 'white', height: '32px', width:'80px', text-align: 'center', vertical-align: 'middle', border-top-left-radius: '3px', border-bottom-left-radius: '3px' }";
    style2 = "{'background-color':master === false ? '#85B9D5' : '#b7b5b5', padding-left: '2px', padding-top: '4px', font-size: '15px', color: 'white', height: '32px', width: '80px', text-align: 'center', vertical-align: 'middle', border-top-right-radius: '3px', border-bottom-right-radius: '3px' }";
    style3 ="{'background-color':master === true ? '#F18805' : '#85B9D5' }"
    dynamicStyle = "{background-color:'#313030' !important', margin-top: '5px', margin-bottom: '10px' }";
    modelStyle = " {top: '10px'}";
    optionsList = [     
    
      { id: 2, name: 'Include Duration in shift Display', checked:false },
      { id: 7, name: 'Disable Activity Colors', checked:false },  
      { id: 1, name: 'Hide W1 W2 WKD Display', checked:false },
      { id: 4, name: 'Include Notes in Service Display', checked:false },
      { id: 5, name: 'Include Information Only Services in Worked Hours', checked:false },
    //   { id: 3, name: 'Auto Preview Notes on click', checked:false },
      { id: 8, name: 'Reset Individually Set Column Widths on Refresh', checked:false },
      { id: 6, name: 'Recipient Branch Only', checked:false },

    ];

    optionsList2 = [
        { id: 1, name: 'Booking', checked:false },
        { id: 2, name: 'Direct Care', checked:false },
        { id: 7, name: 'Case Management', checked:false },
        { id: 10, name: 'Transport', checked:false },
        { id: 11, name: 'Facilities', checked:false },
        { id: 12, name: 'Groups', checked:false },
        { id: 0, name: 'Items', checked:false },
        { id: 13, name: 'Unavailable', checked:false },
        { id: 6, name: 'Staff Admin', checked:false },
        { id: 5, name: 'Travel Time', checked:false },
        { id: 11, name: 'Staff Leave', checked:false },
        
      ];
      SortOrderBooking:any ="ORDER BY CASELOAD/RUN";
      lstSortOrderBooking:Array<any>= [
       "ORDER BY ACTIVITY/TIME/RECIPIENT",
       "ORDER BY CASELOAD/RUN",
       "ORDER BY SUBURB/ACTIVITY/TIME",
       "ORDER BY SUBURB/RECIPIENT/TIME",
       "ORDER BY SUBURB/TIME/ACTIVITY",
       "ORDER BY SUBURB/TIME/RECIPIENT",
       "ORDER BY TIME/RECIPIENT",
       "ORDER BY CATEGORY/RECIPIENT/TIME",
       "ORDER BY CATEGORY/TIME/RECIPIENT",
       "ORDER BY CATEGORY/ACTIVITY/TIME",
       "ORDER BY CATEGORY/TIME/ACTIVITY"
      ];      


      async loadComponentAsync(index:any) {
        const {ShiftDetail} = 
          await import('../../roster/shiftdetail');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(ShiftDetail);
        
        // create and inject component into dynamicContainer
       
        this._dynamicInstance = 
          this.dynamicContainer.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
       
       
        
         this._dynamicInstance.instance.timesheetDone.subscribe(data=>{
            this.shiftChanged(data);
         });
         this._dynamicInstance.instance.data =index;
         this._dynamicInstance.instance.isVisible=true;
         this._dynamicInstance.instance.viewType =this.viewType;
         this._dynamicInstance.instance.editRecord=false;
         this._dynamicInstance.instance.ngAfterViewInit();
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      async loadRosterComponent(index:any) {
        const {DMRoster} = 
          await import('../../roster/dm-roster');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(DMRoster);
        
        // create and inject component into dynamicContainer
        this._dynamicRoster = 
          this.dynamicRosterContainer.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
      
       // this._dynamicRoster.instance.loadRoster=this.loadingRoster;
        this._dynamicRoster.instance.RosterDone.subscribe(d=>
            {
                this.AddRosterModel = false;
                this.load_rosters()
            });

            this._dynamicRoster.instance.reLoadGrid.subscribe(d=>
                {
                    this.reLoadGridRosterModel()
                });
      
              
       
                setTimeout(() => {
           
                    this._dynamicRoster.instance.info=index;
        
                    this._dynamicRoster.instance.loadRoster.next(index)
                    this._dynamicRoster.instance.ngAfterViewInit();
                    this.AddRosterModel=true;
                 },1000);
           
           
           

           
                
      }
      async loadComponentAsync_Recipient() {
        const {RecipientExternal} = 
          await import('../../roster/RecipientExternal');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(RecipientExternal);
        
        // create and inject component into dynamicContainer
        this._dynamicContainerRecipientexternal = 
          this.dynamicContainerRecipientexternal.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
        this.recipientexternal = true;
        
         this._dynamicContainerRecipientexternal.instance.recipientexternalDone.subscribe(data=>{
            this.cancelRecipientExternal(data);
         });
         this._dynamicContainerRecipientexternal.instance.AccountNo =this.selected_data;
         this._dynamicContainerRecipientexternal.instance.isVisible=true;
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      async loadComponentAsync_Staff() {
        const {StaffExternal} = 
          await import('../../roster/StaffExternal');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(StaffExternal);
        
        // create and inject component into dynamicContainer
        this._dynamicContainerStaffExternal = 
          this.dynamicContainerStaffExternal.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
        this.staffexternal = true;
        
         this._dynamicContainerStaffExternal.instance.staffexternalDone.subscribe(data=>{
            this.cancelStaffExternal(data);
         });
         this._dynamicContainerStaffExternal.instance.AccountNo =this.selected_data;
         this._dynamicContainerStaffExternal.instance.isVisible=true;
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }

      async loadComponentAsync_StaffSerarch() {
        const {StaffSearch} = 
          await import('../../roster/staffsearch');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(StaffSearch);
        
        // create and inject component into dynamicContainer
        this._dynamicContainerStaffSearch = 
          this.dynamicContainerStaffSearch.createComponent(dynamicComponentFactory);
       
        this.optionsModal=false;
        this.openSearchStaffModal = true;
        
         this._dynamicContainerStaffSearch.instance.searchDone.subscribe(data=>{
            this.onStaffSearch(data);
         });
         this._dynamicContainerStaffSearch.instance.bookingData =this.bookingData;
         this._dynamicContainerStaffSearch.instance.findModalOpen=true;
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      async loadComponentAsync_RecipientSerarch() {
        const {RecipientSearch} = 
          await import('../../roster/recipientsearch');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(RecipientSearch);
        
        // create and inject component into dynamicContainer
        this._ddynamicContainerRecipientSearch = 
          this.dynamicContainerRecipientSearch.createComponent(dynamicComponentFactory);
       
        this.optionsModal=false;
        this.openSearchRecipientModal = true;
        
         this._ddynamicContainerRecipientSearch.instance.searchDone.subscribe(data=>{
            this.onRecipientSearch(data);
         });
         this._ddynamicContainerRecipientSearch.instance.bookingData =this.bookingDataRecipient;
         this._ddynamicContainerRecipientSearch.instance.findModalOpen=true;
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      async loadComponentAsync_CancelShift() {
        const {CancelShift} = 
          await import('../../roster/cancelshift');
        
      
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(CancelShift);
        
        // create and inject component into dynamicContainer
        this._dynamicCancelShift = 
          this.dynamicCancelShift.createComponent(dynamicComponentFactory);
       
        this.optionsModal=false;
        this.ViewCancelShiftModal = true;
        
         this._dynamicCancelShift.instance.Done.subscribe(data=>{
            this.CancelShiftDone(data);
         });
         setTimeout(() => {
           this._dynamicCancelShift.instance.loadCancelShift.next({ diary: this._highlighted, selected: this.selectedOption });
           this._dynamicCancelShift.instance.view=true;
         }, 300);
         
         //this._dynamicCancelShift.instance.loadCancelShift.next({diary:this._highlighted,selected:this.selectedOption});
         //this._dynamicCancelShift.instance.view=true;
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      async loadComponentAsync_UnallocateStaff() {
        const {UnallocateStaff} = 
          await import('../../roster/UnallocateSaff');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(UnallocateStaff);
          //this.location.clear()
        // create and inject component into dynamicContainer
        this._dynamicUnallocateStaff = 
          this.dynamicUnallocateStaff.createComponent(dynamicComponentFactory);
       
          this._dynamicUnallocateStaff.instance.Done.subscribe(data=>{
            this.UnallocateStaffDone(data);
         });
      
         
         setTimeout(() =>{
            this._dynamicUnallocateStaff.instance.findModalOpen=true;
            this._dynamicUnallocateStaff.instance.reallocate=false;
            this._dynamicUnallocateStaff.instance.loadUnallocateStaff.next({diary:this._highlighted,selected:this.selectedOption});
        
         },100)
       
         
      }
    constructor(
        private globalS: GlobalService,
        private clientS: ClientService,
        private timeS:TimeSheetService,
        private modalService: NzModalService,
        private sharedS:ShareService,
        private formBuilder: FormBuilder,
        private listS:ListService,
        private printS: PrintService,
        public sanitizer: DomSanitizer,
        private componentFactoryResolver: ComponentFactoryResolver,
        private location: ViewContainerRef,
        private renderer: Renderer2,
        private cd:ChangeDetectorRef,
        private router: Router,
        @Inject (DOCUMENT) private  document:Document,        
    )

       
     {
        this.Theme= this.globalS.getTheme();
        this.ViewRecipientDetails.subscribe(data => {
            this.optionsModal=false;
            if (data==1){
                this.selected_data = {
                    data: this.selectedOption.recipient,
                    code: this.selectedOption.uniqueid,
                    option:1
                }            
                if ((this.selectedOption.recipient=='!MULTIPLE' || this.selectedOption.recipient=='!INTERNAL')){
                    this.globalS.eToast('Day Manager',`No Recipient to view detail!`)
                }else{
                    this.recipientexternal = true;
                    this.loadComponentAsync_Recipient();
                }
        }else if (data==2){
                this.loadNotes();
               
        }
           
        });

        this.ViewChangeDayTime.subscribe(data => {
            this.optionsModal=false;
             let startTime= this.selectedOption.startTime;
             let endTime= this.selectedOption.endTime;
             let date =this.selectedOption.date;
             this.defaultStartTime = parseISO(new Date(date + " " + startTime).toISOString());
             this.defaultEndTime = parseISO(new Date(date + " " + endTime).toISOString());

            let time:any={startTime:this.defaultStartTime, endTime:this.defaultEndTime}
            this.DateTimeForm.patchValue({
                recordNo: this.selectedOption.recordno,
                rdate: this.selectedOption.date,
                time:time,
                payQty:this.selectedOption.payQty,
                billQty:this.selectedOption.billQty           

            });

           this.ViewChangeDayTimeModal=true;
            
        });

        this.changeModalView.subscribe(data => {

            this.OpenChangeResources(data)
            
        });
    

        
    this.ViewDetails.subscribe(data => {
        // console.log(data);
        this.optionsModal=false;
        if (data==1){                          
            this.selected_data = {
                data: this.selectedOption.recipient,
                code: this.selectedOption.uniqueid,
                option:1
            }            
            if ((this.selectedOption.recipient=='!MULTIPLE' || this.selectedOption.recipient=='!INTERNAL')){
                this.globalS.eToast('Day Manager',`No Recipient to view detail!`)
            }else
                this.loadComponentAsync_Recipient();
        }else if (data==2){                          
            this.selected_data = {
                data: this.selectedOption.staff,
                code: this.selectedOption.uniqueid,
                option:1
            }            
            if ((this.selectedOption.staff=='!MULTIPLE' || this.selectedOption.staff=='!INTERNAL' || this.selectedOption.staff.trim()=='BOOKED'))
                    this.globalS.eToast('Day Manager',`No Staff to view detail!`)
            else                
                this.loadComponentAsync_Staff();
         }else if (data==3){                          
        
            this.serviceType  =this.selectedOption.activity;                    
            this.defaultStartTime  =this.selectedOption.startTime;
            this.notes=this.selectedOption.snotes;
            if (data!=null)
            this.ViewAdditionalModal=true;  
        
        }else if (data==4){          
                this.showDetail(this.selectedOption)   ;             
        }else if (data==5){          
            this.currentMonthRoster_Recipient()   ;             
        }else if (data==6){          
            this.currentMonthRoster_Staff()    ;             
       
         }
       
        
    });
    this.AllocateView.subscribe(data => {
        // console.log(data);
        this.optionsModal=false;
        if (data==1){ 
            //Un-Allocate staff
            this.operation='Un-Allocate'
            this.UnAllocate();
        }else  if (data==2){ 
            //Re-Allocate staff
            this.operation='Re-Allocate'
            this.AllocateStaffModal=true;
        
        }else  if (data==3){ 
            this.openCancelShift();
        }  else  if (data==4){ 
            this.openAIAssistant();
        }
    });

    this.ViewGroupShift.subscribe(data => {
        this.AllocateStaff=false;
        if (data==1){
            this.get_group_Shift_Setting();
            this.optionsModal=false;
            this.showGroupShiftRecipient=true;
        }else  if (data==2){
            this.AllocateStaff=true;
            this.optionsModal=false;
            this.openSearchStaffModal=true;
        }else  if (data==3){
            //this.get_group_Shift_Setting();
            this.optionsModal=false;          
            this.viewStaffAttendees_Timewise();
        
            
           
        }else  if (data==4){
            //this.get_group_Shift_Setting();
            this.optionsModal=false;
            
            this.viewStaffAttendees();
        
            
           
        }
    });

    this.ViewDMStatus.subscribe(data => {
       
        if (data==1){
            this.dmStatusType=1;
            this.ViewDMStatusModal=true;
            this.optionsModal=false;
        }else{
            this.dmStatusType=2;
            this.optionsModal=false;
            this.SaveDMStatus()

        }
        
        
    });

    
    this.ViewNudge.subscribe(data => {
        this.optionsModal=false;
        if (data==1)
            this.NudgeStatus="Up";
        else
            this.NudgeStatus="Down";

       this.ViewNudgeModal=true;
        
    });


    this.OperationView.subscribe(data => {
        // console.log(data);
        this.optionsModal=false;
        if (data==1){  
            //Copy Operation                        
           
           // this.toBePasted.push(this.selectedOption)
            this.operation="Copy"
            
        }else if (data==2){ 
            //Cut Operation                         
          
            this.operation="cut"
          //  this.toBePasted.push(this.selectedOption)
            
         }else if (data==3){                          
            //Paste Operation
            
            this.optionsModal=false;
            //this.pasting_records();
            this.pasteSelectedRecords(this.pastePosition);

        }else if (data==4){   
            //delete Operation 
            
            this.showConfirm();
            
        }  else if (data==5){          
            this.showConfirm_for_additional()   ;             
        
            }
        
    });

    this.ApproveView.subscribe(data => {
        this.optionsModal=false;
        if (data==1){ 
            this.ApproveUnApprove(true);
         }
        else{
            this.ApproveUnApprove(false);
        }
    });

}
private unsubscribe = new Subject();
buildForm() {
    this.DateTimeForm = this.formBuilder.group({
        recordNo: [''],
        rdate: [''],
        time: this.formBuilder.group({
            startTime:  [''],
            endTime:  [''],
        }),
        payQty: [''],
        billQty: ['']
        
    });

    this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    this.fixStartTimeDefault();   

    this.DateTimeForm.get('time.startTime').valueChanges.pipe(
        takeUntil(this.unsubscribe)
    ).subscribe(d => {
        
        this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    });
    this.DateTimeForm.get('time.endTime').valueChanges.pipe(
        takeUntil(this.unsubscribe)
    ).subscribe(d => {
        this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    });
   
}
masterCycle:any ='CYCLE-I';
cycles:any=[
        {cycle:'CYCLE-I', value:'1900/01/01'},
        {cycle:'CYCLE-II', value:'1900/01/07'},
        {cycle:'CYCLE-III', value:'1900/01/14'},
        {cycle:'CYCLE-IV', value:'1900/01/21'},
        ]
Master_Roster_label:string='Day Manager';
  setMasterRoster($event:any){
     this.dayView=7;
    //this.master=!this.master;
    this.master=$event;
    
    if (this.master) {   
      this.Master_Roster_label='Master Roster'   
      this.date='1900/01/01';
    }else {
        this.Master_Roster_label='Day Manager';
        this.date= new Date();
    }
    
    
  }
 
  openAIAssistant(){
    this.viewAIAssistant=true;
    this.loadAIData.next(this._highlighted);
  }
  loadStaffAvailabilityData: Subject<any> = new Subject();
  openStaffAvailability(){
    this.viewStaffAvailability=true;
    let data : any ={
        date: new Date('1900/01/01'),
        dayView:this.dayView
    }
    setTimeout(() => {
    this.loadStaffAvailabilityData.next(data)
    ,1000});
  }

 loadVehicleData: Subject<any> = new Subject();
 
  openVehicleAvailability(){
   
    this.viewVehicleAvailability=true;
    let data : any ={
        date: this.date,
        dayView:this.dayView
    }
    setTimeout(() => {
        this.loadVehicleData.next(data)
    ,1000});
   
}
  
  
  openPublicHoliday(){
    this.showPublicHoliday=true;
    // let sql="SELECT [DESCRIPTION] + ' ' + CONVERT(VARCHAR, CONVERT(DATETIME, [DATE]), 103) AS ph FROM PUBLIC_HOLIDAYS WHERE [DATE] BETWEEN convert(varchar,getDate(),111) AND  dateAdd(day,10,convert(varchar,getDate(),111)) ORDER BY [DESCRIPTION]"   ;

    // this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
    //     this.lstHolidays=data;
    // })
   
    this.timeS.getpublicholiday(""+this.dayView).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
              this.lstHolidays=data;
              this.cd.detectChanges();
    });
  }
  setDeletesRoster(event:any){
    this.deleted=event;
  }
  onKeyPress(event: any) {
    let record:any;
    if (event.keyCode === 13 && this.searchRecord!='') {
        this.dmManager.forEach((element: any) => {
            this.rosters = element.value;
            record= this.rosters.find(x=>x.recordno==this.searchRecord);
            if (record.recordno== toNumber(this.searchRecord)){
                this.showDetail(record);
                return;
            }
                
        });
        
    }
  }

  searchData(){

    if (this.searchRecord !='' ){
        this.CustomFilters.push({key:'RECORD NO', value:this.searchRecord});
        this.applyFilter.next(this.CustomFilters);
    }

  }

  onGroupRecipientChecked(event:any){
    this.selectedOption=event;
   
  }
  updateCheckedSet(id: any, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
    } else {
      this.setOfCheckedId.delete(id);
    }
  }
  refreshCheckedStatus(): void {
   
      //this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
    //  this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
  }
  onGroupItemChecked(id: any, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  setCycle(){
     let cycle= this.cycles.filter (x=>x.cycle==this.masterCycle)
     this.date=cycle[0].value;
     console.log(cycle);
  }
fixStartTimeDefault() {
    const { time } = this.DateTimeForm.value;
    if (!time.startTime) {
        this.ngModelChangeStart(this.defaultStartTime)
    }

    if (!time.endTime) {
        this.ngModelChangeEnd(this.defaultEndTime)
    }
}

ngModelChangeStart(event): void{
    this.DateTimeForm.patchValue({
        time: {
            startTime: event
        }
    })
}
ngModelChangeEnd(event): void{
    this.DateTimeForm.patchValue({
        time: {
            endTime: event
        }
    })
}
data(event:any){
   // console.log(event);
    this.dmManager=event;
}
pasted(event:any){
}
ApplyQuickFilter(){
    this.loading=true;
   // this.ApplyCustomFilter();
   // this.CustomFilters=[];
   this.menuAction();

 
  
    if (!this.AllBranches){
        this.LimitTo="BRANCH LIST"
        this.startWith = JSON.stringify(this.selectedBranches.map(x=>x.title));
        let fltr= this.CustomFilters.find(a => a.key == this.LimitTo && a.value==this.startWith)        
        if (!fltr)
            this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
    }
    if (!this.AllPrograms){
        this.LimitTo="PROGRAM LIST"
        this.startWith = JSON.stringify(this.selectedPrograms.map(x=>x.title));
        let fltr= this.CustomFilters.find(a => a.key == this.LimitTo && a.value==this.startWith)        
        if (!fltr)
            this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
    }
    if (!this.AllFundings){
        this.LimitTo="FUNDING LIST"
        this.startWith = JSON.stringify(this.selectedFundings.map(x=>x.title));
        let fltr= this.CustomFilters.find(a => a.key == this.LimitTo && a.value==this.startWith)        
        if (!fltr)
            this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
    }
    if (!this.AllTeams){
        this.LimitTo="TEAM LIST"
        this.startWith = JSON.stringify(this.selectedTeams.map(x=>x.title));
        let fltr= this.CustomFilters.find(a => a.key == this.LimitTo && a.value==this.startWith)        
        if (!fltr)
            this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
    }
    if (!this.AllActivities){
        this.LimitTo="ACTIVITY LIST"
        this.startWith = JSON.stringify(this.selectedActivities.map(x=>x.title));
        let fltr= this.CustomFilters.find(a => a.key == this.LimitTo && a.value==this.startWith)        
        if (!fltr)
            this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
    }
    this.applyFilter.next(this.CustomFilters);
     this.viewQuickFilter=false;
}
ApplyCustomFilter(){
    this.loading=true;
    this.applyFilter.next(this.CustomFilters);
    
}

AddCustomFilter(){
    if (this.addInExisting){
        let fltr= this.CustomFilters.find(a => a.key == this.LimitTo && a.value==this.startWith)        
        if (!fltr)
            this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
    }else{
        this.CustomFilters=[];
        this.CustomFilters.push({key:this.LimitTo, value:this.startWith});
        return;
    } 
   
}
CloseCustomFilter(){
    this.CustomFilter=false;
    
    
}
ViewCustomFilter(){
    this.ViewExistingFilter=true;
    
}


removeCustomFilter(){
    this.CustomFilter=false;   
    this.CustomFilters=[];
    
    this.applyFilter.next(this.CustomFilters)
}
showCustomFilter(){
    this.SortOrder=this.selectedPersonType;
    this.CustomFilter=!this.CustomFilter;
    this.lstGroup=[];
    this.lstGroup.push('STAFF');
    this.lstGroup.push('STAFF JOB CATEGORY');
    this.lstGroup.push('STAFF TEAM');
    this.lstGroup.push('RECIPIENT');
    this.lstGroup.push('RECIPIENT CATEGORY/REGION');
    this.lstGroup.push('ACTIVITY');
    this.lstGroup.push('PROGRAM');
    this.lstGroup.push('COORDINATOR');
    this.lstGroup.push('SERVICE ORDER/GRID NO');
    this.lstGroup.push('FUNDING');
    switch(this.SortOrder.toUpperCase()){
        case  'RECIPIENT MANAGEMENT':
            this.lstGroup.push('FACILITY');
            this.lstGroup.push('VEHICLE');
            this.lstGroup.push('ACTIVITY GROUP');
            break;
        case  'RECIPIENT MANAGEMENT' :
            this.lstGroup.push('FACILITY');
            this.lstGroup.push('VEHICLE');
            this.lstGroup.push('ACTIVITY GROUP');
            break;
        case "FACILITIES - RECIPIENTS":
             this.lstGroup.push('FACILITY');
             break;
        case  "FACILITIES - STAFF":
            this.lstGroup.push('FACILITY');
            break;
        case "TRANSPORT - RECIPIENTS":
             "TRANSPORT - STAFF"
             this.lstGroup.push('VEHICLE');
             break;
        case "TRANSPORT - STAFF":
            this.lstGroup.push('VEHICLE');
            break;
        case "GROUPS - RECIPIENTS":            
             this.lstGroup.push('ACTIVITY GROUP');
             break;
        case  "GROUPS - STAFF":
            this.lstGroup.push('ACTIVITY GROUP');
            break;  
   
    }
}

FillLimitTo(){
    let sql;
    this.lstStartWith=[];
    this.startWith='';
    this.showSimpleFilterInput=false;
    switch (this.LimitTo.toUpperCase())
    {
        
            case "STAFF":
                this.lstStartWith=[];
                this.nzFilterPlaceHolder="TYPE FIRST PART OF STAFF CODE";
                this.showSimpleFilterInput=true;
                return;
                
            case "RECIPIENT":
                this.nzFilterPlaceHolder="TYPE FIRST PART OF RECIP CODE";
                this.showSimpleFilterInput=true;
                return;
                
            case "STAFF JOB CATEGORY":
                sql=`SELECT UPPER([Description]) as Title FROM DataDomains WHERE [Domain] = 'STAFFGROUP' ORDER BY Description`;
                break;           
            case "STAFF TEAM":
                sql=`SELECT UPPER([Description]) as Title FROM DataDomains WHERE [Domain] = 'STAFFTEAM' ORDER BY Description`;
                break;                       
            case "RECIPIENT CATEGORY/REGION":
                sql=`SELECT UPPER([Description]) as Title FROM DataDomains WHERE [Domain] = 'GROUPAGENCY' ORDER BY Description`;
                break;            
            case "ACTIVITY":
                sql=`SELECT Title FROM ItemTypes WHERE   ProcessClassification = 'OUTPUT'`;
                break;
            case "ACTIVITY GROUP":
                sql=`SELECT UPPER([Description]) as Title FROM DataDomains WHERE [Domain] = 'ACTIVITYGROUPS' ORDER BY Description`;
                break;
            case "FACILITY":
                sql=`SELECT UPPER([Name]) as Title FROM CSTDAOutlets ORDER BY [Name]`;
                break;    
            case "PROGRAM":
                sql=`SELECT UPPER([Name]) as Title FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS'`;
                break;  
            case "COORDINATOR":
                sql=`SELECT UPPER([Description]) as Title FROM DataDomains WHERE [Domain] = 'CASE MANAGERS' ORDER BY Description`;
                break;  
            case "SERVICE ORDER/GRID NO":
                this.nzFilterPlaceHolder="TYPE IN GRID OR ORDER";
                this.showSimpleFilterInput=true;
             break;
            case "FUNDING":
                sql=`SELECT UPPER([Description]) as Title FROM DataDomains WHERE [Domain] = 'FUNDINGBODIES' ORDER BY Description`;
                break;  
            
            return;
                  
    }

    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.lstStartWith=data;
    });
    
}

showMakeUps(){
    let sql =`select * from MakeUpPays`;

    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.lstMakeUps=data;
       
    });
    this.viewMakeUps=true;
    
}
ViewQuickFilterDisplay(){
    this.loading=true;
    this.showCustomFilter();

    setTimeout(() =>{
        if ( this.branchesList==null || this.branchesList.length<=0)
            this.FillQuickFilterLists();
        this.loading=false;
        this.viewQuickFilter=true;
    },500)
    
}


FillQuickFilterLists() {
    let sql;
    let sql_branch=`Select Description as title From DataDomains Where Domain = 'BRANCHES'  ORDER BY Title`;
    let sql_funding=`SELECT [DESCRIPTION] as title FROM DATADOMAINS WHERE DOMAIN = 'FUNDINGBODIES'  ORDER BY Title `;
    let sql_program=`SELECT UPPER([Name]) as title FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' ORDER BY [Title]`;
    let sql_Team=`SELECT [DESCRIPTION] as title FROM DATADOMAINS WHERE DOMAIN = 'STAFFTEAM' ORDER BY Title`;   
    let sql_Activity=`SELECT Title as title FROM ItemTypes WHERE   ProcessClassification = 'OUTPUT' ORDER BY Title`;
    return forkJoin([
      this.listS.getlist(sql_branch),
      this.listS.getlist(sql_funding),    
      this.listS.getlist(sql_program),
      this.listS.getlist(sql_Team),
      this.listS.getlist(sql_Activity),
     
    ]).subscribe(x => {
      this.branchesList =   x[0];    
      this.fundingList  =   x[1];
      this.programsList =   x[2];
      this.teamList     =   x[3];
      this.activityList =   x[4];
      
    });
  }
 
  Check_BreachedRosterRules_Paste(action:string, record:any):any{

    let inputs_breach={
        sMode : 'Add', 
        sStaffCode: record.carercode, 
        sClientCode: record.recipient, 
        sProgram: record.rprogram, 
        sDate : record.date, 
        sStartTime :record.startTime, 
        sDuration : record.duration, 
        sActivity : record.activity,
        PasteAction : action=="cut" ? "Cut": "Copy"
    };

    return inputs_breach;// this.timeS.Check_BreachedRosterRules(inputs_breach);

    
}

pasteSelectedRecords(event:any){
    console.log(event);
    if (this.toBePasted==null || this.toBePasted==null)    return;     

    if (this._highlighted.length<=0)
        this._highlighted=this.toBePasted;
    
   
    // this._highlighted.forEach(function (value) {
    //     //console.log(value);
    //     value.date=moment(event.selected).format('YYYY/MM/DD');
    //     setTimeout(() => {
    //         this.pasting_records(value)                 
    //     }, 100);  
    //   });
   
    let dmType=event.selected.dmType;
    let recordsData:Array<any>=[];
    this.new_date=moment(event.selected.date).format('YYYY/MM/DD');
    
    for ( let v of this._highlighted){  
        this.selectedOption=v;
        v.date=this.new_date;

        recordsData.push(this.Check_BreachedRosterRules_Paste(this.operation,v))
     //this.pasting_records(v)                 
   
    }

    //(  function(next) {
       
        this.timeS.pastingRosters(JSON.stringify(recordsData)).subscribe(data=>{
            let res=data[0];
            if (res.errorValue>0){
                this.Error_Msg=res.errorValue +", "+ res.msg ;
                this.breachRoster=true; 
            }else{
               this.Pasting_Records(this.new_date)
            }

        })                        
   
}
Pasting_Records(rdate:any){
 
 
    for ( let v of this._highlighted){  
        this.selectedOption=v;
        v.date=rdate;//moment(rdate).format('YYYY/MM/DD');
                                                            
        if (this.operation==="cut"){
                    
            this.ProcessRoster("Cut",v,v.date);
            //this.remove_Cells(sheet,this.selectedOption.row,this.selectedOption.col,this.selectedOption.duration)
        }else  
            this.ProcessRoster("Copy",v,v.date);    
            
    }         
                      
}
onTextChangeEvent(event:any){
   // console.log(this.txtSearch);
    let value = this.txtSearch.toUpperCase();
    //console.log(this.serviceActivityList[0].description.includes(value));
    this.serviceActivityList=this.originalList.filter(element=>element.description.includes(value));
}

openStaffModal(OpenSearch:boolean=false){

    if (OpenSearch)
     this.OpenSearchOfStaff=OpenSearch;
    
     this.openSearchStaffModal=true;
    
    if (this.selectedOption!=null) //&& !OpenSearch
        this.booking = {
                     recipientCode: this.selectedOption.recipient,
                     userName:this.token.user,
                     date:this.selectedOption.date,
                     startTime:this.selectedOption.startTime,
                     endTime:this.selectedOption.endTime,
                     endLimit:'20:00'
                    };
                
    this.bookingData.next(this.booking) ; 
    this.openSearchStaffModal = true;
    // if (this._dynamicContainerStaffSearch==null)
    //      this.loadComponentAsync_StaffSerarch();  
    // else{
    //     this._dynamicContainerStaffSearch.instance.bookingData =this.bookingData;
    //     this._dynamicContainerStaffSearch.instance.findModalOpen=true;
    // }
}
onStaffSearch(data:any){
    this.openSearchStaffModal=false;
   
    this.selectedStaff=data.accountno;
    this.selectedCarer=this.selectedStaff
    if (this.OpenSearchOfStaff){
        this.LimitTo='STAFF';
        this.startWith=data.accountno;
        this.AddCustomFilter();
        this.ApplyCustomFilter();
    }
}

openRecipientModal(OpenSearch:boolean=false){

    if (OpenSearch)
     this.OpenSearchOfRecipient=OpenSearch;
    
     this.openSearchRecipientModal=true;
    
    if (this.selectedOption!=null ) //&& !OpenSearch
        this.booking = {
                     recipientCode: this.selectedOption.recipient,
                     userName:this.token.user,
                     date:this.selectedOption.date,
                     startTime:this.selectedOption.startTime,
                     endTime:this.selectedOption.endTime,
                     endLimit:'20:00'
                    };
                
   // this.bookingDataRecipient.next(this.booking) ;    
   if (this._ddynamicContainerRecipientSearch==null)
        this.loadComponentAsync_RecipientSerarch();  
    else{
        this._ddynamicContainerRecipientSearch.instance.bookingData =this.bookingDataRecipient;
        this._ddynamicContainerRecipientSearch.instance.findModalOpen=true;
    }
}
onRecipientSearch(data:any){
    this.openSearchRecipientModal=false;
    this.selectedRecipient=data.accountNo;
    if (this.OpenSearchOfRecipient){
        this.LimitTo='RECIPIENT';
        this.startWith=data.accountNo;
        this.AddCustomFilter();
        this.ApplyCustomFilter();
    }
}
openMenu(){
    this.showMenu=true;
}
menuAction(){
   
    this.showMenu=false;
    this.dayView= this.dayViewValue;
    //this.PayPeriodEndDate = this.PayPeriodEndDateValue;
    this.setPayPeriod();
    this.dmOptions.next({dmOption1:this.optionsList,dmOption2:this.optionsList2})
    console.log(this.optionsList)
    localStorage.setItem('dmOption1', JSON.stringify(this.optionsList));
    localStorage.setItem('dmOption2', JSON.stringify(this.optionsList2));
    if (this.PayPeriodEndDate!=null)
    localStorage.setItem('PayPeriodEndDate', this.PayPeriodEndDate.toString());

    this.loadTheme(this.Theme);
  
}
change(event:any){

}
loadNotes(){
    this.Person.id=this.selectedOption.recordno;
    this.Person.code=this.selectedOption.recipient;
    this.Person.personType="Recipient";
    this.Person.noteType="SVCNOTE";
    this.ViewServiceNoteModal=true;
    this.loadingNote.next(this.Person);
    }
    
 pasting_record_single(record:any) {
    
        if (record==null) return;

        this.Check_BreachedRosterRules_Paste(this.operation, record).subscribe(d=>{
            
            let res=d;
            if (res.errorValue>0){
               // this.globalS.eToast('Error', res.errorValue +", "+ res.msg);
               //this.addBookingModel=false;
                this.Error_Msg=res.errorValue +", "+ res.msg ;
                this.record_being_pasted=record;
                this.breachRoster=true;
                
            }else{
          
                if (this.operation==="cut"){
                    
                    this.ProcessRoster("Cut",record,record.date);
                    //this.remove_Cells(sheet,this.selectedOption.row,this.selectedOption.col,this.selectedOption.duration)
                }else  
                    this.ProcessRoster("Copy",record,record.date);   
            }
        
        })
       
  }
selected_roster(r:any):any{
    let rst:any;
      
    
    rst = {
        "shiftbookNo": r.recordno,
        "date": r.date,
        "startTime": r.starttime,
        "endTime":    r.endTime,
        "duration": r.duration,
        "durationNumber": r.dayno,
        "recipient": r.recipient,
        "program": r.rprogram,
        "activity": r.activity,
        "payType": {paytype : r.shiftType },   
        "paytype": r.shiftType,  
        "pay": {pay_Unit: r.costUnit,
                pay_Rate: r.payRate,
                quantity: r.payQty,
                position: ''
            },                   
        "bill":  {
                pay_Unit: r.billunit,
                bill_Rate: r.billRate,
                quantity: r.billQty,
                tax: r.taxAmount
            },           
        "approved": '0',
        "billto":r.billto,
        "debtor": r.billto,
        "notes": r.notes,
        "selected": false,
        "serviceType": r.type,
        "recipientCode": r.recipient,            
        "staffCode": r.carercode,  
        "serviceActivity": r.activity,
        "serviceSetting": r.servicesetting,
        "analysisCode": r.analysisCode,
        "serviceTypePortal": "",
        "daymask":r.daymask,       
        "recordNo": r.recordno,
        "status":r.status,
        "minorgroup" : r.minorgroup,
        tamode: r.tamode,
        tA_Multishift : r.tA_Multishift,
        tA_EXCLUDEGEOLOCATION : r.tA_EXCLUDEGEOLOCATION,
        tA_EXCLUDEFROMAPPALERTS : r.tA_EXCLUDEFROMAPPALERTS,     
        attendees: r.attendees,
        uniqueId : r.uniqueid,
        
    }
        
    

return rst;
}   

find_roster(RecordNo:number):any{
    let rst:any;
    for(var r of this.rosters)
   {
            if (r.recordNo == RecordNo){
                rst= r;
                break;
            }
        
    } 

    rst = {
        "shiftbookNo": r.recordno,
        "date": r.date,
        "startTime": r.startTime,
        "endTime":    r.endTime,
        "duration": r.duration,
        "durationNumber": r.dayno,
        "recipient": r.recipient,
        "program": r.rProgram,
        "activity": r.activity,
        "payType": r.shiftType ,  
        "paytype": r.payType,        
        "pay": {
            pay_Unit: r.costUnit,
            pay_Rate: r.payRate,
            quantity: r.payQty,
            position: ''
        },                   
        "bill":  {
            pay_Unit: r.billunit,
            bill_Rate: r.billRate,
            quantity: r.billQty,
            tax: r.taxPercent
        },           
        "approved": '0',
        "billto":'',
        "debtor": '',
        "notes": r.notes,
        "selected": false,
        "serviceType": r.type,
        "recipientCode": r.recipient,            
        "staffCode": r.carercode,  
        "serviceActivity": r.activity,
        "serviceSetting": r.servicesetting,
        "analysisCode": r.analysisCode,
        "serviceTypePortal": "",
        "recordNo": r.recordno
        
    }

return rst;
}   
cancelDMRoster(){
    
    this._dynamicRoster.instance.canDeactivate();
   
   // this.reload.next(true);
   // this.load_rosters();
}
load_rosters(refresh:boolean=false){
    
   
   // this.loading=true;
    this.reload.next(refresh);
    
}

reLoadGridRosterModel(){

    this.info.IsMaster=false;
    this.info.ViewType=this.viewType;
    if (this.selectedOption!=null)        {
        this.info.StaffCode=this.selectedOption.staff;
        this.info.date=this.selectedOption.date;
    }else  if (this.pastePosition!=null){
        this.info.StaffCode=this.pastePosition.selected.carercode;
        this.info.date= moment(this.pastePosition.selected.date).format('YYYY/MM/DD');
    }else{
        return;
    }
   
      this.AddRosterModel=false;      
     // this.openModal();
    // this.dmroster.ngOnInit();
    // this.dmroster.loadRosterData(this.info)
    // this.AddRosterModel=true;
   
  
}
async openModal() {

    const { DMRoster } = await import(
      /* webpackPrefetch: true */ 
      '../../roster/dm-roster'
    );
    
    const modalRef = this.modalService.create({
        nzTitle :'Day Manager',
        nzContent: DMRoster,
        nzWidth:470,
        nzStyle : {top: '15px' },
        nzFooter: [
            {
              label: "Cancel",
              type: "danger",
              nzOnOk: () => modalRef.close(),
              nzOnCancel: () => modalRef.close()
            }
          ]
    });
   
  }
  loadTheme(theme: string): void {
    switch (theme) {
        case 'DARK':
           this.styleUrl = './daymanager-main_dark.css';    
          break;
        case 'LIGHT':
          this.styleUrl = './daymanager-main_light.css';
          break;
        case 'CLASSIC':
          this.styleUrl = './daymanager-main.css';
          break;
      }

      const linkElement = this.document.createElement('link');
        // linkElement.rel = 'stylesheet';
        // linkElement.type = 'text/css';
        // linkElement.href =  this.styleUrl;
        // this.document.head.appendChild(linkElement);
       
       // const linkElement = this.renderer.createElement('link');
        linkElement.setAttribute('rel', 'stylesheet');
        linkElement.setAttribute('type', 'text/css');
        linkElement.setAttribute('href', this.styleUrl); // Provide the path to the CSS file
    // document.head.appendChild(linkElement)
        this.renderer.appendChild(this.document.head, linkElement);
  
}
ngOnInit(): void {
    
   // this.loadTheme('DARK');

    this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    //console.log(this.token);
   
    
    this.buildForm(); 
    this.getLocalStorage();
    this.selectedPersonType ='Staff Management';
    this.setPersonTypes();
    this.FillQuickFilterLists();

    this.loadMain.subscribe(data => {
        this.selectedPersonType ='Staff Management';
        
        this.setPersonTypes();
       // this.personTypeChange(null, 'Staff Management');
      // this.dmTypeSelected.emit(this.selectedPersonType);
    });
    this.listS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.PayPeriod=data;
        this.PayPeriodEndDate= data.end_Date ;
        this.PayPeriodEndDateValue=data.payPeriod_Length

        
      });
   
    
    
}
getLocalStorage(){
    let item1 =  localStorage.getItem('dmOption1');
    let item2 = localStorage.getItem('dmOption2');
    if (item1!=null )
      if (item1.length>0)
         this.optionsList =JSON.parse(item1);
     
     if (item2!=null )
       if (item2.length>0)
         this.optionsList2 =JSON.parse(item2);
    
         if (localStorage.getItem('PayPeriodEndDate')!=null)
            this.PayPeriodEndDate = localStorage.getItem('PayPeriodEndDate');
 
  }
  ngAfterViewInit(){
    
}

    ngOnDestroy(): void {
       
    }

    addDays(date: Date, days: number): Date {
        let result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
      }

setPayPeriod(){

   
    //this.PayPeriodEndDate= this.PayPeriodEndDate ;
    
    let input = {
       start_Date : format(addDays(new Date(this.PayPeriodEndDate),-this.PayPeriod.payPeriod_Length),'yyyy/MM/dd'),
       end_Date : format(new Date(this.PayPeriodEndDate),'yyyy/MM/dd'),
       payPeriod_Length : this.PayPeriodEndDateValue,
       payPeriodEndDate : format(new Date(this.PayPeriodEndDate),'yyyy/MM/dd')

    }
    
    
    this.listS.setpayperiod(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
       console.log(data);
        this.payperiodEmiter.emit(input)
      });
}
    Weekday(date:string) : string {

        let dt= new Date(date);
       // return  this.DayOfWeek(dt.toLocaleDateString('en-US', { weekday: 'long' }));
       return  dt.toLocaleDateString('en-US', { weekday: 'short' });
    
    }
    cancelRecipientExternal(value:any){
        this.recipientexternal=value.recipientexternal;
     }
     cancelStaffExternal(value:any){
        this.staffexternal=value.staffexternal;
     }
     SaveAdditionalInfo(notes:string){
        this.notes=notes;
        this.ViewAdditionalModal=false;
        this.ProcessRoster("Additional", this.selectedOption);
       
    }
    showDetail(data: any) {
        
        let rst:any = this.selected_roster(data);

        if (rst==null ) return;
        if ( rst.activity==null) return;

        if (this._dynamicInstance==null)
             this.loadComponentAsync(rst);
        else{
             this._dynamicInstance.instance.data =rst;
             this._dynamicInstance.instance.isVisible=true;
             this._dynamicInstance.instance.viewType =this.viewType;
             this._dynamicInstance.instance.editRecord=false;
             this._dynamicInstance.instance.ngAfterViewInit();
        }
        //this.roster_details(rst);
        
    }
    roster_details(index: any){
       
       // this.optionsModal=false;
        // this.detail.isVisible=true;
        // this.detail.data=index;
        // this.detail.viewType =this.viewType;
        // this.detail.editRecord=false;
        // this.detail.ngAfterViewInit();
        
    }
    navigationExtras: { state: { StaffCode: string; ViewType: string; IsMaster: boolean; }; };
    // navigationExtras = { 
    //     queryParams: { 
    //       StaffCode: '', 
    //       ViewType: 'Recipient', 
    //       IsMaster: false 
    //     } 
    //   };

    currentMonthRoster_Recipient() {
      //  localStorage.setItem('authToken', this.globalS.token); 
       // this.navigationExtras.queryParams.StaffCode = this.selectedRecipient;
       // this.router.navigate(["/admin/rosters"], this.navigationExtras)
    //    const fullUrl = this.router.serializeUrl(this.router.createUrlTree(["/admin/rosters"], this.navigationExtras)); 
    //    window.open(window.location.origin + fullUrl, "_blank");
    this.navigationExtras = {state: {StaffCode: this.user.code, ViewType: 'Recipient', IsMaster: false}};
    this.router.navigate(["/admin/rosters"], this.navigationExtras)

      }

      currentMonthRoster_Staff() {
       
        this.navigationExtras = {state: {StaffCode: this.user.code, ViewType: 'Staff', IsMaster: false}};
        this.router.navigate(["/admin/rosters"], this.navigationExtras)
      }
    showOptions(data: any) {
        console.log(data);
        if (data.selected.staff==null){
            this.pastePosition=data;
            this.optionsModal = true;
            this.displayOption=false;
            this.selectedOption=null;
            return
        }
        this.displayOption=true;
        this.selectedOption = data.selected; 
        this.selectedStaff=this.selectedOption.staff.trim();
        this.rosters=data.diary;
        var uniqueIds = this._highlighted.reduce((acc, data) => {
            acc.push(data.uniqueid);
            return acc;
        },[]);

        var sss = uniqueIds.length > 0 ? uniqueIds : [this.selectedOption.uniqueid]
    
        this.clientS.gettopaddress(sss)
            .subscribe(data => this.address = data)          

        if (this.selectedOption.recipient!='!INTERNAL' && this.selectedOption.recipient!='!MULTIPLE')
            this.ViewCancelShiftOption=true;
        else
            this.ViewCancelShiftOption=false;
        this.optionsModal = true;
        if (data.selected.carercode == 'BOOKED')
            this.mnuMarkInProgress=true;
        else
            this.mnuMarkInProgress=false;

        if (data.selected.type == 10 || data.selected.type ==11 || data.selected.type ==12)
            this.mnuGroupShift=true;
        else
            this.mnuGroupShift=false;
         
    }

    setPersonTypes(){
        this.personTypeList.push('Unallocated Bookings');
        this.personTypeList.push('-----------------------------------------');
        this.personTypeList.push('Staff Management');
        this.personTypeList.push('-----------------------------------------');
        this.personTypeList.push('Transport Recipients');
        this.personTypeList.push('Transport Staff');
        this.personTypeList.push('Transport Daily Planner');
        this.personTypeList.push('-----------------------------------------');
        this.personTypeList.push('Facilities Recipients');
        this.personTypeList.push('Facilities Staff');
        this.personTypeList.push('-----------------------------------------');
        this.personTypeList.push('Group Recipients');
        this.personTypeList.push('Group Staff');
        this.personTypeList.push('-----------------------------------------');
        this.personTypeList.push('Grp/Trns/Facility- Recipients');
        this.personTypeList.push('Grp/Trns/Facility-Staff');
        this.personTypeList.push('-----------------------------------------');
        this.personTypeList.push('Recipient Management');
         
       }
    personTypeChange(event:any, val:any){
        this.selectedPersonType=val;

        this.dmTypeSelected.emit(val);
     
        if (val=='Staff Management' || val.indexOf('Staff')>=0)
            this.viewType='Staff'
        else if (val=='Recipient Management' || val.indexOf('Recipient')>=0)
            this.viewType='Recipient'
       // this.reload.next(true);
       }

    highlighted(data: any) {
        this._highlighted = data;
        this.selectedOption=data[0];
    }

    shiftChanged(value:any){
        
        if (value.show_alert){
            this.showAlertForm('Change');
            this.load_rosters();
        }
     }
    
     showAlertForm(operationDone:string){


        if (operationDone=='Change'){
            if (this.viewType=='Staff'){
                this.txtAlertSubject= 'SHIFT DAY/TIME CHANGE : ';
                this.txtAlertMessage= 'SHIFT TIME CHANGE : \n Date: ' + this.selectedOption.date + '  \n Recipient: ' + this.selectedOption.recipient + '\n'  ,
                //this.clientCode=value.clientCode;      
            
                this.show_alert=true;
            }
        }else if (operationDone=='Add'){
            if (this.viewType=='Staff'){
                //let rst = this._highlighted.filter(x=>x.recordno==recordNo));
                let clientCode =this.selectedOption.recipient;
                let date= this.selectedOption.date
                
                this.txtAlertSubject = 'NEW SHIFT ADDED : ' ;
                this.txtAlertMessage = 'NEW SHIFT ADDED : \n' + date + ' : \n'  + clientCode + '\n'  ;
                this.show_alert=true;
               
            }
        }else if (operationDone=='Delete'){
            if (this.viewType=='Staff'){
                    
                let clientCode =this.selectedOption.recipient;
                let date= this.selectedOption.date
                this.txtAlertSubject = 'SHIFT DELETED : ' ;
                this.txtAlertMessage = 'SHIFT DELETED : \n' + date + ' : \n'  + clientCode + '\n'  ;
            
                this.show_alert=true;
            }
        }
     }
     
  
    numStr(n:number):string {
        let val="" + n;
        if (n<10) val = "0" + n;
        
        return val;
      }

     
      openCancelShift(){
        //this.ViewCancelShiftModal=true;
        //this.loadCancelShift.next({diary:this._highlighted,selected:this.selectedOption});
        if (this._dynamicCancelShift==null)
            this.loadComponentAsync_CancelShift();
        else {
            this._dynamicCancelShift.instance.loadCancelShift.next({diary:this._highlighted,selected:this.selectedOption});
            this._dynamicCancelShift.instance.view=true;
        }

      }
      CancelShiftDone(data:any){
        this.ViewCancelShiftModal=false;
        if (data==true)
            this.load_rosters();

            this._dynamicCancelShift=null;
      }

      AddRoster(view:number){
        this.info.IsMaster=false;
        this.info.ViewType=this.viewType;
        if (this.selectedOption!=null && view==1)        {
            this.info.StaffCode=this.selectedOption.staff;
            this.info.date=this.selectedOption.date;
            this.info.ViewType='Staff'
        } else  if (this.selectedOption!=null && view==2) {        
            this.info.StaffCode=this.selectedOption.recipient;
            this.info.date=this.selectedOption.date;
            this.info.ViewType='Recipient';
        } else  if (view==3) {        
            this.info.StaffCode=this.selectedOption.staff; //this.token.code
            this.info.date=this.selectedOption!=null ? this.selectedOption.date : moment(this.pastePosition.selected.date).format('YYYY/MM/DD');
            this.info.ViewType='Staff';
        } else  if (this.pastePosition!=null){
            if (view==2){
                this.info.StaffCode=this.pastePosition.selected.carercode;
                this.info.date=moment(this.pastePosition.selected.date).format('YYYY/MM/DD');
                this.info.ViewType='Recipient';
            }else{
                this.info.StaffCode=this.pastePosition.selected.carercode;
                this.info.date= moment(this.pastePosition.selected.date).format('YYYY/MM/DD');
            }
        }else{
            return;
        }
        if (this._dynamicRoster==null){           

             this.loadRosterComponent(this.info); 
            
        }else{
           
           
            this._dynamicRoster.instance.loadRoster.next(this.info);
          
            this._dynamicRoster.instance.ngAfterViewInit();  
            this.AddRosterModel=true;        
        }
          //this.loadingRoster.next(this.info); 
          this.optionsModal=false;
         
          //this.openModal();
        // 
         //this.dmroster.info = this.info;
         //this.dmroster.ngAfterViewInit();
      }

     
      ProcessRoster(Option:any, record:any, rdate:string="", start_Time:string=""):any {
        
        
        let dt= new Date(this.date);
        
        let date = dt.getFullYear() + "/" + this.numStr(dt.getMonth()+1) + "/" + this.numStr(dt.getDate());
        if (rdate!=""){
            date=rdate;
        }
        
        let inputs = {
            "opsType": Option,
            "user": this.token.user,
            "recordNo": record.recordno,
            "isMaster": this.master,
            "roster_Date" : record.date,
            "start_Time": record.startTime,
            "carer_code": this.operation=='Re-Allocate' ? this.selectedCarer : record.carercode,
            "recipient_code" :  record.recipient,
            "notes" : this.notes,
            'clientCodes' : record.recipient
        }
        this.timeS.ProcessRoster(inputs).subscribe(data => {        
        //this.deleteRosterModal=false;
            
            let res=data;       
            if (res.errorValue>0){
                this.globalS.eToast('Error', res.errorValue +", "+ res.msg);
                
                 if((Option=='Copy' || Option=='Cut') && (this._highlighted[this._highlighted.length-1].recordno==record.recordno))
                     this.load_rosters();
                return; 
            }
            
            if( Option=='Copy' ||Option=='Cut'){
                this.showAlertForm('Add')
                if (this._highlighted[this._highlighted.length-1].recordno==record.recordno){
                    this.load_rosters();
               
                }
            }else if (Option=='Delete'){
                this.showAlertForm('Delete')
                if (this._highlighted[this._highlighted.length-1].recordno==record.recordno){
                    this.load_rosters();
                   
                  
                }
                
            }else{
                this.load_rosters();
            }
                
    });
        
}
OpenChangeResources(type :number){
    this.optionsModal=false;
    let msg:string='';
    this.txtSearch='';
    switch(type){
        case 1:
            msg='You are running in administration mode - and have elected to force the dataset reporting code (output type) of all tagged shifts/activities to be changed to an alternative value. This is a utility function and bypasses standard error checking any validation against approved services - are you sure this is what you want to do and you wish to proceed??';    
            this.resourceType='Output';
            break;
        case 2:
            this.resourceType='Program';
            break;
        case 3:
            this.resourceType='Activity';
            break;
        case 4:
            this.resourceType='PayType';
            break;
        case 5:
            this.resourceType='Pay Quantity';
            break;
        case 6:
            this.resourceType='Unit Cost';
            break;
        case 7:
            this.resourceType='Debtor';
            break;
        case 8:
            this.resourceType='Bill Amount';
            break;
        case 9:
            this.resourceType='Bill Quantity';
            break;
        case 10:
            this.resourceType='Dataset Quantity';
            break;
    
        }
    if (msg=='')
        msg=`You are running in administration mode - and have elected to force the ${this.resourceType.toLowerCase()} of all tagged shifts/activities to be changed to an alternative value. This is a utility function and bypasses standard error checking any validation against approved services - are you sure this is what you want to do and you wish to proceed??`;
           
    this.showConfirm_for_Resources(type,msg);
     
   
}

ProcessChangeResources(type :number){    
  
    if (type>4 && type!=7) {

        this.InputMode = (type==6 || type==8) ? 'decimal' : 'number';
        this.ResourceValue=0;
        this.ViewAllocateResourceQtyModal=true;
        return;

    }
    let inputs={
        resourceType:this.resourceType,
        carerCode : this.selectedOption.staff,
        rDate: this.selectedOption.date,
        recipientCode : this.selectedOption.recipient
    }
    this.timeS.getDayManagerResources(inputs).subscribe(data => {     
        this.serviceActivityList=data;
        this.originalList=data;
        this.ViewAllocateResourceModal=true;;
        this.loading=false;
    });

}
openAllocateResource(){
    this.resourceType="Resource";
    this.txtSearch='';
    let inputs={
        resourceType:'Resource',
        carerCode : this.selectedOption.staff,
        rDate: this.selectedOption.date,
        recipientCode : this.selectedOption.recipient
    }
    this.timeS.getDayManagerResources(inputs).subscribe(data => {     
        this.serviceActivityList=data;
        this.originalList=data;
        this.ViewAllocateResourceModal=true;
    });
    this.optionsModal=false;
   
}
openCaseLoad(){
    this.optionsModal=false;
    this.ViewCaseLoadModal=true;
    

}
onItemSelected(sel: any, i:number): void {
   this.ServiceSetting=sel.description;    
    this.HighlightRow2=i;

}

onItemDbClick(sel:any , i:number) : void {

    this.HighlightRow2=i;
    this.ServiceSetting=sel.description;
    this.SaveAllocateResource();

}


SaveDMStatus(){
    let sql :any='';
    let records:any='0';
    if (this.dmStatusType==1){
         
        this._highlighted.forEach(v => {
          
       
            sql = sql + `INSERT INTO History (PersonID, Program, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, PublishToApp, Creator ) 
                            VALUES ('${v.recordno}', '', getDate(), 'INPROGRESS-NOTE : -${v.recordno}
                            ${this.ProgressNote}', 'SVCNOTE', 'INPROGRESS-NOTE', '${v.recordno}',  0, '${this.token.user}');
                            UPDATE Roster SET DMStat = 7 WHERE RecordNo IN (${v.recordno});
                            INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES ('${this.token.user}', getDate(), 'MARK IN PROGRESS', 'DAYMANAGER', '${v.recordno}', '${this.token.user}'); `; 
                        });

    }else if (this.dmStatusType==2){
       
        this._highlighted.forEach(v => {
            records=records +"," + v.recordno
        });
        sql = ` UPDATE Roster SET DMStat = 0 WHERE RecordNo IN (${records});
        INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES ('${this.token.user}', getDate(), 'CLEAR IN PROGRESS', 'DAYMANAGER', '0', '${this.token.user}') `;

    }
        

  

    this.listS.executeSql(sql).subscribe(data=>{
       // this.globalS.sToast("Day Manager","Record Updated Successfully");
        this.ViewDMStatusModal=false;
        this.optionsModal=false;
      
        this.load_rosters();
    });
}


SaveAllocateResource(){
    
    let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
        
    sql.TableName='Roster ';          
    
    switch(this.resourceType){ 
    case 'Resource':
        sql.SetClause=`set [ServiceSetting]='${this.ServiceSetting}' `;
        break;
    case 'Output':
        sql.SetClause=`set [HACCType]='${this.ServiceSetting}' `;
        break;
    case 'Program':
        sql.SetClause=`set [Program]='${this.ServiceSetting}' `;
        break;
    case 'Activity':
        sql.SetClause=`set [Service Type]='${this.ServiceSetting}' `;
        break;
    case 'PayType':
         sql.SetClause=` SET [Service Description] = '${this.ServiceSetting}', [Unit Pay Rate] = ISNULL((SELECT TOP 1 Amount FROM ItemTypes it WHERE PROCESSCLASSIFICATION = 'INPUT' AND  it.Title = '${this.ServiceSetting}'),0) `;
         break;
    case 'Debtor':
        sql.SetClause=` SET [BillTo] = '${this.ServiceSetting}' `;
        break;
    case 'Pay Quantity':
        sql.SetClause=` SET [CostQty] = ${this.ResourceValue} `;
        break; 
    case 'Unit Cost':
        sql.SetClause=` SET [Unit Pay Rate] = ${this.ResourceValue} `;
        break;   
    case 'Bill Amount':
        sql.SetClause=` SET  [Unit Bill Rate] = ${this.ResourceValue} `;
        break;   
    case 'Bill Quantity':
        sql.SetClause=` SET  [BillQty] = ${this.ResourceValue} `;
        break;   
    case 'Dataset Quantity':
        sql.SetClause=` SET  [DatasetQty] = ${this.ResourceValue} `;
        break; 
    
    default :
    
    }    
    
    sql.WhereClause=` where RecordNo=${this.selectedOption.recordno} `;

  

    this.listS.updatelist(sql).subscribe(data=>{
       // this.globalS.sToast("Day Manager","Record Updated Successfully");
        this.ViewAllocateResourceModal=false;
        this.ViewAllocateResourceQtyModal=false;
        this.load_rosters();
    });
}
SaveCaseLoad(){
    let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
        
    sql.TableName='Roster ';          
    
        sql.SetClause=`set [Shiftname]='${this.CaseLoadValue}' `;
    
   sql.WhereClause=` where RecordNo=${this.selectedOption.recordno} `;

  

    this.listS.updatelist(sql).subscribe(data=>{
       // this.globalS.sToast("Day Manager","Record Updated Successfully");
        this.ViewCaseLoadModal=false;
      //  this.load_rosters();
    });

}
SaveNudgeUpDown(){
    
        let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
            
            sql.TableName='Roster ';          
            if (this.NudgeStatus=="Up")
                sql.SetClause=`set [start time]=left(dateAdd(minute,-${this.NudgeValue}, convert(time,[start time],111)),5)`;
            else
                sql.SetClause=`set [start time]=left(dateAdd(minute,${this.NudgeValue}, convert(time,[start time],111)),5)`;

           sql.WhereClause=` where RecordNo=${this.selectedOption.recordno} `;
       
          

    this.listS.updatelist(sql).subscribe(data=>{
        //this.globalS.sToast("Day Manager","Record Updated Successfully");
        this.ViewNudgeModal=false;
        //this.load_rosters();
        this.reload.next(true);
    });
  
}
ApproveUnApprove(Approve:boolean){
    
    let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
    let recordnos:string="0";  
    this._highlighted.forEach(v => {  
        recordnos=recordnos +"," + v.recordno      
    });
        sql.TableName='Roster ';          
        if (Approve)
            sql.SetClause=`set [status]=2`;
        else
            sql.SetClause=`set [status]=1`;

       sql.WhereClause=` where RecordNo in (${recordnos}) `;   
      
        
            this.listS.updatelist(sql).subscribe(data=>{
            //  this.globalS.sToast("Day Manager","Record Updated Successfully");
                //this.ViewNudgeModal=false;
                this.load_rosters();
            
            });
       
  
}

showConfirm_for_Resources(type :number, msg:string): void {
    //var deleteRoster = new this.deleteRoster();
    this.modalService.confirm({
      nzTitle: 'Confirm',
      nzContent: msg,
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
      new Promise((resolve,reject) => {
        setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
       
        this.ProcessChangeResources(type);
      }).catch(() => console.log('Oops errors!'))

      
    });
  }

showConfirm_for_additional(): void {
    //var deleteRoster = new this.deleteRoster();
    this.modalService.confirm({
      nzTitle: 'Confirm',
      nzContent: 'Are you sure you want to delete roster notes',
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
      new Promise((resolve,reject) => {
        setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
        this.deleteAdditionalInfo();
       
      }).catch(() => console.log('Oops errors!'))

      
    });
  }

deleteAdditionalInfo(){
    
    let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
        
        sql.TableName='Roster ';          
       
        sql.SetClause=`set notes=null`;

       sql.WhereClause=` where RecordNo=${this.selectedOption.recordno} `;
   
      

        this.listS.updatelist(sql).subscribe(data=>{
            this.globalS.sToast("Day Manager","Record Updated Successfully");
            this.ViewNudgeModal=false;
            this.load_rosters();
        });
}
generate_alert(){
    this.show_alert=false;
    this.notes= this.txtAlertSubject + "\n" + this.txtAlertMessage;
    
    this.ProcessRoster("Alert",this.selectedOption);
}

SaveDayTime(){
    const tvalues= this.DateTimeForm.value;
    let time= format(tvalues.time.startTime,'HH:mm')

        let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
            
            sql.TableName='Roster ';      

           sql.SetClause=`set [date]='${tvalues.rdate}',[start Time]='${time}'
                        ,Duration=${this.durationObject.duration}
                        ,CostQty=${tvalues.payQty}
                        ,BillQty=${tvalues.billQty} `           

           sql.WhereClause=` where RecordNo=${tvalues.recordNo} `;     
          

    this.listS.updatelist(sql).subscribe(data=>{
       // this.globalS.sToast("Day Manager","Record Updated Successfully");
        this.ViewChangeDayTimeModal=false;
        this.load_rosters();
    });
    
}

// Check_BreachedRosterRules(){

//     let inputs_breach={
//         sMode : 'Edit', 
//         sStaffCode: this.selectedOption.carerCode, 
//         sClientCode: this.selectedOption.recipient, 
//         sProgram: this.selectedOption.rprogram, 
//         sDate : this.selectedOption.date, 
//         sStartTime :this.selectedOption.startTime, 
//         sDuration : this.selectedOption.duration, 
//         sActivity : this.selectedOption.activity,
//         // sRORecordno : '-', 
//         // sState : '-', 
//         // bEnforceActivityLimits :0, 
//         // bUseAwards:0, 
//         // bDisallowOT :0, 
//         // bDisallowNoBreaks :0, 
//         // bDisallowConflicts :0, 
//         // bForceNote :0, 
//         // sOldDuration : '-', 
//         // sExcludeRecords : '-', 
//         // bSuppressErrorMessages  :0, 
//         // sStatusMsg : '-',
//         // PasteAction :'-'
//     };

//     this.timeS.Check_BreachedRosterRules(inputs_breach).subscribe(data=>{
//         let res=data
//         if (res.errorValue>0){
//            // this.globalS.eToast('Error', res.errorValue +", "+ res.msg);
//            //this.addBookingModel=false;
//             this.Error_Msg=res.errorValue +", "+ res.msg ;
//             this.breachRoster=true;
//             return; 
//         }else{
//             //this.AddRoster_Entry();
//         }
         
//     });
// }

    showConfirm(): void {
        //var deleteRoster = new this.deleteRoster();
        this.modalService.confirm({
        nzTitle: 'Confirm',
        nzContent: 'Are you sure you want to delete roster',
        nzOkText: 'Yes',
        nzCancelText: 'No',
        nzOnOk: () =>
        new Promise((resolve,reject) => {
            setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
            this.deleteRoster();
        
        }).catch(() => console.log('Oops errors!'))

        
        });
    }
    getStatus(status:string){
        let val: string;
       
        switch(parseInt(status)){
            case 1:
                val="Unapproved"
                break
            case 2:
                val="Approved"
                break
            case 3:
                val="Billed"
                break
            case 4:
                val="Paid and Billed"
                break
            case 5:
                val="Paid"
                break
            default:
                val="Undefined"       
        }
    
        return val;
    }
    deleteRoster(){
        for ( let v of this._highlighted){  
            this.selectedOption=v; 
           
            if (v.status!=1){
                let sts=this.getStatus(v.status)
                this.globalS.eToast("Day Manager", `${sts}  roster (${v.startTime}-${v.endTime}) cannot be deleted`);
                continue;
            }    
            setTimeout(() => {
                this.ProcessRoster("Delete",v);                
            }, 100);   
        }
    }

    handleCancel(): void{
            this.optionsModal = false;
            this.recipientDetailsModal = false;
    }
    
    toMap(uniqueId:any){

     
    
        this.clientS.gettopaddress(uniqueId)
            .subscribe(data => { 
                this.address = data      

                if(this.address.length > 0){         

                    var adds = this.address.reduce((acc,data) => {
                        var { postCode, address1, suburb, state } = data;
                        var address = new Address(postCode, address1, suburb, state);
                        return acc.concat('/',address.getAddress());                
                    }, []);
                    console.log(adds)
                    console.log(adds.join(''))
                        
                    //window.open('https://www.google.com/maps/search/?api=1&query=' + encoded,'_blank');
                    window.open(`https://www.google.com/maps/dir${adds.join('')}`,'_blank');
                    return false;
                }
        this.globalS.eToast('No address found','Error');
        });
        
    }

    
Cancel_ProceedBreachRoster(){
    this.breachRoster=false;
    if (this.operation=='copy' ||this.operation=='cut'){
        //this.load_rosters();
      //  this.pasting=false;
    }
   // this.isPaused=false;
}
ProceedBreachRoster(){
    this.breachRoster=false;
    if (this.token.role!= "ADMIN USER")
    {
        this.globalS.eToast("Permission Denied","You don't have permission to add conflicting rosters");
        return; 
    }
    this.Pasting_Records(this.new_date);

 //   if(this.record_being_pasted==null) return;

    // if (this.operation==="cut"){
                    
    //     this.ProcessRoster("Cut",this.record_being_pasted,this.record_being_pasted.date);
    //     //this.remove_Cells(sheet,this.selectedOption.row,this.selectedOption.col,this.selectedOption.duration)
    // }else  
    //     this.ProcessRoster("Copy",this.record_being_pasted,this.record_being_pasted.date);   

    //this.isPaused=false;

   // if (this.operation=='copy' ||this.operation=='cut'){
        // if (this.operation=="cut"){
        //     this.ProcessRoster("Cut",this.current_roster.recordNo,this.rDate);
        //     this.remove_Cells(sheet,this.copy_value.row,this.copy_value.col,this.copy_value.duration)
        // }else
        //     this.ProcessRoster("Copy",this.current_roster.recordNo,this.rDate);  
   //     this.pasting=false;
     //   this.Pasting_Records()
    // }else{
    //     this.AddRoster_Entry();
    // }

}


UnAllocate(){
    
    if (this.selectedOption==null || this.selectedOption.RecordNo==0) return;
    //this.OpenAllocateStaffComponent=true;
   
    //this.loadUnallocateStaff.next({diary:this._highlighted,selected:this.selectedOption});
  
     //this.ProcessRoster("Un-Allocate", this.selectedOption);

    if (this._dynamicUnallocateStaff==null)
      this.loadComponentAsync_UnallocateStaff();
      
    else{
        this._dynamicUnallocateStaff.instance.loadUnallocateStaff.next({diary:this._highlighted,selected:this.selectedOption});
        this._dynamicUnallocateStaff.instance.findModalOpen=true;
        if (this.operation='Re-Allocate')
            this._dynamicUnallocateStaff.instance.reallocate=true;
    }

    
 }
 
reAllocate(){
   
    this.AllocateStaffModal=false;
    for ( let v of this._highlighted){  
        this.selectedOption=v;      
        setTimeout(() => {
          
            this.ProcessRoster("Re-Allocate", v);   
                     
        }, 100);   
        
    }
    if (this.selectedOption.carercode!='BOOKED'){
        this.UnAllocate(); 
    }
   
    this.load_rosters();
}
reloadVal: boolean = false;
sample:string;
set_reload(reload: boolean){
    
    this.reloadVal = !this.reloadVal;

}
listChange(event: any) {

    if (event == null) {
        this.user = null;
        this.isFirstLoad = false;
        this.sharedS.emitChange(this.user);
        return;
    }

   
      this.selectedCarer=event.accountNo;
     

    this.user = {
        code: event.accountNo,
        id: event.uniqueID,
        view: event.view,
        agencyDefinedGroup: event.agencyDefinedGroup,
        sysmgr: event.sysmgr
    }

    
    if (this.viewType=='Recipient'){
       // this.getPublicHolidyas(this.selectedCarer);

    }
    this.sharedS.emitChange(this.user);
    //this.cd.detectChanges();
}

onItemChecked(data: any, checked: boolean, type:string): void {
    if (type=='Branch'){
        if (checked)
            this.selectedBranches.push(data)
        else           
            this.selectedBranches.splice(this.selectedBranches.indexOf(data),1)
        
    }else if (type=='Program'){
        if (checked)
            this.selectedPrograms.push(data)
        else
            this.selectedPrograms.splice(this.selectedPrograms.indexOf(data),1)    
    }else if (type=='Activity'){
        if (checked)
            this.selectedActivities.push(data)
        else
            this.selectedActivities.splice(this.selectedActivities.indexOf(data),1)    
    }
    else if (type=='Funding'){
        if (checked)
            this.selectedFundings.push(data)
        else
            this.selectedFundings.splice(this.selectedActivities.indexOf(data),1)    
    } else if (type=='Team'){
        if (checked)
            this.selectedTeams.push(data)
        else
            this.selectedTeams.splice(this.selectedActivities.indexOf(data),1)    
    }
    //console.log(this.selectedPrograms)
  }

  onAllChecked(checked: boolean, type:string): void {
     if (type=='Branch') {
        if (checked) 
            this.branchesList.forEach(d => {
                this.selectedBranches.push(d)
            });
        else
            this.selectedBranches=[];
     }else if (type=='Program') {
        if (checked) 
            this.programsList.forEach(d => {
                this.selectedPrograms.push(d)
            });
        else
            this.selectedPrograms=[];
     }
    else if (type=='Activity') {
        if (checked) 
            this.activityList.forEach(d => {
                this.selectedActivities.push(d)
            });
        else
            this.selectedActivities=[];
     } else if (type=='Funding') {
        if (checked) 
            this.activityList.forEach(d => {
                this.selectedFundings.push(d)
            });
        else
            this.selectedFundings=[];
     } else if (type=='Team') {
        if (checked) 
            this.activityList.forEach(d => {
                this.selectedTeams.push(d)
            });
        else
            this.selectedTeams=[];
     }
  }


  FinalizePreview(){  
    var Rpt_id;    
    switch(this.selectedPersonType){ 

        case 'Unallocated Bookings':
            Rpt_id = "rRgGdgvy4Nt5mDTN"
            break;
        case 'Staff Management':
            Rpt_id = "uuYk4H7fejpfFQWj"
            break;
        case 'Transport Recipients':
            Rpt_id = "Qxstgu75xKWMfN5p"
            break;
        case 'Transport Staff':
            Rpt_id = "innxbrye8u7cANmu"
            break;            
        case 'Facilities Recipients':
            Rpt_id = "8XPndoTseQDZs3Gb"
            break;
        case 'Facilities Staff':
            Rpt_id = "2hez0pnfqp1dvp7j"
            break;        
        case 'Group Recipients':
            Rpt_id = "3KmA4VZx8o6VFaFJ"
            break;
        case 'Group Staff':
            Rpt_id = "59dFuwTnmXyc3KJH"
            break; 
        case 'Recipient Management':
            Rpt_id = "rJvnfXqI1AWo0gCs"
            break;
        case 'Grp/Trns/Facility-Staff':
            Rpt_id = "adE1XIUsDP1NKC0P"
            break;
        case 'Grp/Trns/Facility- Recipients':
            Rpt_id = "1IhubIp1q0zWYNtM"
            break;            

        

        default:
            console.log(this.selectedPersonType)
        break;

    }

    
   
            // console.log(s_Sql,lblcriteria)
             const data = {
                 "template": { "_id": Rpt_id },
                 "options": {
                     "reports": { "save": false },
                     "sql_inclusion": format(this.date, 'yyyy/MM/dd'),
                     "Criteria":format(this.date, 'yyyy/MM/dd'),
                     "userid":this.token.user,
                 }
             }
            this.Spiner_loading = true;
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
            // FOR PRINT
                this.drawerVisible = false;                                     
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);                                                               
                this.PrintPDFView(fileURL); 
                 
            /*For Preview
                this.pdfTitle = "Daymanger.pdf"
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false; */
                                                                           
           }, err => {
            console.log(err);
            this.loading = false;
            this.modalService.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
         });
         return;
         
        }
           
  
    PrintPDFView(fileURL){
        const iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = fileURL;
                    iframe.name =  "iFrmDMPrint";
                    document.body.appendChild(iframe);
                    iframe.contentWindow.print();
                    this.tryDoctype = "";
    }
    
    handleDrawerCancel(){
        this.drawerVisible = false;
        this.tryDoctype = "";
    } 
    
    UnallocateStaffDone(data:any){
       
        this.AllocateStaffModal=false;
        this.OpenAllocateStaffComponent=false;

        if (data==false) return;
        this.load_rosters(true);

    //   { for ( let v of this._highlighted){  
    //         this.selectedOption=v;      
    //         this.ProcessRoster("Un-Allocate", v);              
              
    //     }}() =>{
    //         this.load_rosters();
    //     } 
        
    }  

    addGroupShiftRecipients(){

        
        this.RecipientList.filter(x => this.setOfCheckedId.has(x.accountNo)).forEach(x => {
            console.log(x.accountNo)
            let inputs={
                date: this.selectedOption.date,
                startTime: this.selectedOption.startTime,
                serviceSetting: this.selectedOption.servicesetting,
                activity: this.selectedOption.activity,
                recipient: x.accountNo,
                carercode: '!MULTIPLE',
                notes: this.selectedOption.notes,
                billQty: this.selectedOption.billQty ,
                billTo: x.accountNo,
                billUnit: this.selectedOption.billunit,
                blockNo: this.selectedOption.blockNo,
                carerCode: '!MULTIPLE',
                clientCode: x.accountNo,
                costQty: this.selectedOption.billQty,
                costUnit: this.selectedOption.costUnit,  
                duration: this.selectedOption.duration,
                groupActivity: false,
                haccType: this.selectedOption.httype || "",
                dayno: this.selectedOption.dayno,
                monthNo: this.selectedOption.monthno,
                yearNo: this.selectedOption.yearno,
                program: this.selectedOption.rprogram,
                serviceDescription:  this.selectedOption.paytype,                
                serviceType: this.selectedOption.activity || "",
                paytype: this.selectedOption.paytype,
                anal: this.selectedOption.analysisCode,
                staffPosition:  "",              
                status: this.selectedOption.status,
                taxPercent: this.selectedOption.taxPercent || 0,
                transferred: 0,            
                type: this.selectedOption.type,
                unitBillRate: this.selectedOption.billRate ,
                unitPayRate: this.selectedOption.payRate || 0,              
                shiftname: this.selectedOption.shiftname,
                recordNo: this.selectedOption.recordno,                
                creator: this.token.user,
                datasetClient :  x.accountNo
            }
            this.timeS.posttimesheet(inputs).subscribe(data => {
               // this.globalS.iToast('Success', 'Record Added Successfully');
                this.showGroupShiftRecipient=false;  
                
                let history:any ={
                    operator: this.token.user,
                    actionDate:format(this.today,'yyyy/MM/dd HH:MM:ss'),
                    auditDescription:'CREATE NEW ROSTER ENTRY',
                    actionOn:'WEB-ROSTER',
                    whoWhatCode:data,
                    traccsUser:this.token.user
                }

                this.timeS.postaudithistory(history).subscribe(d=>{
                    this.reload.next(true)
                });
                
                 
        });
    });

       
       // this.doneBooking();
    
        
      }
    
  cancel_GroupShift(){
    
    this.showGroupShiftRecipient=false
    
  }
  
  get_group_Shift_Setting()
{
   
    this.GET_GROUP_RECIPIENTS().subscribe(d=>{
        this.RecipientList=d;//.map ( x => x.accountNo);
        this.showGroupShiftRecipient=true;
        this.setOfCheckedId.clear();
    })
    // this.GET_ADDRESS().subscribe(d=>{
    //     this.addressList=d.map ( x => x.address);             
      
    // })

    // this.GET_MOBILITY().subscribe(d=>{
    //     this.mobilityList=d.map ( x => x.description);             
      
    // })

    
}

GET_GROUP_RECIPIENTS(): Observable<any>{
        
    let sql =`SELECT DISTINCT [Recipients].[UniqueID], [Recipients].[AccountNo], [Recipients].[Surname/Organisation], [Recipients].[FirstName], [Recipients].[DateOfBirth], [Recipients].[pSuburb]  
                FROM Recipients INNER JOIN ServiceOverview ON Recipients.UniqueID = ServiceOverview.Personid
                WHERE ServiceOverview.[Service Type] = '${this.selectedOption.activity}' AND ServiceOverview.ServiceStatus = 'ACTIVE'
                AND  AccountNo not in  ('!INTERNAL','!MULTIPLE') AND admissiondate is not null AND (DischargeDate is null)  
                AND NOT EXISTS (SELECT * FROM  ROSTER 
                 WHERE Date = '${this.selectedOption.date}' AND [Start Time] = '${this.defaultStartTime}' AND ServiceSetting = '${this.selectedOption.serviceSetting}' 
                AND Roster.[Client Code] = Recipients.AccountNo ) 
                ORDER BY AccountNo`;
    

       // console.log(sql);
    return this.listS.getlist(sql);
}  
 
AddStaffInGroupShift(data:any){
    
    this.RecipientList=data
    //console.log(data);
    this.openSearchStaffModal=false;

    this.RecipientList.forEach(x => {
        // console.log(x.accountNo)
         let inputs={
             date: this.selectedOption.date,
             startTime: this.selectedOption.startTime,
             serviceSetting: this.selectedOption.servicesetting,
             activity: this.selectedOption.activity,
             recipient: '!MULTIPLE',
             carercode: x,
             clientCode: '!MULTIPLE',
             notes: this.selectedOption.notes,
             billQty: this.selectedOption.billQty ,
             billTo: x,
             billUnit: this.selectedOption.billunit,
             blockNo: this.selectedOption.blockNo,   
             costQty: this.selectedOption.billQty,
             costUnit: this.selectedOption.costUnit,  
             duration: this.selectedOption.duration,
             groupActivity: false,
             haccType: this.selectedOption.httype || "",
             dayno: this.selectedOption.dayno,
             monthNo: this.selectedOption.monthno,
             yearNo: this.selectedOption.yearno,
             program: this.selectedOption.rprogram,
             serviceDescription:  this.selectedOption.paytype,                
             serviceType: this.selectedOption.activity || "",
             paytype: this.selectedOption.paytype,
             anal: this.selectedOption.analysisCode,
             staffPosition:  "",              
             status: this.selectedOption.status,
             taxPercent: this.selectedOption.taxPercent || 0,
             transferred: 0,            
             type: this.selectedOption.type,
             unitBillRate: this.selectedOption.billRate ,
             unitPayRate: this.selectedOption.payRate || 0,              
             shiftname: this.selectedOption.shiftname,
             recordNo: this.selectedOption.recordno,                
             creator: this.token.user,
             datasetClient :  ''
         }
         this.timeS.posttimesheet(inputs).subscribe(data => {
           //  this.globalS.iToast('Success', 'Record Added Successfully');
             this.showGroupShiftRecipient=false;  
             
             let history:any ={
                 operator: this.token.user,
                 actionDate:format(this.today,'yyyy/MM/dd HH:MM:ss'),
                 auditDescription:'CREATE NEW ROSTER ENTRY',
                 actionOn:'WEB-ROSTER',
                 whoWhatCode:data,
                 traccsUser:this.token.user
             }

             this.timeS.postaudithistory(history).subscribe(d=>{
                this.reload.next(true)
             });
         });        
              
 
 });


}

lstRecipientInGroupShift:Array<any>=[];
lstStaffInGroupShift:Array<any>=[];

viewStaffAttendees_Timewise(){
    let sql=`SELECT ro.RecordNo, IsNull(st.FirstName, '') + ' ' + Isnull(st.[LastName], '') AS StaffName FROM Roster ro INNER JOIN Staff st ON ro.[Carer Code] = st.[AccountNo]WHERE DATE = '2024/03/07' AND ServiceSetting = 'COMMUNITY PROGRAMS AGEING PARENT CARERS' AND [Carer Code] > '!z' AND [Start Time] = '09:00'`
   
  let sql2=`SELECT ro.RecordNo, IsNull(re.FirstName, '') + ' ' + Isnull(re.[Surname/Organisation], '') AS ClientName FROM Roster ro INNER JOIN Recipients re ON ro.[Client Code] = re.[AccountNo]WHERE DATE = '2024/03/07' AND ServiceSetting = 'COMMUNITY PROGRAMS AGEING PARENT CARERS' AND [Client Code] > '!z' AND [Start Time] = '09:00'`

    this.listS.getlist(sql).subscribe(data=>{
        this.lstStaffInGroupShift=data;
    });

    this.listS.getlist(sql2).subscribe(data=>{
        this.lstRecipientInGroupShift=data;

        this.showGroupShiftAttendees=true;
    });
    

  }

  viewStaffAttendees(){
    

    let sql=`SELECT ro.RecordNo, IsNull(st.FirstName, '') + ' ' + Isnull(st.[LastName], '') AS StaffName FROM Roster ro INNER JOIN Staff st ON ro.[Carer Code] = st.[AccountNo]WHERE DATE = '2024/03/07' AND ServiceSetting = 'COMMUNITY PROGRAMS AGEING PARENT CARERS' AND [Carer Code] > '!z'`
    let sql2=`SELECT ro.RecordNo, IsNull(re.FirstName, '') + ' ' + Isnull(re.[Surname/Organisation], '') AS ClientName FROM Roster ro INNER JOIN Recipients re ON ro.[Client Code] = re.[AccountNo]WHERE DATE = '2024/03/07' AND ServiceSetting = 'COMMUNITY PROGRAMS AGEING PARENT CARERS' AND [Client Code] > '!z'`

    this.listS.getlist(sql).subscribe(data=>{
        this.lstStaffInGroupShift=data;
    });

    this.listS.getlist(sql2).subscribe(data=>{
        this.lstRecipientInGroupShift=data;
        this.showGroupShiftAttendees=true;
    });

  }

RollbackAIAllocation(){

    this.globalS.setTheme(this.Theme);
  
}

currentTheme:string;
styleUrl:any;
  switchTheme(theme: string): void {
    this.currentTheme = theme;
    let sheetPath='./daymanager_dark.css';
    let themeLink= document.getElementById('daymanager') as HTMLLinkElement;

    if (themeLink){
        sheetPath= './daymanager_' + theme + '.css';
    }else
    sheetPath= './daymanager.css';

         themeLink.rel = 'stylesheet';
         themeLink.href = sheetPath;
         themeLink.id = 'daymanager-theme';
         this.renderer.appendChild(document.head, themeLink);
         this.renderer.setAttribute(themeLink, 'href', sheetPath);

          sheetPath='.src/sass/styles.css';
          themeLink= document.getElementById('app-theme') as HTMLLinkElement;
     
         if (themeLink){
             sheetPath= './src/sass/styles_' + theme + '.css';
         }else
            sheetPath= './src/sass/styles.css';

         themeLink.rel = 'stylesheet';
         themeLink.href = sheetPath;
         themeLink.id = 'app-theme';
         this.renderer.appendChild(document.head, themeLink);
         this.renderer.setAttribute(themeLink, 'href', sheetPath);

         if (theme) return;

 

    // Remove the existing theme link element if any
    const existingLink = document.getElementById('daymanager-theme') as HTMLLinkElement;
  

    if (existingLink) {
        existingLink.href = sheetPath;
    }

    // const head =  document.getElementsByTagName('head')[0];
    // const style = document.createElement('link');
    //     style.id = 'css-styling';
    //     style.rel = 'stylesheet';
    //     style.href = 'daymanager_dark.css';
    //     head.appendChild(style);

    // // Create a new link element for the theme
    // const link = this.renderer.createElement('link');
    // this.renderer.setAttribute(link, 'rel', 'stylesheet');
    // this.renderer.setAttribute(link, 'id', 'theme-link');
    const link = this.renderer.createElement('link');
    switch (theme) {
      case 'DARK':
        // this.renderer.setAttribute(link, 'href', 'daymanager_dark.css');
        // this.style1 = "{'background-color':master === true ? 'black' : '#313030', margin:'7px' , padding-left: '2px', padding-top: '4px', font-size: '15px', color: 'white', height: '32px', width:'80px', text-align: 'center', vertical-align: 'middle', border-top-left-radius: '3px', border-bottom-left-radius: '3px' }";
        // this.style2 = "{'background-color':master === false ? '#313030' : 'black'  , padding-left: '2px', padding-top: '4px', font-size: '15px', color: 'white', height: '32px', width: '80px', text-align: 'center', vertical-align: 'middle', border-top-right-radius: '3px', border-bottom-right-radius: '3px' }";
        // this.style3 ="{'background-color':master === true ? 'black' : '#313030' }";
         this.styleUrl = './daymanager_dark.css';
           sheetPath = './daymanager_dark.css';
          
        break;
      case 'LIGHT':
        this.renderer.setAttribute(link, 'href', 'daymanager_light.css');
        this.styleUrl = './daymanager_light.css';
        sheetPath = './daymanager_light.css';
        break;
      case 'CLASSIC':
        this.renderer.setAttribute(link, 'href', 'daymanager.css');
        this.styleUrl = './daymanager.css';
        sheetPath = './daymanager.css';
        break;
    }
    this.sanitizer.bypassSecurityTrustResourceUrl(this.styleUrl);
   
    link.rel = 'stylesheet';
    link.href = sheetPath;
    link.id = 'daymanagercss';
    this.renderer.appendChild(document.head, link);
     this.renderer.setAttribute(link, 'href', sheetPath);
    // Append the new theme link to the document head
   // this.renderer.appendChild(document.head, link);
  }

  onBlur(): void {
    // On blur, format the date to dd/MM/yyyy
    if (this.date) {
      const [year, month, day] = this.date.split('/');
      this.date = `${day}/${month}/${year}`;
     // this.date = `${year}-${month}-${day}`; // Keep the actual value in native format
    }
  }

  onDateChange(event:any){
  
    if (this.date) {
        const [year, month, day] = this.date.split('/');
        this.date = `${day}/${month}/${year}`;
       // this.date = `${year}-${month}-${day}`; // Keep the actual value in native format
      }
   }
   tabIndex:number=0;

   setTab(index:number){
    this.tabIndex = index;

   }
   
   next(){
    this.date = format(addDays(new Date(this.date), this.dayViewValue), 'yyyy/MM/dd');
    // this.cd.detectChanges();
    // this.load_rosters();
   }
   previous(){
    this.date = format(addDays(new Date(this.date), -this.dayViewValue), 'yyyy/MM/dd');
    // this.cd.detectChanges();
    // this.load_rosters();
   }

   AIRosterDone(data:any){
    this.viewAIAssistant=false;
    this.load_rosters();
   }


   RECIPIENT_OPTION = RECIPIENT_OPTION;
   recipientOptionOpen: any;
   recipientOption:any
   loadOption: Subject<any> = new Subject();
   from: any = {display: 'admit'};

   LoadReferalModel(){
   
   

    this.recipientOption = this.RECIPIENT_OPTION.ADMIN;
    this.recipientOptionOpen = {};
    this.loadOption.next(this.user);
    this.sharedS.setRecipientData(this.selectedRecipient);
    this.cd.detectChanges();

 
}

   fireMenu(value:any){
   // this.showOptions(value.data)
   let data=value.data;
   let index=value.index;
   this.selectedOption=data;

   this.user = {
    code: data.recipient,
    id: data.uniqueid,
    view: 2,
    agencyDefinedGroup: data.agencyDefinedGroup,
    sysmgr: data.sysmgr
}

    switch (index){
        case 3:
            this.toMap(data.uniqueid);
            break;
        case 4:
           this.ViewRecipientDetails.next(1);
            break;
        case 5:
            this.ViewRecipientDetails.next(2);
            break;
        case 6:
            this.ViewDetails.next(2);
            break;
        case 7:
           
            this.LoadReferalModel();
            break;
        case 8:
            this.ViewDetails.next(4);
            break;  
        case 9:
            this.AllocateView.next(1);
                break;  
        case 10:
            this.AllocateView.next(2);
            break;  
        case 11:
            this.AllocateView.next(3);
                break;  
        case 12:
            this.AddRoster(1);
                break;  
        case 13:
            this.AddRoster(2);
                break;    
        case 14:
            this.AddRoster(2);
                break;       
        case 15:
        this.openCaseLoad();
            break;   
        case 16:
            this.ViewDMStatus.next(1);
            break;     
        case 17:
            this.ViewDMStatus.next(2);
            break;   
        case 18:
            this.openAllocateResource();
            break;     
        case 1901:
            this.changeModalView.next(1);
            break; 
        case 1902:
            this.changeModalView.next(2);
            break;   
        case 1903:
            this.changeModalView.next(3);
            break;      
        case 1904:
            this.changeModalView.next(4);
            break;   
        case 1905:
            this.changeModalView.next(5);
            break;   
        case 1906:
            this.changeModalView.next(6);
            break;   
        case 1907:
            this.changeModalView.next(7);
            break;  
        case 1908:
            this.changeModalView.next(8);
            break;  
        case 1909:
            this.changeModalView.next(9);
            break;  
        case 1910:
            this.changeModalView.next(10);
            break;  
        case 2001:
            this.ViewDetails.next(1);
            break;  
        case 2002:
            this.ViewDetails.next(2);
            break;  
            break;  
        case 2003:
            this.ViewDetails.next(3);
            break;  
              
        case 2004:
            this.ViewDetails.next(4);
            break;  
        case 2005:
            this.ViewDetails.next(5);
            break;  
        case 2006:
            this.ViewDetails.next(6);
            break;  
        case 21:
            this.ViewNudge.next(1); 
        case 22:
            this.ViewNudge.next(2); 
            break;  
        case 23:
            this.OperationView.next(1); 
            break; 
        case 24:
            this.OperationView.next(2); 
            break; 
        case 25:
            this.OperationView.next(3); 
            break; 
        case 26:
            this.OperationView.next(3); 
            break; 
        case 27:
            this.ApproveView.next(1)
            break; 
        case 28:
            this.ApproveView.next(2)
            break; 
        case 29:
            this.OperationView.next(4);
             break; 
        case 30:
            this.OperationView.next(5);
            break; 
        case 31:
            this.openAIAssistant(); 
            break;  
        case 32:
            this.LoadReferalModel();     
            break;                 
         }
    
   }
}