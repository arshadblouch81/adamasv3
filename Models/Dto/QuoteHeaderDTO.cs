using System;
using System.Collections.Generic;
using AdamasV3.Models.Dto;

namespace Adamas.Models.DTO{
    public class QuoteHeaderDTO {   
        public int? RecordNumber { get; set; }
        public string? DocNo { get; set; }
        public int? ClientId { get; set; }
        public int? ProgramId { get; set; }
        public int? CPID { get; set; }
        public string Contribution { get; set; }
        public string GovtDistribution { get; set; }
        public double? Amount { get; set; }
        public double? DaysCalc { get; set; }
        public double? Budget { get; set; }
        public string GovtContribution { get; set; }
        public string QuoteBase { get; set; }
        public string IncomeTestedFee { get; set; }
        public string PackageSupplements { get; set; }
        public string DailyCDCRate { get; set; }
        public string AgreedTopUp { get; set; }
        public string BalanceAtQuote { get; set; }
        public string CLAssessedIncomeTestedFee { get; set; }
        public string Basis { get; set; }
        public bool? FeesAccepted { get; set; }
        public string BasePension { get; set; }
        public string DailyBasicCareFee { get; set; }
        public string DailyIncomeTestedFee { get; set; }
        public string DailyAgreedTopUp { get; set; }
        public string HardshipSupplement { get; set; }
        public string QuoteView { get; set; }

        public string PersonId { get; set; }

        public List<QuoteLineDTO> QuoteLines { get; set; }
        public List<QuoteGoalsDto> Goals { get; set; }

        // EXTRA
        public string Program { get; set; }
        public string User { get; set; }
        public string Template { get; set; } 
        public string Type { get; set; }
        public int DocumentId { get; set; }
        public string NewFileName { get; set; }
         public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }
         public string DocumentGroup { get; set; }
        
        public bool? IsCDC { get; set; }

         public string Level { get; set; }
         

    }

    

}