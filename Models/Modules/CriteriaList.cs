using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Adamas.Models.Modules{
    public class CriteriaList {

        public string FieldName  { get; set; }
        public string SearchType { get; set; }
        public string TextToLoc  { get; set; }
        public string EndText    { get; set; }
    }
}