namespace Adamas.Models.Modules{
    public class ProcedureClientStaffNote {

        public string PersonId { get; set; }
        public string Program { get; set; }
        public string DetailDate { get; set; }
        public string ExtraDetail1 { get; set; }
        public string ExtraDetail2 { get; set; }
        public string WhoCode { get; set; }
        public bool PublishToApp { get; set; }
        public string Creator { get; set; }
        public string Note { get; set; }
        public string AlarmDate { get; set; }
        public string ReminderTo { get; set; }
        public string CancellationCode { get; set; }
        public string Reasontype { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
      

       
    }
}