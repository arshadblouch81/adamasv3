using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Adamas.Models.Modules
{
    public class DataSetRecipient
    {

        public string ReferralSource { get; set; }
        public string CountryOfBirth { get; set; }
        public string LivingArrangements { get; set; }
        public string HomeLanguage { get; set; }
        public string IndiginousStatus { get; set; }
        public string DwellingAccomodation { get; set; }
        public string PensionStatus { get; set; }
        public string HACCDVACardHolderStatus { get; set; }
        public string CarerResidency { get; set; }

        public string CarerAvailability { get; set; }

        public string DatasetCarer { get; set; }



        public string CarerRelationship { get; set; }

        public string ResidencyVisaStatus { get; set; }

        public string QCSS_Disabilities { get; set; }

        public bool CARER_MORE_THAN_ONE { get; set; }

        public string FP1_Housework { get; set; }
        public string FP2_WalkingDistance { get; set; }
        public string FP3_Shopping { get; set; }
        public string FP4_Medicine { get; set; }
        public string FP5_Money { get; set; }
        public string FP6_Walking { get; set; }
        public string FP7_Bathing { get; set; }
        public string FP8_Memory { get; set; }
        public string FP9_Behaviour { get; set; }
        public string FPA_Communication { get; set; }
        public string FPA_Dressing { get; set; }
        public string FPA_Eating { get; set; }
        public string FPA_Toileting { get; set; }
        public string FPA_GetUp { get; set; }
        public string PersonID { get; set; }
        public string DEXReferralSource { get; set; }
        public string DEXIndigenousStatus { get; set; }
        public string DEXDVACardholderStatus { get; set; }
        public string DEXAccommodation { get; set; }
        public string ReferralPurpose { get; set; }
        public string ReferralType { get; set; }
        public string AssistanceReason { get; set; }
        public string AssistanceReasons { get; set; }
        public bool HasCarer { get; set; }
        public bool HasDisabilities { get; set; }
        public string FirstArrivalYear { get; set; }
        public string FirstArrivalMonth { get; set; }
        public string VisaCategory { get; set; }
        public string Ancestry { get; set; }
        public string ExitReasonCode { get; set; }
        public bool IsHomeless { get; set; }
        public string HouseholdComposition { get; set; }
        public string MainIncomeSource { get; set; }
        public string IncomeFrequency { get; set; }
        public decimal? IncomeAmount { get; set; }
    }
}