import { Component, OnInit, OnDestroy, Input,ChangeDetectorRef } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router';
import {Subject} from "rxjs";
import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService } from '@services/index';
import {FormBuilder} from "@angular/forms";
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import {takeUntil} from "rxjs/operators";
/**
 * Component
 */
@Component({
  styles: [`
    nz-divider {
      margin: 0;
    }

   

    nz-divider ::ngdeep  .ant-divider, .ant-divider-vertical {
      height: 2.5em;
    }

    .tabs-row > p {
      margin: 0;
      cursor: pointer;
      padding: 8px 5px;
      display: inline-block;
      font-weight: 500;
      background: #e9f9ff;
    }

    .active-tab {
      background: #f5951c !important;
      color: #000000;
    }

    .btn {
      border-radius: 5px;
      border: unset;
      padding: 5px 10px 5px 10px;

      cursor: pointer;
    }

    .bg-dark-blue {
      background-color: #002060;
      color:#FFFFFF;
    }
    .bg-dark-blue:hover {
      background: #85b9d5;
      color: #004165;
    }

    :host  nz-submenu .ant-menu-submenu-ul, .ant-menu-inline .ant-menu-item,  .ant-menu-inline .ant-menu-submenu .ant-menu-submenu-title, .ant-menu-vertical-left .ant-menu-item,   .ant-menu-vertical-right .ant-menu-item, .ant-menu-vertical-right .ant-menu-submenu .ant-menu-submenu-title, .ant-menu-vertical .ant-menu-item, .ant-menu-vertical .ant-menu-submenu .ant-menu-submenu-title{
      height: 30px !important;
      line-height: 30px !important;
      color: #dfdfdf;
     
    }

     
    

    nz-submenu .ant-menu-submenu-ul .ant-menu-submenu-li{
      background: #002060;
      height: 30px !important;
      line-height: 30px !important;
    }
  
  `],
    templateUrl: './history.html'
})
/**
 * Class - Clinical -> history -> history.
 */
export class ClinicalHistoryComponent implements OnInit, OnDestroy {

  private unsubscribe: Subject<void> = new Subject();

  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;
  loading: boolean = false;
  tableData: Array<any> = [];

  constructor(
    private timeS: TimeSheetService,
    private sharedS: ShareService,
    private listS: ListService,
    private router: Router,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private printS:PrintService,
    private sanitizer:DomSanitizer,
    private ModalS:NzModalService,
     private cd: ChangeDetectorRef,
  ) {
    this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      if (data instanceof NavigationEnd) {
        if (!this.sharedS.getPicked()) {
          //this.router.navigate(['/admin/recipient/personal'])
        }
      }
    });
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {
  }

handleCancelTop(): void {
    this.drawerVisible = false;
    this.pdfTitle = ""
}
generatePdf() {

    this.drawerVisible = true;

    this.loading = true;

    var fQuery = ""//"Select Name As Field1, CONVERT(varchar, [Date1],105) As Field2, CONVERT(varchar, [Date2],105) as Field3,Notes as Field4 From HumanResources  HR INNER JOIN Staff ST ON ST.[UniqueID] = HR.[PersonID] WHERE ST.[AccountNo] = '" + this.user.code + "' AND HR.DeletedRecord = 0 AND [Group] = 'STAFFALERT' ORDER BY  RecordNumber DESC";

    const data = {
        "template": { "_id": "0RYYxAkMCftBE9jc" },
        "options": {
            "reports": { "save": false },
            "txtTitle": "History",
            "sql": fQuery,
            "userid": this.tocken.user,
            "head1": "Alert",
            "head2": "Reminder Date",
            "head3": "Expiry Date",
            "head4": "Notes",
        }
    }
    this.printS.printControl(data).subscribe((blob: any) => {
        this.pdfTitle = "History.pdf"
        let _blob: Blob = blob;
        let fileURL = URL.createObjectURL(_blob);
        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
        this.loading = false;
        this.cd.detectChanges();
    }, err => {
        this.loading = false;
        this.cd.detectChanges();
        this.ModalS.error({
            nzTitle: 'TRACCS',
            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
            nzOnOk: () => {
                this.drawerVisible = false;
            },
        });
    });

    this.cd.detectChanges();
    this.loading = true;
    this.tryDoctype = "";
    this.pdfTitle = "";
}

}

