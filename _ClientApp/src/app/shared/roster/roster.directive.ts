import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[dateTime]'
})

export class RosterDateTimeDirective {
    @Input('dateTime') dateTime: string;
}