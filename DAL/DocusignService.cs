﻿using DocuSign.eSign.Model;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

using System;
using System.Text;
using Xero.NetStandard.OAuth2.Model.Identity;
using DocuSign.eSign.Client;
using eg_03_csharp_auth_code_grant_core.Common;
using DocuSign.eSign.Api;
using Xero.NetStandard.OAuth2.Client;
using AdamasV3.Models.Modules;
using Adamas.DAL;
using System.IO;
using AdamasV3.Helper;
using System.Threading.Tasks;
using Adamas.Models.Modules;
using Adamas.Controllers;
using Adamas.Models;
using Adamas.Models.Tables;
using Microsoft.AspNetCore.Routing.Constraints;
using System.Data;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Hosting.Internal;
using Microsoft.AspNetCore.Hosting;
using Recipients = DocuSign.eSign.Model.Recipients;
using Microsoft.Office.Interop.Word;
using Document = DocuSign.eSign.Model.Document;
using MailKit;
using Xero.Api.Infrastructure.OAuth;

using static DocuSign.eSign.Client.Auth.OAuth;
using AdamasV3.Models.Dto;
using DocumentFormat.OpenXml.InkML;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using eg_03_csharp_auth_code_grant_core.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

using User = eg_03_csharp_auth_code_grant_core.Models.User;
using UserInfo = DocuSign.eSign.Client.Auth.OAuth.UserInfo;
using Account = DocuSign.eSign.Client.Auth.OAuth.UserInfo.Account;
using Newtonsoft.Json;

namespace AdamasV3.DAL
{
    public class DocusignService : IDocusignService
    {
        private IConfiguration Configuration { get; }
        private readonly string OUTPUTFILEPATH;
        private readonly string LOGPATH;
        private readonly DatabaseContext context;
        private readonly IWebHostEnvironment env;
        private readonly IMemoryCache _cache;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogService log;

        private readonly string _id;
        public DocusignService(
                IConfiguration _config,
                DatabaseContext _context,
                IWebHostEnvironment _env,
                IMemoryCache cache,
                IHttpContextAccessor httpContextAccessor,
                ILogService log

            ) { 
            Configuration = _config;
            LOGPATH = Environment.CurrentDirectory;
            OUTPUTFILEPATH = Path.Combine(LOGPATH, "Token","Docusign", "token.json");
            context = _context;
            env= _env;
            _cache = cache;
            this.log = log;

            var identity = httpContextAccessor.HttpContext.User.Identity as ClaimsIdentity;

            if (identity != null && identity.IsAuthenticated)
            {
                _id = httpContextAccessor.HttpContext.User.Identity.Name;
            }
        }

        public async Task<bool> TestCopyPasteProcedure(string filename, string sourcePath, string destinationPath)
        {
            try
            {
                await Copy_Paste_File(filename, sourcePath, destinationPath);
                return true;
            } catch(Exception ex)
            {
                await this.log.InsertError(ex);
                throw;
            }
        }

        public async Task<bool> PostCopyMTA(int docID)
        {
            var document = await (from d in context.Documents 
                                        where d.Doc_Id == docID 
                                            select d).FirstOrDefaultAsync();
            if (document is null)
            {
                throw new Exception("No existing Document on that ID");
            }
            return true;
        }

        public Stream DownloadCompletedDocument(string envelopeId, string docSelect)
        {
            if(this.User is null)
            {
                RefreshJWT();
            }
            try
            {
                var docuSignClient = new DocuSignClient(Configuration.GetSection("DocuSign:BaseUri")?.Value + "/restapi");
                docuSignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + this.User.AccessToken);

                EnvelopesApi envelopesApi = new EnvelopesApi(docuSignClient);
                Stream results = envelopesApi.GetDocument(Configuration.GetSection("DocuSign:APIAccountId")?.Value, envelopeId, docSelect);
                return results;
            } catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public User User
        {
            get => _cache.Get<User>(GetKey("User"));
            set => _cache.Set(GetKey("User"), value);
        }

        private string GetKey(string key)
        {
            return string.Format("{0}_{1}", _id, key);
        }

        public async Task<bool> UpdateDocumentStatus(string envelopeId, string status, string fileName,int Doc_Id)
        {
            try
            {
                var docStatus = await context.Documents.Where(x => x.Doc_Id == Doc_Id).Select(x => x).FirstOrDefaultAsync();

                if(docStatus is null)
                {
                    throw new Exception("Document does not exist");
                } 

                if(!string.IsNullOrEmpty(status))
                    docStatus.DocusignStatus = status;

                if (!string.IsNullOrEmpty(envelopeId))
                    docStatus.DocusignEnvelopeId = envelopeId;

                if (!string.IsNullOrEmpty(fileName))
                    docStatus.FileName = fileName;

                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }


        public async Task<string> PostEnvelope(DocusignDocument document = null)
        {
            if(document.DOC_ID == 0)
            {
                document = new DocusignDocument() { 
                    Signers = new List<CustomSigner> { 
                        new CustomSigner() { 
                            Email = "arshad@adamas.net.au",
                            Name = "Arshad Abbas",
                            Company = "Adamas",
                            Title = "Mr",
                            RecipientId = 1,
                            RoutingOrder = "1"
                        } 
                    },
                    Documents = new List<Document>() { 
                        new Document()
                        {
                            DocumentBase64 = "T0100005591-NDIA BARBROOK A 4_10_2023.docx",
                            Name = "NDIA BARBROOK A 4_10_2023",
                            FileExtension = "docx",
                            DocumentId = "46047",
                            RemoteUrl = "C:\\Adamas\\Documents\\"
                        }
                    },                 
                    CCRecipients = new List<CarbonCopy> { },
                    StartingView = StartingView.tagging,
                    DOC_ID = document.DOC_ID

                };
            }

            var startingView    = "tagging";

            var filesDirectory  = await GetDocumentDetails(document.DOC_ID);
            var result          = CopyMTADocument(filesDirectory.Filename, filesDirectory.OriginalLocation);

          
                
            var baseUri         = Configuration.GetSection("Docusign:BaseUri")?.Value;
            var accountId       = Configuration.GetSection("Docusign:APIAccountId")?.Value;
            var access_token    = await GetAccessToken();

            // Make Envelope
            EnvelopeDefinition envelope = MakeEnvelope(document.Signers, document.Documents);
            var docuSignClient = new DocuSignClient(baseUri + "/restapi");
            docuSignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + access_token);

            // Create Envelope
            EnvelopesApi envelopesApi = new EnvelopesApi(docuSignClient);
            EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, envelope);
            string envelopeId = results.EnvelopeId;

            ReturnUrlRequest viewRequest = new ReturnUrlRequest
            {
                //ReturnUrl = "https://adamas.traccs.cloud:9000/",
                
                ReturnUrl = Configuration.GetSection("DocuSignJWT:RedirectURL")?.Value,
            };

            ViewUrl result1 = envelopesApi.CreateSenderView(accountId, envelopeId, viewRequest);

            // Switch to Recipx`ient and Documents view if requested by the user
            string redirectUrl = result1.Url;
            Console.WriteLine("startingView: " + startingView);
            if ("recipient".Equals(startingView))
            {
                redirectUrl = redirectUrl.Replace("send=1", "send=0");
            }

            return redirectUrl;
        }

        private EnvelopeDefinition MakeEnvelope(
            List<CustomSigner> signers, 
            List<Document> documents
            )
        {

            // create the envelope definition
            EnvelopeDefinition env = new EnvelopeDefinition();
            env.EmailSubject = "Please sign this document set";

            var docs = documents.Select(x => new Document()
            {
                // DocumentBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(BuildPathString(x.DocumentBase64, x.DocumentId))),
                DocumentBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(System.IO.Path.Combine(this.env.ContentRootPath, "document", x.DocumentBase64))),
                Name = x.Name,
                FileExtension = x.FileExtension,
                DocumentId = x.DocumentId
            }).ToList();


            // Assign documents
            env.Documents = docs;

            ICollection<Signer> filteredSigners = new List<Signer>();
            var counter = 0;
            foreach (var signer in signers)
            {

                filteredSigners.Add(new Signer()
                {
                    Email           = signer.Email,
                    Name            = signer.Name,
                    RecipientId     = signer.RecipientId.ToString(),
                    RoutingOrder    = signer.RoutingOrder
                });
                counter++;
            }

            int signerCounter = 1;
            foreach (var signer in filteredSigners)
            {
                signer.Tabs = new Tabs
                {
                    SignHereTabs = new List<SignHere> {
                        new SignHere(){
                            AnchorString    = $@"\s{signerCounter}\",
                            Name            = signer.Name
                        }
                    },
                    InitialHereTabs = new List<InitialHere> {
                        new InitialHere(){
                            AnchorString    = $@"\i{signerCounter}\",
                        }
                    },
                    FullNameTabs = new List<FullName> {
                        new FullName(){
                            AnchorString    = $@"\n{signerCounter}\",
                            Name            = signer.Name
                        }
                    },
                    CompanyTabs = new List<Company> {
                        new Company(){
                            AnchorString    = $@"\co{signerCounter}\",
                            Name            = signers[signerCounter - 1].Company,
                            Value           = signers[signerCounter - 1].Company,
                            OriginalValue   = signers[signerCounter - 1].Company,
                            Required        = "false"
                        }
                    },
                    TitleTabs = new List<Title> {
                        new Title(){
                            AnchorString    = $@"\t{signerCounter}\",
                            Name            = signers[signerCounter - 1].Title,
                            Value           = signers[signerCounter - 1].Title,
                            OriginalValue   = signers[signerCounter - 1].Title,
                            Required        = "false"
                        }
                    },
                    DateSignedTabs = new List<DateSigned>{
                        new DateSigned(){
                            AnchorString    = $@"\d{signerCounter}\",
                            Name            = "",
                            Value           = "",
                            TemplateLocked  = "true"
                        }
                    }
                };
                signerCounter++;
            }


            Recipients recipients = new Recipients
            {
                Signers = new List<Signer>(),
                CarbonCopies = new List<CarbonCopy>()
            };

            foreach (var signer in filteredSigners)
            {
                recipients.Signers.Add(signer);
            }

            env.Recipients = recipients;
            // Request that the envelope be sent by setting |status| to "sent".
            // To request that the envelope be created as a draft, set to "created"
            env.Status = "created";

            return env;
        }

        public async Task<DocumentDetails> GetDocumentDetails(int docId)
        {
            var sharedPath = await (from r in context.Registration select r.SharedDocumentPath).FirstOrDefaultAsync();
            
            var documentDetails = await (from d in context.Documents where d.Doc_Id == docId select new { 
                FileName = d.FileName, 
                OriginalLocation = d.OriginalLocation 
            }).FirstOrDefaultAsync();

            if(documentDetails is null ) {
                throw new Exception("No Document associated on given DOC_ID");
            }

            DocumentDetails details = new DocumentDetails();
            details.Filename = documentDetails.FileName;
            details.OriginalLocation = string.IsNullOrEmpty(sharedPath) ?
                                            documentDetails.OriginalLocation : RemovePathRoot(documentDetails.OriginalLocation, sharedPath);
            return details;
        }

        public bool CopyMTADocument(string FileName, string pathDirectory, string sourcePath = null)
        {
            try
            {
                Stream s;
                byte[] byteArr;

                if (string.IsNullOrEmpty(sourcePath))
                {
                    sourcePath = System.IO.Path.Combine(env.ContentRootPath, "document");
                    
                }else{
                     sourcePath = System.IO.Path.Combine(sourcePath, "document");
                }
                //Console.WriteLine("Paths: " + pathDirectory + " " + sourcePath);
                using (var conn = context.GetConnection() as SqlConnection)
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(@"[Copy_MTA_Document_WithoutSharedDocument]", (SqlConnection)conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PersonId", "");
                        cmd.Parameters.AddWithValue("@Extension", "");
                        cmd.Parameters.AddWithValue("@FileName", FileName);
                        cmd.Parameters.AddWithValue("@DocPath", Path.GetFullPath(pathDirectory));

                        using (var rd = cmd.ExecuteReader())
                        {
                            while (rd.Read())
                            {
                                byteArr = ReadFully(rd.GetStream(0));
                                System.IO.File.WriteAllBytes(System.IO.Path.Combine(sourcePath, FileName), byteArr);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }


        public async Task<bool> Copy_Paste_File(string filename, string sourcePath, string destinationPath)
        {
            if(this.env.EnvironmentName == Env.Development)
            {
                var fileSourcePath = Path.Combine(sourcePath, filename);
                var fileDestinationPath = Path.Combine(destinationPath, filename);
                if (File.Exists(fileSourcePath))
                {
                    File.Move(fileSourcePath, fileDestinationPath);
                }
                return true;
            }
            else
            {
                try
                {
                    using (var conn = context.GetConnection() as SqlConnection)
                    {
                        await conn.OpenAsync();
                        using (SqlCommand cmd = new SqlCommand(@"[CopyAndPasteFile]", (SqlConnection)conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@FileName", filename);
                            cmd.Parameters.AddWithValue("@SourceDocPath", sourcePath);
                            cmd.Parameters.AddWithValue("@DestinationDocPath", destinationPath);

                            await cmd.ExecuteNonQueryAsync();
                            return true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }          
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
        public EnvelopeDocumentsResult GetEnvelopes()
        {
           
            return null;
        }

        public async Task<DocusignToken> GetToken()
        {
            return await JsonFileReader.ReadAsync<DocusignToken>(OUTPUTFILEPATH);
        }

        public async Task<string> GetAccessToken()
        {
            var token =  await GetToken();
            return token.access_token;
        }

        public bool PostToken(string token)
        {
            try
            {
                var logPath = Environment.CurrentDirectory;
                var OutputFilePath = Path.Combine(logPath, "Token", "Docusign");

                bool exists = System.IO.Directory.Exists(OutputFilePath);

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(OutputFilePath);
                }

                using (var writer = System.IO.File.CreateText(Path.Combine(OutputFilePath, "token.json")))
                {
                    writer.WriteLine(token);
                }

                return true;
            } catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string BTOAEncoding(string toEncode)
        {
            byte[] bytes = Encoding.GetEncoding(28591).GetBytes(toEncode);
            string toReturn = System.Convert.ToBase64String(bytes);
            return toReturn;
        }

        public string RemovePathRoot(string folder, string directoryPath)
        {
            string root = Path.GetPathRoot(folder);

            if (root != null)
            {
                folder = folder.Substring(root.Length);
            }
            return Path.Combine(directoryPath, folder);
        }

        public OAuthToken AuthenticateWithJWT()
        {
            var token =  JWTAuthService.AuthenticateWithJWT(
                        "ESignature",
                        Configuration.GetSection("DocuSignJWT:ClientId")?.Value,
                        Configuration.GetSection("DocuSignJWT:ImpersonatedUserId")?.Value,
                        Configuration.GetSection("DocuSignJWT:AuthServer")?.Value,
                        DSHelper.ReadFileContent(Configuration.GetSection("DocuSignJWT:PrivateKeyFile")?.Value)
                    );

            return token;
        }

        public void RefreshJWT()
        {
            var token = JWTAuthService.AuthenticateWithJWT(
                        "ESignature",
                        Configuration.GetSection("DocuSignJWT:ClientId")?.Value,
                        Configuration.GetSection("DocuSignJWT:ImpersonatedUserId")?.Value,
                        Configuration.GetSection("DocuSignJWT:AuthServer")?.Value,
                        DSHelper.ReadFileContent(Configuration.GetSection("DocuSignJWT:PrivateKeyFile")?.Value)
                    );

            var docuSignClient = new DocuSignClient();
            docuSignClient.SetOAuthBasePath(Configuration.GetSection("DocuSignJWT:AuthServer")?.Value);
            GetAccount(docuSignClient, token);
        }

        public Account GetAccount(DocuSignClient client, OAuthToken accessToken)
        {
            UserInfo userInfo = client.GetUserInfo(accessToken.access_token);
            Account acct = userInfo.Accounts.FirstOrDefault();

            this.User = new User
            {
                Name = acct.AccountName,
                AccessToken = accessToken.access_token,
                ExpireIn = DateTime.Now.AddSeconds(accessToken.expires_in.Value),
                AccountId = acct.AccountId
            };

            return acct;
        }
    }
}
