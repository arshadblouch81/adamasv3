using System;

namespace Adamas.Models.Modules{
    public class Filters {

        public bool AcceptedQuotes { get; set; }
        public bool AllDates { get; set; }
        public bool ArchiveDocs { get; set; }
        public int Display { get; set; }        
        public bool IncludeArchivedNotes { get; set; }
        public bool IncludeClosedIncidents { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public String Type { get; set; }
        public String Criteria { get; set; }
    }
}