import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute, NavigationEnd } from '@angular/router';

import { GlobalService, SettingsService, ShareService, ClientService, TimeSheetService, UserService } from '@services/index';
import { ViewClientPortalDto } from '@services/global.service';
import { Observable, Subject, EMPTY } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: '',
    templateUrl: './index.html'
})

export class FormIndex implements OnInit {

    constructor(
        private globalS: GlobalService,
        private settingS: SettingsService,
        private router: Router,
        private sharedS: ShareService,
        private clientS: ClientService,
        private timeS: TimeSheetService,
        private activatedRoute: ActivatedRoute,
        private sanitizer: DomSanitizer,
        private userS: UserService
    ) {
        
        
    }

    ngOnInit(): void {

    }
}