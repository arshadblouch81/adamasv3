
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;


namespace Adamas.Models.Modules{
    public class LastIdDto{
        [Key]
        public double LastId { get; set; }
      
    }
}