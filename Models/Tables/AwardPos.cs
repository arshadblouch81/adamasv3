using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Adamas.Models.Tables {
public class AwardPos
{
    [Key]
    public int RECORDNO { get; set; }
    public string Day_MaxOrdHrWD1stOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrWD2ndOTBreak_PayType { get; set; }
    public string FN_MaxOrdHrWD1stOTBreak_PayType { get; set; }
    public string FN_MaxOrdHrWD2ndOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrSat1stOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrSat2ndOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrSun1stOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrSun2ndOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrPH1stOTBreak_PayType { get; set; }
    public string Day_MaxOrdHrPH2ndOTBreak_PayType { get; set; }
    public string Week_MaxOrdHrWD1stOTBreak_PayType { get; set; }
    public string Week_MaxOrdHrWD2ndOTBreak_PayType { get; set; }
    public int?   OvertimeLeeway { get; set; }
    public string TOILDefaultAccrualPaytype { get; set; }
    public int? Day_MaxOrdHrWD1stOTBreak { get; set; }
    public int? FN_MaxOrdHrWD1stOTBreak { get; set; }
    public int? Day_MaxOrdHrSat1stOTBreak { get; set; }
    public int? Day_MaxOrdHrSun1stOTBreak { get; set; }
    public int? Day_MaxOrdHrPH1stOTBreak { get; set; }
    public int? Week_MaxOrdHrWD1stOTBreak { get; set; }
    public int? TOILDefaultAccrualMinutes { get; set; }
    public bool? PayHigherRateEntireShift { get; set; }
    public bool? PayHigherRateEntireDay { get; set; }
    public bool? PayHigherPreviousDay { get; set; }
    public string NoNoticeCancelPayType { get; set; }
    public string ShtNoticeCancelPayType { get; set; }
    public string KMAllowanceType { get; set; }
    public int? MakeUp_Pay_Threshold { get; set; }
    public int? MakeUpLeadTime { get; set; }
    public int? CancellationPayThreshold { get; set; }
    public bool? TeaBreakPaidIncWkdHrs { get; set; }
    public string UnpaidMealBreak_PayType { get; set; }
    public int? UnpaidMealBreakMin { get; set; }
    public int? TeaBreakPaidTime { get; set; }
    public int? UnpaidMealBreakStartAt { get; set; }
    public double? TeaBreakStartAt { get; set; }
    public int? TeaBreakPaidTimeNext { get; set; }
    public double? TeaBreakStartAtNext { get; set; }
    public bool? PayOvertimeForTeaBreaks { get; set; }
    public string BrokenShiftAllowanceExtra_Threshold1 { get; set; }
    public int? BrokenShiftAllowanceExtra_Threshold { get; set; }
    public int BrokenshiftLimit { get; set; }
    public string BrokenShiftAllowance_PayType { get; set; }
    public bool IncludePreviousDayInBrokenShiftCalculation { get; set; }
    public bool? DisableBrokenShiftAllowance { get; set; }
    public int? MinBetweenShiftBreak { get; set; }
    public int? MinBreakBetweenSleepovers { get; set; }
    public string MinBreakPayType { get; set; }
    public bool? PayOvertimeNoMinBreak { get; set; }
    public string Day_SatOrdHr_PayType { get; set; }
    public string Day_SunOrdHr_PayType { get; set; }
    public string Day_PHOrdHr_PayType { get; set; }
    public int PayOvertimeInsuficientMinDays { get; set; }
    public int? Week_MinFreeDays { get; set; }
    public int? FN_FreeDays { get; set; }
    public int? W4_FreeDays { get; set; }
    public int? MinHoursPerDay { get; set; }
    public int? MinHoursPerService { get; set; }
    public bool? BreakTimeWorked { get; set; }
    public string OrdHoursStartTime { get; set; }
    public string OrdHoursEndTime { get; set; }
    public string BASE_PayType { get; set; }
    public double? Week_MaxOrdHr { get; set; }
    public int? Week_MaxOrdDays { get; set; }
    public double? Day_MaxOrdHr { get; set; }
    public int? FN_MaxOrdDays { get; set; }
    public double? FN_MaxOrdHr {get;set;}
    public double?  FN_MaxOrdHrsDay {get;set;}
    public double? W4_MaxOrdHrsDay { get; set; }
    public double? W4_MaxOrdHours { get; set; }
    public int? W4_MaxOrdDays { get; set; }
    public string Notes { get; set; }
    public string Category { get; set; }
    public double? Level { get; set; }
    public string Code { get; set; }
    public string Description { get; set; }


}
}