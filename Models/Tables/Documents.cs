using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class Documents
    {
       
          [Key]
        public int Doc_Id { get; set; }

        public string Status { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

        [MaxLength(200)]
        public string FileName { get; set; }

        [MaxLength(8000)]
        public string OriginalLocation { get; set; }

        [MaxLength(50)]
        public string Author { get; set; }

        [MaxLength(15)]
        public string PersonID { get; set; }

        [MaxLength(15)]
        public string DocumentType { get; set; }

        [MaxLength(50)]
        public string Department { get; set; }

        [MaxLength(50)]
        public string Classification { get; set; }

        [MaxLength(50)]
        public string Category { get; set; }

        [MaxLength(50)]
        public string Typist { get; set; }

        [MaxLength(8000)]
        public string RTFText { get; set; }

        [MaxLength(8)]
        public string Timespent { get; set; }

        public bool DeletedRecord { get; set; }

        public int? COID { get; set; }

        public int? BRID { get; set; }

        public int? DPID { get; set; }

        public bool FileNoteFlag { get; set; }

        public DateTime? FollowUpDate { get; set; }

        [MaxLength(8000)]
        public string Action { get; set; }

        [MaxLength(8)]
        public string TimeLogID { get; set; }

        [MaxLength(80)]
        public string Title { get; set; }

        public DateTime? AlarmDate { get; set; }

        public bool? Acknowledged { get; set; }

        [MaxLength(15)]
        public string DocumentGroup { get; set; }

        [MaxLength(8000)]
        public string Paramaters { get; set; }

        [Column("Doc#")]
        [MaxLength(10)]
        public string DocNo { get; set; }

        public DateTime? DocStartDate { get; set; }

        public DateTime? DocEndDate { get; set; }

        [MaxLength(10)]
        public string DocCharges { get; set; }

        [MaxLength(4000)]
        public string AlarmText { get; set; }

        public int? CareDomain { get; set; }

        public int? SubId { get; set; }

        [MaxLength(50)]
        public string SubCat { get; set; }

        public bool? WADSCPlan { get; set; }

        [MaxLength(10)]
        public string WADSCOptOut { get; set; }

        [MaxLength(15)]
        public string DSOutletID { get; set; }

        public bool? WADSCFundingChanged { get; set; }

        public bool? WADSCPlanHeaderOnly { get; set; }

        public bool? WADSCSupportingEvidence { get; set; }

        public bool? QuoteTemplate { get; set; }

        public bool? PublishToApp { get; set; }

        [MaxLength(10)]
        public string DocusignStatus { get; set; }

        [MaxLength(60)]
        public string DocusignEnvelopeId { get; set; }

        public DateTime? xEndDate { get; set; }

        public bool? PublishToClientPortal { get; set; }
    }
}