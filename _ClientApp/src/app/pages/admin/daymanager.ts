
import { forkJoin,  Subject ,  Observable } from 'rxjs';
import { Component, OnInit, ViewChild, HostListener } from '@angular/core';

import { FormControl, FormGroup, FormArray , Validators, FormBuilder } from '@angular/forms';
import  { ClientService, GlobalService, TimeSheetService, ListService } from '@services/index';
import { BsModalComponent } from 'ng2-bs3-modal';

import * as moment from 'moment';
import * as _ from 'lodash';
import { InputParams } from '@shared/index';

import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarDayViewBeforeRenderEvent
  } from 'angular-calendar';
  

  class Address {
    postcode: string;
    address: string;
    suburb: string;
    state: string;

    constructor(postcode: string, address: string, suburb: string, state: string) {
        this.suburb = suburb.trim();
        this.address = address;
        this.postcode = postcode;
        this.state = state;
    }

    getAddress() {
        var _address = `${this.address} ${this.suburb} ${this.postcode}`;
        return (_address.split(' ').join('+')).split('/').join('%2F');
    }
}

interface UserData {
    serviceInfo?: {
        details: any,
        billing: any,
        payInfo: any,
        mta: any
    },
    tasks?: Array<any>,
    extraInfo?: string,
    serviceNotes?: Array<any>,
    dataset?: any,
    auditHistory?: Array<any>
}


@Component({
    templateUrl:'./daymanager.html',
    styles:[
        `
        select{
            display:block;
            width:100%;
        }
        clr-modal.btrap >>> .modal-footer{
            padding:15px;
        }
        clr-modal.large-modal >>> .modal-dialog{
            width:60rem;
        }
        bs-modal.controlModal >>> .modal-dialog .modal-content {
            padding:0;
        }
        bs-modal >>> .modal-header{
            padding:0 0 1rem 0;
        }
        bs-modal >>> .modal-body{
            overflow-y: inherit;
            overflow-x: inherit;
            max-height:inherit;
        }            
        ul{
            list-style:none;
        }
        div.list-notif-container{
            max-height:20rem;
            overflow-y: scroll;
        }
        ul.list-notif-style{
            position:relative;
            z-index: 1;
        }
        ul.list-notif-style li:hover:not(.selected){
            background:#f4f4f4;
        }
        ul.list-notif-style li{
            cursor:pointer;
            display: inline-block;
            width: 100%;
            margin: 0;
            padding: 5px;
        }
        ul.list-notif-style li:last-child{
            margin:0;
        }
        ul.list-notif-style i{
            float:left;
            margin-right:10px;
            background:#fff;
            font-weight: 600;
            border-radius: 50%;
            color: #908383;
            border: 1px solid #dcdcdc;
        }

        bs-modal-body ul.right-click-menu{
            padding-left:5px;
            position:relative;
            margin: 3px 0 3px 30px;
            border-left:1px solid #eee;
        }
        ul.right-click-menu li{
            padding:1px 0 1px 2px;
            position:relative;
        }
        li > ul{
            position: absolute;
            display:none;         
            right: -192px;
            padding: 2px 5px;
            background: #fff;
            top: -6px;
            width: 192px;
            transition: all 0.5s ease 3s;
        }
        li:hover > ul{           
            display:block;
            transition: all 0.5s ease 0s;
        }
        ul.right-click-menu li:hover{
            background:#eeeeee;
            cursor:pointer;
        }
        .glyphicon-triangle-right {
            float:right;
            margin:4px 0;
        }
        hr{
            margin:5px 0;
        }
        .modal-title span{
            font-size: 15px;
            margin-left: 1rem;
        }
        form{
            display: inline-block;
            padding: 0.5rem;
        }
        .deleted{
            border: 1px solid #d2d2d2;
            margin-bottom: 5px;
            border-radius: 3px;
        }
        .right-click-menu{
            font-size: 12px; line-height: 22px;
        }
        .recdetails{
            padding: 1rem;
            display: flex;
        }
        .min-wrap{
            min-height: 12rem;
            max-height: 15rem;
        }
        bs-modal >>> bs-modal-body clr-tabs ul button{
            margin-right:15px !important; 
        }
        .selected{
            background: #607d96;
            color: #fff;
        }
        clr-tabs{
            width:100%;
        }
        .data-infos{
            display: flex;
        }
        .data-infos > div{
            flex: 1;
        }
        .center-align{
            display: flex;
            justify-content: center;
            width: 100%;
        }
        li.inactive {
            background: #dadada
        }
        .check-font{
            font-size:11px;
            font-weight:500;
        }
        .hilight{
            color: #0089ff;
            font-size: 17px;
        }
        h6{
            margin:0;
        }
        article{
            padding: 12px;
            border-radius: 5px;
            background: #f1f1f1;
            margin-bottom: 8px;
        }
        .dayview i{
            font-size:10px;
            margin:0 10px;
            color:#5ab0fa;
        }
        .dayview-title{
            
        }
        .chks{
            margin:3px 0;
        }
        .form-inline{
            margin-top:8px;
        }
        .note-container{
            display: flex;
            padding: 0 10px 10px 10px;
            background: #fbfbfb;
            border: 1px solid #e0e0e0;
        }

        mwl-calendar-day-view >>> .cal-hour-rows {
            overflow: inherit !important;
        }   
        .time-wrapper{
            flex:1;
        }     
        .date-wrapper{
            flex:2;
        }
        .time-segment{
            width: 6rem;
            position: sticky;
            top: 0;
        }
        `
    ]
})


export class DayManagerAdmin implements OnInit {
    @ViewChild('controlModal', { static: false }) controlModal: BsModalComponent;

    @ViewChild('allocatestaff', { static: false }) allocatestaff: BsModalComponent;
    @ViewChild('nudge', { static: false }) nudge: BsModalComponent;
    @ViewChild('changeDayTimes', { static: false }) changeDayTimes: BsModalComponent;

    @HostListener('contextmenu',['$event'])

    clickEvents(event: MouseEvent){
       this.toBePasted = []
    }

    controlModalOpen: boolean = false;
    viewStaffDetailsOpen: boolean = false;
    viewRecipientDetailsOpen: boolean = false;
    caseLoadOpen: boolean = false;
    allocateResourcesOpen: boolean = false;
    changeDayTimesOpen: boolean = false;
    reallocStaffOpen: boolean = false;
    allocatestaffOpen: boolean = false;
    qikAllocateOpen: boolean = false;
    fp_hide: boolean = false;
    cancelShiftOpen: boolean = false;
    myDiaryOpen: boolean = false;

    toBePasted: Array<any>;
    pasteActivate: any = {
        copy: false,
        cut: false
    }

    addNewStaffModal: boolean = false;
    daymanagers: Array<any>;

    leave_dd: any = {
        paycode: [],
        programs: [],
        activitycode: [],
        leaveActivityCodes: [],
        leaveBalances: []
    }    
    
    rosterInputs: Array<any>;

    _allocatedStaffArray: Array<any>;
    _selectedallocatedStaff: any;

    _highlighted: Array<any> = [];

    tabvrd:number = 1;
    stafftab: number = 1;

    changeNudgeView = new Subject<number>()
    changeModalView = new Subject<number>()
    nudgeTime$ = new Subject<any>()
    changeViewRecipientDetails = new Subject<number>()
    changeViewStaffDetails = new Subject<number>()
    private saveChanges$: Observable<any>

    duration: any;
    categories: Array<any> = [];
    programs: Array<any> = [];
    activities: Array<any> = [];

    optionModal: boolean = false;
    windowShow = new Subject<number>();
    window: number = 1;

    private address: Array<any>;
    inputChange = new Subject<any>();

    selected: any;
    selectedOption:any;
    selectedQikAllocate: any;
    selectedInputParams: InputParams
   
    date: any;

    daynum: Array<number> = [ 5,7,10,14 ];
    dayView: number = 7;

    change: boolean = false;
    reload: boolean = false;
    notifier: boolean = false;
    changeModalViewNo: number = 1;

    changeModal: any = {
        title: '',
        show: false,
        view: 1
    }

    notifierModal: any = {
        title: '',
        show: false,
        view: 1
    }

    nudgeTimeModal: any = {
        title: '',
        view: 1
    }

    outputListModel: any = "";
    programListModel: any = "";
    activityListModel: any = "";
    payTypeListModel: any = "";
    billAmountModel: any = "";
    billQuantityModel: any = "";
    payQuantityModel: any = "";
    setUnitCostModel: any = "";

    changeOutputList: Array<any> = [];
    changeProgramList: Array<any> = [];
    changeActivityList: Array<any> = [];
    changePayTypeList: Array<any> = [];

    nudgetime: string = "00:00";
    newNudgeTime: string;

    user: any;
    staff: any;

    userData: UserData;

    whoTab: any = {
        sInfo: true,
        billing: false,
        payInfo: false,
        mta: false
    }

    changeDayOrTimeGroup: FormGroup;
    unallocateStaffGroup: FormGroup;

    changeDayDuration: any

    timeDuration = new Subject<any>();
    private serviceInfoDuration =  new Subject<any>();
    leaveBalanceList: Array<any> = []

    unallocStaffModal: boolean = false;

    events: CalendarEvent[];

    refresh: Subject<any> = new Subject();

    constructor(
        private clientS: ClientService,
        private globalS: GlobalService,
        private timeS: TimeSheetService,
        private listS: ListService,
        private formBuilder: FormBuilder
    ){

        this.nudgeTime$.subscribe(data =>{
            var time
            if(this.selectedOption){                
                if(this.nudgeTimeModal.view == 1){
                    time = this.globalS.solveTime(this.selectedOption.StartTime, data, true)
                }
                
                if(this.nudgeTimeModal.view == 2){
                    time = this.globalS.solveTime(this.selectedOption.StartTime, data, false)
                }
                this.newNudgeTime = time;           
            }
        })

        this.timeDuration.subscribe(data => {
            this.changeDayDuration = this.globalS.computeTime(this.changeDayOrTimeGroup.value.startTime, this.changeDayOrTimeGroup.value.endTime)
            this.changeDayOrTimeGroup.controls['payQuant'].setValue(this.changeDayDuration.quants)
            this.changeDayOrTimeGroup.controls['billQuant'].setValue(this.changeDayDuration.quants)
            this.changeDayOrTimeGroup.controls['duration'].setValue(this.changeDayDuration.duration)
        })

        this.serviceInfoDuration.subscribe(data => {
            this.duration = this.globalS.computeTime(this.selectedOption.StartTime, this.selectedOption.EndTime).durationStr
        });

        this.changeNudgeView.subscribe(data =>{
            this.nudgetime = moment().format('YYYY/MM/DD 00:00');
            this.controlModal.close();
            switch(data){
                case 1: 
                    this.nudgeTimeModal = {
                        title: 'Nudge Up',
                        view: 1
                    }
                    break;
                case 2:
                    this.nudgeTimeModal = {
                        title: 'Nudge Down',
                        view: 2
                    }
                    break;
            }
            this.nudge.open();
        })

        this.changeViewRecipientDetails.subscribe(data => {
            this.user = {
                name: this.selectedOption.Recipient,
                code: this.selectedOption.uniqueid,
                startDate: '2019/01/15',
                endDate: '2019/01/29'
            }            
            this.tabvrd = data;
            this.viewRecipientDetailsOpen = true;
        });

        this.changeViewStaffDetails.subscribe(data => {            
            this.staff = {
                name: this.selectedOption.carercode,
                uniqueId: this.selectedOption.StaffID
            }
            this.stafftab = data;
            this.viewStaffDetailsOpen = true;
        })

        this.changeModalView.subscribe(data =>{
            //this.controlModal.close();
            this.clearInputs();
            switch(data){
                case 1: 
                    this.changeModal = {
                        title: 'Change Output Type',
                        show: true,
                        view: 1
                    }
                    this.timeS.getoutputtype().subscribe(data => { 
                        this.changeOutputList = data;
                    });
                    break;
                case 2:
                    this.changeModal = {
                        title: 'Change Program',
                        show: true,
                        view: 2
                    }
                    this.timeS.getprogramtype().subscribe(data => this.changeProgramList = data);
                    break;
                case 3:
                    this.changeModal = {
                        title: 'Change Activity',
                        show: true,
                        view: 3
                    }
                    this.timeS.getactivitytype().subscribe(data => this.changeActivityList = data)
                    break;
                case 4:
                    this.changeModal = {
                        title: 'Change Pay Type',
                        show: true,
                        view: 4
                    }
                    this.timeS.getpaytype().subscribe(data => this.changePayTypeList = data)
                    break;
                case 5:
                    this.changeModal = {
                        title: 'Change Bill Amount',
                        show: true,
                        view: 5
                    }
                    break;
                case 6:
                    this.changeModal = {
                        title: 'Change Bill Quantity',
                        show: true,
                        view: 6
                    }
                    break;
                case 7:
                    this.changeModal = {
                        title: 'Change Pay Quantity',
                        show: true,
                        view: 7
                    }
                    break;
                case 8:
                    this.changeModal = {
                        title: 'Change Set Unit Cost',
                        show: true,
                        view: 8
                    }
                    break;
            }
            
        })

        this.windowShow.subscribe(data => {
            this.window = data;
        });

        this.resetForms();
    }

    resetForms(){
        this.changeDayOrTimeGroup = this.formBuilder.group({
            date: null,
            startTime: null,
            endTime: null,
            duration: null,
            payQuant: null,
            billQuant: null,
            recordNo: null
        })

        this.unallocateStaffGroup = this.formBuilder.group({
            makeUnavailable: true,
            unallocAdmin: false,
            activityCode: '',
            payCode: '',
            program: ''
        })
    }

    ngOnInit(){
        this.date = moment().startOf('week').add('day', 1);
        //this.date = "2019/02/11"
    }

    sample(){
        this.tabvrd = 1;
        this.controlModal.close();
    }

    clearInputs(){
        this.outputListModel = "";
        this.programListModel = "";
        this.activityListModel = "";
        this.payTypeListModel = "";
        this.billAmountModel = "";
        this.billQuantityModel = "";
        this.payQuantityModel = "";
        this.setUnitCostModel = "";
    }

    highlighted(data: Array<any>): void{
        console.log(data)
        this._highlighted = data;
    }

    qikselect(data: any){
        console.log(data)
    }

    resetShowDetail(){
        this.whoTab = {
            sInfo: true,
            billing: false,
            payInfo: false,
            mta: false
        }
        this.window = 1;
    }

    showDetail(data: any){
        console.log(data)
        let cloneData = _.cloneDeep(data);

        this.selectedOption = cloneData;

        cloneData.StartTime = moment(cloneData.StartTime,['HH:mm']).format('YYYY-MM-DDTHH:mm:ss')
        cloneData.EndTime = moment(cloneData.EndTime,['HH:mm']).format('YYYY-MM-DDTHH:mm:ss')

        this.selected = {
            recipient: cloneData.Recipient,
            recordNo: cloneData.RecordNo,
            date: cloneData.Date,
            startTime: moment(cloneData.StartTime,['HH:mm']).format('YYYY-MM-DDTHH:mm:ss'),
            endTime: moment(cloneData.EndTime,['HH:mm']).format('YYYY-MM-DDTHH:mm:ss'),
            category: data.Category,
            program: data.rProgram,
            activity: data.Activity,
            staff: data.Staff,
            location: cloneData['Setting/Location'],
            numAttendees: ''
        }

        this.populate(this.selectedOption);
        this.optionModal = true;
        this.resetShowDetail();    
    }

    saveChanges(){
        let data = this.changeModal.view;
        let staff = this.selectedOption;
        let roster: Dto.RosterInput;
        switch(data){
            case 1:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.outputListModel
                }
                this.saveChanges$ = this.timeS.updateoutputtype(roster);
                break;
            case 2:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.programListModel
                }
                this.saveChanges$ = this.timeS.updateprogram(roster);
                break;
            case 3:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.activityListModel
                }
                this.saveChanges$ = this.timeS.updateactivity(roster);
                break;
            case 4:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.payTypeListModel
                }
                this.saveChanges$ = this.timeS.updatepaytype(roster);
                break;
            case 5:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.billAmountModel
                }
                this.saveChanges$ = this.timeS.updatebillamount(roster);
                break;
            case 6:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.billQuantityModel
                }
                this.saveChanges$ = this.timeS.updatebillquantity(roster);
                break;
            case 7:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.payQuantityModel
                }
                this.saveChanges$ = this.timeS.updatepayquantity(roster);
                break;
            case 8:
                roster = {
                    Key: staff.RecordNo,
                    Value: this.setUnitCostModel
                }
                this.saveChanges$ = this.timeS.updatesetunitcost(roster);
                break;
        }

        this.saveChanges$.subscribe(data => {
            console.log(data);
        })
        this.changeModal.show = false;
    }

    populate(data: any){
        this.timeS.getlistcategories().subscribe(data => this.categories = data);
        this.clientS.getactiveprogram({
            IsActive: true,
            Code: data.Recipient
        }).subscribe(data => this.programs = data.data );

        this.timeS.getlistservices(data.Recipient).subscribe(result => {  
            let res = result;                  
            res.push(data.activity);
            this.activities = res;            
        });
    }

    tempRosters:any
    loadModalInfos: boolean

    loadModal(){  
        setTimeout(() => {
            this.rosterInputs = this.tempRosters.diary
        }, 1000);
        this.loadModalInfos = true
    }

    closeModal(){
        this.loadModalInfos = false
    }

    qikAllocateOpenModal(){
        this.qikAllocateOpen = true;
        this.loadModal();
    }


    showOptions(data:any){
        this.rosterInputs = []
        this.tempRosters = data
        this.selectedOption = data.selected; 

        this.selectedInputParams = {
            RecipientCode: this.selectedOption.Recipient,
            User: '',
            BookDate: this.selectedOption.date,
            StartTime: this.selectedOption.StartTime,
            EndTime: this.selectedOption.EndTime,
            EndLimit: '17:00',
            Gender: '',
            Competencys: '',
            CompetenciesCount: 0  
        }    
        console.log(this.selectedOption)

        var uniqueIds = this._highlighted.reduce((acc, data) => {
            acc.push(data.uniqueid);
            return acc;
        },[]);

        var sss = uniqueIds.length > 0 ? uniqueIds : [this.selectedOption.uniqueid]
    
        this.clientS.gettopaddress(sss)
            .subscribe(data => this.address = data)

        // let uniqueId = data.UniqueID;
        // this.clientS.getprimaryaddress(uniqueId).flatMap(x => {
        //     if(x.length > 0)
        //         return Observable.of(x);
        //     return this.clientS.getusualaddress(uniqueId);
        // }).flatMap(x => {
        //     if(x.length > 0)
        //         return Observable.of(x);
        //     return this.clientS.gettopaddress(uniqueId);
        // }).subscribe(data => {       
        //     this.address = data[0];
        // });

        // this.controlModal.open();
        this.controlModalOpen = true;
    }

    openUnallocStaffModal(){
        this.unallocStaffModal = true;

        this.loadLeaveInfos();
        this.timeS.getAllocateDefaults(this.selectedOption.Staff).subscribe(data => {
            this.unallocateStaffGroup.patchValue({
                activityCode: data.defaultLeaveActivity,
                payCode: data.defaultLeavePayType
            });
        })
        this.listS.getleavebalances(this.selectedOption.uniqueid).subscribe(data => this.leaveBalanceList = data)
    }

    loadLeaveInfos(){
        
        let dates = {
            StartDate: '08-01-2019',
            EndDate: '08-30-2019',
            
        };

        forkJoin([
            this.listS.getpaycode(dates),
            this.listS.getprograms(dates),
            this.listS.getleaveactivitycodes(dates),
        ]).subscribe(data => {            
            this.leave_dd = {
                paycode: data[0],
                programs: data[1],
                leaveActivityCodes: data[2]
            }           
        })
    }
    
    data(data: Array<any>){
        this.daymanagers = data;
    }

    deleteShifts(){
        let recordNoArr: Array<number> = this._highlighted.map(data => { return data.recordno })
        this.timeS.deleteshift(recordNoArr).subscribe(data =>{
            if(data){
                this.globalS.sToast('Success','Rsoters deleted')
                this.reload = !this.reload;
            }
        })
    }

    approve(){
        this.timeS.updateapproveroster(this.selectedOption.recordno).subscribe(data => {
           if(data){
                this.reloadDM();
                this.globalS.sToast('Success','Roster Approved');
                this.controlModal.close();
           }
        })
    }

    unapprove(){

        this.timeS.updateunapproveroster(this.selectedOption.recordno).subscribe(data => {
            if(data){
                this.reloadDM();
                this.globalS.sToast('Success', 'Roster Unapproved');
                this.controlModal.close();
            }
        })
    }

    deleteRoster(){    
        if(this._highlighted.length > 0 || this.selectedOption){
            this.notifierModal = {
                title: 'Delete highlighted entries?',
                view: 1
            }
            if(this._highlighted.length == 0 ){
                this._highlighted = [this.selectedOption]
            }
            this.notifier = true;
            this.controlModalOpen = false; 
            return;
        }
        alert('Please highlight an entry');
    }
   
    allocStaff(){
        this.clientS.getqualifiedstaff({
            RecipientCode: '',
            User: 'sysmgr',
            BookDate: '',
            StartTime: '9:00',
            EndTime: '11:00',
            CompetenciesCount: 3,
            Competencys: 'BLUE CARD,FIRST AID,WARM AND LOVING'
        }).subscribe(data => {
            this._allocatedStaffArray = data;
        });
        
        //this.allocatestaff.open();
    }

    allocate(){
        this.timeS.updateallocatestaff(this.selectedOption.RecordNo,
            { 
                AccountNo: this._selectedallocatedStaff.accountNo, 
                Activity: this.selectedOption.Activity
            }).subscribe(data => { 
            this.reload = !this.reload;
        });
    }

    toMap(){

        if(this.address){
          

            var adds = this.address.reduce((acc,data) => {
                var { postCode, address1, suburb, state } = data;
                var address = new Address(postCode, address1, suburb, state);
                return acc.concat('/',address.getAddress());                
            }, []);
            console.log(adds)
            console.log(adds.join(''))
           

                
            //window.open('https://www.google.com/maps/search/?api=1&query=' + encoded,'_blank'); 
            window.open(`https://www.google.com/maps/dir${adds.join('')}`,'_blank');
            return false;
        }
        this.globalS.eToast('No address found','Error');
    }

    openChangeDay(){

        const user = this.selectedOption;

        this.changeDayOrTimeGroup.patchValue({
            date: user.date,
            startTime: user.StartTime,
            endTime: user.EndTime,
            duration: this.globalS.computeTime(user.StartTime,user.EndTime).duration,
            payQuant: user.PayQty,
            billQuant: user.BillQty,
            recordNo: user.recordno
        })

        this.changeDayTimesOpen = true;
    }

    // Get General Details for Recipient Details Modal
    getGeneralDetails(): Observable<any>{
        var uniqueId = this.selectedOption.UniqueID;
        return forkJoin([
            this.clientS.getaddress(uniqueId),
            this.clientS.getcontacts(uniqueId),
            this.clientS.getrelativecontacts(uniqueId),
            this.clientS.getalerts(uniqueId),
            this.clientS.getservicetasklist(uniqueId)
        ]);
    }

    // Get Funding Details for Recipient Details Modal
    getFundingDetails():Observable<any>{
        var uniqueId = this.selectedOption.UniqueID;
        return forkJoin([
            this.clientS.getservicesapproved(uniqueId),
            this.clientS.getprogramsapproved(uniqueId),
            this.clientS.getcurrentcareplan(uniqueId)
        ]);
    }

    // Get Casenote Details
    getCaseNoteDetails(): Observable<any>{
        var uniqueId = this.selectedOption.UniqueID;
        return this.clientS.getcaseprogressnote(uniqueId);
    }

    // Get Operational Note Details
    getOperationalNoteDetails():Observable<any>{
        var uniqueId = this.selectedOption.UniqueID;
        return this.clientS.getopnote(uniqueId);
    }

    // Get Support Worker Details
    getSupportWorkerDetails():Observable<any>{        
        var recipient = this.selectedOption.Recipient;

        let workerInput: Dto.WorkerInput = {
            ClientCode: recipient,
            StartDate: '',
            EndDate:''
        }
        return forkJoin([
            this.clientS.getrostermaster(workerInput),
            this.clientS.getrosterworker(workerInput)
        ]);
    }

    allocateResourceProcess(){
        this.timeS.updateResource({
            resource: this.selectedResource,
            recordno: this.selectedOption.RecordNo
        }).subscribe(data =>{            
            this.caseLoadInput = ''
            if(data){
                this.globalS.sToast('Success', 'Resource changed')
                return;
            }
            this.globalS.eToast('Error','Operation Error')
        })
    }

    caseLoadInput: string;
    caseLoadProcess(){
        this.timeS.updatecaseload({
            caseload: this.caseLoadInput,
            recordno: this.selectedOption.RecordNo
        }).subscribe(data =>{            
            this.caseLoadInput = ''
            if(data){
                this.globalS.sToast('Success', 'Caseload changed')
                return;
            }
            this.globalS.eToast('Error','Operation Error')
        })
    }

    resourceList: Array<any>
    selectedResource: string
    listResources(){
        this.selectedResource = ''
        this.listS.getresources().subscribe(data => this.resourceList = data)
    }

    pickResource(data: any){
        this.selectedResource = data;
    }

    shiftNotes(){
        // this.controlModal.close();
        this.optionModal = true;
        this.windowShow.next(5);
    }

    changeDayTimeProcess(){
        this.changeDayOrTimeGroup.controls["date"].setValue(moment(this.changeDayOrTimeGroup.value.date).isValid() ? moment(this.changeDayOrTimeGroup.value.date).format('YYYY/MM/DD') : null)        
        this.timeS.postdaytime(this.changeDayOrTimeGroup.value)
            .subscribe(data => {
                if(data){
                    this.globalS.sToast('Success','Schedule Changed')
                    this.reload = !this.reload
                    return;
                }
                this.globalS.eToast('Error','Sced')
            })
    }

    openDiary(){
        this.myDiaryOpen = true;
        this.dayViewProcess();

        setTimeout(() => {    
            this.refresh.next();
        }, 10);
    }


    copy(){
        this.toBePasted = []
        if(this._highlighted && this._highlighted.length == 0 ) this._highlighted.push(this.selectedOption);            

        this.toBePasted = this._highlighted;
        this.controlModal.close();
        this.pasteActivate = { copy: true, cut: false }
    }

    pasted(date: any){

        const toBePasted = this.toBePasted.map(x => {
            return {
                Date: date.date,
                RecordNo: x.recordno
        }})
    
        if(this.pasteActivate.copy){
            this.timeS.postcopyroster(toBePasted).subscribe(data =>{     
                if(data){
                    this.globalS.sToast('Success','New Rosters are copied')
                    this.reloadDM();
                }          
            })
            this.pasteActivate = { copy: false, cut: false }
        } 

        if(this.pasteActivate.cut){
            this.timeS.postcutroster(toBePasted).subscribe(data =>{     
                if(data){
                    this.globalS.sToast('Success','New Rosters are copied')
                    this.reloadDM();
                }          
            })
            this.pasteActivate = { copy: false, cut: false }
        }
        this.toBePasted = []      
    }

    reloadDM(): void{
        this.reload = !this.reload
    }


    cut(){
        this.toBePasted = []
        if(this._highlighted && this._highlighted.length == 0 ) this._highlighted.push(this.selectedOption);            

        this.toBePasted = this._highlighted;
        this.controlModal.close();
        this.pasteActivate = { copy: false, cut: true }
    }

    nudgeProcess(){
        this.timeS.updatenudgetime({ StartTime: this.newNudgeTime, RecordNo: this.selectedOption.recordno })
            .subscribe(data => {
                if(data){
                    this.globalS.sToast('Success','Time updated')
                    this.reload = !this.reload
                }
            })
    }

    nudgeTimeChanges(time: any){
        this.nudgeTime$.next(time)
    }

    dayData: Array<any>;
    view: string = 'day';
    
    viewDate: Date;
    timeArray: Array<number> = [5,10,15,30];
    hourSegments: number;
    minuteInterval: number;
    dayStartHour: number; 

    timeSegChange(num: number){

        if(num == 5) {
        this.hourSegments = 12
        }

        if(num == 10) {
        this.hourSegments = 6
        }

        if(num == 15) {
        this.hourSegments = 4
        }

        if(num == 30) {
        this.hourSegments = 2
        }

        setTimeout(() => {    
        this.refresh.next();
        }, 10);
    }

    dayViewProcess(){
        this.hourSegments = 12;
        this.minuteInterval = 5;
        this.dayStartHour = 0;
        this.viewDate = new Date(this.selectedOption.date);

        const { Staff, date } = this.selectedOption;

        // Filter and Maps the Events
        this.events = this.daymanagers.reduce((acc, x) =>  {
            if (x.key == (Staff).trim()){
                x.value.forEach(val => {
                    if(val.date == (date).trim()){
                        acc.push({
                            start: new Date(`${val.date} ${val.StartTime}`),
                            end: new Date(`${val.date} ${val.EndTime}`),
                            title: `${val.Recipient} | ${val.StartTime}-${val.EndTime}`,
                        });
                    }
                });
            }            
            return acc;
        },[]);

        // Get the earliest time in list
        if(this.events.length > 0){            
            this.dayStartHour = new Date(this.events[0].start).getHours() - 1;
        }
    }


    openNewModal(){
        this.addNewStaffModal = true;
        this.dayViewProcess();

        setTimeout(() => {    
            this.refresh.next();
        }, 10);
    }
    

}