namespace Adamas.Models.Modules{
    public class Providers {
        public string Filephoto { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleNames { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string UniqueID { get; set; }
        public string PreferredName { get; set; }
    }
}