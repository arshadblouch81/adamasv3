
import { takeUntil, mergeMap, distinctUntilChanged } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable ,  Subject } from 'rxjs'

import { TimeSheetService, GlobalService, StaffService, ClientService } from '../../services/index';
import * as moment from 'moment';
import * as _ from 'lodash';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'shift-client',
    templateUrl: './shift.html',
    styles:[`
        
        @media (max-width: 1000px)  {
            .web-first{
                display:none;
            }
            .mobile-first{
                display:block;
            }

            .stackview {
                display: inline-block;
                width: 100%;
                padding: 15px;            
            }
        }
        

        @media (min-width:1000px) { 
            .web-first{
                display:block;
            }
            .mobile-first{
                display:none;
            }
        }     

        table{
            margin-top:10px;
        }

        table thead tr th{
            font-size:12px;
            font-weight:normal;
        }
        table thead tr th{
            background: linear-gradient(to right, rgb(29, 166, 234), rgb(26, 172, 236));
            color: #fff;
        }
        .form-group label{
            display:block;
        }
        td{
          font-size:11px !important;
        }
        .cancel-btn{
            border: 0;
            padding: 5px 10px;
            border-radius: 3px;
            background: #ff0841e8;
            color: #fff;
        }
        .cancel-btn:hover{
            background: #ff2457e8;
        }
        .cancel-container h3{
            font-weight:600;
            text-align:center;
            color:#ff2457e8;
        }

        .cancel-wrapper-data{
          display: flex;
          margin: 1rem 0;
        }

        .cancel-wrapper-data > div{
          flex: 1;
          padding: 0 0.5rem;
        }
        .cancel-wrapper-data > div:first-child{
          border-right: 1px solid #b7b7b7;
          flex:2;
        }
    `]
})

export class ShiftClient implements OnInit, OnDestroy {
    @Input() id;

    private unsubscribe$ = new Subject()
    date: string = moment(new Date()).format('YYYY/MM/DD');
    user: any;

    tabStream = new Subject<number>();
    tabActive: number;
    settings: any;

    pickedAction: any;
    travelDefault: Dto.TravelDefaults;
    rosternote:string;

    show: boolean = false
    loading: boolean = false;
    toggleAdminFees: boolean = false;   

    timesheets: Array<any>;

    bookingCancellationType: string = "WithNotice";
    bookingCancellationLabel: string = "UNALLOCATED BOOKING";

    cancelBookingAlertOpen: boolean = false;

    constructor(
        private timeS: TimeSheetService,
        private staffS: StaffService,
        private clientS: ClientService,
        private globalS: GlobalService
    ){
        this.tabStream.pipe(          
            // distinctUntilChanged(),
            takeUntil(this.unsubscribe$)
          ).subscribe(data => {
            this.tabActive = data;
            this.timesheets = []
            this.loading = true;
            switch(data){
                case 0:
                    this.getShiftPending().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                      this.timesheets = data;
                      this.loading = false;
                    });
                    break;
                case 1:
                    this.getShiftAccepted().pipe(takeUntil(this.unsubscribe$)).subscribe(data => { 
                        this.timesheets = data;
                        this.loading = false;
                    });
                    break;
                case 2:
                    this.getShiftCompleted().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                      this.timesheets = data;
                      this.loading = false;
                    });
                    break;
                case 3:
                    this.getShiftApproved().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                      this.timesheets = data
                      this.loading = false;
                    });
                    break;  
                case 4:
                    this.getShiftQuery().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                      this.timesheets = data;
                      this.loading = false;
                    });
                    break;
                case 5:
                    this.getShiftBilled().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                      this.timesheets = data
                      this.loading = false;
                    });
                    break;
            }
        });
    }

    ngOnInit(){
        this.user = this.id || this.globalS.decode()['code'];
        this.tabStream.next(0);

        this.timeS.getuname(this.user)
            .pipe(
              takeUntil(this.unsubscribe$),
              mergeMap(res => {
                return this.staffS.getsettings(res.uname)
              })
            ).subscribe(settings => {
              this.settings = settings;
            });
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    getShiftPending(): Observable<any>{
        return this.timeS.getshiftbooked({
            RecipientCode: this.user,
            ShiftDate: this.date,
            Pending: true
        })
    }

    getShiftAccepted(): Observable<any>{
        return this.timeS.getshiftbooked({
            RecipientCode: this.user,
            ShiftDate: this.date,
            Pending: false
        })
    }

    getShiftCompleted(): Observable<any>{
        return this.timeS.getshiftspecific({
            RecipientCode: this.user,
            ShiftDate: this.date,
            Approved: 0
        })
    }

    getShiftApproved(): Observable<any>{
        return this.timeS.getshiftspecific({
            RecipientCode: this.user,
            ShiftDate: this.date,
            Approved: 2
        })
    }

    getShiftQuery(): Observable<any>{
        return this.timeS.getshiftspecific({
            RecipientCode: this.user,
            ShiftDate: this.date,
            Approved: 1
        })
    }

    getShiftBilled(): Observable<any>{
        return this.timeS.getshiftspecific({
            RecipientCode: this.user,
            ShiftDate: this.date,
            Approved: 3
        })
    }


    out_data(data: any) {

      this.reset();

      if (data.option == 'tclaim') {
        const defaults = this.settings.tA_TRAVELDEFAULT;
        var non_charge = defaults.indexOf("CHARGEABLE") > -1 && defaults.indexOf("NON") > -1 ? false : true;
        var travel = defaults.indexOf("WITHIN") > -1 ? true : false;

        this.pickedAction = {
          title: 'Travel Claim',
          option: 'tclaim',
          value: _.cloneDeep(data.value)
        }

        this.travelDefault = {
          TravelType: travel,
          ChargeType: non_charge,
          VehicleType: false,
          StartKM: 0,
          EndKM: 0,
          Notes: ''
        }

      }

      if (data.option == 'claim') {
        this.pickedAction = {
          title: 'Claim Variation',
          option: 'claim',
          value: _.cloneDeep(data.value)
        }

      }

      if (data.option == 'rnote') {
        this.pickedAction = {
          title: 'Roster Note',
          option: 'rnote',
          value: _.cloneDeep(data.value)
        }
        this.rosternote = this.pickedAction.value.note || "";
      }

      if (data.option == 'rincident') {
        this.pickedAction = {
          title: 'Record Incident',
          option: 'rincident',
          value: _.cloneDeep(data.value)
        }
      }

      if (data.option == 'opnote') {
        this.pickedAction = {
          title: 'OP Note',
          option: 'opnote',
          value: _.cloneDeep(data.value)
        }
      }

      if (data.option == 'casenote') {
        this.pickedAction = {
          title: 'Case Note',
          option: 'casenote',
          value: _.cloneDeep(data.value)
        }
      }
      this.show = true;
    }

    reset() {
      this.pickedAction = {
        title: '',
        option: ''
      }
    }

    timeDuration: string;

    computeTime() {
      var timeObj = this.globalS.computeTime(this.pickedAction.value.activity_Time.start_time, this.pickedAction.value.activity_Time.end_Time)
      this.timeDuration = timeObj.durationStr
    }


    currentShift: any;

    setShift(data: any, i: number) {

      this.currentShift = data;

      console.log(this.currentShift)

      if (this.tabActive === 0) {
        this.cancelBookingAlertOpen = true;
      } else {

      }
    }

    
    cancelBooking(){
        var { date_Value, shiftbookNo,  }  = this.currentShift;
        var rosterDate = moment(date_Value).format('YYYY/MM/DD');
        var currentDate = moment(new Date()).format('YYYY/MM/DD');
        var bookTime: string = moment(this.currentShift.activity_Time.start_time).format("HH:MM");

        var dayDiff = Date.parse(rosterDate) - Date.parse(currentDate);
        dayDiff = dayDiff / (1000 * 60 * 60 * 24);

        if (dayDiff <= this.settings.minimumCancellationLeadTime) {
          this.globalS.eToast("Error", `Booking can not be cancelled ${this.settings.minimumCancellationLeadTime} day(s) prior from today's`);
          return;
        }
        
        let booking = {          
          RecordNo: shiftbookNo,
          ServiceType: this.bookingCancellationType,
          RosterDate: rosterDate,
          RosterTime: bookTime,
          Username: this.id,
          TraccsUser: this.globalS.decode().user
        }

        // this.tabStream.next(this.tabActive);
        // this.cancelBookingAlertOpen = false;

        this.clientS.postcancelbooking(booking)
            .subscribe(data => {
              if(data){
                this.globalS.sToast('Success','Booking Cancelled');
                this.tabStream.next(this.tabActive);
              }
              this.cancelBookingAlertOpen = false;
            }, (err: HttpErrorResponse) =>{
              this.globalS.eToast('Error','An error occurred')
        });

    }

    
}
