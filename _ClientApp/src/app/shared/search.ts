
import {switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component, Output, EventEmitter, ElementRef } from '@angular/core'

import { TimeSheetService, GlobalService } from '../services/index';

import { Subject } from 'rxjs';




@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'search',
    templateUrl: './search.html',
    styles:[
        `
        .search-container{
            position:relative;
            min-width:6rem;
        }
        ul{
            position: absolute;
            z-index: 5;
            background: #fff;
            width: 100%;
            border: 1px solid #d6d6d6;
            max-height: 10rem;
            overflow:auto;
            list-style:none;
        }
        i{
            position: absolute;
            right: 0;
            bottom: 4px;
        }
        li:hover{
            background: #efefef;
        }

        li.selected{
            background: #efefef;
        }
        
        li{
            padding:5px;
            cursor:pointer;
        }
        input{
            padding-bottom: 10px;
            text-transform:uppercase;
        }

        
        `
    ]
})

export class Search {
    @Output() pick = new EventEmitter();
    value: any;
    results: Array<any> = [];
    show:boolean = false;

    public modelChanges = new Subject<any>();
    constructor(
        private timeS: TimeSheetService,
        private elem: ElementRef
    ){

        this.modelChanges.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap(data => this.timeS.getrecipientdetails(data)),)            
            .subscribe(data =>{
                this.show = true;
                this.results = data;
            })
    }

    onClick(event: Event){
        if(!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }   

    modelChange(data:any){
        this.modelChanges.next(data);
    }

    selected(data:any){
        this.show = false;
        this.value = data;
        this.pick.emit(data);
    }

    focus(){
        this.show = true;
    }
}