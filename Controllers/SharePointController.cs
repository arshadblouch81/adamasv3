using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

[ApiController]
[Route("api/sharepoint")]
public class SharePointController : ControllerBase
{
    private readonly SharePointService _sharePointService;
  
    public SharePointController(SharePointService sharePointService, IConfiguration configuration)
    {
        _sharePointService = sharePointService;
       
    }
    [HttpGet]
    public IActionResult GetUrl(){
              var authLink =  _sharePointService.Authenticate();

              return Ok(authLink);
        }

    [HttpGet("token")]
    public async Task<IActionResult> GetToken()
    {
        try
        {
            var token = await _sharePointService.GetAccessTokenAsync();
            return Ok(token);
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Error retrieving token: {ex.Message}");
        }
    }

    [HttpGet("documents/list")]
    public async Task<IActionResult> GetDocumentsList()
    {
        try
        {
            var documents = await _sharePointService.GetDocumentLibraryItemsAsync();
            return Ok(documents);
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Error retrieving documents: {ex.Message}");
        }
    }
[HttpGet("documents/download")]
    public async Task<IActionResult> DownloadDocuments()
    {
        string link ="";
        try
        {
            var documents =  await _sharePointService.GetDocumentLibraryItemsAsync();
            foreach (var item in documents)
            {
                link=item;
            }
        }catch (Exception ex)
        {
            return StatusCode(500, $"Error retrieving documents: {ex.Message}");
        }

        return Redirect(link);
    }
    [HttpGet("documents")]
    public async Task<IActionResult> GetDocuments()
    {
        try
        {
            var documents = await _sharePointService.GetDocumentsAsync();
            return Ok(documents);
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Error retrieving documents: {ex.Message}");
        }
    }

    
   
}
