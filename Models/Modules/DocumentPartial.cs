
namespace Adamas.Models.Modules{
    public class DocumentPartial {
        public string DocumentName { get; set; }
        public string Name { get; set; }
        public string FileExtension { get; set; }
        public string DocumentId { get; set; }
    }
}