using System;

namespace Adamas.Models.Modules{
    public class PlanStrategy {
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; } = "";
        public string Detail { get; set; } = "";
        public string? Outcome { get; set; } = "";

        public string StrategyId { get; set; } = "";
        public DateTime? DateAchieved { get; set; } 
    }
}