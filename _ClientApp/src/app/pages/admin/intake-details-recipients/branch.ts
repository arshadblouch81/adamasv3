import { Component, OnInit,OnDestroy, ViewChild } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, GlobalService, ListService } from '@services/index'

import { BsModalComponent } from 'ng2-bs3-modal';
@Component({
    selector: '',
    templateUrl:'./branch.html',
})

export class IntakeBranch implements OnInit, OnDestroy{
    @ViewChild('addBranchModal', { static: false }) addBranchModal: BsModalComponent;

    dbAction: number = 0
    user: any
    private branch$: Subscription
    branches: Array<any>
    branchesList: Array<string>
    loading: boolean

    branchGroup: FormGroup;

    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private listS: ListService,
            private router: Router,
            private globalS: GlobalService,            
            private formBuilder: FormBuilder
        ){
            this.branch$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'branches')){
                    this.search();
                }
            });
    }

    ngOnInit(){
        this.resetGroup();
        this.reloadAll();
    }

    ngOnDestroy(){
        this.branch$.unsubscribe();
    }

    search(){
        this.loading = true;
        this.user = this.sharedS.getPicked();     

        const recipient = this.sharedS.getPicked();         
        this.timeS.getbranches(recipient.uniqueID).subscribe(branches => {
            this.loading = false;
            this.branches = branches;
        })      
    }

    resetGroup(){
        this.branchGroup = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            branch: new FormControl('', [Validators.required]),
            notes: ''            
         })
    }

    branchProcess(){
        this.branchGroup.controls['personID'].setValue(this.user.uniqueID)
        const branch = this.branchGroup.value;
        if(this.dbAction == 0){
            this.timeS.postbranches(branch)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Branch Inserted')
                            }
                        })
        }

        if(this.dbAction == 1){
            this.timeS.updatebranches(branch)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Branch Updated')
                            }
                        })
        }
    }

    reloadAll(){
        this.search()
        this.listDropDowns()
    }

    listDropDowns(){
        this.listS.getintakebranches(this.user.uniqueID)
                    .subscribe(data =>  this.branchesList = data)
    }

    controls(data: any){
        
    }

    updatebranch(data: any){
        this.addBranchModal.open()
        
        this.branchGroup.patchValue({
            branch: data.branch,
            notes: data.notes,
            recordNumber: data.recordNumber
        })
    }

    deletebranch(data: any){
        this.timeS.deletebranches(data.recordNumber)
                    .subscribe(data => {
                        if(data){
                            this.reloadAll()
                            this.globalS.sToast('Success','Branch Deleted')
                        }
                    })
    }


}