import { Component, OnInit, forwardRef, Input, ChangeDetectorRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl, FormGroup, FormBuilder } from '@angular/forms';

import { dateFormat } from '@services/global.service';
import { TABS, Filters } from '@modules/modules';

const noop = () => {  };

import { of, combineLatest, forkJoin, merge, EMPTY, Subject } from 'rxjs';
import { switchMap, startWith, takeUntil } from 'rxjs/operators';

import startOfMonth from 'date-fns/startOfMonth'
import endOfMonth from 'date-fns/endOfMonth'
import format from 'date-fns/format'

import {  ListService , TimeSheetService, GlobalService} from '@services/index';
import { fork } from 'child_process';

@Component({
  selector: 'discharge-program',
  templateUrl: './discharge.html',
  styleUrls: ['./discharge.css'],
  providers: [
    
  ],
})

export class DischargeProgram implements OnInit {

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;  

 openModel: boolean;
  @Input() ViewDischarge:Subject<any>=new Subject();
  dischargeForm: FormGroup;
 topBelow = { top: '20px' }
 oldProgram: any;
 PersonId:any;
 reasons:Array<any> = [];
 listPrograms:Array<any> = [];
 allPrograms:boolean = false;
 accountNo:string;
 lstdischarge:Array<any> = ['*DISCHARGE'];
 lstCategory:Array<any> = ['DISCHARGE'];
 lstUser:Array<any> = ['sysmgr'];
 lstManagers:Array<any> = ['ADMIN'];
 dateFormat: string = 'dd/MM/yyyy';
 timeFormat: string = 'HH:MM';
 token:any;
 
 private unsubscribe: Subject<void> = new Subject();

  constructor(
    private fb: FormBuilder,
    private listS: ListService,
    private cd: ChangeDetectorRef,
    private timeS: TimeSheetService,
    private globalS: GlobalService,

  ) { 
    
  }

  ngOnInit(): void {
    this.buildForm();

    var firstDate = startOfMonth(new Date());
    var endDate =  endOfMonth(new Date());
    this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        

    this.ViewDischarge.subscribe(res=>{
        this.openModel=true;
        this.oldProgram = res.program;
        this.PersonId = res.personId
        this.accountNo = res.accountNo;

        this.dischargeForm.patchValue({
          dischargeDate : new Date(res.startDate)
        });
        this.populateLists();
        this.cd.detectChanges();
       
    });
  }

  buildForm(){
    var fields = {
    
        reason: '',
      dischargeType: '',
      dischargeDate: new Date(),
      dischargeTime: new Date().getTime(),
      timeSpent: new Date().getTime(),
      noteType: '',
      category: '',
      publishToApp: false,
      dischargeNote:'',
      reminderTo:'',
      multipleStaff: false,
      reminderDate: new Date(),
      emailTo:'',
      multipleEmails: false,



    };

    this.dischargeForm = this.fb.group(fields);
  }

  populateLists(){

    let sql=`SELECT DISTINCT rp.Program FROM RecipientPrograms rp WHERE IsNull(rp.ProgramStatus, '') = 'ACTIVE' AND PersonID = '${this.PersonId}' AND rp.Program <> '${this.oldProgram}'`
    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    this.listPrograms = data.map(x => {
      return {
          label: x.program,
          value: x.program,
          checked: false
      }
    });

    let sql2=`SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE'`;
    this.listS.getlist(sql2).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    this.reasons = data.map(x => x.description);
    });
    
    // let sql3=`SELECT  accountNo FROM Staff where accountNo not in ('!INTERNAL','!MULTIPLE')`;
    // this.listS.getlist(sql3).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    // this.lstManagers = data.map(x => x.accountNo);
    // });
    
    let sql4=`SELECT name FROM UserInfo order by Name`;
    this.listS.getlist(sql4).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    this.lstUser = data.map(x => x.name);
    });
   
    this.cd.detectChanges();
  });

  }
  loadallPrograms(){
    if (this.allPrograms){
        let sql=`SELECT DISTINCT UPPER([Name]) FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111)) '`
        this.listS.getlist(sql).subscribe(data => {
        this.listPrograms = data.map(x => {
          return {
              label: x.program,
              value: x.program,
              checked: false
          }
        });
        this.cd.detectChanges();
        });
    
    }
  }
  handleCancel(){
    this.openModel = false;
  }
 
  saveNewPrgram(){
    let data = this.dischargeForm.value;
  
    let selectedPrograms = this.listPrograms.filter(x => x.checked).map(x => x.value).join(',');
    let input= {
        oldProgram: this.oldProgram,
        selectedPrograms : selectedPrograms,
        reason: data.reason,
        PersonID : this.PersonId,                
        dischargeType : data.dischargeType,
        dischargeDate : format(new Date(data.dischargeDate), 'yyyy/MM/dd'),
        dischargeTime : format(new Date(data.dischargeTime),"HH:MM"),
        timeSpent: format(new Date(data.timeSpent),"HH:MM"),
        noteType: data.noteType,
        category: data.category,
        publishToApp: data.publishToApp,
        dischargeNote: data.dischargeNote,
        reminderTo: data.reminderTo,
        reminderDate: format(new Date(data.reminderDate), 'yyyy/MM/dd'),
        emailTo: data.emailTo,
        multipleEmails: data.multipleEmails,
        user: this.token.user,

    }
    this.timeS.transitionProgram(input).pipe(
        takeUntil(this.unsubscribe))
        .subscribe(data => {
            this.globalS.sToast('Success', 'Data saved successfully');
            this.openModel = false;
        });
}
  logs(event){

  }

}
