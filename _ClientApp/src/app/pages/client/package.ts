
import { switchMap, distinctUntilChanged, debounceTime, mergeMap, map, tap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input, Inject } from '@angular/core'
import { APP_BASE_HREF, Location, PlatformLocation, LocationStrategy } from '@angular/common';

import { ClientService, GlobalService } from '../../services/index';
import { Subscription, Subject, Observable } from 'rxjs';
import { MonthPeriodFilter } from '@pipes/pipes';
import * as moment from 'moment';

@Component({
    selector: 'package-client',
    styles: [`
    section.form-block div.form-group label{
        width: 7rem;
        display: inline-block;
    }
    .user-container{
        padding:0.5rem 0;
        font-size:12px;
    }
    .user-wrapper{
        display:inline-block;
        min-width:10rem;
    }
    .user-wrapper p {
        margin:0;
    }
    .user-wrapper ul {
        list-style:none;
    }
    .user-wrapper ul li{
        float:left;
        padding: 7px;
        background: #f5f5f5;
        border: 1px solid #eaeaea;
        border-radius: 3px;
        margin:2px;
    }
    .user-wrapper ul li label{
        display:block;
    }
    .notice{
        font-size: 11px;
        padding: 5px;
        border: 1px solid #eeeeee;
    }
    .package-header{
        font-size: 17px;
        padding: 5px;
        margin: 0;
        color: #3c507d;
    }
    .package-table th{
        font-size:15px;
        padding: 5px !important;
    }
    .package-table th:nth-child(even){
        background: #10a4d9;
        color: #fff;
    }
    .package-table th:nth-child(odd){
        background: #12b6f1;
        color: #fff;
    }
    .total-title{
        background: #81ff81;
        padding: 11px;
    }
    .total-price{
        background: #c5ffc5;
        padding:11px;
    }
    table.package-table tr.table-content td{
        padding-left:1rem;
        line-height: 30px;
    }
    table.package-table tr.table-content:hover{
        background:#cbeaff;
    }
    .table-vertical th,td{
        font-size:11px;
    }
    `],
    templateUrl: './package.html',
    providers: [
        MonthPeriodFilter
    ]
})

export class PackageClient implements OnInit, OnDestroy {

    @Input() id: any;
    @Input() MINUS_MONTH: number = 0;

    URL: string;

    private unsubscribe$ = new Subject()
    private dateStream = new Subject<string>();
    private programStream = new Subject<string>();

    private dateResult$: Observable<any>;
    private programResult$: Observable<any>;

    private subscriptions$: Subscription[] = [];

    user: any;

    openB: any;
    closeB: any;
    continB: any;

    client: string;
    date: any;
    program: any;

    dataPackage: any;
    table: Array<any>;

    programs: Array<string> = [];
    loading: boolean = false;

    constructor(
        private clientS: ClientService,
        private globalS: GlobalService,
        private monthPeriodFilter: MonthPeriodFilter,
        private platformLocation: PlatformLocation,
        private locationStrategy: LocationStrategy
    ) {
        this.URL = `${(this.platformLocation as any).location.origin}${this.locationStrategy.getBaseHref() ? '/' + this.locationStrategy.getBaseHref() : ''}/StaticFiles/package.html`
        console.log(this.URL);

        this.dateResult$ = this.dateStream.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((date: any) => {
                let data: Dto.GetPackage = {
                    Code: this.client,
                    PCode: this.program,
                    Date: moment(this.date).format('YYYY/MM/DD')
                }
                this.dataPackage = data;
                return this.getPackages(data);
            }));

        this.programResult$ = this.programStream.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((program: any) => {
                let data: Dto.GetPackage = {
                    Code: this.client,
                    PCode: this.program,
                    Date: moment(this.date).format('YYYY/MM/DD')
                }
                this.dataPackage = data;
                return this.getPackages(data);
            }))

        this.subscriptions$.push(this.dateResult$.pipe(
            mergeMap(x => {
                return this.clientS
                    .getbalances(this.dataPackage).pipe(
                        map(data => {
                            let balances = data;
                            x.balances = balances;
                            return x;
                        }))
            }))
            .subscribe(data => {
                this.saveFundingInLocalStorage(data);
                this.loading = false;
                this.computeBalances(data);
                this.table = data.list;
            }));

        this.subscriptions$.push(this.programResult$.pipe(
            mergeMap(x => {
                return this.clientS.getbalances(this.dataPackage).pipe(
                    map(data => {
                        let balances = data;
                        x.balances = balances;
                        return x;
                    }))
            }))
            .subscribe(data => {
                this.saveFundingInLocalStorage(data);
                this.loading = false;
                this.computeBalances(data);
                this.table = data.list;
            }));
    }

    ngOnInit() {
        this.loading = true;
        this.client = this.id || this.globalS.decode().code;
        // if(this.globalS.userProfile == null){
        this.clientS.getprofile(this.client).subscribe(data => {
            this.user = data;
        })
        // } else {
        //     this.user = this.globalS.userProfile;
        // }

        this.clientS.getactiveprogram({ IsActive: true, Code: this.client })
            .subscribe(data => {
                this.loading = false;

                this.programs = data.data
                this.program = this.programs[0];
                this.date = moment().subtract(this.MINUS_MONTH, 'months').format('YYYY-MM-DD')
                this.dateStream.next();
            })
    }

    ngOnDestroy() {
        this.subscriptions$.forEach(x => {
            x.unsubscribe();
        })
        this.programStream.next();
        this.dateStream.next();
        this.programStream.complete();
        this.dateStream.complete();
    }

    saveFundingInLocalStorage(data: any) {
        let packageObject = {
            data: data,
            recipient: `${this.user.title} ${this.user.firstName}  ${this.user.surnameOrg}`,
            accountNo: `${this.user.accountNo}`,
            period: this.monthPeriodFilter.transform(this.date),
            openingBalance: this.openB,
            closingBalance: this.closeB,
            contingency: this.continB,
            totalBalance: this.closeB + this.continB
        }
        this.globalS.packageStatement = JSON.stringify(packageObject);
    }

    computeBalances(data: any): void {
        if (data && data.list && (data.list).length == 0) {
            this.openB = 0;
            this.closeB = 0;
            this.continB = 0;
            return;
        }
        this.openB = (data.balances.list).length > 0 ? this.getOpeningBalance(data.balances.list) : 0;
        this.closeB = (data.balances.list).length > 0 ? this.getClosingBalance(data.balances.list) : 0;
        this.continB = (data.balances.list).length > 0 ? this.getContingencyBalance(data.balances.list) : 0;
    }

    getContingencyBalance(data: any) {
        let sum = 0;
        data.forEach(el => {
            sum = sum + el.bankedContingency
        });
        return sum
    }

    getClosingBalance(date: any): any {
        if (date.length == 2) {
            let maxDate = date[0];
            date.forEach(data => {
                if (new Date(data.periodEnd) > new Date(maxDate.periodEnd))
                    maxDate = data;
            });
            return maxDate.balance;
        }
    }

    getOpeningBalance(date: any): any {
        if (date.length == 2) {
            let minDate = date[0];
            date.forEach(data => {
                if (new Date(data.periodEnd) < new Date(minDate.periodEnd))
                    minDate = data;
            });
            return minDate.balance;
        }
    }

    dateChanges(date: any) {
        this.loading = true;
        this.table = null;
        this.dateStream.next(date);
    }

    programChanges(program: any) {
        this.loading = false;
        this.table = null;
        this.programStream.next(program);
    }

    getPackages(data: Dto.GetPackage) {
        return this.clientS.getpackages(data);
    }
}
