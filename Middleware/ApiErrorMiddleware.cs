
using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Adamas.Middleware{

    public class ApiErrorMiddleware{

        private readonly RequestDelegate _next;
        public ApiErrorMiddleware(RequestDelegate next){
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext){
            try{
                //var sampe = httpContext.Authentication.GetAuthenticateInfoAsync("Bearer");
                await _next(httpContext);
            } catch(Exception ex){
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
    
            return context.Response.WriteAsync(new
            {
                Headers = context.Response.Headers,
                StatusCode = context.Response.StatusCode,
                Message = exception
            }.ToString());
        }
    }
}