﻿namespace Adamas.DAL
{
    using System;
    using System.Collections.Generic;
    using DocuSign.eSign.Client;
    using static DocuSign.eSign.Client.Auth.OAuth;
    using Adamas.Helper;
    public class JWTAuthService
    {
        public static OAuthToken AuthenticateWithJWT(string api, string clientId, string impersonatedUserId, string authServer, byte[] privateKeyBytes)
        {
            var docuSignClient = new DocuSignClient();
            var apiType = Enum.Parse<ExamplesAPIType>(api);
            var scopes = new List<string>
                {
                    "signature",
                    "impersonation",
                };
            if (apiType == ExamplesAPIType.Rooms)
            {
                scopes.AddRange(new List<string>
                {
                    "dtr.rooms.read",
                    "dtr.rooms.write",
                    "dtr.documents.read",
                    "dtr.documents.write",
                    "dtr.profile.read",
                    "dtr.profile.write",
                    "dtr.company.read",
                    "dtr.company.write",
                    "room_forms",
                });
            }

            if (apiType == ExamplesAPIType.Click)
            {
                scopes.AddRange(new List<string>
                {
                    "click.manage",
                    "click.send",
                });
            }

            if (apiType == ExamplesAPIType.Monitor)
            {
                scopes.AddRange(new List<string>
                {
                    "signature",
                    "impersonation",
                });
            }

            if (apiType == ExamplesAPIType.Admin)
            {
                scopes.AddRange(new List<string>
                {
                    "user_read",
                    "user_write",
                    "account_read",
                    "organization_read",
                    "group_read",
                    "permission_read",
                    "identity_provider_read",
                    "domain_read",
                    "user_data_redact",
                    "asset_group_account_read",
                    "asset_group_account_clone_write",
                    "asset_group_account_clone_read",
                });
            }

            return docuSignClient.RequestJWTUserToken(
                clientId,
                impersonatedUserId,
                authServer,
                privateKeyBytes,
                1,
                scopes);
        }
    }
}
