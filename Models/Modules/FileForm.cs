
namespace Adamas.Models.Modules{
    public class FileForm {
        public int Id { get; set; }
        public string Filename { get; set; }
    }
}