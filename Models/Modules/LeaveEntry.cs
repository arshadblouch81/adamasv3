using System;

namespace Adamas.Models.Modules{
    public class LeaveEntry{
        public string StaffCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Address { get; set; } = "";
        public EmailMessage message { get; set; }
        public CoordinatorEmail CoordinatorEmail { get; set; }
    }
}