import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { HttpClient, HttpClientModule, HttpRequest, HttpEventType, HttpResponse, HttpEvent } from '@angular/common/http';
import { ConnectableObservable, Observable, Subject } from 'rxjs';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, MapService } from '@services/index';

@Component({
    selector: 'app-show-award-setup',
    templateUrl: './awardSetup.component.html',
    styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  },
  textarea{
    resize:none;
  },
  .header{
      background-color: #85B9D5;
      border-color: #85B9D5;
      color: #fff;
      padding: 10px;
  }
  .header-dark-orange{
      line-height: 1;
      padding: 10px;
      background-color: #f18805;
      color: #fff;
      border-color: #f18805;
  }
  .header-orange{
      color: #fff;
      padding: 10px;
      line-height: 1;
      background-color: #ffba08;
      border-color: #ffba08;
  }

//TEST CASE
  .tab-title {
    display: inline-block;
    position: relative;
    align-items: left;
    height: 25px;   
    margin-top: 50px;    
    padding-left: 10px;
    padding-right: 5px;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
    color: #85B9D5;
    border: 1px solid #d0d2d6;
    border-top-left-radius: 15px;
    text-wrap: nowrap;
}
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 74vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}

.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {

min-width: auto;
}

nz-tabset>>>.ant-tabs-bar {
border-bottom: 0px;
}

  `]
})
export class AwardSetupComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    modalOpen: boolean = false;

    loading: boolean = false;
    current: number = 0;
    inputForm: FormGroup;
    dateFormat: string = 'dd/MM/yyyy';
    postLoading: boolean = false;
    check: boolean = false;
    private unsubscribe: Subject<void> = new Subject();
    codeList: Array<any>;  
 
    mainTitle: string = 'Change Ruleset';
    
    constructor(
        private cd: ChangeDetectorRef,
        private router: Router,
        private globalS: GlobalService,
        private listS: ListService,
        private formBuilder: FormBuilder,
        private menuS: MenuService,
        private timesheetS: TimeSheetService,
        private billingS: BillingService,
        private mapS: MapService,
        private modal: NzModalService,
        private http: HttpClient,
    ) { }

    ngOnInit(): void {
        this.buildForm();
        this.loadCodeList();
        this.loading = false;
        this.modalOpen = true;
    }

    buildForm() {
      this.inputForm = this.formBuilder.group({
          name: null,
        });
    }

    resetModal() {
        this.current = 0;
        this.inputForm.reset();
        this.postLoading = false;
    }

    loadCodeList() {
      
      this.billingS.getCodeList().subscribe(data => {
        this.codeList = data;
      });
  }
    handleCancel() {
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/configuration']);
    }

    ngOnChanges(changes: SimpleChanges): void {
      for (let property in changes) {
        if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
          this.isVisible = true;
        }
      }
    }

    runProcess() {
        console.log("Test case work"); 
    }

    onKeyPress(data: KeyboardEvent) {
        return this.globalS.acceptOnlyNumeric(data);
    }
}
