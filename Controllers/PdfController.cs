using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Net.Http;
using System.Net;

using System.Linq;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;

using Microsoft.Extensions.Configuration;

// using DinkToPdf;
// using DinkToPdf.Contracts;
using System.IO;
using System.Text;
using System.Globalization;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class PdfController : ControllerBase
    {
    //     private readonly DatabaseContext _context;

    //     private IConverter _converter;
    //     private IGeneralSetting GenSettings;

    //     public PdfController(DatabaseContext context, IConfiguration config, IConverter converter,IGeneralSetting setting)
    //     {
    //         _context = context;
    //         _converter = converter;
    //         GenSettings = setting;
    //     }

    //     [HttpGet, Route("document/{name}")]
    //     public async Task<IActionResult> GenerateSomeDocument(string name)
    //     {
            
    //         try{
    //             List<dynamic> list = new List<dynamic>();
    //         using(var conn = _context.Database.GetDbConnection() as SqlConnection)
    //         {
    //             using (SqlCommand cmd = new SqlCommand(@"SELECT
    //                                 DISTINCT R.UniqueID
    //                                 ,R.AccountNo
    //                                 ,R.AgencyIdReportingCode
    //                                 ,R.[Surname/Organisation]
    //                                 ,R.FirstName
    //                                 ,R.Branch
    //                                 ,R.RECIPIENT_COORDINATOR
    //                                 ,R.AgencyDefinedGroup
    //                                 ,R.ONIRating
    //                                 ,R.AdmissionDate AS [Activation Date]
                                    
    //                                 ,UPPER([Surname/Organisation]) + ', ' + CASE 
    //                                     WHEN FirstName <> ''
    //                                         THEN FirstName
    //                                     ELSE ' '
    //                                     END AS RecipientName
    //                                 ,CASE 
    //                                     WHEN N1.Address <> ''
    //                                         THEN N1.Address
    //                                     ELSE N2.Address
    //                                     END AS ADDRESS
    //                                 ,CASE 
    //                                     WHEN P1.Contact <> ''
    //                                         THEN P1.Contact
    //                                     ELSE P2.Contact
    //                                     END AS CONTACT
    //                                 ,(
    //                                     SELECT TOP 1 DATE
    //                                     FROM Roster
    //                                     WHERE Type IN (
    //                                             2
    //                                             ,3
    //                                             ,7
    //                                             ,8
    //                                             ,9
    //                                             ,10
    //                                             ,11
    //                                             ,12
    //                                             )
    //                                         AND [Client Code] = R.AccountNo
    //                                     ORDER BY DATE DESC
    //                                     ) AS LastDate
    //                             FROM Recipients R
    //                             LEFT JOIN RecipientPrograms ON RecipientPrograms.PersonID = R.UniqueID
    //                             LEFT JOIN HumanResourceTypes ON HumanResourceTypes.Name = RecipientPrograms.Program
    //                             LEFT JOIN ServiceOverview ON ServiceOverview.PersonID = R.UniqueID
    //                             LEFT JOIN (
    //                                 SELECT PERSONID
    //                                     ,CASE 
    //                                         WHEN Address1 <> ''
    //                                             THEN Address1 + ' '
    //                                         ELSE ' '
    //                                         END + CASE 
    //                                         WHEN Address2 <> ''
    //                                             THEN Address2 + ' '
    //                                         ELSE ' '
    //                                         END + CASE 
    //                                         WHEN Suburb <> ''
    //                                             THEN Suburb + ' '
    //                                         ELSE ' '
    //                                         END + CASE 
    //                                         WHEN Postcode <> ''
    //                                             THEN Postcode
    //                                         ELSE ' '
    //                                         END AS Address
    //                                 FROM NamesAndAddresses
    //                                 WHERE PrimaryAddress = 1
    //                                 ) AS N1 ON N1.PersonID = R.UniqueID
    //                             LEFT JOIN (
    //                                 SELECT PERSONID
    //                                     ,CASE 
    //                                         WHEN Address1 <> ''
    //                                             THEN Address1 + ' '
    //                                         ELSE ' '
    //                                         END + CASE 
    //                                         WHEN Address2 <> ''
    //                                             THEN Address2 + ' '
    //                                         ELSE ' '
    //                                         END + CASE 
    //                                         WHEN Suburb <> ''
    //                                             THEN Suburb + ' '
    //                                         ELSE ' '
    //                                         END + CASE 
    //                                         WHEN Postcode <> ''
    //                                             THEN Postcode
    //                                         ELSE ' '
    //                                         END AS Address
    //                                 FROM NamesAndAddresses
    //                                 WHERE PrimaryAddress <> 1
    //                                 ) AS N2 ON N2.PersonID = R.UniqueID
    //                             LEFT JOIN (
    //                                 SELECT PersonID
    //                                     ,PhoneFaxOther.Type + ' ' + CASE 
    //                                         WHEN Detail <> ''
    //                                             THEN Detail
    //                                         ELSE ' '
    //                                         END AS Contact
    //                                 FROM PhoneFaxOther
    //                                 WHERE PrimaryPhone = 1
    //                                 ) AS P1 ON P1.PersonID = R.UniqueID
    //                             LEFT JOIN (
    //                                 SELECT PersonID
    //                                     ,PhoneFaxOther.Type + ' ' + CASE 
    //                                         WHEN Detail <> ''
    //                                             THEN Detail
    //                                         ELSE ' '
    //                                         END AS Contact
    //                                 FROM PhoneFaxOther
    //                                 WHERE PrimaryPhone <> 1
    //                                 ) AS P2 ON P2.PersonID = R.UniqueID
    //                             WHERE R.[AccountNo] > '!MULTIPLE'
    //                                 AND (
    //                                     [R].[Type] = 'RECIPIENT'
    //                                     OR [R].[Type] = 'CARER/RECIPIENT'
    //                                     )
    //                                 AND (RecipientPrograms.ProgramStatus = 'ACTIVE')
    //                                 AND (
    //                                     (R.AdmissionDate IS NOT NULL)
    //                                     AND (DischargeDate IS NULL)
    //                                     )
    //                             ORDER BY R.[Surname/Organisation]
    //                                 ,R.FirstName",(SqlConnection) conn)){
    //                 await conn.OpenAsync();

    //                 SqlDataReader rd = await cmd.ExecuteReaderAsync();
    //                 while(await rd.ReadAsync())
    //                 {
    //                     list.Add(new {
    //                         Surname = GenSettings.Filter(rd["Surname/organisation"]),
    //                         Firstname = GenSettings.Filter(rd["FirstName"]),
    //                         ActivationDate = GenSettings.Filter(rd["Activation Date"]),
    //                         OniRating = GenSettings.Filter(rd["ONIRating"]),
    //                         Phone = GenSettings.Filter(rd["Contact"]),
    //                         Address = GenSettings.Filter(rd["Address"]),
    //                         LastService = GenSettings.Filter(rd["LastDate"])
    //                     });
    //                 }

    //             }
    //         }
    //         // byte[] data = Convert.FromBase64String(name);
    //         // string decodedString = Encoding.UTF8.GetString(data);

    //         DateTime today = DateTime.Now;
    //         var globalSettings = new GlobalSettings
    //         {
    //             ColorMode = ColorMode.Color,
    //             Orientation = Orientation.Landscape,
    //             PaperSize = PaperKind.A4,
    //             Margins = new MarginSettings { Top = 8 },
    //             DocumentTitle = name,                
    //             //Out = @"C:\Users\mark\Desktop\Programming\netcoreapps\Adamas\Resources\Pdf\sample.pdf"
    //         };

    //         var objectSettings = new ObjectSettings
    //         {
    //             PagesCount = true,
    //             HtmlContent = TemplateGenerator.GetHTMLString(list),
    //             WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet =  Path.Combine(Directory.GetCurrentDirectory(), "assets", "styles.css") },
    //             HeaderSettings = { 
    //                 FontName = "Arial", 
    //                 FontSize = 6, 
    //                 Spacing = 2, 
    //                 Left = today.ToString("MMM dd ,yyyy"), 
    //                 Right = "Page [page] of [toPage]", 
    //                 Center = "Printed by Mark Trinidad", 
    //                 Line = false 
    //             },
    //             //FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" },
    //             IncludeInOutline = true,
    //         };
 
    //         var pdf = new HtmlToPdfDocument()
    //         {
    //             GlobalSettings = globalSettings,
    //             Objects = { objectSettings }
    //         };

    //         byte[] file = _converter.Convert(pdf);

    //            return File(file, "application/pdf");
    //         } catch(Exception ex){
    //             return NotFound(ex.ToString());
    //         }

    //     }

    // }


    // public static class TemplateGenerator
    // {
    //     public static string GetHTMLString(List<dynamic> list)
    //     {
            
    //         var sb = new StringBuilder();
    //         sb.Append(@"
    //                     <html>
    //                         <head>
    //                         </head>
    //                         <body>
    //                             <div style='text-align:center;font-size:24px;margin-top:100px;margin-bottom:20px;padding-top:10px;padding-bottom:10px;background-color:#959595;color:#fff;'>
    //                                REFERRAL LIST
    //                             </div>
    //                             <table>
    //                                 <tr>
    //                                     <th>Name</th>
    //                                     <th>Activation Date</th>
    //                                     <th>OP</th>
    //                                     <th>Phone</th>
    //                                     <th>Address</th>
    //                                     <th>Last Service</th>
    //                                 </tr>");

    //         foreach (var emp in list)
    //         {
    //             dynamic[] sample = new dynamic[] {
    //                 emp.Surname + ", " + emp.Firstname, 
    //                 emp.ActivationDate.ToString("dd/MM/yyyy"),
    //                 emp.OniRating,
    //                 emp.Phone, 
    //                 emp.Address, 
    //                 emp.LastService
    //             };

    //             sb.AppendFormat(@"<tr>
    //                                 <td>{0}</td>
    //                                 <td>{1}</td>
    //                                 <td>{2}</td>
    //                                 <td>{3}</td>
    //                                 <td>{4}</td>
    //                                 <td>{5}</td>
    //                               </tr>", sample);
    //         }
 
    //         sb.Append(@"
    //                             </table>
    //                         </body>
    //                     </html>");
 
    //         return sb.ToString();
    //     }
    }


}
