import { Component, OnInit, OnDestroy, Input, AfterViewInit,ChangeDetectorRef, EventEmitter, Output,Inject, Injectable, Renderer2, ViewEncapsulation} from '@angular/core'
import { Router,ActivatedRoute } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService,NavigationService,TimeSheetService, MenuService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';

import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import { takeUntil } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';

export interface Tab {
    id: number;
    title: string;
    content: string;
  }

@Component({
    templateUrl: './daymanager-main.html',
    styleUrls: ['daymanager-main.css'],
   
  })

export class DayManagerMain implements OnInit, OnDestroy, AfterViewInit{

  private unsubscribe = new Subject();
    
    dateFormat: string ='dd/MM/yyyy';
    dmType: string = 'Staff Management';
    tabs: Tab[] = [];
    loadMain :EventEmitter<any> = new EventEmitter() ;
    today: Date = new Date();
    index = 0;
    theme:string;
    payEndingDate:any ='';
    styleUrl:any;
    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private sanitizer: DomSanitizer,
        private modalService: NzModalService,
        private listS: ListService,
        private globalS:GlobalService,
        private cd: ChangeDetectorRef,
        private nzContextMenuService: NzContextMenuService,
        @Inject (DOCUMENT) private  document:Document,
        private renderer: Renderer2,
        ){

      this.listS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.payEndingDate=data.payPeriodEndDate;
            console.log(this.payEndingDate);
          });
        }
        loadTheme(theme: string): void {
          switch (theme) {
              case 'DARK':
                 this.styleUrl = './daymanager-main_dark.css';    
                break;
              case 'LIGHT':
                this.styleUrl = './daymanager-main_light.css';
                break;
              case 'CLASSIC':
                this.styleUrl = './daymanager-main.css';
                break;
            }
      
            const linkElement = this.document.createElement('link');
              // linkElement.rel = 'stylesheet';
              // linkElement.type = 'text/css';
              // linkElement.href =  this.styleUrl;
              // this.document.head.appendChild(linkElement);
             
             // const linkElement = this.renderer.createElement('link');
              linkElement.setAttribute('rel', 'stylesheet');
              linkElement.setAttribute('type', 'text/css');
              linkElement.setAttribute('href', this.styleUrl); // Provide the path to the CSS file
          // document.head.appendChild(linkElement)
              this.renderer.appendChild(this.document.head, linkElement);
        
      }
        ngOnInit(): void {
       // this.loadTheme('LIGHT')
             this.addTab();

             this.loadMain.emit('daymanager-main');
        }
        ngAfterViewInit(): void {
            
        }
        setpayperiod(data:any){

          this.payEndingDate=data.payPeriodEndDate;


        }
        onChangeDmtype(val: any): void {
          this.dmType=val;
          this.tabs[this.index].title = this.dmType;
          this.theme=this.globalS.getTheme();
        }
        onChange(result: Date): void {
            // console.log('onChange: ', result);
            this.theme=this.globalS.getTheme();
        }  
    
        view(index: number) {
           this.index=index;
           this.dmType=this.tabs[index].title;
           this.cd.detectChanges();
        }
        ngOnDestroy(): void {
        }

        contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent): void {
         
          this.nzContextMenuService.create($event, menu);
        }
        menuclick(event:any,i:number){
          if (i==1){
            this.addTab()
  
          }else if (i==2){
              this.removeTab();
          }
      }

  addTab() {
    const newTab: Tab = {
      id: this.tabs.length + 1,
      title: 'Staff Management', //`View - ${this.tabs.length + 1}`,
      content: `Content for Tab ${this.tabs.length + 1}`
    };
    this.tabs.push(newTab);
    if (this.tabs.length ==2) {
      this.tabs[0].title = this.dmType;
    }
    setTimeout(() => {
      this.index = this.tabs.length - 1;
      this.view(this.index);
     
    }, 100);
    
  }

  removeTab() {
    if (this.tabs.length > 0) {
      this.tabs.pop();
    }
  }

    }