using System;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.DAL;
using Microsoft.AspNetCore.Mvc.Filters;
using AdamasV3.Models.Dto;
using DocumentFormat.OpenXml.Wordprocessing;
using NPOI.SS.Formula.Functions;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class GlobalController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private ILoginService login;

        private IConfiguration _configuration;

        public GlobalController(
            DatabaseContext context,
            ILoginService _login,
            IGeneralSetting setting,
            IConfiguration configuration){
            _context = context;
            GenSettings = setting;
            login = _login;
            _configuration= configuration;
        }

        [HttpGet("current-user")]
        public async Task<IActionResult> GetCurrentUser()
        {
            return Ok(await this.login.GetCurrentUser());
        }

      
        [HttpGet, Route("managers")]
        public async Task<IActionResult> GetManagers(){

            using(var conn = _context.Database.GetDbConnection() as SqlConnection){
                using(SqlCommand cmd = new SqlCommand(@"SELECT sr.RecordNumber, sr.[Description], pf.[Type], pf.Detail FROM ( 
                    SELECT d.RecordNumber, d.[Description], s.UniqueID FROM Staff s 
                    RIGHT JOIN DataDomains d ON d.[HACCCode] = s.[AccountNo] WHERE d.[domain] = 'CASE MANAGERS'
                    ) sr 
                    LEFT JOIN PhoneFaxOther pf ON sr.[UniqueID] = pf.[PersonID] AND 
                    pf.[Type] = '<EMAIL>' ORDER BY sr.Description", (SqlConnection) conn)){
                        await conn.OpenAsync();
                        List<dynamic> list = new List<dynamic>();
                        using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                        {
                            while(await rd.ReadAsync())
                            {
                                list.Add(new {
                                    RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                    Description = GenSettings.Filter(rd["Description"]),
                                    Type = GenSettings.Filter(rd["Type"]),
                                    Detail = GenSettings.Filter(rd["Detail"])                                    
                                });
                            }
                            return Ok(list);
                        }
                    }
            }
        }

        [HttpGet,Route("suburb")]
        public async Task<IActionResult> GetSuburb(Suburb sub){
            try
            {
                // var suburb = await _context.Pcodes.Where(x => x.Postcode.StartsWith((sub.Postcode ?? "").Trim()) 
                //                             && x.Suburb.Contains((sub.SuburbName ?? "").Trim().ToUpper()))
                //                               .Select(x => x).Take(50).ToListAsync();

                var suburb = await _context.Pcodes.Where(x => x.Postcode.StartsWith((sub.Postcode ?? "").Trim()) 
                                        && x.Suburb.Contains((sub.SuburbName ?? "").Trim().ToUpper()))
                                            .Select(x => x).Take(50).ToListAsync();
                if (suburb == null)
                    return NotFound();

                return Ok(suburb);
            } 
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("select-query")]
        public async Task<IActionResult> PostSelectQuery([FromBody] GlobalSelectQueryDto input)
        {
            List<dynamic> dynamicList= new List<dynamic>();

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(input.Query, (SqlConnection)conn))
                    {
                        await conn.OpenAsync();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        int numCol = rd.FieldCount;
                        while (rd.Read())
                        {
                            dynamic _temp = new System.Dynamic.ExpandoObject();

                            // LOOP COLUMNS IN ROW
                            for (var a = 0; a < numCol; a++)
                            {
                                ((IDictionary<String, Object>)_temp).Add(rd.GetName(a), rd.GetValue(a));
                            }
                            dynamicList.Add(_temp);
                        }
                    }
                }
            } catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }

            return Ok(dynamicList);
        }
    }
}
