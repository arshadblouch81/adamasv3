import { Component,ElementRef, forwardRef, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export const RATINGS_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => RatingsComponent),
};


@Component({
    selector: 'ratings',
    templateUrl: './ratings.html',
    styles:[`
    .size{
        font-size:17px;
        cursor:pointer;
    }
    .active{
        color:#18a1ff;
    }
    input{
        display:none
    }
    `],
    providers: [ RATINGS_VALUE_ACCESSOR ]
})

export class RatingsComponent implements ControlValueAccessor {
    @Input() readOnly: boolean = false


    private _onChange = (_: any) => {};
    private _onTouched = () => {};

    //The internal data model
    private innerValue: any = '.';

    noOfStars: number = 7;
    starsArr: Array<any> = this.nOfArrays;
    selectedNo: number;

    component(){    }

    get nOfArrays(){
        let _temp = [];
        for(var a = 1 ; a <= this.noOfStars ; a++){
            _temp.push(a);
        }
        return _temp
    }

    get value(){
        return this.innerValue;
    }

    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this._onChange(v);
        }
    }

    activeStar(data: number){
        if(this.readOnly === true) return;
        if(this.selectedNo == data){
            this.selectedNo = -1;
            this.value = this.getStrStar(-1)
            return;
        }
        this.selectedNo = data;
        this.value = this.getStrStar(data) 
    }

    getStrStar(data: number): string{
        const _temp = '*'
        let emptyStr = '';

        if(data < 0) return emptyStr        
        for(var a = 0 ; a < data ; a++){
            emptyStr+=_temp
        }
        return emptyStr
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {        
        if (value !== this.innerValue) {
            this.innerValue = value;
            this.selectedNo = value ? value.length : 0;
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this._onChange = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this._onTouched = fn;
    }


}