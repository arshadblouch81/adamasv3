using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class ItemTypeInputParameters
    {
        public int Recnum { get; set; }
        public string Status { get; set; }
        public string ProcessClassification { get; set; }
        public string Title { get; set; }
        public string RosterGroup { get; set; }
        public int? HACCId { get; set; }
        public string HACCType { get; set; }
        public double? Amount { get; set; }
        public DateTime? EndDate { get; set; } 
        public string MainGroup { get; set; }
        public string MinorGroup { get; set; }
        public string BudgetGroup { get; set; }
        public string BillText { get; set; }
        public string Lifecycle {get;set;}
        public string dataSet {get;set;}
        public string MinChargeRate {get;set;}
        public string IT_Dataset {get;set;}//dataset
        public string datasetGroup { get; set; }//dataset group  
        public string Unit {get;set;}
        public Boolean? AutoApprove { get; set; }
        public Boolean? ExcludeFromAutoLeave { get; set; }
        public Boolean? InfoOnly { get; set; }
        public string AccountingIdentifier {get;set;}
        public string GLRevenue {get;set;}
        public string Job {get;set;}
        public string GLCost {get;set;}
        // public string NDIA_ID {get; set;}
        public string UnitCostUOM { get; set; }
        public string UnitCost { get; set; }
        public Boolean? ExcludeFromPayExport { get; set; }
        public Boolean? ExcludeFromUsageStatements { get; set; }
        public Double? Price2 { get; set; }
        public Double? Price3 { get; set; }
        public Double? Price4 { get; set; }
        public Double? Price5 { get; set; }
        public Double? Price6 { get; set; }
        public Boolean? excludeFromConflicts { get; set; }
        public Boolean? noMonday { get; set; }
        public Boolean? noTuesday { get; set; }
        public Boolean? noWednesday { get; set; }
        public Boolean? noThursday { get; set; }
        public Boolean? noFriday { get; set; }
        public Boolean? noSaturday { get; set; }
        public Boolean? noSunday { get; set; }
        public Boolean? noPubHol { get; set; }
        public string StartTimeLimit { get; set; }
        public string EndTimeLimit { get; set; }
        public int? MaxDurtn {get; set;}
        public int? MinDurtn {get; set;}
        public int? FixedTime {get; set;}
        public Boolean? NoChangeDate  { get; set; }
        public Boolean? NoChangeTime  { get; set; }
        public int? TimeChangeLimit {get; set;}
        public string DefaultAddress { get; set; }
        public string DefaultPhone { get; set; }
        public Boolean? AutoActivityNotes  { get; set; }
        public Boolean? AutoRecipientDetails  { get; set; }
        public Boolean? DeletedRecord { get;set;}
        public Boolean? HACCUse {get;set;}
        public Boolean? CSTDAUse {get;set;}
        public Boolean? NRCPUse {get;set;}
        public Boolean? ExcludeFromRecipSummarySheet {get;set;}
        public Boolean? JobSheetPrompt  { get; set; }
        public string ActivityNotes { get; set; }
        public Boolean? ExcludeFromHigherPayCalculation  { get; set; }
        public Boolean? NoOvertimeAccumulation  { get; set; }
        public Boolean? PayAsRostered  { get; set; }
        public Boolean? ExcludeFromTimebands  { get; set; }
        public Boolean? ExcludeFromInterpretation  { get; set; }
        public string JobType { get; set; }
        public string TA_LOGINMODE { get; set; }
        public Boolean? ExcludeFromClientPortalDisplay  { get; set; }
        public Boolean? ExcludeFromTravelCalc  { get; set; }
        public Boolean? TA_EXCLUDEGEOLOCATION  { get; set; }
        public Boolean? AppExclude1  { get; set; }
        public Boolean? TAExclude1  { get; set; }
        public Boolean? TAEarlyStartTHEmail  { get; set; }
        public Boolean? TALateStartTHEmail  { get; set; }
        public string TALateStartTH { get; set; }
        public string TAEarlyStartTH { get; set; }
        public string TAEarlyStartTHWho { get; set; }
        public string TALateStartTHWho { get; set; }
        public string TANoGoResend { get; set; }
        public string TANoShowResend { get; set; }
        public Boolean? TAEarlyFinishTHEmail  { get; set; }
        public Boolean? TALateFinishTHEmail  { get; set; }
        public string TAEarlyFinishTH { get; set; }
        public string TALateFinishTH { get; set; }
        public string TALateFinishTHWho { get; set; }
        public string TAEarlyFinishTHWho { get; set; }
        public Boolean? TANoWorkTHEmail  { get; set; }
        public Boolean? TAUnderstayTHEmail  { get; set; }
        public Boolean? TAOverstayTHEmail  { get; set; }
        public string TANoWorkTHWho { get; set; }
        public string TAOverstayTHWho { get; set; }
        public string TAUnderstayTHWho { get; set; }
        public string TANoWorkTH { get; set; }
        public string TAUnderstayTH { get; set; }
        public string TAOverstayTH { get; set; }
        public string NDIA_LEVEL2 { get; set; }
        public string NDIA_LEVEL3 { get; set; }
        public string NDIA_LEVEL4 { get; set; }
        public string NDIAClaimType { get; set; }
        public string NDIAPriceType { get; set; }
        public Boolean? NDIATravel {get;set;}

    }
}