using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class Placement
    {

        public string PersonID { get; set; }
        public int? RecordNumber { get; set; } = 0;
        public string Type { get; set; }
        public string Name { get; set; }
        public  DateTime? Date1 { get; set; }
        public  DateTime? Date2 { get; set; }
        public string Notes { get; set; }
        public string Creator { get; set; }
         public bool Referral { get; set; }

          public bool Atc { get; set; }

       
    }
}