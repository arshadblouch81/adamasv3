import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

import { IntakeSelectability } from './intake';
import { ServiceSelectability } from './service-agreement';
import { FormIndex } from '../index'
const routes: Routes = [
    {
        path: '',
        redirectTo: 'intake',
        pathMatch: 'full'
    },
    {
        path: 'intake',
        component: IntakeSelectability
    },
    {
        path: 'service',
        component: ServiceSelectability
    }
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,
    NzSpinModule,
    NzDrawerModule,
    NzButtonModule,
    NzFormModule,
    NzDatePickerModule,
    NzModalModule,
    NzTableModule,
    NzDividerModule,
    NzRadioModule,
    NzCheckboxModule
  ],
  declarations: [
    IntakeSelectability,
    ServiceSelectability,
    FormIndex
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class SelectabilityModule {}

