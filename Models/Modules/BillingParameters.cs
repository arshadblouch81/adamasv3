using System;

namespace Adamas.Models.Modules
{
    public class BillingParameters
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CurrentDate { get; set; }
        public string Description { get; set; }

    }
}