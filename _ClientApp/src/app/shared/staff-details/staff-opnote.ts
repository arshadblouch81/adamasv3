
import {forkJoin,  Observable } from 'rxjs';
import { Component, forwardRef, ViewChild, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormBuilder, FormGroup } from '@angular/forms';

import { GlobalService, TimeSheetService, ListService  } from '@services/index'
import { BsModalComponent } from 'ng2-bs3-modal';

import * as moment from 'moment';

@Component({
    selector: 'staff-opnote',
    templateUrl: './staff-opnote.html',
    styles: [`
        .note-detail{
            display: flex;
            padding: 5px;
            margin: 7px 0;
            background: #f9f9f9;
            border: 1px solid #d8d8d8;
            border-radius: 5px;
        }
        .note-detail > div:first-child{
            flex: 1;
        }
        .note-detail > div:nth-child(2){
            flex:3;
        }
        .note-detail > div:last-child{
            display: flex;
            flex-direction: column;
            min-height: 2rem;
        }
        input[type=radio]{
            width: initial;
        }
        .grouping-checkbox input[type=radio]{
            margin-right:4px;
        }
        .grouping-checkbox label{
            cursor:pointer;
        }
        ul{
            list-style:none;
        }
        li div{
            margin:0;
        }
        .casemanager-list{
            overflow-y: auto;
            max-height: 5rem;
            border: 1px solid #c3c3c3;
            border-radius: 2px;
            margin-bottom: 10px;
            padding:3px 17px;
        }
        .casemanager-list li div{
            display:inline-block;
        }
        .casemanager-list li span{
            float:right
        }
        .casemanager-list li input[type=checkbox]{
            width: initial;
            margin-top:6px;
            margin-right:6px;
        }
    `],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => StaffOpComponent),
        multi: true
    }]
})

export class StaffOpComponent implements OnInit, ControlValueAccessor {
    @ViewChild('addopnoteModal', { static: false }) addopnoteModal: BsModalComponent;

    onChange = (_: any) => { };
    onTouch: () => {};
    dbAction = 0;

    private value: any;
    categoryOP: Array<string>  = [];
    opGroup: FormGroup
    
    notes: Array<any>;
    managerArray: Array<string> = [];

    opnoteModalObject: {
        category?: Array<string>,
        program?: Array<string>,
        discipline?: Array<string>,
        caredomain?: Array<string>,
        manager?: Array<string>
    }

    ngOnInit(){
        this.resetForm();
        this.populate();
    }

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private listS: ListService
    ){

    }

    writeValue(value: any){
        if(value != null){
            this.value = value;
            this.search(this.value);
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    // -----------------------------------------

    search(user: any){        
        this.timeS.getopnotes(user.name).subscribe(data => {
            this.notes = data.map(x => { 
                return { 
                    alarmDate: x.alarmDate,
                    category: x.category,
                    creator: x.creator,
                    discipline: x.discipline,
                    careDomain: x.careDomain,
                    restrictions: x.restrictions,
                    detail: this.globalS.filterFontLiterals(x.detail),
                    date: this.globalS.filterDate(x.detailDate),
                    time: this.globalS.filterTime(x.detailDate),
                    isPrivate: x.isPrivate,
                    recordNumber: x.recordNumber
                }
            });
        })
    }

    resetForm(){
        this.opGroup = this.formBuilder.group({
            notes: '',
            publishToApp: false,
            restrictions: '',
            restrictionsStr: 'public',
            alarmDate: null,
            whocode: '',
            program: '*VARIOUS',
            discipline:'*VARIOUS',
            careDomain: '*VARIOUS',
            category:'',
            recordNumber: null
        })
    }

    populate(){
        forkJoin([
            this.listS.getstaffcategory(),
            this.listS.getstaffdiscipline(),
            this.listS.getstaffcaredomain(),
            this.timeS.getmanagerop(),
        ]).subscribe(data => {
            this.opnoteModalObject = {
                category: data[0],
                discipline: data[1],
                caredomain: data[2],
                manager: data[3]
            }
            this.opnoteModalObject.discipline.push("*VARIOUS");
            this.opnoteModalObject.caredomain.push("*VARIOUS");
        })
    }

    updateopnote(data:any){
        this.addopnoteModal.open();
        this.opGroup.patchValue({
            notes: data.detail,
            category: data.category,
            isPrivate: data.isPrivate,
            discipline: data.discipline,
            careDomain: data.careDomain,
            restrictions: data.restrictions,
            restrictionsStr: data.restrictions == '' && data.isPrivate 
                                ? 'workgroup' : data.restrictions !== '' && !data.isPrivate ? 'restrict' : 'public',
            alarmDate: data.alarmDate,
            recordNumber: data.recordNumber
        })
    }

    deleteopnote(id: number){
        this.timeS.deleteopnote(id)
            .subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success','Note Deleted');
                    this.search(this.value);
                    return;
                }
                this.globalS.eToast('Error','Process Not Completed')
            })
    }

    checkManagerIsPresent(manager: string, groupName: string): boolean{
        if(this.dbAction == 0){
            return false;
        }

        if(this.dbAction == 1){
            const formGroup = groupName == 'opnote' ? this.opGroup : this.opGroup;
            if(formGroup.value.restrictions && formGroup.value.restrictions.length > 0){
                const splitStr = formGroup.value.restrictions.split('|')
                var found = false;
                for(var a = 0; a < splitStr.length; a++){
                    if(splitStr[a] === manager){
                        found = true;
                        break;                    
                    }
                }
                return found;
            }
            return false;
        }
    }

    listChckBoxes(state: boolean, name: string){
        if(state)   this.managerArray.push(name);
        else this.managerArray.splice(this.managerArray.indexOf(name), 1)
    }
   
    opNoteProcess(){

        if(moment(this.opGroup.value.alarmDate).isValid()){
            this.opGroup.controls["alarmDate"].setValue(moment(this.opGroup.value.alarmDate).format())
        }

        this.opGroup.controls["whocode"].setValue(this.value.name);        
        if(this.opGroup.value.restrictionsStr !== 'restrict')   this.managerArray = []        
        this.opGroup.controls["restrictions"].setValue(this.listStringify());   
       
        if(this.dbAction == 0){
            this.timeS.postopnote(this.opGroup.value, this.value.uniqueId)
                .subscribe(data => {
                    if(data){
                        this.search(this.value)
                        this.globalS.sToast('Success','OP Note Added')
                    }
                })
        }

        if(this.dbAction == 1){
            this.timeS.updateopnote(this.opGroup.value, this.opGroup.value.recordNumber)
                .subscribe(data => {
                    if(data){
                        this.search(this.value)
                        this.globalS.sToast('Success','OP Note Updated')
                    }
                })
        }
    }

    listStringify(): string{
        let tempStr = '';
        this.managerArray.forEach((data,index,array) =>{
            array.length-1 != index ?
                tempStr+= data.trim() + '|' :
                    tempStr += data.trim() ;                
        });
        return tempStr;
    }
}