import { Injectable } from '@angular/core';
import { GlobalService } from '@services/global.service';
import { SettingsService } from '@services/settings.service';

import { Observable, Subject, EMPTY, of } from 'rxjs';
import {
    CanActivate,
    Router,
    CanActivateChild,
    ActivatedRouteSnapshot,
    ActivatedRoute,
    RouterStateSnapshot
} from '@angular/router';
import { catchError, map, switchMap } from 'rxjs/operators';
// import 'rxjs/add/operator/map'

@Injectable()

export class AdminRouteGuard implements CanActivate, CanActivateChild{

    constructor(
        private globalS: GlobalService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private settingS: SettingsService
    ){

    }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {        
        return true;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {     
        return true;
    }

 
}