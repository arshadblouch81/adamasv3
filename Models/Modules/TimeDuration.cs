using System;
namespace Adamas.Models.Modules{
    public class TimeDuration {
        public DateTime Start_time { get; set; }
        public DateTime End_Time { get; set; }
        public float Duration { get; set; }
        public string calculated_Duration { get; set; }
        public string Time_Value { get; set; }
       
       public string Calculate_EndTime()
        {
            string functionReturnValue = null;
            if (string.IsNullOrEmpty(this.Start_time.Date.ToString()) || this.End_Time.Date.ToString() == "N/A")
            {
                functionReturnValue= "";
                return functionReturnValue;
            }

            DateTime strdate = default(DateTime);

            //strdate = Convert.ToDateTime(DateTime.Now.Date + " " + this.Start_time);
            strdate =  this.Start_time;
            //strdate = Now.Date & " " & Me.Start_time
            strdate = strdate.AddMinutes ( this.Duration * 5);
            this.End_Time = strdate;
            functionReturnValue= this.End_Time.TimeOfDay.ToString();
            return functionReturnValue;

        }

        public string getDuration()
        {
            string addPad = (((this.Duration*5)%60) < 10) ? "0" + Convert.ToString(((this.Duration * 5) % 60)) : Convert.ToString(((this.Duration * 5) % 60));
            string sduration = addPad;
            calculated_Duration = String.Format("{0:t}", Convert.ToString( Math.Truncate((this.Duration * 5) / 60)) + ":" + sduration  ); 
            return calculated_Duration;
        }

        public string getDuration_From_Time()
        {
            string functionReturnValue = null;
            if (string.IsNullOrEmpty(this.Start_time.Date.ToString()) || this.Start_time.ToString() == "N/A")
            {
                functionReturnValue= "00:00";
                return functionReturnValue;
            }

            double duratn = 0;
            string strTime = null;

            if (Convert.ToDateTime(this.Start_time) > Convert.ToDateTime(this.End_Time))
            {
                DateTime dt1 = Convert.ToDateTime(this.Start_time);
                DateTime dt2 = Convert.ToDateTime(this.End_Time);

                functionReturnValue= dt1.Subtract(dt2).ToString();
            }
            else
            {
                DateTime dt1 = Convert.ToDateTime(this.End_Time);
                DateTime dt2 = Convert.ToDateTime(this.Start_time);
                functionReturnValue= dt1.Subtract(dt2).ToString();
                // return dt1.Subtract(dt2); duratn = Math.Abs(DateDiff(DateInterval.Minute, Convert.ToDateTime(this.End_Time), Convert.ToDateTime(this.Start_time)));
            }

            strTime = String.Format("00", Convert.ToString(Math.Truncate(duratn / 60))) + ":" + String.Format("00",Convert.ToString(duratn % 60) );

            functionReturnValue= strTime;
            return functionReturnValue;
        }
    }
}