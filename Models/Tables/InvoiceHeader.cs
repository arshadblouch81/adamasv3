﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Adamas.Models.Tables
{
    public class InvoiceHeader
    {
        [Key]
        public int SqlId { get; set; }
        [Column("Invoice Number")]
        public string InvoiceNumber { get; set; }
        [Column("Client Code")]
        public string ClientCode { get; set; }
        [Column("Terminal Date")]
        public DateTime? TerminalDate { get; set; }
        [Column("Traccs Processing Date")]
        public DateTime? TraccsProcessingDate { get; set; }
        [Column("Vision Processing Date")]
        public DateTime? VisionProcessingDate { get; set; }
        [Column("Invoice Amount")]
        public double? InvoiceAmount { get; set; }
        [Column("Patient Code")]
        public string PatientCode { get; set; }
        [Column("Printed", TypeName = "bit")]
        public bool Printed { get; set; }
        public double? BatchNumber { get; set; }
        [Column("Order Number")]
        public string OrderNumber { get; set; }
        public int? COID { get; set; }
        public int? BRID { get; set; }
        public int? DPID { get; set; }
        [Column("Invoice Tax")]
        public double? InvoiceTax { get; set; }
        public double? Paid { get; set; }
        public string HType { get; set; }
        public string Notes { get; set; }
        public string Package { get; set; }
        public string Type1 { get; set; }
        public string Type2 { get; set; }
        public int? OPID { get; set; }
        [Column("Tagged", TypeName = "bit")]
        public bool? Tagged { get; set; }
        [Column("Updated", TypeName = "bit")]
        public bool? Updated { get; set; }
        [Column("xDeletedRecord", TypeName = "bit")]
        public bool XDeletedRecord { get; set; }
        public DateTime? XEndDate { get; set; }
    }
}
