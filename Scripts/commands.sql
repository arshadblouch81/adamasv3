

Create or ALTER FUNCTION [dbo].[Decrypt] (@KEY VARCHAR(100)) returns VARCHAR(100) 

AS 
BEGIN

DECLARE @COUNTER INT
DECLARE @Output VARCHAR(100)

SET @COUNTER = 1
SET @Output = ''

WHILE @COUNTER <= LEN(@KEY)
       BEGIN          
         
          SET @Output = @Output + CHAR(ASCII(SUBSTRING(@KEY,@COUNTER,@COUNTER))-50)        

          SET @COUNTER = @COUNTER + 1

       END

       RETURN @Output

END
Go

IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'LoggedUsers' AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    CREATE TABLE [dbo].[LoggedUsers](
        [TokenId] [int] IDENTITY(1,1) NOT NULL,
        [Token] [varchar](max) NULL,
        [BrowserName] [varchar](50) NULL,
        [User] [varchar](50) NULL,
        CONSTRAINT [PK_LoggedUsers] PRIMARY KEY CLUSTERED 
        (
            [TokenId] ASC
        ) WITH (
            PAD_INDEX = OFF, 
            STATISTICS_NORECOMPUTE = OFF, 
            IGNORE_DUP_KEY = OFF, 
            ALLOW_ROW_LOCKS = ON, 
            ALLOW_PAGE_LOCKS = ON, 
            OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
        ) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO




ALTER TABLE HumanResources ALTER COLUMN User4 varchar(50);

IF COL_LENGTH ('[dbo].[UserInfo]','CloudAdmin ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD CloudAdmin  bit null;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','RecipientRecordView ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD RecipientRecordView  VARCHAR(200) NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnReferral ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnReferral  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnReferon ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnReferon BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnNotProceeding ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnNotProceeding  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnAssess ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnAssess  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnAdmit ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnAdmit  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnUnwait ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnUnwait  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnDischarge ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnDischarge  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnSuspend ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnSuspend  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnReinstate ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnReinstate  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnDecease ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnDecease  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnAdmin ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnAdmin  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnItem ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnItem  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnPrint ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnPrint  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnRoster ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnRoster  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnMaster ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnMaster  BIT NULL;
END;

IF COL_LENGTH ('[dbo].[UserInfo]','DisableBtnOnHold ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD DisableBtnOnHold  BIT NULL;
END;


IF COL_LENGTH ('[dbo].[UserInfo]','ShowAllRecipients ') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD ShowAllRecipients  BIT;
END;

IF COL_LENGTH ('[dbo].[Qte_Lne]','EndDate') IS NULL
BEGIN
  ALTER TABLE Qte_Lne ADD EndDate DATE;
END;

IF COL_LENGTH ('[dbo].[Qte_Lne]','xDeletedRecord') IS NULL
BEGIN
  ALTER TABLE Qte_Lne ADD xDeletedRecord BIT;
END;

IF COL_LENGTH ('[dbo].[Qte_Hdr]','EndDate') IS NULL
BEGIN
  ALTER TABLE Qte_Hdr ADD EndDate DATE;
END;

IF COL_LENGTH ('[dbo].[Qte_Hdr]','xDeletedRecord') IS NULL
BEGIN
  ALTER TABLE Qte_Hdr ADD xDeletedRecord BIT;
END;

IF COL_LENGTH ('[dbo].[HumanResources]','EndDate') IS NULL
BEGIN
  ALTER TABLE HumanResources ADD EndDate DATE;
END;


IF COL_LENGTH ('[dbo].[ServiceOverview]','EndDate') IS NULL
BEGIN
  ALTER TABLE ServiceOverview ADD EndDate DATE;
END;

IF COL_LENGTH ('[dbo].[HumanResourceTypes]','xDeletedRecord') IS NULL
BEGIN
  ALTER TABLE HumanResourceTypes ADD xDeletedRecord BIT;
END;

IF COL_LENGTH ('[dbo].[IM_DistributionLists]','Branch') IS NULL
BEGIN
  ALTER TABLE IM_DistributionLists ADD Branch VARCHAR(50);
END;

IF COL_LENGTH ('[dbo].[IM_DistributionLists]','Coordinator') IS NULL
BEGIN
  ALTER TABLE IM_DistributionLists ADD Coordinator VARCHAR(50);
END;

/* ClientPortalManagerMethod - dbo.[Registration] */
IF COL_LENGTH ('[dbo].[Registration]','BrandingLogoBig') IS NULL
BEGIN
  ALTER TABLE Registration ADD BrandingLogoBig VARCHAR(255);
END;

/* ClientPortalManagerMethod - dbo.[Registration] */
IF COL_LENGTH ('[dbo].[Registration]','adamas7') IS NULL
BEGIN
  ALTER TABLE Registration ADD adamas7 VARCHAR(50);
END;

/* ClientPortalManagerMethod - dbo.[Registration] */
IF COL_LENGTH ('[dbo].[Registration]','adamas8') IS NULL
BEGIN
  ALTER TABLE Registration ADD adamas8 VARCHAR(50);
END;

/* ClientPortalManagerMethod - dbo.[Registration] */
IF COL_LENGTH ('[dbo].[Registration]','adamas9') IS NULL
BEGIN
  ALTER TABLE Registration ADD adamas9 VARCHAR(50);
END;

/* ClientPortalManagerMethod - dbo.[Registration] */
IF COL_LENGTH ('[dbo].[Registration]','BrandingLogoSmall') IS NULL
BEGIN
  ALTER TABLE Registration ADD BrandingLogoSmall VARCHAR(255);
END;

/* ClientPortalManagerMethod - dbo.[UserInfo] */
IF COL_LENGTH ('[dbo].[UserInfo]','HidePortalBalance') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD HidePortalBalance BIT;
END;

/* ClientPortalManagerMethod - dbo.[Registration] */
IF COL_LENGTH ('[dbo].[Registration]','ClientPortalManagerMethod') IS NULL
BEGIN
  ALTER TABLE Registration ADD ClientPortalManagerMethod VARCHAR(100);
END;


/* AllowBooking - dbo.[UserInfo] */
IF COL_LENGTH ('[dbo].[UserInfo]','AllowBooking') IS NULL
BEGIN
  ALTER TABLE UserInfo ADD AllowBooking BIT;
END;

/* DocusignStatus - dbo.[Documents] */
IF COL_LENGTH ('[dbo].[Documents]','DocusignStatus') IS NULL
BEGIN
  ALTER TABLE Documents ADD DocusignStatus VARCHAR(10);
END;

/* DocusignStatus - dbo.[Documents] */
IF COL_LENGTH ('[dbo].[Documents]','DocusignEnvelopeId') IS NULL
BEGIN
  ALTER TABLE Documents ADD DocusignEnvelopeId VARCHAR(60);
END;

IF COL_LENGTH ('[dbo].[ItemTypes]','ExcludeFromClientPortal') IS NULL
BEGIN
  ALTER TABLE ItemTypes ADD ExcludeFromClientPortal BIT;
END;

IF COL_LENGTH ('[dbo].[Recipients]','BelongsTo') IS NULL
BEGIN
  ALTER TABLE Recipients ADD BelongsTo VARCHAR(25);
END;

/* Stored Procedure CopyAndPasteFileWithOutSharedDocument */

GO
/****** Object:  StoredProcedure [dbo].[CopyAndPasteFileWithOutSharedDocument]    Script Date: 19/04/2021 3:27:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[CopyAndPasteFileWithOutSharedDocument]'))
   exec('CREATE PROCEDURE [dbo].[CopyAndPasteFileWithOutSharedDocument] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[CopyAndPasteFileWithOutSharedDocument](@FileName VARCHAR(250),@SourceDocPath VARCHAR(500), @DestinationDocPath VARCHAR(500))

AS

 

BEGIN
       --DECLARE @FileName               VARCHAR(250);

       --DECLARE @SourceDocPath          VARCHAR(500);

       --DECLARE @DestinationDocPath VARCHAR (500);

       --SET @Filename = 'adamas meeting.docx'

       --SET @SourceDocPath = 'C:\Adamas\aaTestSourceFolder'

       --SET @DestinationDocPath = 'C:\Adamas\aaTestDestinationFolder'

 

       DECLARE @command                                NVARCHAR(1000);

       DECLARE @file_stream                     VARBINARY(max);

       DECLARE @init                                   INT;

       DECLARE @FullDestinationPathFile  NVARCHAR (500);
 

       --SELECT @DestinationDocPath = shareddocumentpath

       --FROM   registration

       --WHERE  shareddocumentpath IS NOT NULL;

 

       SET @FullDestinationPathFile = @DestinationDocPath + '\' + @FileName

	   select @SourceDocPath +'\'+ @FileName;
	   select @FullDestinationPathFile;
       
	   SET @command =

                                         N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX)) FROM OPENROWSET(BULK '''

                                         + @SourceDocPath + '\' + @FileName

                                         + ''',                  SINGLE_BLOB) ROW_SET'

 

       EXEC Sp_executesql @command, N'@file_stream1 VARBINARY(MAX) OUTPUT', @file_stream1 =@file_stream OUTPUT

      

       --SELECT @file_stream,

       SELECT @SourceDocPath, @FileName, @FullDestinationPathFile, @file_stream

 

       EXEC sp_OACreate 'ADODB.Stream', @init OUTPUT;                                                  -- An instace created

       EXEC sp_OASetProperty @init, 'Type', 1;

       EXEC sp_OAMethod @init, 'Open';                                                                               -- Calling a method

       EXEC sp_OAMethod @init, 'Write', NULL, @file_stream;                                     -- Calling a method

       EXEC sp_OAMethod @init, 'SaveToFile', NULL, @FullDestinationPathFile, 2;   -- Calling a method

       EXEC sp_OAMethod @init, 'Close';                                                                       -- Calling a method

       EXEC sp_OADestroy @init;                                                                                      -- Closed the resources

-- sp_configure 'show advanced options', 1; 
--GO 

--RECONFIGURE;
--GO 
--sp_configure 'Ole Automation Procedures', 1; 
--GO 

--RECONFIGURE; 

--GO 

END

/* Stored Procedure */
GO
/****** Object:  StoredProcedure [dbo].[GetRecipientList]    Script Date: 12/04/2020 8:56:34 PM ******/
CREATE OR ALTER     PROCEDURE [dbo].[GetRecipientList](@UserID varchar(max), @IncludeInactive bit, @IncludeReferrals bit)
AS BEGIN    

	--DECLARE @UserID AS Varchar (50); 
	DECLARE @IncludeActive varchar(100);
	--DECLARE @IncludeReferrals bit;
	DECLARE @ProgramFilter AS Varchar (MAX);
	DECLARE @BranchFilter AS Varchar (MAX);
	DECLARE @CoordFilter AS VarChar(MAX);
	DECLARE @CategoryFilter AS VarChar (MAX);
	DECLARE @SQLCommand as VarChar (MAX);
	DECLARE @BranchDomain as varchar (20);
	DECLARE @CoordDomain AS varchar (20)
	DECLARE @NUL1 AS Varchar (1)
	DECLARE @Referral AS Varchar (20)
	DECLARE @CodeExclusions as Varchar (5)

	SET @UserID = 'sysmgr'
	SET @IncludeActive = ''
	SET @BranchFilter = '(SELECT UserInfo.ViewFilter FROM UserInfo WHERE [Name] = @UserID)'
	--SET @ProgramFilter = Right(Replace(@ProgramFilter, 'RecipientPrograms', 'PR'), LEN(@ProgramFilter) - CHARINDEX(@ProgramFilter, 'WHERE' + 6))
	SET @CoordFilter = '(SELECT UserInfo.ViewFilter FROM UserInfo WHERE [Name] = @UserID)'
	SET @BranchDomain = '''RECIPBRANCHES'''
	SET @CoordDomain = '''COORDINATOR'''
	SET @NUL1 = ''''''
	SET @CodeExclusions = '''!z'''
	if (@IncludeInactive=0)
	begin
		set @IncludeActive= ' and isnull(AdmissionDate,'''')<>'''' and isnull(dischargeDate,'''')=''''';
	end
	
	SET @SQLCommand = 
	'SELECT DISTINCT AccountNo, UniqueID, AgencyDefinedGroup FROM Recipients	
	LEFT JOIN RecipientPrograms ON Recipients.UniqueID = RecipientPrograms.PersonID 
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [Branch] FROM HumanResources WHERE HumanResources.[Group] = ' + @BranchDomain + ') 	AS BR ON RECIPIENTS.UniqueID = BR.PersonID
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [CoOrd] FROM HumanResources WHERE HumanResources.[Group] = ' + @CoordDomain + ') 	AS CM ON RECIPIENTS.UniqueID = CM.PersonID 
    WHERE Accountno > ' + @CodeExclusions + @IncludeActive


	SET @SQLCommand = @SQLCommand + ' ORDER BY ACCOUNTNO ' 
	EXEC (@SQLCommand)

END

/* Stored Procedure */

GO
/****** Object:  StoredProcedure [dbo].[GetActivities]    Script Date: 18/05/2020 3:13:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.[GetActivities]'))
   exec('CREATE PROCEDURE [dbo].[GetActivities] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[GetActivities] (@Recipient        AS VARCHAR (50),
									   @Program          AS VARCHAR (50),
                                       @ForceAll		 AS BIT,
									   @MainGroup        AS VARCHAR (50),
									   @SubGroup		 AS VARCHAR (50),
									   @ViewType		 AS VARCHAR (50),
									   @AllowedDays		 AS VARCHAR (8),
									   @Duration		 AS VARCHAR (8)
																		)
AS
BEGIN
--DECLARE @Recipient	 AS VARCHAR (50)
--DECLARE @Program	 AS VARCHAR (50)
--DECLARE @ForceAll	 AS BIT
--DECLARE @MainGroup	 AS VARCHAR (50)
--DECLARE @SubGroup	 AS VARCHAR (50)
--DECLARE @ViewType	 AS VARCHAR (50)
--DECLARE @AllowedDays AS VARCHAR (8)
--DECLARE @Duration	 AS VARCHAR (8)

SET @Recipient = ''''+ @Recipient +''''
SET @Program = ''''+ @Program +''''
SET @MainGroup = '''' + @MainGroup+ ''''
SET @ForceALL = @ForceAll
SET @ViewType = @ViewType
SET @AllowedDays = @AllowedDays
SET @Duration = @Duration

    DECLARE @Extent As VARCHAR (max)
    DECLARE @GROUPS As VARCHAR (max)
    DECLARE @AllSQL As VARCHAR (max)
    DECLARE @MainGroupSQL As VARCHAR (max)
	DECLARE @SubGroupSQL AS VARCHAR (max)
    DECLARE @RecipientLinkedSQL As VARCHAR (max)
    DECLARE @ProgramLinkedSQL As VARCHAR (max)
    DECLARE @MealLinkedSQL As VARCHAR (max)
    DECLARE @ProgramActivityCountSQL As VARCHAR (max)
    DECLARE @Caption As VARCHAR (max)
    DECLARE @DayTimeSQL AS VARCHAR (max)
	DECLARE @EnforceActivityLimits AS bit
	DECLARE @BaseItemTypesSQL AS VARCHAR (max)
	DECLARE @BaseProgServSQL AS VARCHAR (max)
	DECLARE @BaseRecipServSQL AS VARCHAR (max)
	DECLARE @FinalSQL AS VARCHAR (max)
	SELECT @EnforceActivityLimits = ISNULL(EnforceActivityLimits, 0) FROM Registration
    IF (@EnforceActivityLimits = 1)
		IF (@AllowedDays <> '') AND (@AllowedDays <> '00000000') --THEN
		
			IF SUBSTRING(@AllowedDays, 1, 1) = '1' SET @DayTimeSQL = 'IsNull(ITM.NoMonday, 0) = 0'
			IF SUBSTRING(@AllowedDays, 2, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoTuesday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoTuesday, 0) = 0'
			  --END IF
		  --END IF
			
			IF SUBSTRING(@AllowedDays, 3, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoWednesday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoWednesday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,4, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoThursday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoThursday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,5, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoFriday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoFriday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,6, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoSaturday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoSaturday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,7, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoSunday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoSunday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,8, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoPubHol, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoPubHol, 0) = 0'
			  --END IF
		  --END IF
		  IF  @DayTimeSQL <> '' --THEN
			SET @DayTimeSQL = ' AND (' + @DayTimeSQL  + ')'
      --END IF
        --@DayTimeSQL = @DayTimeSQL +
        -- AND (' &
        --(ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) OR (ISNULL(ITM.MinDurtn, ' & @Duration & ') <= ' & @Duration & _
        -- AND IsNull(ITM.MaxDurtn, ' & @Duration & ') >= ' & @Duration & '))'
  --END IF
    SET @Extent = ''
	SET @BaseItemTypesSQL = 'SELECT DISTINCT Title FROM ItemTypes itm WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(EndDate, '''') = ''''  OR EndDate > GetDate()) '
	SET @BaseProgServSQL = 'SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes pr ON CONVERT(nVarchar, pr.RecordNumber) = SO.PersonID INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) '
	SET @BaseRecipServSQL = 'SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN Recipients re ON SO.PersonID = re.UniqueID INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) '
--SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO
--INNER JOIN HumanResourceTypes pr ON CONVERT(nVarchar, pr.RecordNumber) = SO.PersonID
--INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title
--WHERE ProcessClassification = 'OUTPUT' AND (ISNULL(itm.EndDate, '') = ''  OR itm.EndDate > GetDate())
--AND pr.Name = 'CBI-78001'
    SET @MainGroup = CASE WHEN @MainGroup = 'ALL' THEN '' ELSE @MainGroup END
	--ITEM LIST - UNFILTERED
	SET @AllSQL = @BaseItemTypesSQL + ' ORDER BY Title'
	--SELECT @AllSQL FROM Registration
	--EXEC (@AllSQL)
    --ITEM LIST FILTERED BY ROSTER GROUP ONLY
	SET @MainGroup = '''ADMISSION'''
    SET @MainGroupSQL = @BaseItemTypesSQL + ' AND itm.RosterGroup = ' + @MainGroup + ' ORDER BY Title'
	--SELECT @MainGroupSQL FROM Registration
	--EXEC (@MainGroupSQL)
    --ITEM LIST FILTERED BY MEAL SUBGROUP
	SET @SubGroup = '''MEALS'''
	SET @SubGroupSQL = @BaseItemTypesSQL +  ' AND itm.MinorGroup = ' + @SubGroup + ' ORDER BY Title'
	--SELECT @SubGroupSQL AS SubGroupLinked FROM Registration
	--EXEC (@SubGroupSQL)
    --SERVICE OVERVIEW LIST FILTERED BY PROGRAM APPROVED SERVICES
    SET @ProgramLinkedSQL = @BaseProgServSQL
							+ CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND pr.Name = ' + @Program END
							+ CASE WHEN ISNULL(@MainGroup, 'ALL') = 'ALL' THEN '' ELSE ' AND itm.RosterGroup = ' + @MainGroup END
							+ ' ORDER BY [Service Type]'
	--SELECT @ProgramLinkedSQL AS ProgLinked FROM Registration
	--EXEC (@ProgramLinkedSQL)
	   
    --'ITEM LIST FILTERED BY RECIPIENT APPROVED
	SET @RecipientLinkedSQL = @BaseRecipServSQL
							  + ' AND re.AccountNo = ' + @Recipient
							  + CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND so.ServiceProgram = ' + @Program END
	--SELECT @RecipientLinkedSQL AS ProgLinked FROM Registration
	--EXEC (@RecipientLinkedSQL)
    If (@FORCEAll = 1) --THEN
		SET @FinalSQL =
        CASE
			WHEN @MainGroup = 'ALL' THEN @AllSQL
			ELSE @MainGroupSQL
		END
    ELSE
		IF @ViewType = 'STAFF' --THEN
            IF @Recipient > '!MULTIPLE'
				SET @FinalSQL =
                CASE
					WHEN @MainGroup IN ('ADMISSION', 'TRAVELTIME') THEN @ProgramLinkedSQL
					WHEN @MainGroup =  'ITEM' THEN @MainGroupSQL
					ELSE @RecipientLinkedSQL
				END
			ELSE
				SET @FinalSQL =
				CASE WHEN @SubGroup = 'MEALS' THEN @MealLinkedSQL
				ELSE @ProgramLinkedSQL
				END
            --END IF
        ELSE
			SET @FinalSQL =
            CASE WHEN  @SubGroup = 'MEALS' THEN @MealLinkedSQL
            ELSE
                CASE
					WHEN @MainGroup IN ('ADMISSION', 'TRAVELTIME') THEN @ProgramLinkedSQL
					WHEN @MainGroup =  'ITEM' THEN @MainGroupSQL
					ELSE @RecipientLinkedSQL
				END
            END
		--END IF
		--SELECT @FinalSQL AS FInalSQL FROM Registration
		EXEC (@FinalSQL)
		
END


/****** Object:  StoredProcedure [dbo].[Copy_MTA_Document_WithoutSharedDocument]    Script Date: 24/06/2021 7:46:23 PM ******/
SET ansi_nulls ON

go

SET quoted_identifier ON

go
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Copy_mta_document_withoutshareddocument') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Copy_mta_document_withoutshareddocument]
GO
CREATE PROCEDURE [dbo].[Copy_mta_document_withoutshareddocument](@PersonID
VARCHAR(50),
                                                                @Extension
VARCHAR(10),
                                                                @FileName
VARCHAR(250),
                                                                @DocPath
VARCHAR(500))
AS
  BEGIN
      DECLARE @FinalPath VARCHAR(500);

      SET @FinalPath= @DocPath + '\\' + @FileName;

  ;
      --SELECT @FinalPath
      DECLARE @command NVARCHAR(1000);
      DECLARE @file_stream VARBINARY(max);

      SET @command =
N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX))                  from OPENROWSET(BULK '''
+ @FinalPath
+ ''',                  SINGLE_BLOB) ROW_SET'

    EXEC Sp_executesql
      @command,
      N'@file_stream1 VARBINARY(MAX) OUTPUT',
      @file_stream1 =@file_stream output

    SELECT @file_stream;
END 


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'CopyAndPasteFile') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CopyAndPasteFile]
GO
CREATE PROCEDURE [dbo].[CopyAndPasteFile](@FileName		   VARCHAR(250), 
                                          @SourceDocPath   VARCHAR(500),
										  @DestinationDocPath VARCHAR (500)) 
AS 

BEGIN 

	--DECLARE @FileName			VARCHAR(250);
	--DECLARE @SourceDocPath		VARCHAR(500);
	--DECLARE @DestinationDocPath VARCHAR (500);
	--SET @Filename = 'adamas meeting.docx'
	--SET @SourceDocPath = 'C:\Adamas\aaTestSourceFolder'
	--SET @DestinationDocPath = 'C:\Adamas\aaTestDestinationFolder'

	DECLARE @command					NVARCHAR(1000); 
	DECLARE @file_stream				VARBINARY(max); 
	DECLARE @init						INT;
	DECLARE @FullDestinationPathFile	NVARCHAR (500);

	SELECT  @DestinationDocPath=replace(@DestinationDocPath,(select  top 1 * from split(@DestinationDocPath,'\')),SharedDocumentPath) 
	FROM    Registration 
	WHERE   SharedDocumentPath is not null; 

	IF ( right(@DestinationDocPath,1)='\')
	BEGIN
		SET @FullDestinationPathFile=@DestinationDocPath +'' + @FileName;
	END
	ELSE BEGIN
		SET @FullDestinationPathFile=@DestinationDocPath +'\' + @FileName;
	END
	
	--SELECT @DestinationDocPath = shareddocumentpath 
	--FROM   registration 
	--WHERE  shareddocumentpath IS NOT NULL; 

	--SET @FullDestinationPathFile = @DestinationDocPath + '\' + @FileName

    SET @command = 
						N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX)) FROM OPENROWSET(BULK '''
						+ @SourceDocPath + '\' + @FileName 
						+ ''',                  SINGLE_BLOB) ROW_SET' 

	EXEC Sp_executesql @command, N'@file_stream1 VARBINARY(MAX) OUTPUT', @file_stream1 =@file_stream OUTPUT 
	
	--SELECT @file_stream, 
	SELECT @SourceDocPath, @FileName, @FullDestinationPathFile, @file_stream

	EXEC sp_OACreate 'ADODB.Stream', @init OUTPUT;								-- An instace created
    EXEC sp_OASetProperty @init, 'Type', 1; 
	EXEC sp_OAMethod @init, 'Open';												-- Calling a method
	EXEC sp_OAMethod @init, 'Write', NULL, @file_stream;						-- Calling a method
	EXEC sp_OAMethod @init, 'SaveToFile', NULL, @FullDestinationPathFile, 2;	-- Calling a method
	EXEC sp_OAMethod @init, 'Close';											-- Calling a method
	EXEC sp_OADestroy @init;													-- Closed the resources

END 


GO
/****** Object:  StoredProcedure [dbo].[Copy_MTA_Document]    Script Date: 30/09/2021 6:07:21 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Copy_MTA_Document') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Copy_MTA_Document]
GO
CREATE PROCEDURE [dbo].[Copy_MTA_Document](@PersonID varchar(50), @Extension varchar(10), @FileName varchar(250), @DocPath varchar(500))  
as  begin  

declare @FinalPath varchar(500);


select  @DocPath=replace(@DocPath,(select  top 1 * from split(@DocPath,'\')),SharedDocumentPath) from Registration 
where SharedDocumentPath is not null; 
if ( right(@DocPath,1)='\')
begin
	set @FinalPath=@DocPath +'' + @FileName;
end
else begin
set @FinalPath=@DocPath +'\' + @FileName;
end


 declare @command nvarchar(1000); 
  DECLARE @file_stream VARBINARY(MAX); 
   set @command = N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX))                  from OPENROWSET(BULK ''' + @FinalPath + ''',                  SINGLE_BLOB) ROW_SET'  
EXEC sp_executesql @command, N'@file_stream1 VARBINARY(MAX) OUTPUT',@file_stream1 =@file_stream OUTPUT  
select @file_stream;      

  

end  


GO
/****** Object:  StoredProcedure [dbo].[CreateClientStaffNote]    Script Date: 30/09/2021 6:24:37 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'CreateClientStaffNote') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CreateClientStaffNote]
GO
CREATE PROCEDURE [dbo].[CreateClientStaffNote]@PersonId     NVARCHAR(max) = '',
                                             @Program      NVARCHAR(max) = '',
                                             @DetailDate   DATETIME,
                                             @ExtraDetail1 NVARCHAR(max) = '',
                                             @ExtraDetail2 NVARCHAR(max) = '',
                                             @WhoCode      NVARCHAR(max) = '',
                                             @PublishToApp BIT = 0,
                                             @Creator      NVARCHAR(max) = '',
                                             @Note         NVARCHAR(max) = '',
                                             @AlarmDate    DATETIME = '',
                                             @ReminderTo   NVARCHAR(max)
AS
    INSERT INTO history
                (personid,
                 program,
                 detaildate,
                 detail,
                 extradetail1,
                 extradetail2,
                 whocode,
                 publishtoapp,
                 creator)
    VALUES      (@PersonId,
                 @Program,
                 Format(@DetailDate, 'MM-dd-yyyy HH:mm:ss', 'en-US'),
                 @ExtraDetail2 + ' : ' + @Program + '-' + @WhoCode + ' '
                 + @Note,
                 @ExtraDetail1,
                 @ExtraDetail2,
                 @WhoCode,
                 @PublishToApp,
                 @Creator)

    IF( @Note IS NOT NULL
        AND @Note <> ''
        AND @AlarmDate IS NOT NULL
        AND @AlarmDate <> '' )
      BEGIN
          INSERT INTO history
                      (personid,
                       program,
                       detaildate,
                       detail,
                       extradetail1,
                       extradetail2,
                       whocode,
                       publishtoapp,
                       creator)
          VALUES      (@PersonId,
                       '',
                       Format(@DetailDate, 'MM-dd-yyyy HH:mm:ss', 'en-US'),
                       @ExtraDetail2 + ' : -' + @WhoCode + ' ' + @Note,
                       'RECIPIENTALERT',
                       @ExtraDetail2,
                       @WhoCode,
                       @PublishToApp,
                       @Creator)

         IF ISNULL(@AlarmDate, '') <> '' AND ISNULL(@ReminderTo, '') <> '' 
			BEGIN
			  INSERT INTO humanresources
						  (personid,
						   [group],
						   type,
						   NAME,
						   date1,
						   date2,
						   notes,
						   creator,
						   reminderscope)
			  VALUES      (@PersonId,
						   'RECIPIENTALERT',
						   'RECIPIENTALERT',
						   'CASE NOTE REMINDER',
						   Format(@AlarmDate, 'yyyy/MM/dd', 'en-US'),
						   Format(@AlarmDate, 'yyyy/MM/dd', 'en-US'),
						   @Note,
						   @ReminderTo,
						   '')
		END
      END 





GO
/****** Object:  StoredProcedure [dbo].[CreateShortRosterEntry]    Script Date: 30/09/2021 9:08:54 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'CreateShortRosterEntry') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[CreateShortRosterEntry]
GO
CREATE PROCEDURE [dbo].[CreateShortRosterEntry]
	@ClientCode NVARCHAR(max),			
	@CarerCode NVARCHAR(max),			
	@ServiceType  NVARCHAR(max),		
	@Program NVARCHAR(MAX),				
	@Date DATETIME,				        
	@Time NVARCHAR(max),                
	@Creator NVARCHAR(max),				
	@Editer NVARCHAR(max),				
	@BillUnit NVARCHAR(max),			
	@AgencyDefinedGroup NVARCHAR(max),  
	@ReferralCode NVARCHAR(max),		
	@Type SMALLINT,						
	@Duration SMALLINT,                 
	@TimePercent NVARCHAR(max),         
	@Notes NVARCHAR(max),               
	@BlockNo REAL,                      
	@ReasonType INT,					
	@TabType NVARCHAR(max),
	@HaccType NVARCHAR(MAX),			
	@BillTo NVARCHAR(MAX),				
	@CostUnit NVARCHAR(MAX)	= 'HOUR',	
	@BillDesc NVARCHAR(MAX) = '',
	@UnitPayRate FLOAT = 0,
	@UnitBillRate FLOAT = 0,
	@TaxPercent FLOAT = 0,
	@APIInvoiceDate NVARCHAR(MAX) = '',
	@APIInvoiceNumber NVARCHAR(MAX) = ''
AS		
SELECT 'START CreateShortRosterEntry'
INSERT INTO
  roster (
    [client code],
    [carer code],
    [service type],
    [service description],
    [program],
    [date],
    [hacctype],
    [start time],
    [duration],
    [unit pay rate],
    [unit bill rate],
    [taxpercent],
    [yearno],
    [monthno],
    [dayno],
    [blockno],
    [notes],
    [type],
    [status],
    [anal],
    [date entered],
    [date last mod],
    [groupactivity],
    [billtype],
    [billto],
    [apinvoicedate],
    [apinvoicenumber],
    [costunit],
    [costqty],
    [billunit],
    [billqty],
    [servicesetting],
    [dischargereasontype],
    [datasetclient],
    [nrcp_referral_service],
    [creator],
    [editer],
    [inuse],
    [billdesc],
    [transferred]
  )
VALUES
  (
    @ClientCode,
	CASE WHEN  @CarerCode = 'INTERNAL' THEN '!INTERNAL' ELSE @CarerCode END, 
    @ServiceType,
    'NOPAY',
    @Program,
    FORMAT(@Date, 'yyyy/MM/dd', 'en-US'),
    @HaccType,
    @Time,
    @Duration,
    @UnitPayRate,
    @UnitBillRate,
    @TaxPercent,
    YEAR(@Date),
    MONTH(@Date),
    DAY(@Date),
    @BlockNo,
    @Notes,
    @Type,
    CASE 
		WHEN @Type IN (7) THEN 2
		ELSE
			CASE 
				WHEN @CarerCode IN ('!INTERNAL') THEN 2 
				ELSE 1 
			END
	END,
    @AgencyDefinedGroup,
    GetDate(),
    GetDate(),
    '0',
    '',
    @BillTo,
    @APIInvoiceDate,
    @APIInvoiceNumber,
    @CostUnit,
    @TimePercent,
    @BillUnit,
    @TimePercent,
    '',
    @ReasonType,
    @ClientCode,
    @ReferralCode,
    @Creator,
    @Editer,
    0,
    @BillDesc,
    0
  )
INSERT INTO
  audit (
    operator,
    actiondate,
    auditdescription,
    actionon,
    whowhatcode,
    traccsuser
  )
VALUES
  (
    @Creator,
    @Date,
    'AUTO CREATE ' + @TabType,
    'ROSTER',
    SCOPE_IDENTITY(),
    @Editer
  )
SELECT 'END reateShortRosterEntry';




ALTER TABLE HumanResources ALTER COLUMN User5 varchar(75);


--GetBillDetails Stored Procedure

GO
/****** Object:  StoredProcedure [dbo].[CreateShortRosterEntry]    Script Date: 30/09/2021 9:08:54 pm ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'GetBillDetails') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[GetBillDetails]
GO
CREATE PROCEDURE [dbo].[GetBillDetails]

@reAccountNo VARCHAR(max),

@hrName VARCHAR (max),

@itTitle VARCHAR (max)

AS

BEGIN

 

SELECT TOP 1 * FROM

(

SELECT 0 AS Precedence, so.[Unit Bill Rate] AS Amount, so.UnitType AS Unit, so.ServiceBiller AS Debtor, so.TaxRate AS GST

       FROM ServiceOverview so

       INNER JOIN RECIPIENTS re ON re.UniqueID = so.PersonID

       WHERE

                     re.Accountno = @reAccountNo

              AND so.ServiceProgram = @hrName

              AND so.[Service Type] = @itTitle

              AND ISNULL(so.ForceSpecialPrice, 0) = 1

 

              UNION

 

              SELECT 1 AS Precedence,

                     CASE WHEN re.BillingMethod = 'COMMERCIAL' THEN  it.Amount

                            WHEN re.BillingMethod = 'PERCENTAGE' THEN it.Amount * (ISNULL(re.PercentageRate, 0)/100)

                           WHEN re.BillingMethod = 'FIXED' THEN 0

                           WHEN re.BillingMethod = 'LEVEL1' THEN it.Price2

                           WHEN re.BillingMethod = 'LEVEL2' THEN it.Price3

                           WHEN re.BillingMethod = 'LEVEL3' THEN it.Price4

                           WHEN re.BillingMethod = 'LEVEL4' THEN it.Price5

                           WHEN re.BillingMethod = 'LEVEL5' THEN it.Price6

                           ELSE it.Amount

                     END AS Amount, it.Unit AS Unit, re.BillTo AS Debtor, pr.GSTRate AS GST

                     FROM ItemTypes it

                     INNER JOIN Recipients re ON re.Accountno = @reAccountNo

                     INNER JOIN HumanResourceTypes pr ON pr.[Name] = @hrName

                     WHERE it.Title = @itTitle

) t ORDER BY Precedence
 

END;



--END

GO
/****** Object:  StoredProcedure [dbo].[getDayManager]    Script Date: 02/03/2022 12:49:01 pm ******/





Create or ALTER   PROCEDURE [dbo].[getDayManager](@sDate varchar(10),@eDate varchar(10), @dmType varchar(10))
As
begin
 if (@dmType='1')
	 SELECT DISTINCT
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees 
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity,  ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], 
	isnull([ro].[ShiftName],'NEVER ASSIGNED') as [ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 1 OR DMStat = 9 )) t  
	ORDER BY [Date], ShiftName, [Start Time], Recipient

 else if (@dmType='2' or @dmType='0' or isnull(@dmType,'')='' )
 --Staff Management
	 SELECT DISTINCT
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED'
	 WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], isnull([ro].[ShiftName],'NEVER ASSIGNED') as [ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	FROM Roster ro LEFT JOIN Recipients ON 
	[ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo] 
	 LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') 
	--AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 3 OR [ro].[Type] = 5 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 OR [ro].[Type] = 1 OR [ro].[Type] = 13 OR ([ro].[Type] = 6 AND [ItemTypes].[MinorGroup] <> 'LEAVE') OR ([ItemTypes].[MinorGroup] = 'LEAVE'))
	) t  
	ORDER BY Staff, Date, [Start Time], Recipient

 else if (@dmType='3')
 --Transport Recipients
	 SELECT DISTINCT 
	ServiceSetting as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') 
	AND ([ItemTypes].[RosterGroup] = 'TRANSPORT' )
	
	) t 
	ORDER BY ServiceSetting, Date, [Start Time], Recipient
else if(@dmType='4')
--Transport Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') 
	AND ([ItemTypes].[RosterGroup] = 'TRANSPORT' )
	) t 
	ORDER BY [Setting/Location], Date, [Start Time], Staff

--else if(@dmType='5')
	--Transport Daily Planner

else if(@dmType='6')
--Facilities Recipients
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
		[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees
 
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'CENTREBASED' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Recipient
	
else if(@dmType='7')
--Facilities Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes, ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'CENTREBASED' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Staff
else if(@dmType='8')
--Group Recipients
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees

    FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'GROUPACTIVITY' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Recipient, Activity

else if(@dmType='9')
--Group Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees
	
	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'GROUPACTIVITY' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], [Carer Code]
else if(@dmType='10')
	--Grp/Trns/Facility- Recipients
	SELECT DISTINCT 
	 [Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees

	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
	,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
		
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 )) t  
	ORDER BY  [Setting/Location], Date, [Start Time], Recipient
else if(@dmType='11')
	--Grp/Trns/Facility-Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
   ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON 
	[ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 3 OR [ro].[Type] = 5 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 OR [ro].[Type] = 1 OR [ro].[Type] = 13 OR ([ro].[Type] = 6 AND [ItemTypes].[MinorGroup] <> 'LEAVE') OR ([ItemTypes].[MinorGroup] = 'LEAVE') )) t  
	ORDER BY [Setting/Location], Date, [Start Time], [Carer Code]

Else if(@dmType='12')
	--Recipient Management
	SELECT DISTINCT 
	recipient [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo,tamode,TA_Multishift,TA_EXCLUDEGEOLOCATION,TA_EXCLUDEFROMAPPALERTS, Attendees

	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes , ro.tamode,ro.TA_Multishift,ro.TA_EXCLUDEGEOLOCATION,ro.TA_EXCLUDEFROMAPPALERTS, ro.Attendees 
   ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch] FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') 
	--AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 )
	) t  
	ORDER BY Recipient, Date, [Start Time], Staff

END





IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMPublishPrintRosters ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMPublishPrintRosters int not null
END
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMTimesheetProcessing ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMTimesheetProcessing int not null
END
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMBilling ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMBilling int not null
END
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMPriceUpdates ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMPriceUpdates int not null
END
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMDEXUploads ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMDEXUploads int not null
END
GO


IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMNDIA ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMNDIA int not null
END
GO


IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMHACC ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMHACC int not null
END
GO


IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMOtherDS ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMOtherDS int not null
END
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMAccounting ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMAccounting int not null
END
GO


IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMAnalyseBudget ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMAnalyseBudget bit not null
END


IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMAtAGlance ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMAtAGlance bit not null
END
GO
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'AccessCDC ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD AccessCDC bit not null
END
GO
GO

IF NOT EXISTS(select 1 from sys.columns  where Name = N'AccessCDC ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD AccessCDC bit not null
END
GO
GO

--IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = N'UserInfo'  and COLUMN_NAME = N'Name')
--BEGIN
--	ALTER TABLE UserInfo ALTER COLUMN [Name] varchar(50);	
--END
--GO
--GO



--IF YOU WANT TO ADD SQL COMMANDS ADD IT AT THE END OF THIS FILE
---


Create or ALTER procedure [dbo].[CheckFundingAlerts](

 

  @RecipientAccount varchar(50),

  @RecipientID VARCHAR (50)

 

  )

AS

BEGIN

   --DECLARE @RecipientAccount VARCHAR (50)

   --DECLARE @RecipientID VARCHAR (50)

   --DECLARE @Temp VARCHAR (20)

-- SET @RecipientID = 'T0100004406'

-- SET @RecipientAccount = 'YURMANOVEV NESSI'

 

   DECLARE @tmpTable TABLE(Program VARCHAR (50), GTotal DECIMAL (8,2), GAdmin DECIMAL (8,2), GAdminOnly DECIMAL (8 ,2), GCMONly DECIMAL (8,2), GService DECIMAL (8, 2))

   DECLARE @tmpFinal TABLE(Program VARCHAR (50), GTotal DECIMAL (8,2), GAdmin DECIMAL (8,2), GAdminOnly DECIMAL (8 ,2), GCMONly DECIMAL (8,2), GService DECIMAL (8, 2), PackageStatus VARCHAR (10))

 

   DECLARE @AlertProfiles TABLE(

                                                   RowID int not null identity(1,1) primary key,

                                                   Program VARCHAR (50),

                                                   Allowed DECIMAL (8, 2),

                                                   CostType VARCHAR (20),

                                                   AP_PerUnit VARCHAR (10),

                                                   AP_Period VARCHAR (15),

                                                   ExpireUsing VARCHAR (20),

                                                   AlertStartDate DATETIME,

                                                   Balance DECIMAL (8,2),

                                                   APFloorAmount DECIMAL (8, 2),

                                                   APMinDesirable DECIMAL (8, 2),

                                                   APMaxDesireable DECIMAL (8, 2)

                                             )

 

   DECLARE @Program VARCHAR (50)

   DECLARE @Allowed DECIMAL (8, 2)

   DECLARE @CostType VARCHAR (20)

   DECLARE @AP_PerUnit VARCHAR (10)

   DECLARE @AP_Period VARCHAR (15)

   DECLARE @ExpireUsing VARCHAR (20)

   DECLARE @AlertStartDate DATETIME

   DECLARE @Balance DECIMAL (8,2)

   DECLARE @APFloorAmount DECIMAL (8, 2)

   DECLARE @APMinDesirable DECIMAL (8, 2)

   DECLARE @APMaxDesireable DECIMAL (8, 2)

   DECLARE @StartDate DATETIME

   DECLARE @EndDate DATETIME

   DECLARE @strStartDate VARCHAR (10)

   DECLARE @strEndDate VARCHAR (10)

 

   DECLARE @i_WeeklyPeriod INT

   DECLARE @l_NoDaysSinceStart INT

   DECLARE @i_NoWeeksSinceStart INT

   DECLARE @i_DaysIntoPeriod AS INT

 

   INSERT INTO @AlertProfiles 

        SELECT DISTINCT Program,

            ISNULL(AP_BasedOn, 0) AS Allowed,

            ISNULL(AP_CostType, '') AS CostType,

            ISNULL(AP_PerUnit, '') AS AP_PerUnit,

            ISNULL(AP_Period, '') AS AP_Period,

            ISNULL(ExpireUsing, '') AS ExpireUsing,

            ISNULL(AlertStartDate, '') AS AlertStartDate,

            0 AS Balance,

            ISNULL(AP_YellowQty, 0) AS AP_YellowQty, 

            ISNULL(AP_OrangeQty, 0) AS AP_OrangeQty, 

            ISNULL(AP_RedQty, 0) AS AP_RedQty 

    FROM RecipientPrograms RP

    WHERE

            Personid =  @RecipientID

            AND (isnull(AP_YellowQty, 0) > 0

                        OR isnull(AP_OrangeQty, 0) > 0

                        OR isnull(AP_RedQty, 0) > 0    

                        )

                 AND ProgramStatus = 'ACTIVE'

                

 

   DECLARE @i int

   DECLARE @max int

   SELECT @i = min(RowID) from @AlertProfiles

   SELECT @max = max(RowID) from @AlertProfiles

 

   SELECT @Max

 

   WHILE @i <= @max begin

          SELECT @Program = Program, @Allowed = Allowed, @CostType = CostType, @AP_PerUnit = AP_PerUnit, @AP_PerUnit = AP_PerUnit, @AP_Period = AP_Period,

                    @ExpireUsing = ExpireUsing, @AlertStartDate = AlertStartDate,

                    @APFloorAmount = @APFloorAmount, @APMaxDesireable = APMaxDesireable, @APMinDesirable = APMinDesirable

          FROM @AlertProfiles WHERE RowID = @i

          BEGIN

                 IF @AP_Period = 'DAY' --THEN

                 BEGIN

                        SET @StartDate = GETDATE()

                        SET @EndDate = GETDATE()

                 END

                 IF @AP_Period = 'WEEK' --THEN

                 BEGIN

                        SET @StartDate = DATEADD(week, DATEDIFF(week, 0, GetDate() - 1), 0)

                        SET @EndDate = DATEADD(day, 6, GetDate())

                 END

                 IF @AP_Period = 'FORTNIGHT' --THEN

                 BEGIN

                        SET @l_NoDaysSinceStart = DateDiff(day, @AlertStartDate, GetDate()) 

                        SET @i_NoWeeksSinceStart = Abs(@l_NoDaysSinceStart / 7)       

                        SET @i_DaysIntoPeriod =  @l_NoDaysSinceStart%(@i_WeeklyPeriod * 7)

                        SET @StartDate = DateAdd(day, -1 * @i_DaysIntoPeriod, GetDate())

                        SET @EndDate = DateAdd(day, (2 * 7) - 1, @StartDate)

                 END

                 IF @AP_Period = '4 WEEKS' --THEN

                 BEGIN

                        SET @l_NoDaysSinceStart = DateDiff(day, @AlertStartDate, GetDate()) 

                        SET @i_NoWeeksSinceStart = Abs(@l_NoDaysSinceStart / 7)       

                        SET @i_DaysIntoPeriod =  @l_NoDaysSinceStart%(@i_WeeklyPeriod * 7)

                        SET @StartDate = DateAdd(day, -1 * @i_DaysIntoPeriod, GetDate())

                        SET @EndDate = DateAdd(day, (4 * 7) - 1, @StartDate)

                 END

                 IF @AP_Period = 'MONTH' --THEN

                 BEGIN

                        SET @StartDate = DATEADD(DAY, 1, EOMONTH(GETDATE(), -1))

                        SET @EndDate = EOMONTH(GETDATE())

                 END

                 IF @AP_Period = '6 WEEKS' --THEN

                 BEGIN

                        SET @l_NoDaysSinceStart = DateDiff(day, @AlertStartDate, GetDate()) 

                        SET @i_NoWeeksSinceStart = Abs(@l_NoDaysSinceStart / 7)       

                        SET @i_DaysIntoPeriod =  @l_NoDaysSinceStart%(@i_WeeklyPeriod * 7)

                        SET @StartDate = DateAdd(day, -1 * @i_DaysIntoPeriod, GetDate())

                        SET @EndDate = DateAdd(day, (6 * 7) - 1, @StartDate)

                 END

                 IF @AP_Period = 'QUARTER' --THEN

                 BEGIN

                        SET @l_NoDaysSinceStart = DateDiff(day, @AlertStartDate, GetDate()) 

                        SET @i_NoWeeksSinceStart = Abs(@l_NoDaysSinceStart / 7)       

                        SET @i_DaysIntoPeriod =  @l_NoDaysSinceStart%(@i_WeeklyPeriod * 7)

                        SET @StartDate = DateAdd(day, -1 * @i_DaysIntoPeriod, GetDate())

                        SET @EndDate = DateAdd(day, (6 * 7) - 1, @StartDate)

                 END

                 SET @strStartDate = convert(varchar, @StartDate, 111)

                 SET @strEndDate = convert(varchar, @EndDate, 111)

 

                 SELECT @RecipientAccount, @Program, @ExpireUsing, @CostType, @strStartDate, @strEndDate, @APMaxDesireable, @APMinDesirable

                 INSERT INTO @tmpTable 

                        EXEC GetPeriodUsage @RecipientAccount, @Program,

                               @strStartDate, @strEndDate,

                               @ExpireUsing, @CostType, 1, '', 0, ''

 

 

          END

  

          set @i = @i + 1

   END          

 

                 INSERT INTO @tmpFinal

                 SELECT *, CASE WHEN Gtotal > @APMaxDesireable THEN 'OVER' WHEN Gtotal < @APMinDesirable THEN 'UNDER' ELSE 'OK' END AS PackageStatus FROM @tmpTable

 

   SELECT * FROM @tmpFinal

   RETURN;

END

 GO

Create or ALTER procedure [dbo].[GetPeriodUsage](

 

  @s_AccountNo varchar(50),

  @s_PackageCode varchar (50),

  @s_StartUsageDate varchar (10),

  @s_EndUsageDate varchar (10),

  @s_ExpireUsing varchar (20),

  @s_BalanceType varchar (20),

  @b_IncludeUnapproved bit,

  @s_Status varchar (50),

  @b_ByDateApproved bit,

  @s_Group varchar (50)

 

  )

AS

 

 

BEGIN

 

   SET NOCOUNT ON;

 

   DECLARE @s_TotalGSSQL varchar (MAX);

   DECLARE @s_UnApproved varchar (50);

   DECLARE @s_DateType varchar (50);

   DECLARE @s_SelCriteria varchar (MAX);

   DECLARE @s_Order varchar (50)

   DECLARE @s_BillText varchar (MAX);

   DECLARE @d_GSTotal DECIMAL(10,2)

   DECLARE @s_Type varchar (20);

   DECLARE @rs_GSTot TABLE (GSTotal varchar (50), [Admin] varchar (50), AdminOnly varchar (50), CMOnly varchar (50), [Services] varchar (50));

  

    IF @b_IncludeUnapproved = 0 --THEN

          SET @s_UnApproved = ' ro.Status > 1 AND '

   ELSE

          SET @s_UnApproved = ''

   --END IF

   --SELECT @s_UnApproved AS Approved

 

   IF @b_ByDateApproved = 1 -- THEN

          SET @s_DateType = ' ro.[Date Timesheet] '

   ELSE

          SET @s_DateType = ' ro.[Date] '

   --END IF

  

   SET @s_SelCriteria = ' FROM Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.Title WHERE '

   +                         '      ro.[Client Code] = ''' + @s_AccountNo  + ''''

   +                         '  AND ro.[Program] = ''' + @s_PackageCode + ''''

   +                         '  AND isnull(it.[ExcludeFromUsageStatements], 0) = 0 '

   +                         '  AND NOT (ro.[Type] = 9 AND ro.[Service Description] = ' + '''CONTRIBUTION''' +')'

   +                         '  AND ro.Date > ' + '''2000/01/01'''

   +                         '  AND ' + @s_UnApproved

    +                                          ' ('  + @s_DateType  + ' BETWEEN ''' + @s_StartUsageDate + ''''

   +                                               ' AND ''' +  @s_EndUsageDate + '''' +')'

 

   --SELECT @s_SelCriteria AS SelCriteria

 

   SET @s_Order = ' ORDER BY DATE '

 

    IF @b_ByDateApproved = 1 --THEN

          SET @s_DateType = 'ro.[Date Timesheet]'

   ELSE

          SET @s_DateType = 'ro.[Date]'

   --END IF     

 

    SET @s_BillText = 

    CASE

    WHEN UPPER(@s_ExpireUsing) = 'CHARGE RATE' THEN

            'SELECT ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''PACKAGE ADMIN''' + ') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS AdminOnlyAmount, ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS CMOnlyAmount, ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') NOT IN (' + '''PACKAGE ADMIN''' + ', ' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS ServiceAmount '

     WHEN UPPER(@s_ExpireUsing) = 'PAY UNIT RATE' THEN

            'SELECT ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''PACKAGE ADMIN''' + ') THEN Convert(Money, [CostQty] * [Unit Pay Rate]) END AS AdminOnlyAmount,  ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [CostQty] * [Unit Pay Rate]) END AS CMOnlyAmount,  ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') NOT IN (' + '''PACKAGE ADMIN''' + ', ' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [CostQty] * [Unit Pay Rate]) END AS ServiceAmount  '

     WHEN UPPER(@s_ExpireUsing) = 'ACTIVITY AVG COST' THEN

            'SELECT ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''PACKAGE ADMIN''' + ') THEN Convert(Money, [BillQty] * [it].[UnitCost]) END AS AdminOnlyAmount,  ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [BillQty] * [it].[UnitCost]) END AS CMOnlyAmount,  ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') NOT IN (' + '''PACKAGE ADMIN''' + ', ' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [CostQty] * [Unit Pay Rate]) END AS ServiceAmount  '

     ELSE

            'SELECT ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''PACKAGE ADMIN''' + ') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS AdminOnlyAmount, ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS CMOnlyAmount, ' +

                        'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') NOT IN (' + '''PACKAGE ADMIN''' + ', ' + '''CASE MANAGEMENT''' + ') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS ServiceAmount '

     END

   

        SET @s_TotalGSSQL =

   CASE

    WHEN @s_BalanceType = 'HOURS' THEN

         'SELECT Sum(IsNull(AdminOnlyAmount,0)) + Sum(IsNull(CMOnlyAmount,0)) + Sum(IsNull(ServiceAmount, 0)) AS Gtotal, Sum(IsNull(AdminOnlyAmount,0)) + Sum(IsNull(CMOnlyAmount,0)) AS GAdmin, Sum(IsNull(AdminOnlyAmount,0)) AS GAdminOnly, Sum(IsNull(CMOnlyAmount,0)) AS GCMOnly, Sum(ServiceAmount) as GService FROM ' +

          '(SELECT ' +

          'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''PACKAGE ADMIN''' + ') THEN Round((Duration * 5) / 60, 2) END AS AdminOnlyAmount,  ' +

          'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''CASE MANAGEMENT''' + ') THEN Round((Duration * 5) / 60, 2) END AS CMOnlyAmount, ' +

         'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') NOT IN (' + '''PACKAGE ADMIN''' + ', ' + '''CASE MANAGEMENT''' + ') THEN Round((Duration * 5) / 60, 2) END AS ServiceAmount  ' +

         @s_SelCriteria + ') t'

   WHEN  @s_BalanceType IN ('INSTANCES', 'SERVICES') THEN

         'SELECT Sum(IsNull(AdminOnlyAmount,0)) + Sum(IsNull(CMOnlyAmount,0)) + Sum(IsNull(ServiceAmount, 0)) AS Gtotal, Sum(IsNull(AdminOnlyAmount,0)) + Sum(IsNull(CMOnlyAmount,0)) AS GAdmin, Sum(IsNull(AdminOnlyAmount,0)) AS GAdminOnly, Sum(IsNull(CMOnlyAmount,0)) AS GCMOnly, Sum(ServiceAmount) as GService FROM ' +

         '(SELECT ' +

         'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''PACKAGE ADMIN''' + ') THEN [BillQty] END AS AdminOnlyAmount,  ' +

         'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') IN (' + '''CASE MANAGEMENT''' + ') THEN [BillQty] END AS CMOnlyAmount, ' +

         'CASE WHEN IsNull(it.MainGroup, ' + '''DIRECT SERVICE''' + ') NOT IN (' + '''PACKAGE ADMIN''' +', ' + '''CASE MANAGEMENT''' + ') THEN [BillQty] END AS ServiceAmount  ' +

         @s_SelCriteria + ') t'

     ELSE 

          'SELECT ISNULL(Sum(Round(IsNull(AdminOnlyAmount,0), 2)) + Sum(Round(IsNull(CMOnlyAmount,0), 2)) + Sum(Round(IsNull(ServiceAmount, 0),2)), 0) AS Gtotal, ' +

        'ISNULL(Sum(Round(IsNull(AdminOnlyAmount,0), 2)) + Sum(Round(IsNull(CMOnlyAmount,0), 2)), 0) AS GAdmin, ' +

        'ISNULL(Sum(Round(IsNull(AdminOnlyAmount,0), 2)), 0) AS GAdminOnly, ' +

        'ISNULL(Sum(Round(IsNull(CMOnlyAmount,0), 2)), 0) AS GCMOnly, ' +

        'ISNULL(Sum(Round(IsNull(ServiceAmount, 0), 2)), 0) as GService FROM (' + @s_BillText + @s_SelCriteria  + ') t'

     END

   --Select @s_ExpireUsing AS ExpireUsing

   --Select @s_BalanceType AS BalanceType

   --Select @s_BillText AS BillSQL

   --Select @s_SelCriteria AS FinalSelCriteria

 

   SET @s_TotalGSSQL = 'SELECT ''' + @s_PackageCode + '''' + ', * FROM (' + @s_TotalGSSQL + ') tF'

 

   --Select @s_TotalGSSQL AS FinalSQL

  

    EXEC (@s_TotalGSSQL)

 

END
GO
 
create or alter procedure getBudgetGrpah (@personId varchar(100), @program varchar(500))
as begin

declare @AccountNo varchar(100);

declare @startDate varchar(10);
declare @endDate varchar(10);
declare @lastRosterDate varchar(10);

select @AccountNo=AccountNo from Recipients where UniqueID=@personId;

select top 1 @lastRosterDate=[Date] from roster r inner join itemtypes i on [service type] = title 
Where     [client code] = @AccountNo  AND Program = @program AND i.MinorGroup IN ('ADMISSION', 'FORMAL REVIEW') ORDER BY DATE Desc 

--select @startDate as startDate, @endDate as endDate,DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, @startDate) + 1, 0))

--set @endDate =convert(varchar, DATEADD(m, DATEDIFF(m, 0, @endDate) + 1, 0),111);

set @endDate=convert(varchar, convert(varchar,year(getDate())) +'/06/30/' ,111);
set @startDate = convert(varchar(4),year(@endDate)-1) + '/07/01/'  ;


--select @startDate as startDate, @endDate as endDate;

SELECT  @AccountNo as  AccountNo, allowed,
       f1,
       f1ou,
       f1ou                                                            AS ACB1,
       f2,
       f2ou,
       f1ou + f2ou                                                     AS ACB2,
       f3,
       f3ou,
       f1ou + f2ou + f3ou                                              AS ACB3,
       f4,
       f4ou,
       f1ou + f2ou + f3ou + f4ou                                       AS ACB4,
       f5,
       f5ou,
       f1ou + f2ou + f3ou + f4ou + f5ou                                AS ACB5,
       f6,
       f6ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou                         AS ACB6,
       f7,
       f7ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou                  AS ACB7,
       f8,
       f8ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou           AS ACB8,
       f9,
       f9ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou    AS ACB9,
       f10,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou                                                         AS ACB10,
       f11,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou                                                 AS ACB11,
       f12,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou                                         AS ACB12,
       f13,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou                                 AS ACB13,
       f14,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou                         AS ACB14,
       f15,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou                 AS ACB15,
       f16,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou         AS ACB16,
       f17,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou AS ACB17,
       f18,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou                                                         AS ACB18,
       f19,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou                                                 AS ACB19,
       f20,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou                                         AS ACB20,
       f21,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou + f21ou                                 AS ACB21,
       f22,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou + f21ou + f22ou                         AS ACB22,
       f23,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou + f21ou + f22ou + f23ou                 AS ACB23,
       f24,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou + f21ou + f22ou + f23ou + f24ou         AS ACB24,
       f25,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou + f21ou + f22ou + f23ou + f24ou + f25ou AS ACB25,
       f26,
       f10ou,
       f1ou + f2ou + f3ou + f4ou + f5ou + f6ou + f7ou + f8ou + f9ou
       + f10ou + f11ou + f12ou + f13ou + f14ou + f15ou + f16ou + f17ou
       + f18ou + f19ou + f20ou + f21ou + f22ou + f23ou + f24ou + f25ou
       + f26ou                                                         AS ACB26
FROM   (SELECT allowed,
               f1,
               f1 - allowed  AS F1OU,
               f2,
               f2 - allowed  AS F2OU,
               f3,
               f3 - allowed  AS F3OU,
               f4,
               f4 - allowed  AS F4OU,
               f5,
               f5 - allowed  AS F5OU,
               f6,
               f6 - allowed  AS F6OU,
               f7,
               f7 - allowed  AS F7OU,
               f8,
               f8 - allowed  AS F8OU,
               f9,
               f9 - allowed  AS F9OU,
               f10,
               f10 - allowed AS F10OU,
               f11,
               f11 - allowed AS F11OU,
               f12,
               f12 - allowed AS F12OU,
               f13,
               f13 - allowed AS F13OU,
               f14,
               f14 - allowed AS F14OU,
               f15,
               f15 - allowed AS F15OU,
               f16,
               f16 - allowed AS F16OU,
               f17,
               f17 - allowed AS F17OU,
               f18,
               f18 - allowed AS F18OU,
               f19,
               f19 - allowed AS F19OU,
               f20,
               f20 - allowed AS F20OU,
               f21,
               f21 - allowed AS F21OU,
               f22,
               f22 - allowed AS F22OU,
               f23,
               f23 - allowed AS F23OU,
               f24,
               f24 - allowed AS F24OU,
               f25,
               f25 - allowed AS F25OU,
               f26,
               f26 - allowed AS F26OU
        FROM   (SELECT '2009.00'           AS Allowed,
                       Sum(Isnull(f1, 0))  AS F1,
                       Sum(Isnull(f2, 0))  AS F2,
                       Sum(Isnull(f3, 0))  AS F3,
                       Sum(Isnull(f4, 0))  AS F4,
                       Sum(Isnull(f5, 0))  AS F5,
                       Sum(Isnull(f6, 0))  AS F6,
                       Sum(Isnull(f7, 0))  AS F7,
                       Sum(Isnull(f8, 0))  AS F8,
                       Sum(Isnull(f9, 0))  AS F9,
                       Sum(Isnull(f10, 0)) AS F10,
                       Sum(Isnull(f11, 0)) AS F11,
                       Sum(Isnull(f12, 0)) AS F12,
                       Sum(Isnull(f13, 0)) AS F13,
                       Sum(Isnull(f14, 0)) AS F14,
                       Sum(Isnull(f15, 0)) AS F15,
                       Sum(Isnull(f16, 0)) AS F16,
                       Sum(Isnull(f17, 0)) AS F17,
                       Sum(Isnull(f18, 0)) AS F18,
                       Sum(Isnull(f19, 0)) AS F19,
                       Sum(Isnull(f20, 0)) AS F20,
                       Sum(Isnull(f21, 0)) AS F21,
                       Sum(Isnull(f22, 0)) AS F22,
                       Sum(Isnull(f23, 0)) AS F23,
                       Sum(Isnull(f24, 0)) AS F24,
                       Sum(Isnull(f25, 0)) AS F25,
                       Sum(Isnull(f26, 0)) AS F26
                FROM   (SELECT [client code],
                               quantity,
                               ro.program,
                               [yearno],
                               [monthno],
                               [date],
                               [start time],
                               recordno,
                               CASE
                                 WHEN ro.date BETWEEN '2023/07/01' AND
                                                      CONVERT(VARCHAR,
                                                      Dateadd(day, 1 *
                                                      14 - 1
                                                      ,
                                                      @startDate), 111)
                               THEN CONVERT(MONEY, [billqty] * [unit bill rate]
                                                   + ( (
                                                   ro.taxpercent / 100 ) * (
                               [billqty] * [unit bill rate]
                               ) ))
                               END AS F1,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 1 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 2 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F2,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 2 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 3 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F3,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 3 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 4 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F4,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 4 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 5 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F5,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 5 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 6 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F6,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 6 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 7 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F7,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 7 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 8 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F8,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 8 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 9 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F9,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR, Dateadd(
                                                      day, 9 *
                                                                       14,
                                                      @startDate),
                                                      111)
                                                      AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 10 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F10,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 10 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 11 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F11,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 11 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 12 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F12,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 12 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 13 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F13,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 13 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 14 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F14,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 14 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 15 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F15,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 15 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 16 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F16,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 16 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 17 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F17,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 17 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 18 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F18,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 18 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 19 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F19,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 19 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 20 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F20,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 20 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 21 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F21,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 21 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 22 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F22,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 22 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 23 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F23,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 23 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 24 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F24,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 24 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 25 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F25,
                               CASE
                                 WHEN ro.date BETWEEN CONVERT(VARCHAR,
                                                      Dateadd(day, 25 * 14,
                                                      @startDate), 111
                                                      ) AND
                                      CONVERT(VARCHAR, Dateadd(
                                      day, 26 *
                                                       14 - 1,
                                                       @startDate),
                                      111) THEN CONVERT(MONEY,
                                 [billqty] * [unit bill rate] + (
                                 ( ro.taxpercent / 100 ) * (
                                 [billqty] * [unit bill rate]
                                 ) ))
                               END AS F26
                        FROM   roster ro
                               INNER JOIN recipients re
                                       ON ro.[client code] = re.accountno
                               INNER JOIN itemtypes it
                                       ON ro.[service type] = it.title
                               INNER JOIN humanresourcetypes pr
                                       ON ro.[program] = pr.[name]
                               INNER JOIN recipientprograms rp
                                       ON re.uniqueid = rp.personid
                                          AND pr.[name] = rp.program
                        WHERE  [client code] > '!z'
                               AND [client code] = @AccountNo
                               AND ro.[program] = @program
                                   
                               AND Isnull(it.[excludefromusagestatements], 0) =
                                   0
                               AND ( NOT ro.[type] = 9
                                     AND ro.[service description] <>
                                         'CONTRIBUTION' )
                               AND ro.date > '2000/01/01'
                               AND ( ro.[date] BETWEEN
                                     @startDate AND @endDate ))
                       T) T1) T2 

end
GO


Create or ALTER   procedure [dbo].[getBudgetSummary](@personID varchar(50))
As
begin

declare @AccountNo  varchar(50);
declare @Program  varchar(150);


SELECT @AccountNo=Accountno FROM Recipients where UniqueID=@personID;

select * from 
(SELECT DISTINCT  RecordNumber, @personID AS PersonID, Program, ISNULL(AP_BasedOn, 0) AS Allowed, ISNULL(AP_CostType, '') AS CostType, ISNULL(AP_PerUnit, '') AS AP_PerUnit,
ISNULL(AP_Period, '') AS AP_Period, ISNULL(ExpireUsing, '') AS ExpireUsing, ISNULL(AlertStartDate, '') AS AlertStartDate, '0' AS Balance,  ISNULL(AP_RedQty, 0) AS [RedAmount],   ISNULL(Ap_OrangeQty, 0) AS [GreenAmount],  ISNULL(AP_YellowQty, 0) AS [YellowAmount] 
FROM RecipientPrograms RP  
WHERE Personid = @personID AND  isnull(ProgramStatus, 'INACTIVE') <> 'INACTIVE' AND  isnull(Program, '') <> '')  P   

inner join
(

SELECT program as bProgram,[Client Code],  Sum(Round(IsNull(AdminOnlyAmount,0), 2)) + Sum(Round(IsNull(CMOnlyAmount,0), 2)) + Sum(Round(IsNull(ServiceAmount, 0),2)) AS Gtotal, 
Sum(Round(IsNull(AdminOnlyAmount,0), 2)) + Sum(Round(IsNull(CMOnlyAmount,0), 2)) AS GAdmin, Sum(Round(IsNull(AdminOnlyAmount,0), 2)) AS GAdminOnly, 
Sum(Round(IsNull(CMOnlyAmount,0), 2)) AS GCMOnly, Sum(Round(IsNull(ServiceAmount, 0), 2)) as GService 
FROM (SELECT  CASE WHEN IsNull(it.MainGroup, 'DIRECT SERVICE') IN ('PACKAGE ADMIN') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS AdminOnlyAmount,
CASE WHEN IsNull(it.MainGroup, 'DIRECT SERVICE') IN ('CASE MANAGEMENT') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS CMOnlyAmount, CASE WHEN IsNull(it.MainGroup, 'DIRECT SERVICE') NOT IN ('PACKAGE ADMIN', 'CASE MANAGEMENT') THEN Convert(Money, [BillQty] * [Unit Bill Rate] + ((ro.TaxPercent/100) * ([BillQty] * [Unit Bill Rate]))) END AS ServiceAmount  
,ro.program,ro.[Client Code]
FROM Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.Title 
WHERE       ro.[Client Code] = @AccountNo  --AND ro.[Program] = 'HCP-L4-ABBOTS MORGANICA 17102011' 
AND isnull(it.[ExcludeFromUsageStatements], 0) = 0   AND NOT (ro.[Type] = 9 AND ro.[Service Description] = 'CONTRIBUTION')  
AND ro.Date > '2000/01/01'   AND  (ro.[Date] BETWEEN dateadd(year,-1,getDate()) AND getDate()) ) t
group by Program,[Client Code]
) b on  b.bProgram=p.Program


--left join
--(

--SELECT Re2.AccountNo,  Pr.[Name] , Re1.UniqueID FROM SharedBudgets SB INNER JOIN Recipients Re1 ON SB.Person1 = Re1.SQLID INNER JOIN HumanResourceTypes Pr ON SB.Program = Pr.RecordNumber 
--left JOIN (SELECT SQLID, Accountno FROM Recipients) AS Re2 ON Re2.SQLID = SB.SharedWith
--WHERE Re1.UniqueID = @personID AND     Pr.[Group] = 'PROGRAMS' -- AND Pr.[Name] = 'HCP-L4-ABBOTS MORGANICA 17102011'
--) R on P.Program=R.Name and R.UniqueID = P.PersonID



End
 
 GO

Create or ALTER procedure [dbo].[spSuspension](
				  
                @personID varchar(100),  
				@startDate datetime, 
				@endDate datetime, 
                @putMasterOnHold bit,
                @recordFormalAbsence bit,
                @record24Absence bit,            
                @allServices bit,
                @cancellation varchar(500),
                @cancellationcode varchar(500),
                @notes varchar(MAX),
                @noteType varchar(50),
                @category varchar(100),
                @reminder varchar(100),
                @reminderDate datetime,                
                @selectedServices  varchar(MAX),
                @email  varchar(100),
				@User varchar(50),
				@WinUser varchar(50))
as
begin
		Declare @ForceSuspendReminder bit;
		Declare @servicType varchar(500);
		Declare @AccountNo varchar(100);
		Declare @RosterNo int;
		Declare @Program varchar(500);
		

		SELECT TOP 1 @ForceSuspendReminder=ForceSuspendReminder FROM Registration
		select @AccountNo=AccountNo from Recipients where UniqueID=@personID;

		

		INSERT INTO HumanResources(PersonID, [GROUP], [Type], [Name], Address1, Notes, LockboxLocation, LockboxCode, SerialNumber, DateInstalled, Date1, Date2) 
		VALUES (@personID, 'RECIPLEAVE', 'RECIPLEAVE', @cancellationCode, @cancellation, @notes, @reminder, @category, @noteType, @reminderDate, @startDate, @endDate )
		
		INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser)
		VALUES (@WinUser, getDate(), 'ADD RECIPIENT LEAVE', @AccountNo, @personID, @user);


		INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) 
		VALUES (@WinUser, getDate(), 'SUSPEND CLIENT', @AccountNo, @personID, @user);

		INSERT INTO History (PersonID, DetailDate, Detail, AlarmDate, WhoCode, Creator ) 
		VALUES (@personID, getDate(), @notes,@reminderDate, @AccountNo, @User)

		--SELECT @servicType=Title FROM ItemTypes WHERE Title = @cancellationCode AND ProcessClassification = 'EVENT';		
		--SELECT @Program=[Program] FROM RecipientPrograms WHERE ProgramStatus = 'ACTIVE' AND PersonID = @personID

		-- Declare the cursor
		DECLARE serviceCursor CURSOR FOR
		SELECT SO.[SERVICE TYPE], RP.[Program]
		FROM RecipientPrograms RP
		INNER JOIN ServiceOverview SO 
			ON SO.[SERVICEPROGRAM] = RP.Program
			AND SO.PersonID = RP.PersonID
		WHERE RP.ProgramStatus = 'ACTIVE'
		  AND RP.PersonID = @personID
		AND ( isnull(@allServices,0)=1 OR SO.[SERVICE TYPE] in (@selectedServices))
		ORDER BY RP.Program, SO.[SERVICE TYPE];

		-- Open the cursor
		OPEN serviceCursor;

		-- Fetch the first row into variables
		FETCH NEXT FROM serviceCursor INTO @servicType, @Program;

		-- Loop through all rows
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Process each row here. For example, you might print the values
		DECLARE @Date1 DATE = @StartDate;  -- Start date
		DECLARE @Date2 DATE = @EndDate;  -- End date

		DECLARE @CurrentDate DATE;

		-- Initialize @CurrentDate with the start date
		SET @CurrentDate = @Date1;

		-- Loop through each date from @Date1 to @Date2
		WHILE @CurrentDate <= @Date2
		BEGIN
			-- Example operation: Print the current date
			--PRINT @CurrentDate;
		
		UPDATE ServiceOverview SET ServiceStatus = 'ON HOLD' WHERE [SERVICEPROGRAM] = @Program AND [SERVICE TYPE] = @servicType AND PERSONID = @personID;

		DELETE FROM ROSTER WHERE ([Roster].[Status] = 1 OR [Roster].[Type] = 14) AND ([Client Code] = @AccountNo) AND ([Program] = @Program) 
		AND ([Service Type] = @servicType) AND (Date BETWEEN @startDate AND @endDate) AND [Type] <> 4;

		if (isnull(@servicType,'')<>'')
		begin

			INSERT INTO Roster([Client Code], [carer code], [Service Type], [Service Description], [program], [Date], [start time], [Duration], [Unit Pay Rate], [Unit Bill Rate], [Yearno], [Monthno], [DayNo], [BlockNo], [Type], [Status], [Date Entered], [Creator], [Date Last Mod], [Date Timesheet], [date invoice], [date payroll], [BillTo], [CostQty], [BillQty], [DatasetClient], InUse, [editer], [Notes], [ROS_COPY_BATCH#], GroupActivity ) 
			VALUES (@AccountNo, '!INTERNAL', @servicType, 'N/A', '!INTERNAL', convert(varchar,@CurrentDate,111), '00:00', 288, 0, 0, year(@CurrentDate), Month(@CurrentDate), 16, 0, 4, 1, getDate(), @WinUser, getDate(), getDate(), getDate(), getDate(), '!INTERNAL', 1, 1, '!INTERNAL', 0, @WinUser, 'ON LEAVE', 0, 0 )

			SELECT @RosterNo= IDENT_CURRENT('Roster') ;

			DELETE FROM ROSTER WHERE DATE = @CurrentDate AND [CARER CODE] = '' AND Type = 4 AND RecordNo <> @RosterNo;
		END	
			
			SET @CurrentDate = DATEADD(DAY, 1, @CurrentDate);

		END

			-- Fetch the next row
			FETCH NEXT FROM serviceCursor INTO @servicType, @Program;
		END

		-- Close and deallocate the cursor
		CLOSE serviceCursor;
		DEALLOCATE serviceCursor;

		UPDATE Recipients SET PAN_Uploaded = 0, RecordAltered = 1 WHERE [Recipients].[UniqueId] = @personID;
		SELECT IDENT_CURRENT('HumanResources') AS [LastID]
End
GO

Create or Alter procedure [dbo].[spSuspension_Update](				  
                @personID varchar(100),  
				@startDate datetime, 
				@endDate datetime, 
                @putMasterOnHold bit,
                @recordFormalAbsence bit,
                @record24Absence bit,            
                @allServices bit,
                @cancellation varchar(500),
                @cancellationcode varchar(500),
                @notes varchar(MAX),
                @noteType varchar(50),
                @category varchar(100),
                @reminder varchar(100),
                @reminderDate datetime,                
                @selectedServices  varchar(MAX),
                @email  varchar(100),
				@User varchar(50),
				@WinUser varchar(50),
				@altProgram varchar(500),
				@Activity varchar(500),
				@PayType varchar(500),
				@billOriginalRate bit,
                @retainOriginalRate bit,
				@RecordNumber int
				)
as
begin
		Declare @ForceSuspendReminder bit;
		Declare @servicType varchar(500);
		Declare @AccountNo varchar(100);
		Declare @RosterNo int;
		Declare @Program varchar(500);
		

		SELECT TOP 1 @ForceSuspendReminder=ForceSuspendReminder FROM Registration
		select @AccountNo=AccountNo from Recipients where UniqueID=@personID;

		
		UPDATE HumanResources SET PersonID = @personID, [Group] = 'RECIPLEAVE', [Type] = 'RECIPLEAVE', [Name] = @cancellationcode, 
		Address1 = @cancellation, Address2 = '', Suburb = @PayType, Email = @Activity, 
		Aprimary = '0', Recurring = '0', EquipmentCode = @altProgram , Notes = 'testset', LockboxLocation = @reminder, 
		LockboxCode = @category, SerialNumber = @noteType, DateInstalled = @reminderDate, Date1 = @startDate, Date2 = @endDate
		WHERE RecordNumber = @RecordNumber

		INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) 
		VALUES (@user, getDate(), 'CHANGE RECIPIENT Leave', @cancellationcode, @AccountNo, @WinUser)

		select @RecordNumber;
End

GO

CREATE OR ALTER   procedure [dbo].[updateDocument](@DocID int, @PersonId varchar(50), @Title varchar(500),@Discipline varchar(500),@Program varchar(500),@CareDomain varchar(500), @Classification varchar(500), 
@category varchar(500), @ReminderText varchar(500), @reminderDate datetime, @PublishToApp bit, @User varchar(50), @notes varchar(MAX), @DocStartDate datetime, @DocEndDate datetime)
As 
Begin


	Declare @programid int;
	Declare @DPID int;
	Declare @DMID int;
	Declare @UserId int;
	Declare @AccountNo varchar(100);
	select @AccountNo from Recipients where UniqueID=@PersonId;

	INSERT INTO History (PersonID, DetailDate, Detail, AlarmDate, WhoCode, PublishToApp, Creator ) VALUES (@PersonId, convert(varchar,getDate(),111), @ReminderText, @reminderDate, @AccountNo,  1, @User)
	SELECT IDENT_CURRENT('History ') AS [LastID];

	SELECT @programid=RecordNumber FROM HumanResourceTypes WHERE [Name] = @Program AND [Group] = 'PROGRAMS';

	SELECT @DPID=RecordNumber FROM DataDomains WHERE [Description] = @Discipline AND [DOMAIN] = 'DISCIPLINE';
	SELECT @DMID=RecordNumber FROM DataDomains WHERE [Description] = @CareDomain AND [DOMAIN] = 'CAREDOMAIN';
	SELECT @UserId=Recnum FROM UserInfo WHERE [Name] = @User;
	SELECT RecordNumber FROM DataDomains WHERE [Description] = '' AND [DOMAIN] = 'CAREPLANTYPES';

	UPDATE Documents SET Title = @Title,  Department = @programid, DPID = @DPID, CareDomain = @DMID, Classification = @Classification, 
	Category = @category, PublishToApp  = @PublishToApp, Modified = getDate(), AlarmDate = @reminderDate, AlarmText = @ReminderText, Typist = @UserId,
	RTFText=@notes,DocStartDate =@DocStartDate,DocEndDate=@DocEndDate
	WHERE DOC_ID = @DocID;


End
GO

Create or ALTER    procedure [dbo].[spCloneBudget](@DocId int,@DocNo int, @UniqueID varchar(50),@title varchar(100), @fileName varchar(100), @user varchar(50),@program varchar(100))
As
begin

	Declare @newDocNo int;
	Declare @QuoteNo int;
	Declare @OldQuotHeadId int;
	Declare @quotHeadId int;
	Declare @DOC_ID int;
	Declare @AccountNo varchar;
	Declare @today datetime;

	declare @userId int;
	declare @programId int;
	declare @clientId int;
	

	select @userId=Recnum from UserInfo where name=@user
	set @today=convert(varchar,getDate(),111);
	
	select @AccountNo=AccountNo from Recipients where UniqueID=@UniqueID;
	SELECT @newDocNo=ItemID FROM Systable;

	UPDATE systable SET ItemID = ItemID + 1;
	UPDATE systable SET Quote = CASE WHEN Quote IS NULL THEN 1 ELSE Quote + 1 END;

	select @newDocNo=Itemid, @QuoteNo=Quote from SysTable

	INSERT INTO DOCUMENTS ([Status], [Created], [Modified], [FileName], [OriginalLocation], [Author], [PersonID], [DocumentType], [Department], [Classification], [Category], [Typist], [RTFText], [Timespent], [DeletedRecord], [COID], [BRID], [DPID], [FileNoteFlag], [FollowUpDate], [Action], [TimeLogID], [Title], [AlarmDate], [Acknowledged], [DocumentGroup], [Paramaters], [Doc#], [DocStartDate], [DocEndDate], [DocCharges], [AlarmText], [CareDomain], [SubID], [SubCat], [WADSCPlan], [WADSCOptOut], [DSOutletID], [WADSCFundingChanged], [WADSCPlanHeaderOnly], [WADSCSupportingEvidence], [QuoteTemplate], [PublishToApp], [DocusignStatus], [DocusignEnvelopeId], [xEndDate], [PublishToClientPortal]) 
	SELECT [Status], @today, @today, @fileName, [OriginalLocation], @userId, @UniqueID, [DocumentType], 1562, [Classification], [Category], @userId, [RTFText], [Timespent], [DeletedRecord], [COID], [BRID], [DPID], [FileNoteFlag], [FollowUpDate], [Action], [TimeLogID], @title, [AlarmDate], [Acknowledged], 'CP_QUOTE', [Paramaters], @newDocNo, [DocStartDate], [DocEndDate], [DocCharges], [AlarmText], [CareDomain], [SubID], [SubCat], [WADSCPlan], [WADSCOptOut], [DSOutletID], [WADSCFundingChanged], [WADSCPlanHeaderOnly], [WADSCSupportingEvidence], [QuoteTemplate], [PublishToApp], [DocusignStatus], [DocusignEnvelopeId], [xEndDate], [PublishToClientPortal] 
	FROM DOCUMENTS WHERE Doc# = @DocNo

	SELECT @DOC_ID=IDENT_CURRENT('DOCUMENTS');

	SELECT  @newDocNo=[Doc#] FROM Documents WHERE Title =@fileName AND PersonID = @UniqueID;

	--SELECT IsNull(pr.[UserYesNo1], 0) AS CDC FROM Qte_Hdr QH INNER JOIN HumanResourceTypes PR ON PR.RecordNumber = QH.ProgramID  WHERE Doc# = @DocNo;

	--SELECT [Program] FROM RecipientPrograms rp INNER JOIN Recipients re ON re.UniqueID = rp.PersonID INNER JOIN HumanResourceTypes pr ON rp.Program = pr.Name WHERE re.AccountNo = @AccountNo AND ISNULL(PackageType, '') = 'CDC-HCP' AND IsNull(pr.[User2], '') <> 'Contingency';

	--SELECT qh.*, do.* from Documents do LEFT JOIN qte_hdr qh ON CPID = DOC_ID WHERE qh.Doc# = @DocNo

	select @OldQuotHeadId=recordNumber from Qte_Hdr where CPID= @DocId
	
	UPDATE Documents SET QuoteTemplate = 0 WHERE DOC_ID = @Doc_Id;

	SELECT @programId=RecordNumber FROM HumanResourceTypes WHERE [Name] = @program
	SELECT @clientId=SQLID FROM Recipients WHERE UniqueID = @UniqueID

	INSERT INTO Qte_Hdr ([Date], [Doc#], [ClientID], [ProgramID], [Narration], [COID], [BRID], [DPID], [CPID], [Amount], [Contingency], [Basis], [Contribution], [ContingencyBuildAmount], [AdminAmount_Perc], [DaysCalc], [Budget], [QuoteBase], [IncomeTestedFee], [GovtContribution], [PackageSupplements], [DailyCDCRate], [AgreedTopUp], [BalanceAtQuote], [CLAssessedIncomeTestedFee], [FeesAccepted], [BasePension], [DailyBasicCareFee], [DailyIncomeTestedFee], [DailyAgreedTopUp], [HardshipSupplement], [QuoteView], [xDeletedRecord], [EndDate]) 
	SELECT @today, @QuoteNo, @clientId, @programId, [Narration], [COID], [BRID], [DPID], @DOC_ID, [Amount], [Contingency], [Basis], [Contribution], [ContingencyBuildAmount], [AdminAmount_Perc], [DaysCalc], [Budget], [QuoteBase], [IncomeTestedFee], [GovtContribution], [PackageSupplements], [DailyCDCRate], [AgreedTopUp], [BalanceAtQuote], [CLAssessedIncomeTestedFee], [FeesAccepted], [BasePension], [DailyBasicCareFee], [DailyIncomeTestedFee], [DailyAgreedTopUp], [HardshipSupplement], [QuoteView], [xDeletedRecord], [EndDate] 
	FROM Qte_Hdr WHERE RecordNumber = @OldQuotHeadId
	
	SELECT @quotHeadId=IDENT_CURRENT('Qte_Hdr');
	--Update qte_hdr SET Doc# = @QuoteNo WHERE Doc# = 36555;
	

	INSERT INTO Qte_Lne ([Doc_Hdr_ID], [ItemID], [Qty], [Bill Unit], [Unit Bill Rate], [Line Bill Ex Tax], [Tax], [Line Bill Inc Tax], [COID], [BRID], [DPID], [DisplayText], [Notes], [Line#], [Frequency], [QuoteQty], [PriceType], [QuotePerc], [BudgetPerc], [Day], [DTime], [Week], [RCycle], [LengthInWeeks], [BudgetEnforcement], [BudgetGroup], [Roster], [RosterRecurrence], [StrategyID], [SortOrder], [xDeletedRecord], [EndDate]) 
	SELECT @quotHeadId, [ItemID], [Qty], [Bill Unit], [Unit Bill Rate], [Line Bill Ex Tax], [Tax], [Line Bill Inc Tax], [COID], [BRID], [DPID], [DisplayText], [Notes], [Line#], [Frequency], [QuoteQty], [PriceType], [QuotePerc], [BudgetPerc], [Day], [DTime], [Week], [RCycle], [LengthInWeeks], [BudgetEnforcement], [BudgetGroup], [Roster], [RosterRecurrence], [StrategyID], [SortOrder], [xDeletedRecord], [EndDate] 
	FROM Qte_Lne WHERE Doc_Hdr_ID = @OldQuotHeadId
	





END	
GO
create or alter procedure spTransitionProgram (@oldProgram varchar(MAX),@SelectedPrograms varchar(MAX),@Reason varchar(500),@PersonID varchar(50),@DischargeType varchar(50), 
@DischargeDate datetime,@DischargeTime varchar(50),@TimeSpent varchar(50),@NoteType varchar(50),@Category varchar(50),@PublishToApp bit, @DischargeNote varchar(MAX),
@ReminderTo  varchar(50), @ReminderDate datetime, @EmailTo  varchar(50),@MultipleEmails varchar(50), @user varchar(50))
As
begin
Declare @AccountNo varchar(50);
select @AccountNo=AccountNo from Recipients where UniqueID = @PersonID;

UPDATE RecipientPrograms SET ProgramStatus = 'INACTIVE' WHERE PROGRAM = @oldProgram AND PERSONID = @PersonID

UPDATE ServiceOverview SET ServiceStatus = 'INACTIVE' WHERE SERVICEPROGRAM = @oldProgram AND PERSONID = @PersonID;

UPDATE RecipientPrograms SET ProgramStatus = 'INACTIVE' WHERE PROGRAM = @SelectedPrograms AND PERSONID =  @PersonID;

UPDATE ServiceOverview SET ServiceStatus = 'INACTIVE' WHERE SERVICEPROGRAM = @SelectedPrograms AND PERSONID = @PersonID;

SELECT UserYesNo2  FROM HumanResourceTypes WHERE [Name] = @SelectedPrograms AND [GROUP] = 'PROGRAMS';

UPDATE HumanResourceTypes SET EndDate = @DischargeDate WHERE [NAME] = @SelectedPrograms;

Declare @HACCType varchar(500)
declare @amount float;
declare @Price2 float;
declare @Price3 float;
declare @Price4 float;
declare @Price5 float;
declare @Price6 float;
Declare @Unit varchar(10);
Declare @MainGroup varchar(100)
Declare @MinorGroup varchar(100)
Declare @RosterGroup varchar(100)
declare @BillTo  varchar(100)
declare @BillingMethod  varchar(100)
declare @PercentageRate float
declare @AgencyDefinedGroup varchar(100)
declare @RecordNo int;

SELECT @amount=Amount,@Price2=Price2, @Price3=Price3, @Price4=Price4, @Price5=Price5, @Price6=Price6, @Unit=Unit, @HACCType=HACCType, @MainGroup=MainGroup,@MinorGroup= MinorGroup, @RosterGroup=RosterGroup 
FROM ItemTypes WHERE ProcessClassification IN ('OUTPUT', 'EVENT') And Title = @DischargeType;

SELECT ForceSpecialPrice FROM ServiceOverview WHERE PersonID = @PersonID AND  ServiceProgram = @SelectedPrograms AND [Service Type] = @DischargeType;

SELECT BillingMethod FROM Registration;
SELECT @BillTo=BillTo, @BillingMethod=BillingMethod, @PercentageRate=PercentageRate,@AgencyDefinedGroup=AgencyDefinedGroup FROM Recipients WHERE UniqueID=@PersonID;

SELECT GSTRate FROM HumanResourceTypes WHERE [Name] = @SelectedPrograms;

SELECT HACCType FROM ItemTypes WHERE Title = @DischargeType AND ProcessClassification = 'OUTPUT';

INSERT INTO Roster([Client Code], [Carer Code], [Service Type], [Service Description], [Program], [Date], [HACCType], [Start Time], [Duration], [Unit Pay Rate], [Unit Bill Rate], [TaxPercent], [YearNo], [MonthNo], [DayNo], [BlockNo], [Notes], [Type], [Status], [Anal], [Date Entered], [Date Last Mod], [GroupActivity], [BillType], [BillTo], [APInvoiceDate], [APInvoiceNumber], [CostUnit], [CostQty], [BillUnit], [BillQty], [ServiceSetting], [DischargeReasonType], [DatasetClient], [NRCP_REFERRAL_SERVICE], [Creator], [editer], [InUse], [BillDesc], [Transferred] ) 
VALUES (@AccountNo, '!INTERNAL', @DischargeType, 'ADMIN', @SelectedPrograms, @DischargeDate, @HACCType, @DischargeTime, 3, 0, @amount, 0, year(@dischargeDate), month(@dischargeDate),day(@dischargeDate), 162, '', 7, 1, 'GRAFTON', convert(varchar,getDate(),111),convert(varchar,getDate(),111),'0', '', @AccountNo, '', '', @Unit, '0.25', 'HOUR', '0.25', '', '', @AccountNo, 'Select Referrer from List if applicable....', @user, @user, 0, '', 0 )

Select @RecordNo=@@IDENTITY
INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@user, getDate(), 'AUTO CREATE DISCHARGE', 'ROSTER', @RecordNo, @user)

INSERT INTO Roster_Del SELECT ro.* FROM Roster ro LEFT JOIN Recipients re ON ro.[Client Code] = re.AccountNO LEFT JOIN ServiceOverview so ON so.PersonID = re.UniqueID AND serviceprogram = ro.program AND so.[service type] = ro.[service type] 
WHERE [Client Code] = @AccountNo AND Program = @oldProgram AND (Yearno < 2000 OR Date >= @DischargeDate) AND ro.Type <> 7 AND ISNULL(so.[Service Type], '') = ''

DELETE ro FROM Roster ro LEFT JOIN Recipients re ON ro.[Client Code] = re.AccountNO LEFT JOIN ServiceOverview so ON so.PersonID = re.UniqueID AND serviceprogram = ro.program AND so.[service type] = ro.[service type] 
WHERE [Client Code] = @AccountNo AND Program = @oldProgram AND (Yearno < 2000 OR Date >= @DischargeDate) AND ro.Type <> 7 AND ISNULL(so.[Service Type], '') = ''

Insert into History(PersonId,DetailDate,Detail,AlarmDate,ExtraDetail1,ExtraDetail2, WhoCode,Creator, PublishToApp)
values(@PersonID,getDate(),@DischargeNote,@ReminderDate,@NoteType,@NoteType,@ReminderTo,@user, @PublishToApp);

END
GO
-----------------------------------------------

Create Or Alter  procedure [dbo].[spQuoteChargeType](@AccountNo varchar(50), @chargeType int, @Program varchar(500))
As 
Begin

Declare @listCount int
Declare @today varchar(10);

set @today=@today;

if (@chargeType=1) -- Service Type
begin	
		select @listCount = count(distinct [Service Type]) FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @Program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] IN ('CENTREBASED', 'GROUPACTIVITY', 'ONEONONE', 'SLEEPOVER', 'TRANSPORT') AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) 

		if (@listCount>0)	
		  select distinct [Service Type] FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @Program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] IN ('CENTREBASED', 'GROUPACTIVITY', 'ONEONONE', 'SLEEPOVER', 'TRANSPORT') AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) ORDER BY [Service Type]
		Else begin
			select distinct [Title] as [service type] FROM ItemTypes ITM WHERE ProcessClassification = 'OUTPUT' AND ITM.[RosterGroup] IN ('CENTREBASED', 'GROUPACTIVITY', 'ONEONONE', 'SLEEPOVER', 'TRANSPORT') AND (EndDate Is Null OR EndDate >= @today) ORDER BY [Title];
		End
		
End 
Else if (@chargeType=2) -- Item Type
	begin


		select @listCount = count(distinct [Service Type]) FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @Program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ITEM' AND (ITM.EndDate Is Null OR ITM.EndDate >= @today))
		
		if (@listCount>0)	
			select distinct [Service Type] FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @Program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ITEM' AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) ORDER BY [Service Type]
		 
		Else begin
			select distinct [Title] as [service type] FROM ItemTypes ITM WHERE ProcessClassification = 'OUTPUT' AND ITM.[RosterGroup] = 'ITEM' AND (EndDate Is Null OR EndDate >= @today) ORDER BY [Title]
			
		End
			

	End
Else if (@chargeType=3)	 --Admin Charges
	Begin
		select @listCount = count( distinct [Service Type]) FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMISSION' AND ITM.[MainGroup] = 'PACKAGE ADMIN' AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) 
		if (@listCount>0)
			select distinct [Service Type] FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMISSION' AND ITM.[MainGroup] = 'PACKAGE ADMIN' AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) ORDER BY [Service Type];
		else
			select distinct [Title] as [service type] FROM ItemTypes ITM WHERE ProcessClassification = 'OUTPUT' AND ITM.[RosterGroup] = 'ADMISSION' AND ITM.[MainGroup] = 'PACKAGE ADMIN' AND (EndDate Is Null OR EndDate >= @today) ORDER BY [Title]    
		
	End            
Else if (@chargeType=4)	 --CASE MANAGEMENT
	Begin           
		select @listCount = count( distinct [Service Type]) FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMISSION' AND ITM.[MainGroup] = 'CASE MANAGEMENT' AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) 
		if (@listCount>0)
			select distinct [Service Type] FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMISSION' AND ITM.[MainGroup] = 'CASE MANAGEMENT' AND (ITM.EndDate Is Null OR ITM.EndDate >= @today)) ORDER BY [Service Type];
		else
			select distinct [Title] as [service type] FROM ItemTypes ITM WHERE ProcessClassification = 'OUTPUT' AND ITM.[RosterGroup] = 'ADMISSION' AND ITM.[MainGroup] = 'CASE MANAGEMENT' AND (EndDate Is Null OR EndDate >= @today) ORDER BY [Title]    
		
	End

End
GO
--------------------------------------------------

Create or ALTER   procedure [dbo].[spDeleteBudget](@recordNo int)
AS

begin

	declare @docHdrId int;

	select @docHdrId=recordNumber from Qte_Hdr where CPID=@recordNo;

	delete Qte_Lne where Doc_Hdr_ID=@docHdrId;
	delete from Qte_Hdr where CPID=@recordNo;
	update  Documents set DeletedRecord=1 where DOC_ID=@recordNo;
End
GO
Create or ALTER   procedure [dbo].[spServiceDetail](@AccountNo varchar(50), @ServiceType as varchar(500), @Program as varchar(500), @docHdrId int)
AS
begin

declare @UniqueID varchar(50)
declare @GSTRate float;

declare @budgetgroup varchar(500)
declare @BTotal float
declare @BillingMethod varchar(500);
declare @PercentageRate float;
declare @ForceSpecialPrice bit;

SELECT @UniqueID=UniqueID FROM Recipients WHERE AccountNo = @AccountNo;

SELECT @ForceSpecialPrice=ForceSpecialPrice FROM ServiceOverview WHERE PersonID = @UniqueID AND  ServiceProgram = '' AND [Service Type] = @ServiceType;


SELECT @BillingMethod=BillingMethod FROM Registration;

SELECT @BillingMethod=isnull(BillingMethod,@BillingMethod), @PercentageRate=PercentageRate FROM Recipients WHERE AccountNo = @AccountNo;

--SELECT  BudgetGroup, isnull(NoMonday, 0) As NoMonday, isnull(NoTuesday, 0) AS NoTuesday, isnull(NoWednesday, 0) as NoWednesday, isnull(NoThursday, 0) as NoThursday, isnull(NoFriday, 0) as NoFriday, isnull(NoSaturday, 0) as NoSaturday, isnull(NoSunday, 0) as NoSunday, isnull(NoPubHol, 0) as NoPubHol FROM ITEMTYPES
--WHERE Title = @ServiceType;

SELECT @budgetgroup=budgetgroup, @BTotal =CONVERT(DECIMAL (10,2), Sum(Total)) 
FROM  (  SELECT ql.doc_hdr_id, it.budgetgroup,  ql.QuoteQty * [unit bill rate] + (isnull(tax, 0)/100 * (ql.QuoteQty * [unit bill rate]))as Total  
		FROM qte_lne ql INNER JOIN itemtypes it ON itemid = it.Recnum  WHERE  doc_hdr_id = @docHdrId AND it.BudgetGroup = ''  ) t  
		GROUP BY doc_hdr_id, BudgetGroup  
ORDER BY doc_hdr_id desc;

SELECT @GSTRate=isnull(GSTRate,0) FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND [Name] = @Program AND GST = 1;



SELECT recnum, Title,Amount, Price2, Price3, Price4, Price5, Price6, Unit, HACCType, MainGroup, MinorGroup, RosterGroup,"BillText",
 BudgetGroup, isnull(NoMonday, 0) As NoMonday, isnull(NoTuesday, 0) AS NoTuesday, isnull(NoWednesday, 0) as NoWednesday, isnull(NoThursday, 0) as NoThursday, isnull(NoFriday, 0) as NoFriday, isnull(NoSaturday, 0) as NoSaturday, isnull(NoSunday, 0) as NoSunday, isnull(NoPubHol, 0) as NoPubHol
,isnull(@GSTRate,0) as GstRate , @budgetgroup as Budgetgroup, @BTotal as BTotal, @BillingMethod as BillingMethod, @PercentageRate as PercentageRate
FROM ItemTypes it
WHERE ProcessClassification IN ('OUTPUT', 'EVENT') And Title = @ServiceType

End


--------------------------------------------

Go
Create or ALTER     procedure [dbo].[getContacts](@personId varchar(50))
as
begin
SELECT HR.recordnumber,
       'LOCAL'                           AS [Source],
       Isnull(HR.state, '99')            AS [State],
       HR.personid,
       isnull(HR.[group],'CONTACT')                       AS [Category],
       Cast(0 AS BIT)                    AS APrimary,
       Upper(Replace(Replace(Replace(Replace(Replace(Replace(Replace(
                                                     Replace(HR.[type],
                                                     '1-',
                                                           ''),
                                                           '2-', ''),
                                                           '3-',
                                                                   ''),
                                             '4-'
                                             , ''), '5-', ''), '6-', ''), '7-',
                     ''),
             '8-', ''
             ))                          AS [Type],
       Upper(HR.[subtype])               AS [Contact Type],
       HR.[name]                         AS [Contact Name],
       'No-one'                          AS [Connected To],
       CASE WHEN Isnull(HR.phone1, '') <> '' THEN '(p)' + HR.phone1 ELSE '' END
       + CASE
       WHEN Isnull(HR.phone1, '') <> '' AND Isnull(HR.phone2, '') <> '' THEN
       ', (p)' +
       HR.phone2 ELSE CASE WHEN Isnull(HR.phone2, '') <> '' THEN '(p)' +
       HR.phone2 ELSE
       '' END END + CASE WHEN Isnull(HR.phone1, '') <> '' AND Isnull(HR.phone2,
       '') <>
       '' AND Isnull(HR.mobile, '') <> '' THEN ', (m)' + HR.mobile ELSE CASE
       WHEN
       Isnull(HR.mobile, '') <> '' THEN '(m)' + HR.mobile ELSE '' END END + CASE
       WHEN
       Isnull(HR.phone1, '') <> '' AND Isnull(HR.phone2, '') <> '' AND
       Isnull(HR.mobile, '') <> '' AND Isnull(HR.email, '') <> '' THEN ', (e)' +
       HR.email ELSE
       '(e)' + Isnull(HR.email, '') END  AS Phones,
       CASE WHEN HR.address1 <> '' AND HR.address1 IS NOT NULL THEN HR.address1
       ELSE ''
       END + ' ' + CASE WHEN HR.address2 <> '' AND HR.address2 IS NOT NULL THEN
       HR.address2 ELSE '' END + ' ' + CASE WHEN HR.suburb <> '' AND HR.suburb
       IS NOT
       NULL THEN
       HR.suburb ELSE '' END + ' ' + CASE WHEN HR.postcode <> '' AND HR.suburb
       IS NOT
       NULL THEN HR.postcode ELSE '' END AS AddressDetails,
       HR.notes
FROM   humanresources HR
WHERE  personid = @personId
       AND ( user1 IS NULL
              OR user1 <> 'IMPORT' )
       AND [group] IN ( 'CONTACT', 'OTHER CONTACT' )
UNION
SELECT HR.recordnumber,
       'IMPORT'                                             AS [Source],
       Isnull(HR.state, '99')                               AS [State],
       HR.personid,
       isnull(HR.[group],'CONTACT')                         AS [Category],
       Cast(0 AS BIT)                                       AS APrimary,
       Upper(HR.[type]),
       Upper(HR.[subtype])                                  AS [Contact Type],
       HR2.[name]                                           AS [Contact Name],
       (SELECT accountno
        FROM   recipients
        WHERE  uniqueid = reminderscope)                    AS [Connected To],
       CASE WHEN HR2.phone1 <> '' AND HR2.phone1 IS NOT NULL THEN 'p' +
       HR2.phone1 ELSE
       '' END + CASE WHEN HR2.phone2 <> '' AND HR2.phone2 IS NOT NULL THEN ', p'
       +
       HR2.phone2 ELSE '' END + CASE WHEN HR2.mobile <> '' AND HR2.mobile IS NOT
       NULL
       THEN
       ', m' + HR2.mobile ELSE '' END + CASE WHEN HR2.email <> '' AND HR2.email
       IS NOT
       NULL THEN ', e' + HR2.email ELSE '' END              AS Phones,
       CASE WHEN HR2.address1 <> '' AND HR2.address1 IS NOT NULL THEN
       HR2.address1 ELSE
       '' END + ' ' + CASE WHEN HR2.address2 <> '' AND HR2.address2 IS NOT NULL
       THEN
       HR2.address2 ELSE '' END + ' ' + CASE WHEN HR2.suburb <> '' AND
       HR2.suburb IS
       NOT NULL THEN HR2.suburb ELSE '' END + ' ' + CASE WHEN HR2.postcode <> ''
       AND
       HR2.suburb IS NOT NULL THEN HR2.postcode ELSE '' END AS AddressDetails,
       HR.notes
FROM   humanresources HR
       INNER JOIN (SELECT recordnumber,
                          [name],
                          phone1,
                          phone2,
                          mobile,
                          email,
                          address1,
                          address2,
                          suburb,
                          postcode
                   FROM   humanresources) AS HR2
               ON Cast(HR2.recordnumber AS VARCHAR(20)) = HR.[name]
WHERE  HR.personid = @personId
       AND HR.user1 = 'IMPORT'
       AND HR.importtype = 'HR'
       AND [group] = 'CONTACT'
UNION
SELECT HR.recordnumber,
       'IMPORT'                                      AS [Source],
       Isnull(HR.[state], '99')                      AS [State],
       HR.personid,
       isnull(HR.[group],'CONTACT')                    AS Category,
       Cast(0 AS BIT)                                AS APrimary,
       Upper(HR.[type]),
       Upper(HR.[subtype])                           AS [Contact Type],
       CASE WHEN recipients.[firstname] <> '' THEN recipients.[firstname] + ' '
       ELSE ''
       END + CASE WHEN recipients.[surname/organisation] <> '' THEN
       recipients.[surname/organisation] ELSE '' END AS [Name],
       CASE WHEN recipients.[firstname] <> '' THEN recipients.[firstname] + ' '
       ELSE ''
       END + CASE WHEN recipients.[surname/organisation] <> '' THEN
       recipients.[surname/organisation] ELSE '' END AS [Connected To],
       CASE WHEN PP.detail <> '' AND PP.detail IS NOT NULL THEN '*p' + PP.detail
       ELSE
       '' END + CASE WHEN HP.detail <> '' AND HP.detail IS NOT NULL AND
       HP.detail <>
       PP.detail THEN ', h' + HP.detail ELSE '' END + CASE WHEN WP.detail <> ''
       AND
       WP.detail
       IS NOT NULL AND WP.detail <> PP.detail THEN ', w' + HP.detail ELSE '' END
       + CASE
       WHEN MP.detail <> '' AND MP.detail IS NOT NULL AND MP.detail <> PP.detail
       THEN
       ', m' + MP.detail ELSE '' END + CASE WHEN EM.detail <> '' AND EM.detail
       IS NOT
       NULL THEN ', e' + HP.detail ELSE '' END       AS Phones,
       CASE
         WHEN N1.address <> '' THEN N1.address
         ELSE N2.address
       END                                           AS FullAddress,
       HR.notes
FROM   humanresources HR
       INNER JOIN recipients
               ON HR.[name] = recipients.uniqueid
       LEFT JOIN (SELECT
                        personid,
                         suburb,
                         CASE WHEN address1 <> '' THEN address1 + ' ' ELSE ' '
                        END +
                        CASE WHEN
                        address2
                         <> '' THEN address2 + ' ' ELSE ' ' END + CASE WHEN
                        suburb <>
                        '' THEN
                        suburb
                        +
                         ' ' ELSE '  ' END + CASE WHEN postcode <> '' THEN postcode
                        ELSE
                        ' ' END AS
                        Address
                  FROM   namesandaddresses
                  WHERE  primaryaddress = 1) AS N1
              ON N1.personid = HR.[name]
       LEFT JOIN (SELECT personid,
                         CASE WHEN address1 <> '' THEN address1 + '  ' ELSE ' ' END
+ CASE
WHEN
         address2 <>
          '' THEN address2 + ' ' ELSE ' ' END + CASE WHEN suburb <> ''
                                            THEN suburb
                                                      + ' '
          ELSE ' ' END + CASE WHEN postcode <> '' THEN postcode ELSE ' '
                                            END AS
                                                      Address
   FROM   namesandaddresses
   WHERE  primaryaddress <> 1) AS N2
ON N2.personid = HR.[name]
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [primaryphone] = 1) AS PP
              ON PP.personid = HR.[name]
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<HOME>') AS HP
              ON HP.personid = HR.[name]
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<WORK>') AS WP
              ON WP.personid = HR.[name]
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<MOBILE>') AS MP
              ON MP.personid = HR.[name]
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<EMAIL>') AS EM
              ON EM.personid = HR.[name]
WHERE  HR.personid = @personId
       AND [group] = 'CONTACT'
       AND importtype = 'RE'
UNION
SELECT 0                                             AS RecordNumber,
       'IMPORT'                                      AS [Source],
       '0'                                           AS [State],
       @personId                                 AS PersonID,
       NULL                                          AS Category,
       Cast(1 AS BIT)                                AS APrimary,
       'CARER'                                       AS Type,
       '**PRIMARY CARER'                             AS [Contact Type],
       CASE WHEN recipients.[firstname] <> '' THEN recipients.[firstname] + ' '
       ELSE ''
       END + CASE WHEN recipients.[surname/organisation] <> '' THEN
       recipients.[surname/organisation] ELSE '' END AS [Name],
       CASE WHEN recipients.[firstname] <> '' THEN recipients.[firstname] + ' '
       ELSE ''
       END + CASE WHEN recipients.[surname/organisation] <> '' THEN
       recipients.[surname/organisation] ELSE '' END AS [Connected To],
       CASE WHEN PP.detail <> '' AND PP.detail IS NOT NULL THEN '*p' + PP.detail
       ELSE
       '' END + CASE WHEN HP.detail <> '' AND HP.detail IS NOT NULL AND
       HP.detail <>
       PP.detail THEN ', h' + HP.detail ELSE '' END + CASE WHEN WP.detail <> ''
       AND
       WP.detail
       IS NOT NULL AND WP.detail <> PP.detail THEN ', w' + HP.detail ELSE '' END
       + CASE
       WHEN MP.detail <> '' AND MP.detail IS NOT NULL AND MP.detail <> PP.detail
       THEN
       ', m' + MP.detail ELSE '' END + CASE WHEN EM.detail <> '' AND EM.detail
       IS NOT
       NULL THEN ', e' + HP.detail ELSE '' END       AS Phones,
       CASE
         WHEN N1.address <> '' THEN N1.address
         ELSE N2.address
       END                                           AS FullAddress,
       '' as Notes
FROM   recipients
       LEFT JOIN (SELECT personid,
                         suburb,
                         CASE WHEN address1 <> '' THEN address1 + ' ' ELSE ' '
                         END +
                                           CASE WHEN
                                                     address2
                         <> '' THEN address2 + ' ' ELSE ' ' END + CASE WHEN
                         suburb <>
                                           '' THEN
                                                     suburb +
                         ' ' ELSE ' ' END + CASE WHEN postcode <> '' THEN
                         postcode ELSE
                                           ' ' END
                                                     AS
                         Address
                  FROM   namesandaddresses
                  WHERE  primaryaddress = 1) AS N1
              ON N1.personid = uniqueid
       LEFT JOIN (SELECT personid,
                         CASE WHEN address1 <> '' THEN address1 + ' ' ELSE ' '
                         END +
                                                          CASE WHEN
                                                                    address2
                         <> '' THEN address2 + ' ' ELSE ' ' END + CASE WHEN
                         suburb <>
                                                          '' THEN
                                                                    suburb +
                         ' ' ELSE ' ' END + CASE WHEN postcode <> '' THEN
                         postcode ELSE
                                                          ' ' END
                                                                    AS
                         Address
                  FROM   namesandaddresses
                  WHERE  primaryaddress <> 1) AS N2
              ON N2.personid = uniqueid
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [primaryphone] = 1) AS PP
              ON PP.personid = uniqueid
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<HOME>') AS HP
              ON HP.personid = uniqueid
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<WORK>') AS WP
              ON WP.personid = uniqueid
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<MOBILE>') AS MP
              ON MP.personid = uniqueid
       LEFT JOIN (SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [type] = '<EMAIL>') AS EM
              ON EM.personid = uniqueid
WHERE  recipients.uniqueid = @personId
Union
SELECT hr.recordnumber,
       'LOCAL' AS [Source],
       hr.state,
       hr.personid,
       hr.[Group]       AS [Category],
       cast(0 AS bit)   AS aprimary,
       upper(hr.[Type]) AS [Type] ,
       CASE
              WHEN hr.[SubType] IS NOT NULL
              AND    hr.[SubType] <> '' THEN upper(hr.[SubType])
              ELSE 'UNSPECIFIED'
       END       AS [Contact Type],
       hr.[Name] AS [Contact Name],
       'No-one'  AS [Connected To],
       CASE
              WHEN hr.phone1 <> ''
              AND    hr.phone1 IS NOT NULL THEN '(p)' + hr.phone1
              ELSE ''
       END +
       CASE
              WHEN hr.phone2 <> ''
              AND    hr.phone2 IS NOT NULL THEN ', (p)' + hr.phone2
              ELSE ''
       END +
       CASE
              WHEN hr.mobile <> ''
              AND    hr.mobile IS NOT NULL THEN ', (m)' + hr.mobile
              ELSE ''
       END +
       CASE
              WHEN hr.email <> ''
              AND    hr.email IS NOT NULL THEN ', (e)' + hr.email
              ELSE ''
       END AS phones,
       CASE
              WHEN hr.address1 <> ''
              AND    hr.address1 IS NOT NULL THEN hr.address1
              ELSE ''
       END + ' ' +
       CASE
              WHEN hr.address2 <> ''
              AND    hr.address2 IS NOT NULL THEN hr.address2
              ELSE ''
       END + ' ' +
       CASE
              WHEN hr.suburb <> ''
              AND    hr.suburb IS NOT NULL THEN hr.suburb
              ELSE ''
       END + ' ' +
       CASE
              WHEN hr.postcode <> ''
              AND    hr.suburb IS NOT NULL THEN hr.postcode
              ELSE ''
       END AS addressdetails, hr.Notes
FROM   humanresources hr
WHERE  personid = @PersonID
AND    isnull(user1, '') <> 'IMPORT'
AND    [GROUP] = 'ASSOCIATEDPARTY'
UNION
SELECT     hr.recordnumber,
           'IMPORT' AS [Source],
           hr.state,
           hr.personid,
           hr.[Group]     AS [Category],
           cast(0 AS bit) AS aprimary,
           upper(hr.[Type]) ,
           CASE
                      WHEN hr.[SubType] IS NOT NULL
                      AND        hr.[SubType] <> '' THEN upper(hr.[SubType])
                      ELSE 'UNSPECIFIED'
           END        AS [Contact Type],
           hr2.[Name] AS [Contact Name],
           (
                  SELECT accountno
                  FROM   recipients
                  WHERE  uniqueid = reminderscope) AS [Connected To],
           CASE
                      WHEN hr2.phone1 <> ''
                      AND        hr2.phone1 IS NOT NULL THEN 'p' + hr2.phone1
                      ELSE ''
           END +
           CASE
                      WHEN hr2.phone2 <> ''
                      AND        hr2.phone2 IS NOT NULL THEN ', p' + hr2.phone2
                      ELSE ''
           END +
           CASE
                      WHEN hr2.mobile <> ''
                      AND        hr2.mobile IS NOT NULL THEN ', m' + hr2.mobile
                      ELSE ''
           END +
           CASE
                      WHEN hr2.email <> ''
                      AND        hr2.email IS NOT NULL THEN ', e' + hr2.email
                      ELSE ''
           END AS phones,
           CASE
                      WHEN hr2.address1 <> ''
                      AND        hr2.address1 IS NOT NULL THEN hr2.address1
                      ELSE ''
           END + ' ' +
           CASE
                      WHEN hr2.address2 <> ''
                      AND        hr2.address2 IS NOT NULL THEN hr2.address2
                      ELSE ''
           END + ' ' +
           CASE
                      WHEN hr2.suburb <> ''
                      AND        hr2.suburb IS NOT NULL THEN hr2.suburb
                      ELSE ''
           END + ' ' +
           CASE
                      WHEN hr2.postcode <> ''
                      AND        hr2.suburb IS NOT NULL THEN hr2.postcode
                      ELSE ''
           END AS addressdetails ,
           hr.notes
FROM       humanresources hr
INNER JOIN
           (
                  SELECT recordnumber,
                         [Name],
                         phone1,
                         phone2,
                         mobile,
                         email,
                         address1 ,
                         address2,
                         suburb,
                         postcode
                  FROM   humanresources) AS hr2
ON         cast(hr2.recordnumber AS varchar) = hr.[Name]
WHERE      hr.personid = @personId
AND        (
                      hr.completed = 0
           OR         hr.completed IS NULL)
AND        [GROUP] = 'ASSOCIATEDPARTY'
AND        isnull(hr.user1, '') = 'IMPORT'
AND        hr.importtype = 'HR'
UNION
SELECT     hr.recordnumber,
           'IMPORT' AS [Source],
           hr.[State],
           hr.personid,
           hr.[Group]     AS category,
           cast(0 AS bit) AS aprimary,
           upper(hr.[Type]) ,
           CASE
                      WHEN hr.[SubType] IS NOT NULL
                      AND        hr.[SubType] <> '' THEN upper(hr.[SubType])
                      ELSE 'UNSPECIFIED'
           END AS [Contact Type],
           CASE
                      WHEN recipients.[FirstName] <> '' THEN recipients.[FirstName] + ' '
                      ELSE ''
           END +
           CASE
                      WHEN recipients.[Surname/Organisation] <> '' THEN recipients.[Surname/Organisation]
                      ELSE ''
           END AS [Name],
           CASE
                      WHEN recipients.[FirstName] <> '' THEN recipients.[FirstName] + ' '
                      ELSE ''
           END +
           CASE
                      WHEN recipients.[Surname/Organisation] <> '' THEN recipients.[Surname/Organisation]
                      ELSE ''
           END AS [Connected To],
           CASE
                      WHEN pp.detail <> ''
                      AND        pp.detail IS NOT NULL THEN '*p' + pp.detail
                      ELSE ''
           END +
           CASE
                      WHEN hp.detail <> ''
                      AND        hp.detail IS NOT NULL
                      AND        hp.detail <> pp.detail THEN ', h' + hp.detail
                      ELSE ''
           END +
           CASE
                      WHEN wp.detail <> ''
                      AND        wp.detail IS NOT NULL
                      AND        wp.detail <> pp.detail THEN ', w' + hp.detail
                      ELSE ''
           END +
           CASE
                      WHEN mp.detail <> ''
                      AND        mp.detail IS NOT NULL
                      AND        mp.detail <> pp.detail THEN ', m' + mp.detail
                      ELSE ''
           END +
           CASE
                      WHEN em.detail <> ''
                      AND        em.detail IS NOT NULL THEN ', e' + hp.detail
                      ELSE ''
           END AS phones,
           CASE
                      WHEN n1.address <> '' THEN n1.address
                      ELSE n2.address
           END AS fulladdress ,
           hr.notes
FROM       humanresources hr
INNER JOIN recipients
ON         hr.[Name] = recipients.uniqueid
LEFT JOIN
           (
                  SELECT personid,
                         suburb,
                         CASE
                                WHEN address1 <> '' THEN address1 + ' '
                                ELSE ' '
                         END +
                         CASE
                                WHEN address2 <> '' THEN address2 + ' '
                                ELSE ' '
                         END +
                         CASE
                                WHEN suburb <> '' THEN suburb + ' '
                                ELSE ' '
                         END +
                         CASE
                                WHEN postcode <> '' THEN postcode
                                ELSE ' '
                         END AS address
                  FROM   namesandaddresses
                  WHERE  primaryaddress = 1) AS n1
ON         n1.personid = hr.[Name]
LEFT JOIN
           (
                  SELECT personid,
                         CASE
                                WHEN address1 <> '' THEN address1 + ' '
                                ELSE ' '
                         END +
                         CASE
                                WHEN address2 <> '' THEN address2 + ' '
                                ELSE ' '
                         END +
                         CASE
                                WHEN suburb <> '' THEN suburb + ' '
                                ELSE ' '
                         END +
                         CASE
                                WHEN postcode <> '' THEN postcode
                                ELSE ' '
                         END AS address
                  FROM   namesandaddresses
                  WHERE  primaryaddress <> 1) AS n2
ON         n2.personid = hr.[Name]
LEFT JOIN
           (
                  SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [PrimaryPhone] = 1) AS pp
ON         pp.personid = hr.[Name]
LEFT JOIN
           (
                  SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [Type] = '<HOME>') AS hp
ON         hp.personid = hr.[Name]
LEFT JOIN
           (
                  SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [Type] = '<WORK>') AS wp
ON         wp.personid = hr.[Name]
LEFT JOIN
           (
                  SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [Type] = '<MOBILE>') AS mp
ON         mp.personid = hr.[Name]
LEFT JOIN
           (
                  SELECT personid,
                         detail
                  FROM   phonefaxother
                  WHERE  [Type] = '<EMAIL>') AS em
ON         em.personid = hr.[Name]
WHERE      hr.personid = @personId
AND        [GROUP] = 'ASSOCIATEDPARTY'
AND        importtype = 'RE'
ORDER  BY [state],
          category,
          [contact type] 

End
-------------------------------------------------------

GO


ALTER          PROCEDURE [dbo].[spRosterOperations] (@OpsType varchar(50),@User varchar(50), @RecordNo int, @IsMaster bit,@Roster_Date varchar(50), @Start_Time varchar(5),@carer_code varchar(100), @notes varchar(MAX), @ClientCodes varchar(MAX)=null)
As
begin
declare @userId int;
declare @BlockNo float;


select @userId=Recnum from UserInfo where name=@user;

if (@Start_Time<>'')
	select  @BlockNo=dbo.[GetBlockNo](@Start_Time);
else
	set @BlockNo=0;

	if (@OpsType='Delete')
		begin
		insert into Roster_Del(
		    [RecordNo]
		   ,[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate])

			select 
			RecordNo
		   ,[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate] from roster where Status<>2 and RecordNo=@RecordNo;
			
			delete from Roster where Status<>2 and RecordNo=@RecordNo;
		
		end
	else if (@OpsType='Re-Allocate')
		begin
			
		Declare @sStaffCode varchar(500);
		Declare @sClientCode varchar(500);
		Declare @sProgram varchar(500);
		Declare @sDate varchar(20);
		Declare @sStartTime varchar(5);
		Declare @sDuration varchar(5);
		Declare @sActivity varchar(500);
		
		select @sStaffCode=[Carer Code],@sClientCode=[Client Code],@sProgram=[program],@sDate=[date],@sStartTime=[Start Time],@sDuration=convert(varchar,duration),@sActivity=[Service Type]
		 from Roster where RecordNo=@RecordNo;

		if @sStaffCode in ('BOOKED','!MULTIPLE','!INTERNAL')
		BEGIN
			update Roster set [Carer Code]=@carer_code,type=isnull(dbo.getRosterType([Service Type]),0) where RecordNo=@RecordNo;
			RETURN;
		END
		

		 DECLARE @temp  TABLE 

			(
			ErrorValue int,
			msg varchar(500),
			continueError bit
          
			)
		insert into @temp
		execute [Check_BreachedRosterRules] 'Add',@carer_code,@sClientCode,@sProgram,@sDate,@sStartTime,@sDuration,@sActivity,@RecordNo,'',0,0,0,0,0,0,'','',0,''
	
		if((select ErrorValue from @temp)>0)
		begin
			select * from @temp;
			return;
		End
		else begin
			update Roster set [Carer Code]=@carer_code,type=isnull(dbo.getRosterType([Service Type]),0) where RecordNo=@RecordNo;
		
		end
	End
	else if (@OpsType='Un-Allocate')
		begin
			declare @branch varchar(100);
			select @branch=stf_department from staff where accountNo=@carer_code;
			UPDATE Roster SET [ShiftName] = [Carer Code], [Carer Code] = 'BOOKED', [Branch] = @branch, [TYPE] = 1, [SERVICE DESCRIPTION] = 'NOPAY', [Unit Pay Rate] = 0  WHERE RecordNO IN (375672)
	
		
		end
	else if (@OpsType='SetMultishift')
		begin
			update Roster set TA_Multishift=1 where RecordNo=@RecordNo;
		
		end
   else if (@OpsType='ClearMultishift')
		begin
			update Roster set TA_Multishift=0 where RecordNo=@RecordNo;
		
		end
	else if (@OpsType='Additional')
		begin
			update Roster set Notes=@notes where RecordNo=@RecordNo;
		
		end
	else if (@OpsType='Alert')
		begin

		INSERT INTO [dbo].[DeviceReminders] ([UserID] ,[ActivationDateTime] ,[StaffCode] ,[DestinationStaff] ,[Detail] ,[Status]) 
		values(@userId,@Roster_Date,@carer_code,@carer_code, @notes ,1)
		
	
	End
   
   else if (@OpsType='Copy' or @OpsType='Cut')
	begin
		
	

	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate]
		   )

		   
select	 	isnull(@ClientCodes, [Client Code])
           ,isnull(@carer_code, [Carer Code])
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,convert(varchar(50),convert(datetime,@Roster_Date,111),111)
           ,@Start_Time
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,day(@Roster_date) as [Dayno]
           ,@BlockNo as [BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,1 as [Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate] 
		   
		   from Roster where RecordNo=@RecordNo;


		    if (@OpsType='Cut')
			begin
				delete from Roster where RecordNo=@RecordNo;		
			end
	end
else if (@OpsType='Staff Master' )
	begin
	declare @carerCode varchar(100);
	select @carerCode=[Carer Code] from Roster where RecordNo=@RecordNo;
 		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate])

select		[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,FORMAT(year(getdate()),'0000')+'/'+ FORMAT(month(getdate()),'00') +'/'+format(Dayno,'00')
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,year(getdate()) as [YearNo]
           ,FORMAT(month(getdate()),'00') as [MonthNo]
           , [Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate] from Roster 
		   where [Carer Code]=@carerCode and YearNo=1900 and Dayno<=DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,getDate()),0)))
		   
	End

else if (@OpsType='Client Master' )
	begin
	declare @ClientCode varchar(100);
	select @ClientCode=[Client Code] from Roster where RecordNo=@RecordNo;
 		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate])

select		[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,FORMAT(year(getdate()),'0000')+'/'+ FORMAT(month(getdate()),'00') +'/'+format(Dayno,'00')
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,year(getdate()) as [YearNo]
           ,FORMAT(month(getdate()),'00') as [MonthNo]
           , [Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate] from Roster 
		   where [Client Code]=@ClientCode and YearNo=1900 and Dayno<=DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,getDate()),0)))
		   
	End	
Else if (@OpsType='GroupShift')
begin
	declare @newProgram varchar(500);
	select @newProgram= isnull((SELECT top 1 ServiceProgram FROM ServiceOverview WHERE [Service Type] = r.[Service Type]  AND [ServiceStatus] = 'ACTIVE' AND PersonID = p.UniqueID),[Program]) 
	from roster r left join Recipients p on p.AccountNo in (select * from Split(@ClientCodes,',')) where  r.recordNo=@RecordNo
		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[EndDate])

		   
select		p.AccountNo as [Client Code]
           ,'!MULTIPLE' as [Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,isnull((SELECT top 1 ServiceProgram FROM ServiceOverview WHERE [Service Type] = r.[Service Type]  AND [ServiceStatus] = 'ACTIVE' AND PersonID = p.UniqueID),[Program]) 
           ,r.date
           ,r.[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           , [dbo].[Getbillingrate] (p.AccountNo,r.[service type],@newProgram) as [Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,r.[Dayno]
           ,[BlockNo]
           ,p.SpecialConsiderations as notes
           ,[CarerPhone]
           ,r.[UBDRef]
           ,r.[Type]
           ,r.[Status]
           , p.AgencyDefinedGroup as [Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[dbo].[getBillTo](p.AccountNo, r.[service type])
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,r.[Tagged]
           ,r.[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,r.[InUse]
           ,[DischargeReasonType]
           ,case  when (select top 1 DatasetGroup from ItemTypes where title=r.[Service Type]) in ('RESPITE CARE','CARER TRANSPORT','CARER COUNSELLING/SUPPORT INFO & ADVOCACY') then p.DatasetCarer  else p.AccountNo end  as [DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,r.[DeletedRecord]
           ,r.[COID]
           ,r.[BRID]
           ,r.[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsDocumentationOnly]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,r.[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,r.[EndDate] 
		   from Roster r 
		   left join Recipients p on p.AccountNo in (select * from Split(@ClientCodes,','))
		   where RecordNo=@RecordNo;


	END

	
	select @Recordno=@@identity;
	
	Insert into [Audit] (Operator,ActionDate,ActionOn,WhoWhatCode,TraccsUser,AuditDescription)
					 values ( @User,getDate(),'WEB-ROSTER',@Recordno,@User,@OpsType+' Roster from Web Portal') ;

    select @Recordno as Recordno ;
End
GO
------------------------------------------------------------------------------------------------------------------------
CREATE OR ALTER   Procedure [dbo].[spUnallocateStaff](@RecordNo varchar(MAX), @carerCode varchar(100), @ServiceType varchar(500), @ServiceDescription varchar(500),@program varchar(500), @Branch varchar(100), @user varchar(50), @reallocate bit=0,  @addLeave bit, @UnavailableForBalanceDays bit, @UnallocateMaster bit, @UnallocateAny bit, @serviceDuration int,@AcceptProgram bit, @UnallocateAdminActivities bit)
                        
As
begin

declare @DMTransID int;


update SysTable set DMTransID=isnull(DMTransID,0)+1
select @DMTransID=DMTransID from SysTable;
if (@program='' ) set @program=null;


 INSERT INTO [dbo].[Roster]
(
    [Client Code], [Carer Code], [Service Type], [Service Description], [Program], 
    [Date], [Start Time], [Duration], [Unit Pay Rate], [Unit Bill Rate], 
    [YearNo], [MonthNo], [Dayno], [BlockNo], [Notes], 
    [CarerPhone], [UBDRef], [Type], [Status], [Anal], 
    [Date Entered], [Date Last Mod], [Date Timesheet], [Date Invoice], [Date Payroll], 
    [InvoiceNumber], [TimesheetNumber], [Batch#], [Transferred], [GroupActivity], 
    [BillType], [BillTo], [CostUnit], [CostQty], [HACCType], 
    [HACCID], [BillUnit], [BillQty], [TaxPercent], [TaxAmount], 
    [Link], [Tagged], [UniqueID], [ROS_PANZTEL_UPDATED], [ROS_COPY_BATCH#], 
    [ROS_PAY_BATCH], [NRCP_CARER_CODE], [NRCP_CONTACT_URGENCY], [NRCP_CONTACT_TIME], [NRCP_REFERRAL_SERVICE], 
    [ServiceSetting], [ROS_DAELIBS_UPDATED], [InUse], [DischargeReasonType], [DATASETClient], 
    [NRCP_TypeOfAssitance], [NRCP_ReferralToServices], [NRCP_VolunteerServices], [NRCP_ReasonNoAssistance], [DMStat], 
    [Creator], [Editer], [DateDeleted], [DeletedRecord], [COID], 
    [BRID], [DPID], [NRCP_Episode], [Attendees], [OldStatus], 
    [OldBill], [OldQty], [TimeLogID], [RecurrenceState], [BusyStatus], 
    [ImportanceLevel], [LabelID], [ScheduleID], [RecurrencePatternID], [IsRecurrenceExceptionDeleted], 
    [RExceptionStartTimeOrig], [RExceptionEndTimeOrig], [IsDocumentationOnly], [IsMeeting], [IsPrivate], 
    [IsReminder], [ReminderMinutesBeforeStart], [Branch], [ShiftName], [HasServiceNotes], 
    [Time2], [TA_EarlyStart], [TA_LateStart], [TA_EarlyGo], [TA_LateGo], 
    [TA_TooShort], [TA_TooLong], [BillDesc], [VariationReason], [ClaimedStart], 
    [ClaimedEnd], [ClaimedDate], [ClaimedBy], [KM], [StaffPosition], 
    [StartKM], [EndKM], [APInvoiceDate], [APInvoiceNumber], [TA_EXCLUDEGEOLOCATION], 
    [TA_EXCLUDEFROMAPPALERTS], [InterpreterPresent], [ExtraItems], [CloseDate], [charge], 
    [TAMode], [TA_Multishift], [TravelBatch], [NDIABatch], [OwnVehicle], 
    [ClientApproved], [disable_shift_start_alarm], [disable_shift_end_alarm], [DMTransID], [DatasetQty], 
    [Recipient_Signature], [ShiftLocked], [ServiceTypePortal], [ReminderSent], [EndDate]
)
SELECT 
    '!INTERNAL' AS [Client Code], [carer code], @ServiceType AS [Service Type],  @ServiceDescription AS [Service Description], isnull(@program,[program]) AS [Program], 
    [ro].[date], [ro].[start time], [ro].[duration], '0' AS [Unit Pay Rate],   0 AS [Unit Bill Rate],   [ro].[yearno], [ro].[monthno], [ro].[dayno], [ro].[blockno], 
    [ro].[notes], [ro].[carerphone], [ro].[ubdref], 6 AS [Type],  '1' AS [Status], [ro].[anal], Getdate() AS [Date Entered],  Getdate() AS [Date Last Mod], NULL AS [Date Timesheet], NULL AS [Date Invoice], 
    NULL AS [Date Payroll], NULL AS [InvoiceNumber], NULL AS [TimesheetNumber],  NULL AS [Batch#], 0 AS [Transferred], 0 AS [GroupActivity], [ro].[billtype], 
    [ro].[billto], 'HOUR' AS [Unit Pay Rate], [ro].[costqty], [ro].[hacctype],  [ro].[haccid], [ro].[billunit], [ro].[billqty], [ro].[taxpercent], 
    [ro].[taxamount], NULL AS [Link], [ro].[tagged], [ro].[uniqueid],  0 AS [ROS_PANZTEL_UPDATED], NULL AS [ROS_COPY_BATCH#], NULL AS [ROS_PAY_BATCH], 
    [ro].[nrcp_carer_code], [ro].[nrcp_contact_urgency], [ro].[nrcp_contact_time],  [ro].[nrcp_referral_service], [ro].[servicesetting], [ro].[ros_daelibs_updated], 
    0 AS [InUse], [ro].[dischargereasontype], [ro].[datasetclient],  [ro].[nrcp_typeofassitance], [ro].[nrcp_referraltoservices], 
    [ro].[nrcp_volunteerservices], [ro].[nrcp_reasonnoassistance], [ro].[dmstat],  @user AS [Creator], @user AS [Editer], [ro].[datedeleted],  [ro].[deletedrecord], [ro].[coid], [ro].[brid], [ro].[dpid], 
    [ro].[nrcp_episode], [ro].[attendees], [ro].[oldstatus], [ro].[oldbill],  [ro].[oldqty], [ro].[timelogid], [ro].[recurrencestate], [ro].[busystatus], 
    [ro].[importancelevel], [ro].[labelid], [ro].[scheduleid], [ro].[recurrencepatternid], [ro].[isrecurrenceexceptiondeleted],   [ro].[rexceptionstarttimeorig], [ro].[rexceptionendtimeorig], 
    [ro].[IsDocumentationOnly], [ro].[ismeeting], [ro].[isprivate],  [ro].[isreminder], [ro].[reminderminutesbeforestart], [ro].[branch],   [ro].[shiftname], 1 AS [HasServiceNotes], [ro].[time2], 
    [ro].[ta_earlystart], [ro].[ta_latestart], [ro].[ta_earlygo],  [ro].[ta_latego], [ro].[ta_tooshort], [ro].[ta_toolong],   [ro].[billdesc], [ro].[variationreason], NULL AS [ClaimedStart], 
    NULL AS [ClaimedEnd], NULL AS [ClaimedDate], NULL AS [ClaimedBy],  0 AS [KM], [ro].[staffposition], 0 AS [StartKM], 0 AS [EndKM],  NULL AS [APInvoiceDate], NULL AS [APInvoiceNumber], 
    [ro].[ta_excludegeolocation], [ro].[ta_excludefromappalerts], [ro].[interpreterpresent], [ro].[extraitems], NULL AS [CloseDate],  [ro].[charge], [ro].[tamode], [ro].[ta_multishift], NULL AS [TravelBatch], 
    NULL AS [NDIABatch], [ro].[ownvehicle], [ro].[clientapproved],  [ro].[disable_shift_start_alarm], [ro].[disable_shift_end_alarm],   @DMTransID AS DMTransID, [ro].[datasetqty], [ro].[recipient_signature], 
    [ro].[shiftlocked], [ro].[servicetypeportal], [ro].[remindersent],   [ro].[EndDate]
FROM   
    roster ro
    LEFT JOIN itemtypes it ON ro.[service type] = it.title
WHERE  
    recordno IN (SELECT * FROM Split(@RecordNo, ',')) AND [carer code] = @carerCode and (@UnallocateAdminActivities=1 or ro.type<>6);

	if (@UnavailableForBalanceDays=1)
	begin
		INSERT INTO [dbo].[Roster]
		   ([Client Code], [Carer Code], [Service Type], [Service Description],  [Program], [Date], [Start Time], [Duration], [Unit Pay Rate], [Unit Bill Rate],
			[YearNo], [MonthNo], [Dayno], [BlockNo], [Notes], [CarerPhone], [UBDRef], [Type], [Status], [Anal], [Date Entered], [Date Last Mod],
			[Date Timesheet], [Date Invoice], [Date Payroll], [InvoiceNumber],[TimesheetNumber], [Batch#], [Transferred], [GroupActivity], [BillType],
			[BillTo], [CostUnit], [CostQty], [HACCType], [HACCID], [BillUnit],   [BillQty], [TaxPercent], [TaxAmount], [Link], [Tagged], [UniqueID],
			[ROS_PANZTEL_UPDATED], [ROS_COPY_BATCH#], [ROS_PAY_BATCH], [NRCP_CARER_CODE], [NRCP_CONTACT_URGENCY], [NRCP_CONTACT_TIME],
			[NRCP_REFERRAL_SERVICE], [ServiceSetting], [ROS_DAELIBS_UPDATED], [InUse], [DischargeReasonType], [DATASETClient], [NRCP_TypeOfAssitance],
			[NRCP_ReferralToServices], [NRCP_VolunteerServices],   [NRCP_ReasonNoAssistance], [DMStat], [Creator], [Editer], [DateDeleted],
			[DeletedRecord], [COID], [BRID], [DPID], [NRCP_Episode], [Attendees],  [OldStatus], [OldBill], [OldQty], [TimeLogID], [RecurrenceState],
			[BusyStatus], [ImportanceLevel], [LabelID], [ScheduleID],  [RecurrencePatternID], [IsRecurrenceExceptionDeleted],
			[RExceptionStartTimeOrig], [RExceptionEndTimeOrig],  [IsDocumentationOnly], [IsMeeting], [IsPrivate], [IsReminder],
			[ReminderMinutesBeforeStart], [Branch], [ShiftName], [HasServiceNotes], [Time2], [TA_EarlyStart], [TA_LateStart], [TA_EarlyGo], [TA_LateGo],
			[TA_TooShort], [TA_TooLong], [BillDesc], [VariationReason], [ClaimedStart], [ClaimedEnd], [ClaimedDate], [ClaimedBy], [KM],
			[StaffPosition], [StartKM], [EndKM], [APInvoiceDate], [APInvoiceNumber], [TA_EXCLUDEGEOLOCATION], [TA_EXCLUDEFROMAPPALERTS], [InterpreterPresent],
			[ExtraItems], [CloseDate], [charge], [TAMode], [TA_Multishift], [TravelBatch], [NDIABatch], [OwnVehicle], [ClientApproved],
			[disable_shift_start_alarm], [disable_shift_end_alarm], [DMTransID],  [DatasetQty], [Recipient_Signature], [ShiftLocked], [ServiceTypePortal], [ReminderSent], [EndDate])
	SELECT '!INTERNAL' AS [Client Code],  [ro].[carer code],  'UNAVAILABLE' AS [Service Type], 'UNAVAILABLE' AS [Service Description],'!INTERNAL' AS [Program],
		   [ro].[date], '00:00' AS [Start Time], 288 AS [Duration],   '0' AS [Unit Pay Rate],  0 AS [Unit Bill Rate],  [ro].[yearno],   [ro].[monthno],  [ro].[dayno],
		   0 AS [BlockNo],   'ON LEAVE' AS [Notes],  [ro].[carerphone], [ro].[ubdref],  13 AS [Type],  '1' AS [Status], [ro].[anal],  GETDATE() AS [Date Entered],
		   GETDATE() AS [Date Last Mod],   NULL AS [Date Timesheet], NULL AS [Date Invoice],  NULL AS [Date Payroll],  NULL AS [InvoiceNumber], NULL AS [TimesheetNumber],
		   NULL AS [Batch#], 0 AS [Transferred],  0 AS [GroupActivity],  [ro].[billtype], '!INTERNAL' AS [BillTo], 'HOUR' AS [CostUnit], [ro].[costqty],  [ro].[hacctype],
		   [ro].[haccid],  [ro].[billunit], [ro].[billqty], [ro].[taxpercent],[ro].[taxamount], NULL AS [Link], [ro].[tagged],[ro].[uniqueid], 0 AS [ROS_PANZTEL_UPDATED],
		   NULL AS [ROS_COPY_BATCH#], NULL AS [ROS_PAY_BATCH], [ro].[nrcp_carer_code],  [ro].[nrcp_contact_urgency],[ro].[nrcp_contact_time], [ro].[nrcp_referral_service],
		   [ro].[servicesetting],  [ro].[ros_daelibs_updated], 0 AS [InUse], [ro].[dischargereasontype],  [ro].[datasetclient], [ro].[nrcp_typeofassitance],
		   [ro].[nrcp_referraltoservices], [ro].[nrcp_volunteerservices], [ro].[nrcp_reasonnoassistance],[ro].[dmstat], @user AS [Creator], @user AS [Editer], [ro].[datedeleted],
		   [ro].[deletedrecord], [ro].[coid],  [ro].[brid], [ro].[dpid], [ro].[nrcp_episode], [ro].[attendees], [ro].[oldstatus], [ro].[oldbill],[ro].[oldqty], [ro].[timelogid],
		   [ro].[recurrencestate], [ro].[busystatus], [ro].[importancelevel], [ro].[labelid], [ro].[scheduleid], [ro].[recurrencepatternid],  [ro].[isrecurrenceexceptiondeleted],
		   [ro].[rexceptionstarttimeorig], [ro].[rexceptionendtimeorig], [ro].[IsDocumentationOnly], [ro].[ismeeting], [ro].[isprivate], [ro].[isreminder],  [ro].[reminderminutesbeforestart],
		   [ro].[branch], [ro].[shiftname],   1 AS [HasServiceNotes],  [ro].[time2], [ro].[ta_earlystart], [ro].[ta_latestart],  [ro].[ta_earlygo],  [ro].[ta_latego], [ro].[ta_tooshort],
		   [ro].[ta_toolong],  [ro].[billdesc], [ro].[variationreason], NULL AS [ClaimedStart],  NULL AS [ClaimedEnd], NULL AS [ClaimedDate], NULL AS [ClaimedBy],  0 AS [KM],
		   [ro].[staffposition],   0 AS [StartKM], 0 AS [EndKM],  NULL AS [APInvoiceDate], NULL AS [APInvoiceNumber], [ro].[ta_excludegeolocation], [ro].[ta_excludefromappalerts],
		   [ro].[interpreterpresent], [ro].[extraitems], NULL AS [CloseDate], [ro].[charge], [ro].[tamode], [ro].[ta_multishift], NULL AS [TravelBatch],NULL AS [NDIABatch], [ro].[ownvehicle],
		   [ro].[clientapproved], [ro].[disable_shift_start_alarm], [ro].[disable_shift_end_alarm], NULL AS [DMTransID], [ro].[datasetqty],[ro].[recipient_signature], [ro].[shiftlocked],
		   [ro].[servicetypeportal], [ro].[remindersent], [ro].[EndDate]
	FROM   roster ro
		   LEFT JOIN itemtypes it
				  ON ro.[service type] = it.title
	WHERE  recordno IN (SELECT * FROM Split(@RecordNo, ',')) 
		   AND [carer code] = @carerCode and (@UnallocateAdminActivities=1 or ro.type<>6);
	End

	UPDATE Roster SET [ShiftName] = [Carer Code], [Carer Code] = 'BOOKED', [Branch] = @Branch, [TYPE] = 1, [SERVICE DESCRIPTION] = 'NOPAY', [Unit Pay Rate] = 0  WHERE RecordNo IN (SELECT * FROM Split(@RecordNo,',')) and (@UnallocateAdminActivities=1 or type<>6);

	INSERT INTO AUDIT (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser)  
	SELECT @user AS Operator,getDate() AS ActionDate, 'UNALLOCATE STAFF: ' + [Carer Code] + '-' + [Date] + '-' + [Start Time] AS AuditDescription, 'Roster' AS ActionOn, [RecordNo] AS WhoWhatCode, @user AS TraccsUser 
	FROM Roster WHERE RecordNo IN (SELECT * FROM Split(@RecordNo, ','));

	UPDATE Roster SET HasServiceNotes = 1 WHERE DMTransID = @DMTransID;

	INSERT INTO History (PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator) 
                      SELECT RecordNo, GetDate(), 
                      'UNALLOCATED-ORIGINALLY ALLOCATED TO ' + [ShiftName] + ' BUT WORKER UNABLE TO DO SHIFT DUE TO ' + @ServiceDescription AS Detail, 'SVCNOTE', '', [Client Code], @user
					  FROM Roster WHERE RecordNo IN (SELECT * FROM Split(@RecordNo,',')) AND Date > '2000/01/01';

     INSERT INTO History (PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator) 
     SELECT RecordNo, GetDate(),  [Carer Code] + ' UNAVAILABLE FOR WORK DUE TO ' + [Service Type] AS Detail, 'SVCNOTE', '', [Client Code],  @user
     FROM Roster WHERE  DMTransID = @DMTransID AND Date > '2000/01/01';       

	DELETE FROM Roster WHERE Recordno IN (SELECT * FROM Split(@RecordNo,',')) AND Type in (6,13);
	

End

GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   procedure [dbo].[spSuspendRecipient] (
	@ClientCode NVARCHAR(max),			
	@CarerCode NVARCHAR(max),			
	@ServiceType  NVARCHAR(max),		
	@Program NVARCHAR(MAX),				
	@Date DATETIME,				        
	@Time NVARCHAR(max),                
	@Creator NVARCHAR(max),				
	@Editer NVARCHAR(max),				
	@BillUnit NVARCHAR(max),			
	@AgencyDefinedGroup NVARCHAR(max),  
	@ReferralCode NVARCHAR(max),		
	@Type SMALLINT,						
	@Duration SMALLINT,                 
	@TimePercent NVARCHAR(max),         
	@Notes NVARCHAR(max),               
	@BlockNo REAL,                      
	@ReasonType INT,					
	@TabType NVARCHAR(max),
	@HaccType NVARCHAR(MAX),			
	@BillTo NVARCHAR(MAX),				
	@CostUnit NVARCHAR(MAX)	= 'HOUR',	
	@BillDesc NVARCHAR(MAX) = '',
	@UnitPayRate FLOAT = 0,
	@UnitBillRate FLOAT = 0,
	@TaxPercent FLOAT = 0,
	@APIInvoiceDate NVARCHAR(MAX) = '',
	@APIInvoiceNumber NVARCHAR(MAX) = '',		   
	@StartDate   NVARCHAR(max),
	@EndDate   NVARCHAR(max)
	
	)
	As
	begin
	
	Declare @PersonId     NVARCHAR(max) ;
	select  @PersonId=UniqueID from Recipients where AccountNo=@ClientCode;

		DELETE r FROM ROSTER r INNER JOIN ItemTypes it ON r.[Service Type] = it.Title  INNER JOIN HumanResourceTypes p ON p.name = r.[program] WHERE     r.[Type] IN (7)     AND it.MinorGroup = 'FEE'     AND p.Type = 'DOHA'     AND r.[Client Code] = @ClientCode    AND r.[Program] = @Program    AND r.[Service Type] = @ServiceType     AND r.[Date] BETWEEN @StartDate AND @StartDate 

		INSERT INTO Roster([Client Code], [carer code], [Service Type], [Service Description], [program], [Date], [start time], [Duration], [Unit Pay Rate], [Unit Bill Rate], [Yearno], [Monthno], [DayNo], [BlockNo], [Type], [Status], [Date Entered], [Creator], [Date Last Mod], [Date Timesheet], [date invoice], [date payroll], [BillTo], [CostQty], [BillQty], [DatasetClient], InUse, [editer], [Notes], [ROS_COPY_BATCH#], GroupActivity ) 
		VALUES (@ClientCode, '!INTERNAL', @ServiceType, 'N/A', '!INTERNAL', @Date, '00:00', 288, 0, 0, year(@Date),month(@Date),day(@Date), 0, 4, 1, convert(varchar,getDate(),111), @Creator, convert(varchar,getDate(),111), convert(varchar,getDate(),111), convert(varchar,getDate(),111), convert(varchar,getDate(),111), '!INTERNAL', 1, 1, '!INTERNAL', 0, @Creator, 'ON LEAVE', 0, 0 )
		declare @RecordNo int;
		SELECT @RecordNo=@@IDENTITY;

		DELETE FROM ROSTER WHERE DATE between  @StartDate AND @EndDate AND [CARER CODE] = '' AND Type = 4 AND RecordNo <> @RecordNo


End

Go
Create or Alter procedure spSuspendRecipientNote (			
		
	@PersonId     NVARCHAR(max) = '',
	@Program	  NVARCHAR(max) = '',
    @DetailDate   DATETIME,
    @ExtraDetail1 NVARCHAR(max) = '',
    @ExtraDetail2 NVARCHAR(max) = '',
    @WhoCode      NVARCHAR(max) = '',
    @PublishToApp BIT = 0,   
	@Creator NVARCHAR(max),	
    @Note         NVARCHAR(max) = '',
    @AlarmDate    DATETIME = '',
    @ReminderTo   NVARCHAR(max),
	@CancellationCode  NVARCHAR(max),	
	@ReasonType NVARCHAR(max),	
	@StartDate   NVARCHAR(max),
	@EndDate   NVARCHAR(max)
	
	)
	As
	begin
	declare @ClientCode NVARCHAR(max);	

	select @ClientCode=AccountNo from Recipients where UniqueID=@PersonId;

INSERT INTO HumanResources(PersonID, [GROUP], [Type], Name, Address1, Notes, LockboxLocation, LockboxCode, SerialNumber, DateInstalled, Date1, Date2) VALUES (@PersonId, 'RECIPLEAVE', 'RECIPLEAVE', @CancellationCode, @ReasonType, @Note, @ReminderTo, 'CANCELLATION',@ExtraDetail1 ,  @AlarmDate, @StartDate, @EndDate );

INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@Creator, getDate(), 'ADD RECIPIENT LEAVE', @ClientCode, @PersonId, @Creator)

INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@Creator, getDate(), 'SUSPEND CLIENT', @StartDate + ' ' +  @EndDate, @ClientCode, @Creator)

INSERT INTO History (PersonID, DetailDate, Detail, AlarmDate, WhoCode, Creator ) VALUES (@PersonId, convert(varchar,getDate(),111), @Note,@AlarmDate, @ClientCode, @Creator)

INSERT INTO History (PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator ) VALUES (@PersonId, getDate(), 'SERVICE SUSPENSION -' + @ClientCode + @Note, @ExtraDetail1, @ExtraDetail2, @ClientCode, @Creator)


End

Go
Create or Alter procedure spSuspendRecipientNote (			
		
	@PersonId     NVARCHAR(max) = '',
	@Program	  NVARCHAR(max) = '',
    @DetailDate   DATETIME,
    @ExtraDetail1 NVARCHAR(max) = '',
    @ExtraDetail2 NVARCHAR(max) = '',
    @WhoCode      NVARCHAR(max) = '',
    @PublishToApp BIT = 0,   
	@Creator NVARCHAR(max),	
    @Note         NVARCHAR(max) = '',
    @AlarmDate    DATETIME = '',
    @ReminderTo   NVARCHAR(max),
	@CancellationCode  NVARCHAR(max),	
	@ReasonType NVARCHAR(max),	
	@StartDate   NVARCHAR(max),
	@EndDate   NVARCHAR(max)
	
	)
	As
	begin
	declare @ClientCode NVARCHAR(max);	

	select @ClientCode=AccountNo from Recipients where UniqueID=@PersonId;

INSERT INTO HumanResources(PersonID, [GROUP], [Type], Name, Address1, Notes, LockboxLocation, LockboxCode, SerialNumber, DateInstalled, Date1, Date2) VALUES (@PersonId, 'RECIPLEAVE', 'RECIPLEAVE', @CancellationCode, @ReasonType, @Note, @ReminderTo, 'CANCELLATION',@ExtraDetail1 ,  @AlarmDate, @StartDate, @EndDate );

INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@Creator, getDate(), 'ADD RECIPIENT LEAVE', @ClientCode, @PersonId, @Creator)

INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@Creator, getDate(), 'SUSPEND CLIENT', @StartDate + ' ' +  @EndDate, @ClientCode, @Creator)

INSERT INTO History (PersonID, DetailDate, Detail, AlarmDate, WhoCode, Creator ) VALUES (@PersonId, convert(varchar,getDate(),111), @Note,@AlarmDate, @ClientCode, @Creator)

INSERT INTO History (PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator ) VALUES (@PersonId, getDate(), 'SERVICE SUSPENSION -' + @ClientCode + @Note, @ExtraDetail1, @ExtraDetail2, @ClientCode, @Creator)


End

Go
create or alter procedure deleteRecipient(@AccountNo varchar(50),@WinUser varchar(100), @user varchar(100))
as
begin
      declare @PersonId varchar(50);
      select @PersonId=UniqueID from Recipients where AccountNo=@AccountNo;

      delete  FROM Roster WHERE [Client Code] = @AccountNo AND Status = 1;


      DELETE FROM NamesAndAddresses WHERE PersonID = @PersonId

      DELETE FROM PhoneFaxOther WHERE PersonID = @PersonId

      DELETE FROM NDiagnosis WHERE PersonID = @PersonId

      DELETE FROM MDiagnosis WHERE PersonID = @PersonId

      DELETE FROM MProcedures WHERE PersonID = @PersonId

      DELETE FROM HumanResources WHERE PersonID = @PersonId

      DELETE FROM RecipientPrograms WHERE PersonID = @PersonId

      DELETE FROM ServiceOverview WHERE PersonID = @PersonId

      DELETE FROM ONI WHERE PersonID = @PersonId

      DELETE FROM ONIMainIssues WHERE PersonID = @PersonId

      DELETE FROM ONIActionPlan WHERE PersonID = @PersonId

      DELETE FROM ONIHealthConditions WHERE PersonID = @PersonId

      DELETE FROM ONIMedications WHERE PersonID = @PersonId

      DELETE FROM ONISecondaryIssues WHERE PersonID = @PersonId

      DELETE FROM ONIServices WHERE PersonID = @PersonId

      DELETE FROM Recipients WHERE UniqueID = @PersonId

      INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@WinUser, getDate(), 'DELETE RECIPIENT', 'RECIPIENTS', @AccountNo, @user)

  END
  GO
create or alter procedure UpdateRecipientCode (@OldCode varchar(100), @NewCode varchar(100))
AS 
  Begin
      SELECT AccountNo FROM Recipients WHERE AccountNo = @OldCode;;


      UPDATE Roster SET [Client Code] = @NewCode WHERE [Client Code] = @OldCode;

      UPDATE Roster SET billto = @NewCode WHERE billto = @OldCode;

      UPDATE Roster SET DATASETClient = @NewCode WHERE DATASETClient = @OldCode;

      UPDATE Roster_Del SET [Client Code] = @NewCode WHERE [Client Code] = @OldCode;

      UPDATE Roster_Del SET billto = @NewCode WHERE billto = @OldCode;

      UPDATE Roster_Del SET DATASETClient = @NewCode WHERE DATASETClient = @OldCode;

      UPDATE Roster_Hold SET [Client Code] = @NewCode WHERE [Client Code] = @OldCode;

      UPDATE Roster_Hold SET billto = @NewCode WHERE billto = @OldCode;

      UPDATE Roster_Hold SET DATASETClient = @NewCode WHERE DATASETClient = @OldCode;

      UPDATE History SET WhoCode = @NewCode WHERE WhoCode = @OldCode;

      UPDATE Roster_trans SET rts_billto = @NewCode WHERE rts_billto = @OldCode;

      UPDATE Recipients SET Recipients.AccountNo = @NewCode WHERE Recipients.AccountNo = @OldCode;

      UPDATE Recipients SET Recipients.Billto = @NewCode WHERE Recipients.Billto = @OldCode;

      UPDATE Recipients SET Recipients.DatasetCarer = @NewCode WHERE Recipients.DatasetCarer = @OldCode;

      UPDATE ServiceOverview SET ServiceBiller = @NewCode WHERE ServiceBiller = @OldCode;

      UPDATE InvoiceHeader SET [Client Code] = @NewCode WHERE [Client Code] = @OldCode;

      UPDATE InvoiceHeader SET [Patient Code] = @NewCode WHERE [Patient Code] = @OldCode;

      UPDATE Media SET [Target] = @NewCode WHERE [Target] = @OldCode;

      UPDATE Recipients SET GPUpdated = 0 WHERE AccountNO = @NewCode;
End
GO