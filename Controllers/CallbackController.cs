using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using Adamas.Models.XeroModels;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using System.Net.Http.Headers;
using Adamas.XeroUtilities;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;

using Xero.NetStandard.OAuth2.Api;
using Xero.NetStandard.OAuth2.Client;
using Xero.NetStandard.OAuth2.Config;
using Microsoft.Extensions.Options;
using Xero.NetStandard.OAuth2.Models;

using Xero.NetStandard.OAuth2.Model.PayrollAu;
using Xero.NetStandard.OAuth2.Token;
using Adamas.Models.Modules;


using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using CsvHelper;

namespace Adamas.Controllers
{
    [Route("callback")]
    public class CallbackController : Controller
    {
        private readonly XeroConfigurationPKCE _xeroConfigPKCE;
        private readonly IOptions<XeroConfiguration> _xeroConfig;
        public CallbackController(
            IOptions<XeroConfiguration> xeroConfig
        ){
            _xeroConfig = xeroConfig;
        }

        // [HttpGet("xero")]
        // public async Task<IActionResult> Xero([FromQuery] QueryString uri)
        // {
        //     var code = uri.Code;
        //     var state = uri.Session_State;

        //     const string url = "https://identity.xero.com/connect/token";

        //     var xeroConfig = this._xeroConfig;

        //     var client = new HttpClient();
        //     var formContent = new FormUrlEncodedContent(new[]
        //     {
        //         new KeyValuePair<string, string>("grant_type", "authorization_code"),
        //         new KeyValuePair<string, string>("client_id", xeroConfig.ClientID),
        //         new KeyValuePair<string, string>("code", code),
        //         new KeyValuePair<string, string>("redirect_uri", xeroConfig.UriCallback),
        //         new KeyValuePair<string, string>("code_verifier", xeroConfig.CodeVerifier),
        //     });

        //     var response = await client.PostAsync(url, formContent);
        //     var content = await response.Content.ReadAsStringAsync();

        //     var tokens = JObject.Parse(content);

        //     XeroTokens token = new XeroTokens(){
        //         Access_Token = tokens["access_token"].ToString(),
        //         Id_Token = tokens["id_token"].ToString(),
        //         Refresh_Token = tokens["refresh_token"].ToString(),
        //     };

        //     string serializedXeroToken = System.Text.Json.JsonSerializer.Serialize(token);
        //     System.IO.File.WriteAllText("./xerotoken.json", serializedXeroToken);


        //     var cc = new HttpClient();
        //     var xeroToken = TokenUtilities.GetStoredToken();

        //     cc.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", xeroToken.Access_Token);
            
        //     var res = await cc.GetAsync(this._xeroConfig.ConnectionsUrl);
        //     var ct = await res.Content.ReadAsStringAsync();

        //     //fill the dropdown box based on the results 
        //     var tenants = JsonConvert.DeserializeObject<List<Tenant>>(ct);
            
        //     string tenantString = System.Text.Json.JsonSerializer.Serialize(tenants);
        //     System.IO.File.WriteAllText("./xerotenant.json", tenantString);

        //     return Ok(tenants);
        // }

        [HttpGet("xero")]
        public async Task<IActionResult> Xero([FromQuery] QueryString uri)
        {
            var code = uri.Code;
            var state = uri.Session_State;

            var httpClient = new HttpClient();

            var client = new XeroClient(_xeroConfig.Value, httpClient);
            var xeroToken = (XeroOAuth2Token)await client.RequestAccessTokenAsync(code);

            List<Tenant> tenants = await client.GetConnectionsAsync(xeroToken);

            Tenant firstTenant = tenants[0];

            TokenUtilities.StoreToken(xeroToken);
            TokenUtilities.StoreTenant(firstTenant);

            // const string url = "https://identity.xero.com/connect/token";

            // var xeroConfig = this._xeroConfig;

            // var client = new HttpClient();
            // var formContent = new FormUrlEncodedContent(new[]
            // {
            //     new KeyValuePair<string, string>("grant_type", "authorization_code"),
            //     new KeyValuePair<string, string>("client_id", xeroConfig.ClientID),
            //     new KeyValuePair<string, string>("code", code),
            //     new KeyValuePair<string, string>("redirect_uri", xeroConfig.UriCallback),
            //     new KeyValuePair<string, string>("code_verifier", xeroConfig.CodeVerifier),
            // });

            // var response = await client.PostAsync(url, formContent);
            // var content = await response.Content.ReadAsStringAsync();

            // var tokens = JObject.Parse(content);

            // XeroTokens token = new XeroTokens(){
            //     Access_Token = tokens["access_token"].ToString(),
            //     Id_Token = tokens["id_token"].ToString(),
            //     Refresh_Token = tokens["refresh_token"].ToString(),
            // };

            // string serializedXeroToken = System.Text.Json.JsonSerializer.Serialize(token);
            // System.IO.File.WriteAllText("./xerotoken.json", serializedXeroToken);


            // var cc = new HttpClient();
            // var xeroToken = TokenUtilities.GetStoredToken();

            // cc.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", xeroToken.Access_Token);
            
            // var res = await cc.GetAsync(this._xeroConfig.ConnectionsUrl);
            // var ct = await res.Content.ReadAsStringAsync();

            // //fill the dropdown box based on the results 
            // var tenants = JsonConvert.DeserializeObject<List<Tenant>>(ct);
            
            // string tenantString = System.Text.Json.JsonSerializer.Serialize(tenants);
            // System.IO.File.WriteAllText("./xerotenant.json", tenantString);

            return Ok(tenants);
        }

        [HttpGet("employees")]
        public async Task<IActionResult> GetEmployees()
        {
            var xeroToken = TokenUtilities.GetStoredToken();
            var utcTimeNow = DateTime.UtcNow;

            var httpClient = new HttpClient();

            if (utcTimeNow > xeroToken.ExpiresAtUtc)
            {
                var client = new XeroClient(_xeroConfig.Value, httpClient);
                xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);
                TokenUtilities.StoreToken(xeroToken);
            }

            string accessToken = xeroToken.AccessToken;
            string xeroTenantId = xeroToken.Tenants[0].TenantId.ToString();

            var PayrollAUApi = new PayrollAuApi();  

            var employeesList = await PayrollAUApi.GetEmployeesAsync(accessToken, xeroTenantId);
            var payItemsList = await PayrollAUApi.GetPayItemsAsync(accessToken, xeroTenantId);
            var trackingList = await PayrollAUApi.GetSettingsAsync(accessToken, xeroTenantId);

            var employees = employeesList._Employees;
            var payItems = payItemsList._PayItems;
            var trackingItems = trackingList.Settings;

            IEnumerable<XeroUser> xeroUsers = new List<XeroUser>();
            var finalUsers = new List<XeroUser>();

            using (var reader = new StreamReader(@"C:\Users\mark\Desktop\Programming\Adamas\Timesheet.csv"))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                xeroUsers = csv.GetRecords<XeroUser>().ToList();
            }

            // var distinctUsers = xeroUsers.Select(x => x.Staff).Distinct().ToList();

            foreach(var users in xeroUsers.Select(x => x.Staff).Distinct().ToList())
            {
                var employee = employees.Where(x => x.FirstName.ToUpper() == users.ToUpper()).FirstOrDefault();
                var payItem = payItems.EarningsRates.Where(pi => pi.Name == "Ordinary Hours").FirstOrDefault();
                var trackingItem = trackingItems.Accounts.Where(acc => acc.Code == "804").FirstOrDefault();

                if(employee == null){
                    break;
                }

                finalUsers = xeroUsers.Where(x => x.Staff == users).Select(x => {
                    x.XeroEmployeeId = (employee.EmployeeID).ToString();
                    x.XeroEarningsRateId = (payItem.EarningsRateID).ToString();
                    x.TrackingItemId = (trackingItem.AccountID).ToString();
                    return x;
                }).ToList();
            }


            using (var writer = new StreamWriter(@"C:\Users\mark\Desktop\Programming\Adamas\Timesheet-New.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                // csv.Configuration.SanitizeForInjection = true;
                csv.WriteRecords(finalUsers);
                csv.Flush();
                writer.Flush();
            }



            // var invoiceFile = ProcessFile(@"C:\Users\mark\Desktop\Programming\Adamas\Timesheet.csv");
            // var distinctUsers = invoiceFile.Select(x => x.Staff).Distinct().ToList();

            // distinctUsers.ForEach(data => {
            //     var lists = invoiceFile.Where(x => x.Staff == data).Select(x => x);
            //     var found = employees.Find(x => (x.FirstName).ToUpper() == data.ToUpper());

            //     if(!(found is null))
            //     {
            //         var id = found.EmployeeID;
            //     }
            // });

            return Ok(employees);
        }

        private static List<XeroUser> ProcessFile(string path){
            return System.IO.File.ReadAllLines(path)
                    .Skip(1)
                    .Where(line => line.Length > 1)
                    .Select(XeroUser.ParseFromCSV)
                    .ToList();
        }

        [HttpGet("pay-items")]
        public async Task<IActionResult> GetPayItems()
        {
            var xeroToken = TokenUtilities.GetStoredToken();
            var utcTimeNow = DateTime.UtcNow;

            var httpClient = new HttpClient();

            if (utcTimeNow > xeroToken.ExpiresAtUtc)
            {
                var client = new XeroClient(_xeroConfig.Value, httpClient);
                xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);
                TokenUtilities.StoreToken(xeroToken);
            }

            string accessToken = xeroToken.AccessToken;
            string xeroTenantId = xeroToken.Tenants[0].TenantId.ToString();

            var PayrollAUApi = new PayrollAuApi();  

            var response = await PayrollAUApi.GetPayItemsAsync(accessToken, xeroTenantId);

            var payitems = response._PayItems;          
            return Ok(payitems);
        }

        
        // [HttpGet("invoices")]
        // public async Task<IActionResult> GetInvoices()
        // {
        //     var url = $"{this._xeroConfig.XeroApiUrlBase}/invoices";
        //     var client = new HttpClient();

        //     var xeroToken = TokenUtilities.GetStoredToken();
        //     var xeroTenant = TokenUtilities.GetTenantId();

        //     client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", xeroToken.Access_Token);
        //     client.DefaultRequestHeaders.Add("xero-tenant-id", xeroTenant[0].Id);
            
        //     var response = await client.GetAsync(url);
        //     var content = await response.Content.ReadAsStringAsync();
        //     return Ok(content);
        // }

        // [HttpGet("employees")]
        // public async Task<IActionResult> GetEmployees()
        // {
        //     var url = $"{this._xeroConfig.XeroApiPayrollUrlBase}/employees";
        //     var client = new HttpClient();

        //     var xeroToken = TokenUtilities.GetStoredToken();
        //     var xeroTenant = TokenUtilities.GetTenantId();

        //     client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", xeroToken.Access_Token);
        //     client.DefaultRequestHeaders.Add("xero-tenant-id", xeroTenant[0].Id);

        //     // var AccountingApi = new AccountingApi();
            
        //     try
        //     {
        //         var payroll = new PayrollAuApi();
        //         var response = await payroll.GetEmployeesAsync(xeroToken.Access_Token, xeroTenant[0].Id);
        //         var employees = response._Employees;
        //     }
        //     catch (System.Exception ex)
        //     {
        //         throw ex;
        //     }

        //     // var response = await client.GetAsync(url);
        //     // var content = await response.Content.ReadAsStringAsync();
        //     return Ok(true);
        // }



        // [HttpGet("contacts")]
        // public async Task<IActionResult> GetContacts()
        // {
        //     var url = $"{this._xeroConfig.XeroApiUrlBase}/contacts";
        //     var client = new HttpClient();

        //     var xeroToken = TokenUtilities.GetStoredToken();
        //     var xeroTenant = TokenUtilities.GetTenantId();

        //     client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", xeroToken.Access_Token);
        //     client.DefaultRequestHeaders.Add("xero-tenant-id", xeroTenant[0].Id);
            
        //     var response = await client.GetAsync(url);
        //     var content = await response.Content.ReadAsStringAsync();
        //     return Ok(content);
        // }
    }


    public class QueryString
    {
        public string Code { get; set; }
        public string Scope { get; set; }
        public string Session_State { get; set; }
    }
}
