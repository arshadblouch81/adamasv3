﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adamas.Models.Modules
{
    public class EmailMessage
    {
        public EmailMessage()
        {
            ToAddress = new List<EmailAddress>();
            FromAddress = new EmailAddress();
        }

        public List<EmailAddress> ToAddress { get; set; }
        public EmailAddress FromAddress { get; set; }
        public List<EmailAddress> CCAddress { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string LeaveType { get; set; }
        public string Notes { get; set; }
        public string Body { get; set; }

        public List<EmailAttachment> Attachments { get; set; }
    }

    public class EmailAddress 
    {
        public string Name { get ; set; }
        public string Address { get; set; }
    }

    public class EmailAttachment
{
    public string FileName { get; set; }
    public string ContentBase64 { get; set; }
    public string ContentType { get; set; }
}
    
}
