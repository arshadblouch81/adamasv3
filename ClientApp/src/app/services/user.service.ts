import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

import { GlobalService } from './global.service';

const user: string = "api/user";
declare var Dto: any;

@Injectable()
export class UserService {

    constructor(
        public http: HttpClient,
        public auth: AuthService,
        public globalS: GlobalService
    ) { }

   getclientPortalView(): Observable<any> {
        return this.auth.get(`${user}/GetClientPortalView`);
   }
}

