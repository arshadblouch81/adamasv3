import { Component } from '@angular/core';


@Component({
    selector: 'allocate-staff',
    templateUrl: './allocate-staff.html'
})

export class AllocateStaff {
    selected: boolean = false;
    constructor(){

    }

    permissions: any = [
        {
            type: "Authenticated Users",
            expanded: true,
            rights: [
                {
                    name: "Read",
                    enable: true
                },
                {
                    name: "Modify",
                    enable: true
                },
                {
                    name: "Create",
                    enable: false
                },
                {
                    name: "Delete",
                    enable: false
                }
            ]
        },
        {
            type: "Owners",
            expanded: true,
            rights: [
                {
                    name: "Read",
                    enable: true
                },
                {
                    name: "Modify",
                    enable: true
                },
                {
                    name: "Create",
                    enable: true
                },
                {
                    name: "Delete",
                    enable: true
                }
            ]
        }
    ];
}