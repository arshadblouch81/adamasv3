import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil, delay } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import format from 'date-fns/format';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
    styles: [`
    nz-table{
        margin-top:0px;
    }
    nz-select{
        width:100%;
    }
    label.chk{
        position: absolute;
        top: 1.5rem;
    }
    .overflow-list{
        overflow: auto;
        height: 8rem;
        border: 1px solid #e3e3e3;
    }
    ul{
        list-style:none;
    }
    .chkboxes{
        padding:4px;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
         nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
    }
             tbody  tr:nth-child(odd) {
    background-color: #E9F7FF;
      }
        tbody  tr:nth-child(even) {
            background-color: #fff;
        }
    `],
    templateUrl: './psnote.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffPSAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    loading: boolean = false;
    loadingPDF: boolean = false;
    modalOpen: boolean = false;
    addORView: number = 1;
    categories: Array<any>;
    dateFormat: string = 'dd/MM/yyyy';
    isLoading: boolean = false;
    check : boolean = false;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    isVisibleTop = false;
    orderArr: Array<any> = ['Chronological-First To Last', 'Chronological-Last To First'];   
    enddate : Date;
    startdate: Date;
    bodystyle:object; 
    whereString :string=" HI.DeletedRecord = 0 ";
    
    private default = {
        notes: '',
        isPrivate: false,
        alarmDate: null,
        whocode: '',
        recordNumber: null,
        category: null
    }
    deletedRecord: string;
    recipientStrArr: any;
    mlist: any;
    public editorConfig:AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '15rem',
        minHeight: '5rem',
        translate: 'no',
        customClasses: []
    };
    selectedRow  : number = 0;
    activeRowData:any;
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private printS:PrintService,
        private cd: ChangeDetectorRef,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'ps-note')) {
                    this.cd.reattach();
                    console.log(data);
                    this.user = data;
                    this.search(this.user);
                }
            });      
        }
        
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked();
            if(this.user){
                this.search(this.user);
                this.buildForm();
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            this.inputForm = this.formBuilder.group({
                notes: '',
                isPrivate: false,
                alarmDate: null,
                whocode: '',
                restrictions: '',
                restrictionsStr:'public',
                recordNumber: null,
                category: ['', [Validators.required]],
                orderArr:['Chronological-First To Last'],
            });      
            this.inputForm.get('restrictionsStr').valueChanges.subscribe(data => {
                if (data == 'restrict') {
                    this.getSelect();
                } 
            });
        }
        
        populate(): void{
            this.listS.getlisthr().pipe(takeUntil(this.unsubscribe)).subscribe(data => this.categories = data)
        }
        
        search(user: any = this.user) {
            this.cd.reattach();
            
            this.loading = true;
            this.timeS.getpsnotes(user.code).pipe(delay(200),takeUntil(this.unsubscribe)).subscribe(data => {
                this.tableData = data;
                this.originalTableData = data;
                this.loading = false;
                this.selectedRow = 0;
                this.cd.detectChanges()
            });
            
            this.populate();
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        
        selectedItemGroup(data:any, i:number){
            this.selectedRow=i;
            this.activeRowData=data;
        }

        showAddModal() {
            this.addORView = 1;
            this.modalOpen = true;
        }
        
        showEditModal() {
            
            this.addORView = 0;
            const { alarmDate, detail,detail2, isPrivate,privateFlag,restrictions,category, creator, recordNumber } = this.tableData[this.selectedRow];
            
            this.inputForm.patchValue({
                notes: this.isRTF(detail,detail2),
                isPrivate: isPrivate,
                alarmDate: alarmDate,
                restrictions: '',
                restrictionsStr: this.determineRadioButtonValue(privateFlag, restrictions),
                whocode: creator,
                recordNumber: recordNumber,
                category: category
            });

            
            this.modalOpen = true;
        }
        isRTF(detail:string,detail2:string){
            let respone = this.globalS.isRTF(detail);
            if(respone){
                return detail2;
            }else{
                return detail;
            }
        }
        determineRadioButtonValue(privateFlag: Boolean, restrictions: string): string {
            if (!privateFlag && this.globalS.isEmpty(restrictions)) {
                return 'public';
            }
            
            if (!privateFlag && !this.globalS.isEmpty(restrictions)) {
                return 'restrict'
            }
            
            return 'workgroup';
        }
        archive(){
            const { recordNumber } = this.tableData[this.selectedRow];
            this.timeS.archivepsnotes(recordNumber).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'PS NOTE ARCHIVED');
                        this.search();
                        this.handleCancel();
                    }
                });
            }
        delete(){
            const { recordNumber } = this.tableData[this.selectedRow];
            this.timeS.deletepsnotes(recordNumber).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'PS NOTE ARCHIVED');
                        this.search();
                        this.handleCancel();
                    }
                });
            }
            getarchivedpsnotes(user: any = this.user){
                this.cd.reattach();
                this.loading = true;
                this.timeS.getarchivedpsnotes(user.code).pipe(delay(200),takeUntil(this.unsubscribe)).subscribe(data => {
                    this.tableData = data;
                    this.loading = false;
                    this.cd.detectChanges()
                });
                this.populate();
            }
            fetchAll(e){
                if(e.target.checked){
                    this.whereString = " HI.DeletedRecord = 1 ";
                    this.getarchivedpsnotes(this.user);
                }else{
                    this.whereString = " HI.DeletedRecord = 0 ";
                    this.search(this.user);
                }
            }
        
            save() {
                if (!this.globalS.IsFormValid(this.inputForm))
                return;        
                const cleanDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(
                    this.inputForm.get('alarmDate').value);
                    
                    
                    const { alarmDate, restrictionsStr, whocode, restrictions } = this.inputForm.value;
                    
                    let privateFlag = restrictionsStr == 'workgroup' ? true : false;
                    
                    let restricts = restrictionsStr != 'restrict';
                    
                    this.inputForm.controls["restrictionsStr"].setValue(privateFlag);
                    
                    this.inputForm.controls["restrictions"].setValue(restricts ? '' : this.listStringify());
                    this.inputForm.controls["whocode"].setValue(this.user.code);
                    this.timeS.postpsnotes(this.inputForm.value, this.user.id).pipe(
                        takeUntil(this.unsubscribe)).subscribe(data => {
                            if (data) {
                                this.globalS.sToast('Success', 'Note saved');
                                this.search();
                                this.handleCancel();
                                return;
                            }
                        });
                    }
                    log(event: any) {
                        this.recipientStrArr = event;
                    }
                    listStringify(): string {
                        let tempStr = '';
                        this.recipientStrArr.forEach((data, index, array) => {
                            array.length - 1 != index ?
                            tempStr += data.trim() + '|' :
                            tempStr += data.trim();
                        });
                        return tempStr;
                    }
                    getSelect() {
                        this.timeS.getmanagerop().subscribe(data => {
                            this.mlist = data;
                            this.cd.markForCheck();
                        });
                    }
                    handleCancel() {
                        this.modalOpen = false;
                        this.inputForm.reset(this.default);
                        this.isLoading = false;
                    }
                    handleOkTop() {
                        this.isVisibleTop = true;        
                        this.tryDoctype = ""
                        this.pdfTitle = ""
                    }
                    handleCancelTop(): void {
                        this.drawerVisible = false;
                        this.pdfTitle = ""
                        this.tryDoctype = ""
                        this.isVisibleTop = false;
                    }
                    generatePdf(){
                        var lblcriteria,strdate, endate;
                
                        this.isVisibleTop = false;        
                        
                
                        var date = new Date();
                        if (this.startdate != null) { strdate = format(this.startdate, 'yyyy-MM-dd') } else {                       
                            strdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd')
                        }
                        if (this.enddate != null) { endate = format(this.enddate, 'yyyy-MM-dd') } else {
                            endate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');
                        }
                
                        var Order = this.inputForm.value.orderArr
                        this.drawerVisible = true;        
                        this.loading = true;
                        var fQuery = "Select RecordNumber,CONVERT(varchar, [DetailDate],105) as [DetailDate], dbo.rtf2text(Detail) as Detail, CONVERT(varchar, [AlarmDate],105) as [AlarmDate], Creator as Creator From History HI INNER JOIN Staff ST ON ST.[UniqueID] = HI.[PersonID] WHERE ST.[AccountNo] = '"+this.user.code+"' AND HI.DeletedRecord <> 1 AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr' )) AND ExtraDetail1 = 'PSNOTE' ";        
                        fQuery = fQuery + " AND  (DetailDate between '"+strdate +"' AND '"+endate+"')"
                       
                        if(Order == "Chronological-Last To First"){
                            fQuery = fQuery +" ORDER BY DetailDate DESC "
                        }else{ 
                            fQuery = fQuery + "ORDER by DetailDate ASC ";
                        }
                        
                
                        var txtTitle = "PS Notes";       
                        var Rptid =   "8WTerq14eNFSAnz0"  
                
                        //console.log(fQuery)
                        const data = {                    
                            "template": { "_id": Rptid },                                
                            "options": {
                                "reports": { "save": false },                
                                "sql": fQuery,
                                "Criteria": lblcriteria,
                                "userid": this.tocken.user,
                                "txtTitle": txtTitle,
                                "Extra": this.user.code,                                                                
                            }
                        }
                        this.loading = true;
                        this.drawerVisible = true;         
                            this.printS.printControl(data).subscribe((blob: any) => {
                            this.pdfTitle = txtTitle+".pdf"
                                this.drawerVisible = true;                   
                                let _blob: Blob = blob;
                                let fileURL = URL.createObjectURL(_blob);
                                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                                this.loading = false;
                                this.cd.detectChanges();
                            }, err => {
                                console.log(err);
                                this.loading = false;
                                this.ModalS.error({
                                    nzTitle: 'TRACCS',
                                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                    nzOnOk: () => {
                                        this.drawerVisible = false;
                                    },
                                });
                            });
                            return;  
                      
                    }


    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Details',
        value: 'detail'
    },{
        key: 'Category',
        value: 'category'
    },{
        key: 'Detail Date',
        value: 'detailDate'
    },{
        key: 'Creator',
        value: 'creator'
    }];
    
    
    

    dragDestination = [       
        'Details',
        'Category',
        'Detail Date',
        'Creator'
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
    
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        console.log("staff "+ permissoons.length );
        return permissoons.charAt(index-1);
    }
}