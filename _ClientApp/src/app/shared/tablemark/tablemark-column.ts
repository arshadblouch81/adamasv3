import { Component, Input, ViewEncapsulation } from '@angular/core'

@Component({
    selector: 'tablemark-column',
    template: `

            <tr>
                <th>asd</th>
                <th>asd</th>
                <th>asd</th>
                <th>asd</th>
            </tr>

    `,
    encapsulation: ViewEncapsulation.None
})

export class TableMarkColumn {
    @Input() title: Array<string>;
}