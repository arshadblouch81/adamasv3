import { Component, OnInit, ViewChild, NgZone } from '@angular/core'
import { LandingService, ListService, GlobalService, ExecutableService, roles, TimeSheetService } from '@services/index'
import { SwitchService } from '@services/switch.service'
import { TabService } from '@services/tabs.service'
import { BsModalComponent } from 'ng2-bs3-modal';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { mergeMap, debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import { Subject, Observable } from 'rxjs';
import * as _ from 'lodash';

import * as moment from 'moment';
import { TabsService } from '@clr/angular/layout/tabs/providers/tabs.service';

import { Router } from '@angular/router';

enum User {
    Recipient = 'recipient',
    Staff = 'staff'
}

interface ModalVariables {
    title: string,
    isMultiple: boolean
}

@Component({
    selector: 'divider',
    templateUrl: './landing.html',
    styles: [`
    ul{
        list-style:none;
    }
    div.list-wrap li{
        color:black;
        border-bottom:1px solid #f3f3f3;
    }
    ul.children-secondary{
        position: absolute;
        right: -7.9rem;
        width: 100%;
        top:-1px;
        visibility: hidden;
        z-index:6;
        background-color:#e2e2e2;
        border: 1px solid #bfbfbf;
        box-shadow: 0 8px 20px -7px #b9b9b9;
    }
    ul.children-secondary > li:hover{
        background: #2d89ff;
        color:#fff;
    }
    ul.children-secondary > li{
        padding:2px 4px;
        position:relative;
    }
    ul.children-secondary > li:hover > ul{
        visibility:visible;
    }
    ul.children{        
        position: absolute;
        top: 1.3rem;
        left: 0;
        width: 8rem;
        visibility: hidden;
        text-align: left;
        z-index: 5;
        line-height: 1rem !important;
        background-color: #e2e2e2;
        border: 1px solid #bfbfbf;
        box-shadow: 0 8px 20px -7px #b9b9b9;
    }
    .children-right{
        right:0;
        left: auto !important;
    }
    ul.children > li{
        padding:2px 4px;
        position:relative;
    }
    ul.children > li:hover{
        background: #2d89ff;
        color:#fff;
    }
    ul.children > li:hover > ul{
        visibility: visible;
    }
    ul{
        background:#fff;
    }
    div.list-wrap ul{
        list-style: none;
        margin:0;
    }
    div.list-wrap ul.main-list{
        list-style: none;
        margin: 0;
        display: flex;        
        line-height: 15px;
        background: #808080;
    }
    div.list-wrap > ul.main-list > li{ 
        display: flex;
        flex: 1;
        position: relative;
        justify-content: center;
        align-items: center;
        text-align:center;
        font-size:12px;
        padding: 15px;
        height: 1rem;
    }
    div.list-wrap ul.main-list > li:hover > ul.children{
        visibility: visible !important;
    }
    div.list-wrap li:hover{
        cursor:pointer;
    }
    ul.main-list > li:hover{
        background: #2d89ff;
        color:#fff;
    }
    ul.main-list > li{        
        height:2rem;
        color:#fff;
    }

    div.option-wrap{
        width: 20rem;
        margin: 0 auto;
        height: 85%;
        display: flex;
        flex-wrap: wrap;
        text-align: center;
    }

    div.option-wrap .options{
        flex-grow: 1;
        align-items: center;
        justify-content: center;
        width: 33%;
        display: flex;
    }

    div.option-wrap .options i{
        font-size: 40px;
    }

    div.option-wrap .options > div{
        cursor:pointer;
        background: #13498e;
        padding: 1.5rem 0;
        color: #fff;
        border-radius: 3px;
        width: 6rem;
        height: 6rem;
        position: relative;
    }

    .options > div > div{
        position: absolute;
        bottom: 1rem;
        right: 0;
        left: 0;
    }

    div.option-wrap .options div:hover{
        background-color:#1b549c !important;
    }


    .inline-group{
        display:flex;
    }
    .inline-group div:first-child{
        flex:2;
    }
    .inline-group div:last-child{
        flex:4;
    }
    datepicker.inline-element{
        display:inline-block;
    }    
    i.sub-children{
        position: absolute;
        margin-top: 6px;
        height: 100%;
        right: 8px;
    }
    i.sub-children-down{
        position: absolute;
        right: 8px;
    }
    clr-tabs >>> ul button {
        margin-right: 10px !important;
        font-size:12px !important;
    }
    table tbody tr td{
        padding:3px;
    }
    .table-wrap table.table-controls{
        margin:0;
    }
    td.align-left{
        text-align:left;
    }
   

    `]
})

export class LandingComponent implements OnInit {
    @ViewChild('modalList', { static: false }) modalList: BsModalComponent;
    @ViewChild('modalAddEdit', { static: false }) modalAddEdit: BsModalComponent;
    @ViewChild('modalFunding', { static: false }) modalFunding: BsModalComponent;
    @ViewChild('modalAddEditContactDetails', { static: false }) modalAddEditContactDetails: BsModalComponent;
    @ViewChild('modalAddRosters', { static: false }) modalAddRosters: BsModalComponent;
    @ViewChild('modalAddUserDetails', { static: false }) modalAddUserDetails: BsModalComponent;

    name: string;

    searchText$ = new Subject<string>();

    present: any = moment().startOf('week').add(1, 'day');
    future: any = moment(this.present).add(2, 'week')

    listArray: Array<any> = [];
    templistArray: Array<any> = [];

    tabNo: any;
    whatConfig = new Subject<number>();
    whatFunding = new Subject<number>();

    whatUser = new Subject<string>();

    loading: boolean = false;
    size: string;

    process: number;
    // Add/ChangeUsers
    userDetailFormGroup: FormGroup;
    userDetailRecipientRecGroup: FormGroup;
    userDetailDayManagerFormGroup: FormGroup;
    userDetailMainMenuOpGroup: FormGroup;
    userDetailStaffRecGroup: FormGroup;
    userDetailClientPortalGroup: FormGroup;
    userDetailTimeAttendanceGroup: FormGroup;

    selected: any = {

    }

    Divider: any;

    cdcClaim: any = {
        item: null,
        rate: null,
        recordNumber: null
    }

    selectedbject: any

    modalVariables: ModalVariables;

    secondTitle: any = {
        title: ''
    }

    category: any = {
        tableName: '',
        columns: '',
        columnValues: '',
        categoryName: '',
        setClause: '',
        whereClause: ''

    };

    contact: any = {
        type: '',
        name: '',
        address1: '',
        address2: '',
        suburb: '',
        phone1: '',
        phone2: '',
        fax: '',
        mobile: '',
        email: ''
    };



    constructor(
        private listS: ListService,
        private switchS: SwitchService,
        private globalS: GlobalService,
        private execS: ExecutableService,
        private tabS: TabService,
        private landS: LandingService,
        private ngZone: NgZone,
        private formBuilder: FormBuilder,
        private router: Router,
        private timeS: TimeSheetService
    ) {

        this.resetGroup();

        this.searchText$.pipe(
            debounceTime(200),
            distinctUntilChanged()
        ).subscribe(data => {
            this.listArray = this.templistArray.filter(x => {
                if (x.Description.indexOf(data.toUpperCase()) > -1)
                    return x;
            })
        })

    }


    insuranceProcess() {
        console.log(this.userDetailFormGroup.value)
    }



    resetGroup() {

        //this.userDetailFormGroup.get('Username').value
        //this.userDetailFormGroup.value

        //Add/ChangeUsers
        this.userDetailFormGroup = this.formBuilder.group({
            UserName: '',
            Password: '',
            LoginGroup: '',
            TraccsCode: '',
            LoginMode: '',
            HomeBranch: '',
            RecipientEditAccess: '',
            CAddRecipRec: '',
            CaseNotesAreViewOnly: '',
            CChangeRecipCode: '',
            StaffEditAccess: '',
            AccessHR: '',
            Rosters: '',
            ChangeMasterRoster: '',
            LockOwnRoster: '',
            AllowReAllocateRoster: '',
            AllowMasterSave: '',
            CRosterOvertime: '',
            CRosterWithoutBreaks: '',
            CRosterWithConflicts: '',
            DayManager: '',
            ManualCopyRoster: '',
            AutoCopyRoster: '',
            TSheets: '',
            SuggestedTSheets: '',
            PBCupdates: '',
            Listings: '',
            CDCClaims: '',
            AllowTransitionProgram: '',
            Financial: '',
            InvoiceInquiry: '',
            GenOne: '',
            GenTwo: '',
            GenThree: '',
            GenFour: '',
            GenFive: '',
            GenSix: '',
            GenSeven: '',
            GenEight: '',
            GenNine: '',

        })

        this.userDetailRecipientRecGroup = this.formBuilder.group({
            RRec1: '',
            RRec2: '',
            RRec3: '',
            RRec4: '',
            RRec5: '',
            RRec6: '',
            RRec7: '',
            RRec8: '',
            RRec9: '',
            RRec10: '',
            RRec11: '',
            RRec12: '',
            RRec13: '',
            RRec14: '',
            RRec15: '',
            RRec16: '',
            RRec17: '',
            RRec18: '',
            RRec19: '',
            RRec20: '',
            RRec21: '',
            RRec22: '',
            RRec23: '',
            RRec24: '',
            RRec25: '',
            RRec26: '',
            RRec27: '',
            RRec28: '',
            RRec29: '',
            RRec30: '',
            RRec31: '',
            RRec32: '',
            RRec33: '',
            RRec34: '',
            RRec35: '',
            RRec36: '',
            RRec37: '',
            RRec38: '',
            RRec39: '',
            RRec40: '',
            RRec41: '',
            RRec42: '',
            RRec43: '',
            RRec44: '',
            RRec45: '',
            RRec46: '',
            RRec47: '',
            RRec48: '',
            RRec49: '',
            RRec50: '',
            RRec51: '',
            RRec52: '',
            RRec53: '',
            RRec54: '',
            RRec55: '',
            RRec56: '',
            RRec57: '',
            RRec58: '',
            RRec59: '',
            RRec60: '',
            RRec61: '',
            RRec62: '',
            RRec63: '',
            RRec64: '',
            RRec65: '',
            RRec66: '',
            RRec67: '',
            RRec68: '',
            RRec69: '',
            RRec70: '',
            RRec721: '',
            RRec73: '',
            RRec74: '',
            RRec75: '',
            RRec76: '',
            RRec77: '',
            RRec78: '',
            RRec79: '',
            RRec80: '',
            RRec81: '',
            RRec832: '',
            RRec84: '',
            RRec85: '',
            RRec86: '',
            RRec87: '',
            RRec88: '',
            RRec89: '',
            RRec90: '',
            RRec91: '',
            RRec932: '',
            RRec94: '',
            RRec95: '',
            RRec96: '',
            RRec97: '',
            RRec98: '',
            RRec99: '',
            RRec100: '',
            RRec101: '',
            RRec102: '',
            RRec103: '',
            RRec104: '',
            RRec105: '',
            RRec106: '',
            RRec107: '',
            RRec108: '',
            RRec109: '',
            RRec110: '',
            RRec111: '',
            RRec112: '',
            RRec113: '',
            RRec114: '',
            RRec115: '',
            RRec116: '',
            RRec117: '',
            RRec118: '',
            RRec119: '',
            RRec120: '',
            RRec121: '',
            RRec122: '',
            RRec123: '',
            RRec124: '',
            RRec125: '',
            RRec126: '',
            RRec127: '',
            RRec128: '',
            RRec129: '',
            RRec130: '',

        })


        this.userDetailDayManagerFormGroup = this.formBuilder.group({
            DayManager1: '',
            DayManager2: '',
            DayManager3: '',
            DayManager4: '',
            DayManager5: '',
            DayManager6: '',
            DayManager7: '',
            DayManager8: '',
            DayManager9: '',
            DayManager10: '',
            DayManager11: '',
            DayManager12: '',
            DayManager13: '',
            DayManager14: '',

        })

        this.userDetailMainMenuOpGroup = this.formBuilder.group({
            MainMenuOp1: '',
            MainMenuOp2: '',
            MainMenuOp3: '',
            MainMenuOp4: '',
            MainMenuOp5: '',
            MainMenuOp6: '',
            MainMenuOp7: '',
            MainMenuOp8: '',
            MainMenuOp9: '',
            MainMenuOp10: '',
            MainMenuOp11: '',

        })

        this.userDetailStaffRecGroup = this.formBuilder.group({
            StaffRec1: '',
            StaffRec2: '',
            StaffRec3: '',
            StaffRec4: '',
            StaffRec5: '',
            StaffRec6: '',
            StaffRec7: '',
            StaffRec8: '',
            StaffRec9: '',
            StaffRec10: '',
            StaffRec11: '',
            StaffRec12: '',
            StaffRec13: '',
            StaffRec14: '',
            StaffRec15: '',
            StaffRec16: '',
            StaffRec17: '',
            StaffRec18: '',
            StaffRec19: '',

        })

        this.userDetailClientPortalGroup = this.formBuilder.group({
            ClientPort1: '',
            ClientPort2: '',
            ClientPort3: '',
            ClientPort4: '',
            ClientPort5: '',
            ClientPort6: '',
            ClientPort7: '',
            ClientPort8: '',
            ClientPort9: '',
            ClientPort10: '',
            ClientPort11: '',
            ClientPort12: '',
            ClientPort13: '',
            ClientPort14: '',
            ClientPort15: '',
            ClientPort16: '',
            ClientPortalBooking: '',

        })

        this.userDetailTimeAttendanceGroup = this.formBuilder.group({
            TimeAttendance1: '',
            TimeAttendance2: '',
            TimeAttendance3: '',
            TimeAttendance4: '',
            TimeAttendance5: '',
            TimeAttendance6: '',
            TimeAttendance7: '',
            TimeAttendance8: '',
            TimeAttendance9: '',
            TimeAttendance10: '',
            TimeAttendance11: '',
            TimeAttendance12: '',
            TimeAttendance13: '',
            TimeAttendance14: '',
            TimeAttendance15: '',
            TimeAttendance16: '',
            TimeAttendance17: '',
            TimeAttendance18: '',
            TimeAttendance19: '',
            TimeAttendance20: '',
            TimeAttendance21: '',
            TimeAttendance22: '',
            TimeAttendance23: '',
            TimeAttendance24: '',
            TimeAttendance25: '',
            TimeAttendance26: '',
            TimeAttendance27: '',
            TimeAttendance28: '',
            TimeAttendance29: '',
            TimeAttendance30: '',
            TimeAttendance31: '',
            TimeAttendance32: '',
            TimeAttendance33: '',

        })

        this.attachSubscriptions();
    }

    traccsCodeArray: Array<string> = [];
    attachSubscriptions() {
        //Add/CHangeUsers
        this.userDetailFormGroup.get('LoginGroup').valueChanges
            .pipe(
                distinctUntilChanged(),
                debounceTime(200),
                mergeMap(x => {
                    if (x == 'ADMIN USER' || x == 'SERVICE PROVIDER')
                        return this.listS.GetTraccsStaffCodes();
                    else
                        return this.listS.GetTraccsClientCodes()
                })
            )
            .subscribe(data => {
                this.traccsCodeArray = data
            })
    }

    clear() {

        this.cdcClaim = {
            item: null,
            rate: null
        }

        this.contact = {
            name: '',
            address1: '',
            address2: '',
            suburb: '',
            phone1: '',
            phone2: '',
            fax: '',
            mobile: '',
            email: ''
        };
    }

    branches: any;

    ngOnInit() {

        this.name = this.tabS.getName();

        this.timeS.getusersettings(this.globalS.decode().user).subscribe(data => this.globalS.userSettings = data)

        this.listS.getlistbranches().subscribe(data => this.branches = data)

        this.whatUser.subscribe(whatUser => {
            var exec: string = '';

            if (whatUser === User.Recipient) {
                exec = 'RECIPIENT';
                this.router.navigate(['/admin/recipients'])
            }

            if (whatUser === User.Staff) {
                exec = 'STAFF';
                this.router.navigate(['/admin/staff'])
            }

            // this.execS.updateexecutable({
            //     ExecutableName: exec,
            //     user: this.globalS.decode().user
            // }).subscribe(data =>{
            //     console.log(data)
            // });            
        });

        this.whatConfig.subscribe(data => {

            this.tabNo = data;
            this.listArray = null;
            this.loading = true;

            this.switchS.getData(this.tabNo).subscribe(data => {
                if (!data) {
                    return;
                }
                if (this.tabNo == 7) {
                    this.size = 'xl'
                } else {
                    this.size = ''
                }

                this.modalVariables = data.modalVariables;
                this.templistArray = data.list;
                this.listArray = data.list;

                this.loading = false;
                this.modalList.open();
            })
        })


        this.whatFunding.subscribe(data => {
            switch (data) {
                case 1:
                    this.modalVariables.title = "Debtor Updates And Exports"
                    break;
                case 2:
                    break;
            }
            this.modalFunding.open();
        })
    }

    searchText(text: string) {
        console.log(text);
    }

    onClose() {
        this.ngZone.runOutsideAngular(() => {

            this.listArray = null

        })
    }

    anyVariable: any;

    delete(list: any) {

        // Delete By RecordNumber
        this.switchS.deleteData(list)
            .subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Data Deleted')
                    var indexOf = this.listArray.map(x => x.RecordNumber).indexOf(list.RecordNumber)
                    this.listArray.splice(indexOf, 1);
                }
            })

    }

    edit(list: any) {

        this.process = 1;

        this.switchS.populateData(this.modalVariables, list)
            .subscribe(data => {

                if (this.tabNo == 8) {
                    //Add/ChangeUsers
                    this.modalAddUserDetails.open();

                    this.landS.GetUserRecord(data.display)
                        .subscribe(output => {
                            console.log(output)

                            this.userDetailFormGroup.patchValue({
                                UserName: output.name,
                                Password: output.password,
                                LoginGroup: output.logingroup,
                                TraccsCode: output.traccscode,
                                LoginMode: output.loginmode,
                                HomeBranch: output.homebranch,
                                RecipientEditAccess: output.recipienteditaccess,
                                CAddRecipRec: output.caddreciprec,
                                CaseNotesAreViewOnly: output.casenotesareviewonly,
                                CChangeRecipCode: output.cchangerecipcode,
                                StaffEditAccess: output.staffeditaccess,
                                AccessHR: output.accesshr,
                                Rosters: output.rosters,
                                ChangeMasterRoster: output.changemasterroster,
                                LockOwnRoster: output.lockownroster,
                                AllowReAllocateRoster: output.allowreallocateroster,
                                AllowMasterSave: output.allowmastersave,
                                CRosterOvertime: output.crosterovertime,
                                CRosterWithoutBreaks: output.crosterwithoutBreaks,
                                CRosterWithConflicts: output.crosterwithconflicts,
                                DayManager: output.daymanger,
                                ManualCopyRoster: output.manualcopyroster,
                                AutoCopyRoster: output.autocopyroster,
                                TSheets: output.tsheets,
                                SuggestedTSheets: output.suggestedtsheets,
                                PBCupdates: output.pbcupdates,
                                Listings: output.listings,
                                CDCClaims: output.cdcclaims,
                                AllowTransitionProgram: output.allowtransitionprogram,
                                Financial: output.financial,
                                InvoiceInquiry: output.invoiceinquiry,
                                GenOne: output.genone,
                                GenTwo: output.gentwoo,
                                GenThree: output.genthree,
                                GenFour: output.genfour,
                                GenFive: output.genfive,
                                GenSix: output.gensix,
                                GenSeven: output.seven,
                                GenEight: output.eight,
                                GenNine: output.nine


                            })

                            this.userDetailRecipientRecGroup.patchValue({
                                RRec1: output.rRec1,
                                RRec2: output.rRec2,
                                RRec3: output.rRec3,
                                RRec4: output.rRec4,
                                RRec5: output.rRec5,
                                RRec6: output.rRec6,
                                RRec7: output.rRec7,
                                RRec8: output.rRec8,
                                RRec9: output.rRec9,
                                RRec10: output.rRec10

                            })

                        })

                    return;
                }

                this.selected = data;
                console.log(this.selected)
                this.modalAddEdit.open();
                //this.modalAddUserDetails.open();
            })


    }

    trackByFn(index, item) {
        return index;
    }

    saveProcess(modalVariables: any, inputs: any) {

        // INSERT
        if (this.process == 0) {

            this.switchS.addData(modalVariables, inputs)
                .subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Data Added')
                        this.whatConfig.next(this.tabNo);
                        this.modalAddEdit.close();
                        this.modalAddUserDetails.close()
                    }
                })

        }

        // UPDATE
        if (this.process == 1) {

            this.switchS.updateData(modalVariables, inputs)
                .subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Data Added')
                        this.whatConfig.next(this.tabNo);
                        this.modalAddEdit.close();
                        this.modalAddUserDetails.close()
                        return;
                    }
                    this.globalS.eToast('Error', 'Update Failed')
                })


            // this.listS.updateSql(this.anyVariable)
            //     .subscribe(data => {
            //         if(data){
            //             this.globalS.sToast('Success', 'Data Added')
            //             this.whatConfig.next(this.tabNo);
            //             this.modalAddEdit.close()
            //          }
            //     })


        }

    }

    add() {
        this.clear();
        this.process = 0;

        if (this.tabNo == 8) {
            this.modalAddUserDetails.open();
            return;
        }
        this.modalAddEdit.open();
    }

    populate() {
        if (this.process == 0) {
            this.selected = {}
        }
    }



    insertDataRecord(tableName: any, columns: any, colValues: any): Observable<boolean> {
        return this.listS.insertlist({
            tableName: tableName,
            columns: columns,
            columnValues: colValues
        });
    }

    updateDataRecord(tableName: any, setClause, whereClause: any): Observable<boolean> {
        return this.listS.updatelist({
            tableName: tableName,
            setClause: setClause,
            whereClause: whereClause
        });
    }

    deleteDataRecord(tableName: any, whereClause: any): Observable<boolean> {
        return this.listS.deletelist({
            tableName: tableName,
            whereClause: whereClause
        });
    }

    fastlistArray: Array<any>
    gwapo(data: any) {
        // if(data.length == 0) return;
        this.fastlistArray = data;
        // this.calculateTotalApprovedShifts(this.timesheets);
    }



}
