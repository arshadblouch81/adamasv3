using System;

namespace Adamas.Models.Modules
{
    public class DebtorBilling
    {
        public string Group { get; set; }
        public string InvoiceNumber { get; set; }
        public string BatchNumber { get; set; }
        public string Branches { get; set; }
        public string Programs { get; set; }
        public string Categories { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string InvoiceType { get; set; }
        public string sepInvRecipient { get; set; }
        public string OperatorID { get; set; }
        public DateTime CurrentDateTime { get; set; }

    }
}