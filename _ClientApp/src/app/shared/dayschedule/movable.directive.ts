import { Directive, HostListener, HostBinding, ElementRef, NgZone, Renderer2 } from '@angular/core'
import { DropMarkDirective } from './dragdrop.directive'

import { DomSanitizer, SafeStyle } from '@angular/platform-browser'

interface Position{
    x?: number,
    y: number
}

@Directive({
    selector: '[movable]'
})

export class MovableDirective extends DropMarkDirective {

    position: Position = { x:0, y:0 }
    private startPosition: Position

    dragStartBoolean: boolean = false;

    constructor(
        public el: ElementRef,
        public ngZone: NgZone,
        private sanitizer: DomSanitizer, 
        private renderer: Renderer2,
        public element: ElementRef){
         super(ngZone);
    }

    @HostBinding('style.transform') get hostElementTransform(): SafeStyle {        
        var haha = Math.floor(this.position.y/28) * 28;
        if(this.dragStartBoolean){ 
             this.ngZone.runOutsideAngular(() => {               
                if(this.position.y !== 0 && this.position.y % 28 >= -1 && this.position.y % 28  <= 20)            
                    return this.sanitizer.bypassSecurityTrustStyle(`translateX(${this.position.x}px) translateY(${haha}px)`)  
            });          
        }
        return this.sanitizer.bypassSecurityTrustStyle(`translateX(${this.position.x}px) translateY(${haha}px)`)      
    }


     @HostListener('dragStart',['$event']) onDragStart(event: PointerEvent){
        this.ngZone.runOutsideAngular(() => {
            this.dragStartBoolean = true;
            this.startPosition = {
                //x: event.clientX - this.position.x,
                y: event.clientY - this.position.y
            }       
        })
    }

    @HostListener('dragMove',['$event']) onDragMove(event: PointerEvent){   
        // this.position.x = event.clientX - this.startPosition.x
        this.ngZone.runOutsideAngular(() => {
            this.position.y = event.clientY - this.startPosition.y       
        })
    }

    @HostListener('dragEnd',['$event']) onDragEnd(){
        this.ngZone.runOutsideAngular(() => {
            this.dragStartBoolean = false;  
        })
    }



}