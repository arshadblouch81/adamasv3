import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
    styles: [`
    nz-table{
        margin-top:20px;
    }
    .ant-checkbox-wrapper{
        display:flex;
    }
    .ant-checkbox-wrapper ::ngdeep  span{
        display: flex;
        align-items: center;
        font-size: 12px;
    }
    .options > div{
        margin-bottom:13px;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    .selecteditem{
        background-color: #85B9D5;
        color: white;
    }
    nz-table th{
        font-weight: 600;
        font-family: 'Segoe UI';
        font-size: 14px;
        border: 1px solid #f3f3f3;
    }
    tbody  tr:nth-child(odd) {
            background-color: #E9F7FF;
      }
      tbody  tr:nth-child(even) {
            background-color: #fff;
      }
    `],
    templateUrl: './incident.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffIncidentAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    incidentForm: FormGroup;
    tableData: Array<any>;
    loading: boolean = false;
    postLoading: boolean = false;
    
    incidentOpen: boolean = false;
    
    current: number = 0;
    
    incidentTypeList: Array<any> = []
    incidentRecipient: any;
    operation: any; 
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
     
    selectedRow  : number = 0;
    activeRowData:any;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private printS:PrintService,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'incident')) {
                    this.user = data;
                    this.search(this.user);
                }
            });
        }
        
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked();
            if(this.user){
                this.search(this.user);
                this.buildForm();
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            this.incidentForm = this.formBuilder.group({
                incidentType: ''
            });
        }
        
        search(user: any = this.user) {
            this.cd.reattach();
            this.loading = true;
            this.timeS.getincidents(user.code).subscribe(data => {
                this.tableData = data;
                this.originalTableData = data;
                this.selectedRow=0;
                this.loading = false;
                this.cd.detectChanges();
            });
            
            this.incidentRecipient = this.user;
        }
        
        selectedItemGroup(data:any, i:number){
            this.selectedRow=i;
            this.activeRowData=data;
        }

        trackByFn(index, item) {
            return item.id;
        }  
        
        showAddModal() {        
            const { agencyDefinedGroup, code, id, sysmgr, view } = this.user;
            
            this.operation = {
                process: 'ADD'
            }        
            
            this.incidentOpen = !this.incidentOpen;
        }
        
        showEditModal() {
            const { agencyDefinedGroup, code, id, sysmgr, view } = this.user;
            
            var newPass = {
                agencyDefinedGroup: agencyDefinedGroup,
                code: code,
                id: id,
                sysmgr: sysmgr,
                view: view,
                operation: 'UPDATE',
                recordNo: this.tableData[this.selectedRow].recordNumber
            }
            
            this.operation = {
                process: 'UPDATE'
            }
            
            
            this.incidentRecipient = newPass;


            this.incidentOpen = !this.incidentOpen;
        
        }
        
        reload(data: any){
            this.search(this.user);
        }
        
        
        delete(){
            const { recordNumber } = this.tableData[this.selectedRow];
            this.timeS.deleteincident(recordNumber).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Deleted Successfully');
                        this.search(this.user);
                    }
                });
        }
        closed(){
            const { recordNumber } = this.tableData[this.selectedRow];
            this.timeS.UpdateIncidentstatus(recordNumber).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Closed Successfully');
                        this.search(this.user);
                    }
                });
        }
        save(){
                
        }
        handleOkTop() {
            this.generatePdf();
            this.tryDoctype = ""
            this.pdfTitle = ""
        }
        handleCancelTop(): void {
            this.drawerVisible = false;
            this.pdfTitle = ""
        }
        generatePdf(){            
            
            //Mufeed 16 April 2024
    
            var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid; 
    
            this.drawerVisible = true;
            
            this.loading = true;
            
            fQuery = "SELECT Status ,CONVERT(varchar, [Date],105) as Date, [Type] , ShortDesc AS Description, CurrentAssignee AS [Assigned To] FROM IM_Master IM INNER JOIN Staff S ON S.[UniqueID] = IM.[PersonID] WHERE S.[AccountNo] = '"+this.user.code+"' ORDER BY STATUS DESC, DATE DESC ";
                                
                                       
            txtTitle = "Incident Register";       
            Rptid =   "8WTerq14eNFSAnz0"  
    
            //console.log(fQuery)
            const data = {                    
                "template": { "_id": Rptid },                                
                "options": {
                    "reports": { "save": false },                
                    "sql": fQuery,
                    "Criteria": lblcriteria,
                    "userid": this.tocken.user,
                    "txtTitle": txtTitle,
                    "Extra": this.user.code,                                                                
                }
            }
            this.loading = true;
            this.drawerVisible = true;         
                this.printS.printControl(data).subscribe((blob: any) => {
                this.pdfTitle = txtTitle+".pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
                });
                return;      
        }



    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Status',
        value: 'status'
    },{
        key: 'Date',
        value: 'date'
    },{
        key: 'Type',
        value: 'type'
    },{
        key: 'Description',
        value: 'description'
    },{
        key: 'Assigned To',
        value: 'assigned'
    }];
    
    
    

    dragDestination = [    
        'Status',   
        'Date',
        'Type',
        'Description',
        'Assigned To'
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
    
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        // console.log("staff "+ permissoons.length );
        return permissoons.charAt(index-1);
    }
}