import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { TimeSheetService } from '@services/index'

@Component({
    selector: 'dm-extrainfo',
    templateUrl: './dm-extrainfo.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DmExtraInfoComponent),
            multi: true
        }
    ]
})

export class DmExtraInfoComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    results: "";

    constructor(
        private timeS: TimeSheetService
    ){ }

    writeValue(value: any){
        if(value != null) {
            console.log(value)
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        this.timeS.getextrainformation(user.recordno)
            .subscribe(data => this.results = data.notes);
    }
}