using System.Threading.Tasks;
using Adamas.Models.Modules;
using System;
using Adamas.Models.Tables;

namespace Adamas.DAL
{
    public interface ITimesheetService
    {
        Task<dynamic> PostFundingAsync(dynamic obj);
        Task<SysTable> GetSysTable();
    }
}