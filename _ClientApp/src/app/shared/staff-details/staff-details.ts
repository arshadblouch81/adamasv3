
import {forkJoin,  Observable } from 'rxjs';

import {mergeMap} from 'rxjs/operators';
import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { StaffService, ClientService, TimeSheetService, GlobalService } from '@services/index'
import { RemoveFirstLast } from '@pipes/pipes';

interface User {
    Addresses: Array<any>,
    Contacts: Array<any>,
    Infos: {
        Miscellaneous: string,
        Preference: string
    }
}

@Component({
    selector: 'staff-details',
    templateUrl: './staff-details.html',
    styles: [`
        .detail-container{
            width: 100%; 
            background: #fbfbfb; 
            padding: 10px;
        }
        nav{
            overflow-y: auto;
            max-height: 4rem;
        }
        .detail-data{
            background: #fff; 
            border: 1px solid #efefef; 
            margin-bottom:2px;
            padding: 3px;
            display:flex;
        }
        .detail-data > div:first-child{
            flex: 1;
            font-weight: 600;
            color: #007cbb;
        }
        .detail-data > div:last-child{
            flex: 3;
        }
        .detail-data > div:last-child i{
            float:right;
            color: #18a1ff;
            background: #fff;
        }
        .infos-container{
            display: flex;
            flex-direction: column;
        }
        textarea{
            box-shadow: none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
        }
    `],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => StaffDetailsComponent),
        multi: true
    }]
})

export class StaffDetailsComponent implements ControlValueAccessor {
    onChange = (_: any) => { };
    onTouch: () => {};

    user: User;

    constructor(
        private staffS: StaffService,
        private timeS: TimeSheetService,
        private clientS: ClientService,
        private globalS: GlobalService
    ){

    }

    writeValue(value: any){
        if(value != null){
            this.search(value);
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

     // -----------------------------------------

    search(user: any){
        this.staffS.getprofile(user.name).pipe(
            mergeMap(data => {     
                return this.getUserData(data.uniqueID);
            })).subscribe(data => {
                 this.user = {
                     Addresses: data[0].map(x => {
                         return {
                            address1: x.address1,
                            address2: x.address2,
                            description: new RemoveFirstLast().transform(x.description),
                            personID: x.personID,
                            postCode: x.postCode,
                            primaryAddress: x.primaryAddress,
                            recordNumber: x.recordNumber
                         }
                     }),
                     Contacts: data[1].map(x => {
                        return {
                            detail: x.detail,
                            personID: x.personID,
                            primaryPhone: x.primaryPhone,
                            recordNumber: x.recordNumber,
                            type: new RemoveFirstLast().transform(x.type)
                        }
                     }),
                     Infos: {
                         Miscellaneous: this.globalS.filterFontLiterals(data[2].miscellaneous),
                         Preference: data[2].preferences
                     }
                 }     
          });
    }

    getUserData(code: any){
        return forkJoin([
            this.clientS.getaddress(code),
            this.clientS.getcontacts(code),
            this.timeS.getstaffdetails(code)
        ]);
    }
}