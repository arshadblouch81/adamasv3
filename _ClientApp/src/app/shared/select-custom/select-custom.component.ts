import { Component, Input, OnInit,forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import * as _ from 'lodash';
@Component({
  selector: 'select-custom',
  templateUrl: './select-custom.component.html',
  styleUrls: ['./select-custom.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectCustomComponent),
      multi: true
    }
  ]
})
export class SelectCustomComponent implements OnInit, ControlValueAccessor {

  @Input() lists: Array<any>;
  mapList: Array<any>;

  onChange = (data: any) => {};
  onTouched = () => {};

  value: any;

  constructor() { }

  ngOnInit() {  
    //this.mapList = this.lists.map((x: string) => x.toUpperCase());
  }

  writeValue(value: any): void {
      this.searchAndArrangeList(value);
  }

  onClickHandler(data: any){
    this.onChange(data);
  }

  searchAndArrangeList(value: string): void {  
    if(!this.lists)  return;

    const cloneArray  = Object.assign([], this.lists).map(x => x.toUpperCase());

    if(value != null){
      var ifFound = cloneArray.find((x: string) => x.toUpperCase() === value.toUpperCase());

      if(!ifFound){
        cloneArray.push(value);
      }
    }
    
    this.mapList = cloneArray.sort().filter(x => x != '');
    this.value = value || '';
  }

  registerOnChange(fn: (rating: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

}
