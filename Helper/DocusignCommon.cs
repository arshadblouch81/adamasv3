﻿using System.ComponentModel;

namespace Adamas.Helper
{
    public enum ExamplesAPIType
    {
        [Description("reg")]
        Rooms = 0,

        [Description("eg")]
        ESignature = 1,

        [Description("ceg")]
        Click = 2,

        [Description("meg")]
        Monitor = 3,

        [Description("aeg")]
        Admin = 4,
    }
}
