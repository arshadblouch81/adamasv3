using System;
namespace Adamas.Models.Modules{
    public class RosterInfo {
        public string RecordNo { get; set; }
        public bool IsMaster { get; set; }
        public string Roster_Date { get; set; }
        public string End_Time { get; set; }
        public string Shift_Start { get; set; }
        public string Shift_End { get; set; }
        public string RosterType { get; set; }
        public string Client { get; set; }
        public string Start_Time { get; set; }
        public string DayNo { get; set; }
        public string BlockNo { get; set; }
        public string Duration { get; set; }
        public string ClientCode { get; set; }
        public string CarerCode { get; set; }
        public string Carer_Phone { get; set; }
        public string ServiceType { get; set; }
        public string ServiceSetting { get; set; }
        public string UBDRef { get; set; }
        public string Program { get; set; }
        public string Type { get; set; }
        public string Notes { get; set; }
        public string Tagged { get; set; }   
        public string Service_Description { get; set; }
        public TimeSheetBill Bill { get; set; }
        public bool Approved { get; set; }  
        public string BilledTo { get; set; } 
        public string Anal { get; set; }
        public string YearNo { get; set; }
        public string MonthNo { get; set; }
        public string Status { get; set; }
        public DateTime Date_Timesheet { get; set; }
        public bool Transferred { get; set; }
        public bool GroupActivity { get; set; }
        public string HACCType { get; set; }
        public string DATASETClient { get; set; }
        public string Creator { get; set; }
        public string Admissiontype { get; set; }
        public string AdminExplainCode { get; set; }
        public bool TravleChargeable { get; set; }
        public string ManualMode { get; set; }
        public string AutoMode { get; set; }
        public string BrokerageOrganisation { get; set; }
        public bool AGroupBasedActivity { get; set; }  
        public string Roster_Notes { get; set; }
        public string RosterGroup { get; set; }
        public string Grid_row { get; set; }
        public string Grid_Col { get; set; }
        public string Editer { get; set; }
        public string BatchNo { get; set; }
        public string TimesheetNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string Ros_pay_batch { get; set; }
        public string Attendees { get; set; }    
        public string DatasetGroup { get; set; }
        public string FundingSource { get; set; }  
        public string OutletID { get; set; }
        public string DATASET { get; set; }
        public string DischargeReasonType { get; set; }
        public string NRCP_TypeOfAssitance { get; set; }
        public DateTime Date_Entered { get; set; }  
        public DateTime Date_Last_Mod { get; set; }
        public bool HasServiceNotes { get; set; }
        public string Link { get; set; }
        // public string BillUnit { get; set; }
        // public string BillRate { get; set; }
        // public string BillQty { get; set; }
        // public string Pay_Unit { get; set; }
        // public string Pay_Quantity { get; set; }
        // public string Pay_Rate { get; set; }
         public PayType payType { get; set; }
        public Pay pay { get; set; }
      
    }
}