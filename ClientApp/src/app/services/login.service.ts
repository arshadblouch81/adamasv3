import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

import { TokenRefreshValidation } from '@services/global.service'
 
const login: string = `api/login`;
const global: string = `api/global`;

@Injectable()
export class LoginService {
    constructor(
        public http: HttpClient,
        public auth: AuthService
    ) { }

    getcurrentversion(): Observable<any>{
        return this.http.get('../../assets/version.txt', { responseType: 'text'});
    }

    getcurrentuser() {
        return this.http.get(`${global}/current-user`)
    }

    getuserwithmsaccount(data:any) {
        return this.http.get(`${login}/ms-user/${data}`)
    
    }

    changepassword(data: any){
        return this.auth.put(`${login}/change-password`, data);
    }

    testservice() {
        return this.http.get(`${login}/test-service`)
    }

    login(data: any, bypass: boolean = false) {
        return this.auth.post(`${login}`, data);
    }

    logout(uid: string) {
        return this.auth.post(`${login}/logout`, JSON.stringify(uid));
    }

    refreshToken(token: TokenRefreshValidation) {
        return this.auth.post(`${login}/refresh-tokens`, token);
    }

    serialize(obj: any) {
        let params: URLSearchParams = new URLSearchParams();
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                var element = obj[key];
                params.set(key, element);
            }
        }
        return params;
    }
    getWindowsUsername() {
        return this.http.get(`${login}/win-username`)
      }
    
}