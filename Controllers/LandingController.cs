using System;
using System.Globalization;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using Staff = Adamas.Models.Modules.Staff;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    public class LandingController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
       
        public LandingController(
            DatabaseContext context,
            IGeneralSetting setting
        ){
            _context = context;
            GenSettings = setting;
        }
        // Add/ChangeUsers
        [HttpGet("GetUserRecord/{name}")]
        public async Task<IActionResult> GetUserRecord(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection){
                using(SqlCommand cmd = new SqlCommand(@"SELECT 
                    CASE 
                        WHEN Recipients = 999 THEN 'LEVEL 6'
                        WHEN Recipients between 951 AND 998 THEN 'LEVEL 5'
                        WHEN Recipients between 901 AND 950 THEN 'LEVEL 4'
                        WHEN Recipients between 201 AND 900 THEN 'LEVEL 3'
                        WHEN Recipients between 101 AND 200 THEN 'LEVEL 2'
                        WHEN Recipients between 1 AND 100 THEN 'LEVEL 1'
                        ELSE 'NONE'
                    END AS RecipientEditAccess,
                    CASE 
                        WHEN Staff = 999 THEN 'LEVEL 4'
                        WHEN Staff between 301 AND 998 THEN 'LEVEL 3'
                        WHEN Staff between 251 AND 300 THEN 'LEVEL 2'
                        WHEN Staff between 1 AND 250 THEN 'LEVEL 1'
                        ELSE 'NONE'
                    END AS StaffEditAccess,
                    CASE 
                        WHEN Roster = 999 THEN 'LEVEL 7'
                        WHEN Roster between 501 AND 998 THEN 'LEVEL 6'
                        WHEN Roster between 251 AND 500 THEN 'LEVEL 5'
                        WHEN Roster between 201 AND 250 THEN 'LEVEL 4'
                        WHEN Roster between 121 AND 200 THEN 'LEVEL 3'
                        WHEN Roster between 101 AND 120 THEN 'LEVEL 2'
                        WHEN Roster between 1 AND 100 THEN 'LEVEL 1'
                        ELSE 'NONE'
                    END AS RosterEditAccess,
                    CASE 
                        WHEN Daymanager = 999 THEN 'LEVEL 7'
                        WHEN Daymanager between 501 AND 998 THEN 'LEVEL 6'
                        WHEN Daymanager between 251 AND 500 THEN 'LEVEL 5'
                        WHEN Daymanager between 201 AND 250 THEN 'LEVEL 4'
                        WHEN Daymanager between 121 AND 200 THEN 'LEVEL 3'
                        WHEN Daymanager between 101 AND 120 THEN 'LEVEL 2'
                        WHEN Daymanager between 1 AND 100 THEN 'LEVEL 1'
                        ELSE 'NONE'
                    END AS DaymanagerEditAccess,
                    CASE WHEN ISNULL(Timesheet, 0) <> 0 THEN 1 ELSE 0 END AS Timesheet,
                    CASE WHEN ISNULL([Statistics], 0) <> 0 THEN 1 ELSE 0 END AS [Statistics],
                    CASE WHEN ISNULL(Financial, 0) <> 0 THEN 1 ELSE 0 END AS Financial,
                    CASE WHEN ISNULL(System, 0) <> 0 THEN 1 ELSE 0 END AS System,
                    CASE WHEN ISNULL(SuggestedTimesheets, 0) <> 0 THEN 1 ELSE 0 END AS SuggestedTimesheets,
                    CASE WHEN ISNULL(InvoiceEnquiry, 0) <> 0 THEN 1 ELSE 0 END AS InvoiceEnquiry,
                    CASE WHEN ISNULL(ManualRosterCopy, 0) <> 0 THEN 1 ELSE 0 END AS ManualRosterCopy, 
                    CASE WHEN ISNULL(AutoCopyRoster, 0) <> 0 THEN 1 ELSE 0 END AS AutoCopyRoster, 
                    CASE WHEN ISNULL(TimesheetUpdate, 0) <> 0 THEN 1 ELSE 0 END AS TimesheetUpdate, 
                    * FROM UserInfo WHERE Name = @name",(SqlConnection) conn)){
                    cmd.Parameters.AddWithValue("@name", name);
                    await conn.OpenAsync();

                    using(SqlDataReader rd = cmd.ExecuteReader())
                    {
                        dynamic result = null;
                        if(rd.Read())
                        
                        {
                            result = new {
                                name = GenSettings.Filter(rd["Name"]),
                                password =  GenSettings.Filter(rd["Password"]),
                                logingroup =  GenSettings.Filter(rd["UserType"]),
                                traccscode =  GenSettings.Filter(rd["StaffCode"]),
                                loginmode =  GenSettings.Filter(rd["LoginMode"]),
                                homebranch =  GenSettings.Filter(rd["HomeBranch"]),
                                recipienteditaccess =  GenSettings.Filter(rd["recipienteditaccess"]),
                                caddreciprec =  GenSettings.Filter(rd["AddNewRecipient"]),
                                casenotesareviewonly = GenSettings.Filter(rd["CaseNotesReadOnly"]),
                                cchangerecipcode = GenSettings.Filter(rd["CanChangeClientCode"]),
                                staffeditaccess = GenSettings.Filter(rd["StaffEditAccess"]),
                                accesshr = GenSettings.Filter(rd["AccessHRInfo"]),
                                rosters = GenSettings.Filter(rd["RosterEditAccess"]),
                                dayManager = GenSettings.Filter(rd["DaymanagerEditAccess"]),
                                changemasterroster = GenSettings.Filter(rd["ChangeMasterRoster"]),
                                lockownroster = GenSettings.Filter(rd["OwnRosterOnly"]),
                                allowreallocateroster = GenSettings.Filter(rd["AllowRosterReallocate"]),
                                allowmastersave = GenSettings.Filter(rd["AllowMasterSaveAs"]),
                                crosterovertime = GenSettings.Filter(rd["CanRosterOvertime"]),
                                crosterwithoutbreaks = GenSettings.Filter(rd["CanRosterBreakless"]),
                                crosterwithconflicts = GenSettings.Filter(rd["CanRosterConflicts"]),
                                manualcopyroster = GenSettings.Filter(rd["ManualRosterCopy"]),
                                autocopyroster = GenSettings.Filter(rd["AutoCopyRoster"]),
                                tsheets = GenSettings.Filter(rd["TimesheetUpdate"]),
                                suggestedtsheets = GenSettings.Filter(rd["SuggestedTimesheets"]),
                                pbcupdates = GenSettings.Filter(rd["MMPriceUpdates"]),
                                listings = GenSettings.Filter(rd["Statistics"]),
                                cdcclaims = GenSettings.Filter(rd["AccessCDC"]),
                                allowtransitionprogram = GenSettings.Filter(rd["AllowProgramTransition"]),
                                financial = GenSettings.Filter(rd["Financial"]),
                                invoiceinquiry = GenSettings.Filter(rd["InvoiceEnquiry"]),
                                genone = GenSettings.Filter(rd["LimitProgramLookups"]),
                                gentwo = GenSettings.Filter(rd["LimitServiceLookups"]),
                                genthree = GenSettings.Filter(rd["AutoAdminTime"]),
                                genfour = GenSettings.Filter(rd["KeepOriginalAsImport"]),
                                genfive = GenSettings.Filter(rd["ReportPreview"]),
                                gensix = GenSettings.Filter(rd["LimitStaffLookups"]),
                                genseven = GenSettings.Filter(rd["CanMoveImportedDocuments"]),
                                geneight = GenSettings.Filter(rd["ShowAllProgramsOnActivate"]),
                                gennine = GenSettings.Filter(rd["AllowTypeAhead"]),
                                rRec1 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[1]) == "1" ? true : false,
                                rRec2 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[2]) == "1" ? true : false,
                                rRec3=   Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[3]) == "1" ? true : false,
                                rRec4=   Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[4]) == "1" ? true : false,
                                rRec5 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[5]) == "1" ? true : false,
                                rRec6 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[6]) == "1" ? true : false,
                                rRec7 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[7]) == "1" ? true : false,
                                rRec8 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[8]) == "1" ? true : false,
                                rRec9 =  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[9]) == "1" ? true : false,
                                rRec10 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[10]) == "1" ? true : false,
                                rRec11 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[11]) == "1" ? true : false,
                                rRec12 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[12]) == "1" ? true : false,
                                rRec13 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[13]) == "1" ? true : false,
                                rRec14 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[14]) == "1" ? true : false,
                                rRec15 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[15]) == "1" ? true : false,
                                rRec16 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[16]) == "1" ? true : false,
                                rRec17 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[17]) == "1" ? true : false,
                                rRec18 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[18]) == "1" ? true : false,
                                rRec19 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[19]) == "1" ? true : false,
                                rRec20 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[20]) == "1" ? true : false,
                                rRec21 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[21]) == "1" ? true : false,
                                rRec22 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[22]) == "1" ? true : false,
                                rRec23 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[23]) == "1" ? true : false,
                                rRec24 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[24]) == "1" ? true : false,
                                rRec25 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[25]) == "1" ? true : false,
                                rRec26 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[26]) == "1" ? true : false,
                                rRec27 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[27]) == "1" ? true : false,
                                rRec28 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[28]) == "1" ? true : false,
                                rRec29 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[29]) == "1" ? true : false,
                                rRec30 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[30]) == "1" ? true : false,
                                rRec31 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[31]) == "1" ? true : false,
                                rRec32 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[32]) == "1" ? true : false,
                                rRec33 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[33]) == "1" ? true : false,
                                rRec34 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[34]) == "1" ? true : false,
                                rRec35 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[35]) == "1" ? true : false,
                                rRec36 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[36]) == "1" ? true : false,
                                rRec37 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[37]) == "1" ? true : false,
                                rRec38 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[38]) == "1" ? true : false,
                                rRec39 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[39]) == "1" ? true : false,
                                rRec40 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[40]) == "1" ? true : false,
                                rRec41 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[41]) == "1" ? true : false,
                                rRec42 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[42]) == "1" ? true : false,
                                rRec43 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[43]) == "1" ? true : false,
                                rRec44 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[44]) == "1" ? true : false,
                                rRec45 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[45]) == "1" ? true : false,
                                rRec46 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[46]) == "1" ? true : false,
                                rRec47 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[47]) == "1" ? true : false,
                                rRec48 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[48]) == "1" ? true : false,
                                rRec49 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[49]) == "1" ? true : false,
                                rRec50=  Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[50]) == "1" ? true : false,
                                rRec51 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[51]) == "1" ? true : false,
                                rRec52 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[52]) == "1" ? true : false,
                                rRec53 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[53]) == "1" ? true : false,
                                rRec54 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[54]) == "1" ? true : false,
                                rRec55 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[55]) == "1" ? true : false,
                                rRec56 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[56]) == "1" ? true : false,
                                rRec57 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[57]) == "1" ? true : false,
                                rRec58 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[58]) == "1" ? true : false,
                                rRec59 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[59]) == "1" ? true : false,
                                rRec60 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[60]) == "1" ? true : false,
                                rRec61 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[61]) == "1" ? true : false,
                                rRec62 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[62]) == "1" ? true : false,
                                rRec63 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[63]) == "1" ? true : false,
                                rRec64 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[64]) == "1" ? true : false,
                                rRec65 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[65]) == "1" ? true : false,
                                rRec66 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[66]) == "1" ? true : false,
                                rRec67 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[67]) == "1" ? true : false,
                                rRec68 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[68]) == "1" ? true : false,
                                rRec69 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[69]) == "1" ? true : false,
                                rRec70 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[70]) == "1" ? true : false,
                                rRec71 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[71]) == "1" ? true : false,
                                rRec72 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[72]) == "1" ? true : false,
                                rRec73 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[73]) == "1" ? true : false,
                                rRec74 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[74]) == "1" ? true : false,
                                rRec75 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[75]) == "1" ? true : false,
                                rRec76 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[76]) == "1" ? true : false,
                                rRec77 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[77]) == "1" ? true : false,
                                rRec78 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[78]) == "1" ? true : false,
                                rRec79 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[79]) == "1" ? true : false,
                                rRec80 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[80]) == "1" ? true : false,
                                rRec81 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[81]) == "1" ? true : false,
                                rRec82 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[82]) == "1" ? true : false,
                                rRec83 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[83]) == "1" ? true : false,
                                rRec84 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[84]) == "1" ? true : false,
                                rRec85 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[85]) == "1" ? true : false,
                                rRec86 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[86]) == "1" ? true : false,
                                rRec87 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[87]) == "1" ? true : false,
                                rRec88 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[88]) == "1" ? true : false,
                                rRec89 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[89]) == "1" ? true : false,
                                rRec90 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[90]) == "1" ? true : false,
                                rRec91 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[91]) == "1" ? true : false,
                                rRec92 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[92]) == "1" ? true : false,
                                rRec93 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[93]) == "1" ? true : false,
                                rRec94 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[94]) == "1" ? true : false,
                                rRec95 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[95]) == "1" ? true : false,
                                rRec96 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[96]) == "1" ? true : false,
                                rRec97 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[97]) == "1" ? true : false,
                                rRec98 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[98]) == "1" ? true : false,
                                rRec99 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[99]) == "1" ? true : false,
                                rRec100 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[100]) == "1" ? true : false,
                                rRec101 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[101]) == "1" ? true : false,
                                rRec102 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[102]) == "1" ? true : false,
                                rRec103 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[103]) == "1" ? true : false,
                                rRec104 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[104]) == "1" ? true : false,
                                rRec105 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[105]) == "1" ? true : false,
                                rRec106 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[106]) == "1" ? true : false,
                                rRec107 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[107]) == "1" ? true : false,
                                rRec108 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[108]) == "1" ? true : false,
                                rRec109 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[109]) == "1" ? true : false,
                                rRec110 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[110]) == "1" ? true : false,
                                rRec111 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[111]) == "1" ? true : false,
                                rRec112 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[112]) == "1" ? true : false,
                                rRec113 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[113]) == "1" ? true : false,
                                rRec114 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[114]) == "1" ? true : false,
                                rRec115 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[115]) == "1" ? true : false,
                                rRec116 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[116]) == "1" ? true : false,
                                rRec117 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[117]) == "1" ? true : false,
                                rRec118 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[118]) == "1" ? true : false,
                                rRec119 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[119]) == "1" ? true : false,
                                rRec120 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[120]) == "1" ? true : false,
                                rRec121 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[121]) == "1" ? true : false,
                                rRec122 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[122]) == "1" ? true : false,
                                rRec123 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[123]) == "1" ? true : false,
                                rRec124 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[124]) == "1" ? true : false,
                                rRec125 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[125]) == "1" ? true : false,
                                rRec126 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[126]) == "1" ? true : false,
                                rRec127 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[127]) == "1" ? true : false,
                                rRec128 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[128]) == "1" ? true : false,
                                rRec129 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[129]) == "1" ? true : false,
                                rRec130 = Convert.ToString(GenSettings.Filter(rd["RecipientRecordView"])[130]) == "1" ? true : false,
                            };
                        
                        }
                        return Ok(result);
                    }
                }
            }
        }



    
    
    }
}
