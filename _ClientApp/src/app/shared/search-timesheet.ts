import { Component, EventEmitter, Input, Output, ElementRef, OnInit } from '@angular/core';

import { TimeSheetService, GlobalService } from '../services/index';

const placeholder = {
    staff: 'Search Staff..',
    recipient: 'Search Recipient..'
}
@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'search-timesheet',
    templateUrl: './search-timesheet.html',
    styles:[
`
    div{
        width:100%;
        position:relative;
    }
    input[type=text]{
        padding-bottom:10px;
    }
    i.search{
        position: absolute;
        right: 38px;
        top: 4px;
        font-size:20px;
        padding:3px;
        border-radius: 50px;
    }
    i.swap {
        position: absolute;
        right: 11px;
        top: 0;
        font-size:20px;
        padding:3px;
        border-radius: 50px;
    }
    i:hover{
        background: #ececec;
        border: 1px solid #dadada;
        cursor:pointer;
    }
    ul{  
        z-index: 5;
        background-color: #fff;
        box-shadow: 0 2px 4px rgba(0,0,0,0.2);
        width: 100%;
        position: absolute;
        overflow: auto;
        max-height: 15em;
        margin: 0.1rem 0;
        list-style:none;
    }
    li{
        padding:3px 5px;
        cursor:pointer;
    }
    li:hover{
        background: #a7bdd0;
        color: #fff;
    }
`

    ]
})

export class SearchTimesheet implements OnInit {
    searchString: string = '';
    show: boolean = false;

    placeholder: string = placeholder.staff;
    items: Array<any>;
    loading: boolean = false;

    @Output() selected = new EventEmitter<any>();

    constructor(
        private elem: ElementRef,
        private timeS: TimeSheetService,
        private globalS: GlobalService
    ){

    }

    ngOnInit(){
        this.selected.emit({
            option: this.placeholder == placeholder.staff ?
                1 : 0,
            data: ''
        });
        this.search();
    }

    onClick(event: Event){
        if(!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }

    swap(){
        this.searchString = '';
        this.show = false;
        this.placeholder = this.placeholder == placeholder.staff ?
            placeholder.recipient : placeholder.staff;
        this.search();
    }

    search(){
        let token = this.globalS.decode();
        this.loading = true; 
        if(this.placeholder == placeholder.staff){
            this.timeS.getfilteredstaff({
                User: token.user,
                SearchString: this.searchString,
                IncludeActive: false,
                Status: ''
            }).subscribe(data => { this.loading = false; this.items = data });
        } else if (this.placeholder == placeholder.recipient){
             this.timeS.getfiltteredrecipient({
                User: token.user,
                SearchString: this.searchString,
                IncludeActive: false,
                Status: ''
             }).subscribe(data => { this.loading = false; this.items = data });
        }
    }

    pick(data: any){
        this.show = false;
        this.searchString = data;
        this.selected.emit({
            option: this.placeholder == placeholder.staff ?
                1 : 0,
            data: data.trim()
        });
    }
}