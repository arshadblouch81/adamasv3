

import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {
    GlobalService,
    StaffService,
    nodes,
    checkOptionsOne,
    ShareService,
    ListService,
    TimeSheetService,
    PrintService,
    sampleList
  } from '@services/index';
import { NzFormatEmitEvent } from 'ng-zorro-antd';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
  selector: 'app-recipient-search',
  templateUrl: './recipient-search.html',
  styleUrls: ['./recipient-search.css']
})
export class RecipientSearch implements OnInit {

 findModalOpen: boolean;
@Input() bookingData = new Subject<any>();
@Output() searchDone:EventEmitter<any>= new EventEmitter();
  private unsubscribe: Subject<void> = new Subject();
  searched:Subject<boolean>= new Subject()
  current: number = 0;

  allBranches: boolean = true;
  allBranchIntermediate: boolean = false;

  allProgarms: boolean = true;
  allprogramIntermediate: boolean = false;

  allCordinatore: boolean = true;
  allCordinatorIntermediate: boolean = false;

  allcat: boolean = true;
  allCatIntermediate: boolean = false;
  extendedSearch: any;
  tocken:any;
  allChecked: boolean = true;
  indeterminate: boolean = false;
  QuicksearchOptions:Array<any> = [];
  QuicksearchValue:any;
 casemanagers: any;
  categories: any;
  programsList: any;
  branchesList: any;
  filters: any;
  dateFormat: string = 'dd/MM/yyyy';
  quicksearch: any;
  ExpandAlertsForm:FormGroup;
ExpandAlerts: boolean;
categoriesList: any;
cariteriaList: Array<any> = [];
nodelist: Array<any> = [];
sampleList: Array<any> = [];
loading:boolean = false;
checkOptionsOne = checkOptionsOne;
filteredResult: any;
selectedTypes: any;
selectedbranches: any[];
testcheck: boolean = false;

selectedPrograms: any;
selectedCordinators: any;
selectedCategories: any;
selectedRecipient:any;
user:any;
QuicksearchType:boolean=true;
booking:any;
columns: Array<any> = [
    {
      name: 'ID',
      checked: false
    },
    {
      name: 'URNumber',
      checked: false
    },
    {
      name: 'AccountNo',
      checked: false
    },
    {
      name: 'Surname',
      checked: false
    },
    {
      name: 'Firstname',
      checked: false
    },
    {
      name: 'Fullname',
      checked: false
    },
    {
      name: 'Gender',
      checked: true
    },
    {
      name: 'DOB',
      checked: true
    },
    {
      name: 'Address',
      checked: true
    },
    {
      name: 'Contact',
      checked: true
    },
    {
      name: 'Type',
      checked: true
    },
    {
      name: 'Branch',
      checked: true
    },
    {
      name: 'Coord',
      checked: false
    },
    {
      name: 'Category',
      checked: false
    },
    {
      name: 'ONI',
      checked: false
    },
    {
      name: 'Activated',
      checked: false
    },
    {
      name: 'Deactivated',
      checked: false
    },
    {
      name: 'Suburb',
      checked: false
    }
  ]

  data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    }
  ];

  constructor( private router: Router,
     
      private sharedS: ShareService,
      private cd: ChangeDetectorRef,
      private fb: FormBuilder,
      private listS: ListService,
      private timeS: TimeSheetService,
      private globalS: GlobalService,
    
    ) { }

  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.bookingData.subscribe(data=>{
        this.loadModel(data);
      });
   
    this.nodelist = nodes;    
    
    this.getUserData();
    this.buildForm();

  }
  loadModel(data:any){
    this.findModalOpen=true;
    this.booking=data;
      
    this.openFindModal();
  }
  tabFindIndex: number = 0;

  tabFindChange(index: number) {
    if (index == 1) {
      this.updateAllCheckedFilters(-1);
    }
    this.tabFindIndex = index;
  }
 
 updateAllCheckedFilters(filter: any): void {

    if (filter == 1 || filter == -1) {

      console.log(this.testcheck + "test flag");

      if (this.testcheck == false) {  // why its returing undefined
        if (this.allBranches) {
          this.branchesList.forEach(x => {
            x.checked = true;
          });
        } else {
          this.branchesList.forEach(x => {
            x.checked = false;
          });
        }
      }
    }

    if (filter == 2 || filter == -1) {
      if (this.testcheck == false) {
        if (this.allProgarms) {
          this.programsList.forEach(x => {
            x.checked = true;
          });
        } else {
          this.programsList.forEach(x => {
            x.checked = false;
          });
        }
      }
    }
    if (filter == 3 || filter == -1) {
      if (this.testcheck == false) {
        if (this.allCordinatore) {
          this.casemanagers.forEach(x => {
            x.checked = true;
          });
        } else {
          this.casemanagers.forEach(x => {
            x.checked = false;
          });
        }
      }
    }

    if (filter == 4 || filter == -1) {
      if (this.testcheck == false) {
        if (this.allcat) {
          this.categoriesList.forEach(x => {
            x.checked = true;
          });
        } else {
          this.categoriesList.forEach(x => {
            x.checked = false;
          });
        }
      }
    }
  }
 
  updateSingleChecked(): void {
    if (this.checkOptionsOne.every(item => !item.checked)) {
      this.allChecked = false;
      this.indeterminate = false;
    } else if (this.checkOptionsOne.every(item => item.checked)) {
      this.allChecked = true;
      this.indeterminate = false;
    } else {
      this.indeterminate = true;
      this.allChecked = false;
    }
  }

  updateSingleCheckedFilters(index: number): void {
    if (index == 1) {
      if (this.branchesList.every(item => !item.checked)) {
        this.allBranches = false;
        this.allBranchIntermediate = false;
      } else if (this.branchesList.every(item => item.checked)) {
        this.allBranches = true;
        this.allBranchIntermediate = false;
      } else {
        this.allBranchIntermediate = true;
        this.allBranches = false;
      }
    }
    if (index == 2) {
      if (this.programsList.every(item => !item.checked)) {
        this.allProgarms = false;
        this.allprogramIntermediate = false;
      } else if (this.programsList.every(item => item.checked)) {
        this.allProgarms = true;
        this.allprogramIntermediate = false;
      } else {
        this.allprogramIntermediate = true;
        this.allProgarms = false;
      }
    }
    if (index == 3) {
      if (this.casemanagers.every(item => !item.checked)) {
        this.allCordinatore = false;
        this.allCordinatorIntermediate = false;
      } else if (this.casemanagers.every(item => item.checked)) {
        this.allCordinatore = true;
        this.allCordinatorIntermediate = false;
      } else {
        this.allCordinatorIntermediate = true;
        this.allCordinatore = false;
      }
    }
    if (index == 4) {
      if (this.categoriesList.every(item => !item.checked)) {
        this.allcat = false;
        this.allCatIntermediate = false;
      } else if (this.categoriesList.every(item => item.checked)) {
        this.allcat = true;
        this.allCatIntermediate = false;
      } else {
        this.allCatIntermediate = true;
        this.allcat = false;
      }
    }
  }

  buildForm() {
    // alltypes: true,
    this.quicksearch = this.fb.group({
      active: true,
      inactive: false,
      surname: '',
      firstname: '',
      phoneno: '',
      suburb: '',
      dob: '',
      fileno: '',
      searchText: '',
    
      activeonly: true,
    });

    this.filters = this.fb.group({
      activeprogramsonly: false,
    });

   
    this.extendedSearch = this.fb.group({
      title: '',
      rule: '',
      from: '',
      to: '',

      activeonly: true,
    });

  
    this.ExpandAlertsForm = this.fb.group({
      ExpandAlerts: false,
      ExpandAllGroups: false,
      activeonly: true,
    });
    
  }

  getUserData() {
    return forkJoin([
      this.listS.getlistbranchesObj(),
      this.listS.getprogramsobj(),
      this.listS.getcoordinatorslist(),
      this.listS.getcategoriesobj(),
    ]).subscribe(x => {
      this.branchesList = x[0];
      this.programsList = x[1];
      this.casemanagers = x[2];
      this.categoriesList = x[3];
    });
  }

  filterChange(index: number) {

  }
  delCriteria(i:number){
    this.cariteriaList.splice(i,1);
}
 nzEvent(event: NzFormatEmitEvent): void {
    if (event.eventName === 'click') {
      var title = event.node.origin.title;

      this.extendedSearch.patchValue({
        title: title,
      });
      var keys = event.keys;

    }

}

  handleCancel() {
    this.findModalOpen = false;
    this.quicksearch.reset();
    this.filters.reset();
    this.extendedSearch.reset();
    this.ExpandAlertsForm.reset();
  }

  handleOk() {
    
  }
  prntSearchList(){

  }
   searchData(): void {
      this.loading = true;
  
      this.selectedTypes = this.checkOptionsOne
        .filter(opt => opt.checked)
        .map(opt => opt.value).join("','")
  
      this.selectedPrograms = this.programsList
        .filter(opt => opt.checked)
        .map(opt => opt.name).join("','")
  
      this.selectedCordinators = this.casemanagers
        .filter(opt => opt.checked)
        .map(opt => opt.uniqueID).join("','")
  
      this.selectedCategories = this.categoriesList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join("','")
  
      this.selectedbranches = this.branchesList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join("','")
  
        
      var postdata = {
        active: this.quicksearch.value.active,
        inactive: this.quicksearch.value.inactive,
        alltypes: this.allChecked,
        selectedTypes: this.selectedTypes,
        allBranches: this.allBranches,
        selectedbranches: (this.allBranches == false) ? this.selectedbranches : '',
        allProgarms: this.allProgarms,
        selectedPrograms: (this.allProgarms == false) ? this.selectedPrograms : '',
        allCordinatore: this.allCordinatore,
        selectedCordinators: (this.allCordinatore == false) ? this.selectedCordinators : '',
        allcat: this.allcat,
        selectedCategories: (this.allcat == false) ? this.selectedCategories : '',
        activeprogramsonly: this.filters.value.activeprogramsonly,
        surname: this.quicksearch.value.surname,
        firstname: this.quicksearch.value.firstname,
        phoneno: this.quicksearch.value.phoneno,
        suburb: this.quicksearch.value.suburb,
        dob: (!this.globalS.isEmpty(this.quicksearch.value.dob)) ? this.globalS.convertDbDate(this.quicksearch.value.dob, 'yyyy-MM-dd') : '',
        fileno: this.quicksearch.value.fileno,
        searchText: this.quicksearch.value.searchText,
        criterias: this.cariteriaList // list of rules
      }
  
      this.timeS.postrecipientquicksearch(postdata).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.filteredResult = data;
        this.originalTableData=data;
        this.loading = false;
        this.cd.detectChanges();
        
      });
  
    }
    SearchOnEnter(event:KeyboardEvent){
        if (event.key === 'Enter' ) {
          event.preventDefault();
          this.searchData();
        }
      }
    log(event: any, index: number) {
        this.testcheck = true;
        if (index == 1)
          this.selectedbranches = event;
        if (index == 2)
          this.selectedPrograms = event;
        if (index == 3)
          this.selectedCordinators = event;
        if (index == 4)
          this.selectedCategories = event;
      }
    selectRecipient(data:any){
        this.selectedRecipient=data;
        this.user ={
          code: data.accountNo,
          id: data.id,
          view: 'recipient',
          agencyDefinedGroup: data.category,
          sysmgr: this.tocken.user=='sysmgr' ? true :false
        }
      }

    AllocateRecipient(data:any){
        this.selectedRecipient=data;
        this.user ={
          code: data.accountNo,
          id: data.id,
          view: 'recipient',
          agencyDefinedGroup: data.category,
          sysmgr: this.tocken.user=='sysmgr' ? true :false
        }
        this.searchDone.emit(data);
        this.handleCancel();
       // this.searched.next(data.accountNo);
       // this.router.navigate(['admin/recipient/personal'], { state: { data: data } });
       // this.sharedS.emitChange(this.user);
       
        this.findModalOpen=false;
      }

      setCriteria() {

        this.cariteriaList.push({
          fieldName: this.extendedSearch.value.title,
          searchType: this.extendedSearch.value.rule,
          textToLoc: this.extendedSearch.value.from,
          endText: this.extendedSearch.value.to,
        })
      }

      
ngModelQChange(event:any){
 
    this.QuicksearchOptions.push(event);
  }
   onSearch(value: string): void {
    this.QuicksearchValue=value
      // You can implement search logic here if needed
    }
  
    ExpandAlertsChecked(){
  
    }

    updateAllChecked(): void {
        this.indeterminate = false;
        if (this.allChecked) {
          this.checkOptionsOne = this.checkOptionsOne.map(item => ({
            ...item,
            checked: true
          }));
        } else {
          this.checkOptionsOne = this.checkOptionsOne.map(item => ({
            ...item,
            checked: false
          }));
        }
      }
    
    
      openFindModal() {
        this.tabFindIndex = 0;
        this.findModalOpen = true;
        this.filteredResult=[];
        let lstAddress=[];
        this.listS.getAddressTypes().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          lstAddress=data.map(x=> {
            return {
              key: x,
              title: x,
              isLeaf: true
            }
          })
    
          let index = 0; //this.nodelist.indexOf(x=>x.key=='100');
          let node = this.nodelist.find(x=>x.key=='100');
          let insertAfterIndex = node.children.findIndex(x=>x.key=='1008');
          node.children.splice(insertAfterIndex + 1, 0, ...lstAddress);
    
          this.nodelist[index] = node;
          this.cd.detectChanges();
        });
        
        
      }

       originalTableData: Array<any>;
      dragOrigin: Array<string> = [];
      
          columnDictionary = [{
              key: 'Ur Number',
              value: 'urNumber'
          },{
              key: 'Code',
              value: 'id'
          },{
              key: 'Surname',
              value: 'surnameOrganisation'
          },{
              key: 'First Name',
              value: 'firstName'
          },{
              key: 'Name',
              value: 'name'
          },{
              key: 'Gender',
              value: 'gender'
          },{
              key: 'D.O.B',
              value: 'dob'
          },{
              key: 'Address',
              value: 'address'
          },{
            key: 'Contact',
            value: 'contact'
        },{
              key:'Type',
              value:'type'
          },{
              key:'Branch',
              value:'branch'
          },{
            key:'Alerts',
            value:'alerts'
        },{
          key:'Primary Coordinator',
          value:'primaryCoord'
        },{
          key:'activationDate',
          value:'activationDate'
        },{
          key:'deActivationDate',
          value:'deActivationDate'
        }
        
        ];
          
          
          
      
          dragDestination = [
              'Ur Number',
              'Code',
              'Surname',
              'First Name',
              'Name',
              'Gender',
              'D.O.B',
              'Address',
              'Contact',
              'Type',
              'Branch',
              'Alerts',
              'Primary Coordinator',
              'activationDate',
              'deActivationDate'


          ];
      
      
          flattenObj = (obj, parent = null, res = {}) => {
              for (const key of Object.keys(obj)) {
                  const propName = parent ? parent + '.' + key : key;
                  if (typeof obj[key] === 'object') {
                      this.flattenObj(obj[key], propName, res);
                  } else {
                      res[propName] = obj[key];
                  }
              }
              return res;
          }
      
          searchColumnDictionary(data: Array<any>, tobeSearched: string){
              let index = data.findIndex(x => x.key == tobeSearched);        
              return data[index].value;
          }
      
          drop(event: CdkDragDrop<string[]>) {
              if (event.previousContainer === event.container) {
                  moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
              } else {
                  if(!event.container.data.includes(event.item.data)){
                      copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
                  }
              }
              this.generate();
          }
      
          generate(){
              const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
              console.log(dragColumns)
      
              var convertedObj = groupArray(this.originalTableData, dragColumns);
      
              console.log(convertedObj)
              var flatten = this.flatten(convertedObj, [], 0);
              console.log(flatten)
              if(dragColumns.length == 0){
                  this.filteredResult = this.originalTableData;
              } else {
                  this.filteredResult = flatten;
              }
          }
      
          flatten(obj: any, res: Array<any> = [], counter = null){
              for (const key of Object.keys(obj)) {
                  const propName = key;
                  if(typeof propName == 'string'){                   
                      res.push({key: propName, counter: counter});
                      counter++;
                  }
                  if (!Array.isArray(obj[key])) {
                      this.flatten(obj[key], res, counter);
                      counter--;
                  } else {
                      res.push(obj[key]);
                      counter--;
                  }
              }
              return res;
          }
      
          removeTodo(data: any){
              this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
              this.generate();
          }
      
          isArray(data: any){
              return Array.isArray(data);
          }
          trackByFn(index, item) {
            return item.id;
        }
          isSome(data: any){
              if(data){
                  return data.some(d => 'key' in d);
              }
              return true;        
          }
}
