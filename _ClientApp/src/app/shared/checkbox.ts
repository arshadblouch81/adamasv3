import { Component, forwardRef, Input } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

export const CHECKBOX_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => CheckboxComponent),
};

@Component({
    selector: 'checkbox',
    templateUrl: './checkbox.html',
    styles:[`
    :host{
        display:inline-block;
    }
    label{
        margin-top: 5px;
        cursor: pointer;
        margin-right: 2px;
    }
    .checkbox-custom {
        display:none;
    }
    .checkbox-custom-label {
        display: inline-block;
        position: relative;
        vertical-align: middle; 
    }
    .checkbox-custom-label {
        content: '';
        background: #fff;
        border-radius: 3px;
        border: 1px solid #8c8c8c;
        display: inline-block;
        vertical-align: middle;
        width: 15px;
        height: 15px;
        padding: 2px;
    }
    .checkbox-custom-label-checked{
        content: '';    
        background: #266eab;
        border-radius: 5px;
        display: inline-block;
        vertical-align: middle;
        width: 15px;
        height: 15px;
        padding: 2px;
    }
    .disabled{
        background:#f5f5f5;
        border-color:#dcdcdc;
        cursor: not-allowed;
    }      
    span{
        cursor:pointer;
    }     
    `],
    providers: [
        CHECKBOX_VALUE_ACCESSOR
    ]
})

export class CheckboxComponent implements ControlValueAccessor {
    @Input() disabled: boolean = false
    private innerValue: boolean = false

    constructor(){}

    private _onChange = (_: any) => {};
    private _onTouched = () => {};

    //get accessor
    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this._onChange(v);
        }
    }
    

    writeValue(value: boolean): void{
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    highlight(){
        if(!this.disabled){
            this.value = !this.value
        }
        return;
    }

    registerOnChange(fn: any): void {
        this._onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this._onTouched = fn;
    }
}