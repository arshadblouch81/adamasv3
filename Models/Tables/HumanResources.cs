using System;

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Adamas.Models.Tables {
   public class HumanResources
{
    [Key]
    public int? RecordNumber { get; set; } // IDENTITY(1,1) NOT NULL
    public string PersonID { get; set; } // nvarchar(50) NULL
    public string Group { get; set; } // nvarchar(50) NULL
    public string Type { get; set; } // nvarchar(75) NULL
    public string Name { get; set; } // nvarchar(60) NULL
    public string Address1 { get; set; } // nvarchar(100) NULL
    public string Address2 { get; set; } // nvarchar(50) NULL
    public string Suburb { get; set; } // nvarchar(50) NULL
    public string Postcode { get; set; } // nvarchar(10) NULL
    public string Phone1 { get; set; } // nvarchar(15) NULL
    public string Phone2 { get; set; } // nvarchar(15) NULL
    public string Email { get; set; } //nvarchar(50) NULL

    public string Fax { get; set; } // nvarchar(15) NULL
    public string Mobile { get; set; } // nvarchar(15) NULL
    
    public DateTime? Date1 { get; set; } // smalldatetime NULL
    public DateTime? Date2 { get; set; } // smalldatetime NULL
    public string State { get; set; } // varchar(5) NULL
    public string EquipmentCode { get; set; } // varchar(25) NULL
    public string SerialNumber { get; set; } // varchar(25) NULL
    public string LockBoxCode { get; set; } // varchar(25) NULL
    public string LockBoxLocation { get; set; } // varchar(25) NULL
    public string PurchaseAmount { get; set; } // varchar(25) NULL
    public DateTime? PurchaseDate { get; set; } // datetime NULL
    public DateTime? DateInstalled { get; set; } // datetime NULL
    public bool DeletedRecord { get; set; } // bit NOT NULL
    public int? COID { get; set; } // int NULL
    public int? BRID { get; set; } // int NULL
    public int? DPID { get; set; } // int NULL
    public string Notes { get; set; } // nvarchar(4000) NULL
    public bool ReminderProcessed { get; set; } // bit NOT NULL
    public bool? Completed { get; set; } // bit NULL
    public bool Recurring { get; set; } // bit NOT NULL
    public bool SameDay { get; set; } // bit NOT NULL
    public bool SameDate { get; set; } // bit NOT NULL
    public string Creator { get; set; } // varchar(25) NULL
    public string ReminderScope { get; set; } // varchar(25) NULL
    public bool? APrimary { get; set; } // bit NULL
    public string ReminderCreator { get; set; } // varchar(25) NULL
    public string ReminderMode { get; set; } // varchar(10) NULL
    public string User1 { get; set; } // nvarchar(4000) NULL
    public string SubType { get; set; } // nvarchar(75) NULL
    public string ImportType { get; set; } // nvarchar(2) NULL
    public bool? UserYesNo1 { get; set; } // bit NULL
    public string User2 { get; set; } // nvarchar(10) NULL
    public string User3 { get; set; } // nvarchar(30) NULL
    public string User4 { get; set; } // nvarchar(10) NULL
    public bool? MobileAlert { get; set; } // bit NULL
    public string User5 { get; set; } // varchar(25) NULL
    public bool? Undated { get; set; } // bit NULL
    public DateTime? xEndDate { get; set; } // datetime NULL
}

}