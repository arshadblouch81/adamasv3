using System;

namespace Adamas.Models.Modules{
    public class StartJob {
        public int RecordNo { get; set; }
        public bool Cancel { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Location { get; set; }
    }
}