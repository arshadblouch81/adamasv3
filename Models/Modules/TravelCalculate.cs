using System;

namespace Adamas.Models.Modules
{
    public class TravelCalculate
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string GoogleCustID { get; set; }
        public string TravelProvider { get; set; }
        public string MapApiKey { get; set; }
        public string Batchno { get; set; }
        public string Staff { get; set; }
        public string Date { get; set; }
        public string StartClientEndTime { get; set; }
        public string TimeDuration { get; set; }
        public string StartClient { get; set; }
        public string StartAddress { get; set; }
        public string StartRecord { get; set; }
        public string StartService { get; set; }
        public string EndClient { get; set; }
        public string EndAddress { get; set; }
        public string EndRecord { get; set; }
        public string EndService { get; set; }
        public string VehicleType { get; set; }

    }
}



