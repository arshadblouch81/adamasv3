import { Component, Input, OnInit,forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { SqlWizardService } from '@services/sqlwizard.service';

@Component({
  selector: 'select-list',
  templateUrl: './select-list.component.html',
  styleUrls: ['./select-list.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectListComponent),
      multi: true
    }
  ]
})

export class SelectListComponent implements OnInit, ControlValueAccessor {

    @Input() programVariables: any;

    value: string;
    lists: Array<any>;    

    // Function to call when the rating changes.
    onChange = (data: any) => {};

    // Function to call when the input is touched (when a star is clicked).
    onTouched = () => {};

    constructor(
      private sqlWizardS: SqlWizardService
    ) { }

    ngOnInit(){
      this.populate(this.programVariables)
    }

    populate(d: any){
      console.log(d);
      this.sqlWizardS.GetReferralType(d).subscribe(data => {
          this.lists = data.map(x => x['activity']);
          this.onClickHandler(this.lists[0])
      })   
    } 

    onClickHandler(data: any){   
      this.value = data;
      this.onChange(this.value);
    }

    writeValue(value: any): void {
     
    }
  
    // Allows Angular to register a function to call when the model (rating) changes.
    // Save the function as a property to call later here.
    registerOnChange(fn: (rating: number) => void): void {
      this.onChange = fn;
    }
  
    // Allows Angular to register a function to call when the input has been touched.
    // Save the function as a property to call later here.
    registerOnTouched(fn: () => void): void {
      this.onTouched = fn;
    }

 
}


