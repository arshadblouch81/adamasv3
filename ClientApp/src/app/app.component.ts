import { Component, OnInit, HostListener } from '@angular/core';
import { VersionCheckService, TimerService } from '@services/index';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  timeout: any;
  @HostListener('document:mousemove', ['$event']) 
  onMouseMove(e) {
    this.timerS.setMouseMoving = true;   
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
        this.timerS.setMouseMoving = false;
    }, 5000);
  }

  
  isCollapsed = false;

  constructor(
    private versionCheck: VersionCheckService,
    private timerS: TimerService
  ){

  }

  ngOnInit(): void{
    if(environment.production){
      // this.versionCheck.initVersionCheck(environment.versionCheckURL);
    }
  }
}
