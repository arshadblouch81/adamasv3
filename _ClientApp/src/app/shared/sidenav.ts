import { Component, OnInit, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { GlobalService } from '../services/global.service';
@Component({
    selector: 'sidenav',
    templateUrl: './sidenav.html',
    styles:[
    `
    .clr-vertical-nav{
        width:6.8rem;
    }
    .nav-list{
        list-style:none;
    }
    ul {
        padding: 0 !important;
    }
    a{
        font-size:11px;
    }
    :host {
        display:contents;
    }    
    .nav-link:hover{
        background: #ededed;
    }
    .tooltiptext{
        visibility: hidden;
        font-size: 11px;
        background-color: #565656;
        color: #fff;
        border-radius: 2px;
        position: fixed;
        z-index: 58;
        left: 45px;
        padding: 6px;
    }
    .tooltiptext::after{
        content: "";
        position: absolute;
        bottom: 21%;
        left: -7px;
        margin-left: -7px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #696969 transparent transparent;
    }
    .sidenav .sidenav-content{
        overflow:inherit;
    }

    // @media only screen and (max-width: 600px) {
    //     .side-container {
    //         display:none;
    //     }
    // }
    `
    ]
})

export class SideNav implements OnInit{
    @Output() EmitValue: EventEmitter<any> = new EventEmitter();
    collapsed: boolean = false;
    role: string = null;

    expanded: boolean = true;

    hide: boolean = false;



    constructor(
        private globalS: GlobalService,
        private router: Router
    ){ 
        this.router.events.subscribe(event =>{
            if(event instanceof NavigationEnd)
                this.hideIfClientMemberURL(event.url);
        })

    }

    ngOnInit(){
        this.role = this.globalS.isRole();
    }

    emit(data: any){
        this.EmitValue.emit(data);
    }

    hideIfClientMemberURL(url: string){
        if(url.match(/\/client\/members/gi) != null && url.match(/\/client\/members/gi).length > 0){
            this.hide = false;
            return;
        }
        this.hide = true;
    }
    
}