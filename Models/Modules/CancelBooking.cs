
using System;
namespace Adamas.Models.Modules{
    public class CancelBooking {
        public string RecordNo { get; set; }
        public string ServiceType { get; set; }
        public string RosterDate { get; set; }
        public string RosterTime { get; set; }
        public string Username { get; set; }
        public string TraccsUser { get; set; }
        public string RecipientPersonId { get; set;  }
        public string ManagerPersonId { get; set;  }
    }
}