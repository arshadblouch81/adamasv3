using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adamas.Model.Report
{
    public class ProductModel
    {
        public string Title { get; set; }
        public IEnumerable<GroupModel> Items { get; set; }
        public static ProductModel Example()
        {
            return new ProductModel()
            {
                Title = "Mark",
                Items = new List<GroupModel>(){
                    new GroupModel(){
                        GroupName = "Food & Snacks",
                        Items = new List<Item>(){
                            new Item(){
                                Column1 = "Pizza",
                                Column2 = "Bread",
                                Column3 = "12.00"
                            },
                            new Item(){
                                Column1 = "Piattos",
                                Column2 = "Potato Snack",
                                Column3 = "27.90"
                            },
                            new Item(){
                                Column1 = "Stick-O",
                                Column2 = "Chocolate Wafer",
                                Column3 = "55.00"
                            },
                            new Item(){
                                Column1 = "Magic Flakes",
                                Column2 = "Biscuit",
                                Column3 = "45.00"
                            }
                        }
                    },
                    new GroupModel(){
                        GroupName = "Drinks",
                        Items = new List<Item>(){
                            new Item(){
                                Column1 = "CocaCola",
                                Column2 = "Softdrinks",
                                Column3 = "12"
                            },
                            new Item(){
                                Column1 = "Tanduay",
                                Column2 = "Alcoholic",
                                Column3 = "21.34"
                            },
                            new Item(){
                                Column1 = "Tang",
                                Column2 = "Juice Drink",
                                Column3 = "45.00"
                            },
                            new Item(){
                                Column1 = "CocaCola",
                                Column2 = "Price",
                                Column3 = "Haha"
                            }
                        }
                    }
                }
            };
        }


    }

    public class GroupModel
    {
        public string GroupName { get; set; }
        public IEnumerable<Item> Items { get; set; }
    }

    public class Item
    {
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string Column5 { get; set; }
        public string Column6 { get; set; }
        public string Column7 { get; set; }
        public string Column8 { get; set; }
        public string Column9 { get; set; }
        public string Column10 { get; set; }
    }
}
