# SpreadJS Wrapper Components for Angular

This package contains the wrapper components plug-in to use [SpreadJS](https://www.grapecity.com/spreadjs) in Angular.

GrapeCity SpreadJS Angular Wrapper Components.

For a detailed listing of SpreadJS sub-libraries and plug-ins, please see [Using SpreadJS Libraries](http://help.grapecity.com/spread/SpreadSheets11/webframe.html#modules.html).

## Installation
```sh
npm install @grapecity/spread-sheets-angular
```

## Getting more help
Visit the SpreadJS home page to get more information about the library:
[https://www.grapecity.com/spreadjs](https://www.grapecity.com/spreadjs)

You can ask any question about SpreadJS using the [SpreadJS Forum](https://www.grapecity.com/forums/spread-sheets).
