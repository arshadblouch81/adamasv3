import { Component, Input, Output, EventEmitter } from '@angular/core'
import { HttpClient, HttpRequest, HttpEventType, HttpResponse, HttpEvent } from '@angular/common/http'

import { UploadService, GlobalService } from '../services/index'


export const allowedFiles = [
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "application/vnd.oasis.opendocument.spreadsheet",
    "application/vnd.oasis.opendocument.text",
    "application/vnd.ms-excel",
    "application/vnd.ms-powerpoint",
    "application/x-msaccess",
    "application/msword",
    "application/pdf",
    // "text/html",
    "text/plain",
    "audio/mp3",
    "audio/mp4",
    "image/jpeg",
    "image/png",
    "image/gif"
  ];

const enum ImagePosition {
    DOCUMENT = '-5px -56px',
    DOCMSWORD = '-5px -56px',
    PDF = "-99px -5px",
    SPREADSHEET = "-50px -5px",
    TEXTFILE = "-148px -5px",
    PRESENTATION = "-190px -5px",
    JPG = "-6px -57px",
    PNG = "-53px -56px",
    MP3 = "-95px -107px",
    MP4 = "-10px -157px"
}
  
const enum ImageActivity {
    DOCUMENT = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    DOCMSWORD = 'application/msword',
    PDF = "application/pdf",
    SPREADSHEET = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    SPREADSHEETMS = "application/vnd.ms-excel",
    TEXTFILE = "text/plain",
    PRESENTATION = "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    JPG = "image/jpeg",
    PNG = "image/png",
    MP3 = "audio/mp3",
    MP4 = "video/mp4"
}

@Component({
    selector: 'dragdrop',
    templateUrl: './drag-drop.html',
    styles:[`
    input{
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    .download-pic{
        font-size: 2rem;
        color: #b9b9b9;
        margin: 1rem 0 0 0;
    }
    .drag-wrapper{
        width:100%;
    }
    .drag-outer{
        max-width: 12rem;
        margin: 1rem auto;
        text-align: center;
        border: 2px dashed #e7e7e7;
        height: 7rem;
    }
    .gwapo{
        background: #d3ffe3 !important;
        border:2px solid #88ffd7;

    }
    b:hover{
        text-decoration: underline;
        cursor:pointer;
    }
    p{
        margin:0;
    }
    label{
        color: #2896e2;
        cursor: pointer;
    }
    label:hover{
        text-decoration:underline;
    }
    .loading-file{
        padding:0;
        position:absolute;
        bottom:0;
        height:9px;
        box-shadow:none;
    }
    .drag-title{
        padding: 10px 0 0 0;
    }
    ul{
        list-style:none;
    }
    li{
        margin-top: 10px;
        display: inline-block;
        width: 100%;
        font-size: 12px;
        line-height:15px;
    }
    .filetype-logo{
        background: url(../../assets/logo/filetype.png) no-repeat;
        height: 39px;
        width: 39px;
        float: left;
        background-size: 10rem;
        background-position: -10px -5px;
        margin: 0 15px 0 0;
    }
    .file-name{
        width: 70%;
        display: inline-block;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    .tooltip{
        opacity:1;
    }
    .warning{
      color:#ff4040;
      height:25px !important;
      width:25px !important;
    }
    span.tooltip-content{
      width:7rem !important;
    }
    `]   
})

export class DragDropComponent {
    @Input() personId: string;
    @Input() standalone: boolean=  false;
    @Output() success = new EventEmitter<any>();
    @Output() fileData = new EventEmitter<any>();

    onEnter: boolean = false;    
    uploadReady: boolean = false;
    uploadProgress:number = 0;

    errors: Array<string> = [];    
    fileArr: Array<any> = [];
    files: Array<any> = [];

    displayString: string = 'Drag files to upload or';

    constructor(
        private uploadS: UploadService,
        private globalS: GlobalService,
        private http: HttpClient
    ){

    }

    onChange(e: any){     
        e.preventDefault();
        this.fileUploadProcess(e.target.files);
    }

    reset(){
        this.onEnter = false;
        this.displayString = `DRAG FILE HERE OR`;
    }

    fileUploadProcess(_file: Array<File>){
      this.uploadReady = true;
      this.fileArr = []
      this.files = _file;

        if(this.files.length > 5){
            this.globalS.eToast('Error','Only 5 files allowed')
            this.reset();
            return;
        }
    
        this.uploadS.checkfiles(this.files, this.personId).subscribe((event: any) => {
            if (event.type === HttpEventType.Response) 
            {
                if (event.body.fileStatus.length > 0) {
                        var fileStatus = event.body.fileStatus;
                        for (var a = 0; a < fileStatus.length; a++) {
                        for (var b = 0; b < this.files.length; b++) {
                            if (this.files[b].name == fileStatus[a].name) {
                            this.fileArr.push(fileStatus[a]);
                            break;
                            }
                        }
                        }
                    }

           

                var notASCII = (event.body).filesNotIncluded;

                if (notASCII && notASCII > 0) {
                    this.globalS.wToast('Warning', `${notASCII} files needs to be renamed`)
                }

                this.displayString = `${this.fileArr.length} files to be uploaded`;
                this.uploadReady = false

                if(this.standalone){
                    this.fileData.emit(this.files);                 
                }
            }
        },
          err => {
            this.uploadReady = false
            this.globalS.eToast('Error','Request files too large')
        
        });       
    }

    onDrop(e: any){
        e.preventDefault();
        this.fileUploadProcess(e.dataTransfer.files);
    }

    onDragOver(e: any){
        e.preventDefault();
        this.onEnter = true;
    }

    onDragLeave(){
        this.onEnter = false;
    }

    getPositionImg(data: any) {
        let type = data.type;
        if (type.indexOf(ImageActivity.PDF) !== -1) return ImagePosition.PDF;
        if (type.indexOf(ImageActivity.DOCUMENT) !== -1) return ImagePosition.DOCUMENT;
        if (type.indexOf(ImageActivity.DOCMSWORD) !== -1) return ImagePosition.DOCMSWORD;
        if (type.indexOf(ImageActivity.SPREADSHEET) !== -1 || type.indexOf(ImageActivity.SPREADSHEETMS) !== -1) return ImagePosition.SPREADSHEET;
        if (type.indexOf(ImageActivity.TEXTFILE) !== -1) return ImagePosition.TEXTFILE;
        if (type.indexOf(ImageActivity.PRESENTATION) !== -1) return ImagePosition.PRESENTATION;
        if (type.indexOf(ImageActivity.JPG) !== -1) return ImagePosition.JPG;
        if (type.indexOf(ImageActivity.PNG) !== -1) return ImagePosition.PNG;
        if (type.indexOf(ImageActivity.MP3) !== -1) return ImagePosition.MP3;
        if (type.indexOf(ImageActivity.MP4) !== -1) return ImagePosition.MP4;
    }

    clear(quick: boolean = false){
        this.onEnter = false;

        if(quick){
            this.errors = []
            this.fileArr = []
            this.displayString = 'DRAG FILE HERE OR'
            this.uploadProgress = 0;

            if(this.standalone){
                this.fileData.emit([]);                 
            }

            return;
        }

        setTimeout(() => {
            this.errors = []
            this.fileArr = []
            this.displayString = 'DRAG FILE HERE OR'
            this.uploadProgress = 0;
        }, 1000);
     }
     

    upload(){
        if(!this.detectNotAllowedFileTypeSize(this.fileArr)){

            const formData = new FormData()
         
            for (var file of this.files) {
              formData.append(file.name, file)
            }
          
            
            const req = new HttpRequest('POST', `api/upload/upload/document/${this.personId}`, 
                                            formData, 
                                            { reportProgress: true }
                                        );

            this.http.request(req).subscribe(event => {
                if (event.type === HttpEventType.UploadProgress)
                    this.uploadProgress  = Math.round(100 * event.loaded / event.total);
                else if (event.type ===  HttpEventType.Response)  
                    if(this.uploadProgress == 100){
                        this.clear();
                        this.globalS.sToast('Success','Files Uploaded')
                        this.success.emit();
                    }               
                })
       
            return;
        }
        this.globalS.eToast('Error', 'Filetype not allowed or a file has exceeded 10MB in size')
    }

    allowedFile(file: File){
        if(allowedFiles.indexOf(file.type) == -1){
            return false;
        }  
        return true;
    }

    detectNotAllowedFileTypeSize(list: Array<any>){
        let found = false;   
        for (var a = 0, len = list.length; a < len; a++){
            if(allowedFiles.indexOf(list[a].type) == -1 || list[a].size > 10000000){
                found = true;
                break;
            }  
        }
        return found;
    }
}
