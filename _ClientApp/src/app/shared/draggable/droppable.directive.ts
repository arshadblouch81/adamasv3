import { Directive, ElementRef, NgZone, AfterViewInit, Renderer2, Input, OnInit, Output, EventEmitter } from '@angular/core'
import { DraggableTimeDirective } from './draggable.directive'

import { DomSanitizer, SafeStyle } from '@angular/platform-browser'


interface DateAndTime {
    date: any,
    time: any
}

@Directive({
    selector: '[droppable]'
})

export class DroppableDirective implements OnInit, AfterViewInit {

    @Input() hasValue: any;
    @Output() newTime =  new EventEmitter<any>()

    currDate: DateAndTime;

    constructor(
        public el: ElementRef,
        public ngZone: NgZone){
    }
    
    ngOnInit(){

    }

    ngAfterViewInit(){
        const droppable = this.el.nativeElement;
        this.ngZone.runOutsideAngular(() => {   
                        
            droppable.addEventListener('pointerdown', (e: any) => {
                if(this.hasValue){
                    const day = this.el.nativeElement.getAttribute('day')
                    const time = this.el.nativeElement.getAttribute('time')
                    this.currDate = {
                        date: day,
                        time: time
                    }
                    this.el.nativeElement.style.backgroundColor = 'red'
                    this.newTime.emit(this.currDate)
                }
            })

            document.addEventListener('pointerup', (e: PointerEvent) => {
                this.el.nativeElement.style.backgroundColor = ''
            })        
        })
    } 
}