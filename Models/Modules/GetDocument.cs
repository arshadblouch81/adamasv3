using System;

namespace Adamas.Models.Modules{
    public class GetDocument {
        public string PersonID { get; set; }
        public string Extension { get; set; }
        public string FileName { get; set; }
        public string DocPath { get; set; }

        public string Program { get; set; }
        public string Discipline { get; set; }
        public string CareDomain { get; set; }
        public string Classification { get; set; }
        public string Category { get; set; }
        public DateTime? ReminderDate { get; set; }
        public Boolean? PublishToApp { get; set; }
        public string ReminderText { get; set; }
        public string Notes { get; set; }
        public int? SubId { get; set; }
        public string User { get; set; }
    }

}