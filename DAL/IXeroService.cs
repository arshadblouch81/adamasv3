﻿using AdamasV3.Models.Modules;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xero.NetStandard.OAuth2.Model.PayrollAu;
using Xero.NetStandard.OAuth2.Model.Accounting; 
namespace AdamasV3.DAL
{
    public interface IXeroService
    {
        Task<Timesheets> PostTimesheet(List<Timesheet> timesheet);
        Task<Xero.NetStandard.OAuth2.Model.PayrollAu.Employees> GetEmployees();
        Task<bool> PostAllEarningsRateIds(List<EarningsRate> rates);
        Task<PayItems> GetPayItems();
        Task<Tokens> GetRefreshTokens();
        Task<string> ChangeDbConnection(string dbName, string connString);
        Task<bool> PostXeroEmployeeId(Xero.NetStandard.OAuth2.Model.PayrollAu.Employees employees);
        Task<PayrollCalendars> GetPayrollCalendars();
        Task<int> GetStaffCount();
        Task<Timesheets> GetTimeSheets();
         Task<bool> CreateEmployees(List<Xero.NetStandard.OAuth2.Model.PayrollAu.Employee> employees);
         Task<bool> CreatePayItems( List<PayItem>  payitem);

         public Task<TrackingCategories> GetTrackingCategories();
    }
}
