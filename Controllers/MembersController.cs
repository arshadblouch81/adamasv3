using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.Interfaces;
using Microsoft.Extensions.Configuration;

using StaffTable = Adamas.Models.Tables.Staff;
using Staff = Adamas.Models.Tables.Staff;


using MailKit;
using MailKit.Security;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using Microsoft.AspNetCore.Http;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    public class MembersController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private IEmailService _emailService;
        private readonly IEmailConfiguration  _emailConfiguration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ILoginService login;

        public MembersController(
            DatabaseContext context, 
            IConfiguration config, 
            IGeneralSetting setting,
            IEmailService emailService,
            IEmailConfiguration  emailConfiguration,
            IHttpContextAccessor httpContextAccessor,
            ILoginService _login
        )
        {
            _context = context;
            _emailService = emailService;
            _emailConfiguration = emailConfiguration;
            GenSettings = setting;
            _httpContextAccessor = httpContextAccessor;
            login = _login;
        }

        [HttpGet, Route("show-all-recipients")]
        public async Task<IActionResult> GetShowAllRecipientsValue()
        {
            var user = await this.login.GetCurrentUser();
            var isShowAllRecipients = await (from ui in _context.UserInfo where ui.Name == user.User select ui.ShowAllRecipients).FirstOrDefaultAsync();
            return Ok(isShowAllRecipients);
        }

        [HttpPost, Route("show-all-recipients/{val}")]
        public async Task<IActionResult> PostShowAllRecipientsValue(bool val)
        {
            var user = await this.login.GetCurrentUser();

            var userinfo = await (from useri in _context.UserInfo where useri.Name == user.User select useri).FirstOrDefaultAsync();
            userinfo.ShowAllRecipients = val;
            await _context.SaveChangesAsync();

            return Ok(true);
        }

        [HttpGet, Route("members")]
        public async Task<IActionResult> GetMembers(MemberList member){
            try
            {
                var clientList = new List<Recipients>();
                var user = await this.login.GetCurrentUser();

                var isShowAllRecipients = await (from ui in _context.UserInfo where ui.Name == user.User select new { ui.ShowAllRecipients, ui.Usertype }).FirstOrDefaultAsync();
                var registration = await (from reg in _context.Registration select reg).FirstOrDefaultAsync();

                if(isShowAllRecipients.Usertype == "CLIENT MANAGER" && isShowAllRecipients.ShowAllRecipients.HasValue  && isShowAllRecipients.ShowAllRecipients.Value == true)
                {
                    clientList = await (from recipient in _context.Recipients
                                        orderby recipient.AccountNo

                                        select new Recipients
                                        {
                                            AccountNo = recipient.AccountNo,
                                            UniqueID = recipient.UniqueID,
                                            FirstName = recipient.FirstName,
                                            SurnameOrg = recipient.SurnameOrg,
                                            BelongsTo = recipient.BelongsTo,
                                            RECIPIENT_PARENT_SITE = recipient.RECIPIENT_PARENT_SITE
                                        }).ToListAsync();

                    return Ok(clientList);
                }




                if (!string.IsNullOrEmpty(registration.ClientPortalManagerMethod) 
                    && (registration.ClientPortalManagerMethod).Trim().ToUpper() == "MANAGE BY CLIENT CATEGORY")
                {
                    var serviceRegion = (from staff in _context.Staff where staff.AccountNo == member.PersonId select staff.ServiceRegion).FirstOrDefault();

                    if(serviceRegion == null)
                    {
                        return BadRequest("No record in Staff Table");
                    }

                    clientList =  await(

                        from recipient in _context.Recipients 
                        join domains in _context.DataDomains on recipient.Recipient_Coordinator equals domains.Description

                        join st in _context.Staff on domains.HACCCode equals st.AccountNo into staffgroup
                        from staff in staffgroup.DefaultIfEmpty()

                        where recipient.AgencyDefinedGroup == serviceRegion
                        orderby recipient.AccountNo
                        
                        select new Recipients{
                            AccountNo = recipient.AccountNo,
                            UniqueID = recipient.UniqueID,
                            FirstName = recipient.FirstName,
                            SurnameOrg = recipient.SurnameOrg,
                            BelongsTo = recipient.BelongsTo,
                            RECIPIENT_PARENT_SITE = recipient.RECIPIENT_PARENT_SITE
                        }
                    ).ToListAsync();
                } 
                else
                {

                    clientList = await (from recipient in _context.Recipients 
                                        where recipient.RECIPIENT_PARENT_SITE == member.PersonId orderby recipient.AccountNo 
                                        select new Recipients {
                                            AccountNo = recipient.AccountNo,
                                            UniqueID = recipient.UniqueID,
                                            FirstName = recipient.FirstName,
                                            SurnameOrg = recipient.SurnameOrg,
                                            BelongsTo = recipient.BelongsTo,
                                            RECIPIENT_PARENT_SITE = recipient.RECIPIENT_PARENT_SITE
                                        }).ToListAsync();
                }

                return Ok(clientList);
                
                // using(var conn = _context.Database.GetDbConnection() as SqlConnection)
                // {
                //     string sqlCmd = string.Empty;
            
                //     if(!member.IsActive){
                //         sqlCmd = @"SELECT AccountNo, UniqueID, FirstName, [Surname/Organisation] AS Surname FROM Recipients where belongsto = @personID AND 
                //                                 ISNULL(AdmissionDate, '') <> '' AND ISNULL(DischargeDate, '') = '' ORDER BY AccountNo";
                //     }

                //     if(member.IsActive)
                //         sqlCmd = @"SELECT AccountNo, UniqueID, FirstName, [Surname/Organisation] AS Surname FROM Recipients where belongsto = @personID ORDER BY AccountNo";
                    
                //     using(SqlCommand cmd = new SqlCommand(sqlCmd,(SqlConnection) conn))
                //     {
                //         await conn.OpenAsync();
                //         cmd.Parameters.AddWithValue("@personID", member.PersonId);

                //         List<dynamic> list = new List<dynamic>();
                            
                //         var rd = await cmd.ExecuteReaderAsync();
                //         while(await rd.ReadAsync())
                //         {
                //             list.Add(new {
                //                 AccountNo = GenSettings.Filter(rd["AccountNo"]),
                //                 UniqueID = GenSettings.Filter(rd["UniqueID"]),
                //                 FirstName = GenSettings.Filter(rd["FirstName"]),
                //                 Surname = GenSettings.Filter(rd["Surname"])
                //             });
                //         }
                //         return Ok(list);
                //     }
                // }
            } catch(Exception ex){
                return BadRequest(ex);
            }
        }
        
    }


    public class MemberList {
        public string PersonId { get; set; }
        public bool IsActive { get; set; }
    }
}
