import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, enableProdMode } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DatePipe, HashLocationStrategy, LocationStrategy, APP_BASE_HREF, CurrencyPipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { JwtModule } from '@auth0/angular-jwt';
import { BsModalModule } from 'ng2-bs3-modal';
import { ScrollingModule } from '@angular/cdk/scrolling'

import { AppComponent } from './app.component';

// import { NavMenuComponent } from './nav-menu/nav-menu.component';
// import { HomeComponent } from './home/home.component';
// import { CounterComponent } from './counter/counter.component';
// import { FetchDataComponent } from './fetch-data/fetch-data.component';

import { ToastrModule } from 'ngx-toastr';

import { RouteModule, components } from './app.routes';
import { AuthService,
         LoginService, 
         GlobalService,
         ClientService,
         StaffService,
         ReportService,
         EmailService,
         RouteGuard,
         LoginGuard,
         TimeSheetService,
         ListService,         
         UploadService,
         MemberService,
         ExecutableService,
         LandingService,
         JobService,
         CacheService,
         ScriptService,
         FileService
        } from './services/index';
         
import { SwitchService } from './services/switch.service';

 import { TabService } from './services/tabs.service';

import { SharedService } from './shared/sharedService/shared-service';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DayManagerModule } from './shared/daymanager/dm.module';
import { DraggableModule } from './shared/draggable/draggable.module';
import { RosterTimelineModule } from './shared/roster/roster.module';
import { TableMarkModule } from './shared/tablemark/tablemark.module';
import { TableModule } from './shared/table-nested/table.module';

import { DayScheduleModule } from './shared/dayschedule/dayschedule.module'
// import { WjInputModule } from '@grapecity/wijmo.angular2.input';


import { WjViewerModule } from 'wijmo/wijmo.angular2.viewer';
import { SelectListComponent } from './shared/select-list/select-list.component';
import { SelectListMultipleComponent } from './shared/select-list-multiple/select-list-multiple.component';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';


import { FullCalendarModule } from '@fullcalendar/angular';
import { ClarityModule } from "@clr/angular";
          
import { AppInterceptor } from './interceptor/app.interceptor';
import { MediaComponent } from './shared/media/media.component';
import { DropdownReportComponent } from './shared/dropdown-report/dropdown-report.component';
import { RecipientPopupComponent } from './shared/recipient-popup/recipient-popup.component';
import { ImgCropperComponent } from './shared/img-cropper/img-cropper.component';
import { ModalDetailsComponent } from './shared/modal-details/modal-details.component';
import { AddReferralComponent } from './shared/add-referral/add-referral.component';
import { InputLettersComponent } from './shared/input-letters/input-letters.component';
import { PhonefaxComponent } from './shared/phonefax/phonefax.component';
import { ReferInComponent } from './shared/refer-in/refer-in.component';
import { DocumentQuoteComponent } from './shared/document-quote/document-quote.component';
import { SelectCustomComponent } from './shared/select-custom/select-custom.component';
import { LoginSessionsComponent } from './pages/login-sessions/login-sessions.component';
import { DocumentEditQuoteComponent } from './shared/document-edit-quote/document-edit-quote.component';
import { ReferComponent } from './shared/refer/refer.component';

export function tokenGetter() 
{
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    components,
    SelectListComponent,
    SelectListMultipleComponent,
    MediaComponent,
    DropdownReportComponent,
    RecipientPopupComponent,
    ImgCropperComponent,
    ModalDetailsComponent,
    AddReferralComponent,
    InputLettersComponent,
    PhonefaxComponent,
    ReferInComponent,
    DocumentQuoteComponent,
    SelectCustomComponent,
    LoginSessionsComponent,
    DocumentEditQuoteComponent,
    ReferComponent
  ],
  imports: [    
    NgxDatatableModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    RouteModule,
    BsModalModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      autoDismiss: true,
      maxOpened: 5
    }),
    NgbModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    DayManagerModule.forRoot(),
    RosterTimelineModule.forRoot(),
    DraggableModule,
    TableMarkModule.forRoot(),
    TableModule.forRoot(),
    DayScheduleModule.forRoot(),
    ScrollingModule,
    FullCalendarModule,
    ClarityModule,
    WjViewerModule,
    InfiniteScrollModule
    // WjInputModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    AuthService,
    LoginService,
    GlobalService,
    ClientService,
    StaffService,
    TimeSheetService,
    UploadService,
    MemberService,
    ExecutableService,
    SharedService,
    ReportService,
    RouteGuard,
    LoginGuard,
    DatePipe,
    CurrencyPipe,
    EmailService,
    ListService,
    SwitchService,
    LandingService,
    TabService,
    JobService,
    CacheService,
    ScriptService,
    FileService,
    {
      provide: LocationStrategy, 
      useClass: HashLocationStrategy
    },
    {provide: APP_BASE_HREF, useValue: ''}
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
