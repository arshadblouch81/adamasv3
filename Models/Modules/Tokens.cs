﻿using System.Collections.Generic;
using Xero.NetStandard.OAuth2.Models;

namespace AdamasV3.Models.Modules
{
    public class Tokens
    {
        public string IdToken { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public List<Tenant> Tenants { get; set; }
    }
}
