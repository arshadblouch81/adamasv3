import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input, Sanitizer } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-show-award-list',
    templateUrl: './awardList.component.html',
    styles: [`
`]
})

export class AwardListComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    modalOpen: boolean = false;
    showAwardSetup: any;
    
    listData: Array<any>;
    loading: boolean = false;
    inputForm: FormGroup;
    check: boolean = false;
    userRole: string = "userrole";
    title: string = "Add or Change Award Rule Sets"
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    columnstoSee: any = true;
    bodystyle: object;
    private unsubscribe: Subject<void> = new Subject();
    tmpValue: any;
    confirmModal?: NzModalRef;

    constructor(
        private globalS: GlobalService,
        private timeS: TimeSheetService,
        private menuS: MenuService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private modal: NzModalService,
        private host: ElementRef<Element> = inject(ElementRef),
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
    ) { }

    enterFullscreen() {
        this.host.nativeElement.requestFullscreen();
    }
    exitFullscreen() {
        document.exitFullscreen();
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.buildForm();
        this.loadData();
        this.loading = false;
        this.modalOpen = true;
    }


    loadTitle() {
        return this.title;
    }

    handleCancel() {
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/configuration']);
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            code: '',
            description: '',
            category: '',
            recordNumber: null,
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    loadData() {
        this.loading = true;
        this.billingS.getAwardRuleList().subscribe(getListDetail => {
            this.listData = getListDetail;
            this.loading = false;
        });
    }

    delete(data: any) {
        const group = this.inputForm;
        this.confirmModal = this.modal.confirm({
            nzTitle: 'Do you want to Delete?',
            nzContent: 'Are you sure you want to delete the Award Rule set "' + data.code + '" ?',
            nzOnOk: () => {
                this.tmpValue = data.code;
                this.billingS.deleteAwardRuleSet(data.recordNo)
                    .pipe(takeUntil(this.unsubscribe)).subscribe(dataDeleted => {
                        if (dataDeleted) {
                            this.billingS.postAuditHistory({
                                Operator: this.tocken.user,
                                actionDate: this.globalS.getCurrentDateTime(),
                                auditDescription: 'DELETE AWARD ' + data.code,
                                actionOn: 'AWARDPOS',
                                whoWhatCode: data.recordNo, //inserted
                                TraccsUser: this.tocken.user,
                            }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                                this.globalS.sToast('Success', 'Award Rule set "' + this.tmpValue + '" has been deleted successfully.');
                            }
                            );
                            this.loadData();
                            return;
                        }
                    });
            },
            nzOnCancel: () => {
                // this.globalS.sToast('Success', 'CANCEL Button Selected.')
            }
        });
        this.tmpValue = "";
    }

    showEditModal(index: any) {
        this.showAwardSetup = {}

        // this.title = "Edit Award Levels"
        // this.isUpdate = true;
        // this.current = 0;
        // this.modalOpen = true;
        // const { 
        //   name,
        //   end_date,
        //   recordNumber,
        // } = this.tableData[index];
        // this.inputForm.patchValue({
        //   name: name,
        //   end_date:end_date,
        //   recordNumber:recordNumber,
        // });
        // this.temp_title = name;
    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
    }
    generatePdf(){
      
        this.drawerVisible = true;
        this.loading = true;
        
        var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY RecordNo) AS Field1 , Code AS Field2, Description AS Field3, Category AS Field4 FROM AwardPos";
        
        const data = {
          "template": { "_id": "0RYYxAkMCftBE9jc" },
          "options": {
            "reports": { "save": false },
            "txtTitle": "Award Rules List",
            "sql": fQuery,
            "userid":this.tocken.user,
            "head1" : "Sr#",
            "head2" : "Code",
            "head3" : "Description",
            "head4" :"Category",
          }
        }
  
              this.printS.printControl(data).subscribe((blob: any) => {
          let _blob: Blob = blob;
          let fileURL = URL.createObjectURL(_blob);
          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
          this.loading = false;
          this.pdfTitle = "Award Rules List.pdf"
          }, err => { 
          console.log(err);
          this.loading = false;
          this.ModalS.error({
            nzTitle: 'TRACCS',
            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
            nzOnOk: () => {
              this.drawerVisible = false;
            },
          });
        });
        
        //this.loading = true;
        this.tryDoctype = "";
        this.pdfTitle = "";
      }
}
