using System;

namespace Adamas.Models.Modules
{
    public class RollbackInvoiceBatch {
      public string OperatorID { get; set; }
      public DateTime CurrentDateTime { get; set; }
      public string BatchNumber { get; set; }
      public string BatchDescription { get; set; }

    }
}