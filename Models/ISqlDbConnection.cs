using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Adamas.Models;

namespace Adamas.Models{
    public interface ISqlDbConnection
    {
        SqlConnection GetConnection();
    }
}