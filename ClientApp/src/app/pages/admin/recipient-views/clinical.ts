import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  OnChanges,
  DoCheck,
  SimpleChanges,
  AfterContentChecked, AfterViewChecked
} from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  styles: [`
  
  ul{
        list-style:none;
    }
    
    div.divider-subs div{
        margin-top:2rem;
    }
    nz-divider{
        margin: 0;
    }
    p{
        margin: 0;
        cursor:pointer;
        padding:10px;
        height:50px;
        border: 1px solid #f3f3f3;
        font-size: 16px;
        font-family: 'Segoe UI';
        font-weight: 300;
    }
  .active-tab{
        background: #85B9D5;
        color: #fff;
        padding:10px;
        font-weight: 600;
    }
  
  .tabs-blocks{
    
  }
  
  `],
  templateUrl: './clinical.html'
})


export class RecipientClinicalAdmin implements OnInit, OnChanges, DoCheck, AfterContentChecked, AfterViewChecked, OnDestroy {
  private unsubscribe: Subject<void> = new Subject();
  
  physical_health: Array<any> = [
      {
        "link": "physical-health",
        "title": "Physical Health"
     },
      {
          "link": "health-conditions",
          "title": "Health Conditions"
      },
      {
          "link": "nursing-diagnose",
          "title": "Nursing Diagnoses"
      },
      {
          "link": "medical-diagnose",
          "title": "Medical Diagnoses"
      },
      {
          "link": "procedure",
          "title": "Procedures"
      },
      {
          "link": "vaccinations",
          "title": "Vaccinations"
      }
  
  
  ];
  
  clinical_history: Array<any> = [
    {
      "link":'history',
      "title":'Clinical History',
    }
  ];
  
  primary_issues: Array<any> = [
    
      {
        "link": "primary-issues",
        "title": "Primary Issues"
      },
      {
        "link": "other-issues",
        "title": "Other Issues"
      },
      {
        "link": "current-services",
        "title": "Current Services"
      },
      {
        "link": "daily-living",
        "title": "Daily Living"
      },
      {
        "link": "career-status",
        "title": "Carer Status"
      },
      {
        "link": "action-plans",
        "title": "Action Plan"
      }
    
    
  ];
  
  health_behaviours: Array<any> = [
    {
      "link":'health-behaviours',
      "title":'Health Behaviours',
    },
    {
      "link":'mental-health',
      "title":'Mental Health',
    },
  ];
  
  admin_and_progress_notes: Array<any> = [
    {
      "link":'reminder',
      "title":'Reminders',
    },
    {
      "link":'alert',
      "title":'Alerts',
    },
    {
      "link":'note',
      "title":'NOTES',
    },
  ];
  
  tabs: Array<any> = [];
  
  constructor(
    private timeS: TimeSheetService,
    private sharedS: ShareService,
    private listS: ListService,
    private router: Router,
    ) {
      this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data instanceof NavigationEnd) {
          if (!this.sharedS.getPicked()) {
            this.router.navigate(['/admin/recipient/personal'])
          }
        }
      });
    }
    
    ngOnInit(): void {
      const history = /^\/admin\/recipient\/clinical\/history(\/.*)?$/;
      const healthConditions = /^\/admin\/recipient\/clinical\/physical-health(\/.*)?$/;
      const primaryIssues = /^\/admin\/recipient\/clinical\/primary-issues(\/.*)?$/;
      const AdminProcessNotes = /^\/admin\/recipient\/clinical\/reminder(\/.*)?$/;
      const HealthTabs = /^\/admin\/recipient\/clinical\/health-behaviours(\/.*)?$/;
      this.tabs = this.clinical_history;

      if(healthConditions.test(this.router.url)) {
        this.tabs = this.physical_health;
      } else if(primaryIssues.test(this.router.url)) {
        this.tabs = this.primary_issues;
      } else if(history.test(this.router.url)){
        this.tabs = this.clinical_history;
      } else if(AdminProcessNotes.test(this.router.url)) {
        this.tabs = this.admin_and_progress_notes;
      } else if(HealthTabs.test(this.router.url)) {
        this.tabs = this.health_behaviours;
      }
      
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          
          if(healthConditions.test(this.router.url)) {
            this.tabs = this.physical_health;
          } else if(primaryIssues.test(this.router.url)) {
            this.tabs = this.primary_issues;
          } else if(history.test(this.router.url)){
            this.tabs = this.clinical_history;
          } else if(AdminProcessNotes.test(this.router.url)) {
            this.tabs = this.admin_and_progress_notes;
          } else if(HealthTabs.test(this.router.url)) {
            this.tabs = this.health_behaviours;
          }else{
           // this.tabs = this.clinical_history //this.primary_issues;
          }
        }
      })
    }

    first_call(){
    
    }
    
    ngOnDestroy(): void {
    }
    
    ngDoCheck(): void {
    }
    
    ngOnChanges(changes: SimpleChanges): void {
    }
    
    ngAfterContentChecked(): void {
    }
    
    ngAfterViewChecked(): void {
    }
    
    
  }
  