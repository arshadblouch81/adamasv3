
using System;
namespace Adamas.Models.Modules{
    public class CaseNote {
        public DateTime? DetailDate { get; set; }
        public string PersonID { get; set; }
        public string Creator { get; set ; }
        public string Detail { get; set; }
        public string ExDetails { get; set; }
        public string WhoCode { get; set; }
    }
}