using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules
{
    public class Configfollowups
    {
        public int? RecordNumber { get; set; } = 0;

        public string Group {get;set;}
        public string Name { get; set; }
        public string Activity { get; set; }
        public string Branch { get; set; }
        public string FundingSource { get; set; }
        public string Casemanager{get;set;}
        public string Staff{get;set;}
        public List<String> selectedStaff {get;set;}
        public DateTime? EndDate { get; set; }
    }
}
