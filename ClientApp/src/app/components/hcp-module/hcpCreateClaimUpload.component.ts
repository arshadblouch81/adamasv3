import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
// import { ListService, MenuService } from '@services/index';
import { BillingService, TimeSheetService, LoginService, GlobalService, ListService, MenuService, DllService } from '@services/index';
import { UploadFile, UploadChangeParam } from 'ng-zorro-antd/upload';
import { debounceTime, timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { NgModule } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BrowserModule } from '@angular/platform-browser';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMessageModule } from 'ng-zorro-antd/message';

import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-hcp-create-claim-upload',
  templateUrl: './hcpCreateClaimUpload.component.html',
  styleUrls: ['./hcpCreateClaimUpload.component.css'],
})

// .ant-divider, .ant-divider-vertical {
//     position: relative;
//     top: 10px;
// }

export class HCPCreateClaimUpload implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;
  operatorID: any;
  current: number = 0;
  inputForm: FormGroup;
  loading: boolean = false;
  token: any;
  tocken: any;
  check: boolean = false;
  userRole: string = "userrole";
  id: string;
  private unsubscribe: Subject<void> = new Subject();
  dateFormat: string = 'dd/MM/yyyy';
  tableData: Array<any>;
  updatedRecords: any;

  endDate: any;
  startDate: any;
  currentDateTime: any;

  branchList: Array<any>;
  selectedBranch: any;
  exportFormat: Array<any>;
  selectedExportFormat: any;
  serviceProviderID: Array<any>;
  selectedProviderID: any;
  exportFilePath: any;
  logFilePath: any;
  confirmModal?: NzModalRef;

  recipientsList: Array<any>;
  selectedRecipients: any;
  allRecipientsChecked: boolean;
  lockRecipients: any = false;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private loginS: LoginService,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    private DllServiceS: DllService,
    private modal: NzModalService,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    this.loading = false;
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
    this.isVisible = false;
    this.router.navigate(['/admin/hcp']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      startDate: '12/01/2021',
      endDate: '12/31/2021',
      branchList: 'ALL',
      exportFormat: '',
      serviceProviderID: '',
      chkUnapproved: false,
      chkApproved: false,
      chkBilled: false,
    });

    this.exportFormat = ['CDCCLAIMUPLOAD'];

    var now = new Date();

    // if (now.getMonth() == 11) {
    //     this.startDate = new Date(now.getFullYear() + 1, 0, 1);
    //     this.endDate = new Date(now.getFullYear() + 1, 5, 1);
    // } else {
    //     this.startDate = new Date(now.getFullYear(), now.getMonth() + 1, 1);
    //     this.endDate = new Date(now.getFullYear(), now.getMonth() + 6, 1);
    // }

    this.startDate = new Date(now.getFullYear(), now.getMonth(), 1);
    this.endDate = endOfMonth(new Date());
    this.startDate = formatDate(this.startDate, 'MM/dd/yyyy', 'en_US');
    this.endDate = formatDate(this.endDate, 'MM/dd/yyyy', 'en_US');
    this.inputForm.patchValue({
      startDate: this.startDate,
      endDate: this.endDate,
    });

    let dataPass = {
      CurrentDate: formatDate(this.globalS.getCurrentDateTime(), 'MM-dd-yyyy', 'en_US')
    }

    //getServiceProviderID
    this.billingS.getServiceProviderID(null).subscribe(data => {
      this.serviceProviderID = data;
    });

    //getBranchesDetail
    this.billingS.getBranchesDetail(null).subscribe(data => {
      this.branchList = data;
    });

    //loadRecipients
    this.billingS.getRecipientsTypeDoha(dataPass).subscribe(data => {
      this.recipientsList = data;
      this.tableData = data;
      this.allRecipientsChecked = true;
      this.checkAll(1);
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  //=====================================================================

  beforeUpload = (file: UploadFile): boolean => {
    // Optionally process the file before upload
    return false; // Return false to prevent automatic upload
  };

  handleChange(info: UploadChangeParam): void {
    debugger;
    const file = info.file;
    if (!file) {
      this.getFilePath(file);
      console.error('File object is undefined');
      return;
    }

    if (file.status !== 'uploading') {
      console.log(file);
    }

    if (file.status === 'done') {
      console.log(`${file.name} file uploaded successfully.`);
      this.getFilePath(file);
    } else if (file.status === 'error') {
      console.log(`${file.name} file upload failed.`);
    }
  }

  getFilePath(file: UploadFile): void {
    if (!file) {
      console.error('File object is undefined');
      return;
    }
    // Get the file path
    this.exportFilePath = file.url || file.originFileObj?.name;
    console.log('File Path:', this.exportFilePath);
  }

  // fileList: any[] = [];
  // beforeUpload = (file: any): boolean => {
  //   console.log(file);
  //   this.fileList = this.fileList.concat(file);
  //   return false;
  // }

  onClick(e) {
    console.log(e.files)
    console.log(e.fileList)
  }
  //=====================================================================

  runProcess() {
    this.loading = true;
    this.operatorID = this.token.nameid;
    this.startDate = this.inputForm.get('startDate').value;
    this.endDate = this.inputForm.get('endDate').value;
    this.startDate = formatDate(this.startDate, 'yyyy/MM/dd', 'en_US');
    this.endDate = formatDate(this.endDate, 'yyyy/MM/dd', 'en_US');
    this.selectedBranch = this.inputForm.get('branchList').value;
    this.selectedExportFormat = this.inputForm.get('exportFormat').value;
    this.selectedProviderID = this.inputForm.get('serviceProviderID').value;

    if (this.lockRecipients == false) {
      this.selectedRecipients = null
    } else {
      this.selectedRecipients = this.recipientsList
        .filter(opt => opt.checked)
        .map(opt => opt.recipientTitle).join(",")
    }

    if (this.startDate != ''
      && this.endDate != ''
      && this.selectedBranch != null
      && this.selectedExportFormat != null
      && this.selectedProviderID != ''
      && this.selectedProviderID != null
      && this.selectedRecipients != ''
      && this.exportFilePath != ''
    ) {
      this.processExecution();
    } else if (this.startDate == '') {
      this.globalS.eToast('Error', 'Please select Start Date.')
    } else if (this.endDate == '') {
      this.globalS.eToast('Error', 'Please select End Date.')
    } else if (this.selectedBranch == null) {
      this.globalS.eToast('Error', 'Please select Branch.')
    } else if (this.selectedExportFormat == null) {
      this.globalS.eToast('Error', 'Please select Export Format.')
    } else if (this.exportFilePath == null) {
      this.confirmModal = this.modal.confirm({
        nzTitle: 'Export File Path Requried',
        nzContent: 'You have nominated an Exporting interface - but you have not filled in the field that tells what location and name to store the export file. Click on the Exporting Interface TAB and choose a valid location and name for the export file.',
        nzOnOk: () => {
          // this.globalS.iToast('Information', 'OK Button Selected.')
        },
        nzOnCancel: () => {
          // this.globalS.sToast('Success', 'CANCEL Button Selected.')
        }
      });
    } else {
      this.globalS.eToast('Error', 'Please select Service Provider ID.')
    }
    this.loading = false;
  }

  processExecution() {
    console.log('success');
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
  }

  processExecution1() {
    this.loading = true;
    this.DllServiceS.HCPBalanceExport({
      User: this.operatorID,
      Password: this.operatorID,
      WindowsUer: this.operatorID,
      Batch: this.selectedBranch,
      StartDate: this.startDate,
      EndDate: this.endDate,
      ExportLocation: this.exportFilePath,
      LogFilePath: this.logFilePath
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
          } else {
            this.globalS.sToast('Success', 'Sales Journal Export Records Updated')
            // this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Debtor Run is: ' + this.operatorID)
          }
          this.loading = false;
          this.ngOnInit();
          return false;
        }
      });
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedRecipients = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allRecipientsChecked == false) {
        this.lockRecipients = true
      }
      else {
        this.recipientsList.forEach(x => {
          x.checked = true;
          this.allRecipientsChecked = x.description;
          this.allRecipientsChecked = true;
        });
        this.lockRecipients = false
      }
    }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockRecipients = true;
      this.recipientsList.forEach(x => {
        x.checked = false;
        this.allRecipientsChecked = false;
        this.selectedRecipients = [];
      });
    }
  }
}

