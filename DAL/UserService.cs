﻿using DocumentFormat.OpenXml.Office2021.DocumentTasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Adamas.DAL
{
    public class UserService : IUserService
    {
        private readonly IHttpContextAccessor httpContext;
        private readonly IWebHostEnvironment webHost;
        public UserService(
            IHttpContextAccessor _httpContext,
            IWebHostEnvironment _webHost
            ) { 
            httpContext= _httpContext;
            webHost= _webHost;
        }

        public async Task<List<Claim>> GetUserClaims()
        {

            var claims = httpContext.HttpContext.User.Claims.ToList();
            return claims;
        }

    }
}
