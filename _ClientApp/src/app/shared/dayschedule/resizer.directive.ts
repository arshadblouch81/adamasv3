import { Directive, AfterViewInit, NgZone, ElementRef, Inject, HostListener, Output, EventEmitter, Renderer2, OnInit  } from '@angular/core'
import { DOCUMENT } from '@angular/common'

import { Subject } from 'rxjs'
import { switchMap, takeUntil, take, repeat } from 'rxjs/operators';

interface Position{
    x: number,
    y: number
}

@Directive({
    selector: '[resizer]',
})

export class ResizerDirective implements OnInit, AfterViewInit {

    @Output() dropEmit = new EventEmitter<any>();

    @Output() dragStart = new EventEmitter<any>();
    @Output() dragMove = new EventEmitter<any>();
    @Output() dragEnd = new EventEmitter<any>();

    drag: boolean = false

    private pointerDown = new Subject<PointerEvent>();
    private pointerMove = new Subject<PointerEvent>();
    private pointerUp = new Subject<PointerEvent>();

    position: Position = { x:0, y:0 }
    private startPosition: Position

    elementNow: any;
    starting: any;
    coordinates: Array<any> = []

    constructor(
        private renderer: Renderer2,
        private ngZone: NgZone,
        private el: ElementRef,
        @Inject(DOCUMENT) private document
    ){

    }
    unique;
    hoy;
    dyus;

    ngOnInit(){
        this.pointerDown
            .asObservable()
            .subscribe((event:any) =>{
                this.startPosition = {
                    x: event.clientX - this.position.x,
                    y: event.clientY - this.position.y
                }   

                this.hoy =  Math.abs(event.target.closest('.timedata').style.bottom.split('px')[0])
                for(var i in this.coordinates){
                    if(event.clientX >= this.coordinates[i].left && event.clientX <= this.coordinates[i].right){
                        if(event.clientY > this.coordinates[i].top && event.clientY < this.coordinates[i].bottom){                
                            this.dyus = Math.floor(this.coordinates[i].top);
                        }
                    }
                }
                this.elementNow  = event.target.closest('.timedata')
            })

    
        this.pointerDown
            .pipe(
                switchMap(() => this.pointerMove),
                takeUntil(this.pointerUp),
                repeat()
            ).subscribe((event: any) => {       
            
                this.position.y = event.clientY - this.startPosition.y           
            
                for(var i in this.coordinates){
                    if(event.clientX >= this.coordinates[i].left && event.clientX <= this.coordinates[i].right){
                        if(event.clientY > this.coordinates[i].top && event.clientY < this.coordinates[i].bottom){                   
                            
                            if(this.unique !== Math.floor(this.coordinates[i].top - 8)){
                                var perHeight = (this.unique-this.dyus)/28;
                                if(!isNaN(perHeight)){                                
                                    var total = this.hoy + perHeight * 28;                                
                                    this.elementNow.style.bottom = '-' + total + 'px' 
                                }
                                this.unique = Math.floor(this.coordinates[i].top)
                            }
                        
                        }
                    }
                }
           
           
        })

        this.pointerDown.pipe(
            switchMap(() => this.pointerUp),
            take(1),
            repeat()
        ).subscribe(event => {
            this.position =  { x:0, y:0 }           
        })
    }

    ngAfterViewInit(){
        const timeRows: any = document.getElementsByClassName('time');
        for(let row of timeRows){
            var bounding = row.getBoundingClientRect();
            this.coordinates.push({
                dom: row,
                left: bounding.left,
                top: bounding.top,
                right: bounding.right,
                bottom: bounding.bottom
            })
        }

        var self = this

        this.ngZone.runOutsideAngular(() => {

            this.el.nativeElement.addEventListener('pointerdown', function(event: any){
                self.pointerDown.next(event)
            })

            document.addEventListener('pointermove', function(event: any){
                self.pointerMove.next(event);
            })

            document.addEventListener('pointerup', function(event: any){
                self.pointerUp.next(event);
            })

        })
        
    }


}