
import { takeUntil, switchMap } from 'rxjs/operators';
import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { TimeSheetService, GlobalService, StaffService, JobService } from '@services/index';
import { incidentSeverity, incidentTypes, leaveTypes } from '../../services/global.service';

import { BsModalComponent } from 'ng2-bs3-modal';
import { Subject } from 'rxjs'


import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
    selector: '',
    templateUrl: './package.html',
    styles: [
        `

        @media (min-width:320px), (min-width:480px)  {   
            .date-period .form-group{
                display:flex;
                font-size:12px;
                margin-bottom:0;
            }  
            .date-period .form-group label{
                margin-right:8px;
            }

            i.options{
                position: absolute;
                top: 0;
                right: 0;
                margin: 10px;
            }
            .web-first{
                display:none;
            }
            .mobile-first{
                display:block;
            }
        
            .pp{
                font-size: 10px;
                font-weight: 600;
                color: #626060;
            }
        
            .pp i{
                float:left;
                font-size: 18px;
                margin-top: 3px;
                margin-right:10px;
            }
        
            /* The actual timeline (the vertical ruler) */
            .timeline {
            position: relative;
            max-width: 1200px;
            margin: 0 auto;
            }
        
            /* The actual timeline (the vertical ruler) */
            .timeline::after {
                content: '';
                position: absolute;
                width: 3px;
                background-color: #689284;
                top: 17px;
                bottom: 0;
                left: 50%;
                margin-left: -5px;
            }
        
        
            .container {
                padding: 10px 40px;
                position: relative;
                background-color: inherit;
                width: 50%;
            }
        
        
            .container::after {
                content: '';
                position: absolute;
                width: 20px;
                height: 20px;
                right: -17px;
                background-color: #626060;
                top: 15px;
                border-radius: 50%;
                z-index: 1;
            }
        
        
            .left {
            left: 0;
            }
        
        
            .right {
            left: 50%;
            }
        
        
            .left::before {
                content: " ";
                height: 0;
                position: absolute;
                top: 22px;
                width: 0;
                z-index: 1;
                right: 30px;
                border: medium solid white;
                border-width: 10px 0 10px 10px;
                border-color: transparent transparent transparent white;
            }
        
        
            .right::before {
                content: " ";
                height: 0;
                position: absolute;
                top: 22px;
                width: 0;
                z-index: 1;
                left: 30px;
                border: medium solid white;
                border-width: 10px 10px 10px 0;
                border-color: transparent white transparent transparent;
            }
        
            .right::after {
                left: -16px;
            }
        
        
            .content {
                padding: 10px 12px;
                background-color: #ffffff;
                color: #787878;
                position: relative;
                border-radius: 3px;
                box-shadow: 0 3px 6px rgb(37, 142, 108), 0 3px 6px rgba(0,0,0,0.23);
            }
        
            @media screen and (max-width: 600px) {
            /* Place the timelime to the left */
            .timeline::after {
                left: 10px;
            } 
            
        
            .container {
                width: 100%;
                padding-left: 30px;
                padding-right: 15px;
            }
            
        
            .container::before {
                left: 20px;
                top: 17px;
                border-width: 10px 10px 10px 0;
                border-color: transparent #689284 transparent transparent;
            }
        
        
            .left::after, .right::after {
            left: -3px;
            }
            
        
            .right {
            left: 0%;
            }

        }


        @media (min-width:801px) { 
            .web-first{
                display:block;
            }
            .mobile-first{
                display:none;
            }
            .total-time{
                display:block;
            }

        }



        h4{
            margin: 0;
            font-weight: 600;
            color: #37876d !important;
        }

        .form-group label{
            display:block;
        }
        
        .tclaim i{
            float:left;
            margin-right:0.5rem;
        }
        .tclaim span.option{
            width: 6rem;
            justify-content: center;
            text-align:center;
            display: flex;
            border-radius: 4px;
            font-size: 11px;
            align-items: center;
        }
        span.logo-period i{
            float:left;
            font-size:20px;
            margin-right:10px;
        }
        .odometer{
            position: relative;
            display: inline-block;
            background: #306686;
            margin-right: 5px;
            color: #fff;
            border-radius: 3px;
            padding: 0 5px;
            font-size:12px;
        }
        .odometer i{
            float: left;
            font-size: 15px;
            margin: 4px 5px 0 0;
        }
        th i{
            font-size: 17px;
            float: left;
        }
        th i.desc {
            transform: rotate(180deg);
        }
        .toggle-switch{
            margin:0 auto;
        }
        span.pref{
            background: #6cffbb;
        }
        .tclaim{
            margin:3px 0;
            display:flex;
        }
        .tclaim-var{
            margin-top:2rem;
        }
        textarea{
            resize:none;
        }

        table.table td{
            font-size:12px;
        }
        .table-height{
            height:70vh !important;
        }
        tr > td{
            vertical-align:middle;
        }
        th{
            font-size: 12px;
            background: linear-gradient(rgb(90, 234, 188), rgb(79, 208, 166));
            color: #fff;
            font-weight: normal;
        }
        td:nth-child(6){
            background:#f2f4f3;
        }
        td:nth-child(4),td:nth-child(5){
            background:#eefeff;
        }
        td:nth-child(7),td:nth-child(8){
            background:#e2fff5;
        }
        tbody tr:hover > td{
            background:#fff9af !important;
        }
        .sub-unsub-btn{
            padding: 2px 5px;
            color: #fff;
            border-radius: 3px;
        }
        .sub-unsub-btn:hover{
            cursor:pointer;
        }
        .submit{
            background: #48c373;
        }
        .submit:hover{
            background:#3aad62;
        }
        .unsubmit{
            background: #e66161;
        }
        .unsubmit:hover{
            background: #c54e4e;
        }
        .travel{
            background: #debb3a;
        }
        .travel-time{
            background:#14a9c1;
        }
        .claim-container{
            margin:0;
        }
        .modal-title i {
            font-size: 25px;
            color: #1fbe00;
            float: left;
        }
        select{
            width:100%;
        }
        tr > th{
            -webkit-touch-callout: none;
            -webkit-user-select: none;
             -khtml-user-select: none;
               -moz-user-select: none; 
                -ms-user-select: none; 
                    user-select: none;
        }
        tr > th.clickable:hover{
            background:#34ce9c;
            cursor:pointer;
        }
        button{
            border:0;
        }
        table{
            margin:0;
        }
        action.options{
            position: absolute; 
            top: 0; 
            right: 0; 
            padding: 8px;
        }
        div.jobstart::before{
            border-color: transparent #ff3d75 transparent transparent;
        }
        div.jobstart::after{
            background: #ff3d75;
        }
        div.jobstart div.content{
            background:#ff3d75;
        }
        div.jobstart div.content > div{
            color:#fff !important;
        }
        div.jobfinish::before{
            border-color: transparent #1bc38d transparent transparent;
        }
        div.jobfinish::after{
            background:#1bc38d;
        }
        div.jobfinish div.content{
            background:#1bc38d;
        }
        div.jobfinish div.content > div {
            color:#fff !important;
        }
        
        div.jobfinish div.content div.ts-title > div.pp{
            color: #fff !important;
        }
        div.jobstart div.content div.ts-title > div.pp{
            color: #fff !important;
        }

        div.jobstart div.content h4{
            color:#fff !important;
        }
        div.jobfinish div.content h4{
            color:#fff !important;
        }
        .ts-title{
            margin-top:5px;
        }
        .ts-note{
            padding-left: 15px; 
            font-size: 11px;
            text-align:justify;
            line-height:14px;
        }
        `
    ]
})

export class PackageProvider implements OnInit, AfterViewInit, OnDestroy {
    private unsubscribe$ = new Subject<void>()

    @ViewChild('claimvariation', { static: false }) claimvariation: BsModalComponent;
    @ViewChild('scrollMe', { static: false }) private myScrollContainer: ElementRef;


    columns: any = {
        shift: false,
        date: false,
        recipient: false,
        activit: false,
        duration: false,
    }

    private token: any;
    index: number
    travelDefault: Dto.TravelDefaults;
    load: boolean = false

    duration: any

    show: boolean = false

    pickedAction: any

    settings: any
    payPeriod: any
    timesheets: Array<any> = []
    inTimesheets: Array<any>

    incidentTypes: Array<string> = incidentTypes;
    incidentSeverity: Array<string> = incidentSeverity;
    leaveTypes: Array<string> = leaveTypes;
    incidentLocation: any;

    totalTime: any;
    totalHoursMinutes: any;

    rosterIncidentForms = new FormGroup({
        boolRecipient: new FormControl(false),
        incidentDetail: new FormControl(),
        incidentType: new FormControl(),
        incidentSeverity: new FormControl(),
        incidentLocation: new FormControl()
    });

    location: {
        latitude: any,
        longitude: any
    }

    casenote: string;
    opnote: string;
    rosternote: string;
    clientnote: string;

    private tempSubmit: any;

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private staffS: StaffService,
        private jobS: JobService
    ) {
        this.incidentLocation = this.getIncidentLocation();

        navigator.geolocation.getCurrentPosition((loc) => {
            this.location = {
                latitude: loc.coords.latitude,
                longitude: loc.coords.longitude
            }
        })
    }


    ngOnInit() {
        this.token = this.globalS.decode();
        this.populate();
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    ngAfterViewInit() {
        this.timesheets = []
    }


    gwapo(data: any) {
        if (data.length == 0) return;
        this.timesheets = data;
        this.calculateTotalApprovedShifts(this.timesheets);
    }

    populate() {
        this.staffS.getpayperiod().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.payPeriod = {
                payperiod: moment(data.payPeriodEndDate).format('DD MMM YYYY'),
                sDate: moment(data.start_Date).format('DD MMM YYYY'),
                eDate: moment(data.end_Date).format('DD MMM YYYY')
            }
        });

        this.getTimesheetandStatus();

        this.staffS.getsettings(this.token.nameid).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.settings = data;
        });
    }

    getTimesheetandStatus() {
        this.timeS.gettimesheets({
            AccountNo: this.token.code,
            personType: 'Staff',
            startDate: '',
            endDate: ''
        }).pipe(
            takeUntil(this.unsubscribe$),
            switchMap(data => {
                // var dd:Array<any> = data;
                // data.forEach(x => dd.push(x));
                // data.forEach(x => dd.push(x));

                this.inTimesheets = data.map(x => {
                    return {
                        shiftbookNo: x.shiftbookNo,
                        activityDate: x.activityDate,
                        activityTime: x.activity_Time,
                        approved: x.approved,
                        start: x.activity_Time.start_time,
                        end: x.activity_Time.end_Time,
                        duration: x.activity_Time.calculated_Duration,
                        claimStart: x.claimed_Start,
                        claimEnd: x.claimed_End,
                        km: x.km,
                        startKM: x.start_KM,
                        endKM: x.end_KM,
                        recipient: x.client_Code,
                        activity: x.activity.name,
                        program: x.program.title,
                        ptype: x.payType.paytype,
                        pquant: x.pay.quantity,
                        rosterType: x.roster_Type,
                        submitted: x.submitted,
                        note: x.note,
                        billedTo: x.billedTo.accountNo,
                        status: 0
                    }
                });

                var shiftsList = []
                this.inTimesheets.forEach(x => {
                    shiftsList.push(x.shiftbookNo)
                })

                return this.timeS.getjobstatus(shiftsList);

            })).subscribe(data => {

                this.inTimesheets.forEach(x => {
                    data.forEach(b => {
                        if (x.shiftbookNo == b.jobNo && !b.isCompleted) {
                            x.status = 1;
                        }
                        if (x.shiftbookNo == b.jobNo && b.isCompleted) {
                            x.status = 2;
                        }
                    });
                })
                return data;
            });
    }

    calculateTotalApprovedShifts(timesheets: Array<any>) {
        const sample = timesheets.filter(data => data.approved);

        var sum = 0;
        sample.forEach(element => {
            const hours = moment(element.duration, ['HH:mm']).hours() * 60;
            const minutes = moment(element.duration, ['HH:mm']).minutes();
            sum = sum + hours + minutes;
        });

        this.totalTime = sum / 60;
        this.totalHoursMinutes = Math.floor(sum / 60) + ' hrs — ' + sum % 60 + 'min';
    }

    reset() {
        this.pickedAction = {
            title: '',
            option: ''
        }
    }

    timeDuration: string;

    computeTime() {
        var timeObj = this.globalS.computeTime(this.pickedAction.value.activityTime.start_time, this.pickedAction.value.activityTime.end_Time)
        this.timeDuration = timeObj.durationStr
    }

    confirmSubmit(data: any) {

        if (data.activityTime.duration === 0 && !data.submitted) {
            this.globalS.eToast('Error', 'Time Difference Error');
            return false;
        }

        this.tempSubmit = data;
        this.claimvariation.open();
    }

    submitClaim() {

        let data = this.tempSubmit;

        if (!data.submitted) {
            let variation: Dto.ClaimVariation = {
                RecordNo: data.shiftbookNo,
                ClaimedBy: this.token.nameid,
                ClaimedDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                ClaimedStart: moment(data.claimStart).year(1753).month(0).date(1).hours(moment(data.activityTime.start_time).hour()).minutes(moment(data.activityTime.start_time).minutes()).format('YYYY-MM-DD HH:mm:ss'),
                ClaimedEnd: moment(data.claimEnd).year(1753).month(0).date(1).hours(moment(data.activityTime.end_Time).hour()).minutes(moment(data.activityTime.end_Time).minutes()).format('YYYY-MM-DD HH:mm:ss')
            }
  
            this.staffS.postclaimvariation(variation).subscribe(res => {
                if (res) {
                    data.claimStart = moment(data.claimStart).year(1800).hours(moment(data.activityTime.start_time).hour()).minutes(moment(data.activityTime.start_time).minutes()).format('YYYY-MM-DD HH:mm:ss');
                    data.claimEnd = moment(data.claimEnd).year(1800).hours(moment(data.activityTime.end_Time).hour()).minutes(moment(data.activityTime.end_Time).minutes()).format('YYYY-MM-DD HH:mm:ss');
                    
                    console.log(data.claimStart);
                    console.log(data.claimEnd);
                    this.globalS.sToast("Schedule Submitted", "Success");
                    this.populate();
                }
            });
        } else {
            let variation: Dto.ClaimVariation = {
                RecordNo: data.shiftbookNo,
                ClaimedBy: null,
                ClaimedDate: moment(new Date()).year(1753).month(0).date(1).hours(12).minutes(0).seconds(0).milliseconds(0).format('YYYY-MM-DD HH:mm:ss'),
                ClaimedStart: moment(new Date()).year(1753).month(0).date(1).hours(12).minutes(0).seconds(0).milliseconds(0).format('YYYY-MM-DD HH:mm:ss'),
                ClaimedEnd: moment(new Date()).year(1753).month(0).date(1).hours(12).minutes(0).seconds(0).milliseconds(0).format('YYYY-MM-DD HH:mm:ss')
            }
        
            this.staffS.postclaimvariation(variation).subscribe(res => {
                if (res) {
                    data.claimStart = moment(data.claimStart).year(1800).hours(0).minutes(0).format('YYYY-MM-DD HH:mm:ss');
                    data.claimEnd = moment(data.claimEnd).year(1800).hours(0).minutes(0).format('YYYY-MM-DD HH:mm:ss');

                    console.log(data.claimStart);
                    console.log(data.claimEnd);
                    
                    this.globalS.sToast("Schedule Unsubmitted", "Success");
                    this.populate();
                }
            });
        }
        this.tempSubmit = null;
        this.claimvariation.close();
    }

    out_data(data: any) {
        this.reset();


        if (data.option == "start") {
            // this.timeS.getjobstatus([data.value.shiftbookNo])
            //     .subscribe(data => console.log(data))

            if (!this.location) {
                this.location = {
                    latitude: '',
                    longitude: ''
                }
            }
            this.jobS.poststartjob({
                RecordNo: data.value.shiftbookNo,
                Cancel: false,
                TimeStamp: moment().format(),
                Latitude: (this.location.latitude).toString(),
                Longitude: (this.location.longitude).toString(),
                Location: ''
            }).subscribe(data => {
                if (data) {
                    this.getTimesheetandStatus()
                }
            })
        }

        if (data.option == "end") {
            // this.timeS.getjobstatus([data.value.shiftbookNo])
            //     .subscribe(data => console.log(data))

            this.jobS.postendjob({
                RecordNo: data.value.shiftbookNo,
                Cancel: false,
                TimeStamp: moment().format(),
                Latitude: (this.location.latitude).toString(),
                Longitude: (this.location.longitude).toString(),
                Location: 'asdasd'
            }).subscribe(data => {
                if (data) {
                    this.getTimesheetandStatus()
                }
            })
        }

        if (data.option == 'tclaim') {
            const defaults = this.settings.tA_TRAVELDEFAULT;
            var non_charge = defaults.indexOf("CHARGEABLE") > -1 && defaults.indexOf("NON") > -1 ? false : true;
            var travel = defaults.indexOf("WITHIN") > -1 ? true : false;

            this.pickedAction = {
                title: 'Travel Claim',
                option: 'tclaim',
                value: _.cloneDeep(data.value)
            }

            this.travelDefault = {
                TravelType: travel,
                ChargeType: non_charge,
                VehicleType: false,
                StartKM: 0,
                EndKM: 0,
                Notes: ''
            }

        }

        if (data.option == 'claim') {
            this.pickedAction = {
                title: 'Claim Variation',
                option: 'claim',
                value: _.cloneDeep(data.value)
            }

        }

        if (data.option == 'rnote') {
            this.pickedAction = {
                title: 'Roster Note',
                option: 'rnote',
                value: _.cloneDeep(data.value)
            }
            this.rosternote = this.pickedAction.value.note || "";
        }

        if (data.option == 'rincident') {
            this.pickedAction = {
                title: 'Record Incident',
                option: 'rincident',
                value: _.cloneDeep(data.value)
            }
        }

        if (data.option == 'opnote') {
            this.pickedAction = {
                title: 'OP Note',
                option: 'opnote',
                value: _.cloneDeep(data.value)
            }
        }

        if (data.option == 'casenote') {
            this.pickedAction = {
                title: 'Case Note',
                option: 'casenote',
                value: _.cloneDeep(data.value)
            }
        }
        this.show = data.control;
    }

    getIncidentLocation() {
        return this.timeS.getincidentlocation();
    }

    save(data: any) {

        if (data.option == 'claim') {

            let claim: Dto.ClaimVariation = {
                RecordNo: data.value.shiftbookNo,
                ClaimedBy: this.token.nameid,
                ClaimedDate: moment().format('YYYY-MM-DDTHH:mm:ss'),
                ClaimedEnd: this.pickedAction.value.activityTime.end_Time,
                ClaimedStart: this.pickedAction.value.activityTime.start_time
            }

            console.log(claim)
            this.staffS.postclaimvariation(claim)
                .pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Claim Updated', 'Success');
                        this.populate();
                        this.show = false;
                        return false;
                    }
                    this.globalS.eToast('Update Error', 'Error');
                });
        }

        if (data.option == 'tclaim') {
            if (this.travelDefault.EndKM == 0 || this.travelDefault.Notes == '') {
                this.globalS.eToast('Missing Inputs', 'Inputs Required');
                return false;
            }

            if (this.travelDefault.StartKM > this.travelDefault.EndKM) {
                this.globalS.eToast('StartKM is higher than EndKM', 'Input Error');
                return false;
            }

            let travel: Dto.TravelClaim = {
                RecordNo: data.value.shiftbookNo,
                User: this.token.user,
                Distance: (this.travelDefault.EndKM - this.travelDefault.StartKM).toString(),
                TravelType: this.travelDefault.TravelType ? "TRAVEL WITHIN" : "TRAVEL BETWEEN",
                ChargeType: this.travelDefault.ChargeType ? "Chargeable" : "",
                StartKm: (this.travelDefault.StartKM).toString(),
                EndKm: (this.travelDefault.EndKM).toString(),
                Notes: this.travelDefault.Notes
            }

            this.staffS.posttravelclaim(travel).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                if (data.result) {
                    console.log(data);
                    this.globalS.sToast('Success', data.message);
                    this.populate();
                    return;
                }
            }, (error) => {
                this.globalS.eToast('Error', 'Unable to save travel claim due to missing defaults')
            });
        }

        if (data.option == 'rnote') {
            console.log(this.timesheets[this.index])
            this.timeS.updaterosternote({
                Id: data.value.shiftbookNo,
                Note: this.rosternote
            }).pipe(takeUntil(this.unsubscribe$))
                .subscribe(res => {
                    if (res) {
                        this.timesheets[this.index].note = this.rosternote;
                        this.globalS.sToast('Success', 'Roster Note Updated');
                    }
                })
        }

        if (data.option == 'rincident') {
            const form = this.rosterIncidentForms.value;
            this.rosterIncidentForms.reset();
            let recordIncident: Dto.RecordIncident = {

                PersonId: data.value.billedTo,
                IncidentType: form.incidentType,
                IncidentSeverity: form.incidentSeverity,
                Note: form.incidentDetail,
                Location: form.incidentLocation,
                NoRecipient: form.boolRecipient,

                RecipientCode: data.value.recipient,
                Program: data.value.program,
                Service: data.value.activity,
                Staff: this.token.code,
                OperatorId: this.token.nameid
            }

            this.timeS.addrecordincident(recordIncident)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Sucess', 'Record Incident added')
                    }
                })
        }

        if (data.option == 'opnote') {
            const selected = data.value;
            this.timeS.addclientnote({
                RecipientCode: selected.recipient,
                OperatorID: this.token.nameid,
                Note: this.opnote,
                NoteType: 'OPNOTE'
            }).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
                if (data) {
                    this.globalS.sToast('Sucess', 'OP Note added')
                }
            });
        }

        if (data.option == 'casenote') {
            const selected = data.value;
           
            this.timeS.addclientnote({
                RecipientCode: selected.recipient,
                OperatorID: this.token.nameid,
                Note: this.casenote,
                NoteType: 'CASENOTE'
            })
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Case Note added')
                    }
                })
        }
        setTimeout(() => {
            this.clearInputs();
        }, 500);
        this.show = false;
    }

    clearInputs(): void {
        this.casenote = '';
        this.opnote = '';
        this.rosternote = '';
        this.rosterIncidentForms.reset();
    }

    submitAll(data: Array<any>) {
        var unsubmitted: Array<any> = data.filter(data => {
            let timeStart = moment(data.claimStart).format("HH:mm");
            let timeEnd = moment(data.claimEnd).format("HH:mm");

            if (timeStart == '00:00' && timeEnd == '00:00')
                return data;
        });

        unsubmitted.forEach(data => {

            let variation: Dto.ClaimVariation = {
                RecordNo: data.shiftbookNo,
                ClaimedBy: this.token.nameid,
                ClaimedDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                ClaimedStart: moment(data.claimStart).year(1753).month(0).date(1).hours(moment(data.activityTime.start_time).hour()).minutes(moment(data.activityTime.start_time).minutes()).format('YYYY-MM-DD HH:mm:ss'),
                ClaimedEnd: moment(data.claimEnd).year(1753).month(0).date(1).hours(moment(data.activityTime.end_Time).hour()).minutes(moment(data.activityTime.end_Time).minutes()).format('YYYY-MM-DD HH:mm:ss')
            }
       
            this.staffS.postclaimvariation(variation).subscribe(res => {
                if (res) {
                    data.claimStart = moment(data.claimStart).year(1800).hours(moment(data.activityTime.start_time).hour()).minutes(moment(data.activityTime.start_time).minutes()).format('YYYY-MM-DD HH:mm:ss');
                    data.claimEnd = moment(data.claimEnd).year(1800).hours(moment(data.activityTime.end_Time).hour()).minutes(moment(data.activityTime.end_Time).minutes()).format('YYYY-MM-DD HH:mm:ss');
                    this.globalS.sToast("Schedule Submitted", "Success");

                    this.populate();
                }
            });
        });
    }

    sort(key: string) {

        if (key === 'shiftbookNo') {
            this.columns.shift = !this.columns.shift;
            const sortBy = this.columns.shift ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        if (key === 'activityDate') {
            this.columns.date = !this.columns.date;
            const sortBy = this.columns.date ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        if (key === 'recipient') {
            this.columns.recipient = !this.columns.recipient;
            const sortBy = this.columns.recipient ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        if (key === 'activity') {
            this.columns.activity = !this.columns.activity;
            const sortBy = this.columns.activity ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        if (key === 'duration') {
            this.columns.duration = !this.columns.duration;
            const sortBy = this.columns.duration ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        if (key === 'start') {
            this.columns.start = !this.columns.start;
            const sortBy = this.columns.start ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        if (key === 'end') {
            this.columns.end = !this.columns.end;
            const sortBy = this.columns.end ? 'desc' : 'asc';
            var sample = _.orderBy(this.timesheets, key, [sortBy]);
        }

        this.timesheets = [];
        this.timesheets = sample;
    }



}