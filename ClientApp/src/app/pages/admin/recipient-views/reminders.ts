import { Component, OnInit, OnDestroy, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, recurringInt, recurringStr,PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { Reminders } from '@modules/modules';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
    styles: [`
         nz-select{
            width:100%;
        }
        .spinner{
            margin:1rem auto;
            width:1px;
        } 
        .selected{
        background-color: #85B9D5;
        color: white;
    }
    nz-table th{
    font-weight: 600;
    font-family: 'Segoe UI';
    font-size: 14px;
    border: 1px solid #f3f3f3;
}
    `],
    templateUrl: './reminders.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class RecipientRemindersAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;

    checked: boolean = false;
    isDisabled: boolean = false;
    loading: boolean = false;
    isLoading: boolean = false;

    modalOpen: boolean = false;
    addOREdit: number;
    dateFormat: string = 'dd/MM/yyyy';
    dayInt = recurringInt;
    dayStr = recurringStr;

    private default: any = {
        recordNumber: '',
        personID: '',
        listOrder: '',
        followUpEmail: '',
        recurring: false,
        recurrInt: null,
        recurrStr: null,
        notes: '',
        reminderDate: null,
        dueDate: null,
        staffAlert: null
    }
    
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    
    alist: Array<any> = []
    selectedRow  : number = 0;
    activeRowData:any;
    constructor(
        private timeS: TimeSheetService,
        private listS: ListService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private router: Router,
        private printS: PrintService,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private sanitizer: DomSanitizer,
        private cd: ChangeDetectorRef
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'reminders')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
    selectedItemGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;

        console.log(this.activeRowData);
    }
    search(user: any = this.user) {
        this.loading = true;
        
        this.timeS.getremindersrecipient(user.code).subscribe(data => {
            this.tableData = data.list;
            this.originalTableData = data.list;
            this.loading = false;
            this.selectedRow = 0;
            this.cd.markForCheck();
        });

        this.listS.getlistrecipientreminders().subscribe(data => this.alist = data);
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            recordNumber: 0,
            personID: this.user.id,
            listOrder: '',
            followUpEmail: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            recurring: false,
            sameDate:false,
            sameDay:false,
            recurrInt: null,
            recurrStr: null,
            notes: '',
            reminderDate: null,
            dueDate: null,
            staffAlert: ['', [Validators.required]]
        });

        this.inputForm.controls['recurrStr'].disable();
        this.inputForm.controls['recurrInt'].disable();
        
        this.inputForm.controls['sameDay'].disable();
        this.inputForm.controls['sameDate'].disable();

        this.inputForm.get('recurring').valueChanges.subscribe(data => {
            if (!data) {
                this.inputForm.controls['recurrInt'].setValue(null)
                this.inputForm.controls['recurrStr'].setValue(null)
                this.inputForm.controls['recurrStr'].disable()
                this.inputForm.controls['recurrInt'].disable()
            } else {
                this.inputForm.controls['recurrStr'].enable()
                this.inputForm.controls['recurrInt'].enable()
            }
        });
        this.inputForm.get('recurrStr').valueChanges.subscribe(data => {
            if (data == 'Month/s') {
                this.inputForm.controls['sameDay'].enable();
                this.inputForm.controls['sameDate'].enable();
            } else {
                this.inputForm.controls['sameDay'].setValue(false);
                this.inputForm.controls['sameDate'].setValue(false);
                this.inputForm.controls['sameDay'].disable();
                this.inputForm.controls['sameDate'].disable();
            }
        });
    }

    onKeyPress(data: KeyboardEvent) {
        return this.globalS.acceptOnlyNumeric(data);
    }

    showEditModal(index: number) {
        this.inputForm.patchValue({ personID: this.user.id });

        const { recordNumber, personID, name, address1, address2, email, date1, date2, state, notes, recurring,sameDate,sameDay } = this.tableData[this.selectedRow];
        
        this.inputForm.patchValue({
            recordNumber,
            personID,
            listOrder: state,
            followUpEmail: email,
            recurring,
            sameDate,
            sameDay,
            recurrInt: address1 == '' ? null : address1,
            recurrStr: address2 == '' ? null : address2,
            notes,
            reminderDate: date1,
            dueDate: date2,
            staffAlert: name
        });

        this.addOREdit = 2;
        this.modalOpen = true;
    }

    save() {
        if (!this.globalS.IsFormValid(this.inputForm))
            return;
        
        const remGroup = this.inputForm.value;

        if (remGroup.recurring && (!remGroup.recurrInt || !remGroup.recurrStr)) {            
            this.globalS.eToast('Error', 'Recurring Variables needs to be filled');
            return;
        }
        const reminderDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(remGroup.reminderDate);
        const dueDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(remGroup.dueDate);
        const reminder: Reminders = {
            recordNumber: remGroup.recordNumber,
            personID: this.user.id,
            name: remGroup.staffAlert,
            address1: remGroup.recurring ? remGroup.recurrInt : '',
            address2: remGroup.recurring ? remGroup.recurrStr : '',
            email: remGroup.followUpEmail,
            date1: reminderDate,
            date2: dueDate,
            state: remGroup.listOrder,
            notes: remGroup.notes,
            recurring: remGroup.recurring,
            sameDay: remGroup.sameDay,
            sameDate: remGroup.sameDate,
            creator : this.tocken.user
        }   

        if(this.addOREdit == 1){
            this.timeS.postremindersrecipient(reminder).pipe(takeUntil(this.unsubscribe))
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data added');
                    this.search();
                    this.handleCancel();
                   // this.buildForm();
                    this.inputForm.reset(this.default);
                });
        }
        console.log(this.addOREdit + "addOREdit")
        if (this.addOREdit == 2) {
            this.timeS.updateremindersrecipient(reminder).pipe(takeUntil(this.unsubscribe))
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data updated');
                    this.search();
                    this.handleCancel();
                   // this.buildForm();
                   this.inputForm.reset(this.default);
                })
        }

        console.log(reminder);
    }

    canDeactivate() {
        if (this.inputForm && this.inputForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Save changes before exiting?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {

                }
            });
        }
        return true;
    }

    trackByFn(index, item) {
        return item.id;
    }

    showAddModal() {
        this.modalOpen = true;
        this.addOREdit = 1;
        this.inputForm.patchValue({ personID: this.user.id });
    }

   

    delete(index: number) {
        const { recordNumber } = this.tableData[this.selectedRow];

        this.timeS.deleteremindersrecipient(recordNumber).subscribe(data => {
            this.globalS.sToast('Success', 'Data added');
            this.search();
            this.handleCancel();
        });
    }

    handleCancel() {
        this.modalOpen = false;
        this.isLoading = false;
        this.inputForm.reset(this.default);
    }
    handleOkTop() {        
        this.tryDoctype = ""
        this.pdfTitle = ""
    }
    
    handleCancelTop(){
        this.drawerVisible = false;
        this.pdfTitle = "";
        this.tryDoctype="";
      }
      
      InitiatePrint(){
    
        var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
    
        fQuery = " SELECT Name AS [Review],Name AS [Alert/Review], FORMAT(convert(datetime,Date1), 'dd/MM/yyyy')  as [Reminder Date], FORMAT(convert(datetime,Date2), 'dd/MM/yyyy')   as [Due Date], Notes FROM HumanResources WHERE PersonID = '"+this.user.id+"' AND [Type] = 'RECIPIENTALERT' AND IsNull(ReminderScope, '') <> 'CC' ORDER BY Name  ";
        //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
        txtTitle = "Reminders";       
        Rptid =   "8WTerq14eNFSAnz0"  
    
        //console.log(fQuery)
          const data = {
                      
              "template": { "_id": Rptid },                                
              "options": {
                  "reports": { "save": false },                
                  "sql": fQuery,
                  "Criteria": lblcriteria,
                  "userid": this.tocken.user,
                  "txtTitle": txtTitle,
                  "Extra": this.user.code,
                                                                  
              }
          }
          this.loading = true;
          this.drawerVisible = true;         
              this.printS.printControl(data).subscribe((blob: any) => {
              this.pdfTitle = txtTitle+".pdf"
                  this.drawerVisible = true;                   
                  let _blob: Blob = blob;
                  let fileURL = URL.createObjectURL(_blob);
                  this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                  this.loading = false;
                  this.cd.detectChanges();
              }, err => {
                  console.log(err);
                  this.loading = false;
                  this.modalService.error({
                      nzTitle: 'TRACCS',
                      nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                      nzOnOk: () => {
                          this.drawerVisible = false;
                      },
                  });
              });
              return;      
      }

      originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Alert',
        value: 'name'
    },{
        key: 'Reminder Date',
        value: 'date1'
    },{
        key: 'Due Date',
        value: 'date2'
    },{
        key: 'Notes',
        value: 'notes'
    }
];
    
    
    

    dragDestination = [
        'Alert',
        'Reminder Date',
        'Due Date',
        'Notes',
       
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);
        console.log(flatten)
        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }

}