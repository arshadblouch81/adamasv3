
import {takeUntil} from 'rxjs/operators';
import {
    Component,
    ChangeDetectionStrategy,
    ViewChild,
    TemplateRef,
    OnInit,
    OnDestroy
  } from '@angular/core';

  import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours
  } from 'date-fns';
  import { Subject } from 'rxjs';

  import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

  import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent
  } from 'angular-calendar';

  import { BsModalComponent } from 'ng2-bs3-modal';
  
  
  const colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA'
    },
    purple:{
        primary: '#5f00a4',
        secondary:'#f1ddff'
    },
    brown: {
        primary: '#9b3400',
        secondary: '#ffc5a8'
    },
    blue2: {
        primary: '#25918d',
        secondary: '#fffc94'
    }
  };


import * as moment from 'moment';
import  { TimeSheetService,GlobalService, StaffService,ClientService } from '../../services/index';

const currDateMoment = moment();

interface RosterEvent{
  client: string,
  service: string,
  carer: string,
  startTime: string,
  endTime: string
}

@Component({
    selector: '',
    templateUrl: './calendar.html',
    styles: [
      `
      div.btn.active{
        background:#004a70;
      }
      .btn-prov-calendar{
        background:#4bd0a5 !important;
      }
      .btn-prov-calendar:hover{
        background:#5bddb3 !important;
        color:#fff;
      }
      .btn-prov-calendar.active{
        background:#3ba080 !important;
      }
      `
    ]
})

export class CalendarProvider implements OnInit, OnDestroy {
    private unsubscribe$ = new Subject()
    @ViewChild('modalContent', { static: false }) modalContent: TemplateRef<any>;
    @ViewChild('roster', { static: false }) roster: BsModalComponent;

    dayLimit: number = 10;

    view: string = 'month';  
    viewDate: Date = new Date();
  
    modalData: {
      action: string;
      event: CalendarEvent;
    };

    addresses:Array<any>;

    rosterData: RosterEvent;
  
    actions: CalendarEventAction[] = [
      {
        label: '<i class="fa fa-fw fa-pencil"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.handleEvent('Edited', event);
        }
      },
      {
        label: '<i class="fa fa-fw fa-times"></i>',
        onClick: ({ event }: { event: CalendarEvent }): void => {
          this.events = this.events.filter(iEvent => iEvent !== event);
          this.handleEvent('Deleted', event);
        }
      }
    ];
  
    refresh: Subject<any> = new Subject();
  
    events: CalendarEvent[];
  
    activeDayIsOpen: boolean = false;

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private staffS: StaffService,
        private clientS: ClientService,
        private modal: NgbModal
    ){

    }

    ngOnInit(){
      this.staffS.getmobilefuturelimit(this.globalS.decode().nameid).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {       
        if(data.dayLimit != 0)
          this.dayLimit = data.dayLimit;

        this.dateChanged(moment(), this.view);
      })
    }
    
    ngOnDestroy(): void{
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

    search(startDate: any, endDate: any){
      this.staffS.getroster({
            RosterType: 'SERVICE PROVIDER',
            AccountNo: this.globalS.decode().code,
            StartDate: startDate,
            EndDate: endDate
        }).pipe(takeUntil(this.unsubscribe$))
          .subscribe(data => {

            this.events = data.map(x => {
                let totalColors = Object.keys(colors);
                let colLoc:number = this.getRandomInt(0,totalColors.length);

                let sTime = moment(x.shift_Start).format('HH:mm');
                let eTime = moment(x.shift_End).format('HH:mm');
                return {
                  start: new Date(x.shift_Start),
                  end: new Date(x.shift_End),
                  title:  x.clientCode + ' — ' + x.serviceType + ' — ' +  x.carerCode +' — (' + sTime + '-' + eTime + ')',
                  actions: this.actions,
                  color: colors[totalColors[colLoc]],
                  data: {
                    client: x.clientCode,
                    service: x.serviceType,
                    carer: x.carerCode,
                    startTime: sTime,
                    endTime: eTime,
                    notes: x.notes,
                    recordNo: x.recordNo
                  }
                }
            })

            // if(this.events.length == 0){
            //     this.events = [
            //         {
            //           start: new Date("2018/06/25"),
            //           end: new Date("2018/06/27"),
            //           title: 'A 3 day event',
            //           color: colors.red,
            //           actions: this.actions
            //         },
            //         {
            //           start:  new Date("2018/06/22"),
            //           title: 'An event with no end date',
            //           color: colors.yellow,
            //           actions: this.actions
            //         },
            //         {
            //           start: addHours(startOfDay(new Date()), 2),
            //           end: new Date(),
            //           title: 'A draggable and resizable event',
            //           color: colors.yellow,
            //           actions: this.actions,
            //           resizable: {
            //             beforeStart: true,
            //             afterEnd: true
            //           },
            //           draggable: true
            //         }
            //       ];
            // }

        });
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
      if (isSameMonth(date, this.viewDate)) {
        if (
          (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
          events.length === 0
        ) {
          this.activeDayIsOpen = false;
        } else {
          this.activeDayIsOpen = true;
          this.viewDate = date;
        }
      }
    }

    isDateValid(dateEvent: any):boolean {
      let dynamicDate = moment(dateEvent).format('YYYY/MM/DD');

      const startDate = moment(currDateMoment).subtract(this.dayLimit ,'days').format('YYYY/MM/DD');
      const endDate = moment(currDateMoment).add(this.dayLimit,'days').format('YYYY/MM/DD');

      console.log(dynamicDate);

      if ( moment(dynamicDate).isSame(endDate) || moment(dynamicDate).isSame(startDate) ||moment(dynamicDate).isBefore(endDate,'day') 
          && moment(dynamicDate).isAfter(startDate,'day'))
        return true;

      return false;
    }

    dateChanged(event: any,view: string = ""){

      if(view === 'day' && !this.isDateValid(event)){
        return false;
      }

      if(this.view === 'month'){

        const startDate = moment(currDateMoment).subtract(this.dayLimit,'days');
        const endDate = moment(currDateMoment).add(this.dayLimit,'days');

        this.search(
          moment(startDate).format('YYYY/MM/DD'), 
          moment(endDate).format('YYYY/MM/DD')
        );
      }

      if(this.view === 'week'){

        const startDate = moment(currDateMoment).subtract(this.dayLimit,'days');
        const endDate = moment(currDateMoment).add(this.dayLimit,'days');

        this.search(
          moment(startDate).format('YYYY/MM/DD'), 
          moment(endDate).format('YYYY/MM/DD')
        );
      }

      if(this.view === 'day'){
        const startDate = event;

        this.search(
          moment(startDate).format('YYYY/MM/DD'), 
          moment(startDate).format('YYYY/MM/DD')
        );
      }
    }

    eventTimesChanged({
        event,
        newStart,
        newEnd
      }: CalendarEventTimesChangedEvent): void {

        event.start = newStart;
        event.end = newEnd;
        this.handleEvent('Dropped or resized', event);
        this.refresh.next();
      }
    
      handleEvent(action: string, event: any): void {
          const roster: RosterEvent = event.data;

          this.rosterData = roster;
          this.roster.open();

          this.clientS.getprimaryaddressnyname(roster.client).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.addresses = data
          })

   
        // this.modalData = { event, action };
        // this.modal.open(this.modalContent, { size: 'lg' });
      }
    
      addEvent(): void {

        this.events.push({
          title: 'New event',
          start: startOfDay(new Date()),
          end: endOfDay(new Date()),
          color: colors.red,
          draggable: true,
          resizable: {
            beforeStart: true,
            afterEnd: true
          }
        });
        this.refresh.next();
      }
    
}


