import { Component, OnInit, OnDestroy, Input,Output, AfterViewInit, EventEmitter} from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, Form, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,GlobalService, TimeSheetService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { EMPTY, forkJoin, Observable, Subject } from 'rxjs';
import {switchMap, takeUntil} from 'rxjs/operators';
import { parseISO } from 'date-fns';
import { SetLeftFeature } from 'ag-grid-community';
import { StringNullableChain } from 'lodash';
import { DatePipe } from '@angular/common';

function findDateTimeDifference(date1, date2, interval='h') {
    // always round down
    if (interval=='d')
        return Math.round((date2-date1)/(1000*60*60*24));
    if (interval=='h')
        return Math.round((date2-date1)/(1000*60*60));
    if (interval=='m')
        return Math.round((date2-date1)/(1000*60));
    if (interval=='s')
        return Math.round((date2-date1)/(1000));
}
function getFormatedDate(date: Date, format: string) {
    const datePipe = new DatePipe('en-US');
    return datePipe.transform(date, format);
}
@Component({
    templateUrl: './cancelshift.html',
    styles: [`
    nz-modal{
        hasBackdrop:true;
    }
    ul{
        list-style: none;
        padding: 5px 0 5px 15px;
        margin: 0;
    }
    li {
        padding: 4px 0 4px 10px;
        font-size: 13px;
        position:relative;
        cursor:pointer;
    }
    li:hover{
        background:#f2f2f2;
    }
    li i {
        float:right;
        margin-right:7px;
    }
    hr{
        border: none;
        height: 1px;
        background: #e5e5e5;
        margin: 2px 0;
    }
    li > ul{
        position: absolute;
        display:none;         
        right: -192px;
        padding: 2px 5px;
        background: #fff;
        top: -6px;
        width: 192px;
        transition: all 0.5s ease 3s;
    }
    li:hover > ul{           
        display:block;
        transition: all 0.5s ease 0s;
    }

    .listitem{
        border: 1px solid #fff;
        background-color: #fff;
    }
    
 
      .rectangle{
        width: 100%;       
        display: flex; 
        flex-direction: column; 
        margin-top:5px;
        padding: 5px;        
        background-color: #ffffff; /* #e5ecf1 #85B9D5*/; 
        color: black;       
        /* border : 1px solid #6496b6; */
        border-radius : 5px;
      }

      
  .header{
    display: flex; 
    flex-direction: row; 
    padding-top: 5px; 
    
    /* background-color: #85B9D5; */
    color: black; 
  
    border : 1px solid #6496b6;
    border-radius : 5px;
}

    `],
    selector:'cancel-shift'
})


export class CancelShift implements OnInit, OnDestroy{
    
    dateFormat: string ='yyyy/MM/dd';
    today = new Date();
    currentDate:any;
    lstCancelCodes:Array<any>=[];
    defaultData:any;
    loading:boolean;
    ViewRecipeintList:boolean;
    CancelShiftServiceType:any;
    CancelPayType:any;
    CancelShiftProgram:any
    CancelCode:string;
    CancelType:any;
    CancelRate:number;
    DefaultDuration:string='Accept duration of original service as duration of leave and do not query'
    BillOriginalActivity:boolean;
    OriginalNDAI:boolean;
    showNSDF:boolean;
    chkProgram:boolean;
    chkPayType:boolean;
    chkServiceType:boolean;
    PayStaffLabel:string='Pay Staff ..OR ..';

    txtOPNote:any;
    ViewList:boolean;
    ViewCancelListCode:boolean;
    @Input() view:boolean;
    @Output() Done:EventEmitter<any>= new EventEmitter();
    private unsubscribe = new Subject();
    HighlightRow2:number;
    PayStaff:boolean;
    FillStaff:boolean;
    enableCategory:boolean;
    b_No_Short_NoticeSelected:boolean;
    AlternativeRecipient:string;
    NewBillingRate:any;
    NewBilling :any;  
    TravelInterpreter:any

    @Input() loadCancelShift :Subject<any> = new Subject() ;
    CancelShiftForm:FormGroup;
    chk0:boolean;
    chk3:boolean;
    shift:any;
    program:any;
    NewRecordNo:any
    highlighted:any;
    token:any;
    user:any;
    OpNote:any='OPNOTE';
    MultipleReminder:boolean;
    MultipleEmail:boolean;
    selectedCategory:string;
    CaseNote:boolean;
    ListType:string;
    datalist:Array<any>=[];
    lstServiceTypes:Array<any>=[];
    lstPayTypes:Array<any>=[];
    lstPrograms:Array<any>=[];
    lstRecipient:Array<any>=[];
    lstEmails:Array<any>=[];
    lstcategory:Array<any>=[]
    lstUsers:Array<any>=[]
    lstRecipBranches:Array<any>=[]
    lstDuration:Array<any>=[
        'Accept duration of original service as duration of leave and do not query',
        'Query duration of each individual leave item created'
    ]
    lstReasons:Array<any>=[
         "NSDH : No show due to health reason.",
         "NSDF : No show due to family issues.",
         "NSDT : No show due to Unavailability of transport.",
         "NSDO : Other."
    ]
    reason:string='NSDF : No show due to family issues.';
    CancellationType:any='RECIPIENT CANCELLATION'
    Cancel_shift_Detail='Shift Cancellation';
    s_MultiStatus:string='';
    PayPeriodEndDate:string;
    ReminderUser:any;
    reminderDate:any;
    EmailUser:any;
    FillProgram:boolean=false;
    chk3_Visible:boolean =false;
    chk4_Visible:boolean = true;
    chk5_Visible:boolean = true;
    chk7_Visible:boolean = true;
    
    b_NDIA:any;
    viewType:string='Staff';
    NoPermission:boolean;
    registration:any;
    userData:any;
    getNewReason:boolean;

    private observerable2:Observable<any>;
    constructor(
      
        private modalService: NzModalService,
        private listS: ListService,
        private globalS:GlobalService,
        private timeS:TimeSheetService,
        private formBuilder: FormBuilder,
        ){}

         
    ngOnDestroy(): void {
        
        this.unsubscribe.next();
       
    }

        
        ngOnInit(): void {
            this.loading=true;
            this.currentDate=format(this.today,this.dateFormat);
            this.token = this.globalS.decode(); 
            this.user=this.token.user;
            //this.buildForm();
            this.OpNote='OPNOTE'
           
            
            this.loadCancelShift.subscribe(data=>{
                this.clear_fields();
                //this.loading=true;
                this.highlighted=data.diary;
                this.shift= data.selected; 
                this.program=this.shift.rprogram;              
                this.LoadShiftSetting()
               
            });

            this.load_lists();
         
            
        }
        buildForm() {
            this.CancelShiftForm = this.formBuilder.group({
             ///   recordNo: [''],
              //  date: [this.today, Validators.required],
                serviceType: ['', Validators.required],
              //  type:0,
                program: ['', Validators.required],
              //  serviceActivity: ['', Validators.required],
                payType: ['', Validators.required],                        
             //   recipientCode:  [''],              
              //  staffCode:  [''],
              //  debtor:  [''],               
             //   isMultipleRecipient: false,
                txtOPNote:['', Validators.required],
                AlternativeRecipient: [''],  
                reason: [''],
                PayStaff: false,
                FillStaff: false,
                chkServiceType: false,
                chkPayType: false,
                chkProgram: false,
                
               
                
            });
    
          
    
    
            this.CancelShiftForm.get('PayStaff').valueChanges.pipe(
                takeUntil(this.unsubscribe),
                switchMap(d => {
                    if(!d) return EMPTY;
                   this.setCheckBoxes();
                }));

                this.CancelShiftForm.get('FillStaff').valueChanges.pipe(
                    takeUntil(this.unsubscribe),
                    switchMap(d => {
                        if(!d || this.lstRecipient.length>0) return EMPTY;
                        //return this.fillRecipient();
                    })).subscribe(data => {
                        this.lstRecipient=data;
                    });
                    
            this.CancelShiftForm.get('chkPayType').valueChanges.pipe(
                // distinctUntilChanged(),
                    switchMap(x => {
                        if(!x) return EMPTY;
                    
                        this.CancelPayType=''
                    }));
           
            this.CancelShiftForm.get('chkServiceType').valueChanges.pipe(
               // distinctUntilChanged(),
                switchMap(x => {
                    if(!x) return EMPTY;
                  
                    this.CancelShiftServiceType=''
                }));
                this.CancelShiftForm.get('chkProgram').valueChanges.pipe(
                    // distinctUntilChanged(),
                     switchMap(x => {
                         if(!x) return EMPTY;
                       
                         this.CancelShiftProgram=''
                     }));
                 
                   
    
        }
CheckValidation():boolean{
   
 
    //CHECK APPROVED/PAID/BILLED SHIFTS

    
  let approved= this.highlighted.filter(x=>x.status==2)
   
    if (approved.length>0){
        this.globalS.eToast('Error', "One or more of the selected shifts has been approved, paid or billed - and cannot be cancelled - only unfinalised change your selection to only include shifts that are not finalised.")
        this.NoPermission=true;
        return false;
    }
    if (this.userData.dayManager< 301 ) {
        this.globalS.eToast('Error',"Your access rights are not sufficient to perform this operation.");
        this.NoPermission=true;
        return false;    
    }
   
    if(this.txtOPNote == "" || this.txtOPNote == null){
        this.globalS.eToast('Error',"You must enter a note");
      
        return false;    
    }
    
    if (this.b_No_Short_NoticeSelected && this.chkPayType == false && this.CancelPayType == "" && this.shift.carercode > "!z" )
    {
        this.globalS.eToast('Error',"Pay Type is blank. You must enter a paytype!");      
        return false;   
    }

    if (this.CancelCode != "" || this.CancelCode != null ) //'ABSENCE TYPE
    {     
                if (this.b_No_Short_NoticeSelected ){
                    if (!this.BillOriginalActivity || !this.PayStaff )
                    {
                        if (!this.BillOriginalActivity){
                           // this.globalS.eToast('Error','You have nominated a short or no notice cancellation but have elected to NOT CHARGE the client - are you sure you want to proceed?');      
                            this.modalService.confirm({
                                nzTitle: 'Confirm',
                                nzContent: 'You have nominated a short or no notice cancellation but have selected to NOT CHARGE the client - are you sure you want to proceed?',
                                nzOkText: 'Yes',
                                nzCancelText: 'No',
                                nzOnOk: () =>
                                new Promise((resolve,reject) => {
                                 
                                  setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
                                  if (reject) return false;
                                 
                                }).catch(() => console.log('Oops errors!'))
                          
                                
                              });
                            //s_UserOverride = "User elected to not charge client" & vbCrLf
                        }
                        if (!this.PayStaff){
                            this.modalService.confirm({
                                nzTitle: 'Confirm',
                                nzContent: 'You have nominated a short or no notice cancellation but have selected to NOT PAY the worker - are you sure you want to proceed?',
                                nzOkText: 'Yes',
                                nzCancelText: 'No',
                                nzOnOk: () =>
                                new Promise((resolve,reject) => {
                                 
                                  setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
                                  if (reject) return false;
                                 
                                }).catch(() => console.log('Oops errors!'))
                          
                                
                              });
                            
                          //  s_UserOverride = s_UserOverride & "User elected to not pay worker" & vbCrLf
                        }
                    }
                }
            else {
              
                if (this.chk0 && !this.chk3 && (this.CancelCode=='' || this.CancelCode==null)){
                  this.globalS.eToast('Error',"(1)- You have not completed all selections - please make all necessary selections for the options you have chosen and try again");      
                    return false;  
                }
                   
             }
        }
         
        if (this.FillStaff && this.s_MultiStatus == "SINGLE" && (this.AlternativeRecipient==null || this.AlternativeRecipient=='') ){
            //MsgBox "(2) - You have not completed all selections - please make all necessary selections for the options you have chosen and try again"
            this.globalS.eToast('Error',"(2) - You have not completed all selections - please make all necessary selections for the options you have chosen and try again");      
            return false;  
        }
       
        if  ( this.PayStaff && !this.chkPayType && (this.CancelPayType==null || this.CancelPayType=='' )){
            this.globalS.eToast('Error',"(3) - You have not completed all selections - please make all necessary selections for the options you have chosen and try again");      
            return false;
        }
        
        if  ( this.PayStaff && !this.chkServiceType && (this.CancelShiftServiceType==null || this.CancelShiftServiceType=='') ){
            this.globalS.eToast('Error',"(4) - You have not completed all selections - please make all necessary selections for the options you have chosen and try again");      
            return false;
        }

        if  ( this.FillProgram && !this.chkProgram && (this.CancelShiftProgram==null || this.CancelShiftProgram=='') ){
            this.globalS.eToast('Error',"(5) - You have not completed all selections - please make all necessary selections for the options you have chosen and try again");      
            return false;
        }

        let s_Prompt : string
    s_Prompt = "You have selected to Cancel the selected shift/s "
    
    if (this.chk0)
        s_Prompt = s_Prompt +"\n" + "* AND record an absence against the recipient, ";
    else
        s_Prompt = s_Prompt +"\n" + "*WITHOUT recording an absence against the recipient, ";
   
    
    if (this.PayStaff)
        s_Prompt = s_Prompt +"\n" + "*AND pay the staff for the cancelled shift, "
    else
        s_Prompt = s_Prompt +"\n" + "*WITHOUT paying the staff for the cancelled shift, "
    
        
    if (this.FillStaff)
        s_Prompt = s_Prompt +"\n" + "*AND reallocate the shift to an alternate recipient - is that correct?"
    else
        s_Prompt = s_Prompt +"\n" + "*WITHOUT reallocating the shift to an alternate recipient - is that correct? "
    
       
}
setDefautlValue(type:string){
    if (type=='ServiceType' ){
        if (this.chkServiceType)
            this.CancelShiftServiceType=null;      
           
    }
    if (type=='PayType' && this.chkPayType){
        if (this.chkServiceType)
            this.CancelPayType=null;
       
    }  
    if (type=='Program' && this.chkProgram){
        if (this.chkServiceType)
            this.CancelShiftProgram=null;
       
    }  
}
setCheckBoxes(){
    //chkProgram, chkPayType, chkServiceType
    let s_Mask : string
    s_Mask =(this.chk0 ? '1':'0') + (this.FillStaff ? '1':'0') + (this.PayStaff ? '1':'0') 
    switch (s_Mask)
    {
    case "000" :
      {
        //chk(1).Visible = True
      this.PayStaffLabel = "Pay Staff ..OR .."
      //  chk(2).Visible = True
      //  txt(2) = "SELECT Alternate Recipient for Activity..."
        if (this.s_MultiStatus == "SINGLE" )
            this.CancelCode = "Select Cancellation Code to apply to shift...";
        else  
            this.CancelCode = "Select Cancellation Code to apply to selected shifts...";
       
        this.CancelPayType = "Select pay type for replacement activities created......."
        this.CancelShiftServiceType = "Select activity code for replacement activities created......."
        if (this.s_MultiStatus == "SINGLE" )
            this.CancelShiftProgram = "SELECT Program to apply to Cancellation..."
        else
            this.CancelShiftProgram = "SELECT Program to apply to Cancellations..."

        break;
        }
    case "001" :
     {   
       this.FillStaff = false;
       this.PayStaff = true
       this.PayStaffLabel = "Pay Staff"
       if (this.s_MultiStatus == "SINGLE" )
           this.CancelCode = "Select Cancellation Code to apply to shift..."
        else
           this.CancelCode = "Select Cancellation Code to apply to selected shifts..."
        
        this.CancelPayType= "Select pay type for replacement activities created......."
        this.CancelShiftServiceType = "Select activity code for replacement activities created......."
        if(this.s_MultiStatus == "SINGLE" )
            this.CancelShiftProgram = "SELECT Program to apply to Cancellation..."
        else
            this.CancelShiftProgram = "SELECT Program to apply to Cancellations..."
        break;
     }
    case "010":
    {
            this.PayStaff = false;
            this.FillStaff = true;           
            this.chk0=true;
      
       // txt(2) = "SELECT Alternate Recipient for Activity..."
        if (this.s_MultiStatus == "SINGLE" )
           this.CancelCode= "Select Cancellation Code to apply to shift...";
        else
           this.CancelCode= "Select Cancellation Code to apply to selected shifts...";
        
        break;
    }case "110" :
    {
        if(this.s_MultiStatus == "SINGLE" )
        {
            this.PayStaff = false;
            this.FillStaff = true
            this.enableCategory = true;
        }else{
           
            this.PayStaff = false
            this.FillStaff = true
            this.enableCategory = true;
        }
        this.AlternativeRecipient= "SELECT Alternate Recipient for Activity...";
        break;
       } case "100" :
        {   
       
        this.PayStaff  = true;
        this.FillStaff = true;
        this.PayStaffLabel = "Pay Staff ..OR ..";
        this.AlternativeRecipient = "SELECT Alternate Recipient for Activity..."
        this.CancelShiftServiceType = "Select pay type for replacement activities created......."
        this.CancelPayType = "Select activity code for replacement activities created......."
        if(this.s_MultiStatus == "SINGLE" )
            this.CancelShiftProgram = "SELECT Program to apply to Cancellation...";
        else
            this.CancelShiftProgram = "SELECT Program to apply to Cancellations...";
       break;
    } case "101":
      { 
        this.FillStaff = false;
       
        this.PayStaffLabel= "Pay Staff"
        this.CancelPayType = "Select pay type for replacement activities created......."
        this.CancelShiftServiceType  = "Select activity code for replacement activities created......."
        if(this.s_MultiStatus == "SINGLE" )
            this.CancelShiftProgram = "SELECT Program to apply to Cancellation...";
       else
            this.CancelShiftProgram= "SELECT Program to apply to Cancellations...";
       break;
      }case "111" :
        this.PayStaff=true;
        break;
    }
      
    if (this.shift.carercode == "!MULTIPLE") {
        this.PayStaff=false;
        this.FillStaff=false;
    }
}
MultipleReminder_checked(){
   
   // b_SuppressClick = True
   if (this.MultipleReminder){
    let sql =`SELECT [Description] as name FROM DataDomains WHERE Domain = 'BRANCHES'`;
    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
       
        this.lstUsers=data.map(x=>x.name);
    });
    sql =`SELECT [Name] FROM HumanResources WHERE PersonID = '${this.shift.uniqueid}' AND [Type] = 'RECIPBRANCHES' ORDER BY Name`;
    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
       
        this.lstRecipBranches=data;
    });
    this.lstUsers.forEach(v=>{
        //  lstx(0).CHECKED(ictr) = TBIsInCollection(cRecipBranches, lstx(0).List(ictr))
        //If lstx(0).CHECKED(ictr) Then FillListComboFullSQL lstx(1), "SELECT [Name] FROM UserInfo WHERE isnull(EndDate, '') = '' AND ViewFilterBranches LIKE '%Recipients.Branch = ''" & lstx(0).List(ictr) & "''%'", s_status, , , , False
    
    });

   }
   else     {
    let sql=` SELECT DISTINCT UPPER([Name]) as name FROM USERINFO`;
            this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.lstUsers=data.map(x=>x.name);
            });
   }
}
MultipleEmail_checked(){
    if (this.MultipleEmail){
    //b_SuppressClick = True
    let sql =`SELECT [Description] as name FROM DataDomains WHERE Domain = 'BRANCHES'`;
    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
       
        this.lstUsers=data.map(x=>x.name);
    });
    sql =`SELECT [Name] FROM HumanResources WHERE PersonID = '${this.shift.uniqueid}' AND [Type] = 'RECIPBRANCHES' ORDER BY Name`;
    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
       
        this.lstRecipBranches=data;
    });
    this.lstUsers.forEach(v=>{
        //    lstx(3).CHECKED(ictr) = TBIsInCollection(cRecipBranches, lstx(3).List(ictr))
        //If lstx(3).CHECKED(ictr) Then FillListComboFullSQL lstx(2), "SELECT [Name] FROM UserInfo WHERE isnull(EndDate, '') = '' AND ViewFilterBranches LIKE '%Recipients.Branch = ''" & lstx(3).List(ictr) & "''%'", s_status, , , , False
   
    })


    
    }
    else{
        let sql=` SELECT DISTINCT UPPER([Name]) as name FROM USERINFO`;
        this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.lstUsers=data.map(x=>x.name);
        });
    }
}
LoadShiftSetting(){
           
            let sql='';
            sql=` SELECT 
            DefaultNoNoticeCancel, DefaultNoNoticeBillProgram, NoNoticeLeaveActivity, DefaultNoNoticePayProgram, DefaultNoNoticePaytype, 
            DefaultShortNoticeCancel, DefaultShortNoticeBillProgram, ShortNoticeLeaveActivity, DefaultShortNoticePayProgram, DefaultShortNoticePaytype, 
            DefaultWithNoticeCancel, DefaultWithNoticeProgram,[Type],CloseDate,
            NoNoticeLeadTime, NoNoticeCancelRate, ShortNoticeLeadTime, ShortNoticeCancelRate,
            isnull((select DefaultClientCancelPayType from staff where [AccountNo]='${this.shift.carercode}'),'') as DefaultClientCancelPayType
            FROM HumanResourceTypes 
            WHERE [Name] = '${this.shift.rprogram}'`
            
            this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                let d=data[0];
                this.defaultData=d;
               
              
                this.b_NDIA = d.type=='NDIA'? true : false;
               
               
                this.setDefaultalues();
            });

            this.load_ServiceTypes();
           
            
          }

setDefaultalues(){
    this.CancelCode=this.defaultData.defaultNoNoticeCancel;
    this.CancelShiftProgram=this.defaultData.defaultNoNoticeBillProgram
    let s_Date = this.shift.date;
    let s_Time = this.shift.startTime;
    let d_Duration= this.shift.duration*5;
    let d_Value= this.shift.billQty * this.shift.billRate;
   

    let l_LeadTime = findDateTimeDifference( this.today, new Date(s_Date + " " +s_Time),'m')

    if ((this.defaultData.noNoticeLeadTime !=0 && this.defaultData.noNoticeLeaveActivity != "" && l_LeadTime < this.defaultData.noNoticeLeadTime * 60) 
    ||   (this.b_NDIA && (d_Value > 1000 || d_Duration > 480) && l_LeadTime < 7200))
    {
        this.CancelShiftProgram = this.defaultData.DefaultNoNoticeBillProgram;
        this.CancelCode = this.defaultData.defaultNoNoticeCancel;
        this.CancelRate = this.defaultData.noNoticeCancelRate;
        this.CancellationType = "NONOTICE"
        this.CancelShiftServiceType = this.defaultData.noNoticeLeaveActivity
        this.CancelShiftProgram = this.defaultData.defaultNoNoticePayProgram
        this.CancelPayType = this.defaultData.DefaultNoNoticePaytype
    }  else if (this.defaultData.shortNoticeLeadTime != 0 && this.defaultData.shortNoticeLeaveActivity != "" && l_LeadTime < this.defaultData.shortNoticeLeadTime * 60) 
        {
                                
            this.CancelShiftProgram  = this.defaultData.defaultShortNoticeBillProgram
            this.CancelCode = this.defaultData.defaultShortNoticeCancel
            this.CancelRate = this.defaultData.dhortNoticeCancelRate
            this.CancellationType  = "SHORTNOTICE"
            this.CancelShiftServiceType = this.defaultData.shortNoticeLeaveActivity
            this.CancelShiftProgram = this.defaultData.defaultShortNoticePayProgram
            if (this.defaultData.defaultClientCancelPayType!='' || this.defaultData.defaultClientCancelPayType!=null)
                this.CancelPayType = this.defaultData.defaultClientCancelPayType;
            else
                this.CancelPayType = this.defaultData.defaultShortNoticePaytype;

            
        }else {
            this.CancelShiftProgram  = this.defaultData.defaultWithNoticeProgram
            this.CancelCode = this.defaultData.defaultWithNoticeCancel
            this.CancellationType = "WITHNOTICE"
            
    } 
    
  
    switch(this.CancellationType)
    {
    case "NONOTICE":
        this.Cancel_shift_Detail = "Client Cancellation with NO NOTICE - (Charges may apply)"
        this.lstcategory.push("CANCEL(NO NOTICE)") ;
        this.selectedCategory = "CANCEL(NO NOTICE)"
        this.PayStaff=true;
        this.BillOriginalActivity=true;
        this.b_No_Short_NoticeSelected = true;
        break;
    case "SHORTNOTICE":
        this.Cancel_shift_Detail = "Client Cancellation with SHORT NOTICE - (Charges may apply)"
        this.lstcategory.push("CANCEL(SHORT NOTICE)")
        this.selectedCategory = "CANCEL(SHORT NOTICE)"
        this.PayStaff=true;
        this.BillOriginalActivity=true;
        this.b_No_Short_NoticeSelected = true
        break;
    case "WITHNOTICE":
        this.Cancel_shift_Detail = "Client Cancellation WITH NOTICE - (No Charges apply)";
        this.lstcategory.push("CANCEL(WITH NOTICE)");
        this.selectedCategory = "CANCEL(WITH NOTICE)";
        this.PayStaff=false;
        this.BillOriginalActivity=false;
        this.b_No_Short_NoticeSelected = false
        if (!this.BillOriginalActivity){
           this.getNewBillRate();
        }
        break;
    }   
    
    if (this.highlighted.length>1 )     //|| this.shift.recipient=="!MULTIPLE"
        this.s_MultiStatus ="MULTIPLE";
    else
        this.s_MultiStatus ="SINGLE";
    // if (this.shift.carercode!="!MULTIPLE") 
    //     this.PayStaff=true;
    // if (this.shift.recipient!="!MULTIPLE") 
    //     this.FillStaff=true;

    if(this.s_MultiStatus == "SINGLE" ){
       // txt(2) = "SELECT Alternate Recipients for Activity..."
      //  txt(7) = "SELECT Program to apply to Cancellation..."
        this.chk3_Visible = true
        this.chk4_Visible = true
        this.chk5_Visible = true
        this.chk7_Visible = true
    }else{
       // txt(3) = "Select Cancellation Code to apply to selected shifts..."
       // txt(7) = "SELECT Program to apply to Cancellations..."
    }

    if (this.registration.wizardDefaultNote=='OPNOTE') this.OpNote='OPNOTE';
    else this.OpNote='CASENOTE';
    if(this.shift.carercode == "!MULTIPLE" ) this.MultipleReminder=false;
    if(this.shift.carercode == "!MULTIPLE" ) this.MultipleEmail =true;
    
    if (this.b_NDIA && (this.CancellationType == "NONOTICE" || this.CancellationType == "SHORTNOTICE")) 
    {
        this.BillOriginalActivity=true;
        this.OriginalNDAI=true;
        this.showNSDF =true;      
       
    }
    this.view=true;
    this.loading=false; 
}

getNewBillRate(){
    let sql=`select dbo.GetBillingRate('${this.shift.recipient}','${this.CancelCode}', '${this.shift.rprogram}') as billrate`;
     
    
            this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.NewBillingRate=data[0].billrate;                 
            
        }, error => {this.globalS.eToast('Cancellation-Bill Rate', error);});
 

    sql =`SELECT  isnull(unittype,'${this.shift.billunit}') as NewBillingUnit,
            isnull([unit bill rate],0) as NewBillingRate,
            isnull(servicebiller,'') as NewDebtor,
            (select top 1 HACCType from ItemTypes where Title='${this.shift.activity}') as NewDataset
            FROM   serviceoverview
            WHERE  personid = '${this.shift.uniqueid}'
            AND serviceprogram = '${this.shift.rprogram}'
            AND [service type] = '${this.shift.activity}'`

          
                this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                    this.NewBilling=data[0]; 
                }, error => {this.globalS.eToast('Cancellation-Bill Unit', error);});
          
}

onCancelItemSelected(data:any,i:number){
                this.CancelCode=data.title;
                this.HighlightRow2=i;
                if (this.lstcategory.indexOf(data.title)==-1)
                    this.lstcategory.push(data.title)
          }
onCancelItemDbClick(data:any,i:number){
            this.CancelCode=data.title;
            this.HighlightRow2=i;
            this.ViewCancelListCode=false;
        }
SetCancelCodeValue(){
    this.ViewCancelListCode=false;
          }
SaveCancel_Shift(){
    
    let requests: Observable<Response>[] = [];
    let RemDate:any; 
    
     if (this.reminderDate=='' || this.reminderDate==null)
          RemDate=format(this.today,'yyyy/MM/dd HH:MM:ss');
     else
          RemDate=format(this.reminderDate,'yyyy/MM/dd HH:MM:ss');
          
    if (this.CheckValidation()==false){
        return;
    }

          this.highlighted.forEach(v=>{
            this.shift=v;
            if (this.CancelShiftServiceType==null || this.CancelShiftServiceType=='' || this.chkServiceType) this.CancelShiftServiceType=this.shift.activity;
            if (this.CancelPayType==null || this.CancelPayType=='' || this.chkPayType) this.CancelPayType='';//this.shift.paytype;
            if (this.CancelShiftProgram==null || this.CancelShiftProgram=='' || this.chkProgram) this.CancelShiftProgram=this.shift.rprogram;
            if (this.AlternativeRecipient==null || this.AlternativeRecipient=='' ) this.AlternativeRecipient=this.shift.recipient;
         
           let b_KeepInMDS:boolean=false;
         
          let input= {
            RecordNo: this.shift.recordno,
            ClientCode:this.shift.recipient,
            CancelPayType : this.CancelPayType,
            CancelShiftServiceType:this.CancelShiftServiceType,
            CancelShiftProgram:this.CancelShiftProgram,
            CancelCode:this.CancelCode,
            AlternateRecipient:this.AlternativeRecipient,
            s_Reason:this.reason.substring(0,4),
            Operator: this.user ,
            b_KeepInMDS:b_KeepInMDS,
            b_BillAsOriginal: this.BillOriginalActivity,
            b_RetainDataset: this.OriginalNDAI,
            s_CancelRate:'0',
            RemDate: RemDate,
            OPNote: this.txtOPNote,
            Category: this.selectedCategory,
            ReminderUser: this.ReminderUser,
            travelInterpreter: this.TravelInterpreter
            
           }
         
                this.timeS.cancelShift(input).pipe(
                takeUntil(this.unsubscribe)).subscribe(d => {
                    console.log(d)
                    this.DoneCancelShift();
                });
                setTimeout(() => { },100);
    });
       
 }
        
SaveCancel_Shift_old(){
    
    if (this.CheckValidation()==false){
        return;
    }
            
          let requests: Observable<Response>[] = [];
          let RemDate:any; 
          
           if (this.reminderDate=='' || this.reminderDate==null)
                RemDate=format(this.today,'yyyy/MM/dd HH:MM:ss');
           else
                RemDate=format(this.reminderDate,'yyyy/MM/dd HH:MM:ss');

          this.highlighted.forEach(v=>{
            this.shift=v;
            if (!this.BillOriginalActivity)
                this.getNewBillRate();
           
                
            if (this.CancelShiftServiceType==null || this.CancelShiftServiceType=='' || this.chkServiceType) this.CancelShiftServiceType=this.shift.activity;
            if (this.CancelPayType==null || this.CancelPayType=='' || this.chkPayType) this.CancelPayType=this.shift.paytype;
            if (this.CancelShiftProgram==null || this.CancelShiftProgram=='' || this.chkProgram) this.CancelShiftProgram=this.shift.rprogram;

          
            this.AutoCreateRecipientAbsence(this.shift.recordno, this.CancelCode,this.txtOPNote,false,this.CancelShiftProgram)
          
            
           
            let sqlInsert:any ;

            setTimeout(() => {
                
                if (this.reminderDate=='' || this.reminderDate==null)
                    RemDate=format(this.today,'yyyy/MM/dd HH:MM:ss');
                else
                    RemDate=format(this.reminderDate,'yyyy/MM/dd HH:MM:ss');

                    sqlInsert = {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
                sqlInsert.TableName='History ';   
                sqlInsert.Columns=` PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, AlarmDate,Creator `;
                    
                    sqlInsert.ColumnValues=` '${this.shift.recordno}', '${format(this.today,'yyyy/MM/dd HH:MM:ss')}', 
                        'AUTOMATIC LEAVE PAY ITEM FOR SHIFT# ${this.shift.recordno} :  ${this.shift.date} at  ${this.shift.starttime}  for  + ${this.shift.activity}  WITH   ${this.shift.recipient}  -SERVICE CANCELLED', 
                        'SVCNOTE', '#AUTO-CANCEL', ${this.shift.recordno}, '${RemDate}' , '${this.user}' `; 
                
                       
                this.listS.insertlist(sqlInsert).subscribe(data=>{
                    // this.globalS.sToast("Day Manager","Record Updated Successfully");
                    
                }, error => {this.globalS.eToast('Cancellation-SVC', error);});
            }, 1000); 

            setTimeout(() => {
                if (this.reminderDate==null ) this.reminderDate=this.currentDate;
                sqlInsert.TableName='History ';   
                    sqlInsert.Columns=` PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator, AlarmDate `;
                
                sqlInsert.ColumnValues=` '${this.shift.uniqueid}', '${format(this.today,'yyyy/MM/dd HH:MM:ss')}', 
                '${this.txtOPNote}','${this.OpNote}', '${this.selectedCategory}', '${this.shift.recipient}' , '${this.ReminderUser}', '${format(this.reminderDate,'yyyy/MM/dd')}' `; 

            
                    this.listS.insertlist(sqlInsert).subscribe(data=>{
                        // this.globalS.sToast("Day Manager","Record Updated Successfully");
                     
                    }, error => {this.globalS.eToast('Cancellation-OP', error);});

                
            }, 100); 

            setTimeout(() => {
                let sqlObject :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
                sqlObject.TableName='Roster ';   
                sqlObject.SetClause=` SET [Client Code] = '!INTERNAL', BillTo = '!INTERNAL', [Service Description] = '${this.CancelPayType}', [Service Type] = '${this.CancelShiftServiceType}', 
                                    [Type] = 6, [Unit Bill Rate] = 0, BillQTy = CostQty, Duration = (CostQty * 60) / 5 `;
            
                sqlObject.WhereClause=` where RecordNo=${this.shift.recordno} `; 
            
            
            this.listS.updatelist(sqlObject).subscribe(data=>{
                // this.globalS.sToast("Day Manager","Record Updated Successfully");     
                this.DoneCancelShift();                   
            } , error => {this.globalS.eToast('Cancellation-Roster', error);});

        },500);  

            setTimeout(() => {
                this.Create_PayMakeUp(v);
            }, 100); 
    });
   
    

}


AutoCreateRecipientAbsence(s_RosterRecordNumber : string, s_AbsenceType : string, s_Reason : string, b_KeepInMDS : boolean, s_AlternateProgram : string):boolean 
{
    
    
let RosterYearNo, RosterMonthNo, RosterDayNo, RosterBlockNo :number
let strMessage : string
let s_LeaveRecordNo : string
let s_OldActivity : string
let s_CloseDate : string ;
let s_Worker : string
let s_NewDatasetCode : string
let s_NewBillingRate : number
let s_NewBillingUnit : string
let BillQty:number=1;
let s_NewDebtor : string
let b_NDIAAbsenceCapExceeded :boolean
let b_CancelledAfter3pmPreviousDay :boolean
let sql : string;
let b_RetainDataset:boolean=true;
let HACCType:any
let s_CancelRate:any;

     if (s_Reason == "" )
     this.getNewReason=true;
        // s_Reason = input("Insert a short description of the reason for the cancellation", "Cancellation Reason")



        //CREATE NEW ROSTER ENTRY STARTING WITH A COMPLETE COPY OF ALL FIELDS
        let inputs = {
            "opsType": "Copy",
            "user": this.user,
            "recordNo": this.shift.recordno,
            "isMaster": "0",
            "roster_Date" : this.shift.date,
            "start_Time": this.shift.starttime,
            "carer_code":  this.shift.carercode,
            "recipient_code" : this.shift.recipient,
            "notes" : this.reason,
            'clientCodes' : this.AlternativeRecipient==null || this.AlternativeRecipient=='' ? this.shift.recipient : this.AlternativeRecipient
        }
        this.timeS.ProcessRoster(inputs).subscribe(data => {
            this.NewRecordNo=data.recordno;
      
//CHANGE FIELDS TO NEW DETAIL FOR NEWLY CREATED ABSENCE RECORD
          strMessage = "ORIGINALLY SCHEDULED FOR '" + s_AbsenceType + "' BY '" + this.shift.carercode + "'";
         s_OldActivity = this.shift.activity
         s_CloseDate  = this.defaultData.closeDate;
         if (s_CloseDate!='')
         {
            let cdate:any;
            let s_Date:any = s_CloseDate.split('/');
            if (s_Date[2].length>2)
                 cdate= (s_Date[2].slice(0,4) +'/'+s_Date[1]+'/'+s_Date[0])
            else
                cdate= (s_Date[0].slice(0,4)+'/'+s_Date[1]+'/'+s_Date[2])
            if ( this.shift.date > '2000/01/01' && this.shift.date <= cdate )
                this.globalS.eToast( 'Error',"Rostering for program " + this.shift.rprogram + " Closed before or on " + s_CloseDate);
                
         }

         if (s_AbsenceType == "")
          //If Not EvaluateActivityType("SELECT ACTIVITY", "SELECT ACTIVITY", "RECPTABSENCE", "ATTRIBUTABLE", "EVENT", s_AbsenceType) Then GoTo x_CancelEntry
          this.ViewCancelListCode=true


          if (this.b_NDIA ){

            let  i_NDIAPercent = this.registration.ndiaCancelFeePercent;

                if (this.BillOriginalActivity)
                    s_NewBillingRate = this.shift.billrate * ((i_NDIAPercent / 100)) 
                else
                     s_NewBillingRate = 0

                s_NewDebtor = "NDIA"
          } else{
             if (!this.BillOriginalActivity){
                           
                s_NewBillingRate=this.NewBillingRate;
                s_NewBillingUnit= this.NewBilling.newBillingUnit;
                s_NewDebtor = this.NewBilling.newDebtor;
                s_NewDatasetCode = this.NewBilling.newDataset;
                BillQty= this.shift.billQty;
                if (s_NewBillingUnit==''|| s_NewBillingUnit==null) s_NewBillingUnit=this.shift.billunit
                if (s_NewBillingUnit == "SERVICE") 
                    BillQty = 1;    
                if (!b_RetainDataset ) HACCType = s_NewDatasetCode

                
                //  If GetBillingRate(![Client Code], s_AbsenceType, s_NewBillingRate, s_NewBillingUnit, s_NewDatasetCode, ![Program], s_NewDebtor) Then
                //      ![Unit Bill Rate] = s_NewBillingRate
                //      ![BillUnit] = s_NewBillingUnit
                    
            } else  {
                BillQty= this.shift.billQty;
                s_NewDebtor = this.shift.billto
                s_NewBillingRate = this.shift.billrate
                s_NewBillingUnit=this.shift.billunit
                if (s_CancelRate> "" ) 
                    s_NewBillingRate =Math.round((s_NewBillingRate) * (s_CancelRate) / 100)
                
            }
            
        }

         if (s_NewBillingRate != 0 && s_NewDebtor == "")
                 //this.ViewRecipeintList=true;
                 s_NewDebtor= this.shift.billto;
       
        
        if (b_KeepInMDS ) s_OldActivity = this.shift.activity;
        if (s_AlternateProgram=='' || s_AlternateProgram==null) s_AlternateProgram=this.shift.rprogram

        let sqlObject :any = {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
        sqlObject.TableName='Roster ';   
        sqlObject.SetClause=` SET 
                            [Program] = ${s_AlternateProgram},
                            [ShiftName] = '${s_OldActivity}', BillTo = '${s_NewDebtor}', 
                            [Unit Bill Rate] = ${s_NewBillingRate}, BillQTy = ${BillQty} ,
                            [Unit Pay Rate] = 0, [Carer Code] = '!INTERNAL',
                            [Service Type] = ${s_AbsenceType},
                            [Service Description] = 'N/A',
                            [Type] = 4,
                            VariationReason = ${s_Reason}
                            `;

        sqlObject.WhereClause=` where RecordNo=${this.NewRecordNo} `; 
            
            //'IAMHERE
            //'QUERY - SHOULD status be reliant on autoapprove fuinctionality for recipient absences
            let Otherfilds='';
            let sqlAutoApp =`select IsNull(AutoApprove, 0) from ItemTypes where Title = '${ this.shift.activity}' AND ProcessClassification = 'OUTPUT'`
            this.listS.getlist(sqlAutoApp).subscribe(data=>{
                if (this.shift.carercode!='BOOKED' )                    
                    Otherfilds = `,Status = 2`;
                if (this.shift.date>'2000/01/01' && this.registration.autoAutoApproveRecipientAdmin)                    
                    Otherfilds = `,Status = 2`;
                else
                    Otherfilds = `,Status = 1`;
                    
                Otherfilds =Otherfilds  + `,[Date Timesheet]='${this.PayPeriodEndDate}'`

            });


        Otherfilds = Otherfilds +
                `,[ServiceSetting] = '',
                [Date Entered] = getDate(),
                [Creator] = '${this.user}',
                [Date Last Mod] = getDate(),
                [Date Timesheet] = Null,
                [Date Invoice] = Null,
                InvoiceNumber = Null,
                TimesheetNumber = Null,
                [batch#] = Null,
                [NDIABatch] = Null,
                InUse = 0,
                Transferred = 0,
                ROS_PANZTEL_UPDATED = 0,
                TA_EarlyStart = 0,
                TA_LateStart = 0,
                TA_EarlyGo = 0,
                TA_LateGo = 0,
                TA_TooShort = 0,
                TA_TooLong = 0,
                Editer = '${this.user}'`
                
                if (b_KeepInMDS )
                    Otherfilds = Otherfilds +`,HACCID = -1`;
                else 
                    Otherfilds = Otherfilds +`,HACCID = -0`;

                sqlObject.SetClause=sqlObject.SetClause + Otherfilds;
               // console.log(sqlObject.SetClause +"\n"+sqlObject.WhereClause);

                this.listS.updatelist(sqlObject).subscribe(data=>{
                    // this.globalS.sToast("Day Manager","Record Updated Successfully");                        
                },  error => {console.log(error);}
                );

// 278         x_Roster.AuditAction = "AUTO CREATE CANCELLED SHIFT PAY"
// s_Reason = x_Roster.AuditAction

               
                   let   sqlInsert = {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
                    sqlInsert.TableName='[Audit]';   
                    sqlInsert.Columns=`Operator,ActionDate,ActionOn,WhoWhatCode,TraccsUser,AuditDescription `;

                    
                     sqlInsert.ColumnValues=` '${this.user}', '${format(this.today,'yyyy/MM/dd HH:MM:ss')}', 'Web Day Manager', '${this.user}', '${this.shift.recordno}', 
                             'AUTO CREATE CANCELLED SHIFT PAY ${this.shift.recordno }  ${this.shift.date} at  ${this.shift.starttime} 0 for  + ${s_AbsenceType}  WITH   ${this.shift.recipient}  -SERVICE CANCELLED'`
                         
                    
                    this.listS.insertlist(sqlInsert).subscribe(data=>{
                        // this.globalS.sToast("Day Manager","Record Updated Successfully");
                     },  error => {this.globalS.eToast('Cancellation-Audit', error);}
                     );
     

                    sqlInsert.TableName='History ';   
                    sqlInsert.Columns=` PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator `;
                
              
                    sqlInsert.ColumnValues=` '${this.NewRecordNo}', '${format(this.today,'yyyy/MM/dd HH:MM:ss')}', 
                        'ORIGINALLY SCHEDULED FOR ${s_AbsenceType} BY ${this.shift.carercode}','SVCNOTE', '#AUTO-CANCEL', '${this.NewRecordNo}', '${this.user}'  `; 
        
                    this.listS.insertlist(sqlInsert).subscribe(data=>{
                        // this.globalS.sToast("Day Manager","Record Updated Successfully");
                        //this.DoneCancelShift();
                    },
                    error => {this.globalS.eToast('Cancellation-SVC-2', error);}
                    );

                });  

// 'CREATE THE ROSTER RECORD
// 280         If SaveLeaveAbsence(rsNewRecord, s_LeaveRecordNo) Then
// 282             CreateServiceNote Now, operatorid, s_LeaveRecordNo, strMessage & " : " & s_Reason, "#AUTO-CANCEL", s_status
// TraccsDb.Execute "UPDATE Audit SET WhoWhatCode = '" & N2S(s_LeaveRecordNo) & "' WHERE WhoWhatCode = '" & s_RosterRecordNumber & "'"
// End If

// 'Anwaar 22Jan16:
// let s_MobileFutureLimit : string
// 284         s_MobileFutureLimit = DBLookup(TraccsDb, "ISNULL(MobileFutureLimit , 0) AS MobileFutureLimit ", "UserInfo", "StaffCode  = '" & RosterRecord(2) & "'", 1)
// 286         If s_MobileFutureLimit = "" Then s_MobileFutureLimit = "0"

// 288         If DBLookup(TraccsDb, "ISNULL(FLAG_MessageOnShiftChange, 0) AS FLAG_MessageOnShiftChange", "Staff", "AccountNo = '" & RosterRecord(2) & "'", 1) _
// And Format$(N2S(RosterRecord(6)), "yyyy/mm/dd") < Format$(DateAdd("d"observerable2ngOn, s_MobileFutureLimit, Now), "yyyy/mm/dd") _
// And Format$(N2S(RosterRecord(6)), "yyyy/mm/dd") >= Format$(Now, "yyyy/mm/dd") _
// Then
// 'Anwaar 26Nov2020:
// '290            If MsgBox("Alert Staff", vbYesNo + vbQuestion) = vbYes Then
// rFormOpen.Arguments(0) = "ALERTSTAFF": rFormOpen.Arguments(1) = "SHIFT CANCELLED"
// rFormOpen.Arguments(2) = "Shift CANCELLED :" & vbCrLf & Format$(N2S(RosterRecord(6)), "dd/mm/yyyy") & ":" & N2S(RosterRecord(7)) & ":" & vbCrLf & N2S(RosterRecord(1))
// frmScript.Show 1
// If rFormClose.Status = "OK" Then
// '-----
// 292                 TRACCSMessage "STAFF", N2S(RosterRecord(2)), rFormClose.Arguments(0), rFormClose.Arguments(1)
// End If
// End If
// '---------------------

// End With

// 294 AutoCreateRecipientAbsence = True

return true;
}

Create_PayMakeUp(v:any){
    let s_Records:any;
    let i_Pay_Makeup_Threshold:number=0;
    let i_LeadTime = findDateTimeDifference( this.today, new Date(v.date + " " + v.startTime),'m')
    if ( this.registration.useAwards && i_LeadTime >= i_Pay_Makeup_Threshold ){
        
            s_Records= v.recordno;
       
    }
    this.timeS.create_PayMakeUp(s_Records).subscribe(d=>{});
}
 DoneCancelShift(){
              this.view=false;
              this.Done.emit(true)
              this.ngOnDestroy();
          }

clear_fields(){
    this.showNSDF=false;
    this.AlternativeRecipient=null;
    this.BillOriginalActivity=false;
    this.CancelCode=null;
    this.CancelShiftProgram=null;
    this.CancelPayType=null;
    this.CancelShiftServiceType=null;
    this.CancelType=null;
    this.Cancel_shift_Detail=null;
    this.CancellationType=null
    this.selectedCategory=null;
    this.CaseNote=false;
    this.OpNote=true
    this.PayStaff=false;
    this.FillProgram=false;
    this.OriginalNDAI=false;
    this.EmailUser=null;
    this.NoPermission=false;
    this.MultipleEmail=false;
    this.ListType=null
    this.txtOPNote=null;
    this.ReminderUser=null
    this.reminderDate=null;
    this.chkPayType=false;
    this.chkServiceType=false;
    this.chkProgram=false;
    this.FillProgram=false;
    this.chk0=false;
    this.chk3=false;
    this.chk3_Visible=false;
    this.chk4_Visible=false;
    this.chk5_Visible=false;
    this.chk5_Visible=false;
    this.chk7_Visible=false;
    this.defaultData=null;
    
}
onListlItemSelected(data:any,i:number){
    this.HighlightRow2=i;
    this.SetListValue(data.title);
    
}
onListItemDbClick(data:any,i:number){
    this.HighlightRow2=i;
    this.SetListValue(data.title)
    this.ViewList=false;
}
ListSelectionDone(){
    this.ViewList=false;

          }
SetListValue(value:any){
   

    switch(this.ListType){
        case 'Program':
            this.CancelShiftProgram=value;
            break;
        case 'ServiceType':
            this.CancelShiftServiceType=value;
            break;     
        case 'PayType':
            this.CancelPayType=value;
            break;   
        default:
           
    }
          }

 clearProgram(){
    if (!this.FillProgram) 
        this.CancelShiftProgram=null;
   else
       this.CancelShiftProgram = "SELECT Program to apply to " + this.s_MultiStatus == "SINGLE"? "Cancellation...": "Cancellations..."
     
 }
openList(type:string){
            this.ListType=type;
           
            switch(type){
                case 'Program':
                    this.CancelShiftProgram=null;
                    if (!this.FillProgram) return;
                    this.datalist=this.lstPrograms;
                    break;
                case 'ServiceType':
                    if (this.chkServiceType) return;                    
                    this.datalist=this.lstServiceTypes;
                    break;     
                case 'PayType':
                    if (this.chkPayType) return;
                    this.datalist=this.lstPayTypes;
                    break;   
                default:
                    this.datalist=[];  
            }

            this.ViewList=true;
          }
load_lists(){
              let sql='';

              this.listS.getlist( `SELECT * FROM UserInfo WHERE Name = '${this.user}'`).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.userData=data[0];   
                
              });
              this.listS.getlist( `select	WizardDefaultNote,ndiaCancelFeePercent,autoAutoApproveRecipientAdmin,useAwards,
                                    (select PayPeriodEndDate from systable) as payPeriodEndDate
                                    from Registration`).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                   
                 this.registration=data[0];          
                 this.PayPeriodEndDate=this.registration.payPeriodEndDate;
              });
              
           
            sql=` SELECT DISTINCT UPPER([Name]) as name FROM USERINFO`;
            this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.lstUsers=data.map(x=>x.name);
            });
           
            this.loadCategory();
        }
load_ServiceTypes(){

   
   // if (!event) return;
    let sql='';
    sql=`SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >= '${this.currentDate}') 
    AND EXISTS (SELECT SO.[Service Type] FROM ServiceOverview SO 
        WHERE ItemTypes.[Title] = SO.[Service Type] 
        AND SO.PersonID in  (select top 1 RecordNumber from HumanResourceTypes where Name='${this.program}'))  
        AND RosterGroup = 'RECPTABSENCE'  AND Status = 'ATTRIBUTABLE' 
         AND ProcessClassification = 'EVENT'  ORDER BY Title`;
   
    let  sqlServiceType=`SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >= '${this.currentDate}') AND RosterGroup = 'ADMINISTRATION'  AND Status = 'NONATTRIBUTABLE'  AND ProcessClassification = 'OUTPUT'  ORDER BY Title`;
              
    let  sqlpayType=`SELECT Recnum, LTRIM(RIGHT(Title, LEN(Title) - 0)) AS Title FROM ItemTypes WHERE RosterGroup = 'SALARY'   AND Status = 'NONATTRIBUTABLE'   AND ProcessClassification = 'INPUT'   AND Title BETWEEN ''                 AND 'zzzzzzzzzz'AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY TITLE`;
   
  

      this.observerable2 =  new Observable(observer => {
    
        forkJoin( 
            this.SelectFundingProgram(),
            this.listS.getlist(sqlServiceType),
            this.listS.getlist(sqlpayType) ,
            this.listS.getlist(sql)                    
        ).subscribe(data=>{
            this.lstPrograms=data[0];
            this.lstServiceTypes=data[1];
            this.lstPayTypes=data[2];
            this.lstCancelCodes=data[3];
          
        });
   
});
this.observerable2.subscribe(
    d => {
        console.log(d);
       
        this.loading=false;  

    },
    err => console.error('Observer got an error: ' + err),
    () => console.log('Observer got a complete notification')
  );
 
  
    
}        
log(value: any): void {
    console.log(value);
    this.loadCategory();
   
}

loadCategory(){
    let sql='';
    if (this.OpNote=='OPNOTE')
        sql=`select [Description] from DATADOMAINS WHERE DOMAIN = 'RECIPOPNOTEGROUPS'`
    else
        sql=`select [Description] from DATADOMAINS WHERE DOMAIN = 'CASENOTEGROUPS'`

    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.lstcategory=data.map(x=>x.description);
        this.loading=false;
    });

}
fillRecipient(){
           let  ServiceType= this.shift.activity
              let sql=`SELECT DISTINCT R.UniqueID AS [ID], R.AgencyIdReportingCode AS [UR Number], R.AccountNo , UPPER([Surname/Organisation]) + ', ' + CASE WHEN FirstName <> '' THEN FirstName ELSE ' ' END + CASE WHEN MiddleNames <> '' Then ' ' + MiddleNames ELSE ' ' END AS [Name], R.Gender, R.DateOfBirth AS [D.O.B.], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE N2.Address END  AS Address, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact END AS Contact, R.Type, R.Branch, R.ONIRating, N1.Suburb  
              FROM Recipients R INNER JOIN ServiceOverview ON R.UniqueID = ServiceOverview.Personid 
              LEFT JOIN (SELECT PERSONID, Suburb,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = R.UniqueID LEFT JOIN (SELECT PERSONID,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress <> 1)  AS N2 ON N2.PersonID = R.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = R.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone <> 1)  AS P2 ON P2.PersonID = R.UniqueID  WHERE ServiceOverview.[Service Type] = '${ServiceType}' AND  (AccountNo <> '!INTERNAL') AND (AccountNo <> '!MULTIPLE') AND (admissiondate is not null) and (DischargeDate is null)  ORDER BY AccountNo`
             
              this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.lstRecipient=data;
            });
    

            
    
          }

          GETSERVICEACTIVITY(program: any): Observable<any> {

            //    const { serviceType, date, time } = this.bookingForm.value;
               
                if (!program) return EMPTY;
                
                let recipientCode=this.shift.recipient;
                                        
                 
                    return this.timeS.getActivities({            
                        recipient: recipientCode,
                        program:program,  
                        forceAll: "1", //recipientCode=='!MULTIPLE' || recipientCode=='!INTERNAL' ? "1" : "0",   
                        mainGroup:  'ALL',
                        subGroup: '-',           
                        viewType: this.viewType,
                        AllowedDays: "0",
                        duration: '5'           
                    });
            
               
              
                
            }

            
SelectFundingProgram()
{   

    let SQLStmt='';
    let s_Garbage : string
    let s_LimitScopeSQL :string
    let s_OpenScopeSQL : string
    let s_CloseDateSQL : string

    s_CloseDateSQL = ` (ISNULL(pr.CloseDate, '2000/01/01') < '${this.currentDate}') AND `;
    
    s_LimitScopeSQL = `SELECT DISTINCT [Program] AS Title FROM RecipientPrograms 
                  INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID 
                  INNER JOIN HumanResourceTypes pr ON RecipientPrograms.Program = pr.name 
                  WHERE Recipients.AccountNo = '${this.shift.recipient}' AND 
                  ${s_CloseDateSQL}
                  RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') 
                  ORDER BY [Program]`

    SQLStmt=`SELECT Name as Title FROM HumanResourceTypes HR WHERE  [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= '${this.currentDate}')  ORDER BY [Name]`

    if (!SQLStmt) return EMPTY;
        return this.listS.getlist(SQLStmt);


}
onItemSelected(data:any, inumber){
                this.AlternativeRecipient=data.accountNo;
    }
onItemDbClick(data:any, inumber){
            this.AlternativeRecipient=data.accountNo;
            this.ViewRecipeintList=false;
        }
SetViewRecipeint(){
    
            this.ViewRecipeintList=false;
        }
    }

    