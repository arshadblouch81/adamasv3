import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';

import {
    IntakeAlerts,
    IntakeBranches,
    IntakeConsents,
    IntakeFunding,
    IntakeGoals,
    IntakeGroups,
    IntakePlans,
    IntakeServices,
    IntakeStaff,
    ServicesPlan,
    IntakePrefStaff,
    IntakeCompetencies,
    IntakePlacement
} from '@intakes/index';
import { NzIconModule } from 'ng-zorro-antd';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'branches',
        pathMatch: 'full'
      },
      {
        path: 'alerts',
        component: IntakeAlerts
      },
      {
        path: 'branches',
        component: IntakeBranches
      },
      {
        path: 'consents',
        component: IntakeConsents
      },
      {
        path: 'funding',
        component: IntakeFunding
      },
      {
        path: 'goals',
        component: IntakeGoals
      },
      {
        path: 'groups',
        component: IntakeGroups
      },
      {
        path: 'plans',
        component: IntakePlans
      },
      {
        path: 'services',
        component: IntakeServices
      },
      {
        path: 'staff',
        component: IntakeStaff
      },
      {
        path: 'service-plans',
        component: ServicesPlan
      },
      {
        path: 'pref-staff',
        component: IntakePrefStaff
      },
      {
        path: 'competencies',
        component: IntakeCompetencies
      },
      {
        path: 'placements',
        component: IntakePlacement
      }
      
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,
    NzIconModule
    
  ],
  declarations: [
   
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class IntakeAdminModule {}

