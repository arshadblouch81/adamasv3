:: Call Develop Script
PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& './develop-script-csproj.ps1'"

set rootAdamasReleaseFolder="D:\Programming\Adamas\adamasv3\bin\Release\net6.0-test"
set rootAdamasRootFolder="D:\Programming\Adamas\adamasv3"

D:
echo %rootAdamasReleaseFolder%

cd /d %rootAdamasReleaseFolder%

IF EXIST %rootAdamasReleaseFolder%	(
	echo "exist"
	rmdir /s /q %rootAdamasReleaseFolder%
) ELSE (
	echo "does not exist"
)


cd /d %rootAdamasRootFolder%
dotnet publish --self-contained true --configuration Release --output %rootAdamasReleaseFolder% AdamasV3.csproj

cd /d %rootAdamasReleaseFolder%
del jsreport.Binary.dll
del appsettings.Development.json
del appsettings.json

call git init
call git remote add origin https://github.com/maartri/adamas-production.git
call git branch testing
call git checkout -b testing
call git add -A
call git commit -m "first comm"
call git push origin testing -f

pause