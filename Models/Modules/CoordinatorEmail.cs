namespace Adamas.Models.Modules{
    public class CoordinatorEmail {
        public bool IsRecipient  { get; set; }
        public string AccountName { get; set; }
    }
}