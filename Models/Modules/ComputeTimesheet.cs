using System;

namespace Adamas.Models.Modules
{
    public class ComputeTimesheet
    {
        public double? KMAllowancesQty { get; set; }
        public double? AllowanceQty { get; set; }
        public double? WorkedHours { get; set; }
        public double? PaidAsHours { get; set; }
        public double? PaidAsServices { get; set; }
        public double? WorkedAttributableHours { get; set; }
        public double? PaidQty { get; set; }
        public double? PaidAmount { get; set; }
        public double? ProvidedHours { get; set; }
        public double? BilledAsHours { get; set; }
        public double? BilledAsServices { get; set; }
        public double? BilledQty { get; set; }
        public double? BilledAmount { get; set; }


    }
}