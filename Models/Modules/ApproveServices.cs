
namespace Adamas.Models.Modules{
    public class ApproveServices {
        public string RecipientCode { get; set; }
        public string BookDate { get; set; }
        public string StartTime { get; set; }
    }
}