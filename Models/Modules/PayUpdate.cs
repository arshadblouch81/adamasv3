using System;

namespace Adamas.Models.Modules
{
    public class PayUpdate
    {
        public string BatchNumber { get; set; }
        public string Branches { get; set; }
        public string Fundings { get; set; }
        public string Categories { get; set; }
        public string Programs { get; set; }
        public string Staffs { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ChkStaffPays { get; set; }
        public string ChkPayBrokerage { get; set; }
        public string ChkCreateExportFile { get; set; }
        public string ChkAwardInterExport { get; set; }
        public string SelectedPackage { get; set; }
        public string InputDefaultAccount { get; set; }
        public string InputDefaultProject { get; set; }
        public string InputDefaultActivity { get; set; }
        public string OperatorID { get; set; }
        public DateTime CurrentDateTime { get; set; }
     
    }
}