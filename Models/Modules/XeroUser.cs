using System;

namespace Adamas.Models.Modules{
    public class XeroUser{
        public string Staff { get; set; }
        public string XeroEmployeeId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; } 
        public string GLCode { get; set; }
        public string TrackingItemId { get; set; }
        public string EarningsCode { get; set; }
        public string XeroEarningsRateId { get; set; }
        public string Hours { get; set; }

        internal static XeroUser ParseFromCSV(string line){

            var columns = line.Split(",");

            return new XeroUser
            {
                Staff = columns[0],
                XeroEmployeeId = columns[1],
                StartDate = columns[2],
                EndDate = columns[3],
                GLCode = columns[4],
                TrackingItemId = columns[5],
                EarningsCode = columns[6],
                XeroEarningsRateId = columns[7],
                Hours = columns[8],
            };
        }
    }



}