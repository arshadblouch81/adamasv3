﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DocuSign.eSign.Api;
using DocuSign.eSign.Model;
using eg_03_csharp_auth_code_grant_core.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using DocuSign.eSign.Client;
using eg_03_csharp_auth_code_grant_core.Common;
using Microsoft.AspNetCore.Hosting;

using Adamas.Models.Modules;
using Adamas.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Adamas.Models.Modules;
using AdamasV3.Models.Dto;

namespace Adamas.Controllers
{
    [Route("eg002")]
    [Obsolete]
    public class Eg002SigningViaEmailController : EgController
    {
        private readonly DatabaseContext _context;
        private readonly string pathString;
        public Eg002SigningViaEmailController(
            DSConfiguration config, 
            IRequestItemsService requestItemsService, 
            DatabaseContext context,
            string pathString
            ) 
            : base(config, requestItemsService)
        {
            _context = context;
            this.pathString = pathString;
        }

        public override string EgName => "eg002";

        // ***DS.snippet.0.start
        [Obsolete]
        public EnvelopeSummary DoWork(
            DocusignDocument document, 
            List<CustomSigner> signers, 
            List<Document> documents, 
            string signerEmail, 
            string signerName, 
            string ccEmail, 
            string ccName)
        {

            var accessToken = RequestItemsService.User.AccessToken;
            var basePath = RequestItemsService.Session.BasePath + "/restapi";
            var accountId = RequestItemsService.Session.AccountId;

            EnvelopeDefinition env = MakeEnvelope(signers,documents ,signerEmail, signerName, ccEmail, ccName);
            var config = new Configuration("");
            config.AddDefaultHeader("Authorization", "Bearer " + accessToken);
            EnvelopesApi envelopesApi = new EnvelopesApi(null);
            EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, env);
            RequestItemsService.EnvelopeId = results.EnvelopeId;

            var dbDoc = (from docs in _context.Documents
                where docs.Doc_Id  == document.DOC_ID select docs).FirstOrDefault();

            dbDoc.DocusignEnvelopeId = results.EnvelopeId;
            dbDoc.DocusignStatus = "";
            _context.SaveChanges();

            return results;
        }

        private EnvelopeDefinition MakeEnvelope(List<CustomSigner> signers, List<Document> documents ,string signerEmail, string signerName, string ccEmail, string ccName)
        {
            // \\SJCC.local\CompanyData\Corp

            // document 1 (html) has tag **signature_1**
            // document 2 (docx) has tag /sn1/
            // document 3 (pdf) has tag /sn1/
            
            // string doc2DocxBytes = Convert.ToBase64String(System.IO.File.ReadAllBytes(Config.docDocx));
            // string doc3PdfBytes = Convert.ToBase64String(System.IO.File.ReadAllBytes(Config.docPdf));           

            var docs =  documents.Select(x => new Document(){
                // DocumentBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(BuildPathString(x.DocumentBase64, x.DocumentId))),
                DocumentBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(System.IO.Path.Combine(this.pathString, "document", x.DocumentBase64))),
                Name = x.Name,
                FileExtension = x.FileExtension,
                DocumentId = x.DocumentId
            }).ToList();
            

            // create the envelope definition
            EnvelopeDefinition env = new EnvelopeDefinition();
            env.EmailSubject = "Please sign this document set";

            // The order in the docs array determines the order in the envelope
            env.Documents =  docs;

            ICollection<Signer> filteredSigners = new List<Signer>();

            var counter = 0;
            foreach(var signer in signers){

                filteredSigners.Add(new Signer(){
                    Email = signer.Email,
                    Name = signer.Name,
                    RecipientId = signer.RecipientId.ToString(),
                    RoutingOrder = signer.RoutingOrder
                });
                counter++;
            }

            // Signer signer1 = new Signer {
            //     Email = signers[0].Email,
            //     Name = signers[0].Name,
            //     RecipientId = $"{ Guid.NewGuid() }",
            //     RoutingOrder = "1"
            // };


            // Signer signer2 = new Signer {
            //     Email = signers[1].Email,
            //     Name = signers[1].Name,
            //     RecipientId = $"{ Guid.NewGuid() }",
            //     RoutingOrder = "3"
            // };

            // FOR CC
            // CarbonCopy cc1 = new CarbonCopy {
            //     Email = ccEmail,
            //     Name = ccName,
            //     RecipientId = "2",
            //     RoutingOrder = "2"
            // };            

            // SIGN-HERE - Class that sets signer tabs location and anchor string            
            // SignHere signHere1 = new SignHere
            // {
            //     AnchorString = @"\s1\",
            //     AnchorUnits = "pixels",
            //     AnchorYOffset = "10",
            //     AnchorXOffset = "20"
            // };

            // SignHere signHere2 = new SignHere
            // {
            //     AnchorString = @"\s2\",
            //     AnchorUnits = "pixels",
            //     AnchorYOffset = "10",
            //     AnchorXOffset = "20"
            // };

            // SignHere signHere3 = new SignHere
            // {
            //     AnchorString = @"\s3\",
            //     AnchorUnits = "pixels",
            //     AnchorYOffset = "10",
            //     AnchorXOffset = "20"
            // };

            // SignHere signHere4 = new SignHere
            // {
            //     AnchorString = @"\s4\",
            //     AnchorUnits = "pixels",
            //     AnchorYOffset = "10",
            //     AnchorXOffset = "20"
            // };
           

            // List<SignHere> signHereList = new List<SignHere>();
            // signHereList.Add(signHere1);
            // signHereList.Add(signHere2);
            // signHereList.Add(signHere3);
            // signHereList.Add(signHere4);


            
            // Tabs are set per recipient / signer
            // Tabs signer1Tabs = new Tabs {
            //     SignHereTabs = new List<SignHere> { signHere1 }
            // };

            // Tabs signer2Tabs = new Tabs {
            //     SignHereTabs = new List<SignHere> { signHere2 }
            // };
            
            int signerCounter = 1;
            foreach(var signer in filteredSigners){
                signer.Tabs = new Tabs {
                    SignHereTabs = new List<SignHere> { 
                        new SignHere(){
                            AnchorString = $@"\s{signerCounter}\",
                            Name = signer.Name
                        }
                    },
                    InitialHereTabs = new List<InitialHere> { 
                        new InitialHere(){
                            AnchorString = $@"\i{signerCounter}\",
                        }
                    },
                    FullNameTabs = new List<FullName> { 
                        new FullName(){
                            AnchorString = $@"\n{signerCounter}\",
                            Name = signer.Name
                        }
                    },
                    CompanyTabs = new List<Company> { 
                        new Company(){
                            AnchorString = $@"\co{signerCounter}\",
                            Name = signers[signerCounter - 1].Company,
                            Value = signers[signerCounter - 1].Company,
                            OriginalValue = signers[signerCounter - 1].Company,
                            Required = "false"
                        }
                    },
                    TitleTabs = new List<Title> { 
                        new Title(){
                            AnchorString = $@"\t{signerCounter}\",
                            Name = signers[signerCounter - 1].Title,
                            Value = signers[signerCounter - 1].Title,
                            OriginalValue = signers[signerCounter - 1].Title,
                            Required = "false"
                        }
                    },
                    DateSignedTabs = new List<DateSigned>{
                        new DateSigned(){
                            AnchorString = $@"\d{signerCounter}\",
                            Name = "",
                            Value = "",
                            TemplateLocked = "true"                            
                        }
                    }
                };
                signerCounter++;
            }

            // signer1.Tabs = signer1Tabs;
            // signer2.Tabs = signer2Tabs;

            // Add the recipients to the envelope object
            Recipients recipients = new Recipients
            {
                Signers = new List<Signer>(),
                CarbonCopies = new List<CarbonCopy>()
            };

            foreach(var signer in filteredSigners){
                recipients.Signers.Add(signer);
            }

            // recipients.Signers.Add(signer1);
            // recipients.Signers.Add(signer2);

            env.Recipients = recipients;
            // Request that the envelope be sent by setting |status| to "sent".
            // To request that the envelope be created as a draft, set to "created"
            env.Status = RequestItemsService.Status;

            return env;
        }

        private string GetInitial(string _tempName) {
            return "asd";
        }

        private string BuildPathString(string file, string docId){

            string SharedDocumentPath = string.Empty;
            DocumentDetails details = new DocumentDetails();

            using(var conn = _context.GetConnection() as SqlConnection)
            {
                conn.Open();

                using(SqlCommand cmd = new SqlCommand(@"SELECT TOP 1 ISNULL(SharedDocumentPath, '') AS SharedDocumentPath from Registration", conn))
                {
                    using(var rd = cmd.ExecuteReader())
                    {
                        if(rd.Read())
                        {
                            SharedDocumentPath = rd.GetString(0);
                        }
                    }
                }

                using(SqlCommand cmd = new SqlCommand(@"SELECT TOP 1 FileName, OriginalLocation FROM Documents WHERE DOC_ID = @docID", conn))
                {
                    cmd.Parameters.AddWithValue("@docID", docId);
                    using(var rd = cmd.ExecuteReader())
                    {
                        if(rd.Read())
                        {
                            details.Filename = rd.GetString(0);
                            details.OriginalLocation = string.IsNullOrEmpty(SharedDocumentPath) ? rd.GetString(1) : RemovePathRoot(rd.GetString(1), SharedDocumentPath);
                        }
                    }
                }
            }
            var finalPath = System.IO.Path.Combine("\\SJCC.local\\CompanyData\\Corp", details.OriginalLocation, file);            
            return finalPath;
        }

        private string RemovePathRoot(string folder, string directoryPath){
            string root = System.IO.Path.GetPathRoot(folder);

            if (root != null)
            {
                folder = folder.Substring(root.Length);
            }
            return System.IO.Path.Combine(directoryPath, folder);
        }

        private byte[] document1(string signerEmail, string signerName, string ccEmail, string ccName)
        {
            // Data for this method
            // signerEmail
            // signerName
            // ccEmail
            // ccName

            return Encoding.UTF8.GetBytes(
            " <!DOCTYPE html>\n" +
                "    <html>\n" +
                "        <head>\n" +
                "          <meta charset=\"UTF-8\">\n" +
                "        </head>\n" +
                "        <body style=\"font-family:sans-serif;margin-left:2em;\">\n" +
                "        <h1 style=\"font-family: 'Trebuchet MS', Helvetica, sans-serif;\n" +
                "            color: darkblue;margin-bottom: 0;\">World Wide Corp</h1>\n" +
                "        <h2 style=\"font-family: 'Trebuchet MS', Helvetica, sans-serif;\n" +
                "          margin-top: 0px;margin-bottom: 3.5em;font-size: 1em;\n" +
                "          color: darkblue;\">Order Processing Division</h2>\n" +
                "        <h4>Ordered by " + signerName + "</h4>\n" +
                "        <p style=\"margin-top:0em; margin-bottom:0em;\">Email: " + signerEmail + "</p>\n" +
                "        <p style=\"margin-top:0em; margin-bottom:0em;\">Copy to: " + ccName + ", " + ccEmail + "</p>\n" +
                "        <p style=\"margin-top:3em;\">\n" +
                "  Candy bonbon pastry jujubes lollipop wafer biscuit biscuit. Topping brownie sesame snaps sweet roll pie. Croissant danish biscuit soufflé caramels jujubes jelly. Dragée danish caramels lemon drops dragée. Gummi bears cupcake biscuit tiramisu sugar plum pastry. Dragée gummies applicake pudding liquorice. Donut jujubes oat cake jelly-o. Dessert bear claw chocolate cake gummies lollipop sugar plum ice cream gummies cheesecake.\n" +
                "        </p>\n" +
                "        <!-- Note the anchor tag for the signature field is in white. -->\n" +
                "        <h3 style=\"margin-top:3em;\">Agreed: <span style=\"color:white;\">**signature_1**/</span></h3>\n" +
                "        </body>\n" +
                "    </html>"
                );
        }
        // ***DS.snippet.0.end

        // [HttpPost]
        // public IActionResult Create(List<Signer> signers,string signerEmail, string signerName, string ccEmail, string ccName)
        // {
        //     // Check the token with minimal buffer time.
        //     bool tokenOk = CheckToken(3);
        //     if (!tokenOk)
        //     {
        //         // We could store the parameters of the requested operation 
        //         // so it could be restarted automatically.
        //         // But since it should be rare to have a token issue here,
        //         // we'll make the user re-enter the form data after 
        //         // authentication.
        //         RequestItemsService.EgName = EgName;
        //         return Redirect("/ds/mustAuthenticate");
        //     }
        //     EnvelopeSummary results = DoWork(signers,signerEmail, signerName, ccEmail, ccName);
        //     ViewBag.h1 = "Envelope sent";
        //     ViewBag.message = "The envelope has been created and sent!<br />Envelope ID " + results.EnvelopeId + ".";
        //     return View("example_done");
        // }
    }
}