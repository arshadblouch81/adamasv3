﻿using System.Text;
using Microsoft.VisualBasic;

namespace AdamasV3.DAL
{
    public static class PasswordProcess
    {

        public static string Encrypt(string passwd)
        {
            string enryptedStr = "";

            for (var a = 0; a < passwd.Length; a++)
            {
                byte[] bytes = new byte[1];
                bytes[0] = (byte)(Strings.AscW(passwd[a]) + 50);
                enryptedStr = enryptedStr + new string(Encoding.GetEncoding(1252).GetChars(bytes));
            }

            return enryptedStr;
        }

        public static string Decrypt(string passwd)
        {
            return "";
        }

        public static bool HasSpecialCharacters(string passwd)
        {
            int specialCharacterCounter = 0;

            foreach (char c in passwd)
            {
                if (c > 127)
                {
                    specialCharacterCounter++;
                }
            }

            return (specialCharacterCounter > 0);
        }
    }
}
