using System.Collections.Generic;
using System;

namespace Adamas.Models.Modules
{
    public class CompetenciesInput
    {
      public string Competency { get; set; }
      public DateTime? ExpiryDate { get; set; }
      public DateTime? ReminderDate { get; set; }
      public string CertReg { get; set; }
      public string Notes { get; set; }
      public bool? Mandatory { get; set; }
    }
}