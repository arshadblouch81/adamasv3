namespace Adamas.Models.Modules{
    public class RecordIncident {
        public string PersonId { get; set; }
        public string IncidentType { get; set; }
        public string Service { get; set; }
        public string IncidentSeverity { get; set; }
        public string Location { get; set; }
        public string Note { get; set; }
        public string Staff { get; set; }
        public string OperatorId { get; set; }
        public string RecipientCode { get; set; }
        public string Program { get; set; }
        public bool NoRecipient { get; set; }
    }
}