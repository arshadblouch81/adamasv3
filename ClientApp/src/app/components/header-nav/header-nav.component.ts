import { Component, OnInit, Input } from '@angular/core';
import { GlobalService, LoginService, roles, ShareService, ListService, TimeSheetService, TimerService } from '@services/index';
import { Router } from '@angular/router'
import { DomSanitizer } from '@angular/platform-browser';

import { EmailService } from '@services/index';
import { EmailMessage,EmailAddress } from '@modules/modules';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent implements OnInit {
  @Input() HIDE_CRUMBS: boolean = false;

  isVisible: boolean = false;
  globalSettingsDrawerVisible: boolean = false;
  isAdmin: boolean = false;

  roles = roles;

  tempRole: string;
  ifClientManager: boolean = false;
  
  pickedUser: any;

  settingsDrawerVisible: boolean = false;
  clientPortalMethod: boolean = false;

  logoPath: any;

  emailAddress: string;  
  emailTestingVisible: boolean= false;

  user: any;
 version:string;

  contactForm = this.fb.group({
    To: [''],
    // Attachments: null,
    Subject:[''],
    Body:['']
  });

  constructor(
    private globalS: GlobalService,
    private loginS: LoginService,
    private listS: ListService,
    private emailService:EmailService,
    private fb: FormBuilder,
    private timerS: TimerService
  ) { 
    // this.sharedS.emitMemberPicked$.subscribe(data => {
    //   console.log(data)
    // });
  }

  ngOnInit(): void {

    const token = this.globalS.decode();
    //if (token.role == roles.admin) {
      this.isAdmin = true;
    //}

    this.loginS.getcurrentuser().subscribe(data => this.user = data)

    this.tempRole = this.globalS.isRole();

    if (this.tempRole == roles.admin) {

    }
    else if (this.tempRole == roles.manager) {
        this.ifClientManager = this.tempRole == roles.manager ? true : false;

        this.listS.getclientportalmethod().subscribe(data => console.log(data));
    }
    else if (this.tempRole == roles.provider) {
    }

    // this.timeS.getbrandinglogo().subscribe(blob => {
    //   let objectURL = 'data:image/jpeg;base64,' + blob;
    //   this.logoPath = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    // })
    this.getCurrentVersion();
  }

  getCurrentVersion(): void {
    this.loginS.getcurrentversion().subscribe(d => this.version = d)
  }

  logout() {
    const token = this.globalS.decode();
    this.timerS.stopTimer();

    if(token){
      this.loginS.logout(token.uniqueID)
        .subscribe(data => {
          this.globalS.logout();
        });
    }
    
  }

  onClickOutside(event: Object) {
    if (event && event['value'] === true) {
      this.isVisible = false;
    }
  }

  toClientMembers() {
    var tempRole = this.globalS.isRole();
    if (tempRole == roles.manager) {
        // this.router.navigate(['client-manager/settings']);
    }
  }

  close(){
    this.settingsDrawerVisible = false;
    this.globalSettingsDrawerVisible = false;
  }

  methodChange(event: boolean){
    this.listS.updateclientportalmethod(event).subscribe(data => {
      this.globalS.sToast('Success','Client Method changed')
    });
  }

  populateGlobalSettings(): void{

  }

  populateFromRegistrationTable(): void{
    
  }

  closeEmail(){
    this.emailTestingVisible = false;
  }

  sendTestEmail(){

    var request = { 
      To: [{
          Name: "Mark",
          Email: this.contactForm.value.To
      }],
      Subject: this.contactForm.value.Subject,
      Body: this.contactForm.value.Body, 
    };

    this.emailService.testMail(request).subscribe(d=>{
     this.globalS.sToast('Success','Email succesfully sent');
     this.contactForm.reset();
    });
  }
  

}
