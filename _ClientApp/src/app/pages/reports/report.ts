
import {forkJoin,  Observable ,  merge ,  Subject, Subscriber, Subscription, EMPTY, from, of } from 'rxjs';
import { mergeMap, debounceTime, distinctUntilChanged, takeUntil, switchMap} from 'rxjs/operators';
import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';

import  {  TimeSheetService, ListService, states, types } from '../../services/index';

import * as moment from 'moment';
import { BsModalComponent } from 'ng2-bs3-modal';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';

interface ModalView {
    title: string,
    contentNo: number
}

@Component({
    selector: '',
    templateUrl:'./report.html',
    styles:[

        `
        i{
            float:left;
            margin-right:10px;
        }

        div > select{
            display:block;
            width:100%;
        }
        ul{
            list-style:none;
        }
        ul li{
            padding:5px 10px;
        }
        ul li:hover{
            background:#f8f8f8;
            cursor:pointer;
        }
        h4{
            font-weight:500;
            text-decoration:underline;
        }



    .budget-container{
        display:flex;
        justify-content: space-between;
        height:100%;
    }
    .budget-container h4{
        margin-top:10px;
    }
    .budget-wrapper{
        flex: 1 1 0rem;
        padding: 0 10px;
        overflow: hidden;
    }
    .budget-wrapper-8{
        flex: 7 1 10rem;
        height: 100%;
        background: #f3f3f3;
        padding: 0 15px;
        border: 1px solid #bebebe;
    }
    .budget-container ul{
        list-style:none;
        max-height: 13rem;
        overflow-y: auto;
        margin-left:10px;
    }
    .budget-container ul li{
        line-height:5px;
        padding:0;
    }
    div.budget-container div{
        font-size: 12px;
    }
    div.budget-wrap{   
        padding:8px 0 0 0;
    }
    .budget-options{
        margin-top:5rem;
    }
    li div label{
        font-weight:100;
    }
    .flex-wrapper{
        display:flex;
    }
    .flex-wrapper div{
        flex:1 !important;
    }




    div.clr-radio-wrapper > label{
        font-weight:100;
    }

    div.clr-checkbox-wrapper > label{
        font-weight:100;
    }

    ::ng-deep .stack-view > clr-stack-block > dt clr-icon{
        fill: #fff !important;
    }

    ::ng-deep .stack-view > clr-stack-block > dt clr-stack-label{
        color:#fff;
    }

    ::ng-deep .stack-view > clr-stack-block > dt{
        background:#1f8cc7 !important;
    }

    ::ng-deep .stack-view > clr-stack-block > dd{
        background:#1f8cc7 !important;
    }


    ::ng-deep .stack-view >>> clr-stack-block dt {
        flex: 0 0 65% !important;
        max-width: 100% !important;
        width:100%;
    }
    ::ng-deep .stack-view >>> clr-stack-block dd {
        flex: 0 0 35% !important;
    }
    
    ::ng-deep .stack-view{
        height:60vh !important;
    }
    ::ng-deep .stack-view >>> clr-stack-block.child > dt{
        padding-left: 2.5rem !important;
        font-weight: 100;
        font-size: 12px;
    }

    .filter-result{
        margin-left:10px;
    }

        `

    ]
})

export class ReportComponent implements OnInit, OnDestroy, AfterViewInit {
    private unsubscribe$ = new Subject()

    @ViewChild('modal', { static: false }) modalObject: BsModalComponent;
    modalEvent = new Subject<number>();
    modalView: ModalView;

    tab: number = 5;    
    list: Array<string> = [];
    branchActive: boolean = true;

    startDate: any;
    endDate: any;
    selectedValue: any;
  

    states: Array<string> = states;

    dataTypes: Array<string> = ['Use Billing Date','Use Pay Period End Date','Use Service Date'];
    statusCategories: Array<string> = ['All Services','Approved Services Only','Unapproved Service Only'];
    branchPrimacy: Array<string> = ['Automatic','Recipient Branch Overrides','Staff Branch Overrides'];
    ageStatuses: Array<string> = ['All','Over 64 or ATSI over 49','Under 65 or ATSI under 50']


    criteriaList = {
        branches: [],
        managers: [],
        fundingRegions: [],
        serviceRegions: [],
        serviceProviderIds:[],
        agencyPrograms: [],
        serviceBudgetCodes: [],
        careDomains: [],
        disciplines: [],
        costCentres: [],
        environments: [],
        funders: [],
        funderServiceTypes: [],
        services: [],
        abscences: [],
        paytypes: [],
        jobCategories: [],
        staffTeams: []
    };

    criteriaListGroup: FormGroup;

    constructor(
        private timeS: TimeSheetService,
        private listS: ListService,
        private formBuilder: FormBuilder
    ){
        this.timeS.getreport().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.list = data;
        });

        this.modalEvent.pipe(takeUntil(this.unsubscribe$)).subscribe(data =>{
            switch(data){
                case 1:   
                    this.modalView = {
                        title: 'MTA Reports',
                        contentNo: 1
                    }                
                    break; 
                case 2:
                    this.modalView = {
                        title: 'Audit Register',
                        contentNo: 2
                    }                
                    break; 

            }
            this.modalObject.open();
        })
        
       
    }
    
    ngOnInit(){
        this.startDate = moment();
        this.endDate = moment();
        this.resetGroup();
        // this.getAllCriteria();
    }

    getBranches(): Observable<any>{
        return this.listS.getreportcriterialist({
            listType: 'BRANCHES',
            includeInactive: false
        })
    }


    branches: Array<any>;
    serviceRegions: Array<any>;
    managers: Array<any>;

    funders: Array<any>;
    fundingRegions: Array<any>;
    serviceProviderIds: Array<any>;
    agencyPrograms: Array<any>;

    serviceBudgetCodes: Array<any>;
    funderServiceTypes: Array<any>;
    dataSetTypeStarts:Array<any>;
    rosterTypes: Array<any>;

    outletIds: Array<any>;
    staffCategories: Array<any>;
    staffTeams: Array<any>;
    payTypes: Array<any>;

    careDomains: Array<any>;
    disciplines: Array<any>;
    costCentres: Array<any>;
    environments: Array<any>;
    staffTypes: Array<any> = types.map(x => x).filter(x => x);

    ngAfterViewInit(){

            this.listS.getreportcriterialist({
                listType: 'BRANCHES',
                includeInactive: false
            }).subscribe(x => this.branches = x)

            this.listS.getreportcriterialist({
                listType: 'SERVICEREGIONS',
                includeInactive: false
            }).subscribe(x => this.serviceRegions = x )

            this.listS.getreportcriterialist({
                listType: 'MANAGERS',
                includeInactive: false
            }).subscribe(x => this.managers = x)

            this.listS.getreportcriterialist({
                listType: 'FUNDERS',
                includeInactive: false
            }).subscribe(x => this.funders = x)
       
            this.listS.getreportcriterialist({
                listType: 'FUNDINGREGIONS',
                includeInactive: false
            }).subscribe(x => this.fundingRegions = x)

            this.listS.getreportcriterialist({
                listType: 'PROVIDERIDS',
                includeInactive: false
            }).subscribe(x => this.serviceProviderIds = x)

            this.listS.getreportcriterialist({
                listType: 'PROGRAMS',
                includeInactive: false
            }).subscribe(x => this.agencyPrograms = x)

            this.listS.getreportcriterialist({
                listType: 'BUDGETCODES',
                includeInactive: false
            }).subscribe(x => this.serviceBudgetCodes = x)

            this.listS.getreportcriterialist({
                listType: 'FUNDERSERVICETYPES',
                includeInactive: false
            }).subscribe(x => this.funderServiceTypes = x)
 
            this.listS.getreportcriterialist({
                listType: 'CAREDOMAINS',
                includeInactive: false
            }).subscribe(x => this.careDomains = x)
            
            this.listS.getreportcriterialist({
                listType: 'DISCPLINES',
                includeInactive: false
            }).subscribe(x => this.disciplines = x)

            this.listS.getreportcriterialist({
                listType: 'COSTCENTRES',
                includeInactive: false
            }).subscribe(x => this.costCentres = x)
       

            this.listS.getreportcriterialist({
                listType: 'ENVIRONMENTS',
                includeInactive: false
            }).subscribe(x => this.environments = x)

            this.listS.getlist(`SELECT DISTINCT HACCType FROM ItemTypes WHERE ISNULL(IT_DATASET, 'OTHER') <> 'OTHER'`)
                .subscribe(x => this.dataSetTypeStarts = x.map(b => b.HACCType))

            this.listS.getrostertypelist()
                .subscribe(x => this.rosterTypes = x )

            this.listS.getlist(`SELECT DISTINCT UPPER([SERVICEOUTLETID]) AS outlet FROM CSTDAOutlets WHERE [CSTDA] = 1`)
                .subscribe(x => this.outletIds = x.map(b => b.outlet))

            this.listS.getlist(`SELECT DISTINCT UPPER(Description) AS description FROM DataDomains WHERE [Domain] = 'STAFFGROUP'`)
                .subscribe(x => this.staffCategories = x.map(b => b.description))                

            this.listS.getlist(`SELECT Description FROM DataDomains WHERE Domain = 'STAFFTEAM' ORDER BY Description`)
                .subscribe(x => this.staffTeams = x.map(b => b.Description))

            this.listS.getlist(`SELECT Title FROM ItemTypes WHERE (ProcessClassification = 'INPUT') AND (EndDate Is Null OR EndDate >= '10-09-2019') ORDER BY Title`)
                .subscribe(x => this.payTypes = x.map(b => b.Title))
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

  
    resetGroup(){
        this.criteriaListGroup = this.formBuilder.group({
            state: [''],
            branch: [''],
            serviceRegion: [''],
            manager:[''],

            funder: [''],
            fundingRegion: [''],           
            serviceProviderId: [''],
            agencyProgram: [''],

            serviceBudgetCode: [''],
            funderServiceType: [''],
            dataSetTypeStart: [''],
            rosterType: [''],

            careDomain: [''],
            discipline: [''],
            costCentre: [''],
            environment: [''],

            outletId: [''],
            staffType: [''],
            staffCategory: [''],
            staffTeam: [''],
            payType: [''],

            reportType: 'Detailed',
            excludePageHeader: true,
            includeFinancials: false,
            startDate: '',
            endDate: '',
            dataType: '',
            statusCategory: '',
            branchPrimary: '',
            ageStatus: '',
            includeRecipientLeave: false,
            includeRecipientUR: false,
            includeDateOfBirth: false,
            splitCodeHyphen: false,
            includeAllowances: false,
            includeTraccsInternalCost: false

            
          
        });

    }

    print(){
        this.timeS.getrosters(
            moment(this.startDate).format('YYYY/MM/DD'),
            moment(this.endDate).format('YYYY/MM/DD'),
            this.selectedValue
        ).pipe(   takeUntil(this.unsubscribe$))
            .subscribe(data => {
            console.log(data);
        })
    }

    arrayBuilder(data: any): Array<FormGroup>{

        var groupArr: Array<FormGroup> = [];
        groupArr = data.map(x => {
            return new FormGroup({
                name: new FormControl(x),
                selected: new FormControl(false)
            });
        })
        return groupArr;
    }

    reportTab: any;

    
    samokaaaa(){

        console.log(this.criteriaListGroup.value)
    //     this.reportTab = window.open("", "_blank");

     const { 
        ageStatus, agencyProgram, branch, branchPrimary, careDomain, costCentre, dataType, discipline, endDate,
        environment, excludePageHeader,funder,funderServiceType,
        fundingRegion, includeAllowances,
        includeDateOfBirth, includeFinancials,
        includeRecipientLeave, includeRecipientUR,
        includeTraccsInternalCost, manager,
        reportType, serviceBudgetCode,
        serviceProviderId, serviceRegion,
        splitCodeHyphen, startDate,
        state, statusCategory, 
        dataSetTypeStart,rosterType
     } = this.criteriaListGroup.value

     this.timeS.postsamplereport({
            startDate: moment(startDate).format(),
            endDate: moment(endDate).format(),
            branches: branch,
            states: state,
            serviceRegions: serviceRegion,
            managers: manager,
            funders: funder,
            fundingRegions: fundingRegion,
            serviceProviderIds: serviceProviderId,
            agencyPrograms: agencyProgram,
            serviceBudgetCodes: serviceBudgetCode,
            funderServiceTypes: funderServiceType,
            careDomains: careDomain,
            disciplines: discipline,
            costCentres: costCentre,
            environments: environment,
            reportType: reportType,
            dataSetTypeStart: dataSetTypeStart,
            rosterType: rosterType,
            excludePageHeader: excludePageHeader,
            includeFinancials: includeFinancials,
            dataType: dataType,
            statusCategory: statusCategory,
            branchPrimary: branchPrimary,
            ageStatus: ageStatus,
            includeRecipientLeave: includeRecipientLeave,
            includeRecipientUR: includeRecipientUR,
            includeDateOfBirth: includeDateOfBirth,
            splitCodeHyphen: splitCodeHyphen,
            includeAllowances: includeAllowances,
            includeTraccsInternalCost: includeTraccsInternalCost
        })
        .pipe(
            mergeMap(x => {
                return this.timeS.getreportpdfpath({                    
                    SqlStmt: `select top 1 * from Roster where [carer code]='ABBAS A'`,
                    reportPath: 'Reports',
                    reportName: 'rpt_Roster_3Level.rpx',
                    parameters: x       
                })
            })
        ).subscribe(data => console.log(data))


      


    }

    goToLink(url: string){
        this.reportTab.location.href = url;
        this.reportTab.focus();
    }

    filterMapToArray(list: Array<any>){
        return list.map(x => {
            return {
                name: x
            }
        })
    }

    filterMapName(list: Array<any>){
        return list.filter(x => x.selected).map(x => {
            return {
                name: x.name
            }
        })
    }

     //change(data:any, source: string){
     
         // this.ReportType = source;
       //  const excludePageHeader2 = this.criteriaListGroup.get('excludePageHeader').value;
    
       //  console.log(excludePageHeader2);
   //  }

}