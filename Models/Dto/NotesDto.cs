﻿namespace AdamasV3.Models.Dto
{
    public class NotesDto
    {
        public string Type { get; set; }
        public string Notes { get; set; }
    }
}
