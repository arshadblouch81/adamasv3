using System;
using System.IO;
using System.IO.Compression;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Text.RegularExpressions;

using ProgramEntities = Adamas.Models.Modules.Program;
using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using ImageMagick;
using Repositories;
using AdamasV3.Models.Dto;
//using ImageMagick;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class UploadController : Controller
    {
        private IUploadRepository uploadRepo;
        private readonly ProfilePhotos _profilePhotos;
        private IWebHostEnvironment _hostingEnvironment;
        private IUploadService _uploadService;
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private IConfiguration _configuration;
        private IListService listService;
        private string hostingPath;
        private IWordService wordService;

        // DEFAULT_FILE_SIZE = 5MB
        private const int DEFAULT_FILE_SIZE = 5242880;

        public UploadController(
            IWebHostEnvironment hostingEnvironment,
            IUploadService uploadService,
            DatabaseContext context,
            IGeneralSetting setting,
            IUploadRepository uploadRepo,
            IOptions<ProfilePhotos> profilePhotos,
            IListService _listService,
            IWordService _wordService,
            IConfiguration configuration)
        {
            this.uploadRepo = uploadRepo;
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
            _context = context;
            _profilePhotos = profilePhotos.Value;
            GenSettings = setting;
            hostingPath = _hostingEnvironment.ContentRootPath;
            listService = _listService;
            wordService = _wordService;
            _uploadService = uploadService;
        }

        [HttpPost("profile")]
        public async Task<IActionResult> PostProfilePicture([FromForm] FileUploadClass file)
        {
            var dir = _profilePhotos.Directory;
            var files = file.Files;

            if (files.Count > 1)
                return BadRequest();

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string filePathName = Path.Combine(dir, file.Files[0].FileName);
                    string oldFilePathName = string.Empty;

                    if (file.Role == "recipient")
                    {
                        using (SqlCommand cmd = new SqlCommand(@"SELECT FilePhoto FROM Recipients WHERE UniqueID = @uniqueID", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@uniqueID", file.UniqueID);
                            using (var rd = await cmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync())
                                {
                                    oldFilePathName = GenSettings.Filter(rd[0]);
                                }
                            }
                        }

                        using (SqlCommand cmd = new SqlCommand(@"UPDATE Recipients SET FilePhoto = @filePhoto WHERE UniqueID = @uniqueID", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@uniqueID", file.UniqueID);
                            cmd.Parameters.AddWithValue("@filePhoto", filePathName);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    if (file.Role == "staff")
                    {
                        using (SqlCommand cmd = new SqlCommand(@"SELECT FilePhoto FROM Staff WHERE UniqueID = @uniqueID", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@uniqueID", file.UniqueID);
                            using(var rd = await cmd.ExecuteReaderAsync()){
                                if(await rd.ReadAsync())
                                {
                                    oldFilePathName = GenSettings.Filter(rd[0]);
                                }
                            }
                        }

                        using (SqlCommand cmd = new SqlCommand(@"UPDATE Staff SET FilePhoto = @filePhoto WHERE UniqueID = @uniqueID", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@uniqueID", file.UniqueID);
                            cmd.Parameters.AddWithValue("@filePhoto", filePathName);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    if (!await this._uploadService.FileUploadAsync(dir, files, false))
                    {
                        return BadRequest();
                    }

                    if(!string.IsNullOrEmpty(oldFilePathName) && System.IO.File.Exists(oldFilePathName)){
                        System.IO.File.Delete(oldFilePathName);
                    }

                    Byte[] b = await Task.Run(() => System.IO.File.ReadAllBytes(filePathName));

                    transaction.Commit();
                    return Ok(new {
                        Image = Convert.ToBase64String(b)
                    });

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                     await GenSettings.ErrorLog("UploadController","PostProfilePicture", ex.Message, "System");
                    return BadRequest(ex);
                }
            }
        }

        [HttpPost("profile/temporary")]
        public async Task<IActionResult> PostTempProfile()
        {

            var files = Request.Form.Files;
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;

                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    //await this._uploadService.FileUploadAsync($"media/temporary", files);
                    // string profilePath = Path.Combine(hostingPath,  $"media/temporary");

                    // if(!Directory.Exists(profilePath)){
                    //     Directory.CreateDirectory(profilePath);
                    // } 

                    // foreach (var file in files)
                    // {
                    //     string pathWithImageName = Path.Combine(profilePath, file.FileName);                
                    //     using (var stream = new FileStream(pathWithImageName, FileMode.Create)) {
                    //         await file.CopyToAsync(stream);

                    //         stream.Position = 0;

                    //         var optimizer = new ImageOptimizer();
                    //         optimizer.Compress(stream);                    
                    //     }
                    // }

                    return Ok(files);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                     await GenSettings.ErrorLog("UploadController","profile/temporary", ex.Message, "System");
                    return BadRequest(ex);
                }
            }
        }

        [HttpPost("media/{personID}")]
        public async Task<IActionResult> PostMedia(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    var files = Request.Form.Files;

                    if (files.Count > 1)
                    {
                        return BadRequest();
                    }

                    var title = Request.Form["title"].ToString();
                    var description = Request.Form["description"].ToString();
                    var group = Request.Form["group"].ToString();


                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO Media (MediaDisplay,MediaText,Media,StartDate,EndDate,Program,Item,Type,Target) 
                                        VALUES(@disp, @txt, @media, @sdate, @edate,@prog,@item,@type,@target)", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@disp", title);
                        cmd.Parameters.AddWithValue("@txt", description);
                        cmd.Parameters.AddWithValue("@media", files[0].FileName);
                        cmd.Parameters.AddWithValue("@sdate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@edate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@prog", "");
                        cmd.Parameters.AddWithValue("@item", "");
                        cmd.Parameters.AddWithValue("@type", files[0].ContentType);
                        cmd.Parameters.AddWithValue("@target", string.IsNullOrEmpty(group) ? personID : group);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    string superfullpath = string.Empty;

                    foreach (var file in files)
                    {
                        //var sample = results.Select(x => x == file.Name).ToList();
                        //string fileName = String.Concat(personID,"-", file.FileName.Replace(" ", "_"));
                        superfullpath = Path.Combine(hostingPath, $"media/", file.FileName);


                        using (var stream = new FileStream(superfullpath, FileMode.Create)) await file.CopyToAsync(stream);

                    }

                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex);
                }
            }
        }

        [HttpGet("media/{uname}")]
        public async Task<IActionResult> GetMedias(string uname)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                using (SqlCommand cmd = new SqlCommand(@"  SELECT * FROM Media WHERE  Target = @uname 
                    UNION 
                    SELECT * FROM Media WHERE Target IN (
	                    SELECT hr.Name
	                    FROM HumanResources hr JOIN Recipients r ON r.UniqueID = hr.PersonID
	                    WHERE r.AccountNo = @uname AND hr.Type = 'RECIPTYPE' 
                    )", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@uname", uname);
                    await conn.OpenAsync();

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<BlobFile> files = new List<BlobFile>();

                        while (await rd.ReadAsync())
                        {
                            BlobFile file = new BlobFile();
                            file.MediaDisplay = GenSettings.Filter(rd["MediaDisplay"]);
                            file.MediaText = GenSettings.Filter(rd["MediaText"]);
                            file.Media = GenSettings.Filter(rd["Media"]);
                            file.StartDate = GenSettings.Filter(rd["StartDate"]).ToString();
                            file.EndDate = GenSettings.Filter(rd["EndDate"]).ToString();
                            file.Program = GenSettings.Filter(rd["Program"]);
                            file.Item = GenSettings.Filter(rd["Item"]);
                            file.Type = GenSettings.Filter(rd["Type"]);
                            file.Target = GenSettings.Filter(rd["Target"]);
                            files.Add(file);
                        }

                        return Ok(files);
                    }
                }
            }
        }

        [HttpPost("check/filetypes/{personID}")]
        public async Task<IActionResult> CheckFileTypes(string personID)
        {
            try
            {
                var filePath = _hostingEnvironment.ContentRootPath;

                var files = Request.Form.Files;
                var list = await Task.Run(() =>
                {
                    List<dynamic> listArr = new List<dynamic>();
                    List<dynamic> listStatusArr = new List<dynamic>();

                    int count = 0;

                    foreach (var file in files)
                    {
                        if (FileType.IsEnglish(file.Name))
                        {
                            listArr.Add(file);

                            var newPath = Path.Combine(Path.Combine(filePath, $"document/{personID}"), file.Name);
                            var fileIsUploadedBool = false;

                            if (System.IO.File.Exists(newPath))
                                fileIsUploadedBool = true;

                            listStatusArr.Add(new
                            {
                                name = file.Name,
                                fileIsAlreadyUploaded = fileIsUploadedBool,
                                size = file.Length,
                                type = file.ContentType
                            });
                        }
                        else
                            count++;
                    }

                    return new
                    {
                        filesNotIncluded = count,
                        files = listArr,
                        fileStatus = listStatusArr
                    };
                });

                return Ok(list);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost("upload/document/{personID}")]
        public async Task<IActionResult> UploadFile(string personID)
        {
            var realPath = await _uploadService.GetUploadDownloadDirectoryAsync();

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    // var filePath = _hostingEnvironment.ContentRootPath;     
                    // var newPath = Path.Combine(filePath, $"document\\{ personID }");

                    var files = Request.Form.Files;

                    if (files.Count < 1)
                    {
                        return BadRequest();
                    }

                    var fileArr = new List<string>();
                    var results = new List<string>();


                    // if (!Directory.Exists(realPath))
                    // {
                    //     return BadRequest();
                    // }


                    using (SqlCommand cmd = new SqlCommand())
                    {
                        int count = 0;

                        foreach (var file in files.Select(x => x.Name))
                        {
                            var paramName = "@user" + count.ToString();
                            cmd.Parameters.AddWithValue(paramName, file.ToString());
                            fileArr.Add(paramName);
                            count++;
                        }

                        cmd.CommandText = String.Format("SELECT FileName FROM Documents WHERE FileName IN ({0})", String.Join(",", fileArr));
                        cmd.Transaction = transaction;
                        cmd.Connection = conn;

                        using (var rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                                results.Add(GenSettings.Filter(rd["FileName"]));
                        }
                    }

                    if (files.Count != results.Count)
                    {

                        using (SqlCommand cmd = new SqlCommand())
                        {
                            const string sqlText = @"INSERT INTO Documents(PersonID, DocumentGroup,Title,[Status], Classification,Category,Created, Modified, [Filename], DocumentType,[OriginalLocation])
                                            VALUES(@personId{0},'DOCUMENT', @title{0},@status{0},'DEFAULT', 'FILE', @createdDate{0}, @modifiedDate{0},@fileName{0},@docType{0}, @originalLocation{0});";
                            int count = 0;
                            string query = string.Empty;

                            foreach (var entry in files)
                            {

                                var sample = results.Select(x => x == entry.Name).ToList();

                                if (sample.Count == 0)
                                {
                                    query += string.Format(sqlText, count);
                                    cmd.Parameters.AddWithValue(string.Format("@personId{0}", count), personID);

                                    SqlParameter title = new SqlParameter(string.Format("@title{0}", count), SqlDbType.NVarChar);
                                    title.Value = String.Concat(personID, "-", entry.FileName.Replace(" ", "_"));
                                    cmd.Parameters.Add(title);

                                    //cmd.Parameters.AddWithValue(string.Format("@title{0}",count), entry.FileName.Split('.')[0]);
                                    cmd.Parameters.AddWithValue(string.Format("@status{0}", count), "");
                                    cmd.Parameters.AddWithValue(string.Format("@createdDate{0}", count), DateTime.Now);
                                    cmd.Parameters.AddWithValue(string.Format("@modifiedDate{0}", count), DateTime.Now);

                                    SqlParameter param = new SqlParameter(string.Format("@fileName{0}", count), SqlDbType.NVarChar);
                                    param.Value = String.Concat(personID, "-", entry.FileName.Replace(" ", "_"));

                                    cmd.Parameters.Add(param);

                                    //cmd.Parameters.AddWithValue(string.Format("@fileName{0}",count), entry.FileName.Replace(" ",""));
                                    cmd.Parameters.AddWithValue(string.Format("@docType{0}", count), Path.GetExtension(entry.FileName));
                                    cmd.Parameters.AddWithValue(string.Format("@originalLocation{0}", count), realPath);
                                    count++;
                                }
                            }

                            cmd.Connection = (SqlConnection)conn;
                            cmd.CommandText = query;
                            cmd.Transaction = transaction;

                            await cmd.ExecuteNonQueryAsync();
                        }
                    }

                    string superfullpath = string.Empty;

                    foreach (var file in files)
                    {
                        var sample = results.Select(x => x == file.Name).ToList();
                        string fileName = String.Concat(personID, "-", file.FileName.Replace(" ", "_"));
                        superfullpath = Path.Combine(realPath, fileName);

                        if (sample.Count == 0)
                        {
                            using (var stream = new FileStream(superfullpath, FileMode.Create)) await file.CopyToAsync(stream);
                        }
                        else
                        {
                            if (System.IO.File.Exists(superfullpath))
                            {
                                System.IO.File.Delete(superfullpath);
                                using (var stream = new FileStream(superfullpath, FileMode.Create)) await file.CopyToAsync(stream);
                            }

                        }
                    }

                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        [HttpGet("documents/staff/{name}")]
        public async Task<IActionResult> GetStaffDocuments(string name)
        {

            var filePath = _hostingEnvironment.ContentRootPath;
            var downloadPath = await _uploadService.GetUploadDownloadDirectoryAsync();
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                

                using (SqlCommand cmd = new SqlCommand(@"SELECT DOC_ID, Documents.[Title], [Status] as [St], Classification, Documents.Category ,[Created], UserInfo.Name As Author, [Modified],
                            [Filename], DocumentType As [Type], OriginalLocation, PersonID FROM Documents LEFT JOIN UserInfo ON Documents.Author = UserInfo.Recnum 
                            INNER JOIN Staff ON Documents.[PersonID] = Staff.[UniqueID] WHERE Staff.[AccountNo] = @name AND DOCUMENTGROUP <> 'FORM'
                            AND (Documents.[DeletedRecord] = 0 OR Documents.[DeletedRecord] Is NULL) ORDER BY Documents.[Modified] DESC", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@name", name);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    while (await rd.ReadAsync())
                    {
                        var newPath = Path.Combine(downloadPath, GenSettings.Filter(rd["Filename"]));
                        var type = GetContentType(newPath);

                        long fileSize = 0;

                        if (System.IO.File.Exists(newPath))
                        {
                            fileSize = new FileInfo(newPath).Length;
                        }

                        list.Add(new
                        {
                            DocID = (int)rd["DOC_ID"],
                            Title = GenSettings.Filter(rd["Title"]),
                            Status = GenSettings.Filter(rd["St"]),
                            Classification = GenSettings.Filter(rd["Classification"]),
                            Category = GenSettings.Filter(rd["Category"]),
                            Created = GenSettings.Filter(rd["Created"]),
                            Author = GenSettings.Filter(rd["Author"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Modified = GenSettings.Filter(rd["Modified"]),
                            Filename = GenSettings.Filter(rd["Filename"]),
                            OriginalLocation = GenSettings.Filter(rd["OriginalLocation"]),
                            FileType = type,
                            Size = fileSize
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("documents/client/{name}")]
        public async Task<IActionResult> GetClientDocuments(string name)
        {

            var filePath = _hostingEnvironment.ContentRootPath;
            var downloadPath = await _uploadService.GetUploadDownloadDirectoryAsync();

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = new SqlCommand(@"SELECT DOC_ID, Documents.[PersonID], Documents.[Title], Documents.[Status] as [St], Classification, Documents.Category ,[Created], UserInfo.Name As Author, [Modified],
                        [Filename], DocumentType As [Type], OriginalLocation,PersonID FROM Documents LEFT JOIN UserInfo ON Documents.Author = UserInfo.Recnum 
                        INNER JOIN Recipients ON Documents.[PersonID] = Recipients.[UniqueID] WHERE Recipients.[AccountNo] = @name AND DOCUMENTGROUP <> 'FORM'
                        AND (Documents.[DeletedRecord] = 0 OR Documents.[DeletedRecord] Is NULL)
                        ORDER BY Documents.[Modified] DESC", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@name", name);

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();
                        while (await rd.ReadAsync())
                        {
                            var newPath = Path.Combine(Path.Combine(downloadPath, GenSettings.Filter(rd["Filename"])));
                            var type = GetContentType(newPath);

                            long fileSize = 0;
                            if (System.IO.File.Exists(newPath))
                            {
                                fileSize = new FileInfo(newPath).Length;
                            }

                            list.Add(new
                            {
                                DocID = (int)rd["DOC_ID"],
                                Title = GenSettings.Filter(rd["Title"]),
                                Status = GenSettings.Filter(rd["St"]),
                                Classification = GenSettings.Filter(rd["Classification"]),
                                Category = GenSettings.Filter(rd["Category"]),
                                Created = GenSettings.Filter(rd["Created"]),
                                Author = GenSettings.Filter(rd["Author"]),
                                Type = GenSettings.Filter(rd["Type"]),
                                Modified = GenSettings.Filter(rd["Modified"]),
                                Filename = GenSettings.Filter(rd["Filename"]),
                                OriginalLocation = GenSettings.Filter(rd["OriginalLocation"]),
                                FileType = type,
                                FileSize = fileSize
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("download-quote-document/{docId}")]
        public async Task<IActionResult> DownloadQuoteDocument(int docId)
        {

            try
            {
                var document = await (from d in _context.Documents where d.Doc_Id == docId select new { d.DocumentType, d.OriginalLocation, d.FileName }).FirstOrDefaultAsync();

                GetDocument doc = new GetDocument()
                {
                    FileName = document.FileName,
                    Extension = document.DocumentType,
                    DocPath = document.OriginalLocation,
                    PersonID = ""
                };

                string filename = string.Empty;
                if (!Path.HasExtension(doc.FileName))
                {
                    filename = $"{doc.FileName}{doc.Extension}";
                }
                else
                {
                    filename = doc.FileName;
                }

                var memory = await _uploadService.DownloadDocument(doc);
                var newPath = Path.Combine(this.hostingPath, $"document");
                var filePath = Path.Combine(newPath, filename);

                System.IO.File.WriteAllBytes(filePath, memory.ToArray());
                return File(memory, GetContentType(filePath), Path.GetFileName(filePath));


            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("download/document")]
        public async Task<IActionResult> Download([FromBody] GetDocument doc)
        {
              var newPath="";
                var filePath="";
            try
            {
               
                string filename = string.Empty;
                if (!Path.HasExtension(doc.FileName))
                {
                    filename = $"{doc.FileName}{doc.Extension}";
                }
                else
                {
                    filename = doc.FileName;
                }
              
                var memory = await _uploadService.DownloadDocument(doc);
                 newPath = Path.Combine(this.hostingPath, $"document");
                 filePath = Path.Combine(newPath, filename);
             
                System.IO.File.WriteAllBytes(filePath, memory.ToArray());
                return File(memory, GetContentType(filePath), Path.GetFileName(filePath));
            }
            catch (Exception ex)
            {
                await GenSettings.ErrorLog("UploadController", "download/document",  ex.Message , "System");
                return BadRequest(new { message = ex.Message} );
            }
        }

        [HttpDelete("delete/document/{personId}")]
        public async Task<IActionResult> Delete(string personId, FileForm form)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    //using (SqlCommand cmd = new SqlCommand(@"DELETE FROM documents WHERE DOC_ID = @id", (SqlConnection)conn, transaction))
                    using (SqlCommand cmd = new SqlCommand(@"update documents set DeletedRecord=1 WHERE DOC_ID = @id", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@id", form.Id);

                        var filePath = _hostingEnvironment.ContentRootPath;
                        var file = Path.Combine(filePath, $"document/{ personId }", form.Filename);

                        if (System.IO.File.Exists(file))
                        {
                            System.IO.File.Delete(file);
                        }
                        await cmd.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                     await GenSettings.ErrorLog("UploadController","delete/document/{personId}", ex.Message, "System");
                    return BadRequest(new { err = ex });
                }

            }
        }

        [HttpGet("document/template")]
        public async Task<IActionResult> GetDocumentTemplate()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                // var rootDIR = await uploadRepo.GetFileDirectoryAsync("templates");
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title AS FileTypes,Template as templateDirectory FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 ORDER BY FileTypes", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        var fileName = rd.GetString(0);
                        var templateDirectory = rd.GetString(1);

                        //var file = Path.Combine(templateDirectory, fileName);

                        var exists = false;
                        if (System.IO.File.Exists(templateDirectory)){
                            exists = true;
                        }

                        list.Add(new
                        {
                            Name        = fileName,
                            Description = fileName,
                            Template    = templateDirectory,
                            Exists      = exists
                        });

                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("document/template/{personID}/{fileName}")]
        public async Task<IActionResult> GetCheckFileName(string personID, string fileName)
        {
            if (fileName == null)
                return Content("filename not present");

            var filePath = _hostingEnvironment.ContentRootPath;
            var newPath = Path.Combine(filePath, $"document/{personID}", fileName);
            var exists = false;
            if (System.IO.File.Exists(newPath))
            {
                exists = true;
            }

            return Ok(exists);
        }

        [HttpPost("document-staff/template/referral")]
        public async Task<IActionResult> PostDocumentStaffTemplateReferral([FromBody] PostTemplate template)
        {
            return Ok(await _uploadService.PostDocumentStaffTemplateReferral(template));
        }


        [HttpPost("document-staff/template")]
        public async Task<IActionResult> PostDocumentStaffTemplate([FromBody] PostTemplate template)
        {
            // Get RecipientDocFolder
            var sourcePathWithFile = await uploadRepo.GetRecipientDocFolderViaUserInfo(template.User);

            // GET MERGE FIELDS of 
            var fields = await this.listService.GetRecipientQuoteMergeFields(template.PersonID);

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    // Directory Path
                    var dirPath = _hostingEnvironment.ContentRootPath;
                    
                    var extension = Path.GetExtension(template.Path);
                    var ID_NewFilename =  $"{template.PersonID}-{template.NewFileName}";

                      var sourcePath = Path.Combine(sourcePathWithFile, $"{ID_NewFilename}{extension}" );

                    if (!System.IO.File.Exists(template.Path))
                    {
                        return BadRequest(new Error
                        {
                            Message = "Source File doesn't exist"
                        });
                    }

                     using (SqlCommand cmd = new SqlCommand(@"INSERT INTO documents ([filename], title, personid, department, classification, category, documenttype, created, author, modified, typist, originallocation, paramaters, documentgroup, doc#, doccharges, alarmtext,AlarmDate, status,PublishToApp) 
                    VALUES (@fileName, @title, @personID,@department,  @classification, @category, @docType, @dateCreated, @author, @dateModified, @typist, @originalPath, '', 'DOCUMENT', '', '',@alarmtext, @AlarmDate, '',@PublishToApp)", (SqlConnection)conn, transaction))
                    {
                        
                        cmd.Parameters.AddWithValue("@fileName", ID_NewFilename);
                        cmd.Parameters.AddWithValue("@title", template.OriginalFileName);
                        cmd.Parameters.AddWithValue("@personID", template.PersonID);
                        cmd.Parameters.AddWithValue("@department", (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@classification", template.Classification);
                        cmd.Parameters.AddWithValue("@category", template.Category);
                        cmd.Parameters.AddWithValue("@docType", extension);
                        cmd.Parameters.AddWithValue("@dateCreated", DateTime.Now.ToString("MM-dd-yyyy"));
                        cmd.Parameters.AddWithValue("@dateModified", DateTime.Now.ToString("MM-dd-yyyy"));
                        cmd.Parameters.AddWithValue("@originalPath", sourcePath);
                        cmd.Parameters.AddWithValue("@author", "2");
                        cmd.Parameters.AddWithValue("@typist", "2");
                        cmd.Parameters.AddWithValue("@alarmtext", template.ReminderText ==null ? (object)DBNull.Value : template.ReminderText  );
                        cmd.Parameters.AddWithValue("@AlarmDate", template.ReminderDate ==null ? (object)DBNull.Value : template.ReminderDate );
                        cmd.Parameters.AddWithValue("@PublishToApp", template.PublishToApp ==null ? 0 : template.PublishToApp);


                        await cmd.ExecuteNonQueryAsync();
                    }


                    // using (SqlCommand cmd = new SqlCommand(@"INSERT INTO documents ([filename], title, personid, department, classification, category, documenttype, created, author, modified, typist, originallocation, paramaters, documentgroup, doc#, doccharges, alarmtext, status) VALUES (@fileName, @title, @personID, '', 'DEFAULT', 'FILE', @docType, @dateCreated, '2', @dateModified, '2', @originalPath, '', 'DOCUMENT', '', '', '', '')", (SqlConnection)conn, transaction))
                    // {
                    //     cmd.Parameters.AddWithValue("@fileName", ID_NewFilename);
                    //     cmd.Parameters.AddWithValue("@title", template.NewFileName);
                    //     cmd.Parameters.AddWithValue("@personID", template.PersonID);

                    //     cmd.Parameters.AddWithValue("@docType", extension);
                    //     cmd.Parameters.AddWithValue("@dateCreated", DateTime.Now.ToString("MM-dd-yyyy"));
                    //     cmd.Parameters.AddWithValue("@dateModified", DateTime.Now.ToString("MM-dd-yyyy"));
                    //     cmd.Parameters.AddWithValue("@originalPath", sourcePathWithFile);
                    //     await cmd.ExecuteNonQueryAsync();
                    // }

                  

                    var mergeResult = await wordService.MergeFieldsDocuments(fields, template.Path, sourcePathWithFile, ID_NewFilename);

                    if (!mergeResult.Success)
                    {
                        transaction.Rollback();
                       
                        return BadRequest(mergeResult);
                    }

                    transaction.Commit();
     
                    //await Task.Run(() =>
                    //{
                    //    System.IO.File.Copy(template.Path, sourcePath, true);
                    //});

                    //transaction.Commit();
                    return Ok(new {
                        Template = template,
                        SourcePath = template.Path,
                        Destination = Path.Combine(sourcePath, ID_NewFilename)
                    });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(new Error{
                        Message = ex.ToString()
                    });
                }
            }
        }


        [HttpPost("document/template")]
        public async Task<IActionResult> PostDocumentTemplate([FromBody] PostTemplate template)
        {
            // Get RecipientDocFolder
            var sourcePathWithFile = await uploadRepo.GetRecipientDocFolderViaUserInfo(template.User);

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    // Directory Path
                    var dirPath = _hostingEnvironment.ContentRootPath;
                    
                    var extension = Path.GetExtension(template.Path);
                    var newFileName = template.NewFileName;

                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO documents ([filename], title, personid, department, classification, category, documenttype, created, author, modified, typist, originallocation, paramaters, documentgroup, doc#, doccharges, alarmtext, status) VALUES (@fileName, @title, @personID, '', 'DEFAULT', 'FILE', @docType, @dateCreated, '2', @dateModified, '2', @originalPath, '', 'DOCUMENT', '', '', '', '')", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@fileName", template.NewFileName);
                        cmd.Parameters.AddWithValue("@title", newFileName);
                        cmd.Parameters.AddWithValue("@personID", template.PersonID);

                        cmd.Parameters.AddWithValue("@docType", extension);
                        cmd.Parameters.AddWithValue("@dateCreated", DateTime.Now.ToString("MM-dd-yyyy"));
                        cmd.Parameters.AddWithValue("@dateModified", DateTime.Now.ToString("MM-dd-yyyy"));
                        cmd.Parameters.AddWithValue("@originalPath", sourcePathWithFile);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    var destinationPath = Path.Combine(sourcePathWithFile, $"{newFileName}{extension}" );

                    if (!System.IO.File.Exists(template.Path))
                    {
                       return BadRequest(new Error{
                           Message = "Source File doesn't exist"
                       });
                    }

                    await Task.Run(() =>
                    {
                        System.IO.File.Copy(template.Path, destinationPath, true);
                    });

                    transaction.Commit();
                    return Ok(new {
                        Template = template,
                        Destination = destinationPath
                    });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }


        /* 
            Documents Preview in IFRAME
         */
        [HttpPost("copy-mta-document")]
        public async Task<IActionResult> CopyMTADocument([FromBody] GetDocument doc)
        {
            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();

                    Byte[] blobFile = new Byte[64];
                    using (SqlCommand cmd = new SqlCommand(@"[Copy_MTA_Document]", (SqlConnection)conn))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@PersonID", doc.PersonID);
                        cmd.Parameters.AddWithValue("@Extension", doc.Extension);
                        cmd.Parameters.AddWithValue("@FileName", doc.FileName);
                        cmd.Parameters.AddWithValue("@DocPath", doc.DocPath);

                        using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                        {
                            if (await rd.ReadAsync())
                                blobFile = GenSettings.Filter(rd[0]);
                        }

                        var memory = new MemoryStream(blobFile);

                        var newPath = Path.Combine(this.hostingPath, $"document");
                        var filePath = Path.Combine(newPath, doc.FileName);

                        System.IO.File.WriteAllBytes(filePath, memory.ToArray());
                        var GPath = this._configuration.GetValue<string>("DocumentPaths:GViewPath");
                        var DocPath = this._configuration.GetValue<string>("DocumentPaths:DocumentPreviewPath");
                        var Embed = this._configuration.GetValue<string>("DocumentPaths:Embedded");

                        var completePath = $"{GPath}{DocPath}{doc.FileName}{Embed}";

                        return Ok(
                            new
                            {
                                path = completePath,
                                fileName = doc.FileName
                            }
                        );
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        private string GetContentType(string path)
        {
            var ext = Path.GetExtension(path).ToLowerInvariant();
            var filetype = FileType.GetMimeType(ext);
            return filetype;
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
                {".ppt", "application/vnd.ms-powerpoint"},
                {".ods", "application/vnd.oasis.opendocument.spreadsheet"},
                {".odt", "application/vnd.oasis.opendocument.text"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"},
                {".mp4", "audio/mp4"},
                {".mp3", "audio/mp3"},
                {".html", "text/html"}
            };
        }

        public static string ContentFileType(string fileContentDisposition)
        {
            if (fileContentDisposition == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                fileContentDisposition == "application/msword")
                return "document";
            if (fileContentDisposition == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
                fileContentDisposition == "application/vnd.ms-powerpoint")
                return "presentation";
            if (fileContentDisposition == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                fileContentDisposition == "application/vnd.ms-excel")
                return "sheet";
            if (fileContentDisposition == "application/x-msaccess")
                return "msaccess";

            return fileContentDisposition;
        }
    }

    public class ContentType
    {
        public string ContentTypeString { get; set; }
        public string FileValue { get; set; }
    }

    public class Media
    {
        public string MediaTitle { get; set; }
        public string MediaDescription { get; set; }
    }

    public class FileType
    {

        public static bool IsEnglish(string s)
        {
            //Regex regex = new Regex(@"[A-Za-z0-9 .,-=+(){}\[\]\\]");
            //MatchCollection matches = regex.Matches(inputstring);

            //if (matches.Count.Equals(inputstring.Length))
            //    return true;
            //else
            //    return false;
            for (int i = 0; i < s.Length; ++i)
            {
                char c = s[i];

                if (((int)c) > 127)
                {
                    return false;
                }
            }
            return true;
        }


        private static IDictionary<string, string> _mappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {

            #region Big freaking list of mime types
            {".323", "text/h323"},
            {".3g2", "video/3gpp2"},
            {".3gp", "video/3gpp"},
            {".3gp2", "video/3gpp2"},
            {".3gpp", "video/3gpp"},
            {".7z", "application/x-7z-compressed"},
            {".aa", "audio/audible"},
            {".AAC", "audio/aac"},
            {".aaf", "application/octet-stream"},
            {".aax", "audio/vnd.audible.aax"},
            {".ac3", "audio/ac3"},
            {".aca", "application/octet-stream"},
            {".accda", "application/msaccess.addin"},
            {".accdb", "application/msaccess"},
            {".accdc", "application/msaccess.cab"},
            {".accde", "application/msaccess"},
            {".accdr", "application/msaccess.runtime"},
            {".accdt", "application/msaccess"},
            {".accdw", "application/msaccess.webapplication"},
            {".accft", "application/msaccess.ftemplate"},
            {".acx", "application/internet-property-stream"},
            {".AddIn", "text/xml"},
            {".ade", "application/msaccess"},
            {".adobebridge", "application/x-bridge-url"},
            {".adp", "application/msaccess"},
            {".ADT", "audio/vnd.dlna.adts"},
            {".ADTS", "audio/aac"},
            {".afm", "application/octet-stream"},
            {".ai", "application/postscript"},
            {".aif", "audio/x-aiff"},
            {".aifc", "audio/aiff"},
            {".aiff", "audio/aiff"},
            {".air", "application/vnd.adobe.air-application-installer-package+zip"},
            {".amc", "application/x-mpeg"},
            {".application", "application/x-ms-application"},
            {".art", "image/x-jg"},
            {".asa", "application/xml"},
            {".asax", "application/xml"},
            {".ascx", "application/xml"},
            {".asd", "application/octet-stream"},
            {".asf", "video/x-ms-asf"},
            {".ashx", "application/xml"},
            {".asi", "application/octet-stream"},
            {".asm", "text/plain"},
            {".asmx", "application/xml"},
            {".aspx", "application/xml"},
            {".asr", "video/x-ms-asf"},
            {".asx", "video/x-ms-asf"},
            {".atom", "application/atom+xml"},
            {".au", "audio/basic"},
            {".avi", "video/x-msvideo"},
            {".axs", "application/olescript"},
            {".bas", "text/plain"},
            {".bcpio", "application/x-bcpio"},
            {".bin", "application/octet-stream"},
            {".bmp", "image/bmp"},
            {".c", "text/plain"},
            {".cab", "application/octet-stream"},
            {".caf", "audio/x-caf"},
            {".calx", "application/vnd.ms-office.calx"},
            {".cat", "application/vnd.ms-pki.seccat"},
            {".cc", "text/plain"},
            {".cd", "text/plain"},
            {".cdda", "audio/aiff"},
            {".cdf", "application/x-cdf"},
            {".cer", "application/x-x509-ca-cert"},
            {".chm", "application/octet-stream"},
            {".class", "application/x-java-applet"},
            {".clp", "application/x-msclip"},
            {".cmx", "image/x-cmx"},
            {".cnf", "text/plain"},
            {".cod", "image/cis-cod"},
            {".config", "application/xml"},
            {".contact", "text/x-ms-contact"},
            {".coverage", "application/xml"},
            {".cpio", "application/x-cpio"},
            {".cpp", "text/plain"},
            {".crd", "application/x-mscardfile"},
            {".crl", "application/pkix-crl"},
            {".crt", "application/x-x509-ca-cert"},
            {".cs", "text/plain"},
            {".csdproj", "text/plain"},
            {".csh", "application/x-csh"},
            {".csproj", "text/plain"},
            {".css", "text/css"},
            {".csv", "text/csv"},
            {".cur", "application/octet-stream"},
            {".cxx", "text/plain"},
            {".dat", "application/octet-stream"},
            {".datasource", "application/xml"},
            {".dbproj", "text/plain"},
            {".dcr", "application/x-director"},
            {".def", "text/plain"},
            {".deploy", "application/octet-stream"},
            {".der", "application/x-x509-ca-cert"},
            {".dgml", "application/xml"},
            {".dib", "image/bmp"},
            {".dif", "video/x-dv"},
            {".dir", "application/x-director"},
            {".disco", "text/xml"},
            {".dll", "application/x-msdownload"},
            {".dll.config", "text/xml"},
            {".dlm", "text/dlm"},
            {".doc", "application/msword"},
            {".docm", "application/vnd.ms-word.document.macroEnabled.12"},
            {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {".dot", "application/msword"},
            {".dotm", "application/vnd.ms-word.template.macroEnabled.12"},
            {".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
            {".dsp", "application/octet-stream"},
            {".dsw", "text/plain"},
            {".dtd", "text/xml"},
            {".dtsConfig", "text/xml"},
            {".dv", "video/x-dv"},
            {".dvi", "application/x-dvi"},
            {".dwf", "drawing/x-dwf"},
            {".dwp", "application/octet-stream"},
            {".dxr", "application/x-director"},
            {".eml", "message/rfc822"},
            {".emz", "application/octet-stream"},
            {".eot", "application/octet-stream"},
            {".eps", "application/postscript"},
            {".etl", "application/etl"},
            {".etx", "text/x-setext"},
            {".evy", "application/envoy"},
            {".exe", "application/octet-stream"},
            {".exe.config", "text/xml"},
            {".fdf", "application/vnd.fdf"},
            {".fif", "application/fractals"},
            {".filters", "Application/xml"},
            {".fla", "application/octet-stream"},
            {".flr", "x-world/x-vrml"},
            {".flv", "video/x-flv"},
            {".fsscript", "application/fsharp-script"},
            {".fsx", "application/fsharp-script"},
            {".generictest", "application/xml"},
            {".gif", "image/gif"},
            {".group", "text/x-ms-group"},
            {".gsm", "audio/x-gsm"},
            {".gtar", "application/x-gtar"},
            {".gz", "application/x-gzip"},
            {".h", "text/plain"},
            {".hdf", "application/x-hdf"},
            {".hdml", "text/x-hdml"},
            {".hhc", "application/x-oleobject"},
            {".hhk", "application/octet-stream"},
            {".hhp", "application/octet-stream"},
            {".hlp", "application/winhlp"},
            {".hpp", "text/plain"},
            {".hqx", "application/mac-binhex40"},
            {".hta", "application/hta"},
            {".htc", "text/x-component"},
            {".htm", "text/html"},
            {".html", "text/html"},
            {".htt", "text/webviewhtml"},
            {".hxa", "application/xml"},
            {".hxc", "application/xml"},
            {".hxd", "application/octet-stream"},
            {".hxe", "application/xml"},
            {".hxf", "application/xml"},
            {".hxh", "application/octet-stream"},
            {".hxi", "application/octet-stream"},
            {".hxk", "application/xml"},
            {".hxq", "application/octet-stream"},
            {".hxr", "application/octet-stream"},
            {".hxs", "application/octet-stream"},
            {".hxt", "text/html"},
            {".hxv", "application/xml"},
            {".hxw", "application/octet-stream"},
            {".hxx", "text/plain"},
            {".i", "text/plain"},
            {".ico", "image/x-icon"},
            {".ics", "application/octet-stream"},
            {".idl", "text/plain"},
            {".ief", "image/ief"},
            {".iii", "application/x-iphone"},
            {".inc", "text/plain"},
            {".inf", "application/octet-stream"},
            {".inl", "text/plain"},
            {".ins", "application/x-internet-signup"},
            {".ipa", "application/x-itunes-ipa"},
            {".ipg", "application/x-itunes-ipg"},
            {".ipproj", "text/plain"},
            {".ipsw", "application/x-itunes-ipsw"},
            {".iqy", "text/x-ms-iqy"},
            {".isp", "application/x-internet-signup"},
            {".ite", "application/x-itunes-ite"},
            {".itlp", "application/x-itunes-itlp"},
            {".itms", "application/x-itunes-itms"},
            {".itpc", "application/x-itunes-itpc"},
            {".IVF", "video/x-ivf"},
            {".jar", "application/java-archive"},
            {".java", "application/octet-stream"},
            {".jck", "application/liquidmotion"},
            {".jcz", "application/liquidmotion"},
            {".jfif", "image/pjpeg"},
            {".jnlp", "application/x-java-jnlp-file"},
            {".jpb", "application/octet-stream"},
            {".jpe", "image/jpeg"},
            {".jpeg", "image/jpeg"},
            {".jpg", "image/jpeg"},
            {".js", "application/x-javascript"},
            {".json", "application/json"},
            {".jsx", "text/jscript"},
            {".jsxbin", "text/plain"},
            {".latex", "application/x-latex"},
            {".library-ms", "application/windows-library+xml"},
            {".lit", "application/x-ms-reader"},
            {".loadtest", "application/xml"},
            {".lpk", "application/octet-stream"},
            {".lsf", "video/x-la-asf"},
            {".lst", "text/plain"},
            {".lsx", "video/x-la-asf"},
            {".lzh", "application/octet-stream"},
            {".m13", "application/x-msmediaview"},
            {".m14", "application/x-msmediaview"},
            {".m1v", "video/mpeg"},
            {".m2t", "video/vnd.dlna.mpeg-tts"},
            {".m2ts", "video/vnd.dlna.mpeg-tts"},
            {".m2v", "video/mpeg"},
            {".m3u", "audio/x-mpegurl"},
            {".m3u8", "audio/x-mpegurl"},
            {".m4a", "audio/m4a"},
            {".m4b", "audio/m4b"},
            {".m4p", "audio/m4p"},
            {".m4r", "audio/x-m4r"},
            {".m4v", "video/x-m4v"},
            {".mac", "image/x-macpaint"},
            {".mak", "text/plain"},
            {".man", "application/x-troff-man"},
            {".manifest", "application/x-ms-manifest"},
            {".map", "text/plain"},
            {".master", "application/xml"},
            {".mda", "application/msaccess"},
            {".mdb", "application/x-msaccess"},
            {".mde", "application/msaccess"},
            {".mdp", "application/octet-stream"},
            {".me", "application/x-troff-me"},
            {".mfp", "application/x-shockwave-flash"},
            {".mht", "message/rfc822"},
            {".mhtml", "message/rfc822"},
            {".mid", "audio/mid"},
            {".midi", "audio/mid"},
            {".mix", "application/octet-stream"},
            {".mk", "text/plain"},
            {".mmf", "application/x-smaf"},
            {".mno", "text/xml"},
            {".mny", "application/x-msmoney"},
            {".mod", "video/mpeg"},
            {".mov", "video/quicktime"},
            {".movie", "video/x-sgi-movie"},
            {".mp2", "video/mpeg"},
            {".mp2v", "video/mpeg"},
            {".mp3", "audio/mpeg"},
            {".mp4", "video/mp4"},
            {".mp4v", "video/mp4"},
            {".mpa", "video/mpeg"},
            {".mpe", "video/mpeg"},
            {".mpeg", "video/mpeg"},
            {".mpf", "application/vnd.ms-mediapackage"},
            {".mpg", "video/mpeg"},
            {".mpp", "application/vnd.ms-project"},
            {".mpv2", "video/mpeg"},
            {".mqv", "video/quicktime"},
            {".ms", "application/x-troff-ms"},
            {".msi", "application/octet-stream"},
            {".mso", "application/octet-stream"},
            {".mts", "video/vnd.dlna.mpeg-tts"},
            {".mtx", "application/xml"},
            {".mvb", "application/x-msmediaview"},
            {".mvc", "application/x-miva-compiled"},
            {".mxp", "application/x-mmxp"},
            {".nc", "application/x-netcdf"},
            {".nsc", "video/x-ms-asf"},
            {".nws", "message/rfc822"},
            {".ocx", "application/octet-stream"},
            {".oda", "application/oda"},
            {".odc", "text/x-ms-odc"},
            {".odh", "text/plain"},
            {".odl", "text/plain"},
            {".odp", "application/vnd.oasis.opendocument.presentation"},
            {".ods", "application/oleobject"},
            {".odt", "application/vnd.oasis.opendocument.text"},
            {".one", "application/onenote"},
            {".onea", "application/onenote"},
            {".onepkg", "application/onenote"},
            {".onetmp", "application/onenote"},
            {".onetoc", "application/onenote"},
            {".onetoc2", "application/onenote"},
            {".orderedtest", "application/xml"},
            {".osdx", "application/opensearchdescription+xml"},
            {".p10", "application/pkcs10"},
            {".p12", "application/x-pkcs12"},
            {".p7b", "application/x-pkcs7-certificates"},
            {".p7c", "application/pkcs7-mime"},
            {".p7m", "application/pkcs7-mime"},
            {".p7r", "application/x-pkcs7-certreqresp"},
            {".p7s", "application/pkcs7-signature"},
            {".pbm", "image/x-portable-bitmap"},
            {".pcast", "application/x-podcast"},
            {".pct", "image/pict"},
            {".pcx", "application/octet-stream"},
            {".pcz", "application/octet-stream"},
            {".pdf", "application/pdf"},
            {".pfb", "application/octet-stream"},
            {".pfm", "application/octet-stream"},
            {".pfx", "application/x-pkcs12"},
            {".pgm", "image/x-portable-graymap"},
            {".pic", "image/pict"},
            {".pict", "image/pict"},
            {".pkgdef", "text/plain"},
            {".pkgundef", "text/plain"},
            {".pko", "application/vnd.ms-pki.pko"},
            {".pls", "audio/scpls"},
            {".pma", "application/x-perfmon"},
            {".pmc", "application/x-perfmon"},
            {".pml", "application/x-perfmon"},
            {".pmr", "application/x-perfmon"},
            {".pmw", "application/x-perfmon"},
            {".png", "image/png"},
            {".pnm", "image/x-portable-anymap"},
            {".pnt", "image/x-macpaint"},
            {".pntg", "image/x-macpaint"},
            {".pnz", "image/png"},
            {".pot", "application/vnd.ms-powerpoint"},
            {".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"},
            {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
            {".ppa", "application/vnd.ms-powerpoint"},
            {".ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12"},
            {".ppm", "image/x-portable-pixmap"},
            {".pps", "application/vnd.ms-powerpoint"},
            {".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
            {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
            {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".prf", "application/pics-rules"},
            {".prm", "application/octet-stream"},
            {".prx", "application/octet-stream"},
            {".ps", "application/postscript"},
            {".psc1", "application/PowerShell"},
            {".psd", "application/octet-stream"},
            {".psess", "application/xml"},
            {".psm", "application/octet-stream"},
            {".psp", "application/octet-stream"},
            {".pub", "application/x-mspublisher"},
            {".pwz", "application/vnd.ms-powerpoint"},
            {".qht", "text/x-html-insertion"},
            {".qhtm", "text/x-html-insertion"},
            {".qt", "video/quicktime"},
            {".qti", "image/x-quicktime"},
            {".qtif", "image/x-quicktime"},
            {".qtl", "application/x-quicktimeplayer"},
            {".qxd", "application/octet-stream"},
            {".ra", "audio/x-pn-realaudio"},
            {".ram", "audio/x-pn-realaudio"},
            {".rar", "application/octet-stream"},
            {".ras", "image/x-cmu-raster"},
            {".rat", "application/rat-file"},
            {".rc", "text/plain"},
            {".rc2", "text/plain"},
            {".rct", "text/plain"},
            {".rdlc", "application/xml"},
            {".resx", "application/xml"},
            {".rf", "image/vnd.rn-realflash"},
            {".rgb", "image/x-rgb"},
            {".rgs", "text/plain"},
            {".rm", "application/vnd.rn-realmedia"},
            {".rmi", "audio/mid"},
            {".rmp", "application/vnd.rn-rn_music_package"},
            {".roff", "application/x-troff"},
            {".rpm", "audio/x-pn-realaudio-plugin"},
            {".rqy", "text/x-ms-rqy"},
            {".rtf", "application/rtf"},
            {".rtx", "text/richtext"},
            {".ruleset", "application/xml"},
            {".s", "text/plain"},
            {".safariextz", "application/x-safari-safariextz"},
            {".scd", "application/x-msschedule"},
            {".sct", "text/scriptlet"},
            {".sd2", "audio/x-sd2"},
            {".sdp", "application/sdp"},
            {".sea", "application/octet-stream"},
            {".searchConnector-ms", "application/windows-search-connector+xml"},
            {".setpay", "application/set-payment-initiation"},
            {".setreg", "application/set-registration-initiation"},
            {".settings", "application/xml"},
            {".sgimb", "application/x-sgimb"},
            {".sgml", "text/sgml"},
            {".sh", "application/x-sh"},
            {".shar", "application/x-shar"},
            {".shtml", "text/html"},
            {".sit", "application/x-stuffit"},
            {".sitemap", "application/xml"},
            {".skin", "application/xml"},
            {".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"},
            {".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
            {".slk", "application/vnd.ms-excel"},
            {".sln", "text/plain"},
            {".slupkg-ms", "application/x-ms-license"},
            {".smd", "audio/x-smd"},
            {".smi", "application/octet-stream"},
            {".smx", "audio/x-smd"},
            {".smz", "audio/x-smd"},
            {".snd", "audio/basic"},
            {".snippet", "application/xml"},
            {".snp", "application/octet-stream"},
            {".sol", "text/plain"},
            {".sor", "text/plain"},
            {".spc", "application/x-pkcs7-certificates"},
            {".spl", "application/futuresplash"},
            {".src", "application/x-wais-source"},
            {".srf", "text/plain"},
            {".SSISDeploymentManifest", "text/xml"},
            {".ssm", "application/streamingmedia"},
            {".sst", "application/vnd.ms-pki.certstore"},
            {".stl", "application/vnd.ms-pki.stl"},
            {".sv4cpio", "application/x-sv4cpio"},
            {".sv4crc", "application/x-sv4crc"},
            {".svc", "application/xml"},
            {".swf", "application/x-shockwave-flash"},
            {".t", "application/x-troff"},
            {".tar", "application/x-tar"},
            {".tcl", "application/x-tcl"},
            {".testrunconfig", "application/xml"},
            {".testsettings", "application/xml"},
            {".tex", "application/x-tex"},
            {".texi", "application/x-texinfo"},
            {".texinfo", "application/x-texinfo"},
            {".tgz", "application/x-compressed"},
            {".thmx", "application/vnd.ms-officetheme"},
            {".thn", "application/octet-stream"},
            {".tif", "image/tiff"},
            {".tiff", "image/tiff"},
            {".tlh", "text/plain"},
            {".tli", "text/plain"},
            {".toc", "application/octet-stream"},
            {".tr", "application/x-troff"},
            {".trm", "application/x-msterminal"},
            {".trx", "application/xml"},
            {".ts", "video/vnd.dlna.mpeg-tts"},
            {".tsv", "text/tab-separated-values"},
            {".ttf", "application/octet-stream"},
            {".tts", "video/vnd.dlna.mpeg-tts"},
            {".txt", "text/plain"},
            {".u32", "application/octet-stream"},
            {".uls", "text/iuls"},
            {".user", "text/plain"},
            {".ustar", "application/x-ustar"},
            {".vb", "text/plain"},
            {".vbdproj", "text/plain"},
            {".vbk", "video/mpeg"},
            {".vbproj", "text/plain"},
            {".vbs", "text/vbscript"},
            {".vcf", "text/x-vcard"},
            {".vcproj", "Application/xml"},
            {".vcs", "text/plain"},
            {".vcxproj", "Application/xml"},
            {".vddproj", "text/plain"},
            {".vdp", "text/plain"},
            {".vdproj", "text/plain"},
            {".vdx", "application/vnd.ms-visio.viewer"},
            {".vml", "text/xml"},
            {".vscontent", "application/xml"},
            {".vsct", "text/xml"},
            {".vsd", "application/vnd.visio"},
            {".vsi", "application/ms-vsi"},
            {".vsix", "application/vsix"},
            {".vsixlangpack", "text/xml"},
            {".vsixmanifest", "text/xml"},
            {".vsmdi", "application/xml"},
            {".vspscc", "text/plain"},
            {".vss", "application/vnd.visio"},
            {".vsscc", "text/plain"},
            {".vssettings", "text/xml"},
            {".vssscc", "text/plain"},
            {".vst", "application/vnd.visio"},
            {".vstemplate", "text/xml"},
            {".vsto", "application/x-ms-vsto"},
            {".vsw", "application/vnd.visio"},
            {".vsx", "application/vnd.visio"},
            {".vtx", "application/vnd.visio"},
            {".wav", "audio/wav"},
            {".wave", "audio/wav"},
            {".wax", "audio/x-ms-wax"},
            {".wbk", "application/msword"},
            {".wbmp", "image/vnd.wap.wbmp"},
            {".wcm", "application/vnd.ms-works"},
            {".wdb", "application/vnd.ms-works"},
            {".wdp", "image/vnd.ms-photo"},
            {".webarchive", "application/x-safari-webarchive"},
            {".webtest", "application/xml"},
            {".wiq", "application/xml"},
            {".wiz", "application/msword"},
            {".wks", "application/vnd.ms-works"},
            {".WLMP", "application/wlmoviemaker"},
            {".wlpginstall", "application/x-wlpg-detect"},
            {".wlpginstall3", "application/x-wlpg3-detect"},
            {".wm", "video/x-ms-wm"},
            {".wma", "audio/x-ms-wma"},
            {".wmd", "application/x-ms-wmd"},
            {".wmf", "application/x-msmetafile"},
            {".wml", "text/vnd.wap.wml"},
            {".wmlc", "application/vnd.wap.wmlc"},
            {".wmls", "text/vnd.wap.wmlscript"},
            {".wmlsc", "application/vnd.wap.wmlscriptc"},
            {".wmp", "video/x-ms-wmp"},
            {".wmv", "video/x-ms-wmv"},
            {".wmx", "video/x-ms-wmx"},
            {".wmz", "application/x-ms-wmz"},
            {".wpl", "application/vnd.ms-wpl"},
            {".wps", "application/vnd.ms-works"},
            {".wri", "application/x-mswrite"},
            {".wrl", "x-world/x-vrml"},
            {".wrz", "x-world/x-vrml"},
            {".wsc", "text/scriptlet"},
            {".wsdl", "text/xml"},
            {".wvx", "video/x-ms-wvx"},
            {".x", "application/directx"},
            {".xaf", "x-world/x-vrml"},
            {".xaml", "application/xaml+xml"},
            {".xap", "application/x-silverlight-app"},
            {".xbap", "application/x-ms-xbap"},
            {".xbm", "image/x-xbitmap"},
            {".xdr", "text/plain"},
            {".xht", "application/xhtml+xml"},
            {".xhtml", "application/xhtml+xml"},
            {".xla", "application/vnd.ms-excel"},
            {".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
            {".xlc", "application/vnd.ms-excel"},
            {".xld", "application/vnd.ms-excel"},
            {".xlk", "application/vnd.ms-excel"},
            {".xll", "application/vnd.ms-excel"},
            {".xlm", "application/vnd.ms-excel"},
            {".xls", "application/vnd.ms-excel"},
            {".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
            {".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"},
            {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {".xlt", "application/vnd.ms-excel"},
            {".xltm", "application/vnd.ms-excel.template.macroEnabled.12"},
            {".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
            {".xlw", "application/vnd.ms-excel"},
            {".xml", "text/xml"},
            {".xmta", "application/xml"},
            {".xof", "x-world/x-vrml"},
            {".XOML", "text/plain"},
            {".xpm", "image/x-xpixmap"},
            {".xps", "application/vnd.ms-xpsdocument"},
            {".xrm-ms", "text/xml"},
            {".xsc", "application/xml"},
            {".xsd", "text/xml"},
            {".xsf", "text/xml"},
            {".xsl", "text/xml"},
            {".xslt", "text/xml"},
            {".xsn", "application/octet-stream"},
            {".xss", "application/xml"},
            {".xtp", "application/octet-stream"},
            {".xwd", "image/x-xwindowdump"},
            {".z", "application/x-compress"},
            {".zip", "application/x-zip-compressed"},
            #endregion

        };

        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            string mime;

            return _mappings.TryGetValue(extension, out mime) ? mime : "application/octet-stream";
        }

    }

}