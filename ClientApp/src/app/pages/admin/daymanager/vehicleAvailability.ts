
import { Component, OnInit, OnDestroy, Input, AfterViewInit,ChangeDetectorRef, EventEmitter, Output,Inject, Injectable, Renderer2, ViewEncapsulation} from '@angular/core'
import { Router,ActivatedRoute } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService,NavigationService,TimeSheetService, MenuService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import { takeUntil } from 'rxjs/operators';
import * as groupArray from 'group-array';
import { stubFalse } from 'lodash';
// import { BaseChartDirective } from 'ng2-charts';
import { addDays } from 'date-fns';


interface VehicleData {
  date: any;
  vlist : any;
  
}

@Component({
    templateUrl: './vehicleAvailability.html',
    styleUrls: ['./vehicleAvailability.css'],
    selector: 'vehicleAvailability',
   
   
  })
  
export class VehicleAvailability {

    private unsubscribe = new Subject();
    barChartLabels: any[] = [];
    barChartData: any[] = [];
    barChartOptions: any = {};
    barChartType:any ='bar';
    chartOptions = {
        animationEnabled: true, // Enables animations
        exportEnabled: true,    // Allows data export
        title: {
          text: "Vehicle Availability Analysis"
        },
        axisY: {
          title: "Days",
          prefix: "",
        },
        axisX: {
          title: "Vehicle/Dates"
        },
       
        data: [
          {
          type: "column", // Chart type (e.g., column, line, pie, etc.)
          dataPoints: []
        }
        ]    
          
        
      };
  
    @Input() isVisible:boolean=false;   
    @Input() loadData: Subject<any> = new Subject();
    vehicleList:Array<any>=[];
    vehicleDetails:Array<any>=[]; 
    lstbranches:Array<any>=[];
    timelist:Array<any>=[];
    listDisctinctDates:Array<any>=[];
    graphData:Array<VehicleData>=[];
    loading:boolean=true;
    dateFormat: string = 'dd/MM/yyyy';
    branch:any='ALL BRANCHES';
    startDate:any;
    endDate:any;

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private timeS: TimeSheetService,
        private modalService: NzModalService,
        private listS: ListService,
        private globalS:GlobalService,
        private cd:ChangeDetectorRef,
    
        ){

    //   this.listS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //         this.payEndingDate=data.payPeriodEndDate;
    //         console.log(this.payEndingDate);
    //       });

        }
      
   ngOnInit(): void {
       // this.loadTheme('LIGHT')

       this.loadData.subscribe(data => {
       
        this.isVisible=true;
        
        let startDate = format(data.date, 'yyyy-MM-dd');
        let endDate = format(addDays(data.date, data.dayView-1), 'yyyy-MM-dd');

        this.startDate=new Date(startDate);
        this.endDate=new Date(endDate);
       
       
        for(let i:number=1; i<24; i++){
          if (i<10)   {       
            this.timelist.push(  {value :"0"+i+":00", label: "0"+i+":00"});
            this.timelist.push( {value : "0"+i+":30", label: "30"} );
          }else{
            this.timelist.push(  {value :i+":00", label: i+":00"});
            this.timelist.push( {value : i+":30", label: "30"} );
          }
          
        }
      this.getStaffAvailability();  
       });
}

getBackcolor(t:any, v:any, d:any){
   let time = this.TimeToblock(t.value);
   let vehicleList = this.vehicleDetails.filter(x=>x.serviceSetting==v.vehicleName && x.date==d )
   let vehicle = vehicleList.find(v=>v.blockNo<=time && (v.blockNo+v.duration)>=time);
   if ( vehicle==null) return '#ddf5ff';
   if (vehicle.blockNo==null ) return '#ddf5ff';
  if(vehicle.blockNo<=time && (vehicle.blockNo+vehicle.duration)>=time)
    return 'blue';
  else
    return '#ddf5ff';
}
TimeToblock(startTime: any) {
  if (startTime==null) return 0;
  let time = startTime.split(':');
  
  let hrs = parseInt(time[0]);
  let min = parseInt(time[1]);
  
  let blocks =hrs * 12 + min/5;
   
  return blocks;
  
}
getStaffAvailability(){
  let startDate = format(this.startDate, 'yyyy-MM-dd');
  let endDate = format(this.endDate, 'yyyy-MM-dd');
         
  this.loading=true;
  forkJoin([
    this.timeS.getVehicleList(),
    this.timeS.getVehicleData(startDate,endDate),
    this.listS.getlistbranchesObj(),
]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    this.vehicleList = data[0];
    this.vehicleDetails = data[1];
    this.lstbranches = data[2].map(x=>x.description);
   this.listDisctinctDates = this.getDistinctDates(this.vehicleDetails);
   this.Prepare_data()

  let labels=  this.vehicleList .map(x=>x.vehicleName)
  let datapoints =this.vehicleDetails.map(x=> { return    {label:x.date, y:x.duration}});
    this.chartOptions = {
      animationEnabled: true, // Enables animations
      exportEnabled: true,    // Allows data export
      title: {
        text: "Vehicle Availability Analysis"
      },
      axisY: {
        title: "duration",
        prefix: "",
      },
      axisX: {
        title: "Date"
      },
      
      data: [
        {
        type: "column", // Chart type (e.g., column, line, pie, etc.)
        dataPoints: datapoints
      }
      ]    
        
      
    };

    
    this.cd.detectChanges();
   
    this.loading=false;

    error=>{this.loading=false;}
});
}

getDistinctDates(vehicleDetails: any[]): string[] {
  const distinctDates = Array.from(new Set(vehicleDetails.map(item => item.date)));
  return distinctDates;
}
Prepare_data(){
  this.graphData=[];
  let vlist:Array<any>=[];
  let obj;
  this.listDisctinctDates.forEach(element => {
    vlist=[]
    this.vehicleList.forEach(v => {
      let data = {vehicleName: v.vehicleName, timeData: this.timelist.map(x=> { return    {label:x.label, value:x.value, backcolor: this.getBackcolor(x,v,element)}})};
     vlist.push(data);
  });
  obj = {
    date: element,
    vlist: vlist,
        
  }
  this.graphData.push(obj);
  })
  this.cd.detectChanges();
}
handleCancel(){
    this.isVisible=false;
    // this.AIRosterDone.emit(this.rosters_display);
   
}

}