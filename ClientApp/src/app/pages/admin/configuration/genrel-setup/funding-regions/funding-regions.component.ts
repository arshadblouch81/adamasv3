import { ChangeDetectorRef, Component, OnInit,HostListener,ViewChild,ElementRef,AfterViewInit} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ListService } from '@services/list.service';
import { GlobalService } from '@services/global.service';
import { SwitchService } from '@services/switch.service';
import { Observable, of, from, Subject, EMPTY } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MenuService, NavigationService, PrintService, TimeSheetService} from '@services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'app-funding-regions',
  templateUrl: './funding-regions.component.html',
  styleUrls: ['./funding-regions.component.css']
})

export class FundingRegionsComponent implements OnInit {
  
  tableData: Array<any>;
  fundinglist: Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  modalVariables: any;
  inputVariables:any;
  audithistory:any;
  dateFormat: string = 'dd/MM/yyyy';
  title:string = "Add Funding Regions";
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;  
  check : boolean = false;
  filteredData: Array<any>; // To hold the current visible data
  userRole:string="userrole";
  whereString :string="Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  private unsubscribe: Subject<void> = new Subject();
  temp_title: any;
  searchTerm: string = '';
  currentIndex: number | null = null;
  
  constructor(
    private globalS: GlobalService,
    private cd: ChangeDetectorRef,
    private switchS:SwitchService,
    private listS:ListService,
    private timeS:TimeSheetService,
    private printS: PrintService,
    private menuS:MenuService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    private navigationService: NavigationService,
    private router: Router,// Initialize with all data
    ){}
    
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.loadData();
      this.loading = false;
      this.cd.detectChanges();
    }
    loadTitle()
    {
      return this.title
    }
    
    loadData(){
      this.loading = true;
      this.menuS.getDataDomainByType("FUNDREGION",this.check).subscribe(data => {
        this.tableData = data;
        this.filteredData = [...this.tableData];
        this.loading = false;
      });
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = "WHERE";
        this.loadData();
      }else{
        this.whereString = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
        this.loadData();
      }
    }
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    }
    showAddModal() {
      this.resetModal();
      this.modalOpen = true;
    }
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.title = "Edit Funding Regions"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      const { 
        name,
        end_date,
        recordNumber
      } = this.tableData[index];
      this.inputForm.patchValue({
        description : name,
        end_date    : end_date,
        recordNumber:recordNumber,
      });
    }
    
    handleCancel() {
      this.modalOpen = false;
    }
    
    pre(): void {
      this.current -= 1;
    }
    
    next(): void {
      this.current += 1;
    }
    save() {
      this.postLoading = true;     
      const group = this.inputForm;
      if(!this.isUpdate){         
        this.switchS.addData(  
          this.modalVariables={
            title: 'Funding Regions'
          }, 
          this.inputVariables = {
            display : group.get('description').value.trim().toUpperCase(),
            end_date:!(this.globalS.isVarNull(group.get('end_date').value)) ? this.globalS.convertDbDate(group.get('end_date').value) : null,
            domain: 'FUNDREGION',
          }
          ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data)
            {
              this.timeS.postaudithistory({
                Operator:this.tocken.user,
                actionDate:this.globalS.getCurrentDateTime(),
                auditDescription:'Funding Regions Added',
                actionOn:'FUNDREGION',
                whoWhatCode:data, //inserted
                TraccsUser:this.tocken.user,
              }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                  this.globalS.sToast('Success', 'Saved successful');
                }
              );
            }else
            {
              this.globalS.sToast('Unsuccess', 'Data not saved' + data);
            }
            this.loadData();
            this.postLoading = false;          
            this.handleCancel();
            this.resetModal();
          });
        }else{
          this.postLoading = true;     
          const group = this.inputForm;
          let name        = group.get('description').value.trim().toUpperCase();
          if(this.temp_title != name){
            let is_exist    = this.globalS.isNameExists(this.tableData,name);
            if(is_exist){
              this.globalS.sToast('Unsuccess', 'Title Already Exist');
              this.postLoading = false;
              return false;   
            }
          }
          this.switchS.updateData(  
            this.modalVariables={
              title: 'Funding Regions'
            }, 
            this.inputVariables = {
              display: group.get('description').value,
              primaryId:group.get('recordNumber').value,
              end_date:!(this.globalS.isVarNull(group.get('end_date').value)) ? this.globalS.convertDbDate(group.get('end_date').value) : null,
              domain: 'FUNDREGION',
            }
            ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
              if (data)
              {
                this.timeS.postaudithistory({
                  Operator:this.tocken.user,
                  actionDate:this.globalS.getCurrentDateTime(),
                  auditDescription:'Funding Regions Changed',
                  actionOn:'FUNDREGION',
                  whoWhatCode:group.get('recordNumber').value, //inserted
                  TraccsUser:this.tocken.user,
                }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                    this.globalS.sToast('Success', 'Update successful');
                  }
                );
              }else
              {
                this.globalS.sToast('Unsuccess', 'Data Not Update' + data);
              }
              this.loadData();
              this.isUpdate = false;
              this.postLoading = false;          
              this.handleCancel();
              this.resetModal();
            });
          } 
        }
        
        delete(data: any) {
          this.postLoading = true;     
          const group = this.inputForm;
          this.menuS.deleteDomain(data.recordNumber)
          .pipe(takeUntil(this.unsubscribe)).subscribe(datas => {
            if (datas) {
              this.timeS.postaudithistory({
                Operator:this.tocken.user,
                actionDate:this.globalS.getCurrentDateTime(),
                auditDescription:'Funding Regions Deleted',
                actionOn:'FUNDREGION',
                whoWhatCode:data.recordNumber, //inserted
                TraccsUser:this.tocken.user,
              }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                  this.globalS.sToast('Success', 'Deleted successful');
                }
              );
              this.loadData();
              return;
            }
          });
        }
                  
        
        buildForm() {
          this.inputForm = this.formBuilder.group({
            description: '',
            end_date: '',
            recordNumber:0,
          });
        }
        handleOkTop() {
          this.generatePdf();
          this.tryDoctype = ""
          this.pdfTitle = ""
        }
        handleCancelTop(): void {
          this.drawerVisible = false;
          this.pdfTitle = ""
        }
        generatePdf(){
          this.drawerVisible = true;
          
          this.loading = true;
          
          var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY Description) AS Field1,Description as Field2,CONVERT(varchar, [enddate],105) as Field3 from DataDomains "+this.whereString+" Domain='FUNDREGION'";
          
          const data = {
            "template": { "_id": "0RYYxAkMCftBE9jc" },
            "options": {
              "reports": { "save": false },
              "txtTitle": "Funding Region List",
              "sql": fQuery,
              "userid":this.tocken.user,
              "head1" : "Sr#",
              "head2" : "Name",
              "head3" : "End Date",
            }
          }
           this.printS.printControl(data).subscribe((blob: any) => {
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.pdfTitle = "Funding Regions.pdf";
            }, err => {
            console.log(err);
            this.loading = false;
            this.ModalS.error({
              nzTitle: 'TRACCS',
              nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
              nzOnOk: () => {
                this.drawerVisible = false;
              },
            });
          });
          
          this.loading = true;
          this.tryDoctype = "";
          this.pdfTitle = "";
        }
        
        goBack(): void {
          const previousUrl = this.navigationService.getPreviousUrl();
          const previousTabIndex = this.navigationService.getPreviousTabIndex();
        
          if (previousUrl) {
            this.router.navigate(['/admin/configuration'], {
              queryParams: { tab: previousTabIndex } // Pass tab index in query params
            });
          } else {
            this.router.navigate(['/admin/configuration'], {
              queryParams: { tab: previousTabIndex } // Pass tab index in query params
            });
          }
      }

    

    ngAfterViewInit() {
        console.log('Component initialized');
    }
    
    @HostListener('document:keydown', ['$event'])
        handleKeyboardEvent(event: KeyboardEvent) {
          // Handle character input
          if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
              this.searchTerm += event.key.toLowerCase();
              this.scrollToRow();
          } 
          // Handle backspace
          else if (event.key === 'Backspace') {
              // Only reset if there's something to remove
              this.searchTerm = '';
              if (this.searchTerm.length > 0) {
                  this.searchTerm = this.searchTerm.slice(0, -1);
                  this.scrollToRow();
              }
          }
          // Handle other keys (optional)
          else if (event.key === 'Escape') {
              // Reset search term on escape key
              this.searchTerm = '';
              this.scrollToRow();
          }
        }

        scrollToRow() {
            // Find the index of the first matching item
            const matchIndex = this.tableData.findIndex(item => 
                item.name.toLowerCase().startsWith(this.searchTerm)
            );
            // Scroll to the matching row if it exists
            if (matchIndex !== -1) {
                const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
                if (tableRow) {
                    tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                    this.highlightRow(tableRow);
                }
            } else {
            }
        }

        highlightRow(row: HTMLElement) {
            row.classList.add('highlight');
            setTimeout(() => row.classList.remove('highlight'), 2000);
        }

}
      