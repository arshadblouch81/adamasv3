import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';


@Component({
  styles: [`
      nz-table{
        margin-top:10px;
        margin-bottom:30px;
        
    }

    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
        border : 1px solid #0c77b1;
    }     
    `],
    selector: '',
    templateUrl: './prefstaff.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakePrefStaff implements OnInit, OnDestroy {
  
  private unsubscribe: Subject<void> = new Subject();
  user: any;
  loading: boolean = false;
  isLoading:boolean = false;
  postLoading : boolean = false;
  modalOpen: boolean = false;
  editOrAdd: number;
  inputForm: FormGroup;
  whatView: number;
  activeRowData:any;
  selectedRowIndex:number;
 

  
  includedStaff: Array<any> = []
  staffApproved: boolean =  false;
  careWorkers: any;
  staffUnApproved: boolean = false;
  temp: any[];
  careWorkersCopy: any;
  careWorkersExcluded: any;
  careWorkersExcludedCopy: any;
  checkedListExcluded: any;
  checkedListApproved: any;
  inputvalueSearch:string = '';
  listArray: Array<any>;
  listStaff: any;
  listStaff_original: any;
  selectedStaff: any;
  txtSearch:string;
  lstSelected:Array<any>=[];

  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;

  constructor(
    private timeS: TimeSheetService,
    private sharedS: ShareService,
    private listS: ListService,
    private router: Router,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private printS:PrintService,
    private sanitizer:DomSanitizer,
    private ModalS:NzModalService,
    private cd: ChangeDetectorRef
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                  this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'staff')) {
                this.user = data;
                this.search(data);
            }
        });
    }
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
      this.user = this.sharedS.getPicked();
      this.search(this.user);
      this.buildForm();
    }
    
    ngOnDestroy(): void {
      this.unsubscribe.next();
      this.unsubscribe.complete();
    }
    getPermisson(index:number){
      var permissoons = this.globalS.getRecipientRecordView();
      return permissoons.charAt(index-1);
    }
    getStaffPermisson(index:number){
      var permissoons = this.globalS.getStaffRecordView();
      return permissoons.charAt(index-1);
    }
    confirm(data: any): void {
      let records =this.lstSelected.map(x=>x.recordNumber).join(',').toString();
        this.delete(data.recordNumber);
      

  }
    search(user: any = this.user) {
      this.cd.reattach();
      console.log(this.user);
      this.loading = true;
      forkJoin([
      
        this.timeS.getincludedstaff(user.id),
        this.listS.getintakestaff(user.id)
      ]).subscribe(staff => {
        this.loading = false;
       
        this.includedStaff = staff[0];
        this.listStaff = staff[1].map(x => {
          return {
              label: x,
              value: x,
              checked: false
          }
        });
        this.listStaff_original = this.listStaff;
        this.cd.markForCheck();
      });
    }
    
    buildForm() {
      this.inputForm = this.formBuilder.group({
        recordNumber: '',
        personID: '',
        list: '',
        notes: ['']
      })
    }
    
    get title() {
      const str = this.whatView == 1 ? 'Excluded Staff' : 'Preferred Staff';
      const pro = this.editOrAdd == 1 ? 'Select' : 'Edit';
      return `${pro} ${str}`;
    }
    trackByFn(index, item) {
      return item.id;
    }
    
    processBtn() {
     
      if (this.editOrAdd==1)
        this.save();
      else
        this.edit();
      
    }
    save() {

      console.log(this.user);

      for (const i in this.inputForm.controls) {
        this.inputForm.controls[i].markAsDirty();
        this.inputForm.controls[i].updateValueAndValidity();
      }
      
      if (!this.inputForm.valid)
      return;
      
      const { list, notes } = this.inputForm.value;
      const index = this.whatView;
      this.isLoading = true;
      
      if((this.editOrAdd == 1 && this.selectedStaff === undefined) || (this.selectedStaff !== undefined && this.selectedStaff.length ===0) ){
        this.globalS.sToast('Success', 'Please Select Atleast One Staff ');
        this.isLoading = false;
        return
      }
      
    
        this.timeS.postintakestaff({
          Notes: (notes == null) ? '' : notes,
          personID: this.user.id,
          name: list,
          staffCategory:1,
          Creater:'SYSMGR',
          selectedStaff:this.selectedStaff,
          dateCreated: this.globalS.getCurrentDate()
        }).pipe(takeUntil(this.unsubscribe))
        .subscribe(data => {
          if (data) {
            this.handleCancel();
            this.success();
            this.globalS.sToast('Success', 'Data Added');
          }
        });
      
    }
    edit() {
      
      for (const i in this.inputForm.controls) {
        this.inputForm.controls[i].markAsDirty();
        this.inputForm.controls[i].updateValueAndValidity();
      }
      
      if (!this.inputForm.valid)
      return;
      
      const { list, notes, recordNumber } = this.inputForm.value;
      const index = this.whatView;
      this.isLoading = true;
      
      
          this.timeS.updateintakestaff({
            name: list,
            notes:notes,
            recordNumber:recordNumber
          }).pipe(
            takeUntil(this.unsubscribe))
            .subscribe(data => {
              if (data) {
                this.handleCancel();
                this.success();
                this.globalS.sToast('Success', 'Data Updated');
              }
            })
  }
        
        
        success() {
          this.search(this.sharedS.getPicked());
          this.isLoading = false;
        }
    
        showEditModal( data:any, i :number) {
          this.selectedRowIndex=i;
          this.activeRowData=data;
        
              const { name, notes, recordNumber } = data;
              this.inputForm.patchValue({
                  list: name,
                  notes: notes,
                  recordNumber: recordNumber
              });
          
        
          this.editOrAdd = 2;
          this.modalOpen = true;
      }
        
        delete(recordNo: string) {
          this.timeS.deleteintakestaff(recordNo)
            .subscribe(data => {
                if (data) {
                    this.handleCancel();
                    this.success();
                    this.globalS.sToast('Success', 'Data Deleted');
                }
            });
        }
        
        handleCancel() {
          this.inputForm.reset();
          this.selectedStaff = {};
          this.isLoading = false;
          this.modalOpen = false;
        }
        
        showAddModal(view : number) {
         // this.whatView = view;
          this.editOrAdd = 1;
          this.modalOpen = true;
        }
        
        log(value: string[]): void {
        }
        logs(event: any) {
          this.selectedStaff = event;
        }
        tabFindIndex: number = 0;
        tabFindChange(index: number){
         this.tabFindIndex = index;
        }

        selectedItemGroup(event:any,data:any, i:number){
          this.selectedRowIndex=i;
          this.activeRowData=data;
          if (event.ctrlKey || event.shiftKey) {
            if (this.lstSelected.indexOf(data)<0)
              this.lstSelected.push(data);
             } else {
           
            this.lstSelected = [];
            this.lstSelected.push(data);
          }
        
      }

        onTextChangeEvent(event:any){
          // console.log(this.txtSearch);
           let value = this.txtSearch.toUpperCase();
           //console.log(this.serviceActivityList[0].description.includes(value));
           this.listStaff=this.listStaff_original.filter(element=>element.value.includes(value));
       }
  
      print(){
          
      }
      generatePdf(){
      
        var Title = " Preferred Staff "
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.includedStaff,                        
                "userid": this.tocken.user,
                "txtTitle":  Title+" for "+this.user.code,                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = Title+" for "+this.user.code
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
      close(){
          this.router.navigate(['/admin/recipient/personal']);
      }
      export(){

      }

      }