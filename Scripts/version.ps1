$AdamasProjectPath = "D:\Programming\Adamas\adamasv3\bin\Release\net6.0\ClientApp\dist\assets\version.txt"
$fileExists = Test-Path -Path $AdamasProjectPath

$day = (get-date).day
$month = (get-date).month
$year = (get-date).year

function Angular-Version{
	Param([int]$a)
	if($a -eq $null){
		return 1
	} else {
		return $a
	}
}

if($fileExists){
	$contents = Get-Content -Path $AdamasProjectPath
	
	
	if($contents -ne $null){		
	
		$combiVersion = $contents.Split(".")

		$oldDay = [int]$combiVersion[0]
		$oldMonth = [int]$combiVersion[1]
		$oldYear = [int]$combiVersion[2]
		$oldVersion =  [int]$combiVersion[3]
		
		 if($oldDay -eq $day -and $oldMonth -eq $month -and $oldYear -eq $year){
			$version = [int]$oldVersion+1
			$version = Angular-Version $version
			"$('{0:d2}' -f [int]$day).$('{0:d2}' -f [int]$month).$($year).$('{0:d3}' -f [int]$version)" | Out-File $AdamasProjectPath
		 }else {
			 "$('{0:d2}' -f [int]$day).$('{0:d2}' -f [int]$month).$($year).001" | Out-File $AdamasProjectPath
		 }
	}else{
		"$('{0:d2}' -f [int]$day).$('{0:d2}' -f [int]$month).$($year).$('{0:d3}' -f [int]1)" | Out-File $AdamasProjectPath
	}
}

else{
	"$('{0:d2}' -f [int]$day).$('{0:d2}' -f [int]$month).$($year).001" | Out-File $AdamasProjectPath
}

pause