import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
// import { ListService, MenuService } from '@services/index';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { debounceTime, timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';

import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-bulk-price-update',
  templateUrl: './bulkPriceUpdate.component.html',
  styleUrls: ['./bulkPriceUpdate.component.css'],
})

// .ant-divider, .ant-divider-vertical {
//     position: relative;
//     top: 10px;
// }

export class BulkPriceUpdateComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;
  
    isVisible: boolean = false;
    operatorID: any;
    modalOpen: boolean = false;
    current: number = 0;
    inputForm: FormGroup;
    loading: boolean = false;
    token: any;
    tocken: any;
    check: boolean = false;
    userRole: string = "userrole";
    id: string;
    private unsubscribe: Subject<void> = new Subject();
    dateFormat: string = 'dd/MM/yyyy';
    title: string = "Bulk Price Update";
    tableData: Array<any>;
    updatedRecords: any;
    
    endDate: any;
    startDate: any;
    currentDateTime: any;
    
    chkRosterUnApproved: boolean;
    chkRosterApproved: boolean;
    chkRosterMaster: boolean;

    chkUpdatePrices: boolean;
    chkUpdateQuotes: boolean;

    chkActMasTbl: boolean;
    chkActMasCom: boolean;
    chkActMasLvl1: boolean;
    chkActMasLvl2: boolean;
    chkActMasLvl3: boolean;
    chkActMasLvl4: boolean;
    chkActMasLvl5: boolean;
    chkActMasLvl6: boolean;

    batchNumber: any;
    billUnit: Array<any>;
    inputValue?: string;

    ActivitiesList: Array<any>;
    selectedActivities: any;
    allActivitiesChecked: boolean;
    lockActivities: any = false;
    
    categoriesList: Array<any>;
    selectedCategories: any;
    allCategoriesChecked: boolean;
    lockCategories: any = false;
    
    fundingList: Array<any>;
    selectedFunding: any;
    allFundingChecked: boolean;
    lockFunding: any = false;
  
    ProgramsList: Array<any>;
    selectedPrograms: any;
    allProgramsChecked: boolean;
    lockPrograms: any = false;

    recipientsList: Array<any>;
    selectedRecipients: any;
    allRecipientsChecked: boolean;
    lockRecipients: any = false;

    recipientsGroupsList: Array<any>;
    selectedRecipientsGroups: any;
    allRecipientsGroupsChecked: boolean;
    lockRecipientsGroups: any = false;

    allItemSubgroupsChecked: boolean;
    lockItemSubgroups: any = false;
    checkedAdministration: boolean;
    checkedOneonone: boolean;
    checkedGroupactivity: boolean;
    checkedCentrebased: boolean;
    checkedTransport: boolean;
    checkedRecptabsence: boolean;
    checkedTraveltime: boolean;
    checkedSleepover: boolean;
    checkedAdmission: boolean;
    checkedItem: boolean;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    this.loadData();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/billing']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
        chkUpdatePrices: false,
        chkUpdateQuotes: false,
        chkRosterDates: false,
        chkRosterUnApproved: true,
        chkRosterApproved: true,
        chkRosterMaster: true,
        chkActMasTbl: false,
        chkActMasCom: false,
        chkActMasLvl1: false,
        chkActMasLvl2: false,
        chkActMasLvl3: false,
        chkActMasLvl4: false,
        chkActMasLvl5: false,
        chkActMasLvl6: false,
        rdoUpdateType: "rdoPercentage",
        inputValue: '',
        billUnit: 'ALL',
        startDate: '12/01/2021',
        endDate: '12/31/2021',
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  loadData() {
    this.loading = true;

    this.billUnit = ['ALL', 'HOUR', 'SERVICE'];

    var now = new Date();
    if (now.getMonth() == 11) {
        this.startDate = new Date(now.getFullYear() + 1, 0, 1);
        this.endDate = new Date(now.getFullYear() + 1, 5, 1);
    } else {
        this.startDate = new Date(now.getFullYear(), now.getMonth() + 1, 1);
        this.endDate = new Date(now.getFullYear(), now.getMonth() + 6, 1);
    }
    this.startDate = formatDate(this.startDate, 'MM/dd/yyyy', 'en_US');
    this.endDate = formatDate(this.endDate, 'MM/dd/yyyy', 'en_US');
    this.inputForm.patchValue({
        startDate: this.startDate,
        endDate: this.endDate,
    });

    let dataPass = {
        CurrentDate: formatDate(this.globalS.getCurrentDateTime(), 'MM-dd-yyyy', 'en_US')
      }

    //check all Item Subgroups
        this.allItemSubgroupsChecked = true;
        this.checkAll(6);

    //loadActivities
    this.billingS.getActivitiesAfter(dataPass).subscribe(data => {
      this.ActivitiesList = data;
      this.tableData = data;
      this.allActivitiesChecked = true;
      this.checkAll(2);
    });

    //getFundingDetails
    this.loading = true;
    this.billingS.getFundingSources(null).subscribe(data => {
      this.fundingList = data;
      this.tableData = data;
      this.allFundingChecked = true;
      this.checkAll(5);
    });

    //loadCategories
    this.loading = true;
    this.billingS.getCategories(null).subscribe(data => {
      this.categoriesList = data;
      this.tableData = data;
      this.allCategoriesChecked = true;
      this.checkAll(3);
    });

    //loadPrograms
    this.billingS.getProgramsAfter(dataPass).subscribe(data => {
      this.ProgramsList = data;
      this.tableData = data;
      this.allProgramsChecked = true;
      this.checkAll(1);
    });

    //getRecipientsDetails
    this.loading = true;
    this.billingS.getRecipientsDetails().subscribe(data => {
      this.recipientsList = data;
      this.tableData = data;
      this.allRecipientsChecked = true;
      this.checkAll(4);
    });

    //getRecipientsGroups
    this.loading = true;
    this.billingS.getRecipientsGroups(null).subscribe(data => {
      this.recipientsGroupsList = data;
      this.tableData = data;
      this.allRecipientsGroupsChecked = true;
      this.checkAll(7);
    });

    this.loading = true;
  }

  runProcess() {
    this.loading = true;
    this.operatorID = this.token.nameid;
    this.batchNumber = this.batchNumber + 1;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');

    if (this.inputValue != '') {
      // this.processExecution();
    } else if (this.inputValue == '') {
      this.globalS.eToast('Error', 'Please enter a numeric value for the price.')
    }
    this.loading = false;
  }

  processExecution() {
    this.loading = true;
    
    const modalVal = this.inputForm;
    const params = {
      rdoUpdateType: modalVal.get('rdoUpdateType').value,
      inputValue: modalVal.get('inputValue').value,
      chkUpdatePrices: modalVal.get('chkUpdatePrices').value,
      chkUpdateQuotes: modalVal.get('chkUpdateQuotes').value,
      billUnit: modalVal.get('billUnit').value,
      chkRosterDates: modalVal.get('chkRosterDates').value,
      startDate: modalVal.get('startDate').value,
      endDate: modalVal.get('endDate').value,
      chkRosterUnApproved: modalVal.get('chkRosterUnApproved').value,
      chkRosterApproved: modalVal.get('chkRosterApproved').value,
      chkRosterMaster: modalVal.get('chkRosterMaster').value,
      chkActMasTbl: modalVal.get('chkActMasTbl').value,
      chkActMasCom: modalVal.get('chkActMasCom').value,
      chkActMasLvl1: modalVal.get('chkActMasLvl1').value,
      chkActMasLvl2: modalVal.get('chkActMasLvl2').value,
      chkActMasLvl3: modalVal.get('chkActMasLvl3').value,
      chkActMasLvl4: modalVal.get('chkActMasLvl4').value,
      chkActMasLvl5: modalVal.get('chkActMasLvl5').value,
      chkActMasLvl6: modalVal.get('chkActMasLvl6').value,
      s_SelectedActivities: this.selectedActivities,
      s_SelectedFunding: this.selectedFunding,
      s_SelectedItemSubGroups: 'Need to add all items in list and then show in the modal as same like others',
      s_SelectedPrograms: this.selectedPrograms,
      s_SelectedCategories: this.selectedCategories,
      s_SelectedRecipients: this.selectedRecipients,
      s_SelectedRecipientsGroups: this.selectedRecipientsGroups
    };

    this.billingS.processBulkPriceUpdate({
      RdoUpdateType: params.rdoUpdateType,
      InputValue: params.inputValue,
      ChkUpdatePrices: params.chkUpdatePrices,
      ChkUpdateQuotes: params.chkUpdateQuotes,
      BillUnit: params.billUnit,
      ChkRosterDates: params.chkRosterDates,
      StartDate: params.startDate,
      EndDate: params.endDate,
      ChkRosterUnApproved: params.chkRosterUnApproved,
      ChkRosterApproved: params.chkRosterApproved,
      ChkRosterMaster: params.chkRosterMaster,
      ChkActMasTbl: params.chkActMasTbl,
      ChkActMasCom: params.chkActMasCom,
      ChkActMasLvl1: params.chkActMasLvl1,
      ChkActMasLvl2: params.chkActMasLvl2,
      ChkActMasLvl3: params.chkActMasLvl3,
      ChkActMasLvl4: params.chkActMasLvl4,
      ChkActMasLvl5: params.chkActMasLvl5,
      ChkActMasLvl6: params.chkActMasLvl6,
      SelectedActivities: params.s_SelectedActivities,
      SelectedFunding: params.s_SelectedFunding,
      SelectedItemSubGroups: params.s_SelectedItemSubGroups,
      SelectedPrograms: params.s_SelectedPrograms,
      SelectedCategories: params.s_SelectedCategories,
      SelectedRecipients: params.s_SelectedRecipients,
      SelectedRecipientsGroups: params.s_SelectedRecipientsGroups
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'There records exists')
          } else {
            this.globalS.sToast('Success', this.updatedRecords + ' - Records Updated.')
          }
          this.loading = false;
          this.ngOnInit();
          return false;
        }
      });
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedPrograms = event;
    if (index == 2)
      this.selectedActivities = event;
    if (index == 3)
      this.selectedCategories = event;
    if (index == 4)
      this.selectedRecipients = event;
    if (index == 5)
      this.selectedFunding = event;
    // if (index == 6)
    //   this.selectedSubgroups = event;
    if (index == 7)
        this.selectedRecipientsGroups = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allProgramsChecked == false) {
        this.lockPrograms = true
      }
      else {
        this.ProgramsList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockPrograms = false
      }
    }
    if (index == 2) {
      if (this.allActivitiesChecked == false) {
        this.lockActivities = true
      }
      else {
        this.ActivitiesList.forEach(x => {
          x.checked = true;
          this.allActivitiesChecked = x.description;
          this.allActivitiesChecked = true;
        });
        this.lockActivities = false
      }
    }
    if (index == 3) {
      if (this.allCategoriesChecked == false) {
        this.lockCategories = true
      }
      else {
        this.categoriesList.forEach(x => {
          x.checked = true;
          this.allCategoriesChecked = x.description;
          this.allCategoriesChecked = true;
        });
        this.lockCategories = false
      }
    }
    if (index == 4) {
      if (this.allRecipientsChecked == false) {
        this.lockRecipients = true
      }
      else {
        this.recipientsList.forEach(x => {
          x.checked = true;
          this.allRecipientsChecked = x.description;
          this.allRecipientsChecked = true;
        });
        this.lockRecipients = false
      }
    }
    if (index == 5) {
      if (this.allFundingChecked == false) {
        this.lockFunding = true
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = false
      }
    }
    if (index == 6) {
      if (this.allItemSubgroupsChecked == false) {
        this.lockItemSubgroups = true
      }
      else {
        this.checkedAdministration = true;
        this.checkedOneonone = true;
        this.checkedGroupactivity = true;
        this.checkedCentrebased = true;
        this.checkedTransport = true;
        this.checkedRecptabsence = true;
        this.checkedTraveltime = true;
        this.checkedSleepover = true;
        this.checkedAdmission = true;
        this.checkedItem = true;
        this.lockItemSubgroups = false
      }
    }
    if (index == 7) {
      if (this.allRecipientsGroupsChecked == false) {
        this.lockRecipientsGroups = true
      }
      else {
        this.recipientsGroupsList.forEach(x => {
          x.checked = true;
          this.allRecipientsGroupsChecked = x.description;
          this.allRecipientsGroupsChecked = true;
        });
        this.lockRecipientsGroups = false
      }
    }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockPrograms = true;
      this.ProgramsList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
    if (index == 2) {
      this.lockActivities = true;
      this.ActivitiesList.forEach(x => {
        x.checked = false;
        this.allActivitiesChecked = false;
        this.selectedActivities = [];
      });
    }
    if (index == 3) {
      this.lockCategories = true;
      this.categoriesList.forEach(x => {
        x.checked = false;
        this.allCategoriesChecked = false;
        this.selectedCategories = [];
      });
    }
    if (index == 4) {
      this.lockRecipients = true;
      this.recipientsList.forEach(x => {
        x.checked = false;
        this.allRecipientsChecked = false;
        this.selectedRecipients = [];
      });
    }
    if (index == 5) {
      this.lockFunding = true;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
    if (index == 6) {
      this.lockItemSubgroups = true;
      this.allItemSubgroupsChecked = false;
      this.checkedAdministration = false;
      this.checkedOneonone = false;
      this.checkedGroupactivity = false;
      this.checkedCentrebased = false;
      this.checkedTransport = false;
      this.checkedRecptabsence = false;
      this.checkedTraveltime = false;
      this.checkedSleepover = false;
      this.checkedAdmission = false;
      this.checkedItem = false;
    }
    if (index == 7) {
      this.lockRecipientsGroups = true;
      this.recipientsGroupsList.forEach(x => {
        x.checked = false;
        this.allRecipientsGroupsChecked = false;
        this.selectedRecipientsGroups = [];
      });
    }
  }
}

