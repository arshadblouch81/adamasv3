import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GlobalService, ListService ,MenuService, PrintService,NavigationService,TimeSheetService, TableNavigationService } from '@services/index';
import { SwitchService } from '@services/switch.service';
import { NzModalService } from 'ng-zorro-antd';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-staff-competencies',
  templateUrl: './staff-competencies.component.html',
  styles: [`
  tr.highlight {
    background-color: #85B9D5; /* Change to your preferred highlight color */
  }
  `]
})
export class StaffCompetenciesComponent implements OnInit {
  
  tableData: Array<any>;
  items:Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  modalVariables:any;
  inputVariables:any;
  dateFormat: string ='dd/MM/yyyy';
  check : boolean = false;
  userRole:string="userrole";
  whereString :string="Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  postLoading: boolean = false;
  isUpdate: boolean = false;
  title:string = "Add New Staff Competencies";
  tocken: any;
  pdfTitle: string;
  radioValue:string='';
  tryDoctype: any;
  drawerVisible: boolean =  false;
  private unsubscribe: Subject<void> = new Subject();
  temp_title: any;
  currentIndex: number = -1;
  constructor(
    private globalS: GlobalService,
    private cd: ChangeDetectorRef,
    private timeS:TimeSheetService,
    private listS:ListService,
    private menuS:MenuService,
    private formBuilder: FormBuilder,
    private printS:PrintService,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    private navigationService: NavigationService,
    private router: Router,
    private tableNavigationService: TableNavigationService
    ){}

    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.loadData();
      this.populateDropDown();
      this.loading = false;
      this.cd.detectChanges();
    }
    
    showAddModal() {
      this.title = "Add New Staff Competencies"
      this.resetModal();
      this.modalOpen = true;
    }
    loadTitle()
    {
      return this.title;
    }
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.title = "Edit Staff Competencies"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      const { 
        description,
        cGroup,
        mandatory,
        undated,
        end_date,
        recordNumber
      } = this.tableData[index];
      this.inputForm.patchValue({
        name: description,
        group:cGroup,
        mandatory:(mandatory == 1) ? true:false,
        undated:(undated == 1) ? true:false,
        enddate:end_date,
        recordNumber:recordNumber
      });
      this.temp_title = description;
    }
    
    handleCancel() {
      this.modalOpen = false;
    }
    pre(): void {
      this.current -= 1;
    }
    
    next(): void {
      this.current += 1;
    }
    trueString(data: any): string{
      return data ? '1': '0';
    }
    save() {
      this.postLoading = true;     
      const group = this.inputForm;
      if(!this.isUpdate){        
        this.postLoading = true;   
        const group = this.inputForm;
        let name        = group.get('name').value.trim().toUpperCase();
        let is_exist    = this.globalS.isDescriptionExists(this.tableData,name);
        if(is_exist){
          this.globalS.sToast('Unsuccess', 'Title Already Exist');
          this.postLoading = false;
          return false;   
        }
            name       =  this.globalS.isValueNull(group.get('name').value).trim().toUpperCase();
        let domain       = "'STAFFATTRIBUTE'";
        let groupz       =  this.globalS.isValueNull(group.get('group').value);
        let mandatory    = this.trueString(group.get('mandatory').value);
        let undated      = this.trueString(group.get('undated').value);
        let enddate      = !(this.globalS.isVarNull(group.get('enddate').value)) ?  "'"+this.globalS.convertDbDate(group.get('enddate').value)+"'" : null;
        let values = domain+","+name+","+groupz+","+mandatory+","+undated+","+enddate+","+this.radioValue;
        let sql = "insert into DataDomains([Domain],[Description],[User1],[Embedded],[Undated],[EndDate],[USERS]) Values ("+values+")"; 
        
        this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
          
          if (data) 
          this.globalS.sToast('Success', 'Saved successful');     
          else
          this.globalS.sToast('Success', 'Saved successful');
          this.loadData();
          this.postLoading = false;          
          this.handleCancel();
          this.resetModal();
        });
      }else{
        this.postLoading  = true;   
        const group       = this.inputForm;
        let domain       = "'STAFFATTRIBUTE'";
        let name         = group.get('name').value;
        let is_exist    = this.globalS.isDescriptionExists(this.tableData,name);
        if(this.temp_title != name){
        if(is_exist){
          this.globalS.sToast('Unsuccess', 'Title Already Exist');
          this.postLoading = false;
          return false;   
         }
        }
        name       =  this.globalS.isValueNull(group.get('name').value).trim().toUpperCase();
        let groupz       =  this.globalS.isValueNull(group.get('group').value);
        let mandatory    = this.trueString(group.get('mandatory').value);
        let undated      = this.trueString(group.get('undated').value);
        let enddate      = !(this.globalS.isVarNull(group.get('enddate').value)) ?  "'"+this.globalS.convertDbDate(group.get('enddate').value)+"'" : null;
        let recordNumber  = group.get('recordNumber').value;
        
        let sql  = "Update DataDomains SET [Description]="+ name +",[User1] ="+ groupz +",[Embedded] ="+ mandatory +",[Undated] ="+ undated +",[EndDate] ="+ enddate +",[USER5] ="+ this.radioValue + " WHERE [RecordNumber] ='"+recordNumber+"'";
        
        this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
          if (data) 
          this.globalS.sToast('Success', 'Saved successful');     
          else
          this.globalS.sToast('Success', 'Saved successful');
          this.postLoading = false;      
          this.loadData();
          this.handleCancel();
          this.resetModal();   
          this.isUpdate = false; 
        });
      }   
    }
    loadData(){
      let sql ="SELECT ROW_NUMBER() OVER(ORDER BY Description) AS row_num,RecordNumber,Description,Description as name,Embedded AS Mandatory,User1 as cGroup,Undated as undated,CONVERT(varchar, [enddate],105) as end_date from DataDomains "+this.whereString+" Domain = 'STAFFATTRIBUTE'";
      this.loading = true;
      this.listS.getlist(sql).subscribe(data => {
        this.tableData = data;
        this.tableNavigationService.initializeKeyboardListener(data);
        console.log(data);
        this.loading = false;
      });
    }
    populateDropDown(){
      let sql2 = "Select RecordNumber, Description from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND Domain = 'COMPETENCYGROUP'  ORDER BY DESCRIPTION";
      this.listS.getlist(sql2).subscribe(data => {
        this.items = data;
        console.log(data);
      });
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = "WHERE";
        this.loadData();
      }else{
        this.whereString = "Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
        this.loadData();
      }
    }
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    }
    delete(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.deleteDomain(data.recordNumber)
          .pipe(takeUntil(this.unsubscribe)).subscribe(datas => {
            if (datas) {
              this.timeS.postaudithistory({
                Operator:this.tocken.user,
                actionDate:this.globalS.getCurrentDateTime(),
                auditDescription:'Staff Competencies Deleted',
                actionOn:'STAFFATTRIBUTE',
                whoWhatCode:data.recordNumber, //inserted
                TraccsUser:this.tocken.user,
              }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.globalS.sToast('Success', 'Deleted successful');
              }
              );
              this.loadData();
              return;
            }
          });
    }
    buildForm() {
      this.inputForm = this.formBuilder.group({
        name: '',
        group: '',
        enddate:'',
        mandatory :false,
        undated   :false,
        recordNumber:null
      });
    }
    handleOkTop() {
      this.generatePdf();
      this.tryDoctype = ""
      this.pdfTitle = ""
    }
    handleCancelTop(): void {
      this.drawerVisible = false;
      this.pdfTitle = ""
    }
    generatePdf(){
      this.drawerVisible = true;
      
      this.loading = true;
      
      var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY Description) AS Field1,Description as Field2,Embedded as Field3,CONVERT(varchar, [enddate],105) as Field4 from DataDomains "+this.whereString+" Domain='STAFFATTRIBUTE'";
      
      const data = {
        "template": { "_id": "0RYYxAkMCftBE9jc" },
        "options": {
          "reports": { "save": false },
          "txtTitle": "Staff Competencies List",
          "sql": fQuery,
          "userid":this.tocken.user,
          "head1" : "Sr#",
          "head2" : "Name",
          "head3" : "Mandatory",
          "head4" : "End Date",
        }
      }
      this.printS.printControl(data).subscribe((blob: any) => { 
        let _blob: Blob = blob;
        let fileURL = URL.createObjectURL(_blob);
        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
        this.loading = false;
        this.pdfTitle ="Staff Competencies List";
        }, err => {
        this.loading = false;
        this.ModalS.error({
          nzTitle: 'TRACCS',
          nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
          nzOnOk: () => {
            this.drawerVisible = false;
          },
        });
      });
      this.loading = true;
      this.tryDoctype = "";
      this.pdfTitle = "";
    } 
    goBack(): void {
      const previousUrl = this.navigationService.getPreviousUrl();
      const previousTabIndex = this.navigationService.getPreviousTabIndex();
    
      if (previousUrl) {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      } else {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      } 
    }
    scrollToRow() {
      this.tableNavigationService.scrollToRow(this.tableData);
    }
  }
  