import { Component, OnInit,forwardRef, Input, ElementRef, AfterViewInit, HostListener, Renderer2, ViewChildren, QueryList } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { DatePipe } from '@angular/common';

import * as moment from 'moment'
import { Subject } from 'rxjs'

import { RosterDateTimeDirective } from '../../shared/roster/roster.directive';

interface Position{
    x: number,
    y: number
}


const noop = () => {
};

export const ROSTER_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => RosterTimeline),
};

@Component({
    selector: 'roster-timeline',
    templateUrl: './roster.html',
    styles:[
        `
            table{
                border-spacing: 0;
                box-sizing: border-box;
                width: 100%;
                table-layout: fixed;
                border-collapse: collapse;
                height:100%;
            }            
            .fc-scrollable{
                display:inline-block;
                height: 100%;
                position: relative;
                z-index: 1;
            }
            th{
                text-align:center;
            }
            td{
                text-align:center;
            }
            .fc-col{
                position:absolute;
                top:0;
                bottom:0;
                left:0;
                right:0;
            }

            .fc-col table tbody tr td{
                border-right: 1px solid #e8e8e8;
                border-left: 1px solid #e8e8e8;
            }

            .fc-date{
                position: absolute;
                z-index: 3;
                top: 0;
                left: 0;
                right: 0;
            }
            .date-data{
                position: absolute;
                width: 100%;
                overflow: hidden;
                padding: 2px;
                background: #308c77;
                color: #fff;
                border-right: 1px solid #fff;
                border-top: 1px solid #fff;
                border-radius:4px;
                z-index:2;
            }
            a.date-data > span{
                font-size: 8px;
                text-overflow: ellipsis;
                white-space: nowrap;
            }
            a.date-data:hover{
                cursor:pointer;
                background:#3eb398 !important;
                text-decoration:none;
            }
            td.title{
                background: #f1f1f1;
                border: 1px solid #cacaca;
                color: #636363;
            }
            tbody.fc-body{
                border: 1px solid #e2e2e2;
            }
            .fc-time table tbody tr > td:first-child{
                font-size:10px !important;
            }
            .fc-time table tbody tr:nth-child(odd){
                background: #f4f4f4;
                color: #306772;
            }

            .gwapo{
                display:inline-block;
                background:green;
                padding:1rem;
            }

            .noselect {
                -webkit-touch-callout: none; /* iOS Safari */
                  -webkit-user-select: none; /* Safari */
                   -khtml-user-select: none; /* Konqueror HTML */
                     -moz-user-select: none; /* Firefox */
                      -ms-user-select: none; /* Internet Explorer/Edge */
                          user-select: none; /* Non-prefixed version, currently
                                                supported by Chrome and Opera */
              }

            .date-title{
                position: sticky;
                top: -25px;
                z-index: 2;
                display: flex;
                text-align:center;
                color:#fff;
                flex-direction: row;
                justify-content: space-around;
                background: linear-gradient(to top, #283E51, #4B79A1);
            }

            .date-body{
                position: relative;
                text-align:center;
            }

            .date-time{
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                display: flex;
                width: 100%;
                height: 100%;
                flex-direction: row;
            }

            .date-time .tsk{
                padding:0 5px;
            }
            .date-time .tsk:nth-child(even){
                background: #f1f1f1;
            }

            .date{
                display: flex;
                flex-direction: row;
                justify-content: space-around;
                text-align:center;
            }

            .date > div.tsk{
                z-index:1;
            }
            
            .zeroMin{
                background: #34536e;
                border-bottom-left-radius: 10px;
                color: #fff;
            }

            .time-value{
                font-size: 10px;
            }

        
        `
    ],
    providers: [ ROSTER_VALUE_ACCESSOR ]
})

export class RosterTimeline implements OnInit, ControlValueAccessor, AfterViewInit {
    //Placeholders for the callbacks which are later provided
    //by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    @ViewChildren(RosterDateTimeDirective, { read: ElementRef }) dateTime: QueryList<RosterDateTimeDirective>;

    public modelChanges = new Subject<any>();

    @Input() source: Array<any> = []; 

    days: Array<any> = [];
    times: Array<any> = [];

    private innerValue: any = '';
    private width: any;

    plot: boolean = false;

    constructor(
        private elRef: ElementRef,
        private renderer2: Renderer2,
        private datepipe: DatePipe
    ){

        this.modelChanges.subscribe(data =>{      
            if(data){  
                this.plotGraph(data.date); 
                this.plotRoster();
            }
        })
    }

    @HostListener('document:pointerdown', ['$event'])
    onPointerDown(event) {        
        this.selectedData = ""
        this.hahays = false
    }

    ngOnInit(){
        let start = moment().hour(6).minute(0);
        let end = moment().hour(12).minute(15);
    }

    ngAfterViewInit(){

    }

    get gwapo(){
        return this.width;
    }

    set gwapo(val: any){
        this.width = { 'width': val }
    }

    plotGraph(date: any){
        this.days = [];
        this.times = [];

        let time = moment().hour(0).minute(0);
        let index =  moment(time).subtract(15,'m');

        this.times.push({timeObj: moment(time),time: moment(time).format('HH:mm'),isZeroMin: moment(time).format('mm') == '00' ? true : false });          
        while(moment(time).format('HH:mm') != moment(index).format('HH:mm')){
            time = moment(time).add(15,'m');
            this.times.push({
                timeObj: moment(time),
                time: moment(time).format('HH:mm'),
                isZeroMin: moment(time).format('mm') == '00' ? true : false
                
            });
        }  

        let noOfDays = moment(date).daysInMonth();
        let currMonthStart = moment(date).startOf('month');
        const computeWidth = 100/noOfDays

        this.gwapo = `${computeWidth}%`

        for(var a = 0 ; a < noOfDays ; a++){
            this.days.push({ 
                dateObj: moment(currMonthStart), 
                date :  moment(currMonthStart).format('YYYY/MM/DD'),
                day: moment(currMonthStart).format('DD/MM/YYYY')
            });
            currMonthStart = moment(currMonthStart).add(1,'d');
        }

    }

    plotRoster(){
        this.plot = true;
    }

    get value(): any {
        return this.innerValue;
    };

    //set accessor including call the onchange callback
    set value(v: any) {
        if (v !== this.innerValue) {
            this.innerValue = v;
        }
    }

    //Set touched on blur
    onBlur() {
        this.onTouchedCallback();
    }

    //From ControlValueAccessor interface
    writeValue(value: any) {
        if (value !== this.innerValue) {
            this.innerValue = value;
            this.modelChanges.next(this.innerValue);
        }
    }

    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }

    filterTime(date:any){
        if(moment(date).format('mm') == '00')
            return this.datepipe.transform(date,'h a');

        return this.datepipe.transform(date,'mm');
    }

    computeDimensions(data:any){
        let start = moment(data.shift_Start, ['YYYY/MM/DD HH:mm']);
        let end = moment(data.shift_End,['YYYY/MM/DD HH:mm']);

        let diff = end.diff(start,'minute');
        let asd = moment().hour(0).minute(0);

        // console.log(asd.diff(moment().hour(parseInt(start.format('HH'))).minutes(0),'minutes'))

        // const samp = {
        //     time: start.format('HH: mm')
        // }

        const pos = {
            'top':  Math.abs(asd.diff(moment().hour(parseInt(start.format('HH'))).minutes(start.minutes()),'minutes') / 15) * 24 + 'px',
            'height': (diff/15)*24 + 'px'
        }


        return pos;
    }

    computeWidth(data: any): number{      
        var sameDate = (this.value).filter((e:any) => e.roster_Date == data.roster_Date);
        return sameDate.length;
    }


    position: Position = { x:0, y:0 }
    private startPosition: Position
    draggable: boolean = false;
    hahays = false;
    selectedData: any;

    heller(newTime: any){
        console.log(newTime)
    }

    pointerDown(event: PointerEvent, data: any){        
        event.stopPropagation();
        this.selectedData = data;
        this.hahays = true
        console.log(this.selectedData)
    }

    dragStart(event: PointerEvent){
        this.startPosition = {
            x: event.clientX - this.position.x,
            y: event.clientY - this.position.y
        } 
    }

    dragMove(event: PointerEvent){        
        this.position.x = event.clientX - this.startPosition.x
        this.position.y = event.clientY - this.startPosition.y

        console.log(this.position)
    }

    dragEnd(){
        console.log('drag End')
    }

    documentDown(data: any){

    }


}