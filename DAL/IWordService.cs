using System.Threading.Tasks;
using Adamas.Models.Modules;
using Adamas.Models.DTO;
using System.IO;
using Adamas.Models.Tables;

namespace Adamas.DAL {
    public interface IWordService
    {
        Task<Result> MergeFieldsDocument(QuoteMergeFields person, string sourcePath, string destinationPath, string filename);
        Task<bool> CreateDocument();
        Task<(MemoryStream, string, string)?> PrintQuotes(QuoteHeaderDTO quote, string sourcePath, string destinationPath, string filename, QuoteMergeFields person);
        Task<Result> MergeFieldsDocumentUsingInterop(QuoteMergeFields person, string sourcePath, string destinationPath, string filename, QuoteHeaderDTO quote, Qte_Hdr hdr);
        Task<Result> MergeFieldsDocuments(QuoteMergeFields person, string sourcePath, string destinationPath, string filename);
    }
}