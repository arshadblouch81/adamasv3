import { ChangeDetectorRef, Component, OnInit,HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
import { Observable, of, from, Subject, EMPTY } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { GlobalService, ListService, PrintService,NavigationService,MenuService,TimeSheetService } from '@services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
@Component({
  selector: 'app-incidentsubcat',
  templateUrl: './incidentsubcat.component.html',
  styles: [`
  tr.highlight {
    background-color: #85B9D5; /* Change to your preferred highlight color */
  }
  `]
})
export class IncidentsubcatComponent implements OnInit {
  
  tableData: Array<any>;
  IncidentTypesArr:Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  modalVariables:any;
  inputVariables:any;
  heading:string = "Add New Incident Sub Category";
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;
  dateFormat: string ='dd/MM/yyyy';
  check : boolean = false;
  userRole:string="userrole";
  whereString :string="Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  private unsubscribe: Subject<void> = new Subject();
  temp_title: any;
  searchTerm: string = '';
  currentIndex: number | null = null;
  
  constructor(
    private globalS: GlobalService,
    private cd: ChangeDetectorRef,
    private switchS:SwitchService,
    private listS:ListService,
    private printS:PrintService,
    private menuS:MenuService,
    private formBuilder: FormBuilder,
    private timeS: TimeSheetService,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    private navigationService: NavigationService,
    private router: Router
    ){}
    
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.populateDropDown();
      this.loadData();
      this.loading = false;
      this.cd.detectChanges();
    }
    loadData(){
      this.loading = true;
      this.menuS.getDataDomainByType("INCIDENTSUBGROUP",this.check).subscribe(data => {
        this.tableData = data;
        this.loading = false;
      });
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = "WHERE";
        this.loadData();
      }else{
        this.whereString = "Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
        this.loadData();
      }
    }
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    }
    populateDropDown(){
      let sql2 ="select Description as name,recordNumber from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND Domain='INCIDENT TYPE' ";
      this.loading = true;
      this.listS.getlist(sql2).subscribe(data => {
        this.IncidentTypesArr = data;
        this.loading = false;
      });
    }
    showAddModal() {
      this.resetModal();
      this.modalOpen = true;
    }
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.heading  = "Edit Incident Sub Category"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      const { 
        staff,
        name,
        end_date,
        recordNumber,
        
      } = this.tableData[index];
      this.inputForm.patchValue({
        name: name,
        end_date:end_date,
        incident_type:staff,
        recordNumber:recordNumber
      });
      this.temp_title = name;
    }
    loadtitle(){
      return this.heading
    }
    handleCancel() {
      this.modalOpen = false;
    }
    pre(): void {
      this.current -= 1;
    }
    
    next(): void {
      this.current += 1;
    }
    save() {
      this.postLoading = true;     
      const group = this.inputForm;
      if(!this.isUpdate){         
        let name          = group.get('name').value.trim().toUpperCase();
        let is_exist    = this.globalS.isNameExists(this.tableData,name);
        if(is_exist){
          this.globalS.sToast('Unsuccess', 'Title Already Exist');
          this.postLoading = false;
          return false;   
        }
        this.switchS.addData(  
          this.modalVariables={
            title: 'Incident Sub Categories'
          }, 
          this.inputVariables = {
            display: group.get('name').value,
            type:group.get('incident_type').value,
            end_date:!(this.globalS.isVarNull(group.get('end_date').value)) ? this.globalS.convertDbDate(group.get('end_date').value) : null,
            domain: 'INCIDENTSUBGROUP', 
          }
          ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) 
            this.globalS.sToast('Success', 'Saved successful');     
            else
            this.globalS.sToast('Unsuccess', 'Data not saved' + data);
            this.loadData();
            this.postLoading = false;          
            this.handleCancel();
            this.resetModal();
          });
        }else{
          this.postLoading = true;     
          const group = this.inputForm;
          let name        = group.get('name').value.trim().toUpperCase();
          if(this.temp_title != name){
            let is_exist    = this.globalS.isNameExists(this.tableData,name);
            if(is_exist){
              this.globalS.sToast('Unsuccess', 'Title Already Exist');
              this.postLoading = false;
              return false;   
            }
          }
          this.switchS.updateData(  
            this.modalVariables={
              title: 'Incident Sub Categories'
            }, 
            this.inputVariables = {
              display: group.get('name').value,
              type:group.get('incident_type').value,
              end_date:!(this.globalS.isVarNull(group.get('end_date').value)) ? this.globalS.convertDbDate(group.get('end_date').value) : null,
              primaryId:group.get('recordNumber').value,
              domain: 'INCIDENTSUBGROUP',
            }
            
            ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
              if (data)
              {
                this.timeS.postaudithistory({
                  Operator:this.tocken.user,
                  actionDate:this.globalS.getCurrentDateTime(),
                  auditDescription:'Incident Sub Categories Changed',
                  actionOn:'INCIDENTSUBGROUP',
                  whoWhatCode:group.get('recordNumber').value, //inserted
                  TraccsUser:this.tocken.user,
                }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                  this.globalS.sToast('Success', 'Update successful');
                }
                );
              }else
              {
                this.globalS.sToast('Unsuccess', 'Data Not Update' + data);
              }
              this.loadData();
              this.postLoading = false;  
              this.isUpdate = false;        
              this.handleCancel();
              this.resetModal();
            });
          }
          
        }
        
        delete(data: any) {
          this.postLoading = true;     
          const group = this.inputForm;
          this.menuS.deleteDomain(data.recordNumber)
          .pipe(takeUntil(this.unsubscribe)).subscribe(datas => {
            if (datas) {
              this.timeS.postaudithistory({
                Operator:this.tocken.user,
                actionDate:this.globalS.getCurrentDateTime(),
                auditDescription:'Incident Sub Categories Deleted',
                actionOn:'INCIDENTSUBGROUP',
                whoWhatCode:data.recordNumber, //inserted
                TraccsUser:this.tocken.user,
              }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.globalS.sToast('Success', 'Deleted successful');
              }
              );
              this.loadData();
              return;
            }
          });
        }
        buildForm() {
          this.inputForm = this.formBuilder.group({
            incident_type:'',
            name:'',
            end_date:'',
            recordNumber:null
          });
        }
        
        handleOkTop() {
          this.generatePdf();
          this.tryDoctype = ""
          this.pdfTitle = ""
        }
        handleCancelTop(): void {
          this.drawerVisible = false;
          this.pdfTitle = ""
        }
        generatePdf(){
          this.drawerVisible = true;
          
          this.loading = true;
          
          var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY Description) AS Field1,Description as Field2 ,HACCCODE as Field3,CONVERT(varchar, [enddate],105) as Field4 from DataDomains "+this.whereString+" Domain='INCIDENTSUBGROUP'";
          
          const data = {
            "template": { "_id": "0RYYxAkMCftBE9jc" },
            "options": {
              "reports": { "save": false },
              "txtTitle": "Incident Sub Categories List",
              "sql": fQuery,
              "userid":this.tocken.user,
              "head1" : "Sr#",
              "head2" : "Name",
              "head3" : "Type",
              "head4" : "End Date",
            }
          }
          this.printS.printControl(data).subscribe((blob: any) => { 
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.pdfTitle = "Incident Sub Categories.pdf"
          }, err => {
            this.loading = false;
            this.ModalS.error({
              nzTitle: 'TRACCS',
              nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
              nzOnOk: () => {
                this.drawerVisible = false;
              },
            });
          });
          
          this.loading = true;
          this.tryDoctype = "";
          this.pdfTitle = "";
        }
        goBack(): void {
          const previousUrl = this.navigationService.getPreviousUrl();
          const previousTabIndex = this.navigationService.getPreviousTabIndex();
        
          if (previousUrl) {
            this.router.navigate(['/admin/configuration'], {
              queryParams: { tab: previousTabIndex } // Pass tab index in query params
            });
          } else {
            this.router.navigate(['/admin/configuration'], {
              queryParams: { tab: previousTabIndex } // Pass tab index in query params
            });
          }
          
        }
        @HostListener('document:keydown', ['$event'])
        handleKeyboardEvent(event: KeyboardEvent) {
          // Handle character input
          if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
              this.searchTerm += event.key.toLowerCase();
              this.scrollToRow();
          } 
          // Handle backspace
          else if (event.key === 'Backspace') {
              // Only reset if there's something to remove
              this.searchTerm = '';
              if (this.searchTerm.length > 0) {
                  this.searchTerm = this.searchTerm.slice(0, -1);
                  this.scrollToRow();
              }
          }
          // Handle other keys (optional)
          else if (event.key === 'Escape') {
              // Reset search term on escape key
              this.searchTerm = '';
              this.scrollToRow();
          }
        }
    
        scrollToRow() {
            // Find the index of the first matching item
            const matchIndex = this.tableData.findIndex(item => 
                item.name.toLowerCase().startsWith(this.searchTerm)
            );
            // Scroll to the matching row if it exists
            if (matchIndex !== -1) {
                const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
                if (tableRow) {
                    tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                    this.highlightRow(tableRow);
                }
            } else {
            }
        }
    
        highlightRow(row: HTMLElement) {
            row.classList.add('highlight');
            setTimeout(() => row.classList.remove('highlight'), 2000);
        }
        
        
      }
      