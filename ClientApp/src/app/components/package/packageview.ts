
  import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService,PrintService, ShareService,fundingDropDowns, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject,EMPTY } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import * as moment from 'moment';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { toNumber, values } from 'lodash';
import format from 'date-fns/format';
// import { format } from 'path';


export interface TreeNodeInterface {
    id: string;
    date: string;
    description: string;
    usageDetails: string;
    amount: number;
    level?: number;
    expand?: boolean;
   
    children?: TreeNodeInterface[];
    parent?: TreeNodeInterface;
  }
  interface TreeNode {
    id: number;
    name: string;
    children?: TreeNode[];
  }
const packageDefaultForm: any = {
    personID:'',
    program: null,
    packageLevel:'',
    packageType:[null, Validators.required],
    programStatus:[null, Validators.required],
    expireUsing:[null, Validators.required],
    priority: null,
    notes:'',
    quantity:'',
    itemUnit:'',
    perUnit:'',
    period:'',
    timeUnit:'',
    aP_CostType:'',
    aP_PerUnit :null,
    packg_balance:false,
    recurant:false,
    alertStartDate:'',
    aP_Period:'',
    aP_BasedOn:'',
    aP_YellowQty:'',
    aP_OrangeQty:'',
    aP_RedQty:'',
    shared:false,
    startDate:'',
    expiryDate:'',
    reminderDate:'',
    packageTermType:'',
    autoRenew:false,
    rolloverRemainder:false,
    deactivateOnExpiry:false,
    dailyBasicCareFee:'$0.00',
    clientCont:'$1234',
    dailyIncomeTestedFee:'$0.00',
    incomeTestedFee:'$1234',
    contingency_Start:'',
    contingency:'',
    currentlyBanked:'',
    recordNumber:'',
    startPDate:'',
    endPDate:'',
    hcpOptIn : false,
    packageSupplements:null,
    hardShipSupplement:0,

}

@Component({
    selector: 'package-view',
    templateUrl: './packageview.html',
    styleUrls: ['./packageview.css']
   
    
  })

 export class PackageView implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    dateFormat: string = 'dd/MM/yyyy';
    user: any;
    loading: boolean = false;
    postLoading: boolean = false;
    showSubMenu: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    topBelow = { top: '10px' }
    openPackageBalanceModel:boolean;
    balances:Array<any>=['Balance at Start of Period','Balance at End of Period','Balance at Start of Period and End of Period'];
    payperiod :any;
    @Input() ViewPackage:Subject<any>=new Subject();
    openModel:boolean=false;
    tableData: Array<any> = [];
    fundingData: Array<any> = [];
    fundingData2: Array<any> = [];
    fundingPayments: Array<any> = [];
    fundingPaymentTotal:any;
    BudgetDetail:any;
    PackageBalance:any;
    PackageBalanceData:any;
    MoneySpend:any;
    Final_Balance:any;
    packageDetailForm: FormGroup;
    programsNames: any;
    supplements: FormGroup;
    PackageBalanceForm: FormGroup;
    programLevel: any;
    period: string[];
    levels: string[];
    cycles: string[];
    budgetEnforcement: string[];
    alerts: string[];
    DefPeriod: string[];
    expireUsing: string[];
    unitsArray: string[];
    dailyArry: string[];
    visibleRecurrent: boolean = false;
    isMenuOpen :boolean=false;
    packageTerm: string[];
    status: string[];
    type: string[];
    alertCycle:Array<any>=[];
    fundingprioritylist: any;
    fundingAriaList:Array<any> =[];
    IS_CDC: boolean = true;
    selectedProgram: any;
    activeRowData:any;
    selectedRowIndex:number;
    OptIn:boolean=true;
    formatterPercent = (value: number): string => `${value} %`;
    parserPercent = (value: string): string => value.replace(' %', '');
    formatterDollar = (value: number): string => `$ ${value}`;
    parserDollar = (value: string): string => value.replace('$ ', '');
    OptionsChecked:Array<boolean> = [false,false,false,false];
    OptionDate:any;
    today:any;
    SupplementVisibility:any;
    @ViewChild('select') select: ElementRef;  
  

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
        ) {
           cd.detach();
            
            // this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            //     if (data instanceof NavigationEnd) {
            //         if (!this.sharedS.getPicked()) {
            //             this.router.navigate(['/admin/recipient/personal'])
            //         }
            //     }
            // });
            
            // this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            //     if (this.globalS.isCurrentRoute(this.router, 'funding')) {
            //         this.user = data;
            //         this.search(data);
            //     }
            // });
        }
        
        ngOnInit(): void {
          
          this.user = this.sharedS.getPicked();
          this.dropDowns();
           this.listS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.payperiod  = data;
          });
           this.today = new Date();
          //today.setDate(today.getDate()-60); 

        
           this.buildForm(); 
           
            this.ViewPackage.subscribe(res=>{
                this.openModel=true;
                this.selectedProgram = res;
                this.showEditModal(res.recordNumber);
                this.cd.reattach();
               
            });
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        
        search(user: any = this.user) {
            this.cd.reattach();
            
            // this.loading = true;
            // this.timeS.getfunding(user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            //     this.loading = false;
            //     this.tableData = data;
            //     this.cd.markForCheck();
            // });
            this.dropDowns();
          
            this.OptionDate ="By Date Approved";
           
            let program = this.selectedProgram.program;
            let startDate = this.packageDetailForm.value.startPDate;
            let endDate = this.packageDetailForm.value.endPDate;
            let input={
                code : user.code,
                id: user.id,
                program: program,
                startDate: format(startDate,'yyyy/MM/dd'),
                endDate : format(endDate,'yyyy/MM/dd')
            }
            this.timeS.getfundingreport(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.loading = false;
                this.fundingData = this.convertToTree(data);
                
                data.forEach(item => {
                    this.mapOfExpandedData[item.id] = this.convertTreeToList(item);
                   
                  });
                  
                
            });
            this.timeS.getRecipientBudgetDetail(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
              this.loading = false;
              this.BudgetDetail = data[0];

              this.MoneySpend = this.BudgetDetail.gtotal;
              

             
              
              //this.MoneySpend=Math.round(total).toFixed(2);
          });
         
          
            this.timeS.getfundingPayments(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.loading = false;
                this.fundingPayments = data;              
             
                let sum = 0;             
                let arr= this.fundingPayments.map(x=>x.amount);
                sum=arr.reduce((accumulator, currentValue) => accumulator + currentValue, 0);

                this.fundingPaymentTotal = sum;
              

                
            });
            let temp = new Date(input.startDate );
            temp.setDate(temp.getDate()-1);
            input.startDate = format(temp,'yyyy/MM/dd');
            this.timeS.getRecipientPackageBalance(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
              this.loading = false;
              this.PackageBalanceData=data[0];
              this.PackageBalance = data[0].balance;
             
              this.Final_Balance = (this.PackageBalance +this.fundingPaymentTotal) - this.MoneySpend;

              this.balances[0] = this.balances[0] + " at " + format( new Date(input.startDate) ,'dd/MM/yyyy'); 

              this.PackageBalanceForm.patchValue({
                
                commonBalance :this.PackageBalanceData.balance,
                commonBalProvider:this.PackageBalanceData.commonBalProvider,
                commonWealth: this.PackageBalanceData.commonBalCommonWealth,
                clientBalance: this.PackageBalanceData.clientBalance,
                programId : this.PackageBalanceData.programId,
                balance : this.balances[0] 
              

              });
              this.detectChanges();
          });
            

        }
       
        dropDowns(){
          
            this.timeS.getprogrampackages(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => this.programsNames = data)
            this.listS.getfundingprioritylist().pipe(takeUntil(this.unsubscribe)).subscribe(data => this.fundingprioritylist = data)
            this.listS.getfundingAria().pipe(takeUntil(this.unsubscribe)).subscribe(data => this.fundingAriaList= data)
            
            

           // console.log(this.programsNames);
            this.period        = ['ANNUAL','MONTH','QUARTER'];
            this.levels        = ['Level 1','Level 2','Level 3','Level 4','STRC'];
            this.cycles        = fundingDropDowns.cycle;
            this.budgetEnforcement = ['HARD','SOFT'];
            this.alerts        = fundingDropDowns.alerts;
            this.packageTerm   = fundingDropDowns.packageTerm;
            this.type          = fundingDropDowns.type;
            this.status        = fundingDropDowns.status;
            this.DefPeriod     = fundingDropDowns.period;
            this.expireUsing   = fundingDropDowns.expireUsing;
            this.unitsArray    = fundingDropDowns.perUnit;
            this.levels        = fundingDropDowns.levels;
            this.dailyArry     = ['DAILY'];
            this.alertCycle    = fundingDropDowns.alertCycle;
        }

        resetDisabledDropdowns(){
            this.packageDetailForm.get('packageLevel').enable()
            // this.packageDetailForm.controls.packageLevel.setValue(x.level);
            this.packageDetailForm.get('quantity').enable()
            // this.packageDetailForm.controls.quantity.setValue(x.quantity);
            this.packageDetailForm.get('packageType').enable()
            // this.packageDetailForm.controls.packageType.setValue(typeOpt);
            this.packageDetailForm.get('itemUnit').enable()
            // this.packageDetailForm.controls.itemUnit.setValue('DOLLARS');
            this.packageDetailForm.get('perUnit').enable()
            // this.packageDetailForm.controls.perUnit.setValue('PER');
            this.packageDetailForm.get('period').enable()
            // this.packageDetailForm.controls.period.setValue('DAY');
            this.packageDetailForm.get('expireUsing').enable()
            // this.packageDetailForm.controls.expireUsing.setValue('CHARGE RATE')
            this.detectChanges();
        }

        buildForm() {
            this.packageDetailForm = this.formBuilder.group(packageDefaultForm);
           // this.resetDisabledDropdowns();

            this.supplements = this.formBuilder.group({
                domentica:false,
                levelSupplement:'',
                oxygen:false,
                feedingSuplement:false,
                feedingSupplement:'',
                EACHD:false,
                
                feedingSupplementLevel:'',
                visibilitySuplement:false,              
                supplementVisibilityOption:'',
                visibilitySuplementLevel:'',
                visibilitySuplementLevel2:''
                
                
            });

            this.PackageBalanceForm = this.formBuilder.group({
               commonBalance :'',
               commonBalProvider:'',
               commonWealth: '',
               clientBalance: '',
               programId : '',
              
              
          });
            

            this.packageDetailForm.get('aP_CostType').valueChanges.subscribe(data => {
                this.packageDetailForm.get('recurant').enable()
            });

            this.packageDetailForm.get('alertStartDate').valueChanges.subscribe(data => {
                if(this.globalS.isEmpty(data)){
                    this.packageDetailForm.get('aP_BasedOn').disable()
                    this.packageDetailForm.get('aP_RedQty').disable()
                    this.packageDetailForm.get('aP_OrangeQty').disable()
                    this.packageDetailForm.get('aP_YellowQty').disable()
                }else{
                    this.packageDetailForm.get('aP_BasedOn').enable()
                    this.packageDetailForm.get('aP_RedQty').enable()
                    this.packageDetailForm.get('aP_OrangeQty').enable()
                    this.packageDetailForm.get('aP_YellowQty').enable()
                }
            });
            
            this.packageDetailForm.get('program').valueChanges
            .pipe(
                switchMap(x => {
                    if(!x) {
                        return EMPTY
                    };
                    return this.listS.getprogramlevel(x)
                }),
                switchMap(x => {

                    this.IS_CDC = false;

                    if(x.isCDC){

                        this.IS_CDC = true;
                        var typeOpt = '';

                        if(x.user2 == 'Various'){
                            typeOpt = 'CDC-HCP';
                        }else{
                            typeOpt = x.user2;
                        }
                        this.type.push(typeOpt);
                        
                        this.packageDetailForm.get('packageLevel').disable()
                        this.packageDetailForm.controls.packageLevel.setValue(x.level);
                        this.packageDetailForm.get('quantity').disable()
                        this.packageDetailForm.controls.quantity.setValue(x.quantity);                      
                        this.packageDetailForm.controls.packageType.setValue(typeOpt);
                        this.packageDetailForm.get('packageType').disable()
                        this.packageDetailForm.controls.itemUnit.setValue('DOLLARS');
                        this.packageDetailForm.get('itemUnit').disable()
                        this.packageDetailForm.controls.perUnit.setValue('PER');
                        this.packageDetailForm.get('perUnit').disable()
                        this.packageDetailForm.controls.period.setValue('DAY');
                        this.packageDetailForm.get('period').disable()                       
                      
                        this.packageDetailForm.controls.expireUsing.setValue('CHARGE RATE')
                        this.packageDetailForm.get('expireUsing').disable()
                        this.detectChanges();
                        return this.listS.getlevelRate(x.level);
                    }
                    // else{
                    //     this.packageDetailForm.controls.quantity.setValue('2');
                    //     this.packageDetailForm.get('quantity').enable()
                    //     this.packageDetailForm.controls.packageType.setValue('3')
                    //     this.packageDetailForm.get('packageType').enable()
                    //     this.packageDetailForm.controls.expireUsing.setValue('4')
                    //     this.packageDetailForm.get('expireUsing').enable()
                    //     this.packageDetailForm.controls.packageLevel.setValue('5')
                    //     this.packageDetailForm.get('packageLevel').enable()
                    //     this.packageDetailForm.controls.itemUnit.setValue(null);
                    //     this.packageDetailForm.get('perUnit').enable()
                    //     this.packageDetailForm.controls.perUnit.setValue(null);
                    //     this.packageDetailForm.get('period').enable()
                    //     this.packageDetailForm.controls.period.setValue(null);
                    //     this.packageDetailForm.get('expireUsing').enable()
                    //     this.packageDetailForm.controls.expireUsing.setValue(null)
                    //     this.packageDetailForm.get('itemUnit').enable()
                    //     this.packageDetailForm.controls.itemUnit.setValue(null)
                    // }
                    this.detectChanges();
                    return EMPTY;
                })
                ).subscribe(data => {
                    this.packageDetailForm.controls.quantity.setValue(data.levelRate);
                    this.detectChanges();
                });

              
                
            }
            
            savePackagBalance(){

            }
            detectChanges(){
                this.cd.markForCheck();
                this.cd.detectChanges();
            }
            clickOutsideMenu(data: any) {
              if (data.value) {
                this.showSubMenu = false;
              }
            }
            updateprogramPackageBalance(){

              
              this.openPackageBalanceModel=false;
             let  input = this.PackageBalanceForm.value;

              this.timeS.updateprogramPackageBalance(input).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                
                this.globalS.sToast('Success', 'Package Balance Updated');
                
              });
            }

            openPackageBalance(){

              this.openPackageBalanceModel=true;
              this.PackageBalanceForm.patchValue({
                
                commonBalance :this.PackageBalanceData.balance,
                commonBalProvider:this.PackageBalanceData.commonBalProvider,
                commonWealth: this.PackageBalanceData.commonBalCommonWealth,
                clientBalance: this.PackageBalanceData.clientBalance,
                programId : this.PackageBalanceData.programId,
              

              });
              this.cd.detectChanges();
            }

            save() {
                
                var prog_status = this.packageDetailForm.value.programStatus;
                var expireUsing = this.packageDetailForm.value.expireUsing;
                var packageType = this.packageDetailForm.value.packageType;
                var program     = this.packageDetailForm.value.program;
                if(prog_status == null || expireUsing == null || packageType == null || program == null ){
                        this.globalS.wToast('Error', 'All manmdatpry fields must be completed'); 
                        return false;
                }

                this.getPackageSupplements();

                this.packageDetailForm.controls['personID'].setValue(this.user.id);                
                let obj:any=this.alertCycle.filter(x=>x.label ==this.packageDetailForm.value.aP_PerUnit)[0];
                let aP_PerUnit=obj.value;
                this.packageDetailForm.controls['aP_PerUnit'].setValue(aP_PerUnit);  
                 
                if(this.addOREdit == 1){
                    this.packageDetailForm.patchValue({
                        recordNumber: null
                    })
                    const packageDetail = this.packageDetailForm.value;
                    this.timeS.postprogramdetails(packageDetail)
                    .subscribe(data => {
                        this.openModel = false;
                        this.globalS.sToast('Success', 'Package Details Added');
                       // this.search();
                    });
                }
                if(this.addOREdit == 2){
                    const packageDetail = this.packageDetailForm.value;
                    this.timeS.updateprogramdetails(packageDetail)
                    .subscribe(data => {
                        this.openModel = false;
                        this.globalS.sToast('Success', 'Package Details Updated');
                       // this.search();
                    });
                }
            }
            selectedItemGroup(data:any, i:number){
                this.selectedRowIndex=i;
                this.activeRowData=data;
              }
            showEditModal(index: number) {
                this.activeRowData = this.tableData[index];
                this.selectedRowIndex=index;
                this.addOREdit = 2;

                let startDate= new Date(this.today.getFullYear(), this.today.getMonth()-5, 1); // format(new Date(this.payperiod.start_Date),"yyyy/MM/dd");
                let endDate =  new Date(this.today.getFullYear(), this.today.getMonth()+1, 0); //format(new Date(this.payperiod.end_Date),"yyyy/MM/dd");
               
              
                this.timeS.getprogramdetails(index).subscribe(data=>{
                  this.selectedProgram = data;
                  let obj:any=this.alertCycle.filter(x=>x.value ==this.selectedProgram.aP_PerUnit)[0];
                  let ap_PerUnit=null;
                  if (obj!=null)
                    ap_PerUnit=obj.label;
                   
                    //this.cd.markForCheck();
                    this.packageDetailForm.patchValue({
                        program:this.selectedProgram.program,
                        programStatus:this.selectedProgram.programStatus,
                        packageType:this.selectedProgram.packageType,
                        expireUsing:this.selectedProgram.expireUsing,
                        perUnit:this.selectedProgram.perUnit,
                        period:this.selectedProgram.period,
                        itemUnit:this.selectedProgram.itemUnit,
                        quantity:this.selectedProgram.quantity,
                        timeUnit:this.selectedProgram.timeUnit,
                        aP_CostType:this.selectedProgram.aP_CostType,
                        aP_PerUnit: ap_PerUnit,                       
                        aP_Period:this.selectedProgram.aP_Period,
                        aP_YellowQty:this.selectedProgram.aP_YellowQty,
                        aP_OrangeQty:this.selectedProgram.aP_OrangeQty,
                        aP_RedQty:this.selectedProgram.aP_RedQty,
                        aP_BasedOn:this.selectedProgram.aP_BasedOn,
                        alertStartDate:this.selectedProgram.alertStartDate,
                        dailyBasicCareFee:this.selectedProgram.dailyBasicCareFee,
                        clientCont:this.selectedProgram.clientCont,
                        dailyIncomeTestedFee:this.selectedProgram.dailyIncomeTestedFee,
                        incomeTestedFee:this.selectedProgram.incomeTestedFee,
                        startDate:this.selectedProgram.startDate==null? null : new Date(this.selectedProgram.startDate),
                        reminderDate:this.selectedProgram.reminderDate ==null? null : new Date(this.selectedProgram.reminderDate),
                        expiryDate:this.selectedProgram.expiryDate==null? null : new Date(this.selectedProgram.expiryDate),
                        autoRenew:this.selectedProgram.autoRenew,
                        recurant:this.globalS.isEmpty(this.selectedProgram.aP_Period) ? false : true,
                        rolloverRemainder:this.selectedProgram.rolloverRemainder,
                        deactivateOnExpiry:this.selectedProgram.deactivateOnExpiry,
                        packageTermType:this.selectedProgram.packageTermType,
                        contingency_Start:this.selectedProgram.contingency_Start,
                        contingency:this.selectedProgram.contingency,
                        recordNumber:this.selectedProgram.recordNumber,
                        startPDate: new Date(startDate),
                        endPDate: new Date(endDate),
                        hcpOptIn :this.selectedProgram.hcpOptIn,
                        packageSupplements:this.selectedProgram.packageSupplements,
                        hardShipSupplement:this.selectedProgram.hardShipSupplement,
                     
                    });
                    this.supplements.patchValue({
                       
                        packageLevel:this.selectedProgram.packageLevel,
                       
                    })
                    this.setPackageSupplements(this.selectedProgram.packageSupplements);
                    this.cd.detectChanges();
                    this.search(this.user);
                    
                });
                
                this.openModel = true;
            }
            
            delete(data: any) {
                console.log(data);
                this.timeS.deleteprogramdetails(data.recordNumber).subscribe(data => {
                    this.globalS.sToast('Success', 'Package Deleted');
                 //   this.search();
                });
            }

           getPackageSupplements(){
             let data="00000000000000000000000000";
             let len = 0;
              let bits:any= data.split('');

             
              if (this.supplements.value.domentica){
                bits[0]='1';
                len=this.supplements.value.levelSupplement.length;
                let ind= toNumber((this.supplements.value.levelSupplement).charAt(len - 1));
                bits[ind]='1';
              }
               
              if (this.supplements.value.oxygen)
                bits[5]='1';
             

              if (this.supplements.value.feedingSuplement)
                bits[6]='1';

              if (this.supplements.value.feedingSupplementLevel=='Bolus')
                bits[7]='1';
              if (this.supplements.value.feedingSupplementLevel=='Non-Bolus')
                bits[8]='1';

              if (this.supplements.value.EACHD)
                bits[9]='1';

              if (this.supplements.value.visibilitySuplement){
                bits[10]='1';               
                len=this.supplements.value.visibilitySuplementLevel.length;
                let ind= toNumber((this.supplements.value.visibilitySuplementLevel).charAt(len - 1));
                if (ind>0)
                  bits[ind+10]='1';

                if (this.supplements.value.supplementVisibilityOption=='ARIA')
                  bits[18]='1';

                let selected = this.fundingAriaList.find(x=>x.description == this.supplements.value.visibilitySuplementLevel2);
                let i = this.fundingAriaList.indexOf(selected)
                if (i>-1)
                  bits[i+19]='1';
  
              }
             
            
            this.packageDetailForm.patchValue({
                packageSupplements:bits.join('')
            })  

            }

setPackageSupplements(data: any){

              let bits:any= data.split('');

              let levelSupplement:string='';
              if ( bits[1]=='1')
                levelSupplement='LEVEL 1'
              else if ( bits[2] =='1')
                levelSupplement='LEVEL 2'
              else if ( bits[3] =='1')
                levelSupplement='LEVEL 3'
              else if ( bits[4] =='1')
                levelSupplement='LEVEL 4'
            
              let feedingSupplementLevel:string='';
              if ( bits[7]=='1')
                feedingSupplementLevel='Bolus'
              else if ( bits[8]=='1')
                feedingSupplementLevel='Non-Bolus'

              let visibilitySuplementLevel:string='';
              let visibilitySuplementLevel2:string='';
              if ( bits[11]=='1')
                visibilitySuplementLevel='MMM 1'
              else if ( bits[12]=='1')
                visibilitySuplementLevel='MMM 2'
              else if ( bits[13]=='1')
                visibilitySuplementLevel='MMM 3'
              else if ( bits[14]=='1')
                visibilitySuplementLevel='MMM 4'
              else if ( bits[15]=='1')
                visibilitySuplementLevel='MMM 5'
              else if ( bits[16]=='1')
                visibilitySuplementLevel='MMM 6'
              else if ( bits[17]=='1')
                visibilitySuplementLevel='MMM 7'

              
              let supplementVisibilityOption="";
              if ( bits[18]=='1')
               supplementVisibilityOption='ARIA';
              else
                supplementVisibilityOption='MM';

                this.SupplementVisibility=supplementVisibilityOption;

              for (let i=19;i<25;i++){
                if(bits[i] == 1 ){
                  visibilitySuplementLevel2= this.fundingAriaList[i-19].description
                } 
              }

              this.supplements.patchValue({
                
                domentica:bits[0] == 1 ? true : false,
                oxygen:bits[5] == 1 ? true : false,
                feedingSuplement:bits[6] == 1 ? true : false,
                EACHD:bits[9] == 1 ? true : false,
                visibilitySuplement:bits[10] == 1 ? true : false,
                levelSupplement : levelSupplement,
                feedingSupplementLevel :feedingSupplementLevel,
                visibilitySuplementLevel:visibilitySuplementLevel,
                visibilitySuplementLevel2:visibilitySuplementLevel2,
                supplementVisibilityOption:supplementVisibilityOption

            })

            }
            getPayout(){

            }

            handleCancel() {
                this.openModel = false;   
                this.packageDetailForm.reset();
                
            }

            tabFindIndex: number = 0;
            tabFindChange(index: number){
                this.tabFindIndex = index;
                this.IS_CDC=true;
            }

            showAddModal() {
                this.packageDetailForm = this.formBuilder.group(packageDefaultForm);                
                this.addOREdit = 1;
                this.openModel = true;
            }

            domenticaChange(event: any){
                if(event.target.checked){
                    this.supplements.patchValue({
                        levelSupplement : this.programLevel,
                    });
                }else{
                    this.supplements.patchValue({
                        levelSupplement : '',
                    });
                }
            }
            packgChange(e){
            }
            recurrentChange(e){
                if(e.target.checked){
                    this.visibleRecurrent = true;
                }else{
                    this.visibleRecurrent = false;
                }
            }

            print(){
        
            }
            generatePdf(){
      
        
              const data = {
                  "template": { "_id": "6BoMc2ovxVVPExC6" },
                  "options": {
                      "reports": { "save": false },                        
                      "sql": this.tableData,                        
                      "userid": this.tocken.user,
                      "txtTitle":  "Packages",                      
                  }
              }
              this.loading = true;           
                          
              this.drawerVisible = true;
              this.printS.printControl(data).subscribe((blob: any) => {
                          this.pdfTitle = "Packages"
                          this.drawerVisible = true;                   
                          let _blob: Blob = blob;
                          let fileURL = URL.createObjectURL(_blob);
                          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                          this.loading = false;                       
                      }, err => {
                          console.log(err);
                          this.loading = false;
                          this.ModalS.error({
                              nzTitle: 'TRACCS',
                              nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                              nzOnOk: () => {
                                  this.drawerVisible = false;
                              },
                          });
              });
            }
            handleCancelTop(): void {
            this.drawerVisible = false;
            this.loading = false;
            this.pdfTitle = ""
            this.tryDoctype = ""
            }
            close(){
                this.router.navigate(['/admin/recipient/personal']);
            }
           
            
            toggleMenu(){
              this.isMenuOpen =!this.isMenuOpen ;  
            }

            log(): void {
                console.log('click dropdown button');
              }
            
              showRows:boolean;
              showId:any;
              show_Rows(item:any){
                this.showRows=!this.showRows;
                this.showId= item.id

              }
              expandSet = new Set<number>();
  onExpandChange(id: number, checked: boolean): void {
    if (checked) {
      this.expandSet.add(id);
    } else {
      this.expandSet.delete(id);
    }
  }
            
              mapOfExpandedData: { [id: string]: TreeNodeInterface[] } = {};

              collapse(array: TreeNodeInterface[], data: TreeNodeInterface, $event: boolean): void {
                if (!$event) {
                  if (data.children) {
                    data.children.forEach(d => {
                      const target = array.find(a => a.id === d.id)!;
                      target.expand = false;
                      this.collapse(array, target, false);
                    });
                  } else {
                    return;
                  }
                }
              }
            
              convertTreeToList(root: TreeNodeInterface): TreeNodeInterface[] {

                
                const stack: TreeNodeInterface[] = [];
                const array: TreeNodeInterface[] = [];
                const hashMap = {};
                stack.push({ ...root, level: 0, expand: false });
            
                while (stack.length !== 0) {
                  const node = stack.pop()!;
                  this.visitNode(node, hashMap, array);
                  if (node.children) {
                    for (let i = node.children.length - 1; i >= 0; i--) {
                      stack.push({ ...node.children[i], level: node.level! + 1, expand: false, parent: node });
                    }
                  }
                }
            
                return array;
              }
            
              visitNode(node: TreeNodeInterface, hashMap: { [key: string]: boolean }, array: TreeNodeInterface[]): void {
                if (!hashMap[node.id]) {
                  hashMap[node.id] = true;
                  array.push(node);
                }
              }
            
              
               convertToTree(flatList: any[]): TreeNode[] {
                const map = new Map<number, TreeNode>();
              
                // First, map each item to its corresponding node
                flatList.forEach(item => {
                  map.set(item.id, { ...item, children: [] });
                });
              
                // Then, connect children to their respective parent nodes
                let finallist = [];
                map.forEach(item => {
                  if (item.id !== null) {
                    const parent = map.get(item.id);
                    if (parent) {
                        var children = flatList.filter(x => x.id === item.id);
                        parent.children=children;

                      finallist.push(parent);
                    }

                  }
                });
              
                // Finally, find and return the root nodes
                return finallist; //flatList.filter(item => item.id === null).map(item => map.get(item.id)!);
              }
             
             

            }