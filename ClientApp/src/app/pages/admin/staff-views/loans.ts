import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes,PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';
import { log } from 'console';

@Component({
    styles: [`
    nz-table{
        margin-top:0px;
    }
    nz-select{
        width:100%;
    }
    nz-form-item{
        margin:0;
    }
    nz-select{
        width:100%;
    }
    a:hover{
        text-decoration:underline;
    }
    .ant-card-body {
        padding: 12px !important;
        zoom: 1;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
    }
    tbody  tr:nth-child(odd) {
    background-color: #E9F7FF;
      }
        tbody  tr:nth-child(even) {
            background-color: #fff;
        }
    `],
    templateUrl: './loans.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffLoansAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    dateFormat: string ='dd/MM/yyyy';
    loading: boolean = false;
    userdefined1: Array<any>;
    userdefined1List: Array<string>;
    userdefined2: Array<any>;
    userdefined2List: Array<any>;
    
    modalOpen: boolean = false;
    isLoading: boolean = false;
    whatView: number;
    
    inputForm: FormGroup;
    
    listArray: Array<any>;
    
    private editOrAdd: number;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    isDateEnabled = false; // Initially disabled
    notesloans: Array<any>;
    progarr: any;
    loantypesarr: any;
    typesarr: string[];
    staflones: any;
    selectedRow  : number = 0;
    activeRowData:any;
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef,
        private printS:PrintService
        ) {
            cd.detach();
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'loans')) {
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked()
            //console.log(this.user)
            if(this.user){
                this.search(this.user);
                this.buildForm();
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        toggleDatePicker() {
            if (this.isDateEnabled) {
              this.inputForm.controls['dateInstalled'].enable();
            } else {
              this.inputForm.controls['dateInstalled'].disable();
            }
        }
        buildForm() {
            this.inputForm = this.formBuilder.group({
                personID: [this.user.id],
                recordNumber: 0,
                group:['LOANITEMS'],
                type: ['', Validators.required],
                name: ['', Validators.required],
                dateInstalled:[''],
                date1:['',Validators.required],
                date2:[''],
                address1:[''],
                address2:[''],
                equipmentCode:[''],
                serialNumber:[''],
                purchaseAmount:[''],
                purchaseDate:[''],
                lockBoxLocation:[''],
                lockBoxCode:[''],
                notes:[''],
                
            });
        }
        selectedItemGroup(data:any, i:number){
            this.selectedRow=i;
            this.activeRowData=data;

            console.log(this.activeRowData);
        }
        get title() {
            const str = this.whatView == 1 ? 'Group' : 'Loan Items';
            const pro = this.editOrAdd == 1 ? 'Add' : 'Edit';
            return `${pro} ${str}`;
        }
        
        search(user: any) {
            this.cd.reattach();
            this.loading = true;
            this.typesarr = ['TEST','TESTING','UPDATE WITHOUT ERROR']
            forkJoin([
                this.timeS.getnotesloans(user.id),
                this.listS.GetAllPrograms(),
                this.listS.getloantypes(),
            ]).subscribe(data => {
                this.loading = false;
                this.selectedRow = 0;
                this.notesloans = data[0];
                this.originalTableData= data[0];
                this.progarr = data[1];
                this.loantypesarr = data[2];
                this.cd.detectChanges();
            });
            
            // this.timeS.getloannotes(user.id).subscribe(data => {
            //     this.notesloans = data;
            // });
            
        }
        
        showAddModal(view: number) {
            this.whatView = view;
            this.editOrAdd = 1;

            this.inputForm.patchValue({
                date1:new Date(),
                dateInstalled:new Date(),
            })


            this.modalOpen = true;
        }
        
        showEditModal(view: number, index: number) {
            this.inputForm.patchValue(this.notesloans[this.selectedRow]); 
            this.whatView = view;
            this.editOrAdd = 2;
            this.modalOpen = true;
        }
        
        handleCancel() {
            this.inputForm.reset();
            this.isLoading = false;
            this.modalOpen = false;
        }
        
        trackByFn(index, item) {
            return item.id;
        }
        
        success() {
            this.search(this.sharedS.getPicked());
            this.isLoading = false;
        }
        
        processBtn() {
            if (this.editOrAdd == 1) {
                this.save();
            }
            if (this.editOrAdd == 2) {
                this.edit();
            }
        }
        
        save() {
            for (const i in this.inputForm.controls) {
                this.inputForm.controls[i].markAsDirty();
                this.inputForm.controls[i].updateValueAndValidity();
            }
            
            if (!this.inputForm.valid)
            {
                this.globalS.sToast('Info', 'You Must Have an Item Id / Name');
            return;
            }
                
            
            // console.log(this.inputForm.value);
            // return;
            
            this.isLoading = true;

            this.inputForm.controls["personID"].setValue(this.user.id);
            this.inputForm.controls["group"].setValue("LOANITEMS");
            this.inputForm.controls["recordNumber"].setValue(0);



            this.timeS.postnotesloans(this.inputForm.value).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
                if (data) {
                    this.handleCancel();
                    this.success();
                    this.search(this.user);
                    this.globalS.sToast('Success', 'Data Added');
                }
            });
        }
        
        edit() {
            
            for (const i in this.inputForm.controls) {
                this.inputForm.controls[i].markAsDirty();
                this.inputForm.controls[i].updateValueAndValidity();
            }
            if (!this.inputForm.valid)
            return;
            
            this.isLoading = true;
            
            this.timeS.updatenotesloans(this.inputForm.value).pipe(
                takeUntil(this.unsubscribe))
                .subscribe(data => {
                    if (data) {
                        this.handleCancel();
                        this.success();
                        this.globalS.sToast('Success', 'Data Updated');
                    }
                })
            }
            
        delete(recordNo: any) {
            this.timeS.deletnotesloans(this.activeRowData.recordNumber)
            .subscribe(data => {
                if (data) {
                    this.handleCancel();
                    this.success();
                    this.search(this.user);
                    this.globalS.sToast('Success', 'Data Deleted');
                }
            });
        }
        handleOkTop(view: number) {
            this.generatePdf();
            this.tryDoctype = ""
            this.pdfTitle = ""
        }
        handleCancelTop(): void {
            this.drawerVisible = false;
            this.pdfTitle = "";
            this.tryDoctype="";
        }
        originalTableData: Array<any>;
        dragOrigin: Array<string> = [];

        columnDictionary = [{
            key: 'Name',
            value: 'name'
        },{
            key: 'Date Loaned',
            value: 'date1'
        },{
            key: 'Collected',
            value: 'date2'
        }];

        dragDestination = [       
            'Name',
            'Date Loaned',
            'Collected',
        ];

        flattenObj = (obj, parent = null, res = {}) => {
            for (const key of Object.keys(obj)) {
                const propName = parent ? parent + '.' + key : key;
                if (typeof obj[key] === 'object') {
                    this.flattenObj(obj[key], propName, res);
                } else {
                    res[propName] = obj[key];
                }
            }
            return res;
        }

        searchColumnDictionary(data: Array<any>, tobeSearched: string){
            let index = data.findIndex(x => x.key == tobeSearched);        
            return data[index].value;
        }

        drop(event: CdkDragDrop<string[]>) {
            if (event.previousContainer === event.container) {
                moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
            } else {
                if(!event.container.data.includes(event.item.data)){
                    copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
                }
            }
            this.generate();
        }

        generate(){
            const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));

            var convertedObj = groupArray(this.originalTableData, dragColumns);

            var flatten = this.flatten(convertedObj, [], 0);

            if(dragColumns.length == 0){
                this.notesloans = this.originalTableData;
            } else {
                this.notesloans = flatten;
            }
        }

        flatten(obj: any, res: Array<any> = [], counter = null){
            for (const key of Object.keys(obj)) {
                const propName = key;
                if(typeof propName == 'string'){                   
                    res.push({key: propName, counter: counter});
                    counter++;
                }
                if (!Array.isArray(obj[key])) {
                    this.flatten(obj[key], res, counter);
                    counter--;
                } else {
                    res.push(obj[key]);
                    counter--;
                }
            }
            return res;
        }

        removeTodo(data: any){
            this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
            this.generate();
        }

        isArray(data: any){
            return Array.isArray(data);
        }
    
        isSome(data: any){
            if(data){
                return data.some(d => 'key' in d);
            }
            return true;        
        }
        generatePdf(){
            this.drawerVisible = true;
            
            this.loading = true;
           
          
          
              var fQuery ="SELECT HumanResources.Name,format(HumanResources.Date1,'dd/MM/yyyy') as [Date Loaned], format(HumanResources.Date2,'dd/MM/yyyy') as Collected FROM HumanResources INNER JOIN Staff ON HumanResources.PersonID = Staff.UniqueID  WHERE   HumanResources.[Group] = 'LOANITEMS' and Staff.UniqueID = '"+this.user.id+"'"
           
              //console.log(fQuery);  
              var txtTitle = "Staff Loans for "+this.user.code;
              var heading1 = "Sr#";
              var heading2= "Name";
              var heading3 = "Date Loaned";
              var heading4 = "Collected";
             
                     
            
              const data = {
                "template": { "_id": '0RYYxAkMCftBE9jc' },
                "options": {
                    "reports": { "save": false },
                    "sql": fQuery,
                    "txtTitle": txtTitle,
                    "userid": this.tocken.user,
                   
                    head1: heading1,
                    head2: heading2,
                    head3: heading3,
                    head4: heading4,
                   
                                  
                }
            }
            this.loading = true;
           
                this.tryDoctype = ""; 
                this.drawerVisible = true; 
                this.loading = true;
          
            this.printS.printControl(data).subscribe((blob: any) => {
                this.pdfTitle = "loan Register.pdf"
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();
            }, err => {
                console.log(err);
                this.loading = false;
                this.ModalS.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            
            
            return;
            
        }
        getPermisson(index:number){
            var permissoons = this.globalS.getStaffRecordView();
            // console.log("staff "+ permissoons.length );
            return permissoons.charAt(index-1);
        }
        //Mufeed 15 April 2024            
        InitiatePrint(){

           
            var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid,strdate, endate; 
            
            fQuery = " SELECT RecordNumber, Name, format(Date1,'dd/MM/yyyy') As [Date Loaned], Date2 As [Collected] FROM HumanResources WHERE [PersonID] = '"+this.user.id+"' AND [GROUP] = 'LOANITEMS' ORDER BY [Name] ASC "                                        
           
            //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
            txtTitle = "Staff Loans";       
            Rptid =   "8WTerq14eNFSAnz0"  
    
            // console.log(fQuery)
            const data = {
                        
                "template": { "_id": Rptid },                                
                "options": {
                    "reports": { "save": false },                
                    "sql": fQuery,
                    "Criteria": lblcriteria,
                    "userid": this.tocken.user,
                    "txtTitle": txtTitle,
                    "Extra": this.user.code,
                                                                    
                }
            }
            this.loading = true;
            this.drawerVisible = true;         
                this.printS.printControl(data).subscribe((blob: any) => {
                this.pdfTitle = txtTitle+".pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
                });
                return;      
        }

        }