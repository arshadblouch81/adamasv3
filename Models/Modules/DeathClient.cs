
namespace Adamas.Models.Modules{
    public class DeathClient {
        public string Date { get; set; }
        public string UniqueId { get; set; }
    }
}