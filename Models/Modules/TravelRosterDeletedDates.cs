namespace Adamas.Models.Modules
{
    public class TravelRosterDeletedDates
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool? IsWhere { get; set; }

    }
}
