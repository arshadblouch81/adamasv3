namespace Adamas.Models.Modules{
    public class DateTimeObject {
        public string DateStr { get; set; }
        public string StartTime { get; set; }
        public int Duration { get; set; }
       
    }
}