﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class RemindersDto
    {
        public int RecordNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Creator { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string PersonID { get; set; }
        public bool? Recurring { get; set; }
        public bool? SameDay { get; set; }
        public bool? SameDate { get; set; }
        public string State { get; set; }
        public string ReminderCreator { get; set; }
        public string User2 { get; set; }
        public  List<String> selectedReminders {get;set;}

    }
}
