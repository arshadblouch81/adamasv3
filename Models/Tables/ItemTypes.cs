using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class ItemTypes
    {
        [Key]
        public int Recnum { get; set; }        
        public int? HACCId { get; set; }        
        public string Title { get; set; }
        public string BillText { get; set; }
        public string MainGroup { get; set; }
        public string MinorGroup { get; set; }
        public string RosterGroup { get; set; }
        public string ProcessClassification { get; set; }
        public string Status { get; set; }
        public double? Amount { get; set; }
        public string MinChargeRate { get; set; }
        public string Lifecycle { get; set; }
        public string Unit { get; set; }
        [Column("DataSet")]
        public string dataSet { get; set; }
        public string BudgetGroup { get; set; }
        public bool? AutoApprove { get; set; }
        public bool? ExcludeFromAutoLeave { get; set; }
        public bool? InfoOnly { get; set; }
        public bool OnSpecial { get; set; }
        public bool Discountable { get; set; }
        public string? AccountingIdentifier { get; set; }
        public string GLRevenue { get; set; }
        public string Job { get; set; }
        public string GLCost { get; set; }
        public string UnitCostUOM { get; set; }
        public double? UnitCost { get; set; }
        public bool? ExcludeFromPayExport { get; set; }
        public bool? ExcludeFromUsageStatements { get; set; }
        public double? Price2 { get; set; }
        public double? Price3 { get; set; }
        public double? Price4 { get; set; }
        public double? Price5 { get; set; }
        public double? Price6 { get; set; }
        public bool? excludeFromConflicts { get; set; }
        public bool? ExcludeFromRequote { get; set; }
        public bool? noMonday { get; set; }
        public bool? noTuesday { get; set; }
        public bool? noWednesday { get; set; }
        public bool? noThursday { get; set; }
        public bool? noFriday { get; set; }
        public bool? noSaturday { get; set; }
        public bool? noSunday { get; set; }
        public bool? noPubHol { get; set; }
        public string StartTimeLimit { get; set; }
        public string EndTimeLimit { get; set; }
        public int? DEXID { get; set; }
        public int? MaxDurtn { get; set; }
        public int? MinDurtn { get; set; }
        public int? FixedTime { get; set; }
        public bool? NoChangeDate { get; set; }
        public bool? NoChangeTime { get; set; }
        public int? TimeChangeLimit { get; set; }
        public bool AutoRecipientDetails { get; set; }
        public string DefaultAddress { get; set; }
        public string DefaultPhone { get; set; }
        public bool AutoActivityNotes { get; set; }
        public bool? JobSheetPrompt { get; set; }
        public string ActivityNotes { get; set; }
        [Column("IT_DATASET")]
        public string IT_Dataset { get; set; }
        [Column("DatasetGroup")]
        public string datasetGroup { get; set; }
        public string HACCType { get; set; }
        public string NDIA_ID { get; set; }
        public string NDIAClaimType { get; set; }
        public string NDIAPriceType { get; set; }
        public int? NDIATravel { get; set; }
        public string NDIA_LEVEL2 { get; set; }
        public string NDIA_LEVEL3 { get; set; }
        public string NDIA_LEVEL4 { get; set; }
        public string ALT_NDIANonLabTravelKmActivity { get; set; }
        public string ALT_AppKmWithinActivity { get; set; }
        public bool HACCUse { get; set; }
        public bool CSTDAUse { get; set; }
        public bool NRCPUse { get; set; }
        public bool ExcludeFromRecipSummarySheet { get; set; }
        public bool? ExcludeFromHigherPayCalculation { get; set; }
        public bool ExcludeFromMinHoursCalculation { get; set; }
        public bool ExcludeFromBrokenShift { get; set; }
        public bool? ExcludeFromAppLogging { get; set; }
        public bool? ExcludeFromTravelCalcKM { get; set; }
        public bool? DIsableAppLogin { get; set; }
        public bool? MedicareClaimable { get; set; }
        public bool? ForceTravelClaim { get; set; }
        public bool? ForceShiftReport { get; set; }
        public bool? NoOvertimeAccumulation { get; set; }
        public bool? PayAsRostered { get; set; }
        public bool? ExcludeFromTimebands { get; set; }
        public bool? ExcludeFromInterpretation { get; set; }
        public bool? CDCFee { get; set; }
        public string JobType { get; set; }
        public int TA_LOGINMODE { get; set; }
        public bool? ExcludeFromClientPortalDisplay { get; set; }
        public bool? ExcludeFromClientRoster { get; set; }
        public bool? ExcludeFromTravelCalc { get; set; }
        public bool? TA_EXCLUDEGEOLOCATION { get; set; }
        public bool? TA_EXCLUDEFROMAPPALERTS { get; set; }
        public bool? DisplayFeeInApp { get; set; }
        public bool? DisplayDebtorInApp { get; set; }
        public bool? RetainOriginalDataset { get; set; }
        public bool? AppExclude1 { get; set; }
        public bool? TAExclude1 { get; set; }
        public bool? TAEarlyStartTHEmail { get; set; }
        public bool? TALateStartTHEmail { get; set; }
        public string TALateStartTH { get; set; }
        public string TAEarlyStartTH { get; set; }
        public string TAEarlyStartTHWho { get; set; }
        public string TALateStartTHWho { get; set; }
        public string TANoGoResend { get; set; }
        public string TANoShowResend { get; set; }
        public bool? TAEarlyFinishTHEmail { get; set; }
        public bool? TALateFinishTHEmail { get; set; }
        public string TAEarlyFinishTH { get; set; }
        public string TALateFinishTH { get; set; }
        public string TALateFinishTHWho { get; set; }
        public string TAEarlyFinishTHWho { get; set; }
        public bool? TANoWorkTHEmail { get; set; }
        public bool? TAUnderstayTHEmail { get; set; }
        public bool? TAOverstayTHEmail { get; set; }
        public string TANoWorkTHWho { get; set; }
        public string TAOverstayTHWho { get; set; }
        public string TAUnderstayTHWho { get; set; }
        public string TANoWorkTH { get; set; }
        public string TAUnderstayTH { get; set; }
        public string TAOverstayTH { get; set; }
        public bool? TANOGOTHEMAIL { get; set; }
        public DateTime? EndDate { get; set; }
        public bool DeletedRecord { get; set; }
        public string ExtPayID { get; set; }
        public bool? BreaksTravel { get; set; }
        public bool? ExcludeNoMinBreak { get; set; }
    }
}
