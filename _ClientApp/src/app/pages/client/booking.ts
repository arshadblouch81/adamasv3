
import { forkJoin, Subject, Observable, EMPTY } from 'rxjs';

import { takeUntil, debounceTime, distinctUntilChanged, switchMap, mergeMap } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { ClientService, GlobalService, StaffService, TimeSheetService } from '@services/index';

import * as moment from 'moment'

import { ClrWizard } from '@clr/angular';
import { BsModalComponent } from 'ng2-bs3-modal';
import { InputParams } from '@shared/index';

const enum ImagePosition {
    LaundryService = '-81px -275px',
    PersonalCare = "-247px -276px ",
    CaseManagement = "-164px -181px",
    StaffTravel = "1px -21px",
    Transport = "1px -264px",
    Unavailable = "-50px 0px",
    SocialHelp = "-247px -365px"
}

const enum ImageActivity {
    Laundry = 'DA LAUNDRY PRV',
    Personal = 'PERSONAL CARE PKGE',
    Case = 'CASE MANAGEMENT PKGE',
    StaffTravel = 'STAFF TRAVEL',
    Transport = 'TRANSPORT',
    Unavailable = 'UNAVAILABLE',
    SocialHelp = 'SOCIAL HELP PRIV'
}

@Component({
    selector: 'booking-client',
    templateUrl: './booking.html',
    styles: [
        `
        h3{
            width: 100%;
            margin: 12px 0;
        }
        .booking-wrapper{
            padding: 5px;
            border: 1px solid #f5f5f5;
            background: #fff;
            border-radius: 3px;
            box-shadow: -1px 2px 6px 0px rgb(245, 245, 245);
            display:inline-block;
        }
        .book-wrapper{
            min-width: 6rem;
            height: 9rem;
            border-radius: 10px;
            margin: 0 0 40px 0;
            padding: 11px;
            background: #ffffff;
            box-shadow: 0 8px 20px -7px #dcdcdc;
            position:relative;
        }
        .book-wrapper p:not(.price){
            margin:0;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
            font-size: 12px;
        }
        .book-wrapper i{
            background: #4ac100;
            border-radius: 3px;
            color: #fff;
            border: 0;
            position: absolute;
            right: 29px;
            bottom: 4px;
        }
        .book-wrapper i:hover{
            background:#58ce0f;
            cursor:pointer;
        }
        .book-image{
            background: url(assets/medical-icons.png);
            background-repeat: no-repeat;
            background-size: 12rem;
            margin: 0 auto 14px auto;
            width: 50px;
            height: 50px;
        }
        .alert{
            padding:0;
        }
        article.staff{
            position: absolute;
            bottom: 10px;
            width: 100%;
        }
        .age{
            color: #4ac100;
            float:right;
            margin-right: 1rem;
        }
        .price{
            font-size: 18px;
            color: #3690ff;
            font-weight: bold;
        }
        .pick{
            background: linear-gradient(#7ac4da,#86dcff);
        }
        .deselect{
            background:#ff3939 !important;
        }
        clr-checkbox-wrapper label{
            font-size: 10px !important;
        }
        .competencies{
            margin:10px 0;
        }
        .competencies > span{
            padding: 5px;
            margin: 5px;
            background: #e4e4e4;
            border-radius: 5px;
            color: #6f6f6f;
        }
        .total-container{
            background-color: #f9f9f9;
            padding: 15px 0;
            border: 1px solid #f1f1f1;
        }
        .total-container > div:nth-child(2) div{
            border-bottom: 1px solid #657d7c;     
        }
        .total{
            font-size: 20px;
            font-weight: 500;
            margin-top: 5px;
            color: #29d82e;
        }
        textarea{
            height:3rem;
        }
        .__date{
            font-size: 20px;
            margin-bottom: 15px;
            color:#000;
        }
        h6 span{
            cursor:default;
        }
        .serviceType {
            color: #3497ad;
            font-weight: 500;
        }
        `
    ]
})

export class BookingClient implements OnInit, OnDestroy {

    @Input() inputUser: any;
    @ViewChild('Booking', { static: false }) Booking: ClrWizard;

    selectedOption: any;

    bookingModalOpen: boolean = false;

    public abliha: boolean = true;
    private unsubscribe$ = new Subject()

    serviceLoad: boolean = false;


    bookStream = new Subject<any>();

    bookingType: string;
    bookType: boolean = false;

    user: any;
    token: any;

    startTime: any;
    endTime: any;
    date: any;
    notes: any;


    aprovider: boolean = true;
    cprovider: boolean = false;

    selectedService: any;
    selectedStaff: any;

    once: boolean = true;
    permanent: boolean = false;
    weekly: string = 'Weekly';
    fortnightly: boolean;

    services: Array<any>;
    staffs: Array<any> = [];
    competencies: Array<any> = [];

    canChooseProvider: boolean = false;

    selectedInputParams: InputParams;

    constructor(
        private clientS: ClientService,
        private globalS: GlobalService,
        private staffS: StaffService,
        private timeS: TimeSheetService
    ) { }

    ngOnInit() {
        this.user = this.inputUser || this.globalS.userProfile['accountNo'];
        this.token = this.globalS.decode();

        this.timeS.getuname(this.user).pipe(
            takeUntil(this.unsubscribe$),
            mergeMap(res => {
                return this.staffS.getsettings(res.uname)
            })
        ).subscribe(settings => {

            const leadTime = parseInt(settings.bookingLeadTime);
            this.date = moment().add(leadTime, 'day');
            this.bookStream.next(this.date);

            this.canChooseProvider = (settings.canChooseProvider).toString() == "True" ? true : false;
        })

        // this.staffS.getsettings(this.globalS.decode().nameid).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        //     const leadTime = parseInt(data.bookingLeadTime);
        //     this.date = moment().add(leadTime,'day');
        //     this.bookStream.next(this.date);
        // });

        this.startTime = '9:00';
        this.endTime = '11:00';
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    result(data: any) {
        this.serviceLoad = false;
        this.services = data[1]
        this.staffs = data[0];
    }

    getApprovedServices(service: Dto.ApproveService) {
        return this.clientS.getapprovedservices({
            RecipientCode: service.RecipientCode,
            BookDate: service.BookDate,
            StartTime: service.StartTime
        })
    }

    getCompetencies() {
        this.clientS.getcompetencies(this.user).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.competencies = [];
            this.competencies = data;
        });
    }

    sTimeChange(data: any) {
        console.log(moment(data).format('HH:mm'));
    }

    getPositionImg(data: any): string {
        let temp = data.serviceType || "";
        if (temp.indexOf(ImageActivity.Laundry) !== -1) return ImagePosition.LaundryService;
        if (temp.indexOf(ImageActivity.Unavailable) !== -1) return ImagePosition.Unavailable;
        if (temp.indexOf(ImageActivity.Transport) !== -1) return ImagePosition.Transport;
        if (temp.indexOf(ImageActivity.StaffTravel) !== -1) return ImagePosition.StaffTravel;
        if (temp.indexOf(ImageActivity.Personal) !== -1) return ImagePosition.PersonalCare;
        if (temp.indexOf(ImageActivity.Case) !== -1) return ImagePosition.CaseManagement;
        if (temp.indexOf(ImageActivity.SocialHelp) !== -1) return ImagePosition.SocialHelp;
    }

    change(data: any, source: string) {
        if (source == 'aprov') {
            if (data)
                this.cprovider = false;
        }

        if (source == 'cprov') {
            if (data)
                this.aprovider = false
        }

        if (source == 'once') {
            if (data)
                this.permanent = false;
            else
                this.permanent = true;
        }

        if (source == 'permanent') {
            if (data) {
                this.once = false;
                this.weekly = 'Weekly'
            }
            else {
                this.once = true;
            }

        }
    }

    computeDuration(start: any, end: any) {
        return this.globalS.computeTime(start, end);
    }

    ifShow(data: any) {
        var res = data.competenciesList.filter(x => x.selected == true);
        console.log(res);
        if (res.length > 0)
            return true;
        return false;
    }


    add(data: any) {
        if (Object.is(data, this.selectedService)) {
            this.selectedService = "";
            return false;
        }
        this.selectedService = data;
    }

    isPicked(data: any) {
        return Object.is(data, this.selectedService);
    }

    isPickedStaff(data: any) {
        return Object.is(data, this.selectedStaff);
    }


    loadServices: boolean = false;

    customClick(buttonType: string, level: number = 0): void {

        if ("book" === buttonType) {
            this.bookingModalOpen = true;
        }

        if ("cancel" === buttonType) {
            this.reset()
        }

        if ("next" === buttonType && level === 0) {
            this.Booking.next();
        }

        if ("next" === buttonType && level === 1) {

            const timeDiff = moment.duration(moment(this.endTime, 'HH:mm').diff(moment(this.startTime, 'HH:mm'))).asMinutes();
            if (timeDiff <= 0) {
                this.globalS.eToast('Error', 'Invalid Time Difference');
                return;
            }

            this.loadServices = true;

            if (this.cprovider) {
                this.selectedInputParams = {
                    RecipientCode: this.user,
                    User: this.token['nameid'],
                    BookDate: moment(this.date).format('YYYY/MM/DD'),
                    StartTime: this.startTime,
                    EndTime: this.endTime,
                    EndLimit: '17:00',
                    Gender: '',
                    Competencys: '',
                    CompetenciesCount: 0
                }
            }

            this.getApprovedServices({
                RecipientCode: this.user,
                BookDate: moment(this.date).format('YYYY/MM/DD'),
                StartTime: this.startTime
            }).subscribe(data => {
                this.services = data.list
                this.loadServices = false;
            });

            this.Booking.next();
        }

        if ("back" === buttonType) {
            this.Booking.previous();
        }
    }

    reset() {
        this.Booking.reset();
        this.selectedService = null;
        this.selectedStaff = null;
        this.notes = null;
        this.selectedInputParams = null;
    }

    book() {

        let booking: Dto.AddBooking = {
            BookType: this.bookType,
            StaffCode: !(this.aprovider) ? this.selectedStaff.accountNo : "",
            Service: this.selectedService,
            StartDate: moment(this.date).format('YYYY/MM/DD'),
            StartTime: this.startTime,
            ClientCode: this.user,
            Duration: this.endTime,
            Username: this.token['nameid'],
            AnyProvider: this.aprovider,
            BookingType: this.once ? 'Normal' : this.permanent ? this.weekly : '',
            Notes: this.notes
        }

        console.log(booking);

        // this.clientS.addbooking(booking).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        //     let resultRows = parseInt(data);
        //     if (resultRows == 1) {
        //         this.globalS.sToast('Success', 'Service Booked');
        //     } else if (resultRows > 1)
        //         this.globalS.eToast('Error', 'You already have a booking in that timeslot');

        //     this.reset();
        // }, (err) => {
        //     this.globalS.eToast('Error', 'Booking Unsuccessful')
        // });
        
    }

    raze(data: any): string {
        if (data !== undefined) {
            return 'http://45.77.37.207/assets/letters/' + data.firstName.charAt(0) + '.png';
        }
        return "";
    }
}
