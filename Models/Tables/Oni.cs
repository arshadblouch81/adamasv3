using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables
{
    public class Oni   {
        [Key]
        public int RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string HACCDVACardHolderStatus { get; set; }        
        public string FP1_Housework { get; set; }
        public string FP2_WalkingDistance { get; set; }
        public string FP3_Shopping { get; set; }
        public string FP4_Medicine { get; set; }
        public string FP5_Money { get; set; }
        public string FP6_Walking { get; set; }
        public string FP7_Bathing { get; set; }
        public string FP8_Memory { get; set; }
        public string FP9_Behaviour { get; set; }
        public string FPA_Communication { get; set; }
        public string FPA_Dressing { get; set; }
        public string FPA_Eating { get; set; }
        public string FPA_Toileting { get; set; }
        public string FPA_GetUp { get; set; }
        public string C_General_NeedForCarer { get; set; }
        public string C_Issues_Sustainability { get; set; }        
      
        public string C_Support_Help { get; set; }
        public string C_Support_Allowance { get; set; }
        public string C_Support_Information { get; set; }
        public string C_Support_NeedTraining { get; set; }
        public bool? C_Threats_Emotional { get; set; }
        public bool? C_Threats_Physical { get; set; }
        public bool? C_Threats_Physical_Slow { get; set; }
        public bool? C_Threats_Unrelated { get; set; }
        public bool? C_Threats_ConsumerNeeds { get; set; }
        public bool? C_Threats_ConsumerOther { get; set; }
        public string C_Issues_Comments { get; set; }

    public double? FP_Score { get; set; }
    public int? FA_Domestic { get; set; }
    public int? FA_SelfCare { get; set; }
    public int? FA_Cognition { get; set; }
    public int? FA_Behaviour { get; set; }
    public int? Aids_SelfCare { get; set; }
    public int? Aids_MedicalCare { get; set; }
    public int? Aids_SupportAndMobility { get; set; }
    public int? Aids_CarModifications { get; set; }
    public int? Aids_CommunicationAids { get; set; }
    public int? Aids_Other { get; set; }
    public string AidsOtherList { get; set; }
    public int? Aids_Reading { get; set; }
    public string FP_Comments { get; set; }
    public string LAP_LivingComments { get; set; }
    public string LAP_AccomodationComments { get; set; }
    public string LAP_Employment { get; set; }
    public string LAP_EmploymentComments { get; set; }
    public string LAP_MentalHealth { get; set; }
    public string LAP_Decision { get; set; }
    public string LAP_DecisionCapable { get; set; }
    public string LAP_FinancialDecision { get; set; }
    public string LAP_LivingCostDecision { get; set; }
    public string LAP_LivingCostDecisionComments { get; set; }
    public string HC_Overall_General { get; set; }
    public string HC_Overall_Pain { get; set; }
    public string HC_Overall_Interfere { get; set; }
    public string HC_Vision_Reading { get; set; }
    public string HC_Vision_Long { get; set; }
    public string HC_Hearing { get; set; }
    public string HC_Oral { get; set; }
    public string HC_OralComments { get; set; }
    public string HC_Speech { get; set; }
    public string HC_SpeechComments { get; set; }
    public string HC_Falls { get; set; }
    public int? HC_FallsNumber { get; set; }
    public string HC_Feet { get; set; }
    public string HC_FeetComments { get; set; }
    public int? HC_Vac_Influenza { get; set; }
    public DateTime? HC_Vac_Influenza_Date { get; set; }
    public int? HC_Vac_Pneumo { get; set; }
    public DateTime? HC_Vac_Pneumo_Date { get; set; }
    public int? HC_Vac_Tetanus { get; set; }
    public DateTime? HC_Vac_Tetanus_Date { get; set; }
    public int? HC_Vac_Other { get; set; }
    public DateTime? HC_Vac_Other_Date { get; set; }
    public string HC_Driving { get; set; }
    public string HC_FitToDrive { get; set; }
    public string HC_DrivingComments { get; set; }
    public string HC_Continence_Urine { get; set; }
    public string HC_Continence_Urine_Sneeze { get; set; }
    public string HC_Continence_Faecal { get; set; }
    public string HC_ContinenceComments { get; set; }
    public int? HC_HeightWeightApplicable { get; set; }
    public double? HC_Height { get; set; }
    public double? HC_Weight { get; set; }
    public double? HC_BMI { get; set; }
    public int? HC_BPPulseApplicable { get; set; }
    public double? HC_BP_Systolic { get; set; }
    public double? HC_BP_Diastolic { get; set; }
    public string HC_Pulse { get; set; }
    public double? HC_PulseRate { get; set; }
    public string HC_PHCheck { get; set; }
    public string HC_Med_TakeOwn { get; set; }
    public string HC_Med_Willing { get; set; }
    public string HC_Med_Coop { get; set; }
    public string HC_Med_Webster { get; set; }
    public string HC_Med_Review { get; set; }
    public string HC_MedComments { get; set; }
    public string PP_K10_1 { get; set; }
    public string PP_K10_2 { get; set; }
    public string PP_K10_3 { get; set; }
    public string PP_K10_4 { get; set; }
    public string PP_K10_5 { get; set; }
    public string PP_K10_6 { get; set; }
    public string PP_K10_7 { get; set; }
    public string PP_K10_8 { get; set; }
    public string PP_K10_9 { get; set; }
    public string PP_K10_10 { get; set; }
    public double? PP_K10_Score { get; set; }
    public string PP_SleepingDifficulty { get; set; }
    public string PP_SleepingDifficultyComments { get; set; }
    public string PP_PersonalSupport { get; set; }
    public string PP_PersonalSupportComments { get; set; }
    public string PP_Relationships_KeepUp { get; set; }
    public string PP_Relationships_Problem { get; set; }
    public string PP_Relationships_SP { get; set; }
    public string PP_RelationshipsComments { get; set; }
    public string HBP_HealthChecks { get; set; }
    public string HBP_HealthChecks_Last { get; set; }
    public string HBP_HealthChecks_List { get; set; }
    public string HBP_Smoking { get; set; }
    public string HBP_Smoking_Quit { get; set; }
    public string HBP_Alchohol { get; set; }
    public double? HBP_Alchohol_NoDrinks { get; set; }
    public string HBP_Alchohol_BingeNo { get; set; }
    public string HBP_Malnutrition_LostWeight { get; set; }
    public string HBP_Malnutrition_LostWeightAmount { get; set; }
    public string HBP_Malnutrition_PoorEating { get; set; }
    public double? HBP_Malnutrition_Score { get; set; }
    public string HBP_Hydration_AdequateFluid { get; set; }
    public string HBP_Hydration_DecreasedFluid { get; set; }
    public string HBP_Weight { get; set; }
    public string HBP_PhysicalActivity { get; set; }
    public string HBP_PhysicalFitness { get; set; }
    public string HBP_Comments { get; set; }
    public string HC_FallsComments { get; set; }
    public double? HC_Med_TakeOwn_Score { get; set; }
    public double? HC_Med_Willing_Score { get; set; }
    public double? HC_Med_Coop_Score { get; set; }
   
    public double? RatingToolScore { get; set; }
    public string PP_Relationships_SPComments { get; set; }
    
    // public string FPA_Mobility { get; set; }
    // public string FPA_BladderManagement { get; set; }
    // public string FPA_BowelManagement { get; set; }
    
    // public string FPA_Transport { get; set; }
    // public string FPA_Transfer { get; set; }
    // public string FPA_Behaviour { get; set; }
    // public string FPA_SocialInteraction { get; set; }

    public string HelpToCommunicate { get; set; }
    public bool? HC_Overall_NotApplicable { get; set; }
    public bool? HC_Oral_NotApplicable { get; set; }
    public bool? HC_Speech_NotApplicable { get; set; }
    public bool? HC_Falls_NotApplicable { get; set; }
    public bool? HC_Feet_NotApplicable { get; set; }
    public bool? HC_Vaccinations_NotApplicable { get; set; }
    public bool? HC_Driving_NotApplicable { get; set; }
    public bool? HC_Continence_NotApplicable { get; set; }
    public string HC_Continence_Urine_Frequency { get; set; }
    public int? HC_Continence_Urine_Score { get; set; }
    public string CAL_ArrivalYear { get; set; }
    public string CAL_Citizenship { get; set; }
    public string CAL_ReasonsMoveAustralia { get; set; }
    public string CAL_PrimSecLanguage { get; set; }
    public string CAL_EnglishProf { get; set; }
    public string CAL_PrimaryLiteracy { get; set; }
    public string CAL_EnglishLiteracy { get; set; }
    public string CAL_NonVerbalStyle { get; set; }
    public string CAL_Marital { get; set; }
    public string CAL_Religion { get; set; }
    public string CAL_EmploymentHistory { get; set; }
    public string CAL_EmploymentHistoryAust { get; set; }
    public string CAL_DietaryNeeds { get; set; }
    public string CAL_SpecificCulturalNeeds { get; set; }
    public string CAL_IdentifiesAs { get; set; }
    public string CALSocIsol_1 { get; set; }
    public string CALSocIsol_2 { get; set; }
    public string CALSocIsol_3 { get; set; }
    public string CALSocIsol_4 { get; set; }
    public string CALSocIsol_5 { get; set; }
    public string CALSocIsol_6 { get; set; }
    public string CALSocIsol_7 { get; set; }
    public string CALSocIsol_8 { get; set; }
    public string CALSocIsol_9 { get; set; }
    public string CALSocIsol_10 { get; set; }
    public string CALSocIsol_11 { get; set; }
    public string CSTDA_PrimaryDisabilityDescription { get; set; }
    public bool? xDeletedRecord { get; set; }
    public DateTime? xEndDate { get; set; }
    public string HomeBushFireRisk { get; set; }
    public string HomeEasyToFind { get; set; }
    public string HomeAdequateRoadParking { get; set; }
    public string HomeGoodRoadAccess { get; set; }
    public string HomeVisibleFromStreet { get; set; }
    public string HomeUnbstructedGates { get; set; }
    public string HomeEasyIdentifiablePoint { get; set; }
    public string HomeWellMaintainedGardens { get; set; }
    public string HomeLevelPathways { get; set; }
    public string HomeLevelStairs { get; set; }
    public string HomeLevelVerandaSurfaces { get; set; }
    public string HomeLevelDriveway { get; set; }
    public string HomeExteriorlighting { get; set; }
    public string HomeExitsAreClear { get; set; }
    public string HomeLighting { get; set; }
    public string HomeEmergencyExits { get; set; }
    public string HomeSmokeDetectors { get; set; }
    public string HomeFireExtinguishers { get; set; }
    public string HomePowerPoints { get; set; }
    public string HomeElectricalLeads { get; set; }
    public string HomeHeatersAirConditioners { get; set; }
    public string HomeSafetySwitch { get; set; }
    public string HomeGasMeter { get; set; }
    public string HomeElectricityMeter { get; set; }
    public string HomeWaterMeter { get; set; }
    public string HomeNoHeavyFurniture { get; set; }
    public string HomeNoLooseRugs { get; set; }
    public string HomeNotMonitoredPets { get; set; }
    public string HomeNoTobacco { get; set; }
    public string HomeNoWeapons { get; set; }
    public string HomeUnobstructedSurfaces { get; set; }
    public string HomeSelfMonitoredFridge { get; set; }
    public string HomeSelfMonitoredCupboards { get; set; }
    public string HomeFoodlabels { get; set; }
    public string HomeCookingUtensils { get; set; }
    public string HomeCrockeryCutlery { get; set; }
    public string HomeCleaningCloths { get; set; }
    public string HomeStoveMicrowaves { get; set; }
    public string HomeMobilityAids { get; set; }
    public string HomeAssistiveEquipment { get; set; }
    public string HomeHandrailsModifications { get; set; }
    public string HomeShowerCubicle { get; set; }
    public string HomeToilet { get; set; }
    public string HomeVacuumCleaner { get; set; }
    public string HomeWashingMachine { get; set; }
    public string HomeLongHandledBroom { get; set; }
    public string HomeClothesline { get; set; }
      
   
    }
}