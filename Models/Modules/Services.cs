
namespace Adamas.Models.Modules{
    public class Services {
        public string ServiceName { get; set; }
        public string ServiceProgram { get; set; }
        public string ServiceType { get; set; }
        public string ServiceBillRate { get; set; }
        public string ServiceUnit { get; set; }
        public string ServiceImage { get; set; }
        public bool? ExcludeFromClientPortal { get; set; }
        public int? Recnum { get; set; }
    }
}