namespace Adamas.Models.Modules
{
    public class RemoteDownloadDirectory
    {
        public bool IsRemote { get; set; }
        public string OriginPath { get; set; }
        public string DestinationPath { get; set; }
    }
}