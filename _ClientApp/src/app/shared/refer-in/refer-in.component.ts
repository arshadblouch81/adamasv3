import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators } from '@angular/forms'

import { Subject } from 'rxjs';
import {mergeMap,takeUntil,debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import * as moment from 'moment';
import { ClrWizard } from "@clr/angular";
import {  SqlWizardService } from '@services/sqlwizard.service'
import  { TimeSheetService, GlobalService, ClientService, ListService, statuses, contactGroups, recurringInt, recurringStr } from '@services/index';

const NOTE_TYPE: Array<string> = ['CASENOTE','OPNOTE','CLINNOTE'];

@Component({
  selector: 'refer-in',
  templateUrl: './refer-in.component.html',
  styleUrls: ['./refer-in.component.css']
})
  
export class ReferInComponent implements OnInit, OnChanges, OnDestroy {
  private unsubscribe$ = new Subject()

  @Input() open: boolean = false;

  @Input() user: any;
  @Input() referTab: number = 1;
  
  programTicked: boolean = false;
  referralVariables: any;  
  checkedPrograms: Array<any> = []
  noteArray: Array<string> = []
  NDIA_HCP: any

  noteNumber: number;

  @ViewChild("wizard", {static: false}) wizard: ClrWizard;

  whatOptionVar: Dto.ModalVariables;
  packageCodeChanges = new Subject<string>();

  referralGroup: FormGroup;

  noteTemplate: string = '';

  NDIA_STATIC: any = {

  }

  programs: Array<any> = [];

  referralType: number;
  

  constructor(
    private listS: ListService,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private sqlWizS: SqlWizardService
  ) { 
    this.resetReferral();

    this.packageCodeChanges.pipe(
          debounceTime(300),
          distinctUntilChanged()
        ).subscribe(data => {
        this.checkPackageCodeIfExist(data)
    });
  }

    ngOnInit() {
      // this.referralGroup.get('notes').valueChanges.pipe(debounceTime(200)).subscribe(data => {
      //   if(data && data.length > 0 && this.showThisOnTab_A()){
            
      //       this.referralGroup.get('noteCategory').enable();
      //       this.noteChangeEvent(this.noteNumber)
      //       return;
      //   }

      //   if(this.whatOptionVar){
      //       this.referralGroup.get('noteCategory').disable();
      //       this.referralGroup.patchValue({
      //           noteCategory: this.whatOptionVar.wizardTitle
      //       })
      //   }

      // })
    }

    ngOnChanges(changes: SimpleChanges){
      for(let property in changes){
          if(property == 'open' && !changes[property].firstChange && changes[property].currentValue != null){
              this.wizard.reset();
              this.resetReferral();
              //this.populate('Referral Registration Wizard',this.user,'REFERRAL-IN');
              this.open = true;
          }
      }
  }
  
  ngOnDestroy(): void{
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  resetReferral(){
      const currDate = moment();
      const time = moment(currDate).format('HH:mm');

      this.referralGroup = this.formBuilder.group({
          date: currDate,    
          notes:'',
          noteType: 'CASENOTE',
          noteCategory: '',    
          time: time,
          timeSpent: '00:15',
          publishToApp: false,     
          referralSource: '',
          referralCode: '',
          referralType: '',
          reminderDate: '',
          reminderTo: '',
          notifTo: '',

          quantity: '',
          quantityTitle: '',
          charge: '',
          gst: '',
          suppRefNo: '',
          suppDate: '',
          dateOfDeath: ''
      });   
  }

  mutateToCheckboxes(list: Array<any>): Array<any> {
    return list.map(x => {
      return {
        program: x,
        status: false
      }
    })
  }

  populate(title: string, user: any, wizardTitle: string){
    this.noteNumber = 0;
    this.listS.getwizardprograms(user.id).pipe(takeUntil(this.unsubscribe$))
        .subscribe(data => {
            var programsList = data.map(x => {
              return {
                  program: x.name,
                  type: x.type,
                  status: false,
                  disabled: false
              }
          });
          
          this.whatOptionVar = {
              title: title,
              wizardTitle: wizardTitle,
              programsArr: programsList
          }

          this.referralGroup.get('noteCategory').disable();
          this.referralGroup.patchValue({
              noteCategory: this.whatOptionVar.wizardTitle
          })
        })

       
      
  }

  programCheckedChange(data: any) {
    if (this.referralType == 1 || this.referralType == 2) {
      const input = {
        AccountNo: this.user.code,
        Template: data,
        Type: this.referralType
      }

      this.listS.getmarkreferralid(input).subscribe(data => {
        this.packageCode = data.result;
      });
    }

    this.programTicked = true;
  }

  changeReferralType(data: any) {
    this.programs = [];    
    if (data == 1) {
      this.listS.getndiaprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {        
        this.programs = this.mutateToCheckboxes(data)
      })
    }
    if (data == 2) {
      this.listS.gethcpprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.programs = this.mutateToCheckboxes(data)
      })
    }
    if (data == 3) {
      // this.listS.getdexprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
      //   this.programs = this.mutateToCheckboxes(data)
      // })
    }
    if (data == 4) {
      // this.listS.getotherprograms().pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
      //   this.programs = this.mutateToCheckboxes(data)
      // })
    }
    
  }

  error: boolean = false;
  
  doCustomClick(buttonType: string): void {
    if ("custom-next" === buttonType) {
        this.checkPackageCodeIfExistObs(this.packageCode).subscribe(data => {
          if(data[0].Count == 0){
            this.getOtherDetails();
            this.wizard.next();
          } else {
            this.error = true;
          }
        })
        
        //this.wizard.next();
    }
  }

  getOtherDetails() {
    if (this.referralType == 1 || this.referralType == 2) {
      console.log('okay');
      return;
    }

    this.checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status).map((x, index, arr) => {
        return {
            program: x.program,
            index: index,
            tab: this.referTab,
            checkedProgram: arr,
            selected: ''
        }
    });

    this.getReferralList(this.checkedPrograms);
  }

  NEW_REFERRAL_SOURCE() {
    
  }

  NEW_REFERRAL_CODE() {
    
  }

  searchReferralSource(dataArr: Array<any>, searchStr: string){
    return dataArr.findIndex(x => (x.Description).toUpperCase() === searchStr.toUpperCase());
  }

  hasPropertyRecordNumber(data: Object | any){
    return (data && data.hasOwnProperty('RecordNumber')) ? data.RecordNumber : '';
  }

  patchIfHCPNDIAChecked(data: any): void{

    const checked = this.isHCPNDIAChecked();

    if(checked.length > 0){         
      this.referralGroup.patchValue({
        referralSource: checked[0].program  === '*HCP REFERRAL' ? this.hasPropertyRecordNumber(data[this.searchReferralSource(data,'My Aged Care Gateway')]) 
          : this.hasPropertyRecordNumber(data[this.searchReferralSource(data,'NDIA')])
      });
    }
  }

  getReferralList(programs: Array<any>): void{

      this.referralVariables = {
          referralSourceArr: [],
          referralCodeArr: [],
          referralTypeArr: []
      }

      if(this.referTab == 9){
          this.sqlWizS.GetReferralType(programs[0]).pipe(takeUntil(this.unsubscribe$)).subscribe(data => this.referralVariables.referralTypeArr = data)
      }

      // HTTP Request to GET Referral Source
      this.sqlWizS.GetReferralSource(programs[0]).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
        this.referralVariables.referralSourceArr = data;
        console.log(data);
        this.patchIfHCPNDIAChecked(data);
      });

      // HTTP Request to GET Referral Code
      this.sqlWizS.GetReferralCode(programs[0]).subscribe(data => {
              if(this.referTab == 7){
                  this.referralVariables.referralCodeArr = data.map(x => x.title)
                  return;
              }
              if(this.referTab == 8){
                  this.referralVariables.referralCodeArr = data.map(x => x.Activity)
                  return;
              }                 
              this.referralVariables.referralCodeArr = data           
      });
  }

  showThisOnTab_A(): boolean{
    return this.referTab == 5 || this.referTab == 7  || this.referTab == 8 || this.referTab == 9;
  }

  async showWhenProgramIsTicked(state: boolean, index: number){  
    await this._showWhenProgramIsTicked(state, index);
  }

  _showWhenProgramIsTicked(state: boolean, index: number){

    if(this.referTab == 1 || this.referTab == 2){
        if(this.whatOptionVar.programsArr[index].program === '*HCP REFERRAL'){
          let searchedIndex = this.globalS.findIndex('*NDIA REFERRAL', this.whatOptionVar.programsArr, 'program')
          this.whatOptionVar.programsArr[searchedIndex].disabled = state
        }

        if(this.whatOptionVar.programsArr[index].program === '*NDIA REFERRAL'){     
          let searchedIndex = this.globalS.findIndex('*HCP REFERRAL', this.whatOptionVar.programsArr, 'program')
          console.log(searchedIndex);
          this.whatOptionVar.programsArr[searchedIndex].disabled = state
        }
    }

    this.programTicked = true;
  }

  nextCustomClick(buttonType: string){
    if ("custom-next" === buttonType) {
      this.buildNotes();
      this.wizard.next();
    }
  }

  buildNotes(): void{
    const note = 
    `${ this.whatOptionVar.programsArr.filter(x => x.status).map(x => x.program).join(',') } / INQUIRY FROM: ${this.user.code}
    PHONE:       ${this.user.contact || ''}
    ADDRESS:     ${this.user.address || ''}

    NOTES: 
    `
    this.referralGroup.patchValue({
      notes: note
    })
  }

  noteChangeEvent(index: number){                
    if( this.referralGroup.value.notes != '' && this.showThisOnTab_A()){
        this.referralGroup.patchValue({ noteCategory: '' })
        this.listS.getcasenotecategory(index).subscribe(data => this.noteArray = data);
        return;
    }
    this.referralGroup.patchValue({ noteCategory: this.whatOptionVar.wizardTitle })        
  }

  isHCPNDIAChecked(){
    var checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status);
    return checkedPrograms.filter(x => x.program === '*HCP REFERRAL' || x.program === '*NDIA REFERRAL');
  }

  accept(){ 
      var specialPrograms = this.isHCPNDIAChecked();
      if(specialPrograms.length > 0){
          // this.heheOpen = false;
          this.packageTemplateModalOpen(specialPrograms[0].program);
          return;
      } else {
          this.CreateRosterAndNotes();
      }          
  }

  packageTemplates: Array<any>;
  newPackageName: string;
  packageTemplateOpen: boolean = false;
  packageCode: string = '';
  doPackageExist: boolean;
  templateDefault: any;

  packageTemplateModalOpen(name: string){
    this.packageTemplateOpen = true;
    this.packageCode = '';

    this.newPackageName = name == '*HCP REFERRAL' ? 'DOHA' : name == '*NDIA REFERRAL' ? 'NDIA' : null;

    this.listS.getpackagetemplate(this.newPackageName)
        .subscribe(data => {
            this.packageTemplates = data.map((x,index) => {
                return {
                    name: x.name,
                    packageLevel: x.packageLevel,
                    status: false
                }
            });            
        }); 
  }

  async changeTemplate(state: boolean, index: number){
    await this.loopAndDisableChckBoxes(state, index);
  }

  loopAndDisableChckBoxes(state: boolean, index: number){
      this.packageTemplates[index].status = state;
      this.packageTemplateEvent(this.newPackageName, index)
  }

  packageTemplateEvent(name: string, index: number): void{
    this.packageCode = '';

    if(name === 'DOHA'){
        var level  = '';
        if((this.packageTemplates[index].packageLevel).split(' ').length == 1){
            level = ''
        } else {
            level = '-L' +(this.packageTemplates[index].packageLevel).split(' ')[1]
        }
        this.packageCode =  'HCP' + level + " " + this.user.code
    }
    
    if(name === 'NDIA'){          
        this.packageCode =  'NDIA'  + " " + this.user.code;       
    }

    this.NDIA_HCP = {
        sourcePackage: this.packageTemplates[index].name,
        level: this.packageTemplates[index].packageLevel,
        type: name            
    }

    this.checkPackageCodeIfExist(this.packageCode);
  }

  checkPackageCodeIfExistObs(packageCode: string){
      return this.listS.getlist(`SELECT COUNT(Name) AS Count FROM HumanResourceTypes WHERE [Name] = '${packageCode}'`)
  }

  checkPackageCodeIfExist(packageCode: string){
    this.packageCode = packageCode;
    this.listS.getlist(`SELECT COUNT(Name) AS Count FROM HumanResourceTypes WHERE [Name] = '${packageCode}'`)
        .subscribe(data => {
            this.doPackageExist = data[0].Count > 0 ? true : false;
        });
  }

  saveNewPackageName(){      
    if(!this.doPackageExist){         
        this.NDIA_HCP.newPackageName = this.packageCode;
        this.CreateRosterAndNotes(true);
    } else {
        this.globalS.eToast('Error','An error occured');
    }
  }

  isSpecialProgram(program: string){
      return program == '*HCP REFERRAL' || program == '*NDIA REFERRAL';
  }

  CreateRosterAndNotes(isNDIAOrHCP: boolean = false){
    var referralGroup = this.referralGroup.value;

    const blockNoTime = Math.floor(this.globalS.getMinutes(referralGroup.time)/5)
    const timeInMinutes = this.globalS.getMinutes(referralGroup.timeSpent)
    const timePercentage = (Math.floor(timeInMinutes/60 * 100) / 100).toString()
    const user = this.globalS.decode();

    let defaultValues = {
        // '' is the default
        billDesc: '',
        
        // 7 for everything except 14 for item
        type: '7',          

        // 2 is the default,
        rStatus: '2',    

        // HOUR is the default     
        billUnit: 'HOUR',

        // 0 is the default  
        billType: '0',

        // '' is the default
        payType: '',

        // '' is the default
        payRate: '',

        // '' is the default
        payUnit: '',

        // '' is the default
        apInvoiceDate: '',

        // '' is the default
        apInvoiceNumber: '',

        // 0 is default
        groupActivity: '0',

        // '' is the default
        serviceSetting: ''
    }

    var checkedPrograms = this.whatOptionVar.programsArr.filter(x => x.status)
    var finalRoster: Array<Dto.ProcedureRoster> = [];

    if(this.referTab == 1){
        
        for(var a = 0, len = this.checkedPrograms.length; a < len ; a++){
            let data: Dto.ProcedureRoster = {
                clientCode: this.user.code,
                carerCode: user.code,
                serviceType: this.checkedPrograms[a].selected,
                date: moment(referralGroup.date).format('YYYY/MM/DD'),
                time: referralGroup.time,
                creator: user.user,
                editer: user.user,
                billUnit: 'HOUR',
                agencyDefinedGroup: this.user.agencyDefinedGroup,
                referralCode: referralGroup.referralCode,
                timePercent: timePercentage,
                Notes: '',
                type: 7,
                duration: timeInMinutes / 5,
                blockNo: blockNoTime,
                reasonType: referralGroup.referralSource,
                tabType: 'REFERRAL-IN',
                program: this.isSpecialProgram(this.checkedPrograms[a].program) ? this.NDIA_HCP.newPackageName : this.checkedPrograms[a].program,
                packageStatus: 'REFERRAL'
            }
            finalRoster.push(data);
        }

        let data: Dto.CallProcedure = {
            isNDIAHCP: isNDIAOrHCP,
            oldPackage: isNDIAOrHCP ? this.NDIA_HCP.sourcePackage : '',
            newPackage: isNDIAOrHCP ? this.NDIA_HCP.newPackageName : '',
            level: isNDIAOrHCP ? this.NDIA_HCP.level : '',
            type: isNDIAOrHCP ? this.NDIA_HCP.type: '',
            roster: finalRoster,
            staffNote: {
                personId: this.user.id,
                program: this.globalS.isVarious(this.checkedPrograms),
                detailDate: moment(referralGroup.date).format('YYYY/MM/DD HH:mm:ss'),
                extraDetail1: NOTE_TYPE[this.noteNumber],
                extraDetail2: 'REFERRAL-IN',
                whoCode: this.user.code,
                publishToApp: referralGroup.publishToApp ? 1 : 0,                    
                creator: user.user,
                note: referralGroup.notes,
                alarmDate: referralGroup.reminderDate == "" ? "" : moment(referralGroup.reminderDate).format('YYYY/MM/DD'),
                reminderTo: referralGroup.reminderTo
            }
        }

        console.log(data);
        
        this.listS.postreferralin(data).subscribe(data => {
            if(data){
              this.successResult();
            }
        });
        
    }
  }

  successResult(message: string = 'Transaction Complete'){
    this.globalS.sToast('Success', message);
    this.packageTemplateOpen = false;
    this.open = false;
      // this.statusTab.next(this.user)
  }
}
