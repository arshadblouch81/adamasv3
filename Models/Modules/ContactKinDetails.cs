namespace Adamas.Models.Modules{
    public class ContactKinDetails {
        public string ListOrder { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public string Ecode { get; set; }
        public string Mobile { get; set; }
        public string Group { get; set; }
        public string State { get; set; }
        public string Creator { get; set; } = "SYSMGR";
         public bool? emergencyContact { get; set; } 
         public string Category { get; set; }
       
          public string SubType { get; set; }
       
  
    }
}