using System;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Globalization;

using ProgramEntities = Adamas.Models.Modules.Program;
using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using System.Collections;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using System.Drawing.Imaging;
using System.Security.Principal;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    // [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    //[Authorize]
    public class BillingController : Controller
    {
        private readonly DatabaseContext _context;

        private IGeneralSetting GenSettings;
        private IBillingService billingService;
        private readonly IHttpClientFactory httpClientFactory;
        private IHostingEnvironment _env;
        private IConfiguration _configuration;


        public BillingController(
            DatabaseContext context,
            IConfiguration config,
            IGeneralSetting setting,
            IHostingEnvironment env,
            IHttpClientFactory httpClientFactory,
            IBillingService _billingService)
        {
            _context = context;
            GenSettings = setting;
            _env = env;
            this.billingService = _billingService;
            this.httpClientFactory = httpClientFactory;
            this._configuration = config;
        }

        [HttpGet("getActiveStates/{is_where}")]
        public async Task<IActionResult> getActiveStates(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(Distinct Phone2) AS statesTotal FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND EndDate IS NULL", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                statesTotalActive = GenSettings.Filter(rd["statesTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodStates")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodStates([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.Phone2) AS statesActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON [NAME] = [Program] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                statesActiveInPeriod = GenSettings.Filter(rd["statesActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveFundingRegions/{is_where}")]
        public async Task<IActionResult> getActiveFundingRegions(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(Distinct Suburb) AS fundingRegTotal FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND EndDate IS NULL", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                fundingRegionsTotalActive = GenSettings.Filter(rd["fundingRegTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodFundingRegions")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodFundingRegions([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.Suburb) AS fundingRegActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON [NAME] = [Program] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                fundingRegionsActiveInPeriod = GenSettings.Filter(rd["fundingRegActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveServiceProvider/{is_where}")]
        public async Task<IActionResult> getActiveServiceProvider(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(Distinct Address1) AS serviceProvTotal FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND EndDate IS NULL", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceProviderTotalActive = GenSettings.Filter(rd["serviceProvTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodServiceProvider")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodServiceProvider([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.Address1) AS serviceProvActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON [NAME] = [Program] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceProviderActiveInPeriod = GenSettings.Filter(rd["serviceProvActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveDsOutlets/{is_where}")]
        public async Task<IActionResult> getActiveDsOutlets(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(DISTINCT CSTDAOutletID) AS dsOutletTotal FROM ItemTypes", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                dsOutletsTotalActive = GenSettings.Filter(rd["dsOutletTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodDsOutlets")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodDsOutlets([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT itm.CSTDAOutletID) AS dsOutletActive FROM Roster ro INNER JOIN ItemTypes itm ON Title = [Service Type] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                dsOutletsActiveInPeriod = GenSettings.Filter(rd["dsOutletActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveBranches/{is_where}")]
        public async Task<IActionResult> getActiveBranches(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS branchTotal FROM DataDomains WHERE Domain = 'BRANCHES'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                branchesTotalActive = GenSettings.Filter(rd["branchTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodBranches")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodBranches([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT re.Branch) AS branchActive FROM Roster ro INNER JOIN Recipients re ON Accountno = [Client Code] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                branchesActiveInPeriod = GenSettings.Filter(rd["branchActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveFundingType/{is_where}")]
        public async Task<IActionResult> getActiveFundingType(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS fundingTypeTotal FROM DataDomains WHERE Domain = 'FUNDINGBODIES'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                fundingTypeTotalActive = GenSettings.Filter(rd["fundingTypeTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodFundingType")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodFundingType([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.Type) AS fundingTypeActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON hrt.[Name] = ro.Program  WHERE hrt.[Group] = 'PROGRAMS' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                fundingTypeActiveInPeriod = GenSettings.Filter(rd["fundingTypeActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveCareDomains/{is_where}")]
        public async Task<IActionResult> getActiveCareDomains(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS careDomainTotal FROM DataDomains WHERE Domain = 'CAREDOMAIN'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                careDomainsTotalActive = GenSettings.Filter(rd["careDomainTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodCareDomains")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodCareDomains([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.HRT_DATASET) AS careDomainActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON hrt.[Name] = ro.Program  WHERE hrt.[Group] = 'PROGRAMS' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                careDomainsActiveInPeriod = GenSettings.Filter(rd["careDomainActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveServiceBudget/{is_where}")]
        public async Task<IActionResult> getActiveServiceBudget(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS serviceBudgetTotal FROM DataDomains WHERE Domain = 'BUDGETGROUP'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceBudgetTotalActive = GenSettings.Filter(rd["serviceBudgetTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodServiceBudget")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodServiceBudget([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT itm.BudgetGroup) AS serviceBudgetActive FROM Roster ro INNER JOIN ItemTypes itm ON itm.[Title] = ro.[Service Type]  WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceBudgetActiveInPeriod = GenSettings.Filter(rd["serviceBudgetActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveServiceDiscip/{is_where}")]
        public async Task<IActionResult> getActiveServiceDiscip(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS serviceDiscipTotal FROM DataDomains WHERE Domain = 'DISCIPLINE'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceDiscipTotalActive = GenSettings.Filter(rd["serviceDiscipTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodServiceDiscip")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodServiceDiscip([FromBody] ActivePeriodDto period)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT itm.Dataset) AS serviceDiscipActive FROM Roster ro INNER JOIN ItemTypes itm ON itm.[Title] = ro.[Service Type]  WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceDiscipActiveInPeriod = GenSettings.Filter(rd["serviceDiscipActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveServiceRegions/{is_where}")]
        public async Task<IActionResult> getActiveServiceRegions(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS serviceRegionTotal FROM DataDomains WHERE Domain = 'GROUPAGENCY'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceRegionsTotalActive = GenSettings.Filter(rd["serviceRegionTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodServiceRegions")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodServiceRegions([FromBody] ActivePeriodDto period)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT re.AgencyDefinedGroup) AS serviceRegionActive FROM Roster ro INNER JOIN Recipients re ON Accountno = [Client Code] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceRegionsActiveInPeriod = GenSettings.Filter(rd["serviceRegionActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveServiceTypes/{is_where}")]
        public async Task<IActionResult> getActiveServiceTypes(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(Recnum) AS serviceTypeTotal FROM ItemTypes WHERE ProcessClassification = 'OUTPUT'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceTypesTotalActive = GenSettings.Filter(rd["serviceTypeTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodServiceTypes")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodServiceTypes([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT itm.HACCType) AS serviceTypeActive FROM Roster ro INNER JOIN ItemTypes itm ON itm.[Title] = ro.[Service Type] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                serviceTypesActiveInPeriod = GenSettings.Filter(rd["serviceTypeActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActivePrograms/{is_where}")]
        public async Task<IActionResult> getActivePrograms(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS programTotal FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND EndDate IS NULL", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                programsTotalActive = GenSettings.Filter(rd["programTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodPrograms")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodPrograms([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.RecordNumber) AS programActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON hrt.[Name] = ro.Program  WHERE hrt.[Group] = 'PROGRAMS' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                programsActiveInPeriod = GenSettings.Filter(rd["programActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveCoordinators/{is_where}")]
        public async Task<IActionResult> getActiveCoordinators(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS coordinatorTotal FROM DataDomains WHERE Domain = 'CASE MANAGERS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                coordinatorsTotalActive = GenSettings.Filter(rd["coordinatorTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodCoordinators")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodCoordinators([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT re.RECIPIENT_Coordinator) AS coordinatorActive FROM Roster ro INNER JOIN Recipients re ON Accountno = [Client Code] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                coordinatorsActiveInPeriod = GenSettings.Filter(rd["coordinatorActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveCostCenters/{is_where}")]
        public async Task<IActionResult> getActiveCostCenters(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(Distinct FAX) AS costCenterTotal FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND EndDate IS NULL AND (FAX IS NOT NULL) AND (FAX <> '')", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                costCentersTotalActive = GenSettings.Filter(rd["costCenterTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodCostCenters")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodCostCenters([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT hrt.FAX) AS costCenterActive FROM Roster ro INNER JOIN HumanResourceTypes hrt ON hrt.[Name] = ro.Program  WHERE hrt.[Group] = 'PROGRAMS' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                costCentersActiveInPeriod = GenSettings.Filter(rd["costCenterActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveRecipients/{is_where}")]
        public async Task<IActionResult> getActiveRecipients(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(DISTINCT PersonID) AS recipientTotal FROM Recipients INNER JOIN RecipientPrograms ON UniqueID = PersonID WHERE ProgramStatus = 'ACTIVE'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recipientsTotalActive = GenSettings.Filter(rd["recipientTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodRecipients")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodRecipients([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT ro.[Client Code]) AS recipientActive FROM Roster ro  WHERE ro.[Client Code] > '!z' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recipientsActiveInPeriod = GenSettings.Filter(rd["recipientActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveStaff/{is_where}")]
        public async Task<IActionResult> getActiveStaff(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(DISTINCT Accountno) AS staffTotal FROM Staff WHERE (([CommencementDate] IS NOT NULL) AND ([TerminationDate] IS NULL))", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                staffTotalActive = GenSettings.Filter(rd["staffTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodStaff")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodStaff([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT ro.[Carer Code]) AS staffActive FROM Roster ro  WHERE ro.[Carer Code] > '!z' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                staffActiveInPeriod = GenSettings.Filter(rd["staffActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveStaffCategory/{is_where}")]
        public async Task<IActionResult> getActiveStaffCategory(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS staffCategoryTotal FROM DataDomains WHERE Domain = 'STAFFGROUP'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                staffCategoryTotalActive = GenSettings.Filter(rd["staffCategoryTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodStaffCategory")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodStaffCategory([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT st.StaffGroup) AS staffCategoryActive FROM Roster ro INNER JOIN Staff st ON st.[AccountNo] = ro.[Carer Code] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                staffCategoryActiveInPeriod = GenSettings.Filter(rd["staffCategoryActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getActiveTeams/{is_where}")]
        public async Task<IActionResult> getActiveTeams(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Count(RecordNumber) AS teamTotal FROM DataDomains WHERE Domain = 'STAFFTEAM'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                teamsTotalActive = GenSettings.Filter(rd["teamTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivePeriodTeams")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getActivePeriodTeams([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT st.StaffTeam) AS teamActive FROM Roster ro INNER JOIN Staff st ON st.[AccountNo] = ro.[Carer Code] WHERE ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                teamsActiveInPeriod = GenSettings.Filter(rd["teamActive"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getOutputHours")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getOutputHours([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT ROUND(CONVERT(Decimal (5, 0), (Sum(Duration)) * 5 / 60), 1) AS [HrTotal] FROM Roster ro LEFT JOIN Recipients re ON AccountNo = [Client Code] LEFT JOIN DataDomains dd ON dd.Description = re.branch AND dd.Domain = 'BRANCHES' LEFT JOIN ItemTypes itm ON itm.[Title] = ro.[Service Type] LEFT JOIN HumanResourceTypes hrt ON hrt.[Name] = ro.Program WHERE ([Client Code] > '!z') AND (ro.[Type] IN (2, 3, 5, 7, 8, 10, 11, 12)) AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                totalOutHour = GenSettings.Filter(rd["HrTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getWorkedHours")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getWorkedHours([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT ROUND(CONVERT(Decimal (5, 0), (Sum(Duration)) * 5 / 60), 1) AS [HrTotal] FROM Roster ro LEFT JOIN Recipients re ON AccountNo = [Client Code] LEFT JOIN DataDomains dd ON dd.Description = re.branch AND dd.Domain = 'BRANCHES' LEFT JOIN ItemTypes itm ON itm.[Title] = ro.[Service Type] LEFT JOIN HumanResourceTypes hrt ON hrt.[Name] = ro.Program WHERE ([Carer Code] > '!z') AND (ro.[Type] IN (2, 3, 5, 6, 7, 8, 10, 11, 12)) AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                totalWorkedHour = GenSettings.Filter(rd["HrTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getWorkedAttributeHours")]
        public async Task<IActionResult> getWorkedAttributeHours([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT ROUND(CONVERT(Decimal (5, 0), (Sum(Duration)) * 5 / 60), 1) AS [HrTotal] FROM Roster ro  WHERE ro.[Carer Code] > '!z' AND Type IN (2, 3, 4, 5, 7, 8, 10, 11, 12) AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                totalWorkedAttrHour = GenSettings.Filter(rd["HrTotal"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getTotalStaff")]
        public async Task<IActionResult> getTotalStaff([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT ro.[Carer Code]) AS stActCount FROM Roster ro  WHERE ro.[Carer Code] > '!z' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                totalNoStaff = GenSettings.Filter(rd["stActCount"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getTotalRecipient")]
        public async Task<IActionResult> getTotalRecipient([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Count(DISTINCT ro.[Client Code]) AS clActCount FROM Roster ro  WHERE ro.[Client Code] > '!z' AND ro.Date BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                totalNoRecipient = GenSettings.Filter(rd["clActCount"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        // [HttpPut("getDebtorRecords")]
        // public async Task<IActionResult> getDebtorRecords([FromBody] DebtorRecords group) 
        // {
        //     string sql = "usp_HumanResourceTypes_U";

        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         var cmd = new SqlCommand(string.Format(sql), conn);
        //         await conn.OpenAsync();
        //         cmd.Parameters.AddWithValue("@startdate", Convert.ToDateTime(group.StartDate));
        //         cmd.Parameters.AddWithValue("@enddate", Convert.ToDateTime(group.EndDate));
        //         cmd.Parameters.AddWithValue("@branches", group.Branches);
        //         cmd.Parameters.AddWithValue("@programs", group.Programs);
        //         cmd.Parameters.AddWithValue("@categories", group.Categories);
        //         return Ok(await cmd.ExecuteNonQueryAsync() > 0);
        //     }
        // }

        // [HttpPut("postDebtorBilling")]
        // public async Task<IActionResult> postDebtorBilling([FromBody] DebtorBilling group) 
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //        using (SqlCommand cmd = new SqlCommand(@"usp_DebtorPost_U", (SqlConnection)conn))
        //         {
        //             cmd.CommandType = CommandType.StoredProcedure;
        //             cmd.Parameters.AddWithValue("@invoiceNumber", group.InvoiceNumber);
        //             cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
        //             cmd.Parameters.AddWithValue("@categories", group.Categories);
        //             await conn.OpenAsync();
        //             await cmd.ExecuteNonQueryAsync();
        //             return Ok(true);
        //         }
        //     }
        // }

        [HttpPut("postDebtorBilling")]
        public async Task<IActionResult> postDebtorBilling([FromBody] DebtorBilling group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_DebtorPost_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@invoiceNumber", group.InvoiceNumber);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@branches", group.Branches);
                    cmd.Parameters.AddWithValue("@programs", group.Programs);
                    cmd.Parameters.AddWithValue("@categories", group.Categories);
                    cmd.Parameters.AddWithValue("@startdate", group.StartDate);
                    cmd.Parameters.AddWithValue("@enddate", group.EndDate);
                    cmd.Parameters.AddWithValue("@invoiceType", group.InvoiceType);
                    cmd.Parameters.AddWithValue("@sepInvRecipient", group.sepInvRecipient);
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getBillingCycle/{is_where}")]
        public async Task<IActionResult> getBillingCycle(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Description as description FROM DataDomains WHERE Domain = 'BILLINGCYCLE' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                description = GenSettings.Filter(rd["description"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("closeRosterPeriod")]
        public async Task<IActionResult> closeRosterPeriod([FromBody] CloseRosterGroup group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_HumanResourceTypes_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@closedate", group.CloseDate);
                    cmd.Parameters.AddWithValue("@program", group.Programs);
                    cmd.Parameters.AddWithValue("@funding", group.Fundings);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getInvoiceBatch")]
        public async Task<IActionResult> getInvoiceBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, convert(varchar, BCH_DATE, 105) AS batchDate FROM batch_record WHERE bch_type = 'B' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackInvoiceBatch")]
        public async Task<IActionResult> rollbackInvoiceBatch([FromBody] RollbackInvoiceBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackInvoiceBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getPayrollBatch")]
        public async Task<IActionResult> getPayrollBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, convert(varchar, BCH_DATE, 105) AS batchDate FROM batch_record WHERE bch_type = 'P' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackPayrollBatch")]
        public async Task<IActionResult> rollbackPayrollBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackPayrollBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getRosterBatch")]
        public async Task<IActionResult> getRosterBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT rsc_id as batchNumber, rsc_user as batchUser, convert(varchar, rsc_copy_date, 105) as batchDate FROM rostercopy ORDER BY rsc_copy_date DESC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackRosterBatch")]
        public async Task<IActionResult> rollbackRosterBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackRosterBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getPayBatchRecord/{is_where}")]
        public async Task<IActionResult> getPayBatchRecord(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num FROM batch_record WHERE bch_num = (select max(bch_num) from batch_record where bch_type = 'P') and bch_type = 'P'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchRecordNumber = GenSettings.Filter(rd["bch_num"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("postPayUpdate")]
        public async Task<IActionResult> postPayUpdate([FromBody] PayUpdate group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_PayUpdatePost_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@branches", group.Branches);
                    cmd.Parameters.AddWithValue("@fundings", group.Fundings);
                    cmd.Parameters.AddWithValue("@categories", group.Categories);
                    cmd.Parameters.AddWithValue("@programs", group.Programs);
                    cmd.Parameters.AddWithValue("@staffs", group.Staffs);
                    cmd.Parameters.AddWithValue("@startdate", group.StartDate);
                    cmd.Parameters.AddWithValue("@enddate", group.EndDate);
                    cmd.Parameters.AddWithValue("@chkStaffPays", group.ChkStaffPays);
                    cmd.Parameters.AddWithValue("@chkPayBrokerage", group.ChkPayBrokerage);
                    cmd.Parameters.AddWithValue("@chkCreateExportFile", group.ChkCreateExportFile);
                    cmd.Parameters.AddWithValue("@chkAwardInterExport", group.ChkAwardInterExport);
                    cmd.Parameters.AddWithValue("@selectedPackage", group.SelectedPackage);
                    cmd.Parameters.AddWithValue("@inputDefaultAccount", group.InputDefaultAccount);
                    cmd.Parameters.AddWithValue("@inputDefaultProject", group.InputDefaultProject);
                    cmd.Parameters.AddWithValue("@inputDefaultActivity", group.InputDefaultActivity);
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getSysTableDates")]
        public async Task<IActionResult> getSysTableDates()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                // using (SqlCommand cmd = new SqlCommand(@"SELECT CONVERT(VARCHAR, PayPeriodEndDate, 105) AS PayPeriodEndDate, CONVERT(VARCHAR, TSheetFirstServiceDate, 105) AS TSheetFirstServiceDate, CONVERT(VARCHAR, TSheetLastServiceDate, 105) AS TSheetLastServiceDate FROM SysTable", (SqlConnection)conn))
                // using (SqlCommand cmd = new SqlCommand(@"SELECT CONVERT(VARCHAR, PayPeriodEndDate, 103) AS PayPeriodEndDate, CONVERT(VARCHAR, TSheetFirstServiceDate, 103) AS TSheetFirstServiceDate, CONVERT(VARCHAR, TSheetLastServiceDate, 103) AS TSheetLastServiceDate FROM SysTable", (SqlConnection)conn))
                // using (SqlCommand cmd = new SqlCommand(@"SELECT PayPeriodEndDate, TSheetFirstServiceDate, TSheetLastServiceDate FROM SysTable", (SqlConnection)conn))
                using (SqlCommand cmd = new SqlCommand(@"SELECT FORMAT(PayPeriodEndDate, 'yyyy-MM-dd') AS PayPeriodEndDate, FORMAT(TSheetFirstServiceDate, 'yyyy-MM-dd') AS TSheetFirstServiceDate, FORMAT(TSheetLastServiceDate, 'yyyy-MM-dd') AS TSheetLastServiceDate FROM SysTable", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                payPeriodEndDate = GenSettings.Filter(rd["PayPeriodEndDate"]),
                                tSheetFirstServiceDate = GenSettings.Filter(rd["TSheetFirstServiceDate"]),
                                tSheetLastServiceDate = GenSettings.Filter(rd["TSheetLastServiceDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("getUnapprovedWorkHours")]
        public async Task<IActionResult> getUnapprovedWorkHours([FromBody] UpdateRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetUnapprovedWorkHours_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@startdate", data.actionStartDate);
                    cmd.Parameters.AddWithValue("@enddate", data.actionEndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                Recipient = GenSettings.Filter(rd["Recipient"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["StartTime"]),
                                ServiceType = GenSettings.Filter(rd["ServiceType"]),
                                Staff = GenSettings.Filter(rd["Staff"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getlistcategories")]
        public async Task<IActionResult> getlistcategories()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [RecordNumber], [Description] FROM DataDomains WHERE Domain = 'STAFFGROUP' ORDER BY Description ASC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    ICollection<DataDomain> list = new List<DataDomain>();
                    while (rd.Read())
                    {
                        list.Add(new DataDomain
                        {
                            RecordNumber = rd.GetInt32(0),
                            Description = rd.GetString(1)
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("getProgramPackageslist")]
        public async Task<IActionResult> getProgramPackageslist([FromBody] ProgPackageList period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT [Name] AS PPName FROM HumanResourceTypes WHERE [GROUP] = 'PROGRAMS' " + period.condition + " ORDER BY 1 ASC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                PPName = rd.GetString(0)
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("setPayPeriodDate")]
        public async Task<IActionResult> setPayPeriodDate([FromBody] PayPeriodDate group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_PayPeriodDates_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@payPeriodEndDate", group.PayPeriodEndDate);
                    cmd.Parameters.AddWithValue("@startDate", group.StartDate);
                    cmd.Parameters.AddWithValue("@endDate", group.EndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getStaffDetails")]
        public async Task<IActionResult> getStaffDetails(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER(AccountNo) as AccountNo FROM Staff WHERE AccountNo > '!z' AND (COmmencementDate is not null) and (TerminationDate is null)", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                accountNumber = GenSettings.Filter(rd["AccountNo"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getRecipientsDetails")]
        public async Task<IActionResult> getRecipientsDetails(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER(AccountNo) AS AccountNo FROM Recipients WHERE AccountNo > '!z' ORDER BY UPPER(AccountNo)", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                accountNumber = GenSettings.Filter(rd["AccountNo"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getFundingDetails")]
        public async Task<IActionResult> getFundingDetails(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [TYPE] AS FundingType FROM HUMANRESOURCETYPES WHERE [GROUP] = 'PROGRAMS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                fundingType = GenSettings.Filter(rd["FundingType"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getProgramslist")]
        public async Task<IActionResult> getProgramslist([FromBody] ProgPackageList period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT UPPER([Name]) AS programName FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= '" + period.condition + "') AND ISNULL(UserYesNo3, 0) = 0 AND IsNull(User2, '') <> 'Contingency'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                programName = rd.GetString(0)
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getTravelProgram")]
        public async Task<IActionResult> getTravelProgram(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [Name] AS TravelProgram FROM HumanResourceTypes WHERE [GROUP] = 'PROGRAMS' AND IsNull(ENDDATE, '') = '' ORDER BY [Name]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                travelProgram = GenSettings.Filter(rd["TravelProgram"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getTravelActivity")]
        public async Task<IActionResult> getTravelActivity(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Title AS TravelActivity FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND IsNull(ENDDATE, '') = '' ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                travelActivity = GenSettings.Filter(rd["TravelActivity"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getTravelPaytype")]
        public async Task<IActionResult> getTravelPaytype(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Title AS TravelPaytype FROM ItemTypes WHERE ProcessClassification = 'INPUT' AND IsNull(ENDDATE, '') = '' ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                travelPaytype = GenSettings.Filter(rd["TravelPaytype"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getRscID/{is_where}")]
        public async Task<IActionResult> getRscID(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT MAX(rsc_ID) AS rsc_ID from rostercopy", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                rscID = GenSettings.Filter(rd["rsc_ID"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getUniqueBatchKey/{is_where}")]
        public async Task<IActionResult> getUniqueBatchKey(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT UniqueBatchKey as uniqueBatchKey FROM Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                uniqueBatchKey = GenSettings.Filter(rd["uniqueBatchKey"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("updateUniqueBatchKey")]
        public async Task<IActionResult> updateUniqueBatchKey([FromBody] int batchKey)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE Registration SET UniqueBatchKey = @batchKey + 1", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@batchKey", batchKey);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("getTravelUpdate")]
        public async Task<IActionResult> getTravelUpdate([FromBody] TravelUpdate group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetTravelUpdate_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@branches", group.Branches);
                    cmd.Parameters.AddWithValue("@programs", group.Programs);
                    cmd.Parameters.AddWithValue("@categories", group.Categories);
                    cmd.Parameters.AddWithValue("@staffs", group.Staffs);
                    cmd.Parameters.AddWithValue("@fundings", group.Fundings);
                    cmd.Parameters.AddWithValue("@startdate", group.StartDate);
                    cmd.Parameters.AddWithValue("@enddate", group.EndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Client = GenSettings.Filter(rd["Client"]),
                                Activity = GenSettings.Filter(rd["Activity"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["StartTime"]),
                                EndTime = GenSettings.Filter(rd["EndTime"]),
                                Address = GenSettings.Filter(rd["Address"]),
                                PickUpFrom = GenSettings.Filter(rd["PickUpFrom"]),
                                TakeTo = GenSettings.Filter(rd["TakeTo"]),
                                RecordID = GenSettings.Filter(rd["RecordID"]),
                                VehicleType = GenSettings.Filter(rd["VehicleType"]),
                                VehicleAddress = GenSettings.Filter(rd["VehicleAddress"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getTravelProvider/{is_where}")]
        public async Task<IActionResult> getTravelProvider(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 TravelProvider AS TravelProvider FROM Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                travelProvider = GenSettings.Filter(rd["TravelProvider"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getGoogleCustID/{is_where}")]
        public async Task<IActionResult> getGoogleCustID(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 GoogleCustID AS GoogleCustID FROM Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                googleCustID = GenSettings.Filter(rd["GoogleCustID"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getProviderKey/{is_where}")]
        public async Task<IActionResult> getProviderKey(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 ProviderKey AS ProviderKey FROM Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                providerKey = GenSettings.Filter(rd["ProviderKey"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpDelete("truncateTable/{is_where}")]
        public async Task<IActionResult> truncateTable(bool is_where = false)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("TRUNCATE TABLE [dbo].TravelCalc", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        // [HttpDelete("deleteFromRoster/{is_where}")]
        // public async Task<IActionResult> deleteFromRoster(string dtpStartDate, string dtpEndDate)
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         using (SqlCommand cmd = new SqlCommand("DELETE FROM ROSTER WHERE TYPE = 5 AND [date] BETWEEN @startDate AND @endDate", (SqlConnection)conn))
        //         {
        //             cmd.Parameters.AddWithValue("@startDate", dtpStartDate);
        //             cmd.Parameters.AddWithValue("@endDate", dtpEndDate);
        //             await conn.OpenAsync();
        //             return Ok(await cmd.ExecuteNonQueryAsync() > 0);
        //         }
        //     }
        // }

        [HttpPost("deleteFromRoster")]
        public async Task<IActionResult> deleteFromRoster([FromBody] TravelRosterDeletedDates period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"DELETE FROM ROSTER WHERE TYPE = 5 AND [date] BETWEEN @startDate AND @endDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.StartDate);
                    cmd.Parameters.AddWithValue("@endDate", period.EndDate);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        // [HttpPut("insertTravelCalc")]
        // public async Task<IActionResult> insertTravelCalc([FromBody] TravelCalculate group)
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         using (SqlCommand cmd = new SqlCommand(@"usp_InsertTravelCalc_I", (SqlConnection)conn))
        //         {
        //             cmd.CommandType = CommandType.StoredProcedure;
        //             cmd.Parameters.AddWithValue("@batchno", group.Batchno);
        //             cmd.Parameters.AddWithValue("@gStaff", group.Staff);
        //             cmd.Parameters.AddWithValue("@gDate", group.Date);
        //             cmd.Parameters.AddWithValue("@startClientEndTime", group.StartClientEndTime);
        //             cmd.Parameters.AddWithValue("@timeDuration", group.TimeDuration);
        //             cmd.Parameters.AddWithValue("@startClient", group.StartClient);
        //             cmd.Parameters.AddWithValue("@startAddress", group.StartAddress);
        //             cmd.Parameters.AddWithValue("@startRecord", group.StartRecord);
        //             cmd.Parameters.AddWithValue("@startService", group.StartService);
        //             cmd.Parameters.AddWithValue("@endClient", group.EndClient);
        //             cmd.Parameters.AddWithValue("@endAddress", group.EndAddress);
        //             cmd.Parameters.AddWithValue("@endRecord", group.EndRecord);
        //             cmd.Parameters.AddWithValue("@endService", group.EndService);
        //             cmd.Parameters.AddWithValue("@disMeters", group.DisMeters);
        //             cmd.Parameters.AddWithValue("@disSeconds", group.DisSeconds);
        //             cmd.Parameters.AddWithValue("@vehicleType", group.VehicleType);
        //             await conn.OpenAsync();
        //             using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
        //             {
        //                 List<dynamic> list = new List<dynamic>();

        //                 while (await rd.ReadAsync())
        //                 {
        //                     list.Add(new
        //                     {
        //                         updatedRecords = GenSettings.Filter(rd["updatedRecords"])
        //                     });
        //                 }
        //                 return Ok(list);
        //             }
        //         }
        //     }
        // }

        [HttpGet("travelToFitGapTrue/{batchNumber}")]
        public async Task<IActionResult> travelToFitGapTrue(string batchNumber)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT STAFF, [Date], [Start Time] AS StartTime, EndRecord, EndClient, StartClient, EndService, 
                    DATENAME(dw,[Date]) AS DayName, 
                    ROUND(sum(convert(decimal, TravelMtrs))/1000, 2) AS KM, 
                    CASE WHEN (ROUND(Sum(convert(decimal, Duration)), 0) * 5) <= 0 THEN ROUND(Sum(convert(decimal, Duration)), 0) ELSE CASE WHEN TravelSeconds BETWEEN 30 AND 300 Then 1 ELSE ROUND(Sum(Convert(decimal, TravelSeconds)) / 300, 0) END END AS DRN, 
                    ROUND(Sum(Convert(decimal, TravelSeconds)) / 3600, 2) As HRS
                    FROM travelcalc
                    WHERE 1=1
                    AND (Batch# = @batchNumber)
                    AND StartAddress <> EndAddress 
                    GROUP BY [staff], [date], [start time], EndRecord, EndCLient, EndService, StartClient, TravelSeconds 
                    ORDER BY STAFF", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@batchNumber", batchNumber);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["StartTime"]),
                                EndRecord = GenSettings.Filter(rd["EndRecord"]),
                                EndClient = GenSettings.Filter(rd["EndClient"]),
                                StartClient = GenSettings.Filter(rd["StartClient"]),
                                EndService = GenSettings.Filter(rd["EndService"]),
                                DayName = GenSettings.Filter(rd["DayName"]),
                                KM = GenSettings.Filter(rd["KM"]),
                                DRN = GenSettings.Filter(rd["DRN"]),
                                HRS = GenSettings.Filter(rd["HRS"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("travelToFitGapFalse/{batchNumber}")]
        public async Task<IActionResult> travelToFitGapFalse(string batchNumber)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT STAFF, [Date], [Start Time] AS StartTime, EndRecord, EndClient, StartClient, EndService, 
                    DATENAME(dw,[Date]) AS DayName, 
                    ROUND(sum(convert(decimal, TravelMtrs))/1000, 2) AS KM,
                    CASE WHEN TravelSeconds BETWEEN 30 AND 300 Then 1 ELSE ROUND(Sum(Convert(decimal, TravelSeconds)) / 300, 0) END AS DRN,
                    ROUND(Sum(Convert(decimal, TravelSeconds)) / 3600, 2) As HRS
                    FROM travelcalc
                    WHERE 1=1
                    AND (Batch# = @batchNumber)
                    AND StartAddress <> EndAddress 
                    GROUP BY [staff], [date], [start time], EndRecord, EndClient, EndService, StartClient, TravelSeconds
                    ORDER BY STAFF", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@batchNumber", batchNumber);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["StartTime"]),
                                EndRecord = GenSettings.Filter(rd["EndRecord"]),
                                EndClient = GenSettings.Filter(rd["EndClient"]),
                                StartClient = GenSettings.Filter(rd["StartClient"]),
                                EndService = GenSettings.Filter(rd["EndService"]),
                                DayName = GenSettings.Filter(rd["DayName"]),
                                KM = GenSettings.Filter(rd["KM"]),
                                DRN = GenSettings.Filter(rd["DRN"]),
                                HRS = GenSettings.Filter(rd["HRS"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("travelSeparteClaimFalse/{batchNumber}")]
        public async Task<IActionResult> travelSeparteClaimFalse(string batchNumber)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT STAFF, [Date],
                    DATENAME(dw,[Date]) AS DayName,
                    ROUND(sum(convert(decimal, TravelMtrs))/1000, 2) AS KM,
                    CASE WHEN TravelSeconds BETWEEN 30 AND 300 Then 1 ELSE ROUND(Sum(Convert(decimal, TravelSeconds)) / 300, 0) END As DRN,
                    ROUND(Sum(Convert(decimal, TravelSeconds)) / 3600, 2) As HRS
                    FROM travelcalc
                    WHERE 1=1
                    AND (Batch# = @batchNumber)
                    AND StartAddress <> EndAddress 
                    GROUP BY [staff], [date], TravelSeconds 
                    ORDER BY STAFF, [date]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@batchNumber", batchNumber);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                DayName = GenSettings.Filter(rd["DayName"]),
                                KM = GenSettings.Filter(rd["KM"]),
                                DRN = GenSettings.Filter(rd["DRN"]),
                                HRS = GenSettings.Filter(rd["HRS"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("GetMapDistanceTest")]
        public async Task<IActionResult> GetMapDistanceTest()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Lahore&origins=Faisalabad&units=imperial&key=AIzaSyAMIBJrZxsPVqBAxzoZJgmaxfYIoCpYGWc");
            var client = httpClientFactory.CreateClient();
            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var serializer = new JsonSerializer();
                var ms = await response.Content.ReadAsStreamAsync();
                using (var sr = new StreamReader(ms))
                using (var jsonTextReader = new JsonTextReader(sr))
                {
                    return Ok(serializer.Deserialize(jsonTextReader));

                }
            }
            else
            {
                return BadRequest();
            }
        }

        // [HttpPut("getTravelDistance")]
        // public async Task<IActionResult> getTravelDistance([FromBody] LocationDistance data)
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         using (SqlCommand cmd = new SqlCommand(@"usp_GetDistanceMatrix_S", (SqlConnection)conn))
        //         {
        //             cmd.CommandType = CommandType.StoredProcedure;
        //             cmd.Parameters.AddWithValue("@origin", data.Origin);
        //             cmd.Parameters.AddWithValue("@destination", data.Destination);
        //             cmd.Parameters.AddWithValue("@googleCustID", data.GoogleCustID);
        //             cmd.Parameters.AddWithValue("@travelProvider", data.TravelProvider);
        //             cmd.Parameters.AddWithValue("@mapApiKey", data.MapApiKey);
        //             await conn.OpenAsync();
        //             using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
        //             {
        //                 List<dynamic> list = new List<dynamic>();

        //                 while (await rd.ReadAsync())
        //                 {
        //                     list.Add(new
        //                     {
        //                         apiStatus = GenSettings.Filter(rd["Status"]),
        //                         apiDistance = GenSettings.Filter(rd["Distance"]),
        //                         apiDuration = GenSettings.Filter(rd["Duration"])
        //                     });
        //                 }
        //                 return Ok(list);
        //             }
        //         }
        //     }
        // }

        [HttpPut("insertTravelCalc")]
        public async Task<IActionResult> insertTravelCalc([FromBody] TravelCalculate data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetDistanceInsertTravelCalc_I", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@origin", data.Origin);
                    cmd.Parameters.AddWithValue("@destination", data.Destination);
                    cmd.Parameters.AddWithValue("@googleCustID", data.GoogleCustID);
                    cmd.Parameters.AddWithValue("@travelProvider", data.TravelProvider);
                    cmd.Parameters.AddWithValue("@mapApiKey", data.MapApiKey);
                    cmd.Parameters.AddWithValue("@batchno", data.Batchno);
                    cmd.Parameters.AddWithValue("@gStaff", data.Staff);
                    cmd.Parameters.AddWithValue("@gDate", data.Date);
                    cmd.Parameters.AddWithValue("@startClientEndTime", data.StartClientEndTime);
                    cmd.Parameters.AddWithValue("@timeDuration", data.TimeDuration);
                    cmd.Parameters.AddWithValue("@startClient", data.StartClient);
                    cmd.Parameters.AddWithValue("@startAddress", data.StartAddress);
                    cmd.Parameters.AddWithValue("@startRecord", data.StartRecord);
                    cmd.Parameters.AddWithValue("@startService", data.StartService);
                    cmd.Parameters.AddWithValue("@endClient", data.EndClient);
                    cmd.Parameters.AddWithValue("@endAddress", data.EndAddress);
                    cmd.Parameters.AddWithValue("@endRecord", data.EndRecord);
                    cmd.Parameters.AddWithValue("@endService", data.EndService);
                    cmd.Parameters.AddWithValue("@vehicleType", data.VehicleType);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                // apiStatus = GenSettings.Filter(rd["Status"]),
                                // apiDistance = GenSettings.Filter(rd["Distance"]),
                                // apiDuration = GenSettings.Filter(rd["Duration"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        // [HttpPut("getGlanceDashboardValues")]
        // public async Task<IActionResult> getGlanceDashboardValues([FromBody] TravelUpdate group)
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         using (SqlCommand cmd = new SqlCommand(@"usp_GetGlanceDashboard_S", (SqlConnection)conn))
        //         {
        //             cmd.CommandType = CommandType.StoredProcedure;
        //             cmd.Parameters.AddWithValue("@startdate", group.StartDate);
        //             cmd.Parameters.AddWithValue("@enddate", group.EndDate);
        //             await conn.OpenAsync();
        //             using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
        //             {
        //                 List<dynamic> list = new List<dynamic>();

        //                 while (await rd.ReadAsync())
        //                 {
        //                     list.Add(new
        //                     {
        //                         Staff = GenSettings.Filter(rd["getOutputHours"]),
        //                         Client = GenSettings.Filter(rd["getWorkedHours"])
        //                     });
        //                 }
        //                 return Ok(list);
        //             }
        //         }
        //     }
        // }

        [HttpPut("getGlanceDashboardValues")]
        public async Task<IActionResult> getGlanceDashboardValues([FromBody] TravelUpdate group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetGlanceDashboard_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@startdate", group.StartDate);
                    cmd.Parameters.AddWithValue("@enddate", group.EndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                // OutputHours = GenSettings.Filter(rd["getOutputHours"]),
                                // WorkedHours = GenSettings.Filter(rd["getWorkedHours"]) 
                                field = GenSettings.Filter(rd["field"]),
                                val = GenSettings.Filter(rd["val"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getAwardRuleList")]
        public async Task<IActionResult> GetAwardRuleList()
                {
                try
                {
                // Fetch all data from the AwardPos table
                var awardRules = await _context.AwardPos
                    .Select(a => new
                    {
                        a.RECORDNO,
                        a.Code,
                        a.Description,
                        a.Category,
a.Level,
                a.Notes,
                        a.BASE_PayType,
                        a.Week_MaxOrdHr,
                        a.FN_MaxOrdHr,
                        a.W4_MaxOrdHours,
                        a.Week_MaxOrdDays,
                        a.Day_MaxOrdHr,
                        a.FN_MaxOrdDays,
                        a.FN_MaxOrdHrsDay,
                        a.W4_MaxOrdDays,
                        a.W4_MaxOrdHrsDay,
                        a.OrdHoursStartTime,
                        a.OrdHoursEndTime,
                        a.MinHoursPerDay,
                        a.MinHoursPerService,
                        a.BreakTimeWorked,
                        a.Week_MinFreeDays,
                        a.FN_FreeDays,
                        a.W4_FreeDays,
                        a.PayOvertimeInsuficientMinDays,

                        a.PayOvertimeNoMinBreak,
                        a.MinBetweenShiftBreak,
                        a.MinBreakBetweenSleepovers,
                        a.MinBreakPayType,
                        a.Day_SatOrdHr_PayType,
                        a.Day_SunOrdHr_PayType,
                        a.Day_PHOrdHr_PayType,

                        a.OvertimeLeeway,
                        a.TOILDefaultAccrualMinutes,
                        a.TOILDefaultAccrualPaytype,

                        a.Day_MaxOrdHrWD1stOTBreak,
                        a.Day_MaxOrdHrWD1stOTBreak_PayType,
                        a.Day_MaxOrdHrWD2ndOTBreak_PayType,

                        a.Day_MaxOrdHrSat1stOTBreak,
                        a.Day_MaxOrdHrSat1stOTBreak_PayType,
                        a.Day_MaxOrdHrSat2ndOTBreak_PayType,

                        a.Day_MaxOrdHrSun1stOTBreak,
                        a.Day_MaxOrdHrSun1stOTBreak_PayType,
                        a.Day_MaxOrdHrSun2ndOTBreak_PayType,

                        a.Day_MaxOrdHrPH1stOTBreak,
                        a.Day_MaxOrdHrPH1stOTBreak_PayType,
                        a.Day_MaxOrdHrPH2ndOTBreak_PayType,

                        a.Week_MaxOrdHrWD1stOTBreak,
                        a.Week_MaxOrdHrWD1stOTBreak_PayType,
                        a.Week_MaxOrdHrWD2ndOTBreak_PayType,


                        a.FN_MaxOrdHrWD1stOTBreak,
                        a.FN_MaxOrdHrWD1stOTBreak_PayType,
                        a.FN_MaxOrdHrWD2ndOTBreak_PayType,

                        a.DisableBrokenShiftAllowance,
                        a.BrokenShiftAllowanceExtra_Threshold,
                        a.BrokenshiftLimit,
                        a.BrokenShiftAllowanceExtra_Threshold1,
                        a.IncludePreviousDayInBrokenShiftCalculation,
                        a.BrokenShiftAllowance_PayType,
                        a.UnpaidMealBreakMin,
                        a.UnpaidMealBreakStartAt,
                        a.UnpaidMealBreak_PayType,
                        a.TeaBreakPaidTime,
                        a.TeaBreakStartAt,
                        a.TeaBreakPaidTimeNext,
                        a.TeaBreakStartAtNext,
                        a.TeaBreakPaidIncWkdHrs,
                        a.PayOvertimeForTeaBreaks,
                        a.CancellationPayThreshold,
                        a.MakeUp_Pay_Threshold,
                        a.MakeUpLeadTime,
                        a.NoNoticeCancelPayType,
                        a.ShtNoticeCancelPayType,
                        a.KMAllowanceType,


                    })
                    .ToListAsync();

                return Ok(awardRules);
            }
            catch (Exception ex)
            {
                // Log the exception details
                Console.WriteLine(ex.ToString());  // Use a proper logging framework in production
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }


        [HttpDelete("deleteAwardRuleSet/{recordNo}")]
        public async Task<IActionResult> deleteAwardRuleSet(int recordNo)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM AWARDPOS WHERE RecordNo = @recordNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPost("postAuditHistory")]
        public async Task<IActionResult> postAuditHistory([FromBody] AuditHistory audit)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser) VALUES (@operator,@actionDate,@auditDescription,@actionOn,@whoWhatCode,@tracsUser)", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@operator", audit.Operator);
                    cmd.Parameters.AddWithValue("@actionDate", audit.ActionDate);
                    cmd.Parameters.AddWithValue("@auditDescription", audit.AuditDescription);
                    cmd.Parameters.AddWithValue("@actionOn", audit.ActionOn);
                    cmd.Parameters.AddWithValue("@whoWhatCode", audit.WhoWhatCode);
                    cmd.Parameters.AddWithValue("@tracsUser", audit.TraccsUser);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpGet("getCodeList")]
        public async Task<IActionResult> getCodeList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT Description AS Description FROM DataDomains WHERE Domain = 'AWARDLEVEL' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Description = GenSettings.Filter(rd["Description"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getHcpClaimPreparationBatch")]
        public async Task<IActionResult> getHcpClaimPreparationBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, bch_date AS batchDate FROM batch_record WHERE bch_type = 'C' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getHcpAgingBatch")]
        public async Task<IActionResult> getHcpAgingBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, bch_date AS batchDate FROM batch_record WHERE bch_type = 'X' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getHcpClaimUploadBatch")]
        public async Task<IActionResult> getHcpClaimUploadBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, bch_date AS batchDate FROM batch_record WHERE bch_type = 'K' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackHcpClaimPreparationBatch")]
        public async Task<IActionResult> rollbackHcpClaimPreparationBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackHcpClaimPreparationBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackHcpAgingBatch")]
        public async Task<IActionResult> rollbackHcpAgingBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackHcpAgingBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackHcpClaimUploadBatch")]
        public async Task<IActionResult> rollbackHcpClaimUploadBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackHcpClaimUploadBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getHcpPackageClaimList")]
        public async Task<IActionResult> getHcpPackageClaimList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"Select RecordNumber AS recordNumber, Description AS claimType, User1 AS rate From DataDomains Where Domain = 'PACKAGERATES' ORDER BY DESCRIPTION", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recordNumber = GenSettings.Filter(rd["recordNumber"]),
                                claimType = GenSettings.Filter(rd["claimType"]),
                                rate = "$" + GenSettings.Filter(rd["rate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("changePackageClaimRates")]
        public async Task<IActionResult> changePackageClaimRates([FromBody] cdcPackageClaim data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_ChangePackageClaimRates_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@claimDescription", data.ClaimDescription);
                    cmd.Parameters.AddWithValue("@claimUser1", data.ClaimUser1);
                    cmd.Parameters.AddWithValue("@claimRecordNumber", data.ClaimRecordNumber);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("addCdcPackageClaim")]
        public async Task<IActionResult> addCdcPackageClaim([FromBody] cdcPackageClaim data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_AddCdcPackageClaim_I", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@claimDescription", data.ClaimDescription);
                    cmd.Parameters.AddWithValue("@claimUser1", data.ClaimUser1);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("hcpUpdateRecipientPrograms")]
        public async Task<IActionResult> hcpUpdateRecipientPrograms()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms " +
            " SET Quantity = (SELECT Convert(Money, BaseRate) + Convert(Money, ISNULL(HardshipSupplement, 0)) + Convert(Money, Dementia) + Convert(Money, Oxygen) + Convert(Money, EnteralFeedBolus) +  " +
            " Convert(Money, EnteralFeedNonBolus) + Convert(Money, EACHD) + Convert(Money, ARIA1) + Convert(Money, ARIA2) + Convert(Money, ARIA3) + Convert(Money, ARIA4) + Convert(Money, ARIA5) +  " +
            " Convert(Money, ARIA6) + Convert(Money, ARIA7) AS CurrentRate FROM ( SELECT Program, CASE WHEN ISNULL(PackageLevel, 'LEVEL 1') = 'LEVEL 1' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 1') WHEN ISNULL(PackageLevel, 'LEVEL 2') = 'LEVEL 2' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 2') WHEN ISNULL(PackageLevel, 'LEVEL 3') = 'LEVEL 3' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 3') WHEN ISNULL(PackageLevel, 'LEVEL 4') = 'LEVEL 4' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 4') END AS BaseRate, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 1, 5) <> '00000'  " +
            " THEN     CASE WHEN ISNULL(PackageLevel, 'LEVEL 1') = 'LEVEL 1' THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 1') " +
            " WHEN ISNULL(PackageLevel, 'LEVEL 2') = 'LEVEL 2' THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 2')  " +
            " WHEN ISNULL(PackageLevel, 'LEVEL 3') = 'LEVEL 3' THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 3') " +
            " WHEN ISNULL(PackageLevel, 'LEVEL 4') = 'LEVEL 4' THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 4') " +
            " End Else '0' END AS Dementia, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 6, 1) <> '0' THEN (SELECT User1 AS Oxygen FROM DataDomains  " +
            " WHERE Domain = 'PACKAGERATES' AND Description = 'OXYGEN') ELSE '0' END AS Oxygen, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 8, 1) <> '0'  " +
            " THEN (SELECT User1 AS EnteralBolus FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'ENTERAL FEED-BOLUS') ELSE '0' END AS EnteralFeedBolus,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 9, 1) <> '0' THEN (SELECT User1 AS EnteralNonBolus FROM DataDomains WHERE Domain = 'PACKAGERATES' AND  " +
            " Description = 'ENTERAL FEED-NONBOLUS') ELSE '0' END AS EnteralFeedNonBolus, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 10, 1) <> '0'  " +
            " THEN (SELECT User1 AS EACHD FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'EACHD TOP-UP') ELSE '0' END AS EACHD,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 12, 1) <> '0' THEN (SELECT User1 AS ARIA1 FROM DataDomains WHERE Domain = 'PACKAGERATES'  " +
            " AND Description = 'MMM 1') ELSE '0' END AS ARIA1, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 13, 1) <> '0' THEN (SELECT User1 AS ARIA2  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 2') ELSE '0' END AS ARIA2, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 14, 1) <> '0'  " +
            " THEN (SELECT User1 AS ARIA3 FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 3') ELSE '0' END AS ARIA3,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 15, 1) <> '0' THEN (SELECT User1 AS ARIA4 FROM DataDomains  " +
            " WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 4') ELSE '0' END AS ARIA4, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 16, 1) <> '0'  " +
            " THEN (SELECT User1 AS ARIA5 FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 5') ELSE '0' END AS ARIA5,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 17, 1) <> '0' THEN (SELECT User1 AS ARIA6  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 6') ELSE '0' END AS ARIA6, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 18, 1) <> '0'  " +
            " THEN (SELECT User1 AS ARIA7 FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 7') ELSE '0' END AS ARIA7 " +
            " FROM RecipientPrograms RP LEFT JOIN DataDomains DD ON Description = PackageLevel AND Domain = 'PACKAGERATES'  " +
            " WHERE rp.RecordNumber = RecipientPrograms.RecordNumber) t) - isnull(DailyIncomeTestedFee, 0)  " +
            " WHERE PackageType = 'CDC-HCP' AND ProgramStatus NOT IN ('INACTIVE') AND CHARINDEX('CONTINGENCY', Program) = 0", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("hcpUpdateQuotes")]
        public async Task<IActionResult> hcpUpdateQuotes([FromBody] string s_Filt)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE Qte_Hdr SET GovtContribution = 365 * (SELECT Convert(Money, BaseRate) + Convert(Money, ISNULL(HardshipSupplement, 0)) + Convert(Money, Dementia) + Convert(Money, Oxygen) +  " +
            " Convert(Money, EnteralFeedBolus) + Convert(Money, EnteralFeedNonBolus) + Convert(Money, EACHD) + Convert(Money, ARIA1) + Convert(Money, ARIA2) + Convert(Money, ARIA3) +  " +
            " Convert(Money, ARIA4) + Convert(Money, ARIA5) + Convert(Money, ARIA6) + Convert(Money, ARIA7) AS CurrentRate FROM ( SELECT [Name] AS Program, CASE WHEN ISNULL(pr.User3, 'LEVEL 1') = 'LEVEL 1'  " +
            " THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 1')      WHEN ISNULL(pr.User3, 'LEVEL 2') = 'LEVEL 2'  " +
            " THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 2') WHEN ISNULL(pr.User3, 'LEVEL 3') = 'LEVEL 3'  " +
            " THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 3')      WHEN ISNULL(pr.User3, 'LEVEL 4') = 'LEVEL 4'  " +
            " THEN (SELECT User1 AS DementiaSupplement FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'LEVEL 4') END AS BaseRate,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 1, 5) <> '00000' THEN     CASE WHEN ISNULL(pr.User3, 'LEVEL 1') = 'LEVEL 1' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 1')          WHEN ISNULL(pr.User3, 'LEVEL 2') = 'LEVEL 2' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 2')          WHEN ISNULL(pr.User3, 'LEVEL 3') = 'LEVEL 3' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 3')          WHEN ISNULL(pr.User3, 'LEVEL 4') = 'LEVEL 4' THEN (SELECT User1 AS DementiaSupplement  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'DEMENTIA/COGNITION/VET 4')     End Else '0' END AS Dementia,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 6, 1) <> '0' THEN (SELECT User1 AS Oxygen FROM DataDomains  " +
            " WHERE Domain = 'PACKAGERATES' AND Description = 'OXYGEN') ELSE '0' END AS Oxygen, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 8, 1) <> '0'  " +
            " THEN (SELECT User1 AS EnteralBolus FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'ENTERAL FEED-BOLUS') ELSE '0' END AS EnteralFeedBolus,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 9, 1) <> '0' THEN (SELECT User1 AS EnteralNonBolus FROM DataDomains WHERE Domain = 'PACKAGERATES' AND  " +
            " Description = 'ENTERAL FEED-NONBOLUS') ELSE '0' END AS EnteralFeedNonBolus, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 10, 1) <> '0'  " +
            " THEN (SELECT User1 AS EACHD FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'EACHD TOP-UP') ELSE '0' END AS EACHD,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 12, 1) <> '0' THEN (SELECT User1 AS ARIA1 FROM DataDomains WHERE Domain = 'PACKAGERATES'  " +
            " AND Description = 'MMM 1') ELSE '0' END AS ARIA1, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 13, 1) <> '0' THEN (SELECT User1 AS ARIA2  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 2') ELSE '0' END AS ARIA2, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 14, 1) <> '0'  " +
            " THEN (SELECT User1 AS ARIA3 FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 3') ELSE '0' END AS ARIA3,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 15, 1) <> '0' THEN (SELECT User1 AS ARIA4 FROM DataDomains WHERE Domain = 'PACKAGERATES'  " +
            " AND Description = 'MMM 4') ELSE '0' END AS ARIA4, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 16, 1) <> '0' THEN (SELECT User1 AS ARIA5  " +
            " FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 5') ELSE '0' END AS ARIA5, CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 17, 1) <> '0'  " +
            " THEN (SELECT User1 AS ARIA6 FROM DataDomains WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 6') ELSE '0' END AS ARIA6,  " +
            " CASE WHEN Substring(ISNULL(PackageSupplements, '000000000000000000'), 18, 1) <> '0' THEN (SELECT User1 AS ARIA7 FROM DataDomains  " +
            " WHERE Domain = 'PACKAGERATES' AND Description = 'MMM 7') ELSE '0' END AS ARIA7 FROM Qte_Hdr QH  " +
            " INNER JOIN HumanResourceTypes PR ON QH.ProgramID = PR.RecordNumber LEFT JOIN DataDomains DD ON Description = pr.User3 AND Domain = 'PACKAGERATES'  " +
            " WHERE QH.RecordNumber = Qte_Hdr.RecordNumber) t) - CLAssessedIncomeTestedFee  WHERE Qte_Hdr.RecordNumber IN  (SELECT QH.RecordNumber  FROM  " +
            " Documents D  LEFT JOIN Qte_Hdr QH ON CPID = DOC_ID  LEFT JOIN HumanResourceTypes PR ON QH.ProgramID = PR.RecordNumber  WHERE  ISNULL([Status], '') <> ''  AND PR.UserYesNo1 = 1   " +
            " + (@s_Filt) AND ISNULL(DeletedRecord, 0) = 0 )", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@s_Filt", s_Filt);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("hcpAdjustRostersCmPkg")]
        public async Task<IActionResult> hcpAdjustRostersCmPkg(string s_DateFilter)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_HCPAdjustRostersCmPkg_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@s_DateFilter", s_DateFilter);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getPeriodEndDate")]
        public async Task<IActionResult> getPeriodEndDate()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 Date FROM PACKAGEBALANCES PB INNER JOIN RECIPIENTS RE ON RE.SQLID = PB.PERSONID INNER JOIN HUMANRESOURCETYPES PR ON PR.RECORDNUMBER = PB.PROGRAMID  WHERE ISNULL(PR.USERYESNO1, 0) = 1 AND ISNULL(BatchNumber, 0) <> 0 AND ISNULL(BatchType, '') = 'CDC' ORDER BY pb.RecordNumber Desc", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                periodEndDate = GenSettings.Filter(rd["Date"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getHcpBulkBalances")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getHcpBulkBalances([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT PB.RECORDNUMBER, RE.ACCOUNTNO, PR.NAME, PB.DATE, PB.BALANCE, PB.COMMBALPROVIDER AS [CWealth (Held By Provider)], PB.COMMBALCOMMONWEALTH AS [CWealth (Held By CWealth)], PB.CLIENTBAL AS [Client (Held By Provider)], PB.COMMBALPROVIDER + PB.COMMBALCOMMONWEALTH + PB.CLIENTBAL AS CheckTotal, PB.Balance - (pb.COMMBALPROVIDER + pb.COMMBALCOMMONWEALTH + pb.CLIENTBAL) AS Variance FROM PACKAGEBALANCES PB INNER JOIN RECIPIENTS RE ON RE.SQLID = PB.PERSONID INNER JOIN HUMANRESOURCETYPES PR ON PR.RECORDNUMBER = PB.PROGRAMID WHERE ISNULL(PR.USERYESNO1, 0) = 1 AND ISNULL(BatchNumber, 0) <> 0 AND ISNULL(BatchType, '') = 'CDC' AND PB.[DATE] >= @startDate AND PB.[DATE] <= @endDate ORDER BY ACCOUNTNO", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recordNumber = GenSettings.Filter(rd["RECORDNUMBER"]),
                                accountNumber = GenSettings.Filter(rd["ACCOUNTNO"]),
                                name = GenSettings.Filter(rd["NAME"]),
                                date = GenSettings.Filter(rd["DATE"]),
                                balance = GenSettings.Filter(rd["BALANCE"]),
                                cWealthHeldByProvider = GenSettings.Filter(rd["CWealth (Held By Provider)"]),
                                cWealthHeldByCWealth = GenSettings.Filter(rd["CWealth (Held By CWealth)"]),
                                clientHeldByProvider = GenSettings.Filter(rd["Client (Held By Provider)"]),
                                checkTotal = GenSettings.Filter(rd["CheckTotal"]),
                                variance = GenSettings.Filter(rd["Variance"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }


        // [HttpGet("getHcpDataCheck")]
        // public async Task<IActionResult> getHcpDataCheck()
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         string query = string.Empty;

        //         using (SqlCommand cmd = new SqlCommand(@"SELECT '1' AS Dummy, * from ( SELECT Program as Package, [Client Code] AS [Recipient], ProgramStatus AS Status, CASE WHEN (FIRST_SERVICE < FIRST_ADMISSION AND LAST_SERVICE > LAST_DISCHARGE) THEN '101-Service BEFORE Admission, 102-Service AFTER Discharge' WHEN (FIRST_SERVICE < FIRST_ADMISSION) THEN '101-Service BEFORE Admission' WHEN (LAST_SERVICE > LAST_DISCHARGE AND LAST_ADMISSION > LAST_DISCHARGE) THEN '102-Service AFTER Discharge' WHEN FIRST_ADMISSION IS NULL THEN '103-Services with NO ADMISSION' END As Error, FIRST_ADMISSION As [First Admission],FIRST_SERVICE As [First Service], LAST_SERVICE As [Last Service], Last_DISCHARGE As [Last Discharge] FROM (SELECT RO.Program, RO.[Client Code], RP.ProgramStatus, MIN(CASE WHEN IT.MINORGROUP = 'ADMISSION' THEN [DATE] ELSE NULL END) AS FIRST_ADMISSION, MAX(CASE WHEN IT.MINORGROUP = 'ADMISSION' THEN [DATE] ELSE NULL END) AS LAST_ADMISSION, MAX(CASE WHEN IT.MINORGROUP = 'DISCHARGE' THEN [DATE] ELSE NULL END) AS LAST_DISCHARGE, MIN(CASE WHEN IT.MinorGroup NOT IN ('DISCHARGE', 'ADMISSION', 'REFERRAL-IN', 'NOT PROCEEDING', 'ASSESSMENT', 'DATA UPDATE', 'OTHER')    THEN [DATE] ELSE NULL END) AS FIRST_SERVICE, MAX(CASE WHEN IT.MinorGroup NOT IN ('DISCHARGE', 'ADMISSION', 'REFERRAL-IN', 'NOT PROCEEDING', 'ASSESSMENT', 'DATA UPDATE', 'OTHER')    THEN [DATE] ELSE NULL END) AS LAST_SERVICE FROM Roster RO LEFT JOIN Recipients RE ON RO.[Client Code] = RE.AccountNo INNER JOIN HumanResourceTypes PR ON RO.Program = PR.[Name] LEFT JOIN RecipientPrograms RP ON RE.UniqueID = RP.PersonID AND RP.Program = RO.Program INNER JOIN ItemTypes IT ON RO.[Service Type] = IT.Title AND IT.ProcessClassification = 'OUTPUT' WHERE [DATE] > '2011/01/01' AND ISNULL(PR.UserYesNo2, 0) = 1 AND ISNULL(PR.UserYesNo1, 0) = 1 AND UPPER(ISNULL(PR.User2, '')) <> 'CONTINGENCY' AND ISNULL(RP.ProgramStatus, '') IN ('ACTIVE', 'ON HOLD') AND [Client Code] <> '!INTERNAL' GROUP BY RO.Program, RO.[Client Code], RP.ProgramStatus ) AS TMP WHERE FIRST_SERVICE < FIRST_ADMISSION OR LAST_SERVICE > LAST_DISCHARGE OR FIRST_ADMISSION IS NULL UNION SELECT Program, 'MULTIPLE ACTIVE' AS Recipient, ProgramStatus, '203-' + Convert(nVarChar, ActiveCount) + ' Recipients ACTIVE in package' AS Error, '', '', '', '' FROM ( SELECT Program, ProgramStatus, Count(Program) AS ActiveCount FROM ( SELECT RP.[Program], RE.[Accountno] AS Client, ProgramStatus FROM RecipientPrograms RP INNER JOIN Recipients RE ON RP.PersonID = RE.UniqueID INNER JOIN HumanResourceTypes PR ON RP.[Program] = PR.[Name] AND ISNULL(PR.UserYesNo2, 0) = 1 AND ISNULL(PR.UserYesNo1, 0) = 1 WHERE ProgramStatus IN ('ACTIVE', 'ON HOLD') GROUP BY RP.Program, RE.Accountno, RP.ProgramStatus) e3 GROUP BY Program, ProgramStatus ) e4 WHERE ActiveCount > 1 UNION SELECT RP.Program, RE.Accountno AS Recipient, RP.ProgramStatus, '204-CLAIM RATE IS SET TO 0 IN CLIENTS PACKAGE' AS Error, '', '', '', '' FROM RecipientPrograms RP INNER JOIN Recipients RE ON RP.PersonID = RE.UniqueID INNER JOIN HumanResourceTypes PR ON RP.[Program] = PR.[Name] AND ISNULL(PR.UserYesNo2, 0) = 1 AND ISNULL(PR.UserYesNo1, 0) = 1 AND ISNULL(User2, '') <> 'Contingency' WHERE ProgramStatus IN ('ACTIVE', 'ON HOLD') AND convert(float, ISNULL(Quantity, '0')) = 0 UNION SELECT Program as Package, [Client Code] AS [Recipient], ProgramStatus, CASE WHEN ProgramStatus = 'INACTIVE' AND [FIRST_ADMISSION] IS NOT NULL AND [LAST_DISCHARGE] IS NULL THEN '301-NO Discharge for INACTIVE Package having an ADMISSION' WHEN ProgramStatus = 'ACTIVE' AND [FIRST_ADMISSION] IS NULL THEN '302-NO Admission for ACTIVE Package' END As Error, FIRST_SERVICE As [First Service], FIRST_ADMISSION As [First Admission], Last_DISCHARGE As [Last Discharge], LAST_SERVICE As [Last Service] FROM (SELECT RO.Program, RO.[Client Code], RP.ProgramStatus, MIN(CASE WHEN IT.MINORGROUP = 'ADMISSION' THEN [DATE] ELSE NULL END) AS FIRST_ADMISSION, MAX(CASE WHEN IT.MINORGROUP = 'DISCHARGE' THEN [DATE] ELSE NULL END) AS LAST_DISCHARGE, MIN(CASE WHEN IT.MinorGroup NOT IN ('DISCHARGE', 'ADMISSION', 'REFERRAL-IN', 'NOT PROCEEDING', 'ASSESSMENT', 'DATA UPDATE', 'OTHER') THEN [DATE] ELSE NULL END) AS FIRST_SERVICE, MAX(CASE WHEN IT.MinorGroup NOT IN ('DISCHARGE', 'ADMISSION', 'REFERRAL-IN', 'NOT PROCEEDING', 'ASSESSMENT', 'DATA UPDATE', 'OTHER') THEN [DATE] ELSE NULL END) AS LAST_SERVICE FROM Roster RO LEFT JOIN Recipients RE ON RO.[Client Code] = RE.AccountNo INNER JOIN HumanResourceTypes PR ON RO.Program = PR.[Name] LEFT JOIN RecipientPrograms RP ON RE.UniqueID = RP.PersonID AND RP.Program = RO.Program INNER JOIN ItemTypes IT ON RO.[Service Type] = IT.Title AND IT.ProcessClassification = 'OUTPUT' WHERE [DATE] > '2014/01/01' AND ISNULL(PR.UserYesNo1, 0) = 1 AND ISNULL(PR.UserYesNo2, 0) = 1 AND UPPER(ISNULL(PR.User2, '')) <> 'CONTINGENCY' AND ISNULL(RP.ProgramStatus, '') IN ('ACTIVE', 'ON HOLD') AND [Client Code] <> '!INTERNAL' GROUP BY RO.Program, RO.[Client Code], RP.ProgramStatus ) AS TMP WHERE ProgramStatus = 'INACTIVE' AND [FIRST_ADMISSION] IS NOT NULL AND [LAST_DISCHARGE] IS NULL OR ProgramStatus = 'ACTIVE' AND [FIRST_ADMISSION] IS NULL UNION SELECT Program, [Client Code] AS Recipient, 'N/A', '501-Multiple Services Records against same package without matching ADMISSION and DISCHARGE' AS Error, '','','','' FROM ( SELECT RO.Program, RO.[Client Code], [SERVICE TYPE], MIN(CASE WHEN MINORGROUP = 'ADMISSION' THEN [DATE] ELSE NULL END) AS FIRST_ADMISSION, MAX(CASE WHEN MINORGROUP = 'DISCHARGE' THEN [DATE] ELSE NULL END) AS LAST_DISCHARGE FROM Roster RO INNER JOIN HumanResourceTypes PR ON RO.Program = PR.[Name] AND PR.[GROUP] = 'PROGRAMS' INNER JOIN ItemTypes IT ON RO.[Service Type] = IT.Title AND IT.ProcessClassification = 'OUTPUT' WHERE [DATE] > '2011/01/01' AND ISNULL(PR.UserYesNo1, 0) = 1 AND ISNULL(PR.UserYesNo2, 0) = 1 AND UPPER(ISNULL(PR.User2, '')) <> 'CONTINGENCY' AND [Client Code] <> '!INTERNAL' GROUP BY RO.Program, RO.[Client Code], [SERVICE TYPE] ) AS TMP WHERE Exists (SELECT * FROM ROSTER R2 INNER JOIN ITEMTYPES I2 ON R2.[SERVICE TYPE] = I2.TITLE AND I2.ProcessClassification = 'OUTPUT' AND R2.[CLIENT CODE] <> TMP.[CLIENT CODE] AND R2.PROGRAM = TMP.PROGRAM AND R2.[SERVICE TYPE] = TMP.[SERVICE TYPE] AND I2.MINORGROUP = 'DISCHARGE' AND R2.[DATE] BETWEEN FIRST_ADMISSION AND LAST_DISCHARGE)  UNION SELECT Program, Recipient, Status, Error, 'n/a' AS [First Admission], 'n/a' AS [First Service], 'n/a' AS [Last Service], 'n/a' AS [Last Discharge] From ( SELECT RP.PROGRAM AS program, RE.ACCOUNTNO AS RECIPIENT, RP.PROGRAMSTATUS AS [Status], '001-Program record missing or deleted' AS Error FROM RECIPIENTPROGRAMS RP INNER JOIN RECIPIENTS RE ON RP.PERSONID = RE.UNIQUEID LEFT JOIN HUMANRESOURCETYPES PR ON RP.PROGRAM = PR.[NAME] WHERE ISNULL(PR.NAME, '') = '' AND PACKAGETYPE = 'CDC-HCP'  Union SELECT PR.[NAME] AS PROGRAM, 'N/A', 'N/A', '020-PROGRAM IS MISSING AT LEAST ONE CDC LEAVE TYPE' AS ERROR FROM  HUMANRESOURCES PL LEFT JOIN HUMANRESOURCETYPES PR ON PL.PERSONID = PR.RECORDNUMBER LEFT JOIN ITEMTYPES IT ON PL.NAME = IT.TITLE WHERE PL.[GROUP] = 'PROG_LEAVE' AND ISNULL(MINORGROUP, '') IN ( 'full day-hospital' ,'full day-respite' ,'full day-transition' ,'full day-social leave' ) GROUP BY PR.[NAME] HAVING COUNT(DISTINCT PL.NAME) < 4Union SELECT PR.NAME AS PROGRAM, RE.ACCOUNTNO AS RECIPIENT, RP.PROGRAMSTATUS AS [Status], '002-No contingency program linked to main package' AS Error FROM HUMANRESOURCETYPES PR INNER JOIN RECIPIENTPROGRAMS RP ON RP.Program = PR.NAME  INNER JOIN RECIPIENTS RE ON RP.PERSONID = RE.UNIQUEID WHERE USERYESNO1 = 1 AND ISNULL(USERYESNO3, 0) = 0 AND USER2 <> 'CONTINGENCY' AND ISNULL(USER10, '') = ''AND RP.ProgramStatus NOT IN ('REFERRAL', 'INACTIVE') Union SELECT PRIMARYPACKAGE AS PROGRAM, 'N/A', 'N/A', '003-The linked contingency package no longer exists or has been deleted' AS Error  FROM ( SELECT PR1.NAME AS PRIMARYPACKAGE, PR1.USERYESNO1, PR1.USER10 AS CP_ID, PR.NAME AS CONTINGENCYPACKAGE  FROM HUMANRESOURCETYPES PR RIGHT JOIN HUMANRESOURCETYPES PR1 ON PR1.USER10 = PR.RECORDNUMBER) T WHERE ISNULL(T.CP_ID, '') <> '' AND ISNULL(CONTINGENCYPACKAGE, '') = '' Union SELECT PRIMARYPACKAGE AS PROGRAM, 'N/A', 'N/A', '004-The linked contingency package is not setup as a contingency package' AS Error FROM ( SELECT PR1.NAME AS PRIMARYPACKAGE, PR1.USERYESNO1, PR1.USER10 AS CP_ID, PR.NAME AS CONTINGENCYPACKAGE, PR.USER2  FROM HUMANRESOURCETYPES PR RIGHT JOIN HUMANRESOURCETYPES PR1 ON PR1.USER10 = PR.RECORDNUMBER) T WHERE ISNULL(T.CP_ID, '') <> '' AND ISNULL(CONTINGENCYPACKAGE, '') <> '' AND ISNULL(USER2, '') <> 'CONTINGENCY' Union  SELECT top 1 '!INTERNAL', 'N/A', 'N/A', '005-The MANDATORY  !INTERNAL program is missing or deleted' AS Error From HUMANRESOURCETYPES where not exists (select [name] from humanresourcetypes where [name] = '!INTERNAL') Union SELECT PR.[NAME] AS PROGRAM, 'N/A', 'N/A', '006-The leave type ' + pl.name + ' recorded againt the package is not a valid leave type' AS Error FROM  HUMANRESOURCES PL LEFT JOIN HUMANRESOURCETYPES PR ON PL.PERSONID = PR.RECORDNUMBER LEFT JOIN ITEMTYPES IT ON PL.NAME = IT.TITLE WHERE PL.[GROUP] = 'PROG_LEAVE' AND LEFT(ISNULL(IT.MINORGROUP, 'XXXXXXXX') + 'XXXXXXXX', 8) <> 'FULL DAY' and isnull(pr.name, '') <> '' Union SELECT PROGRAM, 'N/A', 'N/A', CASE WHEN ISNULL(MINORGROUP, '') = 'FULL DAY-HOSPITAL' AND ISNULL(SUBTYPE, '') <> 'CONSECUTIVE DAYS' THEN '999-ACCUMULATION METHOD INCORRECT FOR ' + [PROGRAM] + ' ' + TITLE WHEN ISNULL(MINORGROUP, '') = 'FULL DAY-RESPITE' AND ISNULL(SUBTYPE, '') <> 'CUMULATIVE DAYS' THEN '999-ACCUMULATION METHOD INCORRECT FOR ' + [PROGRAM] + ' ' + TITLE WHEN ISNULL(MINORGROUP, '') = 'FULL DAY-TRANSITION' AND ISNULL(SUBTYPE, '') <> 'CONSECUTIVE DAYS' THEN '999-ACCUMULATION METHOD INCORRECT FOR ' + [PROGRAM] + ' ' + TITLE WHEN ISNULL(MINORGROUP, '') = 'FULL DAY-SOCIAL LEAVE' AND ISNULL(SUBTYPE, '') <> 'CUMULATIVE DAYS' THEN '999-ACCUMULATION METHOD INCORRECT FOR ' + [PROGRAM] + ' ' + TITLE WHEN ISNULL(SUBTYPE, '') NOT IN ('CUMULATIVE DAYS', 'CONSECUTIVE DAYS') OR ISNULL(USER3, '') <> '28' OR ISNULL(USER4, '') <> '25' THEN '999-THE LEAVE TYPE ' + SUBTYPE + ' IS INVALID OR THE SETUP IS INCORRECT - should be 25, 28, CONSECUTIVE DAYS OR CUMULATIVE DAYS' END As Error From (SELECT PR.[NAME] AS PROGRAM, IT.TITLE, IT.MINORGROUP, PL.SUBTYPE, PL.USER3, PL.USER4  FROM HUMANRESOURCES PL LEFT JOIN HUMANRESOURCETYPES PR ON PL.PERSONID = PR.RECORDNUMBER LEFT JOIN ITEMTYPES IT ON PL.NAME = IT.TITLE WHERE ISNULL(PR.USERYESNO1,0) = 1 AND PL.[GROUP] = 'PROG_LEAVE' ) TL ) TL1 where isnull(error, '') > '' AND isnull(status, '') <> 'REFERRAL'  ) t ORDER BY Package, Recipient", (SqlConnection)conn))
        //         {
        //             await conn.OpenAsync();
        //             using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
        //             {
        //                 List<dynamic> list = new List<dynamic>();

        //                 while (await rd.ReadAsync())
        //                 {
        //                     list.Add(new
        //                     {
        //                         package = GenSettings.Filter(rd["Package"]),
        //                         recipient = GenSettings.Filter(rd["Recipient"]),
        //                         status = GenSettings.Filter(rd["Status"]),
        //                         error = GenSettings.Filter(rd["Error"]),
        //                         firstAdmission = GenSettings.Filter(rd["First Admission"]),
        //                         firstService = GenSettings.Filter(rd["First Service"]),
        //                         lastService = GenSettings.Filter(rd["Last Service"]),
        //                         lastDischarge = GenSettings.Filter(rd["Last Discharge"])
        //                     });
        //                 }
        //                 return Ok(list);
        //             }
        //         }
        //     }
        // }

        [HttpPut("getHcpDataCheck")]
        public async Task<IActionResult> getHcpDataCheck()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetHcpDataCheck_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                package = GenSettings.Filter(rd["Package"]),
                                recipient = GenSettings.Filter(rd["Recipient"]),
                                status = GenSettings.Filter(rd["Status"]),
                                error = GenSettings.Filter(rd["Error"]),
                                firstAdmission = GenSettings.Filter(rd["First Admission"]),
                                firstService = GenSettings.Filter(rd["First Service"]),
                                lastService = GenSettings.Filter(rd["Last Service"]),
                                lastDischarge = GenSettings.Filter(rd["Last Discharge"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }


        [HttpGet("getHcpPkgAuditReport/{is_where}")]
        public async Task<IActionResult> getHcpPkgAuditReport(bool is_where = false)
        {
            string query = string.Empty;
            if (is_where)
            {
                query = " AND ((AdmissionDate IS NOT NULL) AND (DischargeDate IS NULL)) ORDER BY RE.Accountno";
            }
            else
            {
                query = " ORDER BY RE.Accountno";
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RP.RecordNumber AS RecordNumber, RE.AccountNo AS AccountNo, RP.Program AS Package, RP.ProgramStatus as Status, RP.PackageLevel as Level, RP.Quantity AS DailyRate, (SELECT TOP 1 DATE FROM ROSTER INNER JOIN ITEMTYPES IT ON Title = [Service Type] WHERE Program = RP.Program AND [Client Code] = RE.AccountNo AND IT.MinorGroup = 'ADMISSION') AS [Admission Date],(SELECT TOP 1 DATE FROM ROSTER INNER JOIN ITEMTYPES IT ON Title = [Service Type] WHERE Program = RP.Program AND [Client Code] = RE.AccountNo AND IT.MinorGroup = 'DISCHARGE') AS [Discharge Date],RP.TimeUnit AS ClaimFrequency,  RP.DailyBasicCareFee AS [Daily Basic Fee], ISNull(RP.DailyIncomeTestedFee, 0) AS [Daily Income Tested Fee], ISNull(RP.DailyAgreedTopUp, 0) as [Daily Top Up Fee], IsNull(RP.Contingency_Start, '0') as [Contingency Rate/Amount], RP.Contingency AS [Max Contingency] FROM RecipientPrograms RP INNER JOIN Recipients RE ON RP.PersonID = RE.UniqueID WHERE PackageType = 'CDC-HCP' AND Right(Program, 11) <> 'CONTINGENCY' " + query, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            accountNo = GenSettings.Filter(rd["AccountNo"]),
                            package = GenSettings.Filter(rd["Package"]),
                            status = GenSettings.Filter(rd["Status"]),
                            level = GenSettings.Filter(rd["Level"]),
                            dailyRate = GenSettings.Filter(rd["DailyRate"]),
                            admissionDate = GenSettings.Filter(rd["Admission Date"]),
                            dischargeDate = GenSettings.Filter(rd["Discharge Date"]),
                            claimFrequency = GenSettings.Filter(rd["ClaimFrequency"]),
                            dailyBasicFee = GenSettings.Filter(rd["Daily Basic Fee"]),
                            dailyIncomeTestedFee = GenSettings.Filter(rd["Daily Income Tested Fee"]),
                            dailyTopUpFee = GenSettings.Filter(rd["Daily Top Up Fee"]),
                            contingencyRateAmount = GenSettings.Filter(rd["Contingency Rate/Amount"]),
                            maxContingency = GenSettings.Filter(rd["Max Contingency"])
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("getHcpPkgStatusSummary/{is_where}")]
        public async Task<IActionResult> getHcpPkgStatusSummary(bool is_where = false)
        {
            string query = string.Empty;
            if (is_where)
            {
                query = " ) T1 ORDER BY [Package ID]";
            }
            else
            {
                query = " AND (EndDate Is Null OR EndDate >= '03-20-2024') ) T1 ORDER BY [Package ID] ";
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT *, LEFT('oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo', ROund([Last Balance], 0) / 1000) FROM (SELECT DISTINCT P.[RecordNumber] As [RecordNumber], P.[Name] As [Package ID], CASE WHEN P.UserYesNo1 = 1 THEN 'CDC-HCP' ELSE RP.PackageType END AS [Type], CASE WHEN ISNULL(P.[User3], '') = '' THEN '-'  ELSE P.[User3] END AS [Level], CASE WHEN RP.[ProgramStatus] IN ('ACTIVE','ON HOLD','WAITING LIST') THEN 'OCCUPIED' WHEN RP.[ProgramStatus] = 'REFERRAL' THEN 'ON REFERRAL'  ELSE 'VACANT' END AS [Status], CASE WHEN RP.[ProgramStatus] IN ('ACTIVE','ON HOLD') THEN r.[accountno] WHEN RP.[ProgramStatus] IN ('REFERRAL') THEN '' ELSE '' END AS [Allocated To], P.[User4] As [Target Group], P.[GST] As [GST],P.[Fax] As [GL Exp A/C], P.[Email] As [GL Rev A/C], (SELECT TOP 1 Balance FROM PackageBalances pb WHERE pb.programid = P.RecordNumber) AS [Last Balance] FROM HumanResourceTypes P LEFT JOIN RecipientPrograms RP ON P.[Name] = RP.[Program] LEFT JOIN Recipients R ON RP.[PersonID] = R.[UniqueID] WHERE (P.[Group] = 'PROGRAMS' AND RP.[ProgramStatus] <> 'INACTIVE' AND P.UserYesNo1 = 1 AND IsNull(User2, '') <> 'CONTINGENCY' AND IsNull(UserYesNo3, 0) = 0) " + query, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            packageID = GenSettings.Filter(rd["Package ID"]),
                            type = GenSettings.Filter(rd["Type"]),
                            level = GenSettings.Filter(rd["Level"]),
                            status = GenSettings.Filter(rd["Status"]),
                            allocatedTo = GenSettings.Filter(rd["Allocated To"]),
                            targetGroup = GenSettings.Filter(rd["Target Group"]),
                            gST = GenSettings.Filter(rd["GST"]),
                            gLExpAC = GenSettings.Filter(rd["GL Exp A/C"]),
                            gLRevAC = GenSettings.Filter(rd["GL Rev A/C"]),
                            lastBalance = GenSettings.Filter(rd["Last Balance"])
                        });
                    }
                    return Ok(list);
                }
            }
        }

        // [HttpGet("getHcpPkgStatusSummary")]
        // public async Task<IActionResult> getHcpPkgStatusSummary()
        // {
        //     using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         string query = string.Empty;

        //         using (SqlCommand cmd = new SqlCommand(@"SELECT *, LEFT('oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo', ROund([Last Balance], 0) / 1000) FROM (SELECT DISTINCT P.[RecordNumber] As [RecordNumber], P.[Name] As [Package ID], CASE WHEN P.UserYesNo1 = 1 THEN 'CDC-HCP' ELSE RP.PackageType END AS [Type], CASE WHEN ISNULL(P.[User3], '') = '' THEN '-'  ELSE P.[User3] END AS [Level], CASE WHEN RP.[ProgramStatus] IN ('ACTIVE','ON HOLD','WAITING LIST') THEN 'OCCUPIED' WHEN RP.[ProgramStatus] = 'REFERRAL' THEN 'ON REFERRAL'  ELSE 'VACANT' END AS [Status], CASE WHEN RP.[ProgramStatus] IN ('ACTIVE','ON HOLD') THEN r.[accountno] WHEN RP.[ProgramStatus] IN ('REFERRAL') THEN '' ELSE '' END AS [Allocated To], P.[User4] As [Target Group], P.[GST] As [GST],P.[Fax] As [GL Exp A/C], P.[Email] As [GL Rev A/C], (SELECT TOP 1 Balance FROM PackageBalances pb WHERE pb.programid = P.RecordNumber) AS [Last Balance] FROM HumanResourceTypes P LEFT JOIN RecipientPrograms RP ON P.[Name] = RP.[Program] LEFT JOIN Recipients R ON RP.[PersonID] = R.[UniqueID] WHERE (P.[Group] = 'PROGRAMS' AND RP.[ProgramStatus] <> 'INACTIVE' AND P.UserYesNo1 = 1 AND IsNull(User2, '') <> 'CONTINGENCY' AND IsNull(UserYesNo3, 0) = 0) AND (EndDate Is Null OR EndDate >= '03-19-2024') ) T1 ORDER BY [Package ID] ", (SqlConnection)conn))
        //         {
        //             await conn.OpenAsync();
        //             using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
        //             {
        //                 List<dynamic> list = new List<dynamic>();

        //                 while (await rd.ReadAsync())
        //                 {
        //                     list.Add(new
        //                     {
        //                         packageID = GenSettings.Filter(rd["Package ID"]),
        //                         type = GenSettings.Filter(rd["Type"]),
        //                         level = GenSettings.Filter(rd["Level"]),
        //                         status = GenSettings.Filter(rd["Status"]),
        //                         allocatedTo = GenSettings.Filter(rd["Allocated To"]),
        //                         targetGroup = GenSettings.Filter(rd["Target Group"]),
        //                         gST = GenSettings.Filter(rd["GST"]),
        //                         gLExpAC = GenSettings.Filter(rd["GL Exp A/C"]),
        //                         gLRevAC = GenSettings.Filter(rd["GL Rev A/C"]),
        //                         lastBalance = GenSettings.Filter(rd["Last Balance"])
        //                     });
        //                 }
        //                 return Ok(list);
        //             }
        //         }
        //     }
        // }

        [HttpPost("getUnapprovedClientHours")]
        // DateTime dtStart, DateTime dtClose, bool is_where = false
        public async Task<IActionResult> getUnapprovedClientHours([FromBody] ActivePeriodDto period)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT RecordNo AS RecordNumber, [Client Code] as Recipient, [Date] AS Date, [Start Time] AS StartTime, [Service Type] AS ServiceType, [Carer Code] AS Staff FROM Roster ro INNER JOIN HUmanResourceTypes pr ON ro.program = pr.[name] WHERE [Date] BETWEEN convert(varchar,@startDate,111) AND convert(varchar,@endDate,111) AND [Client Code] > '!MULTIPLE' AND ro.STatus < 2 AND ro.Type <> 13 ORDER BY [Date], [Start Time], [Client Code]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", period.DateStart);
                    cmd.Parameters.AddWithValue("@endDate", period.DateEnd);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                Recipient = GenSettings.Filter(rd["Recipient"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["StartTime"]),
                                ServiceType = GenSettings.Filter(rd["ServiceType"]),
                                Staff = GenSettings.Filter(rd["Staff"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getTableRegistration")]
        public async Task<IActionResult> getTableRegistration()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                using (SqlCommand cmd = new SqlCommand(@"SELECT DefaultPayPeriod as DefaultPayPeriod FROM Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                defaultPayPeriod = GenSettings.Filter(rd["DefaultPayPeriod"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("getPayIntegrityList")]
        public async Task<IActionResult> getPayIntegrityList([FromBody] TravelUpdate group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetPayIntegrityList_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@startdate", group.StartDate);
                    cmd.Parameters.AddWithValue("@enddate", group.EndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                CreatedBy = GenSettings.Filter(rd["CreatedBy"]),
                                Editer = GenSettings.Filter(rd["Editer"]),
                                ShiftNumber = GenSettings.Filter(rd["Shift#"]),
                                Detail = GenSettings.Filter(rd["Detail"]),
                                ErrorType = GenSettings.Filter(rd["ErrorType"]),
                                Status = GenSettings.Filter(rd["Status"]),
                                Category = GenSettings.Filter(rd["Category"]),
                                Branch = GenSettings.Filter(rd["Branch"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Program = GenSettings.Filter(rd["Program"]),
                                StartTime = GenSettings.Filter(rd["Start Time"]),
                                xDeletedRecord = GenSettings.Filter(rd["xDeletedRecord"]),
                                xEndDate = GenSettings.Filter(rd["xEndDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getPaysBatchHistory")]
        public async Task<IActionResult> getPaysBatchHistory()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                using (SqlCommand cmd = new SqlCommand(@"SELECT   CASE  WHEN BCH_TYPE = 'B' THEN 'BILLING NON HCP'  WHEN BCH_TYPE = 'P' THEN 'PAY'  WHEN BCH_TYPE = 'C' THEN 'HCP PREP'  WHEN BCH_TYPE = 'X' THEN 'BILLING HCP'  WHEN BCH_TYPE = 'N' THEN 'NDIA CLAIM'  WHEN BCH_TYPE = 'I' THEN 'NDIA AGING'  WHEN BCH_TYPE = 'G' THEN 'HCP GENERIC'  WHEN BCH_TYPE = 'D' THEN 'DEX UPLOAD'  WHEN BCH_TYPE = 'K' THEN 'HCP CLAIM'  WHEN BCH_TYPE = 'M' THEN 'NDIA ROLLED BACK'  WHEN BCH_TYPE = 'S' THEN 'BILLING ROLLED BACK'  Else 'UNKNOWN'  END AS Type,  OperatorID AS OperatorID, BatchDate AS BatchDate, BatchNumber AS BatchNumber, COnvert(Varchar (4000), BatchDetail) as BatchDetail from pay_bill_batch B1 INNER JOIN batch_record B2 on b1.batchnumber  = b2.BCH_NUM  WHERE BATCHDATE > '2022/01/01' AND bch_type in  ('P','Q') ORDER BY convert(int, batchnumber) DESC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                type = GenSettings.Filter(rd["Type"]),
                                operatorID = GenSettings.Filter(rd["OperatorID"]),
                                batchDate = GenSettings.Filter(rd["BatchDate"]),
                                batchNumber = GenSettings.Filter(rd["BatchNumber"]),
                                batchDetail = GenSettings.Filter(rd["BatchDetail"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getBillingBatchHistory")]
        public async Task<IActionResult> getBillingBatchHistory()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;
                using (SqlCommand cmd = new SqlCommand(@"SELECT   CASE  WHEN BCH_TYPE = 'B' THEN 'BILLING NON HCP'  WHEN BCH_TYPE = 'P' THEN 'PAY'  WHEN BCH_TYPE = 'C' THEN 'HCP PREP'  WHEN BCH_TYPE = 'X' THEN 'BILLING HCP'  WHEN BCH_TYPE = 'N' THEN 'NDIA CLAIM'  WHEN BCH_TYPE = 'I' THEN 'NDIA AGING'  WHEN BCH_TYPE = 'G' THEN 'HCP GENERIC'  WHEN BCH_TYPE = 'D' THEN 'DEX UPLOAD'  WHEN BCH_TYPE = 'K' THEN 'HCP CLAIM'  WHEN BCH_TYPE = 'M' THEN 'NDIA ROLLED BACK'  WHEN BCH_TYPE = 'S' THEN 'BILLING ROLLED BACK'  Else 'UNKNOWN'  END AS [Type],  OperatorID, BatchDate AS [Batch Date], BatchNumber as Batch#, COnvert(Varchar (4000), BatchDetail) as Detail from pay_bill_batch B1 INNER JOIN batch_record B2 on b1.batchnumber  = b2.BCH_NUM  WHERE BATCHDATE > '2022/01/01' AND bch_type in  ('B','S') ORDER BY convert(int, batchnumber) DESC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                type = GenSettings.Filter(rd["Type"]),
                                operatorID = GenSettings.Filter(rd["OperatorID"]),
                                batchDate = GenSettings.Filter(rd["Batch Date"]),
                                batchNumber = GenSettings.Filter(rd["Batch#"]),
                                batchDetail = GenSettings.Filter(rd["Detail"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("getBillingIntegrityList")]
        public async Task<IActionResult> getBillingIntegrityList([FromBody] TravelUpdate group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_GetPayIntegrityList_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@startdate", group.StartDate);
                    cmd.Parameters.AddWithValue("@enddate", group.EndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                CreatedBy = GenSettings.Filter(rd["CreatedBy"]),
                                Editer = GenSettings.Filter(rd["Editer"]),
                                ShiftNumber = GenSettings.Filter(rd["Shift#"]),
                                ErrorType = GenSettings.Filter(rd["ErrorType"]),
                                Status = GenSettings.Filter(rd["Status"]),
                                Category = GenSettings.Filter(rd["Category"]),
                                Branch = GenSettings.Filter(rd["Branch"]),
                                Staff = GenSettings.Filter(rd["Staff"]),
                                Date = GenSettings.Filter(rd["Date"]),
                                StartTime = GenSettings.Filter(rd["StartTime"]),
                                Program = GenSettings.Filter(rd["Program"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getNdiaClaimBatch")]
        public async Task<IActionResult> getNdiaClaimBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, bch_date AS batchDate FROM batch_record WHERE bch_type = 'N' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackNdiaClaimBatch")]
        public async Task<IActionResult> rollbackNdiaClaimBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackNdiaClaimBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("getNdiaAgingBatch")]
        public async Task<IActionResult> getNdiaAgingBatch()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT bch_num AS batchNumber, bch_user AS batchUser, bch_date AS batchDate FROM batch_record WHERE bch_type = 'I' OR bch_type = 'O' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchNumber = GenSettings.Filter(rd["batchNumber"]),
                                batchUser = GenSettings.Filter(rd["batchUser"]),
                                batchDate = GenSettings.Filter(rd["batchDate"]),
                                batchDetail = GenSettings.Filter(rd["batchNumber"]) + " - " + GenSettings.Filter(rd["batchUser"]) + " - " + GenSettings.Filter(rd["batchDate"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("rollbackNdiaAgingBatch")]
        public async Task<IActionResult> rollbackNdiaAgingBatch([FromBody] RollbackBatch group)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_RollbackNdiaAgingBatch_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@operatorID", group.OperatorID);
                    cmd.Parameters.AddWithValue("@currentDateTime", group.CurrentDateTime);
                    cmd.Parameters.AddWithValue("@batchNumber", group.BatchNumber);
                    cmd.Parameters.AddWithValue("@batchDescription", group.BatchDescription);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("getPackageBalancesDate")]
        public async Task<IActionResult> getPackageBalancesDate()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string query = string.Empty;

                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 [Date] AS Date FROM PackageBalances ORDER BY [Date] DESC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                packageBalancesDate = GenSettings.Filter(rd["Date"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getActivitiesAfter")]
        public async Task<IActionResult> getActivitiesAfter([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT UPPER([Title]) AS Title FROM ItemTypes WHERE [ProcessClassification] IN ('OUTPUT','EVENT','ITEM') AND (EndDate Is Null OR EndDate >= '" + data.CurrentDate + "')", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                activityTitle = GenSettings.Filter(rd["Title"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getFundingSources")]
        public async Task<IActionResult> getFundingSources([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT UPPER([DESCRIPTION]) AS DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'FUNDINGBODIES'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                fundingDescription = GenSettings.Filter(rd["DESCRIPTION"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getProgramsAfter")]
        public async Task<IActionResult> getProgramsAfter([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT UPPER([Name]) AS NAME FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= '" + data.CurrentDate + "') AND ISNULL(UserYesNo3, 0) = 0 AND IsNull(User2, '') <> 'Contingency'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                programName = GenSettings.Filter(rd["NAME"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getCategories")]
        public async Task<IActionResult> getCategories([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT UPPER(Description) AS Description FROM DataDomains WHERE [Domain] = 'GROUPAGENCY'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                categoryDescription = GenSettings.Filter(rd["Description"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getRecipientsGroups")]
        public async Task<IActionResult> getRecipientsGroups([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT UPPER(Description) AS Description FROM DataDomains WHERE [Domain] = 'RECIPTYPE'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recipientsDescription = GenSettings.Filter(rd["Description"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getSPackagesDetail")]
        public async Task<IActionResult> getSPackagesDetail([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT sPackage FROM ( SELECT RE.AccountNo, RP.Program AS Package, Program + '<==>' + Accountno AS sPackage, PR.[Type] AS PackageType, (SELECT TOP 1 DATE FROM ROSTER INNER JOIN ITEMTYPES IT ON Title = [Service Type] WHERE Program = RP.Program AND [Client Code] = RE.AccountNo AND IT.MinorGroup = 'ADMISSION') AS [AdmissionDate], (SELECT TOP 1 DATE FROM ROSTER INNER JOIN ITEMTYPES IT ON Title = [Service Type] WHERE Program = RP.Program AND [Client Code] = RE.AccountNo AND IT.MinorGroup = 'DISCHARGE') AS [DischargeDate] FROM RecipientPrograms RP INNER JOIN Recipients RE ON RP.PersonID = RE.UniqueID INNER JOIN HumanResourceTypes PR ON RP.Program = PR.Name WHERE ISNULL(UserYesNo1, 0) = 0) t WHERE (ISNULL(AdmissionDate, '2100/12/31') <= '2022/12/22') AND ((ISNULL(DischargeDate, '2100/12/31') > ISNULL(AdmissionDate, '2100/12/31') AND (ISNULL(DischargeDate, '2100/12/31') > '2022/12/22'))) AND PackageType = 'NDIA' ORDER BY sPackage", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                sPackages = GenSettings.Filter(rd["sPackage"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getBatchRecord")]
        public async Task<IActionResult> getBatchRecord([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT * FROM batch_record WHERE bch_num = (select max(bch_num) from batch_record)", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                batchRecordNumber = GenSettings.Filter(rd["bch_num"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("insertBatchRecord")]
        public async Task<IActionResult> insertBatchRecord([FromBody] InsertRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO batch_record (BCH_NUM, BCH_TYPE, BCH_USER, BCH_DATE) VALUES (@actionCode, @actionDescription, @Operator, @actionDate)", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@actionCode", data.actionCode);
                    cmd.Parameters.AddWithValue("@actionDescription", data.actionDescription);
                    cmd.Parameters.AddWithValue("@Operator", data.Operator);
                    cmd.Parameters.AddWithValue("@actionDate", data.actionDate);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPost("getSystableRecord")]
        public async Task<IActionResult> getSystableRecord([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT * FROM systable", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                systableSQLID = GenSettings.Filter(rd["SQLID"]),
                                systableCustomerID = GenSettings.Filter(rd["CustomerID"]),
                                systableSupplierID = GenSettings.Filter(rd["SupplierID"]),
                                systableItemID = GenSettings.Filter(rd["ItemID"]),
                                systableAccountID = GenSettings.Filter(rd["AccountID"]),
                                systableTsheetID = GenSettings.Filter(rd["TsheetID"]),
                                systableTsheetNum = GenSettings.Filter(rd["TsheetNum"]),
                                systableCarerID = GenSettings.Filter(rd["CarerID"]),
                                systableBatchNo = GenSettings.Filter(rd["Batch#"]),
                                systableInvoiceNo = GenSettings.Filter(rd["Invoice#"]),
                                systablePayPeriodEndDate = GenSettings.Filter(rd["PayPeriodEndDate"]),
                                systableTSheetFirstServiceDate = GenSettings.Filter(rd["TSheetFirstServiceDate"]),
                                systableTSheetLastServiceDate = GenSettings.Filter(rd["TSheetLastServiceDate"]),
                                systableReceipt = GenSettings.Filter(rd["Receipt"]),
                                systableAdjust = GenSettings.Filter(rd["Adjust"]),
                                systableCredit = GenSettings.Filter(rd["Credit"]),
                                systableQuote = GenSettings.Filter(rd["Quote"]),
                                systableSOrder = GenSettings.Filter(rd["SOrder"]),
                                systablePOrder = GenSettings.Filter(rd["POrder"]),
                                systablePInvoice = GenSettings.Filter(rd["PInvoice"]),
                                systableRemindersLastProcessed = GenSettings.Filter(rd["RemindersLastProcessed"]),
                                systableNDISClaimNumber = GenSettings.Filter(rd["NDISClaimNumber"]),
                                systableDMTransID = GenSettings.Filter(rd["DMTransID"]),
                                systableLinkId = GenSettings.Filter(rd["LinkId"]),
                                systablexDeletedRecord = GenSettings.Filter(rd["xDeletedRecord"]),
                                systablexEndDate = GenSettings.Filter(rd["xEndDate"]),
                                systableAward_Date = GenSettings.Filter(rd["Award_Date"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("insertPayBillBatch")]
        public async Task<IActionResult> insertPayBillBatch([FromBody] InsertRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO PAY_BILL_BATCH (OperatorID, BatchDate, BatchNumber, BatchDetail, BatchType, Date1, Date2) VALUES (@Operator, @actionDate, @actionCode, @actionOn, @actionDescription, @actionStartDate, @actionEndDate)", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@Operator", data.Operator);
                    cmd.Parameters.AddWithValue("@actionDate", data.actionDate);
                    cmd.Parameters.AddWithValue("@actionCode", data.actionCode);
                    cmd.Parameters.AddWithValue("@actionOn", data.actionOn);
                    cmd.Parameters.AddWithValue("@actionDescription", data.actionDescription);
                    cmd.Parameters.AddWithValue("@actionStartDate", data.actionStartDate);
                    cmd.Parameters.AddWithValue("@actionEndDate", data.actionEndDate);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("processAgePackageBalance")]
        public async Task<IActionResult> processAgePackageBalance([FromBody] InsertRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_AgePackageBalance_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@sBatchNo", data.actionCode);
                    cmd.Parameters.AddWithValue("@packageType", data.actionOn);
                    cmd.Parameters.AddWithValue("@packageBalancesDate", data.otherDate);
                    cmd.Parameters.AddWithValue("@startDate", data.actionStartDate);
                    cmd.Parameters.AddWithValue("@endDate", data.actionEndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("processBulkPriceUpdate")]
        public async Task<IActionResult> processBulkPriceUpdate([FromBody] UpdateRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_BulkPriceUpdate_U", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@batchNumber", data.actionCode);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                updatedRecords = GenSettings.Filter(rd["updatedRecords"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }
        public class AwardParameter
        {
            public string User { get; set; }
            public string Password { get; set; }

            public string s_StaffCode { get; set; }
            public string WEDate { get; set; }
        }


        [HttpPost, Route("award/interpreter")] //AWARD DLL
        public async Task<IActionResult> postAwardInterpreter([FromBody] AwardParameter values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/InterpretAwards?User=" + values.User + "&Password=" + values.Password + "&s_StaffCode=" + values.s_StaffCode + "&WEDate=" + values.WEDate + "&b_IncludeAbsences=True";

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }

        [HttpPost("getBankDetails")]
        public async Task<IActionResult> getBankDetails([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT TOP 1 BankName, BankAccountName, BankBSB, BankAccountNumber FROM Registration", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                bankName = GenSettings.Filter(rd["BankName"]),
                                bankAccountName = GenSettings.Filter(rd["BankAccountName"]),
                                bankBSB = GenSettings.Filter(rd["BankBSB"]),
                                bankAccountNumber = GenSettings.Filter(rd["BankAccountNumber"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getServiceProviderID")]
        public async Task<IActionResult> getServiceProviderID([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"select distinct Address1 from HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND UserYesNo1 = 1", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                providerList = GenSettings.Filter(rd["Address1"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getBranchesDetail")]
        public async Task<IActionResult> getBranchesDetail([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT Description FROM DataDomains WHERE Domain = 'BRANCHES' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                branchDescription = GenSettings.Filter(rd["Description"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("getRecipientsTypeDoha")]
        public async Task<IActionResult> getRecipientsTypeDoha([FromBody] BillingParameters data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand($"SELECT DISTINCT re.Accountno FROM Recipients re INNER JOIN RecipientPrograms rp ON rp.PersonID = re.Uniqueid INNER JOIN HumanResourceTypes pr ON rp.Program = pr.[Name] WHERE pr.type = 'DOHA' AND re.AccountNo > '' ORDER BY Accountno", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {

                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                recipientTitle = GenSettings.Filter(rd["Accountno"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPut("updatePackageBalance")]
        public async Task<IActionResult> updatePackageBalance([FromBody] UpdateRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"UPDATE PACKAGEBALANCES SET BALANCE = @BALANCE, COMMBALPROVIDER = @COMMBALPROVIDER, COMMBALCOMMONWEALTH = @COMMBALCOMMONWEALTH, CLIENTBAL = @CLIENTBAL WHERE RECORDNUMBER = @RECORDNUMBER", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@BALANCE", data.value1);
                    cmd.Parameters.AddWithValue("@COMMBALPROVIDER", data.value2);
                    cmd.Parameters.AddWithValue("@COMMBALCOMMONWEALTH", data.value3);
                    cmd.Parameters.AddWithValue("@CLIENTBAL", data.value4);
                    cmd.Parameters.AddWithValue("@RECORDNUMBER", data.recordNumber);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        // public string getLoggedInUser()
        // {
        //     WindowsIdentity identity = WindowsIdentity.GetCurrent();
        //     return identity != null ? identity.Name : "No user logged in";
        // }

        // [HttpGet("getCurrentUser")]
        // public ActionResult<string> GetCurrentUser()
        // {
        //     WindowsIdentity identity = WindowsIdentity.GetCurrent();
        //     if (identity != null)
        //     {
        //         return Ok(identity.Name);  
        //     }
        //     return NotFound("User not found");
        // }

        [HttpGet("getCurrentUser")]
        public ActionResult<string> GetCurrentUser()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            if (identity != null)
            {
                string[] nameParts = identity.Name.Split('\\');
                string computerName = nameParts.Length > 1 ? nameParts[0] : "Unknown";
                string userName = nameParts.Length > 1 ? nameParts[1] : nameParts[0];

                return Ok(new { computerName = computerName, userName = userName });
            }
            return NotFound("User not found");
        }

        [HttpPut("getPrintEmailInvoicesList")]
        public async Task<IActionResult> getPrintEmailInvoicesList([FromBody] UpdateRecord data)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_PrintEmailInvoices_S", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@startdate", data.actionStartDate);
                    cmd.Parameters.AddWithValue("@enddate", data.actionEndDate);
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                branch = GenSettings.Filter(rd["Branch"]),
                                date = GenSettings.Filter(rd["Date"]),
                                invoiceNumber = GenSettings.Filter(rd["Invoice#"]),
                                recipient = GenSettings.Filter(rd["Recipient"]),
                                amount = GenSettings.Filter(rd["Amount"]),
                                batchNumber = GenSettings.Filter(rd["BatchNumber"]),
                                type = GenSettings.Filter(rd["Type"]),
                                notes = GenSettings.Filter(rd["Notes"])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }
    }
}
