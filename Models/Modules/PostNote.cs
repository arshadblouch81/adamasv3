using System;

namespace Adamas.Models.Modules
{
    public class PostNote
    {
        public string PersonID { get; set; }
        public string Notes { get; set; }
        public bool? IsPrivate { get; set; }
        public bool? PrivateFlag { get; set; }
        public bool? PublishToApp { get; set; } 
        public bool? PublishToClientPortal { get; set; }
        public string Restrictions { get; set; }
        public string RestrictionsStr { get; set; }
        public DateTime? AlarmDate { get; set; }
        public string WhoCode { get; set; }
        public string Program { get; set; }
        public string Discipline { get; set; }
        public string CareDomain { get; set; }
        public string Category { get; set; }
        public int? RecordNumber { get; set; }
        public string Creator { get; set; }
        public string type { get; set; }
        public string GroupID { get; set; }
    }
}
