import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';


@Component({
    styles: [`
        nz-table{
            margin-top:0px;
        }
        nz-select{
            width:100%;
        }
        .spinner{
            margin:1rem auto;
            width:1px;
        }
        nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }
        tbody  tr:nth-child(odd) {
            background-color: #E9F7FF;
        }
        tbody  tr:nth-child(even) {
                background-color: #fff;
        }
    `],
    templateUrl: './position.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffPositionAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    modalOpen: boolean = false;
    isLoading: boolean = false;
    loading: boolean = false;
    inputForm: FormGroup;
    tableData: Array<any>;
    lists: Array<any>;

    dateFormat: string = 'MMM dd yyyy';
    editOrAdd: number;
    tocken: any;
    pdfTitle: string;
    selectedStaff: any;
    tryDoctype: any;
    drawerVisible: boolean =  false;
     

    selectedRow  : number = 0;
    activeRowData:any;


    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private printS:PrintService,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/staff/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'position')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.user = this.sharedS.getPicked();
        if(this.user){
            this.search(this.user);
            this.buildForm();
            return;
        }
        this.router.navigate(['/admin/staff/personal'])
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            personID: [''],
            position: ['', [Validators.required]],
            startDate: null,
            endDate: null,
            dates: [[], [Validators.required]],
            positionID: [''],
            notes: [''],
            recordNumber: ['']
        });
    }

    selectedItemGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
    }

    get title() {
        const pro = this.editOrAdd == 1 ? 'Add' : 'Edit';
        return `${pro} Position`;
    }

    search(user: any = this.sharedS.getPicked()) {
        this.cd.reattach();

        this.isLoading = true;
        this.selectedStaff = user;
        this.timeS.getstaffpositions(user.id)
            .subscribe(data => {
                this.tableData = data;
                this.originalTableData = data;
                this.isLoading = false;
                this.cd.detectChanges();
            })

        this.listS.getlistpositions()
            .subscribe(data => this.lists = data)
    }

    showAddModal() {
        this.editOrAdd = 1;
        this.modalOpen = true;
    }

    trackByFn(index, item) {
        return item.id;
    }

    save() {

        for (const i in this.inputForm.controls) {
            this.inputForm.controls[i].markAsDirty();
            this.inputForm.controls[i].updateValueAndValidity();
        }

        if (!this.inputForm.valid)
            return;

        const { position, dates, positionID, notes, recordNumber } = this.inputForm.value;

        const inputForm = {
            personID: this.user.id,
            position: position,
            startDate: dates[0],
            endDate: dates[1],
            positionID: positionID,
            notes: notes,
            recordNumber: recordNumber
        }

        this.isLoading = true;

        if(this.editOrAdd == 1){
            this.timeS.poststaffpositions(inputForm).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.handleCancel();
                        this.success();
                        this.globalS.sToast('Success', 'Position Successfully Added ');
                    }
                })
        }

        if (this.editOrAdd == 2) {
            this.timeS.updatestaffpositions(inputForm, inputForm.recordNumber).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.handleCancel();
                        this.success();
                        this.globalS.sToast('Success', 'Position Successfully Updated');
                    }                    
                })
        }
    }   

    handleCancel() {
        this.inputForm.reset();
        this.isLoading = false;
        this.modalOpen = false;
    }

    success() {
        this.search();
        this.isLoading = false;
        this.selectedRow = 0;
    }

    delete() {
        // this.timeS.deletestaffpositions(recordNumber)
        //     .pipe(takeUntil(this.unsubscribe))
        //     .subscribe(data => {
        //         if (data) {
        //             this.handleCancel();
        //             this.success();
        //             this.globalS.sToast('Success', 'Position Successfully Deleted');
        //         }
        //     });
    }

    showEditModal(index: number) {
        console.log(this.tableData[this.selectedRow]);

        const { position, startDate, endDate, positionID, notes, personID, recordNumber  } = this.tableData[this.selectedRow];

        this.inputForm.patchValue({
            position: position,
            positionID: positionID,
            notes: notes,
            dates: [startDate, endDate],
            recordNumber,
            personID
        });

        this.editOrAdd = 2;
        this.modalOpen = true;
    }

    handleOkTop() {       
    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""        
        this.tryDoctype = ""
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        // console.log("staff "+ permissoons.length );
        return permissoons.charAt(index-1);
    }
    //Mufeed 18 April 2024            
    InitiatePrint(){
           
            var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid,strdate, endate; 
            var head1, head2, head3, head4, head5;
            //fQuery = " Select [Date1],Name As Position, CONVERT(varchar, [Date1],105) As [start Date], Address1 AS [Position Id], Notes From HumanResources WHERE PersonID = '"+this.user.id+"' AND [Group] = 'STAFFPOSITION' ORDER BY  RecordNumber DESC "                                                   
            //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
            txtTitle = "Positions";       
            //Rptid =   "8WTerq14eNFSAnz0"
            head1 = "position";
            head2 = "startDate";
            head3 = "endDate";
            head4 = "positionID";
            head5 = "notes";                        
            Rptid =   "4HcwIWORPtT9HKXw"  
            const data = {
                        
                "template": { "_id": Rptid },                                
                "options": {
                    "reports": { "save": false },                
                    "sql": this.tableData,
                    "Criteria": lblcriteria,
                    "userid": this.tocken.user,
                    "txtTitle": txtTitle+ ' for '+ this.selectedStaff.code,                    
                    "head1":head1,
                    "head2":head2,
                    "head3":head3,
                    "head4":head4,
                    "head5":head5,
                    
                                                                    
                }
            }
            // console.log(fQuery)
            /*
            const data = {
                        
                "template": { "_id": Rptid },                                
                "options": {
                    "reports": { "save": false },                
                    "sql": fQuery,
                    "Criteria": lblcriteria,
                    "userid": this.tocken.user,
                    "txtTitle": txtTitle,
                    "Extra": this.user.code,
                                                                    
                }
            } */
            this.loading = true;
            this.drawerVisible = true;         
                this.printS.printControl(data).subscribe((blob: any) => {
                this.pdfTitle = txtTitle+".pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.drawerVisible = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
                });
                return;      
    }

    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Position',
        value: 'position'
    },{
        key: 'Start Date',
        value: 'startDate'
    },{
        key: 'End Date',
        value: 'endDate'
    },{
        key: 'Position ID',
        value: 'positionID'
    },{
        key: 'Notes',
        value: 'notes'
    }];
    
    dragDestination = [       
        'Position',
        'Start Date',
        'End Date',
        'Position ID',
        'Notes'
    ];
    
    
    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }
    
    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }
    
    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }
    
    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        
        console.log(dragColumns)
        var convertedObj = groupArray(this.originalTableData, dragColumns);
        console.log(convertedObj)

        var flatten = this.flatten(convertedObj, [], 0);
        
        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }
    
    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }
    
    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }
    
    isArray(data: any){
        return Array.isArray(data);
    }
    
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }


}