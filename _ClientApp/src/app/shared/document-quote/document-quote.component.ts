import { Component, OnInit, Input, OnChanges, ViewChild, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormControl,Validators, FormArray } from '@angular/forms'

import { ClrWizard } from "@clr/angular";
import * as moment from 'moment';
import { TimeSheetService } from '@services/index';

@Component({
  selector: 'document-quote',
  templateUrl: './document-quote.component.html',
  styleUrls: ['./document-quote.component.css']
})
export class DocumentQuoteComponent implements OnInit, OnChanges {

  @Input() open: boolean = false;
  @Input() data: any;

  itemOpen: boolean = false;

  documentQuote: any;
  quoteGroup: FormGroup;
  quoteDetailsGroup: FormGroup;
  program: string;

  constructor(
    private timeS: TimeSheetService,
    private formBuilder: FormBuilder
  ) { 

  }

  ngOnInit() {
    this.resetGroup();
  }

  ngOnChanges(changes: SimpleChanges){
    for(let property in changes){
        if(property == 'open' && !changes[property].firstChange 
            && changes[property].currentValue != null){
            this.open = true;
            this.resetGroup();
            this.populate(this.data);
        }
    }
  }

  loopRoster(noOfLoop: number){
    let index = 0;
    while( index < noOfLoop){
      this.addTimeSlot();
      index ++;
    }
  }

  resetGroup(){
      this.quoteGroup = new FormGroup({
          topUpAnnual: new FormControl(''),
          topUpDaily: new FormControl(''),
          topUpMonthly: new FormControl(''),

          basicCareAnnual: new FormControl(''),
          basicCareDaily: new FormControl(''),
          basicCareMonthly: new FormControl(''),

          incomeTestedAnnual: new FormControl(''),
          incomeTestedDaily: new FormControl(''),
          incomeTestedMonthly: new FormControl(''),

          proRata: new FormControl(''),
          totalPackage: new FormControl(''),

          baseQuote: new FormControl(''),
          admin: new FormControl(''),
          totalQuote: new FormControl(''),
          remainingFunds: new FormControl(''),

          no: new FormControl(''),
          type: new FormControl(''),
          period: new FormControl(''),

          startDate: new FormControl(''),
          endDate: new FormControl(''),
      });

      this.quoteDetailsGroup = new FormGroup({
        timeSlots: new FormArray([]),
        roster: new FormControl('NONE')
      })

      
    this.quoteDetailsGroup.get('roster').valueChanges.subscribe(data => {
      this.clearTimeSlot();
      if(data == 'WEEKLY'){
        this.loopRoster(1);
      }
      if(data == 'FORTNIGHTLY'){
        this.loopRoster(2);
      }
      if(data == 'MONTHLY'){
        this.loopRoster(4);
      }
      if(data == 'NONE'){
        this.loopRoster(0);
      }
    });
  }

  clearTimeSlot(){
    const slot = this.quoteDetailsGroup.get('timeSlots') as FormArray;
    slot.clear();
  }

  addTimeSlot(){
    const slot = this.quoteDetailsGroup.get('timeSlots') as FormArray;
    slot.push(this.createTimeSlot());
  }

  createTimeSlot(): FormGroup {
    return this.formBuilder.group({
      monday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      }),
      tuesday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      }),
      wednesday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      }),
      thursday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      }),
      friday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      }),
      saturday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      }),
      sunday: new FormGroup({
        time: new FormControl('9:00'),
        quantity: new FormControl(0)
      })
    });
  }

  populate(data: any){
     this.timeS.getquotelist(data)
            .subscribe(quote => {
                this.documentQuote = quote;

                this.quoteGroup.patchValue({
                  totalPackage: quote.Budget,
                  
                  basicCareAnnual: quote.Contribution,
                  basicCareDaily: quote.DailyBasicCareFee,

                  admin: quote.ADTotal,
                  baseQuote: quote.GSTotal,

                  no: data.quoteNumber,
                  startDate: data.startDate,
                  endDate: data.endDate
                });
            });      
  }

  quoteDetailData: any

  addItem(){
    this.itemOpen = !this.itemOpen;
    this.program = '';   
  }

  openItem(item: any){
this.data.pro

    this.quoteDetailData = item;
    this.itemOpen = !this.itemOpen;
    this.program = this.data.program;
  }


}
