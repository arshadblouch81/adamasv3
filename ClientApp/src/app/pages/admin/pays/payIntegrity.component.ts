import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-billing',
  templateUrl: './payIntegrity.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  },
  textarea{
    resize:none;
  },
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  `]
})
export class PayIntegrityComponent implements OnInit {

  branchList: Array<any>;
  programList: Array<any>;
  categoriesList: Array<any>;
  payIntegrityList: Array<any>;
  tableData: Array<any>;
  PayPeriodLength: number;
  PayPeriodEndDate: any;
  loading: boolean = false;
  allchecked: boolean = false;
  modalOpen: boolean = false;
  billingType: Array<any>;
  AccountPackage: Array<any>;
  invType: Array<any>;
  current: number = 0;
  inputForm: FormGroup;
  modalVariables: any;
  dateFormat: string = 'dd/MM/yyyy';
  inputVariables: any;
  postLoading: boolean = false;
  chkMaster: boolean;
  isUpdate: boolean = false;
  title: string = "Pay Integrity Check";
   
  token: any;
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean = false;
  check: boolean = false;
  temp_title: any;
  settingForm: FormGroup;
  userRole: string = "userrole";
  whereString: string = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  dtpStartDate: any;
  dtpEndDate: any;
  id: string;
  btnid: any;
  btnid1: any;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
  ) { }

  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    // this.loadPayIntegrity();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  resetModal() {
    this.current = 0;
    this.inputForm.reset();
    this.postLoading = false;
  }
  handleCancel() {
    this.modalOpen = false;
    this.router.navigate(['/admin/pays']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      dtpStartDate: startOfMonth(new Date()),
      dtpEndDate: endOfMonth(new Date()),
      chkMaster: false,
    });
  }

  loadPayIntegrity() {
    this.chkMaster = this.inputForm.get('chkMaster').value;
    if (this.chkMaster == false) {
      this.dtpStartDate = '1900/01/01';
      this.dtpEndDate = '1900/01/28';
    }
    else {
      this.dtpStartDate = formatDate(this.dtpStartDate, 'yyyy/MM/dd', 'en_US');
      this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy/MM/dd', 'en_US');
    }

    let sql = "SELECT '0', CreatedBy, Editer, Shift# AS shiftNumber, COnvert(varchar (255), Errortype) AS ErrorType, CASE WHEN ISNULL([Status], 0) > 1 THEN 'APPROVED' ELSE 'UNAPPROVED' END AS [Status], Category, Branch, Staff, Date, [Start Time], Program FROM PErrors ORDER BY CreatedBy";
    this.loading = true;
    this.listS.getlist(sql).subscribe(data => {
      this.payIntegrityList = data;
      this.tableData = data;
      this.loading = false;
    });
  };
}


