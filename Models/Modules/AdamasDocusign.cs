using System;
using DocuSign.eSign.Model;
using System.Collections.Generic;
using System.IO;

namespace Adamas.Models.Modules{
    public class AdamasDocusign {        
        public List<Document> Documents { get; set; }
        public List<Signer> Signers { get; set; }
        public List<SignHere> Signhere { get; set; }
    }
}