
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class CallProcedure{
        public bool IsNDIAHCP { get; set; }
        public string NewPackage { get; set; }
        public string OldPackage { get; set; }
        public string Level { get ;set; }
        public string Type { get; set; }
        // public ProcedureSetClientPackage ClientPackage { get; set; }

        public List<ProcedureRosterNew> Roster { get; set; }
        public ProcedureClientStaffNote StaffNote { get; set; }

    }
}
