﻿using System.ComponentModel.DataAnnotations;

namespace Adamas.Models.Tables
{
    public class LoggedUsers
    {
        [Key]
        public int TokenId { get; set; }
        public string Token { get; set; }
        public string BrowserName { get; set; }
        public string User { get; set; }
    }
}
