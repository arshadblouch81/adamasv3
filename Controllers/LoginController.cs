using System;
//using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;

using Adamas.Models.Modules;
using Adamas.Models.Interfaces;
using Adamas.Models.Tables;
using Adamas.Models;
using Adamas.DAL;
using Adamas.SessionConfigs;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Data.SqlClient;
using Microsoft.VisualBasic;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AdamasV3.Models.Dto;
using Microsoft.AspNetCore.Http;

namespace Adamas.Controllers{

    [Route("api/[controller]")]
    public class LoginController: Controller {

        private readonly JwtIssuerOptions _jwtOptions;
        private readonly DatabaseContext _context;
        private ApplicationUser _user;
        private readonly IJwtHandler _handler;
        private IGeneralSetting GenSettings;        
        private ILoginService login;
        private ICacheService cache;
        private IConfiguration configuration;
        public LoginController(IOptions<JwtIssuerOptions> jwtOptions,
                                IConfiguration _configuration, 
                                DatabaseContext context,
                                IGeneralSetting setting, 
                                IJwtHandler handler,
                                ILoginService _login,
                                ICacheService _cache){
            _context = context;
            _jwtOptions = jwtOptions.Value;
            _handler = handler;
            GenSettings = setting;
            login = _login;
            cache = _cache;
            configuration = _configuration;
        }


        [HttpGet("memorycache/get/{id}")]
        public IActionResult  Get(string id)
        {
            var cacheEntry = cache.GetTokenValue(id);
            
            return Ok(cacheEntry);
        }

        // [HttpGet("memorycache/get")]
        // public IActionResult GetCache()
        // {
        //    return Ok(cache.GetTokenValue());
        // }

        [HttpGet("test-service")]
        public async Task<IActionResult> GetTest(){
           try{
                using(var conn = _context.Database.GetDbConnection()){
                    using(SqlCommand cmd = new SqlCommand(@"SELECT Name FROM UserInfo", (SqlConnection) conn)){
                        await conn.OpenAsync();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        var results = new List<string>();
                        while(await rd.ReadAsync()){
                            results.Add(GenSettings.Filter(rd["Name"]));
                        }
                        return Ok(results);
                    }
                }
           } catch{
               return BadRequest();
           }
        } 

      [HttpGet, Route("ms-user/{user}")]
        public async Task<IActionResult> GetMSUser(String user){
           try{
                using(var conn = _context.Database.GetDbConnection()){
                    using(SqlCommand cmd = new SqlCommand(@"Select Name,isnull(LoginMode,'') as LoginMode, Password, isnull(SSO,0) as SSO from UserInfo where AzureAccount=@msUser", (SqlConnection) conn)){
                        cmd.Parameters.AddWithValue("@msUser",user);
                        await conn.OpenAsync();

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        
                         dynamic dataObj = null;
                        while(await rd.ReadAsync()){
                            
                            dataObj = new
                            {
                                Name = GenSettings.Filter(rd["Name"]),
                                LoginMode = GenSettings.Filter(rd["LoginMode"]),
                                Password =DecryptValue(GenSettings.Filter(rd["Password"])),
                                SingleSignOn = GenSettings.Filter(rd["SSO"])
                            };

                        }
                        return Ok(dataObj);
                    }
                }
           } catch{
               return BadRequest();
           }
        } 
        public string DecryptValue(string strValue)
        {
            string DecryptValueRet = default;

            int i;
            string strTemp;
            i = 1;
            strTemp = "";
            try
            {
                while (i <= Strings.Len(strValue))
                {
                    strTemp = strTemp + Strings.Chr(Strings.Asc(Strings.Mid(strValue, i, 1)) - 50);
                    i = i + 1;
                }
                DecryptValueRet = strTemp;
                return DecryptValueRet;
            }
            catch (Exception ex)
            {
                DecryptValueRet = strTemp;
            }

            return DecryptValueRet;
        }

         public string Decrypt(string passwd = "†¡–“«dcS"){
            string _passwd = passwd;
            string enryptedStr = string.Empty;

            for (var a = 0; a < _passwd.Length; a++)
            {
                enryptedStr = enryptedStr + Strings.ChrW((Strings.AscW(_passwd[a]) - 50)).ToString();
            }

            return enryptedStr;
        }

        [HttpPost("refresh-tokens")]
        public async Task<IActionResult> RefreshToken([FromBody] TokenRefreshValidation tokenModel)
        {
            string accessToken = tokenModel.AccessToken;
            string refreshToken = tokenModel.RefreshToken;

            var principal = login.GetPrincipalFromExpiredToken(accessToken);
            var username = principal.Claims.ToList().FirstOrDefault(x => x.Type.Equals("user"))?.Value;

            var newAccessToken = login.CreateToken(principal.Claims);
            var newRefreshToken = login.GenerateRefreshToken();



            var loginToken = new Token
            {
                User = username,
                Access_token = newAccessToken,
                Expires = Convert.ToDouble(configuration["JwtIssuerOptions:ExpireTime"]),
                Refresh_token = newRefreshToken
            };

            return Ok(loginToken);
        }
        
        [HttpPost]   
        public async Task<IActionResult> Authenticate([FromBody] ApplicationUser user){
            _user = user;

            if(!ModelState.IsValid)
                return Unauthorized();

            try
            {
                var currentLoggedUsers = login.GetNoUsersLoggedIn(_user.Username);
                var currentNumberOfLicenses = await login.GetNoLicenses();

                if(currentNumberOfLicenses == 0)
                {
                    throw new Exception("You have currently no licenses");
                }

                if (currentLoggedUsers >= currentNumberOfLicenses)
                {
                    throw new Exception("You have exceeded maximum licenses");
                }

                var claims = await login.GetClaims(_user);

                if (!claims.Success)
                {
                    return BadRequest(new { message = claims.ErrorInformation });
                }

                var jwt = login.CreateToken(claims.Claims);

                var loginToken = new Token {
                    User = user.Username,
                    Access_token = jwt,
                    Expires = Convert.ToDouble(configuration["JwtIssuerOptions:ExpireTime"]),
                    Refresh_token = claims.RefreshToken
                };
                
                
                var cacheResult = await cache.AppLoginAsync(jwt);

                if(!cacheResult.Result)
                    return BadRequest( new { message = cacheResult.Message   });

 
                HttpContext.Session.Add<Token>("token", loginToken);
                HttpContext.Session.Add<string>("access_token", loginToken.Access_token);
                HttpContext.Session.Add<string>("user", loginToken.User);

                var claimsIdentity = new ClaimsIdentity(claims.Claims, CookieAuthenticationDefaults.AuthenticationScheme);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), new AuthenticationProperties());

                var ifUserAlreadyLoggedIn = await login.AddLoggedUsers(new LoggedUserDto() { Token = jwt, BrowserName = user.BrowserName, User = user.Username });

                //if (!ifUserAlreadyLoggedIn) throw new Exception("User already logged in");

                return Ok(loginToken);

            } catch(Exception ex){

                return BadRequest(new {
                    Message = ex.Message.ToString()
                });                
            }
        }


        [HttpPost,Route("logout")]
        public async Task<IActionResult> Logout([FromBody] string uid){
            HttpContext.Session.Remove("token");
            var token = HttpContext.Session.Get<string>("access_token");
            var user = HttpContext.Session.Get<string>("user");

            await login.DeleteLoggedUsers(new LoggedUserDto() { Token = token, User = user });

            await cache.AppLogoutAsync(uid);
            HttpContext.SignOutAsync();
            return Ok();
        }

    
[HttpGet, Route("win-username")]
public IActionResult GetUsername()
{
    var username = HttpContext.User.Identity.Name;
    return Ok(username);
}

  

  }
}