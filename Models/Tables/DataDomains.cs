using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Adamas.Models.Tables {
    public class DataDomains
    {
        [Key]
        public int RecordNumber { get; set; }
        public string Description { get; set;}
        public string Domain { get; set; }
        public string HACCCode { get; set; }

    }
}