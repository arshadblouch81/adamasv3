import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


const xero: string = "api/xero";

@Injectable()
export class XeroService {
   constructor(
      public http: HttpClient,    
      public auth: AuthService,
   
   ) { }

   
   
   getUrl(): Observable<any> {

      return this.http.get(`${xero}/`);
   }

  
   getpayrollCalendar(): Observable<any> {
      return this.http.get(`${xero}/payroll-calendar` );
   }

   getemployees(): Observable<any> {
      return this.http.get(`${xero}/employees`, );
   }

   getpayitems(): Observable<any> {
      return this.http.get(`${xero}/payitems` );
   }
   getStaffCount(dbName:any): Observable<any> {
      return this.http.get(`${xero}/staff/${dbName}`, );
   }

   PostXeroEmployeeId(dbName:any, data:any): Observable<any> {
      return this.http.get(`${xero}/PostXeroEmployeeId/${dbName}`, data);
   }
   PostXeroEmployee(dbName:any, data:any): Observable<any> {
      return this.http.get(`${xero}/PostXeroEmployee/${dbName}`, data);
   }
   
   PostEarningsRate(dbName:any): Observable<any> {
      return this.http.get(`${xero}/PostEarningsRate/${dbName}`, );
   }

   postTimeSheet(dbName:string, data:any): Observable<any> {
      return this.http.post(`${xero}/timesheets/${dbName}`, data);
   }

   getTimeSheets(dbName:string): Observable<any> {
      return this.http.get(`${xero}/getTimeSheets/`);
   }

   


}