using System;

namespace Adamas.Models.Modules{
    public class LoginPass{
       public int Recnum { get; set; }
       public string Name { get; set; }
       public string UserType { get; set; }
       public string Password { get; set; }
       public string StaffCode { get; set; }
       public string UniqueID { get; set; }
       public bool? CanChooseProvider { get; set; }
       public string RecipientDocFolder { get; set; }
    }
}