using System;

namespace Adamas.Models.Modules {
    public class TimeSheetBill {

        public string Pay_Unit { get; set; }
        public double Bill_Rate { get; set; }
        public double Tax { get; set; }
        public double Quantity { get; set; }
    
        public double Get_Amount()
        {
            return Bill_Rate * Quantity;
        }

        public double Get_Amount_Without_Tax()
        {
            return (Bill_Rate * Quantity) - Tax;
        }

    }
}