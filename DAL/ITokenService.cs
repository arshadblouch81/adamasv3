﻿using System.Threading.Tasks;

namespace AdamasV3.DAL
{
    public interface ITokenService
    {
        Task BackgroundServiceProcess();
    }
}
