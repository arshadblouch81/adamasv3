
using DocuSign.eSign.Model;
using System.Collections.Generic;
using Adamas.Models.Modules;

namespace Adamas.Models.Modules{
    public class DocusignDocument {
        public List<CustomSigner> Signers { get; set; }
        public List<Document> Documents { get; set; }
        public List<CarbonCopy> CCRecipients { get; set; }
        public StartingView StartingView { get; set; }
        public int DOC_ID { get ; set; }

        public string EmailSubject { get ; set; }
        public string EmailBlurb { get ; set; }
         public string status { get ; set; }
    }

    public enum StartingView{
        tagging, recipient
    }
}