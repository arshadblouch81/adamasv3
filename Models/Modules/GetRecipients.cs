namespace Adamas.Models.Modules{
    public class GetRecipients {
        public string User { get; set; }
        public string SearchString { get; set; }
        public bool IncludeInactive { get; set; } = false;
        public bool IncludeReferrals { get; set; } = false;
        public string Status { get; set; }
    }
}