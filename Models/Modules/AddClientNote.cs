
namespace Adamas.Models.Modules{
    public class AddClientNote {
        public string RecipientCode { get; set; }
        public string OperatorID { get; set; }
        public string Note { get; set; }
        public string NoteType { get; set; }
    }
}