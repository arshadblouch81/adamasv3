using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules{
    public class UserGroup {
        public string Notes = "";
        public string Group = "";
        public Boolean? MobileAlert { get; set; } = false;
        public Boolean? Recurring { get; set; } = false;
        
        public string PersonID = "";
        public int? RecordNumber = 0;
        public string Email = "";
        public string Creator = "";
        public  Nullable<DateTime> Date1 { get; set; }
        public  Nullable<DateTime> Date2 { get; set; }
        public  List<String> selectedGroup {get;set;}

    }
}