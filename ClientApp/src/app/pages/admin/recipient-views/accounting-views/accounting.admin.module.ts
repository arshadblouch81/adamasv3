import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';


import {
    ProfileAccounting,
    AccountingHistory,
    Invoices,
    Receipts
} from './index';



const routes: Routes = [
    {
      path: '',
      redirectTo: 'profile',
      pathMatch: 'full'
    },

      
      {
        path: 'profile',
        component: ProfileAccounting
      },
      {
        path: 'accounting',
        component: AccountingHistory
      },
      {
        path: 'invoices',
        component: Invoices
      },
      {
        path: 'receipts',
        component: Receipts
      },
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule
    
  ],
  declarations: [
   
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class AccountingAdminModule {}

