using System;
using System.Data;
using System.IO;
using File = System.IO.File;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using System.Xml;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;

using Adamas.Models;
using DocuSign.eSign.Api;
using DocuSign.eSign.Client;

using eg_03_csharp_auth_code_grant_core.Models;
using eg_03_csharp_auth_code_grant_core.Common;
using Adamas.Models.Modules;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class WebHookController : EgController
    {
        private readonly DatabaseContext _context;
        //private IDocusignRepository docusignRepository;
        private readonly string pathString;

        public WebHookController(
            DSConfiguration config, 
            IRequestItemsService requestItemsService, 
            DatabaseContext context,
            //IDocusignRepository repository,
            IWebHostEnvironment hostingEnvironment
        ): base(config, requestItemsService){

            _context = context;
            //docusignRepository = repository;
            this.pathString = hostingEnvironment.ContentRootPath;
        }

        public override string EgName => "webhook";

        [HttpPost("connect")]
        public async Task ConnectWebHook()
        {

            try
            {
                var tokenDetails = "this.docusignRepository.GetAccessTokens()";

                var accessToken = RequestItemsService.User.AccessToken;
                var basePath = RequestItemsService.Session.BasePath + "/restapi";
                var accountId = RequestItemsService.Session.AccountId;


                Stream stream = Request.Body;

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.Load(stream);

                var mgr = new XmlNamespaceManager(xmldoc.NameTable);
                mgr.AddNamespace("a", "http://www.docusign.net/API/3.0");

                XmlNode envelopeStatus = xmldoc.SelectSingleNode("//a:EnvelopeStatus", mgr);
                XmlNode envelopeId = envelopeStatus.SelectSingleNode("//a:EnvelopeID", mgr);
                XmlNode status = envelopeStatus.SelectSingleNode("./a:Status", mgr);

                string path = Directory.GetCurrentDirectory();

                if (status.InnerText == "Completed") {

                    XmlNode docs = xmldoc.SelectSingleNode("//a:DocumentPDFs", mgr);
                    foreach (XmlNode doc in docs.ChildNodes)
                    {
                        string documentName = doc.ChildNodes[0].InnerText;      // pdf.SelectSingleNode("//a:Name", mgr).InnerText;
                        string documentId = doc.ChildNodes[2].InnerText;        // pdf.SelectSingleNode("//a:DocumentID", mgr).InnerText;
                        string byteStr = doc.ChildNodes[1].InnerText;           // pdf.SelectSingleNode("//a:PDFBytes", mgr).InnerText;

                        int orderNumber = 0;
                        bool isNumericalValue = int.TryParse(documentId, out orderNumber);

                        if(isNumericalValue){
                            await UpdateDocumentStatus(envelopeId, status, orderNumber);
                        } else {
                            throw new Exception("Not a number");
                        }

                        EnvelopeDocItem item = new EnvelopeDocItem() { 
                            Name = documentName,
                            Type = "content",
                            DocumentId = documentId
                        };

                        List<EnvelopeDocItem> itemList = new List<EnvelopeDocItem>() { item }; 

                        var downloadedFile = DoWork(accessToken,basePath, accountId, envelopeId.InnerText, itemList, documentId);
                        var downloadedFileName = downloadedFile.FileDownloadName;

                        using (var fileStream = System.IO.File.Create(Path.Combine(path, "WebHookFile", downloadedFileName)))
                        {
                            downloadedFile.FileStream.Seek(0, SeekOrigin.Begin);
                            downloadedFile.FileStream.CopyTo(fileStream);
                        }

                        var originalFilePath = await GetDirectory(orderNumber);
                        var destinationPath = "this.docusignRepository.GetDocumentDetails(orderNumber)";

                        var log = new LogWriter(Path.Combine(this.pathString, "Logs"),"WebhookLog");

                        CopyAndPasteFile file = new CopyAndPasteFile();
                        file.FileName = downloadedFileName;
                        file.SourceDocPath = @"\\sjcc-sydgw01\portal$\WebHookFile";
                        // file.SourceDocPath = Path.Combine(path, "WebHookFile");
                        file.DestinationDocPath = "destinationPath.OriginalLocation";


                        await this.UpdateDocumentType(envelopeId, downloadedFileName, orderNumber);

                        log.LogWrite(JsonConvert.SerializeObject(file));
                        try
                        {
                            if(await this.Copy_Paste_File(file)){
                                 log.LogWrite("success");
                            } else {
                                log.LogWrite("failed");
                            }
                        }catch(Exception ex){
                            throw ex;
                        }
                        
                        // if(System.IO.Directory.Exists(originalFilePath.FileDir) && System.IO.File.Exists(Path.Combine(path, "WebHookFile", downloadedFileName))){
                        //     System.IO.File.Move(Path.Combine(path, "WebHookFile", downloadedFileName),Path.Combine(originalFilePath.Directory, $"{Path.GetFileNameWithoutExtension(originalFilePath.Filename)}.pdf"));
                        // } else {
                        //     throw new Exception($"Either file or directory doesn't exist - {originalFilePath.FileDir}");
                        // }
                    }                    
                }

                if(status.InnerText == "Sent"){
                    XmlNode docs = xmldoc.SelectSingleNode("//a:DocumentPDFs", mgr);
                    foreach (XmlNode doc in docs.ChildNodes)
                    {
                        string documentName = doc.ChildNodes[0].InnerText;      // pdf.SelectSingleNode("//a:Name", mgr).InnerText;
                        string documentId = doc.ChildNodes[2].InnerText;        // pdf.SelectSingleNode("//a:DocumentID", mgr).InnerText;
                        string byteStr = doc.ChildNodes[1].InnerText;           // pdf.SelectSingleNode("//a:PDFBytes", mgr).InnerText;

                        int orderNumber = 0;
                        bool isNumericalValue = int.TryParse(documentId, out orderNumber);

                        if(isNumericalValue){
                            await UpdateDocumentStatus(envelopeId, status, orderNumber);
                        } else {
                            //throw new Exception("No a number");
                        }
                    }
                }
            } catch(Exception ex)
            {
                var log = new LogWriter(Path.Combine(this.pathString, "Logs"),"WebhookLog");
                log.LogWrite(ex.ToString());
                throw ex;
            }
        }

        public async Task<Boolean> Copy_Paste_File(CopyAndPasteFile doc){
            var log = new LogWriter(Path.Combine(this.pathString, "Logs"),"WebhookLog");
            log.LogWrite(JsonConvert.SerializeObject(doc));
            try{
                using(var conn = _context.GetConnection() as SqlConnection)
                {
                    await conn.OpenAsync();                    
                    using(SqlCommand cmd = new SqlCommand(@"[CopyAndPasteFile]",(SqlConnection) conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileName", doc.FileName);
                        cmd.Parameters.AddWithValue("@SourceDocPath", Path.GetFullPath(doc.SourceDocPath));
                        cmd.Parameters.AddWithValue("@DestinationDocPath", Path.GetFullPath((doc.DestinationDocPath).TrimEnd(new[] { '/', '\\' })));                        
                        
                        await cmd.ExecuteNonQueryAsync();
                        return true;
                    }
                }

           } catch(Exception ex){
               log.LogWrite(ex.ToString());
               return false;
           }
        }

        public async Task UpdateDocumentStatus(XmlNode envelopeId, XmlNode status, int Doc_Id = 0){
            try
            {
                var docStatus = await _context.Documents.Where(x => x.DocusignEnvelopeId == envelopeId.InnerText)
                                            .Select(x => x).FirstOrDefaultAsync();

                docStatus.DocusignStatus = status.InnerText;

                await _context.SaveChangesAsync();
            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task UpdateDocumentType(XmlNode envelopeId, string FileName, int Doc_Id = 0){
            try
            {
                var docStatus = await _context.Documents.Where(x => x.DocusignEnvelopeId == envelopeId.InnerText)
                                            .Select(x => x).FirstOrDefaultAsync();

                docStatus.FileName = FileName;
                docStatus.DocumentType = Path.GetExtension(FileName);

                await _context.SaveChangesAsync();
            } catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<FileDirectory> GetDirectory(int Doc_Id = 0){

            var docStatus = await _context.Documents.Where(x => x.Doc_Id == Doc_Id)
                                            .Select(x => x).FirstOrDefaultAsync();

            return new FileDirectory(){
                Directory = docStatus.OriginalLocation,
                Filename = docStatus.FileName,
                FileDir = System.IO.Path.Combine(docStatus.OriginalLocation, docStatus.FileName)
            };
        }

        [Obsolete]
        private FileStreamResult DoWork(string accessToken, string basePath, string accountId, string envelopeId, List<EnvelopeDocItem> documents, string docSelect)
        {
            var docusignClient = new DocuSignClient("");
            docusignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + accessToken);

            EnvelopesApi envelopesApi = new EnvelopesApi(docusignClient);
            System.IO.Stream results = envelopesApi.GetDocument(accountId, envelopeId, docSelect);

            // Step 2. Look up the document from the list of documents 
            EnvelopeDocItem docItem = documents.FirstOrDefault(d => docSelect.Equals(d.DocumentId));
            // Process results. Determine the file name and mimetype
            string docName = docItem.Name;
            bool hasPDFsuffix = docName.ToUpper().EndsWith(".PDF");
            bool pdfFile = hasPDFsuffix;
            // Add .pdf if it's a content or summary doc and doesn't already end in .pdf
            string docType = docItem.Type.ToLower();
            if (("content".Equals(docType) || "summary".Equals(docType)) && !hasPDFsuffix)
            {
                docName += ".pdf";
                pdfFile = true;
            }
            // Add .zip as appropriate
            if ("zip".Equals(docType))
            {
                docName += ".zip";
            }
            // Return the file information
            // See https://stackoverflow.com/a/30625085/64904
            string mimetype;
            if (pdfFile)
            {
                mimetype = "application/pdf";
            }
            else if ("zip".Equals(docType))
            {
                mimetype = "application/zip";
            }
            else
            {
                mimetype = "application/octet-stream";
            }
            return File(results, mimetype, docName);
        }        
    }

}
