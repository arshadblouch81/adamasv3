import { Component, OnInit, ViewChild, Input, ElementRef, Output, EventEmitter, OnChanges, SimpleChanges, NgZone, Inject } from '@angular/core';
import Cropper from "cropperjs";
import { APP_BASE_HREF } from '@angular/common';

@Component({
  selector: 'img-cropper',
  templateUrl: './img-cropper.component.html',
  styleUrls: ['./img-cropper.component.css']
})
export class ImgCropperComponent implements OnChanges {

  @ViewChild("image", { static: false }) public image: ElementRef;

  @Input("src") public imageSource: File;
  @Input() public role: string;
  @Input() public user: string;

  @Output() imgBLOB = new EventEmitter<FormData>();

  public imageDestination: string = "";
  private cropper: Cropper;
  private canvas: HTMLCanvasElement;

  constructor(
    public ngZone: NgZone,
    @Inject(APP_BASE_HREF) private baseHref:string
  ) {  }

  ngOnChanges(changes: SimpleChanges){
    for(let property in changes){
      if(property == 'imageSource' && !changes[property].isFirstChange() && changes[property].currentValue != null){
          this.replace(changes[property].currentValue) 
      }
    }
  }

  replace(file: File){

    if(FileReader && file){
      this.cropper.destroy();

      var fr = new FileReader();

      fr.onloadend = (e) => {
        this.image.nativeElement.src = fr.result;
        this.cropper.replace(this.image.nativeElement.src);
      }

      fr.readAsArrayBuffer(file);
    }     
  }

  ngAfterViewInit() {
      this.ngZone.runOutsideAngular(() => {
          this.initCropper();
      });
  }

  initCropper(){
    this.cropper = new Cropper(this.image.nativeElement, 
      {
          zoomable: false,
          scalable: false,
          autoCropArea: 1,
          minContainerWidth: 340,
          minContainerHeight: 240,
          aspectRatio: 1,     
          viewMode: 2,
          cropBoxMovable: true,
          cropBoxResizable: false,
          ready:() => {           
            this.cropImage();
          },
          cropstart:() => {
            this.cropImage();
          },
          // crop: () => {
          //   this.cropImage();
          // },
          cropend: (e) =>{
            this.cropImage();
          }
      });
  }

  cropImage(){

    var cnvs = this.cropper.getCroppedCanvas({
      imageSmoothingQuality: "low",
      width: 360,
      height: 360,
    });

    this.canvas = this.getRoundedCanvas(cnvs);
    this.imageDestination = this.canvas.toDataURL("image/png", 0.7);

    this.canvas.toBlob((blob) => {
        var formData = new FormData();      
        formData.append('files', blob, 'profile.png');
        formData.append('uniqueID', this.user);
        formData.append('role', this.role);

        this.imgBLOB.emit(formData);
    });
    
  }

  getRoundedCanvas(sourceCanvas: any) {
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var width = sourceCanvas.width;
    var height = sourceCanvas.height;
    canvas.width = width;
    canvas.height = height;
    context.imageSmoothingEnabled = true;
    context.drawImage(sourceCanvas, 0, 0, width, height);
    context.globalCompositeOperation = 'destination-in';
    context.beginPath();
    context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
    context.fill();
    return canvas;
  }

  errorUrl(event: any){ 
    this.cropper.replace(`assets/logo/profile.png`);
  }



}