import { ChangeDetectorRef, Component, OnInit,HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GlobalService,attendance,ListService, MenuService,statesShortForm,thirdPartyAcounting,wizardDefaultNotes,budgetFormat,serviceTypesByStates,AgencySector, NavigationService } from '@services/index';
import { SwitchService } from '@services/switch.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NzTabPosition } from 'ng-zorro-antd/tabs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:0.3rem;
  }
  textarea{
    resize:none;
  }
  .staff-wrapper{
    height: 20rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  .ant-tabs-bar {
    margin:0px !important;
  }
  .ant-tabs .ant-tabs-left-bar .ant-tabs-tab, .ant-tabs .ant-tabs-right-bar .ant-tabs-tab {
    margin-bottom:0px !important;
  }
  .ant-tabs-nav .ant-tabs-tab {
    margin-bottom:0px !important;
  }
  .ant-tabs .ant-tabs-left-bar .ant-tabs-tab, .ant-tabs .ant-tabs-right-bar .ant-tabs-tab {
    margin:0 0 5px;
  }
  .ant-tabs .ant-tabs-left-bar .ant-tabs-tab{
    margin:0px !important
  }
  .ant-tabs-nav {
    backgroud:#85B9D5 !important
  }
  .ant-tabs-nav .ant-tabs-tab{
    margin-bottom:0px !important
  }
  .ant-tabs .ant-tabs-left-bar .ant-tabs-tab{
    margin-bottom:0px !important;
    width:auto;
  }
  tr.highlight {
    background-color: #85B9D5 !important; /* Change to your preferred highlight color */
  }
  `]
})
export class CompaniesComponent implements OnInit {
  
  private unsubscribe: Subject<void> = new Subject();
  branchList: Array<any>;
  sqlserverList:Array<any>;
  emailTypes:Array<any>;
  incidentTypes:Array<any>;
  billingType:Array<any>;
  emailSubjectType:Array<any>;
  awardEnforcementTypes:Array<any>;
  browsers:Array<any>;
  tabset = false;
  isVisibleTop =false;
  showtabother = false;
  position: NzTabPosition = 'left';
  showtabRostcriteria= false;
  showtabstaffcriteria= false;
  showtabRegcriteria= false;
  showtabrecpcriteria = false;
  show =false ;
  showoption = true;
  tableData: Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  modalOpenelipsis : boolean = false;
  cstdamodalOpene : boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  settingForm:FormGroup;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  title:string = "Agency Detail/Settings";
  workStartHour: Array<any>;
  workFinsihHour: Array<any>;
  drawerVisible: boolean =  false;  
  check : boolean = false;
  userRole:string="userrole";
  whereString :string="Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND";
  tabs = [1, 2, 3];
  options = [
    { value: 'top', label: 'top' },
    { value: 'left', label: 'left' },
    { value: 'right', label: 'right' },
    { value: 'bottom', label: 'bottom' }
  ];
  agencyForm: FormGroup;
  states:Array<any> = statesShortForm;
  thirdPartyAcounting:Array<any> = thirdPartyAcounting;
  wizardDefaultNotes:Array<any>  = wizardDefaultNotes;
  agencySector:any= AgencySector;
  budgetFormat:Array<any> = budgetFormat;
  serviceTypesByStates:any= serviceTypesByStates;
  staffforcementcompetencylevels: string[];
  clientPortalManagerMethod: string[];
  defaultTravelDisplayList: string[];
  appnotecategorylist: any;
  registrationDetail: any;
  agencycstdalist: any;
  cstdaform: FormGroup;
  cstdatypes: string[];
  branches: any;
  fundingSourcelist: string[];
  cmbFundingType: string[];
  serviceTypes: string[];
  personId:null;
  listApprovedStaffByPersonId: any;
  listExcludedStaffByPersonId: any;
  listCompetencyByPersonId: any;
  staffApproved: boolean;
  careWorkers: any;
  staffUnApproved: boolean;
  competencyList: any;
  checkedListExcluded:Array<any>;
  checkedListApproved:Array<any>;
  CompetencycheckedList: any[];
  competencymodal: boolean;
  isUpdatePackageType: boolean;
  careWorkersExcludedCopy: any;
  temp: any[];
  competencyListCopy: any;
  careWorkersCopy: any;
  careWorkersExcluded: any;
  inputvalueSearch:string;
  attendanceAlert: GlobalService;
  searchTerm: string = '';
  currentIndex: number | null = null;

  constructor(
    private globalS: GlobalService,
    private listS: ListService,    
    private switchS: SwitchService,
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private navigationService: NavigationService,
    private router: Router,
    private menuS: MenuService) { }
    
    
    ngOnInit(): void {
      
      this.buildForm(); 
      this.loadData();
      this.populateDropdowns();
      // this.loadBranches();
      
      // this.loading = false;
      // this.cd.detectChanges();
    }
    
    showAddModal() {
      this.title = "Agency Detail/Settings";
      
      this.resetModal();
      this.modalOpen = true;
    }
    showcstdaModal(){
      // serviceTypesByStates
      var prodDesc = this.registrationDetail['prodDesc'];
      if(prodDesc == 'WA'){
        this.serviceTypes = serviceTypesByStates.WA;
      }else if(prodDesc == 'QLD'){
        this.serviceTypes = serviceTypesByStates.QLD;
      }else{
        this.serviceTypes = serviceTypesByStates.OTHER;
      }
      this.cstdamodalOpene = true;
    }
    showEditCstdaModal(data:any){

    }
    deletecstda(data:any){

    }
    showEditElipsisModal(index:any){

      this.menuS.agencycstdalist().subscribe(data=>{
        this.agencycstdalist = data;
      })

      this.isUpdate = true;
      this.agencyForm.patchValue(this.registrationDetail);

      this.agencyForm.patchValue({
        usePositions : ((this.registrationDetail['usePositions'] == 0) ? false : true),
        sessionCheck : ((this.registrationDetail['sessionCheck'] == 0) ? false : true),
        incrementalRosterCreation:((this.registrationDetail['incrementalRosterCreation'] == 0) ? false : true),
      })

      this.modalOpenelipsis = true;
    }

    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
   
    loadTitle()
    {
      return this.title
    }
    loadcstdaTitle(){
     return "CSTDA OUTLETS"; 
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = "WHERE";
        this.loadData();
      }else{
        this.whereString = "Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND";
        this.loadData();
      }
    }
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    } 
    handleCancel() {
      this.modalOpen = false;
    }
    handleCancelellipsis(){
      this.modalOpenelipsis = false;
    }
    handleCancecstda(){
      this.cstdamodalOpene = false;
    }
    showstaffApprovedModal(){
      // this.resetModal();
      this.staffApproved = true;
    }
    handleAprfCancel(){
      this.careWorkers.forEach(x => {
        x.checked = false
      });
      this.staffApproved = false;
    }
    showstaffUnApprovedModal(){
      this.staffUnApproved = true;
    }
    handleUnAprfCancel(){
      this.careWorkers.forEach(x => {
        x.checked = false
      });
      this.staffUnApproved = false;
    }
    showCompetencyModal(){
      this.competencyList.forEach(x => {
        x.checked = false
      });
      this.CompetencycheckedList = [];
      this.competencymodal = true;
      this.isUpdatePackageType = false;
    }
    handleCompetencyCancel(){
      this.competencymodal = false;
      // this.isUpdatePackageType = false;
    }
    pre(): void {
      this.current -= 1;
    }
    
    next(): void {
      this.current += 1;
    }
    save() {
      if(!this.isUpdate){
        console.log("upadte");
      }else{


        var incrementalRosterCreation = this.agencyForm.get('incrementalRosterCreation').value;
        this.agencyForm.controls["incrementalRosterCreation"].setValue((incrementalRosterCreation == true) ? 1 : 0);

        var usePositions = this.agencyForm.get('usePositions').value;
        this.agencyForm.controls["usePositions"].setValue((usePositions == true) ? 1 : 0);

        var sessionCheck = this.agencyForm.get('sessionCheck').value;
        this.agencyForm.controls["sessionCheck"].setValue((sessionCheck == true) ? 1 : 0);

        
        this.menuS.updateagencyRegistartion(this.agencyForm.value)
        .subscribe(data => {
          console.log(data);
          this.globalS.sToast('success','Updated Successfuly');
          this.loading = false;
          this.handleCancelellipsis();
          this.loadData();
        });
        this.isUpdate = false;
      }
    }
    
    delete(data: any) {
      this.globalS.sToast('Success', 'Data Deleted!');
    }
    tabFindIndex: number = 0;
        tabFindChange(index: number){
            this.tabFindIndex = index;        
    }
    tabFindIndexcstda: number = 0;
        tabFindChangecstda(index: number){
            this.tabFindIndexcstda = index;        
    }
    tabFindIndexattendance: number = 0;
        tabFindChangeIndexAttendance(index: number){
            this.tabFindIndexattendance = index;    
                console.log(this.tabFindIndexattendance);
    }
    buildForm() {

      this.agencyForm  = this.formBuilder.group({
        SqlId:1,
        coName:'',
        coPhone2:'',
        acn:'',
        coPhone1:'',
        coFax:'',
        coMobile:'',
        coAddress1:'',
        coEmail:'',
        coAddress2:'',
        coAddress3:'',
        coSuburb:'',
        coPostCode:'',
        prodDesc:'',
        dohTransportProviderID:'',
        logoFile:'',
        bankName:'',
        bpaybillercode:'',
        bankBSB:'',
        bankAccountName:'',
        bankAccountNumber:'',
        cstdaContact:'',
        prodVer:'',
        doesWADSC:false,        
        reg_contact_position:'',
        cstdaEmail:'',
        dscUserName:'',
        dscPassword:'',
        sqlVersion:'',//setting general started
        odbcTimeout:'',
        auditTriggers:false,
        sessionCheck:false,
        brokerageRequestModule:false,
        traccS_MAX_SESSIONS:0,
        recipienT_MAX_SESSIONS:0,
        stafF_MAX_SESSIONS:0,
        rosterS_MAX_SESSIONS:0,
        daymanageR_MAX_SESSIONS:0,
        timesheeT_MAX_SESSIONS:0,
        reportS_MAX_SESSIONS:0,
        allowCDCMultiClaim:false,
        //recipient tab started
        forceRecipientOptions:false,
        accountWidth:'',
        accountingIDgeneration:'',
        legacyCareplanLabel:'',
        wizardDefaultNote:'',
        autoExportCaseNotes:false,
        autoExportCaseNotesFolder:'',
        uppercaseSurname:false,
        autoGenerateFileNumber:false,
        displayAllRecipients:false,
        oniArchive:false,
        forceCaseNote1:false,
        forceSuspendReminder:false,
        autoExportONI:false,
        printONIOnReferral:false,
        customPkgBudgetFormat:'',
        customPkgStatementFormat:'',
        displaySaveWarningInRecipients:false,
        caseNotesWarning:false,
        caseNotesSecurityCheck:false,
        storeNameAsCreator:false,
        includeReferralsInActive:false,
        populateIntakeEmail:false,
        includeWaitingListInActive:false,
        restrictReferrals:false,
        //Incident tab started
        imStyleRecip:'',
        imStyleStaff:'',
        //Roster tab started 
        enforceActivityLimits:false,
        rosterAwardEnforcement:'',
        incrementalRosterCreation:false,// string need to convert to bool when come
        incrementalCompetencyAction:false,
        useNewDayManager:false,
        mealLabelAgencyName:'',
        dotPointXtraInfo:false,
        nmCycle1:'',
        nmCycle2:'',
        nmCycle3:'',
        nmCycle4:'',
        nmCycle5:'',
        nmCycle6:'',
        nmCycle7:'',
        nmCycle8:'',
        nmCycle9:'',
        nmCycle10:'',
        rosterEmail:'',
        //payroll
        defaultPayPeriod:'',
        defaultPayStartDay:'',
        competencyEnforcement:'',
        payPrimacy:false,
        keepPayOnAllocate:false,
        autoAutoApproveRecipientAdmin:false,
        disableDayManagerNotes:false,
        useAwards:false,
        usePositions:false,// save as int
        newTimesheet:false,
        timesheetIsRestricted:false,
        googletravel:false,
        travelProvider:'',
        providerKey:'',
        googleCustID:'',
        //billing tab started
        billingMethod:'',
        customInvoiceFormat:'',
        inclAgedSumInvoice:false,
        bill_DirectDebit:'',
        bill_OnSiteDetails:'',
        bill_MailToDetails:'',
        bill_ByPhoneDetails:'',
        emailInvoiceSubjectSetting:'',
        emailInvoiceSubjectText:'',
        //client portal 
        shortNoticeThreshold:null,
        noNoticeThreshold:null,
        minimumCancellationLeadTime:null,
        bookingLeadTime:null,
        cancellationFeeText:'',
        clientPortalManagerMethod:'',
        //email
        poP3Server:null,
        poP3User:null,
        poP3Password:null,
        smtpServer:null,
        smtpUser:null,
        smtpPassword:null,
        imEmailAddress:null,
        imEmailDisplay:null,
        helpDeskEmail:null,
        defaultInvoiceEmailSubject:null,
        smsPassword:null,
        eMailMode:null,
        outlookPath:null,
        //time and atendece 
        minimumInternetSpeedForOnline:null,
        mobileGEOCodeLimit:null,
        staffLocationUpdateInterval:null,
        mobilUserURL:'',
        webKioskUserURL:'',
        lockWebkiosk:false,
        tA_T1:null,
        tA_T2:null,
        tA_T3:null,
        showClientPhoneInApp:false,
        tA_TRAVELDEFAULT:null,
        tA_ForceNoteOnForcedLogOnOff:false,
        defaultAppNoteCategory:'',
        tA_ApproveWithoutCorrection:false,
        //NDIA
        ndiaAutoTravelClaim:false,
        ndiaTravel:null,
        ndiaCancelFeePercent:null,
        //CDC
        pension:null,
        pensionCoupleEach:null,
        pensionCoupleCombined:null,
        pensionCoupleEachIll:null,
        basicFeePercent:null,
        useMedicareImport:false,
        allowChangeIncomeTestedFee:false,
        //MTA 
        staffLeaveEmailOverrides:false,
        staffLeaveEmail:null,
        clientNoteEmailOverrides:false,
        clientNoteEmail:null,
        //documents
        docusignServerPort:null,
        docusignServerRef:null,
        //po orders
        poStdDisclaimer:null,
        mtA_ErrorLogFile:null,
        appUsesSMTP:null,
        // setting form temporay
        title:'',
        name:'',
        branch:'',
        checkboxcheck:false,
        incRecipient:'NDIA',
        incStaff:'NDIA',
        google:'GOOGLE',
        paN_TANoShowTH:'',
        paN_TANoGoResend:'',
        paN_TAEarlyStartTH:'',
        paN_TALateStartTH:'',        
        paN_TAEarlyFinishTH:'',
        paN_TALateFinishTH:'',
        paN_TAOverstayTH:'',
        paN_TANoWorkTH:'',
        paN_TALateFinishTHEmail:false,
        paN_TAEarlyFinishTHSMS:'',
        paN_TALateFinishTHWho:'',
        paN_TAUnderstayTH:'',
        panLogFileLocation:'',
        paN_DMRefreshInterval:'',
        paN_TAEarlyStartTHEmail:false,
        paN_TAEarlyStartTHWho:'',
        paN_TAEarlyStartTHSMS:'',
        paN_TAEarlyFinishTHWho:'',
        paN_TAEarlyFinishTHEmail:false,
        paN_TALateStartTHEmail:false,
        paN_TALateFinishTHSMS:'',
        paN_TALateStartTHWho:'',
        paN_TAOverstayTHEmail:false,
        paN_TAOverstayTHSMS:'',
        paN_TAOverstayTHWho:'',
        paN_TAUnderstayTHEmail:false,
        paN_TAUnderstayTHSMS:'',
        paN_TAUnderstayTHWho:'',
        paN_TANoWorkTHEmail:false,
        paN_TANoWorkTHSMS:'',
        paN_TANoWorkTHWho:'',
        paN_TANoShowResend:'',
      })


      this.settingForm = this.formBuilder.group({
        title:'',
        name:'',
        branch:'',
        checkboxcheck:false,
        incRecipient:'NDIA',
        incStaff:'NDIA',
        google:'GOOGLE'
      });

      this.cstdaform = this.formBuilder.group({
        ServiceOutletID:'',
        OutletType:'',
        CSTDA:false,
        DCSI:false,
        Branch:'',
        GLRevenue:'',
        GLCost:'',
        GLOverride:false,
        Name:'',
        AddressLine1:'',
        Suburb:'',
        CSTDASLA:'',
        EmailAddress:'',
        postcode:'',
        AgencySector:'',
        CSTDAServiceType:'',
        FundingSource:'',
        FundingType:'',
        RunsheetAlerts:'',
        AH_EarlyFinish:'',AH_EarlyStart:'',AH_LateFinish:'',AH_LateStart:'',AH_NoWork:'',AH_OverStay:'',AH_UndrStay:'',
        BH_EarlyFinish:'',BH_EarlyStart:'',BH_LateFinish:'',BH_LateStart:'',BH_NoWork:'',BH_OverStay:'',BH_UndrStay:'',
      })

    }
    populateDropdowns(){
      this.sqlserverList    = ['2000','2005','2008','2012','2014','>2014'];
      this.emailTypes       = ['OUTLOOK','TRACCS'];
      this.incidentTypes    = ['NDIA','AUTO','WA','QLD','AGED CARE']
      this.billingType      = ['CONSOLIDATED BILLING','PROGRAM BILLING'];
      this.emailSubjectType = ['BILLING CLIENT/INVOICE NUMBER/COMPANY NAME','CUSTOM'];
      this.awardEnforcementTypes = ['No Enforcement','Warn On Breach','Prevent Breach'];
      this.staffforcementcompetencylevels = ['ENFORCE MANDATORY COMPETENCIES','IGNORE MANDATORY COMPETENCIES'];
      this.browsers = ['GOOGLE','BING'];
      this.clientPortalManagerMethod = ['MANAGE BY SPECIFIED MANAGER','MANAGE BY CLIENT CATEGORY'];
      this.defaultTravelDisplayList = ['NON CHARGEABLE BETWEEN','CHARGEABLE BETWEEN','NON CHARGEABLE WITHIN','CHARGEABLE WITHIN'];
      this.cstdatypes = ["CENTRE","GROUP","VEHICLE"];
      this.fundingSourcelist = ["STATE","FEDERAL"];
      this.cmbFundingType = ["Block Funded","BOTH","Individually Funded","N/A"]; 
      this.listS.getlistbranches().subscribe(data => {
        this.branches = data;
      })
      this.attendanceAlert   = this.globalS;
      let careWorker = "SELECT DISTINCT [Accountno] FROM Staff WHERE CommencementDate is not null and terminationdate is null ORDER BY [AccountNo]";
      this.listS.getlist(careWorker).subscribe(data => {
        this.careWorkers = data;
        this.careWorkersCopy = data;
        this.careWorkersExcluded = data;
        this.careWorkersExcludedCopy = data;
      });
      this.listS.getappNotecategory().subscribe(data => {
        this.appnotecategorylist = data;
      })
    }
    loadData(){
      let sql ="Select RecordNumber, Description from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND Domain = 'COMPANY'  ORDER BY DESCRIPTION";
      this.loading = true;
      this.listS.getlist(sql).subscribe(data => {
        this.branchList = data;
        this.tableData = data;
        this.loading = false;
      });
      
      this.menuS.agencyRegistartion().subscribe(data => {
        this.registrationDetail = data;
      });

    }
    loadApprovedStaff(){
      this.menuS.getlistApprovedStaffByPersonId(this.personId).subscribe(data => {
        this.listApprovedStaffByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    } 
    loadExcludedStaff(){
      this.menuS.getlistExcludedStaffByPersonId(this.personId).subscribe(data => {
        this.listExcludedStaffByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    loadCompetency(){
      this.menuS.getlistCompetencyByPersonId(this.personId).subscribe(data => {
        this.listCompetencyByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    deleteApprovedStaff(data:any){
      const group = this.inputForm;
        // this.loading = true;
        this.menuS.deleteApprovedStaff(data.recordNumber)
        .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          if (data) {
            this.globalS.sToast('Success', 'Data Deleted!');
            this.loadApprovedStaff();
            return;
          }
        });
    }
    deleteExcludedStaff(data:any){
      const group = this.inputForm;
        // this.loading = true;
        this.menuS.deleteExcludedStaff(data.recordNumber)
        .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          if (data) {
            this.globalS.sToast('Success', 'Data Deleted!');
            this.loadExcludedStaff();
            return;
          }
        });
    }
    deleteCompetency(data:any){
      const group = this.inputForm;
        this.loading = true;
        this.menuS.deleteCompetency(data.recordNumber)
        .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          if (data) {
            this.globalS.sToast('Success', 'Data Deleted!');
            this.loadCompetency();
            return;
          }
        });
    }
    searchCompetenncy(event){
      this.temp = [];
      this.competencyList = this.competencyListCopy;
      if(event.target.value != ""){
        this.temp = this.competencyList.filter(res=>{
          return res.name.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
        })
        this.competencyList = this.temp;
      }else if(event.target.value == ""){
        this.competencyList = this.competencyListCopy;
      }
    }
    searchStaff(event){
      this.temp = [];
      this.careWorkers = this.careWorkersCopy;
      if(event.target.value != ""){
        this.temp = this.careWorkers.filter(res=>{
          return res.accountno.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
        })
        this.careWorkers = this.temp;
      }else if(event.target.value == ""){
        this.careWorkers = this.careWorkersCopy;
      }      
    }
    searchStaffExcluded(event){
      this.temp = [];
      this.careWorkersExcluded = this.careWorkersExcludedCopy;
      if(event.target.value != ""){
        this.temp = this.careWorkersExcluded.filter(res=>{
          return res.accountno.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
        })
        this.careWorkersExcluded = this.temp;
      }else if(event.target.value == ""){
        this.careWorkersExcluded = this.careWorkersExcludedCopy;
      }      
    }
    onCompetencyCheckboxChange(option, event) {
      if(event.target.checked){
        this.CompetencycheckedList.push(option.name);
      } else {
        this.CompetencycheckedList = this.CompetencycheckedList.filter(m=>m!= option.name)
      }
    }
    onCheckboxUnapprovedChange(option, event) {
      if(event.target.checked){
        this.checkedListExcluded.push(option.accountno);
      } else {
        this.checkedListExcluded = this.checkedListExcluded.filter(m=>m!= option.accountno)
      }
    }
    onCheckboxapprovedChange(option, event)
    {
      if(event.target.checked){
        this.checkedListApproved.push(option.accountno);
      } else {
        this.checkedListApproved = this.checkedListApproved.filter(m=>m!= option.accountno)
      }
    }
    saveExcludeStaff(){

    }
    saveApprovedStaff(){

    }
    saveCompetency(){

    }
    showComptencyEditModal(index: any){

    }
    log(value: string[]): void {
      // console.log(value);
    } 
    goBack(): void {
      const previousUrl = this.navigationService.getPreviousUrl();
      const previousTabIndex = this.navigationService.getPreviousTabIndex();
    
      if (previousUrl) {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      } else {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      }
      
    }
    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
      // Handle character input
      if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
          this.searchTerm += event.key.toLowerCase();
          this.scrollToRow();
      } 
      // Handle backspace
      else if (event.key === 'Backspace') {
          // Only reset if there's something to remove
          this.searchTerm = '';
          if (this.searchTerm.length > 0) {
              this.searchTerm = this.searchTerm.slice(0, -1);
              this.scrollToRow();
          }
      }
      // Handle other keys (optional)
      else if (event.key === 'Escape') {
          // Reset search term on escape key
          this.searchTerm = '';
          this.scrollToRow();
      }
    }

    scrollToRow() {
        // Find the index of the first matching item
        const matchIndex = this.tableData.findIndex(item => 
            item.description.toLowerCase().startsWith(this.searchTerm)
        );
        // Scroll to the matching row if it exists
        if (matchIndex !== -1) {
            const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
            if (tableRow) {
                tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                this.highlightRow(tableRow);
            }
        } else {
        }
    }

    highlightRow(row: HTMLElement) {
        row.classList.add('highlight');
        setTimeout(() => row.classList.remove('highlight'), 2000);
    }
}
  