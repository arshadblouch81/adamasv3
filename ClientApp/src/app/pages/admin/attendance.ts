import { Component, OnInit, OnDestroy, AfterViewInit, Input, ViewChild, HostListener } from '@angular/core'

import { ListService, GlobalService, TimeSheetService, EmailService,PrintService } from '@services/index';
import { forkJoin, Subject } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { format } from 'date-fns';
import parseISO from 'date-fns/parseISO'
import * as moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ShiftDetail } from '../roster/shiftdetail'
import { EmailMessage, EmailAddress } from '@modules/modules';
import { XeroService } from '@services/XeroService';

export interface VirtualDataInterface {
  index: number;
  staff: string;
  recipient: string;
  serviceType: string;
  rosterStart: any;
  duration: any;
  rosterEnd: any;
  taMultishift: any;
}

@Component({
  selector: 'time-attendance',
  styles: [`

  body{
    background-color: #fff;
    color: #000;
  }
  label{
    color: #000;
  }
    nz-checkbox-group ::ngdeep  label {
        display: block;
        padding: 2px;
        font-size:0.75rem;
        color: #000;
    }
    .checkbox-group{
        margin:8px;
    }
    .checkbox-group label{
        display: block;
    }
    .main{
      width:70vw; height:85vh; 
      padding:5px; border: 1px solid #ededed; border-radius: 5px; 
      background-color: #d6eaf75d;
    }

.header{
      display: flex; 
      flex-direction: row; 
      padding-top: 5px; 
      padding-left:10px;
      background-color: #e5ecf1 /*#85B9D5*/; 
      color: black; 
      height:30px;
      border : 1px solid #6496b6;
      border-radius : 5px;

}
    nz-tabset  {
       margin-top:10px;
        cursor: pointer;
       
      }

    nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
        height: 30px !important;
        font-size: 13px !important;
      
    }

   /*
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
        line-height: 24px;
        height: 25px;
       
    }*/
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
        background: #fff;
        color: #000;
    }
    nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
      line-height: 24px;
      height: 25px;
      border-radius: 4px 4px 0 0;
      border: 1px solid #053a6b;
      background: #fff;
      color: black;
    }
    

    i{
        font-size:1.2rem;
        margin-right:1rem;
        cursor:pointer;
        color:#c1c1c1;
    }
    i:hover{
        color:#4d4d4d;
    }
    nz-modal.options ::ngdeep  div div div.ant-modal div.ant-modal-content div.ant-modal-body{
      padding:0;
  }
  nz-modal.options ::ngdeep  div div div.ant-modal div.ant-modal-content div.ant-modal-footer{
      padding:0;
  }
  ul{
      list-style: none;
      padding: 5px 0 5px 15px;
      margin: 0;
  }
  li {
      padding: 4px 0 4px 10px;
      font-size: 13px;
      position:relative;
      cursor:pointer;
  }
  li:hover{
      background:#6496b6;
  }
  li i {
      float:right;
      margin-right:7px;
  }
  hr{
      border: none;
      height: 1px;
      background: #e5e5e5;
      margin: 2px 0;
  }
  li > ul{
      position: absolute;
      display:none;         
      right: -192px;
      padding: 2px 5px;
      background: #fff;
      top: -6px;
      width: 192px;
      transition: all 0.5s ease 3s;
  }
  li:hover > ul{           
      display:block;
      transition: all 0.5s ease 0s;
  }

  .rectangle{
    margin-top: 10px;     
    padding: 10px; 
    padding-left: 5px; 
    border-style:solid; 
    border-width: 2px; 
    border-radius: 5px;  
    border-color: rgb(236, 236, 236);
}

nz-table   {  
 
  border: 1px solid #e5e5e5;
  border-radius : 5px;
}

nz-table th  {  
   background-color:#fff !important;  
  color: black;  
  border: 0.5px solid #e5e5e5; /*#85B9D5; e5ecf1 082f78*/
 
}

nz-table tbody tr.active  {  
  background-color:#6496b6 !important;  
  color: white;  
}   

class.selected
{ 
  background-color:#B98F8F !important;  
  color: white; 

} 

tr:nth-child(even) {
    background-color: aliceblue;
    color: black; 
}
.spinner{
  margin:1rem auto;
  width:1px;
}

    `],
  templateUrl: './attendance.html'
})


export class AttendanceAdmin implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(ShiftDetail) detail!: ShiftDetail;


  allCheckedBranches: boolean = true;
  allCheckedTeams: boolean = true;
  allCheckedCategories: boolean = true;
  allCheckedCoordinators: boolean = true;

  loadingPending: boolean = false;

  indeterminateBranch = true;
  indeterminateTeams = true;
  indeterminateCategories = true;
  indeterminateCoordinators = true;

  date: Date = new Date();
  nzSelectedIndex: number = 0;

  dataSet: Array<any> = [];
  originalData: Array<any> = [];
  completed: Array<any> = [];
  token:any;
  nzWidth: number = 400;
  optionMenuDisplayed: boolean;
  menu: Subject<number> = new Subject();
  TimeAttendnaceModal: boolean;
  TimeAttendnaceLable: string = '';
  clickedData: any;
  dateFormat2 = "dd/MM/yyyy";
  branches: Array<any> = [];
  teams: Array<any> = [];
  categories: Array<any> = [];
  coordinators: Array<any> = [];
  Interval: number = 30;
  AlertInterval: number = 30;
  TimeZoneOffset: number = 0;
  AlertServer: boolean;
  AlertAudit: boolean;

  DateTimeForm: FormGroup;
  durationObject: any;
  today = new Date();
  defaultStartTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
  defaultEndTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);
  dateFormat: string = "dd/MM/YYYY"
  rdate: any;
  action: string;
  private unsubscribe = new Subject();
  emailTestingVisible: boolean = false;

  loading: boolean = false;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;
  tocken: any;

  contactForm = this.fb.group({
    To: [''],
    // Attachments: null,
    Subject: [''],
    Body: ['']
  });


  constructor(
    private listS: ListService,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private timeS: TimeSheetService,
    private modalService: NzModalService,
    private emailService: EmailService,
    private fb: FormBuilder,
    private xero:XeroService,
    private printS:PrintService,
    private sanitizer: DomSanitizer,
  ) {

  }


  ngOnInit(): void {


    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    

    this.getSetting();
    setInterval(() => {
      // refresh data after some interval
      this.reload();

    }, this.Interval * 1000);

    forkJoin([
      this.listS.getlisttimeattendancefilter("BRANCHES"),
      this.listS.getlisttimeattendancefilter("STAFFTEAM"),
      this.listS.getlisttimeattendancefilter("STAFFGROUP"),
      this.listS.getlisttimeattendancefilter("CASEMANAGERS")
    ]).subscribe(data => {
      // console.log(data);
      this.branches = data[0].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });

      this.teams = data[1].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });

      this.categories = data[2].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });

      this.coordinators = data[3].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });


    });
    this.buildForm();
    this.menu.subscribe(data => {
      this.optionMenuDisplayed = false;
      switch (data) {
        case 1:
          this.nzWidth = 400;
          this.TimeAttendnaceModal = true;
          this.TimeAttendnaceLable = "Force Shift Logon";
          this.action = "Force Logon";
          break;
        case 2:
          this.nzWidth = 600;
          this.TimeAttendnaceModal = true;
          this.TimeAttendnaceLable = "Force Shift Logoff";
          this.action = "Force Logoff";
          break;
        case 3:
          this.nzWidth = 600;
          this.TimeAttendnaceModal = true;
          this.TimeAttendnaceLable = "Set Actual Worked Hours";
          this.action = "Actual Worked Hours";
          break;
        case 4:
          this.nzWidth = 600;
          this.TimeAttendnaceModal = true;
          this.TimeAttendnaceLable = "Set Actual Worked Hours";
          break;
        case 4:
        case 5:
          this.nzWidth = 600;
          this.action = 'Force Finalisation';
          this.menuAction();
          break;
        case 6:
          this.action = 'Reset Pending';
          this.showConfirm_for_Pending('This will reset the service back to pending status the highlighted shift/s - do you wich to proceed');

          break;
        case 7:
          this.sendEmail();
      }

      this.action == 'Reset Pending'
    });

  }

  Approve(){
    let input = {
      Date: format(this.date, 'yyyy/MM/dd'),
      LocalTimezoneOffset: 0,
      Coordinators: this.coordinators.filter(item => item.checked).map(x => x.label).join(','),
      Branches: this.branches.filter(item => item.checked).map(x => x.label).join(','),
      Categories: this.categories.filter(item => item.checked).map(x => x.label).join(','),
      Teams: this.teams.filter(item => item.checked).map(x => x.label).join(','),
      TAType: 2
    };
    this.completed = [];

    this.listS.postmtapending(input).pipe( takeUntil(this.unsubscribe)).subscribe(data => {
     // console.log(data)
      this.completed = data;
     let records= this.completed.map(x=>x.jobno).join(',');
     
      let sql =`UPDATE Roster SET Status = 2,[Date Timesheet] = PayPeriodEndDate from Roster , SysTable  WHERE RecordNo IN (${records}) AND Status = 1;
                INSERT INTO Audit (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser);
                SELECT 'Cloud User', getdate(), 'APPROVED VIA WEB T&A','ROSTER', RecordNo,'${this.token.user}' FROM Roster WHERE RecordNo IN (${records})
              `;
      // sql =`UPDATE Roster SET Status = 2,[Date Timesheet] = PayPeriodEndDate from Roster , SysTable  WHERE RecordNo IN (${records}) AND Status = 1;`;
                 
      this.listS.executeSql(sql).pipe(
        takeUntil(this.unsubscribe)).subscribe(d => {
          this.globalS.iToast('Time & Attendance', 'Approved process completed successfully')
        });
    
    }, () => {
     
    })

    

  }


  showConfirm_for_Pending(msg: string): void {
    //var deleteRoster = new this.deleteRoster();
    this.modalService.confirm({
      nzTitle: 'Confirm',
      nzContent: msg,
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
        new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0.5 ? resolve : reject, 100);

          this.menuAction()
        }).catch(() => console.log('Oops errors!'))


    });
  }

  ngAfterViewInit(): void {
    this.reload();
  }
  ngOnDestroy(): void {

  }

  setSetting() {

    localStorage.setItem('Interval', this.Interval.toString());
    localStorage.setItem('AlertInterval', this.AlertInterval.toString());
    localStorage.setItem('TimeZoneOffset', this.TimeZoneOffset.toString());
    localStorage.setItem('AlertServer', this.AlertServer.toString());
    localStorage.setItem('AlertAudit', this.AlertAudit.toString());

  }
  getSetting() {

    if (localStorage.getItem('Interval') == null) return;

    this.Interval = JSON.parse(localStorage.getItem('Interval'));
    this.AlertInterval = JSON.parse(localStorage.getItem('AlertInterval'));
    this.TimeZoneOffset = JSON.parse(localStorage.getItem('TimeZoneOffset'));
    this.AlertServer = JSON.parse(localStorage.getItem('AlertServer'));
    this.AlertAudit = JSON.parse(localStorage.getItem('AlertAudit'));

  }
  buildForm() {
    this.DateTimeForm = this.formBuilder.group({
      recordNo: [''],
      rdate: [''],
      time: this.formBuilder.group({
        startTime: [''],
        endTime: [''],
      }),
      payQty: [''],
      billQty: ['']

    });

    this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    this.fixStartTimeDefault();

    this.DateTimeForm.get('time.startTime').valueChanges.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(d => {

      this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    });
    this.DateTimeForm.get('time.endTime').valueChanges.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(d => {
      this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
    });

  }

  fixStartTimeDefault() {
    const { time } = this.DateTimeForm.value;
    if (!time.startTime) {
      this.ngModelChangeStart(this.defaultStartTime)
    }

    if (!time.endTime) {
      this.ngModelChangeEnd(this.defaultEndTime)
    }
  }

  ngModelChangeStart(event): void {
    this.DateTimeForm.patchValue({
      time: {
        startTime: event
      }
    })
  }
  ngModelChangeEnd(event): void {
    this.DateTimeForm.patchValue({
      time: {
        endTime: event
      }
    })
  }


  @HostListener('document:contextmenu', ['$event']) rightClick(event: MouseEvent) {

    //console.log(event);
    event.preventDefault();
  }

  mousedblclick(event: any, value: any) {
    event.preventDefault();
    event.stopPropagation();

    this.showDetail(value);
  }
  showDetail(data: any) {

    this.getRoster(data.jobno).pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(d => {
      this.detail.isVisible = true;
      this.detail.data = this.selected_roster(d);
      this.detail.viewType = 'Staff';
      this.detail.editRecord = false;
      this.detail.ngAfterViewInit();
    });

  }
  loadAward() {
    // console.log('calling award');
    // let data : any ={
    //   User: "sysmgr",
    //   Password: "sysmgr",
    //   s_StaffCode : "MELLOW MARSHA",
    //   WEDate:"18/06/2023"
    // }
    // this.timeS.postAwardInterpreter(data).subscribe(d=>{
    //     console.log(d)
    // })
  }
  shiftChanged(value: any) {

    this.reload();

  }
  getRoster(recordNo: any) {

    return this.timeS.getrosterRecord(recordNo);


  }
  selected_roster(r: any): any {
    let rst: any;

    rst = {

      "shiftbookNo": r.recordNo,
      "date": r.roster_Date,
      "startTime": r.start_Time,
      "endTime": r.end_Time,
      "duration": r.duration,
      "durationNumber": r.dayNo,
      "recipient": r.clientCode,
      "program": r.program,
      "activity": r.serviceType,
      "payType": r.payType,
      "paytype": r.payType.paytype,
      "pay": r.pay,
      "bill": r.bill,
      "approved": r.Approved,
      "billto": r.billedTo,
      "debtor": r.billedTo,
      "notes": r.notes,
      "selected": false,
      "serviceType": r.type,
      "recipientCode": r.clientCode,
      "staffCode": r.carerCode,
      "serviceActivity": r.serviceType,
      "serviceSetting": r.serviceSetting,
      "analysisCode": r.anal,
      "serviceTypePortal": "",
      "recordNo": r.recordNo

    }

    return rst;
  }

  sendEmail() {
    this.contactForm.patchValue({
      To: this.clickedData.recipientCoordinatorEmail,
      Subject: 'Attendnace Alert',
      Body: 'Your Job is Started Late/Early'
    })
    this.emailTestingVisible = true;
    if (1 == 1) return;
    let emailsMsg: EmailMessage = <EmailMessage>{};
    let emailFrom: EmailAddress ;
    let emailTo: EmailAddress = <EmailAddress>{};

    emailFrom.Address = "support@adamas.net.au";
    //emailFrom.Password="S@mada2012";
    emailFrom.Name = "Arshad Abbas";

    emailTo.Address = "arshadblouch81@gmail.com";
    emailTo.Name = "Arshad Abbas2";

    emailsMsg.FromAddress = null
    emailsMsg.ToAddress = [];
    emailsMsg.FromAddress=(emailFrom);
    emailsMsg.ToAddress.push(emailTo);

    emailsMsg.Content = "Testing email from web portal";
    emailsMsg.Notes = "Testing email note from web portal";
    emailsMsg.Subject = "Testing email";
    emailsMsg.LeaveType = "Causal Leave";

    this.emailService.sendMail(emailsMsg).subscribe(d => {
      console.log(d);
    })

  }
  closeEmail() {
    this.emailTestingVisible = false;
  }

  sendTestEmail() {

    var request = {
      To: [{
        Name: "Mark",
        Email: this.contactForm.value.To
      }],
      Subject: this.contactForm.value.Subject,
      Body: this.contactForm.value.Body,
    };

    this.emailService.testMail(request).subscribe(d => {
      this.globalS.sToast('Success', 'Email succesfully sent');
      this.contactForm.reset();
      this.emailTestingVisible = false;
    });
  }
  TimeDifference(data: any, t: number = 0) {
    let diff: number = 0
    let StartTime;
    let EndTime;
    if (t == 1) {
      StartTime = parseISO(new Date(data.date + ' ' + data.rosterEnd).toISOString());
      EndTime = parseISO(new Date(data.actualEnd).toISOString());
    } else {
      StartTime = parseISO(new Date(data.date + ' ' + data.rosterStart).toISOString());
      EndTime = parseISO(new Date(data.actualStart).toISOString());
    }

    diff = this.globalS.computeTimeDifference(StartTime, EndTime);

    return diff;
  }
  numStr(n: number): string {
    let val = "" + n;
    if (n < 10) val = "0" + n;

    return val;
  }
  BlockToTime(blocks: number) {
    return this.numStr(Math.floor(blocks / 12)) + ":" + this.numStr((blocks % 12) * 5)
  }
  view(index: number) {
    this.nzSelectedIndex = index;
    this.reload();
  }

  rightClickMenu(event: any, value: any) {

    event.preventDefault();
    this.optionMenuDisplayed = true;
    this.clickedData = value;
    console.log(value);
    let date = this.clickedData.date;
    this.defaultStartTime = parseISO(new Date(date + " " + this.clickedData.rosterStart).toISOString());
    this.defaultEndTime = parseISO(new Date(date + " " + this.clickedData.rosterEnd).toISOString());

    this.DateTimeForm.patchValue({
      rdate: this.clickedData.date,
      payQty: this.roundTo(this.clickedData.pay, 2),
      billQty: this.roundTo(this.clickedData.bill, 2)

    })


  }
  roundTo(num: number, places: number) {
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  }

  menuAction() {

    let rosteredstart = moment(this.defaultStartTime).format('YYYY/MM/DD HH:mm');
    let rosteredend = moment(this.defaultEndTime).format('YYYY/MM/DD HH:mm');

    switch (this.action) {
      case 'Force Logon':
        {
          this.forceLogOn(rosteredstart);
          break;
        }
      case 'Force Logoff':
        {
          this.forceLogOff(rosteredend);
          break;
        }
      case 'Actual Worked Hours':
        {
          this.actualWorkedHours(rosteredstart, rosteredend);
          break;
        }
      case 'Force Finalisation':
        {
          this.forceFinalisation(rosteredstart, rosteredend);
          break;
        }
      case 'Reset Pending':
        {
          this.resetPending()
          break;
        }
    }
  }

  ApplyFilters() {

    let branch = (this.branches.filter(item => item.checked).map(x => x.label)).join(',')
    this.dataSet = this.originalData.filter(x => branch.includes(x.branch))
    // console.log(branch);

    this.dataSet = this.originalData.filter(x => {

      return (
        ((this.branches.filter(item => item.checked).map(x => x.label).join(',')).includes(x.branch) || this.allCheckedBranches) &&
        ((this.categories.filter(item => item.checked).map(x => x.label).join(',')).includes(x.category) || this.allCheckedCategories) &&
        ((this.coordinators.filter(item => item.checked).map(x => x.label).join(',')).includes(x.coOrdinator) || this.allCheckedCoordinators) &&
        ((this.teams.filter(item => item.checked).map(x => x.label).join(',')).includes(x.team) || this.allCheckedTeams)
      )

    })


  }
  forceLogOn(timestamp: string) {

    let input = {
      Recordno: this.clickedData.jobno,
      cancel: false,
      timeStamp: timestamp,
      latitude: '',
      longitude: '',
      location: ''
    }
    this.timeS.processStartJob(input).pipe(
      takeUntil(this.unsubscribe)).subscribe(d => {
        if (this.action != 'Force Finalisation') {
          this.TimeAttendnaceModal = false;
          this.reload();
        }
      });

  }
  forceLogOff(timestamp: string) {


    let sql: any = { TableName: '', Columns: '', ColumnValues: '', SetClause: '', WhereClause: '' };

    sql.TableName = 'eziTracker_Log ';

    sql.SetClause = `SET 
            LODateTime = '${timestamp}', 
            LOActualDateTime = '${this.clickedData.date + ' ' + this.clickedData.rosterEnd}',  
            RosteredEnd = '${this.clickedData.date + ' ' + this.clickedData.rosterEnd}',
            WorkDuration = ${this.durationObject.durationInHours},
            ErrorCode = 1
             `;

    sql.WhereClause = `WHERE JobNo = ${this.clickedData.jobno}`;
    this.listS.updatelist(sql).pipe(
      takeUntil(this.unsubscribe)).subscribe(d => {
        if (this.action != 'Force Finalisation')
          this.TimeAttendnaceModal = false;

        this.reload();

      });

  }
  actualWorkedHours(rosteredstart: string, rosteredend: string) {

    let sql: any = { TableName: '', Columns: '', ColumnValues: '', SetClause: '', WhereClause: '' };

    sql.TableName = 'eziTracker_Log ';

    let duration = this.durationObject.durationStr.split(" ");
    let durationstr = this.numStr(duration[0]) + ":" + this.numStr(duration[2]);

    sql.SetClause = `SET 
          DateTime ='${rosteredstart}', 
          ActualDateTime = '${moment(this.clickedData.actualStart).format('YYYY/MM/DD HH:mm')}', 
          LODateTime = '${rosteredend}', 
          LOActualDateTime = '${this.clickedData.date + ' ' + this.clickedData.rosterEnd}',  
          RosteredEnd = '${this.clickedData.date + ' ' + this.clickedData.rosterEnd}',  
          WorkDuration = ${this.durationObject.durationInHours}, 
          WorkDurationHHMM = '${durationstr}', 
          ErrorCode = 1 
          `;


    sql.WhereClause = `WHERE JobNo = ${this.clickedData.jobno}`;
    this.listS.updatelist(sql).pipe(
      takeUntil(this.unsubscribe)).subscribe(d => {
        this.TimeAttendnaceModal = false;
        this.reload();

      });
  }
  forceFinalisation(rosteredstart: string, rosteredend: string) {
    this.forceLogOn(rosteredstart);
    this.forceLogOff(rosteredend);

  }
  resetPending() {
    let sql: any = { TableName: '', Columns: '', ColumnValues: '', SetClause: '', WhereClause: '' };

    sql.TableName = 'Roster ';
    sql.SetClause = `set ros_panztel_updated = 0 `;
    sql.WhereClause = `WHERE RecordNo = ${this.clickedData.jobno}`;


    setTimeout(() => {
      this.listS.updatelist(sql).pipe(
        takeUntil(this.unsubscribe)).subscribe(d => { });

      sql.TableName = 'eziTracker_Log ';
      sql.SetClause = '';
      sql.WhereClause = `WHERE JobNo = ${this.clickedData.jobno}`;
      this.listS.deletelist(sql).pipe(
        takeUntil(this.unsubscribe)).subscribe(d => {
          this.TimeAttendnaceModal = false;
          this.reload();

        });
    }, 100);
  }

  // Branches
  updateAllBranches(): void {
    this.indeterminateBranch = false;

    if (this.allCheckedBranches) {
      this.branches = this.branches.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.branches = this.branches.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }
    this.ApplyFilters()
  }

  updateSingleCheckedBranches(): void {
    this.allCheckedBranches = false;
    if (this.branches.every(item => !item.checked)) {
      this.allCheckedBranches = false;
      this.indeterminateBranch = false;
    } else if (this.branches.every(item => item.checked)) {
      this.allCheckedBranches = true;
      this.indeterminateBranch = false;
    } else {
      this.indeterminateBranch = true;
    }

    this.ApplyFilters()
  }
  // End Branches

  // Teams
  updateAllTeams(): void {
    this.indeterminateTeams = false;

    if (this.allCheckedTeams) {
      this.teams = this.teams.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.teams = this.teams.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }

    this.ApplyFilters()
  }

  updateSingleCheckedTeams(): void {
    this.allCheckedTeams = false;
    if (this.branches.every(item => !item.checked)) {
      this.allCheckedTeams = false;
      this.indeterminateTeams = false;
    } else if (this.branches.every(item => item.checked)) {
      this.allCheckedTeams = true;
      this.indeterminateTeams = false;
    } else {
      this.indeterminateTeams = true;
    }
    this.ApplyFilters()
  }
  // End Teams


  // Categories
  updateAllCategories(): void {
    this.indeterminateCategories = false;

    if (this.allCheckedCategories) {
      this.categories = this.categories.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.categories = this.categories.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }

    this.ApplyFilters()
  }

  updateSingleCheckedCategories(): void {
    this.allCheckedCategories = false;
    if (this.categories.every(item => !item.checked)) {
      this.allCheckedCategories = false;
      this.indeterminateCategories = false;
    } else if (this.categories.every(item => item.checked)) {
      this.allCheckedCategories = true;
      this.indeterminateCategories = false;
    } else {
      this.indeterminateCategories = true;
    }
    this.ApplyFilters()
  }
  // End Categories


  // Categories
  updateAllCoordinators(): void {
    this.indeterminateCoordinators = false;

    if (this.allCheckedCoordinators) {
      this.coordinators = this.coordinators.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.coordinators = this.coordinators.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }

    this.ApplyFilters()
  }

  updateSingleCheckedCoordinators(): void {
    this.allCheckedCoordinators = false;
    if (this.coordinators.every(item => !item.checked)) {
      this.allCheckedCoordinators = false;
      this.indeterminateCoordinators = false;
    } else if (this.coordinators.every(item => item.checked)) {
      this.allCheckedCoordinators = true;
      this.indeterminateCoordinators = false;
    } else {
      this.indeterminateCoordinators = true;
    }

    this.ApplyFilters()
  }
  // End Categories

  reload() {
    this.loadingPending = true;

    let input = {
      Date: format(this.date, 'yyyy/MM/dd'),
      LocalTimezoneOffset: 0,
      Coordinators: this.coordinators.filter(item => item.checked).map(x => x.label).join(','),
      Branches: this.branches.filter(item => item.checked).map(x => x.label).join(','),
      Categories: this.categories.filter(item => item.checked).map(x => x.label).join(','),
      Teams: this.teams.filter(item => item.checked).map(x => x.label).join(','),
      TAType: this.nzSelectedIndex
    };
    this.dataSet = [];

    this.listS.postmtapending(input).subscribe(data => {
     // console.log(data)
      this.dataSet = data;
      this.originalData = data;
      this.loadingPending = false;
    }, () => {
      this.loadingPending = false;
    });
  }

  trackByIndex(_: number, data: VirtualDataInterface): number {
    return data.index;
  }


  handleOkTop() {
    //this.generatePdf();
    this.tryDoctype = ""
    this.pdfTitle = ""
}
handleCancelTop(): void {
    this.drawerVisible = false;
    this.pdfTitle = ""
    this.tryDoctype = ""
}
generatePdf(){
    
    var Title= "Time & Attendance Sheet";
    
   
    this.loading = true;
    this.drawerVisible = true;
//    const temp =  forkJoin([
 //       this.listS.getlist(SQL), 
//        ]);    
//        temp.subscribe(x => {                         
//        this.rptData =  x[0]; 
        //console.log(this.dataSet)      
        
            const data = {
                "template": { "_id": "cyi6QgjCN0jgdOyM" },
                "options": {
                    "reports": { "save": false },                        
                    "sql": this.dataSet,                        
                    "userid": this.tocken.user,
                    "txtTitle":  Title,                      
                }
            }              
            this.loading = true;
           
                    
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
                        this.pdfTitle = Title+".pdf"
                        this.drawerVisible = true;                   
                        let _blob: Blob = blob;
                        let fileURL = URL.createObjectURL(_blob);
                        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                        this.loading = false;
                        
                    }, err => {
                        console.log(err);
                        this.loading = false;
                        this.modalService.error({
                            nzTitle: 'TRACCS',
                            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                            nzOnOk: () => {
                                this.drawerVisible = false;
                            },
                        });
            });                            
            return;        
    //    });         
}

}