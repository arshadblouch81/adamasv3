namespace Adamas.Models.Modules{
    public class InputShiftSpecific{
        public string RecipientCode { get; set; }
        public string ShiftDate { get; set; }
        public int Approved { get; set; }
    }
}