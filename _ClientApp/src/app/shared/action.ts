import { Component, OnInit, Input, Output, ElementRef, EventEmitter, HostListener,ChangeDetectionStrategy } from '@angular/core';

@Component({
    host:{
        '(document:click)': 'onClick($event)'
    },
    selector: 'action',
    templateUrl:'./action.html',
    styles:[
        `
        @media (min-width:320px), (min-width:480px)  {
            div.options{
                right: 0;
            }
            button{
                border: none;
                color: #133963;
                background: transparent;
            }
            button i{                
                font-size:20px;
            }
            i.vert{
                display:block;
            }
            i.settings{
                display:none;
            }
            .claimVar{
                display:none;
            }
        }

        /* Medium devices (desktops, 992px and up) */
        @media (min-width: 992px) { 
            div.options{
                left: 0;
            }
            button{
                border: none;
                padding: 1px;
                color: #fff;
                border-radius: 50%;
                outline:none;
                background:#d83830;
            }
            button i{                
                font-size: 14px;
            }
            i.vert{
                display:none;
            }
            i.settings{
                display:block;
            }
            .claimVar{
                display:block;
            }
        }


        div.action-wrap {
            position:relative;
            width:100%;
        }


        button i{
            float: left;
            padding: 2px;
        }

        div.options{
            position: absolute;
            background-color: #fff;
            z-index: 10;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.22) !important;
            width: 12em;         
        }
        div.options ul{
            list-style:none;
        }
        div.options ul li{
            padding: 8px 0;
            text-align: center;
        }
        div.options ul li:hover{
            cursor:pointer;
            background-color:#e1e5ec;
        }
        .approved{
            background-color:#529849;
        }
        
        `
    ],
    changeDetection: ChangeDetectionStrategy.OnPush

})

export class ActionButton {
    @Input() in_data: any;
    @Input() disableBtns: any;
    @Input() status: number;
    @Output() out_data = new EventEmitter<any>();
    
    @HostListener('mousedown',['$event']) mouseDown(event){
        //console.log(event)
    }

    show = {
        show: false
    }

    constructor(
        private elem: ElementRef
    ){
    }

    onClick(event) {
        if (!this.elem.nativeElement.contains(event.target)){
            this.show = {
                show: false
            };
        }      
    }

    outData(data: boolean, option: string) {       
        this.show = {
            show: false
        }
        this.out_data.emit({ control: data, option: option, value: this.in_data })
    }

    showOption(){        
        this.show = {
            show: true
        };
    }

}