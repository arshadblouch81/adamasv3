namespace Adamas.Models.Modules{
    public class ProcedureSetClientPackage {
        public string PackageStatus { get; set; }
        public string PackageName { get; set; }
        public string ClientCode { get; set; }
       
    }
}