import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
    selector: 'app-hcp-package-status-summary',
    templateUrl: './hcpPkgStatusSummary.component.html',
    styleUrls: ['./hcpFullScreenModal.component.css'],
})

export class HCPPackageStatusSummary implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    modalOpen: boolean = false;
    
    listData: Array<any>;
    loading: boolean = false;
    inputForm: FormGroup;
    check: boolean = false;
    userRole: string = "userrole";
    tocken: any;

    drawerVisible: boolean = false;
    tryDoctype: any;
    pdfTitle: string;
    
    constructor(
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private listS: ListService,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private printS: PrintService,
    ) { }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.loadData();
        this.loading = false;
        this.modalOpen = false;
    }

    handleCancel() {
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/hcp']);
    }

    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    resetModal() {
        this.inputForm.reset();
        this.loading = false;
    }

    fetchDetail(e){
      if(e.target.checked){
        this.loadData();
      }else{
        this.loadData();
      }
    }

    loadData() {
        this.loading = true;
        this.billingS.getHcpPkgStatusSummary(this.check).subscribe(getListDetail => {
            this.listData = getListDetail;
            this.loading = false;
        });
    }
    //Mufeed 19/4/24
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
    }
    PackageStatusReport(){    
            
                        
        const data = {
            "template": { "_id": "BShdx1HihQlxzrsO" },                
            "options": {
                "reports": { "save": false },                                            
                "userid": this.tocken.user,
                "showinactive": this.check,                                    
            }
        }
        this.loading = true;
        var Title = "Package Status Summary List" 
            
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = Title+".pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
                });    
                this.tryDoctype = "";
                this.pdfTitle  = ""        
                               
        return;        
    }
}
