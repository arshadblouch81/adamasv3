using System;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Data;
using System.Data.Common;
//using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;

using ProgramEntities = Adamas.Models.Modules.Program;
using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using System.Collections;
using Microsoft.AspNetCore.Hosting;
using System.Drawing;
using System.Drawing.Imaging;
using AdamasV3.Models.Dto;
using Microsoft.Data.SqlClient;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Diagnostics;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using DocumentFormat.OpenXml.Office.Word;
using DocumentFormat.OpenXml.Presentation;
using System.Runtime.InteropServices;
using System.Linq.Expressions;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Web;


namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    // [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    //[Authorize]
    public class DllController : Controller
    {
        private readonly DatabaseContext _context;

        private IGeneralSetting GenSettings;
        private ITimesheetService timesheetService;
        private readonly ILoginService loginService;
        private IConfiguration _configuration;
        private IHostingEnvironment _env;

        public DllController(
            DatabaseContext context,
            IConfiguration config,
            IGeneralSetting setting,
            IHostingEnvironment env,
            ITimesheetService _timesheetService,
            ILoginService _loginService)
        {
            _context = context;
            GenSettings = setting;
            _env = env;
            timesheetService = _timesheetService;
            loginService = _loginService;
            this._configuration = config;
        }

        [HttpPost, Route("award/interpreter")]
        public async Task<IActionResult> postAwardInterpreter([FromBody] AwardParameter values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/InterpretAwards?User=" + values.User + "&Password=" + values.Password + "&s_StaffCode=" + values.s_StaffCode + "&WEDate=" + values.WEDate + "&b_IncludeAbsences=True";

            Console.WriteLine(finalUrl);
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }

        [HttpPost, Route("roster/createcopyroster")]
        public async Task<IActionResult> CreatRosterCopy([FromBody] RosterCopy values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");

            var finalUrl = ApiPath + "/CreatRosterCopy?User=" + values.User + "&Password=" + values.Password + "&b_AutosetLeaveOptions=" + values.b_AutosetLeaveOptions +
                                      "&b_DeleteAdminTravel=" + values.b_DeleteAdminTravel + "&b_ProcessUnapproved=" + values.b_ProcessUnapproved + "&i_DayOne=" + values.i_DayOne +
                                      "&s_Branches=" + values.s_Branches + "&s_Clients=" + values.s_Clients + "&s_Cycles=" + values.s_Cycles +
                                      "&s_DestinationStartDate=" + values.s_DestinationStartDate + "&s_DestinationEndDate=" + values.s_DestinationEndDate +
                                      "&s_Programs=" + values.s_Programs + "&s_Staff=" + values.s_Staff + "&s_LogFilePath=" + values.s_LogFilePath;
            Console.WriteLine(finalUrl);

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }
        [HttpGet, Route("funding/alerts")]
        public async Task<IActionResult> CheckFundingAlerts(FundingParameters values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/CheckFundingAlerts?User=" + values.User + "&Password=" + values.Password + "&PersonId=" + values.PersonId + "&AccountNo=" + values.AccountNo + "";
           // Console.WriteLine(finalUrl);
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }
        [HttpGet, Route("hcp/balance/export")]
        public async Task<IActionResult> HCPBalanceExport(HCPParameters values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/HCPBalanceExport?User=" + values.User + "&Password=" + values.Password + "&WindowsUer=" + values.WindowsUer + "&Batch=" + values.Batch + "&StartDate=" + values.StartDate + 
                "&EndDate=" + values.EndDate + "&ExportLocation=" + values.ExportLocation + "&LogFilePath=" + values.LogFilePath + "";
            Console.WriteLine(finalUrl);
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }

        [HttpGet, Route("ndia/claim/upload")]
        public async Task<IActionResult> NDIAClaimUpload(NDIParameters values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/NDIAClaimUpload?User=" + values.User + "&Password=" + values.Password + "&WindowsUer=" + values.WindowsUer + "&capital=" + values.Capital + "&ProviderId=" + values.ProviderId + "&Branch=" + values.Branch + "&SelectedClients=" + values.SelectedClients +
            "&StartDate=" + values.StartDate + "" + "&EndDate=" + values.EndDate + "&ExportLocation=" + values.ExportLocation + "&LogFilePath=" + values.LogFilePath + "&SvcBatch=" + values.SvcBatch + "&ItemsOnly=" + values.ItemsOnly + "";
            Console.WriteLine(finalUrl);
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }

        [HttpGet, Route("travel/interpreter")]
        public async Task<IActionResult> TravelInterpreter(TravelParameters values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/TravelInterpreter?User=" + values.User + "&Password=" + values.Password + "&WindowsUer=" + values.WindowsUer + "&Funding=" + values.Funding + "&Branches=" + values.Branches + "&Programs=" + values.Programs + "&StaffCategories=" + values.StaffCategories + "&StaffList=" + values.StaffList +
            "&AvoidTolls=" + values.AvoidTolls + "&AvoidHighways=" + values.AvoidHighways + "&AvoidFerries=" + values.AvoidFerries + "&ExpandTravelToFitGap=" + values.ExpandTravelToFitGap + "&IndividualShift=" + values.IndividualShift + "&FromBase=" + values.FromBase + "&ToBase=" + values.ToBase + "&DeleteTravelAudit=" + values.DeleteTravelAudit +
            "&DeletePreviousBatches=" + values.DeletePreviousBatches + "&DeleteManuallyCreated=" + values.DeleteManuallyCreated + "&IncUnApproved=" + values.IncUnApproved + "&CreateKMAllowance=" + values.CreateKMAllowance + "&CreateTravelTime=" + values.CreateTravelTime + "&ApplyGapLimit=" + values.ApplyGapLimit + "&MazGapForRoster=" + values.MazGapForRoster +
            "&MaxGap=" + values.MaxGap + "&StartDate=" + values.StartDate + "&EndDate=" + values.EndDate + "&KMProgram=" + values.KMProgram + "&KMServiceType=" + values.KMServiceType + "&KMPayType=" + values.KMPayType + "&KMPayRate=" + values.KMPayRate + "&TMProgram=" + values.TMProgram + "&TMServiceType=" + values.TMServiceType + "&TMPayTypeWD=" + values.TMPayTypeWD +
            "&TMPayTypeSA=" + values.TMPayTypeSA + "&TMPayTypeSU=" + values.TMPayTypeSU + "&TMPayTypePH=" + values.TMPayTypePH + "";
            Console.WriteLine(finalUrl);
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }
        
         [HttpGet, Route("PayBillUpdate")]
        public async Task<IActionResult> PayBillUpdate(PayBill_Update values)
        {
            var ApiPath = this._configuration.GetValue<string>("Application:ServiceUrl");
            var finalUrl = ApiPath + "/PayBillUpdate?User=" + values.User + "&Password=" + values.Password + "&WindowsUer=" + values.WindowsUser + "&b_AllDates=" + values.b_AllDates + "&b_Award=" + values.b_Award + "&b_Export=" + values.b_Export + "&b_ExportZeroValueLines=" + values.b_ExportZeroValueLines + "&b_GLCodesRequired=" + values.b_GLCodesRequired +
            "&b_IncludeADMINPaytype=" + values.b_IncludeADMINPaytype + "&b_IncludeRecipientAdmin=" + values.b_IncludeRecipientAdmin + "&b_IncUnapproved=" + values.b_IncUnapproved + "&b_Option0=" + values.b_Option0 + "&b_Option1=" + values.b_Option1 + "&b_Option2=" + values.b_Option2 + "&b_Option3=" + values.b_Option3 + "&b_Option4=" + values.b_Option4 +
            "&b_Option5=" + values.b_Option5 + "&b_Option9=" + values.b_Option9 + "&b_PayBrokerage=" + values.b_PayBrokerage + "&b_UpdateCompletion=" + values.b_UpdateCompletion + "&ProgressBar_Max=" + values.ProgressBar_Max + "&ProgressBar_Text=" + values.ProgressBar_Text + "&ProgressBar_Value=" + values.ProgressBar_Value +
            "&dt_ProcessStartTime=" + values.dt_ProcessStartTime + "&OperatorID=" + values.OperatorID + "&s_ABMDefaultAccount=" + values.s_ABMDefaultAccount + "&s_ABMDefaultActivity=" + values.s_ABMDefaultActivity + "&s_ABMDefaultProject=" + values.s_ABMDefaultProject + "&s_EasyTimeBranch=" + values.s_EasyTimeBranch + "&s_EasyTimeTimesheetID=" + values.s_EasyTimeTimesheetID + 
            "&s_EndDate=" + values.s_EndDate + "&s_EndOfInvoiceString=" + values.s_EndOfInvoiceString + "&s_ExportLocation=" + values.s_ExportLocation + "&s_FirstError=" + values.s_FirstError + "&s_Interface=" + values.s_Interface + "&s_LastError=" + values.s_LastError +"&s_LogFilePath=" + values.s_LogFilePath + "&s_Messages=" + values.s_Messages + "&s_Option1=" + values.s_Option1 + 
            "&s_Option2=" + values.s_Option2 + "&s_Option3=" + values.s_Option3 + "&s_ProcDate=" + values.s_ProcDate + "&s_ProductDataPath=" + values.s_ProductDataPath + "&s_SelectedBranches=" + values.s_SelectedBranches + "&s_SelectedCategories=" + values.s_SelectedCategories + "&s_SelectedFunding=" + values.s_SelectedFunding + "&s_SelectedPrograms=" + values.s_SelectedPrograms + 
            "&s_SelectedStaff=" + values.s_SelectedStaff + "&s_StartDate=" + values.s_StartDate + "&s_StartOfLinString=" + values.s_StartOfLinString + "&s_status=" + values.s_status + "&T1_LineNo=" + values.T1_LineNo + "";

           
            //Console.WriteLine(finalUrl);
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(finalUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    return Ok(apiResponse);
                }
            }


        }
    }

    public class TravelParameters
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string PersonId { get; set; }
        public string AccountNo { get; set; }
        public string WindowsUer { get; set; }
        public string Funding { get; set; }
        public string Branches { get; set; }
        public string Programs { get; set; }
        public string StaffCategories { get; set; }
        public string StaffList { get; set; }

        public string AvoidTolls { get; set; }
        public string AvoidHighways { get; set; }
        public string AvoidFerries { get; set; }
        public string ExpandTravelToFitGap { get; set; }
        public string IndividualShift { get; set; }

        public string FromBase { get; set; }

        public string ToBase { get; set; }

        public string DeleteTravelAudit { get; set; }

        public string DeletePreviousBatches { get; set; }
        public string DeleteManuallyCreated { get; set; }
        public string IncUnApproved { get; set; }
        public string CreateKMAllowance { get; set; }
        public string CreateTravelTime { get; set; }
        public string ApplyGapLimit { get; set; }
        public string MazGapForRoster { get; set; }
        public string MaxGap { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string KMProgram { get; set; }
        public string KMServiceType { get; set; }
        public string KMPayType { get; set; }
        public string KMPayRate { get; set; }
        public string TMProgram { get; set; }
        public string TMServiceType { get; set; }
        public string TMPayTypeWD { get; set; }
        public string TMPayTypeSA { get; set; }
        public string TMPayTypeSU { get; set; }

        public string TMPayTypePH { get; set; }



    }
    public class NDIParameters
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string PersonId { get; set; }
        public string AccountNo { get; set; }
        public string WindowsUer { get; set; }
        public string Capital { get; set; }
        public string ProviderId { get; set; }
        public string Branch { get; set; }
        public string SelectedClients { get; set; }
        public string StartDate { get; set; }

        public string EndDate { get; set; }
        public string ExportLocation { get; set; }
        public string LogFilePath { get; set; }
        public string SvcBatch { get; set; }
        public string ItemsOnly { get; set; }


    }


    public class HCPParameters
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string PersonId { get; set; }
        public string AccountNo { get; set; }
        public string WindowsUer { get; set; }
        public string Batch { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ExportLocation { get; set; }
        public string LogFilePath { get; set; }


    }
    public class FundingParameters
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string PersonId { get; set; }
        public string AccountNo { get; set; }



    }
    public class AwardParameter
    {
        public string User { get; set; }
        public string Password { get; set; }

        public string s_StaffCode { get; set; }
        public string WEDate { get; set; }
    }

    public class RosterCopy
    {
        public string User { get; set; }
        public string Password { get; set; }
        public bool b_AutosetLeaveOptions { get; set; }
        public bool b_DeleteAdminTravel { get; set; }
        public bool b_ProcessUnapproved { get; set; }
        public string i_DayOne { get; set; }
        public string s_Branches { get; set; }
        public string s_Clients { get; set; }
        public string s_Cycles { get; set; }
        public string s_DestinationEndDate { get; set; }
        public string s_DestinationStartDate { get; set; }
        public string s_Programs { get; set; }
        public string s_Recipients { get; set; }
        public string s_Staff { get; set; }

        public string s_LogFilePath { get; set; }
    }
public  class PayBill_Update
{
    public string User { get; set; }
    public string Password { get; set; }
    public string WindowsUser { get; set; }
    public string b_AllDates { get; set; }
    public string b_Award { get; set; }
    public string b_Export { get; set; }
    public string b_ExportZeroValueLines { get; set; }
    public string b_GLCodesRequired { get; set; }
    public string b_IncludeADMINPaytype { get; set; }
    public string b_IncludeRecipientAdmin { get; set; }
    public string b_IncUnapproved { get; set; }
    public string b_Option0 { get; set; }
    public string b_Option1 { get; set; }
    public string b_Option2 { get; set; }
    public string b_Option3 { get; set; }
    public string b_Option4 { get; set; }
    public string b_Option5 { get; set; }
    public string b_Option9 { get; set; }
    public string b_PayBrokerage { get; set; }
    public string b_UpdateCompletion { get; set; }
    public string ProgressBar_Max { get; set; }
    public string ProgressBar_Text { get; set; }
    public string ProgressBar_Value { get; set; }
    public string dt_ProcessStartTime { get; set; }
    public string OperatorID { get; set; }
    public string s_ABMDefaultAccount { get; set; }
    public string s_ABMDefaultActivity { get; set; }
    public string s_ABMDefaultProject { get; set; }
    public string s_EasyTimeBranch { get; set; }
    public string s_EasyTimeTimesheetID { get; set; }
    public string s_EndDate { get; set; }
    public string s_EndOfInvoiceString { get; set; }
    public string s_ExportLocation { get; set; }
    public string s_FirstError { get; set; }
    public string s_Interface { get; set; }
    public string s_LastError { get; set; }
    public string s_LogFilePath { get; set; }
    public string s_Messages { get; set; }
    public string s_Option1 { get; set; }
    public string s_Option2 { get; set; }
    public string s_Option3 { get; set; }
    public string s_ProcDate { get; set; }
    public string s_ProductDataPath { get; set; }
    public string s_SelectedBranches { get; set; }
    public string s_SelectedCategories { get; set; }
    public string s_SelectedFunding { get; set; }
    public string s_SelectedPrograms { get; set; }
    public string s_SelectedStaff { get; set; }
    public string s_StartDate { get; set; }
    public string s_StartOfLinString { get; set; }
    public string s_status { get; set; }
    public string T1_LineNo { get; set; }
}

}