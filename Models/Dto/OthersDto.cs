﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class OthersDto
    {
        public bool? AcceptsReferrals { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public string AdmittedBy { get; set; }
        public string Branch { get; set; }
        public bool? CompanyFlag { get; set; }
        public DateTime? DischargeDate { get; set; }
        public string Type { get; set; }
        public string Recipient_Coordinator { get; set; }
        public bool? ExcludeFromRosterCopy { get; set; }
        public string DischargedBy { get; set; }
        public string Occupation { get; set; }
        public string FinancialClass { get; set; }
        public string MainSupportWorker { get; set; }
        public bool? GeneratesReferrals { get; set; } 
    }
}
