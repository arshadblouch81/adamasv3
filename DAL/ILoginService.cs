using System.Threading.Tasks;
using Adamas.Models.Modules;
using AdamasV3.Models.Dto;
using Adamas.Models.Tables;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Collections.Generic;

namespace Adamas.DAL {
    public interface ILoginService{
        Task<ClaimsAndRefreshToken> GetClaims(ApplicationUser user);
        Task<int> GetRegistrationMaxUsers();
        Task<UserDto> GetCurrentUser();
        Task<bool> AddLoggedUsers(LoggedUserDto user);
        Task<bool> DeleteLoggedUsers(LoggedUserDto user);
        Task<int> GetNoLicenses();
        int GetNoUsersLoggedIn(string name);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
        string CreateToken(IEnumerable<Claim> claims);
    }
}