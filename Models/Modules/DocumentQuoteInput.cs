
namespace Adamas.Models.Modules{
    public class DocumentQuoteInput {
        public string DocId { get; set; }
        public string DocumentType { get; set; }
        public string PersonId { get; set; }
        public bool IncludeArchived { get; set; }
        public bool IncludeAccepted { get; set; }
        public int DisplayLast { get; set; } 
    }
}