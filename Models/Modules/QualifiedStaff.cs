
namespace Adamas.Models.Modules{
    public class QualifiedStaff {        
        public string RecipientCode { get; set; }
        public string User { get; set; }
        public string BookDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string EndLimit { get; set; }
        public string Gender { get; set; }
        public string Competencys { get; set; }
        public int CompetenciesCount { get; set; }
        public string TeamFilter { get; set; }
        public string CategoryFilter { get; set; }
    }
}