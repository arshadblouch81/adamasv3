﻿using System.Collections.Generic;
using System.Net.Http;
using System;
using System.Threading.Tasks;

using AdamasV3.Helper;
using Microsoft.Extensions.Caching.Memory;

using Newtonsoft.Json.Linq;
using AdamasV3.Models.Modules;
using Microsoft.Extensions.Configuration;
using System.IO;
using Xero.NetStandard.OAuth2.Api;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Xero.NetStandard.OAuth2.Models;
using System.Linq;
using Xero.NetStandard.OAuth2.Model.PayrollAu;
using DocumentFormat.OpenXml.InkML;
using Adamas.Models;
using Adamas.Models.XeroModels;
using Xero.NetStandard.OAuth2.Model.Identity;
using Xero.NetStandard.OAuth2.Model.Accounting; 

namespace AdamasV3.DAL
{
    public class XeroService : IXeroService
    {
        private static string CLIENT_ID = string.Empty;
        private static string SCOPES = string.Empty;
        private static string CODE_VERIFIER = string.Empty;
        private static string APPLICATION_NAME = string.Empty;
        private static string REDIRECT_URI = string.Empty;
        private static string STATE = string.Empty;
        public static string AuthorisationUrl = string.Empty;
        public static string ConnectionsUrl = string.Empty;

        private readonly IConfiguration Configuration;
        private IMemoryCache _cache;
        private readonly string OUTPUTFILEPATH;
        private readonly string LOGPATH;
        private readonly DynamicDatabaseContext context;
        public XeroService(IConfiguration configuration, IMemoryCache cache, DynamicDatabaseContext _context)
        {
            Configuration = configuration;

            CLIENT_ID           = Configuration["XeroConfiguration:ClientId"].ToString();
            SCOPES              = Configuration["XeroConfiguration:Scopes"].ToString();
            CODE_VERIFIER       = Configuration["XeroConfiguration:CodeVerifier"].ToString();
            APPLICATION_NAME    = Configuration["XeroConfiguration:ApplicationName"].ToString();
            REDIRECT_URI        = Configuration["XeroConfiguration:REDIRECT_URI"].ToString();
            STATE               = Configuration["XeroConfiguration:State"].ToString();
            AuthorisationUrl    = Configuration["XeroConfiguration:AuthorisationUrl"].ToString();
            ConnectionsUrl      = Configuration["XeroConfiguration:ConnectionsUrl"].ToString();

            _cache = cache;
            LOGPATH = Environment.CurrentDirectory;
            OUTPUTFILEPATH = Path.Combine(LOGPATH, "Output", "token.json");
            context = _context;
        }

        public async Task<Tokens> GetTokensOnFile()
        {
            return await JsonFileReader.ReadAsync<Tokens>(this.OUTPUTFILEPATH);
        }

        public async Task<bool> PostAllEarningsRateIds(List<EarningsRate> rates)
        {

            try
            {
                foreach (var rate in rates)
                {
                    // var itemtype = (from it in context.ItemTypes where it.AccountingIdentifier.ToLower() == rate.Name.ToLower() select it).ToList();
                    // itemtype.ForEach(x => x.ExtPayID = rate.EarningsRateID.ToString());

                    // var itemtype = (from it in context.ItemTypes  where (it.AccountingIdentifier ?? "").ToLower() == rate.Name.ToLower()
                    // select it).ToList();

                    var itemtype = (from it in context.ItemTypes
                    where it.AccountingIdentifier != null && (it.AccountingIdentifier ?? "").ToLower() == rate.Name.ToLower()
                    select it).ToList();

                    itemtype.ForEach(x => x.ExtPayID = rate.EarningsRateID.ToString());

                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
            
                throw new Exception(ex.ToString());
            }
            return true;
        }

        public async Task<Xero.NetStandard.OAuth2.Model.PayrollAu.Employees> GetEmployees()
        {
            try
            {
                var where = "Status==\"ACTIVE\"";
                var order = "LastName ASC";

                // Get RefreshTokens
                var token = await GetRefreshTokens();

                // Get Tenant
                var tenantId = await GetApplicationTenant();

                var apiInstance = new PayrollAuApi();
                var result = await apiInstance.GetEmployeesAsync(token.AccessToken, tenantId, null, where, order);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> PostXeroEmployeeId(Xero.NetStandard.OAuth2.Model.PayrollAu.Employees employees)
        {
            try
            {
                var _staffs = (from s in context.Staff
                               where s.FirstName != null && s.LastName != null
                               select new { s.SQLID, s.FirstName, s.LastName }).ToList();

                var _xeroEmp = (from e in employees._Employees
                                select new { e.EmployeeID, e.FirstName, e.LastName }).ToList();

                var intersectEmployees = (from sf in _staffs
                                          join x in _xeroEmp
                                          on new { FirstName = sf.FirstName, LastName = sf.LastName } equals new { FirstName = x.FirstName, LastName = x.LastName }
                                          select new
                                          {
                                              SQLID = sf.SQLID,
                                              FirstName = x.FirstName,
                                              LastName = x.LastName,
                                              EmployeeId = x.EmployeeID
                                          }).ToList();

                foreach (var emp in intersectEmployees)
                {
                    var _emp = context.Staff.Where(x => x.SQLID == emp.SQLID).FirstOrDefault();
                    _emp.ExtEmployeeID = emp.EmployeeId.ToString();
                    //_emp.STF_CODE = emp.EmployeeId.ToString();
                    await context.SaveChangesAsync();
                }


                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
public async Task<bool> CreatePayItems( List<PayItem>  payitems){
try
            {
                // Get RefreshTokens
                var token = await GetRefreshTokens();

                // Get Tenant
                var tenantId = await GetApplicationTenant();

                var Api = new PayrollAuApi();
                foreach (var payitem in payitems)
                    await Api.CreatePayItemAsync(token.AccessToken, tenantId, payitem);
                
                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
}
  public async Task<bool> CreateEmployees(List<Xero.NetStandard.OAuth2.Model.PayrollAu.Employee> employees)
        {
            try
            {
                // Get RefreshTokens
                var token = await GetRefreshTokens();

                // Get Tenant
                var tenantId = await GetApplicationTenant();

            var Api = new PayrollAuApi();
            await Api.CreateEmployeeAsync(token.AccessToken, tenantId, employees);
                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public async Task<Timesheets> PostTimesheet(List<Timesheet> timesheet)
        {
            try
            {
                // Get RefreshTokens
                var token = await GetRefreshTokens();

                // Get Tenant
                var tenantId = await GetApplicationTenant();

                var apiInstance = new PayrollAuApi();
                var result = await apiInstance.CreateTimesheetAsync(token.AccessToken, tenantId, timesheet);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public async Task<PayItems> GetPayItems()
        {
            var where = "Status=='ACTIVE'";
            var order = "EmailAddress%20DESC";
            var page = 56;

            // Get RefreshTokens
            var token = await GetRefreshTokens();

            // Get Tenant
            var tenantId = await GetApplicationTenant();

            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.GetPayItemsAsync(token.AccessToken, tenantId, null, where, order, page);

            return result;
        }

        public async Task<List<Tenant>> GetTenants()
        {
            var client = new HttpClient();
            var currentToken = await GetTokensOnFile();

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", currentToken.AccessToken);

            var response = await client.GetAsync(ConnectionsUrl);
            var content = await response.Content.ReadAsStringAsync();

            //fill the dropdown box based on the results 
            var tenants = JsonConvert.DeserializeObject<List<Tenant>>(content);

            return tenants;
        }

        public async Task<string> GetApplicationTenant()
        {
            var tenants = await GetTenants();
            var tenantId = tenants.Where(x => x.TenantName == APPLICATION_NAME.ToString()).Select(x => x.TenantId).FirstOrDefault();
            return tenantId.ToString();

        }
        public async Task<Tokens> GetRefreshTokens()
        {
            const string url = "https://identity.xero.com/connect/token";
            var client = new HttpClient();

            Tokens token = await GetTokensOnFile();

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
                new KeyValuePair<string, string>("client_id", CLIENT_ID),
                new KeyValuePair<string, string>("refresh_token", token.RefreshToken),
            });

            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromDays(1));

            var response = await client.PostAsync(url, formContent);

            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new Exception("Error BadRequest");
            }

            var content = await response.Content.ReadAsStringAsync();
            var tokens = JObject.Parse(content);

            string? access_token = tokens["access_token"].ToString();
            _cache.Set("access_token", access_token, cacheEntryOptions);

            string? refresh_token_result = tokens["refresh_token"].ToString();
            _cache.Set("refresh_token", refresh_token_result, cacheEntryOptions);

            token.AccessToken = access_token;
            token.RefreshToken = refresh_token_result;

            PostTokensOnFile(token);

            return new Tokens()
            {
                IdToken = token.IdToken,
                AccessToken = access_token,
                RefreshToken = refresh_token_result
            };
        }

        public bool PostTokensOnFile(Tokens token)
        {
            var serializeJSON = Newtonsoft.Json.JsonConvert.SerializeObject(token);
            using (var writer = System.IO.File.CreateText(OUTPUTFILEPATH))
            {
                writer.WriteLine(serializeJSON);
            }

            return true;
        }

        public async Task<string> ChangeDbConnection(string dbName, string connString)
        {
            DbManager.DbName = dbName;
            DbManager.ConnString = connString;
            return connString;
        }

        public async Task<PayrollCalendars> GetPayrollCalendars()
        {
            // Get RefreshTokens
            var token = await GetRefreshTokens();

            // Get Tenant
            var tenantId = await GetApplicationTenant();


            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.GetPayrollCalendarsAsync(token.AccessToken, tenantId, null, null, null);
            return result;
        }

        public async Task<int> GetStaffCount()
        {
            return (from s in context.Staff select s).Count();
        }

          public async Task<Timesheets> GetTimeSheets()
        {
            // Get RefreshTokens
            var token = await GetRefreshTokens();

            // Get Tenant
            var tenantId = await GetApplicationTenant();


            var apiInstance = new PayrollAuApi();
            var result = await apiInstance.GetTimesheetsAsync(token.AccessToken, tenantId, null, null, null, null);
   
            return result;
        }
        
      public async Task<TrackingCategories> GetTrackingCategories()
        {
            // Get RefreshTokens
            var token = await GetRefreshTokens();

            // Get Tenant
            var tenantId = await GetApplicationTenant();


            var apiInstance = new AccountingApi();
           
            var result = await apiInstance.GetTrackingCategoriesAsync (token.AccessToken, tenantId);
         
   
            return result;
        }
    
    }
}
