
namespace Adamas.Models.Modules{
    public class Suburb {
        public string Postcode { get; set; }
        public string SuburbName { get; set; }
    }
}