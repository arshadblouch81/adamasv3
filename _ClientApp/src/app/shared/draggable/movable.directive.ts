import { Directive, HostListener, HostBinding, ElementRef, NgZone, AfterViewInit, Renderer2 } from '@angular/core'
import { DraggableTimeDirective } from './draggable.directive'

import { DomSanitizer, SafeStyle } from '@angular/platform-browser'

interface Position{
    x: number,
    y: number
}

@Directive({
    selector: '[movable]'
})

export class MovableDirective extends DraggableTimeDirective implements AfterViewInit {

    constructor(
        public el: ElementRef,
        public ngZone: NgZone,
        private sanitizer: DomSanitizer, 
        private renderer: Renderer2,
        public element: ElementRef){
         super();
    }

    
    position: Position = { x:0, y:0 }
    private startPosition: Position

    // ngAfterViewInit(){
    //     this.ngZone.runOutsideAngular(() => {

    //         const dateTime = document.querySelector('.date-data')
    //         const currEl = this.el.nativeElement
          
    //         dateTime.addEventListener('dragStart', (e: PointerEvent) => {
    //             this.startPosition = {
    //                 x: e.clientX - this.position.x,
    //                 y: e.clientY - this.position.y
    //             }  
    //             console.log('asd')
    //         })

    //         dateTime.addEventListener('dragMove', (e: PointerEvent) => {
    //             this.position.x = e.clientX - this.startPosition.x
    //             this.position.y = e.clientY - this.startPosition.y
    //             console.log('asdasd')
    //         })

    //         // dateTime.addEventListener('pointermove', (e: PointerEvent) => {
    //         //     this.pointerMove.next(e)
    //         // })
    //     })
    // }

    @HostBinding('style.transform') get hostElementTransform(): SafeStyle {
        return this.sanitizer.bypassSecurityTrustStyle(`translateX(${this.position.x}px) translateY(${this.position.y}px)`)     
    }


     @HostListener('dragStart',['$event']) onDragStart(event: PointerEvent){
        this.startPosition = {
            x: event.clientX - this.position.x,
            y: event.clientY - this.position.y
        }        
    }

    @HostListener('dragMove',['$event']) onDragMove(event: PointerEvent){        
        this.position.x = event.clientX - this.startPosition.x
        this.position.y = event.clientY - this.startPosition.y
        
    }

    @HostListener('dragEnd',['$event']) onDragEnd(){
        //console.log('dragend')
        //this.position = { x:0, y:0 }
    }



}