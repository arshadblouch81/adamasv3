import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { GlobalService, UploadService } from '@services/index';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse, HttpEvent } from '@angular/common/http'

import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {
  @Input() isAdmin: boolean = false;
  @Input() personID: string;

  uploadModal: boolean = false;

  title: string;
  files: Array<any> = []
  description: string;

  mediaList: Array<any> = []
  safeURL: any;

  @ViewChild('videoPlayer', { static: false }) videoplayer: ElementRef;

  constructor(
    private http: HttpClient,
    private globalS: GlobalService,
    private uploadS: UploadService,
    private sanitizer: DomSanitizer
  ) {   }

  ngOnInit() {
    this.getMedia();
  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  getMedia(){
    this.uploadS.getMedia(this.personID)
        .subscribe(files => {
          this.mediaList = files.map(x => {
            return {
              clientGroup: x.clientGroup,
              endDate: x.endDate,
              fileBlob: x.fileBlob,
              item: x.item,
              media: x.media,
              mediaDisplay: x.mediaDisplay,
              mediaText: x.mediaText,
              program: x.program,
              startDate: x.startDate,
              target: x.target,
              type: x.type,
              url: `${window.location.origin}/media/${x.media}`
            }
          })
        });
  }

  upload(){  
    var formData = new FormData()
       
    for (var file of this.files) {
      formData.append(file.name, file)
    }

    formData.append("title", this.title);
    formData.append("description", this.description);

    const req = new HttpRequest('POST', `api/upload/media/${this.personID}`, formData);

    this.http.request(req).subscribe(event => {
      if(event){
        this.globalS.sToast('Success','Media has been saved');
        this.getMedia();
        this.clear();
        this.uploadModal = false;
      }           
    });
  }

  clear(){
    this.title = ''
    this.description = '',
    this.files = []
  }

  fileData(formdata: any){
    this.files = formdata;
  }

}
