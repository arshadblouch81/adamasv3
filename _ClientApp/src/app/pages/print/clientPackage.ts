﻿import { Component, OnInit } from '@angular/core';

import { GlobalService } from '@services/global.service';

@Component({
    templateUrl: './clientPackage.html',
    styles: [`
    
    @media (max-width: 370px) {
        div.num-pages{
          display:none;
        }
      
        div.statement-header {
          width: 100% !important;
        }
      
      
      }
      
        div.divTableRow.content:hover {
          background-color: #f5f5f5 !important;
        }
      
        .divTable {
          display: table;
          width: 100%;
          border-left: none;
        }
      
        .divTableRow {
          display: table-row;
        }
      
        .divTableHeading {
          background-color: #EEE;
          display: table-header-group;
        }
      
        .divCell, .divHead {
          display: table-cell;
          padding: 0.5rem;
          font-size: 12px;
          vertical-align: middle;
          border-bottom: 1px solid #ffffff;
        }
      
        .divTableHeading {
          background-color: #EEE;
          display: table-header-group;
          font-weight: bold;
        }
      
        .divTableFoot {
          background-color: #EEE;
          display: table-footer-group;
          font-weight: bold;
        }
      
        .divTableBody {
          display: table-row-group;
        }
      
        div.content:nth-child(even) {
          background-color: #fff;
        }
      
        div.content:nth-child(odd) {
          background-color: #dfefff;
        }
      
        div.search {
          width: 100%;
        }
      
        i.search-icon {
          position: absolute;
          top: 5px;
          right: 6px;
          border-radius: 100%;
          padding: 5px;
        }
      
        div.search-container {
          z-index: 100;
          background-color: #fff;
          margin-top: 1px;
          border: 1px solid #d4d4d4;
          height: 10em;
          position: absolute;
          width: 100%;
          overflow: auto;
        }
      
        div.search-container ul {
          padding: 5px 0;
          margin: 0;
        }
      
        div.search-container ul li {
          padding: 5px 7px;
        }
      
        div.search-container ul li:hover:not(.selected) {
          background-color: #edfdff;
          cursor: pointer;
        }
      
        table > tbody > tr > td:nth-child(4) {
          white-space: nowrap;
        }
      
        table > tbody > tr > td:nth-child(5) {
          white-space: nowrap;
        }
      
        table > tbody > tr > td:nth-child(18) {
          white-space: nowrap;
        }
      
        nav a {
          padding: 8px;
          color: #fff;
          background: linear-gradient(141deg,#48639c 15%, rgba(53, 87, 158, 0.87) 51%, rgba(64, 92, 152, 0.91) 75%);
          line-height: 2.2em;
          text-decoration: none;
          border-top-left-radius: 5px;
          border-top-right-radius: 5px;
        }
      
        .toggleBtn {
          margin-left: 5em;
        }
      
        textarea {
          resize: none;
        }
      
        nav a:hover {
          cursor: pointer;
        }
      
        div.form-row {
          margin-top: 5px;
        }
      
        div.glyphs i {
          padding: 7px 9px;
          background: linear-gradient(141deg,#4b68a5 35%, #546fa7 51%, #466096 75%);
          color: #fff;
          border-radius: 50%;
          font-size: 1.3em;
        }
      
          div.glyphs i:hover {
            background: linear-gradient(141deg,#6583c1 35%, #6583c1 51%, #6583c1 75%);
            cursor: pointer;
          }
      
        div.provider-container {
          overflow: hidden;
          height: 13em;
        }
      
          div.provider-container ul {
            padding: 0.5em 0;
            margin: 0;
          }
      
            div.provider-container ul li {
              list-style: none;
              padding: 0.4em;
            }
      
              div.provider-container ul li:hover:not(.selected) {
                background-color: #edf8ff;
                color: #000;
                cursor: pointer;
              }
      
          div.provider-container:hover {
            overflow: auto;
          }
      
        div.img-profile {
          background-position: center center;
          background-size: cover;
          display: inline-block;
          width: 13rem;
          height: 13rem;
          border-radius: 50%;
        }
      
          div.img-profile i {
            font-size: 80px;
            color: #7d7d7d;
            padding: 10px;
          }
      
        div.period {
          margin: 1em 0;
          font-size: 13px;
        }
      
          div.period span {
            color: #ff2b2b;
          }
      
        table tbody tr:hover {
          background-color: #edf8ff;
          cursor: pointer;
        }
      
        table {
          font-size: 0.9em;
          border-bottom: 2px solid #e0e0e0;
        }
      
        div.controls {
          padding: 1rem 0;
          display: flex;
          position:relative;
        }
      
        div.total-shifts {
          font-weight: bolder;
          color: #3c5998;
          border-left: 1px solid #d5d5d5;
          padding: 0 2rem;
          margin: 0 2rem;
        }
      
        div.total-shifts span {
          padding-top: 4px;
          display: inline-block;
        }
      
        .control-wrapper {
          padding: 1rem;
        }
      
        div.controls div.control-arrow {
          border: 1px solid #d4d4d4;
          color: #6f6f6f;
          padding: 3px 6px;
          border-radius: 3px;
          margin-right: 0.5rem;
        }
      
        div.num-pages {
          position: absolute;
          right: 1rem;
        }
      
        div.shift-table {
          min-width: 810px;
        }
      
        div.controls > span {
          display: inline-block;
          right: 1rem;
          position: absolute;
        }
      
        div.controls div.control-arrow:hover {
          background-color: #f3f3f3;
          cursor: pointer;
        }
      
        div.controls button.print {
          float: right;
          border: none;
          padding: 0.7rem;
          background-color: #3c5a98;
          color: #fff;
          outline: none;
          border-radius: 3px;
        }
      
          div.controls button.print:hover {
            background-color: #4d6bab;
          }
      
        div.last-row {
          margin-bottom: 1em;
        }
      
        td.controls {
          width: 100px;
        }
      
          td.controls i {
            font-size: 1.5em;
            margin-right: 0.5em;
          }
      
            td.controls i:hover {
              vertical-align: middle;
            }
      
        div.inp-container {
          margin-top: 30px;
        }
      
        div.inp {
          display: block;
          border: 1px solid #ccc;
          padding-top: 1.5em;
          padding-bottom: 1em;
        }
      
          div.inp label.label-name {
            position: absolute;
            top: -10px;
            background-color: #7a7a7a;
            padding-left: 1em;
            padding-right: 1em;
            color: #e8e8e8 !important;
          }
      
        .error {
          color: #ff0000;
        }
      
        input.input-no-border {
          outline: none;
          border: 0;
          border-bottom: 1px solid #929292;
          width: 100%;
          line-height: 2em;
          padding-left: 8px;
        }
      
        tr.selected {
          background-color: #6a9eff !important;
          color: #fff;
        }
      
        p.populateLoad {
          margin: 1em;
          color: #802424;
        }
      
        h3.name-title {
          text-align: center;
        }
      
        div.ts-container div.no-data h4 {
          font-size: 18px;
          background-color: #fcf8e3;
          color: #ff4d4d;
          border: 1px solid #faebcc;
          padding: 1em;
          text-align: center;
        }
      
      
      
      
      
        div.pak-container div.no-data {
          font-size: 18px;
          background-color: #fcf8e3;
          color: #ff4d4d;
          border: 1px solid #faebcc;
          text-align: center;
          width: 50em;
          margin: 0 auto;
        }
      
        div.divTableBody.divTableBodyClient {
          display: table !important;
          width: 55em;
          margin: 0 auto;
        }
      
        div.statement-header {
          width: 600px;
          margin: 0 auto;
          font-size: 0.9em;
        }
      
        .div-table {
          display: table;
          width: 80%;
          margin: 0 auto;
          border-spacing: 5px; /*cellspacing:poor IE support for  this*/
        }
      
        .div-table-row {
          display: table-row;
          width: auto;
          clear: both;
        }
      
        .div-table-col {
          float: left; /*fix for  buggy browsers*/
          display: table-column;
        }
      
        div.title {
          width: 40% !important;
        }
      
        div.subj {
          width: 60% !important;
          text-align: center;
        }
      
        div.notice {
          width: 80%;
          margin: 0 auto;
          padding: 0.6em;
          margin-top: 2em;
          margin-bottom: 4%;
          border: 1px solid #c7c7c7;
        }
      
        textarea {
          width: 100%;
          height: 80px;
          resize: none;
          padding: 0.7em;
        }
      
        section.period {
          display: inline !important;
        }
      
        section span.abb {
          font-weight: bold;
        }
      
        div.balances-table {
          border: 1px solid #9cb6b5;
          background: #dee7e7;
        }
      
        div.bal-wrap {
          width: 100%;
        }
      
        div.balances-wrap {
          width: 100%;
          min-width: 200px;
          max-width: 400px;
          padding: 1em;
          margin-bottom: 4%;
          text-align: center;
        }
      
          div.balances-wrap div.div-table-col {
            text-align: left !important;
          }
      
        .bold {
          font-weight: bold !important;
        }
      
        div.bal-col {
          width: 50%;
        }
      
        div.desc-col {
          width: 15% !important;
          text-align: center;
        }
      
        div.divTableRowClient.header div.divCell {
          text-align: center;
        }
      
        div.divTableRowClient.content div.divCell {
          border: none !important;
        }
      
        div.divTableRowClient.content {
          background-color: #fff;
          text-align: left;
        }
      
          div.divTableRowClient.content div.divCell:first-child {
            padding-left: 20px !important;
          }
      
          div.divTableRowClient.content div.divCell:nth-child(3),
          div.divTableRowClient.content div.divCell:nth-child(4),
          div.divTableRowClient.content div.divCell:nth-child(5) {
            text-align: right !important;
          }
      
        div.divTableRowClient.header div.divCell {
          border-top: 1px solid #d4d4d4 !important;
          border-bottom: 1px solid #d4d4d4 !important;
          border-right: 0;
          border-left: 0;
          padding: 1rem 0;
        }
      
        div.divTableRowClient.content:nth-child(odd) {
          background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #e8f2ff), color-stop(1, #eff7ff));
        }
      
        div.divTableRowClient.content.total {
          background: none !important;
        }
      
        div.divTableBody.divTableBodyClient h5 span {
          font-weight: 600;
          color: #4a6eb5;
        }
      
        div.divTableClient {
          border: none !important;
        }
      
        div.divCell.total {
          background-color: #4364a7;
          color: #fff;
          font-size: 1em;
          font-weight: 600;
        }
      
        div.div-table-row div.div-table-col.subj {
          text-align: left !important;
        }
      
          div.div-table-row div.div-table-col.subj.user-list {
            background-color: #4a6eb5;
            border-radius: 3px;
            color: #ffffff;
            border: 1px solid #4f72bb;
            padding: 0 1em;
          }
      
        strong.pperiod {
          color: #61f59d;
        }
      
        div.divTableRow.content.history-client div.divCell:nth-child(3),
        div.divTableRow.content.history-client div.divCell:nth-child(4),
        div.divTableRow.content.history-client div.divCell:nth-child(8),
        div.divTableRow.content.history-client div.divCell:nth-child(9) {
          text-align: right;
        }
      
        div.divTableRow.content.history-client div.divCell:nth-child(10) {
          text-align: center;
        }
      
        .tabrow {
          text-align: left;
          margin: 0;
          padding: 0 10px;
          line-height: 24px;
          border-bottom: 1px solid #d2d2d2;
        }
      
        label.selected {
          border: 1px solid #d2d2d2;
          border-bottom: none;
          background-color: #fff;
        }
      
        .tabrow li label:not(.selected):hover {
          background-color: #f9f9f9;
        }
      
        .tabrow li label:hover {
          cursor: pointer !important;
        }
      
        .tabrow li label {
          padding: 6px 10px;
          margin-bottom: -1px;
          font-weight: 100;
          color: #555;
          border-top-left-radius: 3px;
          border-top-right-radius: 3px;
        }
      
        .tabrow li {
          padding: 0 1px;
          display: inline-block;
        }
      
        div.managelist {
          padding: 0;
        }
      
        i.active {
          color: green !important;
        }
      
        div.packageProvider div.divTableRow.content > div.divCell:nth-child(7),
        div.packageProvider div.divTableRow.content > div.divCell:nth-child(8),
        div.packageProvider div.divTableRow.content > div.divCell:nth-child(14) {
          text-align: center;
        }
      
        div.packageProvider div.divTableRow.content > div.divCell:nth-child(12),
        div.packageProvider div.divTableRow.content > div.divCell:nth-child(13) {
          text-align: right;
        }
      
        div.divTableRow.header {
          height: 5rem;
          background: linear-gradient(141deg,#5e7fc2 35%, #4a67a1 51%, #4c6ca9 75%);
        }
      
          div.divTableRow.header div.divCell {
            color: #fff;
            font-weight: 600;
          }
      
        @media print {
          div {
            -webkit-print-color-adjust: exact;
          }
      
          article.package h4 {
            color: #34c722 !important;
            text-decoration: underline;
          }
      
          div.package-wrapper article:not(.data-table) {
            width: 50% !important;
            margin: 0 auto;
          }
      
          div.print {
            display: none !important;
          }
      
          div.divCell.total {
            background-color: #4364a7 !important;
            color: #fff !important;
          }
        }
      
        div.print {
          margin: 2rem 0;
        }
      
        article.profile > div h4 {
          margin: 0 !important;
        }
      
        article.profile > div h5 {
          margin: 0 !important;
        }
      
        article.package h4 {
          color: #34c722;
          text-decoration: underline;
        }
      
        div.profile-wrapper {
          padding: 1.5rem 0;
          border-style: solid;
          border-top: thick double #666f9a;
          border-bottom: thick double #666f9a;
          border-left: none;
          border-right: none;
        }
      
        div.package-wrapper article:not(.data-table) {
          width: 30%;
          margin: 0 auto;
        }
      
        div.package-wrapper article.note p {
          border: 1px solid #969696;
          padding: 01rem;
          margin: 3rem 0;
        }
      
        ul#balances {
          list-style: none;
          padding: 0;
        }
      
          ul#balances li {
            display: inline-block;
            padding: 0.5rem 0;
            width: 100%;
          }
      
            ul#balances li p:nth-child(2) {
              text-align: right;
            }
      
            ul#balances li.total {
              border-top: 2px solid #888888;
            }
      
            ul#balances li p {
              width: 50% !important;
              float: left;
              margin: 0 !important;
            }
      
        div.prog-options {
          margin-bottom: 1rem;
          display: flex;
        }
      
        a.print {
          float: right;
        }
      
        div.shift-header {
          color: #545454;
          margin: 0 0 4rem 0;
        }
      
        div.table-title {
          height: 8rem;
          border-left: 1px solid #d2d2d2;
          border-right: 1px solid #d2d2d2;
          text-align: center;
          background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #eee), color-stop(1, #fff));
        }
      
          div.table-title label {
            font-size: 3rem;
            color: #4a6eb5;
            margin: 2rem;
            font-weight: 200;
          }
      
        label.cancel {
          margin: 5px 0;
          font-size: 11px;
          color: #ff4444;
          border: 1px solid #ff4444;
          border-radius: 3px;
          padding: 0.5rem;
        }
      
          label.cancel:hover {
            color: #fff;
            cursor: pointer;
            background-color: #ff4444;
          }
      
        label.approve {
          margin: 5px 0;
          font-size: 11px;
          color: #03d600;
          border: 1px solid #03d600;
          border-radius: 3px;
          padding: 0.5rem;
        }
      
          label.approve:hover {
            color: #fff;
            cursor: pointer;
            background-color: #03d600;
          }
      
      
        @media print {
          div.table-title {
            height: 5rem !important;
            margin-top: 4rem;
            border: 1px solid #d2d2d2;
            background: -webkit-gradient(linear, left bottom, left top, color-stop(0, #eee), color-stop(1, #fff));
          }
      
            div.table-title label {
              font-size: large !important;
              margin-top: 1rem !important;
              color: #4a6eb5 !important;
            }
      
          div.divTableRow.header {
            background-color: #afafaf !important;
          }
      
            div.divTableRow.header div.divCell {
              color: #fff !important;
              font-size: xx-small !important;
            }
      
          div.history-client div.divCell {
            font-size: xx-small !important;
          }
        }
      
        .approved {
          background-color: #b3ffb2 !important;
        }
      
        .disapproved {
          background-color: #ffcccc !important;
        }
      
        .timesheet-logo {
          background: url('../assets/logo/timesheet.png') 0% 0% / contain no-repeat;
          background-position: center;
          height: 20rem;
          position: relative;
          width: 100%;
          display: flex;
        }
      
        .circle-disapprove:before {
          content: ' \25CF';
          font-size: 5rem;
          color: #ffcccc
        }
      
        .circle-approve:before {
          content: ' \25CF';
          font-size: 5rem;
          color: #b3ffb2;
        }
      
        .sp-circle span {
          position: absolute;
          top: 2.9rem;
        }
      
      
      
      
        .ts-options {
          display: inline-block;
          padding: 1rem 0;
          text-align: center;
          min-width: 5rem;
          color: #666;
          border: 1px solid #d6d6d6;
          border-radius: 50%;
          margin: 0 1%;
          position: relative;
        }
      
          .ts-options:hover {
            color: #fff;
            background: #666;
            cursor: pointer;
          }
      
          .ts-options i {
            font-size: 2.5rem;
          }
      
            .ts-options i .tooltiptext {
              visibility: hidden;
              font-size: 15px;
              width: 100px;
              background-color: #313131;
              color: #fff;
              text-align: center;
              border-radius: 3px;
              position: absolute;
              z-index: 1;
              top: 110%;
              left: 65%;
              margin-left: -60px;
              padding: 5px 1px;
            }
      
              .ts-options i .tooltiptext::after {
                content: "";
                position: absolute;
                bottom: 100%;
                left: 50%;
                margin-left: -5px;
                border-width: 5px;
                border-style: solid;
                border-color: transparent transparent black transparent;
              }
      
          .ts-options:hover i .tooltiptext {
            visibility: visible;
          }
      
    
    `]

})

export class ClientPackagePrint implements OnInit {
    printObject: any;
    constructor(
       private global: GlobalService
    ) { }

    ngOnInit() {
        this.printObject = ""
    }

    getTotal(data: Array<any>): number {
        let sum = 0; let perData = data;
        let lengthArray: number = 0;

        for (var a = 0; lengthArray = perData.length, a < lengthArray; a++)
            sum = sum + perData[a].ServiceCharge;

        return sum;
    }

    print(){
        window.print();
    }
}