import { Component, OnInit, OnDestroy, AfterViewInit, Input, ViewChild, ViewContainerRef, ComponentRef, ComponentFactoryResolver, HostListener, Output, EventEmitter,ChangeDetectorRef } from '@angular/core'

import { ListService, GlobalService, TimeSheetService, EmailService, ClientService,PrintService } from '@services/index';
import { forkJoin, Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { format } from 'date-fns';
import parseISO from 'date-fns/parseISO'
import * as moment from 'moment';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ShiftDetail } from '../../roster/shiftdetail'
import { EmailMessage, EmailAddress } from '@modules/modules';
import { DMRoster } from '../../roster/dm-roster'
import { CancelShift, RecipientExternal } from '../../roster';
import { toNumber } from 'lodash';
import { NumberInput } from '@angular/cdk/coercion';
import { formatDate } from '@angular/common';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { DomSanitizer } from '@angular/platform-browser';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';

export interface VDataInterface {
  index: number;
  staff: string;
  recipient: string;
  serviceType: string;
  rosterStart: any;
  duration: any;
  rosterEnd: any;
  taMultishift: any;
}

class Address {
  postcode: string;
  address: string;
  suburb: string;
  state: string;

  constructor(postcode: string, address: string, suburb: string, state: string) {
    this.suburb = suburb.trim();
    this.address = address;
    this.postcode = postcode;
    this.state = state;
  }

  getAddress() {
    var _address = `${this.address} ${this.suburb} ${this.postcode}`;
    return (_address.split(' ').join('+')).split('/').join('%2F');
  }
}



@Component({
  selector: 'transport-component',
  templateUrl: './transport.html',
  styleUrls: ['./transport.css']
})
export class Transport implements OnInit, AfterViewInit, OnDestroy {



  @ViewChild(ShiftDetail) detail!: ShiftDetail;

  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '20rem',
    minHeight: '5rem',
    translate: 'no',
    customClasses: []
  };

  allCheckedBranches: boolean = true;
  allCheckedVehicles: boolean = true;
  allCheckedCategories: boolean = true;
  allCheckedCoordinators: boolean = true;

  loadingPending: boolean = false;
  loadingData: boolean = false;
  visible: boolean = true;
  indeterminateBranch = true;
  indeterminateVehicles = true;
  indeterminateCategories = true;
  indeterminateCoordinators = true;

  date: Date = new Date();
  nzSelectedIndex: number = 0;
  dateFormat2 = 'dd/MM/yyyy'
  dateFormat = 'yyyy/MM/dd'
  dataSet: Array<any> = [];
  originalData: Array<any> = [];
  nzWidth: number = 400;
  recordNo: any;
  optionMenuDisplayed: boolean;
  menu: Subject<number> = new Subject();
  TransportModal: boolean;
  TransportModal2: boolean;
  ShowVehicleList: boolean
  changeRemarks: boolean;
  TransportLabel: string;
  modelLable: string = '';
  clickedData: any;
  selectedRecords: Array<any> = [];
  recipientShifts: Array<any> = [];

  branches: Array<any> = [];
  Vehicles: Array<any> = [];
  categories: Array<any> = [];
  coordinators: Array<any> = [];
  Interval: number = 30;
  AlertInterval: number = 30;
  TimeZoneOffset: number = 0;
  AlertServer: boolean;
  AlertAudit: boolean;

  DateTimeForm: FormGroup;
  durationObject: any;
  today = new Date();
  defaultStartTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
  defaultEndTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);

  rdate: any;
  action: string;
  private unsubscribe = new Subject();
  emailTestingVisible: boolean = false;
  totalDays: number = 14;

  AddRosterModel: boolean;
  info = { StaffCode: '', ViewType: '', IsMaster: false, date: '' };
  viewType: string = 'Staff'


  @ViewChild('dynamicRoster', { read: ViewContainerRef })
  dynamicRosterContainer: ViewContainerRef;
  private _dynamicRoster: ComponentRef<DMRoster>;

  @ViewChild('dynamicCancelShift', { read: ViewContainerRef })
  dynamicCancelShift: ViewContainerRef;
  private _dynamicCancelShift: ComponentRef<CancelShift>;

  @ViewChild('dynamicContainerRecipientexternal', { read: ViewContainerRef })
  dynamicContainerRecipientexternal: ViewContainerRef;
  private _dynamicContainerRecipientexternal: ComponentRef<RecipientExternal>;

  ViewCancelShiftModal: boolean;
  private address: Array<any> = [];
  modelValue: any;
  VehicleValue: any
  DriverValue: any
  recipientexternal: boolean;
  token: any;

  selectedindex: number
  mainSelectedindex: number
  selected: Array<any> = [];
  lstDrivers: Array<any> = [];
  lstAddress: Array<any> = [];
  lstMobility: Array<any> = [];
  lstPriority: Array<any> = ['HIGH', 'MEDIUM', 'LOW'];
  ShowDriverList: boolean;
  currentVehicle: any;
  VehicleForm: FormGroup
  defaultAppttime: any;

  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;
  tocken: any;
  loading: boolean = false;
  


  constructor(
    private listS: ListService,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private timeS: TimeSheetService,
    private clientS: ClientService,
    private modalService: NzModalService,
    private emailService: EmailService,
    private fb: FormBuilder,
    private componentFactoryResolver: ComponentFactoryResolver,
    private location: ViewContainerRef,
    private sanitizer: DomSanitizer,
    private printS: PrintService,
    private nzContextMenuService: NzContextMenuService,
    private cd: ChangeDetectorRef,

  ) {

  }
  @HostListener('document:contextmenu', ['$event']) rightClick(event: MouseEvent) {

    //console.log(event);
    event.preventDefault();
  } 
  
  buildForm() {
    this.VehicleForm = this.formBuilder.group({
      priority: ['', Validators.required],
      mobility: ['', Validators.required],
      pickUpFrom: ['', Validators.required],
      ToFrom: ['', Validators.required],
      puZone: ['', Validators.required],
      takeTo: ['', Validators.required],
      doZone: ['', Validators.required],
      appttime: [null, Validators.required],
      returnVehicle: ['', Validators.required],
      startOdo: [''],
      endOdo: [''],
      tripKm: [''],
      deadKm: [''],
      notes: ['']

    });
    this.VehicleForm.get('priority').valueChanges.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(d => {
      const { Name, Email, Phone } = this.VehicleForm.value;



    });
  }
  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    this.buildForm()
    this.visible = true;


    this.loadingPending=true;

    forkJoin([
      this.listS.getlisttimeattendancefilter("BRANCHES"),
      this.listS.getlisttimeattendancefilter("VEHICLES"),
      this.listS.getlisttimeattendancefilter("CASEMANAGERS"),
      // this.listS.getlisttimeattendancefilter("STAFFGROUP")
    ]).subscribe(data => {
      // console.log(data);
      this.branches = data[0].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
      });

      this.Vehicles = data[1].map(x => {
        return {
          label: x,
          value: x,
          checked: false,
          display: true,
          list: [],
          list2: []
        }
      });



      this.coordinators = data[2].map(x => {
        return {
          label: x,
          value: x,
          checked: false
        }
        
      });

      // this.categories = data[3].map(x => {
      //   return {
      //     label: x,
      //     value: x,
      //     checked: false
      //   }
      // });
      this.loadingPending=false;
    });


    let sql = `Select AccountNo from Staff WHERE AccountNo > '!z' AND (COmmencementDate is not null) and (TerminationDate is null)  ORDER BY AccountNo`
    //this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => { this.lstDrivers = data })

    let sql2 = `SELECT RecordNumber, PrimaryAddress, Description as [Type],        Address1 +        Case WHEN Address2 <> '' THEN ' ' + Address2 ELSE '' END +        Case WHEN Suburb <> '' THEN ' ' + Suburb ELSE '' END +        Case WHEN Postcode <> '' THEN ' ' + Postcode ELSE '' END As Address FROM NamesAndAddresses WHERE PersonID = '' UNION SELECT RecordNumber, 0 AS PrimaryAddress, 'DESTINATION' AS [Type], LTrim(CASE WHEN [Name] <> '' THEN [Name] ELSE '' END + '-' + CASE WHEN Address1 <> '' THEN Address1 ELSE '' END + ' ' + CASE WHEN Address2 <> '' THEN Address2 ELSE '' END + ' ' + CASE WHEN Suburb <> '' THEN Suburb ELSE '' END + ' ' + CASE WHEN Postcode <> '' THEN Postcode ELSE '' END + ' ' + CASE WHEN Phone1 <> '' THEN ' ' + Phone1 ELSE '' END) AS [Address] FROM HumanResourceTypes WHERE ([Group] IN ('DESTINATION', '1-NEXT OF KIN', '3-MEDICAL', '8-OTHER')) ORDER BY PrimaryAddress DESC, [Address] ASC`
    let sql3 = `SELECT UPPER([Description]) as MobilityCode FROM DataDomains WHERE [Domain] = 'MOBILITY' ORDER BY Description`
    setTimeout(() => {
      forkJoin([
        this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)),
        this.listS.getlist(sql2).pipe(takeUntil(this.unsubscribe)),
        this.listS.getlist(sql3).pipe(takeUntil(this.unsubscribe)),
        this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe))
      ]).subscribe(data => {
        // console.log(data);
        this.lstDrivers = data[0]
        this.lstAddress = data[1]
        this.lstMobility = data[2]
      });
      this.loadingPending=false;
    }, 1000)

    let tdate;
    this.menu.subscribe(data => {
      this.nzSelectedIndex = data;
      this.optionMenuDisplayed = false;
      switch (data) {
        case 1:
          this.AddRoster(1);
          break;
        case 2:
          this.openCancelShift();
          break;
        case 3:
          this.toMap();
          break;
        case 4:
          this.toMap();
          break;
        case 5:
          this.PickUpPlan()
          break;
        case 6:
          this.dropOffPlan()
          break;
        case 7:
          this.ShowVehicleList = true;
          break;
        case 8:
          this.ShowDriverList = true;
          break;
        case 9:
          this.menuAction();
          break;
        case 10:
          tdate = new Date(format(this.date, this.dateFormat) + ' ' + this.clickedData.pickup);
          this.defaultStartTime = tdate;

          this.TransportLabel = "Set Pickup Time";
          this.TransportModal = true;
          break;
        case 11:
          if (this.clickedData.appt != '')
            tdate = new Date(format(this.date, this.dateFormat) + ' ' + this.clickedData.appt);
          else
            tdate = new Date(format(this.date, this.dateFormat) + ' 00:00' + this.clickedData.appt);

          this.defaultStartTime = tdate;

          this.TransportLabel = "Set Appointment Time";
          this.TransportModal = true;
          break;
        case 12:
          tdate = new Date(format(this.date, this.dateFormat) + '  00:00');
          tdate.setMinutes(this.clickedData.duration)
          this.defaultStartTime = tdate;

          this.TransportLabel = "Set Trip Duration";
          this.TransportModal = true;
          break;
        case 13:
          this.TransportLabel = "Set Pickup Area Code";
          this.TransportModal2 = true;
          break;
        case 14:
          this.TransportLabel = "Set Take To Area Code";
          this.TransportModal2 = true;
          break;
        case 15:
          this.TransportLabel = "Set Mobility Code";
          this.TransportModal2 = true;
          break;
        case 16:
          this.ShowVehicleList = true;
          break;
        case 17:
          this.setVehicleDetail()
          break;
        case 18:
          this.showDetail(this.clickedData);
          break;
        case 19:
          this.loadComponentAsync_Recipient();
          break;
        case 20:
          this.showConfirm();
          break;
      }


    });

  }

  toggleTable(index: number) {

    this.Vehicles[index].display = !this.Vehicles[index].display;
    if (!this.Vehicles[index].display) {
      this.Vehicles[index].list = []
    } else {
      this.Vehicles[index].list = this.Vehicles[index].list2
    }

  }

  build_Vehicle_main_list() {
    let index = 0;
    let sublist = [];
    this.Vehicles.forEach(vehicle => {
      if (index == 0) {
        sublist = this.dataSet.filter(data => (data.vehicle == vehicle.label || data.vehicle == ''));
      } else
        sublist = this.dataSet.filter(data => data.vehicle == vehicle.label);
      vehicle.list = sublist
      vehicle.list2 = sublist
      index = index + 1;
    })
  }
  menuAction() {

    let sql = '';

    if (this.nzSelectedIndex == 10) {

      let time = format(this.defaultStartTime, "HH:mm")
      let endTime = new Date(this.defaultStartTime);
      endTime.setHours(endTime.getHours() + 5)

      let durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, endTime);

      sql = `UPDATE ROSTER SET [Start Time] = '${time}', [BlockNo] = '${durationObject.blockNo}' WHERE RecordNo = ${this.clickedData.recordNo}`;

    } else if (this.nzSelectedIndex == 11) {
      let time = format(this.defaultStartTime, "HH:mm")
      sql = `UPDATE ROSTER SET [Time2] = '${time}' WHERE RecordNo = ${this.clickedData.recordNo}`;

    } else if (this.nzSelectedIndex == 12) {
      let time = format(this.defaultStartTime, "HH:mm")
      let duration = this.getBlockNo(time);
      sql = `UPDATE ROSTER SET [duration] = '${duration}' WHERE RecordNo = ${this.clickedData.recordNo}`;

    } else if (this.nzSelectedIndex == 13) {

      sql = `UPDATE TRANSPORTDETAIL SET PUGridRef = '${this.modelValue}' WHERE RosterID = ${this.clickedData.recordNo}`;

    } else if (this.nzSelectedIndex == 14) {

      sql = `UPDATE TRANSPORTDETAIL SET DOGridRef = '${this.modelValue}' WHERE RosterID = ${this.clickedData.recordNo}`;
    } else if (this.nzSelectedIndex == 15) {

      sql = `UPDATE TRANSPORTDETAIL SET PickupAddress3 = '${this.modelValue}' WHERE RosterID = ${this.clickedData.recordNo}`;
    } else if (this.nzSelectedIndex == 16) {
      this.ShowVehicleList = false;
      sql = `UPDATE TRANSPORTDETAIL SET DropOffAddress3 = '${this.VehicleValue}' WHERE RosterID = ${this.clickedData.recordNo}`;
    } else if (this.nzSelectedIndex == 7) {
      this.ShowVehicleList = false;
      sql = `UPDATE Roster SET ServiceSetting = '${this.VehicleValue}' WHERE Recordno = ${this.clickedData.recordNo}`;
    } else if (this.nzSelectedIndex == 8) {
      this.ShowDriverList = false;
      sql = `UPDATE Roster SET ShiftName = '${this.DriverValue}' WHERE Recordno = ${this.clickedData.recordNo}`;
    } else if (this.nzSelectedIndex == 9) {
      this.ShowDriverList = false;

      this.recipientShifts = this.dataSet.filter(item=>item.clientCode==this.clickedData.clientCode)

     // let records:string =JSON.stringify(this.recipientShifts.map(x=>x.recordNo))
    //  records=records.substring(1,records.length-2)    
    //  let recs=records.replace('\"','',)
      let records =this.recipientShifts.map(x=>x.recordNo)
      let records_str=''
      records.forEach(elm => {
        records_str = records_str  + elm + ","
      });
      records_str = records_str  +  "0";    
      
     //and RosterID <> ${this.clickedData.recordNo}
  
      sql = `UPDATE TRANSPORTDETAIL SET DropOffAddress3 = '${this.currentVehicle}' WHERE RosterID in (${records_str}) `;


    }

    if (sql != '') {
      this.listS.executeSql(sql).subscribe(d => {
        this.TransportModal = false;
        this.TransportModal2 = false;
        this.modelValue = null;
        this.reload()
      });
    } else {
      this.TransportModal = false;
      this.TransportModal2 = false;
    }

  }

  getBlockNo(time: string): number {
    let tt = time.split(":")

    let block = (toNumber(tt[0]) * 12) + (toNumber(tt[1]) / 5);

    return block;

  }

  ngAfterViewInit(): void {
    this.reload();
  }
  ngOnDestroy(): void {


  }

  lblButton = 'double-left'
  open(): void {
    this.visible = !this.visible;
    if (this.visible)
      this.lblButton = 'double-left'
    else
      this.lblButton = 'double-right'
  }



  toMap() {


    var adds = `/${this.clickedData.pickupFrom}
                /${this.clickedData.takeTo}`

    window.open(`https://www.google.com/maps/dir${adds}`, '_blank');
    return false;


  }

  PickUpPlan() {


    var adds = `/${this.selectedRecords[0].PickupAddress1}
                /${this.selectedRecords[1].PickupAddress1}`

    window.open(`https://www.google.com/maps/dir${adds}`, '_blank');
    return false;


  }
  dropOffPlan() {


    var adds = `/${this.selectedRecords[0].takeTo}
                /${this.selectedRecords[1].takeTo}`

    window.open(`https://www.google.com/maps/dir${adds}`, '_blank');
    return false;


  }
  async loadRosterComponent(index: any) {
    const { DMRoster } =
      await import('../../roster/dm-roster');

    // create component factory for DynamicComponent
    const dynamicComponentFactory =
      this.componentFactoryResolver.resolveComponentFactory(DMRoster);

    // create and inject component into dynamicContainer
    this._dynamicRoster = this.dynamicRosterContainer.createComponent(dynamicComponentFactory, this.location.length, null);


    // this._dynamicRoster.instance.loadRoster=this.loadingRoster;
    this._dynamicRoster.instance.RosterDone.subscribe(d => {
      this.reload()
    });

    this._dynamicRoster.instance.reLoadGrid.subscribe(d => {
      this.reload()
    });

    this._dynamicRoster.instance.info = index;

    this._dynamicRoster.instance.loadRoster.subscribe(index)
    this._dynamicRoster.instance.ngAfterViewInit();
  }
  async loadComponentAsync_CancelShift() {
    const { CancelShift } =
      await import('../../roster/cancelshift');

    // create component factory for DynamicComponent
    const dynamicComponentFactory =
      this.componentFactoryResolver.resolveComponentFactory(CancelShift);

    // create and inject component into dynamicContainer
    this._dynamicCancelShift =
      this.dynamicCancelShift.createComponent(dynamicComponentFactory);


    this.ViewCancelShiftModal = true;

    this._dynamicCancelShift.instance.Done.subscribe(data => {
      this.CancelShiftDone(data);
    });
    //this._dynamicCancelShift.instance.loadCancelShift.next({diary:this.clickedData,selected:this.clickedData});
    let shift = {...this.clickedData, carercode: this.clickedData.carerCode, rprogram: this.clickedData.program}
    this._dynamicCancelShift.instance.loadCancelShift.next({ diary: this.dataSet, selected: shift});
    this._dynamicCancelShift.instance.view = true;
    //this._dynamicInstance.changeDetectorRef.detectChanges();
  }

  CancelShiftDone(data: any) {
    this.ViewCancelShiftModal = false;
    if (data == true)
      this.reload();
  }

  cancelDMRoster() {
    this.AddRosterModel = false;
    // this.reload.next(true);
    // this.load_rosters();
  }

  AddRoster(view: number) {
    this.info.IsMaster = false;
    this.info.ViewType = this.viewType;
    if (this.clickedData != null && view == 1) {
      this.info.StaffCode = this.clickedData.clientCode;
      this.info.date = this.clickedData.date;
      this.info.ViewType = 'Recipient'
      this.AddRosterModel = true;
      if (this._dynamicRoster == null) {
        this.loadRosterComponent(this.info);

      } else {

        this._dynamicRoster.instance.loadRoster.next(this.info);
        this._dynamicRoster.instance.ngAfterViewInit();
      }
    }
  }
  openCancelShift() {
    //this.ViewCancelShiftModal=true;

    if (this._dynamicCancelShift == null)
      this.loadComponentAsync_CancelShift();
    else {
      let shift = {...this.clickedData, carercode: this.clickedData.carerCode, rprogram: this.clickedData.program}
      this._dynamicCancelShift.instance.loadCancelShift.next({ diary: this.dataSet, selected: shift});
      this._dynamicCancelShift.instance.view = true;
    }

  }
  async loadComponentAsync_Recipient() {
    const { RecipientExternal } =
      await import('../../roster/RecipientExternal');

    // create component factory for DynamicComponent
    const dynamicComponentFactory =
      this.componentFactoryResolver.resolveComponentFactory(RecipientExternal);

    // create and inject component into dynamicContainer
    this._dynamicContainerRecipientexternal =
      this.dynamicContainerRecipientexternal.createComponent(dynamicComponentFactory, this.location.length, null);

    this.optionMenuDisplayed = false;
    this.recipientexternal = true;

    this._dynamicContainerRecipientexternal.instance.recipientexternalDone.subscribe(data => {
      this.cancelRecipientExternal(data);
    });
    let selected_data = {
      data: this.clickedData.clientCode,
      code: this.clickedData.uniqueid,
      option: 1
    }

    this._dynamicContainerRecipientexternal.instance.AccountNo = selected_data;
    this._dynamicContainerRecipientexternal.instance.isVisible = true;

    //this._dynamicInstance.changeDetectorRef.detectChanges();
  }
  cancelRecipientExternal(value: any) {
    this.recipientexternal = value.recipientexternal;
  }

  mousedblclick(event: any, value: any) {
    event.preventDefault();
    event.stopPropagation();
    this.clickedData = value;
    this.showDetail(value);
  }
  showDetail(data: any) {

    this.getRoster(data.recordNo).pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(d => {
      this.detail.isVisible = true;
      this.detail.data = this.selected_roster(d);
      this.detail.viewType = 'Staff';
      this.detail.editRecord = false;
      this.detail.ngAfterViewInit();
    });

  }

  shiftChanged(value: any) {

    this.reload();

  }
  getRoster(recordNo: any) {

    return this.timeS.getrosterRecord(recordNo);


  }
  selected_roster(r: any): any {
    let rst: any;

    rst = {

      "shiftbookNo": r.recordNo,
      "date": r.roster_Date,
      "startTime": r.start_Time,
      "endTime": r.end_Time,
      "duration": r.duration,
      "durationNumber": r.dayNo,
      "recipient": r.clientCode,
      "program": r.program,
      "activity": r.serviceType,
      "payType": r.payType,
      "paytype": r.payType.paytype,
      "pay": r.pay,
      "bill": r.bill,
      "approved": r.Approved,
      "billto": r.billedTo,
      "debtor": r.billedTo,
      "notes": r.notes,
      "selected": false,
      "serviceType": r.type,
      "recipientCode": r.clientCode,
      "staffCode": r.carerCode,
      "serviceActivity": r.serviceType,
      "serviceSetting": r.serviceSetting,
      "analysisCode": r.anal,
      "serviceTypePortal": "",
      "recordNo": r.recordNo

    }

    return rst;
  }

  TimeDifference(data: any, t: number = 0) {
    let diff: number = 0
    let StartTime;
    let EndTime;
    if (t == 1) {
      StartTime = parseISO(new Date(data.date + ' ' + data.rosterEnd).toISOString());
      EndTime = parseISO(new Date(data.actualEnd).toISOString());
    } else {
      StartTime = parseISO(new Date(data.date + ' ' + data.rosterStart).toISOString());
      EndTime = parseISO(new Date(data.actualStart).toISOString());
    }

    diff = this.globalS.computeTimeDifference(StartTime, EndTime);

    return diff;
  }
  numStr(n: number): string {
    let val = "" + n;
    if (n < 10) val = "0" + n;

    return val;
  }
  BlockToTime(blocks: number) {
    return this.numStr(Math.floor(blocks / 12)) + ":" + this.numStr((blocks % 12) * 5)
  }
  view(index: number) {
    this.nzSelectedIndex = index;
    this.reload();
  }
  RecordClicked(event: any, value: any, i: number, ind: number) {

    this.mainSelectedindex =ind;
    this.currentVehicle = this.Vehicles[ind].label;
    this.selectedindex = i;
    //let tindex = this.selected.indexOf(value.recordNo);


    if (event.ctrlKey || event.shiftKey) {
      if (this.selectedRecords.length < 2) {
        this.selectedRecords.push(value)
        this.selected.push(value.recordNo)
      }

    } else {
      this.clickedData = value;
      this.selectedRecords = [];
      this.selected = [];

      if (this.selectedRecords.length <= 0) {
        this.selectedRecords.push(value)
        this.selected.push(value.recordNo)
      }
    }
  }
  rightClickMenu($event: MouseEvent, menu: NzDropdownMenuComponent, value: any, i:number, ind:number) {

    this.nzContextMenuService.create($event, menu);
   // event.preventDefault();

    this.mainSelectedindex =ind;
    this.currentVehicle = this.Vehicles[ind].label;
    this.selectedindex = i;

    //this.optionMenuDisplayed = true;

    this.clickedData = value;

    if (this.selectedRecords.length <= 0) {
      this.selectedRecords.push(value)
    }

    console.log(value);
    let date = this.clickedData.date;
    this.defaultStartTime = parseISO(new Date(date + " " + this.clickedData.pickup).toISOString());
    //this.defaultEndTime = parseISO(new Date(date + " " + this.clickedData.rosterEnd).toISOString());



  }
  roundTo(num: number, places: number) {
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  }


  // Branches
  updateAllBranches(): void {
    this.indeterminateBranch = false;

    if (this.allCheckedBranches) {
      this.branches = this.branches.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.branches = this.branches.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }

  updateSingleCheckedBranches(): void {
    this.allCheckedBranches = false;

    if (this.branches.every(item => !item.checked)) {
      this.allCheckedBranches = false;
      this.indeterminateBranch = false;
    } else if (this.branches.every(item => item.checked)) {
      this.allCheckedBranches = true;
      this.indeterminateBranch = false;
    } else {
      this.indeterminateBranch = true;
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }
  // End Branches

  // Vehicles
  updateAllVehicles(): void {
    this.indeterminateVehicles = false;

    if (this.allCheckedVehicles) {
      this.Vehicles = this.Vehicles.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.Vehicles = this.Vehicles.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }

  updateSingleCheckedVehicles(): void {
    this.allCheckedVehicles = false;

    if (this.branches.every(item => !item.checked)) {
      this.allCheckedVehicles = false;
      this.indeterminateVehicles = false;
    } else if (this.branches.every(item => item.checked)) {
      this.allCheckedVehicles = true;
      this.indeterminateVehicles = false;
    } else {
      this.indeterminateVehicles = true;
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }
  // End Vehicles


  // Categories
  updateAllCategories(): void {
    this.indeterminateCategories = false;

    if (this.allCheckedCategories) {
      this.categories = this.categories.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.categories = this.categories.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }

  updateSingleCheckedCategories(): void {
    
    this.allCheckedCategories = false;

    if (this.categories.every(item => !item.checked)) {
      this.allCheckedCategories = false;
      this.indeterminateCategories = false;
    } else if (this.categories.every(item => item.checked)) {
      this.allCheckedCategories = true;
      this.indeterminateCategories = false;
    } else {
      this.indeterminateCategories = true;
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }
  // End Categories


  // Categories
  updateAllCoordinators(): void {
    this.indeterminateCoordinators = false;

    if (this.allCheckedCoordinators) {
      this.coordinators = this.coordinators.map(item => {
        return {
          ...item,
          checked: true
        };
      });
    } else {
      this.coordinators = this.coordinators.map(item => {
        return {
          ...item,
          checked: false
        };
      });
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }

  updateSingleCheckedCoordinators(): void {
    this.allCheckedCoordinators = false;
    if (this.coordinators.every(item => !item.checked)) {
      this.allCheckedCoordinators = false;
      this.indeterminateCoordinators = false;
    } else if (this.coordinators.every(item => item.checked)) {
      this.allCheckedCoordinators = true;
      this.indeterminateCoordinators = false;
    } else {
      this.indeterminateCoordinators = true;
    }
    this.ApplyFilters();
    this.build_Vehicle_main_list();
  }
  // End Categories
  ApplyFilters() {

    let branch = (this.branches.filter(item => item.checked).map(x => x.label)).join(',')
    this.dataSet = this.originalData.filter(x => branch.includes(x.branch))
    console.log(branch);

    this.dataSet = this.originalData.filter(x => {

      return (
        ((this.branches.filter(item => item.checked).map(x => x.label).join(',')).includes(x.branch) || this.allCheckedBranches) &&
        ((this.Vehicles.filter(item => item.checked).map(x => x.label).join(',')).includes(x.vehicle) || this.allCheckedVehicles) &&
        ((this.coordinators.filter(item => item.checked).map(x => x.label).join(',')).includes(x.coOrdinator) || this.allCheckedCoordinators)
      )

    })
  }
  reload() {
    this.loadingData = true;

    let sql = `SELECT DISTINCT CASE WHEN [Roster].[ServiceSetting] = 'BOOKED' THEN     ' BOOKED' ELSE     LTRIM(RTRIM([Roster].[ServiceSetting])) END AS 
                [Vehicle], [Roster].[RecordNo] AS recordNo, [Roster].[ShiftName] AS Driver, [Roster].[Service Type] As [Code], [Roster].[Start Time] AS [Pickup], isnull([Roster].[Time2],'') AS [Appt], CASE WHEN 
                [Recipients].[Surname/Organisation] <> '' THEN [Recipients].[Surname/Organisation] + ', ' ELSE '' END + CASE WHEN [Recipients].[FirstName] <> '' THEN [Recipients].[FirstName] ELSE '' END AS Name, 
                [Recipients].[Gender] AS Gdr, [PUGridRef], [PickupAddress1] AS [PickupFrom], [DOGridRef], [DropOffAddress1] AS [TakeTo], 
                CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact END AS Phone, [PickupAddress3] AS [mob], [DropOffAddress3] AS [RVehicle], CAST([Roster].[Notes] as NVarchar(4000)) AS [Remarks],
                [Roster].[Program] As [Program], [ItemTypes].[RosterGroup], [Roster].[Date], [Roster].[Duration] * 5 AS Duration, [Roster].[Type], [Roster].[Status], [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[Type] As [RecipientType], 
                [Recipients].[AgencyDefinedGroup] As [Category], [Roster].[DMStat], LTRIM(RTRIM([Recipients].[AccountNo])) AS AccountNo, CASE WHEN [Recipients].[Notes] IS NULL THEN 'NoAlerts' WHEN Convert(Varchar, [Recipients].[Notes]) = '' THEN 'NoAlerts' ELSE 'HasAlerts' END AS AlertStatus 
                ,[Roster].[Carer code] As [CarerCode],[Roster].[Client code] As [ClientCode], Recipients.UniqueID, dbo.RTF2Text(isnull(Roster.Notes,'')) as notes, isnull(TransportDetail.PickUpAddress3,'') as Mobility, [Recipients].[Branch]
                FROM ROSTER LEFT JOIN Recipients ON [Roster].[Client Code] = [Recipients].[AccountNo] LEFT JOIN TransportDetail ON [TransportDetail].[RosterID] = [Roster].[RecordNo]INNER JOIN ItemTypes ON [Roster].[Service Type] = ItemTypes.Title INNER JOIN RecipientPrograms ON Recipients.UniqueID = RecipientPrograms.PersonID LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
                WHERE (Date = '${format(this.date, this.dateFormat)}' AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'TRANSPORT')) 
                ORDER BY [Vehicle], Date, [Pickup], [Name] `

    this.dataSet = [];
    this.listS.getlist(sql).subscribe(data => {
      this.loadingData = false;
      //console.log(data)
      this.dataSet = data;
      this.originalData = data;
      if (data.length > 0)
        this.recordNo = data[0].recordNo;
      this.ApplyFilters();
      this.build_Vehicle_main_list();

    }, () => {
      this.loadingPending = false;
    });
  }

  trackByIndex(_: number, data: VDataInterface): number {
    return data.index;
  }

  onItemSelected(data: any) {
    this.VehicleValue = data;
  }

  onItemdbclicked(event: any, data: any) {
    this.VehicleValue = data;
    this.menuAction();

  }

  onItemSelectedDriver(data: any) {
    this.DriverValue = data;
  }

  onItemdbclickedDriver(event: any, data: any) {
    this.DriverValue = data;
    this.menuAction();

  }

  showConfirm(): void {
    //var deleteRoster = new this.deleteRoster();
    this.modalService.confirm({
      nzTitle: 'Confirm',
      nzContent: 'Are you sure you want to delete roster',
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
        new Promise((resolve, reject) => {
          setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
          this.deleteRoster();

        }).catch(() => console.log('Oops errors!'))


    });
  }
  deleteRoster() {
    for (let v of this.selectedRecords) {
      this.clickedData = v;

      if (v.status != 1) {
        let sts = this.getStatus(v.status)
        this.globalS.eToast("Transport", `${sts}  roster (${v.startTime}-${v.endTime}) cannot be deleted`);
        continue;
      }
      setTimeout(() => {
        this.ProcessRoster("Delete", v);
      }, 100);
    }
  }
  getStatus(status: string) {
    let val: string;

    switch (parseInt(status)) {
      case 1:
        val = "Unapproved"
        break
      case 2:
        val = "Approved"
        break
      case 3:
        val = "Billed"
        break
      case 4:
        val = "Paid and Billed"
        break
      case 5:
        val = "Paid"
        break
      default:
        val = "Undefined"
    }

    return val;
  }
  ProcessRoster(Option: any, record: any): any {



    let inputs = {
      "opsType": Option,
      "user": this.token.user,
      "recordNo": record.recordNo,
      "isMaster": false,
      "roster_Date": record.date,
      "start_Time": record.pickup,
      "carer_code": record.carerCode,
      "recipient_code": record.clientCode,
      "notes": 'Roster Deletion from Transport',
      'clientCodes': record.recipient
    }
    this.timeS.ProcessRoster(inputs).subscribe(data => {
      //this

      this.selectedRecords = [];
      this.reload();


    });
  }
  setVehicleDetail() {
    this.changeRemarks = true;
    this.VehicleForm.patchValue({

      mobility: this.clickedData.mobility,
      pickUpFrom: this.clickedData.pickup,
      takeTo: this.clickedData.takeTo,
      appttime: this.clickedData.appttime,
      PuZone: this.clickedData.puGridRef,
      DoZone: this.clickedData.doGridRef,
      returnVehicle: this.clickedData.vehicle,
      notes: this.clickedData.notes

    })
  }
  setRemarks() {

    const { priority, mobility, pickUpFrom, takeTo, puZone, doZone, appttime, returnVehicle,
      startOdo, endOdo, tripKm, deadKm, notes } = this.VehicleForm.value;

    let input = {
      "RosterID": this.clickedData.recordNo,
      "Priority": priority,
      "Mobility": mobility,
      "PickUpFrom": pickUpFrom,
      "TakeTo": takeTo,
      "PuZone": puZone,
      "DoZone": doZone,
      "Appttime": format(appttime, "HH:mm"),
      "ReturnVehicle": returnVehicle,
      "StartOdo": startOdo,
      "EndOdo": endOdo,
      "TripKm": tripKm,
      "DeadKm": deadKm,
      "Notes": notes
    }

    this.timeS.updateVehicle(input).subscribe(data => {

      this.changeRemarks = false;
      this.reload();
    },
      error => {
        this.globalS.eToast('Vehicle Update', error.value)
        console.log(error);
        this.changeRemarks = false;
      }

    );


  }
  handleCancelTop(): void {
    this.drawerVisible = false;
    this.pdfTitle = ""
    this.tryDoctype = ""
}
  generatePdf(){
    var Title;            
  //console.log(this.dataSet)
    
      Title = "Transport Sheet";
      this.loading = true;
      this.drawerVisible = true;
    
               
        //console.log(this.dataSet)      
        
            const data = {
                "template": { "_id": "kQaPOCUFXrpgSKH7"},//"6BoMc2ovxVVPExC6" },
                "options": {
                    "reports": { "save": false },                        
                    "sql": this.dataSet,                        
                    "userid": this.tocken.user,
                    "txtTitle":  Title + ' for ' + format(this.date, 'yyyy/MM/dd'),                      
                }
            }              
            this.loading = true;           
                    
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
                        this.pdfTitle = Title+".pdf"
                        this.drawerVisible = true;                   
                        let _blob: Blob = blob;
                        let fileURL = URL.createObjectURL(_blob);
                        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                        this.loading = false;
                        this.cd.detectChanges();                       
                    }, err => { 
                        console.log(err);
                        this.loading = false;
                        this.modalService.error({
                            nzTitle: 'TRACCS',
                            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                            nzOnOk: () => {
                                this.drawerVisible = false;
                            },
                        });
            });                            
            return;        
           
             
}

}
