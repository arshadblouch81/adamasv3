import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';

import {
    RouteGuard,
    AdminStaffRouteGuard,
    CanDeactivateGuard,
    LoginGuard,
    AdminRouteGuard,
    ByPassGuard
  } from '@services/index'
  
import {
    StaffAttendanceAdmin,
    StaffCompetenciesAdmin,
    StaffContactAdmin,
    StaffDocumentAdmin,
    StaffGroupingsAdmin,
    StaffLoansAdmin,
    StaffHRAdmin,
    StaffPSAdmin,
    StaffIncidentAdmin,
    StaffLeaveAdmin,
    StaffOPAdmin,
    StaffPayAdmin,
    StaffPersonalAdmin,
    StaffPositionAdmin,
    StaffhistoryAdmin,
    StaffReminderAdmin,
    StaffTrainingAdmin,
    StaffInformationAdmin,
  } from './index';

const routes: Routes = [
    {
        path: 'personal',
        component: StaffPersonalAdmin
      },
      {
        path: 'contacts',
        component: StaffContactAdmin
      },
      {
        path: 'pay',
        component: StaffPayAdmin,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'loans',
        component: StaffLoansAdmin
      },
      {
        path: 'leave',
        component: StaffLeaveAdmin
      },
      {
        path: 'reminders',
        component: StaffReminderAdmin
      },
      {
        path: 'op-note',
        component: StaffOPAdmin
      },
      {
        path: 'hr-note',
        component: StaffHRAdmin
      },
      {
        path: 'ps-note',
        component: StaffPSAdmin
      },
      {
        path: 'competencies',
        component: StaffCompetenciesAdmin
      },
      {
        path: 'training',
        component: StaffTrainingAdmin
      },
      {
        path: 'incident',
        component: StaffIncidentAdmin
      },
      {
        path: 'document',
        component: StaffDocumentAdmin
      },
      {
        path: 'staff-time-attendance',
        component: StaffAttendanceAdmin,
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'position',
        component: StaffPositionAdmin
      },
      {
        path: 'groupings-preferences',
        component: StaffGroupingsAdmin
      },
      {
        path: 'staff-history',
        component: StaffhistoryAdmin
      },
      {
        path: 'other-information',
        component:StaffInformationAdmin,
        canDeactivate: [CanDeactivateGuard]
      }
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,
    NzSpinModule,
    NzDrawerModule,
    NzButtonModule,
    NzFormModule,
    NzDatePickerModule,
    NzModalModule,
    NzTableModule
    
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class StaffAdminModule {}

