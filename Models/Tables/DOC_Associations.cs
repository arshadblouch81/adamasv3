using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class DOC_Associations
    {
        [Key]
        public int RecordNo { get; set; }
        public string Title { get; set; }
        public string FileTypes { get; set; }
        public string Application { get; set; }
        public string Template { get; set; }

        [Column("CanCreateFile", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? CanCreateFile { get; set; }

        [Column("IsForm", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? IsForm { get; set; }

        public string LocalUser { get; set; }
        public string TRACCSType { get; set; }
        public string MainGroup { get; set; }
        public string MinorGroup { get; set; }

        [Column("XDeletedRecord", TypeName = "bit")]
        [DefaultValue(false)]
        public bool? XDeletedRecord { get; set; }

        public DateTime? XEndDate { get; set; }
       
    }
}