﻿using System;

namespace AdamasV3.Models.Dto
{
    public class AccountReceiptDto
    {
        public string Branch { get; set; }
        public string Source { get; set; }
        public string Programs { get; set; }
        public string Type { get; set; }
        public string PaymentType { get; set; }
        
        public DateTime? Date { get; set; }
        public double? Amount { get; set; }
        public string Notes { get; set; }

        public string AccountName { get; set; }

    }
}
