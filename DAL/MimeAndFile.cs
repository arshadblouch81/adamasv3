﻿using eg_03_csharp_auth_code_grant_core.Models;
using System.Collections.Generic;
using System.Linq;

namespace AdamasV3.DAL
{
    public static class MimeAndFile
    {
        public static string GetMimeType(List<EnvelopeDocItem> documents, string docSelect)
        {
            // Step 2. Look up the document from the list of documents 
            EnvelopeDocItem docItem = documents.FirstOrDefault(d => docSelect.Equals(d.DocumentId));

            // Process results. Determine the file name and mimetype
            string docName = docItem.Name;
            bool hasPDFsuffix = docName.ToUpper().EndsWith(".PDF");
            bool pdfFile = hasPDFsuffix;

            // Add .pdf if it's a content or summary doc and doesn't already end in .pdf
            string docType = docItem.Type.ToLower();
            if (("content".Equals(docType) || "summary".Equals(docType)) && !hasPDFsuffix)
            {
                docName += ".pdf";
                pdfFile = true;
            }
            // Add .zip as appropriate
            if ("zip".Equals(docType))
            {
                docName += ".zip";
            }

            // Return the file information
            string mimetype;
            if (pdfFile)
            {
                mimetype = "application/pdf";
            }
            else if ("zip".Equals(docType))
            {
                mimetype = "application/zip";
            }
            else
            {
                mimetype = "application/octet-stream";
            }

            return mimetype;
        }

        public static string GetDocument(List<EnvelopeDocItem> documents, string docSelect)
        {
            // Step 2. Look up the document from the list of documents 
            EnvelopeDocItem docItem = documents.FirstOrDefault(d => docSelect.Equals(d.DocumentId));

            // Process results. Determine the file name and mimetype
            string docName = docItem.Name;
            bool hasPDFsuffix = docName.ToUpper().EndsWith(".PDF");
            bool pdfFile = hasPDFsuffix;

            // Add .pdf if it's a content or summary doc and doesn't already end in .pdf
            string docType = docItem.Type.ToLower();
            if (("content".Equals(docType) || "summary".Equals(docType)) && !hasPDFsuffix)
            {
                docName += ".pdf";
                pdfFile = true;
            }
            // Add .zip as appropriate
            if ("zip".Equals(docType))
            {
                docName += ".zip";
            }
            return docName;
        }
    }   
}
