namespace Adamas.Models.Modules{
    public class InputShiftBooked{
        public string RecipientCode { get; set; }
        public string ShiftDate { get; set; }
        public bool Pending { get; set; }
    }
}