import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ViewChild, ComponentRef, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService,PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import * as groupArray from 'group-array';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';


 const FILTERS: Array<string> = [
    'CARE DOMAIN',
    'CREATOR',
    'DISCIPLINE',
    'PROGRAM',
    'ROSTER/SVC GROUP'
 ]
 

@Component({
    styleUrls:['./history.css'],
    templateUrl: './history.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class RecipientHistoryAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    
    checked: boolean = false;
    isDisabled: boolean = false;
    loading: boolean = false;

    FILTERS = FILTERS;

    filterParameters: any;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    RptDisplay: any;
    selectedRow:number;
    activeRowData:any;
    modalOpen:boolean;
    isLoading:boolean;
    openModel:boolean;
  

    timeSheetVisible:Subject<any> = new Subject<boolean>();
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService,
        private sanitizer: DomSanitizer,
        private printS:PrintService,
       
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'history')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {  
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();      
        this.user = this.sharedS.getPicked();
        // this.search(this.user);
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
    selectedItemGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
    }
    search(user: any, filters: any = null) {
        this.loading = true;
        this.clientS.gethistory(user.code, filters).subscribe(data => {
            this.tableData = data.list;
            this.originalTableData = data.list;

            this.loading = false;
            this.cd.markForCheck();
        });
    }

    trackByFn(index, item) {
        return item.id;
    }

    showAddModal(){

    }

    showEditModal(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
       this. find_roster(data.recordNumber)
      
       // this.loadComponentAsync(data);
    }
    find_roster(RecordNo:number):any{
        let rst:any;
     

    this.timeS.getrosterRecord(RecordNo).pipe(takeUntil(this.unsubscribe)).subscribe(r => {;
        
        rst = {
         
            "shiftbookNo": r.recordNo,
            "date": r.roster_Date,
            "startTime": r.start_Time,
            "endTime":    r.end_Time,
            "duration": r.duration,
            "durationNumber": r.dayNo,
            "recipient": r.clientCode,
            "program": r.program,
            "activity": r.serviceType,
            "payType": r.payType,   
            "paytype": r.payType.paytype,  
            "pay": r.pay,                   
            "bill": r.bill,            
            "approved": r.Approved,
            "billto": r.billedTo,
            "debtor": r.billedTo,
            "notes": r.notes,
            "selected": false,
            "serviceType": r.type,
            "recipientCode": r.clientCode,            
            "staffCode": r.carerCode,  
            "serviceActivity": r.serviceType,
            "serviceSetting": r.serviceSetting,
            "analysisCode": r.anal,
            "serviceTypePortal": "",
            "status": r.status,
            "recordNo": r.recordNo
            
        }
        this.timeSheetVisible.next(rst);
        this.openModel=true;
        this.cd.detectChanges();
    });
        
    
  
} 
    shiftChanged(event:any){

    }
      
    handleCancel() {
        this.modalOpen = false;
        this.isLoading = false;
      
    }
     
    delete(index: number) {
        const { recordNumber } = this.tableData[index];
        
        this.timeS.deleteremindersrecipient(recordNumber).subscribe(data => {
            this.globalS.sToast('Success', 'Data added');
            this.search(this.user);
            this.handleCancel();
        });
    }
    close(index: number){
        this.router.navigate(['/admin/recipient/personal'])
    }
    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];
    dragDestination = [
        'Date',
        'Program',
        'Event',
        'Staff',
        'Amount',
        'Notes'
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
        const propName = parent ? parent + '.' + key : key;
        if (typeof obj[key] === 'object') {
            this.flattenObj(obj[key], propName, res);
        } else {
            res[propName] = obj[key];
        }
        }
        return res;
    }

    drop(event: CdkDragDrop<string[]>) {

        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => x.toLowerCase());

        var convertedObj = groupArray(this.originalTableData, dragColumns);
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }

    filterChange(data: any){
    //    console.log(data.display);
        this.RptDisplay = data.display;
        this.search(this.user, data);
    }

//Mufeed 9 April 2024
handleCancelTop(){
    this.drawerVisible = false;
    this.pdfTitle = "";
    this.tryDoctype="";
  }
  
  InitiatePrint(){

    var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
    this.RptDisplay
    //'"+this.user.id+"'    
    fQuery = " select top "+this.RptDisplay+" R.Recordno, R.Date, R.Program, R.[Service Type] As Event, r.[Carer code] AS Staff, ISNULL(BillQty, 0) * ISNULL([Unit Bill Rate], 0) AS Amount , [R].Notes, [R].Status, [R].InvoiceNumber, IT.DATASET AS Discipline, HRT.HRT_DATASET AS [Care Domain] FROM Roster R INNER JOIN ItemTypes IT ON  [R].[Service Type] = Title INNER JOIN HumanResourceTypes HRT ON [HRT].[Name] = [R].Program WHERE [Client Code] = '"+this.user.code+"' AND YearNo > 2000  AND [R].[Type] = 7 ORDER BY [Date] DESC ";
    //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
    txtTitle = "Recipient History";       
    Rptid =   "8WTerq14eNFSAnz0"  

    //console.log(fQuery)
      const data = {
                  
          "template": { "_id": Rptid },                                
          "options": {
              "reports": { "save": false },                
              "sql": fQuery,
              "Criteria": lblcriteria,
              "userid": this.tocken.user,
              "txtTitle": txtTitle,
              "Extra": this.user.code,
                                                              
          }
      }
      this.loading = true;
      this.drawerVisible = true;         
          this.printS.printControl(data).subscribe((blob: any) => {
          this.pdfTitle = txtTitle+".pdf"
              this.drawerVisible = true;                   
              let _blob: Blob = blob;
              let fileURL = URL.createObjectURL(_blob);
              this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
              this.loading = false;
              this.cd.detectChanges();
          }, err => {
              console.log(err);
              this.loading = false;
              this.modalService.error({
                  nzTitle: 'TRACCS',
                  nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                  nzOnOk: () => {
                      this.drawerVisible = false;
                  },
              });
          });
          return;      
  }


}