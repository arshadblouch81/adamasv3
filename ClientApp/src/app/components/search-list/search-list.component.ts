import { Component, OnInit, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, forwardRef, Input,Output, OnDestroy, OnChanges, SimpleChanges,HostListener, EventEmitter, ViewChild, ViewChildren, ElementRef, QueryList } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

import { mergeMap, takeUntil, concatMap, switchMap, debounceTime,distinctUntilChanged } from 'rxjs/operators';
import { TimeSheetService, GlobalService, view, ClientService, StaffService, ListService, UploadService, months, days, gender, types, titles, caldStatuses, roles, ShareService } from '@services/index';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { DomSanitizer } from '@angular/platform-browser';

const noop = () => {
};

interface SearchProperties{
  uniqueID: string,
  accountNo: string,
  agencyDefinedGroup: string,
  sysmgr: boolean,
  view: string
}

interface Refresh {
  refresh: boolean
}

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SearchListComponent),
    }
  ],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchListComponent implements OnInit , OnChanges, AfterViewInit, OnDestroy, ControlValueAccessor {
  
  private onTouchedCallback: any = () => { };
  private onChangeCallback: any = () => { };
  private unsubscribe: Subject<void> = new Subject();
  private searchChangeEmit: Subject<void> = new Subject();
  private inputSubject: Subject<string> = new Subject<string>();

  showAll: boolean = false;
  
  // 0 if recipient / 1 if staff
  @Input() view: number;
  @Input() reload: boolean;
  @Input() isOpen: boolean = false;
  @Input() classes: any;
  @Input() iconClasses: any;
  
  @Input() searched:Subject<boolean>= new Subject()
  @Output() emitEvent:EventEmitter<any>=new EventEmitter();

  phoneModal: boolean = false;
  phoneSearch: string;
  searchModel: any;
  staffType = 'A';
  listsAll: Array<any> = [];
  lists: Array<any> = [];
  loading: boolean = false;
  
  pageCounter: number = 1;
  take: number = 50;
  activeInactive: boolean;
  
  globalUser: any;
  
  switchValue: boolean = false;
  displayStaffProfile: boolean = false;
  displayRecipientProfile: boolean = false;
  selectedIndex: number = -1;


  // nzFilterOption  = () => true;
  constructor(
    private router: Router,
    private cd: ChangeDetectorRef,
    private timeS: TimeSheetService,
    private globalS: GlobalService,
    private clientS: ClientService,
    private shareS: ShareService,
     private sanitizer:DomSanitizer,
    private ModalS:NzModalService,
    private nzContextMenuService: NzContextMenuService
    ) {
      
      this.searchChangeEmit.pipe(
        debounceTime(100)
        ).subscribe(data => {
          if(this.globalS.isEmpty(data)){
            this.lists = this.listsAll.slice(0, 200);
          } else {
            this.lists = this.listsAll.filter(x => x.accountNo).filter(x => (x.accountNo).toLowerCase().indexOf(data) > -1);
          }
        });
        this.inputSubject.pipe(
          debounceTime(1000), // Wait 1000ms after the user stops typing
          distinctUntilChanged() // Only emit if the value changes
        ).subscribe(value => {
          this.processInput(value); // Call your processing function here
        });
      }
      
      ngOnInit(): void {
        this.globalUser = this.globalS.decode();
        this.search();
        this.searched.subscribe(v => { 
            this.searchModel=v;   
            this.selectedIndex = this.lists.findIndex(x => x.accountNo == v);
            let item = this.lists.find(x => x.accountNo == v);
            this.selectedItem=item;
            this.scrollToSelectedItem();        
      });

    }
      
      ngOnChanges(changes: SimpleChanges) {
        for (let property in changes) {
          if (property == 'reload' &&
          !changes[property].firstChange &&
          changes[property].currentValue != null) {
            this.search();
          }
        }
       
      }
      
      ngAfterViewInit(): void{
        // const user: SearchProperties = {
        //   agencyDefinedGroup: 'event.agencyDefinedGroup',
        //   accountNo: 'event.accountNo',
        //   uniqueID: 'event.uniqueID',
        //   sysmgr: true,
        //   view: this.view == 0 ? 'recipient' : 'staff'
        // }
        
        // this.onChangeCallback(user);
      }
      
      ngOnDestroy(): void{
        this.unsubscribe.next();
        this.unsubscribe.complete();
      }
      selectedItem:any;
      change( item:any) {
        
        this.selectedItem=item;
        let user: SearchProperties | null;
        
        if (!item) {
          user = null;
        } else {
          user = {
            agencyDefinedGroup: item.agencyDefinedGroup,
            accountNo: item.accountNo,
            uniqueID: item.uniqueID,
            sysmgr: true,
            view: this.view == 0 ? 'recipient' : 'staff'
          }
        }

        this.searchModel = user.accountNo;        
        this.isOpen=false;

        if(this.view == 0){
          this.clientS.getprofile(user.accountNo).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            
            this.shareS.emitRecipientStatus(data);
            this.emitEvent.emit('New Referral');
          });
        }
        

        // if(this.view == 1)
        // {
        //   if(!this.displayStaffProfile){
        //     this.router.navigate(['/admin/staff/personal']); 
        //   }
        //   this.displayStaffProfile =  true;
        // }else
        // {
        //   if(!this.displayRecipientProfile){
        //     this.router.navigate(['/admin/recipient/personal']) 
        //   }
        //   this.displayRecipientProfile =  true;
        // }

        this.onChangeCallback(user);       
      }
      selectAll(event: FocusEvent): void {
        const inputElement = event.target as HTMLInputElement;
        if (inputElement) {
          inputElement.select();
        }
      }
     

      textChange(event:any){
        this.inputSubject.next(event); 
      }

      processInput(event: any): void {
        let charlength=1;
        if (event==null || event=='') return;

        let item=this.lists.find(x => x.accountNo.startsWith(event.toUpperCase()));
        if (item==null && event.length<charlength) return;
        if (item==null && event.length>=charlength){
          item=this.selectedItem;
        } 
        let index= this.lists.indexOf(item);
        this.selectedIndex=index;
        this.selectedItem=item;
      
        this.searchModel=event;

        this.scrollToSelectedItem();
        if (event.length<charlength) return;
        setTimeout(()=>{
          this.change(item);
          this.searchModel=item.accountNo;
        },500);
       
      }
      @ViewChild('listContainer') listContainer!: ElementRef; // For the scrollable container
      @ViewChildren('listItem') listItems!: QueryList<ElementRef>; // For each list item
    
      scrollToSelectedItem(): void {
        const selectedElement = this.listItems.toArray()[this.selectedIndex];
        if (selectedElement) {
          selectedElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
        }
      }
      clickEvent(event:any, item:any, i:number) {
        this.selectedIndex=i;
        this.selectedItem=item;
        const rowElement = (event.currentTarget as HTMLElement);
        rowElement.focus();
        let user: SearchProperties | null;
        
        if (!item) {
          user = null;
        } else {
          user = {
            agencyDefinedGroup: item.agencyDefinedGroup,
            accountNo: item.accountNo,
            uniqueID: item.uniqueID,
            sysmgr: true,
            view: this.view == 0 ? 'recipient' : 'staff'
          }
        }

        this.searchModel = user.accountNo;        
        this.isOpen=false;

        if(this.view == 0){
        this.clientS.getprofile(user.accountNo).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
         
          this.shareS.emitRecipientStatus(data);
        });
        }
       

        this.onChangeCallback(user);       
      }
      changeStatus(event){
        if(event == 'A')  this.activeInactive = false;
        if(event == 'B')  this.activeInactive = true;
        this.search();
      }
      
      search(search: string = null) {
        this.loading = true;
        if (this.view == 0) {
          this.searchRecipient(search);
        }
        if (this.view == 1) {
          this.searchStaff(search);
        }
      }

  searchStaff(search: any = null) {
    this.lists = [];

    this.timeS.getstaff({
      User: this.globalUser.nameid,
      SearchString: '',
      IncludeInactive:this.activeInactive,
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.listsAll = data;
      this.lists = data;
      this.loading = false;
      this.cd.markForCheck();
    });

    // this.timeS.getstaffpaginate({
    //   User: this.globalS.decode().nameid,
    //   SearchString: '',
    //   Skip: this.pageCounter,
    //   Take: this.take
    // }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //   this.lists = this.lists.concat(data);
    //   this.loading = false;
    //   this.cd.markForCheck();
    // });

  }

  loadMore(){
    this.pageCounter = this.pageCounter + 1;
    this.timeS.getstaffpaginate({
      User: this.globalUser.nameid,
      SearchString: '',
      Skip: this.pageCounter,
      Take: this.take
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.lists = this.lists.concat(data);
      this.loading = false;
      this.cd.markForCheck();
    });
  }

  searchRecipient(search: any = null): void {
    this.lists = []
    this.timeS.getrecipients({
      User: this.globalUser.nameid,
      SearchString: '',
      IncludeInactive:this.activeInactive ?? true
      
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      if(search){
        var index = data.findIndex(x => x.uniqueID == search.id);
        this.change(data[index]);
        if (index>0){
          this.selectedIndex=index;
          this.scrollToSelectedItem();
        
        }
          
      }

      this.lists = data;
      this.loading = false;
      this.cd.markForCheck();
    });
  }

  //From ControlValueAccessor interface
  writeValue(value: any) {
    this.cd.detectChanges();
    if (value != null) {
     this.search(value);
    }

    if (value instanceof Object) {
      this.searchModel = null;
      this.onChangeCallback(null);
    }
  }

  //From ControlValueAccessor interface
  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  //From ControlValueAccessor interface
  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }

  viewResult(): string{
    return this.view == 0 ? 'Search Recipient' : 'Search Staff';
  }


  clearPhoneModal(){
    this.phoneModal = false;
  }

  listPhoneRecipientsList: Array<any>;
  searchPhone(){
    if(this.view == 0){
      this.timeS.getrecipientsbyphone(this.phoneSearch)
        .subscribe(data => {
          // this.lists = data;
          this.listPhoneRecipientsList = data;
          this.cd.markForCheck();
        });
    }else{
      this.timeS.getstaffbyphone(this.phoneSearch)
        .subscribe(data => {
          // this.lists = data;
          this.listPhoneRecipientsList = data;
          this.cd.markForCheck();
        });
    }
  }

  
  selected(index: number){
    this.selectedIndex = index;
  }

  gotoRecipient(){
    // console.log(this.selectedIndex + "selected index");
    let selected = this.listPhoneRecipientsList[this.selectedIndex];
    if(selected == null){
      this.globalS.iToast('Info', 'please Select any name from the search list');
      return
    }
    // console.log(selected + "selected");
    this.searchModel = this.lists[this.lists.map(x => x.uniqueID).indexOf(selected.uniqueID)];
    // console.log(this.searchModel + "----");
    this.change(this.searchModel);
    this.globalS.sToast('Success', 'Staff Display Sucessfully');
    this.clearPhoneModal();
  }

  clickOutsideMenu(data: any){
    console.log(data);
    if(data.value){
      this.showAll = false;
    }
  }

  changeStatusBool(data: any){
    this.activeInactive = data;
    this.search();
  }
  // Scroll the dropdown to the selected item

  scrollToSelectedItem_old(): void {
    const dropdown = document.querySelector('.ng-dropdown-panel');
    if (!dropdown || this.selectedIndex < 0) return;

    const options = dropdown.querySelectorAll('.ng-option');
    if (options.length > 0) {
      const selectedOption = options[this.selectedIndex] as HTMLElement;
      if (selectedOption) {
        selectedOption.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
      }
    }
  }
  
      @HostListener('document:contextmenu', ['$event']) rightClick(event: MouseEvent) {
       // this.nzContextMenuService.create($event, menu);
        //console.log(event);
        event.preventDefault();
    }
  onKeyPress(event: KeyboardEvent): void {
     if (event.key === 'ArrowDown' || event.key === 'ArrowUp') {
      event.preventDefault();
     }
     switch (event.key) {
      case 'ArrowDown':
        this.selectedIndex = (this.selectedIndex + 1) % this.lists.length;
        break;
      case 'ArrowUp':
        this.selectedIndex = (this.selectedIndex - 1 + this.lists.length) % this.lists.length;
        break;
      case 'Enter':
        if (this.selectedIndex >= 0) {
          this.searchModel =this.lists[this.selectedIndex].accountNo;
        }
        break;
    }
    if (this.selectedIndex==null) return;
    let data = this.lists[this.selectedIndex];
    this.searchModel = data.accountNo;
    this.change(data) ;

    // Automatically scroll the table to keep the selected row in view
    const rows = document.querySelectorAll('tr');
  
    if (rows[this.selectedIndex]) {
    rows[this.selectedIndex].scrollIntoView({ block: 'nearest' });
   }
  }
  
  contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, data:any): void {
               
                  this.nzContextMenuService.create($event, menu);
  }

  menuclick(event: any, index: number){
  
    let evnt:any = '';
    switch(index){
      case 1:
        evnt='New';
        break;
      case 2:
        evnt='Save';
        break;
      case 3:
          evnt='Change';
          break;
      case 4:
          evnt='Delete';
          break;
      case 5:
          evnt='Export';
          break;
      case 6:
          evnt='Exit';
          break;
    }
    this.emitEvent.emit(evnt);
  }
}
