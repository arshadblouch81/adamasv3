namespace Adamas.Models.Modules{
    public interface IEmailConfiguration {
        string SmtpServer { get; set; }
        int SmtpPort { get; set; }
        string SmtpUsername { get; set; }
        string SmtpPassword { get; set; }
        bool SmtpEnableSampleMail { get; set; }
        string SmtpOriginEmail { get; set; }
        string SmtpOriginName { get; set; }
        string SmtpRecipientEmail { get; set; }
        string SmtpCCEmail { get; set; }
        string SmtpReplyTo { get; set; }
        

    }
    public class EmailConfiguration : IEmailConfiguration {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public bool SmtpEnableSampleMail { get; set; }
        public string SmtpOriginEmail { get; set; }
        public string SmtpOriginName { get; set; }
        public string SmtpRecipientEmail { get; set; }
        public string SmtpCCEmail { get; set; }
        public string SmtpReplyTo { get; set; }
    } 
}