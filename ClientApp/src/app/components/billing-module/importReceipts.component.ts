import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import format from 'date-fns/format';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { formatDate } from '@angular/common';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';


interface selectedInputs {
    User: string,
    Password: string,
    b_AutosetLeaveOptions: boolean,
    b_DeleteAdminTravel: boolean,
    b_ProcessUnapproved: boolean,
    i_DayOne: number,
    s_Branches: string,
    s_Clients: string,
    s_Cycles: string,
    s_DestinationStartDate: string,
    s_DestinationEndDate: string,
    s_Recipients: string,
    s_Programs: string,
    s_Staff: string
}
interface Roster {
    clientCode: string;
    staffCode: string;
    program: string;
    serviceType: string;
    payType: string;
    date: string;
    startTime: string;
    duration: string;
    groupFlag: string;
    billQty: string;
    payQty: string;
    payUnit: string;
    BillUnit: string;
    location: string;
    notes: string;

}

@Component({
    selector: 'app-import-receipts',
    templateUrl: './importReceipts.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
.btn1 {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}

`]
})

export class ImportReceiptsComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    
    listData: Array<any>;
    loading: boolean = false;
    inputForm: FormGroup;
    check: boolean = false;
    userRole: string = "userrole";
    mainTitle: string = "UNAPPROVED SERVICES";
    tocken: any;
    unapprovedClientHoursList: Array<any>;
    tableData: Array<any>;
    postLoading: boolean = false;
    endDate: any;
    startDate: any;
    PayPeriodLength: number;
    printLoad: boolean = false;
    fileTypeList: Array<any>;
    
    recordsAdded: Array<any> = [];
    processing: boolean;
    JSONData: any;
    text: any;
    allowedExt = ["csv"];
    index: number = 0;
    inputs: any = {} as selectedInputs;

    lstInterval: any = [
        { name: 'Fortnightly Roster', selected: true },
        { name: '4 Weekly Roster', selected: true },
        { name: 'Cycle 3', selected: true },
        { name: 'Cycle 4', selected: true },
        { name: 'Cycle 5', selected: true },
        { name: 'Cycle 6', selected: true },
        { name: 'Cycle 7', selected: true },
        { name: 'Cycle 8', selected: true },
        { name: 'Cycle 9', selected: true },
        { name: 'Cycle 10', selected: true }
    ]
    
    constructor(
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private timeS: TimeSheetService,
    ) { }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.buildForm();
        this.populateDropdowns();
        // this.loadData();
        this.loading = false;
    }
    
    buildForm() {
        this.inputForm = this.formBuilder.group({
            startDate: '01/12/2021',
            endDate: '01/12/2021',
            fileType: '',
            chkUpdateClientPrices: true,
        });
    }

    handleCancel() {
        this.inputForm.reset();
        this.buildForm();
        this.postLoading = false;
        this.isVisible = false;
        this.router.navigate(['/admin/billing']);
    }

    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    populateDropdowns() {
        this.fileTypeList = ['TRACCS', 'QIKBOOKS DEPOSIT REPORT EXPORT', 'CENTRACARE'];
    }

    convertFile(e) {
        const reader = new FileReader();
        const file = e.files[0].name;
        let ext: string = file.split('.')[1]

        if (this.isInArray(this.allowedExt, ext)) {
            reader.readAsText(e.files[0]);
            reader.onload = () => {
                let text = reader.result;
                this.text = text;
                // console.log(text);
                this.csvJSON(text);
                // this.ImportRoster()
            };
        } else {
            console.log('file not supported');
        }
    }

    isInArray(array, word) {
        return array.indexOf(word.toLowerCase()) > -1;
    }

    csvJSON(csvText) {
        var lines = csvText.split("\n");
        var result = [];
        var headers = lines[0].split(",");
        // console.log(headers);
        headers = this.rest_header(headers);
        // console.log(headers);
        for (var i = 1; i < lines.length - 1; i++) {
            var obj = {};
            var currentline = lines[i].split(",");
            for (var j = 0; j < headers.length; j++) {
                obj[headers[j]] = currentline[j];
            }
            result.push(obj);
        }
        //return result; //JavaScript object
        //console.log(JSON.stringify(result)); //JSON
        this.JSONData = JSON.stringify(result);
    }

    rest_header(headers: any) {
        let new_headers: any = headers;
        let s_Fields: string;
        let regExp: any;
        let i: number = 0;
        headers.forEach(x => {
            s_Fields = x.toUpperCase();
            if (s_Fields.includes('PROGRAM'))
                new_headers[i] = "program";
            else if (s_Fields.includes('STAFF'))
                new_headers[i] = "carerCode";
            else if (s_Fields.includes('CLIENT'))
                new_headers[i] = "clientCode";
            else if (s_Fields.includes('RECIPIENT'))
                new_headers[i] = "clientCode";
            else if (s_Fields.includes('SERVICE'))
                new_headers[i] = "serviceType";
            else if (s_Fields.includes('PAYTYPE') || s_Fields.includes('PAY TYPE'))
                new_headers[i] = "payType";
            else if (s_Fields.includes('DATE'))
                new_headers[i] = "date";
            else if (s_Fields.includes('START') || s_Fields.includes('TIME'))
                new_headers[i] = "startTime";
            else if (s_Fields.includes('DURATION'))
                new_headers[i] = "duration";
            else if (s_Fields.includes('GROUP') || s_Fields.includes('GROUPFLAG'))
                new_headers[i] = "groupFlag";
            else if (s_Fields.includes('BILLQTY') || s_Fields.includes('BILL QTY'))
                new_headers[i] = "billQty";
            else if (s_Fields.includes('PAYQTY') || s_Fields.includes('PAY QTY'))
                new_headers[i] = "payQty";
            else if (s_Fields.includes('LOCATION') || s_Fields.includes('LOC'))
                new_headers[i] = "location";
            else if (s_Fields.includes('NOTES') || s_Fields.includes('NOTE'))
                new_headers[i] = "notes";
            i = i + 1;
        });
        return new_headers;
    }

    Run_Process() {
        this.index = 9;
        if (this.index == 9) {
            this.ImportRoster();
            return;
        }
        this.Build_Criteria()
        this.inputs.User = "sysmgr"
        this.inputs.Password = "sysmgr"
        this.inputs.i_DayOne = 5;
        console.log(this.inputs);
        // this.timeS.postCreateCopyRoster(this.inputs).subscribe(d => {
        //     console.log(d)
        // })
    }
    
    ImportRoster() {
        let listRoster: any = <Roster>JSON.parse(this.JSONData);
        // console.log(listRoster);
        let values = '';
        let s_SQLInsert = '';
        this.processing = true;
        const obsvr = new Observable(observer => {
            setTimeout(() => {
                listRoster.forEach(rst => {
                    // let date = new Date(rst.date)
                    // let stime = new Date(rst.date + ' ' + rst.startTime)
                    // let etime = new Date(rst.date + ' ' + rst.startTime)

                    // etime.setMinutes(stime.getMinutes() + (rst.duration * 5));

                    // let durationObject = this.globalS.computeTimeDATE_FNS(stime, etime);

                    let inputs = {
                        // date: format(date, 'yyyy/MM/dd'),
                        // dayno: parseInt(format(date, 'd')),
                        // startTime: format(stime, 'HH:mm'),
                        // monthNo: format(date, 'M'),
                        // yearNo: format(date, 'yyyy'),
                        serviceDescription: rst.payType,
                        serviceSetting: rst.serviceSetting || "",
                        anal: rst.analysisCode == '' || rst.analysisCode == null ? rst.anal : rst.analysisCode
                    };

                    console.log(inputs)

                    // this.timeS.posttimesheet(inputs).subscribe(data => {                       
                    //     this.recordsAdded.push(data);
                    //     let history: any = {
                    //         operator: this.tocken.user,
                    //         auditDescription: 'CREATE NEW ROSTER ENTRY FROM IMPORT',
                    //         actionOn: 'WEB-ROSTER',
                    //         whoWhatCode: data,
                    //         traccsUser: this.tocken.user
                    //     }
                    //     this.timeS.postaudithistory(history).subscribe(d => { });
                    // });
                });
                observer.next(this.recordsAdded);
            }, 1000);
        });

        obsvr.subscribe(result => {
            setTimeout(() => {
                console.log(result);
                let d = result[0];
                this.processing = false;
                this.processing = false;
                if (d!=null) {
                    this.globalS.sToast('Import Receipts', `Import processed successfully!`);
                } else {
                    this.globalS.eToast('Import Receipts', `Something went wrong while processing! `);
                }
            }, 2000);
        });
    }
    
    Build_Criteria() {
        let criteria: string;
        // let s_StartDate: any = format(this.startDate, 'yyy/MM/dd');
        // let s_EndDate: any = format(this.endDate, 'yyy/MM/dd');
        let s_StartDate: any = this.startDate;
        let s_EndDate: any = this.endDate;
        criteria = ` ([Roster].[Date] BETWEEN '${s_StartDate}' AND '${s_EndDate}') AND ([Roster].[Start Time] BETWEEN '${this.startDate}' AND '${this.endDate}')`
        this.inputs.s_DestinationStartDate = String(s_StartDate).toString();
        this.inputs.s_DestinationEndDate = String(s_EndDate).toString();
        let selectedvalues = this.lstInterval.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        this.inputs.s_Cycles = selectedvalues;
    }

    print() {
    }
}
