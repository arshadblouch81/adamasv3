
import {takeUntil, switchMap, distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import  { TimeSheetService,GlobalService } from '../../services/index';

import { Subject } from 'rxjs';
import * as moment from 'moment';

@Component({
    selector: '',
    templateUrl: './history.html',
    styles:[
        `

        
        @media (max-width: 1000px)  {        


            .web-first{
                display:none;
            }
            .mobile-first{
                display:block;
            }

            .stackview {
                display: inline-block;
                width: 100%;
                padding: 15px;            
            }        
            

        }
        

        @media (min-width:1000px) { 
            .web-first{
                display:block;
            }
            .mobile-first{
                display:none;
            }
        }      


        th{
            font-size: 12px;
            background: linear-gradient(rgb(90, 234, 188), rgb(79, 208, 166));
            color: #fff;
            font-weight: normal;
        }
        .unapprove{
            background:#ffe1e1;
        }
        .approve{
            background:#d5ffd0;
        }
        tr td{
            font-size: 12px;
        }
        .loading{
            text-align:center;
            margin-top:1rem
        }
        table{
            margin:0;
        }

        
        `
    ]
})

export class HistoryProvider implements OnInit, OnDestroy {
    private unsubscribe$ = new Subject()
    private acccountNo: string;
    dateStream = new Subject<any>();
    
    loading: boolean;
    hideNotes: boolean;
    timesheets: Array<any> = [];
    finalList:Array<any> = []

    sDate: any;
    eDate:any;

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService
    ){
        this.dateStream.pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((x: any) => this.getTimeSheet({ 
            AccountNo: this.acccountNo, 
            personType: 'Staff',
            startDate: moment(this.sDate).format('YYYY/MM/DD'),
            endDate:  moment(this.eDate).format('YYYY/MM/DD')
        })), takeUntil(this.unsubscribe$),)
            .subscribe(data => {
            this.loading = false;
            this.timesheets = data;
        })
    }

    ngOnInit(){
        this.acccountNo = this.globalS.decode().code;
        this.sDate = moment();
        this.eDate = moment().endOf('month');
        this.dateStream.next();
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    gwapo(data: Array<any>){
        this.finalList = [];
        this.finalList = data;
    }

    getTimeSheet(data: Dto.GetTimesheet){
        this.loading = true;
        this.timesheets = [];
        return this.timeS.gettimesheets({
            AccountNo: data.AccountNo,
            personType: data.personType,
            startDate: data.startDate,
            endDate: data.endDate
        })
    }
}
