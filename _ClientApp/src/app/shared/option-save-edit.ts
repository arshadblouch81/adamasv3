import { Component, Output, EventEmitter } from '@angular/core'

@Component({
    selector: 'option-edit-save',
    templateUrl: './option-save-edit.html',
    styles:[
        `
        ul{
            list-style:none;
            margin: 12px 0;
        }
        ul li i{
            padding: 5px;
            margin-right: 15px;
            border-radius: 10px;            
            cursor:pointer;
            transition: 0.2s;
        }
        .save:hover{
            background: #ffa51e;
            color: #fff;
        }
        
        `
    ]
})

export class OptionSaveEditComponent {
    @Output() clickEmit: EventEmitter<any> = new EventEmitter();

    save(){
        this.clickEmit.emit('save');
    }

}