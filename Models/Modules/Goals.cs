using AdamasV3.Models.Modules;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class Goals {
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; } = "";
        public string Notes { get; set; } = "";
        public string Goal { get; set; } = "";
        public string Level { get; set; } = "";
        public string Percent { get; set; } = "";
        public DateTime? DateArchived { get; set; }
        public DateTime? LastReviewed { get; set; }
        public DateTime? Anticipated  { get; set; } 
        public List<PlanStrategyDto> Strategies { get; set; }
    }
}