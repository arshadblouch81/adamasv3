﻿$AdamasProjectPath = "D:\Programming\Adamas\adamasv3\AdamasV3.csproj"
$fileExists = Test-Path -Path $AdamasProjectPath

if($fileExists){
    $xml = [Xml] (Get-Content $AdamasProjectPath)
    $target = $xml.SelectSingleNode("//Target[@Name='PublishRunWebpack']")
    $exec = $target.SelectSingleNode("//Exec[@Command]")

    #Overwrite Execute Value
    $exec.SetAttribute('Command',"ng build --prod --aot --base-href /testing")

    #Save File
    $xml.Save($AdamasProjectPath)
} 