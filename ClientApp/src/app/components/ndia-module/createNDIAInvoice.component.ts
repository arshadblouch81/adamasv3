import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
// import { ListService, MenuService } from '@services/index';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { debounceTime, timeout, switchMap, takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { forkJoin, Observable, EMPTY, Subject, combineLatest } from "rxjs";
import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-create-ndia-invoice',
  styleUrls: ['./createNDIAInvoice.component.css'],
  templateUrl: './createNDIAInvoice.component.html',
})
export class CreateNDIAInvoiceComponent implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;
  loggedComputerName: any;
  loggedUserName: any;

  branchList: Array<any>;
  selectedBranches: any;
  allBranchesChecked: boolean;
  lockBranches: any = false;

  programList: Array<any>;
  selectedPrograms: any;
  allProgramsChecked: boolean;
  lockPrograms: any = false;

  categoriesList: Array<any>;
  selectedCategories: any;
  allCategoriesChecked: boolean;
  lockCategories: any = false;

  fundingList: Array<any>;
  selectedFunding: any;
  allFundingChecked: boolean;
  lockFunding: any = false;

  recipientsList: Array<any>;
  selectedRecipients: any;
  allRecipientsChecked: boolean;
  lockRecipients: any = false;

  billingCycleList: Array<any>;
  selectedBillingCycle: any;
  selectedWeeks: any;
  lockBillingCycle: any = false;
  allWeeksChecked: boolean;
  checkedWeekly: boolean;
  checkedFornightly: boolean;
  checkedMonthly: boolean;
  checked4Weekly: boolean;
  checkedMonthly1: boolean;
  checkedWeekly1: boolean;

  selectedPackage: any;
  batchHistoryList: Array<any>;
  weeksList: Array<any>;
  tableData: Array<any>;
  PayPeriodLength: number;
  PayPeriodEndDate: any;
  loading: boolean = false;
  allchecked: boolean = false;
  modalOpen: boolean = false;
  billingType: Array<any>;
  invLineType: Array<any>;
  gstSetting: Array<any>;
  myOB: Array<any>;
  AccountPackage: Array<any>;
  invType: Array<any>;
  invoiceType: any;
  current: number = 0;
  inputForm: FormGroup;
  dateFormat: string = 'dd/MM/yyyy';
  postLoading: boolean = false;
  title: string = "Debtor Updates and Exports";
  token: any;
  tocken: any;
  check: boolean = false;
  userRole: string = "userrole";
  chkAllDates: boolean;
  endDate: any;
  startDate: any;
  id: string;
  private unsubscribe: Subject<void> = new Subject();
  indeterminate = true;
  batchNumber: any;
  BatchDetail: any;
  BatchType: any;
  operatorID: any;
  updatedRecords: any;
  currentDateTime: any;
  bankDetailsString: any;
  chkSepInvRecipient: any;
  chkSepPageRecipient: any;
  chkPrintZeroInvoices: any;
  chkPrintZeroInvoiceLine: any;
  chkGenInternalInvoice: any;
  selectedDirectoryPath: any;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.buildForm();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  handleCancel() {
    this.inputForm.reset();
    this.selectedPackage = '';
    this.buildForm();
    this.postLoading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/ndia']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      //General Tab
      invoiceDate: new Date(),
      chkAllDates: false,
      startDate: '01/12/2021',
      endDate: '01/12/2021',
      chkDebtorUpdate: true,
      chkReceiptUpdate: false,
      chkOtherMasterFile: false,
      chkForceAllDeb: false,

      //Branches & Funding
      invType: 'NDIA Claim Update Invoices',

      //Interface Tab
      selectedPackage: '',
      chkCreateExportFile: false,
      invPrefix: '',
      gstSet: '',
      myOB: '',
      chkTraccsGL: false,
      chkConDateDes: false,
      chkUsePkgGL: false,
      chkPrefGL: false,
      chkAppendActy: false,
      chkTraccsDateMyOB: false,
      chkExcClientDesc: false,
      chkExcContIdDesc: false,
      chkHyphenatedGLAcc: false,
      chkUseActivityGL: false,
      chkUseNDIAItem: false,
      chkUseNDIAClaim: false,
      chkFirstNameSurNam: false,
      chkAppendInvT: false,
      chkRecipientTermDate: false,
      chkBlankInventoryCode: false,
      chkAlternativeDesc: false,
      chkBlankTrackingNamOpt: false,
      chkIncluEmailPosAdd: false,
      selectedDirectoryPath: '',

      //Invoice Presentation Tab
      bankDetailsString: '',
      chkSepInvRecipient: false,
      chkSepPageRecipient: false,
      chkGenInternalInvoice: false,
      chkPrintZeroInvoices: false,
      chkPrintZeroInvoiceLine: false,
      printerCopies: '1',
      chkStaffCodeInv: false,
      rdoStaffCodeInv: "rdoAccountCode",
      chkUpdateBrokServ: true,
      chkBillRecAbsense: true,
    });

    this.allWeeksChecked = true;
    this.checkedWeekly = true;
    this.checkedFornightly = true;
    this.checkedMonthly = true;
    this.checked4Weekly = true;
    this.checkedMonthly1 = true;
    this.checkedWeekly1 = true;

    this.loading = true;

    // Get Bank Details
    this.billingS.getBankDetails(null).subscribe(bankDetails => {
      var bankData = ('Bank: ' + bankDetails[0].bankName + ' BSB: ' + bankDetails[0].bankBSB + ' Account Name: ' + bankDetails[0].bankAccountName + ' A/c#: ' + bankDetails[0].bankAccountNumber);
      this.inputForm.patchValue({
        bankDetailsString: bankData,
      });
    });

    // Get PayPeriod End Date
    this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
      if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
        this.endDate = dataPayPeriodEndDate[0].payPeriodEndDate;
        this.inputForm.patchValue({
          endDate: this.endDate,
        })
      }
      else {
        this.inputForm.patchValue({
          endDate: new Date()
        });
        this.endDate = new Date
      }
      this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
        if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
          this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
        }
        else {
          this.PayPeriodLength = 14
        }
        var firstDate = new Date(this.endDate);
        firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
        this.startDate = formatDate(firstDate, 'MM-dd-yyyy', 'en_US');
        this.inputForm.patchValue({
          startDate: this.startDate,
        });
      });
    });

    // Get Billing Cycle
    this.billingS.getBillingCycle(this.check).subscribe(data => {
      this.billingCycleList = data;
      this.tableData = data;
      this.allWeeksChecked = true;
      this.checkAll(6);
    });

    // Get Branches Details
    this.menuS.getlistbranches(this.check).subscribe(data => {
      this.branchList = data;
      this.tableData = data;
      this.allBranchesChecked = true;
      this.checkAll(1);
    });

    // Get Programs Details
    let dataPassPrograms = {
      condition: formatDate(new Date(), 'MM-dd-yyyy', 'en_US'),
    }
    this.billingS.getProgramslist(dataPassPrograms).subscribe(dataPrograms => {
      this.programList = dataPrograms;
      this.tableData = dataPrograms;
      this.allProgramsChecked = true;
      this.checkAll(2);
    });

    // Get Funding Details
      this.fundingList = ['NDIA'];
      this.tableData = this.fundingList;
      this.allFundingChecked = true;
    // this.billingS.getFundingDetails().subscribe(dataFundingDetails => {
    //   this.fundingList = dataFundingDetails;
    //   this.tableData = dataFundingDetails;
    //   this.allFundingChecked = true;
    //   this.checkAll(5);
    // });

    // let isExist = this.billingS.isDataExists(this.tableData, 'NDIA');
    // if (isExist) {
    //   console.log(isExist)
    // }

    // Get Categories Details
    this.timesheetS.getlistcategories().subscribe(data => {
      this.categoriesList = data;
      this.tableData = data;
      this.allCategoriesChecked = true;
      this.checkAll(3);
    });

    // Get Recipients Details
    this.billingS.getRecipientsDetails().subscribe(dataRecipientsDetail => {
      this.recipientsList = dataRecipientsDetail;
      this.tableData = dataRecipientsDetail;
      this.allRecipientsChecked = true;
      this.checkAll(4);
    });

    //load Batch History Detail
    this.billingS.getBillingBatchHistory().subscribe(data => {
      this.batchHistoryList = data;
      this.tableData = data;
    });

    //get Windows Logged User
    this.billingS.getCurrentUser().subscribe(getCurrentUser => {
      this.loggedComputerName = getCurrentUser.computerName;
      this.loggedUserName = getCurrentUser.userName;
    })

    // Dropdown lists
    this.billingType = ['CONSOLIDATED BILLING', 'PROGRAM BILLING'];
    this.invLineType = ['PRODUCT LINE', 'SERVICE LINE', 'SUNDRY LINE'];
    this.gstSetting = ['AUTO', '0', '1', '2', '3', '4'];
    this.myOB = ['ITEM SALES', 'SERVICE SALES', 'TIME BILLING', 'TIME BILLING ORIGINAL', 'TIME BILLING ACCOUNT RIGHT 2015'];
    this.AccountPackage = [
      'ABM',
      'AFRHA',
      'ATTACHE',
      'AUTHORITY ACCOUNTING',
      'CPL NDIA',
      'EPICOR',
      'EPICOR9',
      'FAMMIS 2 EXPORT',
      'FAMMIS EXPORT',
      'FINANCE ONE',
      'GENERIC',
      'GREENTREE ACCOUNTING',
      'MS DYNAMICS GP',
      'MYOB',
      'PATHWAYS ACCOUNTING',
      'SAGE EVOLUTION',
      'SUN ACCOUNTING',
      'T1 ETL EXPORT',
      'T1 EXPORT',
      'XERO ACCOUNTING'
    ];
    this.invType = ['NDIA Claim Update Invoices'];

    this.selectedDirectoryPath = `C:\\Users\\${this.loggedUserName}\\Documents\\`

    this.loading = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  onKeyPress(data: KeyboardEvent) {
    return this.globalS.acceptOnlyNumeric(data);
  }

  checkedStatus(): void {
    if (this.allWeeksChecked == true) {
      this.checkedWeekly = true;
      this.checkedFornightly = true;
      this.checkedMonthly = true;
      this.checkAll(6);
    } else {
      this.checkedWeekly = false;
      this.checkedFornightly = false;
      this.checkedMonthly = false;
      this.allWeeksChecked = true;
      this.uncheckAll(6);
    }
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedBranches = event;
    if (index == 2)
      this.selectedPrograms = event;
    if (index == 3)
      this.selectedCategories = event;
    if (index == 4)
      this.selectedRecipients = event;
    if (index == 5)
      this.selectedFunding = event;
    if (index == 6)
      this.selectedBillingCycle = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allBranchesChecked == false) {
        this.lockBranches = true
      }
      else {
        this.branchList.forEach(x => {
          x.checked = true;
          this.allBranchesChecked = x.description;
          this.allBranchesChecked = true;
        });
        this.lockBranches = false
      }
    }
    if (index == 2) {
      if (this.allProgramsChecked == false) {
        this.lockPrograms = true
      }
      else {
        this.programList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockPrograms = false
      }
    }
    if (index == 3) {
      if (this.allCategoriesChecked == false) {
        this.lockCategories = true
      }
      else {
        this.categoriesList.forEach(x => {
          x.checked = true;
          this.allCategoriesChecked = x.description;
          this.allCategoriesChecked = true;
        });
        this.lockCategories = false
      }
    }
    if (index == 4) {
      if (this.allRecipientsChecked == false) {
        this.lockRecipients = true
      }
      else {
        this.recipientsList.forEach(x => {
          x.checked = true;
          this.allRecipientsChecked = x.description;
          this.allRecipientsChecked = true;
        });
        this.lockRecipients = false
      }
    }
    if (index == 5) {
      if (this.allFundingChecked == false) {
        this.lockFunding = true
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = false
      }
    }
    if (index == 6) {
      if (this.allWeeksChecked == false) {
        this.lockBillingCycle = true
      }
      else {
        this.billingCycleList.forEach(x => {
          x.checked = true;
          this.allWeeksChecked = x.description;
          this.allWeeksChecked = true;
          this.checkedWeekly = true;
          this.checkedFornightly = true;
          this.checkedMonthly = true;
        });
        this.lockBillingCycle = false
      }
    }
    if (index == 7) {
      if (this.chkGenInternalInvoice == true) {
        this.chkPrintZeroInvoices = true;
        this.chkPrintZeroInvoiceLine = true;
      }
      else {
        this.chkPrintZeroInvoices = false;
        this.chkPrintZeroInvoiceLine = false;
      }
    }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockBranches = true;
      this.branchList.forEach(x => {
        x.checked = false;
        this.allBranchesChecked = false;
        this.selectedBranches = [];
      });
    }
    if (index == 2) {
      this.lockPrograms = true;
      this.programList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
    if (index == 3) {
      this.lockCategories = true;
      this.categoriesList.forEach(x => {
        x.checked = false;
        this.allCategoriesChecked = false;
        this.selectedCategories = [];
      });
    }
    if (index == 4) {
      this.lockRecipients = true;
      this.recipientsList.forEach(x => {
        x.checked = false;
        this.allRecipientsChecked = false;
        this.selectedRecipients = [];
      });
    }
    if (index == 5) {
      this.lockFunding = true;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
    if (index == 6) {
      this.lockBillingCycle = true;
      this.billingCycleList.forEach(x => {
        x.checked = false;
        this.checkedWeekly = false;
        this.checkedFornightly = false;
        this.checkedMonthly = false;
        this.allWeeksChecked = false;
        this.selectedBillingCycle = [];
      });
    }
  }

  startProcess() {
    this.loading = true;
    this.billingS.getBatchRecord(null).subscribe(data => {
      this.batchNumber = data[0].batchRecordNumber;
      this.validateModalFields();
    });
  }

  validateModalFields(): void {
    this.operatorID = this.token.nameid;
    this.batchNumber = this.batchNumber + 1;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');
    this.startDate = this.inputForm.get('startDate').value;
    this.endDate = this.inputForm.get('endDate').value;
    this.startDate = formatDate(this.startDate, 'yyyy/MM/dd', 'en_US');
    this.endDate = formatDate(this.endDate, 'yyyy/MM/dd', 'en_US');
    this.invoiceType = this.inputForm.get('invType').value;

    if (this.lockBranches == false) {
      this.selectedBranches = null
    } else {
      this.selectedBranches = this.branchList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockPrograms == false) {
      this.selectedPrograms = null
    } else {
      this.selectedPrograms = this.programList
        .filter(opt => opt.checked)
        .map(opt => opt.title).join(",")
    }

    if (this.lockCategories == false) {
      this.selectedCategories = null
    } else {
      this.selectedCategories = this.categoriesList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockBillingCycle == false) {
      this.selectedBillingCycle = null
    } else {
      this.selectedBillingCycle = this.billingCycleList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockFunding == false) {
      this.selectedFunding = null
    } else {
      this.selectedFunding = this.fundingList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    const modalVal = this.inputForm;
    this.selectedPackage = modalVal.get('selectedPackage').value

    if (this.selectedBillingCycle != '' && this.selectedBranches != '' && this.selectedPrograms != '' && this.selectedCategories != '' && this.selectedFunding != '') {
      console.log(this.chkSepInvRecipient);
      console.log(this.operatorID);
      console.log(this.currentDateTime);
      // this.processExecution();
    } else if (this.selectedBillingCycle == '') {
      this.globalS.eToast('Error', 'Please select atleast one Billing Cycle to proceed')
    } else if (this.selectedBranches == '') {
      this.globalS.eToast('Error', 'Please select atleast one Branch to proceed')
    } else if (this.selectedFunding == '') {
      this.globalS.eToast('Error', 'Please select atleast one Funding to proceed')
    } else if (this.selectedPrograms == '') {
      this.globalS.eToast('Error', 'Please select atleast one Program to proceed')
    } else if (this.selectedCategories == '') {
      this.globalS.eToast('Error', 'Please select atleast one Category to proceed')
    }
  }

  processExecution() {
    this.postLoading = true;
    this.billingS.postDebtorBilling({
      InvoiceNumber: 'N/A',
      BatchNumber: this.batchNumber,
      Branches: this.selectedBranches,
      Programs: this.selectedPrograms,
      Categories: this.selectedCategories,
      StartDate: this.startDate,
      EndDate: this.endDate,
      InvoiceType: this.invoiceType,
      sepInvRecipient: this.chkSepInvRecipient,
      OperatorID: this.operatorID,
      CurrentDateTime: this.currentDateTime
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
          } else {
            this.globalS.sToast('Success', this.updatedRecords + ' - Debtor Records Updated')
            this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Debtor Run is: ' + this.batchNumber)
          }
          this.postLoading = false;
          this.ngOnInit();
          return false;
        }
      });
  }

  async selectSavePath() {
    const modalVal = this.inputForm;
    var packageName = modalVal.get('selectedPackage').value;

    try {
      let s_Location: string = '';
      let suggestedName: string = '';
      let mimeType: string = '';
      let types: { description: string, accept: { [key: string]: string[] } }[] = [];
      let selectedDirectoryPath: string = '';

      switch (packageName) {
        case 'AFRHA':
          suggestedName = 'EPICOR.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
        
        case 'FINANCE ONE':
          suggestedName = 'FINANCE1.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'GENERIC':
          suggestedName = 'TRACCSINVOICE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'GREENTREE ACCOUNTING':
          suggestedName = 'GREENTREEINVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'CPL NDIA':
          suggestedName = 'CPLNDIA.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'SAGE EVOLUTION':
          suggestedName = 'FINANCE1.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'MS DYNAMICS GP':
          suggestedName = 'GP.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'ATTACHE':
          suggestedName = 'INVOICE.TXT';
          mimeType = 'application/txt';
          types = [
            {
              description: 'TXT Files',
              accept: { 'application/txt': ['.txt'] },
            },
          ];
          break;
      
        case 'ABM':
          suggestedName = 'ABMINVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'MYOB':
          suggestedName = 'TRACCS.TXT';
          mimeType = 'application/txt';
          types = [
            {
              description: 'TXT Files',
              accept: { 'application/txt': ['.txt'] },
            },
          ];
          break;
      
        case 'QUICKBOOK':
          suggestedName = 'QUICKBOOKS.IIF';
          mimeType = 'application/iif';
          types = [
            {
              description: 'IIF Files',
              accept: { 'application/iif': ['.iif'] },
            },
          ];
          break;
      
        case 'CIM':
          suggestedName = 'CIM.TXT';
          mimeType = 'application/txt';
          types = [
            {
              description: 'TXT Files',
              accept: { 'application/txt': ['.txt'] },
            },
          ];
          break;
      
        case 'AUTHORITY ACCOUNTING':
          suggestedName = 'QLDINVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'PATHWAYS ACCOUNTING':
          suggestedName = 'PATHWAYSINVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'XERO ACCOUNTING':
          suggestedName = 'XEROINVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'FAMMIS EXPORT':
          suggestedName = 'FAMMISINVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'FAMMIS 2 EXPORT':
          suggestedName = 'FAMMIS2INVOICES.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'T1 EXPORT':
        case 'T1 ETL EXPORT':
          suggestedName = 'TECHNOLOGYONE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        case 'SUN ACCOUNTING':
          suggestedName = 'SUNACCOUNTING.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;
      
        default:
          // Handle any cases that don't match here if needed
          break;
      }
      

      function getOutputFolder(folder: string, forceFolder: string, dialogTitle: string, operatorId: string): string {
        return `/output/folder/path/${folder}`;
      }

      const handle = await (window as any).showSaveFilePicker({
        suggestedName: suggestedName,
        types: types,
      });

      const filePath = handle.name;
      const dirHandle = await (window as any).showDirectoryPicker();
      const dirName = filePath;
      const fileHandle = await dirHandle.getFileHandle(dirName, { create: true });

      // console.log(`File path in: ${dirHandle.name}/${dirName}`);

      this.inputForm.patchValue({
        selectedDirectoryPath: `C:\\Users\\${this.loggedUserName}\\Documents\\${dirHandle.name}\\${dirName}`
      })

    } catch (error) {
      // console.error('Failed to get path:', error);
      this.globalS.iToast('Information', 'Directory Path not selected.');
    }
  }
}

