using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {

    [Table("PUBLIC_HOLIDAYS")]
    public class PublicHoliday
    {
        [Key]
        public int RecordNo { get; set; }
        public string Date { get; set;}
        public string Description { get; set; }
        public string Stats { get; set; }
        public string PublicHolidayRegion { get; set; }

        public void Deconstruct(out int recordno, out string date, out string description, out string publicholidayregion, out string stats){
            recordno = RecordNo;
            date = Date;
            description = Description;
            stats = Stats;
            publicholidayregion = PublicHolidayRegion;
        }
    }
}