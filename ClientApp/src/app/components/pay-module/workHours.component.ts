import { Component, OnInit, SimpleChanges, Input, } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService,PrintService, MenuService } from '@services/index';
import { Subject } from 'rxjs';
import { formatDate } from '@angular/common';
import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
    selector: 'app-unApproved-work-hours',
    templateUrl: './workHours.component.html',
    styleUrls: ['./workHours.component.css'],
})
export class WorkHoursComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;

    unapprovedWorkHoursList: Array<any>;
    tableData: Array<any>;
    loading: boolean = false;
    modalOpen: boolean = false;
    current: number = 0;
    inputForm: FormGroup;
    dateFormat: string = 'dd/MM/yyyy';
    postLoading: boolean = false;
    token: any;
    tocken: any;
    userRole: string = "userrole";
    dtpStartDate: any;
    dtpEndDate: any;
    RecordNumber: any;
    Recipient: any;
    Date: any;
    printLoad: boolean = false;
    StartTime: any;
    ServiceType: any;
    Staff: any;
    private unsubscribe: Subject<void> = new Subject();
    PayPeriodLength: number;

    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;


    constructor(
        private router: Router,
        private globalS: GlobalService,
        private listS: ListService,
        private formBuilder: FormBuilder,
        private billingS: BillingService,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
    ) { }

    ngOnInit(): void {
        this.token = this.globalS.decode();
        this.buildForm();
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.loadData();
        this.loading = false;
        this.modalOpen = true;
    }

    resetModal() {
    }
    handleCancel() {
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/pays']);
    }
    buildForm() {
        this.inputForm = this.formBuilder.group({
            dtpStartDate: '01/12/2021',
            dtpEndDate: '01/12/2021',
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    loadData() {
        this.loading = true;
        this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
            if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
                this.dtpEndDate = dataPayPeriodEndDate[0].payPeriodEndDate;
            }
            else {
                this.dtpEndDate = new Date
            }
            this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
                if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
                    this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
                }
                else {
                    this.PayPeriodLength = 14
                }
                var firstDate = new Date(this.dtpEndDate);
                firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
                this.dtpStartDate = formatDate(firstDate, 'MM-dd-yyyy', 'en_US');
                this.dtpStartDate = formatDate(this.dtpStartDate, 'yyyy/MM/dd', 'en_US');
                this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy/MM/dd', 'en_US');

                let dataPass = {
                    actionStartDate: this.dtpStartDate,
                    actionEndDate: this.dtpEndDate,
                }

                this.billingS.getUnapprovedWorkHours(dataPass).subscribe(data => {
                    this.unapprovedWorkHoursList = data;
                    this.tableData = data;
                    this.loading = false;
                });
            });
        });
    }
    generatePdf(){
      
        console.log("called")
              
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Unapproved Shifts",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Unapproved Shifts "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }

}

