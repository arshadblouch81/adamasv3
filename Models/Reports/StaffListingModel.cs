using System;
using System.Collections.Generic;

namespace Adamas.Model.Report
{
    public class StaffListingModel
    {
        public string CompanyName { get; set; }
        public string User { get; set; }
        public string Title { get; set; }
        public DateTime CurrentDate { get; set; }
        public List<StaffList> Data { get; set; }
    }

    public class StaffList {
        public string Name { get; set; }
        public string StartDate { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string DMin { get; set; }
        public string DMax { get; set; }
        public string WMin { get; set; }
        public string WMax { get; set; }
    }
}