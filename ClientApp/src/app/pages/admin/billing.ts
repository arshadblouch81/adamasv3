import { ChangeDetectorRef,Component, OnInit, OnDestroy, Input, AfterViewInit} from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { Subject,forkJoin } from 'rxjs';

const inputFormDefault = {
    batchNoArr: [[]],
}
@Component({
    styleUrls: ['./billing.css'],
    templateUrl: './billing.html',
    selector: 'BillingAdmin',
})
export class BillingAdmin implements OnInit, OnDestroy, AfterViewInit{
    
    debtorUpdate: any;
    rollbackInvoiceBatch: any;
    closeBillingRosterPeriod: any;
    printEmailInvoices: any;
    eventID: string;
    id: string;
    unApprovedClientHours: any;
    doBillingIntegrityCheck: any;
    bulkPriceUpdate: any;
    importReceipts: any;

    dateFormat: string ='dd/MM/yyyy';
    format1 = (): string => `1`;
    format2 = (): string => `2`;
    format3 = (): string => `3`;
    format4 = (): string => `4`;
    format5 = (): string => `5`;
    CircleSize = 26;

    inputForm: FormGroup;
    drawerVisible: boolean = false;
    tryDoctype: any;
    pdfTitle: string;
    tocken: any;
    loading: boolean = false;
    batchNoArr: Array<any> = [];
    isvisibleBatchModal:boolean = false;

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private sanitizer: DomSanitizer,
        private modalService: NzModalService,
        private listS: ListService,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef,
        private printS: PrintService,
        private globalS: GlobalService,
        ){}
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
            this.inputForm = this.fb.group(inputFormDefault);
        }
        ngAfterViewInit(): void {
            
        }
        onChange(result: Date): void {
            // console.log('onChange: ', result);
        }
        ngOnDestroy(): void {
        }

        billingEvent(i) {
            i = i || window.event;
            i = i.target || i.srcElement;
            this.eventID = (i.id).toString();
    
            switch (this.eventID) {
                case 'btn-ReviewUnapprvdClientHour':
                    this.unApprovedClientHours = {}
                    break;
                case 'btn-DoBillingIntegrityCheck':
                    this.doBillingIntegrityCheck = {}
                    break;
                case 'btn-CloseRostersForEditing':
                    this.closeBillingRosterPeriod = {}
                    break;
                case 'btn-ProcessBillingUpdate':
                    this.debtorUpdate = {}
                    break;
                case 'btn-PrintEmailInvoices':
                    this.printEmailInvoices = {}
                    break;
                case 'btn-BulkPriceUpdate':
                    this.bulkPriceUpdate = {}
                    break;
                case 'btn-ImportReceipts':
                    this.importReceipts = {}
                    break;
                case 'btn-RollbackBillingUpdateBatch':
                    this.rollbackInvoiceBatch = {}
                    break;
                case 'btn-BillingSummary':
                    forkJoin([
                        this.listS.GetBillingBatchNo()
                        ]).subscribe(data => {                                                             
                            this.batchNoArr = data[0]
                            
                            this.isvisibleBatchModal = true;
                        });
                    
                    break;
    
                default:
                    break;
            }
        }
        handleOkTop(){
            var s_Batch = this.inputForm.value.batchNoArr;
            this.BillingSummaryReport(s_Batch)
            this.isvisibleBatchModal = false;
        }
        handleCancelTop(): void {
            this.drawerVisible = false;
            this.isvisibleBatchModal = false;
            this.pdfTitle = ""
            this.tryDoctype = ""
        }
        BillingSummaryReport(BatchNo){
              //console.log(BatchNo)
            var RptTitle,fQuery
            RptTitle = "Billing Summary(Batch Register)"
              
              const data = {
                  
                "template": { "_id": "6CFrOgDm7IviyPCr" },                  
                              
                "options": {
                    "reports": { "save": false },            
                    "batchNo": BatchNo.toString().substring(0, 3),            
                    "userid": this.tocken.user,
                    "Criteria":BatchNo, 
                    "txtTitle":  RptTitle,                                                                                                                      
                  }
              }
                         
                  this.tryDoctype = ""; 
                  this.drawerVisible = true; 
                  this.loading = true;
          
              this.printS.printControl(data).subscribe((blob: any) => {
                  this.pdfTitle = "Billing Summary.pdf"
                  this.drawerVisible = true;                   
                  let _blob: Blob = blob;
                  let fileURL = URL.createObjectURL(_blob);
                  this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                  this.loading = false;
                  this.cd.detectChanges();
              }, err => {
                  console.log(err);
                  this.loading = false;
                  this.ModalS.error({
                      nzTitle: 'TRACCS',
                      nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                      nzOnOk: () => {
                          this.drawerVisible = false;
                      },
                  });
              });
              
              
              return;
          
          }


    }
    
    