using System;
using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class InsertSql{
        public string Table { get; set; }
        public Dictionary<string, object> Variables { get; set; }
      
    }
}