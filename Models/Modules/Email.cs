using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MimeKit;
namespace Adamas.Models.Modules
{
    public class Email
    {
        public Email()
        {
            ToAddresses = new List<EmailAddress>();
            FromAddresses = new List<EmailAddress>();
        }

        public List<EmailAddress> ToAddresses { get; set; }
        public List<EmailAddress> FromAddresses { get; set; }
        public List<EmailAddress> CCAddresses { get; set; }
        
        public string Subject { get; set; }
        public string Content { get; set; }
        public string RecipientName { get; set; }

        public InternetAddressList GetCCAddresses(){
            InternetAddressList list = new InternetAddressList();

            if(CCAddresses.Count > 0){
                CCAddresses.ForEach(x => {
                    list.Add(new MailboxAddress(x.Name, x.Address));
                });
            }

            return list;
        }

        public InternetAddressList GetToAddresses(){
            
            InternetAddressList list = new InternetAddressList();            
            if(ToAddresses.Count > 0){
                ToAddresses.ForEach(x => {
                    list.Add(new MailboxAddress(x.Name, x.Address));
                });
            }
            return list;
        }

        public InternetAddressList GetFromAddresses(){            
            InternetAddressList list = new InternetAddressList();            
            if(FromAddresses.Count > 0){
                FromAddresses.ForEach(x => {
                    list.Add(new MailboxAddress(x.Name, x.Address));
                });
            }
            return list;
        }

    }    
}
