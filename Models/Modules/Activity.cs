using System;

namespace Adamas.Models.Modules{
    public class Activity {
        public int? ActivityId { get; set; }
        public string Name { get; set; }
        public string RosterGroup { get; set; }
        public bool? InfoOnly { get; set; }
    }
}