import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
// import { ListService, MenuService } from '@services/index';
import { BillingService, TimeSheetService, LoginService, GlobalService, ListService, MenuService, DllService } from '@services/index';
import { UploadFile, UploadChangeParam } from 'ng-zorro-antd/upload';
import { debounceTime, timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { NgModule } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BrowserModule } from '@angular/platform-browser';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMessageModule } from 'ng-zorro-antd/message';

import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-hcp-balance-export',
  templateUrl: './hcpBalanceExport.component.html',
  styleUrls: ['./hcpBalanceExport.component.css'],
})

// .ant-divider, .ant-divider-vertical {
//     position: relative;
//     top: 10px;
// }

export class HCPBalanceExport implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;
  loggedComputerName: any;
  loggedUserName: any;

  operatorID: any;
  current: number = 0;
  inputForm: FormGroup;
  loading: boolean = false;
  token: any;
  tocken: any;
  check: boolean = false;
  userRole: string = "userrole";
  id: string;
  private unsubscribe: Subject<void> = new Subject();
  dateFormat: string = 'dd/MM/yyyy';
  tableData: Array<any>;
  updatedRecords: any;

  currentDateTime: any;

  branchList: Array<any>;
  selectedBranch: any;
  exportFormat: Array<any>;
  selectedExportFormat: any;
  serviceProviderID: Array<any>;
  selectedProviderID: any;
  exportFilePath: any;
  logFilePath: any;
  confirmModal?: NzModalRef;

  recipientsList: Array<any>;
  selectedRecipients: any;
  allRecipientsChecked: boolean;
  lockRecipients: any = false;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private loginS: LoginService,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    private DllServiceS: DllService,
    private modal: NzModalService,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    this.loading = false;
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
    this.isVisible = false;
    this.router.navigate(['/admin/hcp']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      startDate: '12/01/2021',
      endDate: '12/31/2021',
      branchList: 'ALL',
      exportFormat: 'HCP BALANCES FORMAT 1',
      serviceProviderID: '',
      chkUnapproved: false,
      chkApproved: false,
      chkBilled: false,
      selectedDirectoryPath: ''
    });

    this.exportFormat = ['HCP BALANCES FORMAT 1', 'HCP BALANCES FORMAT 2'];

    const now = new Date();

    // if (now.getMonth() == 11) {
    //     this.startDate = new Date(now.getFullYear() + 1, 0, 1);
    //     this.endDate = new Date(now.getFullYear() + 1, 5, 1);
    // } else {
    //     this.startDate = new Date(now.getFullYear(), now.getMonth() + 1, 1);
    //     this.endDate = new Date(now.getFullYear(), now.getMonth() + 6, 1);
    // }

    const startDate = new Date(now.getFullYear(), now.getMonth(), 1);

    const endDate = endOfMonth(new Date());

    const formattedStartDate = formatDate(startDate, 'MM/dd/yyyy', 'en_US');
    const formattedEndDate = formatDate(endDate, 'MM/dd/yyyy', 'en_US');

    this.inputForm.patchValue({
      startDate: formattedStartDate,
      endDate: formattedEndDate,
    });


    let dataPass = {
      CurrentDate: formatDate(this.globalS.getCurrentDateTime(), 'MM-dd-yyyy', 'en_US')
    }

    //getServiceProviderID
    this.billingS.getServiceProviderID(null).subscribe(data => {
      this.serviceProviderID = data;
    });

    //getBranchesDetail
    this.billingS.getBranchesDetail(null).subscribe(data => {
      this.branchList = data;
    });

    //loadRecipients
    this.billingS.getRecipientsTypeDoha(dataPass).subscribe(data => {
      this.recipientsList = data;
      this.tableData = data;
      this.allRecipientsChecked = true;
      this.checkAll(1);
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  //=====================================================================

  beforeUpload = (file: UploadFile): boolean => {
    // Optionally process the file before upload
    return false; // Return false to prevent automatic upload
  };

  handleChange(info: UploadChangeParam): void {
    debugger;
    const file = info.file;
    if (!file) {
      this.getFilePath(file);
      console.error('File object is undefined');
      return;
    }

    if (file.status !== 'uploading') {
      console.log(file);
    }

    if (file.status === 'done') {
      console.log(`${file.name} file uploaded successfully.`);
      this.getFilePath(file);
    } else if (file.status === 'error') {
      console.log(`${file.name} file upload failed.`);
    }
  }

  getFilePath(file: UploadFile): void {
    if (!file) {
      console.error('File object is undefined');
      return;
    }
    // Get the file path
    this.exportFilePath = file.url || file.originFileObj?.name;
    console.log('File Path:', this.exportFilePath);
  }

  // fileList: any[] = [];
  // beforeUpload = (file: any): boolean => {
  //   console.log(file);
  //   this.fileList = this.fileList.concat(file);
  //   return false;
  // }

  onClick(e) {
    console.log(e.files)
    console.log(e.fileList)
  }

  runProcess() {
    this.loading = true;
    const modalVal = this.inputForm;
  
    const rawStartDate = modalVal.get('startDate')?.value;
    const rawEndDate = modalVal.get('endDate')?.value;
  
    if (!rawStartDate) {
      this.globalS.eToast('Error', 'Please select Start Date.');
      this.loading = false;
      return;
    }
    if (!rawEndDate) {
      this.globalS.eToast('Error', 'Please select End Date.');
      this.loading = false;
      return;
    }

    if (new Date(rawStartDate) > new Date(rawEndDate)) {
      this.globalS.eToast('Error', 'Start Date cannot be after End Date.');
      this.loading = false;
      return;
    }  
  
    const selectedBranch = modalVal.get('branchList')?.value;
    if (!selectedBranch || selectedBranch === null || selectedBranch === '') {
      this.globalS.eToast('Error', 'Please select a Branch.');
      this.loading = false;
      return;
    }
  
    const directoryPath = modalVal.get('selectedDirectoryPath')?.value;
    const directoryPattern = /^[a-zA-Z]:\\(?:[^\\/:*?"<>|\r\n]+\\)*[^\\/:*?"<>|\r\n]+$/;
  
    if (!directoryPath || !directoryPattern.test(directoryPath)) {
      this.globalS.eToast('Error', 'Invalid Export File Path. Please provide a valid directory.');
      this.loading = false;
      return;
    }
  
    this.selectedRecipients = this.recipientsList
      .filter(opt => opt.checked)
      .map(opt => opt.recipientTitle)
      .join(",");
  
    if (!this.selectedRecipients || this.selectedRecipients.trim() === '') {
      this.globalS.eToast('Error', 'Please select at least one recipient.');
      this.loading = false;
      return;
    }
  
    const params = {
      operatorID: this.token.nameid,
      startDate: formatDate(rawStartDate, 'yyyy/MM/dd', 'en_US'),
      endDate: formatDate(rawEndDate, 'yyyy/MM/dd', 'en_US'),
      selectedBranch: selectedBranch,
      selectedExportFormat: modalVal.get('exportFormat')?.value || null,
      selectedProviderID: modalVal.get('serviceProviderID')?.value || null,
      chkUnapproved: modalVal.get('chkUnapproved')?.value || false,
      chkApproved: modalVal.get('chkApproved')?.value || false,
      chkBilled: modalVal.get('chkBilled')?.value || false,
      selectedRecipients: this.selectedRecipients,
    };
  
    if (params.selectedExportFormat === 'HCP BALANCES FORMAT 1' && !params.selectedProviderID) {
      this.globalS.eToast('Error', 'Please select Service Provider ID.');
      this.loading = false;
      return;
    }
  
    if (params.selectedExportFormat === 'HCP BALANCES FORMAT 2' &&
      !params.chkUnapproved && !params.chkApproved && !params.chkBilled) {
      this.globalS.eToast('Error', 'Please check at least one status type.');
      this.loading = false;
      return;
    }
  
    try {
      this.processExecution();
    } finally {
      this.loading = false;
    }
  }
  

  processExecution() {
    console.log('success');
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
  }

  processExecution1() {
    this.loading = true;
    this.DllServiceS.HCPBalanceExport({
      User: this.operatorID,
      Password: this.operatorID,
      WindowsUer: this.operatorID,
      Batch: this.selectedBranch,
      // StartDate: this.startDate,
      // EndDate: this.endDate,
      ExportLocation: this.exportFilePath,
      LogFilePath: this.logFilePath
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
          } else {
            this.globalS.sToast('Success', 'Sales Journal Export Records Updated')
            // this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Debtor Run is: ' + this.operatorID)
          }
          this.loading = false;
          this.ngOnInit();
          return false;
        }
      });
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedRecipients = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allRecipientsChecked == false) {
        this.lockRecipients = true
      }
      else {
        this.recipientsList.forEach(x => {
          x.checked = true;
          this.allRecipientsChecked = x.description;
          this.allRecipientsChecked = true;
        });
        this.lockRecipients = false
      }
    }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockRecipients = true;
      this.recipientsList.forEach(x => {
        x.checked = false;
        this.allRecipientsChecked = false;
        this.selectedRecipients = [];
      });
    }
  }

  async selectSavePath() {
    const modalVal = this.inputForm;
    var selectedType = modalVal.get('exportFormat')?.value;
  
    try {
      let suggestedName = '';
      let types = [];
  
      switch (selectedType) {
        case 'HCP BALANCES FORMAT 1':
          suggestedName = 'CDCGP.CSV';
          types = [{ description: 'CSV Files', accept: { 'application/csv': ['.csv'] } }];
          break;
        case 'HCP BALANCES FORMAT 2':
          suggestedName = 'CDCGENERIC.CSV';
          types = [{ description: 'CSV Files', accept: { 'application/csv': ['.csv'] } }];
          break;
        default:
          break;
      }
  
      if (!(window as any).showSaveFilePicker || !(window as any).showDirectoryPicker) {
        this.globalS.eToast('Error', 'File system API not supported.');
        return;
      }
  
      const fileHandle = await (window as any).showSaveFilePicker({
        suggestedName: suggestedName,
        types: types,
      });
  
      const dirHandle = await (window as any).showDirectoryPicker();
  
      this.loggedUserName = this.token?.userName || this.token?.nameid || 'DefaultUser';
  
      const fullPath = `C:\\Users\\${this.loggedUserName}\\Documents\\${dirHandle.name}\\${suggestedName}`;
  
      this.inputForm.patchValue({ selectedDirectoryPath: fullPath });
  
      console.log("Selected Path: ", fullPath);
  
    } catch (error) {
      console.error('Failed to get path:', error);
      this.globalS.iToast('Information', 'Directory Path not selected.');
    }
  }
  

  // async selectSavePath() {
  //   const modalVal = this.inputForm;
  //   var selectedType = modalVal.get('exportFormat').value;

  //   try {

  //     let s_Location: string = '';
  //     let suggestedName: string = '';
  //     let mimeType: string = '';
  //     let types: { description: string, accept: { [key: string]: string[] } }[] = [];
  //     let selectedDirectoryPath: string = '';

  //     //these should need to be udpate as per the same as defined for Debtor update module
  //     switch (selectedType) {
  //       case 'HCP BALANCES FORMAT 1':
  //         suggestedName = 'CDCGP.CSV';
  //         mimeType = 'application/csv';
  //         types = [
  //           {
  //             description: 'CSV Files',
  //             accept: { 'application/csv': ['.csv'] },
  //           },
  //         ];
  //         break;

  //       case 'HCP BALANCES FORMAT 2':
  //         suggestedName = 'CDCGENERIC.CSV';
  //         mimeType = 'application/csv';
  //         types = [
  //           {
  //             description: 'CSV Files',
  //             accept: { 'application/csv': ['.csv'] },
  //           },
  //         ];
  //         break;

  //       default:
  //         break;
  //     }

  //     function getOutputFolder(folder: string, forceFolder: string, dialogTitle: string, operatorId: string): string {
  //       return `/output/folder/path/${folder}`;
  //     }

  //     const handle = await (window as any).showSaveFilePicker({
  //       suggestedName: suggestedName,
  //       types: types,
  //     });

  //     const filePath = handle.name;
  //     const dirHandle = await (window as any).showDirectoryPicker();
  //     const dirName = filePath;
  //     const fileHandle = await dirHandle.getFileHandle(dirName, { create: true });

  //     // console.log(`File path in: ${dirHandle.name}/${dirName}`);

  //     this.inputForm.patchValue({
  //       selectedDirectoryPath: `C:\\Users\\${this.loggedUserName}\\Documents\\${dirHandle.name}\\${dirName}`
  //     })

  //   } catch (error) {
  //     // console.error('Failed to get path:', error);
  //     this.globalS.iToast('Information', 'Directory Path not selected.');
  //   }
  // }
  selectedDirectoryPath: any;
}

