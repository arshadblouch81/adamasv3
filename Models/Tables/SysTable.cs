﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Adamas.Models.Tables
{
    public class SysTable
    {
        [Key]
        public int SqlId { get; set; }
        public int? CustomerId { get; set; }
        public int? SupplierId { get; set; }
        public int? ItemId { get; set; }
        public int? AccountId { get; set; }
        public string TsheetId { get; set; }
        public int? TsheetNum { get; set; }
        public string CarerId { get; set; }

        [Column("Batch#")]
        public int? BatchNo { get; set; }

        [Column("Invoice#")]
        public int? InvoiceNo { get; set; }
        public DateTime? PayPeriodEndDate { get; set; }
        public DateTime? TSheetFirstServiceDate { get; set; }
        public DateTime? TSheetLastServiceDate { get; set; }
        public int? Receipt { get; set; }
        public int? Adjust { get; set; }
        public int? Credit { get; set; }
        public int? Quote { get; set; }
        public int? SOrder { get; set; }
        public int? POrder { get; set; }
        public int? PInvoice { get; set; }
        public DateTime? RemindersLastProcessed { get; set; }
        public int? NDISClaimNumber { get; set; }
        public int? DMTransId { get; set; }
        public int? LinkId { get; set; }

        [Column("XDeletedRecord", TypeName = "bit")]
        public bool? XDeletedRecord { get; set; }
        public DateTime? XEndDate { get; set; }
        [Column("Award_Date")]
        public DateTime? AwardDate { get; set; }
    }
}
