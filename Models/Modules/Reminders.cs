using System;

namespace Adamas.Models.Modules{
    public class Reminders{
        public int? RecordNumber { get; set; } = 0;
        public string PersonID { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Email { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public string State { get; set; }
        public string Notes { get; set; }
        public bool Recurring { get; set; } = false;
        public bool SameDay { get; set; } = false;
        public bool SameDate { get; set; } = false;

        public string Creator { get; set; }

    }
}