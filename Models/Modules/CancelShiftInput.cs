   using System;
namespace Adamas.Models.Modules{
    public class CancelShiftInput {
        public string RecordNo { get; set; }
        public string ClientCode { get; set; }
        
        public string CancelPayType { get; set; }
        public string CancelShiftServiceType { get; set; }
        public string CancelShiftProgram { get; set; }
        public string CancelCode { get; set; }
        public string AlternateRecipient { get; set;  }
        public string s_Reason { get; set;  }
        public string Operator { get; set;  }
        public string b_KeepInMDS { get; set;  }
        public string b_BillAsOriginal { get; set;  }
        public string b_RetainDataset { get; set;  }
        public string s_CancelRate { get; set;  }
        public string RemDate { get; set;  }
        public string OPNote { get; set;  }
        public string Category { get; set;  }
        public string ReminderUser { get; set;  }
        public string TravelInterpreter { get; set;  }
       
        
    }
}
 