import { Component, OnInit, Input, forwardRef, ViewChild, OnDestroy, Inject, ChangeDetectionStrategy, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router,ActivatedRoute, ParamMap } from '@angular/router';
import { TimeSheetService, GlobalService, view, ClientService, StaffService, ListService, UploadService, days, gender, types, titles, caldStatuses, roles, ShareService,PrintService } from '@services/index';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';
import { mergeMap, takeUntil, concatMap, switchMap } from 'rxjs/operators';
import { EMPTY,Subject } from 'rxjs';

import { TitleCasePipe } from '@angular/common';

import { ProfileInterface} from '@modules/modules';
import { NzResizeEvent } from 'ng-zorro-antd/resizable';
import { zIndex } from 'html2canvas/dist/types/css/property-descriptors/z-index';

const noop = () => {
};

@Component({
  selector: 'app-contacts-details',
  templateUrl: './contacts-details.component.html',
  styleUrls: ['./contacts-details.component.css'],
  styles:[ `
  nz-table{
    margin-top:0px;
  }
  nz-select{
    width:100%;
  }
  `
],
providers: [
  {
    provide: NG_VALUE_ACCESSOR,
    multi: true,
    useExisting: forwardRef(() => ContactsDetailsComponent),
  }
],
changeDetection: ChangeDetectionStrategy.OnPush
})

export class ContactsDetailsComponent implements OnInit, OnDestroy, OnChanges,ControlValueAccessor {
  private unsubscribe: Subject<void> = new Subject();
  
  doctor: any;
  @Input() user: any;  
  @Input() group: any;  
  
  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;
  ViewContactGroup:boolean;
  innerValue: ProfileInterface;
  kinsArray: Array<any> = [];
  kindetailsGroup: FormGroup;
  inputForm: FormGroup;  
  selectedContactGroup:any;
  selectedType:any;
  contactGroups: Array<string> = [];
  contactTypes : Array<string>;
  
  modalOpen: boolean = false;
  isUpdate: boolean = false;
  postLoading: boolean = false;  
  modalOpen2: boolean = false;

  selected: any;
  current: number = 0;
  loading: boolean;
  tocken: any;
  
  doctors: Array<any> = [];
  selectedRow  : number = 0;
  activeRowData:any;
  selectedRow2  : number = 0;
  
  tableData: Array<any> = [];
  isLoading: boolean = false;
  
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean = false;
  modalTitle:string;
  
  constructor(
    private globalS: GlobalService,
    private clientS: ClientService,
    private staffS: StaffService,
    private timeS: TimeSheetService,
    private sharedS: ShareService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private http: HttpClient,
    private titleCase: TitleCasePipe,
    private router: Router,
    private printS:PrintService,
    private ModalS: NzModalService,
    private sanitizer: DomSanitizer,
  ) { }
  
  ngOnInit(): void {    
    this.user = this.sharedS.getPicked();
    this.buildForm();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    this.kindetailsGroup.patchValue({ creator : this.tocken.user });

    this.populate();
  }
  
  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      console.log('run contacts')
      this.searchKin(this.user);
    }
  }
  
  doctorChangeEvent(data: any){
    
    var doc = this.doctors.filter(x => x.name == data).shift();
    
    if(!doc){
      this.inputForm.patchValue({
        address1: '',
        address2: '',
        phone1: '',
        phone2:'',
        email: '',
        mobile: '',
        fax: '',
        name: ''
      })
      return;
    }
    
    this.inputForm.patchValue({
      address1: doc.address1,
      address2: doc.address2,
      phone1: doc.phone1,
      phone2:doc.phone2,
      email: doc.email,
      mobile: doc.mobile,
      fax: doc.fax,
      name: doc.name
    })
  }

  AllocateGroup(data:any, i:number){
    this.selectedRow2=i;
    this.selectedType=data;
    this.selectedContactGroup = data.type;
    this.modalTitle = "Add/Change " + this.selectedContactGroup + " Details";
    
    
    
    setTimeout(() => {
    this.listS.getlistkintype(data.description).pipe(takeUntil(this.unsubscribe)).subscribe(data => {  
      this.contactTypes = data;      
      this.cd.detectChanges();
    });
   },500);

  this.ViewContactGroup = false;
  //this.buildForm();
  this.kindetailsGroup.reset();
  this.modalOpen=true;
 
  }
  selectContactGroupItem(data,i){

    this.selectedType=data;
    this.selectedContactGroup = data.type;
    this.modalTitle = "Add/Change " + this.selectedContactGroup + " Details";
    this.selectedRow2=i;
  }
  populate(){
    this.listS.getdoctorinformation().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      //console.log(data);
      this.doctors = data;
    })

    this.listS.getcontactGroup().pipe(takeUntil(this.unsubscribe)).subscribe(data => {      //console.log(data);
     
      this.contactGroups = data;
    })

    
    
    
  }
  
  buildForm(): void {
    this.kindetailsGroup = this.formBuilder.group({
      group: [''],
      listOrder: [''],
      type: [''],
      name: [''],
      email: [''],
      address1: [''],
      address2: [''],
      suburbcode: [''],
      suburb: [''],
      postcode: [''],
      phone1: [''],
      phone2: [''],
      mobile: [''],
      fax: [''],
      notes: [''],
      oni1: false,
      oni2: false,
      ecode: [''],
      creator: [''],
      recordNumber: null,
      subType: '',
      emergencyContact:false,
      category :''
    });

    
    
    this.inputForm = this.formBuilder.group({
      group: [''],
      listOrder: [''],
      type: [''],
      name: [''],
      email: [''],
      address1: [''],
      address2: [''],
      suburbcode: [null],
      suburb: [''],
      state: [],
      postcode: [''],
      phone1: [''],
      phone2: [''],
      mobile: [''],
      fax: [''],
      notes: [''],
      oni1: false,
      oni2: false,
      ecode: [''],
      creator: [''],
      recordNumber: null,
      emergencyContact:false
    })
    
    // if(!this.isUpdate){
    //   this.kindetailsGroup.get('group').valueChanges.pipe(
    //     switchMap( x => {
    //       if(!x)
    //         return EMPTY;
    //       return this.listS.getlistkintype(x)  })
    //     ).subscribe(data => {
    //       this.contactTypes = data;
    //     });
    //   }else{
    //     this.listS.getlistkintype(this.kindetailsGroup.get('group').value())
    //     .subscribe(data => {
    //       this.contactTypes = data;
    //     });
    //   }

     }
    
    ngAfterViewInit(): void{
      
    }
    
    ngOnDestroy(): void{
      this.unsubscribe.next();
      this.unsubscribe.complete();
    }
    
    searchKin(token: ProfileInterface){
      this.loading = true;
      console.log(token)
      if (token.view == view.recipient) {
        this.timeS.getcontactskinrecipient(token.id)
        .subscribe(data => {
        if (this.group=='ASSOCIATEDPARTY')
          this.kinsArray = data.list.filter(x => x.group == 'ASSOCIATEDPARTY' || x.group == 'OTHER CONTACT' );
        else
          this.kinsArray = data.list.filter(x => x.group != 'ASSOCIATEDPARTY' );

          if (this.kinsArray.length > 0) {
            this.selected = this.kinsArray[0];
            this.activeRowData = this.kinsArray[0];
            this.showDetails(this.kinsArray[0]);
          }
          
          this.loading = false
          this.cd.markForCheck();
          this.cd.detectChanges();
        });
      }
      
      if (token.view == view.staff) {
        
        this.timeS.getcontactskinstaff(token.code)
        .subscribe(data => {
          this.kinsArray = data;
          if (this.kinsArray.length > 0) {
            this.selected = this.kinsArray[0];
            this.activeRowData = this.kinsArray[0];
          }
          this.loading = false
          this.cd.markForCheck();
          this.cd.detectChanges();
        });
      }
    }
    selectedItemGroup(data:any, i:number){
      this.selectedRow=i;
      this.activeRowData=data;
      
    }
    showDetails(kin: any) {
      
      
      this.timeS.getcontactskinstaffdetails(this.activeRowData.recordNumber)
      .subscribe(data => {
        this.kindetailsGroup.patchValue({
          address1: data.address1,
          address2: data.address2,
          name: data.contactName,
          type: data.contactType,
          subType: data.subType,
          email: data.email,
          fax: data.fax,
          mobile: data.mobile,
          notes: data.notes,
          phone1: data.phone1,
          phone2: data.phone2,
          suburbcode: (data.postcode != '') ? (data.postcode || '').trim() + ' ' + (data.suburb || '').trim() : '',
          suburb: data.suburb,
          postcode: data.postcode,
          listOrder: '',
         
          ecode: (data.equipmentCode || '').toUpperCase() == 'PERSON1',
          ecode2: (data.equipmentCode || '').toUpperCase() == 'PERSON2',
          recordNumber: data.recordNumber,
          emergencyContact: data.emergencyContact? null : false ,
          creator : this.tocken.user
          
        })

        this.selectedContactGroup = data.group;
        // this.modalOpen = true;
        this.cd.markForCheck();
        this.cd.detectChanges();
      })
      
      
     
    }
    
    //From ControlValueAccessor interface
    writeValue(value: any) {
      if (value != null) {
        console.log(value)
        
        this.innerValue = value;
        this.searchKin(this.innerValue);
      }
    }
    
    //From ControlValueAccessor interface
    registerOnChange(fn: any) {
      this.onChangeCallback = fn;
    }
    
    //From ControlValueAccessor interface
    registerOnTouched(fn: any) {
      this.onTouchedCallback = fn;
    }
    
    save() {
      
      if (this.user.view === view.staff)
        {
        var sub = this.kindetailsGroup.get('suburbcode').value;
        let address = sub ? this.getPostCodeAndSuburb(sub) : null;
        
        if (!this.globalS.isEmpty(address)) {
          this.kindetailsGroup.controls["postcode"].setValue(address.pcode);
          this.kindetailsGroup.controls["suburb"].setValue(address.suburb);
        }
        
        if (this.kindetailsGroup.get('oni1').value) {
          this.kindetailsGroup.controls['ecode'].setValue('PERSON1')
        } else if (this.kindetailsGroup.get('oni2').value) {
          this.kindetailsGroup.controls['ecode'].setValue('PERSON2')
        }
        
        if (this.group=='ASSOCIATEDPARTY'){
          this.selectedContactGroup='8-OTHER'
        }
     
          
            this.kindetailsGroup.patchValue({           
                type: this.selectedContactGroup,
                creator : this.tocken.user,
                ecode : '',
                email : '',
                emergencyContact : false,
                fax:'',
                mobile:'',
                listOrder : ''
    
    
    
            });
        

        const details = this.kindetailsGroup.value;
        
        this.timeS.updatecontactskinstaffdetails(
          details,
          details.recordNumber
        ).subscribe(data => {
          // this.searchKin(this.user);
          this.globalS.sToast('Success', 'Contact Updated');       
          this.searchKin(this.user);
        });
      }
      
      if (this.user.view === view.recipient)
        {
        console.log('recipient');
        var sub = this.kindetailsGroup.get('suburbcode').value;
        let address = sub ? this.getPostCodeAndSuburb(sub) : null;
        
        if (!this.globalS.isEmpty(address)) {
          this.kindetailsGroup.controls["postcode"].setValue(address.pcode);
          this.kindetailsGroup.controls["suburb"].setValue(address.suburb);
        }
        
        if (this.kindetailsGroup.get('oni1').value) {
          this.kindetailsGroup.controls['ecode'].setValue('PERSON1')
        } else if (this.kindetailsGroup.get('oni2').value) {
          this.kindetailsGroup.controls['ecode'].setValue('PERSON2')
        }
        if (this.group==null){
          this.group="CONTACT"
        }
       
        if (this.user.view === view.recipient) {
          this.kindetailsGroup.patchValue({
              type: this.selectedContactGroup,
              Category: this.group,
          });
          
        }

        const details = this.kindetailsGroup.value;
        this.timeS.updatecontactskinrecipientdetails(details,details.recordNumber)
        .subscribe(data => {
           this.searchKin(this.user);
          this.handleCancel();
          this.globalS.sToast('Success', 'Contact Updated');       
        });
      }
      
    }
    
    getPostCodeAndSuburb(address: any): any {
      const rs = address;
      let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0] : "";
      let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0].split(',')[0] : "";
      
      return {
        pcode: pcode.trim() || '',
        suburb: suburb.trim() || ''
      }
    }
    
    
    get nextRequired() {
      const { group, type, name } = this.kindetailsGroup.value;
      
      if (this.current == 0 && this.globalS.isEmpty(group)) {
        return false;
      }
      
      if (this.current == 1 && (this.globalS.isEmpty(type) || this.globalS.isEmpty(name)) ) {
        return false;
      }
      
      return true;
    }
    
    add() {
      if (this.kindetailsGroup.controls['suburbcode'].dirty) {
        var rs = this.kindetailsGroup.get('suburbcode').value;
        
        
        let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0].trim() : "";
        let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0].trim() : "";
        let state = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[1].replace(/,/g, '').trim() : "";
        
        if (pcode !== "") {
          this.kindetailsGroup.controls["postcode"].setValue(pcode);
          this.kindetailsGroup.controls["suburb"].setValue(suburb);
          this.kindetailsGroup.controls["state"].setValue(state);
        }
      }
      
      if (this.kindetailsGroup.get('oni1').value) {
        this.kindetailsGroup.controls['ecode'].setValue('PERSON1')
      } else if (this.kindetailsGroup.get('oni2').value) {
        this.kindetailsGroup.controls['ecode'].setValue('PERSON2')
      }

      if (this.group=='ASSOCIATEDPARTY'){
        this.selectedContactGroup='8-OTHER'
      }

      if (this.user.view === view.recipient) {
        this.kindetailsGroup.patchValue({
            type: this.selectedContactGroup,           
            creator : this.tocken.user,
            category : this.group 
        });
        
      }
      if (this.user.view === view.staff) {
        this.kindetailsGroup.patchValue({           
            type: this.selectedContactGroup,
            creator : this.tocken.user,
            ecode : '',
            email : '',
            emergencyContact : false,
            fax:'',
            mobile:'',
            listOrder : '',
            category: this.group


        });
        
      }
      
      this.timeS.postcontactskinstaffdetails(
        this.kindetailsGroup.value,
        this.user.id
      ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.globalS.sToast('Success', 'Contact Inserted');
        this.handleCancel();
        this.searchKin(this.user);
        this.handleCancel();
      });
    }
    
    delete() {
      this.timeS.deletecontactskin(this.activeRowData.recordNumber).subscribe(data => {      
        this.globalS.sToast('Success', 'Contact Deleted');      
        this.searchKin(this.user);
      });
    }
    
    handleCancel() {
      this.modalOpen = false;
      this.isUpdate  = false;
      this.selectedContactGroup ='';
      this.kindetailsGroup.reset();
      this.current = 0;
    }
    
    showAddModal(){
      this.ViewContactGroup = true;
    }
    
    showEditModal(){
      
      this.isUpdate  = true;
      this.current = 1 ;
      
      this.load();     
      
      this.timeS.getcontactskinstaffdetails(this.activeRowData.recordNumber)
      .subscribe(data => {
        
        this.listS.getlistkintype(data.contactType).pipe(takeUntil(this.unsubscribe)).subscribe(data => {  
          this.contactTypes = data;      
          this.cd.detectChanges();
        });  
        
        this.kindetailsGroup.patchValue({
          address1: data.address1,
          address2: data.address2,
          name: data.contactName,
          type: data.contactType,
          email: data.email,
          phone1: data.phone1,
          phone2: data.phone2,
          suburb: data.suburb,
          notes:data.notes,
          recordNumber: data.recordNumber,
          
          mobile:data.mobile,
          fax:data.fax,
          postcode:data.postcode,
          listOrder:data.state,
          oni1:data.equipmentCode,
          group:data.group,
          category: this.group,
        })
      
        this.modalTitle = "Add/Change " + data.contactType + " Details";
        this.selectedContactGroup = data.group;
        
        this.modalOpen = true
        
      })
    }
    
    load(){
      
    }
    
    pre() {
      this.current -= 1;
    }
    
    next() {
      this.current += 1;
    }
    
    getPermisson(index:number){
      var permissoons = this.globalS.getRecipientRecordView();
      return permissoons.charAt(index-1);
    }
    getStaffPermisson(index:number){
      var permissoons = this.globalS.getStaffRecordView();
      return permissoons.charAt(index-1);
    }
    
    handleOkTop(){
      
    }
    handleCancelTop(){
      this.drawerVisible = false;
      this.pdfTitle = "";
      this.tryDoctype="";
    }
    
    InitiatePrint(){
      
      var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
      
      fQuery = " SELECT  (HR.[Type]) AS Type,UPPER(HR.[Type]) AS Type,UPPER(HR.[SubType]) AS [Contact Type],HR.[Name] AS [Contact Name] ,CASE  WHEN ISNULL(HR.Phone1, '') <> '' THEN '(p)' + HR.Phone1 ELSE '' END + CASE  WHEN ISNULL(HR.Phone1, '') <> '' AND ISNULL(HR.Phone2, '') <> '' THEN ', (p)' + HR.Phone2 ELSE CASE WHEN ISNULL(HR.Phone2, '') <> '' THEN '(p)' + HR.Phone2 ELSE '' END END + CASE  WHEN ISNULL(HR.Phone1, '') <> '' AND ISNULL(HR.Phone2, '') <> '' AND ISNULL(HR.Mobile, '') <> '' THEN ', (m)' + HR.Mobile ELSE CASE  WHEN ISNULL(HR.Mobile, '') <> '' THEN '(m)' + HR.Mobile ELSE '' END END + CASE WHEN ISNULL(HR.Phone1, '') <> '' AND ISNULL(HR.Phone2, '') <> '' AND ISNULL(HR.Mobile, '') <> '' AND ISNULL(HR.Email, '') <> '' THEN ', (e)' + HR.Email ELSE '(e)' + IsNull(HR.Email, '') END AS Phones,CASE  WHEN HR.Address1 <> '' AND HR.Address1 IS NOT NULL THEN HR.ADDRESS1 ELSE '' END + ' ' + CASE  WHEN HR.Address2 <> '' AND HR.Address2 IS NOT NULL THEN HR.ADDRESS2 ELSE '' END + ' ' + CASE  WHEN HR.Suburb <> '' AND HR.Suburb IS NOT NULL THEN HR.Suburb ELSE '' END + ' ' + CASE WHEN HR.Postcode <> '' AND HR.Suburb IS NOT NULL THEN HR.Postcode ELSE '' END AS AddressDetails,Notes FROM HumanResources HR WHERE PersonID = '"+this.user.id+"' AND (User1 IS NULL OR User1 <> 'IMPORT')AND [GROUP] IN ('CONTACT','OTHER CONTACT') ORDER BY [Contact Name],[State],[Contact Type]  ";
      //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
      txtTitle = "Contact Detail";       
      Rptid =   "8WTerq14eNFSAnz0"  
      
      //console.log(fQuery)
      const data = {
        
        "template": { "_id": Rptid },                                
        "options": {
          "reports": { "save": false },                
          "sql": fQuery,
          "Criteria": lblcriteria,
          "userid": this.tocken.user,
          "txtTitle": txtTitle,
          "Extra": this.user.code,
          
        }
      }
      this.loading = true;
      this.drawerVisible = true;         
      this.printS.printControl(data).subscribe((blob: any) => {
        this.pdfTitle = txtTitle+".pdf"
        this.drawerVisible = true;                   
        let _blob: Blob = blob;
        let fileURL = URL.createObjectURL(_blob);
        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
        this.loading = false;
        this.cd.detectChanges();
      }, err => {
        console.log(err);
        this.loading = false;
        this.ModalS.error({
          nzTitle: 'TRACCS',
          nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
          nzOnOk: () => {
            this.drawerVisible = false;
          },
        });
      });
      return;      
    }
    generatePdf(){
      
        
      const data = {
          "template": { "_id": "6BoMc2ovxVVPExC6" },
          "options": {
              "reports": { "save": false },                        
              "sql": this.kinsArray,                        
              "userid": this.tocken.user,
              "txtTitle":  "Contact Detail",                                          
              "Extra": this.user.code,                     
          }
      }
      this.loading = true;           
                  
      this.drawerVisible = true;
      this.printS.printControl(data).subscribe((blob: any) => {
                  this.pdfTitle = "Contact Detail "
                  this.drawerVisible = true;                   
                  let _blob: Blob = blob;
                  let fileURL = URL.createObjectURL(_blob);
                  this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                  this.loading = false;  
                  this.cd.detectChanges();                     
              }, err => {
                  console.log(err);
                  this.loading = false;
                  this.ModalS.error({
                      nzTitle: 'TRACCS',
                      nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                      nzOnOk: () => {
                          this.drawerVisible = false;
                      },
                  });
      });
    }
    
  }
  