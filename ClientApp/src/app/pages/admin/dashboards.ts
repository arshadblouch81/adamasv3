import { Component, OnInit, OnDestroy, Input, AfterViewInit} from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { Subject } from 'rxjs';
@Component({
    templateUrl: './dashboards.html',
    styleUrls : ['./dashboard.css']
})
export class DashboardAdmin implements OnInit, OnDestroy, AfterViewInit{
    
    dateFormat: string ='dd/MM/yyyy';

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private sanitizer: DomSanitizer,
        private modalService: NzModalService,
        private listS: ListService,
        ){}
        ngOnInit(): void {
            
        }
        ngAfterViewInit(): void {
            
        }
        onChange(result: Date): void {
            // console.log('onChange: ', result);
        }  
        view(index: number) {
            // console.log(index);
            if(index == 1){
                this.router.navigate(['/admin/glance']);
            }
            if(index == 2){
                this.router.navigate(['/admin/analyse-budget']);
            }
        }
        ngOnDestroy(): void {
        }
    }
    
    