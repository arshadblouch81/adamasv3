    
using System;
 
namespace Adamas.Models.Modules{
    public class PayTypeParms {
        
        public bool  ChooseEach { get; set; }
        public string PayTypeMode { get; set; }
        public string s_PayItem { get; set; }
        public string s_PayUnit { get; set; }
        public string s_PayRate { get; set; }
        public string s_Status { get; set; }
        public string s_DayMask { get; set; }
        public bool b_Award { get; set; }
        public string s_RosterStaff { get; set; }
        public bool b_TestForSingle { get; set; }
        public string s_TimespanStart { get; set; }
        public string s_TimespanEnd { get; set; }
         

    }
}
 