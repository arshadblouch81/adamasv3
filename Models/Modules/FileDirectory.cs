
namespace Adamas.Models.Modules{
    public class FileDirectory {
        public string Directory { get; set; }
        public string Filename { get; set; }
        public string FileDir { get; set; }
    }
}