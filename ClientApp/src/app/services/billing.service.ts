import { Router, NavigationEnd } from '@angular/router';
import { DatePipe } from '@angular/common';
import { JwtHelperService } from '@auth0/angular-jwt';
import { filter } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NzMessageService } from 'ng-zorro-antd/message';
import parseISO from 'date-fns/parseISO';
import format from 'date-fns/format';
import parse from 'date-fns/parse';
import isValid from 'date-fns/isValid'
import getHours from 'date-fns/getHours'
import getMinutes from 'date-fns/getMinutes'
import differenceInDays from 'date-fns/differenceInDays'
import differenceInWeeks from 'date-fns/differenceInWeeks'
import differenceInMinutes from 'date-fns/differenceInMinutes'
import { Jwt, DateTimeVariables } from '@modules/modules';
import { JsConfig } from '@modules/modules';
import { FormGroup } from '@angular/forms';
import { toNumber } from 'lodash';
import { LoginService } from './login.service';
import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { GlobalService } from './global.service';
import { GetStaff, MiscellaneousNote, AttendanceStaff, CoordinatorEmail, InputAllocateStaff, UpdateNote, ClaimVariation, DayManager, GetRecipient, InputFilter, RecordIncident, GetTimesheet, RosterInput, AddClientNote, ApplicationUser, InputShiftBooked, InputShiftSpecific } from '@modules/modules';
const helper = new JwtHelperService();
const billing: string = "api/billing";

declare var Dto: any;

@Injectable()
export class BillingService {

    private previousUrl: string | null = null;
    private currentUrl: string | null = null;
    private previousTabIndex: number | null = null;

    constructor(
        public http: HttpClient,
        public auth: AuthService,
        public globalS: GlobalService,
        private router: Router
    ) {
        this.currentUrl = this.router.url;
        this.router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                this.previousUrl = this.currentUrl;
                this.currentUrl = event.url;
            });
    }
    // postdebtorbilling(data: any): Observable<any> {
    //     return this.auth.post(`${billing}/postDebtorBilling`, data)
    // }

    getlistProgramPackagesFilter(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/programPackagesFilter/${is_where}`)
    }
    closeRosterPeriod(data: any): Observable<any> {
        return this.auth.put(`${billing}/closeRosterPeriod`, data);
    }

    //States
    getActiveStates(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveStates/${is_where}`)
    }
    getActivePeriodStates(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodStates`, data);
    }

    //Funding Regions
    getActiveFundingRegions(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveFundingRegions/${is_where}`)
    }
    getActivePeriodFundingRegions(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodFundingRegions`, data);
    }

    //Service Provider ID's (SPID)
    getActiveServiceProvider(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveServiceProvider/${is_where}`)
    }
    getActivePeriodServiceProvider(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodServiceProvider`, data);
    }

    //DS Outlets
    getActiveDsOutlets(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveDsOutlets/${is_where}`)
    }
    getActivePeriodDsOutlets(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodDsOutlets`, data);
    }

    //Branches
    getActiveBranches(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveBranches/${is_where}`)
    }
    getActivePeriodBranches(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodBranches`, data);
    }

    //Funding Type
    getActiveFundingType(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveFundingType/${is_where}`)
    }
    getActivePeriodFundingType(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodFundingType`, data);
    }

    //Care Domains
    getActiveCareDomains(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveCareDomains/${is_where}`)
    }
    getActivePeriodCareDomains(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodCareDomains`, data);
    }

    //Service Budget Codes
    getActiveServiceBudget(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveServiceBudget/${is_where}`)
    }
    getActivePeriodServiceBudget(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodServiceBudget`, data);
    }

    //Service Disciplines
    getActiveServiceDiscip(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveServiceDiscip/${is_where}`)
    }
    getActivePeriodServiceDiscip(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodServiceDiscip`, data);
    }

    //Service Regions
    getActiveServiceRegions(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveServiceRegions/${is_where}`)
    }
    getActivePeriodServiceRegions(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodServiceRegions`, data);
    }

    //Service Types
    getActiveServiceTypes(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveServiceTypes/${is_where}`)
    }
    getActivePeriodServiceTypes(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodServiceTypes`, data);
    }

    //Programs
    getActivePrograms(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActivePrograms/${is_where}`)
    }
    getActivePeriodPrograms(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodPrograms`, data);
    }

    //Coordinators/Staff Mgrs
    getActiveCoordinators(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveCoordinators/${is_where}`)
    }
    getActivePeriodCoordinators(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodCoordinators`, data);
    }

    //Cost Centers
    getActiveCostCenters(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveCostCenters/${is_where}`)
    }
    getActivePeriodCostCenters(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodCostCenters`, data);
    }

    //Recipients
    getActiveRecipients(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveRecipients/${is_where}`)
    }
    getActivePeriodRecipients(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodRecipients`, data);
    }

    //Staff
    getActiveStaff(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveStaff/${is_where}`)
    }
    getActivePeriodStaff(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodStaff`, data);
    }

    //Staff Category
    getActiveStaffCategory(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveStaffCategory/${is_where}`)
    }
    getActivePeriodStaffCategory(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodStaffCategory`, data);
    }

    //Teams
    getActiveTeams(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getActiveTeams/${is_where}`)
    }
    getActivePeriodTeams(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivePeriodTeams`, data);
    }

    //Output Hours
    getOutputHours(data: any): Observable<any> {
        return this.auth.post(`${billing}/getOutputHours`, data);
    }

    //Worked Hours
    getWorkedHours(data: any): Observable<any> {
        return this.auth.post(`${billing}/getWorkedHours`, data);
    }

    //Worked Attribute Hours
    getWorkedAttributeHours(data: any): Observable<any> {
        return this.auth.post(`${billing}/getWorkedAttributeHours`, data);
    }

    //Total Staff
    getTotalStaff(data: any): Observable<any> {
        return this.auth.post(`${billing}/getTotalStaff`, data);
    }

    //Total Recipient
    getTotalRecipient(data: any): Observable<any> {
        return this.auth.post(`${billing}/getTotalRecipient`, data);
    }
    getDebtorRecords(data: any): Observable<any> {
        return this.auth.put(`${billing}/getDebtorRecords`, data);
    }
    postDebtorBilling(data: any): Observable<any> {
        return this.auth.put(`${billing}/postDebtorBilling`, data);
    }
    getBillingCycle(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getBillingCycle/${is_where}`)
    }
    getInvoiceBatch(): Observable<any> {
        return this.auth.get(`${billing}/getInvoiceBatch`)
    }
    rollbackInvoiceBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackInvoiceBatch`, data);
    }
    getPayrollBatch(): Observable<any> {
        return this.auth.get(`${billing}/getPayrollBatch`)
    }
    rollbackPayrollBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackPayrollBatch`, data);
    }
    getRosterBatch(): Observable<any> {
        return this.auth.get(`${billing}/getRosterBatch`)
    }
    rollbackRosterBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackRosterBatch`, data);
    }
    getPayBatchRecord(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getPayBatchRecord/${is_where}`)
    }
    postPayUpdate(data: any): Observable<any> {
        return this.auth.put(`${billing}/postPayUpdate`, data);
    }
    getSysTableDates(): Observable<any> {
        return this.auth.get(`${billing}/getSysTableDates`)
    }
    getUnapprovedWorkHours(data: any): Observable<any> {
        return this.auth.put(`${billing}/getUnapprovedWorkHours`, data);
    }
    getProgramPackageslist(data: any): Observable<any> {
        return this.auth.post(`${billing}/getProgramPackageslist`, data);
    }
    getlistcategories(): Observable<any> {
        return this.auth.get(`${billing}/getlistcategories`)
    }
    setPayPeriodDate(data: any): Observable<any> {
        return this.auth.put(`${billing}/setPayPeriodDate`, data);
    }
    getStaffDetails(): Observable<any> {
        return this.auth.get(`${billing}/getStaffDetails`)
    }
    getRecipientsDetails(): Observable<any> {
        return this.auth.get(`${billing}/getRecipientsDetails`)
    }
    getFundingDetails(): Observable<any> {
        return this.auth.get(`${billing}/getFundingDetails`)
    }
    getProgramslist(data: any): Observable<any> {
        return this.auth.post(`${billing}/getProgramslist`, data);
    }
    getTravelProgram(): Observable<any> {
        return this.auth.get(`${billing}/getTravelProgram`)
    }
    getTravelActivity(): Observable<any> {
        return this.auth.get(`${billing}/getTravelActivity`)
    }
    getTravelPaytype(): Observable<any> {
        return this.auth.get(`${billing}/getTravelPaytype`)
    }
    getRscID(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getRscID/${is_where}`)
    }
    getUniqueBatchKey(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getUniqueBatchKey/${is_where}`)
    }
    updateUniqueBatchKey(data: any): Observable<any> {
        return this.auth.put(`${billing}/updateUniqueBatchKey`, data)
    }
    getTravelUpdate(data: any): Observable<any> {
        return this.auth.put(`${billing}/getTravelUpdate`, data);
    }
    getTravelProvider(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getTravelProvider/${is_where}`)
    }
    getGoogleCustID(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getGoogleCustID/${is_where}`)
    }
    getProviderKey(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getProviderKey/${is_where}`)
    }
    truncateTable(is_where: boolean): Observable<any> {
        return this.auth.delete(`${billing}/truncateTable/${is_where}`)
    }
    deleteFromRoster(data: any): Observable<any> {
        return this.auth.post(`${billing}/deleteFromRoster`, data);
    }
    GetMapDistanceTest(): Observable<any> {
        return this.auth.get(`${billing}/GetMapDistanceTest`)
    }
    // getTravelDistance(data: any):Observable<any>{
    //     return this.auth.get(`${billing}/getTravelDistance`, data);
    // }
    // getTravelDistance():Observable<any>{
    //     // return this.http.get(data);
    //     return this.http.get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Lahore&origins=Faisalabad&units=imperial&key=AIzaSyAMIBJrZxsPVqBAxzoZJgmaxfYIoCpYGWc');
    // }
    insertTravelCalc(data: any): Observable<any> {
        return this.auth.put(`${billing}/insertTravelCalc`, data);
    }
    // getTravelSeperateClaim(data: any): Observable<any> {
    //     return this.auth.put(`${billing}/getTravelSeperateClaim`, data);
    // }
    travelToFitGapTrue(batchNumber: string): Observable<any> {
        return this.auth.get(`${billing}/travelToFitGapTrue/${batchNumber}`)
    }
    travelToFitGapFalse(batchNumber: string): Observable<any> {
        return this.auth.get(`${billing}/travelToFitGapFalse/${batchNumber}`)
    }
    travelSeparteClaimFalse(batchNumber: string): Observable<any> {
        return this.auth.get(`${billing}/travelSeparteClaimFalse/${batchNumber}`)
    }
    getTravelDistance(data: any): Observable<any> {
        return this.auth.put(`${billing}/getTravelDistance`, data);
    }
    getGlanceDashboardValues(data: any): Observable<any> {
        return this.auth.put(`${billing}/getGlanceDashboardValues`, data);
    }
    postAwardInterpreterTest(user: string, pwd: string, staff: string, date: string, absences: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');
        //let serviceURL=AppConfiguration.Setting().Application.ServiceUrl;

        return this.http.get(`http://20.213.152.181:4040/dll.asmx/InterpretAwards?User=${user}&Password=${pwd}&s_StaffCode=${staff}&WEDate=${date}&b_IncludeAbsences=${absences}`);

        //  return this.http.post('http://20.213.152.181:4040/dll.asmx/InterpretAwards?User=sysmgr&Password=sysmgr&s_StaffCode=MELLOW MARSHA&WEDate=18/06/2023&b_IncludeAbsences=True'
        // , id, { headers: headers || headers, responseType: 'text' });

    }
    getAwardRuleList(): Observable<any> {
        return this.auth.get(`${billing}/getAwardRuleList`)
    }
    deleteAwardRuleSet(recordNo: number): Observable<any> {
        return this.auth.delete(`${billing}/deleteAwardRuleSet/${recordNo}`)
    }
    postAuditHistory(data: any): Observable<any> {
        return this.auth.post(`${billing}/postAuditHistory`, data)
    }
    getCodeList(): Observable<any> {
        return this.auth.get(`${billing}/getCodeList`)
    }
    getHcpClaimPreparationBatch(): Observable<any> {
        return this.auth.get(`${billing}/getHcpClaimPreparationBatch`)
    }
    getHcpAgingBatch(): Observable<any> {
        return this.auth.get(`${billing}/getHcpAgingBatch`)
    }
    getHcpClaimUploadBatch(): Observable<any> {
        return this.auth.get(`${billing}/getHcpClaimUploadBatch`)
    }
    rollbackHcpClaimPreparationBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackHcpClaimPreparationBatch`, data);
    }
    rollbackHcpAgingBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackHcpAgingBatch`, data);
    }
    rollbackHcpClaimUploadBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackHcpClaimUploadBatch`, data);
    }
    getHcpPackageClaimList(): Observable<any> {
        return this.auth.get(`${billing}/getHcpPackageClaimList`)
    }
    changePackageClaimRates(data: any): Observable<any> {
        return this.auth.put(`${billing}/changePackageClaimRates`, data);
    }
    addCdcPackageClaim(data: any): Observable<any> {
        return this.auth.put(`${billing}/addCdcPackageClaim`, data);
    }
    hcpUpdateRecipientPrograms(): Observable<any> {
        return this.auth.put(`${billing}/hcpUpdateRecipientPrograms`)
    }
    hcpUpdateQuotes(data: any): Observable<any> {
        return this.auth.put(`${billing}/hcpUpdateQuotes`, data)
    }
    hcpAdjustRostersCmPkg(data: any): Observable<any> {
        return this.auth.put(`${billing}/hcpAdjustRostersCmPkg`, data)
    }
    getPeriodEndDate(): Observable<any> {
        return this.auth.get(`${billing}/getPeriodEndDate`)
    }
    getHcpBulkBalances(data: any): Observable<any> {
        return this.auth.post(`${billing}/getHcpBulkBalances`, data);
    }
    // getHcpDataCheck(): Observable<any> {
    //     return this.auth.get(`${billing}/getHcpDataCheck`)
    // }
    getHcpDataCheck(): Observable<any> {
        return this.auth.put(`${billing}/getHcpDataCheck`);
    }
    getHcpPkgAuditReport(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getHcpPkgAuditReport/${is_where}`)
    }
    getHcpPkgStatusSummary(is_where: boolean): Observable<any> {
        return this.auth.get(`${billing}/getHcpPkgStatusSummary/${is_where}`)
    }
    getUnapprovedClientHours(data: any): Observable<any> {
        return this.auth.post(`${billing}/getUnapprovedClientHours`, data);
    }
    getTableRegistration(): Observable<any> {
        return this.auth.get(`${billing}/getTableRegistration`)
    }
    getPayIntegrityList(data: any): Observable<any> {
        return this.auth.put(`${billing}/getPayIntegrityList`, data);
    }
    getPaysBatchHistory(): Observable<any> {
        return this.auth.get(`${billing}/getPaysBatchHistory`)
    }
    getBillingBatchHistory(): Observable<any> {
        return this.auth.get(`${billing}/getBillingBatchHistory`)
    }
    getBillingIntegrityList(data: any): Observable<any> {
        return this.auth.put(`${billing}/getBillingIntegrityList`, data);
    }
    getNdiaClaimBatch(): Observable<any> {
        return this.auth.get(`${billing}/getNdiaClaimBatch`)
    }
    rollbackNdiaClaimBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackNdiaClaimBatch`, data);
    }
    getNdiaAgingBatch(): Observable<any> {
        return this.auth.get(`${billing}/getNdiaAgingBatch`)
    }
    rollbackNdiaAgingBatch(data: any): Observable<any> {
        return this.auth.put(`${billing}/rollbackNdiaAgingBatch`, data);
    }
    getPackageBalancesDate(): Observable<any> {
        return this.auth.get(`${billing}/getPackageBalancesDate`);
    }
    getActivitiesAfter(data: any): Observable<any> {
        return this.auth.post(`${billing}/getActivitiesAfter`, data);
    }
    getFundingSources(data: any): Observable<any> {
        return this.auth.post(`${billing}/getFundingSources`, data);
    }
    getProgramsAfter(data: any): Observable<any> {
        return this.auth.post(`${billing}/getProgramsAfter`, data);
    }
    getCategories(data: any): Observable<any> {
        return this.auth.post(`${billing}/getCategories`, data);
    }
    getRecipientsGroups(data: any): Observable<any> {
        return this.auth.post(`${billing}/getRecipientsGroups`, data);
    }
    getSPackagesDetail(data: any): Observable<any> {
        return this.auth.post(`${billing}/getSPackagesDetail`, data);
    }
    getBatchRecord(data: any): Observable<any> {
        return this.auth.post(`${billing}/getBatchRecord`, data);
    }
    insertBatchRecord(data: any): Observable<any> {
        return this.auth.post(`${billing}/insertBatchRecord`, data)
    }
    getSystableRecord(data: any): Observable<any> {
        return this.auth.post(`${billing}/getSystableRecord`, data)
    }
    insertPayBillBatch(data: any): Observable<any> {
        return this.auth.post(`${billing}/insertPayBillBatch`, data)
    }
    processAgePackageBalance(data: any): Observable<any> {
        return this.auth.put(`${billing}/processAgePackageBalance`, data)
    }
    processBulkPriceUpdate(data: any): Observable<any> {
        return this.auth.put(`${billing}/processBulkPriceUpdate`, data);
    }
    getBankDetails(data: any): Observable<any> {
        return this.auth.post(`${billing}/getBankDetails`, data);
    }
    getServiceProviderID(data: any): Observable<any> {
        return this.auth.post(`${billing}/getServiceProviderID`, data);
    }
    getBranchesDetail(data: any): Observable<any> {
        return this.auth.post(`${billing}/getBranchesDetail`, data);
    }
    getRecipientsTypeDoha(data: any): Observable<any> {
        return this.auth.post(`${billing}/getRecipientsTypeDoha`, data);
    }
    updatePackageBalance(data: any): Observable<any> {
        return this.auth.put(`${billing}/updatePackageBalance`, data)
    }
    getLoggedInUser(): Observable<any> {
        return this.auth.get(`${billing}/getLoggedInUser`)
    }
    getCurrentUser(): Observable<any> {
        return this.auth.get(`${billing}/getCurrentUser`);
    }
    isDataExists(tableData: any[], username: string): boolean {
        username = username ? username.replace(/'/g, '') : '';
        return tableData.some(function (el) {
            const name = typeof el.name === 'string' ? el.name.trim().toUpperCase() : '';
            const processedUsername = typeof username === 'string' ? username.trim().toUpperCase() : '';
            return name === processedUsername;
        });
    }
    getPrintEmailInvoicesList(data: any): Observable<any> {
        return this.auth.put(`${billing}/getPrintEmailInvoicesList`, data);
    }

    public resetPreviousTabIndex(): void {
        this.previousTabIndex = null;
    }

    public setPreviousTabIndex(index: number): void {
        this.previousTabIndex = index;
    }

    public getPreviousTabIndex(): number | null {
        return this.previousTabIndex;
    }

    public getPreviousUrl(): string | null {
        return this.previousUrl;
    }

}
