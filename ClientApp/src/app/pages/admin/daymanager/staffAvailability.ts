
import { Component, OnInit, OnDestroy, Input, AfterViewInit,ChangeDetectorRef, EventEmitter, Output,Inject, Injectable, Renderer2, ViewEncapsulation} from '@angular/core'
import { Router,ActivatedRoute } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService,NavigationService,TimeSheetService, MenuService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import { takeUntil } from 'rxjs/operators';
import { addDays } from 'date-fns';

@Component({
    templateUrl: './staffAvailability.html',
    styleUrls: ['./staffAvailability.css'],
    selector: 'staffAvailability',
   
   
  })
  
export class StaffAvailability {

    private unsubscribe = new Subject();
    barChartLabels: any[] = [];
    barChartData: any[] = [];
    barChartOptions: any = {};
    barChartType:any ='bar';
    chartOptions = {
        animationEnabled: true, // Enables animations
        exportEnabled: true,    // Allows data export
        title: {
          text: "Sales Analysis"
        },
        axisY: {
          title: "Revenue",
          prefix: "$",
        },
        axisX: {
          title: "Months"
        },
        data: [
          {
            type: "column", // Chart type (e.g., column, line, pie, etc.)
            dataPoints: [
              { label: "January", y: 5000 },
              { label: "February", y: 7000 },
              { label: "March", y: 8000 },
              { label: "April", y: 6000 },
              { label: "May", y: 10000 }
            ]
          }
        ]
      };

      @Input() isVisible:boolean=false;   
      @Input() loadData: Subject<any> = new Subject();
      StaffList:Array<any>=[];
      StaffGroups:Array<any>=[]; 
   
      loading:boolean=true;

     
      lstbranches:Array<any>=[];
      timelist:Array<any>=[];
      listDisctinctDates:Array<any>=[];
      lstDisctinctTimes:Array<any>=[];
      graphData:Array<any>=[];
    
      dateFormat: string = 'dd/MM/yyyy';
      branch:any='ALL BRANCHES';
      startDate:any;
      endDate:any;
      dayView:number=1;
      lstcycles:Array<any>=['CYCLE-1', 'CYCLE-2','CYCLE-3','CYCLE-4'];
      cycle:any='CYCLE-1';
      lstDays:Array<any>=['1','7','14','28'];
      day=14;
      lstDaysWeek:Array<any>=[];
      

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private timeS: TimeSheetService,
        private modalService: NzModalService,
        private listS: ListService,
        private globalS:GlobalService,
        private cd:ChangeDetectorRef,
    
        ){

    //   this.listS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //         this.payEndingDate=data.payPeriodEndDate;
    //         console.log(this.payEndingDate);
    //       });

        }
      
        ngOnInit(): void {
          this.loadData.subscribe(data => {
       
            
            this.isVisible=true;
            this.dayView=data.dayView;
            this.startDate = new Date(data.date);
            this.endDate = new Date(addDays(this.startDate, this.dayView-1));
         
            
            for(let i:number=1; i<24; i++){
              if (i<10)   {       
                this.timelist.push(  {value :"0"+i+":00", label: "0"+i+":00"});
               // this.timelist.push( {value : "0"+i+":30", label: "30"} );
              }else{
                this.timelist.push(  {value :i+":00", label: i+":00"});
               // this.timelist.push( {value : i+":30", label: "30"} );
              }
              
            }
            this.getStaffAvailability();
            
           
           });
    
}
getStaffAvailability(){
  
  this.loading=true;
 

  let startDate= format(this.startDate,'yyy-MM-dd');
  let endDate=format(this.endDate,'yyy-MM-dd');
  
 
  forkJoin([
    this.listS.getliststaffgroup(),
    this.listS.getlistbranchesObj(),
    this.timeS.getStaffAvailability(startDate,endDate)
]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    this.StaffGroups = data[0];
    this.lstbranches = data[1];
    this.StaffList = data[2];
   this.listDisctinctDates = this.getDistinctDates(this.StaffList);
   this.lstDisctinctTimes =this.getArrayFromObject(this.getStaffCountByDateAndTime(this.StaffList));
  
  this. Prepare_data();

   let labels=  this.StaffGroups .map(x=>x.description)
  let datapoints =this.StaffList.map(x=> { return    {label:x.date, y:x.duration}});

    this.chartOptions = {
      animationEnabled: true, // Enables animations
      exportEnabled: true,    // Allows data export
      title: {
        text: "Staff Availability Analysis"
      },
      axisY: {
        title: "duration",
        prefix: "",
      },
      axisX: {
        title: "Date"
      },
      
      data: [
        {
        type: "column", // Chart type (e.g., column, line, pie, etc.)
        dataPoints: datapoints
      }
      ]    
        
      
    };

    
    this.cd.detectChanges();
   
    this.loading=false;

    error=>{this.loading=false;}
});
}
getDistinctDates(dataList: any[]): any[] {
  //const distinctDates = Array.from(new Set(dataList.map(item => item.date)));
  //return distinctDates;
  const uniqueDateObjects = dataList.filter(
    (value, index, self) =>
      self.findIndex(item => item.date === value.date) === index
  );
  return uniqueDateObjects;
}
getStaffCountByDateAndTime(staffList: any[]): any {
  return staffList.reduce((acc, staff) => {
    const key = `${staff.date}_${staff.blockNo}`; // Combine date and time as a key
    if (!acc[key]) {
      acc[key] = { date: staff.date, blockNo: staff.blockNo, duration:staff.duration, count: 0 };
    }
    acc[key].count++;
    return acc;
  }, {});
}
getArrayFromObject(result: any): any[] {
  return Object.values(result);
}

Prepare_data(){
  this.graphData=[];
  let vlist:Array<any>=[];
  let obj;
  this.listDisctinctDates.forEach(element => {
    vlist=[]
   // this.lstDisctinctTimes.forEach(v => {
      let obj = {date: element.date, timeData: this.timelist.map(x=> { return    {label:x.label, value:x.value, backcolor: this.getBackcolor(x,element)}})};
   //  vlist.push(data);
   this.graphData.push(obj);
 // });
  // obj = {
  //   date: element,
  //   vlist: vlist,
        
  // }
  // this.graphData.push(obj);
  })
  this.cd.detectChanges();
}

getBackcolor(t:any,  d:any){
  let time = this.TimeToblock(t.value);
  let timeList = this.lstDisctinctTimes.filter(x=>x.date==d.date  )
  let timeData = timeList.find(v=>v.blockNo<=time && (v.blockNo+v.duration)>=time);
  if ( timeData==null) return '#ddf5ff';
  if (timeData.blockNo==null ) return '#ddf5ff';
 if(timeData.count>0)
   return 'blue';
 else
   return '#ddf5ff';
}
TimeToblock(startTime: any) {
 if (startTime==null) return 0;
 let time = startTime.split(':');
 
 let hrs = parseInt(time[0]);
 let min = parseInt(time[1]);
 
 let blocks =hrs * 12 + min/5;
  
 return blocks;
 
}
handleCancel(){
    this.isVisible=false;
    // this.AIRosterDone.emit(this.rosters_display);
   
}

}