import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes,status,period,budgetTypes,enforcement,billunit, ClientService, MenuService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import format from 'date-fns/format';

const defaultValue = {
    includeCaseManagement:false,
    status: null,
    program:null,
    activity:'',
    freq:'',
    period:'',
    duration:'',
    enforcement:'',
    starting:'',
    budgetType: null,
    bamount:'',
    specialPricing:false,
    billunit:'',
    namount:'',
    gst: false,
    compRecord:'',
    activityBreakDown:'',
    serviceBiller:'',
    autoInsertNotes:false,
    excludeFromNDIAPriceUpdates:false,
    PersonID:'',
    recordNumber:'',  
    EffectiveFrom : new Date(),
    EffectiveTo : new Date()
}

@Component({
    selector: '',
    templateUrl: './services.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styles: [`
        .ant-modal-body{
            padding-top:0px !important  
        },
        .ant-card-small>.ant-card-body {
            padding: 6px !important;
        },
        .ant-card-small>.ant-card-head {
            min-height: 20px !important;
        }

        nz-tabset{
            margin-top:1rem;
        }
        nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
            height: 25px !important;
            font-size: 13px !important;
        }
        
        nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
            line-height: 24px;
            height: 25px;
        }
        nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
            background: #717e94;
            color: #fff;
        }
        nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 15px;
            font-weight: 500;
            margin-top: 10px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
    }

    .line{
        border-bottom: 1px solid #e5e5e5;
        margin-bottom: 10px;
    }
    
    
    `],
})

export class IntakeServices implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    dateFormat: string ='dd/MM/yyyy';
    loading: boolean = false;
    postLoading:boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    tableData: Array<any> = [];
    services: Array<any> = [];
    alist: Array<any> = [];
    status: string[];
    activities: any;
    program: any;
    periods: string[];
    budgetTypes: string[];
    enforcement: string[];
    billunit: string [];
    competencymodal: boolean= false;
    isUpdateCompetency: boolean = false;
    inputvalueSearch:string;
    competencyList: any;
    CompetencycheckedList: any[];
    competencyListCopy: any;
    temp: any[];
    listRecipients: any;
    isUpdate: any;
    listCompetencyByPersonId: any;
    competencyForm: FormGroup;
    activeRowData:any;
    selectedRowIndex:number;
    specialPricing:boolean;
    selectedComptency:any;
    selectedRow:number;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private menuS: MenuService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal']);
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'services')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.CompetencycheckedList = new Array<string>();
        this.buildForm();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(data: any): void {
      
          
          this.delete(data);
        

    }
    
    search(user: any = this.user) {
        this.cd.reattach();

        this.loading = true;
        this.timeS.getintakeservices(this.user.id).subscribe(plans => {
            this.tableData = plans;
            this.loading = false;
            this.cd.markForCheck();
        });
    }
    listDropDown(user: any = this.user) {
        this.status = [];
        this.status = status;
        this.periods = period;
        this.budgetTypes = budgetTypes;
        this.enforcement = enforcement;
        this.billunit    = billunit;
        this.activities = [];
        let acti = "SELECT TITLE FROM ITEMTYPES WHERE ProcessClassification IN ('OUTPUT', 'EVENT', 'ITEM') AND ENDDATE IS NULL";
        this.listS.getlist(acti).subscribe(data => {
            this.activities = data.map(x=>x.title);
        });
        this.program = [];
        
        this.listS.getintakeprogram(this.user.id).subscribe(data => {
            this.program = data;
        })

        let comp = "SELECT Description as name from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND Domain = 'STAFFATTRIBUTE' OR Domain = 'RECIPATTRIBUTE' ORDER BY Description";
        this.listS.getlist(comp).subscribe(data => {
            this.competencyList = data;
            this.competencyListCopy = data;
        });

        this.timeS.getrecipients({
            User: this.globalS.decode().nameid,
            SearchString: ''
          }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.listRecipients = data;
            this.cd.markForCheck();
          });
    }
    buildForm() {
        this.inputForm = this.formBuilder.group(defaultValue)
        this.competencyForm = this.formBuilder.group({
            competency:'',
            mandatory:false,
            PersonID:'',
            notes:'',
            recordNumber:'',
            pricing :false,
           
        });

        this.inputForm
        .get("program")
        .valueChanges.pipe(
          switchMap(x => {
          
            return this.listS.getchargetype({
                program: x,
                index: 1,
                AccountNo: this.user.code
                
              });
     })).subscribe(data => {
        this.activities = data;
     });

}
    resetModal(){
      this.inputForm.reset();
      this.buildForm();
      this.postLoading = false;
    };
    save() {

        if (!this.globalS.IsFormValid(this.inputForm))
            return;
        
            const { PersonID,status,program,activity,freq,period,duration,billunit,namount,activityBreakDown,serviceBiller,specialPricing,gst,autoInsertNotes,excludeFromNDIAPriceUpdates,budgetType,bamount,enforcement,starting,recordNumber } = this.inputForm.value;


            this.postLoading = true;     
            if(!this.isUpdate){  

            this.timeS.postintakeRservices({
              PersonID: this.user.id,
              ServiceStatus: status,
              ServiceProgram: program,
              ServiceType:activity,
              Frequency:freq,
              Period:period,
              Duration:duration,
              UnitType:billunit,
              UnitBillRate:namount,
              ActivityBreakDown:activityBreakDown,
              ServiceBiller:serviceBiller,
              ForceSpecialPrice:specialPricing,
              TaxRate:gst,
              AutoInsertNotes:autoInsertNotes,
              ExcludeFromNDIAPriceUpdates:excludeFromNDIAPriceUpdates,
              BudgetType:budgetType,
              BudgetAmount:bamount,
              BudgetLimitType:enforcement,
              BudgetStartDate: format(new Date(starting),'yyyy/MM/dd'),
            }).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
              if (data) {
                this.globalS.sToast('Success', 'Data Added');
                this.loading = false;
                this.postLoading = false;
                this.handleCancel();
                this.search();
                this.resetModal();
              }
            })
        }else{
            this.timeS.updateintakeRservices({
                ServiceStatus: status,
                ServiceProgram: program,
                ServiceType:activity,
                Frequency:freq,
                Period:period,
                Duration:duration,
                UnitType:billunit,
                UnitBillRate:namount,
                ActivityBreakDown:activityBreakDown,
                ServiceBiller:serviceBiller,
                ForceSpecialPrice:specialPricing,
                TaxRate:gst,
                AutoInsertNotes:autoInsertNotes,
                ExcludeFromNDIAPriceUpdates:excludeFromNDIAPriceUpdates,
                BudgetType:budgetType,
                BudgetAmount:bamount,
                BudgetLimitType:enforcement,
                BudgetStartDate:format(new Date(starting),'yyyy/MM/dd'),
                RecordNumber:recordNumber
              }).pipe(takeUntil(this.unsubscribe))
              .subscribe(data => {
                if (data) {
                  this.globalS.sToast('Success', 'Data Updated');
                  this.loading = false;
                  this.postLoading = false;
                  this.isUpdate = false;
                  this.handleCancel();
                  this.search();
                  this.resetModal();
                }
              })
        }
        this.resetModal();
    }

    showEditModal(data: any) {
        console.log(data);
        
        this.addOREdit = 2;
        this.listDropDown();
        this.loadCompetency();
        this.modalOpen = true;
        this.isUpdate = true;
        // this.checkValueChange(data.specialPricing);

        this.inputForm.patchValue({
            PersonID: this.user.id,
            status: data.status,
            program: data.serviceProgram,
            activity:data.activity,
            freq:data.frequency,
            period:data.period,
            duration:data.duration,
            billunit:data.unitType,
            namount:data.unitBillRate,
            activityBreakDown:data.activityBreakDown,
            serviceBiller:data.serviceBiller,
            specialPricing:(data.forceSpecialPrice == false) ? false : true,
            gst:(data.taxRate == false) ? false : true,
            autoInsertNotes:(data.autoInsertNotes == false) ? false : true,
            excludeFromNDIAPriceUpdates:(data.excludeFromNDIAPriceUpdates == false) ? false : true,
            budgetType:data.budgetType,
            bamount:data.budgetAmount,
            enforcement:data.budgetLimitType,
            starting:data.budgetStartDate,
            recordNumber:data.recordNumber
        });
        
    }

    delete(data: any) {
        this.timeS.deleteintakerservice(data.recordNumber).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                            if(data){
                                this.search()
                                this.loading = false
                                this.globalS.sToast('Success','Service Deleted')
                            }
             })
    }
    
    deletecompetency(data: any){
        this.timeS.deleteintakeServicecompetency(data.recordNumber).pipe(
        takeUntil(this.unsubscribe)).subscribe(data => {
                        if(data){
                            this.loadCompetency()
                            this.globalS.sToast('Success','Competency Deleted')
                        }
                    })
    }
    handleCancel() {
        this.modalOpen = false;
    }
    
    log(value: string[]): void {
    }
    showAddModal() {
        this.addOREdit = 1;
        this.isUpdate=false;
        this.inputForm.reset(defaultValue)
        this.listDropDown();
        this.loadCompetency();
        this.modalOpen = true;
    }
    showCompetencyModal(){
        this.competencyList.forEach(x => {
          x.checked = false
        });
        this.CompetencycheckedList = [];
        this.competencymodal = true;
    }
    showComptencyEditModal(data: any){
       
        this.competencymodal = true;
        this.isUpdateCompetency = true;
        this.competencyForm.patchValue({
          competency: data.competency,
          mandatory: (data.mandatory == false || data.mandatory == null ) ? false : true,
          notes:data.notes,
          recordNumber:data.recordNumber,
        });
      }
    searchCompetenncy(event){
        this.temp = [];
        this.competencyList = this.competencyListCopy;
        if(event.target.value != ""){
          this.temp = this.competencyList.filter(res=>{
            return res.name.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
          })
          this.competencyList = this.temp;
        }else if(event.target.value == ""){
          this.competencyList = this.competencyListCopy;
        }
    }
    onCompetencyCheckboxChange(option, event) {
        if(event.target.checked){
          this.CompetencycheckedList.push(option.name);
        } else {
          this.CompetencycheckedList = this.CompetencycheckedList.filter(m=>m!= option.name)
        }
    }
    loadCompetency(){
        this.menuS.getlistServiceCompetencyByPersonId(this.user.id).subscribe(data => {
          this.listCompetencyByPersonId = data;
          this.loading = false;
          this.cd.detectChanges();
        });
    }
    saveCompetency(){
        this.postLoading = true;
        let insertOne = false;
        
        this.competencyForm.controls['mandatory'].setValue((this.competencyForm.value.mandatory == null) ? false : this.competencyForm.value.mandatory)
        this.competencyForm.controls['notes'].setValue((this.competencyForm.value.notes == null) ? '' : this.competencyForm.value.notes)
        const { notes,competency,mandatory,PersonID,recordNumber} = this.competencyForm.value;
        if(!this.isUpdateCompetency){
          this.CompetencycheckedList.forEach( (element) => {
            let is_exist   = this.globalS.isCompetencyExists(this.listCompetencyByPersonId,element);
            if(!is_exist){
                this.timeS.postintakeServicecompetency({
                    PersonID: this.user.id,
                    competencyValue: element,
                    mandatory:0,
                   })
                    .subscribe(data => {
                        insertOne = true;
                    });
                }
          });
          if(insertOne){
            this.globalS.sToast('Success', 'Saved successful');
          }
        //   this.cd.detectChanges();
          insertOne = false;    
          this.postLoading = false;
          this.handleCompetencyCancel();
          this.loading = true;
          this.loadCompetency();
          this.competencyForm.reset();
          this.CompetencycheckedList = [];
        }
        else{
           this.postLoading = true;
           this.timeS.updateintakeServicecompetency({
            PersonID: this.user.id,
            competencyValue: competency,
            mandatory:mandatory,
            notes:notes,
            recordNumber:recordNumber
           })
            .subscribe(data => {
                this.globalS.sToast('Success', 'Saved successful');
                this.postLoading = false;
                this.handleCompetencyCancel();
                this.loading = true;
                this.cd.detectChanges();
                this.competencyForm.reset();
                this.loadCompetency();
            });
        }
      }

    handleCompetencyCancel(){
        this.competencyForm.reset();
        this.competencymodal = false;
        this.isUpdateCompetency = false;
    }
    
    tabFindIndex: number = 0;
        tabFindChange(index: number){
         this.tabFindIndex = index;
    }
    tabFindIndexcomp: number = 0;
        tabFindChangecomp(index: number){
         this.tabFindIndexcomp = index;
    }
    checkValueChange(event: any){
        this.specialPricing=event;
        if(( !event.currentTarget.checked) || (event == false) ){
            this.inputForm.patchValue({
                gst:false,
                budgetType:'',
                bamount:'',
                enforcement:'',
                starting:'',
            })
        }
    }

    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }
onItemClick(data:any, i:number){
    this.selectedComptency=data;
    this.selectedRow=i;
}
onItemDblClick(data:any, i:number){
    this.selectedComptency=data;
    this.selectedRow=i;
    this.showComptencyEditModal(data);
}
    print(){
        
    }
    generatePdf(){

        let filers = forkJoin([
            this.timeS.getMinimalintakeservices(this.user.id)
        ]);
        filers.subscribe(svcs => {
            this.services = svcs[0];  
            var Title = " Services "
            const data = {
                "template": { "_id": "6BoMc2ovxVVPExC6" },
                "options": {
                    "reports": { "save": false },                        
                    "sql": this.services,                        
                    "userid": this.tocken.user,
                    "txtTitle":  Title+" for "+this.user.code,                       
                }
            }
            this.loading = true;                               
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
                this.pdfTitle = Title+" for "+this.user.code, 
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();                       
            }, err => {
                console.log(err);
                this.loading = false;
                this.ModalS.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });         
        });
        
       
      
       
      }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}