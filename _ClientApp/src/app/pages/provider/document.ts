
import { BsModalComponent } from 'ng2-bs3-modal';
import {takeUntil} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core'
import { TimeSheetService, GlobalService, UploadService } from '@services/index'
import { DomSanitizer } from '@angular/platform-browser';
import { Subject } from 'rxjs'

const enum ImagePosition {
    DOCUMENT = '-5px -5px',
    DOCMSWORD = '-5px -5px',
    PDF = "-125px -5px",
    SPREADSHEET = "-65px -5px",
    TEXTFILE = "-185px -5px",
    PRESENTATION = "-245px -5px",
    JPG = "-5px -70px",
    PNG = "-65px -70px",
    MP3 = "-125px -135px",
    MP4 = "-5px -200px"
}
  
const enum ImageActivity {
    DOCUMENT = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    DOCMSWORD = 'application/msword',
    PDF = "application/pdf",
    SPREADSHEET = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    SPREADSHEETMS = "application/vnd.ms-excel",
    TEXTFILE = "text/plain",
    PRESENTATION = "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    JPG = "image/jpeg",
    PNG = "image/png",
    MP3 = "audio/mp3",
    MPEG = "audio/mpeg",
    MP4 = "video/mp4"
}

@Component({
    templateUrl: './document.html',
    styles:[`
        ul{
            list-style:none;
        }
        section{
            padding: 10px;
            margin-bottom: 8px;
            background: #fff;
            position:relative;
        }
        section h5{
            margin:0;
        }
        .filetype-logo{
            background: url(../../assets/logo/filetype.png) no-repeat;
            height: 2.5rem;
            width: 2rem;
            float: left;
            background-size: 13rem;
            background-position: -10px -5px;
            margin: 0 15px 0 0;
        }
      .options{
        position: absolute;
        top: 0.5rem;
        right: 1rem;
      }
    `]
})

export class DocumentProvider implements OnInit, OnDestroy {

    @ViewChild('preview', { static: true }) preview: BsModalComponent;
    private unsubscribe$ = new Subject()
    token: any
    tableData: Array<any> = []

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private uploadS: UploadService,
        public sanitizer: DomSanitizer
    )
    { }

    ngOnInit(){
        this.token = this.globalS.decode();   
        this.load();     
    }

    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    load(){        
        this.uploadS.staffdocuments(this.token.code).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.tableData = data;
        })
    }
    
    reload(){
        this.load();
    }

    downloadFile(doc: any){
        const filename = doc.filename;
        this.uploadS.download(
            {
                PersonID: this.token.uniqueID,
                Extension: doc.type,
                FileName: doc.filename,
                DocPath: doc.originalLocation
            }
        ).pipe(takeUntil(this.unsubscribe$)).subscribe(blob => {
            let data = window.URL.createObjectURL(blob);
            let link = document.createElement('a');
            link.href = data;
            link.download = filename;
            link.click();
            setTimeout(() =>
            {
                // For Firefox it is necessary to delay revoking the ObjectURL
                window.URL.revokeObjectURL(data);
            }, 100);
        })
    }

    deleteFile(data: any) {
        this.uploadS.delete(this.token.uniqueID,{ 
            id: data.docID,
            filename: data.filename
        }).pipe(  takeUntil(this.unsubscribe$))
            .subscribe(data => {
            if(data){
                this.reload();
                this.globalS.sToast('Success','File deleted')
                return;
            }
        })
    }

    urlFile: string = "";
    fileDocumentName: string = "";
    loadFile: boolean = false;

    previewDocument(doc: any){


        this.loadFile = true;
        this.uploadS.getdocumentblob({
            PersonID: this.token.uniqueID,
            Extension: doc.type,
            FileName: doc.filename,
            DocPath: doc.originalLocation
        }).subscribe(data => {
            console.log(data);
            //let data = window.URL.createObjectURL(blob);
            this.fileDocumentName = data.fileName
            this.urlFile = "about:blank";
            setTimeout(() => {
                this.urlFile = `${data.path}`;
                this.loadFile = false;
                //this.urlFile = `https://docs.google.com/gview?url=http://dev.ljd.dk/vertic/alinea-pdfviewer/deadlydelivery.pdf&embedded=true`
            }, 100);        
      
            // let link = document.createElement('a');
            // link.href = data;
            // link.download = doc.title;
            // link.click();
            // setTimeout(() =>{           
            //     window.URL.revokeObjectURL(data);
            // }, 100);
        })
    
    
    
        // var adamasUrl = `http://45.77.37.207:9000/document/${this.user.id}/${doc.title}`
    
        // var docUrl = "http://writing.engr.psu.edu/workbooks/formal_report_template.doc";
        // this.urlFile = "about:blank";
        // setTimeout(() => {
        //     //this.urlFile = `https://docs.google.com/gview?url=${adamasUrl}&embedded=true`;
        //     this.urlFile = `https://docs.google.com/gview?url=http://dev.ljd.dk/vertic/alinea-pdfviewer/deadlydelivery.pdf&embedded=true`
        // }, 500);
        
    
        this.preview.open(); 
      }

    getPositionImg(data: any) {
        let type = data;
        if (type.indexOf(ImageActivity.PDF) !== -1) return ImagePosition.PDF;
        if (type.indexOf(ImageActivity.DOCUMENT) !== -1) return ImagePosition.DOCUMENT;
        if (type.indexOf(ImageActivity.DOCMSWORD) !== -1) return ImagePosition.DOCMSWORD;
        if (type.indexOf(ImageActivity.SPREADSHEET) !== -1 || type.indexOf(ImageActivity.SPREADSHEETMS) !== -1) return ImagePosition.SPREADSHEET;
        if (type.indexOf(ImageActivity.TEXTFILE) !== -1) return ImagePosition.TEXTFILE;
        if (type.indexOf(ImageActivity.PRESENTATION) !== -1) return ImagePosition.PRESENTATION;
        if (type.indexOf(ImageActivity.JPG) !== -1) return ImagePosition.JPG;
        if (type.indexOf(ImageActivity.PNG) !== -1) return ImagePosition.PNG;
        if (type.indexOf(ImageActivity.MP3) !== -1 || type.indexOf(ImageActivity.MPEG) !== -1) return ImagePosition.MP3;
        if (type.indexOf(ImageActivity.MP4) !== -1) return ImagePosition.MP4;
    }
}
