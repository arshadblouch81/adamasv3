using System.Collections.Generic;
using System;
namespace Adamas.Models.Modules{
    
    public class Preferences {
        public int? RecordNumber { get; set; } = 0;
        public string PersonID { get; set; }
        public string? Preference { get; set; }
        public string Notes { get; set; }
        public  List<String> selectedPrefernces {get;set;}
    }
}