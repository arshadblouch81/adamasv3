
import {forkJoin,  Subscription ,  Observable ,  Subject } from 'rxjs';

import {takeUntil} from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Component, OnInit,OnDestroy, ViewChild } from '@angular/core'

import { Router } from '@angular/router'
import { BsModalComponent } from 'ng2-bs3-modal';
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, GlobalService, ListService, achievementIndex } from '@services/index'


@Component({
    selector: '',
    templateUrl:'./alerts.html'
})

export class IntakeAlert implements OnInit,OnDestroy{
    private unsubscribe$ = new Subject()

    @ViewChild('addCompetencyModal', { static: false }) addCompetencyModal: BsModalComponent;

    dbAction: number = 0
    user: any
    private alerts$: Subscription

    healthalerts: Array<any>
    rosteralerts: Array<any>
    competencies: Array<any>
    competenciesListArr: Array<string>

    loading: boolean;
    alertGroup: FormGroup;
    competencyGroup: FormGroup;
    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private router: Router,
            private globalS: GlobalService,
            private listS: ListService,
            private formBuilder: FormBuilder

        ){
            this.alerts$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'alerts')){
                    this.search();
                }
            });
    }

    ngOnInit(){
        this.resetGroup()
        this.reloadAll()
    }

    ngOnDestroy(): void{
        this.alerts$.unsubscribe();
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    resetGroup(){
        this.alertGroup = this.formBuilder.group({
            safetyAlert: '',
            rosterAlert: ''
        })

        this.competencyGroup = this.formBuilder.group({
            competencyValue: '',
            mandatory: false,
            notes: '',
            personID: '',
            recordNumber: 0
        })
    }

    reloadAll(){
        this.search();
        this.dropDowns();
    }

    dropDowns(){
        this.listS.getintakecompetencies(this.user.uniqueID).pipe(takeUntil(this.unsubscribe$)).subscribe(data => this.competenciesListArr = data)
    }

    search(){
        this.loading = true;
        this.user = this.sharedS.getPicked(); 
        
        forkJoin([
            this.timeS.gethealthalerts(this.user.uniqueID),
            this.timeS.getrosteralerts(this.user.uniqueID),
            this.timeS.getspecificcompetencies(this.user.uniqueID)
        ]).pipe(takeUntil(this.unsubscribe$))
            .subscribe(data => {
            this.loading = false

            this.alertGroup.patchValue({
                safetyAlert: data[0].data,
                rosterAlert: data[1].data
            })
            this.competencies = data[2]
        })
    }

    competencyProcess(){
        this.competencyGroup.controls['personID'].setValue(this.user.uniqueID)
        const competency = this.competencyGroup.value;

        if(this.dbAction == 0){
            this.timeS.postintakecompetency(competency).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data =>{
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Competency Deleted')
                            }
                        })
        }

        if(this.dbAction == 1){
            this.timeS.updateintakecompetency(competency).pipe(
            takeUntil(this.unsubscribe$)).subscribe(data => {
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Competency Deleted')
                            }
                        })
        }
    }

    updatecompetency(data: any){
        this.addCompetencyModal.open()
        this.competencyGroup.patchValue({
            competencyValue: data.competency,
            mandatory: data.mandatory,
            notes: data.notes,
            recordNumber: data.recordNumber
        })
    }
    
    deletecompetency(data: any){
        this.timeS.deleteintakecompetency(data.recordNumber).pipe(
        takeUntil(this.unsubscribe$)).subscribe(data => {
                        if(data){
                            this.reloadAll()
                            this.globalS.sToast('Success','Competency Deleted')
                        }
                    })
    }

    updateHealthAlert(){
        this.timeS.updatehealthalerts(this.alertGroup.value.safetyAlert, this.user.uniqueID ).pipe(
        takeUntil(this.unsubscribe$)).subscribe(data => {
                        if(data){
                            this.globalS.sToast('Success','Alert Updated')
                        }
                    })
    }

    updateRosterAlert(){
        this.timeS.updaterosteralerts(this.alertGroup.value.rosterAlert, this.user.uniqueID ).pipe(
        takeUntil(this.unsubscribe$)).subscribe(data => {
                        if(data){
                            this.globalS.sToast('Success','Alert Updated')
                        }
                    })
    }
}