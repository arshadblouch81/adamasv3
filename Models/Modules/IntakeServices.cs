using Adamas.Models.Tables;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System;

namespace Adamas.Models.Modules{
    public class IntakeServices : ServiceOverview
    {
        public IntakeServices(){

        }
        
        public IntakeServices(ServiceOverview overview){
            this.ServiceType = overview.ServiceType;
            this.Frequency = overview.Frequency;
            this.Period = overview.Period;
            this.Duration = overview.Duration;
            this.UnitType = overview.UnitType;            
            this.UnitBillRate = overview.UnitBillRate;            
            this.ActivityBreakDown = overview.ActivityBreakDown;
            this.ServiceProgram = overview.ServiceProgram;
            this.ServiceBiller = overview.ServiceBiller;
            this.ServiceStatus = overview.ServiceStatus;
            this.ForceSpecialPrice = overview.ForceSpecialPrice;
            this.TaxRate = overview.TaxRate;
            this.ExcludeFromNDIAPriceUpdates = overview.ExcludeFromNDIAPriceUpdates;
            this.AutoInsertNotes = overview.AutoInsertNotes;
        }
        public List<HumanResources> HumanResources { get; set; }
    }
}