using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Diagnostics;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Text.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;

using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Http.Features;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OAuth;

using System.Text;
using Microsoft.AspNetCore.Http;
using System;

using Adamas.Converters;
using Adamas.Models;
using Adamas.Middleware;
using Adamas.Models.Modules;
using Adamas.Controllers;
using Adamas.Formatters;
using Adamas.Models.Interfaces;
using AdamasV3.Models.Modules;
using AdamasV3.Helper;

using System.Runtime.Loader;
using System.Reflection;
using System.IO;

using Adamas.DAL;
using Adamas.ServiceWorker;
using Adamas.Authorization;

using Repositories;

using jsreport.AspNetCore;
using jsreport.Binary;
using jsreport.Local;

using eg_03_csharp_auth_code_grant_core;
using eg_03_csharp_auth_code_grant_core.Common;
using eg_03_csharp_auth_code_grant_core.Models;
using Newtonsoft.Json.Serialization;


using Adamas.Models.XeroModels;
using Xero.NetStandard.OAuth2.Config;
using AdamasV3.DAL;
using System.Linq;
using NPOI.HSSF.Record;

namespace AdamasV3
{
    public class Startup
    {

        readonly string AllowOriginString = "_AllowOrigin";
        readonly string AllowAll = "_AllowAll";
        public static string EmbeddedReportsPrefix = "AdamasV3.Reports";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy(AllowAll, builder => builder.AllowAnyOrigin()
                                                              .AllowAnyHeader()
                                                              .AllowAnyMethod());
            });

            services.AddHttpClient();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // services.AddHttpsRedirection(options => options.HttpsPort = 5001);

            services.AddControllersWithViews();

            services.AddControllers()
                    .AddJsonOptions(options => {
                        options.JsonSerializerOptions.Converters.Add(new DateTimeOffsetConverter());
                    });

            services.AddSpaStaticFiles(config =>
            {
                config.RootPath = "ClientApp/dist";
            });
            services.AddSingleton<SharePointService>();
   
            var jsReportConfig = Configuration.GetSection("JSReportConfiguration");
            services.Configure<JSReportConfiguration>(jsReportConfig);

            // Connection String Connection
            services.AddDbContext<DatabaseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Production"),
            
            sqlServerOptionsAction: sqlOptions =>
            {
                sqlOptions.EnableRetryOnFailure(
                    maxRetryCount: 5,
                    maxRetryDelay: TimeSpan.FromSeconds(30),
                    errorNumbersToAdd: null);
                    
            }));
            

            services.AddDbContext<DynamicDatabaseContext>((serviceProvider, dbContextBuilder) =>
            {
                var connectionStringPlaceHolder = Configuration.GetConnectionString("DynamicConnection");
                var httpContextAccessor = serviceProvider.GetRequiredService<IHttpContextAccessor>();
                //var dbName = httpContextAccessor.HttpContext.Request.Headers["tenantId"].First();
                var dbName = (httpContextAccessor.HttpContext.Request.Path).ToString().Split("/").Last();
                var connectionString = connectionStringPlaceHolder.Replace("{dbName}", dbName);
               dbContextBuilder.UseSqlServer(connectionString,             
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure(
                        maxRetryCount: 3,
                        maxRetryDelay: TimeSpan.FromSeconds(30),
                        errorNumbersToAdd: null);
                });
                dbContextBuilder.EnableDetailedErrors();
                 dbContextBuilder.EnableSensitiveDataLogging();
            });

            

            DSConfiguration config = new DSConfiguration();
            Configuration.Bind("DocuSign", config);
            services.AddSingleton(config);

            services.AddHttpContextAccessor();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("markpolicy", policy => policy.Requirements.Add(new CacheRequirement()));
            });

            services.AddSingleton<IAuthorizationHandler, CacheHandler>();

            services.AddHostedService<TimerHostedService>();
            services.AddHostedService<TokenVerificationService>();

            services.AddResponseCompression();

            services.AddMvc().AddNewtonsoftJson(options =>{
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            
            services.AddControllers()
            .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<XeroConfiguration>(Configuration.GetSection("XeroConfiguration"));
            services.AddSingleton<XeroConfigurationPKCE>(Configuration.GetSection("XeroConfigurationPKCE").Get<XeroConfigurationPKCE>());

            services.AddSingleton<ISqlDbConnection, SqlDbConnection>();
            services.AddSingleton<IRequestItemsService, RequestItemsService>();

            services.AddScoped<IGeneralSetting, GeneralSetting>();
            services.AddScoped<IUploadService, UploadService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IUploadRepository, UploadRepository>();

            //services.AddScoped<IDocusignRepository, DocusignRepository>();

            services.AddScoped<ITimesheetService, TimesheetService>();
            services.AddScoped<IBillingService, BillingService>();
            services.AddScoped<IListService, ListService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IWordService, WordService>();
            services.AddScoped<IPrintService, PrintService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IXeroService, XeroService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IDocusignService, DocusignService>();
            services.AddScoped<ILogService, LogService>();

            services.AddSingleton<ICacheService, CacheService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IJwtHandler, JwtHandler>();
            services.AddSingleton<IEmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<Adamas.Models.Modules.EmailConfiguration>());
            services.AddTransient<Adamas.Controllers.IEmailService, EmailController>();

            services.AddTransient<Adamas.DAL.IEmailService, EmailService>();

            services.AddMemoryCache();
            services.AddHttpClient();   

             services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            services.Configure<FormOptions>(options =>
            {
                options.MemoryBufferThreshold = Int32.MaxValue;
            });
                        

            services.AddSession(options =>{
                options.Cookie.Name = "Adamas.Cookie.Session";
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.IsEssential = true;
            });

            services.AddResponseCaching();

            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            // If using IIS:
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.Configure<Adamas.Models.Modules.FilesizeLimit>(Configuration.GetSection("FilesizeLimit"));
            services.Configure<Adamas.Models.Modules.ProfilePhotos>(Configuration.GetSection("ProfilePhotos"));
            services.Configure<Adamas.Models.Modules.RemoteDownloadDirectory>(Configuration.GetSection("RemoteDownloadDirectory"));
            
            services.Configure<Adamas.Models.JwtIssuerOptions>(options =>
            {
                options.Issuer = Configuration["JwtIssuerOptions:Issuer"];
                options.Audience = Configuration["JwtIssuerOptions:Audience"];
                options.ValidFor = TimeSpan.FromMinutes(Convert.ToDouble(Configuration["JwtIssuerOptions:ExpireTime"] + .05));
                options.SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["JwtIssuerOptions:Key"])), SecurityAlgorithms.HmacSha256
                );
            });

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });


            services.AddAuthentication(ops =>
            {
                ops.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                ops.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddCookie()
            .AddOAuth("Docusign", options => {
                options.ClientId = Configuration["DocuSign:ClientId"];
                options.ClientSecret = Configuration["DocuSign:ClientSecret"];
                options.CallbackPath = new PathString("/ds/callback");

                options.AuthorizationEndpoint = Configuration["DocuSign:AuthorizationEndpoint"];
                options.TokenEndpoint = Configuration["DocuSign:TokenEndpoint"];
                options.UserInformationEndpoint = Configuration["DocuSign:UserInformationEndpoint"];
                options.Scope.Add("signature");
                options.SaveTokens = true;
                options.ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "sub");
                options.ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
                options.ClaimActions.MapJsonKey("accounts", "accounts");

                options.ClaimActions.MapCustomJson("account_id", obj => ExtractDefaultAccountValue(obj, "account_id"));
                options.ClaimActions.MapCustomJson("account_name", obj => ExtractDefaultAccountValue(obj, "account_name"));
                options.ClaimActions.MapCustomJson("base_uri", obj => ExtractDefaultAccountValue(obj, "base_uri"));
                options.ClaimActions.MapJsonKey("access_token", "access_token");
                options.ClaimActions.MapJsonKey("refresh_token", "refresh_token");
                options.ClaimActions.MapJsonKey("expires_in", "expires_in");
                
                options.Events = new OAuthEvents
                {
                    OnCreatingTicket = async context =>
                    {
                        var request = new HttpRequestMessage(HttpMethod.Get, context.Options.UserInformationEndpoint);
                        request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", context.AccessToken);

                        var response = await context.Backchannel.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, context.HttpContext.RequestAborted);
                        response.EnsureSuccessStatusCode();
                        var user = JObject.Parse(await response.Content.ReadAsStringAsync());

                        user.Add("access_token", context.AccessToken);
                        user.Add("refresh_token", context.RefreshToken);
                        user.Add("expires_in", DateTime.Now.Add(context.ExpiresIn.Value).ToString());

                        using (JsonDocument payload = JsonDocument.Parse(user.ToString()))
                        {
                            context.RunClaimActions(payload.RootElement);
                        }
                    },
                    OnRemoteFailure = context =>
                    {
                        context.HandleResponse();
                        context.Response.Redirect("/Home/Error?message=" + context.Failure.Message);
                        return Task.FromResult(0);
                    }
                };
            })
            .AddJwtBearer(ops =>
            {
                
                ops.RequireHttpsMetadata = false;
                ops.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["JwtIssuerOptions:Issuer"],
                    ValidateAudience = true,
                    ValidAudience = Configuration["JwtIssuerOptions:Audience"],
                    ValidateLifetime = true,
                    RequireExpirationTime = true,
                    RequireSignedTokens = true,
                    ValidateIssuerSigningKey = true,
                    ClockSkew = TimeSpan.Zero,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(Configuration["JwtIssuerOptions:Key"])
                    )
                };

            });

            services.AddJsReport(new LocalReporting().UseBinary(JsReportBinary.GetBinary()).KillRunningJsReportProcesses().AsUtility().Create());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.ConfigureCustomExceptionMiddleware();
            // app.UseHttpsRedirection();
            app.UseStaticFiles();
         
            

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "document")),
                RequestPath = "/document"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "media")),
                RequestPath = "/media"
            });

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "StaticFiles")),
                RequestPath = "/StaticFiles",
                EnableDirectoryBrowsing = true
            });

            // app.UseReporting(settings =>
            // {
            //     settings.UseEmbeddedTemplates(EmbeddedReportsPrefix, Assembly.GetAssembly(GetType()));
            //     settings.UseCompression = true;
            // });

            // app.UseRouting();

            // app.UseEndpoints(endpoints =>
            // {
            //     endpoints.MapControllerRoute(                    
            //         name: "default",
            //         pattern: "{controller}/{action=Index}/{id?}");
            // });

            app.UseCors(AllowAll);

            app.UseSpaStaticFiles();
            app.UseResponseCaching();

            app.UseSession();

            //app.UseMiddleware<ApiErrorMiddleware>();

            app.UseResponseCompression();
            
    
            //app.UseExceptionHandler(a => a.Run(async context =>{
            //    var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
            //    var exception = exceptionHandlerPathFeature.Error;
                
            //    var result = JsonConvert.SerializeObject(new { error = exception.Message });
            //    context.Response.ContentType = "application/json";
            //    await context.Response.WriteAsync(result);
            //}));

            app.UseRouting();
           
            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });


            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "document")),
                RequestPath = "/document"
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "media")),
                RequestPath = "/media"
            });

            

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    //spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }

        private string ExtractDefaultAccountValue(JsonElement obj, string key)
        {
            if (!obj.TryGetProperty("accounts", out var accounts))
            {
                return null;
            }

            string keyValue = null;

            foreach (var account in accounts.EnumerateArray())
            {
                if (account.TryGetProperty("is_default", out var defaultAccount) && defaultAccount.GetBoolean())
                {
                    if (account.TryGetProperty(key, out var value))
                    {
                        keyValue = value.GetString();
                    }
                }
            }

            return keyValue;
        }

    }
}
