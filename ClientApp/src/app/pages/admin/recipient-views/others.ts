import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, othersType, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
    styles: [`
        nz-select {
            width: 100%;
            background-color: #cbe8f7 !important;
        }

     input{
        background-color: #cbe8f7;
    }

        input:focus {
            background-color: #85B9D5;
        }   

        nz-select:focus {
            background-color: #85B9D5;
        }  

        .row{
            margin:5px;
        }

        .input{
            background-color: #cbe8f7;
            border: 1px solid #cbe8f7;
        }
        .selected{
            background-color: #85B9D5;
            border: 1px solid #85B9D5;
        }
    

        ul{
            list-style:none;
        }

        div.divider-subs div{
            margin-top:2rem;
        }

        .layer2 > span{
            width:13rem;
            text-align:right;
        }
        .layer2 > input{
            width:4rem;
        }
        .layer2 {
            margin-bottom:5px;
        }
        .layer2 > *{
            display:inline-block;
            margin-right:5px;
            font-size:11px;
        }
        nz-select{
            width:12rem;
        }

         /* Set background color for the dropdown options */
         :host ::ng-deep .nz-option {
        background-color: #cbe8f7 !important; /* Change to your desired background color */
        }

        :host ::ng-deep  .ant-select-selection   {
            background-color: #cbe8f7 !important;
            }
        
    `],
    templateUrl: './others.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class RecipientOthersAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

    types: Array<string> = othersType;
    branches: Array<string> = [];
    casemanagers: Array<string> = [];
    lstFinancialClass:Array<any> = []
    othersForm: FormGroup;
    staffs: Array<string> = []

    dateFormat: string = dateFormat;

    occupationDefault: string;
    financialClassDefault: string;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'others')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        this.user = this.sharedS.getPicked();
        this.buildForm();        
        this.populate();
        this.search(this.user);

        let sql=`SELECT Description FROM DataDomains WHERE DOMAIN = 'FINANCIALCLASS'`;

        this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
           this.lstFinancialClass = data.map(x => x.description);
        });

        this.sharedS.emitSaveAll$.subscribe(data => {
            if (data.type=='Recipient' && data.tab=='Other'){
            
              this.save();
            }
          })

        
    
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    search(data: any){
        this.timeS.getothers(data.id)
            .subscribe(data => {
                this.othersForm.patchValue(data)
                this.occupationDefault = data.occupation;
                this.detectChanges();
            });
    }

    buildForm(){
        this.othersForm = this.formBuilder.group({
            type: null,
            admissionDate: null,
            branch: null,
            recipient_Coordinator: null,
            mainSupportWorker: null,
            occupation: null,
            generatesReferrals: false,
            acceptsReferrals: false,
            companyFlag: false,
            excludeFromRosterCopy: false,
            financialClass: null,
            admittedBy: null,
            dischargeDate: null,
            dischargedBy: null     
            

        });
    }

    canDeactivate() {
        if (this.othersForm && this.othersForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Changes have been detected. Save Changes?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                    
                }
            });
        }
        return true;
    }

  

    populate(){
        forkJoin([
            this.listS.getlistbranches(),
            this.listS.getlistcasemanagers(),
           // this.listS.getfinancialclass()
        ]).subscribe(data => {
            this.branches = data[0];
            this.casemanagers = data[1];
         //   this.lstFinancialClass = data[2];
            this.detectChanges();
        })

        // this.timeS.getstaff({
        //     User: this.globalS.decode().nameid,
        //     SearchString: '',
        //     IncludeInactive: false,
        //   }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     this.staffs = data.map(x => x.accountNo);
        //     this.detectChanges();
        //   });
    }

    save(){
        this.timeS.updateothers(this.othersForm.value, this.user.id)
            .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
    }

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }


}