namespace Adamas.Models.Modules{
    public class RosterInput {
        public string Value { get; set; }
        public string Key { get; set; }
    }
}