

import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, othersType, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { functionalStatus} from '@services/global.service'

@Component({
    styles: [`
        nz-select {
            width: 100%;
            background-color: #cbe8f7 !important;
        }

     input{
        background-color: #cbe8f7;
    }

        input:focus {
            background-color: #85B9D5;
        }   

        nz-select:focus {
            background-color: #85B9D5;
        }  

        .row{
            margin:5px;
        }

        .input{
            background-color: #cbe8f7;
            border: 1px solid #cbe8f7;
        }
        .selected{
            background-color: #85B9D5;
            border: 1px solid #85B9D5;
        }
    

        ul{
            list-style:none;
        }

        div.divider-subs div{
            margin-top:2rem;
        }

        .layer2 > span{
            width:13rem;
            text-align:right;
        }
        .layer2 > input{
            width:4rem;
        }
        .layer2 {
            margin-bottom:5px;
        }
        .layer2 > *{
            display:inline-block;
            margin-right:5px;
            font-size:11px;
        }
        nz-select{
            width:12rem;
        }

         /* Set background color for the dropdown options */
         :host ::ng-deep .nz-option {
        background-color: #cbe8f7 !important; /* Change to your desired background color */
        }

        :host ::ng-deep  .ant-select-selection   {
            background-color: #cbe8f7 !important;
            }

            .heading  {
              font-size:15px; font-weight:bold; font-family:'Segoe UI';
            }
            .heading-div{
                margin-left:5px;
                  margin-top :20px; margin-bottom :20px; font-size:15px; font-weight:bold; font-family:'Segoe UI';
            }
        
    `],
    templateUrl:'./career-status.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ClinicalCareerStatus implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

    carerRequiredList:any= functionalStatus.carerRequired;
    supportIssues:any= functionalStatus.supportIssues;
    
    carerAvailabilityList: any;
    carerResidencyList: any;
    ClientCarerNeeded :any;
    SustainabilityList:any;
    

    carerstatusForm: FormGroup;
 
    dateFormat: string = dateFormat;

    occupationDefault: string;
    financialClassDefault: string;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private modalService:NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'career-status')) {
                this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        this.user = this.sharedS.getPicked();
        this.buildForm();        
       // this.populate();
       

       // let sql=`SELECT Description FROM DataDomains WHERE DOMAIN = 'FINANCIALCLASS'`;

        // this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //    this.lstFinancialClass = data.map(x => x.description);
        // });
       
        this.sharedS.emitSaveAll$.subscribe(data => {
         
            if (data.type=='Recipient' && data.tab =='Clinical Detail' && this.globalS.isCurrentRoute(this.router, 'career-status')){
            
              this.save();
            }
          })

        this.getCarerDataLists();
    
    }
  getCarerDataLists(){
            return forkJoin([            
                this.listS.GetCarerDataAvailability(),
                this.listS.GetCarerDataResidency(),
                this.listS.GetCarerSustainability(),
            ]).subscribe(x => {
               
                this.carerAvailabilityList  = x[0];
                this.carerResidencyList     = x[1];
                this.SustainabilityList     = x[2];
                this.search(this.user);
            });
        }
    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    search(data: any){
        this.timeS.getCareerStatus(data.id)
            .subscribe(data => {
                this.carerstatusForm.patchValue(data)               
                this.detectChanges();
            });
    }
    //  c_General_NeedForCarer,CarerAvailability, c_Issues_Sustainability, CarerResidency, CarerRelationship, CarerAvailability, DatasetCarer, , 
    //c_Support_Help, c_Support_Allowance, c_Support_Information, c_Support_NeedTraining, c_Threats_Emotional, c_Threats_Physical, 
    //c_Threats_Physical_Slow, c_Threats_Unrelated, c_Threats_ConsumerNeeds, c_Threats_ConsumerOther, c_Issues_Comments 
    


    buildForm(){
        this.carerstatusForm = this.formBuilder.group({
        c_General_NeedForCarer: [''],
        carerAvailability: [''],
        c_Issues_Sustainability: [''],
        carerResidency: [''],
        carerRelationship: [''],
        datasetCarer: [''],
        c_Support_Help: [''],
        c_Support_Allowance: [''],
        c_Support_Information: [''],
        c_Support_NeedTraining: [''],
        c_Threats_Emotional: [false],
        c_Threats_Physical: [false],
        c_Threats_Physical_Slow: [false],
        c_Threats_Unrelated: [false],
        c_Threats_ConsumerNeeds: [false],
        c_Threats_ConsumerOther: [false],
        c_Issues_Comments: ['']
            

        });
    }

    canDeactivate() {
        if (this.carerstatusForm && this.carerstatusForm.dirty) {
            this.modalService.confirm({
                nzTitle: 'Changes have been detected. Save Changes?',
                nzContent: '',
                nzOkText: 'Yes',
                nzOnOk: () => {
                    this.save();
                },
                nzCancelText: 'No',
                nzOnCancel: () => {
                    
                }
            });
        }
        return true;
    }

  

    populate(){
        // forkJoin([
        //     this.listS.getlistbranches(),
        //     this.listS.getlistcasemanagers(),
        //    // this.listS.getfinancialclass()
        // ]).subscribe(data => {
        //     this.branches = data[0];
        //     this.casemanagers = data[1];
        //  //   this.lstFinancialClass = data[2];
        //     this.detectChanges();
        // })

        // this.timeS.getstaff({
        //     User: this.globalS.decode().nameid,
        //     SearchString: '',
        //     IncludeInactive: false,
        //   }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        //     this.staffs = data.map(x => x.accountNo);
        //     this.detectChanges();
        //   });
    }

    save(){
        this.timeS.updateCareerStatus(this.carerstatusForm.value, this.user.id)
            .subscribe(data => this.globalS.sToast('Success','The form is saved successfully'));
    }

    detectChanges(){
        this.cd.markForCheck();
        this.cd.detectChanges();
    }


}