import { Component, OnInit, OnDestroy, Input, AfterViewInit,ChangeDetectorRef, EventEmitter, Output,Inject, Injectable, Renderer2, ViewEncapsulation} from '@angular/core'
import { Router,ActivatedRoute } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import {  NzModalService } from 'ng-zorro-antd/modal';
import { ListService,PrintService,GlobalService,NavigationService,TimeSheetService, MenuService} from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';
import { takeUntil } from 'rxjs/operators';
import * as groupArray from 'group-array';
import { stubFalse } from 'lodash';



@Component({
    templateUrl: './AIAssistant.html',
    styleUrls: ['./AIAssistant.css'],
    selector: 'app-aiassistant',
   
  })

export class AIAssistant implements OnInit, OnDestroy, AfterViewInit{

  private unsubscribe = new Subject();
    
    dateFormat: string ='dd/MM/yyyy';
    dmType: string = 'Staff Management';
    Spiner_loading:boolean=false;
    loadMain :EventEmitter<any> = new EventEmitter() ;
    @Input() isVisible:boolean=false;    
    @Output() AIRosterDone = new EventEmitter();
    @Input() loadData:Subject<any> = new Subject();
    today: Date = new Date();
    index = 0;
    theme:string;
    payEndingDate:any ='';
    styleUrl:any;
    rosters_display:Array<any> =[];
    selectedRosters:Array<any> =[];
    loading:boolean=false;
    inputForm: FormGroup;
    token:any;

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private timeS: TimeSheetService,
        private modalService: NzModalService,
        private listS: ListService,
        private globalS:GlobalService,
        private nzContextMenuService: NzContextMenuService,
    
        ){

      this.listS.getpayperiod().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.payEndingDate=data.payPeriodEndDate;
            console.log(this.payEndingDate);
          });
        }
      
        ngOnInit(): void {
       // this.loadTheme('LIGHT')

       this.loadData.subscribe(data => {
        this.selectedRosters = data;
        this.isVisible=true;
        
        this.timeS.getAIRosters(this.token.user).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.rosters_display = data;
           
           });
       });
      
            this.token=this.globalS.decode();
       this.inputForm = this.fb.group({
            jobStatus : 'partTime',
            partTime:false,
            fullTime:false,
            casual:false,
            allDesirableCompetencies :false,
            previousWork :false,
            duration:3,
            limitedPreferredStaff :false,
            withinStraightLine:0,
            minFromKnownAddress:0,
            rating:0,
            distance:0,
            autofillGapsTravelTime:false,
            minGapTravelTime:0,
            maxGapTravelTime:0,
            autofillRequiredBreaks:false,
            allJobCategory:false,

       });

    
           
        }
        ngAfterViewInit(): void {
            
        }

     
        ngOnDestroy(): void {
        }
        handleCancel(){
            this.isVisible=false;
            this.AIRosterDone.emit(this.rosters_display);
           
        }
        
        contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent): void {
         
          this.nzContextMenuService.create($event, menu);
        }
        menuclick(event:any,i:number){
          if (i==1){
            //this.addTab()
  
          }else if (i==2){
            //  this.removeTab();
          }
      }

      acceptRoster(){

      }
      redo(){

      }
      messageStaff(){

      }
      lstPermanentRosters = [];
      generate_lines(){

       
        const dragColumns = "cycleGroup" ;//this.quoteLines.map(x => "mainGroup");
       
        var convertedObj = groupArray(this.lstPermanentRosters, dragColumns);
    
        //console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);
    
        if(dragColumns.length == 0){
          this.rosters_display = this.lstPermanentRosters;
        } else {
            this.rosters_display = flatten;
        }
    
        
    }
    flatten(obj: any, res: Array<any> = [], counter = null){
      for (const key of Object.keys(obj)) {
          const propName = key;
          if(typeof propName == 'string'){                   
              res.push({key: propName, counter: counter, display:true, count: '1'});
              counter++;
          }
          if (!Array.isArray(obj[key])) {
              this.flatten(obj[key], res, counter);
              counter--;
          } else {
              res.push(obj[key]);
              counter--;
          }
      }
      return res;
    }
    
    isArray(data: any){
      return Array.isArray(data);
    }
    
    isSome(data: any){
      if(data){
          return data.some(d => 'key' in d);
      }
      return true;        
    }
    trackByFn(index, item) {
      return item.id;
    }

    toggleTable(index: number) {
    
        this.rosters_display[index].display = !this.rosters_display[index].display;
       
      
      }

    }