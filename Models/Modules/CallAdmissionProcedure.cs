

using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class CallAdmissionProcedure{

        public List<ProcedureRoster> Roster { get; set; }
        public ProcedureClientStaffNote Note { get; set; }
      
        public string Program { get; set; }
        public string Service { get; set; } 

    }

    
}
