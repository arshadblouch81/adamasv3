using System;

namespace Adamas.Models.Modules{
    public class CopyCutRosters {
        public int RecordNo { get; set; }
        public DateTime Date { get; set; }
    }
}