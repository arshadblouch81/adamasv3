import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { formatDate } from '@angular/common';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-unApproved-client-hours',
    templateUrl: './clientHours.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
.btn1 {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}

`]
})

export class ClientHoursComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    modalOpen: boolean = false;

    listData: Array<any>;
    loading: boolean = false;
    inputForm: FormGroup;
    check: boolean = false;
    userRole: string = "userrole";
    tocken: any;
    unapprovedClientHoursList: Array<any>;
    tableData: Array<any>;
    postLoading: boolean = false;
    dtpEndDate: any;
    dtpStartDate: any;
    PayPeriodLength: number;
    printLoad: boolean = false;

    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean = false;
    

    constructor(
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
    ) { }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.buildForm();
        this.loadData();
        this.loading = false;
        this.modalOpen = false;
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            dtpStartDate: '01/12/2021',
            dtpEndDate: '01/12/2021',
        });
    }

    handleCancel() {
        this.inputForm.reset();
        this.buildForm();
        this.postLoading = false;
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/billing']);
    }

    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    loadData() {
        this.loading = true;
        this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
            if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
                this.dtpEndDate = dataPayPeriodEndDate[0].payPeriodEndDate;
            }
            else {
                this.dtpEndDate = new Date
            }
            this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
                if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
                    this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
                }
                else {
                    this.PayPeriodLength = 14
                }
                var firstDate = new Date(this.dtpEndDate);
                firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
                this.dtpStartDate = formatDate(firstDate, 'MM-dd-yyyy', 'en_US');
                this.dtpStartDate = formatDate(this.dtpStartDate, 'yyyy/MM/dd', 'en_US');
                this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy/MM/dd', 'en_US');

                let dataPass = {
                    DateStart: this.dtpStartDate,
                    DateEnd: this.dtpEndDate,
                    IsWhere: null
                }

                this.billingS.getUnapprovedClientHours(dataPass).subscribe(data => {
                    this.unapprovedClientHoursList = data;
                    this.tableData = data;
                    this.loading = false;
                });
            });
        });
    }
    generatePdf(){
      
                     
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "UNAPPROVED SERVICES",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "UNAPPROVED SERVICES"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }



}
