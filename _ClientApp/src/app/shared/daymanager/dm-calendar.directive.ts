import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[option-click]'
})
export class OptionClickDirective {

  @Output() clickEmit = new EventEmitter<boolean>();
  @Output() mousedownEmit = new EventEmitter<boolean>();

  constructor(
      private el: ElementRef
  ) {  }
  
  @HostListener('mousedown', ['$event']) onMouseDown(event){
      event.stopPropagation();
      this.mousedownEmit.emit(true);
  }

  @HostListener('click', ['$event']) onMouseClick(event){
      event.stopPropagation();
      this.clickEmit.emit(true);
  }
}