import { HttpClient } from '@angular/common/http';
import { Component, OnInit,Pipe, PipeTransform } from '@angular/core';
import { XeroService } from '@services/XeroService';
import { error } from 'console';



@Component({
    selector: 'app-xero-processes',
    templateUrl: './XeroProcesses.html',
    styleUrls: ['./XeroProcesses.css']
})
export class XeroProcesses implements OnInit {
    data:any;
    activeLink:any;

    constructor(private xero:XeroService,
        private http: HttpClient,    
        ) { }

    ngOnInit(): void {
        // Initialization code here
        
    }

    addEmployee(){

      

      

          let data= JSON .parse(`[
            {
                "gender": "M",
                "status": "ACTIVE",
                "firstName": "Kamal",
                "lastName": "Ahmad",
                "dateOfBirth": "1988-06-14T00:00:00Z",
                "startDate": "2022-12-30T00:00:00Z",
                "email": "send2akmal@gmail.com",
                "phone": "0400-000-123",
                "mobile": " 408-230-9732",
                "bankAccounts": [],
                "leaveBalances": [],
                "leaveLines": [],
                "superMemberships": [],
                "updatedDateUTC": "2024-03-17T09:58:27Z",
                "validationErrors": [],
                "HomeAddress": {                   
                    "AddressLine1": "10",
                    "City": "Stone Street ",
                    "Region": "NSW",
                    "PostalCode": "6000",
                    "Country": "AUSTRALIA"
                  }
            },
            {
                "gender": "F",
                "status": "ACTIVE",
                "firstName": "Mufeed",
                "lastName": "Aslam",
                "dateOfBirth": "1985-03-19T00:00:00Z",
                "startDate": "2023-03-05T00:00:00Z",
                "email": "send2rida@gmail.com",
                "phone": "9000 1234",         
                "bankAccounts": [],
                "leaveBalances": [],
                "leaveLines": [],
                "superMemberships": [],
                "updatedDateUTC": "2024-03-17T09:58:27Z",
                "validationErrors": [],
                "HomeAddress": {
                    "AddressLine1": "11",
                    "City": "Stone Street ",
                    "Region": "NSW",
                    "PostalCode": "6000",
                    "Country": "AUSTRALIA"
                  }
            }]`);

        //   this.http.post("http://localhost:5000/api/xero/PostXeroEmployee/Adamas_Dev",data1).subscribe(d=>{
        //     this.data=d;
        //     error=>{console.log(error);
        //         this.data=error
        //     }
        // });

        // this.xero.PostXeroEmployee("Adamas_Dev",data).subscribe(d=>{
        //    this.data=d;
        
        // })

        this.xero.PostXeroEmployeeId("Adamas_Dev",data).subscribe(d=>{
            this.data=d;
         
         })
        
    }
    async getUrl(){
       // let link=  this.xero.getUrl();
        //console.log(link);
       
        

    }

   getpayrollCalendar() {
   
        this.xero.getpayrollCalendar().subscribe(d=>{
            
            this.data=JSON.stringify(d.payrollCalendars);
        })
    }


    getemployees() {
   
        this.xero.getemployees().subscribe(d=>{
            
            this.data= JSON.stringify(d.employees);
        })
    }

    getpayitems(){
        this.xero.getpayitems().subscribe(d=>{
            
            this.data= JSON.stringify(d);
        }) 
    }

    getTimeSheets(){
        this.xero.getTimeSheets('Adamas_Dev').subscribe(d=>{
            
            this.data= JSON.stringify(d);
        }) 
    }

    postTimeSheet(){

        let data= JSON .parse(` [{"status":"PROCESSED","employeeID":"cac696bd-5498-4297-a954-0c56f96d403f","startDate":"2024-02-02T00:00:00Z","endDate":"2024-02-08T00:00:00Z","hours":31,"timesheetID":"763fd82e-2fd5-4f8c-9dba-568572c4e2ff","timesheetLines":[{"earningsRateID":"c9e751f3-c5c4-48e9-be05-873ba38d0db6","numberOfUnits":[7,6,6,5,0,0,0],"updatedDateUTC":"2024-03-17T09:58:27Z"},{"earningsRateID":"b61cb9e4-dcce-4ddc-9f3d-1e943e0d8268","numberOfUnits":[3,4,0,0,0,0,0],"updatedDateUTC":"2024-03-17T09:58:27Z"}],"updatedDateUTC":"2024-03-17T09:58:27Z","validationErrors":[]}]`);

        // let data= JSON .parse(`{
        //     "Timesheets": [
        //       {
                
        //         "EmployeeID": "9e56531d-5363-4c10-ac29-7f75ea81f66c",
        //         "StartDate": "/Date(1577836800000)/",
        //         "EndDate": "/Date(1585612800000)/",
        //         "Status": "PROCESSED",
        //         "Hours": 16.0000,
        //         "TimesheetLines": [
        //           {
        //             "EarningsRateID": "8468a3fd-34b9-4b69-844a-2401588fd996",
        //             "NumberOfUnits": [
        //               8.00,
        //               8.00
        //             ],
        //             "UpdatedDateUTC": "/Date(1572581829000+0000)/"
        //           }
        //         ],
        //         "UpdatedDateUTC": "/Date(1572581829000+0000)/"
        //       }
        //     ]
        //   }`);

        this.xero.postTimeSheet('Adamas_DEV',data).subscribe(d=>{
            
            this.data= JSON.stringify(d);
        })  
    }

}