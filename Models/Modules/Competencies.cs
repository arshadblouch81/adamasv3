using System.Collections.Generic;

namespace Adamas.Models.Modules
{
    public class Competencies
    {
      public string GroupName { get; set; }
      public List<CompetencyObject> CompetenciesList { get; set; }
    }
}