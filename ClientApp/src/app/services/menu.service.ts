import { Injectable } from '@angular/core'
import { Observable, Subject } from 'rxjs';
import { AuthService } from './auth.service';
import { Branch } from '@modules/modules';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import { FormBuilder } from '@angular/forms';
import { GlobalService } from './global.service';
const menu: string = "api/menu";


@Injectable()
export class MenuService {
     
    token:any;
    private unsubscribe: Subject<void> = new Subject();
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    
    drawerVisible: boolean =  false;
    loading: boolean = false;
    constructor(
        public http: HttpClient,
        public auth: AuthService,
        private fb: FormBuilder,
        private globalS: GlobalService,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        ){ }
       


        
        /*******************************************************
         *  Services activities
        */

        getlistServices(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/Services/${is_where}`)
        }
        
        postServices(data: any): Observable<any> {
            return this.auth.post(`${menu}/Services/`, data)
        }

        updateServices(data: any): Observable<any> {
            return this.auth.put(`${menu}/Services/`, data)
        }

        /** */

        /**
         *  Staff Admin Activities
        */
        getlistItemConsumables(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/itemConsumable/${is_where}`)
        }
       
        postItemConsumables(data: any): Observable<any> {
            return this.auth.post(`${menu}/itemConsumable/`, data)
        }
        updateItemConsumables(data: any): Observable<any> {
            return this.auth.put(`${menu}/itemConsumable/`, data)
        }
        /** */

        /**
         *  Staff Menu Meals
        */
        getlistMenuMeals(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/menuMeals/${is_where}`)
        }
        postMenuMeals(data: any): Observable<any> {
            return this.auth.post(`${menu}/menuMeals/`, data)
        }
        updateMenuMeals(data: any): Observable<any> {
            return this.auth.put(`${menu}/menuMeals/`, data)
        }
        /** */

        /**
         *  Staff Case Management
        */
        GetlistcaseManagement(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/caseManagement/${is_where}`)
        }
        postcaseManagement(data: any): Observable<any> {
            return this.auth.post(`${menu}/caseManagement/`, data)
        }
        updatecaseManagement(data: any): Observable<any> {
            return this.auth.put(`${menu}/caseManagement/`, data)
        }
        /** */
        /**
         *  user detail
        */
        postuserDetails(data: any): Observable<any> {
            return this.auth.post(`${menu}/userDetails/`, data)
        }
        updateuserDetails(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetails`, data)
        }


        postuserDetailsGeneral(data: any): Observable<any> {
            return this.auth.post(`${menu}/userDetailsGeneral`, data)
        }
        updateuserDetailsGeneral(data: any): Observable<any> {
           
            return this.auth.put(`${menu}/userDetailsGeneral`, data)
        }
        updateuserDetailsRecipients(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsRecipients`, data)
        }
        
        updateuserDetailsStaff(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsStaff`, data)
        }

        updateuserDetailsDocuments(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsDocuments`, data)
        }
        updateuserDetailsRoster(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsRoster`, data)
        }
        updateuserDetailsDayManager(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsDayManager`, data)
        }
        updateuserDetailsClientPortal(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsClientPortal`, data)
        }
        updateuserDetailsMobile(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsMobile`, data)
        }
        updateuserDetailsMainMenuForm(data: any): Observable<any> {
            return this.auth.put(`${menu}/userDetailsMainMenuForm`, data)
        }
        
        /** */
        loggedOnDesktopUser():Observable<any>{
            return this.auth.get(`${menu}/loggedOnDesktopUser`)
        }
        loggedOnAppUser():Observable<any>{
            return this.auth.get(`${menu}/loggedOnAppUser`)
        }
        
        licenseRegistration():Observable<any>{
            return this.auth.get(`${menu}/licenseRegistration`)
        }
        updatelicenseRegistration(data:any):Observable<any>{
            return this.auth.put(`${menu}/licenseRegistration`, data)
        }
        /** */
        agencyRegistartion():Observable<any>{
            return this.auth.get(`${menu}/agencyRegistartion`)
        }
        updateagencyRegistartion(data: any): Observable<any> {
            return this.auth.put(`${menu}/agencyRegistartion`, data)
        }

        agencycstdalist():Observable<any>{
            return this.auth.get(`${menu}/companycstdalist`)
        }

        /**
         *  Staff Admin Activities
        */
        
        GetlistStaffAdminActivities(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/staffAdminActivities/${is_where}`)
        }
        poststaffAdminActivities(data: any): Observable<any> {
            return this.auth.post(`${menu}/staffAdminActivities/`, data)
        }
        updatestaffAdminActivities(data: any): Observable<any> {
            return this.auth.put(`${menu}/staffAdminActivities/`, data)
        }
        /** */
        
        /**
         *  Recipient Absenses
        */
        GetlistRecipientAbsenses(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/recipientAbsenses/${is_where}`)
        }
        postRecipientAbsenses(data: any): Observable<any> {
            return this.auth.post(`${menu}/recipientAbsenses/`, data)
        }
        updateRecipientAbsenses(data: any): Observable<any> {
            return this.auth.put(`${menu}/recipientAbsenses/`, data)
        }
        /**
         *  Services competencies
        */
        getconfigurationservicescompetency(id: string): Observable<any> {
            return this.auth.get(`${menu}/services/competency/${id}`)
        }

        postconfigurationservicescompetency(data: any): Observable<any> {
            return this.auth.post(`${menu}/services/competency`, data)
        }
       
        updateconfigurationservicescompetency(data: any): Observable<any> {
            return this.auth.put(`${menu}/services/competency`, data)
        }
    
        deleteconfigurationservicescompetency(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/services/competency/${recordNo}`)
        }
        
        /**
         *  Services competencies
        */
        getconfigurationserviceschecklist(id: string): Observable<any> {
            return this.auth.get(`${menu}/services/checklist/${id}`)
        }

        postconfigurationserviceschecklist(data: any): Observable<any> {
            return this.auth.post(`${menu}/services/checklist`, data)
        }
       
        updateconfigurationserviceschecklist(data: any): Observable<any> {
            return this.auth.put(`${menu}/services/checklist`, data)
        }
    
        deleteconfigurationserviceschecklist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/services/checklist/${recordNo}`)
        }
        /***/
        /**
         *  Recipient Absenses
        */
        workflowstafflist(): Observable<any>{
            return this.auth.get(`${menu}/workflowstafflist`)
        }
        getconfigurationworkflows(group:string,is_where:number):Observable<any>{
            return this.auth.get(`${menu}/configurationworkflows/${group}/${is_where}`)
        }

        postconfigurationfollowups(data: any): Observable<any> {
            return this.auth.post(`${menu}/configurationfollowups`, data);
        }
    
        updateconfigurationfollowups(data: any): Observable<any> {
            return this.auth.put(`${menu}/configurationfollowups`, data);
        }
        deleteconfigurationfollowups(recordNo: number,group:string): Observable<any> {
            return this.auth.delete(`${menu}/configurationfollowups/${group}/${recordNo}`)
        }

        activateconfigurationfollowups(recordNo: number,group:string): Observable<any> {
            return this.auth.delete(`${menu}/activeconfigurationfollowups/${group}/${recordNo}`)
        }

        /***************************************************************************/
        
        
        postawardpossetup(data: any): Observable<any> {
            return this.auth.post(`${menu}/awardpossetup`, data)
        }

        updateawardpossetup(data: any): Observable<any> {
            return this.auth.put(`${menu}/awardpossetup`, data)
        }

        /**
         *  Award Setup timebands Apis
        */
        getawardsetuptimebands(id: number): Observable<any> {
            return this.auth.get(`${menu}/awardsetup/timebands/${id}`)
        }
        postawardsetuptimebands(data: any): Observable<any> {
            return this.auth.post(`${menu}/awardsetup/timebands`, data)
        }
       
        updateawardsetuptimebands(data: any): Observable<any> {
            return this.auth.put(`${menu}/awardsetup/timebands`, data)
        }
    
        deleteawardsetuptimebands(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/awardsetup/timebands/${recordNo}`)
        }
        /*********************************end award setup timebands******************************************/

        /**
         *  Award Setup allonces Apis
        */
        getawardsetupallocances(id: number): Observable<any> {
            return this.auth.get(`${menu}/awardsetup/allownces/${id}`)
        }
        postawardsetupallownces(data: any): Observable<any> {
            return this.auth.post(`${menu}/awardsetup/allownces`, data)
        }
       
        updateawardsetupallocances(data: any): Observable<any> {
            return this.auth.put(`${menu}/awardsetup/allownces`, data)
        }
    
        deleteawardsetupallocances(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/awardsetup/allownces/${recordNo}`)
        }
        /*********************************end award setup timebands******************************************/

        /**
         *  Award Setup spcialshifts Apis
        */
        getawardsetupspecialshifts(id: number): Observable<any> {
            return this.auth.get(`${menu}/awardsetup/specialshifts/${id}`)
        }
        postawardsetupspecialshifts(data: any): Observable<any> {
            return this.auth.post(`${menu}/awardsetup/specialshifts`, data)
        }
       
        updateawardsetupspecialshifts(data: any): Observable<any> {
            return this.auth.put(`${menu}/awardsetup/specialshifts`, data)
        }
    
        deleteawardsetupspecialshifts(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/awardsetup/specialshifts/${recordNo}`)
        }


        getawardsetupspecialshiftsbreakdown(id: number): Observable<any> {
            return this.auth.get(`${menu}/awardsetup/specialshifts/breakdown/${id}`)
        }
        getawardsetupallowancesrules(id: number): Observable<any> {
            return this.auth.get(`${menu}/awardsetup/allowances/rules/${id}`)
        }
        getawardsetupallowancesinclusioncompetency(recordNumber: string): Observable<any> {
            return this.auth.get(`${menu}/allowancesinclusioncompetency/data/${recordNumber}`);
        }
        getawardsetupallowancesexclusionactivities(recordNumber: string): Observable<any> {
            return this.auth.get(`${menu}/allowancesexclusionactivities/data/${recordNumber}`);
        }
        getawardsetupallowancesexclusioncompetency(recordNumber: string): Observable<any> {
            return this.auth.get(`${menu}/allowancesexclusioncompetency/data/${recordNumber}`);
        }



        getawardsetupallowancesinclusionactivity(recordNumber: string): Observable<any> {
            return this.auth.get(`${menu}/allowancesinclusionactivity/data/${recordNumber}`);
        }
        
        /*********************************end award setup timebands******************************************/




        getsystemnumber(id: number): Observable<any> {
            return this.auth.get(`${menu}/configuration/sysnumbers/${id}`)
        }

        getloggedPortalUsers():Observable<any>{
            return this.auth.get(`${menu}/configuration/loggedPortalUsers`)
        }

        getUserList(is_where:boolean):Observable<any>{
            return this.auth.get(`${menu}/getUserList`)
        }
        Getlistequipments(is_where:boolean):Observable<any>{
            return this.auth.get(`${menu}/equipments/${is_where}`)
        }
        GetlistagencyPayTypes(is_where:boolean):Observable<any>{
            return this.auth.get(`${menu}/agencyPayTypes/${is_where}`)
        }
        getlistFundingSource(is_where: boolean): Observable<any>{
            return this.auth.get(`${menu}/fundingSource/${is_where}`)
        }
        getlistProgramPackages(is_where: boolean):Observable<any>{
            return this.auth.get(`${menu}/programPackages/${is_where}`)
        }
        postProgramPackagesClone(recordNo: number, title: string): Observable<any> {
            const url = `${menu}/cloneProgram/${recordNo}/${encodeURIComponent(title)}`;
            return this.http.post<any>(url, {});
        }
        postActivityClone(recordNo: number, title: string): Observable<any> {
            const url = `${menu}/cloneActivity/${recordNo}/${encodeURIComponent(title)}`;
            return this.http.post<any>(url, {});
        }
        mergeHistories(newServiceType: string, data: { audit: any, existingServiceTypes: string[] }): Observable<any> {
            const url = `${menu}/mergeHistories/${newServiceType}`;
            const headers = new HttpHeaders({
              'Content-Type': 'application/json'
            });
            return this.http.post<any>(url, data, { headers });
        }
        programMergeHistories(newServiceType: string, data: { audit: any, existingServiceTypes: string[] }): Observable<any> {
            const url = `${menu}/programMergeHistories/${newServiceType}`;
            const headers = new HttpHeaders({
              'Content-Type': 'application/json'
            });
            return this.http.post<any>(url, data, { headers });
        }
        getlistvehicles():Observable<any>{
            return this.auth.get(`${menu}/vehicles`)
        }
        getlistPackageLeaveTypes():Observable<any>{
            return this.auth.get(`${menu}/packgeLeaveTypes`);
        }
        getlistCompetencyByPersonId(recordNo:number):Observable<any>{
            return this.auth.get(`${menu}/competencyByPersonId/${recordNo}`);
        }
        getlistServiceCompetencyByPersonId(recordNo:string):Observable<any>{
            return this.auth.get(`${menu}/serviceCompetencyByPersonId/${recordNo}`);
        }
        
        getlistApprovedServicesByPersonId(recordNo:string):Observable<any>{
            return this.auth.get(`${menu}/approvedServicesByPersonId/${recordNo}`);
        }
        getlistApprovedStaffByPersonId(recordNo:number):Observable<any>{
            return this.auth.get(`${menu}/approvedStaffByPersonId/${recordNo}`);
        }
        getlistExcludedStaffByPersonId(recordNo:number):Observable<any>{
            return this.auth.get(`${menu}/excludeStaffByPersonId/${recordNo}`);
        }
        getlistactivityGroups(is_where:boolean):Observable<any>{
            return this.auth.get(`${menu}/activityGroups/${is_where}`)
        }
        getlistcenterFacilityLoc(is_where:boolean):Observable<any>{
            return this.auth.get(`${menu}/centerFacilityLoc/${is_where}`)
        }
        getlistserviceNotesCat():Observable<any>{
            return this.auth.get(`${menu}/serviceNotesCat`)
        }
        getlistbranches(is_where:boolean): Observable<any>{
            return this.auth.get(`${menu}/branches/${is_where}`)
        }
        AddBranch(brnch: Branch): Observable<any> {
            return this.auth.put(`${menu}/addBranch`, brnch)
        }
        UpdateBranch(brnch: Branch): Observable<any> {
            return this.auth.put(`${menu}/UpdateBranch`, brnch)
        }
        updatUDomain(sqlString: string):Observable<any>{
            return this.auth.post(`${menu}/updateDomain/`, { Sql: sqlString});
        }
        InsertRecord(sqlString: string): Observable<any>{
            return this.auth.post(`${menu}/insertSql-list`, { Sql: sqlString})
        }
        getDataDomainByType(domain: string,is_where:boolean):Observable<any>{
            return this.auth.get(`${menu}/getDomains/${domain}/${is_where}`)
        }
        InsertDomain(sqlString: string): Observable<any>{
            return this.auth.post(`${menu}/addDomain`, { Sql: sqlString})
        }
        deleteDomain(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/datadomains/${recordNo}`)
        }
        activeDomain(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/datadomains/${recordNo}`)
        }
        deleteDocumentTemplatelist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/documentTemplate/${recordNo}`)
        }
        activateDocumentTemplatelist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/documentTemplate/${recordNo}`)
        }
        deleteDistributionlist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/distribution/${recordNo}`)
        }
        activateDistributionlist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/distribution/${recordNo}`)
        }
        deleteBudgetlist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/budgets/${recordNo}`)
        }
        deletepostcodeslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/postcodes/${recordNo}`)
        }
        activatepostcodeslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/postcodes/${recordNo}`)
        }
        deleteholidayslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/holidays/${recordNo}`)
        }
        activateholidayslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/holidays/${recordNo}`)
        }
        
        deletemedicalContacts(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/medicalContacts/${recordNo}`)
        }
        activatemedicalContacts(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/medicalContacts/${recordNo}`)
        }
        deletenursingDiagnosis(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/nursingDiagnosis/${recordNo}`)
        }
        activatenursingDiagnosis(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/nursingDiagnosis/${recordNo}`)
        }
        deleteMDiagnosisTypes(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/MDiagnosisTypes/${recordNo}`)
        }
        activateMDiagnosisTypes(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/MDiagnosisTypes/${recordNo}`)
        }
        deleteMProcedureTypes(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/MProcedureTypes/${recordNo}`)
        }
        activateMProcedureTypes(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/MProcedureTypes/${recordNo}`)
        }
        deleteActivityServiceslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ActivityServices/${recordNo}`)
        }
        activateActivityServiceslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/ActivityServices/${recordNo}`)
        }
        deleteEquipmentslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/Equipments/${recordNo}`)
        }
        activateEquipmentslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/Equipments/${recordNo}`)
        }
        deletePayTypeslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/PayTypes/${recordNo}`)
        }
        activatePayTypeslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/activate/PayTypes/${recordNo}`)
        }
        deleteProgarmPackageslist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ProgarmPackages/${recordNo}`)
        }
        deletePackageLeaveTypelist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ProgarmPackages/packageLeaveType/${recordNo}`)
        }
        deleteCompetency(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ProgarmPackages/deleteCompetency/${recordNo}`)
        }
        deleteApprovedService(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ProgarmPackages/deleteApprovedServices/${recordNo}`)
        }
        deleteApprovedStaff(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ProgarmPackages/approvedStaff/${recordNo}`)
        }
        deleteExcludedStaff(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/ProgarmPackages/excludedStaff/${recordNo}`)
        }
        deleteCenterFacilityLoclist(recordNo: number): Observable<any> {
            return this.auth.delete(`${menu}/configuration/delete/CenterFacilityLoc/${recordNo}`)
        }
        activateCenterFacitlityLoclist(recordNo: number): Observable<any>{
            return this.auth.delete(`${menu}/configuration/activate/centerFacilityLoc/${recordNo}`)
        }
        

    }