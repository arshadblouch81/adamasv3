using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class AwardAllowance
    {
      public int? RecordNumber { get; set; } = 0;
      public string AwardID { get; set; }
      
      public bool? Default { get; set; }=null;
      
      public string Name { get; set; }=null;
      public string PayType { get; set; }=null;
      
      public string Notes {get;set;} = null;
      public string Category1 {get;set;} = null; 

      public string Program {get;set;} = null; 

      public string ServiceType {get;set;} = null; 
      public bool ReplaceActivitiesWithAllowance { get; set; }
      
    }
}
