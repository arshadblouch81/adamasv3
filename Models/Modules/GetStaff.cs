namespace Adamas.Models.Modules{
    public class GetStaff {
        public string User { get; set; }
        public string SearchString { get; set; }
        public bool IncludeInactive { get; set; }
        public string Status { get; set; }
        public int Take { get; set; }
        public int Skip { get; set; }
    }
}