import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes,status,period,budgetTypes,enforcement,billunit, ClientService, MenuService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import format from 'date-fns/format';
const defaultValue = {
    includeCaseManagement:false,
    status: null,
    program:null,
    activity:'',
    freq:'',
    period:'',
    duration:'',
    enforcement:'',
    starting:'',
    budgetType: null,
    bamount:'',
    specialPricing:false,
    billunit:'',
    namount:'',
    gst: false,
    compRecord:'',
    activityBreakDown:'',
    serviceBiller:'',
    autoInsertNotes:false,
    excludeFromNDIAPriceUpdates:false,
    PersonID:'',
    recordNumber:'',  
}

@Component({
    selector: '',
    templateUrl: './servicesplan.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styles: [`
        .ant-modal-body{
            padding-top:0px !important  
        },
        .ant-card-small>.ant-card-body {
            padding: 6px !important;
        },
        .ant-card-small>.ant-card-head {
            min-height: 20px !important;
        }

        nz-tabset{
            margin-top:1rem;
        }
        nz-tabset ::ng-deep div > div.ant-tabs-nav-container{
            height: 25px !important;
            font-size: 13px !important;
        }
        
        nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab{
            line-height: 24px;
            height: 25px;
        }
        nz-tabset ::ng-deep div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active{
            background: #717e94;
            color: #fff;
        }
        nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }

    .selected{
        background-color: #85B9D5;
        color: white;
    }

    .creator{
        width:200px;
        height:25px;
        margin-bottom:5px;
        background-image: linear-gradient(#fab8a4, #f73535);
    }

    `],
})

export class ServicesPlan implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    dateFormat: string ='dd/MM/yyyy';
    loading: boolean = false;
    postLoading:boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
   
    tableData: Array<any> = [];
    alist: Array<any> = [];
    alist2: Array<any> = [];
    status: string[];
    activities: any;
    program: any;
    periods: string[];
    budgetTypes: string[];
    enforcement: string[];
    billunit: string [];
    competencymodal: boolean= false;
    isUpdateCompetency: boolean = false;
    inputvalueSearch:string;
    competencyList: any;
    CompetencycheckedList: any[];
    competencyListCopy: any;
    temp: any[];
    listRecipients: any;
    isUpdate: any;
    listCompetencyByPersonId: any;
    ServiceFormGroup: FormGroup;
    activeRowData:any;
    selectedRowIndex:number;
    token:any;
    
    selectedRow  : number = 0;
    
    public editorConfig:AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '20rem',
        minHeight: '5rem',
        translate: 'no',
        customClasses: []
    };
    
 
    
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private menuS: MenuService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal']);
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'service-plans')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.user = this.sharedS.getPicked();
        this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        
        
        this.buildForm();

        this.search(this.user);
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

   

    trackByFn(index, item) {
        return item.id;
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(data: any): void {
      
          
          this.delete(data);
        

    }
    search(user: any = this.user) {
        this.cd.reattach();

        this.loading = true;
        this.timeS.getintakeserviceplans(this.user.id).subscribe(plans => {
            this.tableData = plans;
            this.loading = false;
            this.cd.markForCheck();
        });
    }
    listDropDown(user: any = this.user) {
       

        let sql = `select distinct [Service Type] as title FROM ServiceOverview WHERE ServiceOverview.[PersonID] = '${user.id}' AND ServiceStatus <> 'INACTIVE' ORDER BY [Service Type]`;
        this.listS.getlist(sql).subscribe(data => {
            this.alist = data.map(x=>x.title);
          
        });
        this.alist2=[];
        for (let i=1; i<6; i++){
            this.alist2.push(i);
        }

       
    }
    buildForm() {
        let today = new Date();
        
        this.ServiceFormGroup = this.formBuilder.group({
           
                PersonID: '',
                planName: '',
                planNumber: '',
                dvaPeriod:'',
                coPayAmount:'',
                startDate:today,
                endDate:today,
                notes:'',
                reminderDate:today,
                reminderText:'',          
                recordNumber:0,
                creator : ''
            
        });
    }
    resetModal(){
      this.ServiceFormGroup.reset();
      this.buildForm();
      this.postLoading = false;
    };
    save() {
        
            let data = this.ServiceFormGroup.value;

            this.postLoading = true;     
            if(!this.isUpdate){  

            this.timeS.postintakeserviceplans({
             
                PersonID: this.user.id,
                planName: data.planName,
                planNumber: data.planNumber,
                creator: this.token.user,
                dvaPeriod:data.dvaPeriod,
                coPayAmount:data.coPayAmount,
                startDate: format(new Date(data.startDate),'yyyy/MM/dd'),
                endDate:format(new Date(data.endDate),'yyyy/MM/dd'),
                detail:data.notes,
                reminderDate: data.reminderDate==null? null :  format(new Date(data.reminderDate),'yyyy/MM/dd'),
                reminderText:data.reminderText,          
                recordNumber:data.recordNumber,                
                code : this.user.code,
                editer :   this.token.user 
            }).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
              if (data) {
                this.globalS.sToast('Success', 'Data Added');
                this.loading = false;
                this.modalOpen = false;
                this.handleCancel();
                this.search();
                this.resetModal();
              }
            })
        }else{
            this.timeS.updateintakeserviceplans({
                PersonID: this.user.id,
                planName: data.planName,
                planNumber: data.planNumber,
                creator: this.token.user,
                dvaPeriod:data.dvaPeriod,
                coPayAmount:data.coPayAmount,
                startDate:format(new Date(data.startDate),'yyyy/MM/dd'),
                endDate:format(new Date(data.endDate),'yyyy/MM/dd'),
                detail:data.notes,
                reminderDate: data.reminderDate==null? null : format(new Date(data.reminderDate),'yyyy/MM/dd'),
                reminderText:data.reminderText,          
                recordNumber:data.recordNumber,               
                code : this.user.code,
                editer :   this.token.user     
                
              }).pipe(takeUntil(this.unsubscribe))
              .subscribe(data => {
                if (data) {
                  this.globalS.sToast('Success', 'Data Updated');
                  this.loading = false;
                  this.modalOpen = false;
                  this.isUpdate = false;
                  this.handleCancel();
                  this.search();
                  this.resetModal();
                }
              })
        }
        
    }

    showEditModal(data: any) {
        console.log(data);
        this.activeRowData = data;
        this.addOREdit = 2;
        this.listDropDown();
        this.isUpdate=true;
        this.modalOpen = true;
       

        this.ServiceFormGroup.patchValue({
            PersonID: this.user.id,
            planName: data.planname,
            planNumber: data.plannumber,
            creator: data.plancreator,
            dvaPeriod:data.dvaPeriod,
            coPayAmount:data.plancopayamount,
            startDate:new Date(data.planstartdate),
            endDate: new Date(data.planenddate),
            notes:data.plandetail,
            reminderDate: new Date(data.planreminderdate),
            reminderText:data.planremindertext,          
            recordNumber:data.recordNumber,
           
           
        });
        
    }

    delete(data: any) {
        this.timeS.deleteintakerservice(data.recordNumber).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                            if(data){
                                this.search()
                                this.loading = false
                                this.globalS.sToast('Success','Service Deleted')
                            }
             })
    }
    
   
    handleCancel() {
        this.modalOpen = false;
    }
    
    log(value: string[]): void {
    }
    showAddModal() {
        this.addOREdit = 1;
        this.ServiceFormGroup.reset(defaultValue)
      
        this.listDropDown();
      
        this.modalOpen = true;
    }
  

    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }

    print(){
        
    }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}