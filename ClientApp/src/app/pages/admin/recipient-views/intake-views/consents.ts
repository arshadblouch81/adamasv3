import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService, dateFormat } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';
import { RECIPIENT_OPTION, ModalVariables, ProcedureRoster, UserToken, CallAssessmentProcedure, Consents } from '@modules/modules';
import { DomSanitizer } from '@angular/platform-browser';
import format from 'date-fns/format';

@Component({
    selector: '',
    templateUrl: './consents.html',
    styles:[`
    h4{
        margin-top:10px;
    }
    
    .selected{
        background-color: #85B9D5;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }  
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakeConsents implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;

    consentOpen: boolean = false;
    consentGroup: FormGroup;
    consents: Array<any> = [];
    selectedconsents: Array<any> = [];

    addOREdit: number;

    lists: Array<any>;

    dateFormat: string = dateFormat;
    selectedConsent: any;
    activeRowData:any;
    selectedRowIndex:number;
    ViewAllocateResourceModal:boolean=false;
    serviceActivityList:Array<any>=[];
    originalList:Array<any>=[];
    txtSearch:any;
    HighlightRow2:number;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/intake/branches'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'consents')) {
                console.log('sasd')
                this.user = data;
                this.search(data);
            }
        });

       
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();        
        this.buildForm();
        this.search(this.user);
     
       
        this.listDropDowns()
    }

    buildForm(){
        this.consentGroup = this.formBuilder.group({
            recordNumber: null,
            personID: null,
            consent: '',
            notes: '',
            expiryDate: null,
            startDate: null
         })

        setTimeout(() => {
            this.consentGroup.controls['consent'].enable();
        }, 0);
    }

    trackByFn(index, item) {
        return item.id;
    }

    resetAll(){
        this.search();
        this.selectedConsent = {};
    }

    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(data: any): void {
        let records =this.selectedconsents.map(x=>x.recordNumber).join(',').toString();
          this.deleteconsent(records);
        
  
    }
    consentProcess(){

      
        const group = this.consentGroup.value;
        let _consentGroup: Consents = {
            recordNumber: group.recordNumber,
            personID: this.user.id,
            notes: group.notes,
            date1: group.expiryDate ? format(new Date(group.expiryDate),'yyyy-MM-dd') : null,
            date2: group.startDate ?  format(new Date(group.startDate),'yyyy-MM-dd') : null,
            name: group.consent,
            Creator: this.tocken.user,
           
        }
       
        if(this.addOREdit == 1){
            this.timeS.updateconsents(_consentGroup).subscribe(data => {
                if(data){
                    this.resetAll();
                    this.globalS.sToast('Success','Consent Updated');
                    this.handleCancel();
                }
            })
        }
    }

    showAddModal() {
      
        this.addOREdit = 0;
        this.ViewAllocateResourceModal=true;
       // this.buildForm();
       // this.consentOpen = true;
        //this.listDropDowns();

    
    }

    listDropDowns(){
        this.listS.getconsents(this.user.id).subscribe(data => {
            this.lists = data.map(x => {
                return {
                    label: x,
                    value: x,
                    checked: false
                }
            });
            this.cd.markForCheck();
        });
    }
    logs(event: any) {
        this.selectedConsent = event;
    }
    showEditModal(data: any){

        this.consentOpen = true;
        this.addOREdit = 1;
        this.activeRowData = data;
      //  this.lists = [data.consent];

        this.consentGroup.patchValue({
            
            recordNumber: data.recordNumber,
            personID: data.personID,
            consent: data.consent,
            notes: data.notes,
            expiryDate: data.expiryDate,
            startDate: data.startDate
        });
    
        // this.consentGroup.controls['consent'].disable();

    }

    deleteconsent(data: any){
        this.timeS.deleteconsents(data)
                    .subscribe(data => {
                        if(data){
                            this.resetAll();
                            this.globalS.sToast('Success','Consent Deleted')
                        }
                    })
    }

    search(user: any = this.user){
        this.cd.reattach();
        this.loading = true;

        forkJoin([
        this.timeS.getconsents(user.id),
        this.timeS.getrecipientconsents(this.user.id)
        ]).subscribe(data => {
            this.loading = false;
            this.consents = data[0];
            this.serviceActivityList = data[1];
            this.originalList = data[1];
            this.cd.markForCheck();
        })        
    }
    save() {

      
     
      
            let inputs={
                personID : this.user.id,               
                selectedConsent : this.selectedconsents,
                creator : this.tocken.user,
              
                notes : '',
              
            };
    
        
        this.ViewAllocateResourceModal=false;
        this.timeS.postconsents(inputs).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data) {
                
                this.success();
                this.globalS.sToast('Success', 'Data Added');

                
              }
        });
    }
    success() {
        this.search(this.sharedS.getPicked());
        this.loading = false;
      }
    ngOnDestroy(): void {

    }

    handleCancel(){
        this.consentOpen = false;
    }

    handleOk(){

    }
    RecordClicked(event: any, value: any, i: number, main:boolean) {

        this.selectedConsent=value;  
        if (main)
            this.selectedRowIndex =i;
        else
        this.selectedRowIndex =-1;
       
       //console.log(this.selectedconsents.includes(value))
        //let tindex = this.selected.indexOf(value.recordNo);
    
    
        if (event.ctrlKey || event.shiftKey) {
         
            if (!this.selectedconsents.includes(value))
                this.selectedconsents.push(value);
            //this.selected.push(value.recordNo)
          
    
        } else {
          
          this.selectedconsents = [];
         // this.selected = [];
    
          if (this.selectedconsents.length <= 0) {
            this.selectedconsents.push(value)
            
          }
        }
      }
   
     onItemDbClick(sel:any , i:number) : void {
     
         this.HighlightRow2=i;
         this.selectedConsent=sel;
         if (!this.selectedconsents.includes(sel)) { 
            this.selectedconsents.push(sel);
        }

         this.save();
     
     }
    onTextChangeEvent(event:any){
        // console.log(this.txtSearch);
         let value = this.txtSearch.toUpperCase();
         //console.log(this.serviceActivityList[0].description.includes(value));
         this.serviceActivityList=this.originalList.filter(element=>element.description.includes(value));
     }
    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
        this.selectedConsent=data.consent ;
      }
    print(){
        
    }
    generatePdf(){
      
        var Title = " Consents "
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.consents,                        
                "userid": this.tocken.user,
                "txtTitle":  Title+" for "+this.user.code,                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = Title+" for "+this.user.code
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.loading = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}