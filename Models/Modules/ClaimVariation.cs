using System;
namespace Adamas.Models.Modules{
    public class ClaimVariation
    {
        public int RecordNo { get; set; }
        public string ClaimedDate { get; set;}
        public string ClaimedStart { get; set; }
        public string ClaimedEnd { get; set; }
        public string ClaimedBy { get; set; }
        public string Notes { get; set; }
    }
}