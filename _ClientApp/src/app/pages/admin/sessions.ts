import { Component, OnInit } from '@angular/core';
import { CacheService, GlobalService } from '../../services/index';

@Component({
    templateUrl:'./sessions.html',
    styles: [`
        ul{
            list-style:none;
            width:20rem;
        }
        ul li{
            padding: 10px;
            border-bottom: 1px solid #e9e9e9;
        }
        article{
            position:relative;
        }
        .user{
            font-size: 17px;
            color: #4f94fb;
        }
        i.spacer{
            font-size: 10px;
            margin: 0 0.5rem;
        }    
        li button{
            position: absolute;
            top:0;
            right:0;
        }
        button.delete{
            padding: 5px 6px;
        }
        button.delete i{
            float:left
        }
    `]
})

export class SessionsAdmin implements OnInit {
    source: any = null;
    users: Array<any>;
    
    constructor(
      private cacheS: CacheService,
      private globalS: GlobalService
    ){    }

    ngOnInit(){
        this.getUsers();
    }

    getUsers(){
        this.cacheS.getusers()
            .subscribe(data => {

                this.users = data.map(x => {
                    var token = this.globalS.decode(x.value);
                    return {
                        key: x.key,
                        token
                    };
                });
            });
    }

    logout(index: number){
        var id = this.users[index].key;

        this.cacheS.deleteuser(id)
            .subscribe(data => {
                if(data){
                    this.globalS.sToast('Success','User has been logged out')
                    this.getUsers();
                }
            })

    }
}