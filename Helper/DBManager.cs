﻿namespace AdamasV3.Helper
{
    public static class DbManager
    {
        public static string? DbName;
        public static string? ConnString;

        public static string GetDbConnectionString(string dbName, string connString)
        {
            return DbConnectionManager.GetConnectionString(dbName, connString);
        }
    }
}
