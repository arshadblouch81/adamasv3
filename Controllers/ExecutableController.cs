using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class ExecutableController : Controller
    {

        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;

        public ExecutableController(
            DatabaseContext context, 
            IConfiguration config,
            IGeneralSetting setting)
        {
            _context = context;
            GenSettings = setting;
        }


        [HttpPut("select-user")]
        public async Task<IActionResult> UpdateCallExeCall([FromBody] Executable user){
            using (var conn = _context.Database.GetDbConnection() as SqlConnection){
                using(SqlCommand cmd = new SqlCommand(@"UPDATE UserInfo SET CloudExeCall = @exec WHERE Name=@user ",(SqlConnection) conn))
                {
                    cmd.Parameters.AddWithValue("@exec", user.ExecutableName);
                    cmd.Parameters.AddWithValue("@user", user.User);

                    await conn.OpenAsync();

                    //return Ok(user);

                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

 


    }

    public class Executable {
        public string ExecutableName { get; set; }
        public string User { get; set; }
    }
}
