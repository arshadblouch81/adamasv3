namespace Adamas.Models.Modules{
    public class Branches {
        public int? RecordNumber { get; set; } = 0;
        public string PersonID { get; set; }
        public string Branch { get; set; }
        public string Notes { get; set; } 
    }
}