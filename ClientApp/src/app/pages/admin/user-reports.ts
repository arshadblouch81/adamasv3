import { Component } from "@angular/core";

import { NzModalService } from 'ng-zorro-antd/modal';
import { OnInit, OnDestroy, AfterViewInit,ChangeDetectorRef } from "@angular/core";
import { takeUntil } from 'rxjs/operators';
import { Router,ActivatedRoute, ParamMap } from '@angular/router';
import { forkJoin, Subject } from 'rxjs';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzFormatEmitEvent, NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, FormControlName, } from '@angular/forms';
import { parseJSON } from "date-fns";
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { GlobalService, ReportService,ListService,MenuService,PrintService, LoginService } from '@services/index';
import { concat, flatMapDeep, indexOf, size } from "lodash";
import eachDayOfInterval from "date-fns/esm/eachDayOfInterval/index";
import { stringify } from "querystring";
import { LoginComponent } from "../login/login.component";



const inputFormDefault = {
  
  
  btnid:[''],
  content:  [''],
  one:[[]],
  list: [[]],
  entity:[[]],
  condition:[[]],
  value:[[]],
  Endvalue:[[]],
  frm_SecondValue : false,
  frm_SecondVal : false,
  frm_stdlayout: false,
  frm_Customlayout: false,
  RecipRptBtn : false,
  StaffRptBtn: false,
  conditionentry:false,
  // = ['title','ASAD','key'] "IN",
  //  data : Array<any> = [{'title':'ASAD','key':'00'},{'title':'ASAD','key':'01'},{'title':'ASAD','key':'02'}]
  exportitemsArr:[[]],
  criteriaArr:  [["EQUALS", "BETWEEN", "LESS THAN", "GREATER THAN", "NOT EQUAL TO", "IS NOTHING", "IS ANYTHING", "IS TRUE", "IS FALSE"]],
  titleArr : [[]],
  Filterheck : true,
  //itemsArr: [[]],
  itemsArr: [''],
  valueArr:[''],
  SecondvalueArr:[''],
  SQLtxt:[''],
  RptTitle :[''],
  //valArr:[[]],
  data: [[]],
  activeonly:[true],
  incl_internalCostclient: [false],
  radiofiletr:['meet'],
  datarow: [[]],  
  
  drawerVisible:  false,  

  addresstypeArr: [[]],
  contacttypeArr: [[]],
  
  isVisibleprompt : false,
  isVisibleprompt_IN : false,
  isVisibleRptType : false,
  rptfieldname:0,
  RptFormat: [''],

  csvExport : [false],
  tblView: [true],
 
}

@Component({
  host: {
    
  },
  styles: [`
  .spinner{
    margin:1rem auto;
    width:1px;
  }
  .item-right{
    text-align: right;
    align-content: right;
    float:right;
  }
  .btn-orange{
    background: #f18805 !important;
}
.btn-Powderblue{
    background: #85b9d5 !important;
}
  .btn{    
    border:none;
    cursor:pointer;
    outline:none;
    transition: background-color 0.5s ease;
    padding: 5px 7px;
    border-radius: 3px;
    text-align: center !important;        
    width: 88px !important;            
    height: 33px;
    margin:6px;
    background-color: #D6D3D3;
  }
  .button{                   
    width: 100px !important;            
    height: 40px;                
    padding: 5px;
    margin : 5px;
    text-align: center;    
    font-size: 12px;
}
  .largebutton{  
     width: 200pt;    
    background: #f7f7f7;         
    margin:5px;
    text-align: center;
    align-content: center;
    float:center;
    }
  .item-left{
    text-align: left;
    align-content: left;
    float:left;
  }
  .item-center{
    align-content: center;
    text-align: center;
    vertical-align: center;    
  }
  .dialog{
    width: 100% !important;
    height: fit-content !important;
    margin: 5px !important;
    border: 0.5px solid #696c6d;
    padding: 5px !important;
}
  .colwidth{
    width: 200px;
  }
  .colwidthentity{
    width: 300px;
  }
  nz-select{
    width:100%;
  }
  .list_frame{
     
    margin: 5px; 
    margin: 5px;  
    overflow-y: scroll;
  }
  .criteria{
    border: 1px solid #dbdee2; 
    margin: 5px;
    height: 100px;
    overflow-y: scroll;
  }
  .list_frame td{
    width: 150px;
  }
  .margin{
    margin-left: 20px;
  }
  .tree_frame{
    height: 100vh;
    overflow-y: scroll;
  }
  .form-group label{
    font-weight: bold;
  }
  nz-layout[_ngcontent-dhj-c382] {
    background: #85B9D5;
    height: 100vh;
  }
    
.heading{
    font-weight: bold;
    line-height:16px;
    font-size:14px;
    padding: 5px;
    width: auto;
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    background-color:#F0F0F0 ;
    
}
.colheading{
    width: fit-content !important;
    font-weight: bold;
    line-height:16px;
    font-size:14px;
    padding: 5px;    
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    background-color:#F0F0F0 ;
}
.data{    
    font-size:12px;
    padding: 1px;
    width: fit-content;
    text-align: left;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.table{
    background-color: #FFFFFF;
}
.table tr:nth-child(odd){background-color: #E9F7FF;}

.table tr:hover {background-color: #85B9D5;}

  
  `],
  
  templateUrl: './user-reports.html'
})

export class UserReports implements OnInit, OnDestroy, AfterViewInit {
  frm_nodelist: boolean = true;
  frm_SecondValue :  boolean;
  frm_SecondVal :  boolean;
  frm_stdlayout:  boolean;
  frm_Customlayout:  boolean;
  RecipRptBtn :  boolean;
  StaffRptBtn:  boolean;
  conditionentry:  boolean;
  frm_delete : boolean = false;
  inputForm: FormGroup;
  btnid: string;
  content: string;
  one: Array<any>;
  list: Array<any>;
  entity:Array<any>;
  datarow:Array<any>;
  condition:Array<any>;
  value:Array<any>;
  titleArr :Array<any>;
  addresstypeArr:Array<any>;
  contacttypeArr:Array<any>;
  Filterheck : boolean = true;
  Endvalue :Array<any>; // = ['title','ASAD','key'] "IN",
  //data : Array<any> ;= [{'title':'ASAD','key':'00'},{'title':'ASAD','key':'01'},{'title':'ASAD','key':'02'}]
  exportitemsArr: Array<any>;
  criteriaArr: Array<any> = ["EQUALS", "BETWEEN", "LESS THAN", "GREATER THAN", "NOT EQUAL TO", "IS NOTHING", "IS ANYTHING", "IS TRUE", "IS FALSE"];
  UserRpt_IN_list :  Array<any>= [];
  Entity_IN :  Array<any>= [];
  populatedINArr :  Array<any>= [];
  UserRpt_IN : [false];
  bodystyleModal_IN :object;
  ModalTitle : string;
  isVisibleprompt_IN:boolean;
  isVisibleRptType:boolean;
  // itemsArr: Array<any>;
  // valArr: Array<any>;
  nodes: Array<any>;

  tableData: Array<any> = [];
  headings: Array<any> = [];
  dvalue: Array<any> = [];
  ModalName: string;
  isVisible = false;
  
  needQuerysetting : number=1;
  itemsArr: string;
  valueArr:string;
  SecondvalueArr:string;
  SQLtxt:string;
  RptTitle : string;
  
  ReportPreview :boolean;
  
  data: Array<any>;
  activeonly: boolean;
  incl_internalCostclient: boolean;
  
  StrType:string;
  
  id: string;
  tryDoctype: any;
  pdfTitle: string;
  drawerVisible: boolean ;
  Applybutton : boolean = true ;
  loading: boolean =  false;
  isVisibleprompt: boolean ;
  bodystyleModal_RptType:object;
  reportid: string;
  tocken :any;
  radiofilter:any;
  rptfieldname:number ;
  
  sqlWhere: string;
  sql: string;
  Saverptsql : string;     
  sqlselect: string;
  sqlcondition: string;
  Savesqlcondition :string;
  sqlorder: string;
  ConditionEntity: string;
  FieldsNo: number;
  includeConatctWhere:string;
  includeGoalcareWhere:string;
  includeReminderWhere:string;
  includeUserGroupWhere:string;
  includePrefrencesWhere:string;
  includeIncludSWhere:string;  
  includeExcludeSWhere:string;
  includeRecipientPensionWhere:string;
  includeLoanitemWhere :string;
  includeSvnDetailNotesWhere :string;
  includeSvcSpecCompetencyWhere :string;
  includeOPHistoryWhere :string;
  includeClinicHistoryWhere :string;
  includeRecipientCompetencyWhere :string;
  includeCareplanWhere :string;
  includeHRCaseStaffWhere :string;
  includeNotesWhere :string;

  includeStaffPositionWhere:string;
  includeStaffOPNotesWhere :string;
  includeStaffAttributesWhere : string;
  includeStaffHRHistoryWhere: string;
  
  //bodystyle:object;
  RptFormat :string ;
  
  
  IncludeFundingSource: boolean;  IncludeProgram: boolean;  IncludeStaffAttributes: boolean;  IncludePensions: boolean;  IncludeExcluded: boolean; IncludeIncluded: boolean;  IncludePreferences : boolean;
  IncludeGoals: boolean;  IncludeLoanItems: boolean;  IncludeContacts: boolean;  IncludeConsents : boolean; IncludeDocuments : boolean; IncludeUserGroups : boolean; IncludeReminders : boolean; IncludeStaffReminders : boolean; IncludeLeaves: boolean;
  IncludeCaseStaff: boolean;  IncludeCarePlans : boolean; IncludeServiceCompetencies : boolean; includeStaffIncidents : boolean; includeRecipIncidents : boolean; IncludeStaffUserGroups : boolean; IncludeStaffPreferences
  IncludeNursingDiagnosis: boolean;  IncludeMedicalDiagnosis : boolean; IncludeMedicalProcedure : boolean; IncludeAgreedServices : boolean; IncludePlacements: boolean;  IncludeCaseNotes : boolean;
  IncludeRecipientOPNotes: boolean;  IncludeRecipientClinicalNotes: boolean;  IncludeStaffOPNotes : boolean; IncludeStaffHRNotes : boolean; IncludeONI : boolean; IncludeONIMainIssues : boolean;
  IncludeONIOtherIssues : boolean; IncludeONICurrentServices: boolean;  IncludeONIActionPlan : boolean; IncludeONIMedications : boolean; IncludeONIHealthConditions : boolean; IncludeStaffPosition : boolean;
  IncludeDEX : boolean; IncludeCarerInfo : boolean; IncludeHACC : boolean; IncludeRecipBranches : boolean; includeHRRecipAttribute: boolean;  IncludeRecipCompetencies : boolean; IncludeStaffLoanItems : boolean;  IncludeCarePlan : boolean; IncludeGoalsAndStrategies : boolean; IncludeMentalHealth: boolean;
    
  groupedData: any[] = [];
  groupedAndSortedData: any[] = [];
  selectedGroupColumns: string[] = []; 

  sortColumns = [{ column: '', direction: 'ascend' }]; 

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    private GlobalS:GlobalService,
    private ReportS:ReportService,
    private ListS:ListService,
    private MenuS:MenuService,
    private printS: PrintService,
    private loginS: LoginService,
    private router: Router,
    private cd: ChangeDetectorRef,
    
    
    ) {
      
    }
    ngOnInit(): void {              
      this.inputForm = this.fb.group(inputFormDefault);              
      this.tocken = this.GlobalS.pickedMember ? this.GlobalS.GETPICKEDMEMBERDATA(this.GlobalS.GETPICKEDMEMBERDATA):this.GlobalS.decode();
      this.RptFormat = this.GlobalS.var2.toString();
      
      if(this.RptFormat == "AGENCYSTFLIST" || this.RptFormat == "USERSTFLIST"){
        this.StaffRptBtn = true;
        this.nodes = [
          { title: 'Name and Address', key: '056' },
          { title: 'Contacts & Next of Kin', key: '005' },
          { title: 'User Groups', key: '011' },
          { title: 'Preferences', key: '012' },
          { title: 'Loan Items', key: '035' },
          { title: 'Reminders', key: '010' },
          { title: 'Skills and Qualifications', key: '046' },      
          { title: 'Leaves', key: '047' },
          { title: 'General Info', key: '048' },
          { title: 'Staff Attributes', key: '049' },
          { title: 'Staff HR Notes', key: '050' },
          { title: 'Staff OP Notes', key: '051' },
          { title: 'Staff Incidents', key: '052' },      
          { title: 'Work Hours', key: '053' },
          { title: 'Staff Position', key: '054' },
          { title: 'Service Information Fields', key: '055' },
        ]
      }else{
        this.RecipRptBtn = true;
        this.nodes = [
          { title: 'Name and Address', key: '000' },
          { title: 'General Demographics', key: '001' },
          { title: 'Admin Information', key: '002' },
          { title: 'Staff', key: '003' },
          { title: 'Other Genral', key: '004' },
          { title: 'Contacts & Next of Kin', key: '005' },
          { title: 'Carer Info', key: '006' },
          { title: 'Documents', key: '007' },
          { title: 'Consents', key: '008' },
          { title: 'Goals of Care', key: '009' },
          { title: 'Reminders', key: '010' },
          { title: 'User Groups', key: '011' },
          { title: 'Preferences', key: '012' },
          { title: 'Fixed Review Dates', key: '013' },
          { title: 'Staffing Inclusion/Exclusion', key: '014' },
          { title: 'Agreed Funding Information', key: '015' },
          { title: 'Legacy Care Plan', key: '016' },
          { title: 'Agreed Service Information', key: '017' },
          { title: 'Clinical Information', key: '018' },
          { title: 'Billing Information', key: '019' },
          { title: 'Time Logging', key: '020' },
          { title: 'Notes', key: '21' },
          { title: 'Insurance and Pension', key: '022' },
          { title: 'HACCS Dataset Fields', key: '023' },
          { title: 'DEX', key: '24' },
          { title: 'CSTDA Dataset Fields', key: '025' },
          { title: 'NRCP Dataset Fields', key: '026' },
          { title: 'ONI Core', key: '027' },
          { title: 'ONI-Functional Profile', key: '028' },
          { title: 'ONI-Living Arrangements Profile', key: '029' },
          { title: 'ONI-Health Conditions Profile', key: '030' },
          { title: 'ONI-Psychosocial Profile', key: '031' },
          { title: 'ONI-Health Behaviours Profile', key: '032' },
          { title: 'ONI-Carer Profile', key: '033' },
          { title: 'ONI-Cultural Profile', key: '034' },
          { title: 'Loan Items', key: '035' },
          { title: 'Service Information Fields', key: '036' },
          { title: 'Service Specific Competencies', key: '037' },
          { title: 'Recipient OP Notes', key: '038' },
          { title: 'Recipient Clinical Notes', key: '039' },
          { title: 'Recipient Incidents', key: '040' },
          { title: 'Recipient Competencies', key: '041' },
          { title: 'Care Plan', key: '042' },
          { title: 'Mental Health', key: '043' },
          { title: 'Recipient Placements', key: '044' },
          { title: 'Quote Goals and Stratagies', key: '045' },
        ];
      }      
      if(this.RptFormat == "AGENCYLIST" || this.RptFormat == "AGENCYSTFLIST"){
        this.isVisibleRptType = true;
      }else{   
        this.frm_stdlayout = true;        
      }
      //this.isVisibleRptType = true; :Array<any>;
      
    }
    ngOnDestroy(): void {
      
    }
    ngAfterViewInit(): void {
      
      
    }
    
    ContentSetter(ekey) {        
      var s_ekey = (ekey.toString()).slice(-2)
      this.StrType = s_ekey;
      switch (s_ekey) {
        //NAME AND ADDRESS
        case "00":
        this.data = [ 
          { "title": "Full Name-Surname First", "key": "00", isLeaf: true },
          { "title": "Full Name-Mailing", "key": "01", isLeaf: true },
          { "title": "Title", "key": "02", isLeaf: true },
          { "title": "First Name", "key": "03", isLeaf: true },
          { "title": "Middle Names", "key": "04", isLeaf: true },
          { "title": "Surname/Organisation", "key": "05", isLeaf: true },
          { "title": "Preferred Name", "key": "06", isLeaf: true },
          { "title": "Other", "key": "07", isLeaf: true },
          { "title": "Address-Line1", "key": "08", isLeaf: true },
          { "title": "Address-Line2", "key": "09", isLeaf: true },
          { "title": "Address-Suburb", "key": "10", isLeaf: true },
          { "title": "Address-Postcode", "key": "11", isLeaf: true },
          { "title": "Address-State", "key": "12", isLeaf: true },

        ]
        break;
        //General Demographics
        case "01":
        this.data = [
          { "title": "Gender", "key": "01", isLeaf: true },
          { "title": "Date Of Birth", "key": "02", isLeaf: true },
          { "title": "Age", "key": "03", isLeaf: true },
          { "title": "Ageband-Statistical", "key": "04", isLeaf: true },
          { "title": "Ageband-5 Year", "key": "04", isLeaf: true },
          { "title": "Ageband-10 Year", "key": "05", isLeaf: true },
          { "title": "Age-ATSI Status", "key": "06", isLeaf: true },
          { "title": "Month Of Birth", "key": "07", isLeaf: true },
          { "title": "Day Of Birth No.", "key": "08", isLeaf: true },
          { "title": "CALD Score", "key": "09", isLeaf: true },
          { "title": "Country Of Birth", "key": "10", isLeaf: true },
          { "title": "Language", "key": "11", isLeaf: true },
          { "title": "Indigenous Status", "key": "12", isLeaf: true },
          { "title": "Primary Disability", "key": "13", isLeaf: true },
          { "title": "Financially Dependent", "key": "14", isLeaf: true },
          { "title": "Financial Status", "key": "15", isLeaf: true },
          { "title": "Occupation", "key": "16", isLeaf: true },
          
        ]
        break;
        //Admin Info
        case "02":
        this.data = [
          { "title": "UniqueID", "key": "00", isLeaf: true },
          { "title": "Code", "key": "01", isLeaf: true },
          { "title": "Type", "key": "02", isLeaf: true },
          { "title": "Category", "key": "03", isLeaf: true },
          { "title": "CoOrdinator", "key": "04", isLeaf: true },
          { "title": "Admitting Branch", "key": "05", isLeaf: true },
          { "title": "Secondary Branch", "key": "06", isLeaf: true },
          { "title": "File number", "key": "07", isLeaf: true },
          { "title": "File Number 2", "key": "08", isLeaf: true },
          { "title": "NDIA/MAC Number", "key": "09", isLeaf: true },
          { "title": "Last Activated Date", "key": "10", isLeaf: true },
          { "title": "Created By", "key": "11", isLeaf: true },
          //{ "title": "Other", "key": "12", isLeaf: true },
        ]
        break;
        //Staff 
        case "03":
        this.data = [
          { "title": "Staff Name", "key": "00", isLeaf: true },
          { "title": "Program Name", "key": "01", isLeaf: true },
          { "title": "Notes", "key": "02", isLeaf: true },
        ]
        break;
        //Other Genral info
        case "04": 
        this.data = [
          { "title": "OH&S Profile", "key": "00", isLeaf: true },
          { "title": "OLD WH&S Date", "key": "01", isLeaf: true },
          { "title": "Billing Profile", "key": "02", isLeaf: true },
          { "title": "Sub Category", "key": "03", isLeaf: true },
          { "title": "Roster Alerts", "key": "04", isLeaf: true },
          { "title": "Timesheet Alerts", "key": "05", isLeaf: true },
          { "title": "Contact Issues", "key": "06", isLeaf: true },
          { "title": "Survey Consent Given", "key": "07", isLeaf: true },
          { "title": "Copy Rosters Enabled", "key": "08", isLeaf: true },
          { "title": "Activation Date", "key": "09", isLeaf: true },
          { "title": "DeActivation Date", "key": "10", isLeaf: true },
          { "title": "Mobility", "key": "11", isLeaf: true },
          { "title": "Specific Competencies", "key": "12", isLeaf: true },
        ]
        break;
        //  Contacts & Next of Kin
        case "05":
        this.data = [
          { "title": "Contact Group", "key": "00", isLeaf: true },
          { "title": "Contact Type", "key": "01", isLeaf: true },
          { "title": "Contact Sub Type", "key": "02", isLeaf: true },
          { "title": "Contact User Flag", "key": "03", isLeaf: true },
          { "title": "Contact Person Type", "key": "04", isLeaf: true },
          { "title": "Contact Name", "key": "05", isLeaf: true },
          { "title": "Contact Address", "key": "06", isLeaf: true },
          { "title": "Contact Suburb", "key": "07", isLeaf: true },
          { "title": "Contact Postcode", "key": "08", isLeaf: true },
          { "title": "Contact Phone 1", "key": "09", isLeaf: true },
          { "title": "Contact Phone 2", "key": "10", isLeaf: true },
          { "title": "Contact Mobile", "key": "11", isLeaf: true },
          { "title": "Contact FAX", "key": "12", isLeaf: true },
          { "title": "Contact Email", "key": "13", isLeaf: true },
          
        ]
        break;      
        //Carer Info
        case "06":
        this.data = [
          { "title": "Carer First Name", "key": "00", isLeaf: true },
          { "title": "Carer Last Name", "key": "01", isLeaf: true },
          { "title": "Carer Age", "key": "02", isLeaf: true },
          { "title": "Carer Gender", "key": "03", isLeaf: true },
          { "title": "Carer Indigenous Status", "key": "04", isLeaf: true },
          { "title": "Carer Address", "key": "05", isLeaf: true },
          { "title": "Carer Email", "key": "06", isLeaf: true },
          { "title": "Carer Phone <Home>", "key": "07", isLeaf: true },
          { "title": "Carer Phone <Work>", "key": "08", isLeaf: true },
          { "title": "Carer Phone <Mobile>", "key": "09", isLeaf: true },
          
        ]
        break;          
        // Documents 
        case "07":
        this.data = [
          { "title": "DOC_ID", "key": "00", isLeaf: true },
          { "title": "Doc_Title", "key": "01", isLeaf: true },
          { "title": "Created", "key": "02", isLeaf: true },
          { "title": "Modified", "key": "03", isLeaf: true },
          { "title": "Status", "key": "04", isLeaf: true },
          { "title": "Classification", "key": "05", isLeaf: true },
          { "title": "Category", "key": "06", isLeaf: true },
          { "title": "Filename", "key": "07", isLeaf: true },
          { "title": "Doc#", "key": "08", isLeaf: true },
          { "title": "DocStartDate", "key": "09", isLeaf: true },
          { "title": "DocEndDate", "key": "10", isLeaf: true },
          { "title": "AlarmDate", "key": "11", isLeaf: true },
          { "title": "AlarmText", "key": "12", isLeaf: true },
        ]
        break;        
        //Consents
        case "08":
        this.data = [
          { "title": "Consent", "key": "00", isLeaf: true },
          { "title": "Consent Start Date", "key": "01", isLeaf: true },
          { "title": "Consent Expiry", "key": "02", isLeaf: true },
          { "title": "Consent Notes", "key": "03", isLeaf: true },
        ]
        break;
        //  GOALS OF CARE
        case "09":
        this.data = [
          { "title": "Goal", "key": "00", isLeaf: true },
          { "title": "Goal Detail", "key": "01", isLeaf: true },
          { "title": "Goal Achieved", "key": "02", isLeaf: true },
          { "title": "Anticipated Achievement Date", "key": "03", isLeaf: true },
          { "title": "Date Achieved", "key": "04", isLeaf: true },
          { "title": "Last Reviewed", "key": "05", isLeaf: true },
          { "title": "Logged By", "key": "06", isLeaf: true },
        ]
        break;        
        //REMINDERS
        case "10":
        this.data = [
          { "title": "Reminder Detail", "key": "00", isLeaf: true },
          { "title": "Event Date", "key": "01", isLeaf: true },
          { "title": "Reminder Date", "key": "02", isLeaf: true },
          { "title": "Reminder Notes", "key": "03", isLeaf: true },
        ]
        break;
        // USER GROUPS
        case "11":
        this.data = [
          
          { "title": "Group Name", "key": "00", isLeaf: true },
          { "title": "Group Note", "key": "01", isLeaf: true },
        ]
        break;
        //Preferences
        case "12":
        this.data = [
          { "title": "Preference Name", "key": "00", isLeaf: true },
          { "title": "Preference Note", "key": "01", isLeaf: true },
        ]
        break;        
        // FIXED REVIEW DATES
        case "13":
        this.data = [
          { "title": "Review Date 1", "key": "00", isLeaf: true },
          { "title": "Review Date 2", "key": "01", isLeaf: true },
          { "title": "Review Date 3", "key": "02", isLeaf: true },
        ]
        break;
        //Staffing Inclusions/Exclusions
        case "14":
        this.data = [
          { "title": "Excluded Staff", "key": "00", isLeaf: true },
          { "title": "Excluded_Staff Notes", "key": "01", isLeaf: true },
          { "title": "Included Staff", "key": "02", isLeaf: true },
          { "title": "Included_Staff Notes", "key": "03", isLeaf: true },
        ]
        break; 
        // AGREED FUNDING INFORMATION
        case "15":
        this.data = [
          { "title": "Funding Source", "key": "00", isLeaf: true },
          { "title": "Funded Program", "key": "01", isLeaf: true },
          { "title": "Funded Program Agency ID", "key": "02", isLeaf: true },
          { "title": "Program Status", "key": "03", isLeaf: true },
          { "title": "Program Coordinator", "key": "04", isLeaf: true },
          { "title": "Funding Start Date", "key": "05", isLeaf: true },
          { "title": "Funding End Date", "key": "06", isLeaf: true },
          { "title": "AutoRenew", "key": "07", isLeaf: true },
          { "title": "Rollover Remainder", "key": "08", isLeaf: true },
          { "title": "Funded Qty", "key": "09", isLeaf: true },
          { "title": "Funded Type", "key": "10", isLeaf: true },
          { "title": "Funding Cycle", "key": "11", isLeaf: true },
          { "title": "Funded Total Allocation", "key": "02", isLeaf: true },
          { "title": "Used", "key": "13", isLeaf: true },
          { "title": "Remaining", "key": "14", isLeaf: true },
        ]
        break;
        //LEGACY CARE PLAN
        case "16":
        this.data = [
          { "title": "Name", "key": "00", isLeaf: true },
          { "title": "Start Date", "key": "01", isLeaf: true },
          { "title": "End Date", "key": "02", isLeaf: true },
          { "title": "Details", "key": "03", isLeaf: true },
          { "title": "Reminder Date", "key": "04", isLeaf: true },
          { "title": "Reminder Text", "key": "05", isLeaf: true },
        ]
        break;
        //Agreed Service Information
        case "17":
        this.data = [
          { "title": "Agreed Service Code", "key": "00", isLeaf: true },
          { "title": "Agreed Program", "key": "01", isLeaf: true },
          { "title": "Agreed Service Agency ID", "key": "02", isLeaf: true },
          { "title": "Agreed Service Status", "key": "03", isLeaf: true },
          { "title": "Agreed Service Duration", "key": "04", isLeaf: true },
          { "title": "Agreed Service Frequency", "key": "05", isLeaf: true },
          { "title": "Agreed Service Cost Type", "key": "06", isLeaf: true },
          { "title": "Agreed Service Unit Cost", "key": "07", isLeaf: true },
          { "title": "Agreed Service Billing Unit", "key": "08", isLeaf: true },
          { "title": "Agreed Service Billing Rate", "key": "09", isLeaf: true },
          { "title": "Agreed Service Debtor", "key": "10", isLeaf: true },
        ]
        break;
        //  CLINICAL INFORMATION
        case "18":
        this.data = [
          
          { "title": "Nursing Diagnosis", "key": "00", isLeaf: true },
          { "title": "Medical Diagnosis", "key": "01", isLeaf: true },
          { "title": "Medical Procedure", "key": "02", isLeaf: true },
        ]
        break;
        //BILLING INFORMATION
        case "19":
        this.data = [
          { "title": "Billing Client", "key": "00", isLeaf: true },
          { "title": "Billing Cycle", "key": "01", isLeaf: true },
          { "title": "Billing Rate", "key": "02", isLeaf: true },
          { "title": "Billing %", "key": "03", isLeaf: true },
          { "title": "Billing Amount", "key": "04", isLeaf: true },
          { "title": "Account Identifier", "key": "05", isLeaf: true }, 
          { "title": "External Order Number", "key": "06", isLeaf: true },
          { "title": "Financially Dependent", "key": "07", isLeaf: true },
          { "title": "BPay Reference", "key": "08", isLeaf: true },
          { "title": "NDIA/MAC ID", "key": "09", isLeaf: true },
          { "title": "Bill Profile", "key": "10", isLeaf: true },
          { "title": "Allow Different Biller", "key": "11", isLeaf: true },
          { "title": "Capped Bill", "key": "12", isLeaf: true },
          { "title": "Hide Fare on Transport Runsheet", "key": "13", isLeaf: true },
          { "title": "Print Invoice", "key": "14", isLeaf: true },
          { "title": "Email Invoice", "key": "15", isLeaf: true },
          { "title": "Direct Debit", "key": "16", isLeaf: true },
          { "title": "DVA CoBiller", "key": "17", isLeaf: true },						
        ]
        break;
        //PANZTEL Timezone
        case "20":
        this.data = [
          { "title": "PANZTEL PBX Site", "key": "00", isLeaf: true },
          { "title": "PANZTEL Parent Site", "key": "01", isLeaf: true },
          { "title": "DAELIBS Logger ID", "key": "02", isLeaf: true },
        ]
        break;
        //NOTES
        case "21":
        this.data = [
          
          { "title": "General Notes", "key": "00", isLeaf: true },
          { "title": "Case Notes Date", "key": "01", isLeaf: true },
          { "title": "Case Notes Detail", "key": "02", isLeaf: true },
          { "title": "Case Notes Creator", "key": "03", isLeaf: true },
          { "title": "Case Notes Alarm", "key": "04", isLeaf: true },
          { "title": "Case Notes Program", "key": "05", isLeaf: true }, 
          { "title": "Case Notes Category", "key": "06", isLeaf: true },	
        ]
        break;
        //INSURANCE AND PENSION
        case "22":
        this.data = [
          { "title": "Medicare Number", "key": "00", isLeaf: true },
          { "title": "Medicare Recipient ID", "key": "01", isLeaf: true },
          { "title": "Pension Status", "key": "02", isLeaf: true },
          { "title": "Unable to Determine Pension Status", "key": "03", isLeaf: true },
          { "title": "Concession Number", "key": "04", isLeaf: true },
          { "title": "DVA Benefits Flag", "key": "05", isLeaf: true },
          { "title": "DVA Number", "key": "06", isLeaf: true },
          { "title": "DVA Card Holder Status", "key": "07", isLeaf: true },
          { "title": "Ambulance Subscriber", "key": "08", isLeaf: true },
          { "title": "Ambulance Type", "key": "09", isLeaf: true },
          { "title": "Pension Name", "key": "10", isLeaf: true },
          { "title": "Pension Number", "key": "11", isLeaf: true },
          { "title": "Will Available", "key": "12", isLeaf: true },
          { "title": "Will Location", "key": "13", isLeaf: true },
          { "title": "Funeral Arrangements", "key": "14", isLeaf: true },
          { "title": "Date Of Death", "key": "15", isLeaf: true },
        ]
        break;       
        //HACCS DATSET FIELDS
        case "23":
        this.data = [
          { "title": "HACC-SLK", "key": "00", isLeaf: true },
          { "title": "HACC-First Name", "key": "01", isLeaf: true },
          { "title": "HACC-Surname", "key": "02", isLeaf: true },
          { "title": "HACC-Referral Source", "key": "03", isLeaf: true },
          { "title": "HACC-Date Of Birth", "key": "04", isLeaf: true },
          { "title": "HACC-Date Of Birth Estimated", "key": "05", isLeaf: true },
          { "title": "HACC-Gender", "key": "06", isLeaf: true },
          { "title": "HACC-Area Of Residence", "key": "07", isLeaf: true },
          { "title": "HACC-Country Of Birth", "key": "08", isLeaf: true },
          { "title": "HACC-Preferred Language", "key": "09", isLeaf: true },
          { "title": "HACC-Indigenous Status", "key": "10", isLeaf: true },
          { "title": "HACC-Living Arrangements", "key": "11", isLeaf: true },
          { "title": "HACC-Dwelling/Accomodation", "key": "12", isLeaf: true },
          { "title": "HACC-Main Reasons For Cessation", "key": "13", isLeaf: true },
          { "title": "HACC-Pension Status", "key": "14", isLeaf: true },
          { "title": "HACC-Primary Carer", "key": "15", isLeaf: true },
          { "title": "HACC-Carer Availability", "key": "16", isLeaf: true },
          { "title": "HACC-Carer Residency", "key": "17", isLeaf: true },
          { "title": "HACC-Carer Relationship", "key": "18", isLeaf: true },
          { "title": "HACC-Exclude From Collection", "key": "19", isLeaf: true },
          { "title": "HACC-Housework", "key": "20", isLeaf: true },
          { "title": "HACC-Transport", "key": "21", isLeaf: true },
          { "title": "HACC-Shopping", "key": "22", isLeaf: true },
          { "title": "HACC-Medication", "key": "23", isLeaf: true },
          { "title": "HACC-Money", "key": "24", isLeaf: true },
          { "title": "HACC-Walking", "key": "25", isLeaf: true },
          { "title": "HACC-Bathing", "key": "26", isLeaf: true },
          { "title": "HACC-Memory", "key": "27", isLeaf: true },
          { "title": "HACC-Behaviour", "key": "28", isLeaf: true },
          { "title": "HACC-Communication", "key": "29", isLeaf: true },          
          { "title": "HACC-Eating", "key": "30", isLeaf: true },
          { "title": "HACC-Toileting", "key": "31", isLeaf: true },
          { "title": "HACC-GetUp", "key": "32", isLeaf: true },
          { "title": "HACC-Carer More Than One", "key": "33", isLeaf: true },
        ]
        break;
        //"DEX"
        case "24":
        this.data = [
          { "title": "DEX-Exclude From MDS", "key": "00", isLeaf: true },
          { "title": "DEX-Referral Purpose", "key": "01", isLeaf: true },
          { "title": "DEX-Referral Source", "key": "02", isLeaf: true },
          { "title": "DEX-Referral Type", "key": "03", isLeaf: true },
          { "title": "DEX-Reason For Assistance", "key": "04", isLeaf: true },
          { "title": "DEX-Consent To Provide Information", "key": "05", isLeaf: true },
          { "title": "DEX-Consent For Future Contact", "key": "06", isLeaf: true },
          { "title": "DEX-Sex", "key": "07", isLeaf: true },
          { "title": "DEX-Date Of Birth", "key": "08", isLeaf: true },
          { "title": "DEX-Estimated Birth Date", "key": "09", isLeaf: true },
          { "title": "DEX-Indigenous Status", "key": "10", isLeaf: true },
          { "title": "DEX-DVA Card Holder Status", "key": "11", isLeaf: true },
          { "title": "DEX-Has Disabilities", "key": "12", isLeaf: true },
          { "title": "DEX-Has A Carer", "key": "13", isLeaf: true },
          { "title": "DEX-Country of Birth", "key": "14", isLeaf: true },
          { "title": "DEX-First Arrival Year", "key": "15", isLeaf: true },
          { "title": "DEX-First Arrival Month", "key": "16", isLeaf: true },
          { "title": "DEX-Visa Code", "key": "17", isLeaf: true },
          { "title": "DEX-Ancestry", "key": "18", isLeaf: true },
          { "title": "DEX-Main Language At Home", "key": "19", isLeaf: true },
          { "title": "DEX-Accomodation Setting", "key": "20", isLeaf: true },
          { "title": "DEX-Is Homeless", "key": "21", isLeaf: true },
          { "title": "DEX-Household Composition", "key": "22", isLeaf: true },
          { "title": "DEX-Main Source Of Income", "key": "23", isLeaf: true },
          { "title": "DEX-Income Frequency", "key": "24", isLeaf: true },
          { "title": "DEX-Income Amount", "key": "25", isLeaf: true },
        ]
        break;
        // CSTDA Dataset Fields
        case "25":
        this.data = [
          { "title": "CSTDA-Date Of Birth", "key": "00", isLeaf: true },
          { "title": "CSTDA-Gender", "key": "01", isLeaf: true },
          { "title": "CSTDA-DISQIS ID", "key": "02", isLeaf: true },
          { "title": "CSTDA-Indigenous Status", "key": "03", isLeaf: true },
          { "title": "CSTDA-Country Of Birth", "key": "04", isLeaf: true },
          { "title": "CSTDA-Interpreter Required", "key": "05", isLeaf: true },
          { "title": "CSTDA-Communication Method", "key": "06", isLeaf: true },
          { "title": "CSTDA-Living Arrangements", "key": "07", isLeaf: true },
          { "title": "CSTDA-Suburb", "key": "08", isLeaf: true },
          { "title": "CSTDA-Postcode", "key": "09", isLeaf: true },
          { "title": "CSTDA-State", "key": "10", isLeaf: true },
          { "title": "CSTDA-Residential Setting", "key": "11", isLeaf: true },
          { "title": "CSTDA-Primary Disability Group", "key": "12", isLeaf: true },
          { "title": "CSTDA-Primary Disability Description", "key": "13", isLeaf: true },
          { "title": "CSTDA-Intellectual Disability", "key": "14", isLeaf: true },
          { "title": "CSTDA-Specific Learning ADD Disability", "key": "15", isLeaf: true },
          { "title": "CSTDA-Autism Disability", "key": "16", isLeaf: true },
          { "title": "CSTDA-Physical Disability", "key": "17", isLeaf: true },
          { "title": "CSTDA-Acquired Brain Injury Disability", "key": "18", isLeaf: true },
          { "title": "CSTDA-Neurological Disability", "key": "19", isLeaf: true },
          { "title": "CSTDA-Psychiatric Disability", "key": "20", isLeaf: true },
          { "title": "CSTDA-Other Psychiatric Disability", "key": "21", isLeaf: true },
          { "title": "CSTDA-Vision Disability", "key": "22", isLeaf: true },
          { "title": "CSTDA-Hearing Disability", "key": "23", isLeaf: true },
          { "title": "CSTDA-Speech Disability", "key": "24", isLeaf: true },
          { "title": "CSTDA-Developmental Delay Disability", "key": "25", isLeaf: true },
          { "title": "CSTDA-Disability Likely To Be Permanent", "key": "26", isLeaf: true },
          { "title": "CSTDA-Support Needs-Self Care", "key": "27", isLeaf: true },
          { "title": "CSTDA-Support Needs-Mobility", "key": "28", isLeaf: true },
          { "title": "CSTDA-Support Needs-Communication", "key": "29", isLeaf: true },
          { "title": "CSTDA-Support Needs-Interpersonal", "key": "30", isLeaf: true },
          { "title": "CSTDA-Support Needs-Learning", "key": "31", isLeaf: true },
          { "title": "CSTDA-Support Needs-Education", "key": "32", isLeaf: true },
          { "title": "CSTDA-Support Needs-Community", "key": "33", isLeaf: true },
          { "title": "CSTDA-Support Needs-Domestic", "key": "34", isLeaf: true },
          { "title": "CSTDA-Support Needs-Working", "key": "35", isLeaf: true },
          { "title": "CSTDA-Carer-Existence Of Informal", "key": "36", isLeaf: true },
          { "title": "CSTDA-Carer-Assists client in ADL", "key": "37", isLeaf: true },
          { "title": "CSTDA-Carer-Lives In Same Household", "key": "38", isLeaf: true },
          { "title": "CSTDA-Carer-Relationship", "key": "39", isLeaf: true },
          { "title": "CSTDA-Carer-Age Group", "key": "40", isLeaf: true },
          { "title": "CSTDA-Carer Allowance to Guardians", "key": "41", isLeaf: true },
          { "title": "CSTDA-Labor Force Status", "key": "42", isLeaf: true },
          { "title": "CSTDA-Main Source Of Income", "key": "43", isLeaf: true },
          { "title": "CSTDA-Current Individual Funding", "key": "44", isLeaf: true },
        ]
        break;
        //NRCP Dataset Fields
        case "26":
        this.data = [
          { "title": "NRCP-First Name", "key": "00", isLeaf: true },
          { "title": "NRCP-Surname", "key": "01", isLeaf: true },
          { "title": "NRCP-Date Of Birth", "key": "02", isLeaf: true },
          { "title": "NRCP-Gender", "key": "03", isLeaf: true },
          { "title": "NRCP-Suburb", "key": "04", isLeaf: true },
          { "title": "NRCP-Country Of Birth", "key": "05", isLeaf: true },
          { "title": "NRCP-Preferred Language", "key": "06", isLeaf: true },
          { "title": "NRCP-Indigenous Status", "key": "07", isLeaf: true },
          { "title": "NRCP-Marital Status", "key": "08", isLeaf: true },
          { "title": "NRCP-DVA Card Holder Status", "key": "09", isLeaf: true },
          { "title": "NRCP-Paid Employment Participation", "key": "10", isLeaf: true },
          { "title": "NRCP-Pension Status", "key": "11", isLeaf: true },
          { "title": "NRCP-Carer-Date Role Commenced", "key": "12", isLeaf: true },
          { "title": "NRCP-Carer-Role", "key": "13", isLeaf: true },
          { "title": "NRCP-Carer-Need", "key": "14", isLeaf: true },
          { "title": "NRCP-Carer-Number of Recipients", "key": "15", isLeaf: true },
          { "title": "NRCP-Carer-Time Spent Caring", "key": "16", isLeaf: true },
          { "title": "NRCP-Carer-Current Use Formal Services", "key": "17", isLeaf: true },
          { "title": "NRCP-Carer-Informal Support", "key": "18", isLeaf: true },
          { "title": "NRCP-Recipient-Challenging Behaviour", "key": "19", isLeaf: true },
          { "title": "NRCP-Recipient-Primary Disability", "key": "20", isLeaf: true },
          { "title": "NRCP-Recipient-Primary Care Needs", "key": "21", isLeaf: true },
          { "title": "NRCP-Recipient-Level of Need", "key": "22", isLeaf: true },
          { "title": "NRCP-Recipient-Primary Carer", "key": "23", isLeaf: true },
          { "title": "NRCP-Recipient-Carer Relationship", "key": "24", isLeaf: true },
          { "title": "NRCP-Recipient-Carer Co-Resident", "key": "25", isLeaf: true },
          { "title": "NRCP-Recipient-Dementia", "key": "26", isLeaf: true },
          { "title": "NRCP-CALD Background", "key": "27", isLeaf: true },
          
        ]
        break;
        case "27":
        // "ONI-Core"
        this.data = [
          { "title": "ONI-Family Name", "key": "00", isLeaf: true },
          { "title": "ONI-Title", "key": "01", isLeaf: true },
          { "title": "ONI-First Name", "key": "02", isLeaf: true },
          { "title": "ONI-Other", "key": "03", isLeaf: true },
          { "title": "ONI-Sex", "key": "04", isLeaf: true },
          { "title": "ONI-DOB", "key": "05", isLeaf: true },
          { "title": "ONI-Usual Address-Street", "key": "06", isLeaf: true },
          { "title": "ONI-Usual Address-Suburb", "key": "07", isLeaf: true },
          { "title": "ONI-Usual Address-Postcode", "key": "08", isLeaf: true },
          { "title": "ONI-Contact Address-Street", "key": "09", isLeaf: true },
          { "title": "ONI-Contact Address-Suburb", "key": "10", isLeaf: true },
          { "title": "ONI-Contact Address-Postcode", "key": "11", isLeaf: true },         
          { "title": "ONI-Phone-Home", "key": "12", isLeaf: true },
          { "title": "ONI-Phone-Work", "key": "13", isLeaf: true },
          { "title": "ONI-Phone-Mobile", "key": "14", isLeaf: true },
          { "title": "ONI-Phone-FAX", "key": "15", isLeaf: true },
          { "title": "ONI-EMAIL", "key": "16", isLeaf: true },
          { "title": "ONI-Person 1 Name", "key": "17", isLeaf: true },
          { "title": "ONI-Person 1 Street", "key": "18", isLeaf: true },
          { "title": "ONI-Person 1 Suburb", "key": "19", isLeaf: true },
          { "title": "ONI-Person 1 Postcode", "key": "20", isLeaf: true },
          { "title": "ONI-Person 1 Phone", "key": "21", isLeaf: true },
          { "title": "ONI-Person 1 Relationship", "key": "22", isLeaf: true },
          { "title": "ONI-Person 2 Name", "key": "23", isLeaf: true },
          { "title": "ONI-Person 2 Street", "key": "24", isLeaf: true },
          { "title": "ONI-Person 2 Suburb", "key": "25", isLeaf: true },
          { "title": "ONI-Person 2 Postcode", "key": "26", isLeaf: true },
          { "title": "ONI-Person 2 Phone", "key": "27", isLeaf: true },
          { "title": "ONI-Person 2 Relationship", "key": "28", isLeaf: true },
          { "title": "ONI-Doctor Name", "key": "29", isLeaf: true },
          { "title": "ONI-Doctor Street", "key": "30", isLeaf: true },
          { "title": "ONI-Doctor Suburb", "key": "31", isLeaf: true },
          { "title": "ONI-Doctor Postcode", "key": "32", isLeaf: true },
          { "title": "ONI-Doctor Phone", "key": "33", isLeaf: true },
          { "title": "ONI-Doctor FAX", "key": "34", isLeaf: true },
          { "title": "ONI-Doctor EMAIL", "key": "35", isLeaf: true },
          { "title": "ONI-Referral Source", "key": "36", isLeaf: true },
          { "title": "ONI-Contact Details", "key": "37", isLeaf: true },
          { "title": "ONI-Country Of Birth", "key": "38", isLeaf: true },
          { "title": "ONI-Indigenous Status", "key": "39", isLeaf: true },
          { "title": "ONI-Main Language At Home", "key": "40", isLeaf: true },
          { "title": "ONI-Interpreter Required", "key": "41", isLeaf: true },
          { "title": "ONI-Preferred Language", "key": "42", isLeaf: true },
          { "title": "ONI-Govt Pension Status", "key": "43", isLeaf: true },
          { "title": "ONI-Pension Benefit Card", "key": "44", isLeaf: true },
          { "title": "ONI-Medicare Number", "key": "45", isLeaf: true },
          { "title": "ONI-Health Care Card#", "key": "46", isLeaf: true },
          { "title": "ONI-DVA Cardholder Status", "key": "47", isLeaf: true },
          { "title": "ONI-DVA Number", "key": "48", isLeaf: true },
          { "title": "ONI-Insurance Status", "key": "49", isLeaf: true },
          { "title": "ONI-Health Insurer", "key": "50", isLeaf: true },                 
          { "title": "ONI-Health Insurance Card#", "key": "51", isLeaf: true },
          { "title": "ONI-Alerts", "key": "52", isLeaf: true },
          { "title": "ONI-Rating", "key": "53", isLeaf: true },
          { "title": "ONI-HACC Eligible", "key": "54", isLeaf: true },
          { "title": "ONI-Reason For HACC Status", "key": "55", isLeaf: true },
          { "title": "ONI-Other Support Eligibility", "key": "56", isLeaf: true },
          { "title": "ONI-Other Support Detail", "key": "57", isLeaf: true },
          { "title": "ONI-Functional Profile Complete", "key": "58", isLeaf: true },
          { "title": "ONI-Functional Profile Score 1", "key": "59", isLeaf: true },
          { "title": "ONI-Functional Profile Score 2", "key": "60", isLeaf: true },
          { "title": "ONI-Functional Profile Score 3", "key": "51", isLeaf: true },
          { "title": "ONI-Functional Profile Score 4", "key": "62", isLeaf: true },
          { "title": "ONI-Functional Profile Score 5", "key": "63", isLeaf: true },
          { "title": "ONI-Functional Profile Score 6", "key": "64", isLeaf: true },
          { "title": "ONI-Functional Profile Score 7", "key": "65", isLeaf: true },
          { "title": "ONI-Functional Profile Score 8", "key": "66", isLeaf: true },
          { "title": "ONI-Functional Profile Score 9", "key": "67", isLeaf: true },
          { "title": "ONI-Main Problem-Description", "key": "68", isLeaf: true },
          { "title": "ONI-Main Problem-Action", "key": "69", isLeaf: true },
          { "title": "ONI-Other Problem-Description", "key": "70", isLeaf: true },
          { "title": "ONI-Other Problem-Action", "key": "71", isLeaf: true },
          { "title": "ONI-Current Service", "key": "72", isLeaf: true },
          { "title": "ONI-Service Contact Details", "key": "73", isLeaf: true },
          { "title": "ONI-AP-Agency", "key": "74", isLeaf: true },
          { "title": "ONI-AP-For", "key": "75", isLeaf: true },
          { "title": "ONI-AP-Consent", "key": "76", isLeaf: true },
          { "title": "ONI-AP-Referral", "key": "77", isLeaf: true },
          { "title": "ONI-AP-Transport", "key": "78", isLeaf: true },
          { "title": "ONI-AP-Feedback", "key": "79", isLeaf: true },
          { "title": "ONI-AP-Date", "key": "80", isLeaf: true },
          { "title": "ONI-AP-Review", "key": "81", isLeaf: true },
        ]
        break;
        //  ONI-Functional Profile
        case "28":
        this.data = [
          { "title": "ONI-FPQ1-Housework", "key": "00", isLeaf: true },
          { "title": "ONI-FPQ2-GetToPlaces", "key": "01", isLeaf: true },
          { "title": "ONI-FPQ3-Shopping", "key": "02", isLeaf: true },
          { "title": "ONI-FPQ4-Medicine", "key": "03", isLeaf: true },
          { "title": "ONI-FPQ5-Money", "key": "04", isLeaf: true },
          { "title": "ONI-FPQ6-Walk", "key": "05", isLeaf: true },
          { "title": "ONI-FPQ7-Bath", "key": "06", isLeaf: true },
          { "title": "ONI-FPQ8-Memory", "key": "07", isLeaf: true },
          { "title": "ONI-FPQ9-Behaviour", "key": "08", isLeaf: true },
          { "title": "ONI-FP-Recommend Domestic", "key": "09", isLeaf: true },
          { "title": "ONI-FP-Recommend Self Care", "key": "10", isLeaf: true },
          { "title": "ONI-FP-Recommend Cognition", "key": "11", isLeaf: true },
          { "title": "ONI-FP-Recommend Behaviour", "key": "12", isLeaf: true },
          { "title": "ONI-FP-Has Self Care Aids", "key": "13", isLeaf: true },
          { "title": "ONI-FP-Has Support/Mobility Aids", "key": "14", isLeaf: true },
          { "title": "ONI-FP-Has Communication Aids", "key": "15", isLeaf: true },
          { "title": "ONI-FP-Has Car Mods", "key": "16", isLeaf: true },
          { "title": "ONI-FP-Has Other Aids", "key": "17", isLeaf: true },
          { "title": "ONI-FP-Other Goods List", "key": "18", isLeaf: true },
          { "title": "ONI-FP-Comments", "key": "19", isLeaf: true },
          
        ]
        break;
        //  ONI-Living Arrangements Profile
        case "29":
        this.data = [
          { "title": "ONI-LA-Living Arrangements", "key": "00", isLeaf: true },
          { "title": "ONI-LA-Living Arrangements Comments", "key": "01", isLeaf: true },
          { "title": "ONI-LA-Accomodation", "key": "02", isLeaf: true },
          { "title": "ONI-LA-Accomodation Comments", "key": "03", isLeaf: true },
          { "title": "ONI-LA-Employment Status", "key": "04", isLeaf: true },
          { "title": "ONI-LA-Employment Status Comments", "key": "05", isLeaf: true },
          { "title": "ONI-LA-Mental Health Act Status", "key": "06", isLeaf: true },
          { "title": "ONI-LA-Decision Making Responsibility", "key": "07", isLeaf: true },
          { "title": "ONI-LA-Capable Own Decisions", "key": "08", isLeaf: true },
          { "title": "ONI-LA-Financial Decisions", "key": "09", isLeaf: true },
          { "title": "ONI-LA-Cost Of Living Trade Off", "key": "10", isLeaf: true },
          { "title": "ONI-LA-Financial & Legal Comments", "key": "11", isLeaf: true },
          
        ]
        break;
        // ONI-Health Conditions Profile
        case "30":
        this.data = [
          { "title": "ONI-HC-Overall Health Description", "key": "00", isLeaf: true },
          { "title": "ONI-HC-Overall Health Pain", "key": "01", isLeaf: true },
          { "title": "ONI-HC-Overall Health Interference", "key": "02", isLeaf: true },
          { "title": "ONI-HC-Vision Reading", "key": "03", isLeaf: true },
          { "title": "ONI-HC-Vision Distance", "key": "04", isLeaf: true },
          { "title": "ONI-HC-Hearing", "key": "05", isLeaf: true },
          { "title": "ONI-HC-Oral Problems", "key": "06", isLeaf: true },
          { "title": "ONI-HC-Oral Comments", "key": "07", isLeaf: true },
          { "title": "ONI-HC-Speech/Swallow Problems", "key": "08", isLeaf: true },
          { "title": "ONI-HC-Speech/Swallow Comments", "key": "09", isLeaf: true },
          { "title": "ONI-HC-Falls Problems", "key": "10", isLeaf: true },
          { "title": "ONI-HC-Falls Comments", "key": "11", isLeaf: true },
          { "title": "ONI-HC-Feet Problems", "key": "12", isLeaf: true },
          { "title": "ONI-HC-Feet Comments", "key": "13", isLeaf: true },
          { "title": "ONI-HC-Vacc. Influenza", "key": "14", isLeaf: true },
          { "title": "ONI-HC-Vacc. Influenza Date", "key": "15", isLeaf: true },
          { "title": "ONI-HC-Vacc. Pneumococcus", "key": "16", isLeaf: true },
          { "title": "ONI-HC-Vacc. Pneumococcus  Date", "key": "17", isLeaf: true },
          { "title": "ONI-HC-Vacc. Tetanus", "key": "18", isLeaf: true },
          { "title": "ONI-HC-Vacc. Tetanus Date", "key": "19", isLeaf: true },
          { "title": "ONI-HC-Vacc. Other", "key": "20", isLeaf: true },
          { "title": "ONI-HC-Vacc. Other Date", "key": "21", isLeaf: true },
          { "title": "ONI-HC-Driving MV", "key": "22", isLeaf: true },
          { "title": "ONI-HC-Driving Fit", "key": "23", isLeaf: true },
          { "title": "ONI-HC-Driving Comments", "key": "24", isLeaf: true },
          { "title": "ONI-HC-Continence Urinary", "key": "25", isLeaf: true },
          { "title": "ONI-HC-Urinary Related To Coughing", "key": "26", isLeaf: true },
          { "title": "ONI-HC-Urinary Related To Coughing", "key": "27", isLeaf: true },
          { "title": "ONI-HC-Continence Comments", "key": "28", isLeaf: true },
          { "title": "ONI-HC-Weight", "key": "29", isLeaf: true },
          { "title": "ONI-HC-Height", "key": "30", isLeaf: true },
          { "title": "ONI-HC-BMI", "key": "31", isLeaf: true },
          { "title": "ONI-HC-BP Systolic", "key": "32", isLeaf: true },
          { "title": "ONI-HC-BP Diastolic", "key": "33", isLeaf: true },
          { "title": "ONI-HC-Pulse Rate", "key": "34", isLeaf: true },
          { "title": "ONI-HC-Pulse Regularity", "key": "35", isLeaf: true },
          { "title": "ONI-HC-Check Postural Hypotension", "key": "36", isLeaf: true },
          { "title": "ONI-HC-Conditions", "key": "37", isLeaf: true },
          { "title": "ONI-HC-Diagnosis", "key": "38", isLeaf: true },
          { "title": "ONI-HC-Medicines", "key": "39", isLeaf: true },
          { "title": "ONI-HC-Take Own Medication", "key": "40", isLeaf: true },
          { "title": "ONI-HC-Willing When Prescribed", "key": "41", isLeaf: true },
          { "title": "ONI-HC-Coop With Health Services", "key": "42", isLeaf: true },
          { "title": "ONI-HC-Webster Pack", "key": "43", isLeaf: true },
          { "title": "ONI-HC-Medication Review", "key": "44", isLeaf: true },
          { "title": "ONI-HC-Medical Comments", "key": "45", isLeaf: true },
        ]
        break;
        case "31":
        //ONI-Psychosocial Profile
        
        this.data = [
          
          { "title": "ONI-PS-K10-1", "key": "00", isLeaf: true },
          { "title": "ONI-PS-K10-2", "key": "01", isLeaf: true },
          { "title": "ONI-PS-K10-3", "key": "02", isLeaf: true },
          { "title": "ONI-PS-K10-4", "key": "03", isLeaf: true },
          { "title": "ONI-PS-K10-5", "key": "04", isLeaf: true },
          { "title": "ONI-PS-K10-6", "key": "05", isLeaf: true },
          { "title": "ONI-PS-K10-7", "key": "06", isLeaf: true },
          { "title": "ONI-PS-K10-8", "key": "07", isLeaf: true },
          { "title": "ONI-PS-K10-9", "key": "08", isLeaf: true },
          { "title": "ONI-PS-K10-10", "key": "09", isLeaf: true },
          { "title": "ONI-PS-Sleep Difficulty", "key": "10", isLeaf: true },
          { "title": "ONI-PS-Sleep Details", "key": "11", isLeaf: true },
          { "title": "ONI-PS-Personal Support", "key": "12", isLeaf: true },
          { "title": "ONI-PS-Personal Support Comments", "key": "13", isLeaf: true },
          { "title": "ONI-PS-Keep Friendships", "key": "14", isLeaf: true },
          { "title": "ONI-PS-Problems Interacting", "key": "15", isLeaf: true },
          { "title": "ONI-PS-Family/Relationship Comments", "key": "16", isLeaf: true },
          { "title": "ONI-PS-Svc Prvdr Relations", "key": "17", isLeaf: true },
          { "title": "ONI-PS-Svc Provider Comments", "key": "18", isLeaf: true },
        ]
        break;
        //ONI-Health Behaviours Profile
        case "32":
        this.data = [
          
          
          { "title": "ONI-HB-Regular Health Checks", "key": "00", isLeaf: true },
          { "title": "ONI-HB-Last Health Check", "key": "01", isLeaf: true },
          { "title": "ONI-HB-Health Screens", "key": "02", isLeaf: true },
          { "title": "ONI-HB-Smoking", "key": "03", isLeaf: true },
          { "title": "ONI-HB-If Quit Smoking - When?", "key": "04", isLeaf: true },
          { "title": "ONI-HB-Alcohol-How often?", "key": "05", isLeaf: true },
          { "title": "ONI-HB-Alcohol-How many?", "key": "06", isLeaf: true },
          { "title": "ONI-HB-Alcohol-How often over 6?", "key": "07", isLeaf: true },
          { "title": "ONI-HB-Lost Weight", "key": "08", isLeaf: true },
          { "title": "ONI-HB-Eating Poorly", "key": "09", isLeaf: true },
          { "title": "ONI-HB-How much wieght lost", "key": "10", isLeaf: true },
          { "title": "ONI-HB-Malnutrition Score", "key": "11", isLeaf: true },
          { "title": "ONI-HB-8 cups fluid", "key": "12", isLeaf: true },
          { "title": "ONI-HB-Recent decrease in fluid", "key": "13", isLeaf: true },
          { "title": "ONI-HB-Weight", "key": "14", isLeaf: true },
          { "title": "ONI-HB-Physical Activity", "key": "15", isLeaf: true },
          { "title": "ONI-HB-Physical Fitness", "key": "16", isLeaf: true },
          { "title": "ONI-HB-Fitness Comments", "key": "17", isLeaf: true },
        ]
        break;
        //ONI-CP-Need for carer
        case "33":
        this.data = [
          
          { "title": "ONI-CP-Carer Availability", "key": "00", isLeaf: true },
          { "title": "ONI-CP-Carer Residency Status", "key": "01", isLeaf: true },
          { "title": "ONI-CP-Carer Relationship", "key": "02", isLeaf: true },
          { "title": "ONI-CP-Carer has help", "key": "03", isLeaf: true },
          { "title": "ONI-CP-Carer receives payment", "key": "04", isLeaf: true },
          { "title": "ONI-CP-Carer made aware support services", "key": "05", isLeaf: true },
          { "title": "ONI-CP-Carer needs training", "key": "06", isLeaf: true },
          { "title": "ONI-CP-Carer threat-emotional", "key": "07", isLeaf: true },
          { "title": "ONI-CP-Carer threat-acute physical", "key": "08", isLeaf: true },
          { "title": "ONI-CP-Carer threat-slow physical", "key": "09", isLeaf: true },
          { "title": "ONI-CP-Carer threat-other factors", "key": "10", isLeaf: true },
          { "title": "ONI-CP-Carer threat-increasing consumer needs", "key": "11", isLeaf: true },
          { "title": "ONI-CP-Carer threat-other comsumer factors", "key": "12", isLeaf: true },
          { "title": "ONI-CP-Carer arrangements sustainable", "key": "13", isLeaf: true },
          { "title": "ONI-CP-Carer Comments", "key": "14", isLeaf: true },
        ]
        break;
        //ONI-CS-Year of Arrival
        case "34":
        this.data = [
          
          { "title": "ONI-CS-Year of Arrival", "key": "24", isLeaf: true },
          { "title": "ONI-CS-Citizenship Status", "key": "00", isLeaf: true },
          { "title": "ONI-CS-Reasons for moving to Australia", "key": "01", isLeaf: true },
          { "title": "ONI-CS-Primary/Secondary Language Fluency", "key": "02", isLeaf: true },
          { "title": "ONI-CS-Fluency in English", "key": "03", isLeaf: true },
          { "title": "ONI-CS-Literacy in primary language", "key": "04", isLeaf: true },
          { "title": "ONI-CS-Literacy in English", "key": "05", isLeaf: true },
          { "title": "ONI-CS-Non verbal communication style", "key": "06", isLeaf: true },
          { "title": "ONI-CS-Marital Status", "key": "07", isLeaf: true },
          { "title": "ONI-CS-Religion", "key": "08", isLeaf: true },
          { "title": "ONI-CS-Employment history in country of origin", "key": "09", isLeaf: true },
          { "title": "ONI-CS-Employment history in Australia", "key": "10", isLeaf: true },         
          { "title": "ONI-CS-Specific dietary needs", "key": "11", isLeaf: true },
          { "title": "ONI-CS-Specific cultural needs", "key": "12", isLeaf: true }, 
          { "title": "ONI-CS-Someone to talk to for day to day problems", "key": "13", isLeaf: true },
          { "title": "ONI-CS-Miss having close freinds", "key": "14", isLeaf: true },
          { "title": "ONI-CS-Experience general sense of emptiness", "key": "15", isLeaf: true },
          { "title": "ONI-CS-Plenty of people to lean on for problems", "key": "16", isLeaf: true },
          { "title": "ONI-CS-Miss the pleasure of the company of others", "key": "17", isLeaf: true },
          { "title": "ONI-CS-Circle of friends and aquaintances too limited", "key": "18", isLeaf: true },
          { "title": "ONI-CS-Many people I trust completely", "key": "19", isLeaf: true },
          { "title": "ONI-CS-Enough people I feel close to", "key": "20", isLeaf: true },
          { "title": "ONI-CS-Miss having people around", "key": "21", isLeaf: true },
          { "title": "ONI-CS-Often feel rejected", "key": "22", isLeaf: true },
          { "title": "ONI-CS-Can call on my friends whenever I need them", "key": "23", isLeaf: true },                    
        ]
        break;
        //Loan Items
        case "35":
        this.data = [
          { "title": "Loan Item Type", "key": "00", isLeaf: true },
          { "title": "Loan Item Description", "key": "01", isLeaf: true },
          { "title": "Loan Item Date Loaned/Installed", "key": "02", isLeaf: true },
          { "title": "Loan Item Date Collected", "key": "03", isLeaf: true },
        ]
        break;
        case "36":
        //  service information Fields
        this.data = [
          { "title": "Staff Code", "key": "00", isLeaf: true },
          { "title": "Service Date", "key": "01", isLeaf: true },
          { "title": "Service Start Time", "key": "02", isLeaf: true },
          { "title": "Service Code", "key": "03", isLeaf: true },
          { "title": "Service Hours", "key": "04", isLeaf: true },
          { "title": "Service Pay Rate", "key": "05", isLeaf: true },
          { "title": "Service Bill Rate", "key": "06", isLeaf: true },
          { "title": "Service Bill Qty", "key": "07", isLeaf: true },
          { "title": "Service Location/Activity Group", "key": "08", isLeaf: true },
          { "title": "Service Program", "key": "09", isLeaf: true },
          { "title": "Service Group", "key": "10", isLeaf: true },
          { "title": "Service HACC Type", "key": "11", isLeaf: true },
          { "title": "Service Category", "key": "12", isLeaf: true },
          { "title": "Service Status", "key": "13", isLeaf: true },
          { "title": "Service Pay Type", "key": "14", isLeaf: true },
          { "title": "Service Pay Qty", "key": "15", isLeaf: true },
          { "title": "Service End Time/ Shift End Time", "key": "16", isLeaf: true },
          { "title": "Service Funding Source", "key": "17", isLeaf: true },
          { "title": "Service Notes", "key": "18", isLeaf: true },
        ]
        break;
        //Service Specific Competencies
        case "37":
        this.data = [
          { "title": "Activity", "key": "00", isLeaf: true },
          { "title": "Competency", "key": "01", isLeaf: true },
          { "title": "S Status", "key": "02", isLeaf: true },
        ]
        break; 
        //  RECIPIENT OP NOTES
        case "38":
        this.data = [
          { "title": "OP Notes Date", "key": "00", isLeaf: true },
          { "title": "OP Notes Detail", "key": "01", isLeaf: true },
          { "title": "OP Notes Creator", "key": "02", isLeaf: true },
          { "title": "OP Notes Alarm", "key": "03", isLeaf: true },
          { "title": "OP Notes Program", "key": "04", isLeaf: true },
          { "title": "OP Notes Category", "key": "05", isLeaf: true },
        ] 
        break;
        // RECIPIENT CLINICAL NOTES
        case "39":
        this.data = [
          { "title": "Clinical Notes Date", "key": "00", isLeaf: true },
          { "title": "Clinical Notes Detail", "key": "01", isLeaf: true },
          { "title": "Clinical Notes Creator", "key": "02", isLeaf: true },
          { "title": "Clinical Notes Alarm", "key": "03", isLeaf: true },
          { "title": "Clinical Notes Category", "key": "04", isLeaf: true },
          
        ]
        break;
        // RECIPIENT INCIDENTS 
        case "40":
        this.data = [
          
          
          { "title": "INCD_Status", "key": "00", isLeaf: true },
          { "title": "INCD_Date", "key": "01", isLeaf: true },
          { "title": "INCD_Type", "key": "02", isLeaf: true },
          { "title": "INCD_Description", "key": "03", isLeaf: true },
          { "title": "INCD_SubCategory", "key": "04", isLeaf: true },
          { "title": "INCD_Assigned_To", "key": "05", isLeaf: true },
          { "title": "INCD_Service", "key": "06", isLeaf: true },
          { "title": "INCD_Severity", "key": "07", isLeaf: true },
          { "title": "INCD_Time", "key": "08", isLeaf: true },
          { "title": "INCD_Duration", "key": "09", isLeaf: true },
          { "title": "INCD_Location", "key": "10", isLeaf: true },
          { "title": "INCD_LocationNotes", "key": "11", isLeaf: true },
          { "title": "INCD_ReportedBy", "key": "12", isLeaf: true },
          { "title": "INCD_DateReported", "key": "13", isLeaf: true },
          { "title": "INCD_Reported", "key": "14", isLeaf: true },
          { "title": "INCD_FullDesc", "key": "15", isLeaf: true },
          { "title": "INCD_Program", "key": "16", isLeaf: true },
          { "title": "INCD_DSCServiceType", "key": "17", isLeaf: true },
          { "title": "INCD_TriggerShort", "key": "18", isLeaf: true },
          { "title": "INCD_incident_level", "key": "19", isLeaf: true },
          { "title": "INCD_Area", "key": "20", isLeaf: true },
          { "title": "INCD_Region", "key": "21", isLeaf: true },
          { "title": "INCD_position", "key": "22", isLeaf: true },
          { "title": "INCD_phone", "key": "23", isLeaf: true },
          { "title": "INCD_verbal_date", "key": "24", isLeaf: true },
          { "title": "INCD_verbal_time", "key": "25", isLeaf: true },
          { "title": "INCD_By_Whom", "key": "26", isLeaf: true },
          { "title": "INCD_To_Whom", "key": "27", isLeaf: true },
          { "title": "INCD_BriefSummary", "key": "28", isLeaf: true },
          { "title": "INCD_ReleventBackground", "key": "29", isLeaf: true },
          { "title": "INCD_SummaryofAction", "key": "30", isLeaf: true },
          { "title": "INCD_SummaryOfOtherAction", "key": "31", isLeaf: true },
          { "title": "INCD_Triggers", "key": "32", isLeaf: true },
          { "title": "INCD_InitialAction", "key": "33", isLeaf: true },
          { "title": "INCD_InitialNotes", "key": "34", isLeaf: true },
          { "title": "INCD_InitialFupBy", "key": "35", isLeaf: true },
          { "title": "INCD_Completed", "key": "36", isLeaf: true },
          { "title": "INCD_OngoingAction", "key": "37", isLeaf: true },          
          { "title": "INCD_OngoingNotes", "key": "38", isLeaf: true },
          { "title": "INCD_Background", "key": "39", isLeaf: true },
          { "title": "INCD_Abuse", "key": "40", isLeaf: true },
          { "title": "INCD_DOPWithDisability", "key": "41", isLeaf: true },
          { "title": "INCD_SeriousRisks", "key": "42", isLeaf: true },
          { "title": "INCD_Complaints", "key": "43", isLeaf: true },
          { "title": "INCD_Perpetrator", "key": "46", isLeaf: true },
          { "title": "INCD_Notify", "key": "47", isLeaf: true },
          { "title": "INCD_NoNotifyReason", "key": "48", isLeaf: true },
          { "title": "INCD_Notes", "key": "49", isLeaf: true },
          { "title": "INCD_Setting", "key": "50", isLeaf: true },
          { "title": "INCD_Involved_Staff", "key": "51", isLeaf: true },
        ]
        break;
        //  Recipient Competencies
        case "41":
        this.data = [
          
          { "title": "Recipient Competency", "key": "00", isLeaf: true },
          { "title": "Recipient Competency Mandatory", "key": "01", isLeaf: true },
          { "title": "Recipient Competency Notes", "key": "02", isLeaf: true },
        ] 
        break;
        //Care Plan
        case "42":
        this.data = [
          { "title": "CarePlan ID", "key": "00", isLeaf: true },
          { "title": "CarePlan Name", "key": "01", isLeaf: true },
          { "title": "CarePlan Type", "key": "02", isLeaf: true },
          { "title": "CarePlan Program", "key": "03", isLeaf: true },
          { "title": "CarePlan Discipline", "key": "04", isLeaf: true },
          { "title": "CarePlan CareDomain", "key": "05", isLeaf: true },
          { "title": "CarePlan StartDate", "key": "06", isLeaf: true },
          { "title": "CarePlan SignOffDate", "key": "07", isLeaf: true },
          { "title": "CarePlan ReviewDate", "key": "08", isLeaf: true },
          { "title": "CarePlan ReminderText", "key": "09", isLeaf: true },
          { "title": "CarePlan Archived", "key": "10", isLeaf: true },
          
        ]
        break;
        //Mental Health
        case "43":
        this.data = [
          
          { "title": "MH-PERSONID", "key": "00", isLeaf: true },
          { "title": "MH-HOUSING TYPE ON REFERRAL", "key": "01", isLeaf: true },
          { "title": "MH-RE REFERRAL", "key": "02", isLeaf: true },
          { "title": "MH-REFERRAL SOURCE", "key": "03", isLeaf: true },
          { "title": "MH-REFERRAL RECEIVED DATE", "key": "04", isLeaf: true },
          { "title": "MH-ENGAGED AND CONSENT DATE", "key": "05", isLeaf: true },
          { "title": "MH-OPEN TO HOSPITAL", "key": "06", isLeaf: true },
          { "title": "MH-OPEN TO HOSPITAL DETAILS", "key": "07", isLeaf: true },
          { "title": "MH-ALERTS", "key": "08", isLeaf: true },
          { "title": "MH-ALERTS DETAILS", "key": "09", isLeaf: true },
          { "title": "MH-MH DIAGNOSIS", "key": "10", isLeaf: true },
          { "title": "MH-MEDICAL DIAGNOSIS", "key": "11", isLeaf: true },
          { "title": "MH-REASONS FOR EXIT", "key": "12", isLeaf: true },
          { "title": "MH-SERVICES LINKED INTO", "key": "13", isLeaf: true },
          { "title": "MH-NON ACCEPTED REASONS", "key": "14", isLeaf: true },
          { "title": "MH-NOT PROCEEDED", "key": "15", isLeaf: true },
          { "title": "MH-DISCHARGE DATE", "key": "16", isLeaf: true },
          { "title": "MH-CURRENT AOD", "key": "17", isLeaf: true },
          { "title": "MH-CURRENT AOD DETAILS", "key": "18", isLeaf: true },
          { "title": "MH-PAST AOD", "key": "19", isLeaf: true },
          { "title": "MH-PAST AOD DETAILS", "key": "20", isLeaf: true },
          { "title": "MH-ENGAGED AOD", "key": "21", isLeaf: true },
          { "title": "MH-ENGAGED AOD DETAILS", "key": "22", isLeaf: true },
          { "title": "MH-SERVICES CLIENT IS LINKED WITH ON INTAKE", "key": "23", isLeaf: true },
          { "title": "MH-SERVICES CLIENT IS LINKED WITH ON EXIT", "key": "24", isLeaf: true },
          { "title": "MH-ED PRESENTATIONS ON REFERRAL", "key": "25", isLeaf: true },
          { "title": "MH-ED PRESENTATIONS ON 3 MONTH REVIEW", "key": "26", isLeaf: true },
          { "title": "MH-ED PRESENTATIONS ON EXIT", "key": "27", isLeaf: true },
          { "title": "MH-AMBULANCE ARRIVAL ON REFERRAL", "key": "28", isLeaf: true },
          { "title": "MH-AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW", "key": "29", isLeaf: true },
          { "title": "MH-AMBULANCE ARRIVAL ON EXIT", "key": "30", isLeaf: true },
          { "title": "MH-ADMISSIONS ON REFERRAL", "key": "31", isLeaf: true },
          { "title": "MH-ADMISSIONS ON MID-3 MONTH REVIEW", "key": "32", isLeaf: true },
          { "title": "MH-ADMISSIONS TO ED ON TIME OF EXIT", "key": "33", isLeaf: true },
          { "title": "MH-RESIDENTIAL MOVES", "key": "34", isLeaf: true },
          { "title": "MH-DATE OF RESIDENTIAL CHANGE OF ADDRESS", "key": "35", isLeaf: true },
          { "title": "MH-LOCATION OF NEW ADDRESS", "key": "36", isLeaf: true },
          { "title": "MH-HOUSING TYPE ON EXIT", "key": "37", isLeaf: true },
          { "title": "MH-KPI - INTAKE", "key": "38", isLeaf: true },
          { "title": "MH-KPI - 3 MONTH REVEIEW", "key": "39", isLeaf: true },
          { "title": "MH-KPI - EXIT", "key": "40", isLeaf: true },
          { "title": "MH-MEDICAL DIAGNOSIS DETAILS", "key": "41", isLeaf: true },
          { "title": "MH-SERVICES LINKED DETAILS", "key": "42", isLeaf: true },
          { "title": "MH-NDIS TYPE", "key": "43", isLeaf: true },
          { "title": "MH-NDIS TYPE COMMENTS", "key": "46", isLeaf: true },
          { "title": "MH-NDIS NUMBER", "key": "47", isLeaf: true },
          { "title": "MH-REVIEW APPEAL", "key": "48", isLeaf: true },
          { "title": "MH-REVIEW COMMENTS", "key": "49", isLeaf: true },
          { "title": "MH-KP_Intake_1", "key": "50", isLeaf: true },
          { "title": "MH-KP_Intake_2", "key": "51", isLeaf: true },
          { "title": "MH-KP_Intake_3MH", "key": "52" },
          { "title": "MH-KP_Intake_3PH", "key": "53" },
          { "title": "MH-KP_Intake_4", "key": "54" },
          { "title": "MH-KP_Intake_5", "key": "55" },
          { "title": "MH-KP_Intake_6", "key": "56" },
          { "title": "MH-KP_Intake_7", "key": "57" },
          { "title": "MH-KP_3Months_1", "key": "58" },
          { "title": "MH-KP_3Months_2", "key": "59" },
          { "title": "MH-KP_3Months_3MH", "key": "60" },
          { "title": "MH-KP_3Months_3PH", "key": "51" },
          { "title": "MH-KP_3Months_4", "key": "62" },
          { "title": "MH-KP_3Months_5", "key": "63" },
          { "title": "MH-KP_3Months_6", "key": "64" },
          { "title": "MH-KP_3Months_7", "key": "65" },
          { "title": "MH-KP_6Months_1", "key": "66" },
          { "title": "MH-KP_6Months_2", "key": "67" },
          { "title": "MH-KP_6Months_3MH", "key": "68" },
          { "title": "MH-KP_6Months_3PH", "key": "69" },
          { "title": "MH-KP_6Months_4", "key": "70" },
          { "title": "MH-KP_6Months_5", "key": "71" },
          { "title": "MH-KP_6Months_6", "key": "72" },
          { "title": "MH-KP_6Months_7", "key": "73" },
          { "title": "MH-KP_9Months_1", "key": "74" },
          { "title": "MH-KP_9Months_2", "key": "75" },
          { "title": "MH-KP_9Months_3MH", "key": "76" },
          { "title": "MH-KP_9Months_3PH", "key": "77" },
          { "title": "MH-KP_9Months_4", "key": "78" },
          { "title": "MH-KP_9Months_5", "key": "79" },
          { "title": "MH-KP_9Months_6", "key": "80" },
          { "title": "MH-KP_9Months_7", "key": "81" },
          { "title": "MH-KP_Exit_1", "key": "82" },
          { "title": "MH-KP_Exit_2", "key": "83" },
          { "title": "MH-KP_Exit_3MH", "key": "84" },
          { "title": "MH-KP_Exit_3PH", "key": "85" },
          { "title": "MH-KP_Exit_4", "key": "86" },
          { "title": "MH-KP_Exit_5", "key": "87" },
          { "title": "MH-KP_Exit_6", "key": "88" },
          { "title": "MH-KP_Exit_7", "key": "89" },
          { "title": "MH-KP_Intake_DATE", "key": "90" },
          { "title": "MH-KP_3Months_DATE", "key": "91" },
          { "title": "MH-KP_6Months_DATE", "key": "89" },
          { "title": "MH-KP_9Months_DATE", "key": "90" },
          { "title": "MH-KP_Exit_DATE", "key": "91" },
        ]
        break;
        //Recipient Placements
        case "44":
        this.data = [
          { "title": "Placement Type", "key": "00", isLeaf: true },
          { "title": "Placement Carer Name", "key": "01", isLeaf: true },
          { "title": "Placement Start", "key": "02", isLeaf: true },
          { "title": "Placement End", "key": "03", isLeaf: true },
          { "title": "Placement Referral", "key": "04", isLeaf: true },
          { "title": "Placement ATC", "key": "05", isLeaf: true },
          { "title": "Placement Notes", "key": "06", isLeaf: true },
        ] 
        break;
        //Quote Goals and stratagies
        case "45":
        this.data = [
          { "title": "Quote Goal", "key": "00", isLeaf: true },
          { "title": "Goal Expected Completion Date", "key": "01", isLeaf: true },
          { "title": "Goal Last Review Date", "key": "02", isLeaf: true },
          { "title": "Goal Completed Date", "key": "03", isLeaf: true },
          { "title": "Goal  Achieved", "key": "04", isLeaf: true },
          { "title": "Quote Strategy", "key": "05", isLeaf: true },
          { "title": "Strategy Expected Outcome", "key": "06", isLeaf: true },
          { "title": "Strategy Contracted ID", "key": "07", isLeaf: true },
          { "title": "Strategy DS Services", "key": "08", isLeaf: true },
        ]
        break;
        case "46": //Skills & Qualification
        this.data = [
          { "title": "Aged", "key": "00", isLeaf: true },
          { "title": "Child", "key": "01", isLeaf: true },
          { "title": "Dementia", "key": "02", isLeaf: true },
          { "title": "Disabilities", "key": "03", isLeaf: true },
          { "title": "Host", "key": "04", isLeaf: true },
          { "title": "Spinal", "key": "05", isLeaf: true },
          { "title": "Mental Health", "key": "06", isLeaf: true },
          { "title": "Palliative", "key": "07", isLeaf: true },
          { "title": "Other", "key": "08", isLeaf: true },
          { "title": "Domestic", "key": "09", isLeaf: true },
          { "title": "Registered Nurse ", "key": "10", isLeaf: true },
          { "title": "PCP/PCA", "key": "11", isLeaf: true },
          { "title": "Enrolled Nurse", "key": "12", isLeaf: true },
          { "title": "CACL1", "key": "13", isLeaf: true },
          { "title": "CACL2", "key": "14", isLeaf: true },
          { "title": "Other1", "key": "15", isLeaf: true },
          { "title": "Other2", "key": "16", isLeaf: true },
          { "title": "Other3", "key": "17", isLeaf: true },
          { "title": "Other4", "key": "18", isLeaf: true },
          { "title": "Other5", "key": "19", isLeaf: true },
          { "title": "Other6", "key": "20", isLeaf: true },
          { "title": "Assertiveness", "key": "21", isLeaf: true },
          { "title": "BackCare", "key": "22", isLeaf: true },
          { "title": "Confidentiality", "key": "23", isLeaf: true },
          { "title": "Dementia", "key": "24", isLeaf: true },
          { "title": "Disabilities", "key": "25", isLeaf: true },
          { "title": "DisabilitiesCert", "key": "26", isLeaf: true },
          { "title": "DutyOfCare", "key": "27", isLeaf: true },
          
          
        ]
        break;
        case "47": //Staff leaves
        this.data = [
          { "title": "Name", "key": "00", isLeaf: true },
          { "title": "Approved Status", "key": "01", isLeaf: true },
          { "title": "Leave Reminder Date", "key": "02", isLeaf: true },
          { "title": "Leave Start Date", "key": "03", isLeaf: true },
          { "title": "Leave End Date", "key": "04", isLeaf: true },            
          
        ]
        
        break;
        case "48": // general info
        this.data = [
          { "title": "UniqueID", "key": "00", isLeaf: true },
          { "title": "AcountNo", "key": "01", isLeaf: true },
          { "title": "StaffID", "key": "02", isLeaf: true },
          { "title": "Pin Number", "key": "03", isLeaf: true },
          { "title": "Start Date", "key": "04", isLeaf: true },
          { "title": "Termination Date", "key": "05", isLeaf: true },
          { "title": "Type", "key": "06", isLeaf: true },
          { "title": "Category", "key": "07", isLeaf: true },
          { "title": "Department", "key": "08", isLeaf: true },
          { "title": "Location", "key": "09", isLeaf: true },
          { "title": "Team", "key": "10", isLeaf: true },
          { "title": "Manager/Coordinto ", "key": "11", isLeaf: true },
          { "title": "Service Region", "key": "12", isLeaf: true },
          { "title": "Job Title", "key": "13", isLeaf: true },
          { "title": "Job Status", "key": "14", isLeaf: true },
          { "title": "Job Weighting", "key": "15", isLeaf: true },
          { "title": "Job FTE ", "key": "16", isLeaf: true },
          { "title": "Job Category", "key": "17", isLeaf: true },
          { "title": "Email Timesheet", "key": "18", isLeaf: true },
          { "title": "Award", "key": "19", isLeaf: true },
          { "title": "Award Level", "key": "20", isLeaf: true },
          { "title": "Pay Group", "key": "21", isLeaf: true },
          { "title": "Super %", "key": "22", isLeaf: true },
          { "title": "Super Fund", "key": "23", isLeaf: true },
          { "title": "Vehile Registration", "key": "24", isLeaf: true },
          { "title": "Drivers License", "key": "25", isLeaf: true },
          { "title": "Nurse Registration", "key": "26", isLeaf: true },
          { "title": "Gender", "key": "27", isLeaf: true },
          { "title": "Date of Birth", "key": "28", isLeaf: true },
          { "title": "Age", "key": "29", isLeaf: true },
          { "title": "Ageband-Statistical", "key": "30", isLeaf: true },
          { "title": "Ageband-5 Year", "key": "31", isLeaf: true },
          { "title": "Ageband-10 Year", "key": "32", isLeaf: true },
          { "title": "Age ATSI Status", "key": "33", isLeaf: true },
          { "title": "Month of Birth", "key": "34", isLeaf: true },
          { "title": "Month of Birth No", "key": "35", isLeaf: true },
          { "title": "Leave Start Date", "key": "36", isLeaf: true },
          { "title": "Leave Return Date", "key": "37", isLeaf: true },
          { "title": "Sub Category", "key": "38", isLeaf: true },
          { "title": "Panztel Pin", "key": "39", isLeaf: true },
          { "title": "Daelibs Logger ID", "key": "40", isLeaf: true },
          { "title": "Contact Issues", "key": "41", isLeaf: true },
          { "title": "CALD Status", "key": "42", isLeaf: true },
          { "title": "Indiginous Status", "key": "43", isLeaf: true },
          { "title": "Visa Status", "key": "43", isLeaf: true },
          
          
        ]
        break;
        case "49"://staff attribute
        this.data = [
          { "title": "Competency", "key": "00", isLeaf: true },
          { "title": "Competency Expiry Date", "key": "01", isLeaf: true },
          { "title": "Competency Reminder Date", "key": "02", isLeaf: true },
          { "title": "Competency Completion Date", "key": "03", isLeaf: true },
          { "title": "Mandatory Status", "key": "04", isLeaf: true },
          { "title": "Certificate Number", "key": "05", isLeaf: true },
          { "title": "Competency Notes", "key": "06", isLeaf: true },
          { "title": "Staff Position", "key": "07", isLeaf: true },
          { "title": "Staff Admin Categories", "key": "08", isLeaf: true },
          { "title": "NDIA Staff Level", "key": "09", isLeaf: true },
          
        ]
        break;
        case "50":// hr notes
        this.data = [
          { "title": "HR Notes Date", "key": "00", isLeaf: true },
          { "title": "HR Notes Detail", "key": "01", isLeaf: true },
          { "title": "HR Notes Creator", "key": "02", isLeaf: true },
          { "title": "HR Notes Alarm", "key": "03", isLeaf: true },
          { "title": "HR Notes Categories", "key": "04", isLeaf: true },            
          
        ]
        break;
        case "51"://op notes
        this.data = [
          { "title": "General Notes", "key": "00", isLeaf: true },
          { "title": "OP Notes Date", "key": "01", isLeaf: true },
          { "title": "OP Notes Detail", "key": "02", isLeaf: true },
          { "title": "OP Notes Creator", "key": "03", isLeaf: true },
          { "title": "OP Notes Alarm", "key": "04", isLeaf: true },
          { "title": "OP Notes Category", "key": "05", isLeaf: true },
          
        ]
        break;
        case "52":// staff inident
        this.data = [
          { "title": "INCD_Status", "key": "00", isLeaf: true },
          { "title": "INCD_Date", "key": "01", isLeaf: true },
          { "title": "INCD_TYpe", "key": "02", isLeaf: true },
          { "title": "INCD_Description", "key": "03", isLeaf: true },
          { "title": "INCD_SubCategory", "key": "04", isLeaf: true },
          { "title": "INCD_Assigned_To", "key": "05", isLeaf: true },
          { "title": "INCD_Service", "key": "06", isLeaf: true },
          { "title": "INCD_Severity", "key": "07", isLeaf: true },
          { "title": "INCD_Time", "key": "08", isLeaf: true },
          { "title": "INCD_Duration", "key": "09", isLeaf: true },
          { "title": "INCD_Location", "key": "10", isLeaf: true },
          { "title": "INCD_LocationNotes", "key": "11", isLeaf: true },
          { "title": "INCD_ReportedBy", "key": "12", isLeaf: true },
          { "title": "INCD_DateReported", "key": "13", isLeaf: true },
          { "title": "INCD_Reported", "key": "14", isLeaf: true },
          { "title": "INCD_FullDesc", "key": "15", isLeaf: true },
          { "title": "INCD_Program", "key": "16", isLeaf: true },
          { "title": "INCD_DSCServiceType", "key": "17", isLeaf: true },
          { "title": "INCD_TriggerShort", "key": "18", isLeaf: true },
          { "title": "INCD_level", "key": "19", isLeaf: true },
          { "title": "INCD_Area", "key": "20", isLeaf: true },
          { "title": "INCD_Region", "key": "21", isLeaf: true },
          { "title": "INCD_Position", "key": "22", isLeaf: true },
          { "title": "INCD_Phone", "key": "23", isLeaf: true },
          { "title": "INCD_Verbal_Date", "key": "24", isLeaf: true },
          { "title": "INCD_Verbal_Time", "key": "25", isLeaf: true },
          { "title": "INCD_By_Whom", "key": "26", isLeaf: true },
          { "title": "INCD_To_Whom", "key": "27", isLeaf: true },
          { "title": "INCD_BriefSummary", "key": "28", isLeaf: true },
          { "title": "INCD_ReleventBackground", "key": "29", isLeaf: true },
          { "title": "INCD_SummaryOfAction", "key": "30", isLeaf: true },
          { "title": "INCD_SummaryOfOtherAction", "key": "31", isLeaf: true },
          { "title": "INCD_Triggers", "key": "32", isLeaf: true },
          { "title": "INCD_InitialAtion", "key": "33", isLeaf: true },
          { "title": "INCD_InitialNotes", "key": "34", isLeaf: true },
          { "title": "INCD_InitialFupBy", "key": "35", isLeaf: true },
          { "title": "INCD_Completed", "key": "36", isLeaf: true },
          { "title": "INCD_OngoingAction", "key": "37", isLeaf: true },
          { "title": "INCD_OngoingNotes", "key": "38", isLeaf: true },
          { "title": "INCD_Background", "key": "39", isLeaf: true },
          { "title": "INCD_Abuse", "key": "40", isLeaf: true },
          { "title": "INCD_DOPwithDisability", "key": "41", isLeaf: true },
          { "title": "INCD_SerousRisks", "key": "42", isLeaf: true },
          { "title": "INCD_Complaints", "key": "43", isLeaf: true },
          { "title": "INCD_Perpetrator", "key": "44", isLeaf: true },
          { "title": "INCD_Notify", "key": "45", isLeaf: true },
          { "title": "INCD_NoNotifyReason", "key": "46", isLeaf: true },
          { "title": "INCD_Notes", "key": "47", isLeaf: true },
          { "title": "INCD_Setting", "key": "48", isLeaf: true },                                        
        ]
        break;  
        case "53":// work hours
        this.data = [
          { "title": "Min_Daily_HRS", "key": "00", isLeaf: true },
          { "title": "Max_Daily_HRS", "key": "01", isLeaf: true },
          { "title": "Min_Weekly_HRS", "key": "02", isLeaf: true },
          { "title": "Max_Weekly_HRS", "key": "03", isLeaf: true },
          { "title": "Min_Pay_Period_HRS", "key": "04", isLeaf: true },
          { "title": "Max_Pay_Period_HRS", "key": "05", isLeaf: true },
          { "title": "Week_1_Day_1", "key": "06", isLeaf: true },
          { "title": "Week_1_Day_2", "key": "07", isLeaf: true },
          { "title": "Week_1_Day_3", "key": "08", isLeaf: true },
          { "title": "Week_1_Day_4", "key": "09", isLeaf: true },
          { "title": "Week_1_Day_5", "key": "10", isLeaf: true },
          { "title": "Week_1_Day_6", "key": "11", isLeaf: true },
          { "title": "Week_1_Day_7", "key": "12", isLeaf: true },
          { "title": "Week_2_Day_1", "key": "13", isLeaf: true },
          { "title": "Week_2_Day_2", "key": "14", isLeaf: true },
          { "title": "Week_2_Day_3", "key": "15", isLeaf: true },
          { "title": "Week_2_Day_4", "key": "16", isLeaf: true },
          { "title": "Week_2_Day_5", "key": "17", isLeaf: true },
          { "title": "Week_2_Day_6", "key": "18", isLeaf: true },
          { "title": "Week_2_Day_7", "key": "19", isLeaf: true },
          
        ]
        break;
        case "54": // staff position
        this.data = [
          { "title": "Staff Position", "key": "00", isLeaf: true },
          { "title": "Position Start Date", "key": "01", isLeaf: true },
          { "title": "Position End Date", "key": "02", isLeaf: true },
          { "title": "Position ID", "key": "03", isLeaf: true },
          { "title": "Position Notes", "key": "04", isLeaf: true },
          
        ]
        break;
        case "55": // service information fields
        this.data = [
          { "title": "Staff Code", "key": "00", isLeaf: true },
          { "title": "Service Date", "key": "01", isLeaf: true },
          { "title": "Service Start Time", "key": "02", isLeaf: true },
          { "title": "Service Code", "key": "03", isLeaf: true },
          { "title": "Service Hours", "key": "04", isLeaf: true },
          { "title": "Service Pay Rate", "key": "05", isLeaf: true },
          { "title": "Service Bill Rate", "key": "06", isLeaf: true },
          { "title": "Service Bill Qty", "key": "07", isLeaf: true },
          { "title": "Service Location/Activity Group", "key": "08", isLeaf: true },
          { "title": "Service Progrm", "key": "09", isLeaf: true },
          { "title": "Service Group", "key": "10", isLeaf: true },
          { "title": "Service HACC Type ", "key": "11", isLeaf: true },
          { "title": "Service Category", "key": "12", isLeaf: true },
          { "title": "Service Status", "key": "13", isLeaf: true },
          { "title": "Service Pay Type", "key": "14", isLeaf: true },
          { "title": "Service Pay Qty", "key": "15", isLeaf: true },
          { "title": "Service Bill Unit", "key": "16", isLeaf: true },
          { "title": "Service End Time/Shift End Time", "key": "17", isLeaf: true },
          { "title": "Service Funding Source", "key": "18", isLeaf: true },
          { "title": "Service Notes", "key": "19", isLeaf: true },
          
        ]
        break;
        case "56": //STAFF Name and Address
        this.data = [
          { "title": "Title", "key": "00", isLeaf: true },
          { "title": "First Name", "key": "01", isLeaf: true },
          { "title": "Middle Name", "key": "02", isLeaf: true },
          { "title": "Surname/Orgnisation", "key": "03", isLeaf: true },
          { "title": "Preferred Name", "key": "04", isLeaf: true },
          { "title": "contact Address Line 1", "key": "05", isLeaf: true },
          { "title": "contact Address Line 2", "key": "06", isLeaf: true },
          { "title": "contact Address-Suburb", "key": "07", isLeaf: true },
          { "title": "contact Address-Postcode", "key": "08", isLeaf: true },
          { "title": "contact Address-state", "key": "09", isLeaf: true },
          { "title": "contact Address-GoogleAddress", "key": "10", isLeaf: true },
          { "title": "Usual Address Line 1", "key": "11", isLeaf: true },
          { "title": "Usual Address Line 2", "key": "12", isLeaf: true },
          { "title": "Usual Address-Suburb", "key": "13", isLeaf: true },
          { "title": "Usual Address-Postcode", "key": "14", isLeaf: true },
          { "title": "Usual Address-state", "key": "15", isLeaf: true },
          { "title": "Usual Address-GoogleAddress", "key": "16", isLeaf: true },
          { "title": "Billing Address Line 1", "key": "17", isLeaf: true },
          { "title": "Billing Address Line 2", "key": "18", isLeaf: true },
          { "title": "Billing Address-Suburb", "key": "19", isLeaf: true },
          { "title": "Billing Address-Postcode", "key": "20", isLeaf: true },
          { "title": "Billing Address-state", "key": "21", isLeaf: true },
          { "title": "Billing Address-GoogleAddress ", "key": "22", isLeaf: true },
          { "title": "Destination Address Line 1", "key": "23", isLeaf: true },
          { "title": "Destination Address Line 2", "key": "24", isLeaf: true },
          { "title": "Destination Address-Suburb", "key": "25", isLeaf: true },
          { "title": "Destination Address-Postcode", "key": "26", isLeaf: true },
          { "title": "Destination Address-state", "key": "27", isLeaf: true },
          { "title": "Destination Address-GoogleAddress", "key": "28", isLeaf: true },
          { "title": "Email", "key": "29", isLeaf: true },
          { "title": "Email-SMS", "key": "30", isLeaf: true },
          { "title": "FAX", "key": "31", isLeaf: true },
          { "title": "Home Phone", "key": "32", isLeaf: true },
          { "title": "Mobile Phone", "key": "33", isLeaf: true },
          { "title": "Usual Phone", "key": "34", isLeaf: true },
          { "title": "Work Phone", "key": "35", isLeaf: true },
          { "title": "Current Phone Number", "key": "36", isLeaf: true },
          { "title": "Other Phone Number", "key": "37", isLeaf: true },
          
          
        ]
        break;
        
        
        
        default:
        this.data = [{}]
        break;
      }
      
      return this.data;
      
    }
    nzEvent(event: NzFormatEmitEvent): void {      
      // load child async
      if (event.eventName === 'expand') {
        const node = event.node;
        //  console.log(event.keys)
        this.ContentSetter(event.keys);
        if (node?.getChildren().length === 0 && node?.isExpanded) {
          this.loadNode().then(data => {
            node.addChildren(data);
          });
        }
      }
      if (event.eventName === 'click' && event.keys[0].length < 3 ) {
        // IncludeFundingSource  IncludeProgram  IncludeStaffAttributes  IncludePensions  IncludeExcluded  IncludeIncluded  IncludePreferences
        //'' IncludeGoals  IncludeLoanItems  IncludeContacts  IncludeConsents  IncludeDocuments  IncludeUserGroups  IncludeReminders  IncludeStaffReminders  IncludeLeaves
        //'' IncludeCaseStaff  IncludeCarePlans  IncludeServiceCompetencies  includeStaffIncidents  includeRecipIncidents  IncludeStaffUserGroups  IncludeStaffPreferences
        //'' IncludeNursingDiagnosis  IncludeMedicalDiagnosis  IncludeMedicalProcedure  IncludeAgreedServices  IncludePlacements  IncludeCaseNotes
        //'' IncludeRecipientOPNotes  IncludeRecipientClinicalNotes  IncludeStaffOPNotes  IncludeStaffHRNotes  IncludeONI  IncludeONIMainIssues
        //'' IncludeONIOtherIssues  IncludeONICurrentServices  IncludeONIActionPlan  IncludeONIMedications  IncludeONIHealthConditions  IncludeStaffPosition
        //'' IncludeDEX  IncludeCarerInfo  IncludeHACC  IncludeRecipBranches  includeHRRecipAttribute  IncludeRecipCompetencies  IncludeStaffLoanItems  IncludeCarePlan  IncludeGoalsAndStrategies  IncludeMentalHealth
        if(this.StrType == '06'){
        }                
        if (this.list == null){          
          this.one  = [event.node.title]
        }
        else{         
          if (this.list.includes(event.node.title.toString())){            
          }else{
          this.one = [ ...this.list  , event.node.title  ]
          }          
        }        
        this.FinalizeArray(this.one);                
      }
    }
    loadNode(): Promise<NzTreeNodeOptions[]> {
      
      return new Promise(resolve => {
        setTimeout(
          () =>
          resolve(
            this.data
            //  [ { title: 'Child Node', key: `${new Date().getTime()}-0` },
            //  { title: 'Child Node', key: `${new Date().getTime()}-1` }]
            ),
            100
            );
          });
        }
    FinalizeArray(node) { 
      this.frm_nodelist = true;           
      this.list = node;
                          
      this.exportitemsArr = [...this.list,"Service Date", "Service Start Time", "Service Hours", "Service Code", "Service Location/Activity Group", "Service Program", "Service Group", "Service HACC Type", "Service Category", "Service Pay Rate", "Service Bill Rate", "Service Bill Qty", "Service Status", "Service Pay Type", "Service Pay Qty", "Service Bill Unit", "Service Funding Source"]          
      
    }
    EnablerAplyBtn(){
      this.Applybutton = false;
    }
    SetValueFrame() {          
      if(this.inputForm.value.criteriaArr == "IN")  {           
        this.bodystyleModal_IN = {  overflow: 'auto' };            
        var keys = this.inputForm.value.exportitemsArr;
        //.subscribe(x => this.outletsArr = x);
        /*switch(keys){
          case 'Title':
            var sql = "select Distinct Title from Recipients where title != null or Title != ''"                
            this.bodystyleModal_IN = { height:'200px', overflow: 'auto' };
            this.ModalTitle = "Titles";

          break;
          case'Address-State':
            var sql = "Select distinct state from Recipients where state != null Or State != ''"              
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Address-States";


          break;
          case 'Address-Suburb':
            var sql = "Select distinct Suburb from Recipients where Suburb != null Or Suburb != ''"                
            this.bodystyleModal_IN = { height:'600px' ,overflow: 'auto' };
            this.ModalTitle = "Address-Suburbs";
                  
            break;
            case 'Address-Postcode':
              var sql = "Select distinct Postcode from Recipients where Postcode != null Or Postcode != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Address-Postcodes";
                    
            break;
            /*
            case 'Address-Suburb':
              var sql = "Select distinct Suburb from Recipients where Suburb != null Or Suburb != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Address-Suburb";                        
            break;
            case 'Country Of Birth':
              var sql = "Select distinct CountryOfBirth from Recipients where CountryOfBirth != null Or CountryOfBirth != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Country Of Birth";                        
            break; //Repeat 
            case 'Category':
              var sql = "Select distinct AgencyDefinedGroup from Recipients where AgencyDefinedGroup != null Or AgencyDefinedGroup != '' "                 
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Categories";                        
            break;
            case 'CoOrdinator':
              var sql = "Select distinct RECIPIENT_CoOrdinator from Recipients where RECIPIENT_CoOrdinator != null Or RECIPIENT_CoOrdinator != '' "                 
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CoOrdinator";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break; 
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;
            case '':
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";                        
            break;

          
          
          
          
            
            default:

          break;

        } */
        if(this.RptFormat == "AGENCYSTFLIST" || this.RptFormat == "USERSTFLIST"){              
          switch(keys){
            //DOING                 

            //STAFF NAME AND ADDRESS      
            case 'Title':
              var sql = "select Distinct Title from Staff where title != null or Title != ''"                
              this.bodystyleModal_IN = { height:'200px' };
              this.ModalTitle = "Titles";                  
            break;
            case 'First Name':
              var sql = "Select distinct FirstName from Staff where FirstName != null Or FirstName != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "First Names";                   
              break;
              case 'Middle Name':
                var sql = "Select distinct MiddleNames from Staff where MiddleNames != null Or MiddleNames != '' "                 
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "Middle Names";       
              break;
              case 'Surname/Organisation':
                var sql = "Select distinct LastName as [Surname/Organisation] from Staff where LastName != null Or LastName != '' "                 
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "Surname/Organisation";   
              break;
              case 'Preferred Name':
                var sql = "Select distinct PreferredName from Staff where PreferredName != null Or PreferredName != '' "                 
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "Preferred Name";        
              break; 
              case 'contact Address Line 1':
              var sql = "Select distinct (SELECT address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') as Address1 from Staff  "                 
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "Contact Address-Line1";                          
              break;
              case 'contact Address Line 2':
              this.ConditionEntity ='R.Address2'
              var sql = "Select distinct (SELECT address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') as ContactAddress2 from Staff  "                 
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "Contact Address-Line2";    
              break;
              case 'contact Address-Suburb':
              var sql = "Select distinct (SELECT suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') as Suburb from Staff "                
                this.bodystyleModal_IN = { height:'400px' ,overflow: 'auto' };
                this.ModalTitle = "Contact Address-Suburbs";      
              break;
              case 'contact Address-Postcode':
              var sql = "SELECT postcode FROM   namesandaddresses WHERE [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>'  "                 
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Address-Postcodes";  
              break;
              case 'contact Address-state':
              var sql = "Select distinct CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' and '2618'OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate from Staff where postcode != null Or postcode != ''"              
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Contact Address-States"; 
              break;
              case 'contact Address-GoogleAddress':
                var sql = "Select distinct (SELECT googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as GoogleAddress from Staff"
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Address-States";      
              break;
              case 'Usual Address Line 1 ':
                var sql = "Select distinct (SELECT  address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as address1 from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Usual Address Line 1";       
              break;
              case 'Usual Address Line 2':
                var sql = "Select distinct (SELECT  address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as Address2 from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Usual Address Line 2";  
              break;
              case 'Usual Address-Suburb':
                var sql = "Select distinct (SELECT  Suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as Suburb from Staff";
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Usual Address-Suburb";
              break;
              case 'Usual Address-Postcode  ':
                var sql = "Select distinct (SELECT  postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as postcode from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Usual Address-Postcode";       
              break;
              case 'Usual Address-state':
                var sql = "Select distinct (SELECT  CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as AddressState from Staff"
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Usual Address-state";       
              break;
              case 'Usual Address-GoogleAddress':
                var sql = "Select distinct (SELECT googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') as googleaddress from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Usual Address-GoogleAddress";      
              break;    
              case 'Billing Address Line 1':
                var sql = "Select distinct (SELECT  address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') as Address1 from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Billing Address Line 1";     
              break;
              case 'Billing Address Line 2':
                var sql = " Select distinct (SELECT  address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') as Address2 from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Billing Address Line 2";       
              break;
              case 'Billing Address-Suburb':
                var sql  = "Select distinct (SELECT suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') as suburb from Staff"
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Billing Address-Suburb";        
              break;
              case 'Billing Address-Postcode':
                var sql = " Select distinct(SELECT  postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') as postcode from Staff"
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Billing Address-Postcode";       
              break;
              case 'Billing Address-state':
                var sql = "Select distinct (SELECT  CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') as AddressState from Staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Billing Address-state";  
              break;
              case 'Billing Address-GoogleAddress': 
                var sql = "select distinct (SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') as GoogleAddress from staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Billing Address-GoogleAddress";  
              break;
              case 'Destination Address Line 1':
                var sql = "select (SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') as address1 from Staff  "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Destination Address Line 1";   
              break;
              case 'Destination Address Line 2':
                var sql = "select (SELECT address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') as address2 from staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Destination Address Line 2";       
              break;
              case 'Destination Address-Suburb ':
                var sql = "select (SELECT suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') as suburb from staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Destination Address-Suburb";      
              break;
              case 'Destination Address-Postcode':
                var sql = "select (SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') as postcode from staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Destination Address-Postcode";       
              break;
              case 'Destination Address-state':
                var sql = "select (SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') as State from staff "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Destination Address-state";      
              break;
              case 'Destination Address-GoogleAddress':
                var sql = "select (SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') as googleaddress from staff"
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Destination Address-GoogleAddress";    
              break;
              /*
              case 'Email':
                //var sql = "select (SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<EMAIL>') as suburb from staff"
                      
              break;      
              case 'Email-SMS':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<EMAIL-SMS>') "
                  
              break;
              case 'FAX':
                var sql ="(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<FAX>') "
                      
              break;
              case 'Home Phone':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<HOME>') "
                  
              break;
              case 'Mobile Phone':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<MOBILE>') "
                    
              break;
              case 'Usual Phone':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<USUAL>') "
                    
              break;
              case 'Work Phone':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<WORK>') "
                      
              break;
              case 'Current Phone Number':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = 'CURRENT PHONE NUMBER' ) "
                    
              break;
              case 'Other Phone Number':
                var sql = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = 'OTHER PHONE NUMBER') "
                      
              break;  */                                
              //  Contacts & Next of Kin
              case 'Contact Group':   
                var sql  = "Select distinct [Group] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Group] != null Or [Group] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Group";
              break;
              case 'Contact Type': 
                var sql  = "Select distinct [Type] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Type] != null Or [Type] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Type";   
              break;
              case 'Contact Sub Type':
                var sql  = "Select distinct [SubType] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [SubType] != null Or [SubType] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Sub Type";    
              break;
              case 'Contact User Flag':
                var sql  = "Select distinct [User1] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [User1] != null Or [User1] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact User Flag";
              break;                 
              case 'Contact Person Type':
                var sql  = "Select distinct [EquipmentCode] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [EquipmentCode] != null Or [EquipmentCode] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Person Type";   
              break;
              case 'Contact Name':
                var sql  = "Select distinct [Name] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Name] != null Or [Name] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Name";  
              break;
              case 'Contact Address':
                var sql  = "Select distinct [Address1]  from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Address1]  != null Or [Address1]  != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Address";  
              break;
              case 'Contact Suburb':
                var sql  = "Select distinct [Suburb] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Suburb] != null Or [Suburb] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Suburb";  
              break;
              case 'Contact Postcode':
                var sql  = "Select distinct [Postcode] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Postcode] != null Or [Postcode] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Postcode";  
              break;
              case 'Contact Phone 1':
                var sql  = "Select distinct [Phone1] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Phone1] != null Or [Phone1]  != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Phone 1";  
              break;
              case 'Contact Phone 2':
                var sql  = "Select distinct [Phone2] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Phone2] != null Or [Phone2] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Phone 2";   
              break;
              case 'Contact Mobile': 
                var sql  = "Select distinct [Mobile] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Mobile] != null Or [Mobile] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Mobile";  
              break;
              case 'Contact FAX':   
                var sql  = "Select distinct [FAX] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [FAX] != null Or [FAX] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact FAX";
              break;
              case 'Contact Email':
                var sql  = "Select distinct [Email] from HumanResources where [Group] IN ('NEXTOFKIN', 'CONTACT','1-NEXT OF KIN','OTHERCONTACT') and [Email] != null Or [Email] != '' "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Contact Email";
              break;
            /*//Carer Info      
          case 'Carer First Name':
          var sql  = "Select distinct FirstName from Recipients where FirstName != null Or FirstName != '' "
          this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
          this.ModalTitle = "Carer First Name(s)"; 
          break;
          case 'Carer Last Name':
          var sql  = "Select distinct [Surname/Organisation] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "
          this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
          this.ModalTitle = "Carer Last Name(s)";
          break;
          /*
          case 'Carer Age':
          this.ConditionEntity =  'DateDiff(YEAR,C.Dateofbirth,GetDate())'
          break;                  */
          /*
          case 'Carer Gender':
          var sql  = "Select distinct [Gender] from Recipients where [Gender] != null Or [Gender] != '' "
          this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
          this.ModalTitle = "Carer Gender";
          break;
          case 'Carer Indigenous Status':
          var sql  = "Select distinct [IndiginousStatus] from Recipients where [IndiginousStatus] != null Or [IndiginousStatus] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Carer Indigenous Status";
          break;                       
          case 'Carer Address':
          var sql  = "Select (select Address1 + ' ' + Suburb + ' ' + Postcode from NamesAndAddresses N UNIQUEID = N.PERSONID AND N.[Description] = '<USUAL>' ) as Address from Recipients C where Address1 != null Or Address1 != '' "
          this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
          this.ModalTitle = "Carer Address";
          break; /*
          case 'Carer Email':
          this.ConditionEntity =  'PE.Detail'
          var sql  = "Select distinct  from Recipients where != null Or  != '' "
          this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
          this.ModalTitle = "";
          break;
          case 'Carer Phone <Home>':
          this.ConditionEntity =  'PhHome.Detail'
          break;
          case 'Carer Phone <Work>':
          this.ConditionEntity =  'PhWork.Detail'
          break;
          case 'Carer Phone <Mobile>':
          this.ConditionEntity =  'PhMobile.Detail'
          break; */
          // Documents                
          /*case 'DOC_ID':
          this.ConditionEntity =  'doc.DOC_ID'
          var sql  = "Select distinct DOC_ID from Recipients where DOC_ID != null Or DOC_ID != '' "
          this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
          this.ModalTitle = "";
          break;  */
          /*
          case 'Doc_Title':
          var sql  = "Select distinct Title from DOCUMENTS where Title != null Or Title != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document Title(s)";
          break;
          case 'Created':
          var sql  = "Select distinct Created from DOCUMENTS where Created != null Or Created != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document Creater(s)";
          break;                
          case 'Modified':
          var sql  = "Select distinct Modified from DOCUMENTS where Modified != null Or Modified != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document Modified";
          break;
          case 'Status':
          var sql  = "Select distinct Status from DOCUMENTS where Status != null Or Status != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document Status";
          break;                                
          case 'Classification':
          var sql  = "Select distinct Classification from DOCUMENTS where Classification != null Or Classification != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document Classification";
          break;
          case 'Category':
          var sql  = "Select distinct Category from DOCUMENTS where Category != null Or Category != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document Categories";
          break;
          case 'Filename':
          var sql  = "Select distinct Filename from DOCUMENTS  Filename != null Or Filename != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "File Names";
          break; /*
          case 'Doc#':
          this.ConditionEntity =  'doc.Doc#'
          break;
          case 'DocStartDate':
          this.ConditionEntity =  'doc.DocStartDate'
          break;
          case 'DocEndDate':
          this.ConditionEntity =  'doc.DocEndDate'
          break; */
          /*
          case 'AlarmDate':
          var sql  = "Select distinct format(AlarmDate,'dd/MM/yyyy') as AlarmDate from DOCUMENTS where AlarmDate != null Or AlarmDate != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Document AlarmDate";
          break;     /*                     
          case 'AlarmText':
          this.ConditionEntity =  'doc.AlarmText'
          var sql  = "Select distinct  from Recipients where != null Or  != '' "
          this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
          this.ModalTitle = "";
          break; */              
          // USER GROUPS                     
          case 'Group Name': 
            var sql  = "Select distinct [Name] from HumanResources where [Group] = 'STAFFTYPE' and [Name] != null Or [Name] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Group Name";   
          break;
          case 'Group Note':
            var sql  = "Select distinct [Notes] from HumanResources where [Group] = 'STAFFTYPE' and [Notes] != null Or [Notes] != '' "
            this.bodystyleModal_IN = { height:'500px' , overflow: 'auto' };
            this.ModalTitle = "Group Note";  
          break; 
          case 'Group Start Date':
            var sql  = "Select distinct format([Date1],'dd/MM/yyyy') as GroupStartDate from HumanResources where [Group] = 'STAFFTYPE' and != null Or  != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Group Start Date(s)";    
          break;                      
          case 'Group End Date':
            var sql  = "Select distinct format([Date2],'dd/MM/yyyy') as GroupEndDate from HumanResources where [Group] = 'STAFFTYPE' and [Date2] != null Or [Date2] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Group End Date(s)";  
          break;
          case 'Group Email':
            var sql  = "Select distinct [Email] from HumanResources where [Group] = 'STAFFTYPE' and [Email] != null Or [Email] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Group Email";    
          break;                
          //Preferences                                        
          case 'Preference Name':
            var sql  = "Select distinct [Name] from HumanResources where [Group] = 'STAFFPREF' and [Name] != null Or [Name] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Preference Name";     
          break;                      
          case 'Preference Note':
            var sql  = "Select distinct [Notes] from HumanResources where [Group] = 'STAFFPREF' and [Notes] != null Or [Notes] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Preference Note";   
          break;
          //REMINDERS                      
          case 'Reminder Detail': 
            var sql  = "Select distinct [Name] from HumanResources where [Group] = 'RECIPIENTALERT' and [Name] != null Or [Name] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Reminder Detail";   
          break;
          case 'Event Date':
            var sql  = "Select distinct format([Date2],'dd/MM/yyyy') as ReminderDate from HumanResources where [Group] = 'RECIPIENTALERT' and Date2 != null Or Date2 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Event Date"; 
          break;
          case 'Reminder Date':
            var sql  = "Select distinct format([Date1],'dd/MM/yyyy') as ReminderDate from HumanResources where [Group] = 'RECIPIENTALERT' and Date1 != null Or Date1 != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Reminder Date"; 
          break;
          case 'Reminder Notes':
            var sql  = "Select distinct [Notes] from HumanResources where [Group] = 'RECIPIENTALERT' and [Notes] != null Or [Notes] != '' "
          this.bodystyleModal_IN = { height:'500px' , overflow: 'auto' };
          this.ModalTitle = "Reminder Notes";  
          break;
          //Loan Items                      
          case 'Loan Item Type':  
            var sql  = "Select distinct [Type] from Recipients where [Type] != null Or [Type] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Loan Item Type";
          break;
          case 'Loan Item Description': 
            var sql  = "Select distinct [Name] from Recipients where [Name] != null Or [Name] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Loan Item Description";
          break;
          case 'Loan Item Date Loaned/Installed': 
            var sql  = "Select distinct format([Date1],'dd/MM/yyyy') as LoanItemDateLoaned from Recipients where [Date1] != null Or [Date1] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Loan Item Date Loaned/Installed";
          break;                      
          case 'Loan Item Date Collected':
            var sql  = "Select distinct format([Date2],'dd/MM/yyyy') as [Date2] from Recipients where [Date2] != null Or [Date2] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Loan Item Date Collected";
          break;
          //  service information Fields                      
          case 'Staff Code':
            var sql  = "Select distinct [Carer Code] from Roster where [Carer Code] != null Or [Carer Code] != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "Staff Code(s)";
            break;
            case 'Service Date':
            var sql  = "Select distinct format(Date,'dd/MM/yyyy') as Date from Roster where != null Date Or Date != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Service Date(s)"; 
            break;
            case 'Service Start Time':
            var sql  = "Select distinct [Start Time] from Roster where [Start Time] != null Or [Start Time] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Start Time"; 
            break;
            case 'Service Code':
            var sql  = "Select distinct [Service Type] from Roster where [Service Type] != null Or [Service Type] != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Service Code";  
            break;                      
            case 'Service Hours':
            var sql  = "Select distinct (([Duration]*5) / 60) as ServiceHour from Roster where Duration != null Or Duration != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Hours";  
            break;
            case 'Service Pay Rate':
            var sql  = "Select distinct [Unit Pay Rate] from Roster where [Unit Pay Rate] != null Or [Unit Pay Rate] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Pay Rate";  
            break;
            case 'Service Bill Rate':
            var sql  = "Select distinct [Unit Bill Rate] from Roster where [Unit Bill Rate] != null Or [Unit Bill Rate] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Bill Rate"; 
            break;
            case 'Service Bill Qty':
            var sql  = "Select distinct [BillQty] from Roster where [BillQty] != null Or [BillQty] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Bill Qty"; 
            break;              
            case 'Service Location/Activity Group':
            var sql  = "Select distinct [ServiceSetting] from Roster where [ServiceSetting] != null Or [ServiceSetting] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service Location/Activity Group";  
            break;
            case 'Service Program':
            var sql  = "Select distinct [Program] from Roster where [Program] != null Or [Program] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service Program";  
            break;
            case 'Service Group':  
            var sql  = "Select distinct [Type] from Roster where [Type] != null Or [Type] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service Group";
            break;
            case 'Service HACC Type':  
            var sql  = "Select distinct [HACCType] from Roster where [HACCType] != null Or [HACCType] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service HACC Type";
            break;
            case 'Service Category':
            var sql  = "Select distinct [Anal] from Roster where [Anal] != null Or [Anal] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service Category";   
            break;
            case 'Service Status':
            var sql  = "Select distinct [Status] from Roster where [Status] != null Or [Status] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service Status";  
            break;
            case 'Service Pay Type': 
            var sql  = "Select distinct [Service Description] from Roster where [Service Description] != null Or [Service Description] != '' "
          this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
          this.ModalTitle = "Service Pay Type"; 
            break;
            case 'Service Pay Qty':
            var sql  = "Select distinct [CostQty] from Roster where [CostQty] != null Or [CostQty] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ervice Pay Qty";   
            break;
            case 'Service End Time/ Shift End Time':              
            var sql  = "Select distinct Convert(varchar,DATEADD(minute,([Duration]*5) ,[Start Time]),108) as ShiftEndTime from Roster where [Duration] != null Or [Duration] != '' order by ShiftEndTime Asc "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service End Time/ Shift End Time";
            break;
            case 'Service Funding Source':
            var sql  = "Select distinct [Type] from Humanresourcetypes where [Type] != null Or [Type] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Funding Source"; 
            break;
            /*
            case 'Service Notes':
            CAST(History.Detail AS varchar(4000))   
            var sql  = "Select distinct  from History where != null Or  != '' "
          this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
          this.ModalTitle = "";
            break; */
          //Staff Leaves
          case 'Name':
            var sql  = "Select distinct Name from HumanResources where [Group] = 'LEAVEAPP' and Name != null Or Name != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Name(s)";  
            break;
          case 'Approved Status':
            var sql  = "Select case when completed = '1' then 'completed' else 'Not completed' end as Status from HumanResources where [Group] = 'LEAVEAPP'  and completed != null Or completed != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Approved Status(s)";  
            break; 
          case 'Leave Reminder Date':
            var sql  = "Select distinct format([DateInstalled],'dd/MM/yyyy') as DateInstalled from HumanResources where [Group] = 'LEAVEAPP' and DateInstalled != null Or DateInstalled != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Leave Reminder Date(s)";   
            break; 
          case 'Leave Start Date':
            var sql  = "Select distinct format([Date1],'dd/MM/yyyy') as [Date1] from HumanResources where[Group] = 'LEAVEAPP' and [Date1] != null Or [Date1] != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Leave Start Date(s)";  
            break; 
          case 'Leave End Date':
            var sql  = "Select distinct format([Date2],'dd/MM/yyyy') as [Date2] from HumanResources where [Group] = 'LEAVEAPP' and [Date2] != null Or [Date2] != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Leave End Date(s)";  
            break;
            //STAFF HR NOTES
          case 'HR Notes Date':
            var sql  = "Select distinct format(DetailDate,'dd/MM/yyyy') as DetailDate from History where ExtraDetail1 = 'HRNOTE' and DetailDate != null Or DetailDate != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "HR Notes Date(s)";
            break;
          case 'HR Notes Detail':
            var sql  = "Select distinct dbo.RTF2TEXT(HRHistory.[Detail]) as [HR Notes Detail] from History where ExtraDetail1 = 'HRNOTE' and ExtraDetail1 != null Or ExtraDetail1 != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "HR Notes Detail(s)";
            break;
          case 'HR Notes Creator':
            var sql  = "Select distinct Creator from History where ExtraDetail1 = 'HRNOTE' and Creator != null Or Creator != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "HR Notes Creator(s)";
            break;
          case 'HR Notes Alarm':
            var sql  = "Select distinct format (AlarmDate,'dd/MM/yyyy') as AlarmDate from History where ExtraDetail1 = 'HRNOTE' and AlarmDate != null Or AlarmDate != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "HR Notes Alarm(s)";
            break;
          case 'HR Notes Categories':
            var sql  = "Select distinct ExtraDetail2 from History where ExtraDetail1 = 'HRNOTE' and ExtraDetail2 != null Or ExtraDetail2 != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "HR Notes Categories";
            break;
              //Staff Position
              case 'Staff Position': 
              var sql  = "Select distinct [Name] from HumanResources where [Name] != null Or [Name] != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "Staff Position(s)";
              break;           
              case 'Position Start Date':
              var sql  = "Select distinct [Start Date] from HumanResources where [Start Date] != null Or [Start Date] != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "Position Start Date(s)";
              break;
              case 'Position End Date':
              var sql  = "Select distinct [End Date] from HumanResources where [End Date] != null Or [End Date] != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "Position End Date(s)";
              break;
              case 'Position ID':
              var sql  = "Select distinct [Position ID] from HumanResources where [Position ID] != null Or [Position ID] != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "Position ID(s)";
              break;
              case 'Position Notes':
              var sql  = "Select distinct [Notes] from HumanResources where [Notes] != null Or [Notes] != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "Position Notes(s)";
              break;
              //Work Hours
              case 'MIN_DAILY_HRS':
                var sql  = "Select distinct HRS_DAILY_MIN from Staff where HRS_DAILY_MIN != null Or HRS_DAILY_MIN != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "MIN_DAILY_HRS";
              break;
              case 'MAX_DAILY_HRS':
                var sql  = "Select distinct HRS_DAILY_MAX from Staff where HRS_DAILY_MAX != null Or HRS_DAILY_MAX != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "MAX_DAILY_HRS";
              break;
              case 'MIN_WEEKLY_HRS':
                var sql  = "Select distinct HRS_WEEKLY_MIN from Staff where HRS_WEEKLY_MIN != null Or HRS_WEEKLY_MIN != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "MIN_WEEKLY_HRS";
              break;
              case 'MAX_WEEKLY_HRS':
                var sql  = "Select distinct HRS_WEEKLY_MAX from Staff where HRS_WEEKLY_MAX != null Or HRS_WEEKLY_MAX != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "MAX_WEEKLY_HRS";
              break;
              case 'MIN_PAY_PERIOD_HRS':
                var sql  = "Select distinct HRS_FNIGHTLY_MIN from Staff where HRS_FNIGHTLY_MIN != null Or HRS_FNIGHTLY_MIN != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "MIN_PAY_PERIOD_HRS";
              break;
              case 'MAX_PAY_PERIOD_HRS':
                var sql  = "Select distinct HRS_FNIGHTLY_MAX from Staff where HRS_FNIGHTLY_MAX != null Or HRS_FNIGHTLY_MAX != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "MAX_PAY_PERIOD_HRS";
              break;
              case 'WEEK_1_DAY_1':
                var sql  = "Select distinct CH_1_1 from Staff where CH_1_1 != null Or CH_1_1 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_1";
              break;
              case 'WEEK_1_DAY_2':
                var sql  = "Select distinct CH_1_2 from Staff where CH_1_2 != null Or CH_1_2 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_2";
              break;
              case 'WEEK_1_DAY_3':
                var sql  = "Select distinct CH_1_3 from Staff where CH_1_3 != null Or CH_1_3 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_3";
              break;
              case 'WEEK_1_DAY_4':
                var sql  = "Select distinct CH_1_4 from Staff where CH_1_4 != null Or CH_1_4 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_4";
              break;
              case 'WEEK_1_DAY_5':
                var sql  = "Select distinct CH_1_5 from Staff where CH_1_5 != null Or CH_1_5 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_5";
              break;
              case 'WEEK_1_DAY_6':
              var sql  = "Select distinct CH_1_6 from Staff where CH_1_6 != null Or CH_1_6 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_6";
              break;
              case 'WEEK_1_DAY_7':
                var sql  = "Select distinct  from Staff where != null Or  != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_1_DAY_7";
              break;
              case 'WEEK_2_DAY_1':
                var sql  = "Select distinct CH_2_1 from Staff where CH_2_1 != null Or CH_2_1 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_1";
              break;
              case 'WEEK_2_DAY_2':
                var sql  = "Select distinct CH_2_2 from Staff where CH_2_2 != null Or CH_2_2 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_2";
              break;
              case 'WEEK_2_DAY_3':
              var sql  = "Select distinct CH_2_3 from Staff where CH_2_3 != null Or CH_2_3 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_3";
              break;
              case 'WEEK_2_DAY_4':
                var sql  = "Select distinct CH_2_4 from Staff where CH_2_4 != null Or CH_2_4 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_4";
              break;
              case 'WEEK_2_DAY_5':
                var sql  = "Select distinct CH_2_5  from Staff where CH_2_5 != null Or CH_2_5 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_5";
              break;
              case 'WEEK_2_DAY_6':
                var sql  = "Select distinct CH_2_6  from Staff where CH_2_6 != null Or CH_2_6 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_6";
              break;
              case 'WEEK_2_DAY_7':
                this.ConditionEntity =  '  AS WEEK_2_DAY_7'
                var sql  = "Select distinct CH_2_7 from Staff where CH_2_7 != null Or CH_2_7 != '' "
                this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                this.ModalTitle = "WEEK_2_DAY_7";
              break;
            //Staff OP Notes
            case 'General Notes':
                  var sql  = "Select distinct [STF_NOTES] from STAFF where  [STF_NOTES] != null Or [STF_NOTES] != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "General Notes";
                  break;
                case 'OP Notes Date':
                  var sql  = "Select distinct [DetailDate] from History where ExtraDetail1 = 'OPNOTE' and [DetailDate] != null Or [DetailDate] != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "OP Notes Date";
                  break;
                  /*
                case 'OP Notes Detail':
                  this.ConditionEntity =  " dbo.RTF2TEXT(OPHistory.[Detail]) as [OP Notes Detail],"
                  var sql  = "Select distinct  from Recipients where != null Or  != '' "
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "";
                  break; */
                case 'OP Notes Creator':
                  var sql  = "Select distinct [Creator] from History where ExtraDetail1 = 'OPNOTE' and [Creator] != null Or [Creator] != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "OP Notes Creator";
                  break;
                case 'OP Notes Alarm':
                  var sql  = "Select distinct format([AlarmDate],'dd/MM/yyyy') as [AlarmDate] from History where ExtraDetail1 = 'OPNOTE' and [AlarmDate] != null Or [AlarmDate] != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "OP Notes Alarm";
                  break;
                case 'OP Notes Category':
                  var sql  = "Select  CASE WHEN IsNull(ExtraDetail2,'') < 'A' THEN 'UNKNOWN' ELSE ExtraDetail2 END AS [OP Notes Category] from History where ExtraDetail1 = 'OPNOTE' and != null Or  != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "OP Notes Category";                
                  break;
                  //Staff Incident 
                case 'INCD_Status' :
                  var sql  = "Select distinct Status from IM_Master where Status != null Or Status != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Status";
                  break;
              case 'INCD_Date' :
                  var sql  = "Select distinct format(Date,'dd/MM/yyyy') as Date from IM_Master where Date != null Or Date != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Date";
                break;
              case 'INCD_Type' :
                  var sql  = "Select distinct [Type] from IM_Master where [Type] != null Or [Type] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Type";
                break;
              case 'INCD_Description' :
                  this.ConditionEntity =  ' StaffIncidents.'
                  var sql  = "Select distinct ShortDesc from IM_Master where ShortDesc != null Or ShortDesc != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Description";
                break;
              case 'INCD_SubCategory' :
                  var sql  = "Select distinct [PerpSpecify] from IM_Master where [PerpSpecify] != null Or [PerpSpecify] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_SubCategory";
                break;
              case 'INCD_Assigned_To' :
                  var sql  = "Select distinct [CurrentAssignee] from IM_Master where [CurrentAssignee] != null Or [CurrentAssignee] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Assigned_To";
                break;
              case 'INCD_Service' :
                  var sql  = "Select distinct [Service] from IM_Master where [Service] != null Or [Service] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Service";
                break;
              case 'INCD_Severity' :
                  var sql  = "Select distinct [Severity] from IM_Master where [Severity] != null Or [Severity] != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Severity";
                break;
              case 'INCD_Time' :
                  var sql  = "Select distinct [Time] from IM_Master where [Time] != null Or [Time] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Time";
                break;
              case 'INCD_Duration' :
                  var sql  = "Select distinct Duration from IM_Master where Duration != null Or Duration != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Duration";
                break;
              case 'INCD_Location' :
                  var sql  = "Select distinct [Location] from IM_Master where [Location] != null Or [Location] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Location";
                break;
              case 'INCD_LocationNotes' :
                  var sql  = "Select distinct [LocationNotes] from IM_Master where [LocationNotes] != null Or [LocationNotes] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_LocationNotes";
                break;
              case 'INCD_ReportedBy' :
                  var sql  = "Select distinct [ReportedBy] from IM_Master where [ReportedBy] != null Or [ReportedBy] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_ReportedBy";
                break;
              case 'INCD_DateReported' :
                  var sql  = "Select distinct format([DateReported],'dd/MM/yyyy') as [DateReported] from IM_Master where [DateReported] != null Or [DateReported] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_DateReported";
                break;
              case 'INCD_Reported' :
                  var sql  = "Select distinct CASE WHEN Reported = 1 THEN 'YES' ELSE 'NO' END AS [INCD_Reported ] from IM_Master where != null Or  != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Reported";
              case 'INCD_FullDesc' :
                  var sql  = "Select distinct [FullDesc]  from IM_Master where [FullDesc] != null Or [FullDesc] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_FullDesc";
                break;
              case 'INCD_Program' :
                  var sql  = "Select distinct [Program ] from IM_Master where [Program ] != null Or [Program ] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Program";
                break;
              case 'INCD_DSCServiceType' :
                  var sql  = "Select distinct [DSCServiceType] from IM_Master where [DSCServiceType] != null Or [DSCServiceType] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_DSCServiceType";
                break;
              case 'INCD_TriggerShort' :
                  var sql  = "Select distinct [TriggerShort] from IM_Master where [TriggerShort] != null Or [TriggerShort] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_TriggerShort";
                break;
              case 'INCD_incident_level' :
                  var sql  = "Select distinct [incident_level] from IM_Master where [incident_level] != null Or [incident_level] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_incident_level";
                break;
              case 'INCD_Area' :
                  var sql  = "Select distinct [area] from IM_Master where [area] != null Or [area] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Area";
                break;
              case 'INCD_Region' :
                  var sql  = "Select distinct [Region] from IM_Master where [Region] != null Or [Region] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Region";
                break;
              case 'INCD_position' :
                  var sql  = "Select distinct [position] from IM_Master where [position] != null Or [position] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_position";
                break;
              case 'INCD_phone' :
                  var sql  = "Select distinct [phone] from IM_Master where [phone] != null Or [phone] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_phone";
                break;
              case 'INCD_verbal_date' :
                  var sql  = "Select distinct format([verbal_date],'dd/MM/yyyy') as [verbal_date] from IM_Master where [verbal_date] != null Or [verbal_date] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_verbal_date";
                break;
              case 'INCD_verbal_time' :
                  var sql  = "Select distinct [verbal_time] from IM_Master where [verbal_time] != null Or [verbal_time] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_verbal_time";
                break;
              case 'INCD_By_Whome' :
                  var sql  = "Select distinct [By_Whome] from IM_Master where [By_Whome] != null Or [By_Whome] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_By_Whome";
                break;
              case 'INCD_To_Whome' :
                  var sql  = "Select distinct [To_Whome] from IM_Master where [To_Whome] != null Or [To_Whome] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_To_Whome";
                break;
              case 'INCD_BriefSummary' :
                  var sql  = "Select distinct [BriefSummary] from IM_Master where [BriefSummary] != null Or [BriefSummary] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_BriefSummary";
                break;
              case 'INCD_ReleventBackground' :
                  var sql  = "Select distinct [ReleventBackground] from IM_Master where [ReleventBackground] != null Or [ReleventBackground] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_ReleventBackground";
                break;
              case 'INCD_SummaryofAction' :
                  var sql  = "Select distinct [SummaryofAction] from IM_Master where [SummaryofAction] != null Or [SummaryofAction] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_SummaryofAction";
                break;
              case 'INCD_SummaryOfOtherAction' :
                  var sql  = "Select distinct [SummaryOfOtherAction] from IM_Master where [SummaryOfOtherAction] != null Or [SummaryOfOtherAction] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_SummaryOfOtherAction";
                break;
              case 'INCD_Triggers' :
                  var sql  = "Select distinct [Triggers] from IM_Master where [Triggers] != null Or [Triggers] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Triggers";
                break;
              case 'INCD_InitialAction' :
                  var sql  = "Select distinct [InitialAction] from IM_Master where [InitialAction] != null Or [InitialAction] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_InitialAction";
                break;
              case 'INCD_InitialNotes' :
                  var sql  = "Select distinct [InitialNotes] from IM_Master where [InitialNotes] != null Or [InitialNotes] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_InitialNotes";
                break;
              case 'INCD_InitialFupBy' :
                  var sql  = "Select distinct [InitialFupBy] from IM_Master where [InitialFupBy] != null Or [InitialFupBy] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_InitialFupBy";
                break;
              case 'INCD_Completed' :
                  var sql  = "Select distinct CASE WHEN Completed = 1 THEN 'YES' ELSE 'NO' END AS [INCD_Completed ] from IM_Master where != null Or  != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Completed";
                  break;
              case 'INCD_OngoingAction' :
                  var sql  = "Select distinct [OngoingAction] from IM_Master where [OngoingAction] != null Or [OngoingAction] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_OngoingAction";
                break;
              case 'INCD_OngoingNotes' :
                  var sql  = "Select distinct [OngoingNotes] from IM_Master where [OngoingNotes] != null Or [OngoingNotes] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_OngoingNotes";
                break;
              case 'INCD_Background' :
                  var sql  = "Select distinct [Background] from IM_Master where [Background] != null Or [Background] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Background";
                break;
              case 'INCD_Abuse' :
                  var sql  = "Select distinct [Abuse] from IM_Master where [Abuse] != null Or [Abuse] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Abuse";
                break;
              case 'INCD_DOPWithDisability' : //check
                  var sql  = "Select distinct CASE WHEN StaffIncidents.DOPWithDisability  = 1 THEN 'YES' ELSE 'NO' END AS [INCD_DOPWithDisability ] from IM_Master where DOPWithDisability != null Or DOPWithDisability != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_DOPWithDisability";
                  break;
              case 'INCD_SeriousRisks' :
                  var sql  = "Select distinct [SeriousRisks] from IM_Master where [SeriousRisks] != null Or [SeriousRisks] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_SeriousRisks";
                break;
              case 'INCD_Complaints' :
                  var sql  = "Select distinct [Complaints] from IM_Master where [Complaints] != null Or [Complaints] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Complaints";
                break;
              case 'INCD_Perpetrator' :
                  var sql  = "Select distinct [Perpetrator] from IM_Master where [Perpetrator] != null Or [Perpetrator] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Perpetrator";
                break;
              case 'INCD_Notify' :
                  var sql  = "Select distinct [Notify] from IM_Master where [Notify] != null Or [Notify] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Notify";
                break;
              case 'INCD_NoNotifyReason' :
                  var sql  = "Select distinct [NoNotifyReason] from IM_Master where [NoNotifyReason] != null Or [NoNotifyReason] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_NoNotifyReason";
                break;
              case 'INCD_Notes' :
                  var sql  = "Select distinct [Notes] from IM_Master where [Notes] != null Or [Notes] != '' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Notes";
                break;
              case 'INCD_Setting' :
                  var sql  = "Select distinct [Setting] from IM_Master where [Setting] != null Or [Setting] != '' "
                  this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                  this.ModalTitle = "INCD_Setting";
                  break;
              //Doing 
              //Staff Attribute     
                  case 'Competency': 
                  var sql  = " Select distinct Description from DataDomains where Domain = 'STAFFATTRIBUTE' "
                  this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                  this.ModalTitle = "Competency";
                break;
                case 'Competency Expiry Date':  
                    var sql  = "Select distinct [Date1] from HumanResources where [Group] = 'STAFFATTRIBUTE' and [Date1] != null Or [Date1] != '' "
                    this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                    this.ModalTitle = "Competency Expiry Date";
                    break;
                case 'Competency Reminder Date':
                    var sql  = "Select distinct [Date2] from HumanResources where [Group] = 'STAFFATTRIBUTE' and [Date2] != null Or [Date2] != '' "
                    this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                    this.ModalTitle = "Competency Reminder Date";
                    break;
                case 'Competency Completion Date':
                    var sql  = "Select distinct [DateInstalled] from HumanResources where [Group] = 'STAFFATTRIBUTE' and [DateInstalled] != null Or [DateInstalled] != '' "
                    this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                    this.ModalTitle = "Competency Completion Date";
                    break;
                case 'Certificate Number':
                    var sql  = "Select distinct [Address1] from HumanResources where [Group] = 'STAFFATTRIBUTE' and [Address1] != null Or [Address1] != '' "
                    this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                    this.ModalTitle = "Certificate Number";
                    break;
                case 'Mandatory Status':                                      
                    var sql  = "Select distinct [Recurring] from HumanResources where [Group] = 'STAFFATTRIBUTE' and [Recurring] != null Or [Recurring] != '' "
                    this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                    this.ModalTitle = "Mandatory Status";
                    break;
                case 'Competency Notes':
                    var sql  = "Select distinct [Notes] from HumanResources where [Group] = 'STAFFATTRIBUTE' and [Notes] != null Or [Notes] != '' "
                    this.bodystyleModal_IN = { height:'500px' , overflow: 'auto' };
                    this.ModalTitle = "Competency Notes";
                    break;
                case 'Staff Position':
                    var sql  = "Select distinct [Name] from HumanResources where [Group] = 'STAFFPOSITION' and [Name] != null Or [Name] != '' "
                    this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                    this.ModalTitle = "Staff Position";
                    break;
                case 'Staff Admin Categories': 
                    var sql  = "Select distinct Description from DataDomains where Domain = 'STAFFADMINCAT' "
                    this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
                    this.ModalTitle = "Staff Admin Categories";
                    break;
                case 'NDIA Staff Level':
                    var sql  = "Select distinct [NDIAStaffLevel] from Staff where [NDIAStaffLevel] != null Or [NDIAStaffLevel] != '' "
                    this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                    this.ModalTitle = "NDIA Staff Level";
                    break;
                



            


            default:

            break;
            }
        }else{
          //Recipients user reports
          switch(keys){                
            //NAME AND ADDRESS
            case 'First Name':              
              var sql = "Select distinct FirstName from Recipients where FirstName != null Or FirstName != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "First Names";
              break;
              case 'Title':
                var sql = "select Distinct Title from Recipients where title != null or Title != ''"                
                this.bodystyleModal_IN = { height:'200px' };
                this.ModalTitle = "Titles";
              break;
              case 'Surname/Organisation':              
              var sql = "Select distinct [Surname/Organisation] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Surname/Organisation";       
              break;
              case 'Preferred Name':          
              var sql = "Select distinct PreferredName from Recipients where PreferredName != null Or PreferredName != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Preferred Name";
              break;                  
              case 'Other':   
              
              var sql = "Select distinct AliAs from Recipients where AliAs != null Or AliAs != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Other";                              
              break;
              case 'Address-Line1':
              
              var sql = "Select distinct Address1 from Recipients where Address1 != null Or Address1 != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Address-Line1";      
              break;
              case 'Address-Line2':
              this.ConditionEntity ='R.Address2'
              var sql = "Select distinct Address2 from Recipients where Address2 != null Or Address2 != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Address-Line2";     
              break;
              case 'Address-Suburb':
                var sql = "Select distinct Suburb from Recipients where Suburb != null Or Suburb != ''"                
                this.bodystyleModal_IN = { height:'600px' ,overflow: 'auto' };
                this.ModalTitle = "Address-Suburbs";     
              break;
              case 'Address-Postcode':
                var sql = "Select distinct Postcode from Recipients where Postcode != null Or Postcode != '' "                 
                this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                this.ModalTitle = "Address-Postcodes";        
              break;
              case 'Address-State':
                var sql = "Select distinct state from Recipients where state != null Or State != ''"              
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Address-States";                     
              break;
              //General Demographics             
              case 'Full Name-Surname First':  
              var sql = "Select top 10 Surname/Organisation from Recipients "
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Full Name-Surname First";
              break;
              case 'Full Name-Mailing':
                var sql = "Select distinct [Surname/Organisation] " + ' ' +   "[FirstName] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "                 
                this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                this.ModalTitle = "Full Name-Mailing";
              break;
              /* 
              case 'Gender':
              this.ConditionEntity = 'R.Gender'
              var sql = "Select distinct  from Recipients where  != null Or  != '' "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "";
              break;
              case 'Date Of Birth':
              this.ConditionEntity = 'R.DateOfBirth'
              break;            
              case 'Age':
              this.ConditionEntity = ' DateDiff(YEAR,R.Dateofbirth,GetDate())'
              break;
              case 'Ageband-Statistical':
              this.ConditionEntity = "DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() )"
              break;
              case 'Ageband-5 Year':
              this.ConditionEntity = "DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() )"
              break;
              case 'Ageband-10 Year':
              this.ConditionEntity = "DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() )"
              break;
              case 'Age-ATSI Status':
              //  this.ConditionEntity =''
              break;
              case 'Month Of Birth':
              this.ConditionEntity =   'DateName(Month, DateOfBirth)'
              break;
              case 'Day Of Birth No.':
              this.ConditionEntity =   'DateName(Weekday, DateOfBirth)'
              break;
              case 'CALD Score':
              this.ConditionEntity =    'DataDomains.CALDStatus'
              break;                     */
              case 'Country Of Birth':
                var sql  = "Select distinct Description from DataDomains where Domain = 'COUNTRIES'"                 
                this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                this.ModalTitle = "Country Of Birth";
              break;
              case 'Language':            
              var sql  = "Select distinct Description from DataDomains where Domain = 'LANGUAGE' "
                this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                this.ModalTitle = "Languages";
              break;              
              case 'Indigenous Status':              
              var sql  = "Select distinct Description from DataDomains where Domain = 'INDIGINOUS STATUS' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Indigenous Status";
              break;
              case 'Primary Disability':                
              var sql  = " Select distinct Description from DataDomains where Domain = 'CSTDA_DISABILITYGROUP' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Primary Disabilities";
              break;
              case 'Financially Dependent':              
              var sql  = "Select distinct FDP from Recipients where FDP != null Or FDP != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Financially Dependent";
              break;
              case 'Financial Status':               
              var sql  = "Select distinct Description from DataDomains where Domain = 'FINANCIALCLASS' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Financial Status";
              break;          
              case 'Occupation':  
              var sql  = "Select distinct Occupation from Recipients where Occupation != null Or Occupation != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Occupation";
              break;
              //Admin Info
              case 'UniqueID':              
              var sql  = "Select distinct UniqueID from Recipients where UniqueID != null Or UniqueID != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Unique ID";
              break;
              case 'Code':              
              var sql  = "Select distinct AccountNo from Recipients where AccountNo != null Or AccountNo != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Code";
              break;
              case 'Type':               
              var sql  = "Select distinct Type from Recipients where Type != null Or Type != '' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Type";
              break;             
              case 'Category':
                var sql = "Select distinct AgencyDefinedGroup from Recipients where AgencyDefinedGroup != null Or AgencyDefinedGroup != '' "                 
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Categories";
              break;
              case 'CoOrdinator':
                var sql = "Select distinct Description from DataDomains where Domain = 'CASE MANAGERS' "                 
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "CoOrdinator"; 
              break;
              case 'Admitting Branch':                        
              var sql  = "Select distinct Description from DataDomains where Domain = 'BRANCHES' "                 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Branches";
              break;
              case 'Secondary Branch':                   
              var sql  = "Select distinct  HR.name from HumanResources HR  left join Recipients R on hr.PersonID = R.UniqueID where HR.[Type] = 'RECIPBRANCHES' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Secondary Branches";
              break;  
              case 'File number':                  
              var sql  = "Select distinct AgencyIDReportingCode from Recipients where AgencyIDReportingCode != null Or AgencyIDReportingCode != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "File numbers";
              break;
              case 'File Number 2':                
              var sql  = "Select distinct URNumber from Recipients where URNumber != null Or URNumber != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "File Number";
              break;                     
              case 'NDIA/MAC Number':                
              var sql  = "Select distinct NDISNumber from Recipients where NDISNumber != null Or NDISNumber != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "NDIA/MAC Numbers";
              break;    
              /* 
              case 'Last Activated Date':
                this.ConditionEntity ='R.ADMISSIONDATE'
              break;
              case 'Created By':
              this.ConditionEntity = 'R.CreatedBy'
              break;
              case 'Other':
              //  this.ConditionEntity =''
              break;  */
              //Staff       
              case 'Staff Name':
                var sql = "Select distinct FIRSTNAME + ' ' +LASTNAME AS [Staff Name] from staff  where FIRSTNAME != null Or  FIRSTNAME != ''   "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Staff Names";
              break;
              case 'Program Name':
                var sql = "Select distinct [Address1] as [Program Name] from HumanResources  where [Address1] != null Or  [Address1] != ''     "                 
                  this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                  this.ModalTitle = "Program Name";
              break;
              /*
              case 'Notes':
              this.ConditionEntity =  'R.Notes'
              break; */
              //Other Genral info
              case 'OH&S Profile':                
              var sql  = "Select distinct OHSProfile from Recipients where OHSProfile != null Or OHSProfile != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "OH&S Profile";
              break;
              case 'OLD WH&S Date':                
              var sql  = "Select distinct [WH&S] from Recipients where [WH&S] != null Or [WH&S]  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "OLD WH&S Date";
              break;
              case 'Billing Profile':                
              var sql  = "Select distinct BillProfile from Recipients where BillProfile != null Or BillProfile != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Billing Profile";
              break;                       
              case 'Sub Category':                
              var sql  = "Select distinct UBDMap from Recipients where UBDMap != null Or UBDMap != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Sub Categories";
              break;
            //  case 'Roster Alerts':
            //  this.ConditionEntity =  'R.[Notes]'
            //  break;
            /*
              case 'Timesheet Alerts':                
              var sql  = "Select distinct SpecialConsiderations from Recipients where SpecialConsiderations != null Or SpecialConsiderations  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Timesheet Alerts";
              break;    */             
              case 'Contact Issues':                
              var sql  = "Select distinct ContactIssues from Recipients where ContactIssues != null Or ContactIssues != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Issues";
              break;
              case 'Survey Consent Given':                
              var sql  = "Select distinct SurveyConsent from Recipients where SurveyConsent != null Or SurveyConsent != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Survey Consent";
              break;
              /*
              case 'Copy Rosters Enabled':
              this.ConditionEntity =  'R.Autocopy'
              break;           
              case 'Activation Date':
              this.ConditionEntity =  'R.[AdmissionDate]'
              break;
              case 'DeActivation Date':
              this.ConditionEntity =  'R.[DischargeDate]'
              break; */
              case 'Mobility':                
              var sql  = "Select distinct Description from DataDomains where Domain = 'MOBILITY' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Mobilities";
              break;
              case 'Specific Competencies':
                var sql = "Select distinct SpecialConsiderations from Recipients where SpecialConsiderations != null Or SpecialConsiderations != '' "                 
                this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
                this.ModalTitle = "Specific Competencies";
              break;                           
              //  Contacts & Next of Kin
              case 'Contact Group':                
              var sql  = "Select distinct HR.[Group]    From HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [Group] != null Or [Group] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Group";
              break;
              case 'Contact Type':                
              var sql  = "Select distinct [Type] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and Type != null Or Type != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Types";
              break;
              case 'Contact Sub Type':                
              var sql  = "Select distinct [SubType] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [SubType] != null Or [SubType] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Sub Type";
              break;
              case 'Contact User Flag':                
              var sql  = "Select distinct [User1] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [User1] != null Or [User1] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact User Flag(s)";
              break;
              case 'Contact Person Type':                
              var sql  = "Select distinct EquipmentCode from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and EquipmentCode != null Or EquipmentCode != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Person Type";
              break;
              case 'Contact Name':                
              var sql  = "Select distinct [Name] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [Name] != null Or [Name] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Names";
              break;
              case 'Contact Address':                
              var sql  = "Select distinct [Address1] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [Address1] != null Or [Address1] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Address";
              break;
              case 'Contact Suburb':                
              var sql  = "Select distinct [Suburb] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [Suburb] != null Or [Suburb] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Suburb";
              break;                
              case 'Contact Postcode':                
              var sql  = "Select distinct [Postcode] from HumanResources HR  inner join Recipients R on HR.PersonID = R.UniqueID where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and [Postcode] != null Or [Postcode] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Contact Postcode";
              break;
              /*
              case 'Contact Phone 1':
              this.ConditionEntity =   'HR.[Phone1]'
              var sql  = "Select distinct  from HumanResources where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Contact Phone 2':
              this.ConditionEntity =  'HR.[Phone2]'
              var sql  = "Select distinct  from HumanResources where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Contact Mobile':
              this.ConditionEntity =  'HR.[Mobile]'
              var sql  = "Select distinct  from HumanResources where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Contact FAX':
              this.ConditionEntity =  'HR.[FAX]'
              var sql  = "Select distinct  from HumanResources where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Contact Email':
              this.ConditionEntity =  'HR.[Email]'
              var sql  = "Select distinct  from HumanResources where [Group] IN ('NEXTOFKIN',  'CONTACT',   '1-NEXT OF KIN',   'OTHERCONTACT') and != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break; */

              //Carer Info      
              case 'Carer First Name':
              var sql  = "Select distinct FirstName from Recipients where FirstName != null Or FirstName != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "First Name"; 
              break;
              case 'Carer Last Name':
              var sql  = "Select distinct [Surname/Organisation] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Carer Last Names";
              break;
              /*
              case 'Carer Age':
              this.ConditionEntity =  'DateDiff(YEAR,C.Dateofbirth,GetDate())'
              break;                  */
              case 'Carer Gender':
              this.ConditionEntity =  'C.[Gender]'
              var sql  = "Select distinct Description from DataDomains where Domain = 'SEX' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Carer Gender";
              break;
              case 'Carer Indigenous Status':
              var sql  = "Select distinct Description from DataDomains where Domain = 'INDIGINOUS STATUS' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Carer Indigenous Status";
              break;                       
              case 'Carer Address':
              var sql  = "Select distinct N.Address1 + ' ' +  N.Suburb + ' ' + N.Postcode  from NamesAndAddresses where Address1 != null Or Address1 != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Carer Address";
              break; /*
              case 'Carer Email':
              this.ConditionEntity =  'PE.Detail'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Carer Phone <Home>':
              this.ConditionEntity =  'PhHome.Detail'
              break;
              case 'Carer Phone <Work>':
              this.ConditionEntity =  'PhWork.Detail'
              break;
              case 'Carer Phone <Mobile>':
              this.ConditionEntity =  'PhMobile.Detail'
              break; 
              // Documents                
              case 'DOC_ID':
              this.ConditionEntity =  'doc.DOC_ID'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break; */
              case 'Doc_Title':
              var sql  = "Select distinct Title from DOCUMENTS where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document Title(s)";
              break;
              case 'Created':
              var sql  = "Select distinct Created from DOCUMENTS where Created != null Or Created != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document Creater(s)";
              break;                
              case 'Modified':
              var sql  = "Select distinct Modified from DOCUMENTS where Modified != null Or Modified != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document Modified";
              break;
              case 'Status':
              var sql  = "Select distinct Status from DOCUMENTS where Status != null Or Status != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document Status";
              break;                                
              case 'Classification':
              var sql  = "Select distinct Classification from DOCUMENTS where Classification != null Or Classification != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document Classification";
              break;
              case 'Category':
              var sql  = "Select distinct Category from DOCUMENTS where Category != null Or Category != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document Categories";
              break;
              case 'Filename':
              var sql  = "Select distinct Filename from DOCUMENTS  Filename != null Or Filename != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "File Names";
              break; /*
              case 'Doc#':
              this.ConditionEntity =  'doc.Doc#'
              break;
              case 'DocStartDate':
              this.ConditionEntity =  'doc.DocStartDate'
              break;
              case 'DocEndDate':
              this.ConditionEntity =  'doc.DocEndDate'
              break; */
              case 'AlarmDate':
              var sql  = "Select distinct AlarmDate from DOCUMENTS where AlarmDate != null Or AlarmDate != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Document AlarmDate";
              break;     /*                     
              case 'AlarmText':
              this.ConditionEntity =  'doc.AlarmText'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break; */
              //Consents                  
              case 'Consent':                              
              var sql  = "Select Distinct Cons.[Name]   From Recipients R left join HumanResources Cons on Cons.PersonID = R.UniqueID where [Name] != null Or [Name] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Consent";
              break;
              case 'Consent Start Date':                
              var sql  = "Select Distinct Cons.[Name]   From Recipients R left join HumanResources Cons on Cons.PersonID = R.UniqueID where [Date1] != null Or [Date1] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Consent Start Date";
              break;                  
              case 'Consent Expiry':                
              var sql  = "Select Distinct Cons.[Date2]   From Recipients R left join HumanResources Cons on Cons.PersonID = R.UniqueID where [Date2] != null Or [Date2] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Consent Expiry";
              break;
              case 'Consent Notes':                
              var sql  = "Select Distinct Cons.[Notes]   From Recipients R left join HumanResources Cons on Cons.PersonID = R.UniqueID where [Notes] != null Or [Notes] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Consent Notes";
              break;
              //  GOALS OF CARE                  
            case 'Goal':                
              var sql  = "Select distinct Description from DataDomains where Domain = 'GOALOFCARE' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Goal(s)";
              break;               
            /* case 'Goal Detail':
              this.ConditionEntity =  'Goalcare.[Notes]'
              var sql  = "Select distinct Notes from Humanresources where [Group] = 'RECIPIENTGOALS' and Notes != null Or Notes != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break; */
              case 'Goal Achieved':                
              var sql  = "Select distinct Completed from Humanresources where [Group] = 'RECIPIENTGOALS' and Completed != null Or Completed != '' "
              this.ModalTitle = "Goal Archieved";
              break;
              case 'Anticipated Achievement Date':                
              var sql  = "Select distinct format(HumanResources.Date1,'dd/MM/yyyy') as Date1 from Humanresources where [Group] = 'RECIPIENTGOALS' and [Date1] != null Or [Date1] != ''"
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Date Achieved':
              var sql  = "Select distinct format(DateInstalled,'dd/MM/yyyy') as Date1 from Humanresources where [Group] = 'RECIPIENTGOALS' and DateInstalled != null Or DateInstalled != ''"
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Date Achieved";
              break;                  
              case 'Last Reviewed':                
              var sql  = "Select distinct format(Date2,'dd/MM/yyyy') as Date2 from Humanresources where [Group] = 'RECIPIENTGOALS' and Date2 != null Or Date2 != ''"
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Last Reviwed";
              break;          
              case 'Logged By':              
              var sql  = "Select distinct Creator from Humanresources where [Group] = 'RECIPIENTGOALS' and Creator != null Or Creator != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Logged By";
              break;
              //REMINDERS                  
            case 'Reminder Detail':              
              var sql  = "Select distinct [Name] from HumanResources where [Group] = 'RECIPIENTALERT' and [Name] != null Or [Name] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Reminder Detail(s)";
              break;
              case 'Event Date':
              this.ConditionEntity =  'Remind.[Date2]'
              var sql  = "Select distinct format(Date2,'dd/MM/yyyy') as Date2 from Humanresources where [Group] = 'RECIPIENTALERT' and Date2 != null Or Date2 != ''"
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "Event Date";
              break;                                    
              case 'Reminder Date':
              var sql  = "Select distinct format(Date1,'dd/MM/yyyy') as Date1 from Humanresources where [Group] = 'RECIPIENTALERT' and Date1 != null Or Date1 != ''"
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "Reminder Date";
              break;
              /*
              case 'Reminder Notes':
              this.ConditionEntity =  'Remind.[Notes]'
              break; */
              // USER GROUPS                  
            case 'Group Name':                
              var sql  = "Select distinct [Name] from Humanresources where [Group] = 'RECIPTYPE' and [Name] != null Or [Name] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Group Name(s)";
              break; /*
              case 'Group Note':
              this.ConditionEntity = 'UserGroup.[Notes]'
              var sql  = "Select distinct [Notes] from Humanresources where [Notes] != null Or [Notes] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = ""; 
              break; */
              case 'Group Start Date':              
              var sql  = "Select distinct format(Date1,'dd/MM/yyyy') as Date1 from Humanresources where [Group] = 'RECIPTYPE' and Date1 != null Or Date1 != ''"
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Group Start Date";
              break;                                 
              case 'Group End Date':            
                var sql  = "Select distinct format(Date2,'dd/MM/yyyy') as Date2 from Humanresources where [Group] = 'RECIPTYPE' and Date2 != null Or Date2 != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Group End Date(s)";
              break;
              case 'Group Email':              
              var sql  = "Select distinct [Email] from Humanresources where [Group] = 'RECIPTYPE' and [Email] != null Or [Email] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Group Emails";
              break; 
              //Preferences             
              case 'Preference Name':
              this.ConditionEntity = 'Prefr.[Name]' 
              var sql  = "Select distinct [Name] from HumanResources where [Group] = 'STAFFPREF' and [Name] != null Or [Name] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Prefrence Name";
              break; /*
              case 'Preference Note':
              this.ConditionEntity =  'Prefr.[Notes]'
              break; */
              // FIXED REVIEW DATES                   
              case 'Review Date 1':
              var sql  = "Select distinct [OpReASsessmentDate] from Recipients where [OpReASsessmentDate] != null Or [OpReASsessmentDate] != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Review Date 1";
              break;
              case 'Review Date 2':
              var sql  = "Select distinct ClinicalReASsessmentDate from Recipients where ClinicalReASsessmentDate != null Or ClinicalReASsessmentDate != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Review Date 2";
              break;
              case 'Review Date 3':
              var sql  = "Select distinct FileReviewDate from Recipients where FileReviewDate != null Or FileReviewDate != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Review Date 3";
              break;           
              //Staffing Inclusions/Exclusions                  
              case 'Excluded Staff':
              var sql  = "Select Distinct Name from HumanResources ExcludeS where ExcludeS.[Type] = 'EXCLUDEDSTAFF' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Excluded Staff";
              break;/*
              case 'Excluded_Staff Notes':
              this.ConditionEntity =  'ExcludeS.[Notes]'
              break;   */                                 
              case 'Included Staff':                
              var sql  = "Select Distinct Name from HumanResources IncludS where IncludS.[TYPE] = 'INCLUDEDSTAFF' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Included Staff";
              break;/*
              case 'Included_Staff Notes':
              this.ConditionEntity =  'IncludS.[Notes]'
              break;*/ 
              // AGREED FUNDING INFORMATION                         
              case 'Funding Source':
              var sql  = "Select distinct [Type] from HumanResourceTypes where [Type] != null Or [Type] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Funding Source(s)"; 
              break;
              case 'Funded Program':
              var sql  = "Select Distinct [Program] from RecipientPrograms where [Program] != null Or [Program] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Funded Program(s)";
              break;
              case 'Funded Program Agency ID':
              var sql  = "Select Distinct [Address1] from HumanResourceTypes  LEFT JOIN RecipientPrograms  ON RecipientPrograms.Program = HumanResourceTypes.Name where [Address1] != null Or [Address1] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Funded Program Agency ID(s)";
              break;                  
              case 'Program Status':
              var sql  = "Select Distinct [ProgramStatus] from RecipientPrograms where [ProgramStatus] != null Or [ProgramStatus] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Program Status";
              break;
              case 'Program Coordinator':
              var sql  = "Select Distinct [Address2] from HumanResourceTypes where [Address2] != null Or [Address2] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Program Coordinator(s)";
              break;
              case 'Funding Start Date':
              var sql  = "Select distinct format([StartDate],'dd/MM/yyyy') as StartDate from RecipientPrograms where [StartDate] != null Or [StartDate] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Funding Start Date";
              break;
              case 'Funding End Date':
              var sql  = "Select distinct format(ExpiryDate,'dd/MM/yyyy') as ExpiryDate from RecipientPrograms where [StartDate] != null Or [StartDate] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Funding End Date";
              break;
              case 'AutoRenew':
              var sql  = "Select Distinct [AutoRenew] from RecipientPrograms where [AutoRenew] != null Or [AutoRenew] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Auto Renew";
              break;                  
              case 'Rollover Remainder':
              var sql  = "Select distinct [RolloverRemainder] from RecipientPrograms where RolloverRemainder != null Or RolloverRemainder != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Rollover Remainder";
              break;
              case 'Funded Qty':
              var sql  = "Select Distinct Quantity from RecipientPrograms  where Quantity != null Or Quantity != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Funded Qunatities";
              break;
              case 'Funded Type':
              var sql  = "Select Distinct [ItemUnit] from RecipientPrograms  where [ItemUnit] != null Or [ItemUnit] != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Funded Type(s)";
              break;
              //case 'Funding Cycle':
              //  this.ConditionEntity =  
              //break;
              case 'Funded Total Allocation':
              var sql  = "Select Distinct [TotalAllocation] from RecipientPrograms  where [TotalAllocation] != null Or [TotalAllocation] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Funded Total Allocation(s)";
              break;
              case 'Used':
              var sql  = "Select Distinct [Used] from RecipientPrograms  where [Used] != null Or [Used] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Used";
              break;
              case 'Remaining':
              var sql  = "Select ([TotalAllocation] - [Used]) as Remaining from RecipientPrograms  where [Used] != null Or [Used] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Remaining";
              break;                
              //LEGACY CARE PLAN              
            case 'Name':
              var sql  = "Select distinct  from CarePlanItem where != null Or  != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Plan Name(s)";
              break;                                    
              case 'Start Date':
              var sql  = "Select distinct format([PlanStartDate],'dd/MM/yyyy') as PlanStartDate from RecipientPrograms where [PlanStartDate] != null Or [PlanStartDate] != ''  "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Start Date(s)";
              break;
              case 'End Date':
              var sql  = "Select distinct format([PlanEndDate],'dd/MM/yyyy') as PlanEndDate from RecipientPrograms where [PlanEndDate] != null Or [PlanEndDate] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "End Date(s)";
              break;    /*              
              case 'Details':
              var sql  = "Select distinct PlanDetail from CarePlanItem where PlanDetail != null Or PlanDetail != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Details";
              break; */
              case 'Reminder Date':
              var sql  = "Select distinct format([PlanReminderDate],'dd/MM/yyyy') as PlanReminderDate from RecipientPrograms where [PlanReminderDate] != null Or [PlanReminderDate] != ''  "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Reminder Date(s)";
              break;/*
              case 'Reminder Text':
              this.ConditionEntity =  'CarePlanItem.[PlanReminderText]'
              var sql  = "Select PlanReminderText distinct  from CarePlanItem where PlanReminderText != null Or PlanReminderText != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Reminder Text";
              break; */
          //Agreed Service Information                   
            case 'Agreed Service Code':
              this.ConditionEntity =  'ServiceOverview.[Service Type]'
              var sql  = "Select distinct [Service Type] from ServiceOverview where [Service Type] != null Or [Service Type] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Code (s)";
              break;
              case 'Agreed Program':
              this.ConditionEntity =  'ServiceOverview.[ServiceProgram]'
              var sql  = "Select distinct ServiceProgram from ServiceOverview where ServiceProgram!= null Or ServiceProgram  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Program (s)";
              break;
              case 'Agreed Service Billing Rate':
              this.ConditionEntity =  'ServiceOverview.[Unit Bill Rate]'
              var sql  = "Select distinct [Unit Bill Rate]  from ServiceOverview where [Unit Bill Rate] != null Or [Unit Bill Rate] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Billing Rate";
              break;
              case 'Agreed Service Status':
              this.ConditionEntity =  'ServiceOverview.[ServiceStatus]'
              var sql  = "Select distinct ServiceStatus from ServiceOverview where ServiceStatus != null Or ServiceStatus != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Status";
              break;
              case 'Agreed Service Duration':
              this.ConditionEntity =  'ServiceOverview.[Duration]'
              var sql  = "Select distinct Duration from ServiceOverview where Duration != null Or Duration != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Duration";
              break;                 
              case 'Agreed Service Frequency':
              var sql  = "Select distinct Frequency from ServiceOverview where Frequency != null Or Frequency != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Frequency";
              break;
              case 'Agreed Service Cost Type':
              var sql  = "Select distinct [Cost Type] from ServiceOverview where [Cost Type] != null Or [Cost Type] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Cost Type";
              break;
              case 'Agreed Service Unit Cost':
              var sql  = "Select distinct [Unit Pay Rate] from ServiceOverview where [Unit Pay Rate] != null Or [Unit Pay Rate] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Unit Cost";
              break;
              case 'Agreed Service Billing Unit':
              var sql  = "Select distinct UnitType from ServiceOverview where UnitType != null Or UnitType != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Billing Unit"; 
              break;
              case 'Agreed Service Debtor':
              var sql  = "Select distinct ServiceBiller from ServiceOverview where ServiceBiller != null Or ServiceBiller != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Debtor";
              break;                  
              case 'Agreed Service Agency ID':
              var sql  = "Select distinct Address1 from ServiceOverview where Address1 != null Or Address1 != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Agreed Service Agency ID";
              break;
              //  CLINICAL INFORMATION               
              /*
            case 'Nursing Diagnosis':
              this.ConditionEntity =  'NDiagnosis.[Description]'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Medical Diagnosis':
              this.ConditionEntity =  'MDiagnosis.[Description]'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;
              case 'Medical Procedure':
              this.ConditionEntity =  'MProcedures.[Description]'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break; */
            //Billing information
              case 'Billing Client':
              var sql  = "Select distinct [BillTo] from Recipients where [BillTo] != null Or [BillTo] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Billing Client(s)";
              break;
              case 'Billing Cycle':
              var sql  = "Select distinct Description from DataDomains where Domain = 'BILLINGCYCLE' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Billing Cycle(s)";
              break;
              case 'Billing Rate':
              var sql  = "Select distinct BillingMethod from Recipients where BillingMethod != null Or BillingMethod != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Billing Rate(s)";
              break;
              case 'Billing %':
              var sql  = "Select distinct PercentageRate from Recipients where PercentageRate != null Or PercentageRate != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Billing Percentage Rate";
              break;            
              case 'Billing Amount':
              var sql  = "Select distinct DonationAmount from Recipients where DonationAmount != null Or DonationAmount != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Donation Amount";
              break;
              case 'Account Identifier':
              var sql  = "Select distinct AccountingIdentifier from Recipients where AccountingIdentifier != null Or AccountingIdentifier != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Account Identifier";
              break;
              case 'External Order Number':
              var sql  = "Select distinct [Order#] from Recipients where [Order#] != null Or [Order#] != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "External Order Number";
              break;
              case 'Financially Dependent':
              var sql  = "Select distinct [FDP] from Recipients where [FDP] != null Or [FDP] != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Financially Dependent";
              break;
              case 'BPay Reference':
              var sql  = "Select distinct BPayRef from Recipients where BPayRef != null Or BPayRef != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Pay Reference";
              break;
              case 'NDIA/MAC ID':
              var sql  = "Select distinct NDISNumber from Recipients where NDISNumber != null Or NDISNumber != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "NDIA/MAC ID";
              break;
              case 'Bill Profile':
              var sql  = "Select distinct BillProfile from Recipients where BillProfile != null Or BillProfile != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Bill Profile";
              break;
              case 'Allow Different Biller':
              var sql  = "Select distinct [RECIPIENT_SPLIT_BILL] from Recipients where [RECIPIENT_SPLIT_BILL] != null Or [RECIPIENT_SPLIT_BILL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Different Biller";
              break;
              case 'Capped Bill':
              var sql  = "Select distinct [CappedBill] from Recipients where [CappedBill] != null Or [CappedBill] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Capped Bill";
              break;
              case 'Hide Fare on Transport Runsheet':
              var sql  = "Select distinct HideTransportFare from Recipients where HideTransportFare != null Or HideTransportFare != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "'Hide Fare on Transport Runsheet";
              break;
              case 'Print Invoice':
              var sql  = "Select distinct PrintInvoice from Recipients where PrintInvoice != null Or PrintInvoice != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Print Invoice";
              break;
              case 'Email Invoice':
              var sql  = "Select distinct EmailInvoice from Recipients where EmailInvoice != null Or EmailInvoice != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Email Invoice";
              break;
              case 'Direct Debit':
              var sql  = "Select distinct DirectDebit from Recipients where DirectDebit != null Or DirectDebit != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "Direct Debit";
              break;
              case 'DVA CoBiller':
              var sql  = "Select distinct DVACoBiller from Recipients where DVACoBiller != null Or DVACoBiller != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "DVA CoBiller";
              break;
          //PANZTEL Timezone 
            case 'PANZTEL Timezone':
              var sql  = "Select distinct [RECIPIENT_TIMEZONE] from Recipients where [RECIPIENT_TIMEZONE] != null Or [RECIPIENT_TIMEZONE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "PANZTEL Timezone";
              break;               
              case 'PANZTEL PBX Site':
              var sql  = "Select distinct [RECIPIENT_PBX] from Recipients where [RECIPIENT_PBX] != null Or [RECIPIENT_PBX] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "PANZTEL PBX Site(s)";
              break;
              case 'PANZTEL Parent Site':
              var sql  = "Select distinct [RECIPIENT_PARENT_SITE] from Recipients where [RECIPIENT_PARENT_SITE] != null Or [RECIPIENT_PARENT_SITE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "PANZTEL Parent Site(s)";
              break;                  
              case 'DAELIBS Logger ID':
              var sql  = "Select distinct DAELIBSID from Recipients where DAELIBSID != null Or DAELIBSID != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "DAELIBS Logger ID(s)";
              break;
              //NOTES
            /*case 'General Notes':
                this.ConditionEntity =  'CONVERT(TEXT, R.[Recipient_Notes]'
                break; */
                case 'Case Notes Date':
                var sql  = "Select distinct format(DetailDate,'dd/MM/yyyy') as DetailDate  from History where ExtraDetail1 = 'CASENOTE' and (DetailDate != null Or DetailDate != '') "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "";
                break;
                /*
                case 'Case Notes Detail':
                this.ConditionEntity =  'dbo.RTF2TEXT(History.[Detail])'
                break; */
                case 'Case Notes Creator':
                var sql  = "Select distinct Creator from History where ExtraDetail1 = 'CASENOTE' and (Creator != null Or Creator != '') "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Case Notes Creator(s)";
                break;
                case 'Case Notes Alarm':
                var sql  = "Select distinct AlarmDate from History where ExtraDetail1 = 'CASENOTE' and (AlarmDate != null Or AlarmDate != '') "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Case Notes Alarm(s)";
                break;
                case 'Case Notes Program':
                var sql  = "Select distinct Program from History where ExtraDetail1 = 'CASENOTE' and (Program != null Or Program != '') "
                this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
                this.ModalTitle = "Case Notes Program(s)";
                break; /*
                case 'Case Notes Category':
                this.ConditionEntity =  'History.ExtraDetail2'
                var sql  = "Select distinct  from Recipients where != null Or  != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
              break; 
              */
            //INSURANCE AND PENSION                  
            case 'Medicare Number':
            var sql  = "Select distinct MedicareNumber from Recipients where MedicareNumber != null Or MedicareNumber != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Medicare Number(s)";
            break;
            case 'Medicare Recipient ID':
            var sql  = "Select distinct MedicareRecipientID from Recipients where MedicareRecipientID != null Or MedicareRecipientID != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Medicare Recipient ID(s)";
            break;
            case 'Pension Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'PENSION' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Pension Status";
            break;
            case 'Unable to Determine Pension Status':
            var sql  = "Select distinct PensionVoracity from Recipients where PensionVoracity != null Or PensionVoracity != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Determine Pension Status";
            break;
            case 'Concession Number':
            var sql  = "Select distinct ConcessionNumber from Recipients where ConcessionNumber != null Or ConcessionNumber != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Concession Number";
            break;
            case 'DVA Benefits Flag':
            var sql  = "Select distinct DVABenefits from Recipients where DVABenefits != null Or DVABenefits != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DVA Benefits Flag";
            break;                  
            case 'DVA Number':
            var sql  = "Select distinct DVANumber from Recipients where DVANumber != null Or DVANumber != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DVA Number(s)";
            break;
            case 'DVA Card Holder Status':
            var sql  = "Select distinct RECIPT_DVA_Card_Holder_Status from Recipients where RECIPT_DVA_Card_Holder_Status != null Or RECIPT_DVA_Card_Holder_Status != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DVA Card Holder Status";
            break;
            case 'Ambulance Subscriber':
            var sql  = "Select distinct Ambulance from Recipients where Ambulance != null Or Ambulance != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Ambulance Subscriber";
            break;
            case 'Ambulance Type':
            var sql  = "Select distinct AmbulanceType from Recipients where AmbulanceType != null Or AmbulanceType != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Ambulance Type";
            break;
            case 'Pension Name':
            var sql  = "Select distinct [Name] from HumanResources where [Group] = 'PENSION'"
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Pension Name";
            break;
            case 'Pension Number': 
            var sql  = "Select distinct [Pension Number] from HumanResources where [Group] = 'PENSION'"
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Pension Number(s)";
            break;
            case 'Will Available':
            var sql  = "Select distinct WillAvailable from Recipients where WillAvailable != null Or WillAvailable != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Will Available";
            break;         
            case 'Will Location':
            var sql  = "Select distinct WhereWillHeld from Recipients where WhereWillHeld != null Or WhereWillHeld != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Will Location";
            break;
            case 'Funeral Arrangements':
            var sql  = "Select distinct FuneralArrangements from Recipients where FuneralArrangements != null Or FuneralArrangements != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Funeral Arrangements";
            break;
            case 'Date Of Death':
            var sql  = "Select distinct DateOfDeath from Recipients where DateOfDeath != null Or DateOfDeath != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Date Of Death";
            break;
            //HACCS DATSET FIELDS               
            case 'HACC-SLK':
            var sql  = "Select distinct [HACC-SLK] from Recipients where [HACC-SLK] != null Or [HACC-SLK] != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-SLK";
            break;
            case 'HACC-First Name':
            var sql  = "Select distinct FirstName from Recipients where FirstName != null Or FirstName != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-First Name(s)";
            break;
            case 'HACC-Surname':
            var sql  = "Select distinct [Surname/Organisation] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Surname";
            break;
            case 'HACC-Referral Source':
            var sql  = "Select distinct ReferralSource from Recipients where ReferralSource != null Or ReferralSource != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Referral Source(s)";
            break;
            case 'HACC-Date Of Birth':
            var sql  = "Select distinct format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth from Recipients where DateOfBirth != null Or DateOfBirth != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Date Of Birth";
            break;
            case 'HACC-Date Of Birth Estimated':
            var sql  = "Select distinct CSTDA_BDEstimate from Recipients where CSTDA_BDEstimate != null Or CSTDA_BDEstimate != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Date Of Birth Estimated";
            break;                
            case 'HACC-Gender':
            var sql  = "Select distinct Description from DataDomains where Domain = 'SEX' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Gender";
            break;
            case 'HACC-Area Of Residence':
            var sql  = "Select distinct Suburb from Recipients where Suburb != null Or Suburb != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Area Of Residence";
            break;
            case 'HACC-Country Of Birth':
            var sql  = "Select distinct Description from DataDomains where Domain = 'COUNTRIES' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Country Of Birth"; 
            break;
            case 'HACC-Preferred Language,':
            var sql  = "Select distinct Description from DataDomains where Domain = 'LANGUAGE' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Preferred Language";
            break;
            case 'HACC-Indigenous Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'INDIGINOUS STATUS' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Indigenous Status";
            break;
            case 'HACC-Living Arrangements':
            var sql  = "Select distinct Description from DataDomains where Domain = 'LIVING ARRANGEMENTS' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Living Arrangements";
            break;
            case 'HACC-Dwelling/Accomodation':
            var sql  = "Select distinct DwellingAccomodation from Recipients where DwellingAccomodation != null Or DwellingAccomodation != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Dwelling/Accomodation";
            break;
            case 'HACC-Main Reasons For Cessation':
            var sql  = "Select distinct Description from DataDomains where Domain = 'HACC-Main Reasons For Cessation' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Main Reasons For Cessation";
            break;
            case 'HACC-Pension Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'PENSION' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Pension Status";
            break;              
            case 'HACC-Primary Carer':
            var sql  = "Select distinct DatasetCarer from Recipients where DatasetCarer != null Or DatasetCarer != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Primary Carer";
            break;
            case 'HACC-Carer Availability':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CARER AVAILABILITY' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Carer Availability";
            break;
            case 'HACC-Carer Residency':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CARER RESIDENCY' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Carer Residency";
            break;
            case 'HACC-Carer Relationship':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CARER RELATIONSHIP' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "'HACC-Carer Relationship";
            break;
            case 'HACC-Exclude From Collection':
            var sql  = "Select distinct ExcludeFromStats from Recipients where ExcludeFromStats != null Or ExcludeFromStats != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Exclude From Collection";
            break;                                
            case 'HACC-Housework':
            var sql  = "Select distinct ONIFProfile1 from Recipients where ONIFProfile1 != null Or ONIFProfile1 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Housework";
            break;
            case 'HACC-Transport':
            var sql  = "Select distinct ONIFProfile2 from Recipients where ONIFProfile2 != null Or ONIFProfile2 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Transport";
            break;
            case 'HACC-Shopping':
            this.ConditionEntity =  'R.[ONIFProfile3]'
            var sql  = "Select distinct ONIFProfile3 from Recipients where ONIFProfile3 != null Or ONIFProfile3 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Shopping";
            break;
            case 'HACC-Medication':
            var sql  = "Select distinct ONIFProfile4 from Recipients where ONIFProfile4 != null Or ONIFProfile4 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Medication";
            break;
            case 'HACC-Money':
            var sql  = "Select distinct ONIFProfile5 from Recipients where ONIFProfile5 != null Or ONIFProfile5 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Money";
            break;
            case 'HACC-Walking':
            var sql  = "Select distinct ONIFProfile6 from Recipients where ONIFProfile6 != null Or ONIFProfile6 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Walking";
            break;
            case 'HACC-Bathing':
            var sql  = "Select distinct ONIFProfile7 from Recipients where ONIFProfile7 != null Or ONIFProfile7 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Bathing";
            break;
            case 'HACC-Memory':
            var sql  = "Select distinct ONIFProfile8 from Recipients where ONIFProfile8 != null Or ONIFProfile8 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Memory";
            break;
            case 'HACC-Behaviour':
            var sql  = "Select distinct ONIFProfile9 from Recipients where ONIFProfile9 != null Or ONIFProfile9 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Behaviour";
            break;
            case 'HACC-Communication':
            var sql  = "Select distinct FPA_Communication from ONI where FPA_Communication != null Or FPA_Communication != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Communication";
            break;
            case 'HACC-Eating':
            var sql  = "Select distinct FPA_Eating from ONI where FPA_Eating != null Or FPA_Eating != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Eating";
            break;
            case 'HACC-Toileting':
            var sql  = "Select distinct FPA_Toileting from ONI where FPA_Toileting != null Or FPA_Toileting != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Toileting";
            break;
            case 'HACC-GetUp':
            var sql  = "Select distinct FPA_GetUp from ONI where FPA_GetUp != null Or FPA_GetUp != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "HACC-GetUp";
            break;        
            case 'HACC-Carer More Than One':
            var sql  = "Select distinct CARER_MORE_THAN_ONE from Recipients where CARER_MORE_THAN_ONE != null Or CARER_MORE_THAN_ONE != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "HACC-Carer More Than One";
            break;
            //"DEX"                  
            case 'DEX-Exclude From MDS':
            var sql  = "Select distinct ExcludeFromStats from Recipients where ExcludeFromStats != null Or ExcludeFromStats != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Exclude From MDS";
            break;
            case 'DEX-Referral Purpose':
            var sql  = "Select distinct ReferralPurpose from DSSXtra where ReferralPurpose != null Or ReferralPurpose != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Referral Purpose";
            break;
            case 'DEX-Referral Source':
            var sql  = "Select distinct DEXREFERRALSOURCE from DSSXtra where DEXREFERRALSOURCE != null Or DEXREFERRALSOURCE != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Referral Source";
            break;
            case 'DEX-Referral Type':
            var sql  = "Select distinct ReferralType from DSSXtra where ReferralType != null Or ReferralType != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Referral Type";
            break;
            case 'DEX-Reason For Assistance':
            var sql  = "Select distinct AssistanceReason from DSSXtra where AssistanceReason != null Or AssistanceReason != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Reason For Assistance";
            break;
            case 'DEX-Consent To Provide Information':
            var sql  = "Select distinct [DEX-Consent To Provide Information] from DSSXtra where [DEX-Consent To Provide Information] != null Or [DEX-Consent To Provide Information] != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Consent To Provide Information";
            break;
            case 'DEX-Consent For Future Contact':
            var sql  = "Select distinct [DEX-Consent For Future Contact] from DSSXtra where [DEX-Consent For Future Contact] != null Or [DEX-Consent For Future Contact] != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Consent For Future Contact";
            break;          
            case 'DEX-Sex':
            var sql  = "Select distinct Description from DataDomains where Domain = 'SEX' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Gender";
            break;
            case 'DEX-Date Of Birth':
            var sql  = "Select distinct format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth from Recipients where DateOfBirth != null Or DateOfBirth != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Date Of Birth";
            break;                 
            case 'DEX-Estimated Birth Date':
            var sql  = "Select distinct CSTDA_BDEstimate from Recipients where CSTDA_BDEstimate != null Or CSTDA_BDEstimate != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Estimated Birth Date";
            break;
            case 'DEX-Indigenous Status':
            var sql  = "Select distinct DexIndiginousStatus from DSSXtra where DexIndiginousStatus != null Or DexIndiginousStatus != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Indigenous Status";
            break;
            case 'DEX-DVA Card Holder Status':
            var sql  = "Select distinct HACCDVACardHolderStatus from ONI where HACCDVACardHolderStatus != null Or HACCDVACardHolderStatus != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-DVA Card Holder Status";
            break;            
            case 'DEX-Has Disabilities':
            var sql  = "Select distinct HasDisabilities from DSSXtra where HasDisabilities != null Or HasDisabilities != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Has Disabilities";
            break;        
            case 'DEX-Has A Carer':
            var sql  = "Select distinct HasCarer from DSSXtra where HasCarer != null Or HasCarer != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Has A Carer";
            break;
            case 'DEX-Country of Birth':
            var sql  = "Select distinct Description from DataDomains where Domain = 'COUNTRIES' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Country of Birth";
            break;
            case 'DEX-First Arrival Year':
            var sql  = "Select distinct FirstArrivalYear from DSSXtra where FirstArrivalYear != null Or FirstArrivalYear != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-First Arrival Year";
            break;               
            case 'DEX-First Arrival Month':
            var sql  = "Select distinct FirstArrivalMonth from DSSXtra where FirstArrivalMonth != null Or FirstArrivalMonth != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-First Arrival Month";
            break;
            case 'DEX-Visa Code':
            var sql  = "Select distinct VisaCategory from DSSXtra where VisaCategory != null Or VisaCategory != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Visa Code";
            break;
            case 'DEX-Ancestry':
            var sql  = "Select distinct Description from DataDomains where Domain = 'Ancestry' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Ancestry";
            break;
            case 'DEX-Main Language At Home':
            var sql  = "Select distinct Description from DataDomains where Domain = 'LANGUAGE' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Main Language At Home";
            break;
            case 'DEX-Accomodation Setting':
            var sql  = "Select distinct Description from DataDomains where Domain = 'ACCOMODATION SETTING' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Accomodation Setting";
            break;
            case 'DEX-Is Homeless':
            var sql  = "Select distinct IsHomeless from DSSXtra where IsHomeless != null Or IsHomeless != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Is Homeless";
            break;
            case 'DEX-Household Composition':
            var sql  = "Select distinct HouseholdComposition from DSSXtra where HouseholdComposition != null Or HouseholdComposition != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Household Composition";
            break;
            case 'DEX-Main Source Of Income':
            var sql  = "Select distinct MainIncomSource from DSSXtra where MainIncomSource != null Or MainIncomSource != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Main Source Of Income";
            break;
            case 'DEX-Income Frequency':
            var sql  = "Select distinct IncomFrequency from DSSXtra where IncomFrequency != null Or IncomFrequency != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Income Frequency";
            break;                
            case 'DEX-Income Amount':
            var sql  = "Select distinct IncomeAmount from DSSXtra where IncomeAmount != null Or IncomeAmount != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "DEX-Income Amount";
            break;
            // CSTDA Dataset Fields               
            case 'CSTDA-Date Of Birth':
            var sql  = "Select distinct format(DateOfBirth,'dd/MM/yyyy') as DateOfBirth  from Recipients where DateOfBirth != null Or DateOfBirth != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Date Of Birth";
            break;
            case 'CSTDA-Gender':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SEX' "
            this.bodystyleModal_IN = { height:'100px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Gender";
            break;
            case 'CSTDA-DISQIS ID':
            var sql  = "Select distinct CSTDA_ID from Recipients where CSTDA_ID != null Or CSTDA_ID != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-DISQIS ID";
            break;
            case 'CSTDA-Indigenous Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'INDIGINOUS STATUS' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Indigenous Status";
            break;
            case 'CSTDA-Country Of Birth':
            var sql  = "Select distinct CountryOfBirth from Recipients where CountryOfBirth != null Or CountryOfBirth != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Country Of Birth";
            break;              
            case 'CSTDA-Interpreter Required':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_INTERPRETER' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Interpreter Required";
            break;
            case 'CSTDA-Communication Method':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_COMMUNICATION' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Communication Method";
            break;
            case 'CSTDA-Living Arrangements':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_LIVINGARRANGEMENTS' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Living Arrangements";
            break;
            case 'CSTDA-Suburb':
            var sql  = "Select distinct Suburb from Recipients where Suburb != null Or Suburb != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Suburb";
            break;
            case 'CSTDA-Postcode':
            var sql  = "Select distinct Postcode from Recipients where Postcode != null Or Postcode != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Postcode";
            break;
            case 'CSTDA-State':
            var sql  = "Select distinct State from Recipients where State != null Or State != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-State";
            break;
            case 'CSTDA-Residential Setting':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_RESIDENTIALSETTING' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Residential Setting";
            break;
            case 'CSTDA-Primary Disability Group':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_DISABILITYGROUP' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Primary Disability Group";
            break;
            case 'CSTDA-Primary Disability Description':
            var sql  = "Select distinct CSTDA_PrimaryDisabilityDescription from ONI where CSTDA_PrimaryDisabilityDescription != null Or CSTDA_PrimaryDisabilityDescription != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Primary Disability Description";
            break;
            case 'CSTDA-Intellectual Disability':
            var sql  = "Select distinct CSTDA_OtherIntellectual from Recipients where CSTDA_OtherIntellectual != null Or CSTDA_OtherIntellectual != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Intellectual Disability";
            break;
            case 'CSTDA-Specific Learning ADD Disability':
            var sql  = "Select distinct CSTDA_OtherADD from Recipients where CSTDA_OtherADD != null Or CSTDA_OtherADD != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Specific Learning ADD Disability";
            break;
            case 'CSTDA-Autism Disability':
            var sql  = "Select distinct CSTDA_OtherAutism from Recipients where CSTDA_OtherAutism != null Or CSTDA_OtherAutism != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Autism Disability";
            break;
            case 'CSTDA-Physical Disability':
            var sql  = "Select distinct CSTDA_OtherPhysical from Recipients where CSTDA_OtherPhysical != null Or CSTDA_OtherPhysical != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Physical Disability";
            break;  
            case 'CSTDA-Acquired Brain Injury Disability':
            var sql  = "Select distinct CSTDA_OtherAcquiredBrain from Recipients where CSTDA_OtherAcquiredBrain != null Or CSTDA_OtherAcquiredBrain != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Acquired Brain Injury Disability";
            break;
            case 'CSTDA-Neurological Disability':
            var sql  = "Select distinct CSTDA_OtherNeurological from Recipients where CSTDA_OtherNeurological != null Or CSTDA_OtherNeurological != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Neurological Disability";
            break;
            case 'CSTDA-Deaf Blind Disability':
            var sql  = "Select distinct CSTDA_OtherDeafBlind from Recipients where CSTDA_OtherDeafBlind != null Or CSTDA_OtherDeafBlind != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Deaf Blind Disability";
            break;
            case 'CSTDA-Psychiatric Disability':
            var sql  = "Select distinct CSTDA_Psychiatric from Recipients where CSTDA_Psychiatric != null Or CSTDA_Psychiatric != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Psychiatric Disability";
            break;
            case 'CSTDA-Other Psychiatric Disability':
            var sql  = "Select distinct CSTDA_OtherPsychiatric from Recipients where CSTDA_OtherPsychiatric != null Or CSTDA_OtherPsychiatric != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Other Psychiatric Disability";
            break;
            case 'CSTDA-Vision Disability':
            var sql  = "Select distinct CSTDA_OtherVision from Recipients where CSTDA_OtherVision != null Or CSTDA_OtherVision != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Vision Disability";
            break;
            case 'CSTDA-Hearing Disability':
            var sql  = "Select distinct CSTDA_OtherHearing from Recipients where CSTDA_OtherHearing != null Or CSTDA_OtherHearing != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Hearing Disability";
            break;
            case 'CSTDA-Speech Disability':
            var sql  = "Select distinct CSTDA_OtherSpeech from Recipients where CSTDA_OtherSpeech != null Or CSTDA_OtherSpeech != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Speech Disability";
            break;
            case 'CSTDA-Developmental Delay Disability':
            var sql  = "Select distinct CSTDA_DevelopDelay from Recipients where CSTDA_DevelopDelay != null Or CSTDA_DevelopDelay != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Developmental Delay Disability";
            break;
            case 'CSTDA-Disability Likely To Be Permanent':
            var sql  = "Select distinct PermDisability from Recipients where PermDisability != null Or PermDisability != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Disability Likely To Be Permanent";
            break;
            case 'CSTDA-Support Needs-Self Care':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Self Care";
            break;
            case 'CSTDA-Support Needs-Mobility':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Mobility";
            break;
            case 'CSTDA-Support Needs-Communication':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Communication";
            break;
            case 'CSTDA-Support Needs-Interpersonal':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Interpersonal";
            break; 
            case 'CSTDA-Support Needs-Learning':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Learning";
            break;
            case 'CSTDA-Support Needs-Education':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Education";
            break;
            case 'CSTDA-Support Needs-Community':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Community";
            break;
            case 'CSTDA-Support Needs-Domestic':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Domestic";
            break;
            case 'CSTDA-Support Needs-Working':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_SupportNeeds' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Support Needs-Working";
            break;                 
            case 'CSTDA-Carer-Existence Of Informal':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDAYESNO' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Carer-Existence Of Informal";
            break;
            case 'CSTDA-Carer-Assists client in ADL':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDAYESNO' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Carer-Assists client in ADL";
            break;
            case 'CSTDA-Carer-Lives In Same Household':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDAYESNO' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Carer-Lives In Same Household";
            break;
            case 'CSTDA-Carer-Relationship':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_CarerRelationship' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "STDA-Carer-Relationship";
            break;
            case 'CSTDA-Carer-Age Group':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_CarerAgeGroup' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Carer-Age Group";
            break;
            case 'CSTDA-Carer Allowance to Guardians':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_CarerAllowance' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Carer Allowance to Guardians";
            break;
            case 'CSTDA-Labor Force Status':
            var sql  = "Select distinct CSTDA_LaborStatus from Recipients where CSTDA_LaborStatus != null Or CSTDA_LaborStatus != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Labor Force Status";
            break;
            case 'CSTDA-Main Source Of Income':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_MainIncome' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Main Source Of Income";
            break;
            case 'CSTDA-Current Individual Funding':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_FundingStatus' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "CSTDA-Current Individual Funding";
            break;
            //NRCP Dataset Fields                  
            case 'NRCP-First Name':
            var sql  = "Select distinct FirstName from Recipients where FirstName != null Or FirstName != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-First Name";
            break;         
            case 'NRCP-Surname':
            var sql  = "Select distinct [Surname/Organisation] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Surname";
            break;
            case 'NRCP-Date Of Birth':
            this.ConditionEntity =  'R.[DateOfBirth]'
            var sql  = "Select distinct format (DateOfBirth, 'dd/MM/yyyy') as DateOfBirth from Recipients where DateOfBirth != null Or DateOfBirth != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "NRCP DOB";
            break;
            case 'NRCP-Gender':
            var sql  = "Select distinct Description from DataDomains where Domain = 'SEX' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Gender";
            break;
            case 'NRCP-Suburb':
            var sql  = "Select distinct Suburb from Recipients where Suburb != null Or Suburb != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Suburb";
            break;
            case 'NRCP-Country Of Birth':
            var sql  = "Select distinct Description from DataDomains where Domain = 'COUNTRIES' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Country Of Birth";
            break;                
            case 'NRCP-Preferred Language':
            var sql  = "Select distinct Description from DataDomains where Domain = 'LANGUAGE' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Preferred Language";
            break;
            case 'NRCP-Indigenous Status':
            this.ConditionEntity =  'R.[IndiginousStatus]'
            var sql  = "Select distinct Description from DataDomains where Domain = 'INDIGINOUS STATUS' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Indigenous Status";
            break;                 
            case 'NRCP-Marital Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'MARITAL' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Marital Status";
            break;
            case 'NRCP-DVA Card Holder Status':
            var sql  = "Select distinct RECIPT_DVA_Card_Holder_Status from Recipients where RECIPT_DVA_Card_Holder_Status != null Or RECIPT_DVA_Card_Holder_Status != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-DVA Card Holder Status";
            break;
            case 'NRCP-Paid Employment Participation':
            var sql  = "Select distinct RECIPT_Paid_Employment_Participation from Recipients where RECIPT_Paid_Employment_Participation != null Or RECIPT_Paid_Employment_Participation != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Paid Employment Participation";
            break;
            case 'NRCP-Pension Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'PENSION' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Pension Status";
            break;
            case 'NRCP-Carer-Date Role Commenced':
            var sql  = "Select distinct NRCP-Carer-Date Role Commenced from Recipients where NRCP-Carer-Date Role Commenced != null Or NRCP-Carer-Date Role Commenced != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Date Role Commenced";
            break;
            case 'NRCP-Carer-Role':
            var sql  = "Select distinct RECIPT_Care_Role from Recipients where RECIPT_Care_Role != null Or RECIPT_Care_Role != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Role";
            break;
            case 'NRCP-Carer-Need':
            var sql  = "Select distinct RECIPT_Care_Need from Recipients where RECIPT_Care_Need != null Or RECIPT_Care_Need != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Need";
            break;
            case 'NRCP-Carer-Number of Recipients':
            var sql  = "Select distinct NRCP-Carer-Number of Recipients from Recipients where NRCP-Carer-Number of Recipients != null Or NRCP-Carer-Number of Recipients != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Number of Recipients";
            break;
            case 'NRCP-Carer-Time Spent Caring':
            var sql  = "Select distinct RECIPT_Time_Spent_Caring from Recipients where RECIPT_Time_Spent_Caring != null Or RECIPT_Time_Spent_Caring != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Time Spent Caring";
            break;
            case 'NRCP-Carer-Current Use Formal Services':
            var sql  = "Select distinct NRCP_CurrentUseFormalServices from Recipients where NRCP_CurrentUseFormalServices != null Or NRCP_CurrentUseFormalServices != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Current Use Formal Services";
            break;
            case 'NRCP-Carer-Informal Support':
            var sql  = "Select distinct NRCP_InformalSupport from Recipients where NRCP_InformalSupport != null Or NRCP_InformalSupport != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Carer-Informal Support";
            break;
            case 'NRCP-Recipient-Challenging Behaviour':
            var sql  = "Select distinct RECIPT_Challenging_Behaviour from Recipients where RECIPT_Challenging_Behaviour != null Or RECIPT_Challenging_Behaviour != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Challenging Behaviour";
            break;
            case 'NRCP-Recipient-Primary Disability':
            var sql  = "Select distinct RECIPT_Care_Recipients_Primary_Disability from Recipients where RECIPT_Care_Recipients_Primary_Disability != null Or RECIPT_Care_Recipients_Primary_Disability != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Primary Disability";
            break;         
            case 'NRCP-Recipient-Primary Care Needs':
            var sql  = "Select distinct RECIPT_Care_Recipients_Primary_Care_Needs from Recipients where RECIPT_Care_Recipients_Primary_Care_Needs != null Or RECIPT_Care_Recipients_Primary_Care_Needs != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Primary Care Needs";
            break;
            case 'NRCP-Recipient-Level of Need':
            var sql  = "Select distinct RECIPT_Care_Recipients_Level_Need from Recipients where RECIPT_Care_Recipients_Level_Need != null Or RECIPT_Care_Recipients_Level_Need != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Level of Need";
            break;
            case 'NRCP-Recipient-Primary Carer':
            var sql  = "Select distinct DatasetCarer from Recipients where DatasetCarer != null Or DatasetCarer != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Primary Carer";
            break;
            case 'NRCP-Recipient-Carer Relationship':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_CarerRelationship' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Carer Relationship";
            break;
            case 'NRCP-Recipient-Carer Co-Resident':
            var sql  = "Select distinct NRCP_CarerCoResidency from Recipients where NRCP_CarerCoResidency != null Or NRCP_CarerCoResidency != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Carer Co-Resident";
            break;
            case 'NRCP-Recipient-Dementia':
            var sql  = "Select distinct RECIPT_Dementia from Recipients where RECIPT_Dementia != null Or RECIPT_Dementia != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-Recipient-Dementia";
            break;
            case 'NRCP-CALD Background':
            var sql  = "Select distinct CALDStatus from Recipients where CALDStatus != null Or CALDStatus != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NRCP-CALD Background";
            break;
            // "ONI-Core"                
            case 'ONI-Family Name':
            var sql  = "Select distinct [Surname/Organisation] from Recipients where [Surname/Organisation] != null Or [Surname/Organisation] != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Family Name(s)";
            break;
            case 'ONI-Title':
            var sql  = "Select distinct Title from Recipients where Title != null Or Title != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Title(s)";
            break;
            case 'ONI-First Name':
            var sql  = "Select distinct FirstName from Recipients where FirstName != null Or FirstName != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-First Name";
            break;
            case 'ONI-Other':
            var sql  = "Select distinct MiddleNames from Recipients where MiddleNames != null Or MiddleNames != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Other(s)";
            break;
            case 'ONI-Sex':
            var sql  = "Select distinct Description from DataDomains where Domain = 'SEX' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI Gender";
            break;
            case 'ONI-DOB':
            var sql  = "Select distinct format (DateOfBirth,'dd/MM/yyyy') as DateOfBirth from Recipients where DateOfBirth != null Or DateOfBirth != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-DOB";
            break;
            case 'ONI-Usual Address-Street':
              var sql   = "SELECT distinct Address1 from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>' " 
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Usual Address-Street";
            break;
            case 'ONI-Usual Address-Suburb':
              var sql  =  "SELECT distinct Suburb from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>'"
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Usual Address-Suburb";
            break;
            case 'ONI-Usual Address-Postcode':
              var sql  =  " SELECT distinct Postcode from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Usual Address-Postcode";
            break;
            case 'ONI-Contact Address-Street':
              var sql  =  " SELECT distinct Address1 from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Contact Address-Street";
            break;
            case 'ONI-Contact Address-Suburb':
              var sql  =  " SELECT distinct Suburb from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "NI-Contact Address-Suburb";
            break;
            case 'ONI-Contact Address-Postcode':
              var sql  =  " SELECT distinct Postcode from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Contact Address-Postcode";
            break; /*
            case 'ONI-Phone-Home':
              this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<HOME>') "
            break;
            case 'ONI-Phone-Work':
              this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<WORK>')  "

            break;
            case 'ONI-Phone-Mobile':
              this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<MOBILE>') "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
            break;
            case 'ONI-Phone-FAX':
              this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<FAX>') "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
            break;
            case 'ONI-EMAIL':
              this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<EMAIL>') "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
            break; */
            case 'ONI-Person 1 Name':
              var sql =  " SELECT distinct [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 1 Name";
            break;
            case 'ONI-Person 1 Street':
              var sql =  " SELECT distinct [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 1 Street";
            break;
            case 'ONI-Person 1 Suburb':
              var sql =  " SELECT distinct [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 1 Suburb";
            break;
            case 'ONI-Person 1 Postcode':
              var sql =  " SELECT distinct [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 1 Postcode";
            break;/*
            case 'ONI-Person 1 Phone':
              var sql =  " SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 1 Phone";
            break; */ 
            case 'ONI-Person 1 Relationship':
              var sql =  " SELECT distinct [Type] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 1 Relationship";
            break;
            case 'ONI-Person 2 Name':
              var sql =  " SELECT distinct [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 2 Name";
            break;
            case 'ONI-Person 2 Street':
              var sql =  " SELECT distinct [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 2 Street";
            break;
            case 'ONI-Person 2 Suburb':
              var sql =  " SELECT distinct [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
            break;
            case 'ONI-Person 2 Postcode':
              var sql =  " SELECT distinct [Postcode] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2'  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 2 Postcode";
            break; /*
            case 'ONI-Person 2 Phone':
              this.ConditionEntity =  " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2')  "
            break; */
            case 'ONI-Person 2 Relationship':
              var sql =  " SELECT distinct [Type] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Person 2 Relationship";
            break;
            case 'ONI-Doctor Name':
              var sql =  " SELECT distinct [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor Name";
            break;
            case 'ONI-Doctor Street':
              var sql =  " SELECT distinct [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor Street";
            break;
            case 'ONI-Doctor Suburb':
              var sql =  " SELECT distinct [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor Suburb";
            break;
            case 'ONI-Doctor Postcode':
              var sql =  " SELECT distinct [Postcode] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor Postcode";
            break;
            case 'ONI-Doctor Phone':
              var sql =  " SELECT distinct [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber  "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor Phone";
            break;                           
            case 'ONI-Doctor FAX':
              var sql =   " SELECT distinct [FAX] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor FAX";
            break;
            case 'ONI-Doctor EMAIL':
              var sql =  " SELECT distinct [Email] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber"
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Doctor EMAIL";
            break;
            case 'ONI-Referral Source':
            var sql  = "Select distinct ReferralSource from Recipients where ReferralSource != null Or ReferralSource != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Referral Source";
            break;
            case 'ONI-Contact Details':
            var sql  = "Select distinct ReferralContactInfo from Recipients where ReferralContactInfo != null Or ReferralContactInfo != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Contact Details";
            break;
            case 'ONI-Country Of Birth':
            var sql  = "Select distinct Description from DataDomains where Domain = 'COUNTRIES' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Country Of Birth";
            break;
            case 'ONI-Indigenous Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'INDIGINOUS STATUS' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Indigenous Status";
            break;
            case 'ONI-Main Language At Home':
            var sql  = "Select distinct Description from DataDomains where Domain = 'LANGUAGE'"
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Main Language At Home";
            break;
            case 'ONI-Interpreter Required':
            this.ConditionEntity =  'R.[InterpreterRequired]'
            var sql  = "Select distinct InterpreterRequired from Recipients where InterpreterRequired != null Or InterpreterRequired != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Interpreter Required";
            break;                  
            case 'ONI-Preferred Language':
            var sql  = "Select distinct Description from DataDomains where Domain = 'LANGUAGE' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Preferred Language";
            break;
            case 'ONI-Govt Pension Status':
            var sql  = "Select distinct Description from DataDomains where Domain = 'PENSION' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Govt Pension Status";
            break;
            case 'ONI-Pension Benefit Card':
            var sql  = "Select distinct ConcessionNumber from Recipients where ConcessionNumber != null Or ConcessionNumber != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Pension Benefit Card"; 
            break;
            case 'ONI-Medicare Number':
            var sql  = "Select distinct MedicareNumber from Recipients where MedicareNumber != null Or MedicareNumber != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Medicare Number";
            break;
            case 'ONI-Health Care Card#':
            var sql  = "Select distinct [Healthcare#] from Recipients where [Healthcare#] != null Or [Healthcare#] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Health Care Card#";
            break;                     
            case 'ONI-DVA Cardholder Status':
            var sql  = "Select distinct HACCDVACardHolderStatus from Recipients where HACCDVACardHolderStatus != null Or HACCDVACardHolderStatus != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-DVA Cardholder Status";
            break;
            case 'ONI-DVA Number':
            var sql  = "Select distinct DVANumber from Recipients where DVANumber != null Or DVANumber != '' "
            this.bodystyleModal_IN = { height:'500px' , overflow: 'auto' };
            this.ModalTitle = "ONI-DVA Number";
            break;
            case 'ONI-Insurance Status':
            this.ConditionEntity =  'R.[InsuranceStatus]'
            var sql  = "Select distinct InsuranceStatus from Recipients where InsuranceStatus != null Or InsuranceStatus != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Insurance Status";
            break; /*
            case 'ONI-Health Insurer':
            //   this.ConditionEntity =  
            break;        
            case 'ONI-Health Insurance Card#':
            //   this.ConditionEntity =  
            break;
            case 'ONI-Alerts':
            this.ConditionEntity =  'R.[Notes]'
            var sql  = "Select distinct Notes from Recipients where Notes != null Or Notes != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Alerts";
            break; */
            case 'ONI-Rating':
            var sql  = "Select distinct ONIRating from Recipients where ONIRating != null Or ONIRating != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Rating";
            break;
            case 'ONI-HACC Eligible':
            var sql  = "Select distinct HACCEligible from Recipients where HACCEligible != null Or HACCEligible != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HACC Eligible";
            break;
            case 'ONI-Reason For HACC Status':
            var sql  = "Select distinct DSCEligible from Recipients where DSCEligible != null Or DSCEligible != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Reason For HACC Status";
            break;
            case 'ONI-Other Support Eligibility':
            var sql  = "Select distinct HACCReason  from Recipients where HACCReason != null Or HACCReason != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Other Support Eligibility";
            break;                 
            case 'ONI-Other Support Detail':
            var sql  = "Select distinct OtherEligibleDetails from Recipients where OtherEligibleDetails != null Or OtherEligibleDetails != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Other Support Detail";
            break;              
            case 'ONI-Functional Profile Complete':
            var sql  = "Select distinct ONIFProfileComplete from Recipients where ONIFProfileComplete != null Or ONIFProfileComplete != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Complete";
            break;
            case 'ONI-Functional Profile Score 1':
            var sql  = "Select distinct ONIFProfile1 from Recipients where ONIFProfile1 != null Or ONIFProfile1 != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 1";
            break;
            case 'ONI-Functional Profile Score 2':
            var sql  = "Select distinct ONIFProfile2 from Recipients where ONIFProfile2 != null Or ONIFProfile2 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 2";
            break;
            case 'ONI-Functional Profile Score 3':
            var sql  = "Select distinct ONIFProfile3 from Recipients where ONIFProfile3 != null Or ONIFProfile3 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 3";
            break;
            case 'ONI-Functional Profile Score 4':
            var sql  = "Select distinct ONIFProfile4 from Recipients where ONIFProfile4 != null Or ONIFProfile4 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 4";
            break;
            case 'ONI-Functional Profile Score 5':
            var sql  = "Select distinct ONIFProfile5 from Recipients where ONIFProfile5 != null Or ONIFProfile5 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 5";
            break;
            case 'ONI-Functional Profile Score 6':
            var sql  = "Select distinct ONIFProfile6 from Recipients where ONIFProfile6 != null Or ONIFProfile6 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 6";
            break;
            case 'ONI-Functional Profile Score 7':
            var sql  = "Select distinct ONIFProfile7 from Recipients where ONIFProfile7 != null Or ONIFProfile7 != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 7";
            break;
            case 'ONI-Functional Profile Score 8':
            var sql  = "Select distinct ONIFProfile8 from Recipients where ONIFProfile8 != null Or ONIFProfile8 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 8";
            break;
            case 'ONI-Functional Profile Score 9':
            var sql  = "Select distinct ONIFProfile9 from Recipients where ONIFProfile9 != null Or ONIFProfile9 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Functional Profile Score 9"; 
            break;
            case 'ONI-Main Problem-Description':
            var sql  = "Select distinct Description from ONIMainIssues where Description != null Or Description != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Main Problem-Description";
            break;
            case 'ONI-Main Problem-Action':
            var sql  = "Select distinct Action from ONIMainIssues where Action != null Or Action != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Main Problem-Action";
            break;
            case 'ONI-Other Problem-Description':
            var sql  = "Select distinct Description from ONISecondaryIssues where Description != null Or Description != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Other Problem-Description";
            break;
            case 'ONI-Other Problem-Action':
            var sql  = "Select distinct Action from ONISecondaryIssues where Action != null Or Action != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Other Problem-Action";
            break;                     
            case 'ONI-Current Service':
            var sql  = "Select distinct Service from ONIServices where Service != null Or Service != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Current Service";
            break;
            case 'ONI-Service Contact Details':
            var sql  = "Select distinct Information from ONIServices where Information != null Or Information != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-Service Contact Details";
            break;
            case 'ONI-AP-Agency':
            var sql  = "Select distinct HealthProfessional from ONIActionPlan where HealthProfessional != null Or HealthProfessional != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Agency";
            break;
            case 'ONI-AP-For':
            var sql  = "Select distinct [For] from ONIActionPlan where [For] != null Or [For] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-For";
            break;
            case 'ONI-AP-Consent':
            var sql  = "Select distinct Consent from ONIActionPlan where Consent != null Or Consent != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Consent"; 
            break;
            case 'ONI-AP-Referral':
            var sql  = "Select distinct Referral from ONIActionPlan where Referral != null Or Referral != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Referral";
            break;
            case 'ONI-AP-Transport':
            var sql  = "Select distinct Transport from ONIActionPlan where Transport != null Or Transport != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Transport";
            break;
            case 'ONI-AP-Feedback':
            var sql  = "Select distinct Feedback from ONIActionPlan where Feedback != null Or Feedback != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Feedback";
            break;
            case 'ONI-AP-Date':
            var sql  = "Select distinct format ([Date],'dd/MM/yyyy') as [Date] from ONIActionPlan where != null Or  != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Date";
            break;
            case 'ONI-AP-Review':
            var sql  = "Select distinct Review from ONIActionPlan where Review != null Or Review != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-AP-Review";
            break;
            //  ONI-Functional Profile                  
            case 'ONI-FPQ1-Housework':
            var sql  = "Select distinct FP1_Housework from ONI where FP1_Housework != null Or FP1_Housework != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ1-Housework";
            break;
            case 'ONI-FPQ2-GetToPlaces':
            var sql  = "Select distinct FP2_WalkingDistance from ONI where FP2_WalkingDistance != null Or FP2_WalkingDistance != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ2-GetToPlaces";
            break;
            case 'ONI-FPQ3-Shopping':
            var sql  = "Select distinct FP3_Shopping from ONI where FP3_Shopping != null Or FP3_Shopping != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ3-Shopping";
            break;
            case 'ONI-FPQ4-Medicine': 
            var sql  = "Select distinct FP4_Medicine from ONI where FP4_Medicine != null Or FP4_Medicine != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ4-Medicine";
            break;
            case 'ONI-FPQ5-Money':
            var sql  = "Select distinct FP5_Money from ONI where FP5_Money != null Or FP5_Money != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ5-Money";
            break;
            case 'ONI-FPQ6-Walk':
            var sql  = "Select distinct FP6_Walking from ONI where FP6_Walking != null Or FP6_Walking != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ6-Walk";
            break;                 
            case 'ONI-FPQ7-Bath':
            var sql  = "Select distinct FP7_Bathing from ONI where FP7_Bathing != null Or FP7_Bathing != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ7-Bath";
            break;                  
            case 'ONI-FPQ8-Memory':
            var sql  = "Select distinct FP8_Memory from ONI where FP8_Memory != null Or FP8_Memory != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ8-Memory";
            break;
            case 'ONI-FPQ9-Behaviour':
            var sql  = "Select distinct FP9_Behaviour from ONI where FP9_Behaviour != null Or FP9_Behaviour != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FPQ9-Behaviour";
            break;
            case 'ONI-FP-Recommend Domestic':
            var sql  = "Select distinct FA_Domestic from ONI where FA_Domestic != null Or FA_Domestic != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Recommend Domestic";
            break;
            case 'ONI-FP-Recommend Self Care':
            var sql  = "Select distinct FA_SelfCare from ONI where FA_SelfCare != null Or FA_SelfCare != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Recommend Self Care";
            break;
            case 'ONI-FP-Recommend Cognition':
            var sql  = "Select distinct FA_Cognition from ONI where FA_Cognition != null Or FA_Cognition != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Recommend Cognition";
            break;
            case 'ONI-FP-Recommend Behaviour':
            var sql  = "Select distinct FA_Behaviour from ONI where FA_Behaviour != null Or FA_Behaviour != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Recommend Behaviour";
            break;
            case 'ONI-FP-Has Self Care Aids':
            var sql  = "Select distinct Aids_SelfCare from ONI where Aids_SelfCare != null Or Aids_SelfCare != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Self Care Aids";
            break;
            case 'ONI-FP-Has Support/Mobility Aids':
            var sql  = "Select distinct Aids_SupportAndMobility from ONI where Aids_SupportAndMobility != null Or Aids_SupportAndMobility != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Support/Mobility Aids";
            break;
            case 'ONI-FP-Has Communication Aids':
            var sql  = "Select distinct Aids_CommunicationAids from ONI where Aids_CommunicationAids != null Or Aids_CommunicationAids != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Communication Aids";
            break;
            case 'ONI-FP-Has Car Mods':
            var sql  = "Select distinct Aids_CarModifications from ONI where Aids_CarModifications != null Or Aids_CarModifications != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Car Mods";
            break;
            case 'ONI-FP-Has Other Aids':
            var sql  = "Select distinct Aids_Other from ONI where Aids_Other != null Or Aids_Other != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Other Aids";
            break;
            case 'ONI-FP-Other Goods List':
            var sql  = "Select distinct AidsOtherList from ONI where AidsOtherList != null Or AidsOtherList != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Other Goods List";
            break;
            case 'ONI-FP-Has Medical Care Aids':
            var sql  = "Select distinct Aids_MdicalCare from ONI where Aids_MedicalCare != null Or Aids_MedicalCare != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Medical Care Aids";
            break;                  
            case 'ONI-FP-Has Reading Aids':
            var sql  = "Select distinct Aids_Reading from ONI where Aids_Reading != null Or Aids_Reading != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Has Reading Aids";
            break;
            case 'ONI-FP-Comments':
            var sql  = "Select distinct FP_Comments from ONI where FP_Comments != null Or FP_Comments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-FP-Comments";
            break;
            //  ONI-Living Arrangements Profile                                   
            case 'ONI-LA-Living Arrangements':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_LIVINGARRANGEMENTS' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Living Arrangements";
            break;
            case 'ONI-LA-Living Arrangements Comments':
            var sql  = "Select distinct LAP_LivingComments from Recipients where LAP_LivingComments != null Or LAP_LivingComments != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Living Arrangements Comments";
            break;
            case 'ONI-LA-Accomodation':
            this.ConditionEntity =  'R.[DwellingAccomodation]'
            var sql  = "Select distinct DwellingAccomodation from Recipients where DwellingAccomodation != null Or DwellingAccomodation != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Accomodation";
            break;
            case 'ONI-LA-Accomodation Comments':
            var sql  = "Select distinct LAP_AccomodationComments from ONI where LAP_AccomodationComments != null Or LAP_AccomodationComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Accomodation Comments";
            break;
            case 'ONI-LA-Employment Status':
            var sql  = "Select distinct LAP_Employment from ONI where LAP_Employment != null Or LAP_Employment != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Employment Status";
            break;
            case 'ONI-LA-Employment Status Comments':
            var sql  = "Select distinct LAP_EmploymentComments from ONI where LAP_EmploymentComments != null Or LAP_EmploymentComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Employment Status Comments";
            break;                  
            case 'ONI-LA-Mental Health Act Status':
            var sql  = "Select distinct LAP_MentalHealth from ONI where LAP_MentalHealth != null Or LAP_MentalHealth != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Mental Health Act Status";
            break;
            case 'ONI-LA-Decision Making Responsibility':
            var sql  = "Select distinct LAP_Decision from ONI where LAP_Decision != null Or LAP_Decision != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Decision Making Responsibility";
            break;
            case 'ONI-LA-Capable Own Decisions':
            var sql  = "Select distinct LAP_DecisionCapable from ONI where LAP_DecisionCapable != null Or LAP_DecisionCapable != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Capable Own Decisions";
            break;
            case 'ONI-LA-Financial Decisions':
            var sql  = "Select distinct LAP_FinancialDecision from ONI where LAP_FinancialDecision != null Or LAP_FinancialDecision != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Financial Decisions";
            break;
            case 'ONI-LA-Cost Of Living Trade Off':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDAYESNO' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Cost Of Living Trade Off";
            break;          
            case 'ONI-LA-Financial & Legal Comments':
            var sql  = "Select distinct LAP_LivingCostDecisionComments from ONI where LAP_LivingCostDecisionComments != null Or LAP_LivingCostDecisionComments != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-LA-Financial & Legal Comments";
            break;
            // ONI-Health Conditions Profile   
            case 'ONI-HC-Overall Health Description':
            var sql  = "Select distinct HC_Overall_General from ONI where HC_Overall_General != null Or HC_Overall_General != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "'ONI-HC-Overall Health Description";
            break;               
            case 'ONI-HC-Overall Health Pain':
            var sql  = "Select distinct HC_Overall_PainHC_Overall_Pain from ONI where HC_Overall_Pain != null Or HC_Overall_Pain != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Overall Health Pain";
            break;
            case 'ONI-HC-Overall Health Interference':
            var sql  = "Select distinct HC_Overall_Interfere from ONI where HC_Overall_Interfere != null Or HC_Overall_Interfere != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Overall Health Interference";
            case 'ONI-HC-Vision Reading':
            var sql  = "Select distinct HC_Vision_Reading from ONI where HC_Vision_Reading != null Or HC_Vision_Reading != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vision Reading";
            break;
            case 'ONI-HC-Vision Distance':
            var sql  = "Select distinct HC_Vision_Long from ONI where HC_Vision_Long != null Or HC_Vision_Long != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vision Distance";
            break;
            case 'ONI-HC-Hearing':
            var sql  = "Select distinct HC_Hearing from ONI where HC_Hearing != null Or HC_Hearing != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Hearing";
            break;
            case 'ONI-HC-Oral Problems':
            var sql  = "Select distinct HC_Oral from ONI where HC_Oral != null Or HC_Oral != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Oral Problems";
            break;
            case 'ONI-HC-Oral Comments':
            var sql  = "Select distinct HC_OralComments from ONI where HC_OralComments != null Or HC_OralComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Oral Comments";
            break;                  
            case 'ONI-HC-Speech/Swallow Problems':
            var sql  = "Select distinct HC_Speech from ONI where HC_Speech != null Or HC_Speech != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Speech/Swallow Problems";
            break;
            case 'ONI-HC-Speech/Swallow Comments':
            var sql  = "Select distinct HC_SpeechComments from ONI where HC_SpeechComments != null Or HC_SpeechComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Speech/Swallow Comments";
            break;
            case 'ONI-HC-Falls Problems':
            var sql  = "Select distinct HC_Falls from ONI where HC_Falls != null Or HC_Falls != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Falls Problems";
            break;
            case 'ONI-HC-Falls Comments':
            var sql  = "Select distinct HC_FallsComments from ONI where HC_FallsComments != null Or HC_FallsComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Falls Comments";
            break;
            case 'ONI-HC-Feet Problems':
            var sql  = "Select distinct HC_Feet from ONI where HC_Feet != null Or HC_Feet != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Feet Problems";
            break;
            case 'ONI-HC-Feet Comments':
            var sql  = "Select distinct HC_FeetComments from ONI where HC_FeetComments != null Or HC_FeetComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Feet Comments";
            break;
            case 'ONI-HC-Vacc. Influenza':
            var sql  = "Select distinct HC_Vac_Influenza from ONI where HC_Vac_Influenza != null Or HC_Vac_Influenza != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Influenza";
            break;
            case 'ONI-HC-Vacc. Influenza Date':
            var sql  = "Select distinct format(HC_Vac_Influenza_Date,'dd/MM/yyyy') as HC_Vac_Influenza_Date from ONI where HC_Vac_Influenza_Date != null Or HC_Vac_Influenza_Date != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Influenza Date";
            break;
            case 'ONI-HC-Vacc. Pneumococcus':
            var sql  = "Select distinct HC_Vac_Pneumo from ONI where HC_Vac_Pneumo != null Or HC_Vac_Pneumo != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Pneumococcus";
            break;
            case 'ONI-HC-Vacc. Pneumococcus  Date':
            var sql  = "Select distinct format(HC_Vac_Pneumo_Date,'dd/MM/yyyy') as HC_Vac_Pneumo_Date from ONI where HC_Vac_Pneumo_Date != null Or HC_Vac_Pneumo_Date != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Pneumococcus  Date";
            break;     
            case 'ONI-HC-Vacc. Tetanus':
            var sql  = "Select distinct HC_Vac_Tetanus from ONI where HC_Vac_Tetanus != null Or HC_Vac_Tetanus != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Tetanus";
            break;
            case 'ONI-HC-Vacc. Tetanus Date':
            var sql  = "Select distinct format(HC_Vac_Tetanus_Date,'dd/MM/yyyy') as HC_Vac_Tetanus_Date from ONI where HC_Vac_Tetanus_Date != null Or HC_Vac_Tetanus_Date != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Tetanus Date";
            break;
            case 'ONI-HC-Vacc. Other':
            var sql  = "Select distinct HC_Vac_Other from ONI where HC_Vac_Other != null Or HC_Vac_Other != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Other";
            break;
            case 'ONI-HC-Vacc. Other Date':
            var sql  = "Select distinct HC_Vac_Other_Date from ONI where HC_Vac_Other_Date != null Or HC_Vac_Other_Date != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Vacc. Other Date";
            break;
            case 'ONI-HC-Driving MV':
            var sql  = "Select distinct HC_Driving from ONI where HC_Driving != null Or HC_Driving != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Driving MV";
            break;
            case 'ONI-HC-Driving Fit':
            var sql  = "Select distinct HC_FitToDrive from ONI where HC_FitToDrive != null Or HC_FitToDrive != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
            break;
            case 'ONI-HC-Driving Comments':
            var sql  = "Select distinct HC_DrivingComments from ONI where HC_DrivingComments != null Or HC_DrivingComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Driving Comments";
            break;
            case 'ONI-HC-Continence Urinary':
            var sql  = "Select distinct HC_Continence_Urine from ONI where HC_Continence_Urine != null Or HC_Continence_Urine != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Continence Urinary";
            break;
            case 'ONI-HC-Urinary Related To Coughing':
            var sql  = "Select distinct HC_Continence_Urine_Sneeze from ONI where HC_Continence_Urine_Sneeze != null Or HC_Continence_Urine_Sneeze != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Urinary Related To Coughing";
            break;
            case 'ONI-HC-Continence Comments':
            var sql  = "Select distinct HC_ContinenceComments from ONI where HC_ContinenceComments != null Or HC_ContinenceComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Continence Comments";
            break;
            case 'ONI-HC-Weight':
            var sql  = "Select distinct HC_Weight from ONI where HC_Weight != null Or HC_Weight != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Weight";
            break;
            case 'ONI-HC-Height':
            var sql  = "Select distinct HC_Height from ONI where HC_Height != null Or HC_Height != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Height";
            break;
            case 'ONI-HC-BMI':
            var sql  = "Select distinct HC_BMI from ONI where HC_BMI != null Or HC_BMI != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-BMI";
            break;
            case 'ONI-HC-BP Systolic':
            var sql  = "Select distinct HC_BP_Systolic from ONI where HC_BP_Systolic != null Or HC_BP_Systolic != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "NI-HC-BP Systolic"; 
            break;                 
            case 'ONI-HC-BP Diastolic':
            var sql  = "Select distinct HC_BP_Diastolic from ONI where HC_BP_Diastolic != null Or HC_BP_Diastolic != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-BP Diastolic";
            break;
            case 'ONI-HC-Pulse Rate':
            var sql  = "Select distinct HC_PulseRate from ONI where HC_PulseRate != null Or HC_PulseRate != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Pulse Rate";
            break;          
            case 'ONI-HC-Pulse Regularity':
            var sql  = "Select distinct HC_Pulse from ONI where HC_Pulse != null Or HC_Pulse != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Pulse Regularity";
            break;
            case 'ONI-HC-Check Postural Hypotension':
            var sql  = "Select distinct HC_PHCheck from ONI where HC_PHCheck != null Or HC_PHCheck != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Check Postural Hypotension";
            break;
            case 'ONI-HC-Conditions':
            var sql  = "Select distinct Description from ONIHealthConditions where Description != null Or Description != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Conditions";
            break;
            case 'ONI-HC-Diagnosis':
            var sql  = "Select distinct Description from MDiagnosis where Description != null Or Description != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Diagnosis";
            break;
            case 'ONI-HC-Medicines':
            var sql  = "Select distinct Description from ONIMedications where Description != null Or Description != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Medicines";
            break;
            case 'ONI-HC-Take Own Medication':
            var sql  = "Select distinct HC_Med_TakeOwn from ONI where HC_Med_TakeOwn != null Or HC_Med_TakeOwn != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Take Own Medication";
            break;                
            case 'ONI-HC-Willing When Prescribed': 
            var sql  = "Select distinct HC_Med_Willing from ONI where HC_Med_Willing != null Or HC_Med_Willing != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Willing When Prescribed";
            break;
            case 'ONI-HC-Coop With Health Services': 
            var sql  = "Select distinct HC_Med_Coop from ONI where HC_Med_Coop != null Or HC_Med_Coop != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Coop With Health Services";
            break;
            case 'ONI-HC-Webster Pack':
            var sql  = "Select distinct HC_Med_Webster from ONI where HC_Med_Webster != null Or HC_Med_Webster != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Webster Pack";
            break;
            case 'ONI-HC-Medication Review':
            var sql  = "Select distinct HC_Med_Review from ONI where HC_Med_Review != null Or HC_Med_Review != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Medication Review";
            break;
            case 'ONI-HC-Medical Comments':
            var sql  = "Select distinct HC_MedComments from ONI where HC_MedComments != null Or HC_MedComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HC-Medical Comments";
            break;
          //ONI-Psychosocial Profile                  
            case 'ONI-PS-K10-1':
            var sql  = "Select distinct PP_K10_1 from ONI where PP_K10_1 != null Or PP_K10_1 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-1";
            break;
            case 'ONI-PS-K10-2':
            var sql  = "Select distinct PP_K10_2 from ONI where PP_K10_2 != null Or PP_K10_2 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-2";
            break;
            case 'ONI-PS-K10-3':
            var sql  = "Select distinct PP_K10_3 from ONI where PP_K10_3 != null Or PP_K10_3 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-3";
            break;
            case 'ONI-PS-K10-4':
            var sql  = "Select distinct PP_K10_4 from ONI where PP_K10_4 != null Or PP_K10_4 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-4";
            break;
            case 'ONI-PS-K10-5':
            var sql  = "Select distinct PP_K10_5 from ONI where PP_K10_5 != null Or PP_K10_5 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-5";
            break;
            case 'ONI-PS-K10-6':
            var sql  = "Select distinct PP_K10_6 from ONI where PP_K10_6 != null Or PP_K10_6 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-6";
            break;
            case 'ONI-PS-K10-7':
            var sql  = "Select distinct PP_K10_7 from ONI where PP_K10_7 != null Or PP_K10_7 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-7";
            break;
            case 'ONI-PS-K10-8':
            var sql  = "Select distinct PP_K10_8 from ONI where PP_K10_8 != null Or PP_K10_8 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-8";
            break;
            case 'ONI-PS-K10-9':
            var sql  = "Select distinct PP_K10_9 from ONI where PP_K10_9 != null Or PP_K10_9 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-9";
            break;
            case 'ONI-PS-K10-10':
            var sql  = "Select distinct PP_K10_10 from ONI where PP_K10_10 != null Or PP_K10_10 != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-K10-10";
            break;
            case 'ONI-PS-Sleep Difficulty':
            var sql  = "Select distinct PP_SleepingDifficulty from ONI where PP_SleepingDifficulty != null Or PP_SleepingDifficulty != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Sleep Difficulty";
            break;
            case 'ONI-PS-Sleep Details':
            var sql  = "Select distinct  from ONI where != null Or  != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Sleep Details";
            break;
            case 'ONI-PS-Personal Support':
            var sql  = "Select distinct PP_PersonalSupport from ONI where PP_PersonalSupport != null Or PP_PersonalSupport != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Personal Support";
            break;
            case 'ONI-PS-Personal Support Comments':
            var sql  = "Select distinct PP_PersonalSupportComments from ONI where PP_PersonalSupportComments != null Or PP_PersonalSupportComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Personal Support Comments";
            break;
            case 'ONI-PS-Keep Friendships':
            var sql  = "Select distinct PP_Relationships_KeepUp from ONI where PP_Relationships_KeepUp != null Or PP_Relationships_KeepUp != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Keep Friendships";
            break;
            case 'ONI-PS-Problems Interacting':
            var sql  = "Select distinct PP_Relationships_Problem from ONI where PP_Relationships_Problem != null Or PP_Relationships_Problem != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Problems Interacting";
            break;
            case 'ONI-PS-Family/Relationship Comments':
            var sql  = "Select distinct PP_RelationshipsComments from ONI where PP_RelationshipsComments != null Or PP_RelationshipsComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Family/Relationship Comments";
            break;
            case 'ONI-PS-Svc Provider Relations':
            var sql  = "Select distinct PP_Relationships_SP from ONI where PP_Relationships_SP != null Or PP_Relationships_SP != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Svc Provider Relations";
            break;
            case 'ONI-PS-Svc Provider Comments': 
            var sql  = "Select distinct PP_Relationships_SPComments from ONI where PP_Relationships_SPComments != null Or PP_Relationships_SPComments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-PS-Svc Provider Comments";
            break;
            //ONI-Health Behaviours Profile                  
            case 'ONI-HB-Regular Health Checks':
            var sql  = "Select distinct HBP_HealthChecks from ONI where HBP_HealthChecks != null Or HBP_HealthChecks != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Regular Health Checks";
            break;
            case 'ONI-HB-Last Health Check':
            var sql  = "Select distinct HBP_HealthChecks_Last from ONI where HBP_HealthChecks_Last != null Or HBP_HealthChecks_Last != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Last Health Check";
            break;                                    
            case 'ONI-HB-Health Screens':
            var sql  = "Select distinct HBP_HealthChecks_List from ONI where HBP_HealthChecks_List != null Or HBP_HealthChecks_List != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Health Screens";
            break;
            case 'ONI-HB-Smoking':
            var sql  = "Select distinct HBP_Smoking from ONI where HBP_Smoking != null Or HBP_Smoking != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Smoking";
            break;                  
            case 'ONI-HB-If Quit Smoking - When?':
            var sql  = "Select distinct HBP_Smoking_Quit from ONI where HBP_Smoking_Quit != null Or HBP_Smoking_Quit != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-If Quit Smoking - When?";
            break;
            case 'ONI-HB-Alcohol-How often?': 
            var sql  = "Select distinct HBP_Alcohol from ONI where HBP_Alcohol != null Or HBP_Alcohol != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Alcohol-How often?";
            break;
            case 'ONI-HB-Alcohol-How many?':
            var sql  = "Select distinct HBP_Alcohol_NoDrinks from ONI where HBP_Alcohol_NoDrinks != null Or HBP_Alcohol_NoDrinks != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Alcohol-How many?";
            break;
            case 'ONI-HB-Alcohol-How often over 6?':
            var sql  = "Select distinct HBP_Alcohol_BingeNo from ONI where HBP_Alcohol_BingeNo != null Or HBP_Alcohol_BingeNo != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NI-HB-Alcohol-How often over 6?";
            break; 
            case 'ONI-HB-Lost Weight':
            var sql  = "Select distinct HBP_Malnutrition_LostWeight from ONI where HBP_Malnutrition_LostWeight != null Or HBP_Malnutrition_LostWeight != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Lost Weight";
            break;
            case 'ONI-HB-Eating Poorly':
            var sql  = "Select distinct HBP_Malnutrition_PoorEating from ONI where HBP_Malnutrition_PoorEating != null Or HBP_Malnutrition_PoorEating != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Eating Poorly";
            break;
            case 'ONI-HB-How much wieght lost':
            var sql  = "Select distinct HBP_Malnutrition_LostWeightAmount from ONI where HBP_Malnutrition_LostWeightAmount != null Or HBP_Malnutrition_LostWeightAmount != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-How much wieght lost";
            break;
            case 'ONI-HB-Malnutrition Score':
            var sql  = "Select distinct HBP_Malnutrition_Score from ONI where HBP_Malnutrition_Score != null Or HBP_Malnutrition_Score != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Malnutrition Score";
            break;
            case 'ONI-HB-8 cups fluid':
            var sql  = "Select distinct HBP_Hydration_AdequateFluid from ONI where HBP_Hydration_AdequateFluid != null Or HBP_Hydration_AdequateFluid != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-8 cups fluid";
            break;
            case 'ONI-HB-Recent decrease in fluid':
            var sql  = "Select distinct HBP_Hydration_DecreasedFluid from ONI where HBP_Hydration_DecreasedFluid != null Or HBP_Hydration_DecreasedFluid != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Recent decrease in fluid";
            break;
            case 'ONI-HB-Weight':
            var sql  = "Select distinct HBP_Weight from ONI where HBP_Weight != null Or HBP_Weight != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Weight";
            break;
            case 'ONI-HB-Physical Activity':
            var sql  = "Select distinct HBP_PhysicalActivity from ONI where HBP_PhysicalActivity != null Or HBP_PhysicalActivity != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Physical Activity";
            break;
            case 'ONI-HB-Physical Fitness':
            var sql  = "Select distinct HBP_PhysicalFitness from ONI where HBP_PhysicalFitness != null Or HBP_PhysicalFitness != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Physical Fitness";
            break;
            case 'ONI-HB-Fitness Comments':
            var sql  = "Select distinct HBP_Comments from ONI where HBP_Comments != null Or HBP_Comments != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-HB-Fitness Comments";
            break;
            //ONI-CP-Need for carer 
            case 'ONI-CP-Need for carer':
            var sql  = "Select distinct C_General_NeedForCarer from ONI where C_General_NeedForCarer != null Or C_General_NeedForCarer != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Need for carer";
            break;                 
            case 'ONI-CP-Carer Availability':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CARER AVAILABILITY' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer Availability";
            break;                            
            case 'ONI-CP-Carer Residency Status':
            var sql  = "Select distinct CarerResidency from Recipients where CarerResidency != null Or CarerResidency != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer Residency Status";
            break;
            case 'ONI-CP-Carer Relationship':
            var sql  = "Select distinct Description from DataDomains where Domain = 'CSTDA_CarerRelationship' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer Relationship";
            break;
            case 'ONI-CP-Carer has help':
            var sql  = "Select distinct C_Support_Help from ONI where C_Support_Help != null Or C_Support_Help != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer has help";
            break;
            case 'ONI-CP-Carer receives payment':
            var sql  = "Select distinct C_Support_Allowance from ONI where C_Support_Allowance != null Or C_Support_Allowance != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer receives payment";
            break;
            case 'ONI-CP-Carer made aware support services':
            var sql  = "Select distinct C_Support_Information from ONI where C_Support_Information != null Or C_Support_Information != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer made aware support services";
            break;               
            case 'ONI-CP-Carer needs training':
            var sql  = "Select distinct C_Support_NeedTraining from ONI where C_Support_NeedTraining != null Or C_Support_NeedTraining != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer needs training";
            break;
            case 'ONI-CP-Carer threat-emotional':
            var sql  = "Select distinct C_Threats_Emotional from ONI where C_Threats_Emotional != null Or C_Threats_Emotional != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer threat-emotional";
            break;
            case 'ONI-CP-Carer threat-acute physical':
            var sql  = "Select distinct C_Threats_Physical from ONI where C_Threats_Physical != null Or C_Threats_Physical != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer threat-acute physical";
            break;
            case 'ONI-CP-Carer threat-slow physical':
            var sql  = "Select distinct C_Threats_Physical_Slow from ONI where C_Threats_Physical_Slow != null Or C_Threats_Physical_Slow != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer threat-slow physical";
            break;
            case 'ONI-CP-Carer threat-other factors':
            var sql  = "Select distinct C_Threats_Unrelated from ONI where C_Threats_Unrelated != null Or C_Threats_Unrelated != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer threat-other factors";
            break;
            case 'ONI-CP-Carer threat-increasing consumer needs':
            var sql  = "Select distinct C_Threats_ConsumerNeeds from ONI where C_Threats_ConsumerNeeds != null Or C_Threats_ConsumerNeeds != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer threat-increasing consumer needs";
            break;
            case 'ONI-CP-Carer threat-other comsumer factors':
            var sql  = "Select distinct C_Threats_ConsumerOther from ONI where C_Threats_ConsumerOther != null Or C_Threats_ConsumerOther != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer threat-other comsumer factors";
            break;
            case 'ONI-CP-Carer arrangements sustainable':
            var sql  = "Select distinct C_Issues_Sustainability from ONI where C_Issues_Sustainability != null Or C_Issues_Sustainability != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer arrangements sustainable";
            break;
            case 'ONI-CP-Carer Comments':
            var sql  = "Select distinct C_Issues_Comments from ONI where C_Issues_Comments != null Or C_Issues_Comments != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CP-Carer Comments";
            break;
            //ONI-CS-Year of Arrival                
            case 'ONI-CS-Year of Arrival':
            var sql  = "Select distinct CAL_ArrivalYear from ONI where CAL_ArrivalYear != null Or CAL_ArrivalYear != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Year of Arrival";
            break;               
            case 'ONI-CS-Citizenship Status':
            var sql  = "Select distinct CAL_Citizenship from ONI where CAL_Citizenship != null Or CAL_Citizenship != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Citizenship Status";
            break;
            case 'ONI-CS-Reasons for moving to Australia':
            var sql  = "Select distinct CAL_ReasonsMoveAustralia from ONI where CAL_ReasonsMoveAustralia != null Or CAL_ReasonsMoveAustralia != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Reasons for moving to Australia";
            break;
            case 'ONI-CS-Primary/Secondary Language Fluency':
            var sql  = "Select distinct CAL_PrimSecLanguage from ONI where CAL_PrimSecLanguage != null Or CAL_PrimSecLanguage != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Primary/Secondary Language Fluency";
            break;
            case 'ONI-CS-Fluency in English':
            var sql  = "Select distinct CAL_EnglishProf from ONI where CAL_EnglishProf != null Or CAL_EnglishProf != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Fluency in English";
            break;
            case 'ONI-CS-Literacy in primary language':
            var sql  = "Select distinct CAL_PrimaryLiteracy from ONI where CAL_PrimaryLiteracy != null Or CAL_PrimaryLiteracy != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Literacy in primary language";
            break;
            case 'ONI-CS-Literacy in English':
            var sql  = "Select distinct CAL_EnglishLiteracy from ONI where CAL_EnglishLiteracy != null Or CAL_EnglishLiteracy != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "NI-CS-Literacy in English";
            break;
            case 'ONI-CS-Non verbal communication style':
            var sql  = "Select distinct CAL_NonVerbalStyle from ONI where CAL_NonVerbalStyle != null Or CAL_NonVerbalStyle != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Non verbal communication style";
            break;
            case 'ONI-CS-Marital Status':
            var sql  = "Select distinct CAL_Marital from ONI where CAL_Marital != null Or CAL_Marital != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Marital Status";
            break;
            case 'ONI-CS-Religion':
            var sql  = "Select distinct Description from DataDomains where Domain = 'RELIGION' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Religion";
            break;
            case 'ONI-CS-Employment history in country of origin':
            var sql  = "Select distinct CAL_EmploymentHistory from ONI where CAL_EmploymentHistory != null Or CAL_EmploymentHistory != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Employment history in country of origin";
            break;
            case 'ONI-CS-Employment history in Australia':
            var sql  = "Select distinct CAL_EmploymentHistoryAust from ONI where CAL_EmploymentHistoryAust != null Or CAL_EmploymentHistoryAust != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Employment history in Australia";
            break;
            case 'ONI-CS-Specific dietary needs':
            var sql  = "Select distinct CAL_DietaryNeeds from ONI where CAL_DietaryNeeds != null Or CAL_DietaryNeeds != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Specific dietary needs";
            break;
            case 'ONI-CS-Specific cultural needs':
            var sql  = "Select distinct CAL_SpecificCulturalNeeds from ONI where CAL_SpecificCulturalNeeds != null Or CAL_SpecificCulturalNeeds != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Specific cultural needs";
            break;
            case 'ONI-CS-Someone to talk to for day to day problems':
            var sql  = "Select distinct CALSocIsol_1 from ONI where CALSocIsol_1 != null Or CALSocIsol_1 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Someone to talk to for day to day problems";
            break;          
            case 'ONI-CS-Miss having close freinds':
            var sql  = "Select distinct CALSocIsol_2 from ONI where CALSocIsol_2 != null Or CALSocIsol_2 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Miss having close freinds";
            break;
            case 'ONI-CS-Experience general sense of emptiness':
            var sql  = "Select distinct CALSocIsol_3 from ONI where CALSocIsol_3 != null Or CALSocIsol_3 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Experience general sense of emptiness";
            break;
            case 'ONI-CS-Plenty of people to lean on for problems':
            var sql  = "Select distinct CALSocIsol_4 from ONI where CALSocIsol_4 != null Or CALSocIsol_4 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Plenty of people to lean on for problems";
            break;
            case 'ONI-CS-Miss the pleasure of the company of others':
            var sql  = "Select distinct CALSocIsol_5 from ONI where CALSocIsol_5 != null Or CALSocIsol_5 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Miss the pleasure of the company of others";
            break;
            case 'ONI-CS-Circle of friends and aquaintances too limited':
            var sql  = "Select distinct CALSocIsol_6 from ONI where CALSocIsol_6 != null Or CALSocIsol_6 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Circle of friends and aquaintances too limited";
            break;
            case 'ONI-CS-Many people I trust completely':
            var sql  = "Select distinct CALSocIsol_7 from ONI where CALSocIsol_7 != null Or CALSocIsol_7 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Many people I trust completely";
            break;               
            case 'ONI-CS-Enough people I feel close to':
            var sql  = "Select distinct CALSocIsol_8 from ONI where CALSocIsol_8 != null Or CALSocIsol_8 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Enough people I feel close to";
            break;
            case 'ONI-CS-Miss having people around':
            var sql  = "Select distinct CALSocIsol_9 from ONI where CALSocIsol_9 != null Or CALSocIsol_9 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Miss having people around";
            break;
            case 'ONI-CS-Often feel rejected':
            var sql  = "Select distinct CALSocIsol_10 from ONI where CALSocIsol_10 != null Or CALSocIsol_10 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Often feel rejected";
            break;
            case 'ONI-CS-Can call on my friends whenever I need them':
            var sql  = "Select distinct CALSocIsol_11 from ONI where CALSocIsol_11 != null Or CALSocIsol_11 != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "ONI-CS-Can call on my friends whenever I need them";
            break;
            //Loan Items                  
            case 'Loan Item Type':
            var sql  = "Select distinct [Type] from HumanResources where  HRLoan.[Group] = 'LOANITEMS' and [Type] != null  "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Loan Item Type";
            break;
            case 'Loan Item Description':
            var sql  = "Select distinct [Name] from HumanResources where HRLoan.[Group] = 'LOANITEMS' and [Name] != null Or [Name] != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "Loan Item Description";
            break;
            case 'Loan Item Date Loaned/Installed':
            var sql  = "Select distinct format([Date1],'dd/MM/yyyy') as [Date1] from HumanResources where HRLoan.[Group] = 'LOANITEMS' and Date2 != null "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Loan Item Date Loaned/Installed";
            break;
            case 'Loan Item Date Collected':
            var sql  = "Select distinct format([Date2],'dd/MM/yyyy') as [Date2] from HumanResources where HRLoan.[Group] = 'LOANITEMS' and Date2 != null "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Loan Item Date Collected";
            break;
            //  service information Fields                  
            case 'Staff Code':
            var sql  = "Select distinct [Carer Code] from Roster where [Carer Code] != null Or [Carer Code] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Staff Code(s)";
            break;
            case 'Service Date':
            var sql  = "Select distinct Date as Date from Roster where Date != null Or Date != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Date";
            break;
            case 'Service Start Time':
            var sql  = "Select distinct [Start Time] from Roster where [Start Time] != null Or [Start Time] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Start Time";
            break;
            case 'Service Code':
            var sql  = "Select distinct [Service Type] from Roster where [Service Type] != null Or [Service Type] != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Service Code";
            break;
            case 'Service Hours':
            var sql  = "Select distinct (([Duration]*5) / 60) as ServiceHours from Roster where Duration != null Or Duration != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Service Hours";
            break;
            case 'Service Pay Rate':
            var sql  = "Select distinct [Unit Pay Rate] from Roster where [Unit Pay Rate] != null Or [Unit Pay Rate] != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Service Pay Rate";
            break;
            case 'Service Bill Rate':
            var sql  = "Select distinct [Unit Bill Rate] from Roster where [Unit Bill Rate] != null Or [Unit Bill Rate] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Bill Rate";
            break;
            case 'Service Bill Qty':
            var sql  = "Select distinct BillQty from Roster where BillQty != null Or BillQty != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Bill Qty";
            break;
            case 'Service Location/Activity Group':
            var sql  = "Select distinct [ServiceSetting] from Roster where [ServiceSetting] != null Or [ServiceSetting] != '' "
            this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
            this.ModalTitle = "Service Location/Activity Group";
            break;
            case 'Service Program':
            var sql  = "Select distinct [Program] from Roster where [Program] != null Or [Program] != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Service Program";
            break;
            case 'Service Group':
            var sql  = "Select distinct [Type] from Roster where [Type] != null Or [Type] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Group";
            break;
            case 'Service HACC Type': 
            var sql  = "Select distinct [HACCType] from Roster where [HACCType] != null Or [HACCType] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service HACC Type";
            break;
            case 'Service Category':
            var sql  = "Select distinct [Anal] from Roster where [Anal] != null Or [Anal] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Category";
            break;
            case 'Service Status':
            var sql  = "Select distinct [Status] from Roster where [Status] != null Or [Status] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Status";
            break;
            case 'Service Pay Type':
            var sql  = "Select distinct [Service Description] from Roster where [Service Description] != null Or [Service Description] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Pay Type";
            break;
            case 'Service Pay Qty':
            var sql  = "Select distinct [CostQty] from Roster where [CostQty] != null Or [CostQty] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Pay Qty";
            break;                  
            case 'Service End Time/ Shift End Time':
            var sql  = "Select distinct Convert(varchar,DATEADD(minute,([Duration]*5) ,[Start Time]),108) as EndTime from Roster  "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Service End Time/ Shift End Time";
            break;
            case 'Service Funding Source':
            var sql  = "Select distinct [Type] from Humanresourcetypes where [Type] != null Or [Type] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Service Funding Source";
            break;    /*                 
            case 'Service Notes':
            this.ConditionEntity =  'CAST(History.Detail AS varchar(4000))'
            break; */
            //Service Specific Competencies                
            case 'Activity':
              var sql  = "Select distinct [Group] from HumanResources where [Type] = 'STAFFATTRIBUTE' and [Group] != null Or [Group] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Activity";
              break;
              case 'Competency':
              var sql  = "Select distinct [Name] from HumanResources where [Type] = 'STAFFATTRIBUTE' and [Name] != null Or [Name] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "Competencies";
              break;
              case 'S Status': //check
                var sql  = "Select distinct [ServiceStatus] from Recipients where [ServiceStatus] != null Or [ServiceStatus] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "S Status";
              break;
              //  RECIPIENT OP NOTES                  
            case 'OP Notes Date':
              var sql  = "Select distinct  from History where ExtraDetail1 = 'OPNOTE' != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "OP Notes Date";
              break; /*
              case 'OP Notes Detail':
              this.ConditionEntity =  'OPHistory.[Detail]'
              var sql  = "Select distinct [Detail] from History where [Detail] != null Or [Detail] != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;   */                 
              case 'OP Notes Creator':
              var sql  = "Select distinct Creator from History where ExtraDetail1 = 'OPNOTE' and Creator != null Or Creator != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "OP Notes Creator";
              break;
              case 'OP Notes Alarm':
              var sql  = "Select distinct format(AlarmDate,'dd/MM/yyyy') as AlarmDate from History where ExtraDetail1 = 'OPNOTE' and AlarmDate != null Or AlarmDate != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "OP Notes Alarm";
              break;
              case 'OP Notes Program':
              var sql  = "Select distinct Program from History where ExtraDetail1 = 'OPNOTE' and Program != null Or Program != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "OP Notes Program(s)";
              break;
              case 'OP Notes Category':
              var sql  = "Select distinct ExtraDetail2 from History where ExtraDetail1 = 'OPNOTE' and ExtraDetail2 != null Or ExtraDetail2 != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "OP Notes Categories";
              break;
            
              // RECIPIENT CLINICAL NOTES                  
              case 'Clinical Notes Date':
              var sql  = "Select distinct format(DetailDate,'dd/MM/yyyy') as DetailDate from History where ExtraDetail1 = 'CLINNOTE' and DetailDate != null Or DetailDate != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Clinical Notes Date";
              break; /*
              case 'Clinical Notes Detail':
              this.ConditionEntity =  'dbo.RTF2TEXT(CliniHistory.[Detail])'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break; */
              case 'Clinical Notes Creator':
              var sql  = "Select distinct Creator from History where ExtraDetail1 = 'CLINNOTE' and Creator != null Or Creator != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Clinical Notes Creator";
              break;
              
              case 'Clinical Notes Alarm':
              var sql  = "Select distinct format(AlarmDate,'dd/MM/yyyy') as AlarmDate from History where ExtraDetail1 = 'CLINNOTE' and AlarmDate != null Or AlarmDate != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Clinical Notes Alarm";
              break;
              case 'Clinical Notes Category':
              var sql  = "Select distinct ExtraDetail2 from History where ExtraDetail1 = 'CLINNOTE' and ExtraDetail2 != null Or ExtraDetail2 != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "Clinical Notes Category";
              break;
              // RECIPIENT INCIDENTS                  
              case 'INCD_Status':
              var sql  = "Select distinct Status from IM_Master where Status != null Or Status != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Status";
              break;
              case 'INCD_Date':
              var sql  = "Select distinct format(Date,'dd/MM/yyyy') as Date from IM_Master where Date != null Or Date != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Date";
              break;
              case 'INCD_Type':
              var sql  = "Select distinct [Type] from IM_Master where [Type] != null Or [Type] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Type";
              break;
              case 'INCD_Description':
              var sql  = "Select distinct ShortDesc from IM_Master where ShortDesc != null Or ShortDesc != '' "
              this.bodystyleModal_IN = { height:'500px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Description";
              break;
              case 'INCD_SubCategory':
              var sql  = "Select distinct PerpSpecify from IM_Master where PerpSpecify != null Or PerpSpecify != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_SubCategory";
              break;
              case 'INCD_Assigned_To':
              var sql  = "Select distinct CurrentAssignee from IM_Master where CurrentAssignee != null Or CurrentAssignee != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Assigned_To";
              break;         
              case 'INCD_Service':
              var sql  = "Select distinct Service from IM_Master where Service != null Or Service != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Service";
              break;
              case 'INCD_Severity':
              var sql  = "Select distinct Severity from IM_Master where Severity != null Or Severity != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Severity";
              break;
              case 'INCD_Time':
              var sql  = "Select distinct Time from IM_Master where Time != null Or Time != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Time";
              break;
              case 'INCD_Duration':
              var sql  = "Select distinct  from IM_Master where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Duration";
              break;
              case 'INCD_Location':
              var sql  = "Select distinct Location from IM_Master where Location != null Or Location != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Location";
              break;
              case 'INCD_LocationNotes': //space check
              var sql  = "Select distinct LocationNotes from IM_Master where LocationNotes != null Or LocationNotes != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_LocationNotes";
              break;
              case 'INCD_ReportedBy':
              var sql  = "Select distinct ReportedBy from IM_Master where ReportedBy != null Or ReportedBy != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_ReportedBy";
              break;
              case 'INCD_DateReported':
              var sql  = "Select distinct format(DateReported,'dd/MM/yyyy') as DateReported from IM_Master where DateReported != null Or DateReported != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_DateReported";
              break;
              case 'INCD_Reported':
              var sql  = "Select distinct Reported from IM_Master where Reported != null Or Reported != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Reported";
              break;/*
              case 'INCD_FullDesc':
              this.ConditionEntity =  'IMM.FullDesc'
              var sql  = "Select distinct  from Recipients where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "";
              break;*/
              case 'INCD_Program':
              var sql  = "Select distinct Program from IM_Master where Program != null Or Program != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Program";
              break;
              case 'INCD_DSCServiceType': //space
              var sql  = "Select distinct DSCServiceType from IM_Master where DSCServiceType != null Or DSCServiceType != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_DSCServiceType";
              break;
              case 'INCD_TriggerShort':
              var sql  = "Select distinct TriggerShort from IM_Master where TriggerShort != null Or TriggerShort != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_TriggerShort";
              break;
              case 'INCD_incident_level':
              var sql  = "Select distinct incident_level from IM_Master where incident_level != null Or incident_level != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_incident_level";
              break;
              case 'INCD_Area':
              var sql  = "Select distinct area from IM_Master where area != null Or area != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Area";
              break;
              case 'INCD_Region':
              var sql  = "Select distinct Region from IM_Master where Region != null Or Region != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Region";
              break;
              case 'INCD_position':
              var sql  = "Select distinct position from IM_Master where position != null Or position != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_position";
              break; /*
              case 'INCD_phone':
              this.ConditionEntity =  'IMM.phone'
              var sql  = "Select distinct  from IM_Master where != null Or  != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "INCD_phone";
              break; */
              case 'INCD_verbal_date':
              var sql  = "Select distinct format(verbal_date,'dd/MM/yyyy') as verbal_date from IM_Master where verbal_date != null Or verbal_date != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_verbal_date";
              break;
              case 'INCD_verbal_time':
              var sql  = "Select distinct verbal_time from IM_Master where verbal_time != null Or verbal_time != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_verbal_time";
              break;
              case 'INCD_By_Whom': 
              var sql  = "Select distinct By_Whome from IM_Master where By_Whome != null Or By_Whome != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_By_Whom";
              break;
              case 'INCD_To_Whom':
              var sql  = "Select distinct To_Whome from IM_Master where To_Whome != null Or To_Whome != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_To_Whom";
              break;
              case 'INCD_BriefSummary':
              var sql  = "Select distinct BriefSummary from IM_Master where BriefSummary != null Or BriefSummary != '' "
              this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
              this.ModalTitle = "INCD_BriefSummary";
              break;
              case 'INCD_ReleventBackground':
              var sql  = "Select distinct ReleventBackground from IM_Master where ReleventBackground != null Or ReleventBackground != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_ReleventBackground";
              break;
              case 'INCD_SummaryofAction':
              var sql  = "Select distinct SummaryofAction from IM_Master where SummaryofAction != null Or SummaryofAction != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_SummaryofAction";
              break;
              case 'INCD_SummaryOfOtherAction':
              var sql  = "Select distinct SummaryOfOtherAction from IM_Master where SummaryOfOtherAction != null Or SummaryOfOtherAction != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_SummaryOfOtherAction";
              break;
              case 'INCD_Triggers':
              var sql  = "Select distinct Triggers from IM_Master where Triggers != null Or Triggers != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Triggers";
              break;
              case 'INCD_InitialAction':
              var sql  = "Select distinct InitialAction from IM_Master where InitialAction != null Or InitialAction != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_InitialAction";
              break;
              case 'INCD_InitialNotes':
              var sql  = "Select distinct InitialNotes from IM_Master where InitialNotes != null Or InitialNotes != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_InitialNotes";
              break;
              case 'INCD_InitialFupBy':
              this.ConditionEntity =  'IMM.InitialFupBy'
              var sql  = "Select distinct InitialFupBy from IM_Master where InitialFupBy != null Or InitialFupBy != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_InitialFupBy";
              break;
              case 'INCD_Completed':
              var sql  = "Select distinct Completed from IM_Master where Completed != null Or Completed != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Completed";
              break;
              case 'INCD_OngoingAction':
              var sql  = "Select distinct OngoingAction from IM_Master where OngoingAction  != null Or OngoingAction != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_OngoingAction";
              break;
              case 'INCD_OngoingNotes':
              var sql  = "Select distinct OngoingNotes from IM_Master where OngoingNotes != null Or OngoingNotes != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_OngoingNotes";
              break;
              case 'INCD_Background':
              var sql  = "Select distinct Background from IM_Master where Background != null Or Background != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Background";
              break;
              case 'INCD_Abuse':
              var sql  = "Select distinct Abuse from IM_Master where Abuse != null Or Abuse != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Abuse";
              break;
              case 'INCD_DOPWithDisability':
              var sql  = "Select distinct DOPWithDisability from IM_Master where DOPWithDisability != null Or DOPWithDisability != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "INCD_DOPWithDisability";
              break;
              case 'INCD_SeriousRisks':
              var sql  = "Select distinct SeriousRisks from IM_Master where SeriousRisks != null Or SeriousRisks != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_SeriousRisks";
              break;
              case 'INCD_Complaints':
              var sql  = "Select distinct Complaints from IM_Master where Complaints != null Or Complaints != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Complaints";
              break;                                   
              case 'INCD_Perpetrator':
              var sql  = "Select distinct Perpetrator from IM_Master where Perpetrator != null Or Perpetrator != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Perpetrator";
              break;
              case 'INCD_Notify':
              var sql  = "Select distinct Notify from IM_Master where Notify != null Or Notify != '' "
              this.bodystyleModal_IN = { height:'200px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Notify";
              break;
              case 'INCD_NoNotifyReason':
              var sql  = "Select distinct NoNotifyReason from IM_Master where NoNotifyReason != null Or NoNotifyReason != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_NoNotifyReason";
              break; 
              case 'INCD_Notes':
              var sql  = "Select distinct Notes from IM_Master where Notes != null Or Notes != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Notes";
              break;
              case 'INCD_Setting':
              var sql  = "Select distinct Setting from IM_Master where Setting != null Or Setting != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Setting";
              break;
              case 'INCD_Involved_Staff':
              var sql  = "Select distinct [Staff#] from IM_INVOLVEDSTAFF where [Staff#] != null Or [Staff#] != '' "
              this.bodystyleModal_IN = { height:'500px' , overflow: 'auto' };
              this.ModalTitle = "INCD_Involved_Staff";
              break;
              //  Recipient Competencies                  
              case 'Recipient Competency':
              this.ConditionEntity =  'RecpCompet.Name'
              var sql  = "Select distinct Recurring from HumanResources where [Type] = 'RECIPATTRIBUTE' and Recurring != null Or Recurring != '' "
              this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
              this.ModalTitle = "Recipient Competencies";
              break;
              case 'Recipient Competency Mandatory':
              this.ConditionEntity =  'RecpCompet.Recurring'
              var sql  = "Select distinct Recurring from HumanResources where [Type] = 'RECIPATTRIBUTE' and Recurring != null Or Recurring != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Recipient Competency Mandatory";
              break;
              case 'Recipient Competency Notes':
              this.ConditionEntity =  'RecpCompet.[Notes]'
              var sql  = "Select distinct Notes from HumanResources where [Type] = 'RECIPATTRIBUTE' and Notes != null Or Notes != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "Recipient Competency Notes";
              break;
              //Care Plan                  
              case 'CarePlan ID':
              var sql  = "Select distinct [Doc#] from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and [Doc#] != null Or [Doc#] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan ID(s)";
              break;
              case 'CarePlan Name':
              var sql  = "Select distinct Title from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and Title != null Or Title != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan Name(s)";
              break;
              case 'CarePlan Type':
              var sql  = "SELECT Description FROM DataDomains Left join DOCUMENTS D on  DataDomains.RecordNumber = D.SubId"
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan Type(s)";
              break;
              case 'CarePlan Program':
              var sql  = "SELECT [Name] FROM HumanResourceTypes left join DOCUMENTS on  HumanResourceTypes.RecordNumber = DOCUMENTS.Department WHERE [Group] = 'PROGRAMS'"
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan Program(s)";
              break;                  
              case 'CarePlan Discipline':
              var sql  = "SELECT [Description] FROM DataDomains left join DOCUMENTS on  DataDomains.RecordNumber = DOCUMENTS.DPID"
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan Discipline";
              break;
              case 'CarePlan CareDomain':
              var sql  = "SELECT [Description] FROM DataDomains left join DOCUMENTS on DataDomains.RecordNumber = DOCUMENTS.CareDomain"
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan CareDomain";
              break;
              case 'CarePlan StartDate': 
              var sql  = "Select distinct format(DocStartDate,'dd/MM/yyyy') as DocStartDate from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and DocStartDate != null Or DocStartDate != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan StartDate";
              break;
              case 'CarePlan SignOffDate':
              this.ConditionEntity =  'D.DocEndDate' 
              var sql  = "Select distinct format(DocEndDate,'dd/MM/yyyy') as DocEndDate from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and DocEndDate != null Or DocEndDate != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan SignOffDate";
              break;
              case 'CarePlan ReviewDate':
              var sql  = "Select distinct format(AlarmDate,'dd/MM/yyyy') as AlarmDate from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and AlarmDate != null Or AlarmDate != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan ReviewDate";
              break;
              case 'CarePlan ReminderText':
              var sql  = "Select distinct AlarmText from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and AlarmText != null Or AlarmText != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan ReminderText";
              break;
              case 'CarePlan Archived':
              var sql  = "Select distinct DeletedRecord from Documents where ISNULL([Status], '') <> '' AND DOCUMENTGROUP IN ('CAREPLAN') and DeletedRecord != null Or DeletedRecord != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "CarePlan Archived";
              break;
              //Mental Health                  
              case 'MH-PERSONID':
              var sql  = "Select distinct [PERSONID] from MENTALHEALTHDATASET where [PERSONID] != null Or [PERSONID] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-PERSONID(s)";
              break;
              case 'MH-HOUSING TYPE ON REFERRAL':
              var sql  = "Select distinct [HOUSING TYPE ON REFERRAL] from MENTALHEALTHDATASET where [HOUSING TYPE ON REFERRAL] != null Or [HOUSING TYPE ON REFERRAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-HOUSING TYPE ON REFERRAL";
              break;
              case 'MH-RE REFERRAL':
              var sql  = "Select distinct [RE REFERRAL] from MENTALHEALTHDATASET where [RE REFERRAL] != null Or [RE REFERRAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-RE REFERRAL";
              break;
              case 'MH-REFERRAL SOURCE':
              var sql  = "Select distinct [REFERRAL SOURCE] from MENTALHEALTHDATASET where [REFERRAL SOURCE] != null Or [REFERRAL SOURCE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-REFERRAL SOURCE";
              break;
              case 'MH-REFERRAL RECEIVED DATE':
              var sql  = "Select distinct format([REFERRAL RECEIVED DATE],'dd/MM/yyyy') as [REFERRAL RECEIVED DATE] from MENTALHEALTHDATASET where [REFERRAL RECEIVED DATE] != null Or [REFERRAL RECEIVED DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-REFERRAL RECEIVED DATE";
              break;                
              case 'MH-ENGAGED AND CONSENT DATE':
              var sql  = "Select distinct format([ENGAGED AND CONSENT DATE],'dd/MM/yyyy') as [ENGAGED AND CONSENT DATE] from MENTALHEALTHDATASET where [ENGAGED AND CONSENT DATE] != null Or [ENGAGED AND CONSENT DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ENGAGED AND CONSENT DATE";
              break;
              case 'MH-OPEN TO HOSPITAL':
              var sql  = "Select distinct [OPEN TO HOSPITAL] from MENTALHEALTHDATASET where [OPEN TO HOSPITAL] != null Or [OPEN TO HOSPITAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-OPEN TO HOSPITAL";
              break;   /*             
              case 'MH-OPEN TO HOSPITAL DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[OPEN TO HOSPITAL DETAILS]'
              var sql  = "Select distinct [OPEN TO HOSPITAL DETAILS] from MENTALHEALTHDATASET where [OPEN TO HOSPITAL DETAILS] != null Or [OPEN TO HOSPITAL DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-OPEN TO HOSPITAL DETAILS";
              break; */
              case 'MH-ALERTS':
              var sql  = "Select distinct [ALERTS] from MENTALHEALTHDATASET where [ALERTS] != null Or [ALERTS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ALERTS";
              break;
              case 'MH-ALERTS DETAILS':
              var sql  = "Select distinct [ALERTS DETAILS] from MENTALHEALTHDATASET where [ALERTS DETAILS] != null Or [ALERTS DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ALERTS DETAILS";
              break;
              case 'MH-MH DIAGNOSIS':
              var sql  = "Select distinct [MH DIAGNOSIS] from MENTALHEALTHDATASET where [MH DIAGNOSIS] != null Or [MH DIAGNOSIS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-MH DIAGNOSIS";
              break;
              case 'MH-MEDICAL DIAGNOSIS':
              var sql  = "Select distinct [MEDICAL DIAGNOSIS] from MENTALHEALTHDATASET where [MEDICAL DIAGNOSIS] != null Or [MEDICAL DIAGNOSIS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-MEDICAL DIAGNOSIS";
              break;
              case 'MH-REASONS FOR EXIT':
              var sql  = "Select distinct [REASONS FOR EXIT] from MENTALHEALTHDATASET where [REASONS FOR EXIT] != null Or [REASONS FOR EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-REASONS FOR EXIT";
              break;                  
              case 'MH-SERVICES LINKED INTO':
              var sql  = "Select distinct [SERVICES LINKED INTO] from MENTALHEALTHDATASET where [SERVICES LINKED INTO] != null Or [SERVICES LINKED INTO] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-SERVICES LINKED INTO";
              break;
              case 'MH-NON ACCEPTED REASONS':
              var sql  = "Select distinct [NON ACCEPTED REASONS] from MENTALHEALTHDATASET where [NON ACCEPTED REASONS] != null Or [NON ACCEPTED REASONS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-NON ACCEPTED REASONS";
              break;
              case 'MH-NOT PROCEEDED':
              var sql  = "Select distinct [NOT PROCEEDED] from MENTALHEALTHDATASET where [NOT PROCEEDED] != null Or [NOT PROCEEDED] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-NOT PROCEEDED";
              break;               
              case 'MH-DISCHARGE DATE':
              var sql  = "Select distinct format([DISCHARGE DATE],'dd/MM/yyyy') as [DISCHARGE DATE] from MENTALHEALTHDATASET where [DISCHARGE DATE] != null Or [DISCHARGE DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-DISCHARGE DATE";
              break;
              case 'MH-CURRENT AOD':
              var sql  = "Select distinct [CURRENT AOD] from MENTALHEALTHDATASET where [CURRENT AOD]  != null Or [CURRENT AOD] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-CURRENT AOD";
              break;
              case 'MH-CURRENT AOD DETAILS':
              var sql  = "Select distinct [CURRENT AOD DETAILS] from MENTALHEALTHDATASET where [CURRENT AOD DETAILS]  != null Or [CURRENT AOD DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-CURRENT AOD DETAILS";
              break;
              case 'MH-PAST AOD':
              var sql  = "Select distinct [PAST AOD] from MENTALHEALTHDATASET where [PAST AOD]  != null Or [PAST AOD] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-PAST AOD";
              break;
              case 'MH-PAST AOD DETAILS':
              var sql  = "Select distinct [PAST AOD DETAILS] from MENTALHEALTHDATASET where [PAST AOD DETAILS]  != null Or [PAST AOD DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-PAST AOD DETAILS";
              break;
              case 'MH-ENGAGED AOD':
              var sql  = "Select distinct [ENGAGED AOD] from MENTALHEALTHDATASET where [ENGAGED AOD]  != null Or [ENGAGED AOD] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ENGAGED AOD";
              break;
              case 'MH-ENGAGED AOD DETAILS':
              var sql  = "Select distinct [ENGAGED AOD DETAILS] from MENTALHEALTHDATASET where [ENGAGED AOD DETAILS]  != null Or [ENGAGED AOD DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ENGAGED AOD DETAILS";
              break;
              case 'MH-SERVICES CLIENT IS LINKED WITH ON INTAKE':
              var sql  = "Select distinct [SERVICES CLIENT IS LINKED WITH ON INTAKE] from MENTALHEALTHDATASET where [SERVICES CLIENT IS LINKED WITH ON INTAKE]  != null Or [SERVICES CLIENT IS LINKED WITH ON INTAKE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-SERVICES CLIENT IS LINKED WITH ON INTAKE";
              break;
              case 'MH-SERVICES CLIENT IS LINKED WITH ON EXIT':
              var sql  = "Select distinct [SERVICES CLIENT IS LINKED WITH ON EXIT] from MENTALHEALTHDATASET where [SERVICES CLIENT IS LINKED WITH ON EXIT]  != null Or [SERVICES CLIENT IS LINKED WITH ON EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-SERVICES CLIENT IS LINKED WITH ON EXIT";
              break;
              case 'MH-ED PRESENTATIONS ON REFERRAL':
              var sql  = "Select distinct [ED PRESENTATIONS ON REFERRAL] from MENTALHEALTHDATASET where [ED PRESENTATIONS ON REFERRAL]  != null Or [ED PRESENTATIONS ON REFERRAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ED PRESENTATIONS ON REFERRAL";
              break;               
              case 'MH-ED PRESENTATIONS ON 3 MONTH REVIEW':
              var sql  = "Select distinct [ED PRESENTATIONS ON 3 MONTH REVIEW] from MENTALHEALTHDATASET where [ED PRESENTATIONS ON 3 MONTH REVIEW]  != null Or [ED PRESENTATIONS ON 3 MONTH REVIEW] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ED PRESENTATIONS ON 3 MONTH REVIEW";
              break;
              case 'MH-ED PRESENTATIONS ON EXIT':
              var sql  = "Select distinct [ED PRESENTATIONS ON EXIT] from MENTALHEALTHDATASET where [ED PRESENTATIONS ON EXIT]  != null Or [ED PRESENTATIONS ON EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ED PRESENTATIONS ON EXIT";
              break;
              case 'MH-AMBULANCE ARRIVAL ON REFERRAL':
              var sql  = "Select distinct [AMBULANCE ARRIVAL ON REFERRAL] from MENTALHEALTHDATASET where [AMBULANCE ARRIVAL ON REFERRAL] != null Or [AMBULANCE ARRIVAL ON REFERRAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-AMBULANCE ARRIVAL ON REFERRAL";
              break;
              case 'MH-AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW':
              var sql  = "Select distinct [AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW] from MENTALHEALTHDATASET where [AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW] != null Or [AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW";
              break;
              case 'MH-AMBULANCE ARRIVAL ON EXIT':
              var sql  = "Select distinct [AMBULANCE ARRIVAL ON EXIT] from MENTALHEALTHDATASET where [AMBULANCE ARRIVAL ON EXIT] != null Or [AMBULANCE ARRIVAL ON EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-AMBULANCE ARRIVAL ON EXIT";
              break;
              case 'MH-ADMISSIONS ON REFERRAL':
              var sql  = "Select distinct [ADMISSIONS ON REFERRAL] from MENTALHEALTHDATASET where [ADMISSIONS ON REFERRAL] != null Or [ADMISSIONS ON REFERRAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ADMISSIONS ON REFERRAL";
              break;
              case 'MH-ADMISSIONS ON MID-3 MONTH REVIEW':
              var sql  = "Select distinct [ADMISSIONS ON MID- 3 MONTH REVIEW] from MENTALHEALTHDATASET where [ADMISSIONS ON MID- 3 MONTH REVIEW] != null Or [ADMISSIONS ON MID- 3 MONTH REVIEW] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ADMISSIONS ON MID-3 MONTH REVIEW";
              break;
              case 'MH-ADMISSIONS TO ED ON TIME OF EXIT':
              var sql  = "Select distinct [ADMISSIONS TO ED ON TIME OF EXIT] from MENTALHEALTHDATASET where [ADMISSIONS TO ED ON TIME OF EXIT] != null Or [ADMISSIONS TO ED ON TIME OF EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-ADMISSIONS TO ED ON TIME OF EXIT"; 
              break;                  
              case 'MH-RESIDENTIAL MOVES':
              var sql  = "Select distinct [RESIDENTIAL MOVES] from MENTALHEALTHDATASET where [RESIDENTIAL MOVES] != null Or [RESIDENTIAL MOVES] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-RESIDENTIAL MOVES";
              break;
              case 'MH-DATE OF RESIDENTIAL CHANGE OF ADDRESS':
              var sql  = "Select distinct [DATE OF RESIDENTIAL CHANGE OF ADDRESS] from MENTALHEALTHDATASET where [DATE OF RESIDENTIAL CHANGE OF ADDRESS] != null Or [DATE OF RESIDENTIAL CHANGE OF ADDRESS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-DATE OF RESIDENTIAL CHANGE OF ADDRESS";
              break;
              case 'MH-LOCATION OF NEW ADDRESS':
              var sql  = "Select distinct [LOCATION OF NEW ADDRESS] from MENTALHEALTHDATASET where [LOCATION OF NEW ADDRESS] != null Or [LOCATION OF NEW ADDRESS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-LOCATION OF NEW ADDRESS";
              break;
              case 'MH-HOUSING TYPE ON EXIT':
              var sql  = "Select distinct [HOUSING TYPE ON EXIT] from MENTALHEALTHDATASET where [HOUSING TYPE ON EXIT] != null Or [HOUSING TYPE ON EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-HOUSING TYPE ON EXIT";
              break;
              case 'MH-KPI - INTAKE':
              var sql  = "Select distinct [KPI - INTAKE] from MENTALHEALTHDATASET where [KPI - INTAKE] != null Or [KPI - INTAKE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KPI - INTAKE";
              break;
              case 'MH-KPI - 3 MONTH REVEIEW':
              var sql  = "Select distinct [KPI - 3 MONTH REVEIEW] from MENTALHEALTHDATASET where [KPI - 3 MONTH REVEIEW] != null Or [KPI - 3 MONTH REVEIEW] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "'MH-KPI - 3 MONTH REVEIEW";
              break;
              case 'MH-KPI - EXIT':
              var sql  = "Select distinct [KPI - EXIT] from MENTALHEALTHDATASET where [KPI - EXIT] != null Or [KPI - EXIT] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KPI - EXIT";
              break;
              case 'MH-MEDICAL DIAGNOSIS DETAILS':
              var sql  = "Select distinct [MEDICAL DIAGNOSIS DETAILS] from MENTALHEALTHDATASET where [MEDICAL DIAGNOSIS DETAILS] != null Or [MEDICAL DIAGNOSIS DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-MEDICAL DIAGNOSIS DETAILS";
              break;
              case 'MH-SERVICES LINKED DETAILS':
              var sql  = "Select distinct [SERVICES LINKED DETAILS] from MENTALHEALTHDATASET where [SERVICES LINKED DETAILS] != null Or [SERVICES LINKED DETAILS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-SERVICES LINKED DETAILS";
              break;
              case 'MH-NDIS TYPE':
              var sql  = "Select distinct [NDIS TYPE] from MENTALHEALTHDATASET where [NDIS TYPE] != null Or [NDIS TYPE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-NDIS TYPE";
              break;
              case 'MH-NDIS TYPE COMMENTS':
              var sql  = "Select distinct [NDIS TYPE COMMENTS] from MENTALHEALTHDATASET where [NDIS TYPE COMMENTS] != null Or [NDIS TYPE COMMENTS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-NDIS TYPE COMMENTS";
              break;
              case 'MH-NDIS NUMBER':
              var sql  = "Select distinct [NDIS NUMBER] from MENTALHEALTHDATASET where [NDIS NUMBER] != null Or [NDIS NUMBER] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-NDIS NUMBER";
              break;
              case 'MH-REVIEW APPEAL':
              var sql  = "Select distinct [REVIEW APPEAL] from MENTALHEALTHDATASET where [REVIEW APPEAL] != null Or [REVIEW APPEAL] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-REVIEW APPEAL";
              break;
              case 'MH-REVIEW COMMENTS':
              var sql  = "Select distinct [REVIEW COMMENTS] from MENTALHEALTHDATASET where [REVIEW COMMENTS] != null Or [REVIEW COMMENTS] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-REVIEW COMMENTS";
              break;
              case 'MH-KP_Intake_1':
              var sql  = "Select distinct [KP_IN_1] from MENTALHEALTHDATASET where [KP_IN_1] != null Or [KP_IN_1] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_1";
              break;
              case 'MH-KP_Intake_2':
              var sql  = "Select distinct [KP_IN_2] from MENTALHEALTHDATASET where [KP_IN_2] != null Or [KP_IN_2] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_2";
              break;
              case 'MH-KP_Intake_3MH':
              var sql  = "Select distinct [KP_IN_3M] from MENTALHEALTHDATASET where [KP_IN_3M] != null Or [KP_IN_3M] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_3MH";
              break;                                   
              case 'MH-KP_Intake_3PH':
              var sql  = "Select distinct [KP_IN_3P] from MENTALHEALTHDATASET where [KP_IN_3P] != null Or [KP_IN_3P] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_3PH";
              break;
              case 'MH-KP_Intake_4':
              var sql  = "Select distinct [KP_IN_4] from MENTALHEALTHDATASET where [KP_IN_4] != null Or [KP_IN_4] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_4";
              break;
              case 'MH-KP_Intake_5':
              var sql  = "Select distinct [KP_IN_5] from MENTALHEALTHDATASET where [KP_IN_5] != null Or [KP_IN_5] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_5"; 
              break;
              case 'MH-KP_Intake_6':
              var sql  = "Select distinct [KP_IN_6] from MENTALHEALTHDATASET where [KP_IN_6] != null Or [KP_IN_6] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_6";
              break;
              case 'MH-KP_Intake_7':
              var sql  = "Select distinct [KP_IN_7] from MENTALHEALTHDATASET where [KP_IN_7] != null Or [KP_IN_7] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_7";
              break;
              case 'MH-KP_3Months_1':
              var sql  = "Select distinct [KP_3_1] from MENTALHEALTHDATASET where [KP_3_1] != null Or [KP_3_1] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_1";
              break;
              case 'MH-KP_3Months_2':
              var sql  = "Select distinct [KP_3_2] from MENTALHEALTHDATASET where [KP_3_2] != null Or [KP_3_2] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_2";
              break;
              case 'MH-KP_3Months_3MH':
              var sql  = "Select distinct [KP_3_3M] from MENTALHEALTHDATASET where [KP_3_3M] != null Or [KP_3_3M] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_3MH";
              break;
              case 'MH-KP_3Months_3PH':
              var sql  = "Select distinct [KP_3_3P] from MENTALHEALTHDATASET where [KP_3_3P] != null Or [KP_3_3P] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_3PH";
              break;
              case 'MH-KP_3Months_4':
              var sql  = "Select distinct [KP_3_4] from MENTALHEALTHDATASET where [KP_3_4] != null Or [KP_3_4] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_4";
              break;
              case 'MH-KP_3Months_5':
              var sql  = "Select distinct [KP_3_5] from MENTALHEALTHDATASET where [KP_3_5] != null Or [KP_3_5] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_5";
              break;
              case 'MH-KP_3Months_6':
              var sql  = "Select distinct [KP_3_6] from MENTALHEALTHDATASET where [KP_3_6] != null Or [KP_3_6] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_6";
              break;
              case 'MH-KP_3Months_7':
              var sql  = "Select distinct [KP_3_7] from MENTALHEALTHDATASET where [KP_3_7] != null Or [KP_3_7] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_7";
              break;
              case 'MH-KP_6Months_1':
              var sql  = "Select distinct [KP_6_1] from MENTALHEALTHDATASET where [KP_6_1] != null Or [KP_6_1] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_1";
              break; 
              case 'MH-KP_6Months_2':
              var sql  = "Select distinct [KP_6_2] from MENTALHEALTHDATASET where [KP_6_2] != null Or [KP_6_2] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_2";
              break;
              case 'MH-KP_6Months_3MH':
              var sql  = "Select distinct [KP_6_3M] from MENTALHEALTHDATASET where [KP_6_3M] != null Or [KP_6_3M] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_3MH";
              break;
              case 'MH-KP_6Months_3PH':
              var sql  = "Select distinct [KP_6_3P] from MENTALHEALTHDATASET where [KP_6_3P] != null Or [KP_6_3P] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_3PH";
              break;
              case 'MH-KP_6Months_4':
              var sql  = "Select distinct [KP_6_4] from MENTALHEALTHDATASET where [KP_6_4] != null Or [KP_6_4] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_4";
              break;
              case 'MH-KP_6Months_5':
              var sql  = "Select distinct [KP_6_5] from MENTALHEALTHDATASET where [KP_6_5] != null Or [KP_6_5] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_5";
              break;
              case 'MH-KP_6Months_6':
              var sql  = "Select distinct [KP_6_6] from MENTALHEALTHDATASET where [KP_6_6] != null Or [KP_6_6] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_6";
              break;
              case 'MH-KP_6Months_7':
              var sql  = "Select distinct [KP_6_7] from MENTALHEALTHDATASET where [KP_6_7] != null Or [KP_6_7] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_7";
              break;
              case 'MH-KP_9Months_1':
              var sql  = "Select distinct [KP_9_1] from MENTALHEALTHDATASET where [KP_9_1] != null Or [KP_9_1] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_1";
              break;
              case 'MH-KP_9Months_2':
              var sql  = "Select distinct [KP_9_2] from MENTALHEALTHDATASET where [KP_9_2] != null Or [KP_9_2] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_2";
              break;
              case 'MH-KP_9Months_3MH':
              var sql  = "Select distinct [KP_9_3M] from MENTALHEALTHDATASET where [KP_9_3M] != null Or [KP_9_3M] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_3MH";
              break;
              case 'MH-KP_9Months_3PH':
              var sql  = "Select distinct [KP_9_3P] from MENTALHEALTHDATASET where [KP_9_3P] != null Or [KP_9_3P] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_3PH";
              break;
              case 'MH-KP_9Months_4':
              var sql  = "Select distinct [KP_9_4] from MENTALHEALTHDATASET where [KP_9_4] != null Or [KP_9_4] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_4";
              break;
              case 'MH-KP_9Months_5':
              var sql  = "Select distinct [KP_9_5] from MENTALHEALTHDATASET where [KP_9_5] != null Or [KP_9_5] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_5";
              break;
              case 'MH-KP_9Months_6':
              var sql  = "Select distinct [KP_9_6] from MENTALHEALTHDATASET where [KP_9_6] != null Or [KP_9_6] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_6";
              break;
              case 'MH-KP_9Months_7':
              var sql  = "Select distinct [KP_9_7] from MENTALHEALTHDATASET where [KP_9_7]  != null Or [KP_9_7] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_7";
              break;
              case 'MH-KP_Exit_1':
              var sql  = "Select distinct [KP_EX_1] from MENTALHEALTHDATASET where [KP_EX_1] != null Or [KP_EX_1] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_1";
              break;
              case 'MH-KP_Exit_2':
              var sql  = "Select distinct [KP_EX_2] from MENTALHEALTHDATASET where [KP_EX_2]  != null Or [KP_EX_2] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_2";
              break;
              case 'MH-KP_Exit_3MH':
              var sql  = "Select distinct [KP_EX_3M] from MENTALHEALTHDATASET where [KP_EX_3M] != null Or [KP_EX_3M] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_3MH";
              break;
              case 'MH-KP_Exit_3PH':
              var sql  = "Select distinct [KP_EX_3P] from MENTALHEALTHDATASET where [KP_EX_3P] != null Or [KP_EX_3P] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_3PH";
              break;
              case 'MH-KP_Exit_4':
              var sql  = "Select distinct [KP_EX_4] from MENTALHEALTHDATASET where [KP_EX_4] != null Or [KP_EX_4] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_4";
              break;       
              case 'MH-KP_Exit_5':
              var sql  = "Select distinct [KP_EX_5] from MENTALHEALTHDATASET where [KP_EX_5] != null Or [KP_EX_5] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_5";
              break;
              case 'MH-KP_Exit_6':
              var sql  = "Select distinct [KP_EX_6] from MENTALHEALTHDATASET where [KP_EX_6] != null Or [KP_EX_6] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_6";
              break;
              case 'MH-KP_Exit_7':
              var sql  = "Select distinct [KP_EX_7] from MENTALHEALTHDATASET where [KP_EX_7] != null Or [KP_EX_7] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_7";
              break;
              case 'MH-KP_Intake_DATE':
              var sql  = "Select distinct format ([KP_IN_DATE],'dd/MM/yyyy') as [KP_IN_DATE]  from MENTALHEALTHDATASET where [KP_IN_DATE] != null Or [KP_IN_DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Intake_DATE";
              break;
              case 'MH-KP_3Months_DATE':
              var sql  = "Select distinct format ([KP_3_DATE],'dd/MM/yyyy') as [KP_3_DATE] from MENTALHEALTHDATASET where [KP_3_DATE] != null Or [KP_3_DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_3Months_DATE";
              break;
              case 'MH-KP_6Months_DATE':
              var sql  = "Select distinct format ([KP_6_DATE],'dd/MM/yyyy') as [KP_6_DATE] from MENTALHEALTHDATASET where [KP_6_DATE] != null Or [KP_6_DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_6Months_DATE"; 
              break;
              case 'MH-KP_9Months_DATE':
              var sql  = "Select distinct format ([KP_9_DATE],'dd/MM/yyyy') as [KP_9_DATE] from MENTALHEALTHDATASET where [KP_9_DATE] != null Or [KP_9_DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_9Months_DATE";
              break;
              case 'MH-KP_Exit_DATE':
              var sql  = "Select distinct format ([KP_EX_DATE],'dd/MM/yyyy') as [KP_EX_DATE] from MENTALHEALTHDATASET where [KP_EX_DATE] != null Or [KP_EX_DATE] != '' "
              this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
              this.ModalTitle = "MH-KP_Exit_DATE";
              break;
            //Recipient Placements                  
            case 'Placement Type':
            var sql  = "Select distinct [Type] from HumanResources where [Type] != null Or [Type] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Placement Type";
            break;
            case 'Placement Carer Name':
            var sql  = "Select distinct [Name] from HumanResources where [Name] != null Or [Name] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Placement Carer Name";
            break;
            case 'Placement Start':
            var sql  = "Select distinct format([Date1],'dd/MM/yyyy') as [Date1] from HumanResources where [Date1] != null Or [Date1] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Placement Start";
            break;
            case 'Placement End':
            var sql  = "Select distinct format([Date2],'dd/MM/yyyy') as [Date2] from HumanResources where [Date2] != null Or [Date2] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Placement End";
            break;
            case 'Placement Referral': 
            var sql  = "Select distinct [Recurring] from HumanResources where [Recurring] != null Or [Recurring] != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Placement Referral'";
            break;        
            case 'Placement ATC':
            var sql  = "Select distinct [Completed] from HumanResources where [Completed] != null Or [Completed] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Placement ATC";
            break;
            case 'Placement Notes':
            var sql  = "Select distinct [Notes] from HumanResources where [Notes] != null Or [Notes] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Placement Notes";
            break;
            //Quote Goals and stratagies                  
            case 'Quote Goal':
            this.ConditionEntity =  'GOALS.'
            var sql  = "Select distinct User1 from HumanResources where User1 and User1 != null Or User1 != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "GOALS.User1";
            break;
            case 'Goal Expected Completion Date':
            this.ConditionEntity =  'GOALS.'
            var sql  = "Select distinct format(Date1,'dd/MM/yyyy') as Date1 from HumanResources where Date1 != null Or Date1 != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "Goal Expected Completion Date";
            break;
            case 'Goal Last Review Date':
            var sql  = "Select distinct format(Date2,'dd/MM/yyyy') as Date2 from HumanResources where Date2 != null Or Date2 != '' "
            this.bodystyleModal_IN = { height:'400px' , overflow: 'auto' };
            this.ModalTitle = "Goal Last Review Date";
            break;
            case 'Goal Completed Date':
            var sql  = "Select distinct DateInstalled from HumanResources where DateInstalled != null Or DateInstalled != '' "
            this.bodystyleModal_IN = { height:'600px' , overflow: 'auto' };
            this.ModalTitle = "";
            break;                  
            case 'Goal  Achieved':
            var sql  = "Select distinct [State] from HumanResources where [State] != null Or [State] != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Goal  Achieved";
            break;
            case 'Quote Strategy':
            var sql  = "Select distinct Notes from HumanResources where  != null Or  != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Quote Strategy";
            break;
            case 'Strategy Expected Outcome':
            var sql  = "Select distinct Address1 from HumanResources where != null Or  != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Strategy Expected Outcome";
            break;
            case 'Strategy Contracted ID':
            var sql  = "Select distinct [State] from HumanResources where != null Or  != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Strategy Contracted ID";
            break;
            case 'Strategy DS Services':
            var sql  = "Select distinct User1 from HumanResources where != null Or  != '' "
            this.bodystyleModal_IN = { height:'300px' , overflow: 'auto' };
            this.ModalTitle = "Strategy DS Services";
            break;





              
              
              
            default:

            break;
          }
      }

      
        let temp_dataset = forkJoin([                       
          this.ReportS.GetReportlist(sql),             
          ]) 
          temp_dataset.subscribe(data => {  
                                            
              this.UserRpt_IN_list = data[0];          
          }); 
      if(this.UserRpt_IN_list.length >= 1){
        this.isVisibleprompt_IN = true;
      }
      // console.log(this.UserRpt_IN_list)
        /*
        let temp = forkJoin([
          //this.ReportS.GetReportNames('AGENCYSTFLIST') ,                        
          this.ListS.getcstdaoutlets()
        ]) 
        temp.subscribe(data => {       
          this.UserRpt_IN_list = data[0];
        });
        */
      }
      if(this.inputForm.value.criteriaArr == "BETWEEN")  {
        this.frm_SecondValue = true;
        this.frm_SecondVal = true;
      }else{
        this.frm_SecondValue = false;
        this.frm_SecondVal  = false;
      }          
    }

      customfunction(item){
        //console.log(item);                    
        //this.isVisibleprompt_IN = false;
        if(this.Entity_IN == null){
          //this.Entity_IN = [item]; 
          this.populatedINArr = [item];         
        }else{
          this.populatedINArr = [...this.populatedINArr,item];
        }     
        //console.log(this.populatedINArr);    
      }
        setvalue(){        
          this.value = [this.valueArr,...this.value]
        
          this.handleCancelTop();   
        } 
        applyIN(){                                  
          this.inputForm.patchValue({
            valueArr: this.populatedINArr,             
          });
          this.isVisibleprompt_IN = false;
          this.cd.detectChanges();          
          
        }
        apply(){
          this.frm_delete = true; 
          this.conditionentry = true;
          if(this.inputForm.value.criteriaArr == "IN" ){
 
            if (this.entity == null){
              this.entity  = [this.inputForm.value.exportitemsArr];
              this.condition  = [this.inputForm.value.criteriaArr];
              this.value  = [this.inputForm.value.valueArr];               
                        
            }           
            else{
              this.entity =[...this.entity, this.inputForm.value.exportitemsArr];
              this.condition = [...this.condition, this.inputForm.value.criteriaArr];
              this.value = [...this.value,this.inputForm.value.valueArr];                                                              
            }

            }else{
            if (this.entity == null){
              this.entity  = [this.inputForm.value.exportitemsArr];
              this.condition  = [this.inputForm.value.criteriaArr];
              this.value  = [this.inputForm.value.valueArr];
              
              if(this.inputForm.value.criteriaArr == "BETWEEN"){
                this.Endvalue  = [this.inputForm.value.SecondvalueArr]; 
                
              }
            }           
            else{
              this.entity =[...this.entity, this.inputForm.value.exportitemsArr];
              this.value = [...this.value, this.inputForm.value.valueArr];
              this.condition = [...this.condition, this.inputForm.value.criteriaArr];
              if(this.inputForm.value.criteriaArr == "BETWEEN"){
                this.Endvalue  = [...this.Endvalue, this.inputForm.value.SecondvalueArr];    
                
              }                                   
            }
          }                     
          if(this.RptFormat == "AGENCYSTFLIST" || this.RptFormat == "USERSTFLIST"){
            switch ((this.inputForm.value.exportitemsArr).toString()) {
              		//STAFF NAME AND ADDRESS      
                  case 'Title':
                    this.ConditionEntity = 'Staff.Title'
                  break;
                  case 'First Name':
                    this.ConditionEntity = 'Staff.FirstName'
                    break;
                    case 'Middle Name':
                       this.ConditionEntity = 'Staff.MiddleNames'        
                    break;
                    case 'Surname/Organisation':
                       this.ConditionEntity = 'Staff.LastName'        
                    break;
                    case 'Preferred Name':
                       this.ConditionEntity = 'Staff.PreferredName'      
                    break; 
                    case 'contact Address Line 1':
                     this.ConditionEntity = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>')"        
                    break;
                    case 'contact Address Line 2':
                    this.ConditionEntity = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') "                      
                    break;
                    case 'contact Address-Suburb':
                    this.ConditionEntity = "(SELECT TOP 1 suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') "                         
                    break;
                    case 'contact Address-Postcode':
                     this.ConditionEntity = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') "                          
                    break;
                    case 'contact Address-state':
                     this.ConditionEntity = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' and '2618'OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') "                       
                    break;
                    case 'contact Address-GoogleAddress':
                     this.ConditionEntity = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "                      
                    break;
                    case 'Usual Address Line 1 ':
                     this.ConditionEntity = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "                         
                    break;
                    case 'Usual Address Line 2':
                     this.ConditionEntity = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "                          
                    break;
                    case 'Usual Address-Suburb':
                     this.ConditionEntity = "(SELECT TOP 1suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "                       
                    break;
                    case 'Usual Address-Postcode  ':
                     this.ConditionEntity = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "     
                    break;
                    case 'Usual Address-state':
                     this.ConditionEntity = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>')"                         
                    break;
                    case 'Usual Address-GoogleAddress':
                     this.ConditionEntity = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "                         
                    break;    
                    case 'Billing Address Line 1':
                     this.ConditionEntity = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "                       
                    break;
                    case 'Billing Address Line 2':
                     this.ConditionEntity = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "                       
                    break;
                    case 'Billing Address-Suburb':
                     this.ConditionEntity = "(SELECT TOP 1 suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "                         
                    break;
                    case 'Billing Address-Postcode':
                     this.ConditionEntity = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "                        
                    break;
                    case 'Billing Address-state':
                     this.ConditionEntity = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "                        
                    break;
                    case 'Billing Address-GoogleAddress':
                    this.ConditionEntity = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "                         
                    break;
                    case 'Destination Address Line 1':
                     this.ConditionEntity = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "                         
                    break;
                    case 'Destination Address Line 2':
                     this.ConditionEntity = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "                         
                    break;
                    case 'Destination Address-Suburb ':
                     this.ConditionEntity = "(SELECT TOP 1 suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "                        
                    break;
                    case 'Destination Address-Postcode':
                     this.ConditionEntity = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "                          
                    break;
                    case 'Destination Address-state':
                     this.ConditionEntity = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "                          
                    break;
                    case 'Destination Address-GoogleAddress':
                     this.ConditionEntity = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "                         
                    break;
                    case 'Email':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<EMAIL>') "                     
                    break;      
                    case 'Email-SMS':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<EMAIL-SMS>') "                         
                    break;
                    case 'FAX':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<FAX>') "                          
                    break;
                    case 'Home Phone':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<HOME>') "
                    break;
                    case 'Mobile Phone':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<MOBILE>') "                       
                    break;
                    case 'Usual Phone':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<USUAL>') "                     
                    break;
                    case 'Work Phone':    
                       
                    break;
                    case 'Current Phone Number':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = 'CURRENT PHONE NUMBER' ) "                       
                    break;
                    case 'Other Phone Number':
                     this.ConditionEntity = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = 'OTHER PHONE NUMBER') "                       
                    break;                                    
                    //  Contacts & Next of Kin
                    case 'Contact Group':                 
                       this.ConditionEntity = 'HR.[Group]'
                    break;
                    case 'Contact Type':
                       this.ConditionEntity = 'HR.[Type]'
                    break;
                    case 'Contact Sub Type':                 
                       this.ConditionEntity = 'HR.[SubType]' 
                    break;
                    case 'Contact User Flag':
                       this.ConditionEntity = 'HR.[User1]'
                    break;                 
                    case 'Contact Person Type':
                       this.ConditionEntity = 'HR.[EquipmentCode]'
                    break;
                    case 'Contact Name':
                       this.ConditionEntity = 'HR.[Name]' 
                    break;
                    case 'Contact Address':
                       this.ConditionEntity = 'HR.[Address1]'
                    break;
                    case 'Contact Suburb':
                       this.ConditionEntity = 'HR.[Suburb]'
                    break;
                    case 'Contact Postcode':
                       this.ConditionEntity = 'HR.[Postcode]' 
                    break;
                    case 'Contact Phone 1':
                       this.ConditionEntity = 'HR.[Phone1]' 
                    break;
                    case 'Contact Phone 2':
                       this.ConditionEntity = 'HR.[Phone2]' 
                    break;
                    case 'Contact Mobile': 
                       this.ConditionEntity = 'HR.[Mobile]'
                    break;
                    case 'Contact FAX':
                       this.ConditionEntity = 'HR.[FAX]'
                    break;
                    case 'Contact Email':
                       this.ConditionEntity = 'HR.[Email]'
                    break;
                    // USER GROUPS                     
                    case 'Group Name':
                       this.ConditionEntity = 'UserGroup.[Name]'  
                    break;
                    case 'Group Note':
                       this.ConditionEntity = 'UserGroup.[Notes]' 
                    break; 
                    case 'Group Start Date':
                       this.ConditionEntity = 'UserGroup.[Date1]'  
                    break;                      
                    case 'Group End Date':
                       this.ConditionEntity = 'UserGroup.[Date2]' 
                    break;
                    case 'Group Email':
                       this.ConditionEntity = 'UserGroup.[Email]' 
                    break;                  
                    //Preferences                                        
                    case 'Preference Name':
                       this.ConditionEntity = 'Prefr.[Name]'  
                    break;                      
                    case 'Preference Note':
                       this.ConditionEntity = 'Prefr.[Notes]'  
                    break;
                    //REMINDERS                      
                    case 'Reminder Detail':
                       this.ConditionEntity = 'Remind.[Name]'
                    break;
                    case 'Event Date':
                       this.ConditionEntity = 'Remind.[Date2]'  
                    break;                  
                    case 'Reminder Date':
                       this.ConditionEntity = 'Remind.[Date1]'  
                    break;
                    case 'Reminder Notes':
                       this.ConditionEntity = 'Remind.[Notes]'  
                    break;
                    //Leaves
                    case 'Name':
                      this.ConditionEntity =  'HRLeave.[Name]' 
                      break;
                    case 'Approved Status':
                        this.ConditionEntity =  'HRLeave.[completed]'  
                        break; 
                    case 'Leave Reminder Date':
                        this.ConditionEntity =  'HRLeave.[DateInstalled]'  
                        break; 
                    case 'Leave Start Date':
                        this.ConditionEntity =  'HRLeave.[Date1]'  
                        break; 
                    case 'Leave End Date':
                      this.ConditionEntity =  'HRLeave.[Date2]'  
                      break; 
                    //Loan Items                      
                    case 'Loan Item Type':
                       this.ConditionEntity = 'HRLoan.[Type]'
                    break;
                    case 'Loan Item Description':
                       this.ConditionEntity = 'HRLoan.[Name]'
                    break;
                    case 'Loan Item Date Loaned/Installed':
                       this.ConditionEntity = 'HRLoan.[Date1]'
                    break;                      
                    case 'Loan Item Date Collected':
                       this.ConditionEntity = 'HRLoan.[Date2]'
                    break;
                    //  service information Fields                      
                    case 'Staff Code':
                       this.ConditionEntity = 'SvcDetail.[Carer Code]'
                    break;
                    case 'Service Date':
                       this.ConditionEntity = 'SvcDetail.Date'
                    break;
                    case 'Service Start Time':
                       this.ConditionEntity = 'SvcDetail.[Start Time]'
                    break;
                    case 'Service Code':
                       this.ConditionEntity = 'SvcDetail.[Service Type]'
                    break;                      
                    case 'Service Hours':
                       this.ConditionEntity = '((SvcDetail.[Duration]*5) / 60)'
                    break;
                    case 'Service Pay Rate':
                       this.ConditionEntity = 'SvcDetail.[Unit Pay Rate]'
                    break;
                    case 'Service Bill Rate':
                       this.ConditionEntity = 'SvcDetail.[Unit Bill Rate]'
                    break;
                    case 'Service Bill Qty':
                       this.ConditionEntity = 'SvcDetail.[BillQty]'
                    break;
                    case 'Service Location/Activity Group':
                       this.ConditionEntity = 'SvcDetail.[ServiceSetting]'
                    break;
                    case 'Service Program':
                       this.ConditionEntity = 'SvcDetail.[Program]'
                    break;
                    case 'Service Group':
                       this.ConditionEntity = 'SvcDetail.[Type]'
                    break;
                    case 'Service HACC Type': 
                       this.ConditionEntity = 'SvcDetail.[HACCType]'
                    break;
                    case 'Service Category':
                       this.ConditionEntity = 'SvcDetail.[Anal]'
                    break;
                    case 'Service Status':
                       this.ConditionEntity = 'SvcDetail.[Status]'
                    break;
                    case 'Service Pay Type':
                       this.ConditionEntity = 'SvcDetail.[Service Description]'
                    break;
                    case 'Service Pay Qty':
                       this.ConditionEntity = 'SvcDetail.[CostQty]'
                    break; /*
                    case 'Service End Time/ Shift End Time':
                     this.ConditionEntity = " Convert(varchar,DATEADD(minute,(SvcDetail.[Duration]*5) ,SvcDetail.[Start Time]),108) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([endtime+'  '])
                    }else{columnNames = ([endtime+'  '])}
                    break; */
                    case 'Service Funding Source':
                       this.ConditionEntity = 'Humanresourcetypes.[Type]'
                    break;  
                    case 'Service Notes':
                       this.ConditionEntity = 'History.Detail'
                    break;
                    //Staff Position
                    case 'Staff Position':
                    this.ConditionEntity =  'StaffPosition.[Name]' 
                    break;           
                    case 'Position Start Date':
                    this.ConditionEntity = 'StaffPosition.[Start Date]'
                    break;
                    case 'Position End Date':
                    this.ConditionEntity = 'StaffPosition.[End Date]'
                    break;
                    case 'Position ID':
                    this.ConditionEntity = 'StaffPosition.[Position ID]'
                    break;
                    case 'Position Notes':
                    this.ConditionEntity =  'StaffPosition.[Notes]'
                    break;
                    //Work Hours
                    case 'MIN_DAILY_HRS':
                      this.ConditionEntity =  ' HRS_DAILY_MIN AS MIN_DAILY_HRS'
                    break;
                    case 'MAX_DAILY_HRS':
                      this.ConditionEntity =  ' HRS_DAILY_MAX AS MAX_DAILY_HRS'
                    break;
                    case 'MIN_WEEKLY_HRS':
                      this.ConditionEntity =  ' HRS_WEEKLY_MIN AS MIN_WEEKLY_HRS'
                    break;
                    case 'MAX_WEEKLY_HRS':
                      this.ConditionEntity =  ' HRS_WEEKLY_MAX AS MAX_WEEKLY_HRS'
                    break;
                    case 'MIN_PAY_PERIOD_HRS':
                      this.ConditionEntity =  ' HRS_FNIGHTLY_MIN AS MIN_PAY_PERIOD_HRS'
                    break;
                    case 'MAX_PAY_PERIOD_HRS':
                      this.ConditionEntity =  ' HRS_FNIGHTLY_MAX AS MAX_PAY_PERIOD_HRS'
                    break;
                    case 'WEEK_1_DAY_1':
                      this.ConditionEntity =  ' CH_1_1 AS WEEK_1_DAY_1'
                    break;
                    case 'WEEK_1_DAY_2':
                      this.ConditionEntity =  ' CH_1_2 AS WEEK_1_DAY_2'
                    break;
                    case 'WEEK_1_DAY_3':
                      this.ConditionEntity =  ' CH_1_3 AS WEEK_1_DAY_3'
                    break;
                    case 'WEEK_1_DAY_4':
                      this.ConditionEntity =  ' CH_1_4 AS WEEK_1_DAY_4'
                    break;
                    case 'WEEK_1_DAY_5':
                      this.ConditionEntity =  ' CH_1_5 AS WEEK_1_DAY_5'
                    break;
                    case 'WEEK_1_DAY_6':
                    this.ConditionEntity =  ' CH_1_6 AS WEEK_1_DAY_6'
                    break;
                    case 'WEEK_1_DAY_7':
                      this.ConditionEntity =  ' CH_1_7 AS WEEK_1_DAY_7'
                    break;
                    case 'WEEK_2_DAY_1':
                      this.ConditionEntity =  ' CH_2_1 AS WEEK_2_DAY_1'
                    break;
                    case 'WEEK_2_DAY_2':
                      this.ConditionEntity =  ' CH_2_2 AS WEEK_2_DAY_2'
                    break;
                    case 'WEEK_2_DAY_3':
                    this.ConditionEntity =  ' CH_2_3 AS WEEK_2_DAY_3'
                    break;
                    case 'WEEK_2_DAY_4':
                      this.ConditionEntity =  ' CH_2_4 AS WEEK_2_DAY_4'
                    break;
                    case 'WEEK_2_DAY_5':
                      this.ConditionEntity =  ' CH_2_5 AS WEEK_2_DAY_5'
                    break;
                    case 'WEEK_2_DAY_6':
                      this.ConditionEntity =  ' CH_2_6 AS WEEK_2_DAY_6'
                    break;
                    case 'WEEK_2_DAY_7':
                      this.ConditionEntity =  ' CH_2_7 AS WEEK_2_DAY_7'
                    break;
                    //Staff Incident 
                    case 'INCD_Status' :
                        this.ConditionEntity =  ' StaffIncidents.Status'
                        break;
                    case 'INCD_Date' :
                        this.ConditionEntity =  ' StaffIncidents.Date'
                     break;
                    case 'INCD_Type' :
                        this.ConditionEntity =  ' StaffIncidents.[Type]'
                     break;
                    case 'INCD_Description' :
                        this.ConditionEntity =  ' StaffIncidents.ShortDesc'
                      break;
                   case 'INCD_SubCategory' :
                        this.ConditionEntity =  ' StaffIncidents.[PerpSpecify]'
                     break;
                   case 'INCD_Assigned_To' :
                        this.ConditionEntity =  ' StaffIncidents.[CurrentAssignee] '
                     break;
                   case 'INCD_Service' :
                        this.ConditionEntity =  ' StaffIncidents.[Service] '
                     break;
                   case 'INCD_Severity' :
                        this.ConditionEntity =  ' StaffIncidents.[Severity] '
                     break;
                   case 'INCD_Time' :
                        this.ConditionEntity =  ' StaffIncidents.[Time]'
                     break;
                   case 'INCD_Duration' :
                        this.ConditionEntity =  ' StaffIncidents.Duration'
                     break;
                   case 'INCD_Location' :
                        this.ConditionEntity =  ' StaffIncidents.[Location ]'
                     break;
                   case 'INCD_LocationNotes' :
                        this.ConditionEntity =  ' StaffIncidents.[LocationNotes ]'
                     break;
                   case 'INCD_ReportedBy' :
                        this.ConditionEntity =  ' StaffIncidents.[ReportedBy ]'
                     break;
                   case 'INCD_DateReported' :
                        this.ConditionEntity =  ' StaffIncidents.[DateReported ]'
                     break;
                   case 'INCD_Reported' :
                        this.ConditionEntity =  " CASE WHEN StaffIncidents.Reported = 1 THEN 'YES' ELSE 'NO' END AS [INCD_Reported ]"
                    case 'INCD_FullDesc' :
                        this.ConditionEntity =  ' StaffIncidents.[FullDesc]'
                     break;
                   case 'INCD_Program' :
                        this.ConditionEntity =  ' StaffIncidents.[Program ]'
                     break;
                   case 'INCD_DSCServiceType' :
                        this.ConditionEntity =  ' StaffIncidents.[DSCServiceType]'
                     break;
                   case 'INCD_TriggerShort' :
                        this.ConditionEntity =  ' StaffIncidents.[TriggerShort]'
                     break;
                   case 'INCD_incident_level' :
                        this.ConditionEntity =  ' StaffIncidents.[incident_level  ]'
                     break;
                   case 'INCD_Area' :
                        this.ConditionEntity =  ' StaffIncidents.[area]'
                     break;
                   case 'INCD_Region' :
                        this.ConditionEntity =  ' StaffIncidents.[Region]'
                     break;
                   case 'INCD_position' :
                        this.ConditionEntity =  ' StaffIncidents.[position]'
                     break;
                   case 'INCD_phone' :
                        this.ConditionEntity =  ' StaffIncidents.[phone]'
                     break;
                   case 'INCD_verbal_date' :
                        this.ConditionEntity =  ' StaffIncidents.[verbal_date]'
                     break;
                   case 'INCD_verbal_time' :
                        this.ConditionEntity =  ' StaffIncidents.[verbal_time]'
                     break;
                   case 'INCD_By_Whome' :
                        this.ConditionEntity =  ' StaffIncidents.[By_Whome]'
                     break;
                   case 'INCD_To_Whome' :
                        this.ConditionEntity =  ' StaffIncidents.[To_Whome]'
                     break;
                   case 'INCD_BriefSummary' :
                        this.ConditionEntity =  ' StaffIncidents.[BriefSummary]'
                     break;
                   case 'INCD_ReleventBackground' :
                        this.ConditionEntity =  ' StaffIncidents.[ReleventBackground]'
                     break;
                   case 'INCD_SummaryofAction' :
                        this.ConditionEntity =  ' StaffIncidents.[SummaryofAction]'
                     break;
                   case 'INCD_SummaryOfOtherAction' :
                        this.ConditionEntity =  ' StaffIncidents.[SummaryOfOtherAction]'
                     break;
                   case 'INCD_Triggers' :
                        this.ConditionEntity =  ' StaffIncidents.[Triggers]'
                     break;
                   case 'INCD_InitialAction' :
                        this.ConditionEntity =  ' StaffIncidents.[InitialAction ]'
                     break;
                   case 'INCD_InitialNotes' :
                        this.ConditionEntity =  ' StaffIncidents.[InitialNotes ]'
                     break;
                   case 'INCD_InitialFupBy' :
                        this.ConditionEntity =  ' StaffIncidents.[InitialFupBy ]'
                     break;
                   case 'INCD_Completed' :
                        this.ConditionEntity =  " CASE WHEN StaffIncidents.Completed = 1 THEN 'YES' ELSE 'NO' END AS [INCD_Completed ]"
                    case 'INCD_OngoingAction' :
                        this.ConditionEntity =  ' StaffIncidents.[OngoingAction]'
                     break;
                   case 'INCD_OngoingNotes' :
                        this.ConditionEntity =  ' StaffIncidents.[OngoingNotes]'
                     break;
                   case 'INCD_Background' :
                        this.ConditionEntity =  ' StaffIncidents.[Background]'
                     break;
                   case 'INCD_Abuse' :
                        this.ConditionEntity =  ' StaffIncidents.[Abuse ]'
                     break;
                   case 'INCD_DOPWithDisability' :
                        this.ConditionEntity =  " CASE WHEN StaffIncidents.  = 1 THEN 'YES' ELSE 'NO' END AS [INCD_DOPWithDisability ]"
                    case 'INCD_SeriousRisks' :
                        this.ConditionEntity =  ' StaffIncidents.[SeriousRisks]'
                     break;
                   case 'INCD_Complaints' :
                        this.ConditionEntity =  ' StaffIncidents.[Complaints]'
                     break;
                   case 'INCD_Perpetrator' :
                        this.ConditionEntity =  ' StaffIncidents.[Perpetrator]'
                     break;
                   case 'INCD_Notify' :
                        this.ConditionEntity =  ' StaffIncidents.[Notify ]'
                     break;
                   case 'INCD_NoNotifyReason' :
                        this.ConditionEntity =  ' StaffIncidents.[NoNotifyReason ]'
                     break;
                   case 'INCD_Notes' :
                        this.ConditionEntity =  ' StaffIncidents.[Notes ]'
                     break;
                   case 'INCD_Setting' :
                        this.ConditionEntity =  ' StaffIncidents.[Setting ]'
                        break; /*
                   case 'INCD_CreatedBy':
                      this.ConditionEntity =  'RecipientIncidents.[INCD_CreatedBy]'
                      break;
                   case 'INCD_Involved_Staff':
                      this.ConditionEntity =  'RecipientIncidents.[INCD_Involved_Staff]' 
                      break; */                   
                    //STAFF OP NOTES
                    case 'General Notes':
                      this.ConditionEntity =  ' CONVERT(TEXT, STAFF.[STF_NOTES]) as [General Notes],'
                     break;
                   case 'OP Notes Date':
                      this.ConditionEntity =  ' OPHistory.[DetailDate] as [OP Notes Date],'
                     break;
                   case 'OP Notes Detail':
                      this.ConditionEntity =  " dbo.RTF2TEXT(OPHistory.[Detail]) as [OP Notes Detail],"
                    case 'OP Notes Creator':
                      this.ConditionEntity =  ' OPHistory.[Creator] as [OP Notes Creator],'
                     break;
                   case 'OP Notes Alarm':
                      this.ConditionEntity =  ' OPHistory.[AlarmDate] as [OP Notes Alarm],'
                     break;
                   case 'OP Notes Category':
                      this.ConditionEntity =  " CASE WHEN IsNull(OPHistory.ExtraDetail2,'') < 'A' THEN 'UNKNOWN' ELSE OPHistory.ExtraDetail2 END AS [OP Notes Category],"                
                    //STAFF HR NOTES
                    case 'HR Notes Date':
                      this.ConditionEntity =  ' HRHistory.[DetailDate] as [HR Notes Date],'
                     break;
                   case 'HR Notes Detail':
                      this.ConditionEntity =  ' dbo.RTF2TEXT(HRHistory.[Detail]) as [HR Notes Detail],'
                     break;
                   case 'HR Notes Creator':
                      this.ConditionEntity =  ' HRHistory.[Creator] as [HR Notes Creator],'
                     break;
                   case 'HR Notes Alarm':
                      this.ConditionEntity =  ' HRHistory.[AlarmDate] as [HR Notes Alarm],'
                     break;
                   case 'HR Notes Categories':
                      this.ConditionEntity =  ' HRHistory.[ExtraDetail2] as [HR Notes Categories],'
                     //Doing
                     //Staff Attribute
                     case 'Competency': 
                        this.ConditionEntity =  ' HumanResources.[Name] AS [Competency],'
		                  break;
                      case 'Competency Expiry Date': 
                          this.ConditionEntity =  ' HumanResources.[Date1] as [Competency Expiry Date],'
                          break;
                      case 'Competency Reminder Date':
                          this.ConditionEntity =  ' HumanResources.[Date2] AS [Competency Reminder Date],'
                          break;
                      case 'Competency Completion Date':
                            this.ConditionEntity =  ' HumanResources.[DateInstalled] AS [Competency Completion Date],'
                          break;
                      case 'Certificate Number':
                        this.ConditionEntity =  ' HumanResources.[Address1] AS [Certificate Number],'
                          break;
                      case 'Mandatory Status':                                      
                      this.ConditionEntity =  ' HumanResources.[Recurring] AS [Mandatory Status],'
                          break;
                      case 'Competency Notes':
                        this.ConditionEntity =  ' HumanResources.[Notes] AS [Competency Notes],'
                          break;
                      case 'Staff Position':
                        this.ConditionEntity =  " Select [Name] from HumanResources where [Group] = 'STAFFPOSITION' ,"
                          break;
                      case 'Staff Admin Categories':
                        this.ConditionEntity =  ' Staff.[SubCategory] as [Staff Admin Categories],'
                          break;
                      case 'NDIA Staff Level':
                        this.ConditionEntity =  ' Staff.[NDIAStaffLevel] as [NDIA Staff Level],'
                          break;








                    default:

                    break;
                    
            }
            

          }else{
            switch ((this.inputForm.value.exportitemsArr).toString()) {
              //NAME AND ADDRESS
              case 'First Name':
              this.ConditionEntity = 'R.FirstName'
              break;
              case 'Title':
              this.ConditionEntity ='R.title'       
              break;
              case 'Surname/Organisation':
              this.ConditionEntity ='R.[Surname/Organisation]'        
              break;
              case 'Preferred Name':
              this.ConditionEntity ='R.PreferredName'
              break;                  
              case 'Other':   
              this.ConditionEntity ='R.AliAs'                               
              break;
              case 'Address-Line1':
              this.ConditionEntity ='R.Address1'        
              break;
              case 'Address-Line2':
              this.ConditionEntity ='R.Address2'     
              break;
              case 'Address-Suburb':
              this.ConditionEntity ='R.Suburb'     
              break;
              case 'Address-Postcode':
              this.ConditionEntity ='R.Postcode'        
              break;
              case 'Address-State':
              this.ConditionEntity ='R.State'        
              break;
              //General Demographics             
              case 'Full Name-Surname First':
              this.ConditionEntity = '(R.[Surname/Organisation]' + ' + ' +' FirstName)'
              break;
              case 'Full Name-Mailing':
              this.ConditionEntity = '(R.[Surname/Organisation] ' + ' + ' +' FirstName)'  
              break;
              
              case 'Gender':
              this.ConditionEntity = 'R.Gender'
              break;
              case 'Date Of Birth':
              this.ConditionEntity = 'R.DateOfBirth'
              break;            
              case 'Age':
              this.ConditionEntity = ' DateDiff(YEAR,R.Dateofbirth,GetDate())'
              break;
              case 'Ageband-Statistical':
              this.ConditionEntity = "DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() )"
              break;
              case 'Ageband-5 Year':
              this.ConditionEntity = "DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() )"
              break;
              case 'Ageband-10 Year':
              this.ConditionEntity = "DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() )"
              break;
              case 'Age-ATSI Status':
              //  this.ConditionEntity =''
              break;
              case 'Month Of Birth':
              this.ConditionEntity =   'DateName(Month, DateOfBirth)'
              break;
              case 'Day Of Birth No.':
              this.ConditionEntity =   'DateName(Weekday, DateOfBirth)'
              break;
              case 'CALD Score':
              this.ConditionEntity =    'DataDomains.CALDStatus'
              break;                     
              case 'Country Of Birth':
              this.ConditionEntity =  'R.CountryOfBirth'
              break;
              case 'Language':
              this.ConditionEntity = 'R.HomeLanguage'
              break;              
              case 'Indigenous Status':
              this.ConditionEntity = 'R.IndiginousStatus'
              break;
              case 'Primary Disability':
              this.ConditionEntity = 'R.PermDisability'
              break;
              case 'Financially Dependent':
              this.ConditionEntity = 'R.[FDP]'
              break;
              case 'Financial Status':
              this.ConditionEntity = 'R.FinancialStatus'
              break;          
              case 'Occupation':
              this.ConditionEntity = 'R.Occupation'
              break;
              //Admin Info
              case 'UniqueID':
              this.ConditionEntity = 'R.UniqueID'
              break;
              case 'Code':
              this.ConditionEntity = 'R.[AccountNo]'
              break;
              case 'Type':
              this.ConditionEntity ='R.[Type]'
              break;             
              case 'Category':
              this.ConditionEntity ='R.[AgencyDefinedGroup]'
              break;
              case 'CoOrdinator':
              this.ConditionEntity = 'R.RECIPIENT_CoOrdinator'
              break;
              case 'Admitting Branch':
              this.ConditionEntity = 'R.BRANCH'
              break;
              case 'Secondary Branch':
                this.ConditionEntity ='HR.name'
              break;  
              case 'File number':
                this.ConditionEntity ='R.[AgencyIDReportingCode]'
              break;
              case 'File Number 2':
                this.ConditionEntity ='R.[URNumber]'
              break;                     
              case 'NDIA/MAC Number':
              this.ConditionEntity = 'R.NDISNumber'
              break;     
              case 'Last Activated Date':
                this.ConditionEntity ='R.ADMISSIONDATE'
              break;
              case 'Created By':
              this.ConditionEntity = 'R.CreatedBy'
              break;
              case 'Other':
              //  this.ConditionEntity =''
              break; 
              //Staff       
              case 'Staff Name':
                this.ConditionEntity =  "S.FIRSTNAME + ' ' + S.LASTNAME"
              break;
              case 'Program Name':
                this.ConditionEntity = 'HRCaseStaff.[Address1]' 
              break;
              case 'Notes':
              this.ConditionEntity =  'R.Notes'
              break;
              //Other Genral info
              case 'OH&S Profile':
              this.ConditionEntity =  'R.OHSProfile'
              break;
              case 'OLD WH&S Date':
              this.ConditionEntity =  'R.[WH&S]'
              break;
              case 'Billing Profile':
              this.ConditionEntity =  'R.BillProfile'
              break;                       
              case 'Sub Category':
              this.ConditionEntity =  'R.[UBDMap]'
              break;
              case 'Roster Alerts':
              this.ConditionEntity =  'R.[Notes]'
              break;
              case 'Timesheet Alerts':
              this.ConditionEntity =  'R.[SpecialConsiderations]'
              break;                 
              case 'Contact Issues':
              this.ConditionEntity =  'R.ContactIssues'
              break;
              case 'Survey Consent Given':
              this.ConditionEntity =  'R.SurveyConsent'
              break;
              case 'Copy Rosters Enabled':
              this.ConditionEntity =  'R.Autocopy'
              break;           
              case 'Activation Date':
              this.ConditionEntity =  'R.[AdmissionDate]'
              break;
              case 'DeActivation Date':
              this.ConditionEntity =  'R.[DischargeDate]'
              break;
              case 'Mobility':
              this.ConditionEntity =  'R.Mobility'
              break;
              case 'Specific Competencies':
              this.ConditionEntity =  'R.SpecialConsiderations'
              break;                           
              //  Contacts & Next of Kin
              case 'Contact Group':
              this.ConditionEntity =  'HR.[Group]'
              break;
              case 'Contact Type':
              this.ConditionEntity =  'HR.[Type]'
              break;
              case 'Contact Sub Type':
              this.ConditionEntity = 'HR.[SubType]' 
              break;
              case 'Contact User Flag':
              this.ConditionEntity =  'HR.[User1]'
              break;
              case 'Contact Person Type':
              this.ConditionEntity =  'HR.[EquipmentCode]'
              break;
              case 'Contact Name':
              this.ConditionEntity =   'HR.[Name]'
              break;
              case 'Contact Address':
              this.ConditionEntity =  'HR.[Address1]'
              break;
              case 'Contact Suburb':
              this.ConditionEntity = 'HR.[Suburb]' 
              break;                
              case 'Contact Postcode':
              this.ConditionEntity = 'HR.[Postcode]' 
              break;
              case 'Contact Phone 1':
              this.ConditionEntity =   'HR.[Phone1]'
              break;
              case 'Contact Phone 2':
              this.ConditionEntity =  'HR.[Phone2]'
              break;
              case 'Contact Mobile':
              this.ConditionEntity =  'HR.[Mobile]'
              break;
              case 'Contact FAX':
              this.ConditionEntity =  'HR.[FAX]'
              break;
              case 'Contact Email':
              this.ConditionEntity =  'HR.[Email]'
              break;
              //Carer Info                
              case 'Carer First Name':
              this.ConditionEntity = 'C.FirstName' 
              break;
              case 'Carer Last Name':
              this.ConditionEntity =  'C.[Surname/Organisation]'
              break;
              case 'Carer Age':
              this.ConditionEntity =  'DateDiff(YEAR,C.Dateofbirth,GetDate())'
              break;                 
              case 'Carer Gender':
              this.ConditionEntity =  'C.[Gender]'
              break;
              case 'Carer Indigenous Status':
              this.ConditionEntity =  'C.[IndiginousStatus]'
              break;                       
              case 'Carer Address':
              this.ConditionEntity =  "N.Address1 + ' ' +  N.Suburb + ' ' + N.Postcode"
              break;
              case 'Carer Email':
              this.ConditionEntity =  'PE.Detail'
              break;
              case 'Carer Phone <Home>':
              this.ConditionEntity =  'PhHome.Detail'
              break;
              case 'Carer Phone <Work>':
              this.ConditionEntity =  'PhWork.Detail'
              break;
              case 'Carer Phone <Mobile>':
              this.ConditionEntity =  'PhMobile.Detail'
              break;
              // Documents                
              case 'DOC_ID':
              this.ConditionEntity =  'doc.DOC_ID'
              break;
              case 'Doc_Title':
              this.ConditionEntity =  'doc.Title'
              break;
              case 'Created':
              this.ConditionEntity =  'doc.Created'
              break;                
              case 'Modified':
              this.ConditionEntity =  'doc.Modified'
              break;
              case 'Status':
              this.ConditionEntity =  'doc.Status'
              break;                                
              case 'Classification':
              this.ConditionEntity =  'doc.Classification'
              break;
              case 'Category':
              this.ConditionEntity =  'doc.Category'
              break;
              case 'Filename':
              this.ConditionEntity =  'doc.Filename'
              break;
              case 'Doc#':
              this.ConditionEntity =  'doc.Doc#'
              break;
              case 'DocStartDate':
              this.ConditionEntity =  'doc.DocStartDate'
              break;
              case 'DocEndDate':
              this.ConditionEntity =  'doc.DocEndDate'
              break;
              case 'AlarmDate':
              this.ConditionEntity =   'doc.AlarmDate'
              break;                          
              case 'AlarmText':
              this.ConditionEntity =  'doc.AlarmText'
              break;
              //Consents                  
              case 'Consent':
              this.ConditionEntity =  'Cons.[Name]'
              break;
              case 'Consent Start Date':
              this.ConditionEntity =  'Cons.[Date1]'
              break;                  
              case 'Consent Expiry':
              this.ConditionEntity =  'Cons.[Date2]'
              break;
              case 'Consent Notes':
              this.ConditionEntity = 'Cons.[Notes]' 
              break;
              //  GOALS OF CARE                  
              case 'Goal':
              this.ConditionEntity =  'Goalcare.[User1]'
              break;               
              case 'Goal Detail':
              this.ConditionEntity =  'Goalcare.[Notes]'
              break;
              case 'Goal Achieved':
              this.ConditionEntity =  'Goalcare.[Completed]'
              break;
              case 'Anticipated Achievement Date':
              this.ConditionEntity =  'Goalcare.[Date1]'
              break;
              case 'Date Achieved':
              this.ConditionEntity =  'Goalcare.[DateInstalled]'
              break;                  
              case 'Last Reviewed':
              this.ConditionEntity =  'Goalcare.[Date2]'
              break;          
              case 'Logged By':
              this.ConditionEntity =  'Goalcare.[Creator]'
              break;
              //REMINDERS                  
              case 'Reminder Detail':
              this.ConditionEntity =  'Remind.[Name]'
              break;
              case 'Event Date':
              this.ConditionEntity =  'Remind.[Date2]'
              break;                                    
              case 'Reminder Date':
              this.ConditionEntity =  'Remind.[Date1]'
              break;
              case 'Reminder Notes':
              this.ConditionEntity =  'Remind.[Notes]'
              break;
              // USER GROUPS                  
              case 'Group Name':
              this.ConditionEntity =  'UserGroup.[Name]'
              break;
              case 'Group Note':
              this.ConditionEntity = 'UserGroup.[Notes]' 
              break;
              case 'Group Start Date':
              this.ConditionEntity =  'UserGroup.[Date1]'
              break;                                 
              case 'Group End Date':
              this.ConditionEntity = 'UserGroup.[Date2]'
              break;
              case 'Group Email':
              this.ConditionEntity = 'UserGroup.[Email]'
              break; 
              //Preferences 
              
              case 'Preference Name':
              this.ConditionEntity = 'Prefr.[Name]' 
              break;
              case 'Preference Note':
              this.ConditionEntity =  'Prefr.[Notes]'
              break;
              // FIXED REVIEW DATES                   
              case 'Review Date 1':
              this.ConditionEntity =  'R.[OpReASsessmentDate]'
              break;
              case 'Review Date 2':
              this.ConditionEntity =  'R.[ClinicalReASsessmentDate]'
              break;
              case 'Review Date 3':
              this.ConditionEntity =  'R.[FileReviewDate]'
              break;           
              //Staffing Inclusions/Exclusions                  
              case 'Excluded Staff':
              this.ConditionEntity =  'ExcludeS.[Name]'
              break;
              case 'Excluded_Staff Notes':
              this.ConditionEntity =  'ExcludeS.[Notes]'
              break;                                    
              case 'Included Staff':
              this.ConditionEntity =  'IncludS.[Name]'
              break;
              case 'Included_Staff Notes':
              this.ConditionEntity =  'IncludS.[Notes]'
              break;                 
              // AGREED FUNDING INFORMATION                         
              case 'Funding Source':
              this.ConditionEntity = 'HumanResourceTypes.[Type]' 
              break;
              case 'Funded Program':
              this.ConditionEntity =  'RecipientPrograms.[Program]'
              break;
              case 'Funded Program Agency ID':
                  this.ConditionEntity =  'HumanResourceTypes.[Address1]'
              break;                  
              case 'Program Status':
              this.ConditionEntity =  'RecipientPrograms.[ProgramStatus]'
              break;
              case 'Program Coordinator':
              this.ConditionEntity =  'HumanResourceTypes.[Address2]'
              break;
              case 'Funding Start Date':
              this.ConditionEntity =  'RecipientPrograms.[StartDate]'
              break;
              case 'Funding End Date':
              this.ConditionEntity = 'RecipientPrograms.[ExpiryDate]' 
              break;
              case 'AutoRenew':
              this.ConditionEntity =  'RecipientPrograms.[AutoRenew]'
              break;                  
              case 'Rollover Remainder':
              this.ConditionEntity =  'RecipientPrograms.[RolloverRemainder]'
              break;
              case 'Funded Qty':
              this.ConditionEntity =  'RecipientPrograms.[Quantity]'
              break;
              case 'Funded Type':
              this.ConditionEntity =  'RecipientPrograms.[ItemUnit]'
              break;
              case 'Funding Cycle':
              //  this.ConditionEntity =  
              break;
              case 'Funded Total Allocation':
              this.ConditionEntity =  'RecipientPrograms.[TotalAllocation]'
              break;
              case 'Used':
              this.ConditionEntity =  'RecipientPrograms.[Used]'
              break;
              case 'Remaining':
              this.ConditionEntity =  '(RecipientPrograms.[TotalAllocation] - RecipientPrograms.[Used])'
              break;
              //LEGACY CARE PLAN                  
              case 'Name':
              this.ConditionEntity =   'CarePlanItem.[PlanName]'
              break;                                    
              case 'Start Date':
              this.ConditionEntity =  'CarePlanItem.[PlanStartDate]'
              break;
              case 'End Date':
              this.ConditionEntity =  'CarePlanItem.[PlanEndDate]'
              break;                  
              case 'Details':
              this.ConditionEntity =  'CarePlanItem.[PlanDetail]'
              break;
              case 'Reminder Date':
              this.ConditionEntity =  'CarePlanItem.[PlanReminderDate]'
              break;
              case 'Reminder Text':
              this.ConditionEntity =  'CarePlanItem.[PlanReminderText]'
              break;   
              //Agreed Service Information                   
              case 'Agreed Service Code':
              this.ConditionEntity =  'ServiceOverview.[Service Type]'
              break;
              case 'Agreed Program':
              this.ConditionEntity =  'ServiceOverview.[ServiceProgram]'
              break;
              case 'Agreed Service Billing Rate':
              this.ConditionEntity =  'ServiceOverview.[Unit Bill Rate]'
              break;
              case 'Agreed Service Status':
              this.ConditionEntity =  'ServiceOverview.[ServiceStatus]'
              break;
              case 'Agreed Service Duration':
              this.ConditionEntity =  'ServiceOverview.[Duration]'
              break;                 
              case 'Agreed Service Frequency':
              this.ConditionEntity =  'ServiceOverview.[Frequency]'
              break;
              case 'Agreed Service Cost Type':
              this.ConditionEntity =  'ServiceOverview.[Cost Type]'
              break;
              case 'Agreed Service Unit Cost':
              this.ConditionEntity =  'ServiceOverview.[Unit Pay Rate]'
              break;
              case 'Agreed Service Billing Unit':
              this.ConditionEntity = 'ServiceOverview.[UnitType]' 
              break;
              case 'Agreed Service Debtor':
              this.ConditionEntity =  'ServiceOverview.[ServiceBiller]'
              break;                  
              case 'Agreed Service Agency ID':
              this.ConditionEntity =  'HRAgreedServices.[Address1]'
              break;
              //  CLINICAL INFORMATION                  
              case 'Nursing Diagnosis':
              this.ConditionEntity =  'NDiagnosis.[Description]'
              break;
              case 'Medical Diagnosis':
              this.ConditionEntity =  'MDiagnosis.[Description]'
              break;
              case 'Medical Procedure':
              this.ConditionEntity =  'MProcedures.[Description]'
              break;
              //Billing information
              case 'Billing Client':
              this.ConditionEntity =  'R.[BillingCycle]'
              break;
              case 'Billing Cycle':
              this.ConditionEntity =  'R.[BillingCycle]'
              break;
              case 'Billing Rate':
              this.ConditionEntity =  'R.[BillingMethod]'
              break;
              case 'Billing %':
              this.ConditionEntity =  'R.[PercentageRate]'
              break;            
              case 'Billing Amount':
              this.ConditionEntity =  'R.[DonationAmount]'
              break;
              case 'Account Identifier':
              this.ConditionEntity =  'R.[AccountingIdentifier]'
              break;
              case 'External Order Number':
              this.ConditionEntity =  'R.[Order#]'
              break;
              case 'Financially Dependent':
              this.ConditionEntity =  'R.[FDP]'
              break;
              case 'BPay Reference':
              this.ConditionEntity =  'R.[BPayRef]'
              break;
              case 'NDIA/MAC ID':
              this.ConditionEntity =  'R.[NDISNumber]'
              break;
              case 'Bill Profile':
              this.ConditionEntity =  'R.[BillProfile]'
              break;
              case 'Allow Different Biller':
              this.ConditionEntity =  'R.[RECIPIENT_SPLIT_BILL]'
              break;
              case 'Capped Bill':
              this.ConditionEntity =  'R.[CappedBill]'
              break;
              case 'Hide Fare on Transport Runsheet':
              this.ConditionEntity =  'R.[HideTransportFare]'
              break;
              case 'Print Invoice':
              this.ConditionEntity =  'R.[PrintInvoice]'
              break;
              case 'Email Invoice':
              this.ConditionEntity =  'R.[EmailInvoice]'
              break;
              case 'Direct Debit':
              this.ConditionEntity =  'R.[DirectDebit]'
              break;
              case 'DVA CoBiller':
              this.ConditionEntity =  'R.[DVACoBiller]'
              break;
              //PANZTEL Timezone 
              case 'PANZTEL Timezone':
              this.ConditionEntity =  'R.[RECIPIENT_TIMEZONE]'
              break;               
              case 'PANZTEL PBX Site':
              this.ConditionEntity =  'R.[RECIPIENT_PBX]'
              break;
              case 'PANZTEL Parent Site':
              this.ConditionEntity =  'R.[RECIPIENT_PARENT_SITE]'
              break;                  
              case 'DAELIBS Logger ID':
              this.ConditionEntity =  'R.[DAELIBSID]'
              break;
              //NOTES
              case 'General Notes':
                  this.ConditionEntity =  'CONVERT(TEXT, R.[Recipient_Notes]'
                  break;
                  case 'Case Notes Date':
                  this.ConditionEntity =  'History.[DetailDate]'
                  break;
                  case 'Case Notes Detail':
                  this.ConditionEntity =  'dbo.RTF2TEXT(History.[Detail])'
                  break;
                  case 'Case Notes Creator':
                  this.ConditionEntity =  'History.[Creator]'
                  break;
                  case 'Case Notes Alarm':
                  this.ConditionEntity =  'History.[AlarmDate]'
                  break;
                  case 'Case Notes Program':
                  this.ConditionEntity =  'History.Program'
                  break;
                  case 'Case Notes Category':
                  this.ConditionEntity =  'History.ExtraDetail2'
                break;
              //INSURANCE AND PENSION                  
              case 'Medicare Number':
              this.ConditionEntity =  'R.[MedicareNumber]'
              break;
              case 'Medicare Recipient ID':
              this.ConditionEntity =  'R.[MedicareRecipientID]'
              break;
              case 'Pension Status':
              this.ConditionEntity =  'R.[PensionStatus]'
              break;
              case 'Unable to Determine Pension Status':
              this.ConditionEntity =  'R.[PensionVoracity]'
              break;
              case 'Concession Number':
              this.ConditionEntity =  'R.[ConcessionNumber]'
              break;
              case 'DVA Benefits Flag':
              this.ConditionEntity =  'R.[DVABenefits]'
              break;                  
              case 'DVA Number':
              this.ConditionEntity =  'R.[DVANumber]'
              break;
              case 'DVA Card Holder Status':
              this.ConditionEntity =  'R.[RECIPT_DVA_Card_Holder_Status]'
              break;
              case 'Ambulance Subscriber':
              this.ConditionEntity =  'R.[Ambulance]'
              break;
              case 'Ambulance Type':
              this.ConditionEntity =  'R.[AmbulanceType]'
              break;
              case 'Pension Name':
              this.ConditionEntity =  'RecipientPensions.[Pension Name]'
              break;
              case 'Pension Number':
              this.ConditionEntity =  'RecipientPensions.[Pension Number]'
              break;
              case 'Will Available':
              this.ConditionEntity =  'R.[WillAvailable]'
              break;         
              case 'Will Location':
              this.ConditionEntity =  'R.[WhereWillHeld]'
              break;
              case 'Funeral Arrangements':
              this.ConditionEntity =  'R.[FuneralArrangements]'
              break;
              case 'Date Of Death':
              this.ConditionEntity =  'R.[DateOfDeath]'
              break;
              //HACCS DATSET FIELDS                  
              case 'HACC-SLK':
              this.ConditionEntity =  'RHACC.[HACC-SLK]'
              break;
              case 'HACC-First Name':
              this.ConditionEntity =  'R.[FirstName]'
              break;
              case 'HACC-Surname':
              this.ConditionEntity =  'R.[Surname/Organisation]'
              break;
              case 'HACC-Referral Source':
              this.ConditionEntity =  'R.[ReferralSource]'
              break;
              case 'HACC-Date Of Birth':
              this.ConditionEntity =  'R.[DateOfBirth]'
              break;
              case 'HACC-Date Of Birth Estimated':
              this.ConditionEntity =  'R.[CSTDA_BDEstimate]'
              break;                
              case 'HACC-Gender':
              this.ConditionEntity =  'R.[Gender]'
              break;
              case 'HACC-Area Of Residence':
              this.ConditionEntity =  'R.[Suburb]'
              break;
              case 'HACC-Country Of Birth':
              this.ConditionEntity = 'R.[CountryOfBirth]' 
              break;
              case 'HACC-Preferred Language,':
              this.ConditionEntity =  'R.[HomeLanguage]'
              break;
              case 'HACC-Indigenous Status':
              this.ConditionEntity =  'R.[IndiginousStatus]'
              break;
              case 'HACC-Living Arrangements':
              this.ConditionEntity =  'R.[LivingArrangements]'
              break;
              case 'HACC-Dwelling/Accomodation':
              this.ConditionEntity =  'R.[DwellingAccomodation]'
              break;
              case 'HACC-Main Reasons For Cessation':
              this.ConditionEntity =  'DATADOMAINS.[DESCRIPTION]'
              break;
              case 'HACC-Pension Status':
              this.ConditionEntity =  'R.[PensionStatus]'
              break;              
              case 'HACC-Primary Carer':
              this.ConditionEntity =  'R.[DatasetCarer]'
              break;
              case 'HACC-Carer Availability':
              this.ConditionEntity =  'R.[CarerAvailability]'
              break;
              case 'HACC-Carer Residency':
              this.ConditionEntity =  'R.[CarerResidency]'
              break;
              case 'HACC-Carer Relationship':
              this.ConditionEntity =  'R.[CarerRelationship]'
              break;
              case 'HACC-Exclude From Collection':
              this.ConditionEntity =  'R.[ExcludeFromStats]'
              break;                                
              case 'HACC-Housework':
              this.ConditionEntity =  'R.[ONIFProfile1]'
              break;
              case 'HACC-Transport':
              this.ConditionEntity =  'R.[ONIFProfile2]'
              break;
              case 'HACC-Shopping':
              this.ConditionEntity =  'R.[ONIFProfile3]'
              break;
              case 'HACC-Medication':
              this.ConditionEntity =  'R.[ONIFProfile4]'
              break;
              case 'HACC-Money':
              this.ConditionEntity =  'R.[ONIFProfile5]'
              break;
              case 'HACC-Walking':
              this.ConditionEntity =  'R.[ONIFProfile6]'
              break;
              case 'HACC-Bathing':
              this.ConditionEntity =  'R.[ONIFProfile7]'
              break;
              case 'HACC-Memory':
              this.ConditionEntity =  'R.[ONIFProfile8]'
              break;
              case 'HACC-Behaviour':
              this.ConditionEntity =  'R.[ONIFProfile9]'
              break;
              case 'HACC-Communication':
              this.ConditionEntity =  'ONI.[FPA_Communication]'
              break;
              case 'HACC-Eating':
              this.ConditionEntity =  'ONI.[FPA_Eating]'
              break;
              case 'HACC-Toileting':
              this.ConditionEntity =  'ONI.[FPA_Toileting]'
              break;
              case 'HACC-GetUp':
              this.ConditionEntity =  'ONI.[FPA_GetUp]'
              break;        
              case 'HACC-Carer More Than One':
              this.ConditionEntity =  'R.[CARER_MORE_THAN_ONE]'
              break;
              //"DEX"                  
              case 'DEX-Exclude From MDS':
              this.ConditionEntity =  'R.ExcludeFromStats'
              break;
              case 'DEX-Referral Purpose':
              this.ConditionEntity =  'DSS.ReferralPurpose'
              break;
              case 'DEX-Referral Source':
              this.ConditionEntity =  'DSS.[DEXREFERRALSOURCE]'
              break;
              case 'DEX-Referral Type':
              this.ConditionEntity =  'DSS.ReferralType'
              break;
              case 'DEX-Reason For Assistance':
              this.ConditionEntity =  'DSS.AssistanceReason'
              break;
              case 'DEX-Consent To Provide Information':
              this.ConditionEntity =  'DSS.[DEX-Consent To Provide Information]'
              break;
              case 'DEX-Consent For Future Contact':
              this.ConditionEntity =  'DSS.[DEX-Consent For Future Contact]'
              break;          
              case 'DEX-Sex':
              this.ConditionEntity =  'R.Gender'
              break;
              case 'DEX-Date Of Birth':
              this.ConditionEntity =  'R.DateOfBirth'
              break;                 
              case 'DEX-Estimated Birth Date':
              this.ConditionEntity =  'R.CSTDA_BDEstimate'
              break;
              case 'DEX-Indigenous Status':
              this.ConditionEntity =  'DSS.DexIndiginousStatus'
              break;
              case 'DEX-DVA Card Holder Status':
              this.ConditionEntity =  'ONI.HACCDVACardHolderStatus'
              break;            
              case 'DEX-Has Disabilities':
              this.ConditionEntity =  'DSS.HasDisabilities'
              break;        
              case 'DEX-Has A Carer':
              this.ConditionEntity =  'DSS.HasCarer'
              break;
              case 'DEX-Country of Birth':
              this.ConditionEntity =  'R.CountryOfBirth'
              break;
              case 'DEX-First Arrival Year':
              this.ConditionEntity =  'DSS.FirstArrivalYear'
              break;               
              case 'DEX-First Arrival Month':
              this.ConditionEntity =  'DSS.FirstArrivalMonth'
              break;
              case 'DEX-Visa Code':
              this.ConditionEntity =  'DSS.VisaCategory'
              break;
              case 'DEX-Ancestry':
              this.ConditionEntity =  'DSS.Ancestry'
              break;
              case 'DEX-Main Language At Home':
              this.ConditionEntity =  'R.HomeLanguage'
              break;
              case 'DEX-Accomodation Setting':
              this.ConditionEntity =  'DSS.DEXACCOMODATION'
              break;
              case 'DEX-Is Homeless':
              this.ConditionEntity =  'DSS.IsHomeless'
              break;
              case 'DEX-Household Composition':
              this.ConditionEntity =  'DSS.HouseholdComposition'
              break;
              case 'DEX-Main Source Of Income':
              this.ConditionEntity =  'DSS.MainIncomSource'
              break;
              case 'DEX-Income Frequency':
              this.ConditionEntity =  'DSS.IncomFrequency'
              break;                
              case 'DEX-Income Amount':
              this.ConditionEntity =  'DSS.IncomeAmount'
              break; 
              // CSTDA Dataset Fields                  
              case 'CSTDA-Date Of Birth':
              this.ConditionEntity =  'R.[DateOfBirth]'
              break;
              case 'CSTDA-Gender':
              this.ConditionEntity =  'R.[CSTDA_Sex]'
              break;
              case 'CSTDA-DISQIS ID':
              this.ConditionEntity =  'R.[CSTDA_ID]'
              break;
              case 'CSTDA-Indigenous Status':
              this.ConditionEntity =  'R.[CSTDA_Indiginous]'
              break;
              case 'CSTDA-Country Of Birth':
              this.ConditionEntity =  'R.[CountryOfBirth]'
              break;
              
              case 'CSTDA-Interpreter Required':
              this.ConditionEntity =  'R.[CSTDA_Interpreter]'
              break;
              case 'CSTDA-Communication Method':
              this.ConditionEntity =  'R.[CSTDA_Communication]'
              break;
              case 'CSTDA-Living Arrangements':
              this.ConditionEntity =  'R.[CSTDA_LivingArrangements]'
              break;
              case 'CSTDA-Suburb':
              this.ConditionEntity =  'R.[Suburb]'
              break;
              case 'CSTDA-Postcode':
              this.ConditionEntity =  'R.[Postcode]'
              break;
              case 'CSTDA-State':
              this.ConditionEntity = 'R.[State]'
              break;
              case 'CSTDA-Residential Setting':
              this.ConditionEntity =  'R.[CSTDA_ResidentialSetting]'
              break;
              case 'CSTDA-Primary Disability Group':
              this.ConditionEntity =  'R.[CSTDA_DisabilityGroup]'
              break;
              case 'CSTDA-Primary Disability Description':
              this.ConditionEntity =  'ONI.[CSTDA_PrimaryDisabilityDescription]'
              break;
              case 'CSTDA-Intellectual Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherIntellectual]'
              break;
              case 'CSTDA-Specific Learning ADD Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherADD]'
              break;
              case 'CSTDA-Autism Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherAutism]'
              break;
              case 'CSTDA-Physical Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherPhysical]'
              break;  
              case 'CSTDA-Acquired Brain Injury Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherAcquiredBrain]'
              break;
              case 'CSTDA-Neurological Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherNeurological]'
              break;
              case 'CSTDA-Deaf Blind Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherDeafBlind]'
              break;
              case 'CSTDA-Psychiatric Disability':
              this.ConditionEntity =  'R.[CSTDA_Psychiatric]'
              break;
              case 'CSTDA-Other Psychiatric Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherPsychiatric]'
              break;
              case 'CSTDA-Vision Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherVision]'
              break;
              case 'CSTDA-Hearing Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherHearing]'
              break;
              case 'CSTDA-Speech Disability':
              this.ConditionEntity =  'R.[CSTDA_OtherSpeech]'
              break;
              case 'CSTDA-Developmental Delay Disability':
              this.ConditionEntity =  'R.[CSTDA_DevelopDelay]'
              break;
              case 'CSTDA-Disability Likely To Be Permanent':
              this.ConditionEntity =  'R.[PermDisability]'
              break;
              case 'CSTDA-Support Needs-Self Care':
              this.ConditionEntity = ' R.[CSTDA_SupportNeeds_SelfCare]'
              break;
              case 'CSTDA-Support Needs-Mobility':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Mobility]'
              break;
              case 'CSTDA-Support Needs-Communication':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Communication]'
              break;
              case 'CSTDA-Support Needs-Interpersonal':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Relationships]'
              break; 
              case 'CSTDA-Support Needs-Learning':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Learning]'
              break;
              case 'CSTDA-Support Needs-Education':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Education]'
              break;
              case 'CSTDA-Support Needs-Community':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Community]'
              break;
              case 'CSTDA-Support Needs-Domestic':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Domestic]'
              break;
              case 'CSTDA-Support Needs-Working':
              this.ConditionEntity =  'R.[CSTDA_SupportNeeds_Domestic]'
              break;                 
              case 'CSTDA-Carer-Existence Of Informal':
              this.ConditionEntity =  'R.[CSTDA_CarerExists]'
              break;
              case 'CSTDA-Carer-Assists client in ADL':
              this.ConditionEntity =  'R.[CSTDA_CarerStatus]'
              break;
              case 'CSTDA-Carer-Lives In Same Household':
              this.ConditionEntity =  'R.[CSTDA_CarerResidencyStatus]'
              break;
              case 'CSTDA-Carer-Relationship':
              this.ConditionEntity =  'R.[CSTDA_CarerRelationship]'
              break;
              case 'CSTDA-Carer-Age Group':
              this.ConditionEntity =  'R.[CSTDA_CarerAgeGroup]'
              break;
              case 'CSTDA-Carer Allowance to Guardians':
              this.ConditionEntity =  'R.[CSTDA_CarerAllowance]'
              break;
              case 'CSTDA-Labor Force Status':
              this.ConditionEntity =  'R.[CSTDA_LaborStatus]'
              break;
              case 'CSTDA-Main Source Of Income':
              this.ConditionEntity =  'R.[CSTDA_MainIncome]'
              break;
              case 'CSTDA-Current Individual Funding':
              this.ConditionEntity =  'R.[CSTDA_FundingStatus]'
              break;
              //NRCP Dataset Fields                  
              case 'NRCP-First Name':
              this.ConditionEntity =  'R.[FirstName]'
              break;         
              case 'NRCP-Surname':
              this.ConditionEntity =  'R.[Surname/Organisation]'
              break;
              case 'NRCP-Date Of Birth':
              this.ConditionEntity =  'R.[DateOfBirth]'
              break;
              case 'NRCP-Gender':
              this.ConditionEntity =  'R.[Gender]'
              break;
              case 'NRCP-Suburb':
              this.ConditionEntity =  'R.[Suburb]'
              break;
              case 'NRCP-Country Of Birth':
              this.ConditionEntity =  'R.[CountryOfBirth]'
              break;                
              case 'NRCP-Preferred Language':
              this.ConditionEntity =  'R.[HomeLanguage]'
              break;
              case 'NRCP-Indigenous Status':
              this.ConditionEntity =  'R.[IndiginousStatus]'
              break;                 
              case 'NRCP-Marital Status':
              this.ConditionEntity =  'R.[MaritalStatus]'
              break;
              case 'NRCP-DVA Card Holder Status':
              this.ConditionEntity =  'R.[RECIPT_DVA_Card_Holder_Status]'
              break;
              case 'NRCP-Paid Employment Participation':
              this.ConditionEntity =  'R.[RECIPT_Paid_Employment_Participation]'
              break;
              case 'NRCP-Pension Status':
              this.ConditionEntity =  'R.[NRCP_GovtPensionStatus]'
              break;
              case 'NRCP-Carer-Date Role Commenced':
              this.ConditionEntity =  'R.[RECIPT_Date_Caring_Role_Commenced]'
              break;
              case 'NRCP-Carer-Role':
              this.ConditionEntity =  'R.[RECIPT_Care_Role]'
              break;
              case 'NRCP-Carer-Need':
              this.ConditionEntity =  'R.[RECIPT_Care_Need]'
              break;
              case 'NRCP-Carer-Number of Recipients':
              this.ConditionEntity =  'R.[RECIPT_Number_Care_Recipients]'
              break;
              case 'NRCP-Carer-Time Spent Caring':
              this.ConditionEntity =  'R.[RECIPT_Time_Spent_Caring]'
              break;
              case 'NRCP-Carer-Current Use Formal Services':
              this.ConditionEntity =  'R.[NRCP_CurrentUseFormalServices]'
              break;
              case 'NRCP-Carer-Informal Support':
              this.ConditionEntity =  'R.[NRCP_InformalSupport]'
              break;
              case 'NRCP-Recipient-Challenging Behaviour':
              this.ConditionEntity =  'R.[RECIPT_Challenging_Behaviour]'
              break;
              case 'NRCP-Recipient-Primary Disability':
              this.ConditionEntity =  'R.[RECIPT_Care_Recipients_Primary_Disability]'
              break;         
              case 'NRCP-Recipient-Primary Care Needs':
              this.ConditionEntity =  'R.[RECIPT_Care_Recipients_Primary_Care_Needs]'
              break;
              case 'NRCP-Recipient-Level of Need':
              this.ConditionEntity =  'R.[RECIPT_Care_Recipients_Level_Need]'
              break;
              case 'NRCP-Recipient-Primary Carer':
              this.ConditionEntity =  'R.[DatasetCarer]'
              break;
              case 'NRCP-Recipient-Carer Relationship':
              this.ConditionEntity =  'R.[NRCP_CarerRelationship]'
              break;
              case 'NRCP-Recipient-Carer Co-Resident':
              this.ConditionEntity =  'R.[NRCP_CarerCoResidency]'
              break;
              case 'NRCP-Recipient-Dementia':
              this.ConditionEntity =  'R.[RECIPT_Dementia]'
              break;
              case 'NRCP-CALD Background':
              this.ConditionEntity =  'R.[CALDStatus]'
              break; 
              // "ONI-Core"  
              
              case 'ONI-Family Name':
              this.ConditionEntity =  'R.[Surname/Organisation]'
              break;
              case 'ONI-Title':
              this.ConditionEntity =  'R.[Title]'
              break;
              case 'ONI-First Name':
              this.ConditionEntity =  'R.[FirstName]'
              break;
              case 'ONI-Other':
              this.ConditionEntity =  'R.[MiddleNames]'
              break;
              case 'ONI-Sex':
              this.ConditionEntity =  'R.[Gender]'
              break;
              case 'ONI-DOB':
              this.ConditionEntity =  'R.[DateOfBirth]'
              break;
              case 'ONI-Usual Address-Street':
                this.ConditionEntity = "(SELECT TOP 1 Address1 from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>')" 
              break;
              case 'ONI-Usual Address-Suburb':
                this.ConditionEntity =  "(SELECT TOP 1 Suburb from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>')"
              break;
              case 'ONI-Usual Address-Postcode':
                this.ConditionEntity =  " (SELECT TOP 1 Postcode from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>') "
              break;
              case 'ONI-Contact Address-Street':
                this.ConditionEntity =  " (SELECT TOP 1 Address1 from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>')  "
              break;
              case 'ONI-Contact Address-Suburb':
                this.ConditionEntity =  " (SELECT TOP 1 Suburb from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>') "
              break;
              case 'ONI-Contact Address-Postcode':
                this.ConditionEntity =  " (SELECT TOP 1 Postcode from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>') "
              break;
              case 'ONI-Phone-Home':
                this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<HOME>') "
              break;
              case 'ONI-Phone-Work':
                this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<WORK>')  "
              break;
              case 'ONI-Phone-Mobile':
                this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<MOBILE>') "
              break;
              case 'ONI-Phone-FAX':
                this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<FAX>') "
              break;
              case 'ONI-EMAIL':
                this.ConditionEntity =  " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<EMAIL>') "
              break;
              case 'ONI-Person 1 Name':
                this.ConditionEntity =  " (SELECT TOP 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1')  "
              break;
              case 'ONI-Person 1 Street':
                this.ConditionEntity =  " (SELECT TOP 1 [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1')  "
              break;
              case 'ONI-Person 1 Suburb':
                this.ConditionEntity =  " (SELECT TOP 1 [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1')  "
              break;
              case 'ONI-Person 1 Postcode':
                this.ConditionEntity =  " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
              break;
              case 'ONI-Person 1 Phone':
                this.ConditionEntity =  " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
              break;
              case 'ONI-Person 1 Relationship':
                this.ConditionEntity =  " (SELECT TOP 1 [Type] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1')  "
              break;
              case 'ONI-Person 2 Name':
                this.ConditionEntity =  " (SELECT TOP 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2')  "
              break;
              case 'ONI-Person 2 Street':
                this.ConditionEntity =  " (SELECT TOP 1 [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2')  "
              break;
              case 'ONI-Person 2 Suburb':
                this.ConditionEntity =  " (SELECT TOP 1 [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
              break;
              case 'ONI-Person 2 Postcode':
                this.ConditionEntity =  " (SELECT TOP 1 [Postcode] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2')  "
              break;
              case 'ONI-Person 2 Phone':
                this.ConditionEntity =  " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2')  "
              break;
              case 'ONI-Person 2 Relationship':
                this.ConditionEntity =  " (SELECT TOP 1 [Type] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
              break;
              case 'ONI-Doctor Name':
                this.ConditionEntity =  " (SELECT Top 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
              break;
              case 'ONI-Doctor Street':
                this.ConditionEntity =  " (SELECT Top 1 [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber)  "
              break;
              case 'ONI-Doctor Suburb':
                this.ConditionEntity =  " (SELECT Top 1 [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber)"
              break;
              case 'ONI-Doctor Postcode':
                this.ConditionEntity =  " (SELECT Top 1 [Postcode] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
              break;
              case 'ONI-Doctor Phone':
                this.ConditionEntity =  " (SELECT Top 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber)  "
              break;                           
              case 'ONI-Doctor FAX':
                this.ConditionEntity =   " (SELECT Top 1 [FAX] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
              break;
              case 'ONI-Doctor EMAIL':
                this.ConditionEntity =  " (SELECT Top 1 [Email] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber)"
              break;
              case 'ONI-Referral Source':
              this.ConditionEntity =  'R.[ReferralSource]'
              break;
              case 'ONI-Contact Details':
              this.ConditionEntity =  'R.[ReferralContactInfo]'
              break;
              case 'ONI-Country Of Birth':
              this.ConditionEntity =  'R.[CountryOfBirth]'
              break;
              case 'ONI-Indigenous Status':
              this.ConditionEntity =  'R.[IndiginousStatus]'
              break;
              case 'ONI-Main Language At Home':
              this.ConditionEntity =  'R.[HomeLanguage]'
              break;
              case 'ONI-Interpreter Required':
              this.ConditionEntity =  'R.[InterpreterRequired]'
              break;                  
              case 'ONI-Preferred Language':
              this.ConditionEntity =  'R.[HomeLanguage]'
              break;
              case 'ONI-Govt Pension Status':
              this.ConditionEntity =  'R.[PensionStatus]'
              break;
              case 'ONI-Pension Benefit Card':
              this.ConditionEntity =  'R.[ConcessionNumber]' 
              break;
              case 'ONI-Medicare Number':
              this.ConditionEntity =  'R.[MedicareNumber]'
              break;
              case 'ONI-Health Care Card#':
              this.ConditionEntity =  'R.[Healthcare#]'
              break;                     
              case 'ONI-DVA Cardholder Status':
              this.ConditionEntity =  'ONI.[HACCDVACardHolderStatus]'
              break;
              case 'ONI-DVA Number':
              this.ConditionEntity =  'R.[DVANumber]'
              break;
              case 'ONI-Insurance Status':
              this.ConditionEntity =  'R.[InsuranceStatus]'
              break;
              case 'ONI-Health Insurer':
              //   this.ConditionEntity =  
              break;        
              case 'ONI-Health Insurance Card#':
              //   this.ConditionEntity =  
              break;
              case 'ONI-Alerts':
              this.ConditionEntity =  'R.[Notes]'
              break;
              case 'ONI-Rating':
              this.ConditionEntity =  'R.[ONIRating]'
              break;
              case 'ONI-HACC Eligible':
              this.ConditionEntity =  'R.[HACCEligible]'
              break;
              case 'ONI-Reason For HACC Status':
              this.ConditionEntity =  'R.[DSCEligible]'
              break;
              case 'ONI-Other Support Eligibility':
              this.ConditionEntity =  'R.[HACCReason]'
              break;                 
              case 'ONI-Other Support Detail':
              this.ConditionEntity =  'R.[OtherEligibleDetails]'
              break;              
              case 'ONI-Functional Profile Complete':
              this.ConditionEntity =  'R.[ONIFProfileComplete]'
              break;
              case 'ONI-Functional Profile Score 1':
              this.ConditionEntity =  'R.[ONIFProfile1]'
              break;
              case 'ONI-Functional Profile Score 2':
              this.ConditionEntity =  'R.[ONIFProfile2]'
              break;
              case 'ONI-Functional Profile Score 3':
              this.ConditionEntity =  'R.[ONIFProfile3]'
              break;
              case 'ONI-Functional Profile Score 4':
              this.ConditionEntity =  'R.[ONIFProfile4]'
              break;
              case 'ONI-Functional Profile Score 5':
              this.ConditionEntity =  'R.[ONIFProfile5]'
              break;
              case 'ONI-Functional Profile Score 6':
              this.ConditionEntity = 'R.[ONIFProfile6]'
              break;
              case 'ONI-Functional Profile Score 7':
              this.ConditionEntity =  'R.[ONIFProfile7]'
              break;
              case 'ONI-Functional Profile Score 8':
              this.ConditionEntity =  'R.[ONIFProfile8]'
              break;
              case 'ONI-Functional Profile Score 9':
              this.ConditionEntity =  'R.[ONIFProfile9]' 
              break;
              case 'ONI-Main Problem-Description':
              this.ConditionEntity =  'ONIMainIssues.[Description]'
              break;
              case 'ONI-Main Problem-Action':
              this.ConditionEntity =  'ONIMainIssues.[Action]'
              break;
              case 'ONI-Other Problem-Description':
              this.ConditionEntity =  'ONISecondaryIssues.[Description]'
              break;
              case 'ONI-Other Problem-Action':
              this.ConditionEntity =  'ONISecondaryIssues.[Action]'
              break;                     
              case 'ONI-Current Service':
              this.ConditionEntity =  'ONIServices.[Service]'
              break;
              case 'ONI-Service Contact Details':
              this.ConditionEntity =  'ONIServices.[Information]'
              break;
              case 'ONI-AP-Agency':
              this.ConditionEntity =  'ONIActionPlan.[HealthProfessional]'
              break;
              case 'ONI-AP-For':
              this.ConditionEntity =  'ONIActionPlan.[For]'
              break;
              case 'ONI-AP-Consent':
              this.ConditionEntity = 'ONIActionPlan.[Consent]' 
              break;
              case 'ONI-AP-Referral':
              this.ConditionEntity =  'ONIActionPlan.[Referral]'
              break;
              case 'ONI-AP-Transport':
              this.ConditionEntity =  'ONIActionPlan.[Transport]'
              break;
              case 'ONI-AP-Feedback':
              this.ConditionEntity =  'ONIActionPlan.[Feedback]'
              break;
              case 'ONI-AP-Date':
              this.ConditionEntity =  'ONIActionPlan.[Date]'
              break;
              case 'ONI-AP-Review':
              this.ConditionEntity =  'ONIActionPlan.[Review]'
              break;
              //  ONI-Functional Profile                  
              case 'ONI-FPQ1-Housework':
              this.ConditionEntity =  'ONI.[FP1_Housework]'
              break;
              case 'ONI-FPQ2-GetToPlaces':
              this.ConditionEntity =  'ONI.[FP2_WalkingDistance]'
              break;
              case 'ONI-FPQ3-Shopping':
              this.ConditionEntity =  'ONI.[FP3_Shopping]'
              break;
              case 'ONI-FPQ4-Medicine': 
              this.ConditionEntity =  'ONI.[FP4_Medicine]'
              break;
              case 'ONI-FPQ5-Money':
              this.ConditionEntity =  'ONI.[FP5_Money]'
              break;
              case 'ONI-FPQ6-Walk':
              this.ConditionEntity =  'ONI.[FP6_Walking]'
              break;                 
              case 'ONI-FPQ7-Bath':
              this.ConditionEntity =  'ONI.[FP7_Bathing]'
              break;                  
              case 'ONI-FPQ8-Memory':
              this.ConditionEntity =  'ONI.[FP8_Memory]'
              break;
              case 'ONI-FPQ9-Behaviour':
              this.ConditionEntity =  'ONI.[FP9_Behaviour]'
              break;
              case 'ONI-FP-Recommend Domestic':
              this.ConditionEntity =  'ONI.[FA_Domestic]'
              break;
              case 'ONI-FP-Recommend Self Care':
              this.ConditionEntity =  'ONI.[FA_SelfCare]'
              break;
              case 'ONI-FP-Recommend Cognition':
              this.ConditionEntity =  'ONI.[FA_Cognition]'
              break;
              case 'ONI-FP-Recommend Behaviour':
              this.ConditionEntity =  'ONI.[FA_Behaviour]'
              break;
              case 'ONI-FP-Has Self Care Aids':
              this.ConditionEntity =  'ONI.[Aids_SelfCare]'
              break;
              case 'ONI-FP-Has Support/Mobility Aids':
              this.ConditionEntity =  'ONI.[Aids_SupportAndMobility]'
              break;
              case 'ONI-FP-Has Communication Aids':
              this.ConditionEntity =  'ONI.[Aids_CommunicationAids]'
              break;
              case 'ONI-FP-Has Car Mods':
              this.ConditionEntity =  'ONI.[Aids_CarModifications]'
              break;
              case 'ONI-FP-Has Other Aids':
              this.ConditionEntity =  'ONI.[Aids_Other]'
              break;
              case 'ONI-FP-Other Goods List':
              this.ConditionEntity =  'ONI.[AidsOtherList]'
              break;
              case 'ONI-FP-Has Medical Care Aids':
              this.ConditionEntity =  'ONI.[Aids_MedicalCare]'
              break;                  
              case 'ONI-FP-Has Reading Aids':
              this.ConditionEntity =  'ONI.[Aids_Reading]'
              break;
              case 'ONI-FP-Comments':
              this.ConditionEntity =  'ONI.[FP_Comments]'
              break;
              //  ONI-Living Arrangements Profile                                   
              case 'ONI-LA-Living Arrangements':
              this.ConditionEntity =  'R.[LivingArrangements]'
              break;
              case 'ONI-LA-Living Arrangements Comments':
              this.ConditionEntity =  'ONI.[LAP_LivingComments]'
              break;
              case 'ONI-LA-Accomodation':
              this.ConditionEntity =  'R.[DwellingAccomodation]'
              break;
              case 'ONI-LA-Accomodation Comments':
              this.ConditionEntity =  'ONI.[LAP_AccomodationComments]'
              break;
              case 'ONI-LA-Employment Status':
              this.ConditionEntity =  'ONI.[LAP_Employment]'
              break;
              case 'ONI-LA-Employment Status Comments':
              this.ConditionEntity =  'ONI.[LAP_EmploymentComments]'
              break;                  
              case 'ONI-LA-Mental Health Act Status':
              this.ConditionEntity =  'ONI.[LAP_MentalHealth]'
              break;
              case 'ONI-LA-Decision Making Responsibility':
              this.ConditionEntity =  'ONI.[LAP_Decision]'
              break;
              case 'ONI-LA-Capable Own Decisions':
              this.ConditionEntity =  'ONI.[LAP_DecisionCapable]'
              break;
              case 'ONI-LA-Financial Decisions':
              this.ConditionEntity =  'ONI.[LAP_FinancialDecision]'
              break;
              case 'ONI-LA-Cost Of Living Trade Off':
              this.ConditionEntity =  'ONI.[LAP_LivingCostDecision]'
              break;          
              case 'ONI-LA-Financial & Legal Comments':
              this.ConditionEntity =  'ONI.[LAP_LivingCostDecisionComments]'
              break;
              // ONI-Health Conditions Profile   
              case 'ONI-HC-Overall Health Description':
              this.ConditionEntity =  'ONI.[HC_Overall_General]'
              break;               
              case 'ONI-HC-Overall Health Pain':
              this.ConditionEntity =  'ONI.[HC_Overall_Pain]'
              break;
              case 'ONI-HC-Overall Health Interference':
              this.ConditionEntity = 'ONI.[HC_Overall_Interfere]'
              case 'ONI-HC-Vision Reading':
              this.ConditionEntity =  'ONI.[HC_Vision_Reading]'
              break;
              case 'ONI-HC-Vision Distance':
              this.ConditionEntity =  'ONI.[HC_Vision_Long]'
              break;
              case 'ONI-HC-Hearing':
              this.ConditionEntity =  'ONI.[HC_Hearing]'
              break;
              case 'ONI-HC-Oral Problems':
              this.ConditionEntity =  'ONI.[HC_Oral]'
              break;
              case 'ONI-HC-Oral Comments':
              this.ConditionEntity =  'ONI.[HC_OralComments]'
              break;                  
              case 'ONI-HC-Speech/Swallow Problems':
              this.ConditionEntity =  'ONI.[HC_Speech]'
              break;
              case 'ONI-HC-Speech/Swallow Comments':
              this.ConditionEntity =  'ONI.[HC_SpeechComments]'
              break;
              case 'ONI-HC-Falls Problems':
              this.ConditionEntity =  'ONI.[HC_Falls]'
              break;
              case 'ONI-HC-Falls Comments':
              this.ConditionEntity =  'ONI.[HC_FallsComments]'
              break;
              case 'ONI-HC-Feet Problems':
              this.ConditionEntity =  'ONI.[HC_Feet]'
              break;
              case 'ONI-HC-Feet Comments':
              this.ConditionEntity =  'ONI.[HC_FeetComments]'
              break;
              case 'ONI-HC-Vacc. Influenza':
              this.ConditionEntity =  'ONI.[HC_Vac_Influenza]'
              break;
              case 'ONI-HC-Vacc. Influenza Date':
              this.ConditionEntity =  'ONI.[HC_Vac_Influenza_Date]'
              break;
              case 'ONI-HC-Vacc. Pneumococcus':
              this.ConditionEntity =  'ONI.[HC_Vac_Pneumo]'
              break;
              case 'ONI-HC-Vacc. Pneumococcus  Date':
              this.ConditionEntity =  'ONI.[HC_Vac_Pneumo_Date]'
              break;     
              case 'ONI-HC-Vacc. Tetanus':
              this.ConditionEntity =  'ONI.[HC_Vac_Tetanus]'
              break;
              case 'ONI-HC-Vacc. Tetanus Date':
              this.ConditionEntity =  'ONI.[HC_Vac_Tetanus_Date]'
              break;
              case 'ONI-HC-Vacc. Other':
              this.ConditionEntity =  'ONI.[HC_Vac_Other]'
              break;
              case 'ONI-HC-Vacc. Other Date':
              this.ConditionEntity =  'ONI.[HC_Vac_Other_Date]'
              break;
              case 'ONI-HC-Driving MV':
              this.ConditionEntity =  'ONI.[HC_Driving]'
              break;
              case 'ONI-HC-Driving Fit':
              this.ConditionEntity =  'ONI.[HC_FitToDrive]'
              break;
              case 'ONI-HC-Driving Comments':
              this.ConditionEntity =  'ONI.[HC_DrivingComments]'
              break;
              case 'ONI-HC-Continence Urinary':
              this.ConditionEntity =  'ONI.[HC_Continence_Urine]'
              break;
              case 'ONI-HC-Urinary Related To Coughing':
              this.ConditionEntity =  'ONI.[HC_Continence_Urine_Sneeze]'
              break;
              case 'ONI-HC-Continence Comments':
              this.ConditionEntity =  'ONI.[HC_ContinenceComments]'
              break;
              case 'ONI-HC-Weight':
              this.ConditionEntity =  'ONI.[HC_Weight]'
              break;
              case 'ONI-HC-Height':
              this.ConditionEntity =  'ONI.[HC_Height]'
              break;
              case 'ONI-HC-BMI':
              this.ConditionEntity =  'ONI.[HC_BMI]'
              break;
              case 'ONI-HC-BP Systolic':
              this.ConditionEntity = 'ONI.[HC_BP_Systolic]' 
              break;                 
              case 'ONI-HC-BP Diastolic':
              this.ConditionEntity =  'ONI.[HC_BP_Diastolic]'
              break;
              case 'ONI-HC-Pulse Rate':
              this.ConditionEntity =  'ONI.[HC_PulseRate]'
              break;          
              case 'ONI-HC-Pulse Regularity':
              this.ConditionEntity =  'ONI.[HC_Pulse]'
              break;
              case 'ONI-HC-Check Postural Hypotension':
              this.ConditionEntity =  'ONI.[HC_PHCheck]'
              break;
              case 'ONI-HC-Conditions':
              this.ConditionEntity =  'ONIHealthConditions.[Description]'
              break;
              case 'ONI-HC-Diagnosis':
              this.ConditionEntity =  'MDiagnosis.[Description]'
              break;
              case 'ONI-HC-Medicines':
              this.ConditionEntity =  'ONIMedications.[Description]'
              break;
              case 'ONI-HC-Take Own Medication':
              this.ConditionEntity =  'ONI.[HC_Med_TakeOwn]'
              break;                
              case 'ONI-HC-Willing When Prescribed':
              this.ConditionEntity =  'ONI.[HC_Med_Willing]'
              break;
              case 'ONI-HC-Coop With Health Services':
              this.ConditionEntity =  'ONI.[HC_Med_Coop]'
              break;
              case 'ONI-HC-Webster Pack':
              this.ConditionEntity =  'ONI.[HC_Med_Webster]'
              break;
              case 'ONI-HC-Medication Review':
              this.ConditionEntity =  'ONI.[HC_Med_Review]'
              break;
              case 'ONI-HC-Medical Comments':
              this.ConditionEntity =  'ONI.[HC_MedComments]'
              break;
              //ONI-Psychosocial Profile                  
              case 'ONI-PS-K10-1':
              this.ConditionEntity =  'ONI.[PP_K10_1]'
              break;
              case 'ONI-PS-K10-2':
              this.ConditionEntity =  'ONI.[PP_K10_2]'
              break;
              case 'ONI-PS-K10-3':
              this.ConditionEntity =  'ONI.[PP_K10_3]'
              break;
              case 'ONI-PS-K10-4':
              this.ConditionEntity =  'ONI.[PP_K10_4]'
              break;
              case 'ONI-PS-K10-5':
              this.ConditionEntity =  'ONI.[PP_K10_5]'
              break;
              case 'ONI-PS-K10-6':
              this.ConditionEntity =  'ONI.[PP_K10_6]'
              break;
              case 'ONI-PS-K10-7':
              this.ConditionEntity =  'ONI.[PP_K10_7]'
              break;
              case 'ONI-PS-K10-8':
              this.ConditionEntity =  'ONI.[PP_K10_8]'
              break;
              case 'ONI-PS-K10-9':
              this.ConditionEntity =  'ONI.[PP_K10_9]'
              break;
              case 'ONI-PS-K10-10':
              this.ConditionEntity =  'ONI.[PP_K10_10]'
              break;
              case 'ONI-PS-Sleep Difficulty':
              this.ConditionEntity =  'ONI.[PP_SleepingDifficulty]'
              break;
              case 'ONI-PS-Sleep Details':
              this.ConditionEntity =  'ONI.[PP_SleepingDifficultyComments]'
              break;
              case 'ONI-PS-Personal Support':
              this.ConditionEntity =  'ONI.[PP_PersonalSupport]'
              break;
              case 'ONI-PS-Personal Support Comments':
              this.ConditionEntity =  'ONI.[PP_PersonalSupportComments]'
              break;
              case 'ONI-PS-Keep Friendships':
              this.ConditionEntity =  'ONI.[PP_Relationships_KeepUp]'
              break;
              case 'ONI-PS-Problems Interacting':
              this.ConditionEntity =  'ONI.[PP_Relationships_Problem]'
              break;
              case 'ONI-PS-Family/Relationship Comments':
              this.ConditionEntity =  'ONI.[PP_RelationshipsComments]'
              break;
              case 'ONI-PS-Svc Provider Relations':
              this.ConditionEntity =  'ONI.[PP_Relationships_SP]'
              break;
              case 'ONI-PS-Svc e Comments':
              this.ConditionEntity =  'ONI.[PP_Relationships_SPComments]'
              break;
              //ONI-Health Behaviours Profile                  
              case 'ONI-HB-Regular Health Checks':
              this.ConditionEntity =  'ONI.[HBP_HealthChecks]'
              break;
              case 'ONI-HB-Last Health Check':
              this.ConditionEntity =  'ONI.[HBP_HealthChecks_Last]'
              break;                                    
              case 'ONI-HB-Health Screens':
              this.ConditionEntity =  'ONI.[HBP_HealthChecks_List]'
              break;
              case 'ONI-HB-Smoking':
              this.ConditionEntity =  'ONI.[HBP_Smoking]'
              break;                  
              case 'ONI-HB-If Quit Smoking - When?':
              this.ConditionEntity =  'ONI.[HBP_Smoking_Quit]'
              break;
              case 'ONI-HB-Alcohol-How often?':
              this.ConditionEntity =  'ONI.[HBP_Alcohol]'
              break;
              case 'ONI-HB-Alcohol-How many?':
              this.ConditionEntity =  'ONI.[HBP_Alcohol_NoDrinks]'
              break;
              case 'ONI-HB-Alcohol-How often over 6?':
              this.ConditionEntity =  'ONI.[HBP_Alcohol_BingeNo]'
              break;
              case 'ONI-HB-Lost Weight':
              this.ConditionEntity =  'ONI.[HBP_Malnutrition_LostWeight]'
              break;
              case 'ONI-HB-Eating Poorly':
              this.ConditionEntity =  'ONI.[HBP_Malnutrition_PoorEating]'
              break;
              case 'ONI-HB-How much wieght lost':
              this.ConditionEntity =  'ONI.[HBP_Malnutrition_LostWeightAmount]'
              break;
              case 'ONI-HB-Malnutrition Score':
              this.ConditionEntity =  'ONI.[HBP_Malnutrition_Score]'
              break;
              case 'ONI-HB-8 cups fluid':
              this.ConditionEntity =  'ONI.[HBP_Hydration_AdequateFluid]'
              break;
              case 'ONI-HB-Recent decrease in fluid':
              this.ConditionEntity =  'ONI.[HBP_Hydration_DecreasedFluid]'
              break;
              case 'ONI-HB-Weight':
              this.ConditionEntity =  'ONI.[HBP_Weight]'
              break;
              case 'ONI-HB-Physical Activity':
              this.ConditionEntity =  'ONI.[HBP_PhysicalActivity]'
              break;
              case 'ONI-HB-Physical Fitness':
              this.ConditionEntity =  'ONI.[HBP_PhysicalFitness]'
              break;
              case 'ONI-HB-Fitness Comments':
              this.ConditionEntity =  'ONI.[HBP_Comments]'
              break;
              //ONI-CP-Need for carer 
              case 'ONI-CP-Need for carer':
              this.ConditionEntity =  'ONI.[C_General_NeedForCarer]'
              break;                 
              case 'ONI-CP-Carer Availability':
              this.ConditionEntity =  'R.[CarerAvailability]'
              break;                            
              case 'ONI-CP-Carer Residency Status':
              this.ConditionEntity =  'R.[CarerResidency]'
              break;
              case 'ONI-CP-Carer Relationship':
              this.ConditionEntity =  'R.[CarerRelationship]'
              break;
              case 'ONI-CP-Carer has help':
              this.ConditionEntity =  'ONI.[C_Support_Help]'
              break;
              case 'ONI-CP-Carer receives payment':
              this.ConditionEntity =  'ONI.[C_Support_Allowance]'
              break;
              case 'ONI-CP-Carer made aware support services':
              this.ConditionEntity =  'ONI.[C_Support_Information]'
              break; 
              
              case 'ONI-CP-Carer needs training':
              this.ConditionEntity =  'ONI.[C_Support_NeedTraining]'
              break;
              case 'ONI-CP-Carer threat-emotional':
              this.ConditionEntity =  'ONI.[C_Threats_Emotional]'
              break;
              case 'ONI-CP-Carer threat-acute physical':
              this.ConditionEntity =  'ONI.[C_Threats_Physical]'
              break;
              case 'ONI-CP-Carer threat-slow physical':
              this.ConditionEntity =  'ONI.[C_Threats_Physical_Slow]'
              break;
              case 'ONI-CP-Carer threat-other factors':
              this.ConditionEntity =  'ONI.[C_Threats_Unrelated]'
              break;
              case 'ONI-CP-Carer threat-increasing consumer needs':
              this.ConditionEntity =  'ONI.[C_Threats_ConsumerNeeds]'
              break;
              case 'ONI-CP-Carer threat-other comsumer factors':
              this.ConditionEntity =  'ONI.[C_Threats_ConsumerOther]'
              break;
              case 'ONI-CP-Carer arrangements sustainable':
              this.ConditionEntity =  'ONI.[C_Issues_Sustainability]'
              break;
              case 'ONI-CP-Carer Comments':
              this.ConditionEntity =  'ONI.[C_Issues_Comments]'
              break;
              //ONI-CS-Year of Arrival  
              
              case 'ONI-CS-Year of Arrival':
              this.ConditionEntity =  'ONI.[CAL_ArrivalYear]'
              break;               
              case 'ONI-CS-Citizenship Status':
              this.ConditionEntity =  'ONI.[CAL_Citizenship]'
              break;
              case 'ONI-CS-Reasons for moving to Australia':
              this.ConditionEntity =  'ONI.[CAL_ReasonsMoveAustralia]'
              break;
              case 'ONI-CS-Primary/Secondary Language Fluency':
              this.ConditionEntity =  'ONI.[CAL_PrimSecLanguage]'
              break;
              case 'ONI-CS-Fluency in English':
              this.ConditionEntity =  'ONI.[CAL_EnglishProf]'
              break;
              case 'ONI-CS-Literacy in primary language':
              this.ConditionEntity =  'ONI.[CAL_PrimaryLiteracy]'
              break;
              case 'ONI-CS-Literacy in English':
              this.ConditionEntity =  'ONI.[CAL_EnglishLiteracy]'
              break;
              case 'ONI-CS-Non verbal communication style':
              this.ConditionEntity =  'ONI.[CAL_NonVerbalStyle]'
              break;
              case 'ONI-CS-Marital Status':
              this.ConditionEntity =  'ONI.[CAL_Marital]'
              break;
              case 'ONI-CS-Religion':
              this.ConditionEntity =  'ONI.[CAL_Religion]'
              break;
              case 'ONI-CS-Employment history in country of origin':
              this.ConditionEntity =  'ONI.[CAL_EmploymentHistory]'
              break;
              case 'ONI-CS-Employment history in Australia':
              this.ConditionEntity =  'ONI.[CAL_EmploymentHistoryAust]'
              break;
              case 'ONI-CS-Specific dietary needs':
              this.ConditionEntity =  'ONI.[CAL_DietaryNeeds]'
              break;
              case 'ONI-CS-Specific cultural needs':
              this.ConditionEntity =  'ONI.[CAL_SpecificCulturalNeeds]'
              break;
              case 'ONI-CS-Someone to talk to for day to day problems':
              this.ConditionEntity =  'ONI.[CALSocIsol_1]'
              break;          
              case 'ONI-CS-Miss having close freinds':
              this.ConditionEntity =  'ONI.[CALSocIsol_2]'
              break;
              case 'ONI-CS-Experience general sense of emptiness':
              this.ConditionEntity =  'ONI.[CALSocIsol_3]'
              break;
              case 'ONI-CS-Plenty of people to lean on for problems':
              this.ConditionEntity =  'ONI.[CALSocIsol_4]'
              break;
              case 'ONI-CS-Miss the pleasure of the company of others':
              this.ConditionEntity =  'ONI.[CALSocIsol_5]'
              break;
              case 'ONI-CS-Circle of friends and aquaintances too limited':
              this.ConditionEntity =  'ONI.[CALSocIsol_6]'
              break;
              case 'ONI-CS-Many people I trust completely':
              this.ConditionEntity =  'ONI.[CALSocIsol_7]'
              break;               
              case 'ONI-CS-Enough people I feel close to':
              this.ConditionEntity =  'ONI.[CALSocIsol_8]'
              break;
              case 'ONI-CS-Miss having people around':
              this.ConditionEntity =  'ONI.[CALSocIsol_9]'
              break;
              case 'ONI-CS-Often feel rejected':
              this.ConditionEntity =  'ONI.[CALSocIsol_10]'
              break;
              case 'ONI-CS-Can call on my friends whenever I need them':
              this.ConditionEntity =  'ONI.[CALSocIsol_11]'
              break;
              //Loan Items                  
              case 'Loan Item Type':
              this.ConditionEntity =  'HRLoan.[Type]'
              break;
              case 'Loan Item Description':
              this.ConditionEntity =  'HRLoan.[Name]'
              break;
              case 'Loan Item Date Loaned/Installed':
              this.ConditionEntity =  'HRLoan.[Date1]'
              break;
              case 'Loan Item Date Collected':
              this.ConditionEntity =  'HRLoan.[Date2]'
              break;          
              //  service information Fields                  
              case 'Staff Code':
              this.ConditionEntity =  'SvcDetail.[Carer Code]'
              break;
              case 'Service Date':
              this.ConditionEntity =  'SvcDetail.Date'
              break;
              case 'Service Start Time':
              this.ConditionEntity =  'SvcDetail.[Start Time]'
              break;
              case 'Service Code':
              this.ConditionEntity =  'SvcDetail.[Service Type]'
              break;
              case 'Service Hours':
              this.ConditionEntity =  '(SvcDetail.[Duration]*5) / 60'
              break;
              case 'Service Pay Rate':
              this.ConditionEntity =  'SvcDetail.[Unit Pay Rate]'
              break;
              case 'Service Bill Rate':
              this.ConditionEntity =  'SvcDetail.[Unit Bill Rate]'
              break;
              case 'Service Bill Qty':
              this.ConditionEntity =  'SvcDetail.[BillQty]'
              break;
              case 'Service Location/Activity Group':
              this.ConditionEntity =  'SvcDetail.[ServiceSetting]'
              break;
              case 'Service Program':
              this.ConditionEntity =  'SvcDetail.[Program]'
              break;
              case 'Service Group':
              this.ConditionEntity =  'SvcDetail.[Type]'
              break;
              case 'Service HACC Type':
              this.ConditionEntity =  'SvcDetail.[HACCType]'
              break;
              case 'Service Category':
              this.ConditionEntity =  'SvcDetail.[Anal]'
              break;
              case 'Service Status':
              this.ConditionEntity =  'SvcDetail.[Status]'
              break;
              case 'Service Pay Type':
              this.ConditionEntity =  'SvcDetail.[Service Description]'
              break;
              case 'Service Pay Qty':
              this.ConditionEntity =  'SvcDetail.[CostQty]'
              break;                  
              case 'Service End Time/ Shift End Time':
              this.ConditionEntity =  'Convert(varchar,DATEADD(minute,(SvcDetail.[Duration]*5) ,SvcDetail.[Start Time]),108)'
              break;
              case 'Service Funding Source':
              this.ConditionEntity =  'Humanresourcetypes.[Type]'
              break;                     
              case 'Service Notes':
              this.ConditionEntity =  'CAST(History.Detail AS varchar(4000))'
              break;
              //Service Specific Competencies                
              case 'Activity':
              this.ConditionEntity =  'SvcSpecCompetency.[Group]'
              break;
              case 'Competency':
              this.ConditionEntity =  'SvcSpecCompetency.[Name]'
              break;
              case 'S Status':
                this.ConditionEntity =  'ServiceCompetencyStatus.[ServiceStatus]'
              break;
              //  RECIPIENT OP NOTES                  
              case 'OP Notes Date':
              this.ConditionEntity =  'OPHistory.[DetailDate]'
              break;
              case 'OP Notes Detail':
              this.ConditionEntity =  'OPHistory.[Detail]'
              break;                   
              case 'OP Notes Creator':
              this.ConditionEntity =  'OPHistory.[Creator]'
              break;
              case 'OP Notes Alarm':
              this.ConditionEntity =  'OPHistory.[AlarmDate]'
              break;
              case 'OP Notes Program':
              this.ConditionEntity =  'OPHistory.[Program]'
              break;
              case 'OP Notes Category':
              this.ConditionEntity =  'OPHistory.ExtraDetail2'
              break;
              // RECIPIENT CLINICAL NOTES                  
              case 'Clinical Notes Date':
              this.ConditionEntity =  'CliniHistory.[DetailDate]'
              break;
              case 'Clinical Notes Detail':
              this.ConditionEntity =  'dbo.RTF2TEXT(CliniHistory.[Detail])'
              break;
              case 'Clinical Notes Creator':
              this.ConditionEntity =   'CliniHistory.[Creator]'
              break;
              
              case 'Clinical Notes Alarm':
              this.ConditionEntity =  'CliniHistory.[AlarmDate]'
              break;
              case 'Clinical Notes Category':
              this.ConditionEntity =  'CliniHistory.[ExtraDetail2]'
              break;
              // RECIPIENT INCIDENTS                  
              case 'INCD_Status':
              this.ConditionEntity =  'IMM.Status'
              break;
              case 'INCD_Date':
              this.ConditionEntity =  'IMM.Date'
              break;
              case 'INCD_Type':
              this.ConditionEntity =  'IMM.[Type]'
              break;
              case 'INCD_Description':
              this.ConditionEntity =  'IMM.ShortDesc'
              break;
              case 'INCD_SubCategory':
              this.ConditionEntity =  'IMM.PerpSpecify'
              break;
              case 'INCD_Assigned_To':
              this.ConditionEntity =  'IMM.CurrentAssignee'
              break;         
              case 'INCD_Service':
              this.ConditionEntity =  'IMM.Service'
              break;
              case 'INCD_Severity':
              this.ConditionEntity =  'IMM.Severity'
              break;
              case 'INCD_Time':
              this.ConditionEntity =  'IMM.Time'
              break;
              case 'INCD_Duration':
              this.ConditionEntity =  'IMM.Duration'
              break;
              case 'INCD_Location':
              this.ConditionEntity =  'IMM.Location'
              break;
              case 'INCD_LocationNotes':
              this.ConditionEntity =  'IMM.LocationNotes'
              break;
              case 'INCD_ReportedBy':
              this.ConditionEntity =  'IMM.ReportedBy'
              break;
              case 'INCD_DateReported':
              this.ConditionEntity =  'IMM.DateReported'
              break;
              case 'INCD_Reported':
              this.ConditionEntity =  'IMM.Reported'
              break;
              case 'INCD_FullDesc':
              this.ConditionEntity =  'IMM.FullDesc'
              break;
              case 'INCD_Program':
              this.ConditionEntity =  'IMM.Program'
              break;
              case 'INCD_DSCServiceType':
              this.ConditionEntity =  'IMM.DSCServiceType'
              break;
              case 'INCD_TriggerShort':
              this.ConditionEntity =  'IMM.TriggerShort'
              break;
              case 'INCD_incident_level':
              this.ConditionEntity =  'IMM.incident_level'
              break;
              case 'INCD_Area':
              this.ConditionEntity =  'IMM.area'
              break;
              case 'INCD_Region':
              this.ConditionEntity =  'IMM.Region'
              break;
              case 'INCD_position':
              this.ConditionEntity =  'IMM.position'
              break;
              case 'INCD_phone':
              this.ConditionEntity =  'IMM.phone'
              break;
              case 'INCD_verbal_date':
              this.ConditionEntity =  'IMM.verbal_date'
              break;
              case 'INCD_verbal_time':
              this.ConditionEntity =  'IMM.verbal_time'
              break;
              case 'INCD_By_Whom':
              this.ConditionEntity =  'IMM.By_Whome'
              break;
              case 'INCD_To_Whom':
              this.ConditionEntity =  'IMM.To_Whome'
              break;
              case 'INCD_BriefSummary':
              this.ConditionEntity =  'IMM.BriefSummary'
              break;
              case 'INCD_ReleventBackground':
              this.ConditionEntity =  'IMM.ReleventBackground'
              break;
              case 'INCD_SummaryofAction':
              this.ConditionEntity =  'IMM.SummaryofAction'
              break;
              case 'INCD_SummaryOfOtherAction':
              this.ConditionEntity =  'IMM.SummaryOfOtherAction'
              break;
              case 'INCD_Triggers':
              this.ConditionEntity =  'IMM.Triggers'
              break;
              case 'INCD_InitialAction':
              this.ConditionEntity =  'IMM.InitialAction'
              break;
              case 'INCD_InitialNotes':
              this.ConditionEntity =  'IMM.InitialNotes'
              break;
              case 'INCD_InitialFupBy':
              this.ConditionEntity =  'IMM.InitialFupBy'
              break;
              case 'INCD_Completed':
              this.ConditionEntity =  'IMM.Completed'
              break;
              case 'INCD_OngoingAction':
              this.ConditionEntity =  'IMM.OngoingAction'
              break;
              case 'INCD_OngoingNotes':
              this.ConditionEntity =  'IMM.OngoingNotes'
              break;
              case 'INCD_Background':
              this.ConditionEntity =  'IMM.Background'
              break;
              case 'INCD_Abuse':
              this.ConditionEntity =  'IMM.Abuse'
              break;
              case 'INCD_DOPWithDisability':
              this.ConditionEntity =  'IMM.DOPWithDisability'
              break;
              case 'INCD_SeriousRisks':
              this.ConditionEntity =  'IMM.SeriousRisks'
              break;
              case 'INCD_Complaints':
              this.ConditionEntity =  'IMM.Complaints'
              break;                                   
              case 'INCD_Perpetrator':
              this.ConditionEntity =  'IMM.Perpetrator'
              break;
              case 'INCD_Notify':
              this.ConditionEntity =  'IMM.Notify'
              break;
              case 'INCD_NoNotifyReason':
              this.ConditionEntity =  'IMM.NoNotifyReason'
              break; 
              case 'INCD_Notes':
              this.ConditionEntity =  'IMM.Notes'
              break;
              case 'INCD_Setting':
              this.ConditionEntity =  'IMM.Setting'
              break;
              case 'INCD_Involved_Staff':
              this.ConditionEntity =  'IMI.Staff#'
              break;
              //  Recipient Competencies                  
              case 'Recipient Competency':
              this.ConditionEntity =  'RecpCompet.Name'
              break;
              case 'Recipient Competency Mandatory':
              this.ConditionEntity =  'RecpCompet.Recurring'
              break;
              case 'Recipient Competency Notes':
              this.ConditionEntity =  'RecpCompet.[Notes]'
              break;
              //Care Plan                  
              case 'CarePlan ID':
              this.ConditionEntity =  'D.Doc#'
              break;
              case 'CarePlan Name':
              this.ConditionEntity =  'D.Title'
              break;
              case 'CarePlan Type':
                this.ConditionEntity =  "(SELECT Description FROM DataDomains Left join DOCUMENTS D on  DataDomains.RecordNumber = D.SubId)"
              break;
              case 'CarePlan Program':
                this.ConditionEntity =  "(SELECT [Name] FROM HumanResourceTypes WHERE HumanResourceTypes.RecordNumber = D.Department AND [Group] = 'PROGRAMS')"
              break;                  
              case 'CarePlan Discipline':
                this.ConditionEntity =  " (SELECT [Description] FROM DataDomains WHERE DataDomains.RecordNumber = D.DPID)"
              break;
              case 'CarePlan CareDomain':
                this.ConditionEntity =  "(SELECT [Description] FROM DataDomains WHERE DataDomains.RecordNumber = D.CareDomain)"
              break;
              case 'CarePlan StartDate':
              this.ConditionEntity =  'D.DocStartDate'
              break;
              case 'CarePlan SignOffDate':
              this.ConditionEntity =  'D.DocEndDate'
              break;
              case 'CarePlan ReviewDate':
              this.ConditionEntity =  'D.AlarmDate'
              break;
              case 'CarePlan ReminderText':
              this.ConditionEntity =  'D.AlarmText'
              break;
              case 'CarePlan Archived':
              this.ConditionEntity =  'D.DeletedRecord'
              break;
              //Mental Health                  
              case 'MH-PERSONID':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[PERSONID]'
              break;
              case 'MH-HOUSING TYPE ON REFERRAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[HOUSING TYPE ON REFERRAL]'
              break;
              case 'MH-RE REFERRAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[RE REFERRAL]'
              break;
              case 'MH-REFERRAL SOURCE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[REFERRAL SOURCE]'
              break;
              case 'MH-REFERRAL RECEIVED DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[REFERRAL RECEIVED DATE]'
              break;
              
              case 'MH-ENGAGED AND CONSENT DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ENGAGED AND CONSENT DATE]'
              break;
              case 'MH-OPEN TO HOSPITAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[OPEN TO HOSPITAL]'
              break;                
              case 'MH-OPEN TO HOSPITAL DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[OPEN TO HOSPITAL DETAILS]'
              break;
              case 'MH-ALERTS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ALERTS]'
              break;
              case 'MH-ALERTS DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ALERTS DETAILS]'
              break;
              case 'MH-MH DIAGNOSIS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[MH DIAGNOSIS]'
              break;
              case 'MH-MEDICAL DIAGNOSIS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[MEDICAL DIAGNOSIS]'
              break;
              case 'MH-REASONS FOR EXIT':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[REASONS FOR EXIT]'
              break;                  
              case 'MH-SERVICES LINKED INTO':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[SERVICES LINKED INTO]'
              break;
              case 'MH-NON ACCEPTED REASONS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[NON ACCEPTED REASONS]'
              break;
              case 'MH-NOT PROCEEDED':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[NOT PROCEEDED]'
              break;               
              case 'MH-DISCHARGE DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[DISCHARGE DATE]'
              break;
              case 'MH-CURRENT AOD':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[CURRENT AOD]'
              break;
              case 'MH-CURRENT AOD DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[CURRENT AOD DETAILS]'
              break;
              case 'MH-PAST AOD':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[PAST AOD]'
              break;
              case 'MH-PAST AOD DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[PAST AOD DETAILS]'
              break;
              case 'MH-ENGAGED AOD':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ENGAGED AOD]'
              break;
              case 'MH-ENGAGED AOD DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ENGAGED AOD DETAILS]'
              break;
              case 'MH-SERVICES CLIENT IS LINKED WITH ON INTAKE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[SERVICES CLIENT IS LINKED WITH ON INTAKE]'
              break;
              case 'MH-SERVICES CLIENT IS LINKED WITH ON EXIT':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[SERVICES CLIENT IS LINKED WITH ON EXIT]'
              break;
              case 'MH-ED PRESENTATIONS ON REFERRAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ED PRESENTATIONS ON REFERRAL]'
              break;               
              case 'MH-ED PRESENTATIONS ON 3 MONTH REVIEW':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ED PRESENTATIONS ON 3 MONTH REVIEW]'
              break;
              case 'MH-ED PRESENTATIONS ON EXIT':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ED PRESENTATIONS ON EXIT]'
              break;
              case 'MH-AMBULANCE ARRIVAL ON REFERRAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON REFERRAL]'
              break;
              case 'MH-AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW]'
              break;
              case 'MH-AMBULANCE ARRIVAL ON EXIT':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON EXIT]'
              break;
              case 'MH-ADMISSIONS ON REFERRAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ADMISSIONS ON REFERRAL]'
              break;
              case 'MH-ADMISSIONS ON MID-3 MONTH REVIEW':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[ADMISSIONS ON MID- 3 MONTH REVIEW]'
              break;
              case 'MH-ADMISSIONS TO ED ON TIME OF EXIT':
              this.ConditionEntity = 'MENTALHEALTHDATASET.[ADMISSIONS TO ED ON TIME OF EXIT]' 
              break;                  
              case 'MH-RESIDENTIAL MOVES':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[RESIDENTIAL MOVES]'
              break;
              case 'MH-DATE OF RESIDENTIAL CHANGE OF ADDRESS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[DATE OF RESIDENTIAL CHANGE OF ADDRESS]'
              break;
              case 'MH-LOCATION OF NEW ADDRESS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[LOCATION OF NEW ADDRESS]'
              break;
              case 'MH-HOUSING TYPE ON EXIT':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[HOUSING TYPE ON EXIT]'
              break;
              case 'MH-KPI - INTAKE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KPI - INTAKE]'
              break;
              case 'MH-KPI - 3 MONTH REVEIEW':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KPI - 3 MONTH REVEIEW]'
              break;
              case 'MH-KPI - EXIT':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KPI - EXIT]'
              break;
              case 'MH-MEDICAL DIAGNOSIS DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[MEDICAL DIAGNOSIS DETAILS]'
              break;
              case 'MH-SERVICES LINKED DETAILS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[SERVICES LINKED DETAILS]'
              break;
              case 'MH-NDIS TYPE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[NDIS TYPE]'
              break;
              case 'MH-NDIS TYPE COMMENTS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[NDIS TYPE COMMENTS]'
              break;
              case 'MH-NDIS NUMBER':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[NDIS NUMBER]'
              break;
              case 'MH-REVIEW APPEAL':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[REVIEW APPEAL]'
              break;
              case 'MH-REVIEW COMMENTS':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[REVIEW COMMENTS]'
              break;
              case 'MH-KP_Intake_1':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_1]'
              break;
              case 'MH-KP_Intake_2':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_2]'
              break;
              case 'MH-KP_Intake_3MH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_3M]'
              break;                                   
              case 'MH-KP_Intake_3PH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_3P]'
              break;
              case 'MH-KP_Intake_4':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_4]'
              break;
              case 'MH-KP_Intake_5':
              this.ConditionEntity = 'MENTALHEALTHDATASET.[KP_IN_5]' 
              break;
              case 'MH-KP_Intake_6':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_6]'
              break;
              case 'MH-KP_Intake_7':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_7]'
              break;
              case 'MH-KP_3Months_1':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_1]'
              break;
              case 'MH-KP_3Months_2':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_2]'
              break;
              case 'MH-KP_3Months_3MH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_3M]'
              break;
              case 'MH-KP_3Months_3PH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_3P]'
              break;
              case 'MH-KP_3Months_4':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_4]'
              break;
              case 'MH-KP_3Months_5':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_5]'
              break;
              case 'MH-KP_3Months_6':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_6]'
              break;
              case 'MH-KP_3Months_7':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_7]'
              break;
              case 'MH-KP_6Months_1':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_1]'
              break; 
              case 'MH-KP_6Months_2':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_2]'
              break;
              case 'MH-KP_6Months_3MH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_3M]'
              break;
              case 'MH-KP_6Months_3PH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_3P]'
              break;
              case 'MH-KP_6Months_4':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_4]'
              break;
              case 'MH-KP_6Months_5':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_5]'
              break;
              case 'MH-KP_6Months_6':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_6]'
              break;
              case 'MH-KP_6Months_7':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_6_7]'
              break;
              case 'MH-KP_9Months_1':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_1]'
              break;
              case 'MH-KP_9Months_2':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_2]'
              break;
              case 'MH-KP_9Months_3MH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_3M]'
              break;
              case 'MH-KP_9Months_3PH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_3P]'
              break;
              case 'MH-KP_9Months_4':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_4]'
              break;
              case 'MH-KP_9Months_5':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_5]'
              break;
              case 'MH-KP_9Months_6':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_6]'
              break;
              case 'MH-KP_9Months_7':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_7]'
              break;
              case 'MH-KP_Exit_1':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_1]'
              break;
              case 'MH-KP_Exit_2':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_2]'
              break;
              case 'MH-KP_Exit_3MH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_3M]'
              break;
              case 'MH-KP_Exit_3PH':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_3P]'
              break;
              case 'MH-KP_Exit_4':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_4]'
              break;       
              case 'MH-KP_Exit_5':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_5]'
              break;
              case 'MH-KP_Exit_6':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_6]'
              break;
              case 'MH-KP_Exit_7':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_7]'
              break;
              case 'MH-KP_Intake_DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_IN_DATE]'
              break;
              case 'MH-KP_3Months_DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_3_DATE]'
              break;
              case 'MH-KP_6Months_DATE':
              this.ConditionEntity = 'MENTALHEALTHDATASET.[KP_6_DATE]' 
              break;
              case 'MH-KP_9Months_DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_9_DATE]'
              break;
              case 'MH-KP_Exit_DATE':
              this.ConditionEntity =  'MENTALHEALTHDATASET.[KP_EX_DATE]'
              break;
              //Recipient Placements                  
              case 'Placement Type':
              this.ConditionEntity =  'HRPlacements.[Type]'
              break;
              case 'Placement Carer Name':
              this.ConditionEntity =  'HRPlacements.[Name]'
              break;
              case 'Placement Start':
              this.ConditionEntity =  'HRPlacements.[Date1]'
              break;
              case 'Placement End':
              this.ConditionEntity =  'HRPlacements.[Date2]'
              break;
              case 'Placement Referral':
              this.ConditionEntity = 'HRPlacements.[Recurring]' 
              break;        
              case 'Placement ATC':
              this.ConditionEntity =  'HRPlacements.[Completed]'
              break;
              case 'Placement Notes':
              this.ConditionEntity =  'HRPlacements.[Notes]'
              break;
              //Quote Goals and stratagies                  
              case 'Quote Goal':
              this.ConditionEntity =  'GOALS.User1'
              break;
              case 'Goal Expected Completion Date':
              this.ConditionEntity =  'GOALS.Date1'
              break;
              case 'Goal Last Review Date':
              this.ConditionEntity =  'GOALS.Date2'
              break;
              case 'Goal Completed Date':
              this.ConditionEntity =  'GOALS.DateInstalled'
              break;                  
              case 'Goal  Achieved':
              this.ConditionEntity =  'GOALS.[State]'
              break;
              case 'Quote Strategy':
              this.ConditionEntity =  'STRATEGIES.Notes'
              break;
              case 'Strategy Expected Outcome':
              this.ConditionEntity =  'STRATEGIES.Address1'
              break;
              case 'Strategy Contracted ID':
              this.ConditionEntity =  'STRATEGIES.[State]'
              break;
              case 'Strategy DS Services':
              this.ConditionEntity =  'STRATEGIES.User1'
              break;
              


              default:
              break;
            }
          }
          this.QueryFormation();
          this.Entity_IN = [];
        }
        
        delete(index){
          //console.log(index)          
          if (this.entity != null){
            if (index != -1) {
              this.value.splice(index, 1); 
              this.entity.splice(index, 1);
              this.condition.splice(index, 1); 
              if(this.Endvalue != null){
                this.Endvalue.splice(index, 1);
              }               
            
            }
            this.sqlcondition = '';
            this.needQuerysetting = 2;
          } 
       
        
        }
        deletelistitem(index){        
          this.list.splice(index, 1); 
          this.exportitemsArr.splice(index, 1);                                  
        }
        QueryFormation(){
                                                
        var keys = this.inputForm.value.criteriaArr
        //["EQUALS", "BETWEEN", "LESS THAN", "GREATER THAN", "NOT EQUAL TO", "IS NOTHING", "IS ANYTHING", "IS TRUE", "IS FALSE"]         
          
          
        if(this.conditionentry == true){
          if(this.value.length > 0){
            //console.log(keys)
            switch (keys) {
              case 'IN':
                //console.log("'"+this.value[this.value.length-1].join("','")+"'")
              if( this.value.length == 1){                
                this.sqlcondition = this.ConditionEntity +" in ('"+this.value[this.value.length-1].join("','")+"')"
                this.Savesqlcondition = this.ConditionEntity +" in ('+'''"  + this.value[this.value.length-1] + "'''+')" //'''NSW'''               
              }else{                 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +" like ('"  + this.value[this.value.length-1] + "')"      
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" like (' + '''"  + this.value[this.value.length-1] + "'''+')"
              }
              break;
              case 'EQUALS':
              if( this.value.length == 1){
                this.sqlcondition = this.ConditionEntity +" like ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.ConditionEntity +" like ('+'''"  + this.value[this.value.length-1] + "'''+')" //'''NSW'''
              }else{ 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +" like ('"  + this.value[this.value.length-1] + "')"      
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" like (' + '''"  + this.value[this.value.length-1] + "'''+')"
              }
              
              break;
              case 'BETWEEN':
              if(this.sqlcondition == undefined && this.value.length == 1 && this.Endvalue.length == 1){
                this.sqlcondition = this.ConditionEntity +"  Between ('"  + this.value[this.value.length-1] + "') and ('"  + this.Endvalue[this.Endvalue.length-1] + "')"
                this.Savesqlcondition = this.ConditionEntity +" Between ('+'"  + this.value[this.value.length-1] + "'+') and ('+'" + this.Endvalue[this.Endvalue.length-1] + "'+')"
              }else{ 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +" Between ('"  + this.value[this.value.length-1] + "') and ('"  + this.Endvalue[this.Endvalue.length-1] + "')"
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" Between (' + '"  + this.value[this.value.length-1] + "'+') and (' + '" + this.Endvalue[this.Endvalue.length-1] + "'+')"
              }
              break;
              case 'LESS THAN':
              if(this.sqlcondition == undefined && this.value.length == 1){
                this.sqlcondition = this.ConditionEntity +"  < ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.ConditionEntity +" < ('+'"  + this.value[this.value.length-1] + "'+')"
              }else{ 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +" <  ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" < (' + '"  + this.value[this.value.length-1] + "'+')"
              }
              break;
              case 'GREATER THAN':
              if(this.sqlcondition == undefined && this.value.length == 1){
                this.sqlcondition = this.ConditionEntity +" >  ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.ConditionEntity +" > ('+'"  + this.value[this.value.length-1] + "'+')"
              }else{ 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +"  > ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" > (' + '"  + this.value[this.value.length-1] + "'+')"
              }
              break;
              case 'NOT EQUAL TO':
              if(this.sqlcondition == undefined && this.value.length == 1){
                this.sqlcondition = this.ConditionEntity +" <>  ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.ConditionEntity +" <> ('+'"  + this.value[this.value.length-1] + "'+')"
              }else{ 
                this.sqlcondition = this.sqlcondition + " AND " + this.ConditionEntity +" <>  ('"  + this.value[this.value.length-1] + "')"
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" <> (' + '"  + this.value[this.value.length-1] + "'+')"
              }
              break;
              case 'IS NOTHING':
              if(this.sqlcondition == undefined && this.value.length == 1){
                // this.sqlcondition = this.ConditionEntity +"   ('"  + this.value[this.value.length-1] + "')"
                // this.Savesqlcondition = this.ConditionEntity +" like ('+'"  + this.value[this.value.length-1] + "'+')"
              }else{ 
                //  this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +"   ('"  + this.value[this.value.length-1] + "')"
                //  this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" like (' + '"  + this.value[this.value.length-1] + "'+')"
              }
              break;
              case 'IS ANYTHING':
              if(this.sqlcondition == undefined && this.value.length == 1){
                //  this.sqlcondition = " "//this.ConditionEntity +"   ('"  + this.value[this.value.length-1] + "')"
              }else{ 
                //  this.sqlcondition =this.sqlcondition + " "// " AND " + this.ConditionEntity +"   ('"  + this.value[this.value.length-1] + "')"
                //  this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" like (' + '"  + this.value[this.value.length-1] + "'+')"
              }
              break; 
              case 'IS TRUE':
              if(this.sqlcondition == undefined && this.value.length == 1){
                this.sqlcondition = this.ConditionEntity +"  = true "
                this.Savesqlcondition = this.ConditionEntity +" = true "
              }else{ 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +"  = true "
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" = true "
              }
              break;
              case 'IS FALSE':
              if(this.sqlcondition == undefined && this.value.length == 1){
                this.sqlcondition = this.ConditionEntity +"   = false"
                this.Savesqlcondition = this.ConditionEntity +" = false "
              }else{ 
                this.sqlcondition =this.sqlcondition + " AND " + this.ConditionEntity +"   = false "
                this.Savesqlcondition = this.Savesqlcondition + " AND " +this.ConditionEntity +" = false "
              }
              break;
              default:
                this.sqlcondition = ''
                this.Savesqlcondition = ''
              break;
            }
          }
          
        }                             
        //  this.sqlselect = "Select Distinct SQLID," + this.ColumnNameAdjuster(this.list)//.join(" as Field"+ this.feildname() +", ")
          this.sqlselect = "Select " + this.ColumnNameAdjuster(this.list)                        
          this.sql = this.sqlselect + this.TablesSetting(this.list);          
          this.Saverptsql = this.sqlselect + this.TablesSetting(this.list) ;
          
          
          if(this.sqlcondition != ''){
          if ((this.inputForm.value.radiofiletr).toString() == 'donotmeet') {
            
            if(this.sqlcondition != undefined){  
              this.sql =  this.sql + " where Not " +  this.sqlcondition  ;
              this.Saverptsql = this.Saverptsql +  "  where Not  " + this.Savesqlcondition ;
            }
            
          }else{               
            if(this.sqlcondition != undefined){                           
              this.sql =  this.sql + ' where ' +  this.sqlcondition ;
              this.Saverptsql = this.Saverptsql +  " where " + this.Savesqlcondition ;
            }
          }
          
        }
      
        }
        ShowReport(){
          this.tryDoctype = ""
          this.ReportPreview = true;          
          if(this.needQuerysetting == 1){
          this.QueryFormation();
          }
          if(this.needQuerysetting == 2){
            this.QueryFormation();
            }          
          this.ReportRender(this.sql);  
          this.needQuerysetting = 0;                                                                             
        }
        showprompt(){
          //this.bodystyle = { height:'300px', overflow: 'auto'}
          this.isVisibleprompt = true;
        }
        SaveReport(){
          this.ReportPreview = false;
          this.QueryFormation();     
          this.isVisibleprompt = false;
          //var RptSQL  =  this.QueryFinlization(this.Saverptsql);
          var re = /'/gi;
          var RptSQL  =  this.Saverptsql.replace(re,"~");
          this.RptTitle = (this.inputForm.value.RptTitle).toString();                                    
          let filtertitle = forkJoin([
            this.ReportS.GetReportNames(this.RptFormat.toString()),                                              
          ]);   
          filtertitle.subscribe(data => {           
            this.titleArr = data[0];  
            if(data[0].includes(this.RptTitle)){
              
              this.Filterheck = false;
              
              this.ModalS.warning({
                nzTitle: 'TRACCS',
                nzContent: 'The Report Title Alredy Exists.',
                nzOnOk: () => {
                  this.isVisibleprompt = true;
                  this.Filterheck = true;
                },
              });
            }
          });
          
          
          
          if(this.RptTitle != null && RptSQL != null &&  this.Filterheck == true){
            //var Title  = this.RptTitle;
            var Format  = this.RptFormat;            
            var CriteriaDisplay = "Report Criteria" ;
            var DateType = " ";
            var UserID = this.tocken.nameid;
                        
            var insertsql = " INSERT INTO ReportNames(Title, Format,SQLText,UserID, DateType, CriteriaDisplay) " +
            " VALUES( '" + this.RptTitle +"' , '"+Format+"' , '"+RptSQL+"' , '"+UserID+"' , '"+DateType+"' , '"+CriteriaDisplay + "') "   
                                                
            this.ReportS.InsertReport(insertsql).subscribe(x=>{              
              if(x != null) {
                this. GlobalS.sToast('Success', 'Saved successful'); 
                this.router.navigate(['/admin/reports']);    
              }    
            });    
          }
          
          
        }
        back2adminRpt(){
          this.router.navigate(['/admin/reports']); 
        }
        QueryFinlization(s_sql:string){        
          var S_SQL = " ";
          var sql = " ";
          
          if(this.includeConatctWhere != undefined && this.includeConatctWhere != ""){ sql = sql + " AND " + this.includeConatctWhere}
          if(this.includeGoalcareWhere != undefined && this.includeGoalcareWhere != ""){sql = sql + " AND " + this.includeGoalcareWhere}
          if(this.includeReminderWhere != undefined && this.includeReminderWhere != ""){sql = sql + " AND " + this.includeReminderWhere}
          if(this.includeUserGroupWhere != undefined && this.includeUserGroupWhere != ""){sql = sql + " AND " + this.includeUserGroupWhere}
          if(this.includePrefrencesWhere != undefined && this.includePrefrencesWhere != ""){sql = sql + " AND " + this.includePrefrencesWhere}
          if(this.includeIncludSWhere != undefined && this.includeIncludSWhere != ""){sql = sql + " AND " + this.includeIncludSWhere}
          if(this.includeExcludeSWhere != undefined && this.includeExcludeSWhere != ""){sql = sql + " AND " + this.includeExcludeSWhere}
          if(this.includeRecipientPensionWhere != undefined && this.includeRecipientPensionWhere != ""){sql = sql + " AND " + this.includeRecipientPensionWhere}
          if(this.includeLoanitemWhere != undefined && this.includeLoanitemWhere != ""){sql = sql + " AND " + this.includeLoanitemWhere}
          if(this.includeSvnDetailNotesWhere != undefined && this.includeSvnDetailNotesWhere != ""){sql = sql + " AND " + this.includeSvnDetailNotesWhere}
          if(this.includeSvcSpecCompetencyWhere != undefined && this.includeSvcSpecCompetencyWhere != ""){sql = sql + " AND " + this.includeSvcSpecCompetencyWhere}
          if(this.includeOPHistoryWhere != undefined && this.includeOPHistoryWhere != ""){sql = sql + " AND " + this.includeOPHistoryWhere}
          if(this.includeClinicHistoryWhere != undefined && this.includeClinicHistoryWhere != ""){sql = sql + " AND " + this.includeClinicHistoryWhere}
          if(this.includeRecipientCompetencyWhere != undefined && this.includeRecipientCompetencyWhere != ""){sql = sql + " AND " + this.includeRecipientCompetencyWhere}
          if(this.includeCareplanWhere != undefined && this.includeCareplanWhere != ""){sql = sql + " AND " + this.includeCareplanWhere}
          if(this.includeHRCaseStaffWhere != undefined && this.includeHRCaseStaffWhere != ""){sql = sql + " AND " + this.includeHRCaseStaffWhere}
          if(this.includeNotesWhere != undefined && this.includeNotesWhere != ""){sql = sql + " AND " + this.includeNotesWhere}         
        
        if(this.sqlcondition == undefined && sql != " "){
            S_SQL = s_sql + ' where ' + sql.substring(6,sql.length+1);
          }else{
            S_SQL = s_sql + ' ' + sql
          }        
          if(this.ReportPreview == true){
            return this.sql = S_SQL;
          }else{
            return this.Saverptsql = S_SQL;            
          }
        }
        ReportRender(sql:string){
                    
          //this.loading = true;       
          var fQuery = this.QueryFinlization(this.sql); 
          var Title = "User Defined Report";
          //console.log(fQuery)          
          const data = {
            "template": { "_id": "qTQEyEz8zqNhNgbU" },            
            "options": {
              "reports": { "save": false },
              //"sql": "SELECT DISTINCT R.UniqueID, R.AccountNo, R.AgencyIdReportingCode, R.[Surname/Organisation], R.FirstName, R.Branch, R.RECIPIENT_COORDINATOR, R.AgencyDefinedGroup, R.ONIRating, R.AdmissionDate As [Activation Date], R.DischargeDate As [DeActivation Date], HumanResourceTypes.Address2, RecipientPrograms.ProgramStatus, CASE WHEN RecipientPrograms.Program <> '' THEN RecipientPrograms.Program + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.Quantity <> '' THEN RecipientPrograms.Quantity + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.ItemUnit <> '' THEN RecipientPrograms.ItemUnit + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.PerUnit <> '' THEN RecipientPrograms.PerUnit + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.TimeUnit <> '' THEN RecipientPrograms.TimeUnit + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.Period <> '' THEN RecipientPrograms.Period + ' ' ELSE ' ' END AS FundingDetails, UPPER([Surname/Organisation]) + ', ' + CASE WHEN FirstName <> '' THEN FirstName ELSE ' ' END AS RecipientName, CASE WHEN N1.Address <> '' THEN  N1.Address ELSE N2.Address END  AS ADDRESS, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact END AS CONTACT, (SELECT TOP 1 Date FROM Roster WHERE Type IN (2, 3, 7, 8, 9, 10, 11, 12) AND [Client Code] = R.AccountNo ORDER BY DATE DESC) AS LastDate FROM Recipients R LEFT JOIN RecipientPrograms ON RecipientPrograms.PersonID = R.UniqueID LEFT JOIN HumanResourceTypes ON HumanResourceTypes.Name = RecipientPrograms.Program LEFT JOIN ServiceOverview ON ServiceOverview.PersonID = R.UniqueID LEFT JOIN (SELECT PERSONID,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = R.UniqueID LEFT JOIN (SELECT PERSONID,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress <> 1)  AS N2 ON N2.PersonID = R.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = R.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone <> 1)  AS P2 ON P2.PersonID = R.UniqueID WHERE R.[AccountNo] > '!MULTIPLE'   AND (R.DischargeDate is NULL)  AND  (RecipientPrograms.ProgramStatus = 'REFERRAL')  ORDER BY R.ONIRating, R.[Surname/Organisation]"
              "sql": fQuery,              
              "userid": this.tocken.user,
              "txtTitle": Title,              
            }
          }
          
          //this.loading = true;
          //var Title = "User Defined Report";
          
          if(this.inputForm.value.csvExport == true){                   
            this.ListS.getlist(fQuery).subscribe((blob) => {                
                const headings = Object.keys(blob[0]);                
                let tempArr:Array<any> = [];
                for(let i=0; i < blob.length-1; i++){
                  tempArr = [...tempArr ,blob[i]]               
                }               
                this.downloadFile(tempArr,Title,headings)
            });
          }if(this.inputForm.value.tblView == true){
            var temp;
            this.ModalName = Title;
            //console.log(fQuery)
            temp =  forkJoin([            
                this.ListS.getlist(fQuery)
                ]);    
                temp.subscribe(x => {                     
                    this.tableData  = x[0] 
                    this.headings= Object.keys(this.tableData[0])                                                                         
                    for(let i=0; i <= x[0].length-1; i++){
                        //this.dvalue = [...this.dvalue,Object.values(this.tableData[i])]
                        this.dvalue = [...this.dvalue,this.tableData[i]]               
                    }                     
                    this.selectedGroupColumns = [this.headings[0]];                     
                    this.applyGroupingAndSorting();                       
                
            }) 
        
          }else{  
            this.loading = true;
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = "User Defined Report.pdf";
            this.drawerVisible = true;                   
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
          }, err => {
            console.log(err);
            this.loading = false;
            this.ModalS.error({
              nzTitle: 'TRACCS',
              nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
              nzOnOk: () => {
                this.drawerVisible = false;
              },
            });
          });
        }  
          return;
        }
        
        handleCancelTop(){
          this.drawerVisible = false;
          this.isVisibleprompt = false;
          this.isVisibleprompt_IN = false;
          this.frm_Customlayout= false;
          this.isVisibleRptType= false;
          this.isVisible= false;
        }
        feildname(){
          var tempVar;
          this.rptfieldname =  1;
          tempVar = this.rptfieldname;
          return tempVar
        }
        ColumnNameAdjuster(fld){
          
          var columnNames:Array<any> = [];
          this.FieldsNo = fld.length;          
          if(this.RptFormat == "AGENCYSTFLIST" || this.RptFormat == "USERSTFLIST"){
            for (var key of fld){
              switch (key) {
                //STAFF NAME AND ADDRESS      
                case 'Title':
                if(columnNames.length >= 1){          
                  columnNames = columnNames.concat(['Staff.Title as Title'])
                }else{
                  columnNames = (['Staff.Title as Title'])
                }        
                break;
                case 'First Name':
                if(columnNames.length >= 1){          
                  columnNames = columnNames.concat(['Staff.FirstName as [First Name]'])
                }else{          
                  columnNames = (['Staff.FirstName as [First Name]'])}
                  
                  break;
                  case 'Middle Name':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Staff.MiddleNames as [Middle Name]'])
                  }else{columnNames = (['Staff.MiddleNames as [Middle Name]'])}        
                  break;
                  case 'Surname/Organisation':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Staff.LastName as [Surname/Organisation] '])
                  }else{columnNames = (['Staff.LastName as  [Surname/Organisation]'])}        
                  break;
                  case 'Preferred Name':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Staff.PreferredName as [Preferred Name] '])
                  }else{columnNames = (['Staff.PreferredName as [Preferred Name] '])}        
                  break; 
                  case 'contact Address Line 1':
                  var Address1 = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>')"
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Address1 +'as [Address-Line1]'])
                  }else{columnNames = ([Address1 +'as Address-Line1'])}        
                  break;
                  case 'contact Address Line 2':
                  var Address2 = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Address2 +'as [ Address-Line2]'])
                  }else{columnNames = ([Address2 +'as [ Address-Line2]'])}        
                  break;
                  case 'contact Address-Suburb':
                  var contact_suburb = "(SELECT TOP 1 suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([contact_suburb +'AS [ Address-Suburb]'])
                  }else{columnNames = ([contact_suburb +'AS [ Address-Suburb]'])}        
                  break;
                  case 'contact Address-Postcode':
                  var AddressPostcode = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') " 
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([AddressPostcode +'AS [ Address-Postcode]'])
                  }else{columnNames = ([AddressPostcode +'AS [ Address-Postcode]'])}        
                  break;
                  case 'contact Address-state':
                  var state = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' and '2618'OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<CONTACT>') " 
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([state +'AS [ Address-State]'])
                  }else{columnNames = ([state +'AS [ Address-State]']) }      
                  break;
                  case 'contact Address-GoogleAddress':
                  var googleaddress = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([googleaddress+' AS [ Address-GoogleAddress]'])
                  }else{columnNames = ([googleaddress+' AS [ Address-GoogleAddress]'])}        
                  break;
                  case 'Usual Address Line 1 ':
                  var U_Address1 = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([U_Address1+'AS [ Address-Line1]'])
                  }else{columnNames = ([U_Address1+'AS [ Address-Line1]'])}        
                  break;
                  case 'Usual Address Line 2':
                  var U_Address2 = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([U_Address2+'AS [ Address-Line2]'])
                  }else{columnNames = ([U_Address2+'AS [ Address-Line2]'])}        
                  break;
                  case 'Usual Address-Suburb':
                  var U_Address_Suburb = "(SELECT TOP 1suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([U_Address_Suburb+'AS [ Address-Suburb]'])
                  }else{columnNames = ([U_Address_Suburb+'AS [ Address-Suburb]'])}        
                  break;
                  case 'Usual Address-Postcode  ':
                  var U_Address_postode = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([U_Address_postode+'AS [ Address-Postcode]'])
                  }else{columnNames = ([U_Address_postode+'AS [ Address-Postcode]'])}        
                  break;
                  case 'Usual Address-state':
                  var U_Address_state = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>')"
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([U_Address_state+'AS [ Address-State]'])
                  }else{columnNames = ([U_Address_state+'AS [ Address-State]'])}        
                  break;
                  case 'Usual Address-GoogleAddress':
                  var U_Address_google = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([U_Address_google+'AS [ Address-GoogleAddress]'])
                  }else{columnNames = ([U_Address_google+'AS [ Address-GoogleAddress]'])}        
                  break;    
                  case 'Billing Address Line 1':
                  var B_Address1 = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([B_Address1+'AS [BILLING ADDRESS Address-Line1]'])
                  }else{columnNames = ([B_Address1+'AS [BILLING ADDRESS Address-Line1]'])}        
                  break;
                  case 'Billing Address Line 2':
                  var B_Address2 = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([B_Address2+'AS [BILLING ADDRESS Address-Line2]'])
                  }else{columnNames = ([B_Address2+'AS [BILLING ADDRESS Address-Line2]'])}        
                  break;
                  case 'Billing Address-Suburb':
                  var B_Address_Suburb = "(SELECT TOP 1 suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([B_Address_Suburb+'AS [BILLING ADDRESS Address-Suburb]'])
                  }else{columnNames = ([B_Address_Suburb+'AS [BILLING ADDRESS Address-Suburb]'])}        
                  break;
                  case 'Billing Address-Postcode':
                  var B_Address_postcode = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([B_Address_postcode+'AS [BILLING ADDRESS Address-Postcode]'])
                  }else{columnNames = ([B_Address_postcode+'AS [BILLING ADDRESS Address-Postcode]'])}        
                  break;
                  case 'Billing Address-state':
                  var B_Address_State = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([B_Address_State+'AS [BILLING ADDRESS Address-State]'])
                  }else{columnNames = ([B_Address_State+'AS [BILLING ADDRESS Address-State]'])}        
                  break;
                  case 'Billing Address-GoogleAddress':
                  var B_Address_Google = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'BILLING ADDRESS') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([B_Address_Google+'AS [BILLING ADDRESS Address-GoogleAddress]'])
                  }else{columnNames = ([B_Address_Google+'AS [BILLING ADDRESS Address-GoogleAddress]'])}        
                  break;
                  case 'Destination Address Line 1':
                  var D_Address1 = "(SELECT TOP 1 address1 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([D_Address1+'AS [DESTINATION Address-Line1]'])
                  }else{columnNames = ([D_Address1+'AS [DESTINATION Address-Line1]'])}        
                  break;
                  case 'Destination Address Line 2':
                  var D_Address2 = "(SELECT TOP 1 address2 FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([D_Address2+'AS [DESTINATION Address-Line2]'])
                  }else{columnNames = ([D_Address2+'AS [DESTINATION Address-Line2]'])}        
                  break;
                  case 'Destination Address-Suburb ':
                  var D_suburb = "(SELECT TOP 1 suburb FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([D_suburb+'AS [DESTINATION Address-Suburb]'])
                  }else{columnNames = ([D_suburb+'AS [DESTINATION Address-Suburb]'])}        
                  break;
                  case 'Destination Address-Postcode':
                  var D_Address_postcode = "(SELECT TOP 1 postcode FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([D_Address_postcode+'AS [DESTINATION Address-Postcode]'])
                  }else{columnNames = ([D_Address_postcode+'AS [DESTINATION Address-Postcode]'])}        
                  break;
                  case 'Destination Address-state':
                  var D_Address_State = "(SELECT TOP 1 CASE WHEN LEFT(postcode, 1) = '0' THEN 'NT' WHEN LEFT(postcode, 1) = '2' THEN CASE WHEN postcode BETWEEN '2600' AND    '2618' OR     postcode BETWEEN '2900' AND    '2999' THEN 'ACT' ELSE 'NSW' END WHEN LEFT(postcode, 1) = '3' THEN 'VIC' WHEN LEFT(postcode, 1) = '4' THEN 'QLD' WHEN LEFT(postcode, 1) = '5' THEN 'SA' WHEN LEFT(postcode, 1) = '6' THEN 'WA' WHEN LEFT(postcode, 1) = '7' THEN 'TAS' END AS primarystate FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([D_Address_State+'AS [DESTINATION Address-State]'])
                  }else{columnNames = ([D_Address_State+'AS [DESTINATION Address-State]'])}        
                  break;
                  case 'Destination Address-GoogleAddress':
                  var D_Address_google = "(SELECT TOP 1 googleaddress FROM   namesandaddresses WHERE  personid = uniqueid AND    [Type] = 'PERSONALADDRESS' AND    description = 'DESTINATION') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([D_Address_google+'AS [DESTINATION Address-GoogleAddress]'])
                  }else{columnNames = ([D_Address_google+'AS [DESTINATION Address-GoogleAddress]'])}        
                  break;
                  case 'Email':
                  var Email = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<EMAIL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Email+'AS [Email]'])
                  }else{columnNames = ([Email+'AS [Email]'])}        
                  break;      
                  case 'Email-SMS':
                  var Email_sms = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<EMAIL-SMS>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Email_sms+'AS [Email-SMS]'])
                  }else{columnNames = ([Email_sms+'AS [Email-SMS]'])}        
                  break;
                  case 'FAX':
                  var fax = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<FAX>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([fax+'AS [FAX]'])
                  }else{columnNames = ([fax+'AS [FAX]'])}        
                  break;
                  case 'Home Phone':
                  var H_Phone = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<HOME>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([H_Phone+'AS [Home-Phone]'])
                  }else{columnNames = ([H_Phone+'AS [Home-Phone]'])}        
                  break;
                  case 'Mobile Phone':
                  var Mobile_Phone = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<MOBILE>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Mobile_Phone+'AS [Mobile-Phone]'])
                  }else{columnNames = ([Mobile_Phone+'AS [Mobile-Phone]'])}        
                  break;
                  case 'Usual Phone':
                  var Usual_Phone = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<USUAL>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Usual_Phone+'AS [Usual-Phone]'])
                  }else{columnNames = ([Usual_Phone+'AS [Usual-Phone]'])}        
                  break;
                  case 'Work Phone':
                  var Work_Phone = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = '<WORK>') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Work_Phone+'AS [Work-Phone]'])
                  }else{columnNames = ([Work_Phone+'AS [Work-Phone]'])}        
                  break;
                  case 'Current Phone Number':
                  var Current_phone = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = 'CURRENT PHONE NUMBER' ) "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([Current_phone+'AS [CURRENT PHONE NUMBER]'])
                  }else{columnNames = ([Current_phone+'AS [CURRENT PHONE NUMBER]'])}        
                  break;
                  case 'Other Phone Number':
                  var other_phone = "(SELECT TOP 1 phonefaxother.detail FROM   phonefaxother WHERE  personid = uniqueid AND    phonefaxother.type = 'OTHER PHONE NUMBER') "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([other_phone+'AS [OTHER PHONE NUMBER]'])
                  }else{columnNames = ([other_phone+'AS [OTHER PHONE NUMBER]'])}        
                  break;                                    
                  //  Contacts & Next of Kin
                  case 'Contact Group':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Group]   '])
                  }else{columnNames = (['HR.[Group]   '])}
                  break;
                  case 'Contact Type':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Type]    '])
                  }else{columnNames = (['HR.[Type]   '])} 
                  break;
                  case 'Contact Sub Type':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[SubType]  '])
                  }else{columnNames = (['HR.[SubType]  '])}  
                  break;
                  case 'Contact User Flag':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[User1]  '])
                  }else{columnNames = (['HR.[User1]  '])}
                  break;                 
                  case 'Contact Person Type':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[EquipmentCode]  '])
                  }else{columnNames = (['HR.[EquipmentCode]  '])} 
                  break;
                  case 'Contact Name':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Name]  '])
                  }else{columnNames = (['HR.[Name]  '])} 
                  break;
                  case 'Contact Address':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Address1]  '])
                  }else{columnNames = (['HR.[Address1]  '])} 
                  break;
                  case 'Contact Suburb':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Suburb]  '])
                  }else{columnNames = (['HR.[Suburb]  '])}
                  break;
                  case 'Contact Postcode':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Postcode]  '])
                  }else{columnNames = (['HR.[Postcode]  '])} 
                  break;
                  case 'Contact Phone 1':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Phone1] '])
                  }else{columnNames = (['HR.[Phone1] '])} 
                  break;
                  case 'Contact Phone 2':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Phone2]  '])
                  }else{columnNames = (['HR.[Phone2]  '])} 
                  break;
                  case 'Contact Mobile': 
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Mobile]  '])
                  }else{columnNames = (['HR.[Mobile]  '])}
                  break;
                  case 'Contact FAX':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[FAX]  '])
                  }else{columnNames = (['HR.[FAX]  '])}
                  break;
                  case 'Contact Email':
                  this.IncludeContacts = true
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HR.[Email]  '])
                  }else{columnNames = (['HR.[Email]  '])}
                  break;
                  // USER GROUPS                     
                  case 'Group Name':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['UserGroup.[Name]  '])
                  }else{columnNames = (['UserGroup.[Name]  '])}  
                  break;
                  case 'Group Note':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['UserGroup.[Notes]  '])
                  }else{columnNames = (['UserGroup.[Notes]  '])} 
                  break; 
                  case 'Group Start Date':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['UserGroup.[Date1] As [Group Start Date] '])
                  }else{columnNames = (['UserGroup.[Date1] as [Group Start Date] '])}  
                  break;                      
                  case 'Group End Date':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['UserGroup.[Date2] As [Group End Date] '])
                  }else{columnNames = (['UserGroup.[Date2] [Group End Date] '])} 
                  break;
                  case 'Group Email':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['UserGroup.[Email]  '])
                  }else{columnNames = (['UserGroup.[Email]  '])} 
                  break;
                  
                  //Preferences                                        
                  case 'Preference Name':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Prefr.[Name]  '])
                  }else{columnNames = (['Prefr.[Name]  '])}  
                  break;                      
                  case 'Preference Note':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Prefr.[Notes]  '])
                  }else{columnNames = (['Prefr.[Notes]  '])}  
                  break;
                  //REMINDERS                      
                  case 'Reminder Detail':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Remind.[Name]  '])
                  }else{columnNames = (['Remind.[Name]  '])}
                  break;
                  case 'Event Date':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Remind.[Date2] as [Event Date] '])
                  }else{columnNames = (['Remind.[Date2] as [Event Date] '])}  
                  break;
                  
                  case 'Reminder Date':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Remind.[Date1] as [Reminder Date] '])
                  }else{columnNames = (['Remind.[Date1] as [Reminder Date] '])}  
                  break;
                  case 'Reminder Notes':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Remind.[Notes] '])
                  }else{columnNames = (['Remind.[Notes] '])}  
                  break;
                  //Leaves
                  case 'Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLeave.[Name] '])
                    }else{columnNames = (['HRLeave.[Name] '])}  
                    break;
                case 'Approved Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLeave.[completed] '])
                    }else{columnNames = (['HRLeave.[completed] '])}  
                    break; 
                case 'Leave Reminder Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLeave.[DateInstalled] '])
                    }else{columnNames = (['HRLeave.[DateInstalled] '])}  
                    break; 
                case 'Leave Start Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLeave.[Date1] '])
                    }else{columnNames = (['HRLeave.[Date1] '])}  
                    break; 
                case 'Leave End Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLeave.[Date2] '])
                    }else{columnNames = (['HRLeave.[Date2] '])}  
                    break; 
                  //Loan Items                      
                  case 'Loan Item Type':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HRLoan.[Type] '])
                  }else{columnNames = (['HRLoan.[Type] '])}
                  break;
                  case 'Loan Item Description':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HRLoan.[Name] '])
                  }else{columnNames = (['HRLoan.[Name] '])}
                  break;
                  case 'Loan Item Date Loaned/Installed':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HRLoan.[Date1] as [Loan Item Installed] '])
                  }else{columnNames = (['HRLoan.[Date1] as [Loan Item Installed] '])}
                  break;                      
                  case 'Loan Item Date Collected':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['HRLoan.[Date2] as [Loan Item Collected Date] '])
                  }else{columnNames = (['HRLoan.[Date2] as [Loan Item Collected Date] '])}
                  break;
                  //  service information Fields                      
                  case 'Staff Code':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Carer Code]  '])
                  }else{columnNames = (['SvcDetail.[Carer Code]  '])}
                  break;
                  case 'Service Date':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.Date  '])
                  }else{columnNames = (['SvcDetail.Date  '])}
                  break;
                  case 'Service Start Time':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Start Time]  '])
                  }else{columnNames = (['SvcDetail.[Start Time]  '])}
                  break;
                  case 'Service Code':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Service Type]  '])
                  }else{columnNames = (['SvcDetail.[Service Type]  '])}
                  break;                      
                  case 'Service Hours':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['(SvcDetail.[Duration]*5) / 60  '])
                  }else{columnNames = (['(SvcDetail.[Duration]*5) / 60  '])}
                  break;
                  case 'Service Pay Rate':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Unit Pay Rate]  '])
                  }else{columnNames = (['SvcDetail.[Unit Pay Rate]  '])}
                  break;
                  case 'Service Bill Rate':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Unit Bill Rate]  '])
                  }else{columnNames = (['SvcDetail.[Unit Bill Rate]  '])}
                  break;
                  case 'Service Bill Qty':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[BillQty]  '])
                  }else{columnNames = (['SvcDetail.[BillQty]  '])}
                  break;
                  case 'Service Location/Activity Group':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[ServiceSetting]  '])
                  }else{columnNames = (['SvcDetail.[ServiceSetting]  '])}
                  break;
                  case 'Service Program':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Program]  '])
                  }else{columnNames = (['SvcDetail.[Program]  '])}
                  break;
                  case 'Service Group':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Type]  '])
                  }else{columnNames = (['SvcDetail.[Type]  '])}
                  break;
                  case 'Service HACC Type': 
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[HACCType]  '])
                  }else{columnNames = (['SvcDetail.[HACCType]  '])}
                  break;
                  case 'Service Category':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Anal]  '])
                  }else{columnNames = (['SvcDetail.[Anal]  '])}
                  break;
                  case 'Service Status':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Status]  '])
                  }else{columnNames = (['SvcDetail.[Status]  '])}
                  break;
                  case 'Service Pay Type':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[Service Description]  '])
                  }else{columnNames = (['SvcDetail.[Service Description]  '])}
                  break;
                  case 'Service Pay Qty':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['SvcDetail.[CostQty]  '])
                  }else{columnNames = (['SvcDetail.[CostQty]  '])}
                  break;
                  case 'Service End Time/ Shift End Time':
                  var endtime = " Convert(varchar,DATEADD(minute,(SvcDetail.[Duration]*5) ,SvcDetail.[Start Time]),108) "
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat([endtime+'  '])
                  }else{columnNames = ([endtime+'  '])}
                  break;
                  case 'Service Funding Source':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Humanresourcetypes.[Type]  '])
                  }else{columnNames = (['Humanresourcetypes.[Type]  '])}
                  break;  
                  case 'Service Notes':
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['CAST(History.Detail AS varchar(4000))  '])
                  }else{columnNames = (['CAST(History.Detail AS varchar(4000))  '])}
                  break;
                  //Staff Position
                  case 'Staff Position':                   
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['StaffPosition.[Name]  '])
                  }else{columnNames = (['StaffPosition.[Name]  '])}   
                    break;           
                    case 'Position Start Date':                    
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['StaffPosition.[Start Date]  '])
                  }else{columnNames = (['StaffPosition.[Start Date]  '])}  
                    break;
                    case 'Position End Date':                    
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['StaffPosition.[End Date]  '])
                  }else{columnNames = (['StaffPosition.[End Date]  '])}  
                    break;
                    case 'Position ID':                   
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['StaffPosition.[Position ID]  '])
                  }else{columnNames = (['StaffPosition.[Position ID]  '])}  
                    break;
                    case 'Position Notes':                   
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['StaffPosition.[Notes]  '])
                  }else{columnNames = (['StaffPosition.[Notes]  '])}  
                    break;7
                    //Work Hours
                    case 'MIN_DAILY_HRS':                    
                  if(columnNames.length >= 1){
                    columnNames = columnNames.concat(['Staff.HRS_DAILY_MIN  '])
                  }else{columnNames = (['Staff.HRS_DAILY_MIN  '])}  
                    break;
                    case 'MAX_DAILY_HRS':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.HRS_DAILY_MAX  '])
                      }else{columnNames = (['Staff.HRS_DAILY_MAX  '])}
                    break;
                    case 'MIN_WEEKLY_HRS':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.HRS_WEEKLY_MIN  '])
                      }else{columnNames = (['Staff.HRS_WEEKLY_MIN  '])}
                    break;
                    case 'MAX_WEEKLY_HRS':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.HRS_WEEKLY_MAX  '])
                      }else{columnNames = (['Staff.HRS_WEEKLY_MAX  '])}
                    break;
                    case 'MIN_PAY_PERIOD_HRS':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.HRS_FNIGHTLY_MIN  '])
                      }else{columnNames = (['Staff.HRS_FNIGHTLY_MIN  '])}
                    break;
                    case 'MAX_PAY_PERIOD_HRS':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.HRS_FNIGHTLY_MAX  '])
                      }else{columnNames = (['Staff.HRS_FNIGHTLY_MAX  '])}
                    break;
                    case 'WEEK_1_DAY_1':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_1_1  '])
                      }else{columnNames = (['Staff.CH_1_1  '])}
                    break;
                    case 'WEEK_1_DAY_2':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_1_2  '])
                      }else{columnNames = (['Staff.CH_1_2  '])}
                    break;
                    case 'WEEK_1_DAY_3':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_1_3  '])
                      }else{columnNames = (['Staff.CH_1_3  '])}
                    break;
                    case 'WEEK_1_DAY_4':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_1_4  '])
                      }else{columnNames = (['Staff.CH_1_4  '])}
                    break;
                    case 'WEEK_1_DAY_5':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_1_5  '])
                      }else{columnNames = (['Staff.CH_1_5  '])}
                    break;
                    case 'WEEK_1_DAY_6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Staff.CH_1_6  '])
                    }else{columnNames = (['Staff.CH_1_6  '])}
                    break;
                    case 'WEEK_1_DAY_7':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_1_7  '])
                      }else{columnNames = (['Staff.CH_1_7  '])}
                    break;
                    case 'WEEK_2_DAY_1':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_2_1  '])
                      }else{columnNames = (['Staff.CH_2_1  '])}
                    break;
                    case 'WEEK_2_DAY_2':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_2_2  '])
                      }else{columnNames = (['Staff.CH_2_2  '])}
                    break;
                    case 'WEEK_2_DAY_3':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Staff.CH_2_3  '])
                    }else{columnNames = (['Staff.CH_2_3  '])}
                    break;
                    case 'WEEK_2_DAY_4':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_2_4  '])
                      }else{columnNames = (['Staff.CH_2_4  '])}
                    break;
                    case 'WEEK_2_DAY_5':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_2_5  '])
                      }else{columnNames = (['Staff.CH_2_5  '])}
                    break;
                    case 'WEEK_2_DAY_6':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_2_6  '])
                      }else{columnNames = (['Staff.CH_2_6  '])}
                    break;
                    case 'WEEK_2_DAY_7':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['Staff.CH_2_7  '])
                      }else{columnNames = (['Staff.CH_2_7  '])}
                    break;
                    //Staff Incident 
                    case 'INCD_Status' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Status]  '])
                        }else{columnNames = (['StaffIncidents.[Status]  '])} 
                    case 'INCD_Date' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Date]  '])
                        }else{columnNames = (['StaffIncidents.[Date]  '])} 
                        break;
                    case 'INCD_Type' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Type]  '])
                        }else{columnNames = (['StaffIncidents.[Type]  '])} 
                        break;
                    case 'INCD_Description' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[ShortDesc]  '])
                        }else{columnNames = (['StaffIncidents.[ShortDesc]  '])} 
                        break;
                    case 'INCD_SubCategory' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[PerpSpecify]  '])
                        }else{columnNames = (['StaffIncidents.[PerpSpecify]  '])} 
                        break;
                    case 'INCD_Assigned_To' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[CurrentAssignee]  '])
                        }else{columnNames = (['StaffIncidents.[CurrentAssignee]  '])} 
                        break;
                    case 'INCD_Service' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Service]  '])
                        }else{columnNames = (['StaffIncidents.[Service]  '])} 
                        break;
                    case 'INCD_Severity' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Severity]  '])
                        }else{columnNames = (['StaffIncidents.[Severity]  '])} 
                        break;
                    case 'INCD_Time' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Time]  '])
                        }else{columnNames = (['StaffIncidents.[Time]  '])} 
                        break;
                    case 'INCD_Duration' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Duration]  '])
                        }else{columnNames = (['StaffIncidents.[Duration]  '])} 
                        break;
                    case 'INCD_Location' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Location]  '])
                        }else{columnNames = (['StaffIncidents.[Location]  '])} 
                        break;
                    case 'INCD_LocationNotes' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[LocationNotes]  '])
                        }else{columnNames = (['StaffIncidents.[LocationNotes]  '])} 
                        break;
                    case 'INCD_ReportedBy' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[ReportedBy]  '])
                        }else{columnNames = (['StaffIncidents.[ReportedBy]  '])} 
                        break;
                    case 'INCD_DateReported' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[DateReported]  '])
                        }else{columnNames = (['StaffIncidents.[DateReported]  '])} 
                        break;
                    case 'INCD_Reported' : //NEED TO CHECK
                        this.ConditionEntity =  " CASE WHEN StaffIncidents.Reported = 1 THEN 'YES' ELSE 'NO' END AS [INCD_Reported ]"
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['  '])
                        }else{columnNames = (['  '])} 
                        break;
                    case 'INCD_FullDesc' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[FullDesc]  '])
                        }else{columnNames = (['StaffIncidents.[FullDesc]  '])} 
                        break;
                    case 'INCD_Program' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Program]  '])
                        }else{columnNames = (['StaffIncidents.[Program]  '])} 
                        break;
                    case 'INCD_DSCServiceType' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[DSCServiceType]  '])
                        }else{columnNames = (['StaffIncidents.[DSCServiceType]  '])} 
                        break;
                    case 'INCD_TriggerShort' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[TriggerShort]  '])
                        }else{columnNames = (['StaffIncidents.[TriggerShort]  '])} 
                        break;
                    case 'INCD_incident_level' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[incident_level]  '])
                        }else{columnNames = (['StaffIncidents.[incident_level]  '])} 
                        break;
                    case 'INCD_Area' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[area]  '])
                        }else{columnNames = (['StaffIncidents.[area]  '])} 
                        break;
                    case 'INCD_Region' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Region]  '])
                        }else{columnNames = (['StaffIncidents.[Region]  '])} 
                        break;
                    case 'INCD_position' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[position]  '])
                        }else{columnNames = (['StaffIncidents.[position]  '])} 
                        break;
                    case 'INCD_phone' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[phone]  '])
                        }else{columnNames = (['StaffIncidents.[phone]  '])} 
                        break;
                    case 'INCD_verbal_date' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[verbal_date]  '])
                        }else{columnNames = (['StaffIncidents.[verbal_date]  '])} 
                        break;
                    case 'INCD_verbal_time' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[verbal_time]  '])
                        }else{columnNames = (['StaffIncidents.[verbal_time]  '])} 
                        break;
                    case 'INCD_By_Whome' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[By_Whome]  '])
                        }else{columnNames = (['StaffIncidents.[By_Whome]  '])} 
                        break;
                    case 'INCD_To_Whome' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[To_Whome]  '])
                        }else{columnNames = (['StaffIncidents.[To_Whome]  '])} 
                        break;
                    case 'INCD_BriefSummary' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[BriefSummary]  '])
                        }else{columnNames = (['StaffIncidents.[BriefSummary]  '])} 
                        break;
                    case 'INCD_ReleventBackground' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[ReleventBackground]  '])
                        }else{columnNames = (['StaffIncidents.[ReleventBackground]  '])} 
                        break;
                    case 'INCD_SummaryofAction' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[SummaryofAction]  '])
                        }else{columnNames = (['StaffIncidents.[SummaryofAction]  '])} 
                        break;
                    case 'INCD_SummaryOfOtherAction' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[SummaryOfOtherAction]  '])
                        }else{columnNames = (['StaffIncidents.[SummaryOfOtherAction]  '])} 
                        break;
                    case 'INCD_Triggers' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Triggers]  '])
                        }else{columnNames = (['StaffIncidents.[Triggers]  '])} 
                        break;
                    case 'INCD_InitialAction' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[InitialAction]  '])
                        }else{columnNames = (['StaffIncidents.[InitialAction]  '])} 
                        break;
                    case 'INCD_InitialNotes' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[InitialNotes]  '])
                        }else{columnNames = (['StaffIncidents.[InitialNotes]  '])} 
                        break;
                    case 'INCD_InitialFupBy' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[InitialFupBy]  '])
                        }else{columnNames = (['StaffIncidents.[InitialFupBy]  '])} 
                        break;
                    case 'INCD_Completed' : //NEED TO CHECK
                        this.ConditionEntity =  " CASE WHEN StaffIncidents.Completed = 1 THEN 'YES' ELSE 'NO' END AS [INCD_Completed ]"
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['  '])
                        }else{columnNames = (['  '])} 
                        break;
                    case 'INCD_OngoingAction' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[OngoingAction]  '])
                        }else{columnNames = (['StaffIncidents.[OngoingAction]  '])} 
                        break;
                    case 'INCD_OngoingNotes' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[OngoingNotes]  '])
                        }else{columnNames = (['StaffIncidents.[OngoingNotes]  '])} 
                        break;
                    case 'INCD_Background' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Background]  '])
                        }else{columnNames = (['StaffIncidents.[Background]  '])} 
                        break;
                    case 'INCD_Abuse' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Abuse]  '])
                        }else{columnNames = (['StaffIncidents.[Abuse]  '])} 
                        break;
                    case 'INCD_DOPWithDisability' : //NEED TO CHECK DOPWithDisability
                        this.ConditionEntity =  " CASE WHEN StaffIncidents.INCD_DOPWithDisability = 1 THEN 'YES' ELSE 'NO' END AS [INCD_DOPWithDisability ]"
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['  '])
                        }else{columnNames = (['  '])} 
                        break;
                    case 'INCD_SeriousRisks' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[SeriousRisks]  '])
                        }else{columnNames = (['StaffIncidents.[SeriousRisks]  '])} 
                        break;
                    case 'INCD_Complaints' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Complaints]  '])
                        }else{columnNames = (['StaffIncidents.[Complaints]  '])} 
                        break;
                    case 'INCD_Perpetrator' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Perpetrator]  '])
                        }else{columnNames = (['StaffIncidents.[Perpetrator]  '])} 
                        break;
                    case 'INCD_Notify' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Notify]  '])
                        }else{columnNames = (['StaffIncidents.[Notify]  '])} 
                        break;
                    case 'INCD_NoNotifyReason' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[NoNotifyReason]  '])
                        }else{columnNames = (['StaffIncidents.[NoNotifyReason]  '])} 
                        break;
                    case 'INCD_Notes' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Notes]  '])
                        }else{columnNames = (['StaffIncidents.[Notes]  '])} 
                        break;
                    case 'INCD_Setting' :
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[Setting]  '])
                        }else{columnNames = (['StaffIncidents.[Setting]  '])} 
                        break;
                        /*
                    case 'INCD_CreatedBy':
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[INCD_CreatedBy]  '])
                        }else{columnNames = (['StaffIncidents.[INCD_CreatedBy]  '])} 
                        break;
                    case 'INCD_Involved_Staff':
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['StaffIncidents.[INCD_Involved_Staff]  '])
                        }else{columnNames = (['StaffIncidents.[INCD_Involved_Staff]  '])} 
                        break; */                     
                      //STAFF OP NOTES
                    case 'General Notes':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['STAFF.[STF_NOTES]  '])
                      }else{columnNames = (['STAFF.[STF_NOTES]  '])}  
                      break;
                    case 'OP Notes Date':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['OPHistory.[DetailDate]  '])
                      }else{columnNames = (['OPHistory.[DetailDate]  '])}  
                      break;
                    case 'OP Notes Detail':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['OPHistory.[Detail]  '])
                      }else{columnNames = (['OPHistory.[Detail]  '])}  
                    case 'OP Notes Creator':
                      if(columnNames.length >= 1){
                        columnNames = columnNames.concat(['OPHistory.[Creator]  '])
                      }else{columnNames = (['OPHistory.[Creator]  '])}  
                      break;
                    case 'OP Notes Alarm':
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['OPHistory.[AlarmDate]  '])
                        }else{columnNames = (['OPHistory.[AlarmDate]  '])}  
                      break;                        
                    case 'OP Notes Category': //need to adjust
                        this.ConditionEntity =  " CASE WHEN IsNull(OPHistory.ExtraDetail2,'') < 'A' THEN 'UNKNOWN' ELSE OPHistory.ExtraDetail2 END AS [OP Notes Category],"                
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['  '])
                        }else{columnNames = (['  '])}
                        break;  
                      //STAFF HR NOTES
                      case 'HR Notes Date':
                        if(columnNames.length >= 1){
                          columnNames = columnNames.concat(['HRHistory.[DetailDate]  '])
                        }else{columnNames = (['HRHistory.[DetailDate]  '])}  
                          break;
                      case 'HR Notes Detail':
                          if(columnNames.length >= 1){
                            columnNames = columnNames.concat(['HRHistory.[Detail]  '])
                          }else{columnNames = (['HRHistory.[Detail]  '])}  
                          break;
                      case 'HR Notes Creator':
                          if(columnNames.length >= 1){
                            columnNames = columnNames.concat(['HRHistory.[Creator]  '])
                          }else{columnNames = (['HRHistory.[Creator]  '])}  
                          break;
                      case 'HR Notes Alarm':
                          if(columnNames.length >= 1){
                            columnNames = columnNames.concat(['HRHistory.[AlarmDate]  '])
                          }else{columnNames = (['HRHistory.[AlarmDate]  '])}  
                          break;
                      case 'HR Notes Categories':
                          if(columnNames.length >= 1){
                            columnNames = columnNames.concat(['HRHistory.[ExtraDetail2]  '])
                          }else{columnNames = (['HRHistory.[ExtraDetail2]  '])}  
                          break;
                          //Staff Attribute
                          case 'Competency': 
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat(['StaffAttribute.[Name] '])
                            }else{columnNames = ([' StaffAttribute.[Name] '])} 
                          break;
                          case 'Competency Expiry Date':  
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat([' StaffAttribute.[Date1] '])
                            }else{columnNames = (['StaffAttribute.[Date1] '])} 
                          break;
                          case 'Competency Reminder Date':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat(['StaffAttribute.[Date2]  '])
                            }else{columnNames = ([ 'StaffAttribute.[Date2] '])} 
                          break;
                          case 'Competency Completion Date':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat(['StaffAttribute.[DateInstalled]  '])
                            }else{columnNames = ([' StaffAttribute.[DateInstalled] '])} 
                          break;
                          case 'Certificate Number':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat([' StaffAttribute.[Address1] '])
                            }else{columnNames = ([' StaffAttribute.[Address1] '])} 
                          break;
                          case 'Mandatory Status':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat([' StaffAttribute.[Recurring] '])
                            }else{columnNames = ([' StaffAttribute.[Recurring] '])} 
                          break;
                          case 'Competency Notes':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat(['  '])
                            }else{columnNames = (['  '])} 
                          break;
                          case 'Staff Position':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat([' StaffPosition.[Name] '])
                            }else{columnNames = ([' StaffPosition.[Name] '])} 
                          break;
                          case 'Staff Admin Categories':
                          if(columnNames.length >= 1){
                          columnNames = columnNames.concat([' Staff.[SubCategory] '])
                          }else{columnNames = ([' Staff.[SubCategory] '])} 
                          break;
                          case 'NDIA Staff Level':
                          if(columnNames.length >= 1){
                              columnNames = columnNames.concat([' Staff.[NDIAStaffLevel] '])
                            }else{columnNames = ([' Staff.[NDIAStaffLevel] '])} 
                          break;
                    

 


                  
                  
                  
                  
                }
              }
              
            }else{
              for (var key of fld){
                switch (key) {
                  //NAME AND ADDRESS 
                  case 'First Name':
                  if(columnNames.length >= 1){          
                    columnNames = columnNames.concat(['R.FirstName as [First Name]'])
                  }else{          
                    columnNames = (['R.FirstName as [First Name]'])}
                    
                    break;
                    case 'Title':
                    if(columnNames.length >= 1){          
                      columnNames = columnNames.concat(['R.title as Title'])
                    }else{
                      columnNames = (['R.title as Title'])
                    }        
                    break;
                    case 'Surname/Organisation':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Surname/Organisation] as [Surname/Organisation] '])
                    }else{columnNames = (['R.[Surname/Organisation] as  [Surname/Organisation]'])}        
                    break;
                    case 'Preferred Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.PreferredName as [Preferred Name] '])
                    }else{columnNames = (['R.PreferredName as [Preferred Name] '])}        
                    break;                  
                    case 'Other':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.title as Other '])
                    }else{columnNames = (['R.title as Other '])}        
                    break;
                    case 'Address-Line1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Address1 as [Address-Line1] '])
                    }else{columnNames = (['R.Address1 as  [Address-Line1]'])}        
                    break;
                    case 'Address-Line2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Address2 as [Address-Line2] '])
                    }else{columnNames = (['R.Address2 as  [Address-Line2]'])}        
                    break;
                    case 'Address-Suburb':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Suburb as  [Address-Suburb]'])
                    }else{columnNames = (['R.Suburb as  [Address-Suburb]'])}        
                    break;
                    case 'Address-Postcode':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Postcode as  [Address-Postcode]'])
                    }else{columnNames = (['R.Postcode as [Address-Postcode] '])}        
                    break;
                    case 'Address-State':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.State as  [Address-State]'])
                    }else{columnNames = (['R.State as  [Address-State]'])}        
                    break;
                    //General Demographics          
                    case 'Full Name-Surname First':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['(R.[Surname/Organisation]' + ' + ' +'R.FirstName) as  [Full Name]'])
                    }else{columnNames = (['(R.[Surname/Organisation]' + ' + ' +'R.FirstName) as  [Full Name]'])}        
                    break;
                    case 'Full Name-Mailing':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['(R.[Surname/Organisation]' + ' + ' +'R.FirstName) as  [Full Name-Mailing]'])
                    }else{columnNames = (['(R.[Surname/Organisation]' + ' + ' +'R.FirstName) as  [Full Name-Mailing]'])}        
                    break;
                    case 'Gender':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Gender as [Gender] '])
                    }else{columnNames = (['R.Gender as [Gender] '])}        
                    break;
                    case 'Date Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(["FORMAT(convert(datetime,R.DateOfBirth), 'dd/MM/yyyy')  as [DOB] "])
                    }else{columnNames = (['R.DateOfBirth as [DOB] '])}        
                    break;
                    case 'Age':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' DateDiff(YEAR,R.Dateofbirth,GetDate()) as [Age] '])
                    }else{columnNames = ([' DateDiff(YEAR,R.Dateofbirth,GetDate()) as  [Age]'])}        
                    break;
                    case 'Ageband-Statistical':
                    var AgebandStatic = " CASE " +
                    "case WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN 0 AND 5 THEN ' 0- 5' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN 6 AND 13 THEN ' 6-13' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) BETWEEN 14 AND 17 THEN '14-17' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) BETWEEN 18 AND 45 THEN '18-45' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) BETWEEN 46 AND 65 THEN '46-65' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) BETWEEN 66 AND 70 THEN '66-70' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) BETWEEN 71 AND 75 THEN '71-75' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) BETWEEN 76 AND 80 THEN '76-80' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN 81 AND 90 THEN '81-90' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()   ) > 90 THEN 'OVER 90' "+
                    "ELSE 'UNKNOWN' END  "
                    
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AgebandStatic+' as  [Ageband-Statistical]'])
                    }else{columnNames = ([AgebandStatic+' as  [Ageband-Statistical]'])}        
                    break;
                    case 'Ageband-5 Year':
                    var Ageband5 =" CASE " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN 0 AND 5 THEN ' 0- 5' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  6 AND 10 THEN ' 6-10' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  11 AND 15 THEN '11-15' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  16 AND 20 THEN '16-20' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  21 AND 25 THEN '21-25' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  26 AND 30 THEN '26-30' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  36 AND 40 THEN '36-40' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  41 AND 45 THEN '41-45' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  46 AND 50 THEN '46-50' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  51 AND 55 THEN '51-55' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  56 AND 60 THEN '56-60' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  61 AND 65 THEN '61-65' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  66 AND 70 THEN '66-70' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  71 AND 75 THEN '71-75' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() ) BETWEEN 76 AND 80 THEN '76-80' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  81 AND 85 THEN '81-85' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  86 AND 90 THEN '86-90' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  91 AND 95 THEN '91-95' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  96 AND 100 THEN '96-100' "+ 
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth, GETDATE()) > 100 THEN 'OVER 100' " +
                    "ELSE 'UNKNOWN' END " 
                    
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Ageband5+' as [ Ageband-5 Year]'])
                    }else{columnNames = ([Ageband5 +' as  [Ageband-5 Year]'])}        
                    break;
                    case 'Ageband-10 Year':
                    var Ageban10 = "CASE "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  0 AND 10 THEN '0- 10' " +
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  11 AND 20 THEN '11-20' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE()  ) BETWEEN  21 AND 30 THEN '21-30' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() ) BETWEEN 31 AND 40 THEN '31-40' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,  GETDATE() ) BETWEEN 41 AND 50 THEN '41-50' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth, GETDATE() ) BETWEEN 51 AND 60 THEN '51-60' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE() ) BETWEEN 61 AND 70 THEN '61-70' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE() ) BETWEEN 71 AND 80 THEN '71-80' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE() ) BETWEEN 81 AND 90 THEN '81-90' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE() ) BETWEEN 91 AND 100 THEN '91-100' "+
                    "WHEN DATEDIFF(YEAR, R.DateOfBirth,   GETDATE() ) > 100 THEN 'OVER 100' "+
                    "ELSE 'UNKNOWN' END  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Ageban10+ ' as  [Ageband-10 Year]'])
                    }else{columnNames = ([Ageban10+ ' as  [Ageband-10 Year]'])}        
                    break;
                    case 'Age-ATSI Status':
                    var AgeATSI = " case WHEN DATEADD(YEAR,65, CONVERT(DATETIME,DateOfBirth)) <= GETDATE() OR (DATEADD(YEAR,50, CONVERT(DATETIME,DateOfBirth)) <= GETDATE() AND LEFT( CSTDA_INDIGINOUS, 3) IN ('ABO', 'TOR', 'BOT')) THEN 'OVER 64 OR ATSI OVER 49' ELSE 'UNDER 65 OR ATSI UNDER 50' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AgeATSI +'  as [Age-ATSI Status] '])
                    }else{columnNames = ([AgeATSI + '  as [Age-ATSI Status] '])}        
                    break;
                    case 'Month Of Birth':
                    var Month = "DateName(Month, DateOfBirth)  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Month +' as [Month Of Birth] '])
                    }else{columnNames = ([Month +' as [Month Of Birth] '])}        
                    break;
                    case 'Day Of Birth No.':
                    var day = "DateName(Weekday, DateOfBirth)"
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([day +' as  [Day Of Birth]'])
                    }else{columnNames = ([day +' as [Day Of Birth] '])}        
                    break;
                    case 'CALD Score': 
                    var caldscore = " (SELECT distinct DD.CALDStatus FROM DataDomains DD WHERE DOMAIN = 'Countries' and DD.DESCRIPTION = R.CountryOfBirth) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([caldscore +'  as  [CALD Score]'])
                    }else{columnNames = ([caldscore +'  as [CALD Score] '])}        
                    break;             
                    case 'Country Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.CountryOfBirth as [Country Of Birth] '])
                    }else{columnNames = (['R.CountryOfBirth as [Country Of Birth] '])}        
                    break;
                    case 'Language':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.HomeLanguage as  [Language]'])
                    }else{columnNames = (['R.HomeLanguage as  [Language]'])}        
                    break;
                    case 'Indigenous Status':
                    var IndStatus = "CASE "+
                    "WHEN R.[IndiginousStatus] = 'Aboriginal but not Torres Strait Islander origin' THEN 'ATSI' " +
                    "WHEN R.[IndiginousStatus] = 'Both Aboriginal and Torres Strait Islander origin' THEN 'ATSI'" +
                    "WHEN R.[IndiginousStatus] = 'Torres Strait Islander but not Aboriginal origin' THEN 'ATSI' " +
                    "ELSE 'NON ATSI' " +
                    "END  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([IndStatus+' as  [Indigenous Status]'])
                    }else{columnNames = ([IndStatus+' as [Indigenous Status]'])}        
                    break;
                    case 'Primary Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.CSTDA_DisabilityGroup as  [Primary Disability]'])
                    }else{columnNames = (['R.CSTDA_DisabilityGroup as  [Primary Disability]'])}        
                    break;
                    case 'Financially Dependent':
                    var FinanceDepend = "CASE WHEN R.[FDP] = 1 THEN 'YES'   ELSE 'NO' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([FinanceDepend+'  as  [Financially Dependent]'])
                    }else{columnNames = ([FinanceDepend +'  as  [Financially Dependent]'])}        
                    break;
                    case 'Financial Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.FinancialClass as  [Financial Status]'])
                    }else{columnNames = (['R.FinancialClass as [Financial Status] '])}        
                    break;          
                    case 'Occupation':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Occupation as  [Occupation]'])
                    }else{columnNames = (['R.Occupation as [Occupation] '])}        
                    break;
                    //Admin Info
                    case 'UniqueID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.UniqueID as  [UniqueID]'])
                    }else{columnNames = (['R.UniqueID as [UniqueID] '])}        
                    break;
                    case 'Code':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[AccountNo]  as  [Code]'])
                    }else{columnNames = (['R.[AccountNo]  as  [Code]'])}        
                    break;             
                    case 'Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Type]  as [Type] '])
                    }else{columnNames = (['R.[Type]  as [Type] '])}        
                    break;             
                    case 'Category':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[AgencyDefinedGroup] AS [Category]'])
                    }else{columnNames = (['R.[AgencyDefinedGroup] AS [Category]'])}        
                    break;
                    case 'CoOrdinator':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.RECIPIENT_CoOrdinator as [CoOrdinator] '])
                    }else{columnNames = (['R.RECIPIENT_CoOrdinator as [CoOrdinator] '])}        
                    break;
                    case 'Admitting Branch':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.BRANCH as [Admitting Branch] '])
                    }else{columnNames = (['R.BRANCH as [Admitting Branch] '])}        
                    break;                
                    case 'Secondary Branch':
                    var SecondaryBranch =  "(Select top 1  HR.name from HumanResources HR  left join Recipients R on hr.PersonID = R.UniqueID where HR.[Type] = 'RECIPBRANCHES') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([ SecondaryBranch +  ' as [Secondary Branch] '])
                    }else{columnNames = ([SecondaryBranch + ' as  [Secondary Branch]'])}        
                    break;  
                    case 'File number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[AgencyIDReportingCode] as [File Number] '])
                    }else{columnNames = (['R.[AgencyIDReportingCode] as [File Number] '])}        
                    break;
                    case 'File Number 2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[URNumber] as [File Number 2] '])
                    }else{columnNames = ([' R.[URNumber] as [File Number 2] '])}        
                    break;
                    case 'NDIA/MAC Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.NDISNumber as [NDIA/MAC Number] '])
                    }else{columnNames = (['R.NDISNumber as [NDIA/MAC Number] '])}        
                    break;     
                    case 'Last Activated Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.ADMISSIONDATE  as [Last Activated Date] '])
                    }else{columnNames = (['R.ADMISSIONDATE  as [Last Activated Date] '])}        
                    break;
                    case 'Created By':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.CreatedBy as  [Created By]'])
                    }else{columnNames = (['R.CreatedBy as  [Created By]'])}        
                    break;/*
                    case 'Other':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['  '])
                    }else{columnNames = (['  '])}        
                    break;  */
                    //Staff                
                    case 'Staff Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' S.FIRSTNAME + S.LASTNAME AS [Staff Name] '])
                    }else{columnNames = ([' S.FIRSTNAME + S.LASTNAME AS [Staff Name] '])}  
                    break;
                    case 'Program Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' HRCaseStaff.[Address1] AS [Program Name] '])
                    }else{columnNames = ([' HRCaseStaff.[Address1] AS [Program Name] '])} 
                    break;
                    case 'Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRCaseStaff.[Notes] AS [Notes] '])
                    }else{columnNames = (['HRCaseStaff.[Notes] AS [Notes] '])}
                    break;
                    //Other Genral info
                    case 'OH&S Profile':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.OHSProfile as  [OH&S Profile]'])
                    }else{columnNames = (['R.OHSProfile as [OH&S Profile] '])}
                    break;
                    case 'OLD WH&S Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[WH&S]  as [OLD WH&S Date] '])
                    }else{columnNames = (['R.[WH&S]  as [OLD WH&S Date] '])}  
                    break;   
                    case 'Billing Profile':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.BillProfile as [Billing Profile] '])
                    }else{columnNames = (['R.BillProfile as [Billing Profile] '])}  
                    break;          
                    case 'Sub Category':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[UBDMap]  as [Sub Category] '])
                    }else{columnNames = (['R.[UBDMap]  as  [Sub Category]'])} 
                    break;
                    case 'Roster Alerts':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Notes]  as [Roster Alerts] '])
                    }else{columnNames = (['R.[Notes]  as [Roster Alerts] '])} 
                    break;
                    case 'Timesheet Alerts':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[SpecialConsiderations]  as [Timesheet Alerts] '])
                    }else{columnNames = (['R.[SpecialConsiderations]  as [Timesheet Alerts] '])} 
                    break;                 
                    case 'Contact Issues':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.ContactIssues as [Contact Issues] '])
                    }else{columnNames = (['R.ContactIssues as [Contact Issues] '])}
                    break;
                    case 'Survey Consent Given':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.SurveyConsent as [Survey Consent Given] '])
                    }else{columnNames = (['R.SurveyConsent as [Survey Consent Given] '])}
                    break;
                    case 'Copy Rosters':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Autocopy as [Copy Rosters] '])
                    }else{columnNames = (['R.Autocopy as [Copy Rosters] '])}
                    break;
                    /*case 'Enabled':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['  '])
                    }else{columnNames = ([' '])} 
                    break; */
                    case 'Activation Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' R.[AdmissionDate] as [Activation Date] '])
                    }else{columnNames = ([' R.[AdmissionDate] as [Activation Date] '])} 
                    break;
                    case 'DeActivation Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' R.[DischargeDate] as [DeActivation Date] '])
                    }else{columnNames = ([' R.[DischargeDate] as [DeActivation Date] '])}  
                    break;
                    case 'Mobility':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Mobility as [Mobility] '])
                    }else{columnNames = (['R.Mobility as [Mobility] '])}
                    break;
                    case 'Specific Competencies':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.SpecialConsiderations as  [Specific Competencies]'])
                    }else{columnNames = (['R.SpecialConsiderations as [Specific Competencies] '])}
                    break; /*
                    case 'Carer Info':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['  '])
                    }else{columnNames = (['  '])} 
                    break; */
                    //  Contacts & Next of Kin
                    case 'Contact Group':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Group]   '])
                    }else{columnNames = (['HR.[Group]   '])}
                    break;
                    case 'Contact Type':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Type]    '])
                    }else{columnNames = (['HR.[Type]   '])} 
                    break;
                    case 'Contact Sub Type':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[SubType]  '])
                    }else{columnNames = (['HR.[SubType]  '])}  
                    break;
                    case 'Contact User Flag':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[User1]  '])
                    }else{columnNames = (['HR.[User1]  '])}
                    break;                 
                    case 'Contact Person Type':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[EquipmentCode]  '])
                    }else{columnNames = (['HR.[EquipmentCode]  '])} 
                    break;
                    case 'Contact Name':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Name]  '])
                    }else{columnNames = (['HR.[Name]  '])} 
                    break;
                    case 'Contact Address':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Address1]  '])
                    }else{columnNames = (['HR.[Address1]  '])} 
                    break;
                    case 'Contact Suburb':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Suburb]  '])
                    }else{columnNames = (['HR.[Suburb]  '])}
                    break;
                    case 'Contact Postcode':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Postcode]  '])
                    }else{columnNames = (['HR.[Postcode]  '])} 
                    break;
                    case 'Contact Phone 1':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Phone1] '])
                    }else{columnNames = (['HR.[Phone1] '])} 
                    break;
                    case 'Contact Phone 2':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Phone2]  '])
                    }else{columnNames = (['HR.[Phone2]  '])} 
                    break;
                    case 'Contact Mobile': 
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Mobile]  '])
                    }else{columnNames = (['HR.[Mobile]  '])}
                    break;
                    case 'Contact FAX':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[FAX]  '])
                    }else{columnNames = (['HR.[FAX]  '])}
                    break;
                    case 'Contact Email':
                    this.IncludeContacts = true
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HR.[Email]  '])
                    }else{columnNames = (['HR.[Email]  '])}
                    break;
                    //Carer Info                  
                    case 'Carer First Name':
                    
                    if(columnNames.length >= 1){ 
                      columnNames = columnNames.concat(['C.FirstName  '])
                    }else{columnNames = (['C.FirstName  '])} 
                    break;
                    case 'Carer Last Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['C.[Surname/Organisation]  '])
                    }else{columnNames = (['C.[Surname/Organisation]  '])}
                    break;
                    case 'Carer Age':
                    var CarerAge = " CASE WHEN DATEADD(YEAR,DATEDIFF(YEAR, C.DateOfBirth,Format(GETDATE(), 'yyyy-MM-dd') ),C.DateOfBirth) "+
                    ">  Format(GETDATE(), 'yyyy-MM-dd') THEN DATEDIFF(YEAR, C.DateOfBirth,Format(GETDATE(), 'yyyy-MM-dd')  )-1  "+
                    "ELSE DATEDIFF(YEAR, C.DateOfBirth,Format(GETDATE(), 'yyyy-MM-dd')  ) END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([CarerAge+' '])
                    }else{columnNames = ([CarerAge+' '])}  
                    break;                 
                    case 'Carer Gender':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['C.[Gender]  '])
                    }else{columnNames = (['C.[Gender]  '])}  
                    break;
                    case 'Carer Indigenous Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['C.[IndiginousStatus]  '])
                    }else{columnNames = (['C.[IndiginousStatus]  '])} 
                    break;
                    
                    case 'Carer Address':
                    var CarerAddress= " ISNULL(N.Address1,'') + ' ' +  ISNULL(N.Suburb,'') + ' ' + ISNULL(N.Postcode,'') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([CarerAddress+ ' '])
                    }else{columnNames = ([CarerAddress+' '])}  
                    break;
                    case 'Carer Email': //    
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['PE.Detail  '])
                    }else{columnNames = (['PE.Detail  '])}  
                    break;
                    case 'Carer Phone <Home>': 
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['PhHome.Detail  '])
                    }else{columnNames = (['PhHome.Detail  '])}  
                    break;
                    case 'Carer Phone <Work>':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['PhWork.Detail  '])
                    }else{columnNames = (['PhWork.Detail  '])} 
                    break;
                    case 'Carer Phone <Mobile>':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['PhMobile.Detail  '])
                    }else{columnNames = (['PhMobile.Detail  '])} 
                    break;
                    // Documents                    
                    case 'DOC_ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.DOC_ID  '])
                    }else{columnNames = (['doc.DOC_ID  '])} 
                    break;
                    case 'Doc_Title':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Title  '])
                    }else{columnNames = (['doc.Title  '])}  
                    break;
                    
                    case 'Created':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Created  '])
                    }else{columnNames = (['doc.Created  '])}  
                    break;                
                    case 'Modified':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Modified  '])
                    }else{columnNames = (['doc.Modified  '])} 
                    break;                    
                    case 'Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Status  '])
                    }else{columnNames = (['doc.Status  '])}
                    break;
                    case 'Classification':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Classification  '])
                    }else{columnNames = (['doc.Classification  '])}  
                    break;
                    case 'Category':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Category  '])
                    }else{columnNames = (['doc.Category  '])}  
                    break;
                    case 'Filename':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Filename  '])
                    }else{columnNames = (['doc.Filename  '])}  
                    break;
                    case 'Doc#':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.Doc#  '])
                    }else{columnNames = (['doc.Doc#  '])}  
                    break;
                    case 'DocStartDate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.DocStartDate  '])
                    }else{columnNames = (['doc.DocStartDate  '])}  
                    break;                  
                    case 'DocEndDate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.DocEndDate  '])
                    }else{columnNames = (['doc.DocEndDate  '])}  
                    break;
                    case 'AlarmDate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.AlarmDate  '])
                    }else{columnNames = (['doc.AlarmDate  '])}
                    break;                          
                    case 'AlarmText':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['doc.AlarmText  '])
                    }else{columnNames = (['doc.AlarmText  '])}  
                    break;
                    //Consents                      
                    case 'Consent':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Cons.[Name]  '])
                    }else{columnNames = (['Cons.[Name]  '])}  
                    break;                     
                    case 'Consent Start Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Cons.[Date1] as [Consent Start Date] '])
                    }else{columnNames = (['Cons.[Date1] as [Consent Start Date] '])}  
                    break;
                    case 'Consent Expiry':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Cons.[Date2] as [Consent Expiry] '])
                    }else{columnNames = (['Cons.[Date2] as [Consent Expiry] '])}  
                    break;
                    case 'Consent Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Cons.[Notes]  '])
                    }else{columnNames = (['Cons.[Notes]  '])}  
                    break;
                    //  GOALS OF CARE                      
                    case 'Goal':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[User1]  '])
                    }else{columnNames = (['Goalcare.[User1]  '])}
                    break;               
                    case 'Goal Detail':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[Notes]  '])
                    }else{columnNames = (['Goalcare.[Notes]  '])}  
                    break;
                    case 'Goal Achieved':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[Completed]  '])
                    }else{columnNames = (['Goalcare.[Completed]  '])}  
                    break;                      
                    case 'Anticipated Achievement Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[Date1] as [Anticipated Achievement Date] '])
                    }else{columnNames = (['Goalcare.[Date1] as [Anticipated Achievement Date] '])}  
                    break;
                    case 'Date Achieved':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[DateInstalled]  '])
                    }else{columnNames = (['Goalcare.[DateInstalled]  '])}
                    break;
                    case 'Last Reviewed':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[Date2] as [Last Reviewed] '])
                    }else{columnNames = (['Goalcare.[Date2] as [Last Reviewed] '])}  
                    break;              
                    case 'Logged By':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Goalcare.[Creator]  '])
                    }else{columnNames = (['Goalcare.[Creator]  '])}
                    break;
                    //REMINDERS                      
                    case 'Reminder Detail':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Remind.[Name]  '])
                    }else{columnNames = (['Remind.[Name]  '])}
                    break;
                    case 'Event Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Remind.[Date2]  as [Event Date]'])
                    }else{columnNames = (['Remind.[Date2] as [Event Date] '])}  
                    break;
                    
                    case 'Reminder Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Remind.[Date1] as [Reminder Date] '])
                    }else{columnNames = (['Remind.[Date1] as [Reminder Date] '])}  
                    break;
                    case 'Reminder Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Remind.[Notes]  '])
                    }else{columnNames = (['Remind.[Notes]  '])}  
                    break;
                    // USER GROUPS                      
                    case 'Group Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['UserGroup.[Name]  '])
                    }else{columnNames = (['UserGroup.[Name]  '])}  
                    break;
                    case 'Group Note':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['UserGroup.[Notes]  '])
                    }else{columnNames = (['UserGroup.[Notes]  '])} 
                    break; 
                    case 'Group Start Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['UserGroup.[Date1] as [Group Start Date] '])
                    }else{columnNames = (['UserGroup.[Date1] as [Group Start Date] '])}  
                    break;                      
                    case 'Group End Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['UserGroup.[Date2] as [Group End Date] '])
                    }else{columnNames = (['UserGroup.[Date2] as [Group End Date] '])} 
                    break;
                    case 'Group Email':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['UserGroup.[Email]  '])
                    }else{columnNames = (['UserGroup.[Email]  '])} 
                    break;
                    
                    //Preferences                                      
                    case 'Preference Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Prefr.[Name]  '])
                    }else{columnNames = (['Prefr.[Name]  '])}  
                    break;                      
                    case 'Preference Note':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Prefr.[Notes]  '])
                    }else{columnNames = (['Prefr.[Notes]  '])}  
                    break;
                    // FIXED REVIEW DATES                    
                    case 'Review Date 1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[OpReASsessmentDate]  '])
                    }else{columnNames = (['R.[OpReASsessmentDate]  '])}  
                    break;
                    case 'Review Date 2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ClinicalReASsessmentDate]  '])
                    }else{columnNames = (['R.[ClinicalReASsessmentDate]  '])}  
                    break;
                    case 'Review Date 3':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[FileReviewDate]  '])
                    }else{columnNames = (['R.[FileReviewDate]  '])} 
                    break;   
                    //Staffing Inclusions/Exclusions                                 
                    case 'Excluded Staff':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ExcludeS.[Name]  '])
                    }else{columnNames = (['ExcludeS.[Name]  '])}  
                    break;                
                    case 'Excluded_Staff Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ExcludeS.[Notes]  '])
                    }else{columnNames = (['ExcludeS.[Notes]  '])}  
                    break;
                    case 'Included Staff':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IncludS.[Name]  '])
                    }else{columnNames = (['IncludS.[Name]  '])} 
                    break;
                    case 'Included_Staff Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IncludS.[Notes]  '])
                    }else{columnNames = (['IncludS.[Notes]  '])} 
                    break;                      
                    // AGREED FUNDING INFORMATION                            
                    case 'Funding Source':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HumanResourceTypes.[Type]  '])
                    }else{columnNames = (['HumanResourceTypes.[Type]  '])}  
                    break;
                    case 'Funded Program':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPrograms.[Program]  '])
                    }else{columnNames = (['RecipientPrograms.[Program]  '])}  
                    break;
                    case 'Funded Program Agency ID':
                      
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HumanResourceTypes.[Address1] As [Funded Program Agency ID]  '])
                    }else{columnNames = (['HumanResourceTypes.[Address1] As [Funded Program Agency ID]  '])}  
                    break;
                    case 'Program Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPrograms.[ProgramStatus]  '])
                    }else{columnNames = (['RecipientPrograms.[ProgramStatus]  '])}  
                    break;
                    case 'Program Coordinator':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HumanResourceTypes.[Address2]  '])
                    }else{columnNames = (['HumanResourceTypes.[Address2]  '])}  
                    break;
                    case 'Funding Start Date':                      
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' RecipientPrograms.[StartDate] As [Funding Start Date]  '])
                    }else{columnNames = ([' RecipientPrograms.[StartDate] As [Funding Start Date]  '])} 
                    break;
                    case 'Funding End Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPrograms.[ExpiryDate]  '])
                    }else{columnNames = (['RecipientPrograms.[ExpiryDate]  '])} 
                    break;
                    
                    case 'AutoRenew':
                    var Autorenew = " CASE WHEN RecipientPrograms.[AutoRenew] = 1 THEN 'YES' ELSE 'NO' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Autorenew +'  '])
                    }else{columnNames = ([Autorenew +'  '])}  
                    break;
                    case 'Rollover Remainder':
                    var RolloverReminder = " CASE WHEN RecipientPrograms.[RolloverRemainder] = 1 THEN 'YES' ELSE 'NO' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([RolloverReminder +' '])
                    }else{columnNames = ([RolloverReminder +' '])}  
                    break;
                    case 'Funded Qty':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPrograms.[Quantity]  '])
                    }else{columnNames = (['RecipientPrograms.[Quantity]  '])}
                    break;
                    case 'Funded Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPrograms.[ItemUnit]  '])
                    }else{columnNames = (['RecipientPrograms.[ItemUnit]  '])} 
                    break;
                    case 'Funding Cycle':
                    var FundingCycle = "CASE WHEN RecipientPrograms.[PerUnit] <> '' THEN RecipientPrograms.[PerUnit] + ' ' ELSE '' END + "  +
                    " CASE WHEN RecipientPrograms.[TimeUnit] <> '' THEN RecipientPrograms.[TimeUnit] + ' ' ELSE '' END + "  +
                    " CASE WHEN RecipientPrograms.[Period] <> '' THEN RecipientPrograms.[Period] ELSE '' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([FundingCycle+' AS [Funding Cycle] '])
                    }else{columnNames = ([FundingCycle+' AS [Funding Cycle] '])}
                    break;
                    case 'Funded Total Allocation':
                    var Allocation = " CASE WHEN RecipientPrograms.[TotalAllocation] <> '' THEN RecipientPrograms.[TotalAllocation] ELSE 0 END  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Allocation +'  '])
                    }else{columnNames = ([Allocation +'  '])}
                    break;
                    case 'Used':
                    var used = " CASE WHEN RecipientPrograms.[Used] <> '' THEN RecipientPrograms.[Used] ELSE 0 END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([used +'  '])
                    }else{columnNames = ([used +'  '])} 
                    break;
                    case 'Remaining':
                    var remaining = " CASE WHEN RecipientPrograms.[TotalAllocation] <> '' AND RecipientPrograms.[Used] <> '' THEN (RecipientPrograms.[TotalAllocation] - RecipientPrograms.[Used]) ELSE 0 END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([remaining + '  '])
                    }else{columnNames = ([remaining +'  '])}  
                    break;
                    //LEGACY CARE PLAN                      
                    case 'Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CarePlanItem.[PlanName]  '])
                    }else{columnNames = (['CarePlanItem.[PlanName]  '])}  
                    break;                                    
                    case 'Start Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CarePlanItem.[PlanStartDate]  '])
                    }else{columnNames = (['CarePlanItem.[PlanStartDate]  '])}
                    break;
                    case 'End Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CarePlanItem.[PlanEndDate]  '])
                    }else{columnNames = (['CarePlanItem.[PlanEndDate]  '])}
                    break;                      
                    case 'Details':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CarePlanItem.[PlanDetail]  '])
                    }else{columnNames = (['CarePlanItem.[PlanDetail]  '])}
                    break;
                    case 'Reminder Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CarePlanItem.[PlanReminderDate]  '])
                    }else{columnNames = (['CarePlanItem.[PlanReminderDate]  '])}  
                    break;
                    case 'Reminder Text':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CarePlanItem.[PlanReminderText]  '])
                    }else{columnNames = (['CarePlanItem.[PlanReminderText]  '])}  
                    break;  
                    //Agreed Service Information                                    
                    case 'Agreed Service Code':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[Service Type]  '])
                    }else{columnNames = (['ServiceOverview.[Service Type]  '])}  
                    break;
                    case 'Agreed Program':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[ServiceProgram]  '])
                    }else{columnNames = (['ServiceOverview.[ServiceProgram]  '])}  
                    break;
                    case 'Agreed Service Billing Rate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[Unit Bill Rate]  '])
                    }else{columnNames = (['ServiceOverview.[Unit Bill Rate]  '])}  
                    break;                       
                    case 'Agreed Service Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[ServiceStatus]  '])
                    }else{columnNames = (['ServiceOverview.[ServiceStatus]  '])} 
                    break;
                    case 'Agreed Service Duration':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[Duration]  '])
                    }else{columnNames = (['ServiceOverview.[Duration]  '])}
                    break;
                    case 'Agreed Service Frequency':
                    var Frequency = " CONVERT(VARCHAR, ServiceOverview.[Frequency]) + ' ' + ServiceOverview.[Period] "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Frequency+'  '])
                    }else{columnNames = ([Frequency+'  '])} 
                    break;
                    case 'Agreed Service Cost Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[Cost Type]  '])
                    }else{columnNames = (['ServiceOverview.[Cost Type]  '])} 
                    break;
                    case 'Agreed Service Unit Cost':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[Unit Pay Rate]  '])
                    }else{columnNames = (['ServiceOverview.[Unit Pay Rate]  '])}  
                    break;
                    case 'Agreed Service Billing Unit':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[UnitType]  '])
                    }else{columnNames = (['ServiceOverview.[UnitType]  '])}  
                    break;
                    case 'Agreed Service Debtor':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ServiceOverview.[ServiceBiller]  '])
                    }else{columnNames = (['ServiceOverview.[ServiceBiller]  '])}  
                    break;
                    //  CLINICAL INFORMATION                       
                    case 'Nursing Diagnosis':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['NDiagnosis.[Description]  '])
                    }else{columnNames = (['NDiagnosis.[Description]  '])}  
                    break;
                    case 'Medical Diagnosis':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MDiagnosis.[Description]  '])
                    }else{columnNames = (['MDiagnosis.[Description]  '])} 
                    break;
                    case 'Medical Procedure':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MProcedures.[Description]  '])
                    }else{columnNames = (['MProcedures.[Description]  '])}  
                    break;
                    //Billing Information
                    case 'Billing Client':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[BillTo] as [Billing Client]  '])
                    }else{columnNames = (['R.[BillTo] as [Billing Client]  '])}
                    break;
                    case 'Billing Cycle':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[BillingCycle] as [Billing Cycle]  '])
                    }else{columnNames = (['R.[BillingCycle] as [Billing Cycle]  '])}
                    break;
                    case 'Billing Rate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[BillingMethod] as [Billing Rate]  '])
                    }else{columnNames = (['R.[BillingMethod] as [Billing Rate]  '])}
                    break;                                                                                                                                                                                    
                    case 'Billing %':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[PercentageRate] as [Billing %]  '])
                    }else{columnNames = (['R.[PercentageRate] as [Billing %]  '])}
                    break;            
                    case 'Billing Amount':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DonationAmount] as [Billing Amount]  '])
                    }else{columnNames = (['R.[DonationAmount] as [Billing Amount]  '])}
                    break;
                    case 'Account Identifier':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' R.[AccountingIdentifier] AS [Account Identifier] '])
                    }else{columnNames = ([' R.[AccountingIdentifier] AS [Account Identifier] '])}
                    break;
                    case 'External Order Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Order#] as [External Order Number]  '])
                    }else{columnNames = (['R.[Order#] as [External Order Number]  '])}
                    break;
                    case 'Financially Dependent':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[FDP] as [Financially Dependent]  '])
                    }else{columnNames = (['R.[FDP] as [Financially Dependent]  '])}
                    break;
                    case 'BPay Reference':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[BPayRef] as [BPay Reference]  '])
                    }else{columnNames = (['R.[BPayRef] as [BPay Reference]  '])}
                    break;
                    case 'NDIA/MAC ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[NDISNumber] as [NDIA/MAC ID]  '])
                    }else{columnNames = (['R.[NDISNumber] as [NDIA/MAC ID]  '])}
                    break;
                    case 'Bill Profile':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Convert(nVarChar(4000), R.[BillProfile]) as [Bill Profile]  '])
                    }else{columnNames = (['Convert(nVarChar(4000), R.[BillProfile]) as [Bill Profile]  '])}
                    break;
                    case 'Allow Different Biller':
                      var DifferentBiller = "CASE WHEN [RECIPIENT_SPLIT_BILL] = 1 THEN 'YES' ELSE 'NO' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DifferentBiller+ ' AS [Allow Different Biller] '])
                    }else{columnNames = ([DifferentBiller +' AS [Allow Different Biller] '])}
                    break;
                                                                                
                    case 'Capped Bill':
                      var CappedBill = "CASE WHEN [CappedBill] = 1 THEN 'YES' ELSE 'NO' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([CappedBill +' AS [Capped Bill] '])
                    }else{columnNames = ([CappedBill +' AS [Capped Bill] '])}
                    break;
                    case 'Hide Fare on Transport Runsheet':
                      var HideTransportSheet = "CASE WHEN [HideTransportFare] = 1 THEN 'YES' ELSE 'NO' END  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([HideTransportSheet + ' AS [Hide Fare on Transport Runsheet] '])
                    }else{columnNames = ([HideTransportSheet + ' AS [Hide Fare on Transport Runsheet] '])}
                    break;
                    case 'Print Invoice':
                      var PrintInvoice = "CASE WHEN [PrintInvoice] = 1 THEN 'YES' ELSE 'NO' END   "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([PrintInvoice +' AS [Print Invoice] '])
                    }else{columnNames = ([PrintInvoice +' AS [Print Invoice] '])}
                    break;                                                          
                    case 'Email Invoice':
                      var EmailInvoice = "CASE WHEN [EmailInvoice] = 1 THEN 'YES' ELSE 'NO' END AS [Email Invoice]"
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([EmailInvoice +' AS [Email Invoice] '])
                    }else{columnNames = ([EmailInvoice + ' AS [Email Invoice] '])}
                    break;
                    case 'Direct Debit':
                      var DirectDebit = "CASE WHEN [DirectDebit] = 1 THEN 'YES' ELSE 'NO' END  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DirectDebit + ' AS [Direct Debit] '])
                    }else{columnNames = ([DirectDebit + ' AS [Direct Debit] '])}
                    break;
                    case 'DVA CoBiller':
                      var dvaCobiller = "CASE WHEN [DVACoBiller] = 1 THEN 'YES' ELSE 'NO' END  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([dvaCobiller +' AS [DVA CoBiller] '])
                    }else{columnNames = ([dvaCobiller +' AS [DVA CoBiller] '])}
                    break;
                    
                    //PANZTEL Timezone 
                    case 'PANZTEL Timezone':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPIENT_TIMEZONE]  '])
                    }else{columnNames = (['R.[RECIPIENT_TIMEZONE]  '])}  
                    break;                     
                    case 'PANZTEL PBX Site':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPIENT_PBX]  '])
                    }else{columnNames = (['R.[RECIPIENT_PBX]  '])}  
                    break;
                    case 'PANZTEL Parent Site':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPIENT_PARENT_SITE]  '])
                    }else{columnNames = (['R.[RECIPIENT_PARENT_SITE]  '])} 
                    break;
                    case 'DAELIBS Logger ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DAELIBSID]  '])
                    }else{columnNames = (['R.[DAELIBSID]  '])} 
                    break;
                    //NOTES
                    case 'General Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(TEXT, R.[Recipient_Notes]) as [General Notes]   '])
                    }else{columnNames = (['CONVERT(TEXT, R.[Recipient_Notes]) as [General Notes]   '])}
                    break;
                    case 'Case Notes Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['History.[DetailDate] as [Case Notes Date]  '])
                    }else{columnNames = (['History.[DetailDate] as [Case Notes Date]  '])}
                    break;
                    case 'Case Notes Detail':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['dbo.RTF2TEXT(History.[Detail]) as [Case Notes Detail]  '])
                    }else{columnNames = (['dbo.RTF2TEXT(History.[Detail]) as [Case Notes Detail]  '])}
                    break;
                    case 'Case Notes Creator':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['History.[Creator] as [Case Notes Creator]   '])
                    }else{columnNames = (['History.[Creator] as [Case Notes Creator]   '])}
                    break;
                    case 'Case Notes Alarm':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['History.[AlarmDate] as [Case Notes Alarm]  '])
                    }else{columnNames = (['History.[AlarmDate] as [Case Notes Alarm]  '])}
                    break;
                    case 'Case Notes Program':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['History.Program as [Case Notes Program]  '])
                    }else{columnNames = (['History.Program as [Case Notes Program]  '])}
                    break;
                    case 'Case Notes Category':
                    var CasenoteCategory = "CASE WHEN IsNull(History.ExtraDetail2,'') < 'A' THEN 'UNKNOWN' ELSE History.ExtraDetail2 END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([CasenoteCategory + ' AS [Case Notes Category] '])
                    }else{columnNames = ([CasenoteCategory + ' AS [Case Notes Category] '])}
break;
                    //INSURANCE AND PENSION                      
                    case 'Medicare Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[MedicareNumber]  '])
                    }else{columnNames = (['R.[MedicareNumber]  '])}  
                    break;
                    case 'Medicare Recipient ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[MedicareRecipientID]  '])
                    }else{columnNames = (['R.[MedicareRecipientID]  '])}  
                    break;
                    case 'Pension Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[PensionStatus]  '])
                    }else{columnNames = (['R.[PensionStatus]  '])}  
                    break;
                    case 'Unable to Determine Pension Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[PensionVoracity]  '])
                    }else{columnNames = (['R.[PensionVoracity]  '])}
                    break;
                    case 'Concession Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ConcessionNumber]  '])
                    }else{columnNames = (['R.[ConcessionNumber]  '])}  
                    break;
                    case 'DVA Benefits Flag':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DVABenefits]  '])
                    }else{columnNames = (['R.[DVABenefits]  '])}  
                    break;
                    case 'DVA Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DVANumber]  '])
                    }else{columnNames = (['R.[DVANumber]  '])}  
                    break;
                    case 'DVA Card Holder Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_DVA_Card_Holder_Status]  '])
                    }else{columnNames = (['R.[RECIPT_DVA_Card_Holder_Status]  '])} 
                    break;
                    case 'Ambulance Subscriber':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Ambulance]  '])
                    }else{columnNames = (['R.[Ambulance]  '])}
                    break;
                    case 'Ambulance Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[AmbulanceType]  '])
                    }else{columnNames = (['R.[AmbulanceType]  '])}  
                    break;
                    case 'Pension Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPensions.[Pension Name]  '])
                    }else{columnNames = (['RecipientPensions.[Pension Name]  '])} 
                    break;
                    case 'Pension Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecipientPensions.[Pension Number]  '])
                    }else{columnNames = (['RecipientPensions.[Pension Number]  '])}
                    break;
                    case 'Will Available':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[WillAvailable]  '])
                    }else{columnNames = (['R.[WillAvailable]  '])}
                    break;         
                    case 'Will Location':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[WhereWillHeld]  '])
                    }else{columnNames = (['R.[WhereWillHeld]  '])}
                    break;
                    case 'Funeral Arrangements':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[FuneralArrangements]  '])
                    }else{columnNames = (['R.[FuneralArrangements]  '])}
                    break;
                    case 'Date Of Death':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DateOfDeath]  '])
                    }else{columnNames = (['R.[DateOfDeath]  '])} 
                    break;
                    //HACCS DATSET FIELDS                      
                    case 'HACC-SLK':
                    if(columnNames.length >= 1){
                      var HACCSLACK = " UPPER(Substring(R.[Surname/Organisation], 2, 1) +  Substring(R.[Surname/Organisation], 3, 1) +   Substring(R.[Surname/Organisation], 5, 1) +   Substring(R.[FirstName], 2, 1) +   Substring(R.[FirstName], 3, 1) +   Replace(Convert(Varchar,DateOfBirth,103),'/','') +    Case When R.[Gender] = 'Male' Then '1'  When R.[Gender] = 'Female' Then '2' Else '9' End) "
                      columnNames = columnNames.concat([HACCSLACK +'  '])
                    }else{columnNames = ([HACCSLACK +' '])} 
                    break;
                    case 'HACC-First Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[FirstName]  '])
                    }else{columnNames = (['R.[FirstName]  '])} 
                    break;
                    case 'HACC-Surname':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Surname/Organisation]  '])
                    }else{columnNames = (['R.[Surname/Organisation]  '])} 
                    break;
                    case 'HACC-Referral Source':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ReferralSource]  '])
                    }else{columnNames = (['R.[ReferralSource]  '])} 
                    break;
                    case 'HACC-Date Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DateOfBirth]  '])
                    }else{columnNames = (['R.[DateOfBirth]  '])} 
                    break;
                    /**
                    * RHACC.[HACC-SLK] as [HACC-SLK]
                    R.[FirstName] as [HACC-First Name]
                    R.[Surname/Organisation] as [HACC-Surname]
                    R.[ExcludeFromStats] as [HACC-Exclude From Collection]
                    R.[DateOfBirth] as [HACC-Date Of Birth]
                    R.[ONIFProfile1] as [HACC-Housework]
                    R.[ONIFProfile2] as [HACC-Transport]
                    R.[ONIFProfile3] as [HACC-Shopping]
                    R.[ONIFProfile4] as [HACC-Medication]
                    R.[ONIFProfile5] as [HACC-Money]
                    R.[ONIFProfile6] as [HACC-Walking]
                    R.[ONIFProfile7] as [HACC-Bathing]
                    R.[ONIFProfile8] as [HACC-Memory],
                    R.[ONIFProfile9] as [HACC-Behaviour]
                    ONI.[FPA_Communication] as [HACC-Communication],
                    ONI.[FPA_Dressing] as [HACC-Dressing]
                    ONI.[FPA_Eating] as [HACC-Eating]
                    ONI.[FPA_Toileting] as [HACC-Toileting]
                    ONI.[FPA_GetUp] as [HACC-GetUp]
                    R.[CARER_MORE_THAN_ONE] as [HACC-Carer More Than One]
                    R.[ReferralSource] as [HACC-Referral Source]
                    (CASE R.[CSTDA_BDEstimate]  WHEN 1 THEN 'True' else 'False' END) as [HACC-Date Of Birth Estimated]
                    R.[Gender] as [HACC-Gender]
                    R.[Suburb] as [HACC-Area Of Residence]
                    R.[CountryOfBirth] as [HACC-Country Of Birth]
                    R.[HomeLanguage] as [HACC-Preferred Language]
                    R.[IndiginousStatus] as [HACC-Indigenous Status]
                    R.[LivingArrangements] as [HACC-Living Arrangements]
                    R.[DwellingAccomodation] as [HACC-Dwelling/Accomodation]
                    DATADOMAINS.[DESCRIPTION] as [HACC-Main Reasons For Cessation]
                    R.[PensionStatus] as [HACC-Pension Status]
                    R.[DatasetCarer] as [HACC-Primary Carer]
                    R.[CarerAvailability] as [HACC-Carer Availability]
                    R.[CarerResidency] as [HACC-Carer Residency]
                    R.[CarerRelationship] as [HACC-Carer Relationship]
                    *  */                      
                    case 'HACC-Date Of Birth Estimated':
                      var HaccDB = "(CASE R.[CSTDA_BDEstimate]  WHEN 1 THEN 'True' else 'False' END) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([HaccDB +' as [HACC-Date Of Birth Estimated] '])
                    }else{columnNames = ([HaccDB +' as [HACC-Date Of Birth Estimated] '])} 
                    break;                
                    case 'HACC-Gender':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Gender]  '])
                    }else{columnNames = (['R.[Gender]  '])}  
                    break;
                    case 'HACC-Area Of Residence':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Suburb]  '])
                    }else{columnNames = (['R.[Suburb]  '])} 
                    break;
                    case 'HACC-Country Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CountryOfBirth]  '])
                    }else{columnNames = (['R.[CountryOfBirth]  '])}  
                    break;
                    case 'HACC-Preferred Language,':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[HomeLanguage]  '])
                    }else{columnNames = (['R.[HomeLanguage]  '])}  
                    break;
                    case 'HACC-Indigenous Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[IndiginousStatus]  '])
                    }else{columnNames = (['R.[IndiginousStatus]  '])}  
                    break;
                    case 'HACC-Living Arrangements':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[LivingArrangements]  '])
                    }else{columnNames = (['R.[LivingArrangements]  '])} 
                    break;
                    case 'HACC-Dwelling/Accomodation':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DwellingAccomodation]  '])
                    }else{columnNames = (['R.[DwellingAccomodation]  '])}  
                    break;
                    case 'HACC-Main Reasons For Cessation':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DATADOMAINS.[DESCRIPTION]  '])
                    }else{columnNames = (['DATADOMAINS.[DESCRIPTION]  '])} 
                    break;
                    case 'HACC-Pension Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[PensionStatus]  '])
                    }else{columnNames = (['R.[PensionStatus]  '])}  
                    break;              
                    case 'HACC-Primary Carer':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DatasetCarer]  '])
                    }else{columnNames = (['R.[DatasetCarer]  '])}  
                    break;
                    case 'HACC-Carer Availability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CarerAvailability]  '])
                    }else{columnNames = (['R.[CarerAvailability]  '])}  
                    break;
                    case 'HACC-Carer Residency':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CarerResidency]  '])
                    }else{columnNames = (['R.[CarerResidency]  '])} 
                    break;
                    case 'HACC-Carer Relationship':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CarerRelationship]  '])
                    }else{columnNames = (['R.[CarerRelationship]  '])}
                    break;
                    case 'HACC-Exclude From Collection':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ExcludeFromStats]  '])
                    }else{columnNames = (['R.[ExcludeFromStats]  '])}  
                    break;                                
                    case 'HACC-Housework':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile1]  '])
                    }else{columnNames = (['R.[ONIFProfile1]  '])}
                    break;
                    case 'HACC-Transport':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile2]  '])
                    }else{columnNames = (['R.[ONIFProfile2]  '])} 
                    break;
                    case 'HACC-Shopping':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile3]  '])
                    }else{columnNames = (['R.[ONIFProfile3]  '])}  
                    break;
                    case 'HACC-Medication':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile4]  '])
                    }else{columnNames = (['R.[ONIFProfile4]  '])} 
                    break;
                    case 'HACC-Money':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile5]  '])
                    }else{columnNames = (['R.[ONIFProfile5]  '])} 
                    break;
                    case 'HACC-Walking':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile6]  '])
                    }else{columnNames = (['R.[ONIFProfile6]  '])}  
                    break;
                    case 'HACC-Bathing':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile7]  '])
                    }else{columnNames = (['R.[ONIFProfile7]  '])}  
                    break;
                    case 'HACC-Memory':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile8]  '])
                    }else{columnNames = (['R.[ONIFProfile8]  '])}  
                    break;
                    case 'HACC-Behaviour':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile9]  '])
                    }else{columnNames = (['R.[ONIFProfile9]  '])}
                    break;
                    case 'HACC-Communication':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FPA_Communication]  '])
                    }else{columnNames = (['ONI.[FPA_Communication]  '])} 
                    break;
                    case 'HACC-Eating':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FPA_Eating]  '])
                    }else{columnNames = (['ONI.[FPA_Eating]  '])} 
                    break;
                    case 'HACC-Toileting':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FPA_Toileting]  '])
                    }else{columnNames = (['ONI.[FPA_Toileting]  '])} 
                    break;
                    case 'HACC-GetUp': 
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FPA_GetUp]  '])
                    }else{columnNames = (['ONI.[FPA_GetUp]  '])} 
                    break;        
                    case 'HACC-Carer More Than One':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CARER_MORE_THAN_ONE]  '])
                    }else{columnNames = (['R.[CARER_MORE_THAN_ONE]  '])}  
                    break;
                    //"DEX"                      
                    case 'DEX-Exclude From MDS':
                    var dexexcludemds = " CASE WHEN ISNULL(R.ExcludeFromStats, 0) = 0 THEN 'NO' ELSE 'YES' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([dexexcludemds+ '  '])
                    }else{columnNames = ([dexexcludemds+'  '])}  
                    break;
                    case 'DEX-Referral Purpose':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.ReferralPurpose  '])
                    }else{columnNames = (['DSS.ReferralPurpose  '])}
                    break;
                    case 'DEX-Referral Source':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.[DEXREFERRALSOURCE]  '])
                    }else{columnNames = (['DSS.[DEXREFERRALSOURCE]  '])} 
                    break;
                    case 'DEX-Referral Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.ReferralType  '])
                    }else{columnNames = (['DSS.ReferralType  '])}  
                    break;
                    case 'DEX-Reason For Assistance':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.AssistanceReason  '])
                    }else{columnNames = (['DSS.AssistanceReason  '])} 
                    break;
                    case 'DEX-Consent To Provide Information':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.[DEX-Consent To Provide Information]  '])
                    }else{columnNames = (['DSS.[DEX-Consent To Provide Information]  '])}  
                    break;
                    case 'DEX-Consent For Future Contact':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.[DEX-Consent For Future Contact]  '])
                    }else{columnNames = (['DSS.[DEX-Consent For Future Contact]  '])}  
                    break;             
                    case 'DEX-Sex':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.Gender  '])
                    }else{columnNames = (['R.Gender  '])} 
                    break;                      
                    case 'DEX-Date Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.DateOfBirth  '])
                    }else{columnNames = (['R.DateOfBirth  '])}  
                    break;
                    case 'DEX-Estimated Birth Date':
                    var BDEstimate = " CASE WHEN ISNULL(R.CSTDA_BDEstimate, '') = 0 THEN 'NO' ELSE 'YES' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([BDEstimate+'  '])
                    }else{columnNames = ([BDEstimate+'  '])}  
                    break;
                    case 'DEX-Indigenous Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.DexIndiginousStatus  '])
                    }else{columnNames = (['DSS.DexIndiginousStatus  '])}  
                    break;
                    case 'DEX-DVA Card Holder Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.HACCDVACardHolderStatus  '])
                    }else{columnNames = (['ONI.HACCDVACardHolderStatus  '])}
                    break;        
                    case 'DEX-Has Disabilities':
                    var Dexhasdisability = " CASE WHEN ISNULL(DSS.HasDisabilities, 0) = 0 THEN 'NO' ELSE 'YES' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Dexhasdisability+'  '])
                    }else{columnNames = ([Dexhasdisability+ '  '])}  
                    break;
                    case 'DEX-Has A Carer':
                    var DEXcarer = " CASE WHEN ISNULL(DSS.HasCarer, 0) = 0 THEN 'NO' ELSE 'YES' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DEXcarer  '])
                    }else{columnNames = (['DEXcarer  '])}
                    break;
                    case 'DEX-Country of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.CountryOfBirth  '])
                    }else{columnNames = (['R.CountryOfBirth  '])} 
                    break;
                    case 'DEX-First Arrival Year':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.FirstArrivalYear  '])
                    }else{columnNames = (['DSS.FirstArrivalYear  '])}
                    break;               
                    case 'DEX-First Arrival Month':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.FirstArrivalMonth  '])
                    }else{columnNames = (['DSS.FirstArrivalMonth  '])}  
                    break;
                    case 'DEX-Visa Code':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.VisaCategory  '])
                    }else{columnNames = (['DSS.VisaCategory  '])}  
                    break;
                    case 'DEX-Ancestry':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.Ancestry  '])
                    }else{columnNames = (['DSS.Ancestry  '])}  
                    break;
                    
                    case 'DEX-Main Language At Home':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.HomeLanguage  '])
                    }else{columnNames = (['R.HomeLanguage  '])}  
                    break;
                    case 'DEX-Accomodation Setting':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.DEXACCOMODATION  '])
                    }else{columnNames = (['DSS.DEXACCOMODATION  '])} 
                    break;
                    case 'DEX-Is Homeless':
                    var dexhomeless = " CASE WHEN ISNULL(DSS.IsHomeless, 0) = 0 THEN 'NO' ELSE 'YES' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([dexhomeless+'  '])
                    }else{columnNames = ([dexhomeless+'  '])}  
                    break;
                    case 'DEX-Household Composition':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.HouseholdComposition  '])
                    }else{columnNames = (['DSS.HouseholdComposition  '])}  
                    break;
                    case 'DEX-Main Source Of Income':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.MainIncomSource  '])
                    }else{columnNames = (['DSS.MainIncomSource  '])} 
                    break;
                    case 'DEX-Income Frequency':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.IncomFrequency  '])
                    }else{columnNames = (['DSS.IncomFrequency  '])}  
                    break;
                    case 'DEX-Income Amount': 
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['DSS.IncomeAmount  '])
                    }else{columnNames = (['DSS.IncomeAmount  '])} 
                    break; 
                    // CSTDA Dataset Fields                      
                    case 'CSTDA-Date Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DateOfBirth]  '])
                    }else{columnNames = (['R.[DateOfBirth]  '])}  
                    break;
                    case 'CSTDA-Gender':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_Sex]  '])
                    }else{columnNames = (['R.[CSTDA_Sex]  '])}  
                    break;
                    case 'CSTDA-DISQIS ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_ID]  '])
                    }else{columnNames = (['R.[CSTDA_ID]  '])} 
                    break;
                    case 'CSTDA-Indigenous Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_Indiginous]  '])
                    }else{columnNames = (['R.[CSTDA_Indiginous]  '])}  
                    break;
                    case 'CSTDA-Country Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CountryOfBirth]  '])
                    }else{columnNames = (['R.[CountryOfBirth]  '])} 
                    break;
                    case 'CSTDA-Interpreter Required':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_Interpreter]  '])
                    }else{columnNames = (['R.[CSTDA_Interpreter]  '])}  
                    break;
                    case 'CSTDA-Communication Method':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_Communication]  '])
                    }else{columnNames = (['R.[CSTDA_Communication]  '])}  
                    break;
                    case 'CSTDA-Living Arrangements':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_LivingArrangements]  '])
                    }else{columnNames = (['R.[CSTDA_LivingArrangements]  '])} 
                    break;
                    case 'CSTDA-Suburb':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Suburb]  '])
                    }else{columnNames = (['R.[Suburb]  '])}
                    break;
                    case 'CSTDA-Postcode':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Postcode]  '])
                    }else{columnNames = (['R.[Postcode]  '])}  
                    break;
                    case 'CSTDA-State':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[State]  '])
                    }else{columnNames = (['R.[State]  '])}  
                    break;
                    case 'CSTDA-Residential Setting':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_ResidentialSetting]  '])
                    }else{columnNames = (['R.[CSTDA_ResidentialSetting]  '])}  
                    break;            
                    case 'CSTDA-Primary Disability Group':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_DisabilityGroup]  '])
                    }else{columnNames = (['R.[CSTDA_DisabilityGroup]  '])}  
                    break;
                    case 'CSTDA-Primary Disability Description':
                    var PrmaryDiabilityDesc = " CAST(ONI.[CSTDA_PrimaryDisabilityDescription] AS VARCHAR(4000)) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([PrmaryDiabilityDesc + '  '])
                    }else{columnNames = ([PrmaryDiabilityDesc + '  '])}  
                    break;
                    case 'CSTDA-Intellectual Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherIntellectual]  '])
                    }else{columnNames = (['R.[CSTDA_OtherIntellectual]  '])}
                    break;
                    case 'CSTDA-Specific Learning ADD Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherADD]  '])
                    }else{columnNames = (['R.[CSTDA_OtherADD]  '])}  
                    break;
                    case 'CSTDA-Autism Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherAutism]  '])
                    }else{columnNames = (['R.[CSTDA_OtherAutism]  '])}  
                    break;
                    case 'CSTDA-Physical Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherPhysical]  '])
                    }else{columnNames = (['R.[CSTDA_OtherPhysical]  '])}  
                    break;  
                    case 'CSTDA-Acquired Brain Injury Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherAcquiredBrain]  '])
                    }else{columnNames = (['R.[CSTDA_OtherAcquiredBrain]  '])}  
                    break;
                    case 'CSTDA-Neurological Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherNeurological]  '])
                    }else{columnNames = (['R.[CSTDA_OtherNeurological]  '])} 
                    break;
                    case 'CSTDA-Deaf Blind Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherDeafBlind]  '])
                    }else{columnNames = (['R.[CSTDA_OtherDeafBlind]  '])}                         
                    break;
                    case 'CSTDA-Psychiatric Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_Psychiatric]  '])
                    }else{columnNames = (['R.[CSTDA_Psychiatric]  '])} 
                    break;
                    case 'CSTDA-Other Psychiatric Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherPsychiatric]  '])
                    }else{columnNames = (['R.[CSTDA_OtherPsychiatric]  '])}
                    break;
                    case 'CSTDA-Vision Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherVision]  '])
                    }else{columnNames = (['R.[CSTDA_OtherVision]  '])}  
                    break;
                    case 'CSTDA-Hearing Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherHearing]  '])
                    }else{columnNames = (['R.[CSTDA_OtherHearing]  '])} 
                    break;
                    case 'CSTDA-Speech Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_OtherSpeech]  '])
                    }else{columnNames = (['R.[CSTDA_OtherSpeech]  '])}  
                    break;
                    case 'CSTDA-Developmental Delay Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_DevelopDelay]  '])
                    }else{columnNames = (['R.[CSTDA_DevelopDelay]  '])}  
                    break;
                    case 'CSTDA-Disability Likely To Be Permanent':
                    var Disabilitylikelyoperm = "(CASE R.[PermDisability]  WHEN 1 THEN 'True' else 'False' END)"
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Disabilitylikelyoperm +'  '])
                    }else{columnNames = ([Disabilitylikelyoperm +'  '])}  
                    break;
                    case 'CSTDA-Support Needs-Self Care':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_SelfCare]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_SelfCare]  '])}  
                    break;
                    case 'CSTDA-Support Needs-Mobility':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Mobility]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Mobility]  '])}  
                    break;
                    case 'CSTDA-Support Needs-Communication':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Communication]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Communication]  '])}  
                    break;
                    case 'CSTDA-Support Needs-Interpersonal':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Relationships]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Relationships]  '])}  
                    break; 
                    case 'CSTDA-Support Needs-Learning':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Learning]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Learning]  '])} 
                    break;
                    case 'CSTDA-Support Needs-Education':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Education]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Education]  '])}
                    break;
                    case 'CSTDA-Support Needs-Community':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Community]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Community]  '])} 
                    break;
                    case 'CSTDA-Support Needs-Domestic':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Domestic]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Domestic]  '])}  
                    break;
                    case 'CSTDA-Support Needs-Working':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_SupportNeeds_Domestic]  '])
                    }else{columnNames = (['R.[CSTDA_SupportNeeds_Domestic]  '])}
                    break;                    
                    case 'CSTDA-Carer-Existence Of Informal':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_CarerExists]  '])
                    }else{columnNames = (['R.[CSTDA_CarerExists]  '])}  
                    break;
                    case 'CSTDA-Carer-Assists client in ADL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_CarerStatus]  '])
                    }else{columnNames = (['R.[CSTDA_CarerStatus]  '])} 
                    break;
                    case 'CSTDA-Carer-Lives In Same Household':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_CarerResidencyStatus]  '])
                    }else{columnNames = (['R.[CSTDA_CarerResidencyStatus]  '])}  
                    break;
                    case 'CSTDA-Carer-Relationship':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_CarerRelationship]  '])
                    }else{columnNames = (['R.[CSTDA_CarerRelationship]  '])}  
                    break;
                    case 'CSTDA-Carer-Age Group':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_CarerAgeGroup]  '])
                    }else{columnNames = (['R.[CSTDA_CarerAgeGroup]  '])}  
                    break;
                    case 'CSTDA-Carer Allowance to Guardians':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_CarerAllowance]  '])
                    }else{columnNames = (['R.[CSTDA_CarerAllowance]  '])} 
                    break;
                    case 'CSTDA-Labor Force Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_LaborStatus]  '])
                    }else{columnNames = (['R.[CSTDA_LaborStatus]  '])}  
                    break;
                    case 'CSTDA-Main Source Of Income':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_MainIncome]  '])
                    }else{columnNames = (['R.[CSTDA_MainIncome]  '])}  
                    break;
                    case 'CSTDA-Current Individual Funding':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CSTDA_FundingStatus]  '])
                    }else{columnNames = (['R.[CSTDA_FundingStatus]  '])} 
                    break;
                    //NRCP Dataset Fields                      
                    case 'NRCP-First Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[FirstName]  '])
                    }else{columnNames = (['R.[FirstName]  '])}  
                    break;         
                    case 'NRCP-Surname':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Surname/Organisation]  '])
                    }else{columnNames = (['R.[Surname/Organisation]  '])}  
                    break;
                    case 'NRCP-Date Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DateOfBirth]  '])
                    }else{columnNames = (['R.[DateOfBirth]  '])} 
                    break;
                    case 'NRCP-Gender':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Gender]  '])
                    }else{columnNames = (['R.[Gender]  '])}  
                    break;
                    case 'NRCP-Suburb':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Suburb]  '])
                    }else{columnNames = (['R.[Suburb]  '])}  
                    break;
                    case 'NRCP-Country Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CountryOfBirth]  '])
                    }else{columnNames = (['R.[CountryOfBirth]  '])}  
                    break;                
                    case 'NRCP-Preferred Language':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[HomeLanguage]  '])
                    }else{columnNames = (['R.[HomeLanguage]  '])}  
                    break;                    
                    case 'NRCP-Indigenous Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[IndiginousStatus]  '])
                    }else{columnNames = (['R.[IndiginousStatus]  '])} 
                    break;
                    case 'NRCP-Marital Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[MaritalStatus]  '])
                    }else{columnNames = (['R.[MaritalStatus]  '])}  
                    break;
                    case 'NRCP-DVA Card Holder Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_DVA_Card_Holder_Status]  '])
                    }else{columnNames = (['R.[RECIPT_DVA_Card_Holder_Status]  '])}  
                    break;
                    case 'NRCP-Paid Employment Participation':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Paid_Employment_Participation]  '])
                    }else{columnNames = (['R.[RECIPT_Paid_Employment_Participation]  '])} 
                    break;
                    case 'NRCP-Pension Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[NRCP_GovtPensionStatus]  '])
                    }else{columnNames = (['R.[NRCP_GovtPensionStatus]  '])} 
                    break;
                    case 'NRCP-Carer-Date Role Commenced':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Date_Caring_Role_Commenced]  '])
                    }else{columnNames = (['R.[RECIPT_Date_Caring_Role_Commenced]  '])} 
                    break;
                    case 'NRCP-Carer-Role':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Care_Role]  '])
                    }else{columnNames = (['R.[RECIPT_Care_Role]  '])}  
                    break;
                    case 'NRCP-Carer-Need':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Care_Need]  '])
                    }else{columnNames = (['R.[RECIPT_Care_Need]  '])}  
                    break;
                    case 'NRCP-Carer-Number of Recipients':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Number_Care_Recipients]  '])
                    }else{columnNames = (['R.[RECIPT_Number_Care_Recipients]  '])}  
                    break;
                    case 'NRCP-Carer-Time Spent Caring':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Time_Spent_Caring]  '])
                    }else{columnNames = (['R.[RECIPT_Time_Spent_Caring]  '])}  
                    break;
                    case 'NRCP-Carer-Current Use Formal Services':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[NRCP_CurrentUseFormalServices]  '])
                    }else{columnNames = (['R.[NRCP_CurrentUseFormalServices]  '])}  
                    break;
                    case 'NRCP-Carer-Informal Support':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[NRCP_InformalSupport]  '])
                    }else{columnNames = (['R.[NRCP_InformalSupport]  '])}  
                    break;
                    case 'NRCP-Recipient-Challenging Behaviour':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Challenging_Behaviour]  '])
                    }else{columnNames = (['R.[RECIPT_Challenging_Behaviour]  '])}  
                    break;
                    case 'NRCP-Recipient-Primary Disability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Care_Recipients_Primary_Disability]  '])
                    }else{columnNames = (['R.[RECIPT_Care_Recipients_Primary_Disability]  '])}  
                    break;         
                    case 'NRCP-Recipient-Primary Care Needs':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Care_Recipients_Primary_Care_Needs]  '])
                    }else{columnNames = (['R.[RECIPT_Care_Recipients_Primary_Care_Needs]  '])}  
                    break;
                    case 'NRCP-Recipient-Level of Need':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Care_Recipients_Level_Need]  '])
                    }else{columnNames = (['R.[RECIPT_Care_Recipients_Level_Need]  '])} 
                    break;
                    case 'NRCP-Recipient-Primary Carer':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DatasetCarer]  '])
                    }else{columnNames = (['R.[DatasetCarer]  '])}  
                    break;
                    case 'NRCP-Recipient-Carer Relationship':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[NRCP_CarerRelationship]  '])
                    }else{columnNames = (['R.[NRCP_CarerRelationship]  '])} 
                    break;
                    case 'NRCP-Recipient-Carer Co-Resident':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[NRCP_CarerCoResidency]  '])
                    }else{columnNames = (['R.[NRCP_CarerCoResidency]  '])}  
                    break;
                    case 'NRCP-Recipient-Dementia':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[RECIPT_Dementia]  '])
                    }else{columnNames = (['R.[RECIPT_Dementia]  '])}  
                    break;
                      case 'NRCP-CALD Background':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CALDStatus] as [NRCP-CALD Background]  '])
                    }else{columnNames = (['R.[CALDStatus] as [NRCP-CALD Background]  '])}  
                    break; 
                    // "ONI-Core"     
                    /**
                    * 
                    *  */                 
                    case 'ONI-Family Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Surname/Organisation]  '])
                    }else{columnNames = (['R.[Surname/Organisation]  '])}  
                    break;
                    case 'ONI-Title':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Title]  '])
                    }else{columnNames = (['R.[Title]  '])} 
                    break;
                    case 'ONI-First Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[FirstName]  '])
                    }else{columnNames = (['R.[FirstName]  '])}  
                    break;
                    case 'ONI-Other':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[MiddleNames]  '])
                    }else{columnNames = (['R.[MiddleNames]  '])} 
                    break;
                    case 'ONI-Sex':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Gender]  '])
                    }else{columnNames = (['R.[Gender]  '])}  
                    break;
                    case 'ONI-DOB':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DateOfBirth]  '])
                    }else{columnNames = (['R.[DateOfBirth]  '])}  
                    break;
                    case 'ONI-Usual Address-Street':
                    var AddressStreet = " (SELECT TOP 1 Address1 from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AddressStreet +'  '])
                    }else{columnNames = ([AddressStreet +'  '])}  
                    break;
                    case 'ONI-Usual Address-Suburb':
                    var AddressSburb = " (SELECT TOP 1 Suburb from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AddressSburb +'  '])
                    }else{columnNames = ([AddressSburb +'  '])}  
                    break;
                    case 'ONI-Usual Address-Postcode':
                    var AddressPostCode = " (SELECT TOP 1 Postcode from namesandaddresses WHERE personid = R.UniqueID AND Description = '<USUAL>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AddressPostCode+ '  '])
                    }else{columnNames = ([AddressPostCode +'  '])}  
                    break;
                    case 'ONI-Contact Address-Street':
                    var AddressCotactStreet = " (SELECT TOP 1 Address1 from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AddressCotactStreet +'  '])
                    }else{columnNames = ([AddressCotactStreet +'  '])}  
                    break;
                    case 'ONI-Contact Address-Suburb':
                    var AddressCotactSuburb = " (SELECT TOP 1 Suburb from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AddressCotactSuburb +'  '])
                    }else{columnNames = ([AddressCotactSuburb +'  '])} 
                    break;
                    case 'ONI-Contact Address-Postcode':
                    var AddressCotactPostCode = " (SELECT TOP 1 Postcode from namesandaddresses WHERE personid = R.UniqueID AND Description = '<CONTACT>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([AddressCotactPostCode +'  '])
                    }else{columnNames = ([AddressCotactPostCode +'  '])} 
                    break;
                    case 'ONI-Phone-Home':
                    var PhoneHome = " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<HOME>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([PhoneHome + '  '])
                    }else{columnNames = ([PhoneHome +'  '])}  
                    break;
                    case 'ONI-Phone-Work':
                    var PhoneWork = " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<WORK>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([PhoneWork +'  '])
                    }else{columnNames = ([PhoneWork +'  '])}  
                    break;
                    case 'ONI-Phone-Mobile':
                    var PhoneMobile = " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<MOBILE>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([PhoneMobile +'  '])
                    }else{columnNames = ([PhoneMobile + '  '])}  
                    break;
                    case 'ONI-Phone-FAX':
                    var PhoneFax = " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<FAX>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([PhoneFax +'  '])
                    }else{columnNames = ([PhoneFax +'  '])} 
                    break;
                    case 'ONI-EMAIL':
                    var Email = " (SELECT TOP 1 Detail from PhoneFaxOther WHERE personid = R.UniqueID AND [Type] = '<EMAIL>') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Email +'  '])
                    }else{columnNames = ([Email +'  '])}
                    break;
                    case 'ONI-Person 1 Name':
                    var Person1name = " (SELECT TOP 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person1name +'  '])
                    }else{columnNames = ([Person1name +'  '])}  
                    break;
                    case 'ONI-Person 1 Street':
                    var Person1Street = " (SELECT TOP 1 [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person1Street +'  '])
                    }else{columnNames = ([Person1Street +'  '])}
                    break;
                    case 'ONI-Person 1 Suburb':
                    var Person1Suburb = " (SELECT TOP 1 [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person1Suburb +'  '])
                    }else{columnNames = ([Person1Suburb +'  '])}
                    break;
                    case 'ONI-Person 1 Postcode':
                    var Person1PostCode = " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person1PostCode+'  '])
                    }else{columnNames = ([Person1PostCode+'  '])} 
                    break;
                    case 'ONI-Person 1 Phone':
                    var Person1Phone = " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person1Phone+'  '])
                    }else{columnNames = ([Person1Phone+'  '])} 
                    break;
                    case 'ONI-Person 1 Relationship':
                    var Person1Relationship = " (SELECT TOP 1 [Type] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON1') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person1Relationship+'  '])
                    }else{columnNames = ([Person1Relationship+'  '])} 
                    break;
                    case 'ONI-Person 2 Name':
                    var Person2name = " (SELECT TOP 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person2name+'  '])
                    }else{columnNames = ([Person2name+'  '])}  
                    break;
                    case 'ONI-Person 2 Street':
                    var Person2Street = " (SELECT TOP 1 [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person2Street+'  '])
                    }else{columnNames = ([Person2Street+'  '])}
                    break;
                    case 'ONI-Person 2 Suburb':
                    var Person2Suburb = " (SELECT TOP 1 [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person2Suburb+'  '])
                    }else{columnNames = ([Person2Suburb+'  '])} 
                    break;
                    case 'ONI-Person 2 Postcode':
                    var Person2PostCode = " (SELECT TOP 1 [Postcode] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person2PostCode+'  '])
                    }else{columnNames = ([Person2PostCode+'  '])} 
                    break;
                    case 'ONI-Person 2 Phone':
                    var Person2Phone = " (SELECT TOP 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person2Phone+'  '])
                    }else{columnNames = ([Person2Phone+ ' '])}  
                    break;
                    case 'ONI-Person 2 Relationship':
                    var Person2Relationship = " (SELECT TOP 1 [Type] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'PERSON2') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Person2Relationship+'  '])
                    }else{columnNames = ([Person2Relationship+'  '])}
                    break;
                    case 'ONI-Doctor Name':
                    var Doctorname = " (SELECT Top 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([Doctorname+'  '])
                    }else{columnNames = ([Doctorname+'  '])}
                    break;
                    case 'ONI-Doctor Street':
                    var DoctorStreet = " (SELECT Top 1 [Address1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DoctorStreet+'  '])
                    }else{columnNames = ([DoctorStreet+'  '])} 
                    break;
                    case 'ONI-Doctor Suburb':
                    var DoctorSuburb = " (SELECT Top 1 [Suburb] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DoctorSuburb+'  '])
                    }else{columnNames = ([DoctorSuburb+'  '])}  
                    break;
                    case 'ONI-Doctor Postcode':
                    var DoctorPostCode = " (SELECT Top 1 [Postcode] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DoctorPostCode+'  '])
                    }else{columnNames = ([DoctorPostCode+'  '])}
                    break;
                    case 'ONI-Doctor Phone':
                    var DoctorPhone = " (SELECT Top 1 [Phone1] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DoctorPhone+'  '])
                    }else{columnNames = ([DoctorPhone+'  '])}
                    break;                           
                    case 'ONI-Doctor FAX':
                    var DoctorFax = " (SELECT Top 1 [FAX] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DoctorFax+'  '])
                    }else{columnNames = ([DoctorFax+'  '])}
                    break;
                    case 'ONI-Doctor EMAIL':
                    var DoctorEmail = " (SELECT Top 1 [Email] from HumanResources WHERE personid = R.UniqueID AND [EquipmentCode] = 'GP' ORDER BY RecordNumber)  "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([DoctorEmail+'  '])
                    }else{columnNames = ([DoctorEmail+'  '])}
                    break;
                    case 'ONI-Referral Source':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ReferralSource]  '])
                    }else{columnNames = (['R.[ReferralSource]  '])}
                    break;
                    case 'ONI-Contact Details':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ReferralContactInfo]  '])
                    }else{columnNames = (['R.[ReferralContactInfo]  '])}
                    break;
                    case 'ONI-Country Of Birth':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CountryOfBirth]  '])
                    }else{columnNames = (['R.[CountryOfBirth]  '])}
                    break;
                    case 'ONI-Indigenous Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[IndiginousStatus]  '])
                    }else{columnNames = (['R.[IndiginousStatus]  '])}
                    break;
                    case 'ONI-Main Language At Home':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[HomeLanguage]  '])
                    }else{columnNames = (['R.[HomeLanguage]  '])}
                    break;
                    case 'ONI-Interpreter Required':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[InterpreterRequired]  '])
                    }else{columnNames = (['R.[InterpreterRequired]  '])}
                    break;
                    
                    case 'ONI-Preferred Language':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[HomeLanguage]  '])
                    }else{columnNames = (['R.[HomeLanguage]  '])}
                    break;
                    case 'ONI-Govt Pension Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[PensionStatus]  '])
                    }else{columnNames = (['R.[PensionStatus]  '])}
                    break;
                    case 'ONI-Pension Benefit Card':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ConcessionNumber]  '])
                    }else{columnNames = (['R.[ConcessionNumber]  '])}
                    break;
                    case 'ONI-Medicare Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[MedicareNumber]  '])
                    }else{columnNames = (['R.[MedicareNumber]  '])}
                    break;
                    case 'ONI-Health Care Card#':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Healthcare#]  '])
                    }else{columnNames = (['R.[Healthcare#]  '])}
                    break;                     
                    case 'ONI-DVA Cardholder Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HACCDVACardHolderStatus]  '])
                    }else{columnNames = (['ONI.[HACCDVACardHolderStatus]  '])}
                    break;
                    case 'ONI-DVA Number':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DVANumber]  '])
                    }else{columnNames = (['R.[DVANumber]  '])}
                    break;
                    case 'ONI-Insurance Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[InsuranceStatus]  '])
                    }else{columnNames = (['R.[InsuranceStatus]  '])}
                    break;
                    case 'ONI-Health Insurer':
                    var healthinsurer = " (SELECT TOP 1 [Type] from HumanResources WHERE personid = R.UniqueID AND [GROUP]= 'HEALTHINSURER') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([healthinsurer +'  '])
                    }else{columnNames = ([healthinsurer +'  '])}
                    break;        
                    case 'ONI-Health Insurance Card#':
                    var healthinsurercardNum = " (SELECT TOP 1 [Name] from HumanResources WHERE personid = R.UniqueID AND [GROUP]= 'HEALTHINSURER') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([healthinsurercardNum +'  '])
                    }else{columnNames = ([healthinsurercardNum +'  '])}
                    break;
                    case 'ONI-Alerts':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[Notes]  '])
                    }else{columnNames = (['R.[Notes]  '])}
                    break;
                    case 'ONI-Rating':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIRating]  '])
                    }else{columnNames = (['R.[ONIRating]  '])}
                    break;
                    case 'ONI-HACC Eligible':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[HACCEligible]  '])
                    }else{columnNames = (['R.[HACCEligible]  '])}
                    break;
                    case 'ONI-Reason For HACC Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DSCEligible]  '])
                    }else{columnNames = (['R.[DSCEligible]  '])}
                    break;
                    case 'ONI-Other Support Eligibility':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[HACCReason]  '])
                    }else{columnNames = (['R.[HACCReason]  '])}
                    break;                                       
                    case 'ONI-Other Support Detail':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[OtherEligibleDetails]  '])
                    }else{columnNames = (['R.[OtherEligibleDetails]  '])}
                    break;
                    case 'ONI-Functional Profile Complete':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfileComplete]  '])
                    }else{columnNames = (['R.[ONIFProfileComplete]  '])}
                    break;
                    case 'ONI-Functional Profile Score 1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile1]  '])
                    }else{columnNames = (['R.[ONIFProfile1]  '])}
                    break;
                    case 'ONI-Functional Profile Score 2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile2]  '])
                    }else{columnNames = (['R.[ONIFProfile2]  '])}
                    break;
                    case 'ONI-Functional Profile Score 3':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile3]  '])
                    }else{columnNames = (['R.[ONIFProfile3]  '])}
                    break;
                    case 'ONI-Functional Profile Score 4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile4]  '])
                    }else{columnNames = (['R.[ONIFProfile4]  '])}
                    break;
                    case 'ONI-Functional Profile Score 5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile5]  '])
                    }else{columnNames = (['R.[ONIFProfile5]  '])}
                    break;
                    case 'ONI-Functional Profile Score 6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile6]  '])
                    }else{columnNames = (['R.[ONIFProfile6]  '])}
                    break;
                    case 'ONI-Functional Profile Score 7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile7]  '])
                    }else{columnNames = (['R.[ONIFProfile7]  '])}
                    break;
                    case 'ONI-Functional Profile Score 8':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile8]  '])
                    }else{columnNames = (['R.[ONIFProfile8]  '])}
                    break;
                    case 'ONI-Functional Profile Score 9':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[ONIFProfile9]  '])
                    }else{columnNames = (['R.[ONIFProfile9]  '])}
                    break;
                    case 'ONI-Main Problem-Description':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIMainIssues.[Description]  '])
                    }else{columnNames = (['ONIMainIssues.[Description]  '])}
                    break;
                    case 'ONI-Main Problem-Action':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIMainIssues.[Action]  '])
                    }else{columnNames = (['ONIMainIssues.[Action]  '])}
                    break;
                    case 'ONI-Other Problem-Description':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONISecondaryIssues.[Description]  '])
                    }else{columnNames = (['ONISecondaryIssues.[Description]  '])}
                    break;
                    case 'ONI-Other Problem-Action':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONISecondaryIssues.[Action]  '])
                    }else{columnNames = (['ONISecondaryIssues.[Action]  '])}
                    break;    
                    case 'ONI-Current Service':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIServices.[Service]  '])
                    }else{columnNames = (['ONIServices.[Service]  '])}
                    break;
                    case 'ONI-Service Contact Details':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIServices.[Information]  '])
                    }else{columnNames = (['ONIServices.[Information]  '])}
                    break;                      
                    case 'ONI-AP-Agency':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[HealthProfessional]  '])
                    }else{columnNames = (['ONIActionPlan.[HealthProfessional]  '])}
                    break;
                    case 'ONI-AP-For':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[For]  '])
                    }else{columnNames = (['ONIActionPlan.[For]  '])}
                    break;
                    case 'ONI-AP-Consent':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[Consent]  '])
                    }else{columnNames = (['ONIActionPlan.[Consent]  '])}
                    break;
                    case 'ONI-AP-Referral':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[Referral]  '])
                    }else{columnNames = (['ONIActionPlan.[Referral]  '])}
                    break;
                    case 'ONI-AP-Transport':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[Transport]  '])
                    }else{columnNames = (['ONIActionPlan.[Transport]  '])}
                    break;
                    case 'ONI-AP-Feedback':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[Feedback]  '])
                    }else{columnNames = (['ONIActionPlan.[Feedback]  '])}
                    break;
                    case 'ONI-AP-Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[Date]  '])
                    }else{columnNames = (['ONIActionPlan.[Date]  '])}
                    break;
                    case 'ONI-AP-Review':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIActionPlan.[Review]  '])
                    }else{columnNames = (['ONIActionPlan.[Review]  '])}
                    break;
                    //  ONI-Functional Profile                      
                    case 'ONI-FPQ1-Housework':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP1_Housework  '])
                    }else{columnNames = (['ONI.[FP1_Housework  '])}
                    break;
                    case 'ONI-FPQ2-GetToPlaces':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP2_WalkingDistance]  '])
                    }else{columnNames = (['ONI.[FP2_WalkingDistance]  '])}
                    break;
                    case 'ONI-FPQ3-Shopping':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP3_Shopping]  '])
                    }else{columnNames = (['ONI.[FP3_Shopping]  '])}
                    break;
                    case 'ONI-FPQ4-Medicine':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP4_Medicine]  '])
                    }else{columnNames = (['ONI.[FP4_Medicine]  '])}
                    break;
                    case 'ONI-FPQ5-Money':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP5_Money]  '])
                    }else{columnNames = (['ONI.[FP5_Money]  '])}
                    break;
                    case 'ONI-FPQ6-Walk':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP6_Walking]  '])
                    }else{columnNames = (['ONI.[FP6_Walking]  '])}
                    break;                 
                    case 'ONI-FPQ7-Bath':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP7_Bathing]  '])
                    }else{columnNames = (['ONI.[FP7_Bathing]  '])}
                    break;
                    case 'ONI-FPQ8-Memory':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP8_Memory]  '])
                    }else{columnNames = (['ONI.[FP8_Memory]  '])}
                    break;                      
                    case 'ONI-FPQ9-Behaviour':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP9_Behaviour]  '])
                    }else{columnNames = (['ONI.[FP9_Behaviour]  '])}
                    break;
                    case 'ONI-FP-Recommend Domestic':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FA_Domestic]  '])
                    }else{columnNames = (['ONI.[FA_Domestic]  '])}
                    break;
                    case 'ONI-FP-Recommend Self Care':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FA_SelfCare]  '])
                    }else{columnNames = (['ONI.[FA_SelfCare]  '])}
                    break;
                    case 'ONI-FP-Recommend Cognition':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FA_Cognition]  '])
                    }else{columnNames = (['ONI.[FA_Cognition]  '])}
                    break;
                    case 'ONI-FP-Recommend Behaviour':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FA_Behaviour]  '])
                    }else{columnNames = (['ONI.[FA_Behaviour]  '])}
                    break;
                    case 'ONI-FP-Has Self Care Aids':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_SelfCare]  '])
                    }else{columnNames = (['ONI.[Aids_SelfCare]  '])}
                    break;
                    case 'ONI-FP-Has Support/Mobility Aids':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_SupportAndMobility]  '])
                    }else{columnNames = (['ONI.[Aids_SupportAndMobility]  '])}
                    break;
                    case 'ONI-FP-Has Communication Aids':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_CommunicationAids]  '])
                    }else{columnNames = (['ONI.[Aids_CommunicationAids]  '])}
                    break;
                    case 'ONI-FP-Has Car Mods':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_CarModifications]  '])
                    }else{columnNames = (['ONI.[Aids_CarModifications]  '])}
                    break;
                    case 'ONI-FP-Has Other Aids':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_Other]  '])
                    }else{columnNames = (['ONI.[Aids_Other]  '])}
                    break;
                    case 'ONI-FP-Other Goods List':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[AidsOtherList]  '])
                    }else{columnNames = (['ONI.[AidsOtherList]  '])}
                    break;
                    case 'ONI-FP-Has Medical Care Aids':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_MedicalCare]  '])
                    }else{columnNames = (['ONI.[Aids_MedicalCare]  '])}
                    
                    break;
                    case 'ONI-FP-Has Reading Aids':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[Aids_Reading]  '])
                    }else{columnNames = (['ONI.[Aids_Reading]  '])}                                    
                    break;                      
                    case 'ONI-FP-Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[FP_Comments]  '])
                    }else{columnNames = (['ONI.[FP_Comments]  '])}
                    break; 
                    //  ONI-Living Arrangements Profile                                      
                    case 'ONI-LA-Living Arrangements':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[LivingArrangements]  '])
                    }else{columnNames = (['R.[LivingArrangements]  '])}
                    break;
                    case 'ONI-LA-Living Arrangements Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_LivingComments]  '])
                    }else{columnNames = (['ONI.[LAP_LivingComments]  '])}
                    break;
                    case 'ONI-LA-Accomodation':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[DwellingAccomodation]  '])
                    }else{columnNames = (['R.[DwellingAccomodation]  '])}
                    break;
                    case 'ONI-LA-Accomodation Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_AccomodationComments]  '])
                    }else{columnNames = (['ONI.[LAP_AccomodationComments]  '])}
                    break;
                    
                    case 'ONI-LA-Employment Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_Employment]  '])
                    }else{columnNames = (['ONI.[LAP_Employment]  '])}
                    break;
                    case 'ONI-LA-Employment Status Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_EmploymentComments]  '])
                    }else{columnNames = (['ONI.[LAP_EmploymentComments]  '])}
                    break;
                    case 'ONI-LA-Mental Health Act Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_MentalHealth]  '])
                    }else{columnNames = (['ONI.[LAP_MentalHealth]  '])}
                    break;
                    case 'ONI-LA-Decision Making Responsibility':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_Decision]  '])
                    }else{columnNames = (['ONI.[LAP_Decision]  '])}
                    break;
                    case 'ONI-LA-Capable Own Decisions':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_DecisionCapable]  '])
                    }else{columnNames = (['ONI.[LAP_DecisionCapable]  '])}
                    break;
                    case 'ONI-LA-Financial Decisions':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_FinancialDecision]  '])
                    }else{columnNames = (['ONI.[LAP_FinancialDecision]  '])}
                    break;              
                    case 'ONI-LA-Cost Of Living Trade Off':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_LivingCostDecision]  '])
                    }else{columnNames = (['ONI.[LAP_LivingCostDecision]  '])}
                    break;
                    case 'ONI-LA-Financial & Legal Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[LAP_LivingCostDecisionComments]  '])
                    }else{columnNames = (['ONI.[LAP_LivingCostDecisionComments]  '])}
                    break;
                    // ONI-Health Conditions Profile 
                    case 'ONI-HC-Overall Health Description':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Overall_General]  '])
                    }else{columnNames = (['ONI.[HC_Overall_General]  '])}
                    break                      
                    case 'ONI-HC-Overall Health Pain':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Overall_Pain]  '])
                    }else{columnNames = (['ONI.[HC_Overall_Pain]  '])}
                    break;
                    case 'ONI-HC-Overall Health Interference':                
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Overall_Interfere]  '])
                    }else{columnNames = (['ONI.[HC_Overall_Interfere]  '])}
                    break;
                    case 'ONI-HC-Vision Reading':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vision_Reading]  '])
                    }else{columnNames = (['ONI.[HC_Vision_Reading]  '])}
                    break;
                    case 'ONI-HC-Vision Distance':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vision_Long]  '])
                    }else{columnNames = (['ONI.[HC_Vision_Long]  '])}
                    break;                   
                    case 'ONI-HC-Hearing':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Hearing]  '])
                    }else{columnNames = (['ONI.[HC_Hearing]  '])}
                    break;
                    case 'ONI-HC-Oral Problems':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Oral]  '])
                    }else{columnNames = (['ONI.[HC_Oral]  '])}
                    break;
                    case 'ONI-HC-Oral Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_OralComments]  '])
                    }else{columnNames = (['ONI.[HC_OralComments]  '])}
                    break;
                    case 'ONI-HC-Speech/Swallow Problems':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Speech]  '])
                    }else{columnNames = (['ONI.[HC_Speech]  '])}
                    break;
                    case 'ONI-HC-Speech/Swallow Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_SpeechComments]  '])
                    }else{columnNames = (['ONI.[HC_SpeechComments]  '])}
                    break;
                    case 'ONI-HC-Falls Problems':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Falls]  '])
                    }else{columnNames = (['ONI.[HC_Falls]  '])}
                    break;
                    case 'ONI-HC-Falls Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_FallsComments]  '])
                    }else{columnNames = (['ONI.[HC_FallsComments]  '])}
                    break;
                    case 'ONI-HC-Feet Problems':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Feet]  '])
                    }else{columnNames = (['ONI.[HC_Feet]  '])}
                    break;
                    case 'ONI-HC-Feet Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_FeetComments]  '])
                    }else{columnNames = (['ONI.[HC_FeetComments]  '])}
                    break;
                    case 'ONI-HC-Vacc. Influenza':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Influenza]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Influenza]  '])}
                    break;
                    case 'ONI-HC-Vacc. Influenza Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Influenza_Date]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Influenza_Date]  '])}
                    break;
                    case 'ONI-HC-Vacc. Pneumococcus':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Pneumo]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Pneumo]  '])}
                    break;
                    case 'ONI-HC-Vacc. Pneumococcus  Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Pneumo_Date]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Pneumo_Date]  '])}
                    break;                     
                    case 'ONI-HC-Vacc. Tetanus':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Tetanus]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Tetanus]  '])}
                    break;
                    case 'ONI-HC-Vacc. Tetanus Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Tetanus_Date]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Tetanus_Date]  '])}
                    break;
                    case 'ONI-HC-Vacc. Other':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Other]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Other]  '])}
                    break;
                    case 'ONI-HC-Vacc. Other Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Vac_Other_Date]  '])
                    }else{columnNames = (['ONI.[HC_Vac_Other_Date]  '])}
                    break;
                    case 'ONI-HC-Driving MV':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Driving]  '])
                    }else{columnNames = (['ONI.[HC_Driving]  '])}
                    break;
                    case 'ONI-HC-Driving Fit':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_FitToDrive]  '])
                    }else{columnNames = (['ONI.[HC_FitToDrive]  '])}
                    break;
                    case 'ONI-HC-Driving Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_DrivingComments]  '])
                    }else{columnNames = (['ONI.[HC_DrivingComments]  '])}
                    break;
                    case 'ONI-HC-Continence Urinary':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Continence_Urine]  '])
                    }else{columnNames = (['ONI.[HC_Continence_Urine]  '])}
                    break;
                    case 'ONI-HC-Urinary Related To Coughing':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Continence_Urine_Sneeze]  '])
                    }else{columnNames = (['ONI.[HC_Continence_Urine_Sneeze]  '])}
                    break;
                    
                    case 'ONI-HC-Faecal Continence':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Continence_Faecal]  '])
                    }else{columnNames = (['ONI.[HC_Continence_Faecal]  '])}
                    break;          
                    case 'ONI-HC-Continence Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_ContinenceComments]  '])
                    }else{columnNames = (['ONI.[HC_ContinenceComments]  '])}
                    break;
                    case 'ONI-HC-Weight':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Weight]  '])
                    }else{columnNames = (['ONI.[HC_Weight]  '])}
                    break;
                    case 'ONI-HC-Height':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Height]  '])
                    }else{columnNames = (['ONI.[HC_Height]  '])}
                    break;
                    case 'ONI-HC-BMI':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_BMI]  '])
                    }else{columnNames = (['ONI.[HC_BMI]  '])}
                    break;
                    case 'ONI-HC-BP Systolic':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_BP_Systolic]  '])
                    }else{columnNames = (['ONI.[HC_BP_Systolic]  '])}
                    break;
                    case 'ONI-HC-BP Diastolic':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_BP_Diastolic]  '])
                    }else{columnNames = (['ONI.[HC_BP_Diastolic]  '])}
                    break;
                    case 'ONI-HC-Pulse Rate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_PulseRate]  '])
                    }else{columnNames = (['ONI.[HC_PulseRate]  '])}
                    break;          
                    case 'ONI-HC-Pulse Regularity':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Pulse]  '])
                    }else{columnNames = (['ONI.[HC_Pulse]  '])}
                    break;
                    case 'ONI-HC-Check Postural Hypotension':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_PHCheck]  '])
                    }else{columnNames = (['ONI.[HC_PHCheck]  '])}
                    break;                                                                  
                    case 'ONI-HC-Conditions':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIHealthConditions.[Description]  '])
                    }else{columnNames = (['ONIHealthConditions.[Description]  '])}
                    break;
                    case 'ONI-HC-Diagnosis':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MDiagnosis.[Description]  '])
                    }else{columnNames = (['MDiagnosis.[Description]  '])}
                    break;
                    case 'ONI-HC-Medicines':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONIMedications.[Description]  '])
                    }else{columnNames = (['ONIMedications.[Description]  '])}
                    break;
                    case 'ONI-HC-Take Own Medication':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Med_TakeOwn]  '])
                    }else{columnNames = (['ONI.[HC_Med_TakeOwn]  '])}
                    break;
                    case 'ONI-HC-Willing When Prescribed':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Med_Willing]  '])
                    }else{columnNames = (['ONI.[HC_Med_Willing]  '])}
                    break;              
                    case 'ONI-HC-Coop With Health Services':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Med_Coop]  '])
                    }else{columnNames = (['ONI.[HC_Med_Coop]  '])}
                    break;
                    case 'ONI-HC-Webster Pack':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Med_Webster]  '])
                    }else{columnNames = (['ONI.[HC_Med_Webster]  '])}
                    break;
                    case 'ONI-HC-Medication Review':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_Med_Review]  '])
                    }else{columnNames = (['ONI.[HC_Med_Review]  '])}
                    break;
                    case 'ONI-HC-Medical Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HC_MedComments]  '])
                    }else{columnNames = (['ONI.[HC_MedComments]  '])}
                    break;
                    //ONI-Psychosocial Profile                      
                    case 'ONI-PS-K10-1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_1]  '])
                    }else{columnNames = (['ONI.[PP_K10_1]  '])}
                    break;
                    case 'ONI-PS-K10-2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_2]  '])
                    }else{columnNames = (['ONI.[PP_K10_2]  '])}
                    break;
                    case 'ONI-PS-K10-3':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_3]  '])
                    }else{columnNames = (['ONI.[PP_K10_3]  '])}
                    break;
                    case 'ONI-PS-K10-4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_4]  '])
                    }else{columnNames = (['ONI.[PP_K10_4]  '])}
                    break;
                    case 'ONI-PS-K10-5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_5]  '])
                    }else{columnNames = (['ONI.[PP_K10_5]  '])}
                    break;
                    case 'ONI-PS-K10-6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_6]  '])
                    }else{columnNames = (['ONI.[PP_K10_6]  '])}
                    break;
                    case 'ONI-PS-K10-7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_7]  '])
                    }else{columnNames = (['ONI.[PP_K10_7]  '])}
                    break;                      
                    case 'ONI-PS-K10-8':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_8]  '])
                    }else{columnNames = (['ONI.[PP_K10_8]  '])}
                    break;
                    case 'ONI-PS-K10-9':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_9]  '])
                    }else{columnNames = (['ONI.[PP_K10_9]  '])}
                    break;
                    case 'ONI-PS-K10-10':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_K10_10]  '])
                    }else{columnNames = (['ONI.[PP_K10_10]  '])}
                    break;
                    case 'ONI-PS-Sleep Difficulty':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_SleepingDifficulty]  '])
                    }else{columnNames = (['ONI.[PP_SleepingDifficulty]  '])}
                    break;
                    case 'ONI-PS-Sleep Details':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_SleepingDifficultyComments]  '])
                    }else{columnNames = (['ONI.[PP_SleepingDifficultyComments]  '])}
                    break;
                    case 'ONI-PS-Personal Support':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_PersonalSupport]  '])
                    }else{columnNames = (['ONI.[PP_PersonalSupport]  '])}
                    break;
                    case 'ONI-PS-Personal Support Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['NI.[PP_PersonalSupportComments]  '])
                    }else{columnNames = (['NI.[PP_PersonalSupportComments]  '])}
                    break;
                    case 'ONI-PS-Keep Friendships':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_Relationships_KeepUp]  '])
                    }else{columnNames = (['ONI.[PP_Relationships_KeepUp]  '])}
                    break;
                    case 'ONI-PS-Problems Interacting':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_Relationships_Problem]  '])
                    }else{columnNames = (['ONI.[PP_Relationships_Problem]  '])}
                    break;
                    case 'ONI-PS-Family/Relationship Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_RelationshipsComments]  '])
                    }else{columnNames = (['ONI.[PP_RelationshipsComments]  '])}
                    break;
                    case 'ONI-PS-Svc Provider Relations':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_Relationships_SP]  '])
                    }else{columnNames = (['ONI.[PP_Relationships_SP]  '])}
                    break;
                    case 'ONI-PS-Svc Provider Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[PP_Relationships_SPComments]  '])
                    }else{columnNames = (['ONI.[PP_Relationships_SPComments]  '])}
                    break;
                    //ONI-Health Behaviours Profile                      
                    case 'ONI-HB-Regular Health Checks':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_HealthChecks]  '])
                    }else{columnNames = (['ONI.[HBP_HealthChecks]  '])}
                    break;
                    case 'ONI-HB-Last Health Check':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_HealthChecks_Last]  '])
                    }else{columnNames = (['ONI.[HBP_HealthChecks_Last]  '])}
                    break;                                    
                    case 'ONI-HB-Health Screens':
                    var healthscreen = " Convert(nVarChar(4000), ONI.[HBP_HealthChecks_List]) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([healthscreen+'  '])
                    }else{columnNames = ([healthscreen+'  '])}
                    break;
                    case 'ONI-HB-Smoking':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Smoking]  '])
                    }else{columnNames = (['ONI.[HBP_Smoking]  '])}
                    break;
                    case 'ONI-HB-If Quit Smoking - When?':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Smoking_Quit]  '])
                    }else{columnNames = (['ONI.[HBP_Smoking_Quit]  '])}
                    break;
                    case 'ONI-HB-Alcohol-How often?':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Alcohol]  '])
                    }else{columnNames = (['ONI.[HBP_Alcohol]  '])}
                    break;
                    case 'ONI-HB-Alcohol-How many?':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Alcohol_NoDrinks]  '])
                    }else{columnNames = (['ONI.[HBP_Alcohol_NoDrinks]  '])}
                    break;
                    case 'ONI-HB-Alcohol-How often over 6?':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Alcohol_BingeNo]  '])
                    }else{columnNames = (['ONI.[HBP_Alcohol_BingeNo]  '])}
                    break;
                    case 'ONI-HB-Lost Weight':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Malnutrition_LostWeight]  '])
                    }else{columnNames = (['ONI.[HBP_Malnutrition_LostWeight]  '])}
                    break;
                    case 'ONI-HB-Eating Poorly':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Malnutrition_PoorEating]  '])
                    }else{columnNames = (['ONI.[HBP_Malnutrition_PoorEating]  '])}
                    break;
                    case 'ONI-HB-How much wieght lost':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Malnutrition_LostWeightAmount]  '])
                    }else{columnNames = (['ONI.[HBP_Malnutrition_LostWeightAmount]  '])}
                    break;
                    case 'ONI-HB-Malnutrition Score':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Malnutrition_Score]  '])
                    }else{columnNames = (['ONI.[HBP_Malnutrition_Score]  '])}
                    break;
                    case 'ONI-HB-8 cups fluid':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Hydration_AdequateFluid]  '])
                    }else{columnNames = (['ONI.[HBP_Hydration_AdequateFluid]  '])}
                    break;
                    case 'ONI-HB-Recent decrease in fluid':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Hydration_DecreasedFluid]  '])
                    }else{columnNames = (['ONI.[HBP_Hydration_DecreasedFluid]  '])}
                    break;
                    case 'ONI-HB-Weight':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Weight]  '])
                    }else{columnNames = (['ONI.[HBP_Weight]  '])}
                    break;
                    case 'ONI-HB-Physical Activity':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_PhysicalActivity]  '])
                    }else{columnNames = (['ONI.[HBP_PhysicalActivity]  '])}
                    break;
                    case 'ONI-HB-Physical Fitness':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_PhysicalFitness]  '])
                    }else{columnNames = (['ONI.[HBP_PhysicalFitness]  '])}
                    break;
                    case 'ONI-HB-Fitness Comments':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[HBP_Comments]  '])
                    }else{columnNames = (['ONI.[HBP_Comments]  '])}
                    break;
                    //ONI-CP-Need for carer     
                    case 'ONI-CP-Need for carer':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_General_NeedForCarer]  '])
                    }else{columnNames = (['ONI.[C_General_NeedForCarer]  '])}
                    break;                                       
                    case 'ONI-CP-Carer Availability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CarerAvailability]  '])
                    }else{columnNames = (['R.[CarerAvailability]  '])}
                    break;                            
                    case 'ONI-CP-Carer Residency Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CarerResidency]  '])
                    }else{columnNames = (['R.[CarerResidency]  '])}
                    break;
                    case 'ONI-CP-Carer Relationship':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['R.[CarerRelationship]  '])
                    }else{columnNames = (['R.[CarerRelationship]  '])}
                    break;
                    case 'ONI-CP-Carer has help':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Support_Help]  '])
                    }else{columnNames = (['ONI.[C_Support_Help]  '])}
                    break;
                    case 'ONI-CP-Carer receives payment':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Support_Allowance]  '])
                    }else{columnNames = (['ONI.[C_Support_Allowance]  '])}
                    break;
                    case 'ONI-CP-Carer made aware support services':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Support_Information]  '])
                    }else{columnNames = (['ONI.[C_Support_Information]  '])}
                    break;        
                    case 'ONI-CP-Carer needs training':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Support_NeedTraining]  '])
                    }else{columnNames = (['ONI.[C_Support_NeedTraining]  '])}
                    break;
                    case 'ONI-CP-Carer threat-emotional':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Threats_Emotional]  '])
                    }else{columnNames = (['ONI.[C_Threats_Emotional]  '])}
                    break;
                    case 'ONI-CP-Carer threat-acute physical':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Threats_Physical]  '])
                    }else{columnNames = (['ONI.[C_Threats_Physical]  '])}
                    break;
                    case 'ONI-CP-Carer threat-slow physical':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Threats_Physical_Slow]  '])
                    }else{columnNames = (['ONI.[C_Threats_Physical_Slow]  '])}
                    break;
                    case 'ONI-CP-Carer threat-other factors':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Threats_Unrelated]  '])
                    }else{columnNames = (['ONI.[C_Threats_Unrelated]  '])}
                    break;
                    case 'ONI-CP-Carer threat-increasing consumer needs':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Threats_ConsumerNeeds]  '])
                    }else{columnNames = (['ONI.[C_Threats_ConsumerNeeds]  '])}
                    break;
                    case 'ONI-CP-Carer threat-other comsumer factors':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Threats_ConsumerOther]  '])
                    }else{columnNames = (['ONI.[C_Threats_ConsumerOther]  '])}
                    break;
                    case 'ONI-CP-Carer arrangements sustainable':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Issues_Sustainability]  '])
                    }else{columnNames = (['ONI.[C_Issues_Sustainability]  '])}
                    break;
                    case 'ONI-CP-Carer Comments': 
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[C_Issues_Comments]  '])
                    }else{columnNames = (['ONI.[C_Issues_Comments]  '])}
                    break;
                    //ONI-CS-Year of Arrival   
                    
                    case 'ONI-CS-Year of Arrival':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_ArrivalYear]  '])
                    }else{columnNames = (['ONI.[CAL_ArrivalYear]  '])}
                    break;                   
                    case 'ONI-CS-Citizenship Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_Citizenship]  '])
                    }else{columnNames = (['ONI.[CAL_Citizenship]  '])}
                    break;
                    case 'ONI-CS-Reasons for moving to Australia':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_ReasonsMoveAustralia]  '])
                    }else{columnNames = (['ONI.[CAL_ReasonsMoveAustralia]  '])}
                    break;
                    case 'ONI-CS-Primary/Secondary Language Fluency':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_PrimSecLanguage]  '])
                    }else{columnNames = (['ONI.[CAL_PrimSecLanguage]  '])}
                    break;
                    case 'ONI-CS-Fluency in English':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_EnglishProf]  '])
                    }else{columnNames = (['ONI.[CAL_EnglishProf]  '])}
                    break;
                    case 'ONI-CS-Literacy in primary language':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_PrimaryLiteracy]  '])
                    }else{columnNames = (['ONI.[CAL_PrimaryLiteracy]  '])}
                    break;
                    case 'ONI-CS-Literacy in English':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_EnglishLiteracy]  '])
                    }else{columnNames = (['ONI.[CAL_EnglishLiteracy]  '])}
                    break;
                    case 'ONI-CS-Non verbal communication style':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_NonVerbalStyle]  '])
                    }else{columnNames = (['ONI.[CAL_NonVerbalStyle]  '])}
                    break;
                    case 'ONI-CS-Marital Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_Marital]  '])
                    }else{columnNames = (['ONI.[CAL_Marital]  '])}
                    break;
                    case 'ONI-CS-Religion':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_Religion]  '])
                    }else{columnNames = (['ONI.[CAL_Religion]  '])}
                    break;
                    case 'ONI-CS-Employment history in country of origin':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_EmploymentHistory]  '])
                    }else{columnNames = (['ONI.[CAL_EmploymentHistory]  '])}
                    break;
                    case 'ONI-CS-Employment history in Australia':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_EmploymentHistoryAust]  '])
                    }else{columnNames = (['ONI.[CAL_EmploymentHistoryAust]  '])}
                    break;
                    case 'ONI-CS-Specific dietary needs':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_DietaryNeeds]  '])
                    }else{columnNames = (['ONI.[CAL_DietaryNeeds]  '])}
                    break;
                    case 'ONI-CS-Specific cultural needs':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CAL_SpecificCulturalNeeds]  '])
                    }else{columnNames = (['ONI.[CAL_SpecificCulturalNeeds]  '])}
                    break;            
                    case 'ONI-CS-Someone to talk to for day to day problems':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_1]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_1]  '])}
                    break;
                    case 'ONI-CS-Miss having close freinds':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_2]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_2]  '])}
                    break;                                            
                    case 'ONI-CS-Experience general sense of emptiness':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_3]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_3]  '])}
                    break;
                    case 'ONI-CS-Plenty of people to lean on for problems':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_4]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_4]  '])}
                    break;
                    case 'ONI-CS-Miss the pleasure of the company of others':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_5]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_5]  '])}
                    break;
                    case 'ONI-CS-Circle of friends and aquaintances too limited':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_6]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_6]  '])}
                    break;
                    case 'ONI-CS-Many people I trust completely':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_7]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_7]  '])}
                    break;                                                           
                    case 'ONI-CS-Enough people I feel close to':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_8]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_8]  '])}
                    break;
                    case 'ONI-CS-Miss having people around':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_9]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_9]  '])}
                    break;
                    case 'ONI-CS-Often feel rejected':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_10]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_10]  '])}
                    break;
                    case 'ONI-CS-Can call on my friends whenever I need them':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['ONI.[CALSocIsol_11]  '])
                    }else{columnNames = (['ONI.[CALSocIsol_11]  '])}
                    break;
                    //Loan Items                      
                    case 'Loan Item Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLoan.[Type]  '])
                    }else{columnNames = (['HRLoan.[Type]  '])}
                    break;
                    case 'Loan Item Description':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLoan.[Name]  '])
                    }else{columnNames = (['HRLoan.[Name]  '])}
                    break;
                    case 'Loan Item Date Loaned/Installed':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLoan.[Date1] as [Loan Item Installed] '])
                    }else{columnNames = (['HRLoan.[Date1] as [Loan Item Installed] '])}
                    break;                      
                    case 'Loan Item Date Collected':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRLoan.[Date2] as [loan Item Collected Date] '])
                    }else{columnNames = (['HRLoan.[Date2] as [loan Item Collected Date] '])}
                    break;
                    //  service information Fields                      
                    case 'Staff Code':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Carer Code]  '])
                    }else{columnNames = (['SvcDetail.[Carer Code]  '])}
                    break;
                    case 'Service Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.Date  '])
                    }else{columnNames = (['SvcDetail.Date  '])}
                    break;
                    case 'Service Start Time':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Start Time]  '])
                    }else{columnNames = (['SvcDetail.[Start Time]  '])}
                    break;
                    case 'Service Code':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Service Type]  '])
                    }else{columnNames = (['SvcDetail.[Service Type]  '])}
                    break;                      
                    case 'Service Hours':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['(SvcDetail.[Duration]*5) / 60  '])
                    }else{columnNames = (['(SvcDetail.[Duration]*5) / 60  '])}
                    break;
                    case 'Service Pay Rate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Unit Pay Rate]  '])
                    }else{columnNames = (['SvcDetail.[Unit Pay Rate]  '])}
                    break;
                    case 'Service Bill Rate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Unit Bill Rate]  '])
                    }else{columnNames = (['SvcDetail.[Unit Bill Rate]  '])}
                    break;
                    case 'Service Bill Qty':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[BillQty]  '])
                    }else{columnNames = (['SvcDetail.[BillQty]  '])}
                    break;
                    case 'Service Location/Activity Group':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[ServiceSetting]  '])
                    }else{columnNames = (['SvcDetail.[ServiceSetting]  '])}
                    break;
                    case 'Service Program':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Program]  '])
                    }else{columnNames = (['SvcDetail.[Program]  '])}
                    break;
                    case 'Service Group':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Type]  '])
                    }else{columnNames = (['SvcDetail.[Type]  '])}
                    break;
                    case 'Service HACC Type': 
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[HACCType]  '])
                    }else{columnNames = (['SvcDetail.[HACCType]  '])}
                    break;
                    case 'Service Category':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Anal]  '])
                    }else{columnNames = (['SvcDetail.[Anal]  '])}
                    break;
                    case 'Service Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Status]  '])
                    }else{columnNames = (['SvcDetail.[Status]  '])}
                    break;
                    case 'Service Pay Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[Service Description]  '])
                    }else{columnNames = (['SvcDetail.[Service Description]  '])}
                    break;
                    case 'Service Pay Qty':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcDetail.[CostQty]  '])
                    }else{columnNames = (['SvcDetail.[CostQty]  '])}
                    break;
                    case 'Service End Time/ Shift End Time':
                    var endtime = " Convert(varchar,DATEADD(minute,(SvcDetail.[Duration]*5) ,SvcDetail.[Start Time]),108) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([endtime+'  '])
                    }else{columnNames = ([endtime+'  '])}
                    break;
                    case 'Service Funding Source':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Humanresourcetypes.[Type]  '])
                    }else{columnNames = (['Humanresourcetypes.[Type]  '])}
                    break;  
                    case 'Service Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CAST(History.Detail AS varchar(4000))  '])
                    }else{columnNames = (['CAST(History.Detail AS varchar(4000))  '])}
                    break;
                    //Service Specific Competencies           
                    case 'Activity':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcSpecCompetency.[Group]  '])
                    }else{columnNames = (['SvcSpecCompetency.[Group]  '])}
                    break;
                    case 'Competency':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['SvcSpecCompetency.[Name]  '])
                    }else{columnNames = (['SvcSpecCompetency.[Name]  '])}
                    break;
                    case 'S Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([' ServiceCompetencyStatus.[ServiceStatus] '])
                    }else{columnNames = ([' ServiceCompetencyStatus.[ServiceStatus] '])}
                    break;
                    //  RECIPIENT OP NOTES                      
                    case 'OP Notes Date':
                    var notesdate = " format(CONVERT(datetime,[DetailDate],102),'dd/MM/yyyy') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([notesdate +' '])
                    }else{columnNames = ([notesdate +' '])}
                    break;
                    case 'OP Notes Detail':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['dbo.RTF2TEXT(OPHistory.[Detail])  '])
                    }else{columnNames = (['dbo.RTF2TEXT(OPHistory.[Detail])  '])}
                    break;
                    case 'OP Notes Creator':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['OPHistory.[Creator]  '])
                    }else{columnNames = (['OPHistory.[Creator]  '])}
                    break;                                              
                    case 'OP Notes Alarm':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['OPHistory.[AlarmDate]  '])
                    }else{columnNames = (['OPHistory.[AlarmDate]  '])}
                    break;
                    case 'OP Notes Program':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['OPHistory.[Program]  '])
                    }else{columnNames = (['OPHistory.[Program]  '])}
                    break;
                    case 'OP Notes Category':
                    var NotesCategory = " CASE WHEN IsNull(OPHistory.ExtraDetail2,'') < 'A' THEN 'UNKNOWN' ELSE OPHistory.ExtraDetail2 END "                
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([NotesCategory + '  '])
                    }else{columnNames = ([NotesCategory +'  '])}
                    break;
                    // RECIPIENT CLINICAL NOTES                      
                    case 'Clinical Notes Date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CliniHistory.[DetailDate]  '])
                    }else{columnNames = (['CliniHistory.[DetailDate]  '])}
                    break;
                    case 'Clinical Notes Detail':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CliniHistory.[Detail])  '])
                    }else{columnNames = (['CliniHistory.[Detail])  '])}
                    break;
                    case 'Clinical Notes Creator':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CliniHistory.[Creator]  '])
                    }else{columnNames = (['CliniHistory.[Creator]  '])}
                    break;
                    
                    case 'Clinical Notes Alarm':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CliniHistory.[AlarmDate]  '])
                    }else{columnNames = (['CliniHistory.[AlarmDate]  '])}
                    break;
                    case 'Clinical Notes Category':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CliniHistory.[ExtraDetail2]  '])
                    }else{columnNames = (['CliniHistory.[ExtraDetail2]  '])}
                    break;
                    // RECIPIENT INCIDENTS                      
                    case 'INCD_Status':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Status  '])
                    }else{columnNames = (['IMM.Status  '])}
                    break;
                    case 'INCD_Date':
                    if(columnNames.length >= 1){
                      var incdDate = " format(CONVERT(datetime,IMM.Date,102),'dd/MM/yyyy') "
                      columnNames = columnNames.concat([incdDate + '  '])
                    }else{columnNames = ([incdDate +'  '])}
                    break;
                    case 'INCD_Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.[Type]  '])
                    }else{columnNames = (['IMM.[Type]  '])}
                    break;
                    case 'INCD_Description':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.ShortDesc  '])
                    }else{columnNames = (['IMM.ShortDesc  '])}
                    break;
                    case 'INCD_SubCategory':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.PerpSpecify  '])
                    }else{columnNames = (['IMM.PerpSpecify  '])}
                    break;
                    case 'INCD_Assigned_To':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.CurrentAssignee  '])
                    }else{columnNames = (['IMM.CurrentAssignee  '])}
                    break;         
                    case 'INCD_Service':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Service  '])
                    }else{columnNames = (['IMM.Service  '])}
                    break;
                    case 'INCD_Severity':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Severity  '])
                    }else{columnNames = (['IMM.Severity  '])}
                    break;
                    case 'INCD_Time':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Time  '])
                    }else{columnNames = (['IMM.Time  '])}
                    break;
                    case 'INCD_Duration':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Duration  '])
                    }else{columnNames = (['IMM.Duration  '])}
                    break;
                    case 'INCD_Location':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Location  '])
                    }else{columnNames = (['IMM.Location  '])}
                    break;
                    case 'INCD_LocationNotes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.LocationNotes)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.LocationNotes)  '])}
                    break;
                    case 'INCD_ReportedBy':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.ReportedBy  '])
                    }else{columnNames = (['IMM.ReportedBy  '])}
                    break;
                    case 'INCD_DateReported':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Convert(varchar,IMM.DateReported,111)  '])
                    }else{columnNames = (['Convert(varchar,IMM.DateReported,111)  '])}
                    break;
                    case 'INCD_Reported':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Reported  '])
                    }else{columnNames = (['IMM.Reported  '])}
                    break;
                    case 'INCD_FullDesc':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.FullDesc)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.FullDesc)  '])}
                    break;
                    case 'INCD_Program':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Program  '])
                    }else{columnNames = (['IMM.Program  '])}
                    break;
                    case 'INCD_DSCServiceType':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.DSCServiceType  '])
                    }else{columnNames = (['IMM.DSCServiceType  '])}
                    break;
                    case 'INCD_TriggerShort':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.TriggerShort  '])
                    }else{columnNames = (['IMM.TriggerShort  '])}
                    break;
                    case 'INCD_incident_level':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.incident_level  '])
                    }else{columnNames = (['IMM.incident_level  '])}
                    break;
                    case 'INCD_Area':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.area  '])
                    }else{columnNames = (['IMM.area  '])}
                    break;
                    case 'INCD_Region':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Region  '])
                    }else{columnNames = (['IMM.Region  '])}
                    break;
                    case 'INCD_position':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.position  '])
                    }else{columnNames = (['IMM.position  '])}
                    break;
                    case 'INCD_phone':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.phone  '])
                    }else{columnNames = (['IMM.phone  '])}
                    break;
                    case 'INCD_verbal_date':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['Convert(varchar,IMM.verbal_date,111)  '])
                    }else{columnNames = (['Convert(varchar,IMM.verbal_date,111)  '])}
                    break;
                    case 'INCD_verbal_time':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.verbal_time  '])
                    }else{columnNames = (['IMM.verbal_time  '])}
                    break;
                    case 'INCD_By_Whom':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.By_Whome  '])
                    }else{columnNames = (['IMM.By_Whome  '])}
                    break;
                    case 'INCD_To_Whom':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.To_Whome  '])
                    }else{columnNames = (['IMM.To_Whome  '])}
                    break;
                    case 'INCD_BriefSummary':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.BriefSummary  '])
                    }else{columnNames = (['IMM.BriefSummary  '])}
                    break;
                    case 'INCD_ReleventBackground':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.ReleventBackground)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.ReleventBackground)  '])}
                    break;
                    case 'INCD_SummaryofAction':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.SummaryofAction)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.SummaryofAction)  '])}
                    break;
                    case 'INCD_SummaryOfOtherAction':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.SummaryOfOtherAction)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.SummaryOfOtherAction)  '])}
                    break;
                    case 'INCD_Triggers':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.Triggers)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.Triggers)  '])}
                    break;
                    case 'INCD_InitialAction':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.InitialAction  '])
                    }else{columnNames = (['IMM.InitialAction  '])}
                    break;
                    case 'INCD_InitialNotes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.InitialNotes)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.InitialNotes)  '])}
                    break;
                    case 'INCD_InitialFupBy':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.InitialFupBy  '])
                    }else{columnNames = (['IMM.InitialFupBy  '])}
                    break;
                    case 'INCD_Completed':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Completed  '])
                    }else{columnNames = (['IMM.Completed  '])}
                    break;
                    case 'INCD_OngoingAction':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.OngoingAction  '])
                    }else{columnNames = (['IMM.OngoingAction  '])}
                    break;
                    case 'INCD_OngoingNotes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.OngoingNotes)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.OngoingNotes)  '])}
                    break;
                    case 'INCD_Background':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.Background)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.Background)  '])}
                    break;
                    case 'INCD_Abuse':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Abuse  '])
                    }else{columnNames = (['IMM.Abuse  '])}
                    break;
                    case 'INCD_DOPWithDisability':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.DOPWithDisability  '])
                    }else{columnNames = (['IMM.DOPWithDisability  '])}
                    break;
                    case 'INCD_SeriousRisks':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.SeriousRisks  '])
                    }else{columnNames = (['IMM.SeriousRisks  '])}
                    break;
                    case 'INCD_Complaints':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Complaints  '])
                    }else{columnNames = (['IMM.Complaints  '])}
                    break;                                   
                    case 'INCD_Perpetrator':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Perpetrator  '])
                    }else{columnNames = (['IMM.Perpetrator  '])}
                    break;
                    case 'INCD_Notify':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Notify  '])
                    }else{columnNames = (['IMM.Notify  '])}
                    break;
                    case 'INCD_NoNotifyReason':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.NoNotifyReason)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.NoNotifyReason)  '])}
                    break;
                    case 'INCD_Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['CONVERT(varchar(1000),IMM.Notes)  '])
                    }else{columnNames = (['CONVERT(varchar(1000),IMM.Notes)  '])}
                    break;
                    case 'INCD_Setting':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMM.Setting  '])
                    }else{columnNames = (['IMM.Setting  '])}
                    break;
                    case 'INCD_Involved_Staff':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IMI.Staff#  '])
                    }else{columnNames = (['IMI.Staff#  '])}
                    break;
                    //  Recipient Competencies                      
                    case 'Recipient Competency':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecpCompet.Name  '])
                    }else{columnNames = (['RecpCompet.Name  '])}
                    break;
                    case 'Recipient Competency Mandatory':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecpCompet.Recurring  '])
                    }else{columnNames = (['RecpCompet.Recurring  '])}
                    break;
                    case 'Recipient Competency Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['RecpCompet.[Notes]  '])
                    }else{columnNames = (['RecpCompet.[Notes]  '])}
                    break;
                    //Care Plan                      
                    case 'CarePlan ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['D.Doc#  '])
                    }else{columnNames = (['D.Doc#  '])}
                    break;
                    case 'CarePlan Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['D.Title  '])
                    }else{columnNames = (['D.Title  '])}
                    break;
                    case 'CarePlan Type':
                    var careplantype = " (SELECT Description FROM DataDomains Left join DOCUMENTS D on  DataDomains.RecordNumber = D.SubId) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([careplantype +'  '])
                    }else{columnNames = ([careplantype +'  '])}
                    break;                      
                    case 'CarePlan Program':
                    var careplanprogram = " (SELECT [Name] FROM HumanResourceTypes WHERE HumanResourceTypes.RecordNumber = D.Department AND [Group] = 'PROGRAMS') "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([careplanprogram +'  '])
                    }else{columnNames = ([careplanprogram +'  '])}
                    break;                  
                    case 'CarePlan Discipline':
                    var careplandescipline = " (SELECT [Description] FROM DataDomains WHERE DataDomains.RecordNumber = D.DPID) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([careplandescipline + '  '])
                    }else{columnNames = ([careplandescipline +'  '])}
                    break;
                    case 'CarePlan CareDomain':
                    var careplandomain = " (SELECT [Description] FROM DataDomains WHERE DataDomains.RecordNumber = D.CareDomain) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([careplandomain + '  '])
                    }else{columnNames = ([careplandomain + '  '])}
                    break;
                    case 'CarePlan StartDate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['D.DocStartDate  '])
                    }else{columnNames = (['D.DocStartDate  '])}
                    break;
                    case 'CarePlan SignOffDate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['D.DocEndDate  '])
                    }else{columnNames = (['D.DocEndDate  '])}
                    break;
                    case 'CarePlan ReviewDate':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['D.AlarmDate  '])
                    }else{columnNames = (['D.AlarmDate  '])}
                    break;
                    case 'CarePlan ReminderText':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['D.AlarmText  '])
                    }else{columnNames = (['D.AlarmText  '])}
                    break;
                    case 'CarePlan Archived':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['IsNull(D.DeletedRecord,0)  '])
                    }else{columnNames = (['IsNull(D.DeletedRecord,0)  '])}
                    break;
                    //Mental Health                       
                    case 'MH-PERSONID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[PERSONID]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[PERSONID]  '])}
                    break;
                    case 'MH-HOUSING TYPE ON REFERRAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[HOUSING TYPE ON REFERRAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[HOUSING TYPE ON REFERRAL]  '])}
                    break;
                    case 'MH-RE REFERRAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[RE REFERRAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[RE REFERRAL]  '])}
                    break;
                    case 'MH-REFERRAL SOURCE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[REFERRAL SOURCE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[REFERRAL SOURCE]  '])}
                    break;
                    case 'MH-REFERRAL RECEIVED DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[REFERRAL RECEIVED DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[REFERRAL RECEIVED DATE]  '])}
                    break;
                    case 'MH-ENGAGED AND CONSENT DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ENGAGED AND CONSENT DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ENGAGED AND CONSENT DATE]  '])}
                    break;
                    case 'MH-OPEN TO HOSPITAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[OPEN TO HOSPITAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[OPEN TO HOSPITAL]  '])}
                    break;                
                    case 'MH-OPEN TO HOSPITAL DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[OPEN TO HOSPITAL DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[OPEN TO HOSPITAL DETAILS]  '])}
                    break;
                    case 'MH-ALERTS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ALERTS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ALERTS]  '])}
                    break;
                    case 'MH-ALERTS DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ALERTS DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ALERTS DETAILS]  '])}
                    break;
                    case 'MH-MH DIAGNOSIS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[MH DIAGNOSIS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[MH DIAGNOSIS]  '])}
                    break;
                    case 'MH-MEDICAL DIAGNOSIS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[MEDICAL DIAGNOSIS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[MEDICAL DIAGNOSIS]  '])}
                    break;
                    case 'MH-REASONS FOR EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[REASONS FOR EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[REASONS FOR EXIT]  '])}
                    break;
                    case 'MH-SERVICES LINKED INTO':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[SERVICES LINKED INTO]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[SERVICES LINKED INTO]  '])}
                    break;
                    case 'MH-NON ACCEPTED REASONS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[NON ACCEPTED REASONS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[NON ACCEPTED REASONS]  '])}
                    break;
                    case 'MH-NOT PROCEEDED':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[NOT PROCEEDED]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[NOT PROCEEDED]  '])}
                    break;               
                    case 'MH-DISCHARGE DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[DISCHARGE DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[DISCHARGE DATE]  '])}
                    break;
                    case 'MH-CURRENT AOD':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[CURRENT AOD]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[CURRENT AOD]  '])}
                    break;
                    case 'MH-CURRENT AOD DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[CURRENT AOD DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[CURRENT AOD DETAILS]  '])}
                    break;
                    case 'MH-PAST AOD':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[PAST AOD]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[PAST AOD]  '])}
                    break;
                    case 'MH-PAST AOD DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[PAST AOD DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[PAST AOD DETAILS]  '])}
                    break;
                    case 'MH-ENGAGED AOD':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ENGAGED AOD]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ENGAGED AOD]  '])}
                    break;
                    case 'MH-ENGAGED AOD DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ENGAGED AOD DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ENGAGED AOD DETAILS]  '])}
                    break;
                    case 'MH-SERVICES CLIENT IS LINKED WITH ON INTAKE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[SERVICES CLIENT IS LINKED WITH ON INTAKE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[SERVICES CLIENT IS LINKED WITH ON INTAKE]  '])}
                    break;
                    case 'MH-SERVICES CLIENT IS LINKED WITH ON EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[SERVICES CLIENT IS LINKED WITH ON EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[SERVICES CLIENT IS LINKED WITH ON EXIT]  '])}
                    break;
                    case 'MH-ED PRESENTATIONS ON REFERRAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ED PRESENTATIONS ON REFERRAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ED PRESENTATIONS ON REFERRAL]  '])}
                    break;               
                    case 'MH-ED PRESENTATIONS ON 3 MONTH REVIEW':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ED PRESENTATIONS ON 3 MONTH REVIEW]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ED PRESENTATIONS ON 3 MONTH REVIEW]  '])}
                    break;
                    case 'MH-ED PRESENTATIONS ON EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ED PRESENTATIONS ON EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ED PRESENTATIONS ON EXIT]  '])}
                    break;
                    case 'MH-AMBULANCE ARRIVAL ON REFERRAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON REFERRAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON REFERRAL]  '])}
                    break;
                    case 'MH-AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW]  '])}
                    break;
                    case 'MH-AMBULANCE ARRIVAL ON EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[AMBULANCE ARRIVAL ON EXIT]  '])}
                    break;
                    case 'MH-ADMISSIONS ON REFERRAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ADMISSIONS ON REFERRAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ADMISSIONS ON REFERRAL]  '])}
                    break;
                    case 'MH-ADMISSIONS ON MID-3 MONTH REVIEW':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ADMISSIONS ON MID- 3 MONTH REVIEW]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ADMISSIONS ON MID- 3 MONTH REVIEW]  '])}
                    break;
                    case 'MH-ADMISSIONS TO ED ON TIME OF EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[ADMISSIONS TO ED ON TIME OF EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[ADMISSIONS TO ED ON TIME OF EXIT]  '])}
                    break;                  
                    case 'MH-RESIDENTIAL MOVES':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[RESIDENTIAL MOVES]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[RESIDENTIAL MOVES]  '])}
                    break;
                    case 'MH-DATE OF RESIDENTIAL CHANGE OF ADDRESS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[DATE OF RESIDENTIAL CHANGE OF ADDRESS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[DATE OF RESIDENTIAL CHANGE OF ADDRESS]  '])}
                    break;
                    case 'MH-LOCATION OF NEW ADDRESS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[LOCATION OF NEW ADDRESS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[LOCATION OF NEW ADDRESS]  '])}
                    break;
                    case 'MH-HOUSING TYPE ON EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[HOUSING TYPE ON EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[HOUSING TYPE ON EXIT]  '])}
                    break;
                    case 'MH-KPI - INTAKE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KPI - INTAKE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KPI - INTAKE]  '])}
                    break;
                    case 'MH-KPI - 3 MONTH REVEIEW':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KPI - 3 MONTH REVEIEW]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KPI - 3 MONTH REVEIEW]  '])}
                    break;
                    case 'MH-KPI - EXIT':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KPI - EXIT]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KPI - EXIT]  '])}
                    break;
                    case 'MH-MEDICAL DIAGNOSIS DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[MEDICAL DIAGNOSIS DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[MEDICAL DIAGNOSIS DETAILS]  '])}
                    break;
                    case 'MH-SERVICES LINKED DETAILS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[SERVICES LINKED DETAILS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[SERVICES LINKED DETAILS]  '])}
                    break;
                    case 'MH-NDIS TYPE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[NDIS TYPE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[NDIS TYPE]  '])}
                    break;
                    case 'MH-NDIS TYPE COMMENTS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[NDIS TYPE COMMENTS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[NDIS TYPE COMMENTS]  '])}
                    break;
                    case 'MH-NDIS NUMBER':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[NDIS NUMBER]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[NDIS NUMBER]  '])}
                    break;
                    case 'MH-REVIEW APPEAL':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[REVIEW APPEAL]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[REVIEW APPEAL]  '])}
                    break;
                    case 'MH-REVIEW COMMENTS':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[REVIEW COMMENTS]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[REVIEW COMMENTS]  '])}
                    break;
                    case 'MH-KP_Intake_1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_1]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_1]  '])}
                    break;
                    case 'MH-KP_Intake_2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_2]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_2]  '])}
                    break;                     
                    case 'MH-KP_Intake_3MH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_3M]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_3M]  '])}
                    break;                                   
                    case 'MH-KP_Intake_3PH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_3P]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_3P] '])}
                    break;
                    case 'MH-KP_Intake_4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_4]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_4]  '])}
                    break;
                    case 'MH-KP_Intake_5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_5]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_5]  '])}
                    break;
                    case 'MH-KP_Intake_6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_6]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_6]  '])}
                    break;
                    case 'MH-KP_Intake_7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_7]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_7]  '])}
                    break;
                    case 'MH-KP_3Months_1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_1]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_1]  '])}
                    break;
                    case 'MH-KP_3Months_2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_2]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_2]  '])}
                    break;
                    case 'MH-KP_3Months_3MH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_3M]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_3M]  '])}
                    break;
                    case 'MH-KP_3Months_3PH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_3P]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_3P]  '])}
                    break;
                    case 'MH-KP_3Months_4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_4]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_4]  '])}
                    break;
                    case 'MH-KP_3Months_5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_5]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_5]  '])}
                    break;
                    case 'MH-KP_3Months_6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_6]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_6]  '])}
                    break;
                    case 'MH-KP_3Months_7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_7]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_7]  '])}
                    break;
                    case 'MH-KP_6Months_1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_1]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_1]  '])}
                    break;
                    case 'MH-KP_6Months_2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_2]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_2]  '])}
                    break;
                    case 'MH-KP_6Months_3MH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_3M]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_3M]  '])}
                    break;
                    case 'MH-KP_6Months_3PH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_3P]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_3P]  '])}
                    break;
                    case 'MH-KP_6Months_4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_4]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_4]  '])}
                    break;
                    case 'MH-KP_6Months_5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_5]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_5]  '])}
                    break;
                    case 'MH-KP_6Months_6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_6]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_6]  '])}
                    break;
                    case 'MH-KP_6Months_7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_7]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_7]  '])}
                    break;
                    case 'MH-KP_9Months_1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_1]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_1]  '])}
                    break;
                    case 'MH-KP_9Months_2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_2]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_2]  '])}
                    break;
                    case 'MH-KP_9Months_3MH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_3M]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_3M]  '])}
                    break;
                    case 'MH-KP_9Months_3PH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_3P]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_3P]  '])}
                    break;
                    case 'MH-KP_9Months_4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_4]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_4]  '])}
                    break;
                    case 'MH-KP_9Months_5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_5]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_5]  '])}
                    break;
                    case 'MH-KP_9Months_6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_6]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_6]  '])}
                    break;
                    case 'MH-KP_9Months_7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_7]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_7]  '])}
                    break;
                    case 'MH-KP_Exit_1':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_1]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_1]  '])}
                    break;
                    case 'MH-KP_Exit_2':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_2]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_2]  '])}
                    break;
                    case 'MH-KP_Exit_3MH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_3M]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_3M]  '])}
                    break;
                    case 'MH-KP_Exit_3PH':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_3P]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_3P]  '])}
                    break;
                    case 'MH-KP_Exit_4':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_4]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_4]  '])}
                    break;                        
                    case 'MH-KP_Exit_5':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_5]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_5]  '])}
                    break;
                    case 'MH-KP_Exit_6':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_6]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_6]  '])}
                    break;
                    case 'MH-KP_Exit_7':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_7]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_7]  '])}
                    break;
                    case 'MH-KP_Intake_DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_IN_DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_IN_DATE]  '])}  
                    break;
                    case 'MH-KP_3Months_DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_3_DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_3_DATE]  '])}  
                    break;
                    case 'MH-KP_6Months_DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_6_DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_6_DATE]  '])}
                    break;
                    case 'MH-KP_9Months_DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_9_DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_9_DATE]  '])}  
                    break;
                    case 'MH-KP_Exit_DATE':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['MENTALHEALTHDATASET.[KP_EX_DATE]  '])
                    }else{columnNames = (['MENTALHEALTHDATASET.[KP_EX_DATE]  '])}
                    break;
                    //Recipient Placements                      
                    case 'Placement Type':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRPlacements.[Type]  '])
                    }else{columnNames = (['HRPlacements.[Type]  '])} 
                    break;
                    case 'Placement Carer Name':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRPlacements.[Name]  '])
                    }else{columnNames = (['HRPlacements.[Name]  '])}
                    break;
                    case 'Placement Start':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRPlacements.[Date1] as [Placement Start] '])
                    }else{columnNames = (['HRPlacements.[Date1] as [Placement Start] '])}  
                    break;
                    case 'Placement End':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRPlacements.[Date2] as [Placement End] '])
                    }else{columnNames = (['HRPlacements.[Date2] as [Placement End] '])}  
                    break;
                    case 'Placement Referral':
                    var placementrefferal = " CASE HRPlacements.[Recurring]  WHEN 1 THEN 'True' else 'False' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([placementrefferal + '  '])
                    }else{columnNames = ([placementrefferal + '  '])}  
                    break;        
                    case 'Placement ATC':
                    var placementATC = " CASE HRPlacements.[Completed]  WHEN 1 THEN 'True' else 'False' END "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([placementATC +'  '])
                    }else{columnNames = ([placementATC + '  '])} 
                    break;
                    case 'Placement Notes':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['HRPlacements.[Notes]  '])
                    }else{columnNames = (['HRPlacements.[Notes]  '])} 
                    break;
                    //Quote Goals and stratagies                      
                    case 'Quote Goal':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['GOALS.User1  '])
                    }else{columnNames = (['GOALS.User1  '])}  
                    break;
                    case 'Goal Expected Completion Date':
                    var GExpecCompletion = " Convert(varchar,GOALS.Date1,103) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([GExpecCompletion + '  '])
                    }else{columnNames = ([GExpecCompletion + '  '])}  
                    break;
                    case 'Goal Last Review Date':
                    var GlastReviewDate = " Convert(varchar,GOALS.Date2,103) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([GlastReviewDate + '  '])
                    }else{columnNames = ([GlastReviewDate + '  '])}
                    break;
                    case 'Goal Completed Date':
                    var GCompleteDate = " Convert(varchar,GOALS.DateInstalled,103) "
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat([GCompleteDate + '  '])
                    }else{columnNames = ([GCompleteDate + '  '])} 
                    break;
                    case 'Goal  Achieved':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['GOALS.[State]  '])
                    }else{columnNames = (['GOALS.[State]  '])}  
                    break;
                    case 'Quote Strategy':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['STRATEGIES.Notes  '])
                    }else{columnNames = (['STRATEGIES.Notes  '])}
                    break;
                    case 'Strategy Expected Outcome':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['STRATEGIES.Address1  '])
                    }else{columnNames = (['STRATEGIES.Address1  '])}
                    break;
                    case 'Strategy Contracted ID':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['STRATEGIES.[State]  '])
                    }else{columnNames = (['STRATEGIES.[State]  '])}  
                    break;
                    case 'Strategy DS Services':
                    if(columnNames.length >= 1){
                      columnNames = columnNames.concat(['STRATEGIES.User1  '])
                    }else{columnNames = (['STRATEGIES.User1  '])}  
                    break;
                    
                    
                    default:
                    break;
                  }
                  
                }
              }              
              return columnNames;
            }
        Condition(fld){
          var columnNames:Array<any> = [];
          var temp = fld.join(',')
          for (var key of fld){            
            columnNames = [key]                        
          }          
          return columnNames;
        }
        TablesSetting(itemsArr){
              var FromSql ;              
              if(this.RptFormat == "AGENCYSTFLIST" || this.RptFormat == "USERSTFLIST"){
                FromSql = " From Staff"                 
                if( itemsArr.includes("Contact Group") || itemsArr.includes("Contact Email") || itemsArr.includes("Contact FAX") || itemsArr.includes("Contact Mobile") || itemsArr.includes("Contact Type") || itemsArr.includes("Contact Phone 2") || itemsArr.includes("Contact Phone 1") || itemsArr.includes("Contact Postcode") || itemsArr.includes("Contact Suburb") || itemsArr.includes("Contact Name")   || itemsArr.includes("Contact Address") || itemsArr.includes("Contact Sub Type")  || itemsArr.includes("Contact User Flag")  || itemsArr.includes("Contact Person Type") ){ 
                  FromSql = FromSql + " left join HumanResources HR on Hr.PersonID = Staff.UniqueID "
                  this.includeConatctWhere = " HR.[Group] IN ('NEXTOFKIN',  'CONTACT', 'CARER',  '1-NEXT OF KIN',  '2-CARER',  '3-MEDICAL',  '4-ALLIED HEALTH',  '5-HEALTH INSURANCE',  '6-POWER OF ATTORNEY',  '7-LEGAL OTHER',  '8-OTHER','ALLIED HEALTH',  'PHARMACIST',  'HOSPITAL',  'HEALTHINSURER',  'POWERATTORNEY',  'OTHERLEGAL',  'OTHERCONTACT',  'MANAGER',  'HUMAN RESOURCES',  'ACCOUNTS',  'PAYROLL',  'SALES',  'PURCHASING',  'OTHERCONTACT') OR  HR.[Group] IN  (SELECT DESCRIPTION FROM DataDomains WHERE DOMAIN IN ('CONTACTGROUP', 'CARER RELATIONSHIP')AND DATASET = 'USER') "
                }
                if(itemsArr.includes("Group Email") || itemsArr.includes("Group End Date") || itemsArr.includes("Group Start Date") || itemsArr.includes("Group Note") || itemsArr.includes("Group Name") ){
                  FromSql = FromSql + " left join HumanResources UserGroup on UserGroup.PersonID = Staff.UniqueID "    
                  this.includeUserGroupWhere = " UserGroup.[Group] = 'STAFFTYPE' "
                }
                if(itemsArr.includes("Preference Note") || itemsArr.includes("Preference Name")){
                  FromSql = FromSql + " left join HumanResources Prefr on Prefr.PersonID = Staff.UniqueID "    
                  this.includePrefrencesWhere = " Prefr.[Group] = 'STAFFPREF' "
                }
                if(itemsArr.includes("Reminder Detail") || itemsArr.includes("Event Date") || itemsArr.includes("Reminder Date") || itemsArr.includes("Reminder Notes") ){
                  FromSql = FromSql + " left join HumanResources Remind on Remind.PersonID = Staff.UniqueID "    
                  this.includeReminderWhere = " Remind.[Group] = 'RECIPIENTALERT' "
                }
                if(itemsArr.includes("Name") || itemsArr.includes("Approved Status") || itemsArr.includes("Leave Reminder Date") || itemsArr.includes("Leave Start Date")
                || itemsArr.includes("Leave End Date") || itemsArr.includes("Notes") ){
                  FromSql = FromSql + " left join HumanResources HRLeave on Staff.UniqueID = HRLeave.PersonID"    
                  this.includeReminderWhere = " HRLeave.[Group] = 'LEAVEAPP' "
                }
                if(itemsArr.includes("Loan Item Date Collected") || itemsArr.includes("Loan Item Date Loaned/Installed")  || itemsArr.includes("Loan Item Description") || itemsArr.includes("Loan Item Type") ){
                  FromSql = FromSql + " left join HumanResources HRLoan on Staff.UniqueID = HRLoan.PersonID"    
                  this.includeLoanitemWhere = " HRLoan.[Group] = 'LOANITEMS'  "
                }
                if(itemsArr.includes("Staff Code") || itemsArr.includes("Service Date")  || itemsArr.includes("Service Start Time") || itemsArr.includes("Service Code") || itemsArr.includes("Service Hours") || itemsArr.includes("Service Pay Rate")  || itemsArr.includes("Service Bill Rate") || itemsArr.includes("Service Bill Qty") || itemsArr.includes("Service Location/Activity Group") 
                || itemsArr.includes("Service Program")  || itemsArr.includes("Service Group") || itemsArr.includes("Service HACC Type") || itemsArr.includes("Service End Time/ Shift End Time")  || itemsArr.includes("Service Pay Type") || itemsArr.includes("Service Category") || itemsArr.includes("Service Status") || itemsArr.includes("Service Pay Qty") ){
                  FromSql = FromSql + " INNER JOIN Roster SvcDetail ON Staff.accountno = SvcDetail.[client code]   "          
                }
                if(itemsArr.includes("Service Funding Source")    ){
                  FromSql = FromSql + " LEFT JOIN Humanresourcetypes HRMT ON SvcDetail.Program = HRMT.Name  "
                }
                if(itemsArr.includes("Service Notes")    ){
                  FromSql = FromSql + " LEFT JOIN History H ON CONVERT(varchar,SvcDetail.RecordNo,100) = H.PersonID  "
                  this.includeSvnDetailNotesWhere = "   History.ExtraDetail1 = 'SVCNOTE'  "
                }
                if(itemsArr.includes("Staff Position") || itemsArr.includes("Position Start Date") || itemsArr.includes("Position End Date") || itemsArr.includes("Position ID") || itemsArr.includes("Position Notes")    ){
                  FromSql = FromSql + " LEFT JOIN HumanResources StaffPosition ON STAFF.UniqueID = StaffPosition.PersonID "
                  this.includeStaffPositionWhere = "   StaffPosition.[Group] = 'STAFFPOSITION'  "
                } 
                if(itemsArr.includes("INCD_Status") || itemsArr.includes("INCD_Date") || itemsArr.includes("INCD_Type") || itemsArr.includes("INCD_Description") || itemsArr.includes("INCD_SubCategory") || itemsArr.includes("INCD_Assigned_To")
                 || itemsArr.includes("INCD_Service") || itemsArr.includes("INCD_Severity") || itemsArr.includes("INCD_Time") || itemsArr.includes("INCD_Duration") || itemsArr.includes("INCD_Location") || itemsArr.includes("INCD_LocationNotes") 
                || itemsArr.includes("INCD_ReportedBy") || itemsArr.includes("INCD_DateReported") || itemsArr.includes("INCD_Reported") || itemsArr.includes("INCD_Reported") || itemsArr.includes("INCD_FullDesc") || itemsArr.includes("INCD_Program") 
                || itemsArr.includes("INCD_DSCServiceType") || itemsArr.includes("INCD_TriggerShort") || itemsArr.includes("INCD_incident_level") 
                || itemsArr.includes("INCD_Area") || itemsArr.includes("INCD_Region") || itemsArr.includes("INCD_position") || itemsArr.includes("INCD_phone") || itemsArr.includes("INCD_BriefSummary") || itemsArr.includes("INCD_ReleventBackground") 
                || itemsArr.includes("INCD_SummaryofAction") || itemsArr.includes("INCD_verbal_date") || itemsArr.includes("INCD_By_Whome") || itemsArr.includes("INCD_To_Whome") || itemsArr.includes("INCD_SummaryOfOtherAction") || itemsArr.includes("INCD_Triggers") || itemsArr.includes("INCD_InitialAction") || itemsArr.includes("INCD_InitialNotes") 
                || itemsArr.includes("INCD_InitialFupBy") || itemsArr.includes("INCD_Completed") || itemsArr.includes("INCD_OngoingAction") || itemsArr.includes("INCD_OngoingNotes") || itemsArr.includes("INCD_Background") || itemsArr.includes("INCD_Abuse") || itemsArr.includes("INCD_DOPWithDisability") || itemsArr.includes("INCD_SeriousRisks") || itemsArr.includes("INCD_Complaints") || itemsArr.includes("INCD_Perpetrator") || itemsArr.includes("INCD_Notify") || itemsArr.includes("INCD_NoNotifyReason") || itemsArr.includes("INCD_Notes") || itemsArr.includes("INCD_Setting")  
                || itemsArr.includes("INCD_CreatedBy") || itemsArr.includes("INCD_Involved_Staff") 
                ){
                  FromSql = FromSql + " LEFT JOIN IM_Master StaffIncidents ON STAFF.UniqueID = StaffIncidents.PersonID "
                }
                if(itemsArr.includes("OP Notes Date") ||itemsArr.includes("OP Notes Detail") ||itemsArr.includes("OP Notes Creator") ||itemsArr.includes("OP Notes Alarm") ){
                  FromSql = FromSql + " LEFT JOIN History OPHistory ON STAFF.UniqueID = OPHistory.PersonID "
                  this.includeStaffOPNotesWhere = "   OPHistory.ExtraDetail1 = 'OPNOTE'  "
                }
                if(itemsArr.includes("HR Notes Date") || itemsArr.includes("HR Notes Detail") ||itemsArr.includes("HR Notes Creator") ||itemsArr.includes("HR Notes Alarm") || itemsArr.includes("HR Notes Categories") ){
                  FromSql = FromSql + "LEFT JOIN History HRHistory ON STAFF.UniqueID = HRHistory.PersonID "
                  this.includeStaffHRHistoryWhere = "   HRHistory.ExtraDetail1 = 'HRNOTE'  "
                } 
                if(itemsArr.includes("Competency") || itemsArr.includes("Competency Expiry Date") ||itemsArr.includes("Competency Reminder Date") ||itemsArr.includes("Competency Completion Date") || itemsArr.includes("Certificate Number")
                || itemsArr.includes("Mandatory Status") || itemsArr.includes("Competency Notes") ){
                  FromSql = FromSql + "LEFT JOIN HumanResources StaffAttribute ON STAFF.UniqueID = StaffAttribute.PersonID "
                  this.includeStaffAttributesWhere = " WHERE StaffAttribute.[Group] = 'STAFFATTRIBUTE' "
                }
                if(itemsArr.includes("Staff Position")){ 
                  FromSql = FromSql + "LEFT JOIN HumanResources StaffPosition ON STAFF.UniqueID = StaffPosition.PersonID "
                  this.includeStaffAttributesWhere = " WHERE StaffPosition.[Group] = 'STAFFATTRIBUTE' "
                }
                if(itemsArr.includes("SvcDetail.Date") || itemsArr.includes("SvcDetail.[Start Time]") || itemsArr.includes("(SvcDetail.[Duration]*5) / 60") || itemsArr.includes("SvcDetail.[Service Type]") || itemsArr.includes("SvcDetail.[ServiceSetting]") ||
                    itemsArr.includes("SvcDetail.[Program]") || itemsArr.includes("SvcDetail.[Type]") || itemsArr.includes("SvcDetail.[HACCType]") || itemsArr.includes("SvcDetail.[Anal]") || itemsArr.includes("SvcDetail.[Unit Pay Rate]") || itemsArr.includes("SvcDetail.[Unit Bill Rate]") 
                    || itemsArr.includes("SvcDetail.[BillQty]") || itemsArr.includes("SvcDetail.[Status]") || itemsArr.includes("SvcDetail.[Service Description]") || itemsArr.includes("SvcDetail.[CostQty]") 
                  ){
                    FromSql = FromSql + " INNER JOIN Roster SvcDetail ON Staff.accountno = SvcDetail.[client code] "      
                  }
                  if(itemsArr.includes("Humanresourcetypes.[Type]") ){
                    FromSql = FromSql + " INNER JOIN Roster ON Staff.AccountNo = Roster.[Carer Code] LEFT JOIN Humanresourcetypes ON Roster.Program = HumanResourceTypes.Name "      
                  }
                /*
                if(this.ConditionEntity.includes("SvcDetail.Date") || this.ConditionEntity.includes("SvcDetail.[Start Time]") || this.ConditionEntity.includes("(SvcDetail.[Duration]*5) / 60") || this.ConditionEntity.includes("SvcDetail.[Service Type]") || this.ConditionEntity.includes("SvcDetail.[ServiceSetting]") ||
                    this.ConditionEntity.includes("SvcDetail.[Program]") || this.ConditionEntity.includes("SvcDetail.[Type]") || this.ConditionEntity.includes("SvcDetail.[HACCType]") || this.ConditionEntity.includes("SvcDetail.[Anal]") || this.ConditionEntity.includes("SvcDetail.[Unit Pay Rate]") || this.ConditionEntity.includes("SvcDetail.[Unit Bill Rate]") 
                    || this.ConditionEntity.includes("SvcDetail.[BillQty]") || this.ConditionEntity.includes("SvcDetail.[Status]") || this.ConditionEntity.includes("SvcDetail.[Service Description]") || this.ConditionEntity.includes("SvcDetail.[CostQty]") 
                  ){
                    FromSql = FromSql + " INNER JOIN Roster SvcDetail ON Staff.accountno = SvcDetail.[client code] "      
                  }
                  if(this.ConditionEntity.includes("Humanresourcetypes.[Type]") ){
                    FromSql = FromSql + " INNER JOIN Roster ON Staff.AccountNo = Roster.[Carer Code] LEFT JOIN Humanresourcetypes ON Roster.Program = HumanResourceTypes.Name "      
                  }   */                                              
              }else{
                FromSql = " From Recipients R" 
                if(itemsArr.includes("Carer Last Name") || itemsArr.includes("Carer Age") || itemsArr.includes("Carer Gender") || itemsArr.includes("Carer Indigenous Status") || itemsArr.includes("Carer First Name") ){
                  FromSql = FromSql + " INNER JOIN RECIPIENTS C ON R.DatasetCarer = C.AccountNo  "
                }         
                if(itemsArr.includes("Carer Phone <Home>") ){ 
                  FromSql = FromSql + " LEFT JOIN PhoneFaxOther PhHome ON C.UniqueID = PhHome.PersonID AND PhHome.[Type] = '<HOME>'  "
                }
                if(itemsArr.includes("Carer Phone <Work>") ){
                  FromSql = FromSql + " LEFT JOIN PhoneFaxOther PhWork ON C.UniqueID = PhWork.PersonID AND PhWork.[Type] = '<WORK>' "
                }
                if(itemsArr.includes("Carer Phone <Mobile>") ){
                  FromSql = FromSql + " LEFT JOIN PhoneFaxOther PhMobile ON C.UniqueID = PhMobile.PersonID AND PhMobile.[Type] = '<MOBILE>'  "
                }
                if(itemsArr.includes("Carer Email") ){
                  FromSql = FromSql + " LEFT JOIN PhoneFaxOther PE ON C.UniqueID = PE.PersonID AND PE.[Type] = '<EMAIL>' "
                }
                if(itemsArr.includes("Carer Address") ){
                  FromSql = FromSql + " LEFT JOIN NamesAndAddresses N ON C.UNIQUEID = N.PERSONID AND N.[Description] = '<USUAL>' "
                }
                if( itemsArr.includes("Contact Group") || itemsArr.includes("Contact Email") || itemsArr.includes("Contact FAX") || itemsArr.includes("Contact Mobile") || itemsArr.includes("Contact Type") || itemsArr.includes("Contact Phone 2") || itemsArr.includes("Contact Phone 1") || itemsArr.includes("Contact Postcode") || itemsArr.includes("Contact Suburb") || itemsArr.includes("Contact Name")   || itemsArr.includes("Contact Address") || itemsArr.includes("Contact Sub Type")  || itemsArr.includes("Contact User Flag")  || itemsArr.includes("Contact Person Type") ){ 
                  FromSql = FromSql + " left join HumanResources HR on Hr.PersonID = R.UniqueID "
                  this.includeConatctWhere = " HR.[Group] IN ('NEXTOFKIN',  'CONTACT', 'CARER',  '1-NEXT OF KIN',  '2-CARER',  '3-MEDICAL',  '4-ALLIED HEALTH',  '5-HEALTH INSURANCE',  '6-POWER OF ATTORNEY',  '7-LEGAL OTHER',  '8-OTHER','ALLIED HEALTH',  'PHARMACIST',  'HOSPITAL',  'HEALTHINSURER',  'POWERATTORNEY',  'OTHERLEGAL',  'OTHERCONTACT',  'MANAGER',  'HUMAN RESOURCES',  'ACCOUNTS',  'PAYROLL',  'SALES',  'PURCHASING',  'OTHERCONTACT') OR  HR.[Group] IN  (SELECT DESCRIPTION FROM DataDomains WHERE DOMAIN IN ('CONTACTGROUP', 'CARER RELATIONSHIP')AND DATASET = 'USER') "
                }  
                if(itemsArr.includes("DOC_ID") ||itemsArr.includes("Doc_Title") ||itemsArr.includes("Created") ||itemsArr.includes("Modified") ||itemsArr.includes("Status") ||itemsArr.includes("Classification") ||itemsArr.includes("Category") ||itemsArr.includes("Filename") ||itemsArr.includes("Doc#") ||itemsArr.includes("DocStartDate") ||itemsArr.includes("DocEndDate") ||itemsArr.includes("AlarmDate") ||itemsArr.includes("AlarmText") ){
                  FromSql = FromSql + " left join Documents doc on doc.PersonID = R.UniqueID  "
                }
                if(itemsArr.includes("Consent") || itemsArr.includes("Consent Start Date") || itemsArr.includes("Consent Expiry") || itemsArr.includes("Consent Notes") ){
                  FromSql = FromSql + " left join HumanResources Cons on Cons.PersonID = R.UniqueID  "
                } 
                if(itemsArr.includes("Goal") || itemsArr.includes("Goal Detail") || itemsArr.includes("Goal Achieved") || itemsArr.includes("Anticipated Achievement Date") || itemsArr.includes("Date Achieved") || itemsArr.includes("Last Reviewed") || itemsArr.includes("Logged By") ){
                  FromSql = FromSql + " left join HumanResources Goalcare on Goalcare.PersonID = R.UniqueID "    
                  this.includeGoalcareWhere = " Goalcare.[Group] = 'RECIPIENTGOALS' "
                } 
                if(itemsArr.includes("Reminder Detail") || itemsArr.includes("Event Date") || itemsArr.includes("Reminder Date") || itemsArr.includes("Reminder Notes") ){
                  FromSql = FromSql + " left join HumanResources Remind on Remind.PersonID = R.UniqueID "    
                  this.includeReminderWhere = " Remind.[Group] = 'RECIPIENTALERT' "
                }
                if(itemsArr.includes("Group Email") || itemsArr.includes("Group End Date") || itemsArr.includes("Group Start Date") || itemsArr.includes("Group Note") || itemsArr.includes("Group Name") ){
                  FromSql = FromSql + " left join HumanResources UserGroup on UserGroup.PersonID = R.UniqueID "    
                  this.includeUserGroupWhere = " UserGroup.[Group] = 'RECIPTYPE' "
                }
                if(itemsArr.includes("Preference Note") || itemsArr.includes("Preference Name")){
                  FromSql = FromSql + " left join HumanResources Prefr on Prefr.PersonID = R.UniqueID "    
                  this.includePrefrencesWhere = " Prefr.[Group] = 'STAFFPREF' "
                }  
                if(itemsArr.includes("Excluded Staff") || itemsArr.includes("Excluded_Staff Notes") ){
                  FromSql = FromSql + " left join HumanResources ExcludeS on ExcludeS.PersonID = R.UniqueID "    
                  this.includeExcludeSWhere = " ExcludeS.[Type] = 'EXCLUDEDSTAFF'  "
                } 
                if(itemsArr.includes("Included Staff") || itemsArr.includes("Included_Staff Notes") ){
                  FromSql = FromSql + " left join HumanResources IncludS on IncludS.PersonID = R.UniqueID "    
                  this.includeIncludSWhere = " IncludS.[TYPE] = 'INCLUDEDSTAFF'  "
                }
                if(itemsArr.includes("Funding Source") ){
                  FromSql = FromSql + " LEFT JOIN RecipientPrograms RP ON R.UniqueID = RP.PersonID LEFT JOIN HumanResourceTypes HRT ON RP.Program = HRT.Name "
                }
                if(itemsArr.includes("Funded Program") || itemsArr.includes("Program Status") || itemsArr.includes("Program Coordinator") || itemsArr.includes("Funding Start Date") || itemsArr.includes("Funding End Date") || itemsArr.includes("AutoRenew") || itemsArr.includes("Rollover Remainder") || itemsArr.includes("Funded Qty") || itemsArr.includes("Funded Type")|| itemsArr.includes("Funding Cycle")  || itemsArr.includes("Funded Total Allocation") || itemsArr.includes("Funded Program Agency ID") ){
                  FromSql = FromSql + " LEFT JOIN RecipientPrograms ON R.UniqueID = RecipientPrograms.PersonID LEFT JOIN HumanResourceTypes ON RecipientPrograms.Program = HumanResourceTypes.Name "
                }
                if(itemsArr.includes("Name") || itemsArr.includes("Start Date") || itemsArr.includes("End Date") || itemsArr.includes("Details") || itemsArr.includes("Reminder Date") || itemsArr.includes("Reminder Text")  ){
                  FromSql = FromSql + " left join CarePlanItem on CarePlanItem.PersonID = R.UniqueID "
                }
                if(itemsArr.includes("Agreed Service Code") || itemsArr.includes("Agreed Program") || itemsArr.includes("Agreed Service Billing Rate") || itemsArr.includes("Agreed Service Status") || itemsArr.includes("Agreed Service Duration") || itemsArr.includes("Agreed Service Frequency") || itemsArr.includes("Agreed Service Unit Cost") || itemsArr.includes("Agreed Service Debtor")   || itemsArr.includes("Agreed Service Unit Cost") || itemsArr.includes("Agreed Service Cost Type")    ){
                  FromSql = FromSql + " LEFT JOIN ServiceOverview ON R.UniqueID = ServiceOverview.PersonID LEFT JOIN HumanResourceTypes HRAgreedServices ON HRAgreedServices.Name = ServiceOverview.ServiceProgram "
                }
                if(itemsArr.includes("Nursing Diagnosis")  ){
                  FromSql = FromSql + " LEFT JOIN NDiagnosis ON R.UniqueID = NDiagnosis.PersonID "
                }
                if(itemsArr.includes("Medical Diagnosis ") || itemsArr.includes("ONI-HC-Diagnosis")  ){
                  FromSql = FromSql + " LEFT JOIN MDiagnosis ON R.UniqueID = MDiagnosis.PersonID "
                }
                if(itemsArr.includes("Medical Procedure") ){
                  FromSql = FromSql + " LEFT JOIN MProcedures ON R.UniqueID = MProcedures.PersonID "
                }  
                if(itemsArr.includes("Pension Number") || itemsArr.includes("Pension Name")){
                  FromSql = FromSql + " left join HumanResources RecipientPensions on RecipientPensions.PersonID = R.UniqueID "
                  this.includeRecipientPensionWhere = " RecipientPensions.[Group] = 'PENSION'  "
                }    
                if(itemsArr.includes("HACC-GetUp") || itemsArr.includes("HACC-Toileting") || itemsArr.includes("HACC-Eating") || itemsArr.includes("HACC-Communication")  || itemsArr.includes("CSTDA-Primary Disability Description")   || itemsArr.includes("ONI-DVA Cardholder Status") ||
                itemsArr.includes("ONI-FP-Comments") || itemsArr.includes("ONI-FP-Has Reading Aids") || itemsArr.includes("ONI-FP-Has Medical Care Aids") || itemsArr.includes("ONI-FPQ7-Bath") || itemsArr.includes("ONI-FP-Has Self Care Aids") || itemsArr.includes("ONI-FP-Has Other Aids") || itemsArr.includes("ONI-FP-Has Support/Mobility Aids") || itemsArr.includes("ONI-FP-Other Goods List") || itemsArr.includes("ONI-FP-Has Car Mods") || itemsArr.includes("ONI-FP-Has Communication Aids")  || itemsArr.includes("ONI-FPQ5-Money") || itemsArr.includes("ONI-FPQ6-Walk") || itemsArr.includes("ONI-FPQ8-Memory") || itemsArr.includes("ONI-FPQ9-Behaviour") || itemsArr.includes("ONI-FP-Recommend Domestic") || itemsArr.includes("ONI-FP-Recommend Self Care") || itemsArr.includes("ONI-FP-Recommend Cognition") || itemsArr.includes("ONI-FP-Recommend Behaviour")  
                || itemsArr.includes("ONI-FPQ4-Medicine") || itemsArr.includes("ONI-FPQ3-Shopping") || itemsArr.includes("ONI-FPQ2-GetToPlaces") || itemsArr.includes("ONI-FPQ1-Housework")  
                || itemsArr.includes("ONI-LA-Financial & Legal Comments") || itemsArr.includes("ONI-LA-Cost Of Living Trade Off") || itemsArr.includes("ONI-LA-Capable Own Decisions") || itemsArr.includes("ONI-LA-Employment Status Comments") || itemsArr.includes("ONI-LA-Accomodation Comments") || itemsArr.includes("ONI-LA-Living Arrangements Comments")  || itemsArr.includes("ONI-LA-Employment Status") || itemsArr.includes("ONI-LA-Financial Decisions") || itemsArr.includes("ONI-LA-Decision Making Responsibility") || itemsArr.includes("ONI-LA-Mental Health Act Status")
                || itemsArr.includes("ONI-HC-Overall Health Description") || itemsArr.includes("ONI-HC-Oral Comments") || itemsArr.includes("ONI-HC-Speech/Swallow Problems") || itemsArr.includes("ONI-HC-Pulse Regularity") || itemsArr.includes("ONI-HC-Vacc. Influenza Date") || itemsArr.includes("ONI-HC-Driving MV") || itemsArr.includes("ONI-HC-Driving Fit") || itemsArr.includes("ONI-HC-Urinary Related To Coughing") 
                || itemsArr.includes("ONI-HC-Overall Health Pain") || itemsArr.includes("ONI-HC-Oral Problems") || itemsArr.includes("ONI-HC-Falls Problems") || itemsArr.includes("ONI-HC-Vacc. Other Date") || itemsArr.includes("ONI-HC-Feet Problems") || itemsArr.includes("ONI-HC-Feet Comments") || itemsArr.includes("ONI-HC-Driving Comments") || itemsArr.includes("ONI-HC-Continence Urinary") || itemsArr.includes("ONI-HC-Check Postural Hypotension")  
                || itemsArr.includes("ONI-HC-Overall Health Interference") || itemsArr.includes("ONI-HC-Hearing") || itemsArr.includes("ONI-HC-Speech/Swallow Comments") || itemsArr.includes("ONI-HC-Faecal Continence") || itemsArr.includes("ONI-HC-Vacc. Influenza") || itemsArr.includes("ONI-HC-Vacc. Tetanus") || itemsArr.includes("ONI-HC-Vacc. Other") || itemsArr.includes("ONI-HC-Continence Comments")  || itemsArr.includes("ONI-HC-BMI") || itemsArr.includes("ONI-HC-BP Systolic") 
                || itemsArr.includes("ONI-HC-Vision Reading") || itemsArr.includes("ONI-HC-Vision Distance") || itemsArr.includes("ONI-HC-Falls Comments") || itemsArr.includes("ONI-HC-Vacc. Pneumococcus") || itemsArr.includes("ONI-HC-Pulse Rate") || itemsArr.includes("ONI-HC-Vacc. Pneumococcus  Date") || itemsArr.includes("ONI-HC-Vacc. Tetanus Date") || itemsArr.includes("ONI-HC-BP Diastolic") || itemsArr.includes("ONI-HC-Weight") || itemsArr.includes("ONI-HC-Height")    
                || itemsArr.includes("ONI-PS-K10-1,ONI-PS-K10-2") || itemsArr.includes("ONI-PS-K10-3") || itemsArr.includes("ONI-PS-K10-4") || itemsArr.includes("ONI-PS-K10-5") || itemsArr.includes("ONI-PS-K10-6") || itemsArr.includes("ONI-PS-K10-7") || itemsArr.includes("ONI-PS-K10-8") || itemsArr.includes("ONI-PS-K10-9") || itemsArr.includes("ONI-PS-K10-10")
                || itemsArr.includes("ONI-PS-Sleep Difficulty") || itemsArr.includes("ONI-PS-Sleep Details") || itemsArr.includes("ONI-PS-Personal Support") || itemsArr.includes("ONI-PS-Personal Support Comments") || itemsArr.includes("ONI-PS-Keep Friendships") || itemsArr.includes("ONI-PS-Problems Interacting")
                || itemsArr.includes("ONI-PS-Family/Relationship Comments") || itemsArr.includes("ONI-PS-Svc Prvdr Relations") || itemsArr.includes("ONI-PS-Svc Provider Comments")  
                || itemsArr.includes("ONI-HB-Regular Health Checks") || itemsArr.includes("ONI-HB-Last Health Check") || itemsArr.includes("ONI-HB-Health Screens") || itemsArr.includes("ONI-HB-Smoking") || itemsArr.includes("ONI-HB-If Quit Smoking - When?") || itemsArr.includes("ONI-HB-Alcohol-How many?") || itemsArr.includes("ONI-HB-Alcohol-How often over 6?") || itemsArr.includes("ONI-HB-Lost Weight")
                || itemsArr.includes("ONI-HB-Eating Poorly") || itemsArr.includes("ONI-HB-How much wieght lost") || itemsArr.includes("ONI-HB-Malnutrition Score") || itemsArr.includes("ONI-HB-8 cups fluid") || itemsArr.includes("ONI-HB-Recent decrease in fluid") || itemsArr.includes("ONI-HB-Weight") || itemsArr.includes("ONI-HB-Physical Activity") || itemsArr.includes("ONI-HB-Physical Fitness") || itemsArr.includes("ONI-HB-Fitness Comments")
                || itemsArr.includes("ONI-CP-Need for carer") || itemsArr.includes("ONI-CP-Carer has help") || itemsArr.includes("ONI-CP-Carer receives payment") || itemsArr.includes("ONI-CP-Carer made aware support services") || itemsArr.includes("ONI-CP-Carer needs training") || itemsArr.includes("ONI-CP-Carer threat-emotional")
                || itemsArr.includes("ONI-CP-Carer threat-acute physical") || itemsArr.includes("ONI-CP-Carer threat-slow physical") || itemsArr.includes("ONI-CP-Carer threat-other factors") || itemsArr.includes("ONI-CP-Carer threat-increasing consumer needs") || itemsArr.includes("ONI-CP-Carer threat-other comsumer factors")
                || itemsArr.includes("ONI-CP-Carer arrangements sustainable") || itemsArr.includes("ONI-CP-Carer Comments")
                || itemsArr.includes("ONI-CS-Year of Arrival") || itemsArr.includes("ONI-CS-Citizenship Status") || itemsArr.includes("ONI-CS-Reasons for moving to Australia") || itemsArr.includes("ONI-CS-Primary/Secondary Language Fluency") || itemsArr.includes("ONI-CS-Fluency in English")
                || itemsArr.includes("ONI-CS-Literacy in primary language") || itemsArr.includes("ONI-CS-Literacy in English") || itemsArr.includes("ONI-CS-Non verbal communication style") || itemsArr.includes("ONI-CS-Marital Status") || itemsArr.includes("ONI-CS-Religion") || itemsArr.includes("ONI-CS-Employment history in country of origin")
                || itemsArr.includes("ONI-CS-Employment history in Australia") || itemsArr.includes("ONI-CS-Specific dietary needs") || itemsArr.includes("ONI-CS-Specific cultural needs") || itemsArr.includes("ONI-CS-Someone to talk to for day to day problems") || itemsArr.includes("ONI-CS-Miss having close freinds") || itemsArr.includes("ONI-CS-Experience general sense of emptiness")
                || itemsArr.includes("ONI-CS-Plenty of people to lean on for problems") || itemsArr.includes("ONI-CS-Miss the pleasure of the company of others") || itemsArr.includes("ONI-CS-Circle of friends and aquaintances too limited") || itemsArr.includes("ONI-CS-Many people I trust completely") || itemsArr.includes("ONI-CS-Enough people I feel close to") || itemsArr.includes("ONI-CS-Miss having people around")
                || itemsArr.includes("ONI-CS-Often feel rejected") || itemsArr.includes("ONI-CS-Can call on my friends whenever I need them")

                ){
                  FromSql = FromSql + " LEFT JOIN ONI ON R.UniqueID = ONI.PersonID  "
                }  
                if(itemsArr.includes("'HACC-SLK'") ){
                  FromSql = FromSql + " INNER JOIN RECIPIENTS  RHACC ON RHACC.AccountNo = R.AccountNo "
                } 
                if(itemsArr.includes("HACC-Main Reasons For Cessation") ){
                  FromSql = FromSql + " INNER JOIN ITEMTYPES  ON (Roster.[SERVICE TYPE] = ITEMTYPES.TITLE AND ITEMTYPES.MINORGROUP = 'DISCHARGE') INNER JOIN DATADOMAINS  ON (Roster.[DischargeReasonType] = DATADOMAINS.HACCCODE AND DOMAIN = 'REASONCESSSERVICE' and datadomains.[dataset]='HACC')  "
                }   
                if(itemsArr.includes("ONI-Main Problem-Description") || itemsArr.includes("ONI-Main Problem-Action")){
                  FromSql = FromSql + " LEFT JOIN ONIMainIssues ON R.UniqueID = ONIMainIssues.PersonID "
                }
                if(itemsArr.includes("ONI-Other Problem-Description") || itemsArr.includes("ONI-Other Problem-Action")){
                  FromSql = FromSql + " LEFT JOIN ONISecondaryIssues ON R.UniqueID = ONISecondaryIssues.PersonID "
                }
                if(itemsArr.includes("ONI-Current Service") || itemsArr.includes("ONI-Service Contact Details")){
                  FromSql = FromSql + " LEFT JOIN ONIServices ON R.UniqueID = ONIServices.PersonID "
                }
                if(itemsArr.includes("ONI-AP-Agency") || itemsArr.includes("ONI-AP-For") || itemsArr.includes("ONI-AP-Consent") || itemsArr.includes("ONI-AP-Referral") || itemsArr.includes("ONI-AP-Transport") || itemsArr.includes("ONI-AP-Feedback") || itemsArr.includes("ONI-AP-Date") || itemsArr.includes("ONI-AP-Review") ){
                  FromSql = FromSql + " LEFT JOIN ONIActionPlan ON R.UniqueID = ONIActionPlan.PersonID "
                } 
                
                if(itemsArr.includes("HACC-Main Reasons For Cessation") ){
                  FromSql = FromSql + " INNER JOIN ITEMTYPES  ON (Roster.[SERVICE TYPE] = ITEMTYPES.TITLE AND ITEMTYPES.MINORGROUP = 'DISCHARGE') INNER JOIN DATADOMAINS  ON (Roster.[DischargeReasonType] = DATADOMAINS.HACCCODE AND DOMAIN = 'REASONCESSSERVICE' and datadomains.[dataset]='HACC')  "
                }
                if(itemsArr.includes("ONI-HC-Conditions")    ){
                  FromSql = FromSql + " LEFT JOIN ONIHealthCOnditions ON R.UniqueID = ONIHealthCOnditions.PersonID "
                }
                if(itemsArr.includes("ONI-HC-Medicines")    ){
                  FromSql = FromSql + " LEFT JOIN ONIMedications ON R.UniqueID = ONIMedications.PersonID  "
                }
                if(
                  itemsArr.includes("DEX-Income Amount") || itemsArr.includes("DEX-Income Frequency") || itemsArr.includes("DEX-Main Source Of Income") || itemsArr.includes("DEX-Household Composition") || itemsArr.includes("DEX-Is Homeless") || itemsArr.includes("DEX-Accomodation Setting") || itemsArr.includes("DEX-Main Language At Home")
                  || itemsArr.includes("DEX-Ancestry") || itemsArr.includes("DEX-Visa Code") || itemsArr.includes("DEX-First Arrival Month") || itemsArr.includes("DEX-First Arrival Year")|| itemsArr.includes("DEX-Country of Birth") || itemsArr.includes("DEX-Has A Carer") || itemsArr.includes("DEX-Has Disabilities")                      
                  || itemsArr.includes("DEX-DVA Card Holder Status") || itemsArr.includes("DEX-Indigenous Status") || itemsArr.includes("DEX-Estimated Birth Date") || itemsArr.includes("DEX-Date Of Birth") || itemsArr.includes("DEX-Sex") || itemsArr.includes("DEX-Consent For Future Contact") || itemsArr.includes("DEX-Consent To Provide Information")
                  || itemsArr.includes("DEX-Reason For Assistance") || itemsArr.includes("DEX-Referral Type") || itemsArr.includes("DEX-Referral Source") || itemsArr.includes("DEX-Referral Purpose") || itemsArr.includes("DEX-Exclude From MDS")
                  ){
                    FromSql = FromSql + " LEFT JOIN DSSXtra DSS ON R.UniqueID = DSS.PersonID LEFT JOIN ONI ON R.UNIQUEID = ONI.PersonID  "
                  }
                  if(itemsArr.includes("Loan Item Date Collected") || itemsArr.includes("Loan Item Date Loaned/Installed")  || itemsArr.includes("Loan Item Description") || itemsArr.includes("Loan Item Type") ){
                    FromSql = FromSql + " left join HumanResources HRLoan on HRLoan.PersonID = R.UniqueID "    
                    this.includeLoanitemWhere = " HRLoan.[Group] = 'LOANITEMS'  "
                  }
                  if(itemsArr.includes("Staff Code") || itemsArr.includes("Service Date")  || itemsArr.includes("Service Start Time") || itemsArr.includes("Service Code") || itemsArr.includes("Service Hours") || itemsArr.includes("Service Pay Rate")  || itemsArr.includes("Service Bill Rate") || itemsArr.includes("Service Bill Qty") || itemsArr.includes("Service Location/Activity Group") 
                  || itemsArr.includes("Service Program")  || itemsArr.includes("Service Group") || itemsArr.includes("Service HACC Type") || itemsArr.includes("Service End Time/ Shift End Time")  || itemsArr.includes("Service Pay Type") || itemsArr.includes("Service Category") || itemsArr.includes("Service Status") || itemsArr.includes("Service Pay Qty") ){
                    FromSql = FromSql + " INNER JOIN Roster SvcDetail ON R.accountno = SvcDetail.[client code]   "          
                  }
                  if(itemsArr.includes("Service Funding Source")    ){
                    FromSql = FromSql + " LEFT JOIN Humanresourcetypes ON SvcDetail.Program = HumanResourceTypes.Name  "
                  }
                  if(itemsArr.includes("Service Notes")    ){
                    FromSql = FromSql + " LEFT JOIN History HS ON CONVERT(varchar,SvcDetail.RecordNo,100) = HS.PersonID  "
                    this.includeSvnDetailNotesWhere = "   History.ExtraDetail1 = 'SVCNOTE'  "
                  }
                  if(itemsArr.includes("Activity")  || itemsArr.includes("Competency")    ){
                    FromSql = FromSql + " LEFT JOIN HumanResources SvcSpecCompetency  ON R.UniqueID = SvcSpecCompetency.PersonID "
                    this.includeSvcSpecCompetencyWhere = " SvcSpecCompetency.[Type] = 'STAFFATTRIBUTE'  "
                  }         
                  if(itemsArr.includes("S Status")    ){
                    FromSql = FromSql + " LEFT JOIN  ServiceOverview ServiceCompetencyStatus ON ServiceCompetencyStatus.[PersonID]  = R.[UniqueID]  "        
                    if(itemsArr.includes("Activity")  || itemsArr.includes("Competency")    ){
                      FromSql = FromSql +  " AND ServiceCompetencyStatus.[SERVICE TYPE] = SvcSpecCompetency.[Service] "
                    } else { FromSql = FromSql +  " LEFT JOIN  HumanResources SvcSpecCompetency on ServiceCompetencyStatus.[SERVICE TYPE] = SvcSpecCompetency.[Service] " }
                  }
                  if(itemsArr.includes("OP Notes Date")   || itemsArr.includes("OP Notes Detail") ||  itemsArr.includes("OP Notes Creator") || itemsArr.includes("OP Notes Alarm") || itemsArr.includes("OP Notes Program") || itemsArr.includes("OP Notes Category")  ){
                    FromSql = FromSql + " LEFT JOIN History OPHistory ON R.UniqueID = OPHistory.PersonID   "        
                    this.includeOPHistoryWhere = " OPHistory.ExtraDetail1 = 'OPNOTE'  "
                  } 
                  if(itemsArr.includes("Clinical Notes Date")   || itemsArr.includes("Clinical Notes Detail") ||  itemsArr.includes("Clinical Notes Creator") || itemsArr.includes("Clinical Notes Category") || itemsArr.includes("Clinical Notes Alarm")   ){
                    FromSql = FromSql + "  LEFT JOIN History CliniHistory ON R.UniqueID = CliniHistory.PersonID    "        
                    this.includeClinicHistoryWhere = " CliniHistory.ExtraDetail1 = 'CLINNOTE'  "
                  }        
                  if(
                    itemsArr.includes("INCD_Status") || itemsArr.includes("INCD_Date") || itemsArr.includes("INCD_Type") || itemsArr.includes("INCD_Description") || itemsArr.includes("INCD_SubCategory") || itemsArr.includes("INCD_Assigned_To") || itemsArr.includes("INCD_Service")
                    || itemsArr.includes("INCD_Severity") || itemsArr.includes("INCD_Time") || itemsArr.includes("INCD_Duration") || itemsArr.includes("INCD_Location") || itemsArr.includes("INCD_LocationNotes") || itemsArr.includes("INCD_ReportedBy") || itemsArr.includes("INCD_DateReported")
                    || itemsArr.includes("INCD_Reported") || itemsArr.includes("INCD_FullDesc") || itemsArr.includes("INCD_Program") || itemsArr.includes("INCD_DSCServiceType") || itemsArr.includes("INCD_TriggerShort") || itemsArr.includes("INCD_incident_level") || itemsArr.includes("INCD_Area")
                    || itemsArr.includes("INCD_Region") || itemsArr.includes("INCD_position") || itemsArr.includes("INCD_phone") || itemsArr.includes("INCD_verbal_date") || itemsArr.includes("INCD_verbal_time") || itemsArr.includes("INCD_By_Whom") || itemsArr.includes("INCD_To_Whom") || itemsArr.includes("INCD_BriefSummary")
                    || itemsArr.includes("INCD_ReleventBackground") || itemsArr.includes("INCD_SummaryofAction") || itemsArr.includes("INCD_SummaryOfOtherAction") || itemsArr.includes("INCD_Triggers")|| itemsArr.includes("INCD_InitialAction") || itemsArr.includes("INCD_InitialNotes")
                    || itemsArr.includes("INCD_InitialFupBy") || itemsArr.includes("INCD_Completed") || itemsArr.includes("INCD_OngoingAction") || itemsArr.includes("INCD_OngoingNotes") || itemsArr.includes("INCD_Background") || itemsArr.includes("INCD_Abuse") || itemsArr.includes("INCD_DOPWithDisability") || itemsArr.includes("INCD_SeriousRisks")
                    || itemsArr.includes("INCD_Complaints") || itemsArr.includes("INCD_Perpetrator") || itemsArr.includes("INCD_Notify") || itemsArr.includes("INCD_NoNotifyReason") || itemsArr.includes("INCD_Notes") || itemsArr.includes("INCD_Setting") || itemsArr.includes("INCD_Involved_Staff")
                    ){
                      FromSql = FromSql + " Right Join IM_Master IMM on  IMM.PersonID = R.UniqueID " +
                      " left Join ItemTypes IT ON  IT.Title    = [IMM].[Service] " +
                      " left Join HumanResourceTypes HRT ON  [IMM].Program   = [HRT].[Name] " +
                      " left Join  IM_INVOLVEDSTAFF IMI ON   IMM.RecordNo  = IMI.IM_Master# " 
                    }
                    if(itemsArr.includes("Recipient Competency")  || itemsArr.includes("Recipient Competency Mandatory") || itemsArr.includes("Recipient Competency Notes")   ){
                      FromSql = FromSql + " Left Join HumanResources RecpCompet ON R.UniqueID = RecpCompet.PersonID "
                      this.includeRecipientCompetencyWhere = " RecpCompet.[Type] = 'RECIPATTRIBUTE'  "
                    }
                    if(
                      itemsArr.includes("CarePlan Archived") || itemsArr.includes("CarePlan ReminderText") || itemsArr.includes("CarePlan ReviewDate") || itemsArr.includes("CarePlan SignOffDate") || itemsArr.includes("CarePlan StartDate") || itemsArr.includes("CarePlan CareDomain")
                      || itemsArr.includes("CarePlan Discipline") || itemsArr.includes("CarePlan Program") || itemsArr.includes("CarePlan Type") || itemsArr.includes("CarePlan Name") || itemsArr.includes("CarePlan ID")
                      ){
                        FromSql = FromSql + " Right JOIN   Documents D on R.UniqueID = D.PersonID LEFT JOIN qte_hdr QH ON QH.CPID = D.DOC_ID  "
                        this.includeCareplanWhere = "  ISNULL(D.[Status], '') <> '' AND D.DOCUMENTGROUP IN ('CAREPLAN') "
                      }
                      if(     
                        itemsArr.includes("MH-PERSONID") || itemsArr.includes("MH-HOUSING TYPE ON REFERRAL")|| itemsArr.includes("MH-RE REFERRAL") || itemsArr.includes("MH-REFERRAL SOURCE") || itemsArr.includes("MH-REFERRAL RECEIVED DATE") || itemsArr.includes("MH-ENGAGED AND CONSENT DATE")|| itemsArr.includes("MH-OPEN TO HOSPITAL") 
                        || itemsArr.includes("MH-OPEN TO HOSPITAL DETAILS") || itemsArr.includes("MH-ALERTS") || itemsArr.includes("MH-ALERTS DETAILS") || itemsArr.includes("MH-MH DIAGNOSIS") || itemsArr.includes("MH-MEDICAL DIAGNOSIS") || itemsArr.includes("MH-REASONS FOR EXIT") || itemsArr.includes("MH-SERVICES LINKED INTO") 
                        || itemsArr.includes("MH-NON ACCEPTED REASONS") || itemsArr.includes("MH-NOT PROCEEDED") || itemsArr.includes("MH-DISCHARGE DATE") || itemsArr.includes("MH-CURRENT AOD") 
                        || itemsArr.includes("MH-CURRENT AOD DETAILS") || itemsArr.includes("MH-PAST AOD") || itemsArr.includes("MH-PAST AOD DETAILS")|| itemsArr.includes("MH-ENGAGED AOD") || itemsArr.includes("MH-ENGAGED AOD DETAILS") || itemsArr.includes("MH-SERVICES CLIENT IS LINKED WITH ON INTAKE") || itemsArr.includes("MH-SERVICES CLIENT IS LINKED WITH ON EXIT") 
                        || itemsArr.includes("MH-ED PRESENTATIONS ON REFERRAL") 
                        || itemsArr.includes("MH-ED PRESENTATIONS ON 3 MONTH REVIEW") || itemsArr.includes("MH-ED PRESENTATIONS ON EXIT") || itemsArr.includes("MH-AMBULANCE ARRIVAL ON REFERRAL") || itemsArr.includes("MH-AMBULANCE ARRIVAL ON MID 3 MONTH REVIEW") || itemsArr.includes("MH-AMBULANCE ARRIVAL ON EXIT") || itemsArr.includes("MH-ADMISSIONS ON REFERRAL") 
                        || itemsArr.includes("MH-ADMISSIONS ON MID-3 MONTH REVIEW") || itemsArr.includes("MH-ADMISSIONS TO ED ON TIME OF EXIT") 
                        || itemsArr.includes("MH-RESIDENTIAL MOVES") || itemsArr.includes("MH-DATE OF RESIDENTIAL CHANGE OF ADDRESS") || itemsArr.includes("MH-LOCATION OF NEW ADDRESS") || itemsArr.includes("MH-HOUSING TYPE ON EXIT") || itemsArr.includes("MH-KPI - INTAKE")|| itemsArr.includes("MH-KPI - 3 MONTH REVEIEW") || itemsArr.includes("MH-KPI - EXIT")     
                        || itemsArr.includes("MH-MEDICAL DIAGNOSIS DETAILS") || itemsArr.includes("MH-SERVICES LINKED DETAILS") || itemsArr.includes("MH-NDIS TYPE") || itemsArr.includes("MH-NDIS TYPE COMMENTS") 
                        || itemsArr.includes("MH-NDIS NUMBER") || itemsArr.includes("MH-REVIEW APPEAL") || itemsArr.includes("MH-REVIEW COMMENTS")|| itemsArr.includes("MH-KP_Intake_1") || itemsArr.includes("MH-KP_Intake_2") || itemsArr.includes("MH-KP_Intake_3MH") || itemsArr.includes("MH-KP_Intake_3PH") || itemsArr.includes("MH-KP_Intake_4") || itemsArr.includes("MH-KP_Intake_5") 
                        || itemsArr.includes("MH-KP_Intake_6") 
                        || itemsArr.includes("MH-KP_Intake_7") || itemsArr.includes("MH-KP_3Months_1") || itemsArr.includes("MH-KP_3Months_2") || itemsArr.includes("MH-KP_3Months_3MH") || itemsArr.includes("MH-KP_3Months_3PH") || itemsArr.includes("MH-KP_3Months_4") || itemsArr.includes("MH-KP_3Months_5") || itemsArr.includes("MH-KP_3Months_6") || itemsArr.includes("MH-KP_3Months_7")
                        || itemsArr.includes("MH-KP_6Months_1") 
                        || itemsArr.includes("MH-KP_6Months_2") || itemsArr.includes("MH-KP_6Months_3MH") || itemsArr.includes("MH-KP_6Months_3PH") || itemsArr.includes("MH-KP_6Months_4") || itemsArr.includes("MH-KP_6Months_5") || itemsArr.includes("MH-KP_6Months_6") || itemsArr.includes("MH-KP_6Months_7")|| itemsArr.includes("MH-KP_9Months_1") || itemsArr.includes("MH-KP_9Months_2") 
                        || itemsArr.includes("MH-KP_9Months_3MH") 
                        || itemsArr.includes("MH-KP_9Months_3PH") || itemsArr.includes("MH-KP_9Months_4") || itemsArr.includes("MH-KP_9Months_5") || itemsArr.includes("MH-KP_9Months_6") || itemsArr.includes("MH-KP_9Months_7") || itemsArr.includes("MH-KP_Exit_1") || itemsArr.includes("MH-KP_Exit_2") || itemsArr.includes("MH-KP_Exit_3MH") || itemsArr.includes("MH-KP_Exit_3PH") 
                        || itemsArr.includes("MH-KP_Exit_4") || itemsArr.includes("MH-KP_Exit_5") 
                        || itemsArr.includes("MH-KP_Exit_6")|| itemsArr.includes("MH-KP_Exit_7") || itemsArr.includes("MH-KP_Intake_DATE") || itemsArr.includes("MH-KP_3Months_DATE") || itemsArr.includes("MH-KP_6Months_DATE") || itemsArr.includes("MH-KP_9Months_DATE") || itemsArr.includes("MH-KP_Exit_DATE") 
                        ){
                          FromSql = FromSql + "  LEFT JOIN MENTALHEALTHDATASET ON R.UniqueID = MENTALHEALTHDATASET.PersonID "    
                        }         
                        if(itemsArr.includes("Placement Type") || itemsArr.includes("Placement Carer Name") || itemsArr.includes("Placement Start") || itemsArr.includes("Placement End") || itemsArr.includes("Placement Referral") || itemsArr.includes("Placement ATC") || itemsArr.includes("Placement Notes")
                        ){
                          FromSql = FromSql + "  LEFT JOIN HumanResources HRPlacements ON R.UniqueID = HRPlacements.PersonID AND [GROUP] = 'PLACEMENT' "    
                        }                        
                        if( itemsArr.includes("Quote Strategy") || itemsArr.includes("Strategy Expected Outcome") || itemsArr.includes("Strategy Contracted ID") || itemsArr.includes("Strategy DS Services")
                        || itemsArr.includes("Goal  Achieved") || itemsArr.includes("Goal Completed Date") || itemsArr.includes("Goal Last Review Date") || itemsArr.includes("Goal Expected Completion Date") || itemsArr.includes("Quote Goal")
                        ){                          
                          if( itemsArr.includes("Goal  Achieved") || itemsArr.includes("Goal Completed Date") || itemsArr.includes("Goal Last Review Date") || itemsArr.includes("Goal Expected Completion Date") || itemsArr.includes("Quote Goal")
                          ){
                            FromSql = FromSql + "  Right JOIN Documents D   on R.UniqueID = D.PersonID LEFT JOIN HumanResources GOALS on GOALS.PersonID = CONVERT(varchar, D.Doc_ID) "    
                          }                          
                          if( itemsArr.includes("Quote Strategy") || itemsArr.includes("Strategy Expected Outcome") || itemsArr.includes("Strategy Contracted ID") || itemsArr.includes("Strategy DS Services") ){
                            if( itemsArr.includes("Goal  Achieved") || itemsArr.includes("Goal Completed Date") || itemsArr.includes("Goal Last Review Date") || itemsArr.includes("Goal Expected Completion Date") || itemsArr.includes("Quote Goal")){
                              FromSql = FromSql + "  LEFT JOIN HumanResources STRATEGIES on STRATEGIES.PersonID = GOALS.RecordNumber "    
                            }else{
                              FromSql = FromSql + "  Right JOIN Documents D   on R.UniqueID = D.PersonID LEFT JOIN HumanResources GOALS on GOALS.PersonID = CONVERT(varchar, D.Doc_ID) "    
                              FromSql = FromSql + "  LEFT JOIN HumanResources STRATEGIES on STRATEGIES.PersonID = GOALS.RecordNumber "      
                            }          
                          }                    
                        }
                        if( itemsArr.includes("Staff Name") || itemsArr.includes("Program Name") || itemsArr.includes("Notes") ){ 
                          FromSql = FromSql + " LEFT JOIN HumanResources HRCaseStaff ON R.UniqueID = HRCaseStaff.PersonID "      
                          FromSql = FromSql + " INNER JOIN STAFF S ON HRCaseStaff.NAME = S.UNIQUEID "
                          this.includeHRCaseStaffWhere = "  HRCaseStaff.[Group] = 'COORDINATOR'"
                        }
                        if( itemsArr.includes("Case Notes Date") || itemsArr.includes("Case Notes Detail") || itemsArr.includes("Case Notes Creator") || itemsArr.includes("Case Notes Alarm") || itemsArr.includes("Case Notes Program") || itemsArr.includes("Case Notes Category") ){ 
                          FromSql = FromSql + "LEFT JOIN History ON R.UniqueID = History.PersonID"
                          this.includeNotesWhere = " History.ExtraDetail1 = 'CASENOTE'"
                        }
                        if(itemsArr.includes("SvcDetail.Date") || itemsArr.includes("SvcDetail.[Start Time]") || itemsArr.includes("(SvcDetail.[Duration]*5) / 60") || itemsArr.includes("SvcDetail.[Service Type]") || itemsArr.includes("SvcDetail.[ServiceSetting]") ||
                        itemsArr.includes("SvcDetail.[Program]") || itemsArr.includes("SvcDetail.[Type]") || itemsArr.includes("SvcDetail.[HACCType]") || itemsArr.includes("SvcDetail.[Anal]") || itemsArr.includes("SvcDetail.[Unit Pay Rate]") || itemsArr.includes("SvcDetail.[Unit Bill Rate]") 
                         || itemsArr.includes("SvcDetail.[BillQty]") || itemsArr.includes("SvcDetail.[Status]") || itemsArr.includes("SvcDetail.[Service Description]") || itemsArr.includes("SvcDetail.[CostQty]") 
                        ){
                          FromSql = FromSql + " INNER JOIN Roster SvcDetail ON R.accountno = SvcDetail.[client code] "      
                        }
                        if(itemsArr.includes("Humanresourcetypes.[Type]") ){
                          FromSql = FromSql + " INNER JOIN Roster ON R.AccountNo = Roster.[Client Code] LEFT JOIN Humanresourcetypes ON Roster.Program = HumanResourceTypes.Name "      
                        } 
                        /*
                        if(this.ConditionEntity.includes("SvcDetail.Date") || this.ConditionEntity.includes("SvcDetail.[Start Time]") || this.ConditionEntity.includes("(SvcDetail.[Duration]*5) / 60") || this.ConditionEntity.includes("SvcDetail.[Service Type]") || this.ConditionEntity.includes("SvcDetail.[ServiceSetting]") ||
                         this.ConditionEntity.includes("SvcDetail.[Program]") || this.ConditionEntity.includes("SvcDetail.[Type]") || this.ConditionEntity.includes("SvcDetail.[HACCType]") || this.ConditionEntity.includes("SvcDetail.[Anal]") || this.ConditionEntity.includes("SvcDetail.[Unit Pay Rate]") || this.ConditionEntity.includes("SvcDetail.[Unit Bill Rate]") 
                         || this.ConditionEntity.includes("SvcDetail.[BillQty]") || this.ConditionEntity.includes("SvcDetail.[Status]") || this.ConditionEntity.includes("SvcDetail.[Service Description]") || this.ConditionEntity.includes("SvcDetail.[CostQty]") 
                        ){
                          FromSql = FromSql + " INNER JOIN Roster SvcDetail ON R.accountno = SvcDetail.[client code] "      
                        }
                        if(this.ConditionEntity.includes("Humanresourcetypes.[Type]") ){
                          FromSql = FromSql + " INNER JOIN Roster ON R.AccountNo = Roster.[Client Code] LEFT JOIN Humanresourcetypes ON Roster.Program = HumanResourceTypes.Name "      
                        }                        
                        */
                      }
 
                      return FromSql
                    }
        ConvertToCSV(objArray, headerList) {      
          let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
          
          let str = '';
          let row = 'S.No,';
          for (let index in headerList) {
            row += headerList[index] + ',';
          }
          row = row.slice(0, -1);
          str += row + '\r\n';
          for (let i = 0; i < array.length; i++) {
            let line = (i+1)+'';
            for (let index in headerList) {
            let head = headerList[index];
            line += ',' + array[i][head];         
            }
            str += line + '\r\n';
          
          }
          return str;
          }
      downloadFile(data, filename,headings) {         
                  let csvData = this.ConvertToCSV(data, headings );        
                  let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
                  let dwldLink = document.createElement("a");
                  let url = URL.createObjectURL(blob);
                  let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
                  if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
                      dwldLink.setAttribute("target", "_blank");
                  }
                  dwldLink.setAttribute("href", url);
                  dwldLink.setAttribute("download", filename + ".csv");
                  dwldLink.style.visibility = "hidden";
                  document.body.appendChild(dwldLink);
                  dwldLink.click();
                  document.body.removeChild(dwldLink);
      }
      View(e){
        e = e || window.event;
        e = e.target || e.srcElement;
        var RptType = (e.id).toString();          
        this.isVisibleRptType = false;
        if(RptType == 'btn-CustmRpt'){
          this.frm_Customlayout = true;            
        }else{
          this.frm_stdlayout = true;
        }

      }
      SaveCustomScript(){
            var re = /'/gi;
            var Format  = this.RptFormat;
            var CriteriaDisplay = this.inputForm.value.SQLtxt;
            var DateType;
            
            
           
            switch(this.RptFormat){
              case 'AGENCYLIST':
                DateType = "CUSTOMSQL"
              break;
              case 'AGENCYSTFLIST':
                DateType = "CUSTOMSTFSQL"
              break;
              case 'USERSTFLIST':
                DateType = "CUSTOMSTFSQL-" +this.tocken.nameid 
              break;
              case 'USERLIST':
                DateType = "CUSTOMSQL-" + this.tocken.nameid
              break;
              default:

              break;

            }      
            var UserID = this.tocken.nameid;
            var RptSQL = (this.inputForm.value.SQLtxt).toString().replace(re,"~");
            var RptTitle = this.tocken.nameid;
                  
            
            var insertsql = " INSERT INTO ReportNames(Title, Format,SQLText,UserID, DateType, CriteriaDisplay) " +
            " VALUES( '" + RptTitle +"' , '"+Format+"' , '"+RptSQL+"' , '"+UserID+"' , '"+DateType+"' , '"+CriteriaDisplay + "') "   
            if(RptSQL == '' || RptSQL.length <20){
              this. GlobalS.eToast('Failed', 'SQL Text is empty');
            }else{                        
            this.ReportS.InsertReport(insertsql).subscribe(x=>{
              if(x != null) {
                this. GlobalS.sToast('Success', 'Saved successful'); 
                this.router.navigate(['/admin/reports']);    
              }    
            });             
            }
        
          //
        
      }
    
  addGroupColumn(): void {
    if (this.headings.length > 0) {
      this.selectedGroupColumns.push(this.headings[0]); 
    }
  }

  removeGroupColumn(index: number): void {
    this.selectedGroupColumns.splice(index, 1);
    this.applyGroupingAndSorting();
  }
  
  addSortColumn(): void {
    if (this.headings.length > 0) {
      this.sortColumns.push({ column: this.headings[0], direction: 'ascend' }); 
    }
  }

  removeSortColumn(index: number): void {
    this.sortColumns.splice(index, 1);
    this.applyGroupingAndSorting();
  }
  
  applyGroupingAndSorting(): void {
    this.groupData();
    this.sortData();
  }
  
  groupData(): void {
    let grouped = this.tableData;
    this.selectedGroupColumns.forEach((column) => {
      grouped = this.groupByColumn(grouped, column); 
    });
    this.groupedData = grouped;
  }

  
  groupByColumn(data: any[], column: string): any[] {
    const grouped = data.reduce((acc, row) => {
      const key = row[column];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(row);
      return acc;
    }, {});
    return Object.values(grouped); 
  }
  
  sortData(): void {
    let sortedData = this.groupedData;
    this.sortColumns.forEach((sort) => {
      sortedData = this.customSort(sortedData, sort.column, sort.direction);
    });
    this.groupedAndSortedData = sortedData;
  }

  
  customSort(data: any[], column: string, direction: string): any[] {
    return data.sort((a, b) => {
      const aValue = a[column];
      const bValue = b[column];
            
      const type = this.getColumnType(aValue);
      let comparison = 0;
      if (type === 'number') {
        comparison = aValue - bValue;
      } else if (type === 'string') {
        comparison = aValue.localeCompare(bValue);
      } else if (type === 'date') {
        comparison = aValue.getTime() - bValue.getTime();
      } else {
        comparison = 0; 
      }

      if (direction === 'ascend') {
        return comparison;
      } else if (direction === 'descend') {
        return -comparison;
      }
      return 0; 
    });
  }

  getColumnType(value: any): string {
    if (typeof value === 'number') {
      return 'number';
    } else if (typeof value === 'string') {
      return 'string';
    } else if (value instanceof Date) {
      return 'date';
    } else {
      return 'unknown'; 
    }
  }
  onGroupChange(): void {
    this.applyGroupingAndSorting();
  }
  onSortChange(): void {
    this.applyGroupingAndSorting();
  }



      
} //Admin