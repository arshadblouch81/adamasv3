//import { Component, OnInit } from '@angular/core';

import { StaffService, GlobalService, TimeSheetService } from '../../services/index';
import * as moment from 'moment';

//import 'bootstrap.css';
//import '@grapecity/wijmo.styles/wijmo.css';


//
import { Component, OnInit,enableProdMode, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { WjViewerModule } from '@grapecity/wijmo.angular2.viewer';
import { getReports, TReportCategory } from './app.data';
import * as wjcCore from '@grapecity/wijmo';
import { AppModule } from '../../../app/app.module';
import { environment } from '../../../environments/environment';
import { Time } from '@angular/common';

@Component({
    selector: 'report-component',
    templateUrl:'./reports.component.html',
    styles:[
        `  body {
            margin-bottom: 24px;
        }
        
        label {
            margin-right: 3px;
        }
        
        .wj-viewer {
            width: 100%;
            display: block;
        }
        
        .report-names {
            margin: 10px;
        }
        
        @media print {
            .report-names {
                display: none;
            }
        }  
        `]
})

export class ReportsAdmin implements OnInit {
    public categories: TReportCategory[];
    public filePath: string;

    serviceUrl: string = 'http://45.77.37.207:4141/steph/ActiveReports.ReportService.asmx'

    startDate: any = ''; 
    endDate: any = '';
    parameters: any;
    show: boolean = false;

    reportParameters = {
        sql: `select top 100 *  from roster where [carer code]='ABBAS A'`,
        filePath: 'Reports',
        fileName: 'RdlReport1.rdlx'
    }

    constructor(
        private staffS: StaffService,
        private globalS: GlobalService,
        private timeS: TimeSheetService
    ){
       // wjcCore.setLicenseKey('426447146271292');
        let reports = getReports();
        //
        this.categories = reports.categories;
        // Get the selected report's filePath property.
        this.filePath = reports
            .categories.find(cat => cat.name === reports.selectedReport.categoryName)
            .reports.find(rep => rep.reportName === reports.selectedReport.reportName)
            .filePath;
            
    }

    ngOnInit(){


    }

    reportWindow: Window;
    goAhead(){
        // this.reportParameters = {
        //     sql: `select top 100 *  from roster where [carer code]='ABBAS A'`,
        //     filePath: 'Reports',
        //     fileName: 'RdlReport1.rdlx'
        // }

        this.reportWindow = window.open("", "_blank");

        this.timeS.getreportpdfpath(this.reportParameters).subscribe(data => {
            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(data,"text/xml");
            xmlDoc.getElementsByTagName("string")[0].childNodes[0].nodeValue

            const pdfFile = xmlDoc.getElementsByTagName("string")[0].childNodes[0].nodeValue;
            this.goToLink(pdfFile);
        })
    }

    goToLink(url: string){
        this.reportWindow.location.href = url;
        this.reportWindow.focus();
    }

  

    modelChange(data: any){
        this.filePath = data;        
    }

    Go(){
        //this.show = !this.show;
        this.filePath = 'Reports/RdlReport3.rdlx';
        const sdate = moment(this.startDate).format('YYYY/MM/DD');
        const edate = moment(this.endDate).format('YYYY/MM/DD');
        const prog='NDIA - YU G';
        const servce='NDIA SOC/REC PIN CODE START JOB';
         this.parameters= {
            date1: sdate,
            date2: edate,
            program: prog,
            servicetype:servce


         }
 
        console.log(this.parameters)
    }
}

  //Programs: 'NDIA - YU G,DSS SOCIAL SUPPORT,NDIA-NEGRETE M',
  //ServiceType:'NDIA SOC/REC PIN CODE START JOB,NDIA AWDL SELF-CARE ASSIST SUN L1,NDIA AWDL DOMESTIC ACTIVITIES'

//enableProdMode();

// Bootstrap application with hash style navigation and global services.
//platformBrowserDynamic().bootstrapModule(ReportsAdmin);

/*
export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
  }
  
const providers = [
    { provide: 'BASE_URL', useFactory: getBaseUrl, deps: [] }
  ];
  
  if (environment.production) {
    enableProdMode();
  }
  
  platformBrowserDynamic(providers).bootstrapModule(AppModule)
    .catch(err => console.log(err));
    */