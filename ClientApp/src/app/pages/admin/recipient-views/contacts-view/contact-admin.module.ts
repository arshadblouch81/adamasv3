import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';

import {
   ContactsAdmin,
   OthersContact
} from './index';
import { NzIconModule } from 'ng-zorro-antd';


const routes: Routes = [
    {
        path: '',
        redirectTo: 'contact-admin',       
        pathMatch: 'full'
      },
      {
        path: 'contact-admin',
        component: ContactsAdmin,
      },
      {
        path: 'others-contact',
        component: OthersContact
      },
      
      
];

@NgModule({
  imports: [
    FormsModule, 
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NzMessageModule,
    NzNotificationModule,
    NzSelectModule,
    NzGridModule,
    NzIconModule
    
  ],
  declarations: [
   
  ],
  providers: [
    DatePipe,
    DecimalPipe
  ],
  exports: [

  ]
})

export class ContactAdminModule {}

