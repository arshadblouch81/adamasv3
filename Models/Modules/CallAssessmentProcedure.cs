using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class CallAssessmentProcedure{

        public List<ProcedureRoster> Roster { get; set; }
        public ProcedureClientStaffNote Note { get; set; }

    }
}
