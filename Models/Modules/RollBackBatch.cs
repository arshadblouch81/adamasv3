using System;

namespace Adamas.Models.Modules
{
    public class RollbackBatch {
      public string OperatorID { get; set; }
      public DateTime CurrentDateTime { get; set; }
      public string BatchNumber { get; set; }
      public string BatchDescription { get; set; }

    }
}