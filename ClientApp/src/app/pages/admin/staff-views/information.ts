import { Component, OnInit, OnDestroy, Input } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AngularEditorConfig } from '@kolkov/angular-editor';
@Component({
    styles: [`
    ul{
        list-style:none;
    }
    
    div.divider-subs div{
        margin-top:2rem;
    }
    label{
        margin-bottom:15px !important;
    }
    .ant-checkbox-wrapper {
        margin-bottom:15px !important;
    }
    
    `],
    templateUrl: './information.html'
})


export class StaffInformationAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    
    checked: boolean = false;
    isDisabled: boolean = false;
    public editorConfig:AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '37rem',
        minHeight: '37rem',
        translate: 'no',
        customClasses: []
    };
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService
        ) {
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'other-information')) {
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
            this.user = this.sharedS.getPicked();
            if(this.user){
                this.search(this.user);
                this.buildForm();
                
                this.sharedS.emitSaveAll$.subscribe(data => {
                    
                    if(data.type = 'Staff' && (data.tab = 'Payroll')){
                        this.SaveAll_Changes(data);
                    }
                })
                
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        search(user: any) {
            if (!user) this.router.navigate(['/admin/staff/personal']);
            this.timeS.getnotesmiscellaneous(user.id).subscribe(data => {
                this.patchData(data);
            })
        }
        SaveAll_Changes(type:any){
            if (this.inputForm && this.inputForm.dirty) {
                this.save();
            }
        }
        patchData(data: any) {
            this.inputForm.patchValue({
                notes: data.data,
                uniqueId:this.user.id         
            });
        }
        
        
        buildForm() {
            this.inputForm = this.formBuilder.group({
                notes: [''],
                uniqueId:this.user.id
            });
        }
        
        onKeyPress(data: KeyboardEvent) {
            return this.globalS.acceptOnlyNumeric(data);
        }
        
        save() {
            const group = this.inputForm;

            this.timeS.updatemiscellaneous({
                Note: group.get('notes').value,
                UniqueId: this.user.id
            })
            .subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Change successful');
                    this.inputForm.markAsPristine();
                    return;
                }
            });

        }
        edit(){
            
            
            
        }
        canDeactivate() {
            return true;    
        }
    }