﻿namespace AdamasV3.Models.Dto
{
    public class QueryDto
    {
        public string RawQuery { get; set; }
    }
}
