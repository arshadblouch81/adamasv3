import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute, NavigationEnd } from '@angular/router';

import { GlobalService, SettingsService, ShareService, ClientService, TimeSheetService, UserService } from '@services/index';
import { ViewClientPortalDto } from '@services/global.service';
import { Observable, Subject, EMPTY } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: '',
    templateUrl: './home.html',
    styles: [
        `
    .logo {
        height: 2rem;
        background: url(../../../assets/logo/image2.png) no-repeat;
        background-size: 64%;
        margin: 7px 24px;
        width: 10rem;
    }
    
    nz-content {
        margin: 0 16px;
    }

    nz-breadcrumb {
        margin: 16px 0;
    }

    .inner-content {
        padding: 12px;
        background: #fff;
        min-height: 100%;  
    }

    nz-footer {
        text-align: center;
    }

    nz-layout{
        height:100vh;
    }



    .nz-avatar-menu{
        display: inline-block;
        margin-right: 1rem;
    }
    nz-avatar{
        cursor:pointer;
        background-color:#87d068;
    }
    .dropdown{
        position: absolute;
        right: 7px;
        width: 8rem;
        background: #fff;
        border: 1px solid #6161611a;
        z-index: 10;
    }
    .dropdown ul{
        list-style:none;
        padding:0;
        margin:0;
    }
    .dropdown ul li{
        line-height:2.5rem;
        cursor:pointer;
        text-align:center;
    }
    .dropdown ul li i{
        margin-right:10px;
    }
    .dropdown ul li:hover{
        background: #f9f9f9;
    }
    .menu-button{
        text-align:right;
    }

    nz-content {
        background: #85B9D5;
        margin: 6px;
    }

    nz-layout{
        background: #85B9D5;
        height:100vh;
    }

    ul.main-list{
        background:#002060 !important;
    }
    nz-sider{
        background:#002060;
    }
    .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected, .ant-menu.ant-menu-dark .ant-menu-item-selected{
        background-color: #85B9D5;
        color: #004165;
    }
    .ant-menu-submenu-popup.ant-menu-dark .ant-menu-item-selected, .ant-menu.ant-menu-dark .ant-menu-item-selected i{
        color: #004165;
    }
    .ant-menu-dark, .ant-menu-dark .ant-menu-sub{
        background:#004165;
    }
    nz-header {
        padding: 0;
    }
    .logged-in-user{
        height: 3rem;
        display: inline-flex;
        padding: 10px;
        line-height: 1rem;
        width: 100%;
    }
    .logged-in-user div{
        position:relative;
    }
    .belongsto-user{
        font-size: 10px;
        color: #6eff00;
        line-height: 1.5rem;
    }
    .logged-in-user i{
        position: absolute;
        top: 0.5rem;
        right: -2.5rem;
        cursor: pointer;
    }
    .logged-in-user i:hover{
        color:#62ff00;
    }
    .logo-new{
        height: 46px;
        margin-left: 1rem;
    }

    .hide-sidemenu-icon{
        color: white;
        position: absolute;
        z-index: 99;
        font-size: 1.4rem;
        top: 14px;
        left: 28px;
        cursor:pointer;
    }
        `
    ]
})

export class HomeClient implements OnInit {
    isCollapsed = false;
    isVisible: boolean = false;
    hide: boolean = true;

    _settings: SettingsService;
    changeRoute = new Subject<string>();
    currRoute: string;

    pickedUser: any;

    logoPath: any;

    showDocuments: boolean = false;
    showNotes: boolean = false;

    constructor(
        private globalS: GlobalService,
        private settingS: SettingsService,
        private router: Router,
        private sharedS: ShareService,
        private clientS: ClientService,
        private timeS: TimeSheetService,
        private activatedRoute: ActivatedRoute,
        private sanitizer: DomSanitizer,
        private userS: UserService
    ) {
        
       

        var {user} = this.globalS.decode();

        this.changeRoute.pipe(switchMap(index => {
            this.router.navigate([index]);
            return this.settingS.getSettingsObservable(user);
        })).subscribe(index => {
            this.globalS.settings = index;
            console.log(index);
        });

        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd)
          ).subscribe(event => {
            var routeArr = this.removeHomeRoutes(this.createBreadCrumb(this.activatedRoute.root));
            this.currRoute = routeArr[routeArr.length - 1].url;
        });

        // For the Client Manager
        this.sharedS.emitMemberPicked$
        .pipe(
            switchMap(data =>{
                this.globalS.pickedMember = data;
                this.pickedUser = data;

                return this.clientS.getuserinfoname(this.pickedUser.accountNo)
            }),
            switchMap(data => {
                this.globalS.pickedMemberUser = data.name;
                return this.settingS.getSettingsObservable(data.name);
            })
        )
        .subscribe(data => {
            localStorage.removeItem('settings');

            this.globalS.settings = data;
            setTimeout(() => {
                this._settings = this.settingS; 
            }, 100);
        });

        this.userS.getclientPortalView().subscribe((data: ViewClientPortalDto) => {
            this.showDocuments = data.showDocuments;
            this.showNotes = data.showNotesTab;
        });

      
    
    }

    ngOnInit(): void{
        performance.mark('slow-component-init-start');
        this._settings = this.settingS;

        if(this.globalS.pickedMember){
            this.sharedS.emitMemberPickedChange(this.globalS.pickedMember);
        }

        this.timeS.getbrandinglogo('small').subscribe(blob => {
            let objectURL = 'data:image/jpeg;base64,' + blob;
            this.logoPath = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        })

        performance.mark('slow-component-init-end');
        performance.measure('slow-component-init', 'slow-component-init-start', 'slow-component-init-end');
    }

    ngAfterViewInit(): void {
        performance.mark('slow-component-view-start');
        // Component view initialization logic
        performance.mark('slow-component-view-end');
        performance.measure('slow-component-view', 'slow-component-view-start', 'slow-component-view-end');
      }
    
    logout() {
        this.globalS.logout();
    }

    onClickOutside(event: Object) {
        if (event && event['value'] === true) {
            this.isVisible = false;
        }
    }

    createBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: Array<any> = []): Array<any> {
        const children: Array<ActivatedRoute> = route.children;
    
        if (children.length === 0) {
          return breadcrumbs;
        }
    
        for (var child of children) {
          const routeURL: string = child.snapshot.url.map(x => x.path).join('/');
          if (routeURL !== '') {
            url += `/${routeURL}`;
          }
          breadcrumbs.push({ label: routeURL, url: url })
          return this.createBreadCrumb(child, url, breadcrumbs);
        }
    }

    toMemberPage(){
        this.pickedUser = null;
        localStorage.removeItem('settings');
        this.globalS.settings = this.globalS.originalSettings;

        setTimeout(() => {
            this._settings = this.settingS; 
        }, 100);

        localStorage.removeItem('picked_member');
        this.router.navigate(['client/members']);
    }

    removeHomeRoutes(routes: Array<any>): Array<any> {
        let finalRoutes = [...routes];
    
        if (finalRoutes.length === 0) {
          return routes;
        }
    
        var removeIndexes = [];
        for (let route in routes) {
          const url = routes[route].label;
          if (url === 'admin' || url === 'landing') {
            removeIndexes.push(parseInt(route));
          }
        }
    
        for (var i = removeIndexes.length - 1; i >= 0; i--)
          finalRoutes.splice(removeIndexes[i], 1);
    
        finalRoutes.forEach(x => x.label = x.label.replace(/^\w/, c => c.toUpperCase()));
    
        return finalRoutes;
      }
}