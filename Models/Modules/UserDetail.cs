using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Adamas.Models.Modules{
    public class UserDetail {
        // first tab
        public int Recnum { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Usertype { get; set; }
        public string StaffCode { get; set; }
        public string LoginMode { get; set; }
        public string homeBranch { get; set; }
        public int System { get; set; }
        public int Recipients { get; set; }
        public int Staff { get; set; }
        public int Roster { get; set; }
        public int DayManager {get;set;}
        public int Timesheet {get;set;}
        public Boolean? TimesheetPreviousPeriod { get; set; }
        public int Statistics {get;set;}
        public int Financial {get;set;}
        public Boolean reportPreview { get; set; }
        public int InvoiceEnquiry { get; set; }
        public Boolean AllowTypeAhead { get; set; }
        public int SuggestedTimesheets { get; set; }
        public Boolean? PayIntegrityCheck { get; set; }
        public int TimesheetUpdate { get; set; }
        public Boolean AccessCDC { get; set; }
        //recipient tab
        public string RecipientRecordView {get;set;}
        public string StaffRecordView {get;set;}
        public Boolean? DisableBtnReferral { get; set; }
        public Boolean? DisableBtnReferOn { get; set; }
        public Boolean? DisableBtnNotProceeding { get; set; }
        public Boolean? DisableBtnAssess { get; set; }
        public Boolean? DisableBtnAdmit { get; set; }
        public Boolean? DisableBtnUnWait { get; set; }
        public Boolean? DisableBtnDischarge { get; set; }
        public Boolean? DisableBtnSuspend { get; set; }
        public Boolean? DisableBtnReinstate { get; set; }
        public Boolean? DisableBtnDecease { get; set; }
        public Boolean? DisableBtnAdmin { get; set; }
        public Boolean? DisableBtnItem { get; set; }
        public Boolean? DisableBtnPrint { get; set; }
        public Boolean? DisableBtnRoster { get; set; }
        public Boolean? DisableBtnMaster { get; set; }
        public Boolean? DisableBtnOnHold { get; set; }
        public Boolean? AddNewRecipient { get; set; }
        public Boolean? CanChangeClientCode { get; set; }
        public Boolean? CanEditNDIA { get; set; }
        public int AllowProgramTransition { get; set; }
        // viewscope tab
         public Boolean? ViewAllBranches { get; set; }
        public string ViewFilterBranches {get;set;}
        public Boolean? ViewAllProgram { get; set; }
        public string ViewFilter {get;set;}
        public Boolean? ViewAllCategories { get; set; }
        public string ViewFilterCategory {get;set;}
        public Boolean? ViewAllCoordinators { get; set; }
        public string ViewFilterCoord {get;set;}
        public Boolean? ViewAllReminders { get; set; }
        public string ViewFilterReminders {get;set;}
        public Boolean? ViewAllStaffCategories { get; set; }
        public string ViewFilterStaffCategory {get;set;}
        //document tab
        public Boolean? CanMoveImportedDocuments { get; set; }
        public Boolean? KeepOriginalAsImport { get; set; }
        public string RecipientDocFolder { get; set; }
        public Boolean Force_RecipDocFolder { get; set; }
        public string ONIImportExportFolder { get; set; }
        public Boolean Force_ONIImportFolder { get; set; }
        public string ONIArchiveFolder { get; set; }
        public Boolean Force_ONIArchiveFolder { get; set; }
        public string StaffDocFolder { get; set; }
        public Boolean Force_StaffDocFolder { get; set; }
        public string StaffRostersFolder { get; set; }
        public Boolean Force_StaffRosterFolder { get; set; }
        public string ReportExportFolder { get; set; }
        public Boolean Force_ReportExportFolder { get; set; }
        public string ReportSavesFolder { get; set; }
        public Boolean Force_ReportSavesFolder { get; set; }
        public string HACCMDSFolder { get; set; }
        public Boolean Force_HACCMDSFolder { get; set; }
        public string CSTDAMDSFolder { get; set; }
        public Boolean Force_CSTDAMDSFolder { get; set; }
        public string NRCPMDSFolder { get; set; }
        public Boolean Force_NRCPMDSFolder { get; set; }
        public string PayExportFolder { get; set; }
        public Boolean Force_PayExportFolder { get; set; }
        public string BillingExportFolder { get; set; }
        public Boolean Force_BillingExportFolder { get; set; }
        //Roster tab
        public Boolean? ChangeMasterRoster { get; set; }
        public Boolean? AllowRosterReallocate { get; set; }
        public Boolean? AllowMasterSaveAs { get; set; }
        public int ManualRosterCopy { get; set; }
        public int AutoCopyRoster { get; set; }
        public Boolean? CanRosterOvertime { get; set; }
        public Boolean? CanRosterBreakless { get; set; }
        public Boolean? CanRosterConflicts { get; set; }
        public Boolean? EditRosterRecord { get; set; }
        public Boolean? OwnRosterOnly { get; set; }
        //dayManagerForm
        public Boolean? UseDMv2 { get; set; }
        public Boolean? APPROVEDAYMANAGER { get; set; }
        public Boolean? RECIPMGTVIEW { get; set; }
        public Boolean? AllowStaffSwap { get; set; }
        public Boolean? AdminChangeOutputType { get; set; }
        public Boolean? AdminChangeProgram { get; set; }
        public Boolean? AdminChangeActivityCode { get; set; }
        public Boolean? AdminChangePaytype { get; set; }
        public Boolean? AdminChangeDebtor { get; set; }
        public Boolean? AdminChangeBillAmount { get; set; }
        public Boolean? adminChangeBillQty { get; set; }
        public Boolean? AttendeesChangeBillAmount { get; set; }
        public Boolean? AttendeesChangeBillQty { get; set; }
        public Boolean? AdminChangePayQty { get; set; }
        public Boolean? LowChangeActivityCode { get; set; }
        //client portal tab
        public Boolean? AllowsMarketing { get; set; }
        public Boolean? ViewPackageStatement { get; set; }
        public Boolean? CanManagePreferences { get; set; }
        public Boolean? AllowBooking { get; set; }
        public Boolean? CanCreateBooking { get; set; }
        public int? BookingLeadTime { get; set; }
        public Boolean? CanChooseProvider { get; set; }
        public Boolean? ShowProviderPhoto { get; set; }
        public Boolean? CanSeeProviderPhoto { get; set; }
        public Boolean? CanSeeProviderGender { get; set; }
        public Boolean? CanSeeProviderAge { get; set; }
        public Boolean? CanSeeProviderReviews { get; set; }
        public Boolean? CanEditProviderReviews { get; set; }
        public Boolean? HideProviderName { get; set; }
        public Boolean? CanManageServices { get; set; }
        public Boolean? CanCancelService { get; set; }
        public Boolean? CanQueryService { get; set; }
        // main Menu Form 
        public int MMPublishPrintRosters {get;set;}
        public int MMTimesheetProcessing {get;set;}
        public int MMBilling {get;set;}
        public int MMPriceUpdates {get;set;}
        public int MMDexUploads {get;set;}
        public int MMNDIA {get;set;}
        public int MMHacc {get;set;}
        public int MMOtherDS{get;set;}
        public int MMAccounting {get;set;}
        public Boolean MMAnalyseBudget {get;set;}
        public Boolean MMAtAGlance {get;set;}
        public Boolean? UseCloudStaff {get;set;}
        public Boolean? UseCloudRecipients {get;set;}
        public Boolean? UseCloudRosters {get;set;}
        public Boolean? UseCloudDaymanager {get;set;}
        public Boolean? UseCloudTImesheets {get;set;}
        public Boolean? UseCloudReports {get;set;}
        
        // mOBILE FORM
        public Boolean? AllowTravelEntry {get;set;}
        public Boolean? AllowLeaveEntry {get;set;}

        public Boolean? AllowIncidentEntry {get;set;}
        public Boolean? AllowPicUpload {get;set;}
        public Boolean? EnableRosterAvailability {get;set;}
        public Boolean? AllowViewBookings {get;set;}
        public Boolean? AcceptBookings {get;set;}
        public Boolean  ViewClientDocuments {get;set;}
        public Boolean ViewClientCareplans {get;set;}
        public Boolean? AllowViewGoalPlans {get;set;}
        public Boolean AllowTravelClaimWithoutNote {get;set;}
        public Boolean? AllowMTASaveUserPass {get;set;}
        
        public Boolean? AllowOPNote {get;set;}
        public Boolean? AllowCaseNote {get;set;}
        public Boolean AllowClinicalNoteEntry {get;set;}
        public Boolean? AllowRosterNoteEntry {get;set;}
        public Boolean SuppressEmailOnRosterNote {get;set;}
        public Boolean? EnableEmailNotification {get;set;}
        public Boolean UseOPNoteAsShiftReport {get;set;}
        public Boolean UseServiceNoteAsShiftReport {get;set;}
        public String EnableViewNoteCases {get;set;}
        public Boolean ShiftReportReminder {get;set;}
        public int? UserSessionLimit {get;set;}
        public int? MobileFutureLimit {get;set;}
        public string TMMode {get;set;}
        public Boolean? MTAAutRefreshOnLogin {get;set;}
        public Boolean HideClientPhoneInApp {get;set;}
        public Boolean? HideAddress {get;set;}
        public Boolean? AllowSetTime {get;set;}  
        public Boolean? AllowAddAttendee {get;set;}
        public Boolean MultishiftAdminAndMultiple {get;set;}
        public Boolean RestrictTravelSameDay {get;set;}
        public Boolean PushPhonePrefix {get;set;}
        public string PhonePrefix {get;set;}
        public Boolean? Enable_Shift_End_Alarm {get;set;}
        public Boolean? Enable_Shift_Start_Alarm {get;set;}
        public int? CheckAlertInterval {get;set;}
        public Boolean? SystemEmailer {get;set;}
        public Boolean? FullAdmin {get;set;}
        public Boolean? SSO {get;set;}
        public string azureaccount{get;set;}
        
        // new fields also need to add in UI
        public Boolean LimitProgramLookups{get;set;}   
        public Boolean LimitServiceLookups{get;set;}        
        public Boolean LimitStaffLookups{get;set;}   
        public Boolean LimitPayTypeLookups{get;set;}
        public Boolean StaffBranchOperation{get;set;}
        public string AWARDEnforcement{get;set;}
        public string DefAlertsTAB{get;set;}
        public Boolean? BranchReminderFilter{get;set;}
        public Boolean? ShowAllProgramsOnActivate{get;set;}
        public string CoordFilterType {get;set;}
        public Boolean? EmailUserReports {get;set;}
        public string CloudStaffLink {get;set;}
        public string CloudReportsLink {get;set;}
        public string CloudTimesheetsLink {get;set;}
        public string CloudRostersLink {get;set;}
        public string CloudDaymanagerLink {get;set;}
        public string CloudRecipientsLink {get;set;}
        public string ClientPortalViewNotes{get;set;}
        public Boolean HideClientPhone {get;set;}
        public Boolean HiresGraphics {get;set;}
        public Boolean RecipientCurrentDefault{get;set;}
        public Boolean StaffCurrentDefault{get;set;}
        public Boolean AutoAdminTime{get;set;}
        public Boolean UseQikShift {get;set;}
        public Boolean SortTimesheetByDateTime{get;set;}
        public Boolean? CloudAdmin{get;set;}
        
        public Boolean? HidePortalBalance{get;set;}
        public Boolean? ShowAllRecipients{get;set;}
        public bool? ViewClientPortalDocuments { get; set; }
        [StringLength(5)]
        public DateTime? EndDate {get;set;}
        public string CustomDM2 { get; set; }
        public Boolean? UseRecv2{get;set;}
        
        public string EnableViewNotecases { get; set; }
        public Boolean? AllowcaseNote { get; set; }
        
    }
        
    
}