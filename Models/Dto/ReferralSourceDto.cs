﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class ReferralSourceDto
    {
        public string ProgramName { get; set; }
        public string ProgramType { get; set; }
    }
}
