
namespace Adamas.Models.Modules{
    public class CopyAndPasteFile {
        public string FileName  { get; set; }
        public string DocPath { get; set; } 
        public string SourceDocPath { get; set; }
        public string DestinationDocPath { get; set; }
        public string PersonId { get; set; }
    }
}