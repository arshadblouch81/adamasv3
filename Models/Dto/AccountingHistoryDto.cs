﻿using System;

namespace AdamasV3.Models.Dto
{
    public class AccountingHistoryDto
    {
        public int SqlId { get; set; }
        public DateTime? Date { get; set; }
        public string Number { get; set; }
        public decimal? Amount { get; set; } 
        public decimal? Gst { get;set; }
        public decimal? Os { get; set; } 
        public string Type { get; set; }
        public string Package { get; set; }
        public string Notes { get; set; }
    }
}
