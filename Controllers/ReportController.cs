using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.IO;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;

using Microsoft.Extensions.Configuration;
using AdamasV3;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class ReportController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;

        public ReportController(DatabaseContext context, IConfiguration config, IGeneralSetting setting)
        {
            _context = context;
            GenSettings = setting;
        }

        [HttpGet("")]
        public ActionResult Reports()
        {
            string[] validExtensions = {".rdl", ".rdlx", ".rdlx-master", ".rpx"};

            var reportsList = GetEmbeddedReports(validExtensions);
            return new ObjectResult(reportsList);
        }

        /*
		[HttpGet("report")]
        public async Task<IActionResult> GetReports()
        {
             using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader dr = await cmd.ExecuteReaderAsync();
                    while (dr.Read())
                    {
                        list.Add(dr.GetString(0));
                    }
                    return Ok(list);
                }
            }
        } */

        [HttpGet("rosters")]
        public async Task<IActionResult> GetRosterReport(string sdate, string edate, string servicetype)
        {
             using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Client Code],[Date],[Start Time],[Service Type] FROM Roster WHERE Date BETWEEN @sdate AND @edate AND [Service Type] = @servicetype", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@sdate", sdate);
                    cmd.Parameters.AddWithValue("@edate", edate);
                    cmd.Parameters.AddWithValue("@servicetype", servicetype);

                    await conn.OpenAsync();

                    List<object> list = new List<object>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (rd.Read())
                    {
                        list.Add(
                            new {
                                ClientCode = rd.GetString(0),
                                Date = rd.GetString(1),
                                StartTime = rd.GetString(2),
                                ServiceType = rd.GetString(3)
                            }
                        );
                    }
                    return Ok(list);
                }
            }
        }
		[HttpPost("addReport")]
         public async Task<IActionResult> addReport([FromBody] ReportSave obj)  {
            
                try{
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(obj.Sql, (SqlConnection)conn))
                    {
                        await conn.OpenAsync();

                        string getValue =cmd.ExecuteScalar().ToString();
                        // newId = (Int32) cmd.ExecuteScalar();

                        return Ok(getValue);
                    }
                }
            }catch(Exception ex)
            { return Ok(ex);}                     
        }
		
		
		[HttpGet("getreportlist/{format}")]
        public async Task<IActionResult> GetReportNames(string format){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select Title  from ReportNames where Format = @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", format);
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
				//	List<dynamic> list = new List<dynamic>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {							 
                            list.Add(GenSettings.Filter(rd["Title"]));							
                        }
                        return Ok(list);
					}
                }
            }
        }
		[HttpGet("report-format/{name}")]
        public async Task<IActionResult> GetReportFormat(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select Format from ReportNames where title like @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", name);
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }
		[HttpGet("report-sql/{title}/{format}")]
        public async Task<IActionResult> GetReportSql(string title,string format){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select SQLText from ReportNames where title like @RptTitle and Format like @RptFormat", (SqlConnection)conn))
                {
					cmd.Parameters.AddWithValue("@RptTitle", title);
					cmd.Parameters.AddWithValue("@RptFormat", format);
                    
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {                            
							list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }
		
		
		[HttpGet("branchviewfilter/{name}")]
        public async Task<IActionResult> GetBranchFilters(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select ViewFilterBranches from UserInfo where Name like @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", name);
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }
		[HttpGet("Programviewfilter/{name}")]
        public async Task<IActionResult> GetProgramFilters(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select ViewFilter from UserInfo where Name like @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", name);
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }
		[HttpGet("coordinaterviewfilter/{name}")]
        public async Task<IActionResult> GetCoordinaterFilters(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select ViewFilterCoOrd from UserInfo where Name like @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", name);
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }

        [HttpGet("staffgroupviewfilter/{name}")]
        public async Task<IActionResult> GetStaffCategoryFilters(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select ViewFilterStaffCategory from UserInfo where Name like @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", name);
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }

		[HttpGet("categoryviewfilter/{name}")]
        public async Task<IActionResult> GetCategoryFilters(string name){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select ViewFilterCategory from UserInfo where Name like @txt", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@txt", name);
                   await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
					}
                }
            }
        }
         [HttpGet("getlist/{sql}")]
        public async Task<IActionResult> GetReportlist(string sql){
            //public async Task<IActionResult> GetReportlist(string col,string tbl){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
				    //List<dynamic> list = new List<dynamic>();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while(await rd.ReadAsync())
                        {							 
                            list.Add(GenSettings.Filter(rd[0]));							
                        }
                        return Ok(list);
					}
                }
            }
        }



					
        [HttpGet, Route("referrallist")]
        public async Task<IActionResult> GetReferralList(){
            using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT
                                    DISTINCT R.UniqueID
                                    ,R.AccountNo
                                    ,R.AgencyIdReportingCode
                                    ,R.[Surname/Organisation]
                                    ,R.FirstName
                                    ,R.Branch
                                    ,R.RECIPIENT_COORDINATOR
                                    ,R.AgencyDefinedGroup
                                    ,R.ONIRating
                                    ,R.AdmissionDate AS [Activation Date]
                                    
                                    ,UPPER([Surname/Organisation]) + ', ' + CASE 
                                        WHEN FirstName <> ''
                                            THEN FirstName
                                        ELSE ' '
                                        END AS RecipientName
                                    ,CASE 
                                        WHEN N1.Address <> ''
                                            THEN N1.Address
                                        ELSE N2.Address
                                        END AS ADDRESS
                                    ,CASE 
                                        WHEN P1.Contact <> ''
                                            THEN P1.Contact
                                        ELSE P2.Contact
                                        END AS CONTACT
                                    ,(
                                        SELECT TOP 1 DATE
                                        FROM Roster
                                        WHERE Type IN (
                                                2
                                                ,3
                                                ,7
                                                ,8
                                                ,9
                                                ,10
                                                ,11
                                                ,12
                                                )
                                            AND [Client Code] = R.AccountNo
                                        ORDER BY DATE DESC
                                        ) AS LastDate
                                FROM Recipients R
                                LEFT JOIN RecipientPrograms ON RecipientPrograms.PersonID = R.UniqueID
                                LEFT JOIN HumanResourceTypes ON HumanResourceTypes.Name = RecipientPrograms.Program
                                LEFT JOIN ServiceOverview ON ServiceOverview.PersonID = R.UniqueID
                                LEFT JOIN (
                                    SELECT PERSONID
                                        ,CASE 
                                            WHEN Address1 <> ''
                                                THEN Address1 + ' '
                                            ELSE ' '
                                            END + CASE 
                                            WHEN Address2 <> ''
                                                THEN Address2 + ' '
                                            ELSE ' '
                                            END + CASE 
                                            WHEN Suburb <> ''
                                                THEN Suburb + ' '
                                            ELSE ' '
                                            END + CASE 
                                            WHEN Postcode <> ''
                                                THEN Postcode
                                            ELSE ' '
                                            END AS Address
                                    FROM NamesAndAddresses
                                    WHERE PrimaryAddress = 1
                                    ) AS N1 ON N1.PersonID = R.UniqueID
                                LEFT JOIN (
                                    SELECT PERSONID
                                        ,CASE 
                                            WHEN Address1 <> ''
                                                THEN Address1 + ' '
                                            ELSE ' '
                                            END + CASE 
                                            WHEN Address2 <> ''
                                                THEN Address2 + ' '
                                            ELSE ' '
                                            END + CASE 
                                            WHEN Suburb <> ''
                                                THEN Suburb + ' '
                                            ELSE ' '
                                            END + CASE 
                                            WHEN Postcode <> ''
                                                THEN Postcode
                                            ELSE ' '
                                            END AS Address
                                    FROM NamesAndAddresses
                                    WHERE PrimaryAddress <> 1
                                    ) AS N2 ON N2.PersonID = R.UniqueID
                                LEFT JOIN (
                                    SELECT PersonID
                                        ,PhoneFaxOther.Type + ' ' + CASE 
                                            WHEN Detail <> ''
                                                THEN Detail
                                            ELSE ' '
                                            END AS Contact
                                    FROM PhoneFaxOther
                                    WHERE PrimaryPhone = 1
                                    ) AS P1 ON P1.PersonID = R.UniqueID
                                LEFT JOIN (
                                    SELECT PersonID
                                        ,PhoneFaxOther.Type + ' ' + CASE 
                                            WHEN Detail <> ''
                                                THEN Detail
                                            ELSE ' '
                                            END AS Contact
                                    FROM PhoneFaxOther
                                    WHERE PrimaryPhone <> 1
                                    ) AS P2 ON P2.PersonID = R.UniqueID
                                WHERE R.[AccountNo] > '!MULTIPLE'
                                    AND (
                                        [R].[Type] = 'RECIPIENT'
                                        OR [R].[Type] = 'CARER/RECIPIENT'
                                        )
                                    AND (RecipientPrograms.ProgramStatus = 'ACTIVE')
                                    AND (
                                        (R.AdmissionDate IS NOT NULL)
                                        AND (DischargeDate IS NULL)
                                        )
                                ORDER BY R.[Surname/Organisation]
                                    ,R.FirstName",(SqlConnection) conn)){
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    while(await rd.ReadAsync())
                    {
                        list.Add(new {
                            Surname = GenSettings.Filter(rd["Surname/organisation"]),
                            Firstname = GenSettings.Filter(rd["FirstName"]),
                            ActivationDate = GenSettings.Filter(rd["Activation Date"]),
                            OniRating = GenSettings.Filter(rd["ONIRating"]),
                            Phone = GenSettings.Filter(rd["Contact"]),
                            Address = GenSettings.Filter(rd["Address"]),
                            LastService = GenSettings.Filter(rd["LastDate"])
                        });
                    }
                    return Ok(list);
                }
            }
        }


        /// <summary>
        /// Gets report names from assembly resources
        /// </summary>
        /// <returns>Report names</returns>
        private static string[] GetEmbeddedReports(string[] validExtensions) =>
            typeof(ReportController).Assembly.GetManifestResourceNames()
                .Where(x => x.StartsWith(Startup.EmbeddedReportsPrefix))
                .Where(x => validExtensions.Any(x.EndsWith))
                .Select(x => x.Substring(Startup.EmbeddedReportsPrefix.Length + 1))
                .ToArray();

        

    }					
	public class ReportSave{  		   		
		public string Sql { get; set; }
		
    }
}
