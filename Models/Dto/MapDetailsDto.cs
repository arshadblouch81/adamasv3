﻿namespace AdamasV3.Models.Dto
{
    public class MapDetailsDto
    {
        public string OriginDestination { get; set; }
        public string FinalDestination { get; set; }
        public string ResultType { get; set; }
        public string GoogleCustID { get; set; }
        public string TravelProvider { get; set; }
        public string MapApiKey { get; set; }
    }
}
