
namespace Adamas.Models.Modules
{
       public class ComputeTimeInput{
        public string AccountName { get; set; }
        public bool IsCarerCode { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
    }       
}