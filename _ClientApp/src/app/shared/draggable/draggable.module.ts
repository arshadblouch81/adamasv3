import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { DraggableTimeDirective } from './draggable.directive'
import { MovableDirective } from './movable.directive'
import { DroppableDirective } from './droppable.directive'
import { ContainerDraggableDirective } from './containerDraggable.directive'
@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        DraggableTimeDirective,
        MovableDirective,
        DroppableDirective,
        ContainerDraggableDirective
    ],
    exports:[
        DraggableTimeDirective,
        MovableDirective,
        DroppableDirective,
        ContainerDraggableDirective
    ]
})

export class DraggableModule { }
