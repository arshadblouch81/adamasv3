﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AdamasV3.Models.Modules;
using Microsoft.Extensions.Options;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JSReportController : ControllerBase
    {
        private readonly JSReportConfiguration _appsettings;

        public JSReportController(
            IOptions<JSReportConfiguration> appsettings
            )
        {
            _appsettings = appsettings.Value;
            //_config = config;
        }

        [HttpGet("configuration")]
        public async Task<IActionResult> GetConfiguration(string program)
        {
            return Ok(_appsettings);
        }
    }
}
