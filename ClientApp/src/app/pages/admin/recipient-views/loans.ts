import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { format } from 'date-fns';

@Component({
    selector: '',
    templateUrl: './loans.html',
    styles: [`
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
        }
        
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class RecipientLoans implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;
    OpenAddModel: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;
    tableData: Array<any> = [];
    alist: Array<any> = [];
    alist2: Array<any> = [];
    loantypes: Array<any> = [];
    lstprograms: Array<any> = [];
    activeRowData:any;
    selectedRowIndex:number;
    selectedServiceAgreement:any;
    selectedprogram:any;
    isLoading:boolean;
    ItemID:any;
    option: string = 'add';
    dateFormat:string='dd/MM/yyyy'
    record:any;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'loans')) {
                this.user = data;
                this.search(data);
            }
        });

       
    }

    ngOnInit(): void {
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }

    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm( data: any): void {
      
       
          this.delete(data);
        

    }
    search(user: any = this.user) {
        this.cd.reattach();

        this.loading = true;
       // let sqlPlan=`SELECT Title AS FileTypes FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY FileTypes `
        this.listS.getreceiptloans(user.id).subscribe(data => {
            this.tableData = data;
            this.loading = false;
            this.cd.markForCheck();
        });

        this.listS.getloansType().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.alist = data;
           
        });

        this.listS.getloansGoods().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.alist2 = data;
           
        });
        this.listS.getloanprograms().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.lstprograms = data;
           
        });
        
        

    //    let sql=`SELECT Name FROM HumanResourceTypes HR WHERE  [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= convert(varchar,getDate(),111))  ORDER BY [Name]`;
    //     this.listS.getlist(sql).subscribe(data => {
    //         this.lstprograms = data;
    //         //this.cd.markForCheck();
    //     });

    }

    buildForm() {

        let today= new Date();
        this.inputForm = this.formBuilder.group({
            Type: [null, Validators.required],
            ItemID: [null, Validators.required],
            loanDate: [today, Validators.required],
            installed: [today],
            collectionDate: null,
            program: [null, Validators.required],
            EquipmentCode: [''],
            SerialNo: [''],
            purchaseDate: [today],
            PurchaseAmount: [''],
            lockboxLoc: [''],
            lockboxCode: [''],
            notes: [''],
            RecordNumber:0



        })

    }
    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }
    save() {

        let collection = this.inputForm.value.collectionDate;
        let input={
            type: this.inputForm.value.Type,
            item: this.inputForm.value.ItemID,
            program: this.inputForm.value.program,
            loaned: format(this.inputForm.value.loanDate, 'yyyy/MM/dd'),
            dateInstalled: format(this.inputForm.value.installed, 'yyyy/MM/dd'),
            collected: collection? format(collection, 'yyyy/MM/dd') : null,          
            equipmentCode: this.inputForm.value.EquipmentCode,
            serialNumber: this.inputForm.value.SerialNo,
            purchaseDate: format(this.inputForm.value.purchaseDate, 'yyyy/MM/dd') ,
            purchaseAmount: this.inputForm.value.PurchaseAmount,
            lockBoxLocation: this.inputForm.value.lockboxLoc,
            lockBoxCode: this.inputForm.value.lockboxCode,
            notes: this.inputForm.value.notes,
            personID: this.user.id,
            recordNumber:0
        }

        if (this.addOREdit==2){
            input.recordNumber=this.activeRowData.recordNumber;

            this.listS.updaterecipientloan(input,this.user.id).subscribe(d=>{
                if(d){
                    this.search();
                    this.OpenAddModel = false;
                    this.globalS.sToast('Success','Loan updated')
                    this.inputForm.reset();
                }
            
            });

        }else{
        this.listS.postrecipientloan(input,this.user.id).subscribe(d=>{
            if(d){
                this.search();
                this.OpenAddModel = false;
                this.globalS.sToast('Success','Loan Added')
                this.inputForm.reset();
            }
        
        });
    }

    }

    showEditModal(index: number) {

        this.selectedprogram=this.activeRowData.program;
        this.inputForm.patchValue({
            Type: this.activeRowData.type,
            ItemID: this.activeRowData.name,           
            program: this.activeRowData.program,
            EquipmentCode: this.activeRowData.equipmentCode,
            SerialNo: this.activeRowData.serialNumber,
            purchaseDate: new Date(this.activeRowData.purchaseDate),
            PurchaseAmount: this.activeRowData.purchaseAmount,
            lockboxLoc: this.activeRowData.lockBoxLocation,
            lockboxCode: this.activeRowData.lockBoxCode,
            notes: this.activeRowData.notes,
            loanDate: new Date(this.activeRowData.dateLoaned),
            installed: new Date(this.activeRowData.dateInstalled),
            collectionDate: new Date(this.activeRowData.collected),
            

        })
        console.log(this.lstprograms);
        this.addOREdit=2;
        this.OpenAddModel=true;

    }

    delete(data: any) {
        this.timeS.deleteintakeservicecompetency(data.recordNumber).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                            if(data){
                                this.search()
                                this.loading = false
                                this.OpenAddModel=false;
                                this.globalS.sToast('Success','Service Deleted')
                            }
             })
    }

    handleCancel() {
        this.OpenAddModel = false;   
    }

    selectService(data:any, i:number){
        this.selectedServiceAgreement=data;
        this.selectedRowIndex=i;
      }

    showAddModal() {
        this.addOREdit = 1;
        this.modalOpen = true;
    }
    AllocatePlan(data:any, i:number){
               
    this.option='add';
    
        this.ItemID=data;
        this.inputForm.patchValue({ItemID:data.title});
    
                  this.modalOpen = false;
                  this.OpenAddModel = true;
    }
    refreshQuote(data: any){
        if(data){
            console.log('refresh')
            this.search();
        }
    }
    print(){
        
    }
    generatePdf(){
      
        
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Loan"                     
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Loan "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}