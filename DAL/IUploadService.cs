using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.IO;
using Adamas.Models.Modules;
using AdamasV3.Models.Dto;

namespace Adamas.DAL
{
    public interface IUploadService
    {
        Task<Boolean> FileUploadAsync(string fileDirectory, List<IFormFile> files, bool restrict = true);
        Task<String> GetUploadDownloadDirectoryAsync(SqlTransaction transaction = null);
        Task<MemoryStream> DownloadDocument(GetDocument doc);
        Task<bool> PostDocumentStaffTemplateReferral(PostTemplate template);
        Task<bool> PostDocumentStaffTemplateReferral(PostTemplate template, SqlTransaction transaction, string sourcePathWithFile, string docSource);
    }
}