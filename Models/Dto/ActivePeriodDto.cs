// DateTime dtStart, DateTime dtClose, bool is_where = false

using System;

public class ActivePeriodDto {
    public DateTime DateStart { get; set; }
    public DateTime DateEnd { get; set; }
    public bool? IsWhere { get; set; }
}