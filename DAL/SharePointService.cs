using Azure.Identity;
using Microsoft.Graph;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using Microsoft.Identity.Client;
using Microsoft.Graph.Core.Requests;
public class SharePointService
{
    private readonly GraphServiceClient _graphClient;
    
    private string tenantId;
    private string clientId;
    private string clientSecret;
    private string siteId;
    private string siteLink;
    private string driveId;
    private ClientSecretCredential credential;
    public SharePointService(IConfiguration configuration)
    {
        this.tenantId = configuration["AzureAd:TenantId"];
        this.clientId = configuration["AzureAd:ClientId"];
        this.clientSecret = configuration["AzureAd:ClientSecret"];
        this.siteId = configuration["AzureAd:SharePointSiteId"];
        this.siteLink = configuration["AzureAd:SiteLink"];

        // Use ClientSecretCredential for authentication
         credential = new ClientSecretCredential(tenantId, clientId, clientSecret);
         var authProvider = new CustomAuthenticationProvider(credential);
        // Initialize GraphServiceClient with TokenCredential
        _graphClient = new GraphServiceClient(authProvider);
    }
public async Task<string> Authenticate(){

 try
    {

          var token = await credential.GetTokenAsync(
            new Azure.Core.TokenRequestContext(new[] { "https://graph.microsoft.com/.default" }),
            default);

     var drives = await _graphClient.Sites[siteId].Drives
        .Request()
        .GetAsync();

    // Get the Drive ID (assuming the first drive is the document library)
         driveId = drives.First().Id;

        // Make a test request to the Graph API
        var sites = await _graphClient.Sites
            .Request()
            .Top(1) // Fetch only 1 site to test
            .GetAsync();

        // Return the Site ID
          Console.WriteLine("Authentication successful!");
        return  "Authentication successful!";
    
      
    }
    catch (ServiceException ex)
    {
        Console.WriteLine("Authentication failed!");
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine($"Status Code: {ex.StatusCode}");
        Console.WriteLine($"Raw Response: {ex.RawResponseBody}");
    }

    return null;

    
}
public async Task<string> GetAccessTokenAsync()
{
    try
    {
        var token = await credential.GetTokenAsync(
            new Azure.Core.TokenRequestContext(new[] { "https://graph.microsoft.com/.default" }),
            default);


        Console.WriteLine("Authentication successful!");  
        return token.Token;
    }
    catch (ServiceException ex)
    {
        Console.WriteLine("Authentication failed!");
        Console.WriteLine($"Error: {ex.Message}");
        Console.WriteLine($"Status Code: {ex.StatusCode}");
        Console.WriteLine($"Raw Response: {ex.RawResponseBody}");
    }

    return null;
}
    public async Task<List<string>> GetDocumentLibraryItemsAsync()
    {
         var drives = await _graphClient.Sites[siteId].Drives
        .Request()
        .GetAsync();

    // Get the Drive ID (assuming the first drive is the document library)
        string driveId = drives.Last().Id;

         var driveItems = await _graphClient.Drives[driveId].Root.Children
            .Request()
            .GetAsync();

        // Loop through each item and download the file content

        var files = new List<string>();
    foreach (var item in driveItems)
    {
        if (item.File != null) // Check if the item is a file
        {
            // Download the file content
            // var file = await _graphClient.Drives[driveId].Items[item.Id].webUrl
            //     .Request()
            //     .GetAsync();

                files.Add(item.WebUrl);

          
        }

        
    }
        return files;

    }

public async Task<List<(DriveItem FileInfo, byte[] FileContent)>> GetDocumentsAsync()
{
    // Get the drives for the specified site
    var drives = await _graphClient.Sites[siteId].Drives
        .Request()
        .GetAsync();

    // Get the Drive ID (assuming the first drive is the document library)
    string driveId = drives.Last().Id;

    // Get the items in the root of the drive
    var driveItems = await _graphClient.Drives[driveId].Root.Children
        .Request()
        .GetAsync();

    // List to store file info and content
    var filesWithContent = new List<(DriveItem FileInfo, byte[] FileContent)>();

    // Loop through each item and download the file content
    foreach (var item in driveItems)
    {
        if (item.File != null) // Check if the item is a file
        {
            // Download the file content
            var fileContent = await _graphClient.Drives[driveId].Items[item.Id].Content
                .Request()
                .GetAsync();

            // Convert the file content stream to a byte array
            using (var memoryStream = new System.IO.MemoryStream())
            {
                await fileContent.CopyToAsync(memoryStream);
                var fileBytes = memoryStream.ToArray();

                // Add the file info and content to the list
                filesWithContent.Add((item, fileBytes));
            }
        }
    }

    return filesWithContent;
}
}
