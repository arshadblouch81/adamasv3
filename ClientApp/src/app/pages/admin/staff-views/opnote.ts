import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';

import format from 'date-fns/format';
import getYear from 'date-fns/getYear';
import getDate from 'date-fns/getDate';
import getMonth from 'date-fns/getMonth';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularEditorConfig } from '@kolkov/angular-editor';

import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
    styles: [`
    nz-table{
        margin-top:0px;
    }
     nz-select{
        width:100%;
    }
    label.chk{
        position: absolute;
        top: 1.5rem;
    }
    .overflow-list{
        overflow: auto;
        height: 8rem;
        border: 1px solid #e3e3e3;
    }
    ul{
        list-style:none;
    }
    .chkboxes{
        padding:4px;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
    }
     tbody  tr:nth-child(odd) {
    background-color: #E9F7FF;
      }
        tbody  tr:nth-child(even) {
            background-color: #fff;
        }
    `],
    templateUrl: './opnote.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffOPAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;

    modalOpen: boolean = false;
    isLoading: boolean = false;
    

    default = {
        notes: '',
        isPrivate: false,
        alarmDate: null,
        whocode: '',
        recordNumber: null,
        category: null
    }

    dateFormat: string = 'dd/MM/yyyy';
    addOREdit: number;
    categories: Array<any>;
    loading:boolean = false;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    enddate : Date;
    startdate: Date;  
    bodystyle:object;   
    isVisibleTop = false;
    orderArr: Array<any> = ['Chronological-First To Last', 'Chronological-Last To First'];   
    mlist: Array<any> = [];
    
    public editorConfig:AngularEditorConfig = {
        editable: true,
        spellcheck: true,
        height: '20rem',
        minHeight: '5rem',
        translate: 'no',
        customClasses: []
    };

    content = ''; // Editor content

    recipientStrArr: any;
    restrict_list: any;
    selectedRow  : number = 0;
    activeRowData:any;


    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private http: HttpClient,
        private sanitizer: DomSanitizer,
        private ModalS: NzModalService,
        private printS: PrintService
    ) {
        cd.detach();
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/staff/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'op-note')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.user = this.sharedS.getPicked();
        if(this.user){
            this.search(this.user);
            this.buildForm();
            return;
        }
        this.router.navigate(['/admin/staff/personal'])
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
    
    HtmlToRtfconverter(html: string): string {
        // Replace basic HTML tags with RTF equivalents
        html = html.replace(/<b>/g, '\\b ')  // Bold
                   .replace(/<\/b>/g, '\\b0 ') // End bold
                   .replace(/<i>/g, '\\i ')  // Italics
                   .replace(/<\/i>/g, '\\i0 ') // End italics
                   .replace(/<u>/g, '\\u ')  // Underline
                   .replace(/<\/u>/g, '\\u0 ') // End underline
                   .replace(/<p align="center">/g, '\\qc ') // Center alignment
                   .replace(/<p align="right">/g, '\\qr ')  // Right alignment
                   .replace(/<p align="left">/g, '\\ql ')   // Left alignment (default)
                   .replace(/<\/p>/g, '\\par ') // End paragraph
                   .replace(/<br>/g, '\\par ') // Line break
                   .replace(/<font color="(.*?)">/g, (match, p1) => `\\cf1\\highlight${p1} `) // Color (simple example)
                   .replace(/<\/font>/g, '\\cf0\\highlight0 ') // End color
    
        return `{\\rtf1\\ansi\\deff0 {\\fonttbl {\\f0 Arial;}} \\viewkind4\\uc1 \\pard ${html} \\par}`;
    }
    
    

    RtfToHtmlConverter(rtf: string): string {
        return rtf
      // Remove RTF header
      .replace(/{\\rtf1[\\s\\S]*?}/, '')
      
      // Convert bold text
      .replace(/\\b(.*?)\\b0/g, '<b>$1</b>')
      
      // Convert italic text
      .replace(/\\i(.*?)\\i0/g, '<i>$1</i>')
      
      // Convert underlined text
      .replace(/\\ul(.*?)\\ulnone/g, '<u>$1</u>')
      
      // Convert strikethrough text
      .replace(/\\strike(.*?)\\strike0/g, '<s>$1</s>')
      
      // Convert font sizes (RTF uses half-points, so divide by 2)
      .replace(/\\fs([0-9]+) (.*?)(?=\\f[0-9]+|$)/g, '<span style="font-size:${parseInt("$1") / 2}px;">$2</span>')
      
      // Convert text color (assumes a color table exists in RTF, which should be extracted separately)
      .replace(/\\cf([0-9]+) (.*?)(?=\\cf[0-9]+|$)/g, '<span style="color:var(--color$1);">$2</span>')
      
      // Convert background color
      .replace(/\\highlight([0-9]+) (.*?)(?=\\highlight[0-9]+|$)/g, '<span style="background-color:var(--highlight$1);">$2</span>')
      
      // Convert line breaks
      .replace(/\\par/g, '<br>')
      
      // Convert unordered lists (basic approach)
      .replace(/\\u8226 (.*?)\\par/g, '<ul><li>$1</li></ul>')
      
      // Convert numbered lists
      .replace(/\\pntext(.*?)\\par/g, '<ol><li>$1</li></ol>')
      
      // Convert tables (simple extraction of rows and cells)
      .replace(/\\trowd(.*?)\\cell/g, '<table><tr><td>$1</td></tr></table>')
      
      // Handle nested tables (experimental)
      .replace(/\\trowd(.*?)\\row/g, '<tr>$1</tr>')
      .replace(/\\cell(.*?)\\cell/g, '<td>$1</td>')
      .replace(/\\row/g, '</tr>')
      
      // Remove any remaining RTF commands
      .replace(/\\[a-zA-Z0-9]+ ?/g, '')
      
      .trim();
      }


    buildForm() {
        this.inputForm = this.formBuilder.group({
            notes: '',
            privateFlag: false,
            alarmDate: null,
            publishToApp:false,
            whocode: '',
            restrictions: '',
            restrictionsStr:'public',
            recordNumber: null,
            category: [null, [Validators.required]],
            orderArr:['Chronological-First To Last'],
        });

        this.inputForm.get('restrictionsStr').valueChanges.subscribe(data => {
            if (data == 'restrict') {
                this.getSelect();
            } 
        });
    }
    getSelect() {
        this.timeS.getmanagerop().subscribe(data => {
            data.forEach(x => {
                this.mlist.push({
                     name:x, value:x, checked:false
                  });
            });
            this.cd.markForCheck();
        });
        

    //     this.timeS.getdisciplineop().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //         data.push('*VARIOUS');
    //         this.blist = data;
    //     });
    //     this.timeS.getcaredomainop().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //         data.push('*VARIOUS');
    //         this.clist = data;
    //     });
    //     this.timeS.getprogramop(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //         data.push('*VARIOUS');
    //         this.alist = data;
    //     });

    //     this.timeS.getcategoryop().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
    //         this.dlist = data;
    //     })
    }
    log(event: any) {
        this.recipientStrArr = event;
    }
    listStringify(): string {
        let tempStr = '';
        this.recipientStrArr.forEach((data, index, array) => {
            array.length - 1 != index ?
                tempStr += data.trim() + '|' :
                tempStr += data.trim();
        });
        return tempStr;
    }
    search(user: any = this.user) {
        this.cd.reattach();
        this.isLoading = true;
        this.timeS.getopnotes(user.code).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.tableData = data;
            this.originalTableData = data;
            this.isLoading = false;
            this.selectedRow = 0;
            this.cd.detectChanges();
        });

        this.listS.getlistop().pipe(takeUntil(this.unsubscribe)).subscribe(data => this.categories = data)
    }

    selectedItemGroup(data:any, i:number){
        this.selectedRow=i;
        this.activeRowData=data;
    }

    trackByFn(index, item) {
        return item.id;
    }

    showAddModal() {
        this.addOREdit = 1;
        this.modalOpen = true;
    }

    save() {
        if (!this.globalS.IsFormValid(this.inputForm))
            return;        
        
        const cleanDate = this.globalS.VALIDATE_AND_FIX_DATETIMEZONE_ANOMALY(
            this.inputForm.get('alarmDate').value);
        
        this.inputForm.controls["alarmDate"].setValue(cleanDate);

        const { alarmDate, restrictionsStr, whocode, restrictions } = this.inputForm.value;

        let privateFlag = restrictionsStr == 'workgroup' ? true : false;
            
        let restricts = restrictionsStr != 'restrict';

        this.inputForm.controls["restrictionsStr"].setValue(privateFlag);

        this.inputForm.controls["privateFlag"].setValue(privateFlag);

        this.inputForm.controls["notes"].setValue(this.inputForm.get('notes').value);

        this.inputForm.controls["restrictions"].setValue(restricts ? '' : this.listStringify());

        this.isLoading = true;
        if (this.addOREdit == 1) {
            this.inputForm.controls["whocode"].setValue(this.user.code);
            this.timeS.postopnote(this.inputForm.value, this.user.id).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Note saved');
                        this.search();
                        this.handleCancel();
                        return;
                    }
                });
        }

        if (this.addOREdit == 2) {
            this.timeS.updateopnote(this.inputForm.value, this.inputForm.value.recordNumber).pipe(
                takeUntil(this.unsubscribe)).subscribe(data => {
                    if (data) {
                        this.globalS.sToast('Success', 'Note Updated');
                        this.search();
                        if(!this.globalS.isEmpty(this.restrict_list)){
                            this.mlist.forEach(element => {
                                    element.checked = false;
                            });
                        }
                        this.restrict_list = [];
                        
                        this.handleCancel();
                        return;
                    }else{
                        this.globalS.sToast('error', 'Some thing weng wrong');
                        this.search();
                        this.handleCancel();
                    }
                });
        }
    }

    showEditModal() {
        this.addOREdit = 2;
        const { alarmDate, detail,detail2,category, creator,restrictions,isPrivate, recordNumber } = this.tableData[this.selectedRow];

        this.restrict_list = restrictions.split('|');

        this.inputForm.patchValue({
            notes: this.isRTF(detail,detail2),
            isPrivate: isPrivate,
            alarmDate: alarmDate,
            restrictions: '',
            restrictionsStr: this.determineRadioButtonValue(isPrivate, restrictions),
            whocode: creator,
            recordNumber: recordNumber,
            category: category
        });   
        
        if(!this.globalS.isEmpty(this.restrict_list)){
            this.mlist.forEach(element => {
                if(this.restrict_list.includes(element.name)){
                    element.checked = true;
                }
            });
        }
        this.modalOpen = true;
        this.cd.detectChanges();
    }
    isRTF(detail:string,detail2:string){
        let respone = this.globalS.isRTF(detail);
        if(respone){
            return detail2;
        }else{
            return detail;
        }
    }
    determineRadioButtonValue(privateFlag: Boolean, restrictions: string): string {
        if (!privateFlag && this.globalS.isEmpty(restrictions)) {
            return 'public';
        }

        if (!privateFlag && !this.globalS.isEmpty(restrictions)) {
            return 'restrict'
        }

        return 'workgroup';
    }
    delete() {        
        const { recordNumber } = this.tableData[this.selectedRow];
        this.timeS.deleteopnote(recordNumber).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Note Deleted');
                    this.search();
                    this.handleCancel();
                }
            });
    }

    handleCancel() {
        this.modalOpen = false;
        this.inputForm.reset();
        this.isLoading = false;
    }
    handleOkTop() {
        this.isVisibleTop = true;        
        this.tryDoctype = ""
        this.pdfTitle = ""
    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
        this.tryDoctype = ""
        this.isVisibleTop = false;
    }
    generatePdf(){
        var lblcriteria,strdate, endate;

        this.isVisibleTop = false;        
        

        var date = new Date();
        if (this.startdate != null) { strdate = format(this.startdate, 'yyyy-MM-dd') } else {                       
            strdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-MM-dd')
        }
        if (this.enddate != null) { endate = format(this.enddate, 'yyyy-MM-dd') } else {
            endate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy-MM-dd');
        }

        var Order = this.inputForm.value.orderArr
        this.drawerVisible = true;        
        this.loading = true;
        var fQuery = "Select RecordNumber,CONVERT(varchar, [DetailDate],105) as [DetailDate],Detail, CONVERT(varchar, [AlarmDate],105) as [AlarmDate], Creator as Creator From History HI INNER JOIN Staff ST ON ST.[UniqueID] = HI.[PersonID] WHERE ST.[AccountNo] = '"+this.user.code+"' AND HI.DeletedRecord <> 1 AND (([PrivateFlag] = 0) OR ([PrivateFlag] = 1 AND [Creator] = 'sysmgr' )) AND ExtraDetail1 = 'OPNOTE' ";        
        fQuery = fQuery + " AND  (DetailDate between '"+strdate +"' AND '"+endate+"')"
       
        if(Order == "Chronological-Last To First"){
            fQuery = fQuery +" ORDER BY DetailDate DESC "
        }else{ 
            fQuery = fQuery + "ORDER by DetailDate ASC ";
        }
        

        var txtTitle = "OP Notes";       
        var Rptid =   "8WTerq14eNFSAnz0"  

        console.log(fQuery)
        const data = {                    
            "template": { "_id": Rptid },                                
            "options": {
                "reports": { "save": false },                
                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "txtTitle": txtTitle,
                "Extra": this.user.code,                                                                
            }
        }
        this.loading = true;
        this.drawerVisible = true;         
            this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = txtTitle+".pdf"
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();
            }, err => {
                console.log(err);
                this.loading = false;
                this.ModalS.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            return;  
      
    }





    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Details',
        value: 'detail'
    },{
        key: 'Category',
        value: 'category'
    },{
        key: 'Detail Date',
        value: 'detailDate'
    },{
        key: 'Creator',
        value: 'creator'
    }];
    
    
    

    dragDestination = [       
        'Details',
        'Category',
        'Detail Date',
        'Creator'
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        // console.log("staff "+ permissoons.length );
        return permissoons.charAt(index-1);
    }
}