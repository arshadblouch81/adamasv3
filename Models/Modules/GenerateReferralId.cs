using System;

namespace Adamas.Models.Modules
{
    public class GenerateReferralId
    {
        public string AccountNo { get; set; }
        public string Template { get; set;  }
        public int Type { get; set; }
    }
}