﻿using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;
using System.Collections.Generic;
using System.Text;
using System;
using Adamas.Models.Modules;
using AdamasV3.Models.Dto;

namespace AdamasV3.DAL.DocusignRepository
{
    public static class EmbeddedSending
    {
        /// <summary>
        /// Create an envelope and then create an embedded sending view for that envelope
        /// </summary>
        /// <param name="signerEmail">Email address for the signer</param>
        /// <param name="signerName">Full name of the signer</param>
        /// <param name="ccEmail">Email address for the cc recipient</param>
        /// <param name="ccName">Name of the cc recipient</param>
        /// <param name="accessToken">Access Token for API call (OAuth)</param>
        /// <param name="basePath">BasePath for API calls (URI)</param>
        /// <param name="accountId">The DocuSign Account ID (GUID or short version) for which the APIs call would be made</param>
        /// <param name="docPdf">String of bytes representing the document (pdf)</param>
        /// <param name="docDocx">String of bytes representing the Word document (docx)</param>
        /// <param name="startingView">The sending view to show initially (either "tagging" or "recipient")</param>
        /// <param name="returnUrl">URL user will be redirected to after they sign</param>
        /// <returns>URL to embed in your application</returns>
        public static DocusignDto SendEnvelopeUsingEmbeddedSending(List<CustomSigner> _signers, List<Document> _documents, string envStatus, string basePath, string accessToken, string accountId, string contentRootPath)
        {
            var docuSignClient = new DocuSignClient(basePath);
            docuSignClient.Configuration.DefaultHeader.Add("Authorization", "Bearer " + accessToken);
            EnvelopesApi envelopesApi = new EnvelopesApi(docuSignClient);

            // Step 1. Make the envelope with "created" (draft) status
            EnvelopeDefinition env = MakeEnvelope(_signers, _documents, envStatus, contentRootPath);
            EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, env);
            string envelopeId = results.EnvelopeId;

            // Step 2. create the sender view
            // Call the CreateSenderView API
            // Exceptions will be caught by the calling function
            ReturnUrlRequest viewRequest = new ReturnUrlRequest
            {
                ReturnUrl = "",
            };
            ViewUrl result1 = envelopesApi.CreateSenderView(accountId, envelopeId, viewRequest);

            // Switch to Recipient and Documents view if requested by the user
            string redirectUrl = result1.Url;
            if ("recipient".Equals("tagging"))
            {
                redirectUrl = redirectUrl.Replace("send=1", "send=0");
            }

            DocusignDto dsignDto = new DocusignDto() { 
                DocusignEmbeddedUrl = redirectUrl,
                EnvelopeId = envelopeId
            };

            return dsignDto;
        }

        private static EnvelopeDefinition MakeEnvelope(List<CustomSigner> _signers, List<Document> _documents, string envStatus, string contentRootPath)
        {
            //ds-snippet-start:eSign2Step2
            EnvelopeDefinition env = new EnvelopeDefinition();
            env.EmailSubject = "Please sign this document set";

            var signers = GetSigners(_signers);
            var documents = GetDocuments(_documents, contentRootPath);


            // The order in the docs array determines the order in the envelope
            env.Documents = documents;

            int signerCounter = 1;
            foreach (var signer in signers)
            {
                signer.Tabs = new Tabs
                {
                    SignHereTabs = new List<SignHere> {
                        new SignHere(){
                            AnchorString    = $@"\s{signerCounter}\",
                            Name            = signer.Name
                        }
                    },
                    InitialHereTabs = new List<InitialHere> {
                        new InitialHere(){
                            AnchorString    = $@"\i{signerCounter}\",
                        }
                    },
                    FullNameTabs = new List<FullName> {
                        new FullName(){
                            AnchorString    = $@"\n{signerCounter}\",
                            Name            = signer.Name
                        }
                    },
                    CompanyTabs = new List<Company> {
                        new Company(){
                            AnchorString    = $@"\co{signerCounter}\",
                            Name            = _signers[signerCounter - 1].Company,
                            Value           = _signers[signerCounter - 1].Company,
                            OriginalValue   = _signers[signerCounter - 1].Company,
                            Required        = "false"
                        }
                    },
                    TitleTabs = new List<Title> {
                        new Title(){
                            AnchorString    = $@"\t{signerCounter}\",
                            Name            = _signers[signerCounter - 1].Title,
                            Value           = _signers[signerCounter - 1].Title,
                            OriginalValue   = _signers[signerCounter - 1].Title,
                            Required        = "false"
                        }
                    },
                    DateSignedTabs = new List<DateSigned>{
                        new DateSigned(){
                            AnchorString    = $@"\d{signerCounter}\",
                            Name            = "",
                            Value           = "",
                            TemplateLocked  = "true"
                        }
                    }
                };
                signerCounter++;
            }

            // Add the recipients to the envelope object
            Recipients recipients = new Recipients
            {
                Signers = signers
            };
            env.Recipients = recipients;

            // Request that the envelope be sent by setting |status| to "sent".
            // To request that the envelope be created as a draft, set to "created"
            env.Status = envStatus;

            return env;
        }

        public static List<Signer> GetSigners(List<CustomSigner> _signers)
        {
            List<Signer> signers = new List<Signer>();
            foreach (var signer in _signers)
            {
                signers.Add(new Signer()
                {
                    Email = signer.Email,
                    Name = signer.Name,
                    RecipientId = signer.RecipientId.ToString(),
                    RoutingOrder = signer.RoutingOrder
                });
            }
            return signers;
        }

        public static List<Document> GetDocuments(List<Document> _documents, string contentRootPath)
        {
            List<Document> documents = new List<Document>();
            foreach (var document in _documents)
            {
                documents.Add(new Document()
                {
                    DocumentBase64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(System.IO.Path.Combine(contentRootPath, "document", document.DocumentBase64))),
                    Name = document.Name,
                    FileExtension = document.FileExtension,
                    DocumentId = document.DocumentId
                });
            }
            return documents;
        }

        private static byte[] Document1(string signerEmail, string signerName, string ccEmail, string ccName)
        {
            // Data for this method
            // signerEmail
            // signerName
            // ccEmail
            // ccName
            return Encoding.UTF8.GetBytes(
            " <!DOCTYPE html>\n" +
                "    <html>\n" +
                "        <head>\n" +
                "          <meta charset=\"UTF-8\">\n" +
                "        </head>\n" +
                "        <body style=\"font-family:sans-serif;margin-left:2em;\">\n" +
                "        <h1 style=\"font-family: 'Trebuchet MS', Helvetica, sans-serif;\n" +
                "            color: darkblue;margin-bottom: 0;\">World Wide Corp</h1>\n" +
                "        <h2 style=\"font-family: 'Trebuchet MS', Helvetica, sans-serif;\n" +
                "          margin-top: 0px;margin-bottom: 3.5em;font-size: 1em;\n" +
                "          color: darkblue;\">Order Processing Division</h2>\n" +
                "        <h4>Ordered by " + signerName + "</h4>\n" +
                "        <p style=\"margin-top:0em; margin-bottom:0em;\">Email: " + signerEmail + "</p>\n" +
                "        <p style=\"margin-top:0em; margin-bottom:0em;\">Copy to: " + ccName + ", " + ccEmail + "</p>\n" +
                "        <p style=\"margin-top:3em;\">\n" +
                "  Candy bonbon pastry jujubes lollipop wafer biscuit biscuit. Topping brownie sesame snaps sweet roll pie. Croissant danish biscuit soufflé caramels jujubes jelly. Dragée danish caramels lemon drops dragée. Gummi bears cupcake biscuit tiramisu sugar plum pastry. Dragée gummies applicake pudding liquorice. Donut jujubes oat cake jelly-o. Dessert bear claw chocolate cake gummies lollipop sugar plum ice cream gummies cheesecake.\n" +
                "        </p>\n" +
                "        <!-- Note the anchor tag for the signature field is in white. -->\n" +
                "        <h3 style=\"margin-top:3em;\">Agreed: <span style=\"color:white;\">**signature_1**/</span></h3>\n" +
                "        </body>\n" +
                "    </html>");
        }
    }
}
