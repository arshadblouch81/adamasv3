ALTER DATABASE ADAMAS_DEV
SET COMPATIBILITY_LEVEL = 150;

GO
/****** Object:  UserDefinedFunction [dbo].[fn_split]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE OR ALTER    FUNCTION [dbo].[fn_split](  
@delimited NVARCHAR(MAX),  
@delimiter NVARCHAR(100)  
) RETURNS @table TABLE (id INT IDENTITY(1,1), [value] NVARCHAR(MAX))  
AS  
BEGIN  
DECLARE @xml XML  
SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'  
INSERT INTO @table([value])  
SELECT r.value('.','Nvarchar(MAX)') as item  
FROM @xml.nodes('/t') as records(r)  
RETURN  
END 
GO
/****** Object:  UserDefinedFunction [dbo].[GetBillingRate]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE OR ALTER    FUNCTION [dbo].[GetBillingRate](@RecipientCode varchar(Max)                                , @ActivityCode varchar(Max)                                , @Program varchar(Max)                                ) RETURNS float  AS begin    declare @NewBillingRate varchar(Max);    declare @NewBillingUnit varchar(Max);    declare @NewDatasetType varchar(Max);    declare @NewDebtor varchar(Max);      Declare @BillingMethod varchar(Max);      Declare @Percentage varchar(Max) ;      declare @Amount float;      declare @Price2 float;      declare @Price3 float;      declare @Price4 float;      declare @Price5 float;      declare @Price6 float;      declare @Unit varchar(Max);      declare @HACCType varchar(Max);      declare @PersonID varchar(Max); select @PersonID=UniqueID from Recipients where AccountNo=@RecipientCode; select @Amount=Amount, @Price2=Price2, @Price3=Price3, @Price4=Price4, @Price5=Price5, @Price6=Price6, @Unit=Unit, @HACCType=HACCType  from ItemTypes where ProcessClassification IN ('OUTPUT', 'EVENT') And Title =  @ActivityCode; If (@amount is not null) begin        If exists(select ForceSpecialPrice  from ServiceOverview where ForceSpecialPrice=1 and PersonID = @PersonID AND  ServiceProgram = @Program AND [Service Type] = @ActivityCode )        begin             set @BillingMethod = 'SPECIALPRICE';            set @NewDatasetType=@HACCType;             select  @NewBillingUnit=UnitType, @NewBillingRate=[Unit Bill Rate], @NewDebtor=ServiceBiller             from ServiceOverview             where PersonID = @PersonID AND ServiceProgram =@Program AND [Service Type] = @ActivityCode;                if (@NewBillingRate is null)        begin            set @NewBillingUnit=@Unit;            set @NewBillingRate=@amount;                end       end         Else           begin            select @BillingMethod=BillingMethod, @Percentage=isnull(PercentageRate,100), @NewDebtor=BillTo             from Recipients where AccountNo =@RecipientCode ;              If (@Amount is not null)             begin               select  @NewBillingRate=Case @BillingMethod                               when 'FIXED'        then  0                 when 'PERCENTAGE'   then  round(((@Amount*@Percentage) / 100), 0)                 when 'COMMERCIAL'   then  @Amount                 when 'LEVEL1'      then  @price2                when 'LEVEL2'       then  @price3                when 'LEVEL3'       then  @price4                when 'LEVEL4'       then  @price5                when 'LEVEL5'       then  @price6                else @Amount  end;                                                            end             Else            begin                 set @NewBillingRate = 0;             End              set @NewBillingUnit = @unit;             set @NewDatasetType = @HACCType;         End  end Else     begin                  if( @ActivityCode='UNAVAILABLE' )        begin             set @NewBillingRate=0;            set @NewBillingUnit = 'HOUR';            set @NewDatasetType = 'UNAVAILABLE';        end         else if ( @ActivityCode='CONTRIBUTION AS AGREED' OR @ActivityCode='CONTRIBUTION' )        begin   set  @NewBillingRate=0; end         else begin set @NewBillingRate=null;  End       End         return @NewBillingRate;  End 
GO
/****** Object:  UserDefinedFunction [dbo].[getBillTo]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER   FUNCTION  [dbo].[getBillTo](@ACCOUNTNO varchar(100),@ServiceType varchar(500))

		RETURNS varchar(100)
		AS
		BEGIN
			-- Declare the return variable here
			DECLARE @ResultVar varchar(100)

			SELECT @ResultVar=ISNULL(S.ServiceBiller,C.BillTo)
			FROM RECIPIENTS C LEFT JOIN ServiceOverview S ON C.UNIQUEID = S.PersonID
			AND [Service Type] = @ServiceType AND S.ForceSpecialPrice = 1
			WHERE ACCOUNTNO = @ACCOUNTNO;
	
			-- Return the result of the function
			RETURN @ResultVar;

		END
GO
/****** Object:  UserDefinedFunction [dbo].[GetBlockNo]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[GetBlockNo](@s_Time varchar(5)) returns  float
as
begin
	Declare  @Part1 float
    Declare  @Part2 float
    Declare  @Part3 float
    Declare  @Part4 float
	DEclare @val float;
    
    set @Part1 = Convert(int,replace(Left(@s_Time, 2),':',''));
    set @Part2 = Convert(int,replace(Right(@s_Time, 2),':',''));
    set @Part3 = @Part1 * 12
    set @Part4 = @Part2 / 5
    set @val = @Part3 + @Part4
   return @val;
end
GO
/****** Object:  UserDefinedFunction [dbo].[getCoordinator_Email]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER function [dbo].[getCoordinator_Email](@AccountNo varchar(50), @Person_Type varchar(10) )   returns varchar(150)  
	As begin 
		Declare @COORDINATOR varchar(50) Declare @Manager varchar(50) Declare @Email varchar(150) if (@Person_Type='RECIPIENT') BEGIN 
		select @COORDINATOR=RECIPIENT_COORDINATOR from Recipients where  AccountNo=@AccountNo; END ELSE BEGIN select @COORDINATOR=PAN_Manager from Staff where  AccountNo=@AccountNo; END  
		select @Manager=HACCCode from DataDomains where Description =@COORDINATOR AND DOMAIN = 'CASE MANAGERS'; 
		select @Email=Detail from PhoneFaxOther ph, Staff stf  where ph.PersonID=stf.UniqueID and stf.AccountNo=@Manager and [TYPE]='<EMAIL>'  ; 
		return @Email ;
	End
GO
/****** Object:  UserDefinedFunction [dbo].[getDistance]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER function [dbo].[getDistance] (@Lat1 decimal(8,4), @Long1 decimal(8,4), @Lat2 decimal(8,4), @Long2 decimal(8,4)) returns decimal (8,4) as begin declare @d decimal(28,10) set @Lat1 = @Lat1 / 57.2958 set @Long1 = @Long1 / 57.2958 set @Lat2 = @Lat2 / 57.2958 set @Long2 = @Long2 / 57.2958 set @d = (Sin(@Lat1) * Sin(@Lat2)) + (Cos(@Lat1) * Cos(@Lat2) * Cos(@Long2 - @Long1)) if @d <> 0 begin set @d = 3958.75 * Atan(Sqrt(1 - power(@d, 2)) / @d); end set @d =  1.60934 * @d return @d end 
GO
/****** Object:  UserDefinedFunction [dbo].[getPayPeriodWorkedHours]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER    FUNCTION  [dbo].[getPayPeriodWorkedHours](@ACCOUNTNO varchar(100))

		RETURNS varchar(10)
		AS
		BEGIN
		
		
		  Declare @Total_duration int;
		  Declare @PayPeriodEndDate varchar(10);
		  Declare @PayPeriodLength int
		  Declare @WorkingPayStart varchar(10);
		 Declare @ret_value varchar(10);

		  select @PayPeriodEndDate=CONVERT(varchar,PayPeriodEndDate,111) from SysTable;
          select @PayPeriodLength=DefaultPayPeriod from Registration;
          set @WorkingPayStart = convert(varchar, DateAdd(Day, -1 * (@PayPeriodLength - 1), @PayPeriodEndDate),111);
	
		

		select @Total_duration=sum(duration) 
		from roster  inner join ItemTypes on Roster.[Service Type] = ItemTypes.Title    
		where  [Carer Code]=@ACCOUNTNO and [type] not in ( 13,14,15) and isnull(InfoOnly,0)=0
		and [date] >=CONVERT(varchar,@WorkingPayStart,111) 
		and [date] <=CONVERT(varchar,@PayPeriodEndDate,111);
		
		set @ret_value= FORMAT( (@Total_duration/12), '00') + ':' + format( (@Total_duration%12)*5,'00');

		return @ret_value;
		
END
GO
/****** Object:  UserDefinedFunction [dbo].[getPrimaryAddress]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[getPrimaryAddress] (@AccountNo varchar(50))  returns varchar(500)
			AS BEGIN   

			Declare @address varchar(500);
			SELECT     @address= Case when isnull(googleaddress,'')='' then n.Address1  + 
									  Case WHEN n.Address2 <> '' THEN ', ' + n.Address2  ELSE '' END + 
									  Case WHEN n.Suburb <> '' THEN ',' + n.Suburb  ELSE ' ' END +  
									  Case WHEN n.Postcode <> '' THEN ' ' + n.Postcode ELSE ' ' END 
									  Else GoogleAddress END
								FROM Recipients r left join NamesAndAddresses n on n.personid=r.UniqueId  
								left join  PhoneFaxOther p on p.personid=r.UniqueId    
								WHERE r.accountNo = @AccountNo and n.PrimaryAddress=1;
	
			return @address;
                    
END
GO
/****** Object:  UserDefinedFunction [dbo].[getRosterType]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[getRosterType](@ServiceType varchar(500)) returns  int
as
begin
	Declare @type int;
	Declare @RosterGroup varchar(500);
	select @RosterGroup=RosterGroup from ItemTypes where Title=@ServiceType;

	set @type = Case When @RosterGroup='BOOKED' then 1 	--Un-allocated	Icon = Question mark
	When @RosterGroup='ONEONONE' then 2 	--Direct care 	Icon = Heart
	When @RosterGroup='BROKERED' then 3	--Brokered Service 	Icon = No any icon
	When @RosterGroup='RECPTABSENCE' then 4	--Client Absent 	Icon = Calendar with disabled symbol
	When @RosterGroup='TRAVELTIME' then 5	--Travel		Icon = Car
	When @RosterGroup like '%ADMINISTRATION%' and @ServiceType='UNAVAILABLE' then 13	--Staff Administration    Icon = Edit Text icon
	When @RosterGroup like '%ADMINISTRATION%'  then 6	--Staff Administration    Icon = Edit Text icon
	When @RosterGroup='ADMISSION ' then 7	--Recipient Administration @type  = 7  	
	When @RosterGroup='SLEEPOVER' then 8	--Sleepover 	Icon =  Heart with double ZZ
	When @RosterGroup='ALLOWANCE' then 9	--Allowance		Icon = Will not show in roster
	When @RosterGroup='TRANSPORT' then 10	--Transport 		Icon = Bus
	When @RosterGroup='CENTREBASED' then 11	--Centerbased	Icon = House
	When @RosterGroup='GROUPACTIVITY' then 12 --Group Activity	Icon = Double staff icon
	When @RosterGroup='UNAVAILABLE' or @ServiceType='UNAVAILABLE' then 13 --Un-Availability	Icon = Grey disabled icon
	When @RosterGroup='ITEM' then 14 --Item Supplied 	Icon = Circled calendar	to Client 
	ELSE 1
	End
   return @type;
end
GO
/****** Object:  UserDefinedFunction [dbo].[GetStateFromPostcode]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[GetStateFromPostcode](@s_Postcode varchar(100)) returns varchar(100)
begin
Declare @state varchar(100);

  set @state= Case 
    When Left(@s_Postcode, 1)='0' then
         'NT'
    When Left(@s_Postcode, 1)='2' then
        
            Case when  
            convert(int,@s_Postcode)>= 2600 and convert(int,@s_Postcode)<= 2618 then 'ACT'
            When convert(int,@s_Postcode)>= 2900 and convert(int,@s_Postcode)<= 2999 then 'ACT'
            Else  'NSW'
            End 
     When Left(@s_Postcode, 1)='3' then
       'VIC'
    When Left(@s_Postcode, 1)='4' then
        'QLD'
    When Left(@s_Postcode, 1)='5' then 
        'SA'
    When Left(@s_Postcode, 1)='6' then 
       'WA'
    When Left(@s_Postcode, 1)='7' then 
        'TAS'
    End 
  return @state;
End
GO
/****** Object:  UserDefinedFunction [dbo].[getWeekNo]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION  [dbo].[getWeekNo](@DATE varchar(10))

RETURNS int
AS
BEGIN
   DECLARE @TESTDATE varchar (10);
DECLARE @iDay int;
DECLARE @iTestNumber int;
DECLARE @WeekNo int;
IF @DATE < '1950/01/01' 
BEGIN
   SET
      @iDay = RIGHT(@Date, 2) 
      SET
         @WeekNo = 
         CASE
            WHEN
               @iDay BETWEEN 1 and 7 
            THEN
               1 
            WHEN
               @iDay BETWEEN 8 and 14 
            THEN
               2 
            WHEN
               @iDay BETWEEN 15 and 21 
            THEN
               3 
            WHEN
               @iDay BETWEEN 22 and 28 
            THEN
               4 
            ELSE
               1 
         END
END
ELSE
   BEGIN
      SET
         @TESTDATE = 
         (
            SELECT
				TOP 1 CONVERT(Varchar, PayPeriodEndDate, 111) AS TestDate
            FROM
               Systable
         )
         SET
            @iTestNumber = 
            (
               Abs(DATEDIFF("d", DATEADD("d", - 14, @TESTDATE), @DATE) % 14)
            )
            SET
               @WeekNo = 
               CASE
                  WHEN
                     @iTestNumber BETWEEN 1 AND 7 
                  THEN
                     1 
                  ELSE
                     2 
               END
   END
   RETURN @WeekNo

END
GO
/****** Object:  UserDefinedFunction [dbo].[IsPublicHoliday1]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[IsPublicHoliday1](@sDate varchar(20), @sStaffCode varchar(100)) returns bit
begin
	Declare @returnValue bit;
	Declare @s_PubHolState varchar(100)
    Declare @s_PubHolRegion varchar(100)
    Declare @s_PubHolFilter varchar(100)
    Declare @Postcode  varchar(100)
	Declare @dateVal varchar(100)
	Declare @s_FilterMask varchar(2);

	set @dateVal=null;
     If (@sStaffCode <> '' )
     begin
		SELECT @s_PubHolRegion=st.PublicHolidayRegion, @Postcode=sta.Postcode FROM Staff st INNER JOIN NamesAndAddresses sta ON st.Uniqueid = sta.personid WHERE (ISNULL(sta.primaryAddress, 0) = 1 or sta.Type = '<USUAL>') AND st.AccountNo = @sStaffCode;
		
		set @s_PubHolState= dbo.GetStateFromPostcode(@Postcode);
              
        
       -- Set @s_PubHolFilter = [DATE]=@s_Date;
        Set @s_FilterMask = IIf((@s_PubHolState <> ''), 1, 0) & IIf((@s_PubHolRegion <> ''), 1, 0)
           
         if (@s_FilterMask= '01') 
			begin
				select @dateVal=[DATE] from PUBLIC_HOLIDAYS where   [DATE]=@sDate   AND ( (ISNULL(Stats, '') IN ('', 'ALL')) OR (PublicHolidayRegion = @s_PubHolRegion ));
			End
		 else if (@s_FilterMask= '10') 
			Begin 
				select @dateVal=[DATE] from PUBLIC_HOLIDAYS where   [DATE]=@sDate   AND ( (ISNULL(Stats, '') IN ('', 'ALL')) OR (Stats = @s_PubHolState))
			End
          if (@s_FilterMask= '11') 
			Begin
				select @dateVal=[DATE] from PUBLIC_HOLIDAYS where   [DATE]=@sDate  AND ( (ISNULL(Stats, '') IN ('', 'ALL')) OR ((Stats = @s_PubHolState) OR ( (Stats = @s_PubHolState) AND (PublicHolidayRegion =@s_PubHolRegion))))
            End
           

		   End
		   if (@dateVal is not null )
			 set @returnValue=1;
		   else
			 set @returnValue=0;
    
	return @returnValue;
end
GO
/****** Object:  UserDefinedFunction [dbo].[RTF2Text]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[RTF2Text](@rtf Varchar(8000))  RETURNS Varchar(8000)  AS  

BEGIN DECLARE @Pos1 INT, @Pos2 INT  DECLARE @Stage TABLE  (  Chr CHAR(1), Pos INT )  INSERT @Stage  (  Chr, Pos) 
SELECT SUBSTRING(@rtf, Number, 1), Number  FROM master..spt_values  WHERE Type = 'p'  
AND SUBSTRING(@rtf, Number, 1) IN ('{', '}')   
SELECT @Pos1 = MIN(Pos),  @Pos2 = MAX(Pos)  FROM @Stage  
DELETE FROM @Stage  WHERE Pos IN (@Pos1, @Pos2)  
WHILE 1 = 1  
BEGIN  
SELECT TOP 1 @Pos1 = s1.Pos, @Pos2 = s2.Pos  
FROM @Stage AS s1  INNER JOIN @Stage AS s2 ON s2.Pos > s1.Pos  
WHERE s1.Chr = '{'  AND s2.Chr = '}'  
ORDER BY s2.Pos - s1.Pos  
IF @@ROWCOUNT = 0  BREAK  
DELETE  FROM  @Stage  WHERE Pos IN (@Pos1, @Pos2)  
UPDATE @Stage  SET Pos = Pos - @Pos2 + @Pos1 - 1  
WHERE Pos > @Pos2  SET @rtf = STUFF(@rtf, @Pos1, @Pos2 - @Pos1 + 1, '')  
END  
SET @Pos1 = PATINDEX('%\cf[0123456789][0123456789 ]%', @rtf)  WHILE @Pos1 > 0  
SELECT @Pos2 = CHARINDEX(' ', @rtf, @Pos1 + 1), @rtf = STUFF(@rtf, @Pos1, @Pos2 - @Pos1 + 1, ''), 
@Pos1 = PATINDEX('%\cf[0123456789][0123456789 ]%', @rtf) SELECT @rtf = REPLACE(@rtf, '\pard', ''), @rtf = REPLACE(@rtf, '\par }', '')  
SELECT @rtf = REPLACE(@rtf, '\par', ''), @rtf = case when LEN(@rtf)>0 then LEFT(@rtf, LEN(@rtf) - 1) else @rtf end  
SELECT @rtf = REPLACE(@rtf, '\b0', ''), @rtf = REPLACE(@rtf, '\b ', '')  
SELECT @rtf = REPLACE(@rtf, '\cf0', ''), @rtf = REPLACE(@rtf, '\lang1033', '')  
SELECT @rtf = REPLACE(@rtf, '\lang3081', '')
SELECT @rtf = REPLACE(@rtf, '\i0', ''), @rtf = REPLACE(@rtf, '\f1', '')  
SELECT @rtf = REPLACE(@rtf, '\fs16', ''), @rtf = REPLACE(@rtf, '\fs18', '')  
SELECT @rtf = STUFF(@rtf, 1, CHARINDEX(' ', @rtf), '')  
RETURN @rtf  
END
GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER FUNCTION [dbo].[Split](@InputString VARCHAR(8000),
                              @Delimiter   VARCHAR(50))
returns @Items TABLE (
  item VARCHAR(8000))
AS
  BEGIN
      IF @Delimiter = ' '
        BEGIN
            SET @Delimiter = ','
            SET @InputString = Replace(@InputString, ' ', @Delimiter)
        END

      IF ( @Delimiter IS NULL
            OR @Delimiter = '' )
        SET @Delimiter = ','

      DECLARE @Item VARCHAR(8000)
      DECLARE @ItemList VARCHAR(8000)
      DECLARE @DelimIndex INT

      SET @ItemList = @InputString
      SET @DelimIndex = Charindex(@Delimiter, @ItemList, 0)

      WHILE ( @DelimIndex != 0 )
        BEGIN
            SET @Item = Substring(@ItemList, 0, @DelimIndex)

            INSERT INTO @Items
            VALUES      (@Item)

            SET @ItemList = Substring(@ItemList, @DelimIndex + 1,
                            Len(@ItemList) - @DelimIndex)
            SET @DelimIndex = Charindex(@Delimiter, @ItemList, 0)
        END

      IF @Item IS NOT NULL
        BEGIN
            SET @Item = @ItemList

            INSERT INTO @Items
            VALUES      (@Item)
        END
      ELSE
        INSERT INTO @Items
        VALUES      (@InputString)

      RETURN
  END 
GO
/****** Object:  UserDefinedFunction [dbo].[ThereAreConflicts]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
CREATE OR ALTER    FUNCTION [dbo].[ThereAreConflicts] 
(
	@CompareStaffCode Varchar(100), @CompareYearNo int, @CompareMonthNo int, @CompareDayNo int, @CompareStartBlock int, @CompareEndBlock int, @StartEndTolerance Varchar(100), @ConflictRecipient Varchar(100), @rsConflictstart Varchar(100), @ConflictDuration Varchar(100),  @b_DirectConflict bit=0, @b_InDirectConflict bit=0, @s_ConflictActivity Varchar(100)='', @s_Status Varchar(100)='', @s_ExcludeRecords Varchar(100)='', @s_ConflictSetting Varchar(100)='', @s_ConflictRecord Varchar(100)=''
)
RETURNS  @output_Table Table(
	RecordNo int ,
	StartTime varchar(10),
	Blockno int,
	Duration int,
	ClientCode varchar(100),
	ServiceType varchar(500),
	ServiceSetting varchar(500),
	bDirect bit,
	bIndirect bit,
	ConflictExist bit
)
AS
BEGIN
	-- Declare the return variable here
	Declare @return_val bit;
	--Declare @ConflictRecipient varchar(100);
 --   Declare @rsConflictstart varchar(100);
 --   Declare @ConflictDuration  varchar(100);
 --   b_DirectConflict = False
 --   b_InDirectConflict = False
	--set @ConflictRecipient='';
	--set @rsConflictstart='';
	--set @ConflictDuration=''
	Declare @RecordNo int ;
	Declare @StartTime varchar(10);
	Declare @Blockno int
	Declare @Duration int;
	Declare @ClientCode varchar(100);
	Declare @ServiceType varchar(500);
	Declare @ServiceSetting varchar(500);

	Declare @date varchar(20);
	set @date=Convert(varchar,@CompareYearNo) + '/' + iif(@CompareMonthNo<10,'0'+Convert(varchar,@CompareMonthNo),Convert(varchar,@CompareMonthNo)) +  '/' + iif(@CompareDayNo<10, '0'+ convert(varchar,@CompareDayNo),convert(varchar,@CompareDayNo));
	If (@s_ExcludeRecords <> '' )
	    
		 SELECT   @RecordNo=[RecordNo], @StartTime=[Start Time], @Blockno=Blockno, @Duration=[Duration], @ClientCode=[Client Code], @ServiceType=[Service Type], @ServiceSetting=ServiceSetting 
                    FROM roster INNER JOIN ItemTypes ON [roster].[service type] = ItemTypes.Title 
					WHERE 
                    [Date] =    convert(varchar,@date,111) AND 
                    [Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13) AND 
                    [Carer Code] = @CompareStaffCode   AND 
                    (ISNULL([MinorGroup], '') <> 'GAP' AND ISNULL(excludeFromConflicts, 0) = 0) 
                   AND RecordNo NOT IN (@s_ExcludeRecords  )    
                   AND 
    
					(
                            (
                               Blockno BETWEEN 
                                (@CompareStartBlock - (Convert(int,@StartEndTolerance) / 5))
                               AND 
                                (@CompareEndBlock + (Convert(int,@StartEndTolerance) / 5))
                            )
                            OR 
                            (
                               Blockno + Duration BETWEEN 
                               (@CompareStartBlock - (Convert(int,@StartEndTolerance) / 5) + 1  )
                               AND 
                                (@CompareEndBlock + convert(int,@StartEndTolerance / 5)  )
                            )
                            OR 
                            (
                               Blockno <=   (@CompareStartBlock - (convert(int,@StartEndTolerance) / 5)) 
                               AND 
                               BlockNo + Duration >   (@CompareEndBlock + convert(int,@StartEndTolerance) / 5)  
                            )
                        );
		
		Else
			
		 SELECT   @RecordNo=[RecordNo], @StartTime=[Start Time], @Blockno=Blockno, @Duration=[Duration], @ClientCode=[Client Code], @ServiceType=[Service Type], @ServiceSetting=ServiceSetting 
                    FROM roster INNER JOIN ItemTypes ON [roster].[service type] = ItemTypes.Title 
					WHERE 
                    [Date] =    convert(varchar,@date,111) AND 
                    [Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13) AND 
                    [Carer Code] = @CompareStaffCode   AND 
                    (ISNULL([MinorGroup], '') <> 'GAP' AND ISNULL(excludeFromConflicts, 0) = 0)                    
                   AND 
    
					(
                            (
                               Blockno BETWEEN 
                                (@CompareStartBlock - (Convert(int,@StartEndTolerance) / 5))
                               AND 
                                (@CompareEndBlock + (Convert(int,@StartEndTolerance) / 5))
                            )
                            OR 
                            (
                               Blockno + Duration BETWEEN 
                               (@CompareStartBlock - (Convert(int,@StartEndTolerance) / 5) + 1  )
                               AND 
                                (@CompareEndBlock + convert(int,@StartEndTolerance) / 5)  
                            )
                            OR 
                            (
                               Blockno <=   (@CompareStartBlock - (convert(int,@StartEndTolerance) / 5)) 
                               AND 
                               BlockNo + Duration >   (@CompareEndBlock + convert(int,@StartEndTolerance) / 5)  
                            )
                        );

		
        
            
        If (@recordNo is not null) 
		Begin
            If (Not (@Blockno = @CompareStartBlock And @Blockno = @CompareEndBlock) )
			Begin
                set @b_InDirectConflict = 1;
                set @return_val = 1;
                
                If ((@Blockno >= @CompareStartBlock And @Blockno <= @CompareEndBlock) 
                   Or 
                   (@Blockno + @Duration >= @CompareStartBlock And @Blockno + @Duration <= @CompareEndBlock) 
                   Or 
                   (@Blockno < @CompareStartBlock And @Blockno + @Duration > @CompareEndBlock)
				   )
				   set @b_DirectConflict =1;

                set @ConflictRecipient = @ClientCode;
                set @rsConflictstart = @StartTime;
                set @ConflictDuration = convert(varchar,@Duration * 5);
                set @s_ConflictActivity = @ServiceType;
                set @s_ConflictSetting = @ServiceSetting;
                set @s_ConflictRecord = @RecordNo;;
                
            End
		    Else
			Begin
                set @b_DirectConflict = 0;
                set @b_InDirectConflict =0;
                set @ConflictRecipient = '';
                set @rsConflictstart = '';
                set @ConflictDuration = '';
            End 
        End
		Else
		Begin
            set @b_DirectConflict = 0;
            set @b_InDirectConflict = 0;
            set @ConflictRecipient = '';
            set @rsConflictstart = '';
            set @ConflictDuration = '';
        End 
  
  insert into @output_Table(
	RecordNo  ,
	StartTime ,
	Blockno ,
	Duration ,
	ClientCode ,
	ServiceType ,
	ServiceSetting ,
	bDirect,
	bInDirect,
	ConflictExist 
)  
values (@RecordNo  ,
	@StartTime ,
	@Blockno ,
	@Duration ,
	@ClientCode ,
	@ServiceType ,
	@ServiceSetting ,
	@b_DirectConflict,
	@b_InDirectConflict,
	@return_val);

	return


END
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_splitstring]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    FUNCTION [dbo].[ufn_splitstring] (@stringToSplit VARCHAR(MAX),@SpliterChar CHAR(1))
RETURNS
	@returnList TABLE (Val [nvarchar] (500))
AS
BEGIN

 DECLARE @name NVARCHAR(255)
 DECLARE @pos INT

 WHILE CHARINDEX(@SpliterChar, @stringToSplit) > 0
 BEGIN
  SELECT @pos  = CHARINDEX(',', @stringToSplit)  
  SELECT @name = SUBSTRING(@stringToSplit, 1, @pos-1)

  INSERT @returnList 
  SELECT @name

  SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos+1, LEN(@stringToSplit)-@pos)
 END

 INSERT @returnList
 SELECT @stringToSplit

 RETURN
END
GO

/****** Object:  StoredProcedure [dbo].[AutoCreateBookings]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[AutoCreateBookings] 
 (
@User AS varchar(50),
@DocRecNo integer,
@FeeType varchar (6),
@RosterType varchar  (6),
@Cycle varchar (10),
@StartDate varchar (10),
@EndDate varchar (10),
@RosterPeriod as int = 14
 )

 AS 
BEGIN       

--DECLARE @User AS varchar(50) = 'sysmgr'
--DECLARE @DocRecNo int = 45848
--DECLARE @QuoteHdrCPID int = 45848
--DECLARE @QuoteHdrRecordNumber int = 2310
--DECLARE @QuoteLneDocHdrId int = 2310
--DECLARE @QuoteNumber int = 317
--DECLARE @FeeType varchar (6) = 'FEE'
--DECLARE @RosterType varchar  (6) = 'MASTER'
--DECLARE @Cycle varchar (10) = 'CYCLE 1'
--DECLARE @StartDate varchar (10) = '1900/01/01'
--DECLARE @EndDate varchar (10) = '1900/14/01'
--DECLARE @RosterPeriod as int = 14

SELECT 'START AutoCreateBookings'

SET DATEFIRST 1

IF @RosterType = 'MASTER' BEGIN

	SET @StartDate = 
	CASE 
		WHEN @Cycle = 'CYCLE 1' THEN '1900/01/01'
		WHEN @Cycle = 'CYCLE 2' THEN '1900/10/01'
		WHEN @Cycle = 'CYCLE 3' THEN '1901/04/01'
		WHEN @Cycle = 'CYCLE 4' THEN '1901/07/01'
		WHEN @Cycle = 'CYCLE 5' THEN '1902/09/01'
		WHEN @Cycle = 'CYCLE 6' THEN '1902/12/01'
		WHEN @Cycle = 'CYCLE 7' THEN '1903/06/01'
		WHEN @Cycle = 'CYCLE 8' THEN '1904/02/01'
		WHEN @Cycle = 'CYCLE 9' THEN '1904/08/01'
		WHEN @Cycle = 'CYCLE 10' THEN '1905/05/01'
	END

	SET @EndDate = CONVERT(VARCHAR, DATEADD(day, @RosterPeriod - 1, CONVERT(Date, @StartDate)), 111)

END
	
SELECT @StartDate as StartDate, @EndDate AS EndDate

DECLARE @Query nvarchar (max)
DECLARE @qh_Date						DateTime;
DECLARE @qh_DocNo						varchar(15);
DECLARE @qh_ClientID					int;
DECLARE @qh_ProgramID					int;
DECLARE @qh_Narration					varchar(4000);
DECLARE @qh_Amount						money;
DECLARE @qh_Contingency					varchar(10);
DECLARE @qh_Basis						varchar(255);
DECLARE @qh_Contribution				varchar(10);
DECLARE @qh_ContingencyBuildAmount		varchar(20);
DECLARE @qh_AdminAmount_Perc			varchar(20);
DECLARE @qh_DaysCalc					float;
DECLARE @qh_Budget						float;
DECLARE @qh_QuoteBase					varchar(25)
DECLARE @qh_IncomeTestedFee				varchar(15);
DECLARE @qh_GovtContribution			varchar(15);
DECLARE @qh_PackageSupplements			varchar(25);
DECLARE @qh_DailyCDCRate				varchar(10);
DECLARE @qh_AgreedTopUp					varchar(15);
DECLARE @qh_BalanceAtQuote				varchar(15);
DECLARE @qh_CLAssessedIncomeTestedFee	varchar(15);
DECLARE @qh_FeesAccepted				bit;
DECLARE @qh_BasePension					varchar(30);
DECLARE @qh_DailyBasicCareFee			varchar(15);
DECLARE @qh_DailyIncomeTestedFee		varchar(15);
DECLARE @qh_DailyAgreedTopUp			varchar(15);
DECLARE @qh_HardshipSupplement			varchar(10);
DECLARE @qh_QuoteView					varchar(10);
DECLARE @ql_RecordNumber				int;
DECLARE @ql_Doc_Hdr_ID					int;
DECLARE @ql_ItemID						int;
DECLARE @ql_Qty							float;
DECLARE @ql_BillUnit					varchar(10)
DECLARE @ql_UnitBillRate				money;
DECLARE @ql_LineBillExTax				money;
DECLARE @ql_Tax							money;
DECLARE @ql_LineBillIncTax				money;
DECLARE @ql_DisplayText					varchar(500);
DECLARE @ql_BudgetEnforcement			varchar(4);
DECLARE @ql_BudgetGroup					varchar(50);
DECLARE @ql_Notes						varchar(4000);
DECLARE @ql_Line						int;
DECLARE @ql_Frequency					varchar(15);
DECLARE @ql_QuoteQty					numeric(16,2);
DECLARE @ql_PriceType					varchar(2);
DECLARE @ql_QuotePerc					varchar(6);
DECLARE @ql_BudgetPerc					varchar(6);
DECLARE @ql_Day							varchar(10);
DECLARE @ql_DTime						varchar(5);
DECLARE @ql_Week						varchar(10);
DECLARE @ql_RCycle						varchar(15);
DECLARE @ql_LengthInWeeks				int;
DECLARE @ql_RosterRecurrence			int;
DECLARE @ql_StrategyID					int;
DECLARE @ql_SortOrder					int;
DECLARE @it_Title						varchar(50);
DECLARE @it_AutoApprove					bit;
DECLARE @it_RosterGroup					varchar(25);
DECLARE @it_minorgroup					varchar(25);
DECLARE @re_accountno					varchar(50);
DECLARE @re_AgencyDefinedGroup			varchar(25);
DECLARE @pr_Name						varchar(50);
DECLARE @TableID AS nvarchar (12) = CONVERT(VARCHAR, @DocRecNo)
DECLARE @MyCursor CURSOR;
DECLARE @Cursor VARCHAR;
DECLARE @RosterData nvarchar(max);
DECLARE @CursorQuery varchar (max);
DECLARE @ClientCode						varchar(50);
DECLARE @AgencyDefinedGroup				varchar(50);
DECLARE @Program						varchar(50);
DECLARE @ServiceType					varchar(50);
DECLARE @Type							int;
DECLARE @DatasetType					varchar(255);
DECLARE @StaffCode						varchar(50);
DECLARE @Date as varchar (10);
DECLARE @StartTime as varchar (5);
DECLARE @DurationMinutes int;
DECLARE @RosterDuration int;
DECLARE @BillQty int;
DECLARE @RosterBlock int;
DECLARE @DurationTest varchar(5);
DECLARE @DAYCOUNTER INT;
DECLARE @OFFSETCOUNTER INT;
DECLARE @MasterYrMnth as varchar (8);
DECLARE @MaxCount int
DECLARE @DateToCheck varchar(10);
DECLARE @DateWeek int;
DECLARE @DayName varchar(10);
DECLARE @DayNumber int;
DECLARE @Offset int;

--DEBUG
SELECT 'QRoster' + @TableID AS T1, @TableID AS t2

IF OBJECT_ID('QRoster' + @TableID, 'U') IS NOT NULL BEGIN
	EXEC('drop table ' +  'QRoster' + @TableID)
END

	SET @Query = 
	'SELECT convert(varchar (365), ql.roster) AS RosterData, 
	qh.[Date]
	,qh.[Doc#]
	,qh.[ClientID]
	,qh.[ProgramID]
	,qh.[Narration]
	,qh.[Amount]
	,qh.[Contingency]
	,qh.[Basis]
	,qh.[Contribution]
	,qh.[ContingencyBuildAmount]
	,qh.[AdminAmount_Perc]
	,qh.[DaysCalc]
	,qh.[Budget]
	,qh.[QuoteBase]
	,qh.[IncomeTestedFee]
	,qh.[GovtContribution]
	,qh.[PackageSupplements]
	,qh.[DailyCDCRate]
	,qh.[AgreedTopUp]
	,qh.[BalanceAtQuote]
	,qh.[CLAssessedIncomeTestedFee]
	,qh.[FeesAccepted]
	,qh.[BasePension]
	,qh.[DailyBasicCareFee]
	,qh.[DailyIncomeTestedFee]
	,qh.[DailyAgreedTopUp]
	,qh.[HardshipSupplement]
	,qh.[QuoteView]
	,ql.[RecordNumber]
	,ql.[Doc_Hdr_ID]
	,ql.[ItemID]
	,ql.[Qty]
	,ql.[Bill Unit]
	,ql.[Unit Bill Rate]
	,ql.[Line Bill Ex Tax]
	,ql.[Tax]
	,ql.[Line Bill Inc Tax]
	,ql.[DisplayText]
	,ql.[BudgetEnforcement]
	,ql.[BudgetGroup]
	,ql.[Notes]
	,ql.[Line#]
	,ql.[Frequency]
	,ql.[QuoteQty]
	,ql.[PriceType]
	,ql.[QuotePerc]
	,ql.[BudgetPerc]
	,ql.[Day]
	,ql.[DTime]
	,ql.[Week]
	,ql.[RCycle]
	,ql.[LengthInWeeks]
	,ql.[RosterRecurrence]
	,ql.[StrategyID]
	,ql.[SortOrder]		   
	,it.Title, 
	ISNULL(it.AutoApprove, 0) AS AutoApprove, 
	it.RosterGroup, 
	it.minorgroup, 
	re.accountno AS Recipient, 
	re.AgencyDefinedGroup AS Category, 
	pr.[Name] AS Program 
			
	INTO ' + 'QRoster' + @TableID + ' FROM
			Qte_Lne ql 
			INNER JOIN Qte_Hdr qh ON ql.Doc_Hdr_Id = qh.recordnumber 
			INNER JOIN Recipients re ON qh.ClientID = re.SQLID 
			INNER JOIN HumanResourceTypes pr ON qh.ProgramID = pr.RecordNumber 
			INNER JOIN ItemTypes it ON ql.ItemId = it.Recnum 
			WHERE CPID = ' + CONVERT(VARCHAR(12), @DocRecNo) + ' AND ql.Frequency <> ''ONCE OFF'' AND ISNULL(Roster, '''') <> '''''

	IF @FeeType = 'FEE' BEGIN

		SET @Query = @Query + ' AND it.MinorGroup = ''FEE''' + ' ORDER BY ql.RecordNumber '
		EXEC sp_executesql @query 

	END ELSE BEGIN

		SET @Query = @Query + ' AND it.MinorGroup <> ''FEE''' + ' ORDER BY ql.RecordNumber '
		EXEC sp_executesql @query

	END

	SET @CursorQuery = 'SET @MyCursor = CURSOR FOR SELECT * from ' + 'QRoster' + @TableID

	DECLARE @TableName as varchar(256) = 'QRoster' + @TableID
	DECLARE @Sql as nvarchar(max)
	SET @Sql = 'DECLARE cursor1 CURSOR GLOBAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR SELECT * FROM ' + cast(@TableName as nvarchar(256))
	EXEC(@Sql)

	--DEBUG
	SELECT 'ABOUT TO OPEN CURSOR', @CursorQuery AS CursorQuery, @TableName AS Tablename, @SQL 

	OPEN cursor1
	FETCH next FROM cursor1 INTO 
			 @RosterData
			,@qh_Date
			,@qh_DocNo
			,@qh_ClientID
			,@qh_ProgramID
			,@qh_Narration
			,@qh_Amount
			,@qh_Contingency
			,@qh_Basis
			,@qh_Contribution
			,@qh_ContingencyBuildAmount
			,@qh_AdminAmount_Perc
			,@qh_DaysCalc
			,@qh_Budget
			,@qh_QuoteBase
			,@qh_IncomeTestedFee
			,@qh_GovtContribution
			,@qh_PackageSupplements
			,@qh_DailyCDCRate
			,@qh_AgreedTopUp
			,@qh_BalanceAtQuote
			,@qh_CLAssessedIncomeTestedFee
			,@qh_FeesAccepted
			,@qh_BasePension
			,@qh_DailyBasicCareFee
			,@qh_DailyIncomeTestedFee
			,@qh_DailyAgreedTopUp
			,@qh_HardshipSupplement
			,@qh_QuoteView
			,@ql_RecordNumber
			,@ql_Doc_Hdr_ID
			,@ql_ItemID
			,@ql_Qty
			,@ql_BillUnit
			,@ql_UnitBillRate
			,@ql_LineBillExTax
			,@ql_Tax
			,@ql_LineBillIncTax
			,@ql_DisplayText
			,@ql_BudgetEnforcement
			,@ql_BudgetGroup
			,@ql_Notes
			,@ql_Line
			,@ql_Frequency
			,@ql_QuoteQty
			,@ql_PriceType
			,@ql_QuotePerc
			,@ql_BudgetPerc
			,@ql_Day
			,@ql_DTime
			,@ql_Week
			,@ql_RCycle
			,@ql_LengthInWeeks
			,@ql_RosterRecurrence
			,@ql_StrategyID
			,@ql_SortOrder		
			,@it_Title
			,@it_AutoApprove
			,@it_RosterGroup
			,@it_minorgroup
			,@re_accountno
			,@re_AgencyDefinedGroup
			,@pr_Name

	--DEBUG
	SELECT 'CURSOR Opened' 

	WHILE @@FETCH_STATUS = 0

		BEGIN

			DECLARE @ONEONONE AS varchar (20);
			DECLARE @TRAVEL AS varchar (20);
			DECLARE @ADMISSION AS varchar (20);
			DECLARE @TRANSPORT AS varchar (20);
			DECLARE @CENTREBASEDACTIVITY AS varchar (25);
			DECLARE @GROUPACTIVITY AS varchar (20);
			DECLARE @ITEM AS varchar (20);
			DECLARE @BOOKED AS varchar (20);
			DECLARE @MULTIPLE AS varchar (20);
			DECLARE @INTERNAL as varchar (20);

			SELECT 'IN CURSOR LOOP 001'
			SET @ONEONONE = '''ONEONONE'''
			SET @TRAVEL = '''TRAVEL'''
			SET @ADMISSION = '''ADMISSION'''
			SET @TRANSPORT = '''TRANSPORT'''
			SET @CENTREBASEDACTIVITY = '''CENTREBASEDACTIVITY'''
			SET @GROUPACTIVITY = '''GROUPACTIVITY'''
			SET @ITEM = '''ITEM'''
			SET @BOOKED = '''BOOKED'''
			SET @MULTIPLE = '''MULTIPLE'''
			SET @INTERNAL = '''INTERNAL'''

			SELECT 'IN CURSOR LOOP 002'

			SET @Sql = 
			'SELECT 
			@Clientcode = re.accountno,
  		    @AgencyDefinedGroup = re.AgencyDefinedGroup, 
		    @Program = pr.[Name], 
			@ServiceType = it.Title,
			@Type = CASE 
			WHEN it.RosterGroup = ' + @ONEONONE + ' THEN 1
			WHEN it.RosterGroup = ' + @TRAVEL + ' THEN 5
			WHEN it.RosterGroup = ' + @ADMISSION + ' THEN 7
			WHEN it.RosterGroup = ' + @TRANSPORT + ' THEN 10
			WHEN it.RosterGroup = ' + @CENTREBASEDACTIVITY + ' THEN 11
			WHEN it.RosterGroup = ' + @GROUPACTIVITY + ' THEN 12
			WHEN it.RosterGroup = ' + @ITEM + ' THEN 14
			END,
			@StaffCode = CASE 
			WHEN it.RosterGroup IN (' + @ONEONONE + ',' + @TRAVEL + ') THEN ' + @BOOKED + '
			WHEN it.RosterGroup = ' + @ADMISSION + ' AND it.MinorGroup = ' + '''FEE''' + ' THEN ' + @INTERNAL + '
			WHEN it.RosterGroup = ' + @ADMISSION + ' AND it.MinorGroup <> ' + '''FEE''' + ' THEN ' + @BOOKED + '
			WHEN it.RosterGroup IN (' + @TRANSPORT + ',' + @CENTREBASEDACTIVITY + ',' + @GROUPACTIVITY  + ') THEN ' + '''!''' + @MULTIPLE + '
			WHEN it.RosterGroup = ' + @ITEM + ' THEN ' + @INTERNAL + ' END,
			@DatasetType = it.HACCType
			FROM ' + @TableName + ' qt
			INNER JOIN Recipients re ON re.SQLID = qt.ClientID
			INNER JOIN HUmanResourceTypes pr ON pr.recordnumber = qt.ProgramID
			INNER JOIN ItemTypes it ON it.Recnum = qt.ItemID
			WHERE qt.RecordNumber = ' + CONVERT(Varchar, @ql_RecordNumber) 
	
	SELECT @SQL AS CreatedSQL1
	--DEBUG
	SELECT 'About to sp_execute'
	EXEC sp_executesql @Sql, 
		N'@Clientcode varchar (50) OUT, 
		  @AgencyDefinedGroup varchar (50) OUT, 
		  @Program varchar (50) OUT, 
		  @ServiceType varchar (50) OUT,
		  @Type int OUT,
		  @StaffCode varchar (50) OUT, 
		  @DatasetType varchar (255) OUT',
		  @ClientCode OUT,
		  @AgencyDefinedGroup OUT,
		  @Program OUT,
		  @ServiceType OUT,
		  @Type OUT,
		  @StaffCode OUT,
		  @DatasetType OUT

				  
			
			SET @DateToCheck = @StartDate
			--DEBUG
			SELECT 'OPENED CURSOR Anout to do date comparison' AS D1, @DateToCheck AS DateToCheck, @ClientCode AS RosterClientCode, @AgencyDefinedGroup AS RosterAnal,  @Program AS RosterProgram,  	@ServiceType AS RosterServiceTytpe, @DateToCheck AS DateToCheck, @StartDate AS StartDate, @EndDate AS EndDFate

			WHILE @DateToCheck <= @EndDate
				
				BEGIN
					SET @DayNumber = DATEPART(WEEKDAY,@DateToCheck)
					SET @Dayname = UPPER(FORMAT(CONVERT(Date,@DateToCheck), 'ddd'))
					EXEC @DateWeek = GetWeekNo @DateToCheck
					SET @Offset = 
					CASE 
						WHEN @DateWeek = 1 THEN (@DayNumber * 7) + (@DayNumber - 1) * 6
						WHEN @DateWeek = 2 THEN 91 + (@DayNumber * 7) + (@DayNumber - 1) * 6
						WHEN @DateWeek = 3 THEN 196 +(@DayNumber * 7) + (@DayNumber - 1) * 6
						WHEN @DateWeek = 4 THEN 294 + (@DayNumber * 7) + (@DayNumber - 1) * 6
					END
					SET @DurationTest = REPLACE(SUBSTRING(@RosterData, @Offset, 5), ':', '.')
						
					--DEBUG
					SELECT @DateToCheck AS TestDate, @DayNumber AS Daynumber, @DurationTEST AS DurationTest, @Offset AS Offset
						
					IF ISNULL(@DurationTest, '00.00') <> '00.00' BEGIN

						SET @StartTime		 = SUBSTRING(@RosterData, @Offset- 6, 5)
						--SET @DurationTest = SUBSTRING(@RosterData, @Offset, 5)
						SET @RosterBlock	 = Round((Convert(integer, Left(@StartTime, 2)) * 60) / 5 + Convert(integer, Right(@StartTime, 2)) / 5, 0)
						SET @DurationMinutes = CONVERT(Numeric(10,2), @DurationTest) * 60
						SET @RosterDuration  = Round(@DurationMinutes / 5, 0)
						SET @BillQty		 = CASE WHEN @ql_BillUnit = 'HOUR' THEN ROUND(@DurationMinutes/60, 2) ELSE 1 END
						

						--DEBUG
						--SELECT @DateToCheck AS RosterDate, @StartTime AS StartTime,   @DurationTest AS DurationTest, @DurationMinutes, @MaxCount AS MaxCount, 
						--@OFFSETCOUNTER AS OffsetCounter, @RosterType as rostertype, @DayNumber AS DayNumber, @DayName AS [DayName], @DateWeek AS [DateWeek]

						SELECT 'CREATE OR ALTER SHORT ROSTER ENTRY' AS CreatRosterDetails, @ClientCode AS ClientCOde, @StaffCode AS StaffCode, 
								@ServiceType AS ServiceType, @Program AS Program, @DateToCheck AS [Date], 
								@StartTime AS StartTime, @User AS Creator, @User AS Editer, @ql_BillUnit AS BillUnit, @AgencyDefinedGroup AS [GROUP], 
								'', @Type AS [Type], @RosterDuration AS RosterBlocks, @BillQty AS BillQty, @RosterBlock AS BlockNo, @ql_Notes, 
								'AUTO CREATED FROM QUOTE' AS [Tab], @DatasetType AS HACCType, @ClientCode AS BillTo, @ql_BillUnit AS BillUnit,
								'', 0, @ql_UnitBillRate AS UnitBillRate, 0, '', ''

						EXEC CreateShortRosterEntry  @ClientCode, @StaffCode, @ServiceType, @Program, @DateToCheck, 
						  								@StartTime, @User, @user, @ql_BillUnit, @AgencyDefinedGroup, 
														'', @Type, @RosterDuration, @BillQty, @ql_Notes, 
						    							@RosterBlock, '','AUTO CREATED FROM QUOTE',  @DatasetType, @ClientCode, 
														@ql_BillUnit, '', 0, @ql_UnitBillRate, 0, '', ''
					END
					SET @DateToCheck = CONVERT(VARCHAR, DATEADD(day, 1, CONVERT(Date, @DateToCheck)), 111)
				END

    FETCH NEXT FROM cursor1 INTO 
		 @RosterData
		,@qh_Date
		,@qh_DocNo
		,@qh_ClientID
		,@qh_ProgramID
		,@qh_Narration
		,@qh_Amount
		,@qh_Contingency
		,@qh_Basis
		,@qh_Contribution
		,@qh_ContingencyBuildAmount
		,@qh_AdminAmount_Perc
		,@qh_DaysCalc
		,@qh_Budget
		,@qh_QuoteBase
		,@qh_IncomeTestedFee
		,@qh_GovtContribution
		,@qh_PackageSupplements
		,@qh_DailyCDCRate
		,@qh_AgreedTopUp
		,@qh_BalanceAtQuote
		,@qh_CLAssessedIncomeTestedFee
		,@qh_FeesAccepted
		,@qh_BasePension
		,@qh_DailyBasicCareFee
		,@qh_DailyIncomeTestedFee
		,@qh_DailyAgreedTopUp
		,@qh_HardshipSupplement
		,@qh_QuoteView
		,@ql_RecordNumber
		,@ql_Doc_Hdr_ID
		,@ql_ItemID
		,@ql_Qty
		,@ql_BillUnit
		,@ql_UnitBillRate
		,@ql_LineBillExTax
		,@ql_Tax
		,@ql_LineBillIncTax
		,@ql_DisplayText
		,@ql_BudgetEnforcement
		,@ql_BudgetGroup
		,@ql_Notes
		,@ql_Line
		,@ql_Frequency
		,@ql_QuoteQty
		,@ql_PriceType
		,@ql_QuotePerc
		,@ql_BudgetPerc
		,@ql_Day
		,@ql_DTime
		,@ql_Week
		,@ql_RCycle
		,@ql_LengthInWeeks
		,@ql_RosterRecurrence
		,@ql_StrategyID
		,@ql_SortOrder		
		,@it_Title
		,@it_AutoApprove
		,@it_RosterGroup
		,@it_minorgroup
		,@re_accountno
		,@re_AgencyDefinedGroup
		,@pr_Name
		SELECT @ql_RecordNumber
	END

CLOSE cursor1
DEALLOCATE cursor1

SELECT 'END AutoCreateBookings'


 END

 
GO
/****** Object:  StoredProcedure [dbo].[AcceptQuote]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[AcceptQuote] 
 (
@DocRecNo int,
@PersonID varchar (35), 
@AdmissionDate varchar (10),
@User varchar (50),
@RecipientStatus varchar (20),
@AdmissionTime varchar (5),
@AdmissionDuration varchar (5),
@AdmissionActivity varchar (50),
@DeleteExistingService as bit,
@UpdateMasterFees as bit,
@UpdateMasterPrices as biT,
@UpdateRosteredFees as bit,
@UpdateRosteredPrices as bit,
@DeleteMasterActivities as bit,
@DeleteCurrentActivities as bit,
@CreateMasterBookings as bit,
@CreateCurrentBookings as bit,
@StartRosters varchar (10),
@EndRosters varchar (10),
@AutoPopulatePackageStartBalance bit
 )

 AS BEGIN       
 
 --DECLARATIONS
BEGIN

--DECLARE @DocRecNo int = 45819
--DECLARE @PersonID varchar (35) = 'T0100005771'
--DECLARE @AdmissionDate varchar (10) = Convert(varchar, GetDate(), 111)
--DECLARE @User varchar (50) = 'sysmgr'
--DECLARE @RecipientStatus varchar (20) = 'REFERRAL'
--DECLARE @AdmissionTime varchar (5) = '09:00'
--DECLARE @AdmissionDuration varchar (5) = '00:15'
--DECLARE @AdmissionActivity varchar (50) = '*ADMISSION'
--DECLARE @DeleteExistingService as bit = 0
--DECLARE @UpdateMasterFees as bit = 1
--DECLARE @UpdateMasterPrices as bit = 1
--DECLARE @UpdateRosteredFees as bit = 1
--DECLARE @UpdateRosteredPrices as bit = 1
--DECLARE @DeleteMasterActivities as bit = 1
--DECLARE @DeleteCurrentActivities as bit = 1
--DECLARE @CreateMasterBookings as bit = 1
--DECLARE @CreateCurrentBookings as bit = 1
--DECLARE @StartRosters varchar (10) = '2021/05/06'
--DECLARE @EndRosters varchar (10) = '2021/06/06'
--DECLARE @AutoPopulatePackageStartBalance bit = 1


DECLARE @StaffCode varchar (50);
DECLARE @UserStaffCode varchar (50);
DECLARE @ClientCOde varchar (50);
DECLARE @ProgramID int;
DECLARE @RP_ID int;
DECLARE @Contribution varchar (10);
DECLARE @Contingency varchar (10);
DECLARE @IncomeTestedFee varchar (15);
DECLARE @AdminAmount_Perc varchar (20);
DECLARE @ContingencyBuildAmount varchar (20);
DECLARE @QuoteBase varchar (25);
DECLARE @PackageSupplements varchar (30);
DECLARE @HardshipSupplement varchar (10);
DECLARE @AgreedTopUp varchar (15);
DECLARE @DailyBasicCareFee varchar (15);
DECLARE @DailyIncomeTestedFee varchar (15);
DECLARE @DailyAgreedTopUp varchar (15);
DECLARE @GovtContribution varchar (15);
DECLARE @Budget float;
DECLARE @Program varchar (50);
DECLARE @Type varchar (50);
DECLARE @IsCDC bit;
DECLARE @IsIndividual bit;
DECLARE @Cycle varchar (10);
DECLARE @RecipientProgramStatus varchar (20);
DECLARE @RecipientProgramID int;
DECLARE @ConversionFactor float;
DECLARE @PackageType varchar (20);
DECLARE @ExpireUsing varchar (20);
DECLARE @ItemUnit varchar (20);
DECLARE @PerUnit varchar (20);
DECLARE @TimeUnit varchar (20);
DECLARE @Period varchar (20);
DECLARE @AP_CostType varchar (20);
DECLARE @AP_Period varchar (20);
DECLARE @AP_PerUnit varchar (20);
DECLARE @P_Def_Alert_Allowed varchar (15);
DECLARE @P_Def_Alert_Yellow varchar (15);
DECLARE @P_Def_Alert_Orange varchar (15);
DECLARE @P_Def_Alert_Red varchar (15);
DECLARE @AuditDescription varchar (75);
DECLARE @RecipientCategory varchar (50);
DECLARE @PersonSQLID varchar (50);
DECLARE @DatasetType varchar (200);
DECLARE @StartBlock integer;
DECLARE @RosterPeriod integer;

END

----------------------------------------------------------------------------------------------------------------------------------------------
-- GET QUOTE DETAILS

SET @RosterPeriod = 14

SELECT 

@ProgramID = ProgramId, 
@RP_ID = CPID,
@Contribution = IsNull(Contribution, 0),
@Contingency = IsNull(Contingency, 0),
@IncomeTestedFee = IsNull(IncomeTestedFee, 0), 
@AdminAmount_Perc = IsNull(AdminAmount_Perc, 0), 
@ContingencyBuildAmount =IsNull(ContingencyBuildAmount, 0),
@QuoteBase= QuoteBase, 
@ConversionFactor = 
	CASE 
		WHEN QuoteBase = 'ANNUALLY' THEN 12
		WHEN QuoteBase = 'MONTHLY' THEN 1 / 12
		WHEN QuoteBase = 'FORTNIGHTLY' THEN 26/12
		WHEN QuoteBase = 'QUARTERLY' THEN 13/12
		WHEN QuoteBase = 'WEEKLY' THEN 52/12
		WHEN QuoteBase = 'DAILY' THEN 365/12
		ELSE 1
	END ,
@PackageSupplements =PackageSupplements, 
@HardshipSupplement = IsNull(HardshipSupplement, 0), 
@AgreedTopUp = isnull(AgreedTopUp, 0), 
@DailyBasicCareFee = isnull(DailyBasicCareFee, 0),
@DailyIncomeTestedFee = isnull(DailyIncomeTestedFee, 0), 
@DailyAgreedTopUp = isnull(DailyAgreedTopUp, 0), 
@GovtContribution = ISNULL(GovtContribution, 0), 
@Budget = (SELECT SUM((QuoteQty * [Unit Bill Rate]) + (QuoteQty * Tax)) FROM Qte_Lne WHERE Doc_Hdr_ID = Qte_Hdr.RecordNumber)
FROM qte_hdr WHERE CPID = @DocRecNo

-- /END GET QUOTE DETAILS
--=============================================================================================================================================



----------------------------------------------------------------------------------------------------------------------------------------------
-- GET TEMPLATE DEFAULTS

SELECT 
@Program = [Name], 
@Type = [Type], 
@IsCDC = UserYesNo1,
@IsIndividual = UserYesNo2,
@Cycle = User12,
@ExpireUsing = P_Def_Expire_Using,
@ItemUnit = P_Def_Expire_CostType,
@PerUnit = P_Def_Expire_Unit,
@TimeUnit = P_Def_Expire_Period,
@Period = P_Def_Expire_Length,
@AP_CostType = P_Def_Expire_CostType,
@AP_Period = P_Def_Alert_Period,
@AP_PerUnit =
	CASE 
		WHEN ISNULL([P_Def_Alert_Period], '') <> '' AND ISNULL([P_Def_Alert_Allowed], '') <> '' THEN 'REC' 
		WHEN ISNULL([P_Def_Alert_Period], '') = '' And ISNULL([P_Def_Alert_Allowed], '') <> '' THEN 'BAL' 
	END,
@P_Def_Alert_Allowed = P_Def_Alert_Allowed,
@P_Def_Alert_Yellow = P_Def_Alert_Yellow,
@P_Def_Alert_Orange = P_Def_Alert_Orange,
@P_Def_Alert_Red = P_Def_Alert_Red
FROM HumanResourceTypes WHERE RecordNumber = @ProgramID

SELECT @RecipientCategory = AgencyDefinedGroup, @ClientCode = Accountno, @PersonSQLID = SQLID, @RecipientStatus = [Type] FROM Recipients WHERE UniqueID = @PersonID
SELECT @UserStaffCode = StaffCode FROM Userinfo WHERE [Name] = @User
SELECT @RecipientProgramStatus = ProgramStatus, @RecipientProgramID = Recordnumber FROM RecipientPrograms WHERE Program = @Program AND PersonID = @PersonID


IF @IsCDC = 1 
	BEGIN 
		SET @Budget = @Budget/365 
		SET @PackageType = 'CDC-HCP'
	END
ELSE BEGIN
	SET @PackageType = 'OTHER'
END

IF ISNULL(@Cycle, '') = '' 
	BEGIN
		SET @Cycle = 'CYCLE 1'
	END 
-- /END GET TEMPLATE DEFAULTS
--===========================================================================================================================================



----------------------------------------------------------------------------------------------------------------------------------------------
-- UPDATE THE RECIPIENT PACKAGE/PROGRAM DETAILS

					UPDATE RecipientPrograms SET 
	
						Quantity = @Budget,
						ProgramStatus = 'ACTIVE',
						PackageType = @PackageType,
						ClientCont = @Contribution,
						PackageSupplements = @PackageSupplements,
						HardshipSupplement = @HardshipSupplement,
						IncomeTestedFee = @IncomeTestedFee,
						AgreedTopUp = @AgreedTopUp,
						Contingency = @Contingency,
						Contingency_Start = @ContingencyBuildAmount,

						DailyBasicCareFee = @DailyBasicCareFee,
						DailyIncomeTestedFee = @DailyIncomeTestedFee,
						DailyAgreedTopUp = @DailyAgreedTopUp,
						[ExpireUsing] = @ExpireUsing,
						[ItemUnit] = @ItemUnit,
						[PerUnit] = @PerUnit,
						[TimeUnit] = @TimeUnit,
						[Period] = @Period,
						[AP_CostType] = @AP_CostType,
						[AP_Period] = @AP_Period,
						[AP_PerUnit] = @AP_PerUnit,
						AP_BasedOn = 
							CASE WHEN @IsCDC = 1 THEN 
								CASE WHEN CHARINDEX('%', @P_Def_Alert_Allowed) > 0 THEN
									Replace(@P_Def_Alert_Allowed, '%', '') / 100 * (@Budget/12)
								ELSE
									@Budget/12
								END
							ELSE
								@Budget/12
							END,

						AP_YellowQty = 
								CASE WHEN CHARINDEX('%', @P_Def_Alert_Yellow) > 0 THEN
									Round(Convert(Decimal, Replace(@P_Def_Alert_Yellow, '%', '')) / 100 * (@Budget/12), 0)
								ELSE
									@P_Def_Alert_Yellow
								END,

						AP_OrangeQty = 
								CASE WHEN CHARINDEX('%', @P_Def_Alert_Orange) > 0 THEN
									Round(Convert(Decimal, Replace(@P_Def_Alert_Orange, '%', '')) / 100 * (@Budget/12), 0)
								ELSE
									@P_Def_Alert_Orange
								END,
						AP_RedQty = 
								CASE WHEN CHARINDEX('%', @P_Def_Alert_Red) > 0 THEN
									Round(Convert(Decimal, Replace(@P_Def_Alert_Red, '%', '')) / 100 * (@Budget/12), 0)
								ELSE
									@P_Def_Alert_Red
								END,

						StartDate = @AdmissionDate,
						AlertStartDate = 
							CASE WHEN ISNULL(@AP_PerUnit,'') = 'REC' AND ISNULL(@AP_CostType, '') <> '' THEN
								DATEADD(mm, DATEDIFF(mm, 0, @AdmissionDate), 0)
							ELSE
								NULL
							END
					WHERE Program = @Program AND PersonID = @PersonID

-- /END UPDATE THE RECIPIENT PACKAGE/PROGRAM DETAILS
--============================================================================================================================================



----------------------------------------------------------------------------------------------------------------------------------------------
-- INSERT ANY NON EXISTENT SERVICES TO APPROVED SERVICES

					INSERT INTO ServiceOverview 
								(PersonID, [Service Type], UnitType, [Unit Bill Rate], TaxRate, ServiceProgram, ServiceBiller, ServiceStatus,
								ForceSpecialPrice, Duration, Period,
								BudgetType, BudgetAmount, BudgetLimitType, BudgetStartDate) 
								SELECT DISTINCT REC.UniqueID AS PersonID, ITM.Title AS [Service Type], QL.[Bill Unit] AS UnitType, QL.[Unit Bill Rate], CASE WHEN ISNULL(QL.Tax, 0) > 0 THEN 10 ELSE 0 END AS TaxRate, 
								HRT.Name AS ServiceProgram, REC.BillTo AS ServiceBiller, 'ACTIVE' AS ServiceStatus, 
								1 AS ForceSpecialPrice, SUM(QL.[Qty]), QL.[Frequency], 
								'DOLLARS' AS BudgetType, SUM(QL.QuoteQty * [Unit Bill Rate]) AS BudgetAmount, QL.BudgetEnforcement AS BudgetLimitType, @AdmissionDate AS  BudgetStartDate 
								FROM 
								QTE_LNE QL 
								INNER JOIN qte_hdr QH on DOC_HDR_ID = QH.RecordNumber 
								INNER JOIN Recipients REC ON QH.ClientiD = REC.SQLID 
								INNER JOIN ItemTypes ITM ON QL.ItemID = ITM.Recnum 
								INNER JOIN HumanResourceTypes HRT ON QH.ProgramID = HRT.RecordNumber 
								WHERE QH.CPID =  @RP_ID
											AND Itm.Title NOT IN (SELECT [Service Type] FROM ServiceOverview WHERE ServiceProgram = @Program AND PersonID = @PersonID)
												Group By REC.UniqueID, 
											ITM.Title, 
											QL.[Bill Unit], 
											QL.[Unit Bill Rate], 
											QL.TAX, 
											HRT.Name, 
											REC.BillTo, 
											QL.[Frequency], QL.BudgetEnforcement 

-- /END INSERT ANY NON EXISTENT SERVICES TO APPROVED SERVICES
--============================================================================================================================================



-----------------------------------------------------------------------------------------
-- CREATE OR ALTER ADMISSION ROSTER ENTRY, SET PACKAGE BALANCE, CREATE OR ALTER MASTER  AND CURRENT ROSTERS
-----------------------------------------------------------------------------------------

IF @RecipientProgramStatus IN ('REFERRAL', 'INACTIVE') 
	BEGIN
		SET @AuditDescription = @ClientCode + ':' + @Program
		DECLARE @OpeningBalance float
		DECLARE @BillQty AS varchar (5)
		SET @BillQty =           Round((Convert(float, Left(@AdmissionDuration, 2)) * 60) + Convert(float, Right(@Admissionduration, 2)) / 60, 2)
		SET @Admissionduration = Round((Convert(integer, Left(@AdmissionDuration, 2)) * 60) + Convert(integer, Right(@Admissionduration, 2)) / 5, 0)
		SET @StartBlock = Round((Convert(integer, Left(@AdmissionTime, 2)) * 60) + Convert(integer, Right(@AdmissionTime, 2)) / 5, 0)


		EXEC CreateShortRosterEntry   @ClientCode, @UserStaffCode, @AdmissionActivity, @Program, @AdmissionDate, 
									   @AdmissionTime, @User, @user, 'HOUR', @RecipientCategory, '', 7, @AdmissionDuration, @BillQty, '', 
									   @StartBlock, '','ADMISSION (ACCEPT QUOTE)',  @DatasetType, @ClientCode, 'HOUR', '', 0, 0, 0, '', ''

		IF @AutoPopulatePackageStartBalance = 1
		BEGIN
			Set @OpeningBalance = @Budget
		END
		ELSE BEGIN
			Set @OpeningBalance = 0
		END
		--DEBUG LINES

		--SET INITIAL PACKAGE BALANCE
        DELETE FROM PackageBalances WHERE ProgramID = @RecipientProgramID AND PersonID = @PersonSQLID
        INSERT INTO PackageBalances (ProgramID, PersonID, [Date], Balance, BatchNumber) VALUES (@ProgramID, @PersonSQLID, @AdmissionDate, @OpeningBalance, 0)
		SELECT  @AdmissionDate AS AdmissionDate, @ProgramID AS ProgramID, @Program AS Program, @RecipientProgramID AS RecipientProgramID, @OpeningBalance, @RecipientProgramStatus AS RecipientProgramStatus, @BUdget as Budget, @P_Def_Alert_Allowed AS Def_Alert_Allowed, @P_Def_Alert_Yellow  AS Def_Alert_Yellow, @P_Def_Alert_Orange AS Def_Alert_Orange, @P_Def_Alert_Red Def_Alert_Red, @StartBlock AS StartBlock, @Admissionduration AS AdmissionDuration, @BillQty AS BillQty
		SELECT 'ABOUT TO CREATE OR ALTER BOOKINGS' AS DebugState, @User AS [User], @DocRecNo AS  DocRecNo, @Cycle AS Cycle, @StartRosters AS StartRosters, @EndRosters AS EndRosters, @RosterPeriod AS RosterPeriod

		EXEC AutoCreateBookings @user, @DocRecNo, 'FEE', 'MASTER', @Cycle, '', '', @RosterPeriod 
        EXEC AutoCreateBookings @user, @DocRecNo, 'FEE', 'DATED',  '', @StartRosters, @EndRosters, @RosterPeriod
        EXEC AutoCreateBookings @user, @DocRecNo, 'NONFEE', 'MASTER', @Cycle, '', '', @RosterPeriod 
        EXEC AutoCreateBookings @user, @DocRecNo, 'NONFEE', 'DATED', '', @StartRosters, @EndRosters, @RosterPeriod

		SELECT 'FINISHED CREATING BOOKINGS' AS DebugState

	END
ELSE
	BEGIN
		SET @auditDescription = @ClientCode + 'AUDITEXT'

		IF @DeleteMasterActivities = 1
			BEGIN
				DELETE FROM Roster WHERE RecordNo IN 
				(SELECT RecordNo FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.Title WHERE Yearno = 1900 AND [Client Code] = @ClientCode AND [Program] = @Program AND it.MinorGroup <> 'FEE')
            END

		IF @UpdateMasterFees = 1
			BEGIN        
				DELETE FROM Roster WHERE RecordNo IN 
				(SELECT RecordNo FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.Title WHERE Yearno = 1900 AND [Client Code] = @ClientCode AND [Program] = @Program AND it.MinorGroup = 'FEE')
                --AutoCreateBookings s_DocRecNo, FEE, MASTER, s_Cycle
            END
            
        IF @UpdateMasterPrices = 1
			BEGIN
				UPDATE Roster SET [Unit Bill Rate] = 
                    CASE WHEN ISNULL(so.[ForceSpecialPrice], 0) = 0 THEN it.Amount ELSE so.[Unit Bill Rate] END FROM ServiceOverview so 
					INNER JOIN ItemTypes it ON it.Title = so.[Service Type] 
                    WHERE 
                            so.PersonID  = @PersonID
                        AND so.[ServiceProgram] = Roster.[Program]
                        AND so.[Service Type] = Roster.[Service Type]
                        AND Roster.[Date] BETWEEN '1900/01/01' AND '2000/01/01'
                        AND [Client Code] = @ClientCode
                        AND [Program] = @Program
                        AND ISNULL(it.ExcludeFromRequote, 0) = 0
                        AND it.MinorGroup <> 'FEE' 
			         --IF @CreateMasterBookings Then AutoCreateBookings s_DocRecNo, NONFEE, MASTER, s_Cycle
			END
           
		IF @DeleteCurrentActivities = 1
			BEGIN
				DELETE FROM Roster WHERE RecordNo IN 
				(SELECT RecordNo FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.Title WHERE Date BETWEEN @StartRosters AND @EndRosters AND [Client Code] = @ClientCode AND [Program] = @Program AND it.MinorGroup <> 'FEE')
			END
            
		IF @UpdateRosteredFees = 1
			BEGIN        
				DELETE FROM Roster WHERE RecordNo IN 
				(SELECT RecordNo FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.Title WHERE Date >= @StartRosters AND [Client Code] = @ClientCode AND [Program] = @Program AND it.MinorGroup = 'FEE')
				--AutoCreateBookings s_DocRecNo, FEE, DATED, , s_StartDate, s_EndDate
			END

		IF @UpdateRosteredPrices = 1
			BEGIN
                UPDATE Roster SET [Unit Bill Rate] = 
                CASE WHEN ISNULL(so.[ForceSpecialPrice], 0) = 0 THEN it.Amount ELSE so.[Unit Bill Rate] END FROM ServiceOverview so
                INNER JOIN ItemTypes it ON it.Title = so.[Service Type]
                WHERE
                        so.PersonID  = @ClientCode 
                    AND so.[ServiceProgram] = Roster.[Program]
                    AND so.[Service Type] = Roster.[Service Type]
                    AND Roster.[Date] >= @StartRosters
                    AND [Client Code] = @ClientCode
                    AND [Program] = @Program
                    AND ISNULL([ExcludeFromRequote], 0) = 0
                    AND it.MinorGroup <> 'FEE'
	            --CreateCurrentBookings Then AutoCreateBookings s_DocRecNo, 'NONFEE', 'DATED', '', s_StartDate, s_EndDate
            END
	END

----------------------------------------------------------------------------------------------
-- /END CREATE OR ALTER ADMISSION ROSTER ENTRY, SET PACKAGE BALANCE, CREATE OR ALTER MASTER  AND CURRENT ROSTERS
--=================================================================================================================================0



----------------------------------------
-- UPDATE RECIPIENT RECORD STATUS/VALUES
----------------------------------------

	IF @RecipientStatus = 'REFERRAL' 
		BEGIN
			UPDATE Recipients SET 
			[Type] = 'RECIPIENT',
			DischargeDate = Null,
			DischargedBy = Null,
			AdmissionDate = @AdmissionDate,
			AdmittedBy = @User,
			ExcludeFromRosterCopy = 1
			WHERE UniqueID = @PersonID
		END
	ELSE BEGIN

		IF @RecipientStatus = 'CARER' 
			BEGIN

				UPDATE Recipients SET 
				[Type] = 'CARER/RECIPIENT', 
				DischargeDate = Null,
				DischargedBy = Null,
				AdmissionDate = @AdmissionDate,
				AdmittedBy = @User,
				ExcludeFromRosterCopy = 1
				WHERE UniqueID = @PersonID
	
			END
		ELSE BEGIN

				UPDATE Recipients SET DischargeDate = Null,
				DischargedBy = Null,
				AdmissionDate = @AdmissionDate,
				AdmittedBy = @user,
				ExcludeFromRosterCopy = 1
				WHERE UniqueID = @PersonID
	
		END

	END

---------------------------------------------
-- /END UPDATE RECIPIENT RECORD STATUS/VALUES
--=================================================================================================================================0
END
GO

/****** Object:  StoredProcedure [dbo].[Check_BreachedRosterRules]    Script Date: 22/02/2024 12:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery42.sql|7|0|C:\Users\Arshad\AppData\Local\Temp\15\~vsFC17.sql
-- Batch submitted through debugger: SQLQuery37.sql|7|0|C:\Users\Arshad\AppData\Local\Temp\15\~vs49B0.sql
CREATE OR ALTER          PROCEDURE [dbo].[Check_BreachedRosterRules](@sMode varchar(100), 
                                        @sStaffCode varchar(100), 
                                        @sClientCode varchar(100), 
                                        @sProgram varchar(100), 
                                        @sDate varchar(100), 
                                        @sStartTime varchar(100), 
                                        @sDuration varchar(100), 
                                        @sActivity varchar(100)='', 
                                        @sRORecordno varchar(100) = '', 
                                        @sState varchar(100) = '', 
                                        @bEnforceActivityLimits bit=0, 
                                        @bUseAwards bit=0, 
                                        @bDisallowOT bit=0, 
                                        @bDisallowNoBreaks bit=0, 
                                        @bDisallowConflicts bit=0, 
                                        @bForceNote bit=0, 
                                        @sOldDuration varchar(100) = '', 
                                        @sExcludeRecords varchar(100) = '', 
                                        @bSuppressErrorMessages  bit=0, 
                                        @sStatusMsg varchar(100) = '',
										@PasteAction varchar(20)='') 
       
  As

  begin

  declare @ErrorValue smallint;
  declare @Msg varchar(500);
  Declare @continue bit;
  set @continue=0;
        ---------------------------PARAMATER INTEGRITY CHECK-------------------------------------------------
     set @ErrorValue=-1;
     If (isnull(@sMode,'') = '' Or isnull(@sDate,'') = '' Or isnull(@sStartTime,'') = '' Or isnull(@sDuration,'') = '' )
		set @ErrorValue=0;		
     If ( IsDate(@sDate)=0 )
		set @ErrorValue=0
	 IF (@ErrorValue=0) begin set @msg='Invalid Parameter Values'; Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError; return; End;
     set @sDate = convert(varchar,@sDate, 111)

       -------------------------------------------------------------------------
        --DECLARATIONS AND INITIALISATIONS
       -------------------------------------------------------------------------
            Declare @sPHR               varchar(100);        
            Declare @rActivity          As Cursor;
            Declare @rRec               As Cursor;
	        Declare @sWeekday           varchar(100);	set @sWeekday = '';
	        Declare @sEndTime           varchar(100);	set @sEndTime = '';
	        Declare @bPublicHoliday     bit				set @bPublicHoliday = 0;
	        Declare @s1                 varchar(100);   set @s1 = '';
	        Declare @s2                 varchar(100);   set @s2 = '';
	        Declare @s3                 varchar(100);  set @s3 = '';
            Declare @dOldHrs            float;
            Declare @dNewHrs            float;
            Declare @sAwardGroup        varchar(100);
            Declare @sB                 varchar(100);
            Declare @bBreakRequired     bit
            Declare @rsB                cursor
            Declare @sWklyStart         varchar(100)
            Declare @sWklyEnd           varchar(100)
            Declare @sFNStart           varchar(100)
            Declare @sFNEnd             varchar(100)
            Declare @sPPStart           varchar(100)
			Declare @sPayStartDay       varchar(100)			
            Declare @sPPEnd             varchar(100)
            Declare @bFNPP              bit
            Declare @sPPLength          varchar(100)
            Declare @sPPTestStart       varchar(100)
            Declare @sPPTestEnd         varchar(100)
            Declare @s4                 varchar(100)
            Declare @s5                 varchar(100)
            Declare @rAW                cursor
            Declare @rAC                cursor
            Declare @sAWMsg             varchar(100)
            Declare @dHrs               float;
            Declare @bFirstWeek         bit
            Declare @dTeaBreakAfter     float
            Declare @dMealBreakAfter    float
            Declare @sConflictClient    varchar(100)
            Declare @sConflictStart     varchar(100)
            Declare @sConflictDuration  varchar(100)
            Declare @sConflictActivity  varchar(100)
            Declare @sConflictSetting   varchar(100)
            Declare @bDirect            bit
            Declare @bIndirect          bit
            Declare @iYear              As Integer
            Declare @iMonth             As Integer
            Declare @iDay               As Integer
            Declare @iStartBlock        As Integer
            Declare @iEndBlock          As Integer
            Declare @sConflictRecord    varchar(100)
            Declare @bCheckLunchBreaks  bit
            Declare @bCheckTeaBreaks    bit
            Declare @sStaffCat          varchar(100)
            Declare @bCanConflict       bit
            Declare @bAutoBreak         bit
			Declare @OperatorID varchar(100);
			
			
     
     If (CHARINDEX(@sRORecordno,@sExcludeRecords) = 0 )
      begin  
        If (@sExcludeRecords <> '')
			 set @sExcludeRecords = @sExcludeRecords + ',' + @sRORecordno 
		Else 
			set @sExcludeRecords = @sRORecordno
     End       
      
	Select @continue = Case RosterAwardEnforcement 
	When 'Warn On Breach' then 1	
	When 'Prevent Breach' then 1
	Else 0 End
	from Registration; 
        
    --Declare avU As Variant
    --Declare lU As Long
	 
	 declare @bEnforceValidPositions  bit;	
	 declare  @sAwardEnforcementLevel varchar(100)
	 declare @sAWState varchar(5);
	 set @sAWState = 'ALL'

	 select Top 1 @OperatorID=[name] from UserInfo where StaffCode=@sStaffCode;
	 -----------------START NEW DAILY HOURS CODE
	 --RecordsToArray 
	 SELECT TOP 1 @bEnforceValidPositions=UsePositions, @bEnforceActivityLimits=EnforceActivityLimits, @bUseAwards=UseAwards, @sAwardEnforcementLevel=RosterAwardEnforcement 
	 FROM Registration;
	 --@bEnforceValidPositions = N2S(avU(0, 0)) = 'True'
	 --@bEnforceActivityLimits = N2S(avU(1, 0)) = 'True'
	 --@bUseAwards = N2S(avU(2, 0)) = 'True'
	 --@sAwardEnforcementLevel = N2S(avU(3, 0))
	 --@sAWState = 'ALL'
    
-- Erase avU
 SELECT @bDisallowOT=CanRosterOvertime, @bDisallowNoBreaks=CanRosterBreakless, @bDisallowConflicts=CanRosterConflicts FROM UserInfo WHERE [Name] = @OperatorID
 --bDisallowOT = N2S(avU(0, 0)) <> 'True'
 --bDisallowNoBreaks = N2S(avU(1, 0)) <> 'True'
 --bDisallowConflicts = N2S(avU(2, 0)) <> 'True'
        
         set @sEndTime = left(convert (time,DateAdd(MINUTE, (@sDuration * 5), @sStartTime), 108),5);
         If (@sEndTime = '00:00') set @sEndTime = '24:00';

 --  '------------------------------------------------------------------------------------------------------------------------------------------
  --  'START NEW DAILY HOURS CODE
   Declare @s_DaySQL varchar(50)
        Declare @d_ContinuousTime int
		Declare @d_MealBreakAfter int
        Declare @s_LastEndTime varchar(50)
        Declare @s_NextStartTime varchar(50)
        Declare @s_CurrentStartTime varchar(50)
        Declare @s_CurrentEndTime varchar(50)
        Declare @s_ShiftType varchar(50)
        Declare @l_SvcMinutes int
		declare @select_statement varchar(500);
		Declare @s_LastStartTime varchar(50)
		Declare @s_StartTimeOfBlock varchar(50)
		DECLARE @s_EndTimeOfBlock varchar(50)
		Declare @b_FirstShiftOfTimeBlock bit;
		Declare @b_MealBreakExists bit;
		Declare @l_MealCount smallint;
		declare @l_BlockCount smallint;
		DECLARE @a_MBreak TABLE (rowID int, CurrentStartTime varchar(10),    CurrentEndTime varchar(10), ContinuousTime float  ) 
		Declare @i_StartTime int;
		Declare @b_BreakRequired bit;
		DECLARE @a_Block TABLE(i_StartTime int, l_BlockCount int  ) 
		Declare  @b_BrokenShift bit;
		Declare  @b_BrokenShifts bit;
		Declare  @b_BelowMinEngagement bit;
		Declare @b_BelowMinEngagements bit
		Declare  @RecordCount int;
		set @i_StartTime=0;
		set @RecordCount=0;
		 
		 Declare @b_MinEngagementLastShift bit;
		 Declare @d_MinEngagement int;

    --        set  @select_statement=' SELECT [Start Time], Duration * 5 AS SvcMinutes, 
				--left(convert(varchar,DATEADD(Minute,duration*5,[start time]),108),5) AS [End Time], 
				--CASE WHEN ISNULL(it.JobType, '') = '' THEN ''WORK'' ELSE it.jobtype END AS ShiftType 
				--FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[service type] = it.title 
				--WHERE [Carer Code] = @sStaffCode
    --            AND [Date] = @sDate 
    --            AND ((ISNULL(it.InfoOnly, 0) = 0    OR it.JobType  IN (''TEA BREAK'',''MEAL BREAK'')) AND Type NOT IN (9, 13)) 
				--AND    recordno <> CASE when (@sRORecordno <> '''' And @PasteAction <> ''COPY'') then @sRORecordno else 0 end
				--ORDER BY [Start Time]';

    
        
       -- 'If this is add/edit service or timeblock selection add as virtual shift for award intepretation
       -- 'If not then just award interpret daily roster for nominated date


	  IF CURSOR_STATUS('global','rsDWH') > -1
	   BEGIN
		CLOSE rsDWH
		DEALLOCATE rsDWH
	   END
	 
	

        If (@sStartTime <> '' And @sDuration <> '' )
		begin
		
				DECLARE rsDWH CURSOR FOR  SELECT [Start Time], Duration * 5 AS SvcMinutes, 
					  left(convert(varchar,DATEADD(Minute,duration*5,[start time]),108),5) AS [End Time], 
					  CASE WHEN ISNULL(it.JobType, '') = '' THEN 'WORK' ELSE it.jobtype END AS ShiftType 
					  FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[service type] = it.title 
					  WHERE [Carer Code] = @sStaffCode
					  AND [Date] = @sDate 
					  AND ((ISNULL(it.InfoOnly, 0) = 0    OR it.JobType  IN ('TEA BREAK','MEAL BREAK')) AND Type NOT IN (9, 13)) 
					  AND    recordno <> CASE when (@sRORecordno <> '' And @PasteAction <> 'COPY') then @sRORecordno else 0 end
					  UNION SELECT @sStartTime , (@sDuration * 5) , @sEndTime , 'NEWSHIFT'
					  ORDER BY [Start Time];
        
        End 
        Else begin
					DECLARE rsDWH CURSOR FOR  SELECT [Start Time], Duration * 5 AS SvcMinutes, 
					  left(convert(varchar,DATEADD(Minute,duration*5,[start time]),108),5) AS [End Time], 
					  CASE WHEN ISNULL(it.JobType, '') = '' THEN 'WORK' ELSE it.jobtype END AS ShiftType 
					  FROM ROSTER ro INNER JOIN ItemTypes it ON ro.[service type] = it.title 
					  WHERE [Carer Code] = @sStaffCode
					  AND [Date] = @sDate 
					  AND ((ISNULL(it.InfoOnly, 0) = 0    OR it.JobType  IN ('TEA BREAK','MEAL BREAK')) AND Type NOT IN (9, 13)) 
					  AND    recordno <> CASE when (@sRORecordno <> '' And @PasteAction <> 'COPY') then @sRORecordno else 0 end					 
					  ORDER BY [Start Time];
        
		End

		
		OPEN rsDWH
		FETCH NEXT FROM rsDWH INTO @s_CurrentStartTime,@l_SvcMinutes,@s_CurrentEndTime,@s_ShiftType;
			if (@d_ContinuousTime = 0)
				set @l_BlockCount = 0;
			else
				set @l_MealCount = 0;

			set @s_LastEndTime = @s_CurrentStartTime              
		 --'PRIME THE s_LastEndTime variable
            set  @s_LastStartTime = @s_CurrentStartTime;
			set @s_StartTimeOfBlock =  @s_CurrentStartTime
            set @b_FirstShiftOfTimeBlock = 1
			delete from @a_MBreak;

		WHILE @@FETCH_STATUS = 0  
		BEGIN

			set @RecordCount=@RecordCount+1;
			set @s_CurrentStartTime = @s_CurrentStartTime
			set @s_CurrentEndTime = @s_CurrentEndTime
			set @s_ShiftType = @s_ShiftType
			set @l_SvcMinutes = @l_SvcMinutes
			If @s_ShiftType = 'MEAL BREAK' begin
                   --'IF A MEAL BREAK EXISTS THERE MAY BE MORE THAN ONE ON THE DAY SO STORE THE CONTENTS
					set @b_MealBreakExists = 1;
					
					--a_MBreak(0, l_BlockCount) = s_CurrentStartTime
					--a_MBreak(1, l_BlockCount) = s_CurrentEndTime
					--a_MBreak(2, l_BlockCount) = d_ContinuousTime
					insert into @a_MBreak values(@i_StartTime,@s_CurrentStartTime, @s_CurrentEndTime, @d_ContinuousTime)
					set @l_MealCount = @l_MealCount + 1;
            End 
			--b_FirstShiftOfTimeBlock = Not (s_LastEndTime >= s_CurrentStartTime)
			if  (@s_LastEndTime >= @s_CurrentStartTime)
				set @b_FirstShiftOfTimeBlock=0;
			else
				set @b_FirstShiftOfTimeBlock=1;

				If  (@b_FirstShiftOfTimeBlock =0)

            -- 'IF THE START TIME OF THE SHIFT IS LESS THAN OR EQUAL TO THE END TIME OF THE PREVIOUS SHIFT THERE IS NO GAP
            --'ACCUMULATE THE CONTINOUS TIME SO FAR
            If (@s_LastEndTime = @s_CurrentStartTime ) begin
                set @d_ContinuousTime = @d_ContinuousTime + @l_SvcMinutes;
                set @s_EndTimeOfBlock = @s_CurrentEndTime;
                set @b_FirstShiftOfTimeBlock = 0;
            End                                                     
            Else begin				                            
                                                    
                --  'IF THERE IS A GAP, SO BROKEN SHIFT AND FOR MEAL CALC PURPOSE AT LEAST 1 BLOCK OF CONTINUOUS TIME.
                --  'SO FLAG BROKEN SHIFT CONDITION AND CONTINUE MEAL BREAK CHECKING
                                                    
                --  'CONTINUOUS TIME WILL JUST BE THE DURATION OF THE SHIFT???
                If (@d_ContinuousTime > @d_MealBreakAfter) begin
                    --'THERE MAY BE MULTIPLE BLOCKS PER DAY
                    --'IF THE CURRENT BLOCK EXCEEDS MEAL BREAK RULE STORE THE BLOCK DETAILS FOR LATER CHECKING
                    --'INCREMENT THE BLOCK COUNTER
                    --'SET BREAKREQUIRED TRUE
					--a_Block(i_StartTime, l_BlockCount) = s_StartTimeOfBlock
					--a_Block(1, l_BlockCount) = s_EndTimeOfBlock
                    --a_Block(2, l_BlockCount) = d_ContinuousTime

                    insert into @a_Block values(@i_StartTime, @s_StartTimeOfBlock) 
					insert into @a_Block values(1, @s_EndTimeOfBlock) 	
					insert into @a_Block values(2, @d_ContinuousTime) 	
                       
                    set @s_StartTimeOfBlock = @s_CurrentStartTime;
                    set @l_BlockCount = @l_BlockCount + 1;
                    set @b_BreakRequired = 1;
                End 
                            
                  --  '---+-------------------------------------------------------------------------
                  --  '   |MINIMUM ENGAGEMENT AND BROKEN SHIFT CHECK
				  --  '   +-------------------------------------------------------------------------
                  --     'IF WE ARE IN THIS BLOCK OF CODE BY DEFINITION IT IS A BROKEN SHIFT
                  --     'BUT BROKEN SHIFT DOES NOT APPLY IF START TIME OF THIS SERVICE IS WITHIN
                  --     'MINIMUM ENGAGEMENT PERIOD OF THE PREVIOUS SHIFT
				

                    If (@b_MinEngagementLastShift=1 And DateDiff(MINUTE, @s_LastStartTime, @s_CurrentStartTime) < @d_MinEngagement )
                        set @b_BrokenShift = 0;
                    Else begin
                        set @b_BrokenShift = 1;
                        set @b_BrokenShifts = 1;
                    End 
                                                        
                    If (@d_ContinuousTime + @l_SvcMinutes >= @d_MinEngagement) begin
                        set @b_BelowMinEngagement = 0;
                        set @b_MinEngagementLastShift = 0;
					End
                    Else begin
                        set @b_BelowMinEngagement = 1;
                        set @b_BelowMinEngagements = 1
                        set @b_MinEngagementLastShift = 1;
                     end
                                                        
                    --'   +-------------------------------------------------------------------------
                    --'   |MIN ENGAGEMENT CHECK
                    --'---+-------------------------------------------------------------------------
                                                    
                --'RESET THE ACCUMULATED CONTINUOUS TIME
                                                
                    If  (@@FETCH_STATUS = 0 ) begin
                        set @d_ContinuousTime = 0
                        set @b_BelowMinEngagement = 0
                        set @b_BrokenShift = 0
                    End 
                                                
                                                
				End --Else End
				    
                If (@s_CurrentEndTime > @s_LastEndTime) set @s_LastEndTime = @s_CurrentEndTime;
                If (@s_CurrentStartTime > @s_LastStartTime)  set @s_LastStartTime = @s_CurrentStartTime;
                                       
			FETCH NEXT FROM rsDWH INTO @s_CurrentStartTime,@l_SvcMinutes,@s_CurrentEndTime,@s_ShiftType;
	
		END;
	CLOSE rsDWH  
    DEALLOCATE rsDWH
		  --'PROCESS LAST RECORD
--                                         'd_ContinuousTime = d_ContinuousTime + l_SvcMinutes

            If (@d_ContinuousTime > @d_MealBreakAfter And  @b_MealBreakExists=0 ) begin
				--'IF THE CURRENT BLOCK EXCEEDS MEAL BREAK RULE STORE THE BLOCK DETAILS FOR LATER CHECKING
				--'INCREMENT THE BLOCK COUNTER
				--'SET BREAKREQUIRED TRUE
				--a_Block(i_StartTime, l_BlockCount) = s_StartTimeOfBlock
				--a_Block(1, l_BlockCount) = s_EndTimeOfBlock
				--a_Block(2, l_BlockCount) = d_ContinuousTime

				update @a_Block set  l_BlockCount=@s_StartTimeOfBlock where i_StartTime=@i_StartTime;
				update @a_Block set  l_BlockCount=@s_EndTimeOfBlock where i_StartTime=1;
				update @a_Block set  l_BlockCount=@d_ContinuousTime where i_StartTime=2;


				set @l_BlockCount = @l_BlockCount + 1;
				set @b_BreakRequired = 1;
			End 

            If  (@RecordCount = 1 ) begin
				if (@d_ContinuousTime > @d_MealBreakAfter)
					set @b_BreakRequired = 1;
				else
					set @b_BreakRequired = 0;

                set @b_BrokenShift = 0;
                set @b_BrokenShifts = 0;
            End 
                                            
            --'RESET THE ACCUMULATED CONTINOUS TIME
            If (@d_ContinuousTime > 0 And @d_ContinuousTime < @d_MinEngagement ) begin
                set @b_BelowMinEngagements = 1;
            End 

            set @d_ContinuousTime = 0;
                                            
            If (@b_BreakRequired=1 And @b_MealBreakExists = 1 And @l_BlockCount > 0 ) begin

                --'IF THERE IS 1 OR MORE MEAL BREAKS ON THE DAY
                --'AND THERE IS 1 OR MORE BLOCKS OF CONTINUOUS TIME
                --'THEN WE NEED TO CHECK EACH BLOCK TO SEE IF ONE OR MORE OF THE MEAL BREAKS SATISFIES THE REQUIREMENTS
               declare @l_BlockCtr int;
			   set @l_BlockCtr=0;
			   Declare @l_BreakCtr int;
			   set @l_BreakCtr=0;
			   WHILE @l_BlockCtr < @l_BlockCount 
				BEGIN
				  WHILE @l_BreakCtr <@l_MealCount
				  begin
					 --  If  (i_StartTime, l_BreakCtr) > Format$(DateAdd("n", d_MealBreakAfter, a_Block(i_StartTime, l_BlockCtr)), "hh:mm") Then
						--		GoTo MealBreakMissing
						--End 
					  set @l_BreakCtr=@l_BreakCtr+1
				  
				  End
				  set @l_BlockCtr=@l_BlockCtr+1;
				END
				--For l_BlockCtr = 0 To l_BlockCount - 1
    --                For l_BreakCtr = 0 To l_MealCount

    --                    If a_MBreak(i_StartTime, l_BreakCtr) > Format$(DateAdd("n", d_MealBreakAfter, a_Block(i_StartTime, l_BlockCtr)), "hh:mm") Then
    --                        GoTo MealBreakMissing
    --                    End If
                                                        
    --                Next
    --            Next
                                                
                set @b_BreakRequired = 0;
--MealBreakMissing:
            --End 
                                            
        End -- 'NOT .EOF

 --------==========================================================================
  ---END NEW DAILY HOURS CODE
    
     --    set @s1 = 'SELECT ISNULL(NoChangeDate, 0) AS NoChangeDate, ISNULL(NoChangeTime, 0) AS NoChangeTime, ISNULL(TimeChangeLimit, '') AS TimeChangeLimit,   ISNULL(StartTimeLimit, '') AS StartTimeLimit, ISNULL(EndTimeLimit, '') AS EndTimeLimit,          ISNULL(NoMonday, 0) AS NoMonday, ISNULL(NoTuesday, 0)AS NoTuesday, ISNULL(NoWednesday, 0) AS NoWednesday,  ISNULL(NoThursday, 0) AS NoThursday, ISNULL(NoFriday, 0) AS NoFriday, ISNULL(NoSaturday, 0)AS NoSaturday,   ISNULL(NoSunday, 0) AS NoSunday , ISNULL(NoPubHol, 0) AS NoPubHol 
					--FROM ItemTypes WHERE Title = @sActivity AND ProcessClassification IN (''OUTPUT'', ''EVENT'')';

      if  (@sStaffCode ='!MULTIPLE' OR @sStaffCode='!INTERNAL' or  @sStaffCode='BOOKED' or @sStaffCode='') 
				set @sPHR = ''
		 Else Begin
				Select @sPHR=PublicHolidayRegion, @sStaffCat=Category from Staff where AccountNo =  @sStaffCode ;
                 
                 If (@sPHR <> '') 
                  set   @sPHR = ' AND (PUBLICHOLIDAYREGION = @sPHR OR ISNULL(PUBLICHOLIDAYREGION, '') = '''')'
                   
        End 
    
         set @s2 = 'PUBLICHOLIDAYS WHERE [DATE]=convert (varchar,@sDate, 111)' + @sPHR;    
         set @s3 = 'SELECT Date, [Start Time], Duration FROM Roster WHERE RecordNo = ' + @sRORecordno;
            

         select @sAwardGroup = JobType from ItemTypes where  Title=@sActivity ;
		 select   @bAutoBreak = ISNULL(AutoRosterBreak, 0) from Registration;
    
--        '   +-------------------------------------------------------------------------
--        '   |END DECLARATIONS AND INITIALISATIONS
--        '---+-------------------------------------------------------------------------
  

--184     bNoBreachedRosterRules = True

--        '===+======================================================================================================================================
--        '   |**START** CHECK FOR EXCLUSIONS
--        '   +=====================================================================================================================================
            
            --If StaffExcluded(sStaffCode, ''' & sClientCode & ''', ''' & sProgram & ''', '', ''' & sActivity & ''', '') Then
            --    set @Msg = 'Staff cannot be used with this client due to Exclusions'
            --End If
		Declare @s_AllUp varchar(500)
        Declare @s_ProgramIDs  varchar(100)
        Declare @s_ServiceIDs  varchar(100)
        Declare @s_LocationIDs varchar(100)
        Declare @s_ClientIDs  varchar(100)
        
		 If (@sClientCode <> '')  Select @s_ClientIDs=UniqueID FROM Recipients WHERE AccountNo IN ( @sClientCode );
         If (@sActivity <> '') Select @s_ServiceIDs = Recnum FROM ItemTypes WHERE Title IN (@sActivity );
         If (@sProgram <> '')	Select @s_ProgramIDs = RecordNumber FROM HumanResourceTypes WHERE [Name] IN (@sProgram );
       --  If (@sLocations <> '') Select s_LocationIDs = RecordNumber FROM CSTDAOutlets WHERE [Name] IN (@sLocations);
        
        
        set @s_AllUp = ''''
        If (@s_ClientIDs <> '' ) Set @s_AllUp = @s_AllUp + ''',''' + @s_ClientIDs;
        If (@s_ProgramIDs <> '') Set @s_AllUp = @s_AllUp + ''',''' + @s_ProgramIDs;
        If (@s_ServiceIDs <> '') Set @s_AllUp = @s_AllUp + +''',''' + @s_ServiceIDs;
      --  If (@s_LocationIDs <> '') Set @s_AllUp = @s_AllUp + ',' + @s_LocationIDs;
      --  set @s_AllUp = @s_AllUp + ')';
		--set  @s_AllUp = Replace(@s_AllUp, '(,', '(');

			If exists (select RecordNumber from HumanResources where PersonID IN ( @s_AllUp)  AND [Type] IN ('PROG_STAFFX','CENTRE_STAFFX', 'EXCLUDEDSTAFF') AND [Name] = @sStaffCode)
			Begin  set @Msg = 'Staff cannot be used with this client due to Exclusions'  ; set @ErrorValue=1; Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError return End;
  ;

--        '   +-------------------------------------------------------------------------
--        '   |END CHECK FOR EXCLUSIONS
--        '---+-------------------------------------------------------------------------
    

		 If (@sStaffCat = 'BROKERAGE ORGANISATION' Or @sStaffCat = 'SUNDRY BROKERAGE SUPPLIER' )
		 begin
			set @ErrorValue=3;
			set @msg='Invalid Staff, it is ' + @sStaffCat;
			Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError
			return;
		 End
--        '===+======================================================================================================================================
--        '   |**START** CHECK FOR CONFLICTS
--        '   +=====================================================================================================================================
   
    
         set @iYear = year(@sDate);
         set @iMonth = Month(@sDate)
         set @iDay = Day(@sDate)
         set @iStartBlock = dbo.GetBlockNo(@sStartTime)
         set @iEndBlock = dbo.GetBlockNo(@sEndTime) - 1
		 Declare @ConflitExists bit;
		

         If  (@sStaffCode <> '!MULTIPLE' And @sStaffCode <> '!INTERNAL' And @sStaffCode <> 'BOOKED' )
		 Begin
			 set @ConflitExists=0;
			select @ConflitExists=ConflictExist,@bDirect=bDirect, @bIndirect=bIndirect, @sConflictRecord=RecordNo, @sConflictClient=ClientCode, @sConflictStart=StartTime, @sConflictDuration=Duration,@sConflictActivity=ServiceType,  @sConflictSetting=ServiceSetting from dbo.ThereAreConflicts(@sStaffCode, @iYear, @iMonth, @iDay, @iStartBlock, @iEndBlock, '0', @sConflictClient, @sConflictStart, @sConflictDuration, @bDirect, @bIndirect, @sConflictActivity, '', @sExcludeRecords, @sConflictSetting, @sConflictRecord);
             If (@ConflitExists=1)
             begin
			     
				  If( @sConflictRecord = @sRORecordno) begin 
					set @ErrorValue=0;
					set @msg='Invalid Staff, it is ' + @sStaffCat;
					Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError;
					return;
				 End
                 If (not exists(Select Category  from Staff where Accountno = @sStaffCode and Category='BROKERAGE ORGANISATION') )
                  Begin   
					 If exists(select ExcludeFromConflicts from ItemTypes where  Title = @sActivity and ExcludeFromConflicts =0) 
					 Begin
                         If (@bDirect=1)
						 Begin
                             set @sStatusMsg = 'OVERLAP'
                             If (@bDisallowConflicts=0)
							   Begin
                                   Set @sConflictClient= Case 
                                   When @sConflictClient='!INTERNAL' then  ' for Administration'
                                   When @sConflictClient='!MULTIPLE' then ' in/at ' + @sConflictSetting
                                   Else  ' with ' + @sConflictClient  End 
                                   Set @ErrorValue = 4

                                 If  (@bSuppressErrorMessages=0) Begin
                                     set @msg= @sStaffCode + ' is already rostered or unavailable at that time for ' + @sConflictDuration + ' min of ' + @sConflictActivity + @sConflictClient + '. You are not authorised to roster conflicting times';
                                     set @ErrorValue=4;									
									 Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
									 return;
                                 End
                              End
							  Else Begin
									Set @sConflictClient = Case 
                                    When @sConflictClient='!INTERNAL' then  ' for Administration'
									When @sConflictClient='!MULTIPLE' then ' in/at ' + @sConflictSetting
                                    Else ' with ' + @sConflictClient  End 
                                 
                                     set  @msg=@sStaffCode + ' is already rostered or unavailable at that time for ' + @sConflictDuration + ' min of ' + @sConflictActivity + @sConflictClient + '. Are you sure you want to roster this conflicting shift';
									 set @ErrorValue=5;									
									 Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
									 return;
                               End --Else End
                            
						End -- If (@bDirect=1)
					End --- If exists(select ExcludeFromConflicts
				End -- If not Exists
			End --dbo.ThereAreConflicts(
		End -- (@sStaffCode <> '!MULTIPLE'
--        '   +======================================================================================================================================
--        '   |**END** CHECK FOR CONFLICTS
--        '===+=====================================================================================================================================
    
    
--        '===+======================================================================================================================================
--        '   |**START** CHECK ACTIVITY LIMIT COMPLIANCE IF ENABLED
--        '   +======================================================================================================================================
--            'MsgBox 'debug0'
		Declare  @NoChangeDate bit;
		Declare  @NoChangeTime bit;
		Declare  @TimeChangeLimit varchar(5)
		Declare  @StartTimeLimit varchar(5)
		Declare  @EndTimeLimit varchar(5); 
		Declare  @NoMonday bit
		Declare  @NoTuesday bit
		Declare  @NoWednesday bit
		Declare  @NoThursday bit
		Declare  @NoFriday bit
		Declare  @NoSaturday bit
		Declare  @NoSunday bit
		Declare  @NoPubHol bit;
		Declare @rRecDate varchar(20)
		Declare @rRecStarttime varchar(10)
		Declare @rRecDurtion int;
	
	    If (@bEnforceActivityLimits=1 And @sActivity <> '' And CHARINDEX(@sActivity, '|') = 0 )
		Begin
		
			SELECT @NoChangeDate=ISNULL(NoChangeDate, 0), @NoChangeTime=ISNULL(NoChangeTime, 0), @TimeChangeLimit=ISNULL(TimeChangeLimit, ''),   @StartTimeLimit=ISNULL(StartTimeLimit, ''), @EndTimeLimit=ISNULL(EndTimeLimit, ''), 
				   @NoMonday=ISNULL(NoMonday, 0), @NoTuesday=ISNULL(NoTuesday, 0), @NoWednesday=ISNULL(NoWednesday, 0),  @NoThursday=ISNULL(NoThursday, 0), @NoFriday=ISNULL(NoFriday, 0), @NoSaturday=ISNULL(NoSaturday, 0),@NoSunday=ISNULL(NoSunday, 0) , @NoPubHol=ISNULL(NoPubHol, 0) 
			FROM ItemTypes WHERE Title = @sActivity AND ProcessClassification IN ('OUTPUT', 'EVENT')

				
			If (@NoChangeDate is null)
			begin 
				set @msg='Activity no longer exists in Master File';
				set @ErrorValue=6;									
		 		Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
				return;
			End

                 Set @bPublicHoliday = dbo.IsPublicHoliday1(@sDate, @sStaffCode)
                 If (@bPublicHoliday=1)
				  Begin
                   
                    If (@NoPubHol=0) 
					 begin 
						set @msg=@sDate + ' is a Public Holiday and this service (' + @sActivity + ') has been set to not allow rostering on Public Holidays'; 
						set @ErrorValue=7;									
						Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
						return;
					 end
                End
				Else	
				Begin
                     set @sWeekday = Upper(DateName(WEEKDAY,@sDate))
                       Declare @NoDayStatus  bit;
						Set @NoDayStatus=Case 
							When @sWeekday='MONDAY'		then   @NoMonday
							When @sWeekday='TUESDAY'	then   @NoTuesday
							When @sWeekday='WEDNESDAY'	then   @NoWednesday
							When @sWeekday='THURSDAY'	then   @NoThursday
							When @sWeekday='FRIDAY'		then   @NoFriday
							When @sWeekday='SATURDAY'	then   @NoSaturday
							When @sWeekday='SUNDAY'		then   @NoSunday
						End

						 If  (@NoDayStatus=1) begin
							set @msg='This service (' + @sActivity +') has been set to not allow rostering on ' + @sWeekday;
							set @ErrorValue=7;		
							set @continue=1;							
							Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
							return;
						end
                 End 
                
                 If (isnull(@StartTimeLimit,'') <> '' And  @sStartTime <= @StartTimeLimit)  
				 Begin
					set @ErrorValue = 8;
					set @continue=1;
                    set @msg= 'This service (' + @sActivity + ') has been set to not allow rostering before ' + @StartTimeLimit;
					Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
					return;
				 End
                 If (isnull(@EndTimeLimit,'') <> '' and @sEndTime >= @EndTimeLimit) 
				 Begin
					set @ErrorValue = 9;
					set @continue=1;
                    set @msg= 'This service (' + @sActivity + ') has been set to not allow rostering after ' + @EndTimeLimit;
					Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
					return;
				 End
                 
                    if   ( @sMode='EDIT' )
					Begin
						 If (isnull(@sRORecordno,'') = '') begin  
							set @ErrorValue=10 ; 
							set @msg='Invalid Source Roster Record'; 
							Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
							return;
						End
						 If (@NoChangeDate=0 Or @NoChangeTime=0 Or isnull(@TimeChangeLimit,'') = '' )
						 Begin
							 SELECT @rRecDate=Date, @rRecStarttime=[Start Time], @rRecDurtion=Duration FROM Roster WHERE RecordNo = @sRORecordno;
							 
							 If (@rRecDate is null ) 
							 Begin
								set @ErrorValue=11 ;
								Set @msg='Invalid Record Date' ;
								Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
								return;
							 End
							 If (@NoChangeDate=1 and isnull(@rRecDate,'') = isnull(@sDate,''))  
							 Begin
								set @ErrorValue=12 ;
								set @msg='This service (' + @sActivity + ') has been set to not allow a change of date';
								Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
								return;
							 End
							 If (@NoChangeTime=1 and isnull(@rRecDate,'') = isnull(@sDate,''))  
							 Begin
								set @ErrorValue=13 ;
								set @msg='This service (' + @sActivity + ') has been set to not allow a change of time ';
								Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
								return;
							End
							 Else Begin
								
								If (Convert(int,@TimeChangeLimit) > 0 and Abs(DateDiff(MINUTE,@rRecStartTime, @sStartTime)) <= @TimeChangeLimit) 
								 set @ErrorValue=14 ;
								 set @Msg= 'This service (' + @sActivity + ') has been set to not allow a change of time greater than ' + @TimeChangeLimit;
								 Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
								return;
							 End 

							
						End       
                
					End 
--				End
		End  -- (@bEnforceActivityLimits=1 And @sActivity <> '' And CHARINDEX(@sActivity, '|') = 0 )

--        '   +-------------------------------------------------------------------------
--        '   |**END** CHECK ACTIVITY LIMIT COMPLIANCE IF ENABLED
--        '---+-------------------------------------------------------------------------
    
--        '===+======================================================================================================================================
--        '   |**START** CHECK CLIENT BUDGETS IF ENABLED
--        '   +======================================================================================================================================
         If (@bEnforceActivityLimits=1 
                And @sActivity <> '' 
                And @sClientCode <> '' 
                And @sClientCode <> '!INTERNAL' 
                And @sClientCode <> '!MULTIPLE' 
                And @sProgram <> '') 
		Begin
                Declare @bBudgetWarning bit
                Declare @bHardBudget bit
				
			
               If not exists(SELECT so.BudgetType, so.BudgetAmount, so.BudgetLimitType, so.BudgetStartDate FROM ServiceOverview so INNER JOIN Recipients re on personid = uniqueid 
                          WHERE Accountno = @sClientCode AND ServiceProgram = @sProgram  AND [Service Type] = @sActivity  ) -- Not CheckClientServiceBudgets(sClientCode, sProgram, sActivity, bBudgetWarning, bHardBudget) Then
	             Begin
					set @ErrorValue=0 ;
				 --   set @msg='Unable to check budgets'
					--set @ErrorValue=15 ;								 
					--Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
					--return;
					

				End
                Else
                 If (@bBudgetWarning=1)
				 Begin
                     If (@bHardBudget=1)
                      Begin    set @msg= 'This operation would breach the maximum allowable budget for this program for this service - you do not have authority to roster services that would exceed budgets';
							set @ErrorValue=16 ;								 
							Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
							return;
                     End
					 Else 
					 Begin
                         set @msg='This operation would breach the maximum allowable budget for this program for this service - are you sure you wish to continue';
						 set @ErrorValue=17 ;	
						 set @continue=1;							 
						 Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError 
						 return;
					END
                 End
        
		End  -- end of @bEnforceActivityLimits check
--        '   +-------------------------------------------------------------------------
--        '   |**END** CHECK CLIENT BUDGETS IF ENABLED
--        '---+-------------------------------------------------------------------------
        --If (@ErrorValue=0) return; -- Then GoTo xCancelEntry
    
    
--        '===+======================================================================================================================================
--        '   |**START** CHECK AWARD (OVERTIME, UNDERTIME, BREAKS, BROKEN SHIFTS) COMPLIANCE IF ENABLED
--        '   +=====================================================================================================================================
--332         If bUseAwards And (sStaffCode <> 'BOOKED' And sStaffCode <> '!INTERNAL' And sStaffCode <> '!MULTIPLE') Then
--                '---+-------------------------------------------------------------------------
--                '   |OVERTIME CHECK
--                '   +-------------------------------------------------------------------------
                 Declare @bNoOvertimeAccumulation bit
				 if (@bUseAwards=1 and @sStaffCode not in ('!MULTIPLE', '!INTERNAL', 'BOOKED'))
				 begin
					 If  (@bSuppressErrorMessages=0) 
					 Begin
						  if ((select top 1 NoOvertimeAccumulation from ItemTypes where Title = @sActivity ) = 1 Or 
							(select InfoOnly from  ItemTypes where Title = @sActivity ) = 1)
							 set @bNoOvertimeAccumulation=1;
					 End --If
                 
					 If (@bNoOvertimeAccumulation=0)
					 Begin
						if (@bUseAwards=1 and @sStaffCode not in ('!MULTIPLE', '!INTERNAL', 'BOOKED'))
						Begin
						  if ((select isnull(PayPeriodEndDate,'') from SysTable) <> '') 
								Select @sPPEnd=convert(varchar,PayPeriodEndDate,111)  from SysTable;
						   Else
								set @sPPEnd	 = convert (varchar,getDate(),111);
					
							If ((select isnull(DefaultPayPeriod,0) from Registration) <> 0)
								select @sPPLength=DefaultPayPeriod from Registration;
							Else
								set @sPPLength = 14;					
                        
                        
							 set @sPPStart = Convert(varchar,DateAdd(Day, (-1 * (@sPPLength - 1)), @sPPEnd), 111);
							 set @sPayStartDay = Upper(DateName(WEEKDAY,@sPPStart))  ;
							 set @bFNPP = iif(@sPPLength = 14,1,0);
            
						
							 --set @sFNStart = GetExStartOfPP(@sPPStart, @sPPLength, @sDate, @bFirstWeek)
							 Declare @diff int;
							 set @diff=(-DateDiff(day, @sPPStart, @sDate) % @sPPLength );
							 set @sFNStart = Convert(varchar,DateAdd(Day, @diff, @sDate), 111)
							 set @diff=(DateDiff(Day, @sPPStart, @sDate) % @sPPLength);
							 set @bFirstWeek = iif( @diff< 7,1,0)

							 set @sFNEnd = convert(varchar,DateAdd(Day, (@sPPLength - 1), @sFNStart), 111)
							 set @sWklyStart = IIf(@bFirstWeek=1, @sFNStart, Convert(varchar,DateAdd(Day, 7, @sFNStart), 111))
							 set @sWklyEnd = Convert(varchar,DateAdd(Day, 6, @sWklyStart),111)
                                                                    
						   -- set @s4 =
						   Declare @accountno varchar(100);
						   Declare @DayMaxOrdHr int;
						   Declare @WeekMaxOrdHr int;
						   Declare @FNMaxOrdHr int
						   Declare @TeaBreakStartAt int;					    
						   Declare @UnpaidMealBreakStartAt int;
						   Declare @MinBetweenShiftBreak int;
						   Declare @MinBreakBetweenSleepovers int;
						   Declare @MinHoursPerService int;
					 
						   Declare @PayBrokenShifts bit;
					 
								select @accountno=st.accountno, @DayMaxOrdHr=[Day_MaxOrdHr], @WeekMaxOrdHr=[Week_MaxOrdHr],
									@FNMaxOrdHr=[FN_MaxOrdHr], @TeaBreakStartAt=[TeaBreakStartAt],  @MinHoursPerService=[MinHoursPerService], 
									 @UnpaidMealBreakStartAt=[UnpaidMealBreakStartAt], 
									@MinBetweenShiftBreak=[MinBetweenShiftBreak], @MinBreakBetweenSleepovers=[MinBreakBetweenSleepovers] ,
									@PayBrokenShifts=CASE WHEN DisableBrokenShiftAllowance = 0 THEN 1 ELSE 0 END 
									from staff st 
									inner join awardpos aw on st.award = aw.code 
									where st.accountno =@sStaffCode;         
							 --set @s5 = '';
						
							 Declare @DlyHrs int;
							 Declare @WklyHrs int;
							 Declare @FntlyHrs int;
							 select @DlyHrs=sum(dailyhours), @WklyHrs=sum(weeklyhours), @FntlyHrs=sum(fnighthours) FROM
									( SELECT CASE WHEN [Date] = @sDate then round((Duration * 5) / 60, 2) else 0 end as DailyHours,  
									CASE WHEN [Date] BETWEEN @sWklyStart  AND @sWklyEnd  THEN round((Duration * 5) / 60, 2) ELSE 0 END AS WeeklyHours, 
									ROUND((Duration * 5) / 60, 2) As FnightHours 
									FROM Roster ro LEFT JOIN ItemTypes it on ro.[service type] = it.title 
									Where  [carer code] = @sStaffCode  
									AND  [Date] Between @sFNStart and @sFNEnd  
									AND [Type] NOT IN (9, 13) 
									AND ISNULL(it.InfoOnly, 0) = 0  
									AND ISNULL(it.NoOvertimeAccumulation, 0) = 0  
									) t 
								                        
	--366                         rAW.Open s4, cn, adOpenForwardOnly, adLockReadOnly
	--368                         rAC.Open s5, cn, adOpenForwardOnly, adLockReadOnly
                                                                    
	--370                         If rAW.EOF Or rAC.EOF Then GoTo xCancelEntry
                                
								set @dNewHrs = (Convert(int,@sDuration) * 5) / 60
								If ((@sOldDuration) <> '' )
								 set @dOldHrs = @sOldDuration; --TimeToDecimal
								Else
								 set @dOldHrs = 0;
                           
							  set @dHrs = @dNewHrs - @dOldHrs;
						  
							  set @dTeaBreakAfter =@TeaBreakStartAt * 60; 
							  set @bCheckTeaBreaks = iif(@dTeaBreakAfter > 0,1,0);
						
							  set @dMealBreakAfter = (@UnpaidMealBreakStartAt) * 60;
							  set @bCheckLunchBreaks = iif(@dMealBreakAfter > 0,1,0)

							  set @msg='';

							  If (@DayMaxOrdHr <> 0 And ((@DlyHrs + @dHrs) > @DayMaxOrdHr)) 
								Begin
								 set @sStatusMsg = IIf(@sStatusMsg = '', 'WKLY OVERTIME', @sStatusMsg + ', WKLY OVERTIME')
								 set @Msg = '001-This will cause (daily) OVERTIME - (exceeding daily normal hours) ' + CHAR(13) + @sStatusMsg;
							
							  End 
            
								If (@WeekMaxOrdHr <> 0 And ((@WklyHrs + @dHrs) > @WeekMaxOrdHr))
								Begin
								 set @sStatusMsg = IIf(@sStatusMsg = '', 'WKLY OVERTIME', @sStatusMsg + ',' + 'WKLY OVERTIME')
								 set @msg = @msg + '002-This will cause (weekly) OVERTIME - (exceeding weekly normal hours)' + CHAR(13) +@sStatusMsg
                            
								End 
            
								If (@FNMaxOrdHr <> 0 And ((@FntlyHrs + @dHrs) > @FNMaxOrdHr)) 
								Begin
									set @sStatusMsg = IIf(@sStatusMsg = '', 'FNLY OVERTIME', @sStatusMsg + ',' + 'FNLY OVERTIME')
									set @msg = @msg + '003-This will cause (fnightly) OVERTIME - (exceeding fortnightly normal hours)' + char(13) + @sStatusMsg;
                             
								End
                             
							  If isnull(@msg,'') <> '' 
							   Begin
								 If (@bDisallowOT =1)
								 begin
									 If ( @bSuppressErrorMessages=0) 
									 begin
									  set @msg =@msg + 'You cannot roster overtime - please rearrange the roster to avoid overtime';
									  set @ErrorValue=18;
									  return;
									End
								 End
								 Else 
								 Begin
									 If ( @bSuppressErrorMessages=1)
									 Begin
										 if(upper(@sAwardEnforcementLevel)='WARN ON BREACH')
										 Begin
                                          
											 set @msg=@msg + Char(13) + Char(13) + 'DO YOU WISH TO PROCEED??';
											 --Response = MsgBox(sAWMsg, vbCritical + vbYesNo)
											 --If Response <> vbYes Then
											 --    bNoBreachedRosterRules = False
											 --   End If
											 set @ErrorValue=19;
											 set @continue=1;
											 return;
										 End
										 Else If (upper(@sAwardEnforcementLevel)= 'PREVENT BREACH')
										 begin
											 set @msg= @msg + 'You cannot roster overtime - please rearrange the roster to avoid overtime';
											 set @ErrorValue=20;										
											 return;
										 End
                                     
									   End 
									End -- else end
							  End 
					   End
					End
				  end
--                '   +-------------------------------------------------------------------------
--                '   |END OVERTIME CHECK
--                '---+-------------------------------------------------------------------------
                
--                '---+-------------------------------------------------------------------------
--                '   |UNDERTIME CHECK
--                '   +-------------------------------------------------------------------------
                
--                    '---+-------------------------------------------------------------------------
--                    '   |MINIMUM SERVICE TIME CHECK
--                    '   +-------------------------------------------------------------------------
                                    
                
--                    '   +-------------------------------------------------------------------------
--                    '   |END MINIMUM SERVICE TIME CHECK
--                    '---+-------------------------------------------------------------------------
                
--                '   +-------------------------------------------------------------------------
--                '   |END UNDERTIME CHECK
--                '---+-------------------------------------------------------------------------
                
         --    If Not bNoBreachedRosterRules Then GoTo xCancelEntry
                --'---+-------------------------------------------------------------------------
                --'   |BREAK BETWEEN SHIFT CHECK
                --'   +-------------------------------------------------------------------------
                    Declare @bSleepover bit
                    Declare @sPreviousDay varchar(100)
                    Declare @sLastShiftEndDateTime varchar(100)
                    Declare @sNewShiftStartDateTime varchar(100)
					Declare @sLastShiftEndTime varchar(5);

					set @sPreviousDay = Convert(varchar,DateAdd(Day, -1, @sDate), 111);                 
					set @bSleepover=0;
                    If(isnull((select RecordNo from Roster where [Carer Code] = @sStaffCode AND Date = @sDate AND (BlockNo + convert(int,@sDuration)) = 288),'') <> '')
					 set @bSleepover=1;
                        
                
                    Select @sLastShiftEndDateTime=DATEADD(n, Duration * 5, [Date] + ' ' + [Start Time]) from Roster
                                                  Where  @sRORecordno <> '' 
														AND  Recordno <>  @sRORecordno    
                                                        AND [Carer Code] = @sStaffCode 
														AND [Type] IN (2,3,4,5,6,7,8,10,11,12)  
														AND Convert(datetime,[Date] + ' ' + [Start Time]) < convert(datetime, @sDate + ' ' + @sStartTime )
														ORDER BY DATE DESC;

					Set @sNewShiftStartDateTime = @sDate + ' ' + @sStartTime;
                    Set @sLastShiftEndDateTime = Convert(varchar,@sLastShiftEndDateTime, 111);
                                                
                
                 If (@sLastShiftEndTime <> '')
                 Begin
                     If (@bSleepover=1)
					 Begin
                        --'---+-------------------------------------------------------------------------
                        --'   |**START** SLEEPOVER BREAK CHECK
                        --'   +-------------------------------------------------------------------------
                         If (DateDiff(MINUTE, @sLastShiftEndDateTime, @sNewShiftStartDateTime) / 60 < @MinBreakBetweenSleepovers)
						 Begin
                             set @ErrorValue = 21
                             set @msg=@msg + '101S-This is insufficient time between shifts and will cause a penalty rate condition.' + CHAR(13) ;
                             If (@bSuppressErrorMessages=0) 
							 Begin 
								 set @msg= @msg + 'You cannot roster this - please rearrange the roster to ensure sufficient break';
								 set @sStatusMsg = 'INSUFFICIENT BREAK';
							 End
							 Select @ErrorValue,@msg,0;
							 return;
                        End 
                     End   
                        --'   +-------------------------------------------------------------------------
                        --'   |**END** SLEEPOVER BREAK CHECK
                        --'---+-------------------------------------------------------------------------
                     
					Else
					Begin
                        --'---+-------------------------------------------------------------------------
                        --'   |**START** NORMAL BREAK CHECK
                        --'   +-------------------------------------------------------------------------
                         If (Convert(varchar,@sLastShiftEndDateTime, 11) <> Convert(varchar,@sNewShiftStartDateTime, 111))
						 Begin
                             If (DateDiff(Minute, @sLastShiftEndDateTime, @sNewShiftStartDateTime) / 60 < @MinBetweenShiftBreak)
							 Begin
                                 set @ErrorValue = 22
                                 set @msg= @msg + '101-This is insufficient time between shifts and will cause a penalty rate condition.' ;
                                 If (@bSuppressErrorMessages=0)
								 begin
									set @msg=@msg + char(13) + 'You cannot roster this - please rearrange the roster to ensure sufficient break';
									set @sStatusMsg = 'INSUFFICIENT BREAK';
                                 End
							End
					     End	
                    --    '   +-------------------------------------------------------------------------
                    --    '   |**END** NORMAL BREAK CHECK
                    --    '---+-------------------------------------------------------------------------
                    End
                    --'   +-------------------------------------------------------------------------
                    --'   |**END** BREAK BETWEEN SHIFT CHECK
                    --'---+-------------------------------------------------------------------------
					End 
                --'---+-------------------------------------------------------------------------
                --'   |BROKEN SHIFT SPAN CHECK
                --'   +-------------------------------------------------------------------------
				
                --'   +-------------------------------------------------------------------------
                --'   |END BROKEN SPAN SHIFT CHECK
                --'---+-------------------------------------------------------------------------
            
--468             If Not bNoBreachedRosterRules Then GoTo xCancelEntry
                Declare @sTmpStart varchar(100)
                Declare @sTmpEnd varchar(100)
				Declare @ShiftType varchar(100);
				Declare @SvcMinutes int;
				set @sTmpEnd = @sEndTime;
				If (@sEndTime = '24:00' ) set @sTmpEnd = '23:59';
                
--                '---+-------------------------------------------------------------------------
--                '   |**START** TEA BREAK AND LUNCH BREAK CHECK
--                '   +-------------------------------------------------------------------------

--'456                 If (sAwardGroup <> 'MEAL BREAK') And (sAwardGroup <> 'TEA BREAK') Then
                    
--                        '---+-------------------------------------------------------------------------
--                        '   |**START** LUNCH BREAK CHECK
--                        '   +-------------------------------------------------------------------------
                         If (@bCheckLunchBreaks=1 And @dMealBreakAfter > 0)
                         Begin       

                              Select top 1 @sTmpStart= [Start Time] ,
							  @SvcMinutes=(Duration * 5) ,
							  @sTmpEnd=left(convert(varchar,DATEADD(Minute,duration*5,[start time]),108),5)  ,
							  @ShiftType=CASE WHEN ISNULL(it.JobType, '') = '' THEN 'WORK' ELSE it.jobtype END  
							  from Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.title 
                                    Where [date] = @sDate AND [carer code] = @sStaffCode 
									  AND ((ISNULL(it.InfoOnly, 0) = 0    OR it.JobType  IN ('TEA BREAK','MEAL BREAK')) AND Type NOT IN (9, 13)) 
								ORDER BY [Start Time] DESC;

                              If (@sTmpStart = '')
							  begin
                                 Select top 1 @sTmpStart=[Start Time] from Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.title
                                    Where [date] =@sDate  AND [carer code] = @sStaffCode  AND it.JobType NOT IN ('MEAL BREAK', 'TEA BREAK', 'GAP') AND Type <> 13 ORDER BY [Start Time];
                              End
                              If (@sTmpStart = '' And @sTmpStart < @sStartTime ) set @sTmpStart = @sStartTime

                              set @Msg = 'c-Meal Break is missing. A meal break is required after ' + @dMealBreakAfter + ' minutes.'
							   Declare @rMin int;
                             --sB = 
                                        SELECT @rMin=(ISNULL(Sum(Duration),0) * 5)  FROM Roster ro 
                                        INNER JOIN Staff st on ro.[carer code] = st.accountno 
                                        INNER JOIN AWARDPOS aw on st.award = aw.Code  
                                        INNER JOIN ItemTypes it ON it.TItle = ro.[Service Type] 
                                        WHERE  
                                                    ro.[date] = @sDate 
                                                    AND ro.[carer code] =@sStaffCode
                                                    AND ro.[type] NOT IN (9, 13) 
                                                    AND ISNULL(it.[InfoOnly], 0) = 0 
                                                    AND ro.[Start Time] BETWEEN @sTmpStart AND @sStartTime ;

                                     If (@sRORecordno <> '' And @PasteAction <> 'COPY' ) 
									 begin
									
										SELECT @rMin=(ISNULL(Sum(Duration),0) * 5)   FROM Roster ro 
                                        INNER JOIN Staff st on ro.[carer code] = st.accountno 
                                        INNER JOIN AWARDPOS aw on st.award = aw.Code  
                                        INNER JOIN ItemTypes it ON it.TItle = ro.[Service Type] 
                                        WHERE  
                                                    ro.[date] = @sDate 
                                                    AND ro.[carer code] =@sStaffCode
                                                    AND ro.[type] NOT IN (9, 13) 
                                                    AND ISNULL(it.[InfoOnly], 0) = 0 
                                                    AND ro.[Start Time] BETWEEN @sTmpStart AND @sStartTime
													AND ro.RecordNo <> @sRORecordno;
                                     end   
--490                             With rsB
--492                                 .Open sB, TraccsDb, adOpenForwardOnly, adLockReadOnly
                                  set @bBreakRequired = iif((@rMin + Convert(int,@sDuration) * 5) > @dMealBreakAfter,1,0)

                                 If ((Select MinorGroup from ItemTypes where Title = @sActivity) <> 'BREAK')
								 Begin
                                     If (@bBreakRequired=1)
                                         --bNoBreachedRosterRules = False
                                         If (@bAutoBreak=1)
                                                --'AUTOBREAK CODE
												set @bAutoBreak=1;
                                         Else
										 Begin
                                            set @sStatusMsg = IIf(@sStatusMsg = '', 'MISSING MEAL BREAK', @sStatusMsg + ',' + 'MISSING MEAL BREAK')
                                             If (@bDisallowNoBreaks=1)
											 begin
                                                 If (@bSuppressErrorMessages=0)
												 begin
                                                     set @msg=@msg + 'You do not have authority to roster this shift without a Meal Break';
                                                     set @ErrorValue=23;
                                                     Select @ErrorValue, @Msg, @continue;
													 Return;
                                                 End 
                                             End
											 Else
											 Begin
                                                 If (@bSuppressErrorMessages=0)
												 Begin
                                                     if (Upper(@sAwardEnforcementLevel)= 'WARN ON BREACH')
													 begin
                                                        set @Msg = @Msg + CHAR(13) + CHAR(13) + 'DO YOU WISH TO PROCEED??';
                                                       -- Response = MsgBox(sAWMsg, vbCritical + vbYesNo)
															set @ErrorValue=24;
														   Set @continue=1;
														   Select @ErrorValue, @Msg, @continue;
														   Return;
													  End
													  Else if (Upper(@sAwardEnforcementLevel)= 'PREVENT BREACH')
													  begin
                                                     
                                                         If (@bSuppressErrorMessages=0)
															 set @Msg =@Msg + 'You do not have authority to roster this shift without a Meal Break';
															set @ErrorValue=25;
															Set @continue=1;
															Select @ErrorValue, @Msg, @continue;
															Return;                                                     
                                                        End 
                                                    End  
                                                End 
                                            End 
                                        End 
                                 End 
                                
                        
--                        '   +-------------------------------------------------------------------------
--                        '   |**END** LUNCH BREAK CHECK
--                        '---+-------------------------------------------------------------------------
                    
--546                     If Not bNoBreachedRosterRules Then GoTo xCancelEntry
--                        '---+-------------------------------------------------------------------------
--                        '   |**START** TEA BREAK CHECK
--                        '   +-------------------------------------------------------------------------
                         If (@bCheckTeaBreaks=1 And @dTeaBreakAfter > 0)
						 Begin
                             set @Msg = '005b-Tea Break is missing. A tea break is required after ' + @dTeaBreakAfter + ' minutes.'
    
                             Select top 1 @sTmpStart=(CONVERT(Varchar(5), DATEADD(MINUTE, (Duration * 5), [Start Time]), 108)) from Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.title 
                                             Where [date] = @sDate  AND [carer code] = @sStaffCode  AND it.JobType IN ('TEA BREAK') ORDER BY [Start Time] DESC;
                             If (@sTmpStart = '')
							 begin
                                 Select top 1 @sTmpStart=[Start Time] from Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.title 
                                                 Where [date] = @sDate  AND [carer code] = @sStaffCode  AND it.JobType NOT IN ('TEA BREAK', 'GAP') AND Type <> 13 ORDER BY [Start Time];
                             End 
                             If (@sTmpStart = '' And @sTmpStart < @sStartTime )
							  set @sTmpStart = @sStartTime;

                                        SELECT @rMin=(ISNULL(Sum(Duration),0) * 5)  FROM Roster ro 
                                        INNER JOIN Staff st on ro.[carer code] = st.accountno 
                                        INNER JOIN AWARDPOS aw on st.award = aw.Code 
                                        INNER JOIN ItemTypes it ON it.TItle = ro.[Service Type] 
                                        WHERE  
                                                    ro.[date] = @sDate 
                                                    AND ro.[carer code] = @sStaffCode 
                                                    AND ro.[type] NOT IN (9, 13) 
                                                    AND ISNULL(it.[InfoOnly], 0) = 0 
                                                    AND ro.[Start Time] BETWEEN  @sTmpStart AND @sStartTime ;

                                     If (@sRORecordno <> '' And @PasteAction <> 'COPY' )
									  SELECT @rMin=(ISNULL(Sum(Duration),0) * 5)  FROM Roster ro 
                                        INNER JOIN Staff st on ro.[carer code] = st.accountno 
                                        INNER JOIN AWARDPOS aw on st.award = aw.Code 
                                        INNER JOIN ItemTypes it ON it.TItle = ro.[Service Type] 
                                        WHERE  
                                                    ro.[date] = @sDate 
                                                    AND ro.[carer code] = @sStaffCode 
                                                    AND ro.[type] NOT IN (9, 13) 
                                                    AND ISNULL(it.[InfoOnly], 0) = 0 
                                                    AND ro.[Start Time] BETWEEN  @sTmpStart AND @sStartTime 
													AND ro.RecordNo <> @sRORecordno;


--564                              With rsB
--566                                 .Open sB, TraccsDb, adOpenForwardOnly, adLockReadOnly
                                   set @bBreakRequired = IIf( (@rMin + (Convert(int,@sDuration) * 5) > @dTeaBreakAfter),1,0)

                                 If ((select top 1 MinorGroup from ItemTypes where Title =@sActivity) <> 'BREAK')
								Begin

                                     If (@bBreakRequired =1 )
									 Begin
                                         --set @bNoBreachedRosterRules = False
                                         If (@bAutoBreak =1) Begin
                                                --'AUTOBREAK CODE
												set @bAutoBreak =1;
                                         End
										 Else
										 Begin
                                             set @sStatusMsg = IIf(@sStatusMsg = '', 'MISSING TEA BREAK', @sStatusMsg + ',' + 'MISSING TEA BREAK')
                                             If (@bDisallowNoBreaks=1)
											 Begin
                                                 --bNoBreachedRosterRules = False
                                                 If (@bSuppressErrorMessages=0)
												 begin
													  set  @MSg=@MSg +'You do not have authority to roster this shift without a Tea Break';
													  set @ErrorValue=26
													  Select @ErrorValue,@Msg,@continue;
												  End
                                             End
											 Else
											 Begin
                                                 If (@bSuppressErrorMessages=1)
												 Begin
                                                     If (Upper(@sAwardEnforcementLevel)='WARN ON BREACH')
													 Begin
                                                         set @Msg = @Msg + CHAR(13) + CHAR(13) + 'DO YOU WISH TO PROCEED??'
                                                        -- Response = MsgBox(sAWMsg, vbCritical + vbYesNo)
                                                       set @ErrorValue=27;
													   set @continue=1;
													   Select @ErrorValue,@Msg,@continue;
													   
													   Return;
													End												
													Else  If (Upper(@sAwardEnforcementLevel)= 'PREVENT BREACH')
													Begin
                                                         If (@bSuppressErrorMessages=0)
														 begin
															set @MSg= @Msg+'You do not have authority to roster this shift without a Meal Break';
															set @ErrorValue=28;
															set @continue=1;
															Select @ErrorValue,@Msg,@continue;
															Return;
														End
                                                     End 
													
                                                 End 
                                              End
                                         End 
                                        
                                 
                            End 
                        --'   +-------------------------------------------------------------------------
                        --'   |**END** TEA BREAK CHECK
                        --'---+-------------------------------------------------------------------------
                    End 
                --'   +-------------------------------------------------------------------------
                --'   |**END** TEA BREAK AND LUNCH BREAK CHECK
                --'---+-------------------------------------------------------------------------
            
            End 
        --'   +=====================================================================================================================================
        --'   |**END** CHECK AWARD (OVERTIME, UNDERTIME, BREAKS, BROKEN SHIFTS) COMPLIANCE IF ENABLED
        --'===+======================================================================================================================================
    


	--===============================================================================================================================================
		set @Msg='No error';
		set @ErrorValue=0;
		set @continue=0;
		Select @ErrorValue as ErrorValue, @msg as msg,@continue as continueError

End



Go
/****** Object:  StoredProcedure [dbo].[CheckPay]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --CREATE OR ALTER procedure [dbo].[CheckPay] (@StartDate AS Varchar (10), @EndDate AS Varchar (10))  AS BEGIN       IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PErrors')       BEGIN           TRUNCATE TABLE PErrors       END INSERT INTO PErrors SELECT CreatedBy, Editer, CONVERT(VARCHAR (12), [Booking/Shift#]) AS [Shift#], '', ErrorType, [Status], 'FATAL', [Branch], [Date], Staff, Program, [Start Time]  FROM (SELECT R.[Status], CASE WHEN r.ROS_COPY_BATCH# > 0 THEN 'MASTER' ELSE Creator END AS CreatedBy, R.Editer, RECORDNO AS [Booking/Shift#], CASE WHEN ISNULL(PR.[NAME], '') = '' THEN 'Program does not exist in surrent program list'      WHEN ISNULL(PR.[ENDDATE], '') <> '' THEN 'Program is End Dated' END AS ErrorType, PROGRAM, CASE WHEN ISNULL(PR.ENDDATE,'') <> '' THEN CONVERT(VARCHAR (10), ISNULL(PR.ENDDATE, ''), 111) ELSE '' END AS ProgramEndDate, CASE WHEN r.[Type] IN (2, 8) THEN 'ONE ON ONE' WHEN r.[Type] = 5 THEN 'TRAVEL' WHEN r.[Type] = 6 THEN 'ADMIN-' + A.MINORGROUP WHEN r.[Type] = 10 THEN 'TRANSPORT' WHEN r.[Type] = 11 THEN 'CENTRE BASED' WHEN r.[Type] = 12 THEN 'GROUP' END AS 'Shift Type', CASE  WHEN [Client Code] = '!INTERNAL' THEN 'No Client' WHEN [Client Code] = '!MULTIPLE' THEN 'Multiple Clients' ELSE [Client Code] END AS Recipient, [CARER CODE] as [Staff], CATEGORY, [SERVICE TYPE], [DATE], [START TIME],  R.DURATION, S.STF_DEPARTMENT AS BRANCH, CREATOR FROM ROSTER R LEFT JOIN ITEMTYPES A ON R.[Service Type] = A.Title LEFT JOIN ItemTypes P ON R.[Service Description] = P.Title AND P.ProcessClassification = 'INPUT' LEFT JOIN HUMANRESOURCETYPES PR ON R.PROGRAM = PR.[NAME] AND PR.[GROUP] = 'PROGRAMS' INNER JOIN STAFF S ON R.[CARER CODE] = S.ACCOUNTNO  WHERE PROGRAM > '!Z'        AND ISNULL(s.ExcludeFromPayExport, 0) <> 1      AND ISNULL(P.ExcludeFromPayExport, 0) <> 1      AND ISNULL(A.InfoOnly, 0) <> 1      AND [CARER CODE] > '!Z'     AND R.[TYPE] IN (2,3,5,6,8,10,11,12)        AND R.[DATE] BETWEEN @StartDate AND @EndDate        AND (ISNULL(PR.ENDDATE, '') <> '' AND CONVERT(VARCHAR (10), ISNULL(PR.ENDDATE, ''), 111) < R.[DATE]) ) t  INSERT INTO PErrors SELECT CreatedBy, Editer, CONVERT(VARCHAR (12), [Booking/Shift#]) AS [Shift#], '', ErrorType, [Status], 'FATAL', [Branch], [Date], Staff, Program, [Start Time]  FROM ( SELECT      R.[Status],     st.STF_DEPARTMENT AS [Branch],      CASE WHEN r.ROS_COPY_BATCH# > 0 THEN 'MASTER'       ELSE Creator        END AS CreatedBy,       R.Editer,       RECORDNO AS [Booking/Shift#],   [Date], [Start Time], [service type], [carer code] AS Staff, st.ExcludeFromPayExport, Program, pr.Fax, 'Invalid Cost Centre for WageEasy' AS ErrorType FROM Roster r    INNER JOIN HumanResourceTypes PR ON R.program = pr.name and pr.[group] = 'programs' INNER JOIN STAFF st ON r.[carer code] = st.accountno    LEFT JOIN ItemTypes A ON R.[Service Type] = A.Title AND A.ProcessClassification <> 'INPUT'  LEFT JOIN ItemTypes P ON R.[Service Description] = P.Title AND P.ProcessClassification = 'INPUT' WHERE r.[Date] BETWEEN @StartDate AND @EndDate  AND LEN(Fax) < 5    AND [Service Description] NOT IN ('BROKER', 'UNAVAILABLE')  AND r.[Type] IN (2,5,6,8,10,11,12)  AND r.[carer code] > '!z'   AND ISNULL(st.ExcludeFromPayExport, 0) <> 1 AND ISNULL(P.ExcludeFromPayExport, 0) <> 1  AND ISNULL(A.InfoOnly, 0) <> 1  ) t3  INSERT INTO PErrors SELECT CreatedBy, Editer, CONVERT(VARCHAR (12), [Booking/Shift#]) AS [Shift#], '', ErrorType, [Status], 'WARNING', [Branch], [Date], Staff, Program, [Start Time]  FROM ( SELECT st.STF_DEPARTMENT AS [Branch],       R.[Status],     CASE WHEN r.ROS_COPY_BATCH# > 0 THEN 'MASTER'       ELSE Creator        END AS CreatedBy,       R.Editer,       RECORDNO AS [Booking/Shift#],       [Service Description] AS PayType,   [Date], [Start Time], [service type], [carer code] AS Staff, st.ExcludeFromPayExport, Program, pr.Fax, 'Invalid Paytype for this staff-' + [Service Description] + '-AWARD is ' + st.Award AS ErrorType FROM Roster r   INNER JOIN HumanResourceTypes PR ON R.program = pr.name and pr.[group] = 'programs' INNER JOIN STAFF st ON r.[carer code] = st.accountno    LEFT JOIN ItemTypes A ON R.[Service Type] = A.Title AND A.ProcessClassification <> 'INPUT'   LEFT JOIN ItemTypes P ON R.[Service Description] = P.Title AND P.ProcessClassification = 'INPUT'    WHERE       r.[Date] BETWEEN @StartDate AND @EndDate    AND st.Award <> Left([Service Description], Len(st.Award))  AND [Service Description] NOT IN ('BROKER', 'UNAVAILABLE')  AND r.[Type] IN (2,5,6,8,10,11,12)  AND r.[carer code] > '!z'   AND ISNULL(st.ExcludeFromPayExport, 0) <> 1 AND ISNULL(P.ExcludeFromPayExport, 0) <> 1  AND ISNULL(A.InfoOnly, 0) <> 1  ) t4 INSERT INTO PErrors SELECT CreatedBy, Editer, CONVERT(VARCHAR (12), [Booking/Shift#]) AS [Shift#], '', ErrorType, [Status], 'FATAL', [Branch], [Date], Staff, Program, [Start Time]  FROM ( SELECT  R.[Status], A.InfoOnly,     P.ExcludeFromPayExport, CASE WHEN r.ROS_COPY_BATCH# > 0 THEN 'MASTER'   ELSE Creator    END AS CreatedBy,   R.Editer,   RECORDNO AS [Booking/Shift#],   'Overlap' AS ErrorType, R.[Service Description] AS [PayType],   PROGRAM,    CASE    WHEN r.[Type] IN (2, 8) THEN 'ONE ON ONE'   WHEN r.[Type] = 5 THEN 'TRAVEL' WHEN r.[Type] = 6 THEN 'ADMIN-' + A.MINORGROUP  WHEN r.[Type] = 10 THEN 'TRANSPORT' WHEN r.[Type] = 11 THEN 'CENTRE BASED'  WHEN r.[Type] = 12 THEN 'GROUP' END AS 'Shift Type',    CASE    WHEN [Client Code] = '!INTERNAL' THEN 'No Client'   WHEN [Client Code] = '!MULTIPLE' THEN 'Multiple Clients'    ELSE [Client Code]  END AS Recipient, [CARER CODE] as [Staff], CATEGORY, [SERVICE TYPE],  [DATE], [START TIME],  BLOCKNO, R.DURATION, ST.STF_DEPARTMENT AS BRANCH, CREATOR    FROM ROSTER r   INNER JOIN STAFF st ON r.[carer code] = st.accountno    LEFT JOIN ItemTypes A ON R.[Service Type] = A.Title AND A.ProcessClassification <> 'INPUT'  LEFT JOIN ItemTypes P ON R.[Service Description] = P.Title AND P.ProcessClassification = 'INPUT'    WHERE   EXISTS (    SELECT RecordNo FROM ROSTER ro2  INNER JOIN STAFF st ON r.[carer code] = st.accountno    LEFT JOIN ItemTypes A2 ON Ro2.[Service Type] = A2.Title AND A2.ProcessClassification <> 'INPUT' LEFT JOIN ItemTypes P2 ON Ro2.[Service Description] = P2.Title AND P2.ProcessClassification = 'INPUT'   Where r.[CARER CODE] = ro2.[CARER CODE] AND r.RECORDNO <> ro2.RECORDNO  AND r.[DATE] = ro2.[DATE]   AND r.BLOCKNO > ro2.BLOCKNO     AND r.BLOCKNO < ro2.BLOCKNO+ro2.DURATION    AND [Service Description] NOT IN ('BROKER') AND r.[Type] IN (2,5,6,8,10,11,12)  AND ISNULL(P2.ExcludeFromPayExport, 0) = 0  AND ISNULL(A2.InfoOnly, 0) = 0  )   AND [Service Description] NOT IN ('BROKER', 'UNAVAILABLE')   AND r.[Type] IN (2,5,6,8,10,11,12)  AND r.[carer code] > '!z'   AND r.[Date] BETWEEN @StartDate AND @EndDate    AND ISNULL(st.ExcludeFromPayExport, 0) <> 1 AND ISNULL(P.ExcludeFromPayExport, 0) <> 1  AND ISNULL(A.InfoOnly, 0) <> 1  ) t1  END
GO
/****** Object:  StoredProcedure [dbo].[ClonePackage]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[ClonePackage]@SourcePackage NVARCHAR(MAX),   @NewPackage NVARCHAR(MAX)	

--@Type NVARCHAR(MAX),			
--@Level NVARCHAR(MAX)			
AS	

--DECLARE @SourcePackage varchar(50)
--DECLARE @NewPackage varchar(50) ;
--SET @NewPackage = 'TESTER005'
--SET  @SourcePackage = 'ALL SERVICES'

DECLARE @SourcePackageID int ;
DECLARE @NewPackageID int 


SET NOCOUNT ON;
SET @SourcePackageID = (SELECT RecordNumber FROM HumanResourceTypes WHERE [Name] = @SourcePackage)

--CREATE OR ALTER BASE RECORD
INSERT INTO [dbo].[HumanResourceTypes] (            [Group]
           ,[Type]
           ,[Name]
           ,[Address1]
           ,[Address2]
           ,[Suburb]
           ,[Postcode]
           ,[Phone1]
           ,[Phone2]
           ,[Fax]
           ,[Mobile]
           ,[EMail]
           ,[Notes]
           ,[Date1]
           ,[Date2]
           ,[GST]
           ,[GSTRate]
           ,[HRT_DATASET]
           ,[BudgetAmount]
           ,[BudgetPeriod]
           ,[BUDGET_1]
           ,[BUDGET_2]
           ,[BUDGET_3]
           ,[BUDGET_4]
           ,[BUDGET_5]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[EndDate]
           ,[USER1]
           ,[User2]
           ,[UserYesNo1]
           ,[User3]
           ,[UserYesNo2]
           ,[User4]
           ,[User5]
           ,[UserYesNo3]
           ,[UserYesNo4]
           ,[UserYesNo5]
           ,[User6]
           ,[User7]
           ,[User8]
           ,[User9]
           ,[User10]
           ,[User11]
           ,[User12]
           ,[P_Def_Alert_Type]
           ,[P_Def_Alert_BaseOn]
           ,[P_Def_Alert_Period]
           ,[P_Def_Alert_Allowed]
           ,[P_Def_Alert_Yellow]
           ,[P_Def_Alert_Orange]
           ,[P_Def_Alert_Red]
           ,[P_Def_Expire_Amount]
           ,[P_Def_Expire_CostType]
           ,[P_Def_Expire_Unit]
           ,[P_Def_Expire_Period]
           ,[P_Def_Expire_Length]
           ,[P_Def_Fee_BasicCare]
           ,[P_Def_Fee_IncomeTested]
           ,[P_Def_Fee_TopUp]
           ,[P_Def_Contingency_PercAmt]
           ,[P_Def_Admin_AdminType]
           ,[P_Def_Admin_Admin_PercAmt]
           ,[P_Def_Admin_CMType]
           ,[P_Def_Admin_CM_PercAmt]
           ,[P_Def_StdDisclaimer]
           ,[P_Def_Admin_AdminFrequency]
           ,[P_Def_Admin_CMFrequency]
           ,[P_Def_Admin_AdminDay]
           ,[P_Def_Admin_CMDay]
           ,[P_Def_Expire_Using]
           ,[P_Def_Contingency_Max]
           ,[P_Def_QueryAutoDeleteAdmin]
           ,[P_Def_IncludeIncomeTestedFeeInAdmin]
           ,[DefaultCHGTravelWithinActivity]
           ,[DefaultCHGTravelWithinPayType]
           ,[DefaultNCTravelWithinProgram]
           ,[DefaultNCTravelWithinActivity]
           ,[DefaultNCTravelWithinPayType]
           ,[DefaultCHGTravelBetweenActivity]
           ,[DefaultCHGTravelBetweenPayType]
           ,[DefaultNCTravelBetweenProgram]
           ,[DefaultNCTravelBetweenActivity]
           ,[DefaultNCTravelBetweenPayType]
           ,[P_Def_IncludeClientFeesInCont]
           ,[P_Def_IncludeBasicCareFeeInAdmin]
           ,[P_Def_IncludeTopUpFeeInAdmin]
           ,[User13]
           ,[CloseDate]
           ,[NoNoticeLeadTime]
           ,[ShortNoticeLeadTime]
           ,[NoNoticeLeaveActivity]
           ,[ShortNoticeLeaveActivity]
           ,[WithNoticeLeaveActivity]
           ,[UserYesNo6]
           ,[DefaultNoNoticeCancel]
           ,[DefaultNoNoticeBillProgram]
           ,[DefaultNoNoticePayProgram]
           ,[DefaultNoNoticePaytype]
           ,[DefaultShortNoticeCancel]
           ,[DefaultShortNoticeBillProgram]
           ,[DefaultShortNoticePayProgram]
           ,[DefaultShortNoticePaytype]
           ,[DefaultWithNoticeCancel]
           ,[NoNoticeCancelRate]
           ,[ShortNoticeCancelRate]
           ,[DefaultWithNoticeProgram]
           ,[CDCStatementText1]
           ,[DefaultCHGTravelWithinProgram]
           ,[DefaultCHGTravelBetweenProgram]
           ,[DefaultDailyFee]
           ,[BudgetEnforcement]
           ,[BudgetRosterEnforcement]
           ,[xDeletedRecord]
		   )
SELECT 
            [Group]
           ,[Type]
           ,@NewPackage
           ,[Address1]
           ,[Address2]
           ,[Suburb]
           ,[Postcode]
           ,[Phone1]
           ,[Phone2]
           ,[Fax]
           ,[Mobile]
           ,[EMail]
           ,[Notes]
           ,[Date1]
           ,[Date2]
           ,[GST]
           ,[GSTRate]
           ,[HRT_DATASET]
           ,[BudgetAmount]
           ,[BudgetPeriod]
           ,[BUDGET_1]
           ,[BUDGET_2]
           ,[BUDGET_3]
           ,[BUDGET_4]
           ,[BUDGET_5]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[EndDate]
           ,[USER1]
           ,[User2]
           ,[UserYesNo1]
           ,[User3]
           ,[UserYesNo2]
           ,[User4]
           ,[User5]
           ,0
           ,[UserYesNo4]
           ,[UserYesNo5]
           ,[User6]
           ,[User7]
           ,[User8]
           ,[User9]
           ,[User10]
           ,[User11]
           ,[User12]
           ,[P_Def_Alert_Type]
           ,[P_Def_Alert_BaseOn]
           ,[P_Def_Alert_Period]
           ,[P_Def_Alert_Allowed]
           ,[P_Def_Alert_Yellow]
           ,[P_Def_Alert_Orange]
           ,[P_Def_Alert_Red]
           ,[P_Def_Expire_Amount]
           ,[P_Def_Expire_CostType]
           ,[P_Def_Expire_Unit]
           ,[P_Def_Expire_Period]
           ,[P_Def_Expire_Length]
           ,[P_Def_Fee_BasicCare]
           ,[P_Def_Fee_IncomeTested]
           ,[P_Def_Fee_TopUp]
           ,[P_Def_Contingency_PercAmt]
           ,[P_Def_Admin_AdminType]
           ,[P_Def_Admin_Admin_PercAmt]
           ,[P_Def_Admin_CMType]
           ,[P_Def_Admin_CM_PercAmt]
           ,[P_Def_StdDisclaimer]
           ,[P_Def_Admin_AdminFrequency]
           ,[P_Def_Admin_CMFrequency]
           ,[P_Def_Admin_AdminDay]
           ,[P_Def_Admin_CMDay]
           ,[P_Def_Expire_Using]
           ,[P_Def_Contingency_Max]
           ,[P_Def_QueryAutoDeleteAdmin]
           ,[P_Def_IncludeIncomeTestedFeeInAdmin]
           ,[DefaultCHGTravelWithinActivity]
           ,[DefaultCHGTravelWithinPayType]
           ,[DefaultNCTravelWithinProgram]
           ,[DefaultNCTravelWithinActivity]
           ,[DefaultNCTravelWithinPayType]
           ,[DefaultCHGTravelBetweenActivity]
           ,[DefaultCHGTravelBetweenPayType]
           ,[DefaultNCTravelBetweenProgram]
           ,[DefaultNCTravelBetweenActivity]
           ,[DefaultNCTravelBetweenPayType]
           ,[P_Def_IncludeClientFeesInCont]
           ,[P_Def_IncludeBasicCareFeeInAdmin]
           ,[P_Def_IncludeTopUpFeeInAdmin]
           ,[User13]
           ,[CloseDate]
           ,[NoNoticeLeadTime]
           ,[ShortNoticeLeadTime]
           ,[NoNoticeLeaveActivity]
           ,[ShortNoticeLeaveActivity]
           ,[WithNoticeLeaveActivity]
           ,[UserYesNo6]
           ,[DefaultNoNoticeCancel]
           ,[DefaultNoNoticeBillProgram]
           ,[DefaultNoNoticePayProgram]
           ,[DefaultNoNoticePaytype]
           ,[DefaultShortNoticeCancel]
           ,[DefaultShortNoticeBillProgram]
           ,[DefaultShortNoticePayProgram]
           ,[DefaultShortNoticePaytype]
           ,[DefaultWithNoticeCancel]
           ,[NoNoticeCancelRate]
           ,[ShortNoticeCancelRate]
           ,[DefaultWithNoticeProgram]
           ,[CDCStatementText1]
           ,[DefaultCHGTravelWithinProgram]
           ,[DefaultCHGTravelBetweenProgram]
           ,[DefaultDailyFee]
           ,[BudgetEnforcement]
           ,[BudgetRosterEnforcement]
           ,[xDeletedRecord] FROM HumanResourceTypes WHERE [RecordNumber] = @SourcePackageID

		   SET @NewPackageID=SCOPE_IDENTITY()

		   INSERT INTO ServiceOverview 
		   SELECT
			   @NewPackageID
			  ,[Service Type]
			  ,[Cost Type]
			  ,[Frequency]
			  ,[Period]
			  ,[Duration]
			  ,[UnitType]
			  ,[Unit Pay Rate]
			  ,[LineCost]
			  ,[Unit Bill Rate]
			  ,[LineBill]
			  ,[Activity Breakdown]
			  ,[ServiceGroup]
			  ,[ServiceTravelChargeable]
			  ,[ServiceProgram]
			  ,[ServiceCategory]
			  ,[ServiceCarer]
			  ,[ServiceBiller]
			  ,[ServiceStaff]
			  ,[ServiceLocation]
			  ,[ServiceAnalysis]
			  ,[DeletedRecord]
			  ,[COID]
			  ,[BRID]
			  ,[DPID]
			  ,[ServiceStatus]
			  ,[ForceSpecialPrice]
			  ,[NoTimeChange]
			  ,[NoDayChange]
			  ,[NoDurationChange]
			  ,[TaxRate]
			  ,[AutoInsertNotes]
			  ,[ExcludeFromNDIAPriceUpdates]
			  ,[BudgetType]
			  ,[BudgetAmount]
			  ,[BudgetLimitType]
			  ,[BudgetStartDate]
			  ,[xEndDate]
		  FROM [dbo].[ServiceOverview] WHERE PersonID = Convert(varchar, @SourcePackageID)

		  INSERT INTO HumanResources

		  SELECT 
			   @NewPackageID
			  ,[Group]
			  ,[Type]
			  ,[Name]
			  ,[Address1]
			  ,[Address2]
			  ,[Suburb]
			  ,[Postcode]
			  ,[Phone1]
			  ,[Phone2]
			  ,[Fax]
			  ,[Mobile]
			  ,[EMail]
			  ,[Date1]
			  ,[Date2]
			  ,[State]
			  ,[EquipmentCode]
			  ,[SerialNumber]
			  ,[LockBoxCode]
			  ,[LockBoxLocation]
			  ,[PurchaseAmount]
			  ,[PurchaseDate]
			  ,[DateInstalled]
			  ,[DeletedRecord]
			  ,[COID]
			  ,[BRID]
			  ,[DPID]
			  ,[Notes]
			  ,[ReminderProcessed]
			  ,[Completed]
			  ,[Recurring]
			  ,[SameDay]
			  ,[SameDate]
			  ,[Creator]
			  ,[ReminderScope]
			  ,[APrimary]
			  ,[ReminderCreator]
			  ,[ReminderMode]
			  ,[User1]
			  ,[SubType]
			  ,[ImportType]
			  ,[UserYesNo1]
			  ,[User2]
			  ,[User3]
			  ,[User4]
			  ,[MobileAlert]
			  ,[User5]
			  ,[Undated]
			  ,[xEndDate]
		  FROM [dbo].[HumanResources] WHERE PersonID = Convert(varchar, @SourcePackageID)
GO
/****** Object:  StoredProcedure [dbo].[Copy_Document]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER Procedure [dbo].[Copy_Document](@SourcePath varchar(500), @DestinationPath varchar(500), @SourceFileName varchar(250), @DestinationFileName varchar(250))
		AS
		BEGIN

		DECLARE @command varchar(2000)

		EXEC sp_configure 'show advanced options', 1;  
		RECONFIGURE;  
		EXEC sp_configure 'xp_cmdshell', 1;  
		RECONFIGURE;  
		SET @command = N'Copy "' + @SourcePath + '\' + @SourceFileName + '"  "' + @DestinationPath  + '\' +@DestinationFileName + '"'; 
		--PRINT @command; 
		--SELECT @Command as Audit1
		BEGIN TRY  
		--	EXEC sp_xp_cmdshell_proxy_account 'Adamas_corporate\SVC-SQLPROXY','adamas'
			EXEC  master..xp_cmdshell @command;
		END TRY  
		BEGIN CATCH
			PRINT  ERROR_MESSAGE();
		END CATCH
		END
GO
/****** Object:  StoredProcedure [dbo].[Copy_MTA_Document]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER Procedure [dbo].[Copy_MTA_Document](@PersonID varchar(50), @Extension varchar(10), @FileName varchar(250), @DocPath varchar(500))  
as  begin  

declare @FinalPath varchar(500);


select  @DocPath=replace(@DocPath,(select  top 1 * from split(@DocPath,'\')),SharedDocumentPath) from Registration 
where SharedDocumentPath is not null; 
if ( right(@DocPath,1)='\')
begin
	set @FinalPath=@DocPath +'' + @FileName;
end
else begin
set @FinalPath=@DocPath +'\' + @FileName;
end


 declare @command nvarchar(1000); 
  DECLARE @file_stream VARBINARY(MAX); 
   set @command = N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX))                  from OPENROWSET(BULK ''' + @FinalPath + ''',                  SINGLE_BLOB) ROW_SET'  
EXEC sp_executesql @command, N'@file_stream1 VARBINARY(MAX) OUTPUT',@file_stream1 =@file_stream OUTPUT  
select @file_stream;      

  

end  
GO
/****** Object:  StoredProcedure [dbo].[Copy_mta_document_withoutshareddocument]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER   PROCEDURE [dbo].[Copy_mta_document_withoutshareddocument](@PersonID
VARCHAR(50),
                                                                @Extension
VARCHAR(10),
                                                                @FileName
VARCHAR(250),
                                                                @DocPath
VARCHAR(500))
AS
  BEGIN
      DECLARE @FinalPath VARCHAR(500);

      SET @FinalPath= @DocPath + '\\' + @FileName;

  ;
      --SELECT @FinalPath
      DECLARE @command NVARCHAR(1000);
      DECLARE @file_stream VARBINARY(max);

      SET @command =
N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX))                  from OPENROWSET(BULK '''
+ @FinalPath
+ ''',                  SINGLE_BLOB) ROW_SET'

    EXEC Sp_executesql
      @command,
      N'@file_stream1 VARBINARY(MAX) OUTPUT',
      @file_stream1 =@file_stream output

    SELECT @file_stream;
END 


SET ANSI_NULLS ON
GO
/****** Object:  StoredProcedure [dbo].[CopyAndPasteFile]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER   PROCEDURE [dbo].[CopyAndPasteFile](@FileName		   VARCHAR(250), 
                                          @SourceDocPath   VARCHAR(500),
										  @DestinationDocPath VARCHAR (500)) 
AS 

BEGIN 

	--DECLARE @FileName			VARCHAR(250);
	--DECLARE @SourceDocPath		VARCHAR(500);
	--DECLARE @DestinationDocPath VARCHAR (500);
	--SET @Filename = 'adamas meeting.docx'
	--SET @SourceDocPath = 'C:\Adamas\aaTestSourceFolder'
	--SET @DestinationDocPath = 'C:\Adamas\aaTestDestinationFolder'

	DECLARE @command					NVARCHAR(1000); 
	DECLARE @file_stream				VARBINARY(max); 
	DECLARE @init						INT;
	DECLARE @FullDestinationPathFile	NVARCHAR (500);

	SELECT  @DestinationDocPath=replace(@DestinationDocPath,(select  top 1 * from split(@DestinationDocPath,'\')),SharedDocumentPath) 
	FROM    Registration 
	WHERE   SharedDocumentPath is not null; 

	IF ( right(@DestinationDocPath,1)='\')
	BEGIN
		SET @FullDestinationPathFile=@DestinationDocPath +'' + @FileName;
	END
	ELSE BEGIN
		SET @FullDestinationPathFile=@DestinationDocPath +'\' + @FileName;
	END
	
	--SELECT @DestinationDocPath = shareddocumentpath 
	--FROM   registration 
	--WHERE  shareddocumentpath IS NOT NULL; 

	--SET @FullDestinationPathFile = @DestinationDocPath + '\' + @FileName

    SET @command = 
						N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX)) FROM OPENROWSET(BULK '''
						+ @SourceDocPath + '\' + @FileName 
						+ ''',                  SINGLE_BLOB) ROW_SET' 

	EXEC Sp_executesql @command, N'@file_stream1 VARBINARY(MAX) OUTPUT', @file_stream1 =@file_stream OUTPUT 
	
	--SELECT @file_stream, 
	SELECT @SourceDocPath, @FileName, @FullDestinationPathFile, @file_stream

	EXEC sp_OACREATE  'ADODB.Stream', @init OUTPUT;								-- An instace created
    EXEC sp_OASetProperty @init, 'Type', 1; 
	EXEC sp_OAMethod @init, 'Open';												-- Calling a method
	EXEC sp_OAMethod @init, 'Write', NULL, @file_stream;						-- Calling a method
	EXEC sp_OAMethod @init, 'SaveToFile', NULL, @FullDestinationPathFile, 2;	-- Calling a method
	EXEC sp_OAMethod @init, 'Close';											-- Calling a method
	EXEC sp_OADestroy @init;													-- Closed the resources

END 


GO
/****** Object:  StoredProcedure [dbo].[CopyAndPasteFile2]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[CopyAndPasteFile2](@FileName		   VARCHAR(250),
                                          @file_stream   VARBINARY(Max),
										  @DestinationDocPath VARCHAR (500))
AS
BEGIN

	DECLARE @command					NVARCHAR(1000);
	DECLARE @init						INT;
	DECLARE @FullDestinationPathFile	NVARCHAR (500);
	SELECT  @DestinationDocPath=replace(@DestinationDocPath,(select  top 1 * from split(@DestinationDocPath,'\')),SharedDocumentPath)
	FROM    Registration
	WHERE   SharedDocumentPath is not null;
	IF ( right(@DestinationDocPath,1)='\')
	BEGIN
		SET @FullDestinationPathFile=@DestinationDocPath +'' + @FileName;
	END
	ELSE BEGIN
	SET @FullDestinationPathFile=@DestinationDocPath +'\' + @FileName;
	END
	
	EXEC sp_OACREATE 'ADODB.Stream', @init OUTPUT;								-- An instace created
    EXEC sp_OASetProperty @init, 'Type', 1;
	EXEC sp_OAMethod @init, 'Open';												-- Calling a method
	EXEC sp_OAMethod @init, 'Write', NULL, @file_stream;						-- Calling a method
	EXEC sp_OAMethod @init, 'SaveToFile', NULL, @FullDestinationPathFile, 2;	-- Calling a method
	EXEC sp_OAMethod @init, 'Close';											-- Calling a method
	EXEC sp_OADestroy @init;													-- Closed the resources
END
GO
/****** Object:  StoredProcedure [dbo].[CopyAndPasteFileWithOutSharedDocument]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[CopyAndPasteFileWithOutSharedDocument](@FileName VARCHAR(250),@SourceDocPath VARCHAR(500), @DestinationDocPath VARCHAR(500))

AS

 

BEGIN
       --DECLARE @FileName               VARCHAR(250);

       --DECLARE @SourceDocPath          VARCHAR(500);

       --DECLARE @DestinationDocPath VARCHAR (500);

       --SET @Filename = 'adamas meeting.docx'

       --SET @SourceDocPath = 'C:\Adamas\aaTestSourceFolder'

       --SET @DestinationDocPath = 'C:\Adamas\aaTestDestinationFolder'

 

       DECLARE @command                                NVARCHAR(1000);

       DECLARE @file_stream                     VARBINARY(max);

       DECLARE @init                                   INT;

       DECLARE @FullDestinationPathFile  NVARCHAR (500);
 

       --SELECT @DestinationDocPath = shareddocumentpath

       --FROM   registration

       --WHERE  shareddocumentpath IS NOT NULL;

 

       SET @FullDestinationPathFile = @DestinationDocPath + '\' + @FileName

	   select @SourceDocPath +'\'+ @FileName;
	   select @FullDestinationPathFile;
       
	   SET @command =

                                         N'SELECT @file_stream1 = CAST(bulkcolumn AS varbinary(MAX)) FROM OPENROWSET(BULK '''

                                         + @SourceDocPath + '\' + @FileName

                                         + ''',                  SINGLE_BLOB) ROW_SET'

 

       EXEC Sp_executesql @command, N'@file_stream1 VARBINARY(MAX) OUTPUT', @file_stream1 =@file_stream OUTPUT

      

       --SELECT @file_stream,

       SELECT @SourceDocPath, @FileName, @FullDestinationPathFile, @file_stream

 

       EXEC sp_OACREATE 'ADODB.Stream', @init OUTPUT;                                                  -- An instace created

       EXEC sp_OASetProperty @init, 'Type', 1;

       EXEC sp_OAMethod @init, 'Open';                                                                               -- Calling a method

       EXEC sp_OAMethod @init, 'Write', NULL, @file_stream;                                     -- Calling a method

       EXEC sp_OAMethod @init, 'SaveToFile', NULL, @FullDestinationPathFile, 2;   -- Calling a method

       EXEC sp_OAMethod @init, 'Close';                                                                       -- Calling a method

       EXEC sp_OADestroy @init;                                                                                      -- Closed the resources

-- sp_configure 'show advanced options', 1; 
--GO 

--RECONFIGURE;
--GO 
--sp_configure 'Ole Automation Procedures', 1; 
--GO 

--RECONFIGURE; 

--GO 

END

/* Stored Procedure */
GO
/****** Object:  StoredProcedure [dbo].[Create_Rroster]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create or ALTER      PROCEDURE [dbo].[Create_Rroster](@P1 nvarchar(35),@P2 nvarchar(35),@P3 varchar(150),@P4 nvarchar(35),@P5 nvarchar(50),@P6 nvarchar(10),@P7 nvarchar(8),@P8 real,@P9 float,@P10 float,@P11 int,@P12 int,@P13 int,@P14 int,@P15 nvarchar(15)=null,@P16 nvarchar(5)=null,@P17 float,@P18 nvarchar(1)=null,@P19 nvarchar(35)=null,@P20 datetime2,@P21 datetime2,@P22 float,@P23 bit,@P24 nvarchar(35)=null,@P25 nvarchar(50)=null,@P26 float,@P27 varchar(150)=null,@P28 nvarchar(10)=null,@P29 float,@P30 float,@P31 smallint,@P32 varchar(50)=null,@P33 varchar(35)=null,@P34 varchar(25)=null,@P35 int=0,@P36 int=0,@P37 int=0,@P38 int=0,@P39 int=0,@P40 int=0,@P41 int=0)
as 
begin
if (@P4='') set @p4='N/A';
if (@P5='') set @p5='N/A';

INSERT INTO ROSTER ([Client Code],[Carer Code],[Service Type],[Service Description],Program,[Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],YearNo,MonthNo,Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition) 
VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,@P12,@P13,@P14,@P15,@P16,@P17,@P18,@P19,@P20,@P21,@P22,@P23,@P24,@P25,@P26,@P27,@P28,@P29,@P30,@P31,@P32,@P33,@P34,@P35,@P36,@P37,@P38,@P39,@P40,@P41)

select @@identity
end
GO
/****** Object:  StoredProcedure [dbo].[CreateClientStaffNote]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[CreateClientStaffNote]@PersonId     NVARCHAR(max) = '',
                                             @Program      NVARCHAR(max) = '',
                                             @DetailDate   DATETIME,
                                             @ExtraDetail1 NVARCHAR(max) = '',
                                             @ExtraDetail2 NVARCHAR(max) = '',
                                             @WhoCode      NVARCHAR(max) = '',
                                             @PublishToApp BIT = 0,
                                             @Creator      NVARCHAR(max) = '',
                                             @Note         NVARCHAR(max) = '',
                                             @AlarmDate    DATETIME = '',
                                             @ReminderTo   NVARCHAR(max)
AS
    INSERT INTO history
                (personid,
                 program,
                 detaildate,
                 detail,
                 extradetail1,
                 extradetail2,
                 whocode,
                 publishtoapp,
                 creator)
    VALUES      (@PersonId,
                 @Program,
                 Format(@DetailDate, 'MM-dd-yyyy HH:mm:ss', 'en-US'),
                 @ExtraDetail2 + ' : ' + @Program + '-' + @WhoCode + ' '
                 + @Note,
                 @ExtraDetail1,
                 @ExtraDetail2,
                 @WhoCode,
                 @PublishToApp,
                 @Creator)

    IF( @Note IS NOT NULL
        AND @Note <> ''
        AND @AlarmDate IS NOT NULL
        AND @AlarmDate <> '' )
      BEGIN
          INSERT INTO history
                      (personid,
                       program,
                       detaildate,
                       detail,
                       extradetail1,
                       extradetail2,
                       whocode,
                       publishtoapp,
                       creator)
          VALUES      (@PersonId,
                       '',
                       Format(@DetailDate, 'MM-dd-yyyy HH:mm:ss', 'en-US'),
                       @ExtraDetail2 + ' : -' + @WhoCode + ' ' + @Note,
                       'RECIPIENTALERT',
                       @ExtraDetail2,
                       @WhoCode,
                       @PublishToApp,
                       @Creator)

         IF ISNULL(@AlarmDate, '') <> '' AND ISNULL(@ReminderTo, '') <> '' 
			BEGIN
			  INSERT INTO humanresources
						  (personid,
						   [group],
						   type,
						   NAME,
						   date1,
						   date2,
						   notes,
						   creator,
						   reminderscope)
			  VALUES      (@PersonId,
						   'RECIPIENTALERT',
						   'RECIPIENTALERT',
						   'CASE NOTE REMINDER',
						   Format(@AlarmDate, 'yyyy/MM/dd', 'en-US'),
						   Format(@AlarmDate, 'yyyy/MM/dd', 'en-US'),
						   @Note,
						   @ReminderTo,
						   '')
		END
      END 
GO

/****** Object:  StoredProcedure [dbo].[CreateShortRosterEntry]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[CreateShortRosterEntry]
	@ClientCode NVARCHAR(max),			
	@CarerCode NVARCHAR(max),			
	@ServiceType  NVARCHAR(max),		
	@Program NVARCHAR(MAX),				
	@Date DATETIME,				        
	@Time NVARCHAR(max),                
	@Creator NVARCHAR(max),				
	@Editer NVARCHAR(max),				
	@BillUnit NVARCHAR(max),			
	@AgencyDefinedGroup NVARCHAR(max),  
	@ReferralCode NVARCHAR(max),		
	@Type SMALLINT,						
	@Duration SMALLINT,                 
	@TimePercent NVARCHAR(max),         
	@Notes NVARCHAR(max),               
	@BlockNo REAL,                      
	@ReasonType INT,					
	@TabType NVARCHAR(max),
	@HaccType NVARCHAR(MAX),			
	@BillTo NVARCHAR(MAX),				
	@CostUnit NVARCHAR(MAX)	= 'HOUR',	
	@BillDesc NVARCHAR(MAX) = '',
	@UnitPayRate FLOAT = 0,
	@UnitBillRate FLOAT = 0,
	@TaxPercent FLOAT = 0,
	@APIInvoiceDate NVARCHAR(MAX) = '',
	@APIInvoiceNumber NVARCHAR(MAX) = ''
AS		
SELECT 'START CreateShortRosterEntry'
INSERT INTO
  roster (
    [client code],
    [carer code],
    [service type],
    [service description],
    [program],
    [date],
    [hacctype],
    [start time],
    [duration],
    [unit pay rate],
    [unit bill rate],
    [taxpercent],
    [yearno],
    [monthno],
    [dayno],
    [blockno],
    [notes],
    [type],
    [status],
    [anal],
    [date entered],
    [date last mod],
    [groupactivity],
    [billtype],
    [billto],
    [apinvoicedate],
    [apinvoicenumber],
    [costunit],
    [costqty],
    [billunit],
    [billqty],
    [servicesetting],
    [dischargereasontype],
    [datasetclient],
    [nrcp_referral_service],
    [creator],
    [editer],
    [inuse],
    [billdesc],
    [transferred]
  )
VALUES
  (
    @ClientCode,
	CASE WHEN  @CarerCode = 'INTERNAL' THEN '!INTERNAL' ELSE @CarerCode END, 
    @ServiceType,
    'NOPAY',
    @Program,
    FORMAT(@Date, 'yyyy/MM/dd', 'en-US'),
    @HaccType,
    @Time,
    @Duration,
    @UnitPayRate,
    @UnitBillRate,
    @TaxPercent,
    YEAR(@Date),
    MONTH(@Date),
    DAY(@Date),
    @BlockNo,
    @Notes,
    @Type,
    CASE 
		WHEN @Type IN (7) THEN 2
		ELSE
			CASE 
				WHEN @CarerCode IN ('!INTERNAL') THEN 2 
				ELSE 1 
			END
	END,
    @AgencyDefinedGroup,
    GetDate(),
    GetDate(),
    '0',
    '',
    @BillTo,
    @APIInvoiceDate,
    @APIInvoiceNumber,
    @CostUnit,
    @TimePercent,
    @BillUnit,
    @TimePercent,
    '',
    @ReasonType,
    @ClientCode,
    @ReferralCode,
    @Creator,
    @Editer,
    0,
    @BillDesc,
    0
  )
INSERT INTO
  audit (
    operator,
    actiondate,
    auditdescription,
    actionon,
    whowhatcode,
    traccsuser
  )
VALUES
  (
    @Creator,
    @Date,
    'AUTO CREATE OR ALTER ' + @TabType,
    'ROSTER',
    SCOPE_IDENTITY(),
    @Editer
  )
SELECT 'END reateShortRosterEntry'
GO
/****** Object:  StoredProcedure [dbo].[DeterminePayType]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER    PROCEDURE [dbo].[DeterminePayType](@ChooseEach bit=0,  @PayTypeMode varchar(100)='',
 @s_PayItem varchar(100)='',  @s_PayUnit varchar(100)='',  @s_PayRate varchar(100)='',  @s_Status varchar(100)='', 
 @s_DayMask varchar(100)='',  @b_Award bit=0,  @s_RosterStaff varchar(100)='',  @b_TestForSingle bit='', 
 @s_TimespanStart varchar(10)='',  @s_TimespanEnd varchar(10)='')
    

As

begin

Declare @paytype as varchar(100) ;
set @paytype='';
Declare @b_AwardInterpret as bit;

select @b_AwardInterpret=UseAwards from registration;

Declare @Award varchar(100);
Declare @NoMonday bit
Declare @NoTuesday bit 
Declare @NoWednesday bit
Declare @NoThursday bit
Declare @NoFriday bit
Declare @NoSaturday bit
Declare @NoSunday bit
Declare @NoPubHol bit
Declare @NoTimLimit bit

set @NoTimLimit=1;

 If (@ChooseEach=1) 
 BEGIN
      SELECT * FROM itemtypes
      WHERE Recnum = @s_PayItem AND PROCESSCLASSIFICATION = 'INPUT';
	  return;
END

SELECT @Award=Award --UniqueID , AccountNo, Award, JobTitle, ContactIssues, Telephone, STF_Notes, FirstName, LastName, UBDMap, UBDRef, Category 
FROM staff WHERE AccountNo = @s_RosterStaff 
        
	 If (@b_AwardInterpret=1 ) begin
        if(@s_PayItem ='AWARD') begin set @s_PayUnit = 'HOUR'; set @s_Payrate = '0'; end
		
        select 0 as Recnum, @s_PayItem as Title, @s_Payrate as Amount, @s_PayUnit as unit;
    End
    Else
	Begin
	  set @NoMonday =0 set @NoTuesday=0 ; set @NoWednesday=0; set @NoThursday=0; set @NoFriday=0; set @NoSaturday=0; set @NoSunday=0;set @NoPubHol=0;
	  If (@s_DayMask <> '' And @s_DayMask <> '00000000') Begin
            If (left(@s_DayMask, 1) = '1' ) set @NoMonday = 1;		
            If (substring(@s_DayMask, 2, 1) = '1' ) set @NoTuesday=1;
            If (substring(@s_DayMask, 3, 1) = '1' ) set @NoWednesday=1;
            If (substring(@s_DayMask, 4, 1) = '1' ) set @NoThursday=1;
            If (substring(@s_DayMask, 5, 1) = '1' ) set @NoFriday = 1;   
            If (substring(@s_DayMask, 6, 1) = '1' ) set @NoSaturday = 1; 
            If (substring(@s_DayMask, 7, 1) = '1' ) set @NoSunday =1;
            If (substring(@s_DayMask, 8, 1) = '1' ) set @NoPubHol=1;            
      End 
	  

	    If (@s_TimespanStart <> '' And @s_TimespanEnd <> '') set @NoTimLimit=0;

	  --(pt.StartTimeLimit < '" & s_TimespanStart & "' AND pt.EndTimeLimit > '" & s_TimeEnd & "')"
		 If (@PayTypeMode = 'Filter for Pay Group' )
		 Begin
                Declare @i_Mask int
                set @i_Mask = Len(@Award)
				if exists( SELECT Recnum, LTRIM(RIGHT(Title, LEN(Title) - @i_Mask  ))  
					 FROM ItemTypes pt 
					 WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' And ProcessClassification = 'INPUT'
					 AND Title BETWEEN @Award  AND @Award + 'zzzzzzzzzz' AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111))
					 AND isnull(NoMonday,0)=@NoMonday AND  isnull(NoTuesday,0)=@NoTuesday AND  isnull(NoWednesday,0)=@NoWednesday AND  isnull(NoTuesday,0)=@NoTuesday AND  isnull(NoFriday,0)=@NoFriday
					 AND isnull(NoSaturday,0)=@NoSaturday AND  isnull(NoSunday,0)=@NoSunday AND  isnull(NoPubHol,0)=NoPubHol
					 AND ((pt.StartTimeLimit < @s_TimespanStart   AND pt.EndTimeLimit > @s_TimespanEnd ) OR @NoTimLimit=1)
				)  
                
				 SELECT Recnum, LTRIM(RIGHT(Title, LEN(Title) - @i_Mask  )) as Title,  Amount,unit
				 FROM ItemTypes pt 
				 WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' And ProcessClassification = 'INPUT'
                 AND Title BETWEEN @Award  AND @Award + 'zzzzzzzzzz' AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111))
				 AND isnull(NoMonday,0)=@NoMonday AND  isnull(NoTuesday,0)=@NoTuesday AND  isnull(NoWednesday,0)=@NoWednesday AND  isnull(NoTuesday,0)=@NoTuesday AND  isnull(NoFriday,0)=@NoFriday
				 AND isnull(NoSaturday,0)=@NoSaturday AND  isnull(NoSunday,0)=@NoSunday AND  isnull(NoPubHol,0)=NoPubHol
				 AND ((pt.StartTimeLimit < @s_TimespanStart   AND pt.EndTimeLimit > @s_TimespanEnd ) OR @NoTimLimit=1)
				 ORDER BY Title 

				ELSE
				    SELECT Recnum, Title,Amount,unit FROM ItemTypes pt WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' And ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111))  ORDER BY Title 

           End     
           Else begin
                SELECT Recnum, Title,Amount,unit FROM ItemTypes pt WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' And ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111))  ORDER BY Title 
           End
	END

	
END

GO
/****** Object:  StoredProcedure [dbo].[get_Program_Recipeitns]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER Procedure [dbo].[get_Program_Recipeitns]
		As
		begin
		SELECT re.UniqueId as Personid, isnull(re.Title,'') + ' ' + IsNull(re.FirstName, '') + ' ' + Isnull(re.[Surname/Organisation], '') AS ClientName, 
								 re.AccountNo,s.[ServiceProgram],s.[Service Type],dbo.getCoordinator_Email(re.AccountNo,'RECIPIENT') as Coordinator_Email  
								 From SERVICEOVERVIEW s  
								 Join Recipients re On s.PersonID=re.UniqueID  
								 left join itemtypes it on it.title = [service type]  
								 left join humanresourcetypes pr on pr.[name] = serviceprogram  
								 where  ServiceStatus = 'Active' and  
								 re.AdmissionDate is not null and re.DischargeDate is null  
								 And PersonID > 'A' AND RosterGroup IN ('TRANSPORT', 'CENTREBASED', 'GROUPACTIVITY') 
								 And UPPER(isnull(pr.User2,'')) <> 'MEALS' 
								 and  IsNull(re.FirstName, '') + ' ' + Isnull(re.[Surname/Organisation], '') <>'' 
								 Group by s.ServiceProgram,s.[Service Type],re.UniqueId, re.AccountNo,re.Title,re.FirstName,re.[Surname/Organisation] 
								 Order by IsNull(re.FirstName,''), Isnull(re.[Surname/Organisation], '')

		end
GO
/****** Object:  StoredProcedure [dbo].[get_Recipient_Goals]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[get_Recipient_Goals] (@PersonID VARCHAR(20)) 
AS 
  BEGIN 

  if exists (
      SELECT 
             goal ,
             isnull(IT.ItemType,isnull(IT.title ,''))      AS Activity      
           
      FROM   (SELECT DISTINCT qtehdrrecordno, 
                              plannumber, 
                              goalid, 
                              strategyid, 
                              goal, 
                              strategy, 
                              [docstartdate], 
                              [docenddate], 
                              planname AS [Careplan Name] 
              FROM   (SELECT doc_id, 
                             CONVERT(VARCHAR(10), D.created, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.author) AS Created, 
                             CONVERT(VARCHAR(10), D.modified, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.typist) AS Modified, 
                             D.doc#                       AS CareplanID, 
                             D.title                      AS PlanName, 
                             filename, 
                             D.status, 
                             D.docstartdate, 
                             D.docenddate, 
                             D.alarmdate, 
                             D.alarmtext, 
                             GL.user1                     AS Goal, 
                             ST.notes                     AS Strategy, 
                             GL.recordnumber              AS GoalID, 
                             ST.recordnumber              AS StrategyID, 
                             QH.doc#                      AS PlanNumber, 
                             QH.recordnumber              AS QteHdrRecordNo 
                      FROM   documents D 
                             LEFT JOIN qte_hdr QH 
                                    ON cpid = doc_id 
                             LEFT JOIN humanresources GL 
                                    ON CONVERT(VARCHAR, QH.cpid) = GL.personid 
                             LEFT JOIN humanresources ST 
                                    ON CONVERT(VARCHAR, ST.personid) = 
                                       CONVERT(VARCHAR, GL.recordnumber) 
                      WHERE  D.personid = @PersonID 
                             AND documentgroup IN ( 'CAREPLAN', 'CP_QUOTE' ) 
                             AND ( D.deletedrecord = 0 
                                    OR D.deletedrecord IS NULL )) D1) D2 
             LEFT JOIN (SELECT * 
                        FROM   qte_lne QL 
                        WHERE  strategyid <> 0) QL 
                    ON QL.strategyid = D2.strategyid 
                       AND QL.doc_hdr_id = D2.qtehdrrecordno 
             LEFT JOIN itemtypes IT 
                    ON QL.itemid = IT.recnum 
					where goal is not null
      )
	  begin
		SELECT 
             goal ,
             isnull(IT.ItemType,isnull(IT.title ,''))      AS Activity      
           
      FROM   (SELECT DISTINCT qtehdrrecordno, 
                              plannumber, 
                              goalid, 
                              strategyid, 
                              goal, 
                              strategy, 
                              [docstartdate], 
                              [docenddate], 
                              planname AS [Careplan Name] 
              FROM   (SELECT doc_id, 
                             CONVERT(VARCHAR(10), D.created, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.author) AS Created, 
                             CONVERT(VARCHAR(10), D.modified, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.typist) AS Modified, 
                             D.doc#                       AS CareplanID, 
                             D.title                      AS PlanName, 
                             filename, 
                             D.status, 
                             D.docstartdate, 
                             D.docenddate, 
                             D.alarmdate, 
                             D.alarmtext, 
                             GL.user1                     AS Goal, 
                             ST.notes                     AS Strategy, 
                             GL.recordnumber              AS GoalID, 
                             ST.recordnumber              AS StrategyID, 
                             QH.doc#                      AS PlanNumber, 
                             QH.recordnumber              AS QteHdrRecordNo 
                      FROM   documents D 
                             LEFT JOIN qte_hdr QH 
                                    ON cpid = doc_id 
                             LEFT JOIN humanresources GL 
                                    ON CONVERT(VARCHAR, QH.cpid) = GL.personid 
                             LEFT JOIN humanresources ST 
                                    ON CONVERT(VARCHAR, ST.personid) = 
                                       CONVERT(VARCHAR, GL.recordnumber) 
                      WHERE  D.personid = @PersonID 
                             AND documentgroup IN ( 'CAREPLAN', 'CP_QUOTE' ) 
                             AND ( D.deletedrecord = 0 
                                    OR D.deletedrecord IS NULL )) D1) D2 
             LEFT JOIN (SELECT * 
                        FROM   qte_lne QL 
                        WHERE  strategyid <> 0) QL 
                    ON QL.strategyid = D2.strategyid 
                       AND QL.doc_hdr_id = D2.qtehdrrecordno 
             LEFT JOIN itemtypes IT 
                    ON QL.itemid = IT.recnum 
					where goal is not null
      ORDER  BY goal
	  end
	  else begin
			  SELECT Description as goal,'' as activity FROM DataDOmains WHERE Domain = 'GOALOFCARE'
	  end
  END 
GO
/****** Object:  StoredProcedure [dbo].[get_Recipient_Goals_Strategies]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[get_Recipient_Goals_Strategies] (@PersonID VARCHAR(20)) 
AS 
  BEGIN 
 
      SELECT 
             goal ,
             strategy, 
          isnull(IT.ItemType,isnull(IT.title ,''))      AS Activity    
          
           
      FROM   (SELECT DISTINCT qtehdrrecordno, 
                              plannumber, 
                              goalid, 
                              strategyid, 
                              goal, 
                              strategy, 
                              [docstartdate], 
                              [docenddate], 
                              planname AS [Careplan Name] 
              FROM   (SELECT doc_id, 
                             CONVERT(VARCHAR(10), D.created, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.author) AS Created, 
                             CONVERT(VARCHAR(10), D.modified, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.typist) AS Modified, 
                             D.doc#                       AS CareplanID, 
                             D.title                      AS PlanName, 
                             filename, 
                             D.status, 
                             D.docstartdate, 
                             D.docenddate, 
                             D.alarmdate, 
                             D.alarmtext, 
                             GL.user1                     AS Goal, 
                             ST.notes                     AS Strategy, 
                             GL.recordnumber              AS GoalID, 
                             ST.recordnumber              AS StrategyID, 
                             QH.doc#                      AS PlanNumber, 
                             QH.recordnumber              AS QteHdrRecordNo 
                      FROM   documents D 
                             LEFT JOIN qte_hdr QH 
                                    ON cpid = doc_id 
                             LEFT JOIN humanresources GL 
                                    ON CONVERT(VARCHAR, QH.cpid) = GL.personid 
                             LEFT JOIN humanresources ST 
                                    ON CONVERT(VARCHAR, ST.personid) = 
                                       CONVERT(VARCHAR, GL.recordnumber) 
                      WHERE  D.personid = @PersonID 
                             AND documentgroup IN ( 'CAREPLAN', 'CP_QUOTE' ) 
                             AND ( D.deletedrecord = 0 
                                    OR D.deletedrecord IS NULL )) D1) D2 
             LEFT JOIN (SELECT * 
                        FROM   qte_lne QL 
                        WHERE  strategyid <> 0) QL 
                    ON QL.strategyid = D2.strategyid 
                       AND QL.doc_hdr_id = D2.qtehdrrecordno 
             LEFT JOIN itemtypes IT 
                    ON QL.itemid = IT.recnum 
					where goal is not null
      ORDER  BY goal
  END 
GO
/****** Object:  StoredProcedure [dbo].[get_Roster_Recipients]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE OR ALTER PROCEDURE [dbo].[get_Roster_Recipients] (@carer_Code varchar(50))
	AS BEGIN    

	SELECT rst.recordno, 
		   accountno, 
		   servicesetting, 
		   r.title                                          AS pTitle, 
		   r.firstname, 
		   r.[surname/organisation]                         AS lastName, 
		   r.preferredname, 
		   Isnull(itm.billtext, rst.[service type])         AS ServiceType, 
		   Isnull(r.pincode, '0')                           AS PinCode, 
		   rst.[start time], 
		   rst.[duration], 
		   itm.title, 
		   recipient_coordinator, 
		   Isnull(r.specialconsiderations, '')              AS SpecialConsiderations 
		   , 
		   Isnull(rst.[notes], '')                          AS RosterNotes, 
		   Isnull(r.careplanchange, 0)                      AS Careplanchange, 
		   Isnull(r.mobility, '-')                          AS Mobility, 
		   dbo.Getcoordinator_email(accountno, 'RECIPIENT') AS Coordinator_Email, 
		   Isnull(r.dateofbirth, '')                        AS DateOfBirth 
	FROM   roster rst WITH (nolock) 
		   JOIN recipients r WITH (nolock) 
			 ON r.accountno = rst.[client code] 
				AND rst.[carer code] = @carer_Code 
				AND Abs(Datediff(dy, date, Getdate())) <= (select top 1  isnull(MobileFutureLimit,10) from UserInfo with (nolock) where staffCode=@carer_Code)
		   JOIN itemtypes itm 
			 ON itm.[title] = rst.[service type] 

	END
GO
/****** Object:  StoredProcedure [dbo].[get_Service_wise_Shift_goals]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER Procedure [dbo].[get_Service_wise_Shift_goals] (@CarerCode varchar(100), @MobileFutureLimit int)
		as
		begin

		SELECT distinct AccountNo, D.DOC_ID, Q.ItemID, q.StrategyID, gl.User1 as Shift_Goal,I.Title as Service_Type
						FROM QTE_LNE Q INNER JOIN ItemTypes I ON Q.ItemID = I.Recnum
						   INNER JOIN HumanResources ST ON Q.StrategyID = ST.RecordNumber
						   INNER JOIN HumanResources GL ON ST.PersonID = GL.RecordNumber
						   INNER JOIN Qte_Hdr qh ON Q.Doc_Hdr_ID = qh.RecordNumber
						   INNER JOIN Recipients r ON r.SQLID = qh.ClientID
						   INNER JOIN DOCUMENTS D ON R.UNIQUEID = D.PERSONID AND QH.CPID = D.DOC_ID AND DOCUMENTGROUP IN ('CAREPLAN')	
						   inner join Roster rst on rst.[Service Type]=i.Title AND Abs(Datediff(dy, rst.date, Getdate())) <= @MobileFutureLimit
						   and rst.[Carer Code]=@CarerCode
						   WHERE ACCOUNTNO = r.AccountNo


		End 
GO
/****** Object:  StoredProcedure [dbo].[get_Staff_Recipients]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[get_Staff_Recipients] (@carer_Code varchar(50), @MobileFutureLimit int)
		AS BEGIN    

		SELECT n.recordnumber, 
			   r.accountno, 
			   r.uniqueid 
			   AS Personid, 
			   n.primaryaddress, 
			   Replace(Replace(n.[description], '<', ''), '>', '') 
			   AS [Type], 
			   n.address1 + CASE WHEN n.address2 <> '' THEN ' ' + n.address2 ELSE ' ' 
			   END + 
			   CASE WHEN n.suburb <> '' THEN ' ' + n.suburb ELSE ' ' END + CASE WHEN 
			   n.postcode 
			   <> '' THEN ' ' + n.postcode ELSE ' ' END 
			   AS Address, 
			   n.address1 + CASE WHEN n.address2 <> '' THEN ' ' + n.address2 ELSE ' ' 
			   END AS 
			   Simple_Address, 
			   n.googleaddress, 
			   r.specialconsiderations, 
			   r.notes, 
			   Replace(Replace(p.[type], '<', ''), '>', '') 
			   + '-' + p.detail 
			   AS Phone, 
			   Isnull(r.pincode, 0) 
			   AS PinCode, 
			   accountingidentifier, 
			   Isnull(r.dateofbirth, '') 
			   AS DateOfBirth 
		FROM   recipients r 
			   LEFT JOIN namesandaddresses n 
					  ON n.personid = r.uniqueid 
			   LEFT JOIN phonefaxother p 
					  ON p.personid = r.uniqueid 
		WHERE  r.accountno IN (SELECT [client code] 
							   FROM   roster rst 
							   WHERE  [carer code] IN ( @carer_Code ) 
									  AND [date] BETWEEN Dateadd(dy, -@MobileFutureLimit 
														 , 
														 Getdate()) AND 
										  Dateadd(dy, @MobileFutureLimit, 
										  Getdate()) 
						GROUP  BY [client code]) 
		ORDER  BY accountno, 
				  primaryaddress DESC, 
				  primaryphone DESC, 
				  [type] ASC, 
				  n.recordnumber 
		END
GO
/****** Object:  StoredProcedure [dbo].[get_Staff_Recipients2]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER procedure [dbo].[get_Staff_Recipients2] (@carer_Code varchar(50),@MobileFutureLimit int)

as begin


SELECT  r.accountNo,r.UniqueId as Personid,  PrimaryAddress, n.[Type], n.Address,n.Simple_Address ,                             

					    isnull( (SELECT TOP 1   REPLACE(REPLACE(p.[Type], '<', ''), '>', '')  + '-' + p.Detail							   
							   FROM PhoneFaxOther p where PersonID=r.UniqueId 
							   ORDER BY CASE WHEN PrimaryPhone = 1 THEN 1
							   WHEN [Type] = '<HOME>' THEN 2 ELSE 3 end),'-')  as Phone 
                        , isnull(r.PinCode,0) as PinCode,ACCOUNTINGIDENTIFIER  ,
						isnull((select top 1 googleaddress from NamesAndAddresses where PersonID=r.UniqueID and GoogleAddress is not null ),'')googleaddress  
                       ,r.SpecialConsiderations,r.Notes 

                        FROM Recipients r
						Left Join
						(SELECT  PersonId,isnull(GoogleAddress,'') as GoogleAddress,REPLACE(REPLACE([Description], '<', ''), '>', '') AS [Type],
					   CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +
					   CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +
					   CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +
					   CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address,
					    CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +
					   CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END as Simple_Address,
					   CASE WHEN PRIMARYADDRESS = 1 THEN 1
					   WHEN DESCRIPTION = '<USUAL>' THEN 2
                       ELSE 3 END AS PRIMARYADDRESS ,
					   Rank() over (Partition BY PersonId ORDER BY  PRIMARYADDRESS  DESC ) AS Rank
						FROM NAMESANDADDRESSES
						 ) as n on r.UniqueID=n.PersonId and Rank=1
                        WHERE r.accountNo  in (select [Client code] from roster rst  
                        where  [carer code] in (@carer_Code)    
                        and  [date] between dateAdd(dy,-@MobileFutureLimit,getDate()) and dateAdd(dy,@MobileFutureLimit,getDate()) group by [Client code] )  
                       ORDER BY AccountNo
end	
GO
/****** Object:  StoredProcedure [dbo].[get_Strategies]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER procedure [dbo].[get_Strategies]
  as
  begin
  SELECT 
             goal ,
             strategy, 
          isnull(IT.ItemType,isnull(IT.title ,''))      AS Activity    
          
           
      FROM   (SELECT DISTINCT qtehdrrecordno, 
                              plannumber, 
                              goalid, 
                              strategyid, 
                              goal, 
                              strategy, 
                              [docstartdate], 
                              [docenddate], 
                              planname AS [Careplan Name] 
              FROM   (SELECT doc_id, 
                             CONVERT(VARCHAR(10), D.created, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.author) AS Created, 
                             CONVERT(VARCHAR(10), D.modified, 103) + ' ' 
                             + (SELECT [name] 
                                FROM   userinfo 
                                WHERE  recnum = D.typist) AS Modified, 
                             D.doc#                       AS CareplanID, 
                             D.title                      AS PlanName, 
                             filename, 
                             D.status, 
                             D.docstartdate, 
                             D.docenddate, 
                             D.alarmdate, 
                             D.alarmtext, 
                             GL.user1                     AS Goal, 
                             ST.notes                     AS Strategy, 
                             GL.recordnumber              AS GoalID, 
                             ST.recordnumber              AS StrategyID, 
                             QH.doc#                      AS PlanNumber, 
                             QH.recordnumber              AS QteHdrRecordNo 
                      FROM   documents D 
                             LEFT JOIN qte_hdr QH 
                                    ON cpid = doc_id 
                             LEFT JOIN humanresources GL 
                                    ON CONVERT(VARCHAR, QH.cpid) = GL.personid 
                             LEFT JOIN humanresources ST 
                                    ON CONVERT(VARCHAR, ST.personid) = 
                                       CONVERT(VARCHAR, GL.recordnumber) 
                      WHERE   documentgroup IN ( 'CAREPLAN', 'CP_QUOTE' ) 
                             AND ( D.deletedrecord = 0 
                                    OR D.deletedrecord IS NULL )) D1) D2 
             LEFT JOIN (SELECT * 
                        FROM   qte_lne QL 
                        WHERE  strategyid <> 0) QL 
                    ON QL.strategyid = D2.strategyid 
                       AND QL.doc_hdr_id = D2.qtehdrrecordno 
             LEFT JOIN itemtypes IT 
                    ON QL.itemid = IT.recnum 
					where goal is not null
      ORDER  BY goal

End
GO
/****** Object:  StoredProcedure [dbo].[GetActivities]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetActivities] (@Recipient        AS VARCHAR (50),
									   @Program          AS VARCHAR (50),
                                       @ForceAll		 AS BIT,
									   @MainGroup        AS VARCHAR (50),
									   @SubGroup		 AS VARCHAR (50),
									   @ViewType		 AS VARCHAR (50),
									   @AllowedDays		 AS VARCHAR (8),
									   @Duration		 AS VARCHAR (8)
																		)
AS
BEGIN
--DECLARE @Recipient	 AS VARCHAR (50)
--DECLARE @Program	 AS VARCHAR (50)
--DECLARE @ForceAll	 AS BIT
--DECLARE @MainGroup	 AS VARCHAR (50)
--DECLARE @SubGroup	 AS VARCHAR (50)
--DECLARE @ViewType	 AS VARCHAR (50)
--DECLARE @AllowedDays AS VARCHAR (8)
--DECLARE @Duration	 AS VARCHAR (8)

SET @Recipient = ''''+ @Recipient +''''
SET @Program = ''''+ @Program +''''
SET @MainGroup = '''' + @MainGroup+ ''''
SET @ForceALL = @ForceAll
SET @ViewType = @ViewType
SET @AllowedDays = @AllowedDays
SET @Duration = @Duration

    DECLARE @Extent As VARCHAR (max)
    DECLARE @GROUPS As VARCHAR (max)
    DECLARE @AllSQL As VARCHAR (max)
    DECLARE @MainGroupSQL As VARCHAR (max)
	DECLARE @SubGroupSQL AS VARCHAR (max)
    DECLARE @RecipientLinkedSQL As VARCHAR (max)
    DECLARE @ProgramLinkedSQL As VARCHAR (max)
    DECLARE @MealLinkedSQL As VARCHAR (max)
    DECLARE @ProgramActivityCountSQL As VARCHAR (max)
    DECLARE @Caption As VARCHAR (max)
    DECLARE @DayTimeSQL AS VARCHAR (max)
	DECLARE @EnforceActivityLimits AS bit
	DECLARE @BaseItemTypesSQL AS VARCHAR (max)
	DECLARE @BaseProgServSQL AS VARCHAR (max)
	DECLARE @BaseRecipServSQL AS VARCHAR (max)
	DECLARE @FinalSQL AS VARCHAR (max)
	SELECT @EnforceActivityLimits = ISNULL(EnforceActivityLimits, 0) FROM Registration
    IF (@EnforceActivityLimits = 1)
		IF (@AllowedDays <> '') AND (@AllowedDays <> '00000000') --THEN
		
			IF SUBSTRING(@AllowedDays, 1, 1) = '1' SET @DayTimeSQL = 'IsNull(ITM.NoMonday, 0) = 0'
			IF SUBSTRING(@AllowedDays, 2, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoTuesday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoTuesday, 0) = 0'
			  --END IF
		  --END IF
			
			IF SUBSTRING(@AllowedDays, 3, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoWednesday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoWednesday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,4, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoThursday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoThursday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,5, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoFriday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoFriday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,6, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoSaturday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoSaturday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,7, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoSunday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoSunday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,8, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoPubHol, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoPubHol, 0) = 0'
			  --END IF
		  --END IF
		  IF  @DayTimeSQL <> '' --THEN
			SET @DayTimeSQL = ' AND (' + @DayTimeSQL  + ')'
      --END IF
        --@DayTimeSQL = @DayTimeSQL +
        -- AND (' &
        --(ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) OR (ISNULL(ITM.MinDurtn, ' & @Duration & ') <= ' & @Duration & _
        -- AND IsNull(ITM.MaxDurtn, ' & @Duration & ') >= ' & @Duration & '))'
  --END IF
    SET @Extent = ''
	SET @BaseItemTypesSQL = 'SELECT DISTINCT Title FROM ItemTypes itm WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(EndDate, '''') = ''''  OR EndDate > GetDate()) '
	SET @BaseProgServSQL = 'SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes pr ON CONVERT(nVarchar, pr.RecordNumber) = SO.PersonID INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) '
	SET @BaseRecipServSQL = 'SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN Recipients re ON SO.PersonID = re.UniqueID INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) '
--SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO
--INNER JOIN HumanResourceTypes pr ON CONVERT(nVarchar, pr.RecordNumber) = SO.PersonID
--INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title
--WHERE ProcessClassification = 'OUTPUT' AND (ISNULL(itm.EndDate, '') = ''  OR itm.EndDate > GetDate())
--AND pr.Name = 'CBI-78001'
    SET @MainGroup = CASE WHEN @MainGroup = 'ALL' THEN '' ELSE @MainGroup END
	--ITEM LIST - UNFILTERED
	SET @AllSQL = @BaseItemTypesSQL + ' ORDER BY Title'
	--SELECT @AllSQL FROM Registration
	--EXEC (@AllSQL)
    --ITEM LIST FILTERED BY ROSTER GROUP ONLY
	SET @MainGroup = '''ADMISSION'''
    SET @MainGroupSQL = @BaseItemTypesSQL + ' AND itm.RosterGroup = ' + @MainGroup + ' ORDER BY Title'
	--SELECT @MainGroupSQL FROM Registration
	--EXEC (@MainGroupSQL)
    --ITEM LIST FILTERED BY MEAL SUBGROUP
	SET @SubGroup = '''MEALS'''
	SET @SubGroupSQL = @BaseItemTypesSQL +  ' AND itm.MinorGroup = ' + @SubGroup + ' ORDER BY Title'
	--SELECT @SubGroupSQL AS SubGroupLinked FROM Registration
	--EXEC (@SubGroupSQL)
    --SERVICE OVERVIEW LIST FILTERED BY PROGRAM APPROVED SERVICES
    SET @ProgramLinkedSQL = @BaseProgServSQL
							+ CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND pr.Name = ' + @Program END
							+ CASE WHEN ISNULL(@MainGroup, 'ALL') = 'ALL' THEN '' ELSE ' AND itm.RosterGroup = ' + @MainGroup END
							+ ' ORDER BY [Service Type]'
	--SELECT @ProgramLinkedSQL AS ProgLinked FROM Registration
	--EXEC (@ProgramLinkedSQL)
	   
    --'ITEM LIST FILTERED BY RECIPIENT APPROVED
	SET @RecipientLinkedSQL = @BaseRecipServSQL
							  + ' AND re.AccountNo = ' + @Recipient
							  + CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND so.ServiceProgram = ' + @Program END
	--SELECT @RecipientLinkedSQL AS ProgLinked FROM Registration
	--EXEC (@RecipientLinkedSQL)
    If (@FORCEAll = 1) --THEN
		SET @FinalSQL =
        CASE
			WHEN @MainGroup = 'ALL' THEN @AllSQL
			ELSE @MainGroupSQL
		END
    ELSE
		IF @ViewType = 'STAFF' --THEN
            IF @Recipient > '!MULTIPLE'
				SET @FinalSQL =
                CASE
					WHEN @MainGroup IN ('ADMISSION', 'TRAVELTIME') THEN @ProgramLinkedSQL
					WHEN @MainGroup =  'ITEM' THEN @MainGroupSQL
					ELSE @RecipientLinkedSQL
				END
			ELSE
				SET @FinalSQL =
				CASE WHEN @SubGroup = 'MEALS' THEN @MealLinkedSQL
				ELSE @ProgramLinkedSQL
				END
            --END IF
        ELSE
			SET @FinalSQL =
            CASE WHEN  @SubGroup = 'MEALS' THEN @MealLinkedSQL
            ELSE
                CASE
					WHEN @MainGroup IN ('ADMISSION', 'TRAVELTIME') THEN @ProgramLinkedSQL
					WHEN @MainGroup =  'ITEM' THEN @MainGroupSQL
					ELSE @RecipientLinkedSQL
				END
            END
		--END IF
		--SELECT @FinalSQL AS FInalSQL FROM Registration
		EXEC (@FinalSQL)
		
END


/****** Object:  StoredProcedure [dbo].[Copy_MTA_Document_WithoutSharedDocument]    Script Date: 24/06/2021 7:46:23 PM ******/
SET ansi_nulls ON

GO
/****** Object:  StoredProcedure [dbo].[GetActivities2]    Script Date: 27/04/2022 10:57:23 ******/

GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER      PROCEDURE [dbo].[GetActivities2] (@Recipient        AS VARCHAR (50),
									   @Program          AS VARCHAR (50),
                                       @ForceAll		 AS BIT,
									   @MainGroup        AS VARCHAR (50),
									   @SubGroup		 AS VARCHAR (50),
									   @ViewType		 AS VARCHAR (50),
									   @AllowedDays		 AS VARCHAR (8),
									   @Duration		 AS VARCHAR (8)
																		)
AS
BEGIN
--DECLARE @Recipient	 AS VARCHAR (50)
--DECLARE @Program	 AS VARCHAR (50)
--DECLARE @ForceAll	 AS BIT
--DECLARE @MainGroup	 AS VARCHAR (50)
--DECLARE @SubGroup	 AS VARCHAR (50)
--DECLARE @ViewType	 AS VARCHAR (50)
--DECLARE @AllowedDays AS VARCHAR (8)
--DECLARE @Duration	 AS VARCHAR (8)

if (@SubGroup='-') set @SubGroup='';

SET @Recipient = ''''+ @Recipient +''''
SET @Program = ''''+ @Program +''''
SET @MainGroup = '''' + @MainGroup+ ''''
SET @ForceALL = @ForceAll
SET @ViewType = @ViewType
SET @AllowedDays = @AllowedDays
SET @Duration = @Duration

	DECLARE @Columns_1 As VARCHAR (max)
	DECLARE @Columns_2 As VARCHAR (max)
	DECLARE @Columns_3 As VARCHAR (max)
    DECLARE @Extent As VARCHAR (max)
    DECLARE @GroupBy1 AS VARCHAR (max)
	DECLARE @GroupBy2 AS VARCHAR (max)
	DECLARE @GroupBy3 AS VARCHAR (max)

	DECLARE @GROUPS As VARCHAR (max)
    DECLARE @AllSQL As VARCHAR (max)
    DECLARE @MainGroupSQL As VARCHAR (max)
	DECLARE @SubGroupSQL AS VARCHAR (max)
    DECLARE @RecipientLinkedSQL As VARCHAR (max)
    DECLARE @ProgramLinkedSQL As VARCHAR (max)
    DECLARE @MealLinkedSQL As VARCHAR (max)
    DECLARE @ProgramActivityCountSQL As VARCHAR (max)
    DECLARE @Caption As VARCHAR (max)
    DECLARE @DayTimeSQL AS VARCHAR (max)
	DECLARE @EnforceActivityLimits AS bit
	DECLARE @BaseItemTypesSQL AS VARCHAR (max)
	DECLARE @BaseProgServSQL AS VARCHAR (max)
	DECLARE @BaseRecipServSQL AS VARCHAR (max)
	DECLARE @FinalSQL AS VARCHAR (max)
	
	SELECT @EnforceActivityLimits = ISNULL(EnforceActivityLimits, 0) FROM Registration
    IF (@EnforceActivityLimits = 1)
		IF (@AllowedDays <> '') AND (@AllowedDays <> '00000000') --THEN
		
			IF SUBSTRING(@AllowedDays, 1, 1) = '1' SET @DayTimeSQL = 'IsNull(ITM.NoMonday, 0) = 0'
			IF SUBSTRING(@AllowedDays, 2, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoTuesday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoTuesday, 0) = 0'
			  --END IF
		  --END IF
			
			IF SUBSTRING(@AllowedDays, 3, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoWednesday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoWednesday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,4, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoThursday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoThursday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,5, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoFriday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoFriday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,6, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoSaturday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoSaturday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,7, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoSunday, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoSunday, 0) = 0'
			  --END IF
		  --END IF
			IF SUBSTRING(@AllowedDays,8, 1) = '1' --THEN
				IF @DayTimeSQL = '' --THEN
					SET @DayTimeSQL = @DayTimeSQL + ' AND IsNull(ITM.NoPubHol, 0) = 0'
				ELSE
					SET @DayTimeSQL = 'IsNull(ITM.NoPubHol, 0) = 0'
			  --END IF
		  --END IF
		  IF  @DayTimeSQL <> '' --THEN
			SET @DayTimeSQL = ' AND (' + @DayTimeSQL  + ')'
      --END IF
        --@DayTimeSQL = @DayTimeSQL +
        -- AND (' &
        --(ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) OR (ISNULL(ITM.MinDurtn, ' & @Duration & ') <= ' & @Duration & _
        -- AND IsNull(ITM.MaxDurtn, ' & @Duration & ') >= ' & @Duration & '))'
  --END IF
  declare @PayPeriodEndDate varchar(50);
  select @PayPeriodEndDate=convert(varchar,convert(datetime,PayPeriodEndDate),111)  from SysTable
  
 set @GroupBy1=' GROUP BY SO.[Service Type],itm.Title ,Itm.RosterGroup, Itm.AMOUNT, isnull(SO.[Unit Pay Rate],0) ,isnull(SO.TaxRate,0) ,Itm.unit,itm.HACCType,FixedTime,AutoApprove,SO.ServiceBiller,itm.Jobtype';
  set @GroupBy2=' GROUP BY SO.[Service Type],itm.Title ,Itm.RosterGroup, Itm.AMOUNT,hrt.GST, isnull(SO.[Unit Pay Rate],0) ,isnull(SO.TaxRate,0) ,Itm.unit,itm.HACCType,FixedTime,AutoApprove,SO.ServiceBiller,itm.Jobtype';
  set @GroupBy3=' GROUP BY SO.[Service Type],itm.Title ,Itm.RosterGroup, Itm.AMOUNT,hrt.GST, isnull(SO.[Unit Pay Rate],0) ,isnull(SO.TaxRate,0) ,Itm.unit,itm.HACCType,SO.ForceSpecialPrice ,re.BillingMethod,Itm.PRICE2,Itm.PRICE3,Itm.PRICE4,Itm.PRICE5,Itm.PRICE6,SO.[UNIT BILL RATE],So.[Unit Pay Rate],re.AgencyDefinedGroup,FixedTime,AutoApprove,SO.ServiceBiller,itm.Jobtype';


  set @Columns_1=' isnull(SO.[Service Type],itm.Title) AS Activity,Itm.RosterGroup,         
        Itm.AMOUNT AS BILLRATE,itm.Jobtype,
        isnull(SO.[Unit Pay Rate],0) as payrate,isnull(SO.TaxRate,0) as TaxRate,0 as GST,Itm.unit as UnitType,
        (select case when UseAwards=1 then ''AWARD'' ELSE ''N/A'' END from registration) as Service_Description,
        itm.HACCType,'''' as Anal,''' + @PayPeriodEndDate + ''' as date_Timesheet,
		FixedTime,AutoApprove,ISNULL(SO.ServiceBiller,'+ @Recipient +') as BillTo';

  set @Columns_2=' isnull(SO.[Service Type],itm.Title) AS Activity,Itm.RosterGroup,         
        Itm.AMOUNT AS BILLRATE,itm.Jobtype,
        isnull(SO.[Unit Pay Rate],0) as payrate,isnull(SO.TaxRate,0) as TaxRate,isnull(hrt.GST,0) as GST,Itm.unit as UnitType,
         (select case when UseAwards=1 then ''AWARD'' ELSE ''N/A'' END from registration) as Service_Description,
        itm.HACCType,'''' as Anal,''' + @PayPeriodEndDate + ''' as date_Timesheet,
		FixedTime,AutoApprove,ISNULL(SO.ServiceBiller,' + @Recipient + ') as BillTo';

		

	set @Columns_3=' So.[Service Type] AS Activity,Itm.RosterGroup,itm.Jobtype,
            (CASE WHEN ISNULL(SO.ForceSpecialPrice,0) = 0 THEN
            (CASE WHEN re.BillingMethod = ''LEVEL1'' THEN Itm.PRICE2
             WHEN re.BillingMethod = ''LEVEL2'' THEN Itm.PRICE3
             WHEN re.BillingMethod = ''LEVEL3'' THEN Itm.PRICE4
             WHEN re.BillingMethod = ''LEVEL4'' THEN Itm.PRICE5
             WHEN re.BillingMethod = ''LEVEL5'' THEN Itm.PRICE6
            ELSE Itm.Amount END)
            ELSE SO.[UNIT BILL RATE] END ) AS BILLRATE,
            isnull(So.[Unit Pay Rate],0) as payrate,isnull(So.TaxRate,0) as TaxRate,isnull(hrt.GST,0) as GST,Itm.unit as UnitType,
            (select case when UseAwards=1 then ''AWARD'' ELSE '''' END from registration) as Service_Description,
            itm.HACCType,re.AgencyDefinedGroup as Anal,'''+ @PayPeriodEndDate + ''' as date_Timesheet,
			FixedTime,AutoApprove, ISNULL(SO.ServiceBiller,'+ @Recipient +') as BillTo'

	SET @Extent = ''
	--SET @BaseItemTypesSQL = 'SELECT ' + @Columns_1 + ' FROM ItemTypes itm WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(EndDate, '''') = ''''  OR EndDate > GetDate()) ';
	SET @BaseItemTypesSQL = 'SELECT '+ @Columns_1 + '  FROM ItemTypes itm
							 LEFT JOIN ServiceOverview  SO ON  itm.Title = so.[Service Type]
							 WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) ';
							
	

	--SET @BaseProgServSQL = 'SELECT ' + @Columns +' FROM ServiceOverview SO INNER JOIN HumanResourceTypes pr ON CONVERT(nVarchar, pr.RecordNumber) = SO.PersonID INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) '

	SET @BaseProgServSQL = 'SELECT '+ @Columns_2 +'  FROM ServiceOverview SO
							INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title 
							INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
							WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) 
							AND EXISTS
							(SELECT Title
							FROM ItemTypes I
							WHERE Title = SO.[Service Type] 
							AND I.[Status] = ''ATTRIBUTABLE'' AND (I.EndDate Is Null OR I.EndDate >= convert(varchar,getdate(),111))) ';							
    
	

    
	--SET @BaseRecipServSQL = 'SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN Recipients re ON SO.PersonID = re.UniqueID INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) '
	SET @BaseRecipServSQL = 'SELECT ' + @Columns_3 + '  FROM ServiceOverview SO 
							INNER JOIN Recipients re ON SO.PersonID = re.UniqueID 
							INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title 
							LEFT JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
							WHERE ProcessClassification = ''OUTPUT'' AND (ISNULL(itm.EndDate, '''') = ''''  OR itm.EndDate > GetDate()) 
							AND EXISTS
							(SELECT Title
							FROM ItemTypes I
							WHERE Title = SO.[Service Type] 
							AND I.[Status] = ''ATTRIBUTABLE'' AND (I.EndDate Is Null OR I.EndDate >= convert(varchar,getdate(),111))) ';
							
    

           
--SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO
--INNER JOIN HumanResourceTypes pr ON CONVERT(nVarchar, pr.RecordNumber) = SO.PersonID
--INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title
--WHERE ProcessClassification = 'OUTPUT' AND (ISNULL(itm.EndDate, '') = ''  OR itm.EndDate > GetDate())
--AND pr.Name = 'CBI-78001'
    SET @MainGroup = CASE WHEN @MainGroup = 'ALL' THEN '' ELSE @MainGroup END
	--ITEM LIST - UNFILTERED
	SET @AllSQL = @BaseItemTypesSQL + @GroupBy1 + '  ORDER BY [Service Type]'
	--SELECT @AllSQL FROM Registration
	--EXEC (@AllSQL)
    --ITEM LIST FILTERED BY ROSTER GROUP ONLY
	--SET @MainGroup = '''ADMISSION'''
    SET @MainGroupSQL = @BaseItemTypesSQL + ' AND itm.RosterGroup = ' + @MainGroup + @GroupBy1 + ' ORDER BY [Service Type]'
	--SELECT @MainGroupSQL FROM Registration
	--EXEC (@MainGroupSQL)
    --ITEM LIST FILTERED BY MEAL SUBGROUP
	 if (@SubGroup<>'') SET @SubGroup = '''' + @SubGroup + ''''
	SET @SubGroupSQL = @BaseItemTypesSQL +  ' AND itm.MinorGroup = ' + @SubGroup + @GroupBy1 + ' ORDER BY [Service Type]'
	--SELECT @SubGroupSQL AS SubGroupLinked FROM Registration
	--EXEC (@SubGroupSQL)
    --SERVICE OVERVIEW LIST FILTERED BY PROGRAM APPROVED SERVICES
    SET @ProgramLinkedSQL = @BaseProgServSQL
							+ CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND HRT.Name = ' + @Program END
							+ CASE WHEN ISNULL(@MainGroup, 'ALL') = 'ALL' THEN '' ELSE ' AND itm.RosterGroup = ' + @MainGroup END
							+ @GroupBy2 + ' ORDER BY [Service Type]'
	--SELECT @ProgramLinkedSQL AS ProgLinked FROM Registration
	--EXEC (@ProgramLinkedSQL)
	   
    --'ITEM LIST FILTERED BY RECIPIENT APPROVED
	if (@Recipient<>'''!MULTIPLE''')
	begin
		SET @RecipientLinkedSQL = @BaseRecipServSQL					
							  + ' AND re.AccountNo = ' + @Recipient
							  + CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND so.ServiceProgram = ' + @Program END
							  + @GroupBy3 + ' ORDER BY [Service Type]';
	end
	else
	begin
		SET @RecipientLinkedSQL = @BaseRecipServSQL
							 
							  + CASE WHEN ISNULL(@Program, 'ALL') = 'ALL' THEN '' ELSE ' AND so.ServiceProgram = ' + @Program END
							  + @GroupBy3 + ' ORDER BY [Service Type]';

	end
	--ITEM LIST FILTERED BY MEAL SUBGROUP
	IF (REPLACE(@SubGroup,'''','')<>'')
	BEGIN
        set @MealLinkedSQL = @SubGroupSQL;
    END

	--SELECT @RecipientLinkedSQL AS ProgLinked FROM Registration
	--EXEC (@RecipientLinkedSQL)
	SET @FinalSQL ='';
    If (@FORCEAll = 1) --THEN
		SET @FinalSQL =
        CASE
			WHEN @MainGroup = 'ALL' THEN @AllSQL
			ELSE @MainGroupSQL
		END
    ELSE
		IF @ViewType = 'STAFF' --THEN
            if (@Recipient<>'''!MULTIPLE''')
				SET @FinalSQL =
                CASE
					WHEN REPLACE(@MainGroup,'''','') IN ('ADMISSION', 'TRAVELTIME','GROUPACTIVITY')  THEN @ProgramLinkedSQL 
					WHEN REPLACE(@MainGroup,'''','') =  'ITEM' THEN @MainGroupSQL
					ELSE @RecipientLinkedSQL
				END
			ELSE
				SET @FinalSQL =
				CASE WHEN REPLACE(@SubGroup,'''','') <> '' THEN @MealLinkedSQL
				ELSE @ProgramLinkedSQL
				END
            --END IF
        ELSE
			SET @FinalSQL =
            CASE WHEN  REPLACE(@SubGroup,'''','') <> '' THEN @MealLinkedSQL
            ELSE
                CASE
					WHEN REPLACE(@MainGroup,'''','') IN ('ADMISSION', 'TRAVELTIME','GROUPACTIVITY')  THEN @ProgramLinkedSQL
					WHEN REPLACE(@MainGroup,'''','') =  'ITEM' THEN @MainGroupSQL
					ELSE @RecipientLinkedSQL
				END
            END
		
		if (REPLACE(@MainGroup,'''','')='ADMINISTRATION' and REPLACE(@Recipient,'''','')='!INTERNAL')
		begin

			set @FinalSQL='SELECT' + @Columns_1+ 
						' FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID' +
						' INNER JOIN ItemTypes itm ON so.[Service Type] = itm.Title ' +	 
						' WHERE SO.ServiceProgram = ' + @Program +' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = ''ADMINISTRATION'' AND ITM.[Status] = ''NONATTRIBUTABLE'' AND (ITM.EndDate Is Null OR ITM.EndDate >= convert(varchar,getDate(),111))) ORDER BY [Service Type]'
		end
		print(@FinalSQL)
		
		EXEC (@FinalSQL)
		
		
END




/****** Object:  StoredProcedure [dbo].[GetAdminPrograms]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetAdminPrograms]@UniqueId varchAR(MAX)AS SELECT DISTINCT Upper([name]) AS Name , [Type]FROM   humanresourcetypes        LEFT JOIN (SELECT [program]                   FROM   recipientprograms                   WHERE  personid = @UniqueId                          AND programstatus IN ( 'REFERRAL', 'ACTIVE',                                                 'WAITING LIST',                                                 'ON HOLD' )                   GROUP  BY program) AS RPRog1               ON RProg1.program = humanresourcetypes.NAME        LEFT JOIN (SELECT DISTINCT [program]                   FROM   recipientprograms RP                          INNER JOIN humanresourcetypes HRT                                  ON RP.[program] = HRT.NAME                   WHERE  Isnull(useryesno2, 0) = 1                          AND programstatus IN ( 'REFERRAL', 'ACTIVE',                                                 'WAITING LIST',                                                 'ON HOLD' )                  ) AS RProg2               ON RProg2.program = humanresourcetypes.NAME WHERE  ( ( ( [group] = 'PROGRAMS' )            AND ( enddate IS NULL                   OR enddate >= '03-13-2019' )            AND ( Isnull(useryesno2, 0) = 0 )            AND ( RPRog1.program IS NULL ) )           OR ( ( Isnull(useryesno2, 0) = 1 )                AND ( RProg2.program IS NULL )                AND ( enddate IS NULL                       OR enddate >= '03-13-2019' ) ) )        AND Isnull(user2, '') <> 'Contingency'        AND Isnull(useryesno3, 0) <> 1;
GO
/****** Object:  StoredProcedure [dbo].[GetAdminServices]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetAdminServices]@Program varchAR(MAX),@TabType varchAR(MAX)AS    IF(@Program <> '*HCP REFERRAL') AND (@Program <> '*NDIA REFERRAL')  BEGIN	   SELECT [service type] AS Activity 		FROM   serviceoverview SO 			   INNER JOIN humanresourcetypes HRT 					   ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid 		WHERE  HRT.[name] = @program 			   AND EXISTS (SELECT title 						   FROM   itemtypes ITM 						   WHERE  title = SO.[service type] 								  AND ITM.[rostergroup] IN ( 'ADMISSION' ) 								  AND ITM.[minorgroup] IN (@TabType) 								  AND ( ITM.enddate IS NULL 										 OR ITM.enddate >= (select convert(varchar, getdate(), 110)) )) 		ORDER  BY [service type];   END  ELSE 	  BEGIN		SELECT [Title] FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ADMISSION') AND [MinorGroup] IN (@TabType)		AND (EndDate Is Null OR EndDate >= (			select CONVERT(VARCHAR(10), GETDATE(), 110)		)) ORDER BY [Title]	  END  
GO
/****** Object:  StoredProcedure [dbo].[getAllTaskList]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[getAllTaskList] (@carer_Code varchar(50))
		AS BEGIN    

		SELECT [client code], 
			   roTA.rosterid, 
			   roTA.recordno, 
			   CASE 
				 WHEN roTA.taskcomplete = 1 THEN 'U' 
				 ELSE '-' 
			   END              AS TaskCOmplete, 
			   ddTA.description AS TaskDetail 
		FROM   roster_tasklist roTA 
			   INNER JOIN datadomains ddTA 
					   ON roTA.taskid = ddTA.recordnumber 
						  AND domain = 'TASK' 
			   JOIN roster r 
				 ON r.recordno = roTA.rosterid 
		WHERE  r.[carer code] = @carer_code 
					AND Abs(Datediff(dy, date, Getdate())) <= (select top 1  isnull(MobileFutureLimit,10) from UserInfo with (nolock) where staffCode=@carer_Code)
		ORDER  BY [client code], 
				  roTA.rosterid 
		END
GO
/****** Object:  StoredProcedure [dbo].[GetBillDetails]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetBillDetails]

@reAccountNo VARCHAR(max),

@hrName VARCHAR (max),

@itTitle VARCHAR (max)

AS

BEGIN

 

SELECT TOP 1 * FROM

(

SELECT 0 AS Precedence, so.[Unit Bill Rate] AS Amount, so.UnitType AS Unit, so.ServiceBiller AS Debtor, so.TaxRate AS GST

       FROM ServiceOverview so

       INNER JOIN RECIPIENTS re ON re.UniqueID = so.PersonID

       WHERE

                     re.Accountno = @reAccountNo

              AND so.ServiceProgram = @hrName

              AND so.[Service Type] = @itTitle

              AND ISNULL(so.ForceSpecialPrice, 0) = 1

 

              UNION

 

              SELECT 1 AS Precedence,

                     CASE WHEN re.BillingMethod = 'COMMERCIAL' THEN  it.Amount

                            WHEN re.BillingMethod = 'PERCENTAGE' THEN it.Amount * (ISNULL(re.PercentageRate, 0)/100)

                           WHEN re.BillingMethod = 'FIXED' THEN 0

                           WHEN re.BillingMethod = 'LEVEL1' THEN it.Price2

                           WHEN re.BillingMethod = 'LEVEL2' THEN it.Price3

                           WHEN re.BillingMethod = 'LEVEL3' THEN it.Price4

                           WHEN re.BillingMethod = 'LEVEL4' THEN it.Price5

                           WHEN re.BillingMethod = 'LEVEL5' THEN it.Price6

                           ELSE it.Amount

                     END AS Amount, it.Unit AS Unit, re.BillTo AS Debtor, pr.GSTRate AS GST

                     FROM ItemTypes it

                     INNER JOIN Recipients re ON re.Accountno = @reAccountNo

                     INNER JOIN HumanResourceTypes pr ON pr.[Name] = @hrName

                     WHERE it.Title = @itTitle

) t ORDER BY Precedence

 

END
GO
/****** Object:  StoredProcedure [dbo].[getBookingRoster]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[getBookingRoster](@CarerCode VARCHAR(max),                                          @username  VARCHAR(max),                                          @RecordNo  INT =0) AS   BEGIN       SELECT recordno,              [carer code],              [client code],              servicesetting,              [date],              duration,              program,              [service type],              [dayno],              [monthno],              [yearno],              [blockno],              [service description],              r.[notes],              r.[type],              km,              r.billto                                    AS Debtor,              Isnull(itm.infoonly, 0)                     AS InfoOnly,              Isnull(itm.displayfeeinapp, 0)              AS DisplayFeeInApp,              Isnull(itm.displaydebtorinapp, 0)           AS DisplayDebtorInApp,              Isnull(itm.itemtype, '')                    AS ItemType,              Isnull(itm.minorgroup, '')                  AS minorgroup,              Isnull(ht.useryesno6, 0)                    AS MyOwnCarStatus,              Isnull(r.disable_shift_start_alarm, 0)      AS Disable_Shift_start_Alarm,              Isnull(r.disable_shift_end_alarm, 0)        AS Disable_Shift_End_Alarm,              Isnull(ta_multishift, 0)                    AS TA_MultiShift,              Isnull(lo.addressline1, '') + ' '              + Isnull(lo.suburb, '')                     AS Address,              CASE                WHEN [start time] = '24:00' THEN '00:00'                ELSE [start time]              END                                         AS [start time],              CASE                WHEN Isnull(r.ta_excludegeolocation, '') = '' THEN 0                ELSE r.ta_excludegeolocation              END                                         AS              TA_EXCLUDEGEOLOCATION,              CASE                WHEN Isnull(itm.ta_excludegeolocation, '') = '' THEN 0                ELSE itm.ta_excludegeolocation              END                                         AS              TA_EXCLUDEGEOLOCATION2              ,              CASE                WHEN Isnull(itm.appexclude1, '') = '' THEN 0                ELSE itm.appexclude1              END                                         AS appexclude1,              CASE                WHEN Isnull(itm.itemtype, '') <> '' THEN itm.itemtype                WHEN Isnull(itm.billtext, '') <> '' THEN itm.billtext                ELSE itm.title              END                                         AS MTAServiceType,              ( r.billqty * r.[unit bill rate] ) +              CASE                WHEN Isnull(ht.gstrate, 0) = 0 THEN 0                ELSE              ( r.billqty * r.[unit bill rate] ) * ( ht.gstrate / 100 )                                                   END    AS Fee,              Year(ez.lodatetime)                         AS test,              CASE                WHEN Year(Isnull(ez.datetime, '1900')) = '1900' THEN 0                ELSE 1              END                                         AS started,              CASE                WHEN Year(Isnull(ez.lodatetime, '1900')) = '1900' THEN 0                ELSE 1              END                                         AS completed,              Isnull(ez.datetime, 0)                      AS started1,              Isnull(ez.lodatetime, 0)                    AS completed1,              CASE                WHEN Isnull(r.tamode, '') = '' THEN                  CASE                    WHEN Isnull(itm.ta_loginmode, -1) = -1 THEN                    (SELECT TOP 1 Isnull(tmmode, 0)                     FROM   userinfo                     WHERE  NAME = @username                            AND staffcode = @CarerCode)                    ELSE itm.ta_loginmode                  END                ELSE r.tamode              END                                         AS TA_LOGINMODE2,              (SELECT TOP 1 NAME               FROM   humanresources h WITH (nolock),                      recipients rp WITH (nolock)               WHERE  h.[type] = 'RECIPTYPE'                      AND Isnull(mobilealert, 0) = 1                      AND h.personid = rp.uniqueid                      AND rp.accountno = r.[client code]) AS [Group],              (SELECT Count(DISTINCT h.NAME)               FROM   humanresources h WITH (nolock),                      recipients rp WITH (nolock)               WHERE  h.[type] = 'RECIPTYPE'                      AND Isnull(mobilealert, 0) = 1                      AND h.personid = rp.uniqueid                      AND rp.accountno = r.[client code]) AS group_alerts       FROM   roster r              INNER JOIN recipients re                      ON re.accountno = r.[client code]              INNER JOIN itemtypes itm                      ON itm.title = r.[service type]                         AND processclassification IN ( 'OUTPUT', 'EVENT' )              INNER JOIN humanresourcetypes ht                      ON r.[program] = ht.[name]                         AND ht.[group] = 'PROGRAMS'              INNER JOIN userinfo us                      ON us.staffcode = r.[carer code]              LEFT JOIN ezitracker_log ez                     ON ez.jobno = r.recordno              LEFT JOIN cstdaoutlets lo                     ON lo.[name] = r.servicesetting       WHERE  [carer code] = 'BOOKED'              AND ( recordno < @RecordNo                     OR @RecordNo = 0 )              AND Abs(Datediff(dy, date, Getdate())) <=                  Isnull(mobilefuturelimit, 10)              AND Isnull(itm.appexclude1, 0) = 0              AND re.branch IN (SELECT stf_department                                FROM   staff                                WHERE  accountno = @CarerCode)       ORDER  BY CONVERT(DATE, [date]) DESC,                 CONVERT(DATETIME, [start time]),                 recordno   END 
GO


GO
/****** Object:  StoredProcedure [dbo].[getDayManager]    Script Date: 07/06/2022 07:42:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE OR Alter PROCEDURE [dbo].[getDayManager](@sDate varchar(10),@eDate varchar(10), @dmType varchar(10))
As
begin
 if (@dmType='1')
	 SELECT DISTINCT
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity,  ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], 
	isnull([ro].[ShiftName],'NEVER ASSIGNED') as [ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 1 OR DMStat = 9 )) t  
	ORDER BY [Date], ShiftName, [Start Time], Recipient

 else if (@dmType='2' or @dmType='0' or isnull(@dmType,'')='' )
 --Staff Management
	 SELECT DISTINCT
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED'
	 WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], isnull([ro].[ShiftName],'NEVER ASSIGNED') as [ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	FROM Roster ro LEFT JOIN Recipients ON 
	[ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo] 
	 LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') 
	--AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 3 OR [ro].[Type] = 5 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 OR [ro].[Type] = 1 OR [ro].[Type] = 13 OR ([ro].[Type] = 6 AND [ItemTypes].[MinorGroup] <> 'LEAVE') OR ([ItemTypes].[MinorGroup] = 'LEAVE'))
	) t  
	ORDER BY Staff, Date, [Start Time], Recipient

 else if (@dmType='3')
 --Transport Recipients
	 SELECT DISTINCT 
	ServiceSetting as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') 
	AND ([ItemTypes].[RosterGroup] = 'TRANSPORT' )
	
	) t 
	ORDER BY ServiceSetting, Date, [Start Time], Recipient
else if(@dmType='4')
--Transport Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') 
	AND ([ItemTypes].[RosterGroup] = 'TRANSPORT' )
	) t 
	ORDER BY [Setting/Location], Date, [Start Time], Staff

--else if(@dmType='5')
	--Transport Daily Planner

else if(@dmType='6')
--Facilities Recipients
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
		[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
 
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'CENTREBASED' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Recipient
	
else if(@dmType='7')
--Facilities Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
	
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'CENTREBASED' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Staff
else if(@dmType='8')
--Group Recipients
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

    FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'GROUPACTIVITY' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Recipient, Activity

else if(@dmType='9')
--Group Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'GROUPACTIVITY' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], [Carer Code]
else if(@dmType='10')
	--Grp/Trns/Facility- Recipients
	SELECT DISTINCT 
	 [Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype
		
	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 )) t  
	ORDER BY  [Setting/Location], Date, [Start Time], Recipient
else if(@dmType='11')
	--Grp/Trns/Facility-Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode, httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
   ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON 
	[ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 3 OR [ro].[Type] = 5 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 OR [ro].[Type] = 1 OR [ro].[Type] = 13 OR ([ro].[Type] = 6 AND [ItemTypes].[MinorGroup] <> 'LEAVE') OR ([ItemTypes].[MinorGroup] = 'LEAVE') )) t  
	ORDER BY [Setting/Location], Date, [Start Time], [Carer Code]

Else if(@dmType='12')
	--Recipient Management
	SELECT DISTINCT 
	recipient [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],paytype,BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,httype,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, ro.[service Description] as paytype, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
   ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode,pr.type as httype

	FROM Roster ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch] FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.Program
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') 
	--AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 )
	) t  
	ORDER BY Recipient, Date, [Start Time], Staff

END


/****** Object:  StoredProcedure [dbo].[getDayManagerResources]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER    procedure [dbo].[getDayManagerResources](@resourceType varchar(100), @rdate varchar(10), @CarerCode varchar(100), @RecipientCode varchar(100))
AS
begin

		  
		  Declare @PayPeriodEndDate varchar(10);
		  Declare @PayPeriodLength int
		  Declare @WorkingPayStart varchar(10);
		  

		  if (@rdate is null)
			set @rdate=CONVERT(varchar,getDate(),111);

		  select @PayPeriodEndDate=CONVERT(varchar,PayPeriodEndDate,111) from SysTable;
          select @PayPeriodLength=DefaultPayPeriod from Registration;
          set @WorkingPayStart = convert(varchar, DateAdd(Day, -1 * (@PayPeriodLength - 1), @PayPeriodEndDate),111);
		
	If (@resourceType='Output')	
		SELECT DISTINCT HACCType as description FROM ItemTypes WHERE ProcessClassification IN ('OUTPUT', 'EVENT') AND IT_DATASET IN ('NDIA', 'HACC', 'CSTDA', 'NRCP', 'NRCP-SAR', 'DEX') ORDER BY HACCType	
	else if (@resourceType='Program')
		SELECT UPPER([Name]) as description FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' ORDER BY [Name]	
	else if (@resourceType='Activity')
		SELECT Title as description FROM ItemTypes WHERE ProcessClassification <> 'INPUT' AND ISNULL(EndDate, '') = '' ORDER BY Title	
	else if (@resourceType='PayType')
		SELECT Title as description FROM ItemTypes WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' And ProcessClassification = 'INPUT' ORDER BY TITLE	
	else if (@resourceType='Debtor')
		SELECT UPPER([Accountno]) as description FROM Recipients WHERE (ISNULL(DischargeDate, '') = '') ORDER BY [AccountNo]
	else If (@resourceType='Resource')
	Begin
		SELECT DISTINCT description
		FROM   datadomains
		WHERE  domain IN ( 'VEHICLES', 'ACTIVITYGROUPS' )
			   AND ( enddate IS NULL
					  OR enddate >= @WorkingPayStart )
			   AND description IS NOT NULL
			   AND ( enddate IS NULL
					  OR enddate >= @WorkingPayStart )
		UNION
		SELECT DISTINCT [name]
		FROM   cstdaoutlets
		WHERE  [name] IS NOT NULL
			   AND Isnull(enddate, DATEADD(Month,1, @rdate)) > @rdate
			   AND ( [name] NOT IN (SELECT DISTINCT rCtr.[name] AS Centre
									FROM   cstdaoutlets rCtr
										   INNER JOIN humanresources rX
												   ON rX.personid = CONVERT(VARCHAR,
																	rCtr.recordnumber)
													  AND rX.[type] = 'CENTRE_STAFFX'
									WHERE  rX.[name] = @carerCode)
					  OR [name] IN (SELECT DISTINCT rCtr.[name] AS Centre
									FROM   cstdaoutlets rCtr
										   INNER JOIN humanresources rX
												   ON rX.personid = CONVERT(VARCHAR,
																	rCtr.recordnumber)
													  AND rX.[type] = 'CENTRE_STAFFI'
									WHERE  rX.[name] = @carerCode) )
			   AND [name] NOT IN (SELECT centre
								  FROM   (SELECT rco.[name] AS Competency,
												 ctr.NAME   AS Centre
										  FROM   humanresources rco
												 INNER JOIN cstdaoutlets ctr
														 ON rco.personid = CONVERT(
																	VARCHAR,
				ctr.recordnumber)
				WHERE  [type] = 'CENTRE_COMP'
				AND Isnull(useryesno1, 0) = 1
				EXCEPT
				SELECT co.[name],
				'x'
				FROM   humanresources co
				INNER JOIN staff st
				ON personid = uniqueid
				AND [type] = 'STAFFATTRIBUTE'
				INNER JOIN datadomains coMaster
				ON co.[name] = coMaster.description
				WHERE  st.accountno = 'ARSHAD'
				AND ( Isnull(coMaster.undated, 0) = 1
				OR Isnull(CONVERT(VARCHAR, co.date1,
				106),
				'1900/01/01')
				>
				@rdate )) t)
				ORDER  BY description 
	END
End
GO
/****** Object:  StoredProcedure [dbo].[getEventData]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER Procedure [dbo].[getEventData](@START_DATE DATETIME , @END_DATE DATETIME)
AS
Begin


SELECT R.uniqueid       AS URN,
       RST.[date]       AS EVENTDATE,
       RST.[start time] AS STARTTIME,
       LEFT(CONVERT(VARCHAR, Dateadd(minute, RST.duration * 5,
            RST.[start time]), 108),
       5)               AS ENDTIME,
       Isnull(I.billtext, '')  AS VISITDESCRIPTION,
       S.FIRSTNAME AS STAFFNAME,
       RST.recordno     AS VISITID,
       RST.type         AS VISITSTATUS
FROM   roster RST
       INNER JOIN recipients R
               ON RST.[client code] = R.accountno
       INNER JOIN itemtypes I
               ON RST.[service type] = I.title
INNER JOIN STAFF S
               ON RST.[CARER CODE] = S.ACCOUNTNO
INNER JOIN HumanResources HRT ON hrt.PersonID =r.uniqueid and hrt.[group]='RECIPTYPE' --and name like '%CORE%'
WHERE  RST.[date] BETWEEN @START_DATE AND @END_DATE
       AND RST.[carer code] NOT IN ( '!INTERNAL', '!MULTIPLE' )
       AND RST.type NOT IN  ( 1, 13 , 5 , 6 )
ORDER  BY RST.[date],
          RST.[start time] 

End
GO
/****** Object:  StoredProcedure [dbo].[getMonthRoster]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[getMonthRoster] (@CarerCode varchar(max), @username varchar(max))AS BEGIN    SELECT        RecordNo,        [carer code],        [client code],        servicesetting,        [Date],        Duration,        Program,        [Service Type],        [Dayno],        [Monthno],        [YearNo],        [blockNo],        [Service Description],        r.[notes],        r.[type],        KM,        r.billto AS Debtor,        ISNULL(itm.InfoOnly, 0) AS InfoOnly,        ISNULL(itm.DisplayFeeInApp, 0) AS DisplayFeeInApp,        ISNULL(itm.DisplayDebtorInApp, 0) AS DisplayDebtorInApp,        ISNULL(itm.ItemType, '') AS ItemType,        ISNULL(itm.minorgroup, '') AS minorgroup,        ISNULL(ht.UserYesNo6, 0) AS MyOwnCarStatus,        ISNULL(r.Disable_Shift_start_Alarm, 0) AS Disable_Shift_start_Alarm,        ISNULL(r.Disable_Shift_End_Alarm, 0) AS Disable_Shift_End_Alarm,        ISNULL(TA_MultiShift, 0) AS TA_MultiShift,        ISNULL(lo.addressline1, '') + ' ' + ISNULL(lo.suburb, '') AS Address,        CASE        WHEN [start time] = '24:00' THEN '00:00'        ELSE REPLACE([start time], '.', ':')        END AS [start time],        CASE        WHEN ISNULL(r.TA_EXCLUDEGEOLOCATION, '') = '' THEN 0        ELSE r.TA_EXCLUDEGEOLOCATION        END AS TA_EXCLUDEGEOLOCATION,        CASE        WHEN ISNULL(itm.TA_EXCLUDEGEOLOCATION, '') = '' THEN 0        ELSE itm.TA_EXCLUDEGEOLOCATION        END AS TA_EXCLUDEGEOLOCATION2,        CASE        WHEN ISNULL(itm.appexclude1, '') = '' THEN 0        ELSE itm.appexclude1        END AS appexclude1,        CASE        WHEN ISNULL(itm.itemtype, '') <> '' THEN itm.itemtype        WHEN ISNULL(itm.billtext, '') <> '' THEN itm.billtext        ELSE itm.title        END AS MTAServiceType,        (r.billqty * r.[unit bill rate]) + CASE        WHEN ISNULL(ht.GSTRate, 0) = 0 THEN 0        ELSE (r.billqty * r.[unit bill rate]) * (ht.GSTRate / 100)        END AS Fee,        YEAR(ez.lodatetime) AS test,        CASE        WHEN YEAR(ISNULL(ez.DateTime, '1900')) = '1900' THEN 0        ELSE 1        END AS started,        CASE        WHEN YEAR(ISNULL(ez.lodatetime, '1900')) = '1900' THEN 0        ELSE 1        END AS completed,        ISNULL(ez.datetime, 0) AS started1,        ISNULL(ez.lodatetime, 0) AS completed1,        CASE        WHEN ISNULL(r.TAMode, '') = '' THEN CASE            WHEN ISNULL(itm.TA_LOGINMODE, -1) = -1 THEN (SELECT TOP 1                    ISNULL(TMMode, 0)                FROM userinfo                WHERE name = @username                AND staffcode = @CarerCode)            ELSE itm.TA_LOGINMODE            END        ELSE r.TAMode        END AS TA_LOGINMODE2,        (SELECT TOP 1            Name        FROM HumanResources h WITH (NOLOCK),             Recipients rp WITH (NOLOCK)        WHERE h.[Type] = 'RECIPTYPE'        AND ISNULL(MobileAlert, 0) = 1        AND h.PersonID = rp.UniqueID        AND rp.AccountNo = r.[Client Code])        AS [Group],        (SELECT            COUNT(DISTINCT h.name)        FROM HumanResources h WITH (NOLOCK),             Recipients rp WITH (NOLOCK)        WHERE h.[Type] = 'RECIPTYPE'        AND ISNULL(MobileAlert, 0) = 1        AND h.PersonID = rp.UniqueID        AND rp.AccountNo = r.[Client Code])        AS group_alerts    FROM Roster r    INNER JOIN Recipients re        ON re.accountno = r.[client code]    INNER JOIN itemtypes itm        ON itm.Title = r.[Service Type]        AND PROCESSCLASSIFICATION IN ('OUTPUT', 'EVENT')    INNER JOIN HumanResourceTypes ht        ON r.[program] = ht.[name]        AND ht.[GROUP] = 'PROGRAMS'    INNER JOIN UserInfo us        ON us.staffcode = r.[carer code]    LEFT JOIN eziTracker_Log ez        ON ez.jobno = r.recordno    LEFT JOIN CSTDAOutlets lo        ON lo.[name] = r.ServiceSetting    WHERE [Carer Code] IN (@CarerCode)    AND ABS(DATEDIFF(DY, date, GETDATE())) <= ISNULL(MobileFutureLimit, 10)    AND ISNULL(itm.appexclude1, 0) = 0    ORDER BY CONVERT(date, [date]) DESC, CONVERT(datetime, [start time]), RecordNo;    IF EXISTS (SELECT            'kill ' + CONVERT(varchar, spid) + '; '        FROM master..sysprocesses        WHERE (blocked > 0)        AND program_name = '.Net SqlClient Data Provider'        AND loginame = 'IIS APPPOOL\TimesheetAppPool')    BEGIN        SET NOCOUNT ON        DECLARE @query varchar(max)        SET @query = ''        SELECT            @query = COALESCE(@query, ',') + 'kill ' + CONVERT(varchar, spid) + '; '        FROM master..sysprocesses        WHERE blocked > 0        AND program_name = '.Net SqlClient Data Provider'        AND loginame = 'IIS APPPOOL\TimesheetAppPool'        IF LEN(@query) > 0        BEGIN            PRINT @query            EXEC (@query)        END    END END
GO
/****** Object:  StoredProcedure [dbo].[getMonthRoster2]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[getMonthRoster2] (@CarerCode VARCHAR(max), 
                                         @username  VARCHAR(max)) 
AS 
  BEGIN 
      SELECT DISTINCT recordno, 
                      [carer code], 
                      [client code], 
					  re.UniqueID as PersonID,
                      servicesetting, 
                      [date], 
                      duration, 
                      program, 
                      [service type], 
                      [dayno], 
                      [monthno], 
                      [yearno], 
                      [blockno], 
                      [service description], 
                      CONVERT (VARCHAR(MAX), r.[notes])                AS notes, 
                      r.[type], 
                      km, 
                      r.billto                                    AS Debtor, 
                      Isnull(itm.infoonly, 0)                     AS InfoOnly, 
                      Isnull(itm.displayfeeinapp, 0)              AS 
                      DisplayFeeInApp, 
                      Isnull(itm.displaydebtorinapp, 0)           AS 
                      DisplayDebtorInApp, 
                      Isnull(re.accountingidentifier, '')         AS 
                      ACCOUNTINGIDENTIFIER, 
                      Isnull(itm.itemtype, '')                    AS ItemType, 
                      Isnull(itm.minorgroup, '')                  AS minorgroup, 
                      Isnull(ht.useryesno6, 0)                    AS 
                      MyOwnCarStatus, 
                      Isnull(r.disable_shift_start_alarm, 0)      AS 
                      Disable_Shift_start_Alarm, 
                      Isnull(r.disable_shift_end_alarm, 0)        AS 
                      Disable_Shift_End_Alarm, 
                      Isnull(ta_multishift, 0)                    AS 
                      TA_MultiShift ,
                      
                     dbo.getPrimaryAddress   (r.[Client Code])                AS Address, 
                      CASE 
                        WHEN [start time] = '24:00' THEN '00:00' 
                        ELSE Replace([start time], '.', ':') 
                      END                                         AS 
                      [start time], 
                      CASE 
                        WHEN Isnull(r.ta_excludegeolocation, '') = '' THEN 0 
                        ELSE r.ta_excludegeolocation 
                      END                                         AS 
                      TA_EXCLUDEGEOLOCATION, 
                      CASE 
                        WHEN Isnull(itm.ta_excludegeolocation, '') = '' THEN 0 
                        ELSE itm.ta_excludegeolocation 
                      END                                         AS 
                      TA_EXCLUDEGEOLOCATION2, 
                      CASE 
                        WHEN Isnull(itm.appexclude1, '') = '' THEN 0 
                        ELSE itm.appexclude1 
                      END                                         AS appexclude1 
                      , 
                      CASE 
                        WHEN Isnull(itm.itemtype, '') <> '' THEN itm.itemtype 
                        WHEN Isnull(itm.billtext, '') <> '' THEN itm.billtext 
                        ELSE itm.title 
                      END                                         AS 
                      MTAServiceType, 
                      ( r.billqty * r.[unit bill rate] ) + CASE 
                                                             WHEN 
                      Isnull(ht.gstrate, 0) = 0 THEN 0 
                                                             ELSE 
                      ( r.billqty * r.[unit bill rate] ) * ( ht.gstrate / 100 ) 
                                                           END    AS Fee, 
                      Year(ez.lodatetime)                         AS test, 
                      CASE 
                        WHEN Year(Isnull(ez.datetime, '1900')) = '1900' THEN 0 
                        ELSE 1 
                      END                                         AS started, 
                      CASE 
                        WHEN Year(Isnull(ez.lodatetime, '1900')) = '1900' THEN 0 
                        ELSE 1 
                      END                                         AS completed, 
                      Isnull(ez.datetime, 0)                      AS started1, 
                      Isnull(ez.lodatetime, 0)                    AS completed1, 
                      CASE 
                        WHEN Isnull(r.tamode, '') = '' THEN 
                          CASE 
                            WHEN Isnull(itm.ta_loginmode, -1) = -1 THEN 
                            (SELECT TOP 1 Isnull(tmmode, 0) 
                             FROM   userinfo 
                             WHERE  NAME = @username 
                                    AND staffcode = @CarerCode) 
                            ELSE itm.ta_loginmode 
                          END 
                        ELSE r.tamode 
                      END                                         AS 
                      TA_LOGINMODE2 
                      , 
                      (SELECT TOP 1 NAME 
                       FROM   humanresources h WITH (nolock), 
                              recipients rp WITH (nolock) 
                       WHERE  h.[type] = 'RECIPTYPE' 
                              AND Isnull(mobilealert, 0) = 1 
                              AND h.personid = rp.uniqueid 
                              AND rp.accountno = r.[client code]) AS [Group], 
                      (SELECT Count(DISTINCT h.NAME) 
                       FROM   humanresources h WITH (nolock), 
                              recipients rp WITH (nolock) 
                       WHERE  h.[type] = 'RECIPTYPE' 
                              AND Isnull(mobilealert, 0) = 1 
                              AND h.personid = rp.uniqueid 
                              AND rp.accountno = r.[client code]) AS 
                      group_alerts, 
					CASE
					WHEN r.[client code] NOT IN ('!INTERNAL', '!MULTIPLE') THEN convert(varchar,r.notes)
					WHEN r.[Client Code] = '!MULTIPLE' AND r.Type = 11 THEN convert(varchar,lo.GeneralComments)
					ELSE ''
					END AS RunsheetAlerts,
					substring(convert(varchar,isnull(ez.[DateTime],''),108),0,6) as Actual_Start,substring(convert(varchar,isnull(ez.LODateTime,''),108),0,6) as Actual_End
                      
      FROM   roster r 
             INNER JOIN recipients re 
                     ON re.accountno = r.[client code] 
             INNER JOIN itemtypes itm 
                     ON itm.title = r.[service type] 
                        AND processclassification IN ( 'OUTPUT', 'EVENT' ) 
             INNER JOIN humanresourcetypes ht 
                     ON r.[program] = ht.[name] 
                        AND ht.[group] = 'PROGRAMS' 
             INNER JOIN userinfo us 
                     ON us.staffcode = r.[carer code] 
             LEFT JOIN ezitracker_log ez 
                    ON ez.jobno = r.recordno 
             LEFT JOIN cstdaoutlets lo 
                    ON lo.[name] = r.servicesetting 
      WHERE  [carer code] IN ( @CarerCode ) 
             AND Abs(Datediff(dy, date, Getdate())) <= 
                 Isnull(mobilefuturelimit, 10) 
             AND Isnull(itm.appexclude1, 0) = 0 
      ORDER  BY [date] DESC, 
                [start time], 
                recordno; 

      IF EXISTS (SELECT 'kill ' + CONVERT(VARCHAR, spid) + '; ' 
                 FROM   master..sysprocesses 
                 WHERE  ( blocked > 0 ) 
                        AND program_name = '.Net SqlClient Data Provider' 
                        AND loginame = 'IIS APPPOOL\TimesheetAppPool') 
        BEGIN 
            SET nocount ON 

            DECLARE @query VARCHAR(max) 

            SET @query = '' 

            SELECT @query = COALESCE(@query, ',') + 'kill ' 
                            + CONVERT(VARCHAR, spid) + '; ' 
            FROM   master..sysprocesses 
            WHERE  blocked > 0 
                   AND program_name = '.Net SqlClient Data Provider' 
                   AND loginame = 'IIS APPPOOL\TimesheetAppPool' 

            IF Len(@query) > 0 
              BEGIN 
                  PRINT @query 

                  EXEC (@query) 
              END 
        END 
  END 
GO

ALTER    PROCEDURE [dbo].[getMTAPending] (@Date VARCHAR(20),
										@LocalTimezoneOffset INTEGER=0,
                                        @Branches varchar(MAX)='',
                                        @Managers varchar(MAX)='',
                                        @JobCategories varchar(MAX)='',
										@teams varchar(MAX)='',
										@TAType int=0
									)
AS 
BEGIN 

DECLARE @sql nVARCHAR(MAX)

--DECLARE @Date VARCHAR(10)
--DECLARE @LocalTimezoneOffset INTEGER
--DECLARE @Branches varchar(MAX)
--DECLARE @Managers varchar(MAX)
--DECLARE @JobCategories varchar(MAX)
--SET @Date = CONVERT(VARCHAR (10), GetDate(), 111)
--SET @LocalTimezoneOffset = 0
--SET @Branches = '
--SET @Managers = '
--SET @JobCategories = '
 
if (@TAType=0)

	SELECT 
	  DISTINCT RecordNo AS Jobno, [Carer Code],
	  CASE WHEN ISNULL(Staff.[LastName], '') <> '' THEN Staff.[LastName] ELSE '' END + ' ' + CASE WHEN ISNULL(Staff.[FirstName], '') <> '' THEN Staff.[FirstName] ELSE '' END + ' ' + CASE WHEN ISNULL(Staff.[STF_CODE], '') <> '' THEN ':' + Staff.[STF_CODE] ELSE '' END AS [Staff], 
	  CASE WHEN Recipients.Accountno = '!MULTIPLE' THEN Roster.ServiceSetting WHEN Recipients.Accountno = '!INTERNAL' THEN 'ADMINISTRATION' WHEN ISNULL(
		Recipients.[Surname/Organisation], 
		''
	  ) <> '' THEN Recipients.[Surname/Organisation] + CASE WHEN ISNULL(Recipients.[FirstName], '') <> '' THEN ', ' + Recipients.[FirstName] ELSE '**NO NAME' END END AS [Recipient], 
	  [Service Type], [Date],
	  [Start Time] AS [Roster Start], 
	  ([Duration] * 5) AS [duration], 
	  Convert(
		nvarchar(5), 
		DateAdd(
		  Minute, 
		  ([Duration] * 5), 
		  [Start Time]
		), 
		114
	  ) As [Roster End], 
	  CASE WHEN DATEADD(
		HOUR, 
		-1 * (
		  CONVERT(
			FLOAT, 
			ISNULL(TIMEZONEOFFSET, 0) -0
		  )
		), 
		ISNULL([Start Time], '')
	  ) < CONVERT(VARCHAR (5), GetDate(), 108)
	  AND ISNULL(Date, '') = CONVERT(VARCHAR (10), GetDate(), 111) THEN 'LATE START' ELSE ' ' END AS S1, 
	  '' AS S2, 
	  DATEDIFF(
		n, 
		DATEADD(
		  HOUR, 
		  -1 * (
			CONVERT(
			  FLOAT, 
			  ISNULL(TIMEZONEOFFSET, 0) -0
			)
		  ), 
		  ISNULL([Start Time], '')
		), 
		CONVERT(VARCHAR (5), GetDate(), 108)
	  ) AS StartVAR, 
	  CASE WHEN ISNULL(ItemTypes.TALateStartTH, 0) = 0 THEN ISNULL(Recipients.PANLateStartTH, 0) ELSE ISNULL(ItemTypes.TALateStartTH, 0) END AS PANLateStartTH, 
	  TA_Multishift ,
	  dbo.getCoordinator_Email( Recipients.AccountNo,'RECIPIENT') as RecipientCoordinatorEmail,
	  dbo.getCoordinator_Email( Staff.AccountNo,'STAFF') as StaffCoordinatorEmail,
	  [Recipients].[AgencyDefinedGroup] As [Category],
	  [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator,
	  [Recipients].[Branch]
	FROM 
	  Roster 
	  INNER JOIN Staff ON Roster.[Carer Code] = Staff.AccountNo 
	  INNER JOIN Recipients ON Roster.[Client Code] = Recipients.AccountNo 
	  LEFT JOIN ItemTypes ON Roster.[Service Type] = ItemTypes.Title 
	WHERE 
	  Date = @Date  
	  AND ISNULL(
		Staff.FLAG_ExcludeFromLogDisplay, 
		0
	  ) = 0 
	  AND ISNULL([ItemTypes].[TAExclude1], 0) = 0 
	  AND [Carer Code] > '!MULTIPLE' 
	  AND (
		ROS_PANZTEL_UPDATED IS NULL 
		OR ROS_PANZTEL_UPDATED = 0
	  ) 
	  AND Roster.[Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12) 
	  AND (@Branches='' or STF_DEPARTMENT IN ( select * from split( @Branches, ',')))  
	  AND (@Managers='' or STAFF.PAN_MANAGER IN (select * from split(@Managers,',')))
	  AND (@JobCategories='' or STAFFGROUP IN ( select * from split(@JobCategories, ',')))
	  AND (@teams='' or [RECIPIENT_CoOrdinator] IN ( select * from split(@teams, ',')))
	  
	ORDER BY [Carer Code], [Start Time]

else if (@TAType=1)

	SELECT 
	  DISTINCT RecordNo AS Jobno, [Carer Code],
	  CASE WHEN ISNULL(Staff.[LastName], '') <> '' THEN Staff.[LastName] ELSE '' END + ' ' + CASE WHEN ISNULL(Staff.[FirstName], '') <> '' THEN Staff.[FirstName] ELSE '' END + ' ' + CASE WHEN ISNULL(Staff.[STF_CODE], '') <> '' THEN ':' + Staff.[STF_CODE] ELSE '' END AS [Staff], 
	  CASE WHEN Recipients.Accountno = '!MULTIPLE' THEN Roster.ServiceSetting WHEN Recipients.Accountno = '!INTERNAL' THEN 'ADMINISTRATION' WHEN ISNULL(
		Recipients.[Surname/Organisation], 
		''
	  ) <> '' THEN Recipients.[Surname/Organisation] + CASE WHEN ISNULL(Recipients.[FirstName], '') <> '' THEN ', ' + Recipients.[FirstName] ELSE '**NO NAME' END END AS [Recipient], 
	  [Service Type],  [Date],
	  [Start Time] AS [Roster Start], 
	  ([Duration] * 5) AS [duration], 
	  Convert(
		nvarchar(5), 
		DateAdd(
		  Minute, 
		  ([Duration] * 5), 
		  [Start Time]
		), 
		114
	  ) As [Roster End], 
	  CASE WHEN DATEADD(
		HOUR, 
		-1 * (
		  CONVERT(
			FLOAT, 
			ISNULL(TIMEZONEOFFSET, 0) -0
		  )
		), 
		ISNULL([Start Time], '')
	  ) < CONVERT(VARCHAR (5), GetDate(), 108)
	  AND ISNULL(Date, '') = CONVERT(VARCHAR (10), GetDate(), 111) THEN 'LATE START' ELSE ' ' END AS S1, 
	  '' AS S2, 
	  DATEDIFF(
		n, 
		DATEADD(
		  HOUR, 
		  -1 * (
			CONVERT(
			  FLOAT, 
			  ISNULL(TIMEZONEOFFSET, 0) -0
			)
		  ), 
		  ISNULL([Start Time], '')
		), 
		CONVERT(VARCHAR (5), GetDate(), 108)
	  ) AS StartVAR, 
	  CASE WHEN ISNULL(ItemTypes.TALateStartTH, 0) = 0 THEN ISNULL(Recipients.PANLateStartTH, 0) ELSE ISNULL(ItemTypes.TALateStartTH, 0) END AS PANLateStartTH, 
	  TA_Multishift ,
	  elz.DateTime as ActualStart,
	  Roster.CostQty as pay,
	  Roster.BillQty as bill,
	  roster.Status,
	  TA_EarlyStart, TA_LateStart, TA_EarlyGo, TA_LateGo, TA_TooShort, TA_TooLong, [ServiceSetting], ISNULL(Roster.TA_EXCLUDEFROMAPPALERTS, 0) AS ExcludeFromAlerts
	  ,CASE WHEN elz.AutoLoggedOut = 1 THEN 'FORCED LOG OFF' ELSE 'NORMAL LOG OFF' END AS [CompletionStatus],	  
	  dbo.getCoordinator_Email( Recipients.AccountNo,'RECIPIENT') as RecipientCoordinatorEmail,
	  dbo.getCoordinator_Email( Staff.AccountNo,'STAFF') as StaffCoordinatorEmail
	FROM 
	  Roster 
	  INNER JOIN Staff ON Roster.[Carer Code] = Staff.AccountNo 
	  INNER JOIN Recipients ON Roster.[Client Code] = Recipients.AccountNo 
	  LEFT JOIN ItemTypes ON Roster.[Service Type] = ItemTypes.Title 
	  INNER JOIN EziTracker_log elz on Roster.RecordNo=elz.JobNo and elz.[lodatetime] is null
	WHERE 
	  Date = @Date  
	  AND ISNULL(
		Staff.FLAG_ExcludeFromLogDisplay, 
		0
	  ) = 0 
	  AND ISNULL([ItemTypes].[TAExclude1], 0) = 0 
	  AND [Carer Code] > '!MULTIPLE' 
	  AND (
		ROS_PANZTEL_UPDATED IS NULL 
		OR ROS_PANZTEL_UPDATED = 1
	  ) 
	  AND Roster.[Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12) 
	  AND (@Branches='' or STF_DEPARTMENT IN ( select * from split( @Branches, ',')))  
	  AND (@Managers='' or STAFF.PAN_MANAGER IN (select * from split(@Managers,',')))
	  AND (@JobCategories='' or STAFFGROUP IN ( select * from split(@JobCategories, ',')))
	   AND (@teams='' or [RECIPIENT_CoOrdinator] IN ( select * from split(@teams, ',')))
	ORDER BY [Carer Code], [Start Time]

else if (@TAType=2)

	SELECT 
	  DISTINCT RecordNo AS Jobno, [Carer Code],
	  CASE WHEN ISNULL(Staff.[LastName], '') <> '' THEN Staff.[LastName] ELSE '' END + ' ' + CASE WHEN ISNULL(Staff.[FirstName], '') <> '' THEN Staff.[FirstName] ELSE '' END + ' ' + CASE WHEN ISNULL(Staff.[STF_CODE], '') <> '' THEN ':' + Staff.[STF_CODE] ELSE '' END AS [Staff], 
	  CASE WHEN Recipients.Accountno = '!MULTIPLE' THEN Roster.ServiceSetting WHEN Recipients.Accountno = '!INTERNAL' THEN 'ADMINISTRATION' WHEN ISNULL(
		Recipients.[Surname/Organisation], 
		''
	  ) <> '' THEN Recipients.[Surname/Organisation] + CASE WHEN ISNULL(Recipients.[FirstName], '') <> '' THEN ', ' + Recipients.[FirstName] ELSE '**NO NAME' END END AS [Recipient], 
	  [Service Type],  [Date],
	  [Start Time] AS [Roster Start], 
	  ([Duration] * 5) AS [duration], 
	  Convert(
		nvarchar(5), 
		DateAdd(
		  Minute, 
		  ([Duration] * 5), 
		  [Start Time]
		), 
		114
	  ) As [Roster End], 
	  CASE WHEN DATEADD(
		HOUR, 
		-1 * (
		  CONVERT(
			FLOAT, 
			ISNULL(TIMEZONEOFFSET, 0) -0
		  )
		), 
		ISNULL([Start Time], '')
	  ) < CONVERT(VARCHAR (5), GetDate(), 108)
	  AND ISNULL(Date, '') = CONVERT(VARCHAR (10), GetDate(), 111) THEN 'LATE START' ELSE ' ' END AS S1, 
	  '' AS S2, 
	  DATEDIFF(
		n, 
		DATEADD(
		  HOUR, 
		  -1 * (
			CONVERT(
			  FLOAT, 
			  ISNULL(TIMEZONEOFFSET, 0) -0
			)
		  ), 
		  ISNULL([Start Time], '')
		), 
		CONVERT(VARCHAR (5), GetDate(), 108)
	  ) AS StartVAR, 
	  CASE WHEN ISNULL(ItemTypes.TALateStartTH, 0) = 0 THEN ISNULL(Recipients.PANLateStartTH, 0) ELSE ISNULL(ItemTypes.TALateStartTH, 0) END AS PANLateStartTH, 
	  TA_Multishift ,
	  elz.DateTime as ActualStart,
	  elz.LoDateTime as ActualEnd, 
	  Roster.CostQty as pay,
	  Roster.BillQty as bill,
	  roster.Status,
	  TA_EarlyStart, TA_LateStart, TA_EarlyGo, TA_LateGo, TA_TooShort, TA_TooLong, [ServiceSetting], ISNULL(Roster.TA_EXCLUDEFROMAPPALERTS, 0) AS ExcludeFromAlerts
	  ,CASE WHEN elz.AutoLoggedOut = 1 THEN 'FORCED LOG OFF' ELSE 'NORMAL LOG OFF' END AS [CompletionStatus],
	  dbo.getCoordinator_Email( Recipients.AccountNo,'RECIPIENT') as RecipientCoordinatorEmail,
	  dbo.getCoordinator_Email( Staff.AccountNo,'STAFF') as StaffCoordinatorEmail
                       
	FROM 
	  Roster 
	  INNER JOIN Staff ON Roster.[Carer Code] = Staff.AccountNo 
	  INNER JOIN Recipients ON Roster.[Client Code] = Recipients.AccountNo 
	  LEFT JOIN ItemTypes ON Roster.[Service Type] = ItemTypes.Title 
	  INNER JOIN EziTracker_log elz on Roster.RecordNo=elz.JobNo and elz.[lodatetime] is not null
	WHERE 
	  Date = @Date  
	  AND ISNULL(
		Staff.FLAG_ExcludeFromLogDisplay, 
		0
	  ) = 0 
	  AND ISNULL([ItemTypes].[TAExclude1], 0) = 0 
	  AND [Carer Code] > '!MULTIPLE' 
	  AND (
		ROS_PANZTEL_UPDATED IS NULL 
		OR ROS_PANZTEL_UPDATED = 1
	  ) 
	  AND Roster.[Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12) 
	  AND (@Branches='' or STF_DEPARTMENT IN ( select * from split( @Branches, ',')))  
	  AND (@Managers='' or STAFF.PAN_MANAGER IN (select * from split(@Managers,',')))
	  AND (@JobCategories='' or STAFFGROUP IN ( select * from split(@JobCategories, ',')))
	   AND (@teams='' or [RECIPIENT_CoOrdinator] IN ( select * from split(@teams, ',')))
	ORDER BY [Carer Code], [Start Time]




END


GO
/****** Object:  StoredProcedure [dbo].[getMTAPending_old]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[getMTAPending_old] (@Date VARCHAR(20),
										@LocalTimezoneOffset INTEGER,
                                        @Branches varchar(MAX),
                                        @Managers varchar(MAX),
                                        @JobCategories varchar(MAX))
AS 
BEGIN 

DECLARE @sql nVARCHAR(MAX)

--DECLARE @Date VARCHAR(10)
--DECLARE @LocalTimezoneOffset INTEGER
--DECLARE @Branches varchar(MAX)
--DECLARE @Managers varchar(MAX)
--DECLARE @JobCategories varchar(MAX)
--SET @Date = CONVERT(VARCHAR (10), GetDate(), 111)
--SET @LocalTimezoneOffset = 0
--SET @Branches = ''
--SET @Managers = ''
--SET @JobCategories = ''
 


SET @sql = 
'SELECT 
  DISTINCT RecordNo AS Jobno, [Carer Code],
  CASE WHEN ISNULL(Staff.[LastName], '''') <> '''' THEN Staff.[LastName] ELSE '''' END + '' '' + CASE WHEN ISNULL(Staff.[FirstName], '''') <> '''' THEN Staff.[FirstName] ELSE '''' END + '' '' + CASE WHEN ISNULL(Staff.[STF_CODE], '''') <> '''' THEN '':'' + Staff.[STF_CODE] ELSE '''' END AS [Staff], 
  CASE WHEN Recipients.Accountno = ''!MULTIPLE'' THEN Roster.ServiceSetting WHEN Recipients.Accountno = ''!INTERNAL'' THEN ''ADMINISTRATION'' WHEN ISNULL(
    Recipients.[Surname/Organisation], 
    ''''
  ) <> '''' THEN Recipients.[Surname/Organisation] + CASE WHEN ISNULL(Recipients.[FirstName], '''') <> '''' THEN '', '' + Recipients.[FirstName] ELSE ''**NO NAME'' END END AS [Recipient], 
  [Service Type], 
  [Start Time] AS [Roster Start], 
  ([Duration] * 5) AS [Durtn], 
  Convert(
    nvarchar(5), 
    DateAdd(
      Minute, 
      ([Duration] * 5), 
      [Start Time]
    ), 
    114
  ) As [Roster End], 
  CASE WHEN DATEADD(
    HOUR, 
    -1 * (
      CONVERT(
        FLOAT, 
        ISNULL(TIMEZONEOFFSET, 0) -0
      )
    ), 
    ISNULL([Start Time], '''')
  ) < CONVERT(VARCHAR (5), GetDate(), 108)
  AND ISNULL(Date, '''') = CONVERT(VARCHAR (10), GetDate(), 111) THEN ''LATE START'' ELSE '' '' END AS S1, 
  '''' AS S2, 
  DATEDIFF(
    n, 
    DATEADD(
      HOUR, 
      -1 * (
        CONVERT(
          FLOAT, 
          ISNULL(TIMEZONEOFFSET, 0) -0
        )
      ), 
      ISNULL([Start Time], '''')
    ), 
    CONVERT(VARCHAR (5), GetDate(), 108)
  ) AS StartVAR, 
  CASE WHEN ISNULL(ItemTypes.TALateStartTH, 0) = 0 THEN ISNULL(Recipients.PANLateStartTH, 0) ELSE ISNULL(ItemTypes.TALateStartTH, 0) END AS PANLateStartTH, 
  TA_Multishift 
FROM 
  Roster 
  INNER JOIN Staff ON Roster.[Carer Code] = Staff.AccountNo 
  INNER JOIN Recipients ON Roster.[Client Code] = Recipients.AccountNo 
  LEFT JOIN ItemTypes ON Roster.[Service Type] = ItemTypes.Title 
WHERE 
  Date = ' + @Date  + '
  AND ISNULL(
    Staff.FLAG_ExcludeFromLogDisplay, 
    0
  ) = 0 
  AND ISNULL([ItemTypes].[TAExclude1], 0) = 0 
  AND [Carer Code] > ''!MULTIPLE'' 
  AND (
    ROS_PANZTEL_UPDATED IS NULL 
    OR ROS_PANZTEL_UPDATED = 0
  ) 
  AND Roster.[Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12) 
  AND STF_DEPARTMENT IN (' + @Branches + ')  AND STAFF.PAN_MANAGER IN (' + @Managers +')
  AND STAFFGROUP IN (' + @JobCategories + ')

ORDER BY [Carer Code], [Start Time]'
SELECT @Managers AS s_Managers, @JobCategories AS s_Jobs FROM registration

SELECT @sql FROM Registration

EXEC sp_executesql @sql

END
GO
/****** Object:  StoredProcedure [dbo].[GetNDIACharge]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetNDIACharge] (@Staff AS varchar(50), @ClientNDIANumber AS varchar(50), @ServiceType AS varchar(50))AS BEGIN    SELECT        NDIAStaffLevel,        CASE        WHEN NDIAStaffLevel = 'LEVEL 1' THEN Level1Code        WHEN NDIAStaffLevel = 'LEVEL 2' THEN Level2Code        WHEN NDIAStaffLevel = 'LEVEL 3' THEN Level3Code        WHEN NDIAStaffLevel = 'LEVEL 4' THEN Level4Code        ELSE Level1Code        END AS NDIACode,        CASE        WHEN NDIAStaffLevel = 'LEVEL 1' THEN Level1Rate        WHEN NDIAStaffLevel = 'LEVEL 2' THEN Level2Rate        WHEN NDIAStaffLevel = 'LEVEL 3' THEN Level3Rate        WHEN NDIAStaffLevel = 'LEVEL 4' THEN Level4Rate        ELSE Level1Rate        END AS NDIARate    FROM (SELECT        re.accountno,        staff.NDIAStaffLevel,        so.personid,        so.[service type],        NDIA_ID AS Level1Code,        so.[unit bill rate] AS Level1Rate,        NDIA_LEVEL2 AS Level2Code,        (SELECT TOP 1            [unit bill rate]        FROM serviceoverview so        INNER JOIN itemtypes            ON so.[Service Type] = itemtypes.title        WHERE so.personid = re.uniqueid        AND itemtypes.NDIA_ID = NDIA_LEVEL2        AND ISNULL(ForceSpecialPrice, 0) = 1)        AS Level2Rate,        NDIA_LEVEL3 AS Level3Code,        (SELECT TOP 1            [unit bill rate]        FROM serviceoverview so        INNER JOIN itemtypes            ON so.[Service Type] = itemtypes.title        WHERE so.personid = re.uniqueid        AND itemtypes.NDIA_ID = NDIA_LEVEL3        AND ISNULL(ForceSpecialPrice, 0) = 1)        AS Level3Rate,        NDIA_LEVEL4 AS Level4Code,        (SELECT TOP 1            [unit bill rate]        FROM serviceoverview so        INNER JOIN itemtypes            ON so.[Service Type] = itemtypes.title        WHERE so.personid = re.uniqueid        AND itemtypes.NDIA_ID = NDIA_LEVEL4        AND ISNULL(ForceSpecialPrice, 0) = 1)        AS Level4Rate    FROM ServiceOverview so    INNER JOIN itemtypes it        ON it.title = so.[Service Type]    INNER JOIN recipients re        ON personid = re.uniqueid    INNER JOIN staff        ON staff.accountno = @Staff    WHERE re.NDISNumber = @ClientNDIANumber    AND [service type] = @ServiceType) t1 END
GO
/****** Object:  StoredProcedure [dbo].[GetPackagePayout]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetPackagePayout] (@Client AS varchar(50), @Package AS varchar(50), @EndDate AS varchar(50), @Balance AS varchar(50)) AS 
BEGIN    

SELECT *,
@Balance AS ClientUnspentHomecareAmount,
ClientPercent * CONVERT(decimal, @Balance, 2) AS CareRecipientPortion,        
@Balance - ClientPercent * CONVERT(decimal, @Balance, 2) AS CommonwealthPortion,    
(SELECT TOP 1 [Date] FROM Roster r INNER JOIN ItemTypes i ON r.[Service Type] = i.Title WHERE
 r.Program = @Package AND i.MinorGroup = 'DISCHARGE' ORDER BY DATE Desc) AS CessationDay,
(SELECT SUM(BillQty * [Unit Bill Rate]) FROM Roster r INNER JOIN ItemTypes i ON r.[Service Type] = i.Title WHERE
 r.Program = @Package AND i.MinorGroup = 'EXIT FEE') AS ExitFeeDeducted,
(SELECT SUM(ISNULL(InvoiceHeader.[Invoice Amount], 0) - ISNULL(InvoiceHeader.Paid,0)) AS TotalOS FROM InvoiceHeader WHERE Package = @Package AND Htype = 'I') AS UnpaidHomecareFeesDeducted
FROM 
	(SELECT GovtContribution, ClientContribution, 
	GovtContribution + ClientContribution AS TotalContributions,        
	ROUND(ClientContribution / (GovtContribution + ClientContribution), 4) AS ClientPercent    
	FROM 
		(SELECT        
		-1 * SUM(GovtContribution) AS GovtContribution,        
		-1 * SUM(ClientContribution) AS ClientCOntribution
		
		FROM 
			(SELECT        
				CASE WHEN Type1 = 'GOVMT' THEN ISNULL([Invoice Amount], 0) ELSE 0 END AS GovtContribution,        
				CASE WHEN Type1 <> 'GOVMT' THEN ISNULL([Invoice Amount], 0) ELSE 0 END AS ClientContribution,        
				[Patient Code], Package, [Invoice Amount], hType, Notes, Type1, Type2    
				FROM invoiceheader    
				WHERE     hType <> 'I'    
					  AND [Patient Code] = @Client    
					  AND Package = @Package
			) t0
		) t1
	) t2 

END
GO
/****** Object:  StoredProcedure [dbo].[GetPlanGoalStrategies]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE OR ALTER    PROCEDURE [dbo].[GetPlanGoalStrategies] (@PersonID varchar(20)) AS BEGIN      SELECT        PlanNumber,        GoalID,        D2.StrategyID,        IT.Recnum AS ItemID,          [Careplan Name],        Goal,        Strategy,          CASE        WHEN ISNULL(QL.DIsplayText, '') <> '' THEN QL.DisplayText          ELSE CASE            WHEN ISNULL(IT.BillText, '') <> '' THEN IT.BillText              ELSE IT.Title            END        END AS Activity,          [DocStartDate],        [DocEndDate] AS [End Date],        QteHdrRecordNo      FROM (SELECT DISTINCT        QteHdrRecordNo,        PlanNumber,          GoalID,        StrategyID,        Goal,        Strategy,        [DocStartDate],          [DocEndDate],        PlanName AS [Careplan Name]      FROM (SELECT        DOC_ID,        CONVERT(varchar(10), D.Created, 103) + ' ' + (SELECT              [Name]        FROM UserInfo        WHERE Recnum = D.Author)        AS Created,          CONVERT(varchar(10), D.Modified, 103) + ' ' + (SELECT            [Name]        FROM UserInfo          WHERE Recnum = D.Typist)        AS Modified,        D.Doc# AS CareplanID,          D.Title AS PlanName,        Filename,        D.Status,        D.DocStartDate,          D.DocEndDate,        D.AlarmDate,        D.AlarmText,        GL.User1 AS Goal,          ST.Notes AS Strategy,        GL.RecordNumber AS GoalID,        ST.RecordNumber AS StrategyID,          QH.Doc# AS PlanNumber,        QH.RecordNumber AS QteHdrRecordNo    FROM Documents D      LEFT JOIN qte_hdr QH        ON CPID = DOC_ID    LEFT JOIN HumanResources GL          ON CONVERT(varchar, QH.CPID) = GL.PersonID    LEFT JOIN HumanResources ST          ON CONVERT(varchar, ST.PersonID) = CONVERT(varchar, GL.RecordNumber)      WHERE D.PersonID = @PersonID    AND DOCUMENTGROUP IN ('CAREPLAN', 'CP_QUOTE')      AND (D.DeletedRecord = 0    OR D.DeletedRecord IS NULL)) D1) D2      LEFT JOIN (SELECT        *    FROM Qte_Lne QL    WHERE StrategyID <> 0) QL          ON QL.StrategyID = D2.StrategyID        AND QL.Doc_Hdr_ID = D2.QteHdrRecordNo      LEFT JOIN ItemTypes IT        ON QL.ItemID = IT.Recnum      ORDER BY [DocStartDate], PlanNumber, GoalID, D2.StrategyID, ItemID END 
GO
/****** Object:  StoredProcedure [dbo].[GETQUALIFIEDSTAFF]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER    PROCEDURE [dbo].[GETQUALIFIEDSTAFF] (@FirstRecipient    VARCHAR(500),
                                           @username          VARCHAR(50),
                                           @DATE              VARCHAR(15),
                                           @STARTTIME         VARCHAR(15),
                                           @ENDTIME           VARCHAR(15),
                                           @ENDLIMIT          VARCHAR(15),
                                           @Gender            VARCHAR(15) = NULL
,
                                           @Competencies      VARCHAR(max) =
NULL,
                                           @CompetenciesCount INT = 0,
                                           @TeamFilter        VARCHAR(max) =
NULL,
                                           @EnforcePositions  BIT = 0,
                                           @BranchFilter      VARCHAR(max) = '',
                                           @CategoryFilter    VARCHAR(max) = '',
                                           @ManagerFilter     VARCHAR(max) = '',
                                           @MAXDISTANCE       INT = 9999,
                                           @Exclusions        VARCHAR(max) = '',
                                           @LimitedStaff      VARCHAR(max) = '',
                                           @RecordType        VARCHAR(max)=NULL)
AS
  BEGIN
      DECLARE @DURATION INT
      DECLARE @LAT FLOAT
      DECLARE @LONG FLOAT
      DECLARE @PERSONID VARCHAR(max)
      DECLARE @PPSTART VARCHAR(max)
      DECLARE @PPEND VARCHAR(max)

      SET @DURATION = Datediff(minute, CONVERT(TIME, @ENDTIME, 114),
                      CONVERT(TIME, @STARTTIME, 114));

      SELECT @PPSTART = Dateadd(day, -1 * defaultpayperiod, payperiodenddate),
             @PPEND = payperiodenddate
      FROM   systable,
             registration;

      SELECT @LAT = latitude,
             @LONG = longditude
      FROM   recipients
             INNER JOIN namesandaddresses
                     ON personid = uniqueid
      WHERE  accountno = @FirstRecipient
             AND Isnull(namesandaddresses.primaryaddress, 0) = 1;

      SELECT  
	    uniqueid as ID,
		HRS_FNIGHTLY_MIN as hfmin,
		HRS_FNIGHTLY_MAX as hfmax,
		Type,	  
		Suburb,
		t4.[accountno] ,
		[name],
		staffgroup as Category,
		gender  ,
		Age, 
		rating,
		alerts,
		team,
		CoOrdinator, 
		Contact,
		jobweighting,
		specialskills,
		Branch,
		lastknownlatlong as lastKnownLocation,
		StaffLastLat + ', ' + StaffLastLong as address,
		StaffLastLat,
		StaffLastLong,
		ClientLat,
		ClientLong, 
		distance,
		Assessment,
		CostFactor,
		filephoto
			  
      FROM   (SELECT 
					uniqueid,
					HRS_FNIGHTLY_MIN ,
					HRS_FNIGHTLY_MAX ,
					Type,
					Suburb,
					filephoto,
                     [accountno],
                     [name],
                     staffgroup,
                     gender,
                     Isnull(CONVERT(VARCHAR, age), '?') AS Age,
                     rating,
					 alerts,
					 team,
					 CoOrdinator,
					 Contact,
                     jobweighting,
                     specialskills,
					 Branch,
                     lastknownlatlong,
                     latitude                           AS StaffLastLat,
                     longditude                         AS StaffLastLong,
                     @LAT                               AS ClientLat,
                     @LONG                              AS ClientLong,
                     CASE  WHEN Isnull(dbo.[Getdistance](latitude, longditude, @LAT,@Long ), 9999 ) < 0
                     THEN 9999
                     ELSE Round(Isnull(dbo.[Getdistance](latitude, longditude, @LAT, @Long) , 9999), 1)
                     END                                AS distance,
                     'SUGGESTED'                        AS Assessment,
                     jobweighting                       AS CostFactor
				FROM   (SELECT filephoto,
							 uniqueid,
							 HRS_FNIGHTLY_MIN,
							 HRS_FNIGHTLY_MAX,
							 Type,
							 Suburb,
                             [accountno],
                             [name],
                             gender,
                             staffgroup,
                             Isnull(CONVERT(VARCHAR, age), '?') AS Age,
                             rating,
							 alerts,
							 team,
							 CoOrdinator,
							 Contact,
                             jobweighting,
                             specialskills,
                             lastknownlatlong,
							 Branch,
                             CASE
                               WHEN Isnull(lastknownlatlong, '?') <> '?' THEN
                               Substring(lastknownlatlong, 12,
                               Charindex('<Lattitude/><Longditude>',
                               lastknownlatlong) - 12)
                             END                                AS Latitude,
                             CASE
                               WHEN Isnull(lastknownlatlong, '?') <> '?' THEN
                               Substring(lastknownlatlong,
                               Charindex('<Lattitude/><Longditude>',
                               lastknownlatlong)
                                                           + 24, (
                               Charindex('<longditude/>', lastknownlatlong)
                               - (
                               Charindex('<Lattitude/><Longditude>',
                               lastknownlatlong)
                                              + 24 ) ))
                             END                                AS Longditude
                      FROM   (SELECT filephoto,
                                     [accountno],
                                     uniqueid,
									 HRS_FNIGHTLY_MIN,
									 HRS_FNIGHTLY_MAX,
									 Type,
									 Suburb,
                                     [name],
                                     staffgroup,
                                     gender,
                                     Isnull(CONVERT(VARCHAR, age), '?') AS Age,
                                     rating,
                                     jobweighting,
									 team,
									 CoOrdinator,
                                     alerts,
									 Contact,
									 Branch,
                                     Isnull(CASE WHEN Isnull(clstlatlon, '') <> '' THEN clstlatlon ELSE phomlatlong END, '?') AS LastKnownLatLong,
                                     LEFT((SELECT Stuff((SELECT ', ' + [name]   FROM   humanresources      
										WHERE personid = uniqueid  AND    [group] = 'STAFFATTRIBUTE'   
										FOR xml path ('')), 1, 1, '') AS SpecialSkills), 500 ) AS specialskills
                              FROM   (SELECT filephoto,
											HRS_FNIGHTLY_MIN,
											HRS_FNIGHTLY_MAX,
											Category as Type,
											Suburb,
                                             stf_department AS Branch,
                                             staffgroup,
                                             CASE WHEN P.[lastname] <> '' THEN Upper( [lastname]) ELSE '?' END  + ', '     + 
											 CASE WHEN P.firstname <> '' THEN  P.firstname ELSE ' '  END +
                                             CASE WHEN P.middlenames  <> '' THEN ' ' + P.middlenames ELSE  ' '  END
                                             AS [Name],
                                             Datediff(year, dob, Getdate())  AS Age,
                                             [accountno],
                                             CASE WHEN Isnull(P.gender, '') NOT IN    ( 'MALE', 'FEMALE' )  THEN  'UNSPECIFIED'  ELSE gender END AS Gender,
                                             P.accountno    AS   [Account],
                                             P.uniqueid,
                                             P.rating      AS    [Rating],
											 staffteam as team,
											 P.PAN_Manager AS CoOrdinator,
                                             P.jobweighting,
                                             phomlatlong,
                                             (SELECT TOP 1 '<Lattitude>' +   CONVERT( VARCHAR, latitude)  + '<Lattitude/><Longditude>'  + CONVERT(VARCHAR, longditude) + '<Longditude/>' AS CLstLatLon
                                              FROM   roster CLoc
                                                     INNER JOIN recipients RE  ON RE.accountno = CLoc.[client code]
													 LEFT JOIN (SELECT personid,
																	primaryaddress,
																	latitude,
																	longditude
																FROM
																	namesandaddresses) NAC
													ON NAC.personid = RE.uniqueid AND NAC.primaryaddress = 1
													WHERE  CLoc.date = @DATE
													AND CLoc.[start time] <
													@STARTTIME
													AND CLoc.[client code] > '!z'
													AND CLoc.[carer code] =
													P.accountno
													ORDER  BY [start time] DESC) AS CLstLatLon,
											Cast(P.contactissues AS NVARCHAR(4000))	AS Alerts,
											CONVERT(DECIMAL(7, 2), (SELECT Sum(( [roster].[duration]* 5 ) /60) AS WorkedHours
													FROM   roster
													INNER JOIN itemtypes ON [roster].[service type] =[itemtypes].[title] 
													WHERE [carer code] = P.accountno AND date BETWEEN @PPSTART AND @PPEND 
													AND ( infoonly IS NULL OR infoonly = 0 )
													AND ( minorgroup <> 'GAP' OR minorgroup IS NULL )
													AND type IN ( 2, 3, 4, 5,6, 7, 8, 10,11, 12, 13 ))) AS WorkedHours,
													(SELECT TOP 1 PhoneFaxOther.Type + ' ' + CASE WHEN Detail <> '' THEN Detail ELSE '' END FROM 
														PhoneFaxOther WHERE PersonID = P.UniqueID ORDER BY PRIMARYPHONE DESC) AS Contact
										FROM   staff P
										LEFT JOIN (SELECT personid,'YES' AS OnLeave
												FROM   humanresources
												WHERE
												(
													( date1 IS NOT NULL
													AND
													date2 IS NOT NULL )
												 AND ( date1 <= @DATE AND date2 >= @DATE ) )
												 AND [type] = 'LEAVEAPP'
												) AS L
										ON L.personid = P.uniqueid
										LEFT JOIN (SELECT
												personid,
												primaryaddress,
												'<Lattitude>' + CONVERT(VARCHAR,latitude)+'<Lattitude/><Longditude>'+ 
												CONVERT(VARCHAR,longditude)+ '<Longditude/>' AS PHomLatLong
												FROM   namesandaddresses)	NA
										ON NA.personid = P.uniqueid AND NA.primaryaddress = 1
										
										WHERE  ( accountno > '!z' )
												AND ( Isnull(@Exclusions, '') = '' OR accountno NOT IN (SELECT *	 FROM Split(@Exclusions, ',')) )
												AND ( Isnull(@LimitedStaff, '') = '' OR accountno IN (SELECT * FROM Split(@LimitedStaff,',')) )
												AND ( commencementdate IS NOT NULL  AND ( terminationdate IS NULL OR terminationdate > @DATE ))
												AND NOT EXISTS (SELECT * FROM   humanresources H 
														WHERE P.uniqueid = H.personid
															  AND @DATE BETWEEN H.date1 AND H.date2
															  AND [group] = 'LEAVEAPP')
												AND ( Isnull(@Gender, '') = '' OR gender IN (SELECT *  FROM Split(@Gender, ',')))
												AND ( Isnull(@BranchFilter, '') = '' OR stf_department IN (SELECT *  FROM Split(@BranchFilter, ',')))
												AND ( Isnull(@CategoryFilter, '') ='' OR staffgroup IN (SELECT * FROM   Split( @CategoryFilter,',')))
												AND ( Isnull(@TeamFilter, '') = '' OR staffteam IN (SELECT * FROM Split(@TeamFilter, ',')))
												AND ( Isnull(@ManagerFilter, '') = '' OR pan_manager IN (SELECT * FROM Split(@ManagerFilter, ',')))
												AND ( Isnull(@RecordType, '') = '' OR category IN (SELECT * FROM Split(@RecordType, ','))) 
												AND accountno IN 
													(SELECT staff 
														FROM (SELECT *, Lead(starttime)	OVER ( partition BY [staff] ORDER BY date, starttime, startfreetime) AS ENDFREETIME
																	FROM   
																		(
																		SELECT [carer code] AS [STAFF], [date], CONVERT(TIME, '00:00') AS STARTTIME,
																			 CONVERT(TIME, '00:00' ) AS STARTFREETIME
																			 FROM   roster
																			 WHERE  date IN ( @DATE )
																		UNION
																			 SELECT staff, [date], starttime, CASE WHEN startfreetime = '00:00:00.0000000' THEN '23:59:59'  ELSE startfreetime  END AS STARTFREETIME
																			 FROM
																			 (SELECT [carer code] AS [STAFF], [date],CONVERT(TIME,[start time]) AS STARTTIME, CONVERT(TIME, Dateadd(minute,duration * 5,CONVERT(TIME,[start time]))) AS STARTFREETIME
																			   FROM   roster WHERE date IN ( @DATE ))  A2
																		UNION
																			SELECT [carer code] AS [STAFF] , [date],CONVERT(TIME, '23:59:59.99') AS STARTTIME,
																			CONVERT(TIME, '23:59:59.99')AS STARTFREETIME
																			FROM   roster
																			WHERE  date IN ( @DATE )) A0
															) A1
															WHERE staff >'!MULTIPLE' AND staff <> 'BOOKED' AND endfreetime IS NOT NULL
																  AND startfreetime <> endfreetime  AND @STARTTIME BETWEEN startfreetime AND endfreetime 
																  AND @ENDTIME BETWEEN startfreetime AND endfreetime  
																  AND Datediff(minute,startfreetime,endfreetime) > @DURATION
															UNION
															SELECT accountno FROM   staff S
															WHERE NOT EXISTS (SELECT [carer code] FROM roster R 
																			   WHERE  date = @DATE
																				AND [carer code]=S.accountno)
															)
														) t1
													) t2
											) t3
										) t4
	INNER JOIN (SELECT accountno 
				FROM   staff
				WHERE  ( stf_department IN ( @BranchFilter )
					OR Isnull(@BranchFilter, '') = '' )
				AND (SELECT Count([name]) AS CompCount
					FROM   humanresources
					WHERE  ( [recurring] = 1
						AND date1 <= @DATE )
				AND ( [type] = 'STAFFATTRIBUTE' )
				AND ( personid = uniqueid )) = 0
			    AND (SELECT Count([name]) AS CompCount
					FROM   humanresources
					WHERE  ( personid = uniqueid )
					AND ( [group] = 'STAFFATTRIBUTE' )
						AND ( [name] IN (SELECT * FROM   Split(@Competencies, ','))
						AND ( Isnull(date1, '2000/01/01') > @DATE OR Isnull(undated, 0) = 1) )
				  ) = @CompetenciesCount OR competencyexception = 1
		) tComp
		 ON tcomp.accountno = t4.accountno
	
	WHERE  distance <= @MAXDISTANCE
	ORDER  BY t4.[accountno]
END 
GO
/****** Object:  StoredProcedure [dbo].[GetRecipientList]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetRecipientList](@UserID varchar(max), @IncludeInactive bit, @IncludeReferrals bit)
AS BEGIN    

	--DECLARE @UserID AS Varchar (50); 
	--DECLARE @IncludeInactive bit;
	--DECLARE @IncludeReferrals bit;
	DECLARE @ProgramFilter AS Varchar (MAX);
	DECLARE @BranchFilter AS Varchar (MAX);
	DECLARE @CoordFilter AS VarChar(MAX);
	DECLARE @CategoryFilter AS VarChar (MAX);
	DECLARE @SQLCommand as VarChar (MAX);
	DECLARE @BranchDomain as varchar (20);
	DECLARE @CoordDomain AS varchar (20)
	DECLARE @NUL1 AS Varchar (1)
	DECLARE @Referral AS Varchar (20)
	DECLARE @CodeExclusions as Varchar (5)

	SET @UserID = 'sysmgr'
	SET @IncludeInactive = 0
	SET @BranchFilter = '(SELECT UserInfo.ViewFilter FROM UserInfo WHERE [Name] = @UserID)'
	--SET @ProgramFilter = Right(Replace(@ProgramFilter, 'RecipientPrograms', 'PR'), LEN(@ProgramFilter) - CHARINDEX(@ProgramFilter, 'WHERE' + 6))
	SET @CoordFilter = '(SELECT UserInfo.ViewFilter FROM UserInfo WHERE [Name] = @UserID)'
	SET @BranchDomain = '''RECIPBRANCHES'''
	SET @CoordDomain = '''COORDINATOR'''
	SET @NUL1 = ''''''
	SET @CodeExclusions = '''!z'''
	
	SET @SQLCommand = 
	'SELECT DISTINCT AccountNo, UniqueID, AgencyDefinedGroup FROM Recipients	
	LEFT JOIN RecipientPrograms ON Recipients.UniqueID = RecipientPrograms.PersonID 
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [Branch] FROM HumanResources WHERE HumanResources.[Group] = ' + @BranchDomain + ') 	AS BR ON RECIPIENTS.UniqueID = BR.PersonID
	LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [CoOrd] FROM HumanResources WHERE HumanResources.[Group] = ' + @CoordDomain + ') 	AS CM ON RECIPIENTS.UniqueID = CM.PersonID 
    WHERE Accountno > ' + @CodeExclusions


	SET @SQLCommand = @SQLCommand + ' ORDER BY ACCOUNTNO ' 
	EXEC (@SQLCommand)

END

/* Stored Procedure */
GO
/****** Object:  StoredProcedure [dbo].[GetReportCriteriaList]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[GetReportCriteriaList] (@ListType AS varchar(50), @IncludeInactive bit)AS BEGIN    IF (@IncludeInactive = 0)    BEGIN        IF (@ListType = 'STATES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'STATES'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'BRANCHES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'BRANCHES'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'MANAGERS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'CASE MANAGERS'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'FUNDERS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'FUNDINGBODIES'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'FUNDINGREGIONS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'FUNDREGION'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'FUNDERSERVICETYPES')        BEGIN            SELECT                HACCType            FROM ItemTypes            WHERE ProcessClassification <> 'INPUT'            AND ISNULL(EndDate, '') = ''            ORDER BY HACCType        END        IF (@ListType = 'PROVIDERIDS')        BEGIN            SELECT DISTINCT                Address1            FROM HumanResourceTypes            WHERE [Group] = 'PROGRAMS'            AND ISNULL(UserYesNo3, 0) = 0            AND ISNULL(address1, '') <> ''            ORDER BY Address1        END        IF (@ListType = 'PROGRAMS')        BEGIN            SELECT                [Name]            FROM HumanResourceTypes            WHERE [Group] = 'PROGRAMS'            AND ISNULL(ENDDATE, '') = ''            AND ISNULL(UserYesNo3, 0) = 0            ORDER BY NAME        END        IF (@ListType = 'BUDGETCODES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'BUDGETGROUP'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'SERVICETYPES')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification <> 'INPUT'            AND ISNULL(EndDate, '') = ''            ORDER BY TITLE        END        IF (@ListType = 'CAREDOMAINS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'CAREDOMAIN'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'DISCPLINES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'DISCIPLINE'            AND ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'COSTCENTRES')        BEGIN            SELECT DISTINCT                Fax            FROM HumanResourceTypes            WHERE [Group] = 'PROGRAMS'            AND ISNULL(EndDate, '') = ''            AND ISNULL(UserYesNo3, 0) = 0            AND ISNULL(FAX, '') <> ''            ORDER BY FAX        END        IF (@ListType = 'ENVIRONMENTS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain IN ('ACTIVITYGROUPS', 'VEHICLES')            AND ISNULL(EndDate, '') = ''            UNION            SELECT                [Name] AS DESCRIPTION            FROM CSTDAOutlets            WHERE ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'SERVICES')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification = 'OUTPUT'        END        IF (@ListType = 'ABSCENCE')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification = 'EVENT'        END        IF (@ListType = 'PAYTYPES')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification = 'INPUT'        END        IF (@ListType = 'JOB CATEGORIES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'STAFFGROUP'            ORDER BY DESCRIPTION        END        IF (@ListType = 'STAFF TEAM')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'STAFFTEAM'            ORDER BY DESCRIPTION        END        ELSE        IF (@ListType = 'STATES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'STATES'            ORDER BY DESCRIPTION        END        IF (@ListType = 'SERVICEREGIONS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'GROUPAGENCY'            ORDER BY DESCRIPTION        END        IF (@ListType = 'BRANCHES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'BRANCHES'            ORDER BY DESCRIPTION        END        IF (@ListType = 'MANAGERS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'CASE MANAGERS'            ORDER BY DESCRIPTION        END        IF (@ListType = 'FUNDERS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'FUNDINGBODIES'            ORDER BY DESCRIPTION        END        IF (@ListType = 'FUNDINGREGIONS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'FUNDREGION'            ORDER BY DESCRIPTION        END        IF (@ListType = 'PROVIDERIDS')        BEGIN            SELECT DISTINCT                Address1            FROM HumanResourceTypes            WHERE [Group] = 'PROGRAMS'            AND ISNULL(UserYesNo3, 0) = 0            AND ISNULL(address1, '') <> ''            ORDER BY Address1        END        IF (@ListType = 'PROGRAMS')        BEGIN            SELECT                [Name]            FROM HumanResourceTypes            WHERE [Group] = 'PROGRAMS'            AND ISNULL(UserYesNo3, 0) = 0            ORDER BY NAME        END        IF (@ListType = 'BUDGETCODES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'BUDGETGROUP'            ORDER BY DESCRIPTION        END        IF (@ListType = 'SERVICETYPES')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification <> 'INPUT'            ORDER BY TITLE        END        IF (@ListType = 'CAREDOMAINS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'CAREDOMAIN'            ORDER BY DESCRIPTION        END        IF (@ListType = 'DISCPLINES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'DISCIPLINE'            ORDER BY DESCRIPTION        END        IF (@ListType = 'COSTCENTRES')        BEGIN            SELECT                Fax            FROM HumanResourceTypes            WHERE [Group] = 'PROGRAMS'            AND ISNULL(UserYesNo3, 0) = 0            ORDER BY FAX        END        IF (@ListType = 'ENVIRONMENTS')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain IN ('ACTIVITYGROUPS', 'VEHICLES')            UNION            SELECT                [Name] AS DESCRIPTION            FROM CSTDAOutlets            WHERE ISNULL(EndDate, '') = ''            ORDER BY DESCRIPTION        END        IF (@ListType = 'SERVICES')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification = 'OUTPUT'            AND ISNULL(EndDate, '') <> ''        END        IF (@ListType = 'ABSCENCE')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification = 'EVENT'            AND ISNULL(EndDate, '') <> ''        END        IF (@ListType = 'PAYTYPES')        BEGIN            SELECT                Title            FROM ItemTypes            WHERE ProcessClassification = 'INPUT'            AND ISNULL(EndDate, '') <> ''        END        IF (@ListType = 'JOB CATEGORIES')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'STAFFGROUP'            ORDER BY DESCRIPTION        END        IF (@ListType = 'STAFF TEAM')        BEGIN            SELECT                Description            FROM DataDomains            WHERE Domain = 'STAFFTEAM'            ORDER BY DESCRIPTION        END    END END
GO
/****** Object:  StoredProcedure [dbo].[getRoster_date_wise]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[getRoster_date_wise] (@CarerCode VARCHAR(max), 
                                         @username  VARCHAR(max),@RosterDate varchar(20)) 
		AS 
		  BEGIN 
			  SELECT DISTINCT recordno, 
							  [carer code], 
							  [client code], 
							  re.UniqueID as PersonID,
							  servicesetting, 
							  [date], 
							  duration, 
							  program, 
							  [service type], 
							  [dayno], 
							  [monthno], 
							  [yearno], 
							  [blockno], 
							  [service description], 
							  CONVERT (VARCHAR, r.[notes])                AS notes, 
							  r.[type], 
							  km, 
							  r.billto                                    AS Debtor, 
							  Isnull(itm.infoonly, 0)                     AS InfoOnly, 
							  Isnull(itm.displayfeeinapp, 0)              AS 
							  DisplayFeeInApp, 
							  Isnull(itm.displaydebtorinapp, 0)           AS 
							  DisplayDebtorInApp, 
							  Isnull(re.accountingidentifier, '')         AS 
							  ACCOUNTINGIDENTIFIER, 
							  Isnull(itm.itemtype, '')                    AS ItemType, 
							  Isnull(itm.minorgroup, '')                  AS minorgroup, 
							  Isnull(ht.useryesno6, 0)                    AS 
							  MyOwnCarStatus, 
							  Isnull(r.disable_shift_start_alarm, 0)      AS 
							  Disable_Shift_start_Alarm, 
							  Isnull(r.disable_shift_end_alarm, 0)        AS 
							  Disable_Shift_End_Alarm, 
							  Isnull(ta_multishift, 0)                    AS 
							  TA_MultiShift 
							  , 
							  Isnull(lo.addressline1, '') + ' ' 
							  + Isnull(lo.suburb, '')                     AS Address, 
							  CASE 
								WHEN [start time] = '24:00' THEN '00:00' 
								ELSE Replace([start time], '.', ':') 
							  END                                         AS 
							  [start time], 
							  CASE 
								WHEN Isnull(r.ta_excludegeolocation, '') = '' THEN 0 
								ELSE r.ta_excludegeolocation 
							  END                                         AS 
							  TA_EXCLUDEGEOLOCATION, 
							  CASE 
								WHEN Isnull(itm.ta_excludegeolocation, '') = '' THEN 0 
								ELSE itm.ta_excludegeolocation 
							  END                                         AS 
							  TA_EXCLUDEGEOLOCATION2, 
							  CASE 
								WHEN Isnull(itm.appexclude1, '') = '' THEN 0 
								ELSE itm.appexclude1 
							  END                                         AS appexclude1 
							  , 
							  CASE 
								WHEN Isnull(itm.itemtype, '') <> '' THEN itm.itemtype 
								WHEN Isnull(itm.billtext, '') <> '' THEN itm.billtext 
								ELSE itm.title 
							  END                                         AS 
							  MTAServiceType, 
							  ( r.billqty * r.[unit bill rate] ) + CASE 
																	 WHEN 
							  Isnull(ht.gstrate, 0) = 0 THEN 0 
																	 ELSE 
							  ( r.billqty * r.[unit bill rate] ) * ( ht.gstrate / 100 ) 
																   END    AS Fee, 
							  Year(ez.lodatetime)                         AS test, 
							  CASE 
								WHEN Year(Isnull(ez.datetime, '1900')) = '1900' THEN 0 
								ELSE 1 
							  END                                         AS started, 
							  CASE 
								WHEN Year(Isnull(ez.lodatetime, '1900')) = '1900' THEN 0 
								ELSE 1 
							  END                                         AS completed, 
							  Isnull(ez.datetime, 0)                      AS started1, 
							  Isnull(ez.lodatetime, 0)                    AS completed1, 
							  CASE 
								WHEN Isnull(r.tamode, '') = '' THEN 
								  CASE 
									WHEN Isnull(itm.ta_loginmode, -1) = -1 THEN 
									(SELECT TOP 1 Isnull(tmmode, 0) 
									 FROM   userinfo 
									 WHERE  NAME = @username 
											AND staffcode = @CarerCode) 
									ELSE itm.ta_loginmode 
								  END 
								ELSE r.tamode 
							  END                                         AS 
							  TA_LOGINMODE2 
							  , 
							  (SELECT TOP 1 NAME 
							   FROM   humanresources h WITH (nolock), 
									  recipients rp WITH (nolock) 
							   WHERE  h.[type] = 'RECIPTYPE' 
									  AND Isnull(mobilealert, 0) = 1 
									  AND h.personid = rp.uniqueid 
									  AND rp.accountno = r.[client code]) AS [Group], 
							  (SELECT Count(DISTINCT h.NAME) 
							   FROM   humanresources h WITH (nolock), 
									  recipients rp WITH (nolock) 
							   WHERE  h.[type] = 'RECIPTYPE' 
									  AND Isnull(mobilealert, 0) = 1 
									  AND h.personid = rp.uniqueid 
									  AND rp.accountno = r.[client code]) AS 
							  group_alerts, 
							  CASE 
								WHEN r.type = 11 
									 AND r.[client code] = '!MULTIPLE' THEN 
								Isnull(lo.runsheetalerts, '') 
								ELSE '' 
							  END                                         AS 
							  RunsheetAlerts 
			  FROM   roster r 
					 INNER JOIN recipients re 
							 ON re.accountno = r.[client code] 
					 INNER JOIN itemtypes itm 
							 ON itm.title = r.[service type] 
								AND processclassification IN ( 'OUTPUT', 'EVENT' ) 
					 INNER JOIN humanresourcetypes ht 
							 ON r.[program] = ht.[name] 
								AND ht.[group] = 'PROGRAMS' 
					 INNER JOIN userinfo us 
							 ON us.staffcode = r.[carer code] 
					 LEFT JOIN ezitracker_log ez 
							ON ez.jobno = r.recordno 
					 LEFT JOIN cstdaoutlets lo 
							ON lo.[name] = r.servicesetting 
			  WHERE  [carer code] IN ( @CarerCode ) and r.Date=@RosterDate
					 AND Isnull(itm.appexclude1, 0) = 0 
			  ORDER  BY [date] DESC, 
						[start time], 
						recordno; 

    
		  END
GO
/****** Object:  StoredProcedure [dbo].[getStaffWorkingHours]    Script Date: 27/04/2022 10:57:23 ******/
Create or Alter PROCEDURE [dbo].[getStaffWorkingHours](
  @sDate varchar(10), 
  @eDate varchar(10), 
  @dmType varchar(10)= ''
) As begin if (
  @dmType in ('2') 
  or isnull(@dmType, '')= ''
) 
select 
  [Carer Code] as AccountNo, 
  DATEPART(Week, Date) Month_WeekNo, 
  RANK() OVER(
    PARTITION BY [Carer Code] 
    ORDER BY 
      DATEPART(Week, Date)
  ) WeekNo, 
  FORMAT(
    (
      floor(sum(duration)/ 12)
    ), 
    '00'
  ) + ':' + format(
    (
      convert(
        int, 
        sum(duration)
      )% 12
    )* 5, 
    '00'
  ) as Total_WrkdHr, 
  [dbo].[getPayPeriodWorkedHours](ro.[Carer Code]) PayprdWrkdHr, 
  ISNULL([Staff].[HRS_FNIGHTLY_MIN], 0) AS HRS_FNIGHTLY_MIN, 
  ISNULL([Staff].[HRS_FNIGHTLY_MAX], 0) AS HRS_FNIGHTLY_MAX, 
  ISNULL([Staff].[HRS_WEEKLY_MIN], 0) AS HRS_WEEKLY_MIN, 
  ISNULL([Staff].[HRS_WEEKLY_MAX], 0) AS HRS_WEEKLY_MAX 
from 
  Roster ro 
  INNER JOIN itemtypes ON [ro].[service type] = itemtypes.title 
  INNER JOIN [Staff] on ro.[Carer Code] = staff.AccountNo 
WHERE 
  (
    date >= @sDate 
    AND date <= @eDate
  ) 
  AND isnull(itemtypes.InfoOnly, 0)= 0 
  AND ([ro].type <> 9) 
  AND ([carer code] > '!z') 
  AND (
    [ro].[type] = 7 
    OR [ro].[type] = 2 
    OR [ro].[type] = 8 
    OR [ro].[type] = 5 
    OR [ro].[type] = 3 
    OR [ro].[type] = 10 
    OR [ro].[type] = 11 
    OR [ro].[type] = 12 
    OR [ro].[type] = 1 
    OR [ro].[type] = 13 
    OR [ro].[type] = 6
  ) 
group by 
  [Carer Code], 
  DATEPART(Week, Date), 
  ISNULL([Staff].[HRS_FNIGHTLY_MIN], 0), 
  ISNULL([Staff].[HRS_FNIGHTLY_MAX], 0), 
  ISNULL([Staff].[HRS_WEEKLY_MIN], 0), 
  ISNULL([Staff].[HRS_WEEKLY_MAX], 0) 
order by 
  [Carer Code], 
  DATEPART(Week, Date) else if (
    @dmType in ('12')
  ) 
select 
  [Client Code] as AccountNo, 
  DATEPART(Week, Date) Month_WeekNo, 
  RANK() OVER(
    PARTITION BY [Client Code] 
    ORDER BY 
      DATEPART(Week, Date)
  ) WeekNo, 
  FORMAT(
    (
      floor(sum(duration)/ 12)
    ), 
    '00'
  ) + ':' + format(
    (
      convert(
        int, 
        sum(duration)
      )% 12
    )* 5, 
    '00'
  ) as Total_WrkdHr, 
  [dbo].[getPayPeriodWorkedHours](ro.[Client Code]) PayprdWrkdHr, 
  ISNULL([Staff].[HRS_FNIGHTLY_MIN], 0) AS HRS_FNIGHTLY_MIN, 
  ISNULL([Staff].[HRS_FNIGHTLY_MAX], 0) AS HRS_FNIGHTLY_MAX, 
  ISNULL([Staff].[HRS_WEEKLY_MIN], 0) AS HRS_WEEKLY_MIN, 
  ISNULL([Staff].[HRS_WEEKLY_MAX], 0) AS HRS_WEEKLY_MAX 
from 
  Roster ro 
  INNER JOIN itemtypes ON [ro].[service type] = itemtypes.title 
  INNER JOIN [Staff] on ro.[Carer Code] = staff.AccountNo 
WHERE 
  (
    date >= @sDate 
    AND date <= @eDate
  ) 
  AND isnull(itemtypes.InfoOnly, 0)= 0 
  AND ([ro].type <> 9) 
  AND ([carer code] > '!z') 
  AND (
    [ro].[type] = 7 
    OR [ro].[type] = 2 
    OR [ro].[type] = 8 
    OR [ro].[type] = 5 
    OR [ro].[type] = 3 
    OR [ro].[type] = 10 
    OR [ro].[type] = 11 
    OR [ro].[type] = 12 
    OR [ro].[type] = 1 
    OR [ro].[type] = 13 
    OR [ro].[type] = 6
  ) 
group by 
  [Client Code], 
  DATEPART(Week, Date), 
  ISNULL([Staff].[HRS_FNIGHTLY_MIN], 0), 
  ISNULL([Staff].[HRS_FNIGHTLY_MAX], 0), 
  ISNULL([Staff].[HRS_WEEKLY_MIN], 0), 
  ISNULL([Staff].[HRS_WEEKLY_MAX], 0) 
order by 
  [Client Code], 
  DATEPART(Week, Date) else 
select 
  ServiceSetting as AccountNo, 
  DATEPART(Week, Date) Month_WeekNo, 
  RANK() OVER(
    PARTITION BY ServiceSetting 
    ORDER BY 
      DATEPART(Week, Date)
  ) WeekNo, 
  FORMAT(
    (
      floor(sum(duration)/ 12)
    ), 
    '00'
  ) + ':' + format(
    (
      convert(
        int, 
        sum(duration)
      )% 12
    )* 5, 
    '00'
  ) as Total_WrkdHr, 
  [dbo].[getPayPeriodWorkedHours](ro.ServiceSetting) PayprdWrkdHr, 
  ISNULL([Staff].[HRS_FNIGHTLY_MIN], 0) AS HRS_FNIGHTLY_MIN, 
  ISNULL([Staff].[HRS_FNIGHTLY_MAX], 0) AS HRS_FNIGHTLY_MAX, 
  ISNULL([Staff].[HRS_WEEKLY_MIN], 0) AS HRS_WEEKLY_MIN, 
  ISNULL([Staff].[HRS_WEEKLY_MAX], 0) AS HRS_WEEKLY_MAX 
from 
  Roster ro 
  INNER JOIN itemtypes ON [ro].[service type] = itemtypes.title 
  INNER JOIN [Staff] on ro.[Carer Code] = staff.AccountNo 
WHERE 
  (
    date >= @sDate 
    AND date <= @eDate
  ) 
  AND isnull(itemtypes.InfoOnly, 0)= 0 
  AND ([ro].type <> 9) 
  AND ([carer code] > '!z') 
  AND (
    [ro].[type] = 7 
    OR [ro].[type] = 2 
    OR [ro].[type] = 8 
    OR [ro].[type] = 5 
    OR [ro].[type] = 3 
    OR [ro].[type] = 10 
    OR [ro].[type] = 11 
    OR [ro].[type] = 12 
    OR [ro].[type] = 1 
    OR [ro].[type] = 13 
    OR [ro].[type] = 6
  ) 
group by 
  ServiceSetting, 
  DATEPART(Week, Date), 
  ISNULL([Staff].[HRS_FNIGHTLY_MIN], 0), 
  ISNULL([Staff].[HRS_FNIGHTLY_MAX], 0), 
  ISNULL([Staff].[HRS_WEEKLY_MIN], 0), 
  ISNULL([Staff].[HRS_WEEKLY_MAX], 0) 
order by 
  ServiceSetting, 
  DATEPART(Week, Date) 
  
  end 

---==================================Pasting_Rosters==============================

CREATE OR ALTER    PROCEDURE  [dbo].[Pasting_Rosters](@JSON nvarchar(4000))
As
Begin
	Declare @sMode varchar(5)
	Declare @sStaffCode varchar(500);
	Declare @sClientCode varchar(500);
	Declare @sProgram varchar(500);
	Declare @sDate varchar(20);
	Declare @sStartTime varchar(5);
	Declare @sDuration varchar(5);
	Declare @sActivity varchar(500);
	Declare @sRORecordno varchar(20);
	Declare @PasteAction varchar(20);

	
	DECLARE @temp  TABLE 

	(
		ErrorValue int,
		msg varchar(500),
		continueError bit
          
	)
	
		
	DECLARE ROSTERS CURSOR 
	FOR
	SELECT sMode, sStaffCode,sClientCode,sProgram,sDate,sStartTime, sDuration,sActivity,sRORecordno,PasteAction
	FROM OPENJSON(@JSON)
	WITH 
	(sMode varchar(10),
	sStaffCode nvarchar(100),
	sClientCode  varchar(100),
	sProgram varchar(100),
	sDate varchar(10),
	sStartTime varchar(5),
	sDuration varchar(5),
	sActivity varchar(100),
	sRORecordno varchar(20),
	PasteAction varchar(20)
	) AS jsonValues;

	OPEN ROSTERS  
	FETCH NEXT FROM ROSTERS
	INTO 
	 @sMode,
	 @sStaffCode,
	 @sClientCode, 
	 @sProgram,
	 @sDate,
	 @sStartTime,
	 @sDuration,
	 @sActivity,
	 @sRORecordno,
	 @PasteAction
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		PRINT @sStaffCode + ' ' + @sClientCode
		
		FETCH NEXT FROM ROSTERS
		INTO 
		 @sMode,
		 @sStaffCode,
		 @sClientCode, 
		 @sProgram,
		 @sDate,
		 @sStartTime,
		 @sDuration,
		 @sActivity,
		 @sRORecordno,
		 @PasteAction	

		insert into @temp
		execute [Check_BreachedRosterRules] 'Add',@sStaffCode,@sClientCode,@sProgram,@sDate,@sStartTime,@sDuration,@sActivity,@sRORecordno,'',0,0,0,0,0,0,'','',0,'',@PasteAction
	END
	CLOSE ROSTERS  
    DEALLOCATE ROSTERS

	select * from @temp;	
	
		
End

GO
/****** Object:  StoredProcedure [dbo].[PutStaffOnLeave]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[PutStaffOnLeave](@StaffCode varchar(50),@LeaveStartDate varchar (10),@LeaveEndDate varchar (10), @Description varchar (MAX),@PayType varchar (50), @LeaveActivityType varchar(50),@AlternateProgram varchar (50), @CurrentRosterAction varchar (50), @MasterRosterAction varchar (50), @UnallocateAdmin varchar (50),@ForceUnavailable varchar (50),  @CreateLeave varchar (50), @LeaveIsProrata varchar (50), @ReminderDate varchar (50), @Approved bit, @User varchar (50)) AS BEGIN	DECLARE @RosterRecords varchar (max);	DECLARE @RosterDate varchar (10);	DECLARE @RecordsForLeave TABLE (RecordNo INT)	DECLARE @PersonID varchar (50)	SET @PersonID = (SELECT UniqueID FROM Staff WHERE Accountno = @StaffCode)		INSERT INTO @RecordsForLeave 		SELECT RecordNo			FROM ROSTER WHERE 			([Carer Code] = @StaffCode) AND (Date BETWEEN @LeaveStartDate AND @LeaveEndDate) AND 			(((Type IN (2, 3, 5, 8, 10, 11, 12)) AND (Status = 1)) OR ([Type] IN (6, 7))) ORDER BY Date		INSERT INTO HUMANRESOURCES 	(PERSONID, [TYPE],[Group],Date1,Date2,[Name], Address1, Address2, 	Suburb, EquipmentCode, LockBoxCode, LockBoxLocation, ReminderScope, 	Completed, creator, User1)	VALUES 	(@PersonID, 'LEAVEAPP','LEAVEAPP',@LeaveStartDate,@LeaveEndDate, @Description, @PayType, @LeaveActivityType,	@AlternateProgram, @CurrentRosterAction, @ForceUnavailable,  @CreateLeave, @LeaveIsProrata,	@Approved, @User, CONVERT(varchar, GetDate()))		IF @CreateLeave = 'AUTOCREATELEAVE' BEGIN		INSERT INTO roster 				([client code], 				 [carer code], 				 [service type], 				 [service description], 				 [program], 				 [date], 				 [hacctype], 				 [start time], 				 [duration], 				 [unit pay rate], 				 [unit bill rate], 				 [taxpercent], 				 [yearno], 				 [monthno], 				 [dayno], 				 [blockno], 				 [notes], 				 [type], 				 [status], 				 [anal], 				 [date entered], 				 [date last mod], 				 [groupactivity], 				 [billtype], 				 [billto], 				 [apinvoicedate], 				 [apinvoicenumber], 				 [costunit], 				 [costqty], 				 [billunit], 				 [billqty], 				 [servicesetting], 				 [dischargereasontype], 				 [datasetclient], 				 [nrcp_referral_service], 				 [creator], 				 [editer], 				 [inuse], 				 [billdesc], 				 [transferred], 				 ROS_PANZTEL_UPDATED,				 TA_EarlyStart,				 TA_LateStart,				 TA_EarlyGo,				 TA_LateGo,				 TA_TooShort,				 TA_TooLong,				 [Date Timesheet],				 [date invoice],				 InvoiceNumber,				 TimesheetNumber,				 [batch#],				 [ros_copy_batch#])			SELECT				 '!INTERNAL',								 [carer code],								 @LeaveActivityType,						 @PayType,									 [Program],									 roster.[Date],								 'LEAVE',									 [start time],								 [duration],								 [unit pay rate], 				 0,											 0,											 [yearno], 				 [monthno], 				 [dayno], 				 [blockno], 				 '',										 6,											 2,											 [anal], 				 GETDATE(),									 GETDATE(),									 [groupactivity], 				 [billtype], 				 '!INTERNAL',								 '',										 '',										 'HOUR',									 [costqty], 				 'HOUR',									 [billqty], 				 '',										 '',										 '!INTERNAL',								 [nrcp_referral_service], 				 @User,								 @User,								 0,											 '',										 0,											 0,											 0,											 0,											 0,											 0,											 0,											 0,											 Null,										 Null,										 Null,										 Null,										 Null,										 Null										 FROM ROSTER WHERE RecordNo IN (SELECT RecordNo FROM @RecordsForLeave)	END		IF (@CurrentRosterAction = 'UNALLOCATECURRENT') BEGIN		UPDATE Roster SET [ShiftName] = [Carer Code], [Carer Code] = 'BOOKED', Type = 1 WHERE RecordNo IN (SELECT RecordNo FROM @RecordsForLeave)	END		IF (@ForceUnavailable = 'FORCEUNAVAILABILITY') BEGIN		SET @RosterDate = @LeaveStartDate		DELETE FROM ROSTER WHERE [Date] BETWEEN @LeaveStartDate and @LeaveEndDate AND Type = 13		WHILE @RosterDate <= @LeaveEndDate BEGIN			INSERT INTO roster ( 							[Client Code], 							[carer code], 							[Service Type], 							[Service Description], 							[program], 							[Date], 							[start time], 							[Duration], 							[Unit Pay Rate], 							[Unit Bill Rate], 							[Yearno], 							[Monthno], 							[DayNo], 							[BlockNo], 							[Type], 							[Status], 							[Date Entered], 							[Creator], 							[Date Last Mod], 							[Date Timesheet], 							[date invoice], 							[date payroll], 							[BillTo], 							[CostQty], 							[BillQty], 							[DatasetClient], 							InUse, 							[editer], 							[Notes], 							[ROS_COPY_BATCH#], 							[GroupActivity])					VALUES						(							'!INTERNAL',							@StaffCode,							'UNAVAILABLE',							'UNAVAILABLE',							'!INTERNAL',							CONVERT(varchar, @RosterDate, 111),							'00:00',							288, 							0,							0,							Year(@RosterDate),							Month(@RosterDate),							Day(@RosterDate),							0,							13,							1,							GetDate(),							@User,							GetDate(),							GetDate(),							GetDate(),							GetDate(),							'!INTERNAL',							1,							1,							'!INTERNAL',							0,							@User,							'ON LEAVE',							Null,							0				)			SET @RosterDate = CONVERT(Varchar, DATEADD(day, 1, @RosterDate), 111) 		END 	END  END
GO
/****** Object:  StoredProcedure [dbo].[SearchAllTables]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER   PROCEDURE [dbo].[SearchAllTables] (@SearchStr nvarchar(100))AS BEGIN    CREATE  TABLE #Results (        ColumnName nvarchar(370),        ColumnValue nvarchar(3630)    )    SET NOCOUNT ON    DECLARE @TableName nvarchar(256),            @ColumnName nvarchar(128),            @SearchStr2 nvarchar(110)    SET @TableName = ''    SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%', '''')    WHILE @TableName IS NOT NULL    BEGIN        SET @ColumnName = ''        SET @TableName = (SELECT            MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))        FROM INFORMATION_SCHEMA.TABLES        WHERE TABLE_TYPE = 'BASE TABLE'        AND QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName        AND OBJECTPROPERTY(        OBJECT_ID(        QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)        ), 'IsMSShipped'        ) = 0)        WHILE (@TableName IS NOT NULL)            AND (@ColumnName IS NOT NULL)        BEGIN            SET @ColumnName = (SELECT                MIN(QUOTENAME(COLUMN_NAME))            FROM INFORMATION_SCHEMA.COLUMNS            WHERE TABLE_SCHEMA = PARSENAME(@TableName, 2)            AND TABLE_NAME = PARSENAME(@TableName, 1)            AND DATA_TYPE IN ('char', 'varchar', 'nchar', 'nvarchar', 'int', 'decimal')            AND QUOTENAME(COLUMN_NAME) > @ColumnName)            IF @ColumnName IS NOT NULL            BEGIN                INSERT INTO #Results                EXEC                (                'SELECT ''' + @TableName + '.' + @ColumnName + ''', LEFT(' + @ColumnName + ', 3630)                     FROM ' + @TableName + 'WITH (NOLOCK) ' +                ' WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2                )            END        END    END    SELECT        ColumnName,        ColumnValue    FROM #Results END
GO
/****** Object:  StoredProcedure [dbo].[SetClientPackage]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[SetClientPackage] @NewPackage nvarchar(max),@ClientCode nvarchar(max),@Status nvarchar(max)AS    INSERT INTO RecipientPrograms (Program, PersonID, ProgramStatus)        VALUES (@NewPackage, @ClientCode, @Status);
GO
/****** Object:  StoredProcedure [dbo].[sp_getspecificCompentencies]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[sp_getspecificCompentencies](@RecipientCode varchar(Max)) AS BEGIN Declare @PersonID varchar(max); SELECT @PersonID=UNIQUEID FROM RECIPIENTS WHERE ACCOUNTNO=@recipientCode; SELECT UPPER([Name]) AS Competency FROM HumanResources  WHERE    ISNULL(Recurring, 0) = 1 and   [PersonID] IN (  select t.RecordNumber from HUMANRESOURCETYPES t  inner join  serviceoverview s on s.ServiceProgram=t.name where personid = @PersonId and servicestatus = 'active' AND [TYPE] = 'PROG_COMP' AND ISNULL(USERYesNo1, 0) = 1) UNION  SELECT UPPER([Name]) AS Competency FROM HumanResources WHERE    ISNULL(Recurring, 0) = 1 and [TYPE] = 'PROG_COMP' AND ISNULL(USERYesNo1, 0) = 1 AND [PersonID] IN (                      SELECT CONVERT(VARCHAR, pr.RecordNumber) FROM HumanResourceTypes pr                      INNER JOIN recipientprograms rpr on pr.name = rpr.program                       WHERE  rpr.personid=@Personid                ) UNION SELECT UPPER([Name]) AS Competency FROM HumanResources WHERE      [PersonID]=@PersonID AND [TYPE] = 'RECIPATTRIBUTE'  AND ISNULL(Recurring, 0) = 1 UNION SELECT UPPER([Name]) AS Competency FROM HumanResources  sco WHERE  sco.PersonID = @PersonId AND [GROUP] IN (SELECT [Service Type] FROM ServiceOverview WHERE personid = @Personid and ServiceStatus = 'ACTIVE') AND ISNULL([Recurring], 0) = 1 union Select [description] as Competency from datadomains where isnull(embedded, 0) = 1 and domain='staffattribute' END 
GO
/****** Object:  StoredProcedure [dbo].[sp_getspecificCompentencies_desireable]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[sp_getspecificCompentencies_desireable] (@RecipientCode varchar(max), @ListType varchar(max))AS BEGIN    DECLARE @PersonID varchar(max);    SELECT        @PersonID = UNIQUEID    FROM RECIPIENTS    WHERE ACCOUNTNO = @recipientCode;    SELECT DISTINCT        CASE        WHEN ISNULL(d.USER1, '') = '' THEN 'Z-OTHER'        ELSE d.USER1        END AS [GROUP],        [DESCRIPTION],        ISNULL(h.PersonID, '0') AS PersonId,        domain    FROM DATADOMAINS d    LEFT JOIN HumanResources h        ON d.[Description] = h.Name        AND h.[group] = 'RECIPATTRIBUTE'        AND h.PersonID = @PersonID    WHERE DOMAIN = 'STAFFATTRIBUTE';END
GO
/****** Object:  StoredProcedure [dbo].[sp_Register_MTA_Device]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[sp_Register_MTA_Device] (@StaffCode   VARCHAR(50), 
													@DeviceToken VARCHAR(max), 
													@DeviceName  VARCHAR(max), 
													@DeviceType  VARCHAR(100)) 
	AS 
	  BEGIN 
		  IF NOT EXISTS (SELECT devicetoken 
						 FROM   mta_devices 
						 WHERE  isnull(devicetoken,') = @DeviceToken 
								AND isnull(staffcode,') = @StaffCode) 
			BEGIN 

				Delete from mta_devices where staffcode=@StaffCode;
				INSERT INTO mta_devices 
							(staffcode, 
							 devicetoken, 
							 devicename, 
							 devicetype) 
				VALUES      (@StaffCode, 
							 @DeviceToken, 
							 @DeviceName, 
							 @DeviceType); 
			END 
	  END 
GO
/****** Object:  StoredProcedure [dbo].[spRosterOperations]    Script Date: 27/04/2022 10:57:23 ******/


Create or ALTER        PROCEDURE [dbo].[spRosterOperations] (@OpsType varchar(50),@User varchar(50), @RecordNo int, @IsMaster bit,@Roster_Date varchar(50), @Start_Time varchar(5),@carer_code varchar(100), @notes varchar(MAX), @ClientCodes varchar(MAX)=null)
As
begin
declare @userId int;
declare @BlockNo float;


select @userId=Recnum from UserInfo where name=@user;

if (@Start_Time<>'')
	select  @BlockNo=dbo.[GetBlockNo](@Start_Time);
else
	set @BlockNo=0;

	if (@OpsType='Delete')
		begin
		insert into Roster_Del(
		    [RecordNo]
		   ,[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate])

			select 
			RecordNo
		   ,[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate] from roster where Status<>2 and RecordNo=@RecordNo;
			
			delete from Roster where Status<>2 and RecordNo=@RecordNo;
		
		end
	else if (@OpsType='Re-Allocate')
		begin
			
		Declare @sStaffCode varchar(500);
		Declare @sClientCode varchar(500);
		Declare @sProgram varchar(500);
		Declare @sDate varchar(20);
		Declare @sStartTime varchar(5);
		Declare @sDuration varchar(5);
		Declare @sActivity varchar(500);
		
		select @sStaffCode=[Carer Code],@sClientCode=[Client Code],@sProgram=[program],@sDate=[date],@sStartTime=[Start Time],@sDuration=convert(varchar,duration),@sActivity=[Service Type]
		 from Roster where RecordNo=@RecordNo;

		if @sStaffCode in ('BOOKED','!MULTIPLE','!INTERNAL')
		BEGIN
			update Roster set [Carer Code]=@carer_code,type=isnull(dbo.getRosterType([Service Type]),0) where RecordNo=@RecordNo;
			RETURN;
		END
		

		 DECLARE @temp  TABLE 

			(
			ErrorValue int,
			msg varchar(500),
			continueError bit
          
			)
		insert into @temp
		execute [Check_BreachedRosterRules] 'Add',@carer_code,@sClientCode,@sProgram,@sDate,@sStartTime,@sDuration,@sActivity,@RecordNo,'',0,0,0,0,0,0,'','',0,''
	
		if((select ErrorValue from @temp)>0)
		begin
			select * from @temp;
			return;
		End
		else begin
			update Roster set [Carer Code]=@carer_code,type=isnull(dbo.getRosterType([Service Type]),0) where RecordNo=@RecordNo;
		
		end
	End
	else if (@OpsType='Un-Allocate')
		begin
			declare @branch varchar(100);
			select @branch=stf_department from staff where accountNo=@carer_code;
			UPDATE Roster SET [ShiftName] = [Carer Code], [Carer Code] = 'BOOKED', [Branch] = @branch, [TYPE] = 1, [SERVICE DESCRIPTION] = 'NOPAY', [Unit Pay Rate] = 0  WHERE RecordNO IN (375672)
	
		
		end
	else if (@OpsType='SetMultishift')
		begin
			update Roster set TA_Multishift=1 where RecordNo=@RecordNo;
		
		end
   else if (@OpsType='ClearMultishift')
		begin
			update Roster set TA_Multishift=0 where RecordNo=@RecordNo;
		
		end
	else if (@OpsType='Additional')
		begin
			update Roster set Notes=@notes where RecordNo=@RecordNo;
		
		end
	else if (@OpsType='Alert')
		begin

		INSERT INTO [dbo].[DeviceReminders] ([UserID] ,[ActivationDateTime] ,[StaffCode] ,[DestinationStaff] ,[Detail] ,[Status]) 
		values(@userId,@Roster_Date,@carer_code,@carer_code, @notes ,1)
		
	
	End
   
   else if (@OpsType='Copy' or @OpsType='Cut')
	begin
		
	

	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate]
		   )

		   
select	 	isnull(@ClientCodes, [Client Code])
           ,isnull(@carer_code, [Carer Code])
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,convert(varchar(50),convert(datetime,@Roster_Date,111),111)
           ,@Start_Time
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,day(@Roster_date) as [Dayno]
           ,@BlockNo as [BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,1 as [Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate] 
		   
		   from Roster where RecordNo=@RecordNo;


		    if (@OpsType='Cut')
			begin
				delete from Roster where RecordNo=@RecordNo;		
			end
	end
else if (@OpsType='Staff Master' )
	begin
	declare @carerCode varchar(100);
	select @carerCode=[Carer Code] from Roster where RecordNo=@RecordNo;
 		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate])

select		[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,FORMAT(year(getdate()),'0000')+'/'+ FORMAT(month(getdate()),'00') +'/'+format(Dayno,'00')
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,year(getdate()) as [YearNo]
           ,FORMAT(month(getdate()),'00') as [MonthNo]
           , [Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate] from Roster 
		   where [Carer Code]=@carerCode and YearNo=1900 and Dayno<=DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,getDate()),0)))
		   
	End

else if (@OpsType='Client Master' )
	begin
	declare @ClientCode varchar(100);
	select @ClientCode=[Client Code] from Roster where RecordNo=@RecordNo;
 		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate])

select		[Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,FORMAT(year(getdate()),'0000')+'/'+ FORMAT(month(getdate()),'00') +'/'+format(Dayno,'00')
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,year(getdate()) as [YearNo]
           ,FORMAT(month(getdate()),'00') as [MonthNo]
           , [Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate] from Roster 
		   where [Client Code]=@ClientCode and YearNo=1900 and Dayno<=DAY(DATEADD(DD,-1,DATEADD(MM,DATEDIFF(MM,-1,getDate()),0)))
		   
	End	
Else if (@OpsType='GroupShift')
begin
	declare @newProgram varchar(500);
	select @newProgram= isnull((SELECT top 1 ServiceProgram FROM ServiceOverview WHERE [Service Type] = r.[Service Type]  AND [ServiceStatus] = 'ACTIVE' AND PersonID = p.UniqueID),[Program]) 
	from roster r left join Recipients p on p.AccountNo in (select * from Split(@ClientCodes,',')) where  r.recordNo=@RecordNo
		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate])

		   
select		p.AccountNo as [Client Code]
           ,'!MULTIPLE' as [Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,isnull((SELECT top 1 ServiceProgram FROM ServiceOverview WHERE [Service Type] = r.[Service Type]  AND [ServiceStatus] = 'ACTIVE' AND PersonID = p.UniqueID),[Program]) 
           ,r.date
           ,r.[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           , [dbo].[Getbillingrate] (p.AccountNo,r.[service type],@newProgram) as [Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,r.[Dayno]
           ,[BlockNo]
           ,p.SpecialConsiderations as notes
           ,[CarerPhone]
           ,r.[UBDRef]
           ,r.[Type]
           ,r.[Status]
           , p.AgencyDefinedGroup as [Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[dbo].[getBillTo](p.AccountNo, r.[service type])
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,r.[Tagged]
           ,r.[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,r.[InUse]
           ,[DischargeReasonType]
           ,case  when (select top 1 DatasetGroup from ItemTypes where title=r.[Service Type]) in ('RESPITE CARE','CARER TRANSPORT','CARER COUNSELLING/SUPPORT INFO & ADVOCACY') then p.DatasetCarer  else p.AccountNo end  as [DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,r.[DeletedRecord]
           ,r.[COID]
           ,r.[BRID]
           ,r.[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,r.[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,r.[xEndDate] 
		   from Roster r 
		   left join Recipients p on p.AccountNo in (select * from Split(@ClientCodes,','))
		   where RecordNo=@RecordNo;


	END

	
	select @Recordno=@@identity;
	
	Insert into [Audit] (Operator,ActionDate,ActionOn,WhoWhatCode,TraccsUser,AuditDescription)
					 values ( @User,getDate(),'WEB-ROSTER',@Recordno,@User,@OpsType+' Roster from Web Portal') ;

    select @Recordno as Recordno ;
End



/****** Object:  StoredProcedure [dbo].[CreateRecurrentRosters]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER    PROCEDURE [dbo].[CreateRecurrentRosters](@RecordNo int, @startDate dateTime, @endDate datetime, @days varchar(50), @Frequency varchar(50), @Pattern varchar(50),@clientCodes varchar(MAX))
 As begin

	declare @week int;
	declare @prev_week int;
		Declare @NRecordNo int;
		Declare @isMaster bit;
		Declare @user varchar(50);
		Declare @startTime varchar(50);
		Declare @carerCode varchar(50);
		Declare @notes nvarchar(MAX);

	set @week=0;
	set @prev_week=0;
	WHILE ( @startDate <= @endDate)
	BEGIN
			--PRINT 'The counter value is = ' + convert(varchar, @startDate)
			
			if (@Frequency='Weekly' ) 
			begin
				if DATENAME(dw,@startDate) in (select * from split(@days,','))
				Begin
					 INSERT INTO ROSTER ([Client Code],[Carer Code],[Service Type],[Service Description],Program,[Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],YearNo, MonthNo, Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition) 
					 select [Client Code],[Carer Code],[Service Type],[Service Description],Program,convert(varchar,@startDate,111) as [Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],Year(@startDate) as YearNo, Month(@startDate) as MonthNo,Day(@startDate) as Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition
					 from Roster where RecordNo=@RecordNo;
				End 
			End
			-------------------------------------------------------------------------------
			if ( @Frequency='Fortnightly') 
			begin

				
				 set @week=(DATEPART(week, @startDate) - DATEPART(week, DATEADD(day, 1, EOMONTH(@startDate, -1)))) + 1;

				if ( @prev_week=0 or @prev_week=@week or  @week-@prev_week=2)   
					
				begin
					
					if (@prev_week=0) begin set @prev_week=@week end;

					if DATENAME(dw,@startDate) in (select * from split(@days,','))
					Begin
						INSERT INTO ROSTER ([Client Code],[Carer Code],[Service Type],[Service Description],Program,[Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],YearNo,MonthNo,Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition) 
						 select [Client Code],[Carer Code],[Service Type],[Service Description],Program,convert(varchar,@startDate,111) as [Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],Year(@startDate) as YearNo, Month(@startDate) as MonthNo,Day(@startDate) as Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition
						 from Roster where RecordNo=@RecordNo;
					End 
				
				End
			End
			-------------------------------------------------------------------------------
		
			if (@Frequency='Monthly') 
			begin
			
				 set @week=(DATEPART(week, @startDate) - DATEPART(week, DATEADD(day, 1, EOMONTH(@startDate, -1)))) + 1;

				if ((@Pattern='First' and @week=1) or  
					(@Pattern='Second' and @week=2) or 
					(@Pattern='Third' and @week=3) or  
					(@Pattern='Fourth' and @week=4) or 
					(@Pattern='All'))
				begin
					
					if DATENAME(dw,@startDate) in (select * from split(@days,','))
					Begin
						INSERT INTO ROSTER ([Client Code],[Carer Code],[Service Type],[Service Description],Program,[Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],YearNo,MonthNo,Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition) 
						 select [Client Code],[Carer Code],[Service Type],[Service Description],Program,convert(varchar,@startDate,111) as [Date],[Start Time],Duration,[Unit Pay Rate],[Unit Bill Rate],Year(@startDate) as YearNo, Month(@startDate) as MonthNo,Day(@startDate) as Dayno,BlockNo,CarerPhone,UBDRef,[Type],[Status],Anal,[Date Entered],[Date Timesheet],Transferred,GroupActivity,BillTo,CostUnit,CostQty,HACCType,BillUnit,BillQty,TaxPercent,ROS_PANZTEL_UPDATED,ServiceSetting,DATASETClient,Creator,TA_EarlyStart,TA_LateStart,TA_EarlyGo,TA_LateGo,TA_TooShort,TA_TooLong,StaffPosition
						 from Roster where RecordNo=@RecordNo;
					End 
				End
			End
	

		Select @NRecordNo=@@identity;
		select @user=user,@isMaster=case when YearNo=1900 then 1 else 0 end,@startTime=[Start Time],@carerCode=[Carer Code],@notes=Notes
		from Roster where recordNo= @NRecordNo;

		execute spRosterOperations 'GroupShift',@user,@NRecordNo, @isMaster,@startDate,@startTime,@carerCode,@notes,@clientCodes;

		set @startDate=dateadd(DAY, 1, @startDate)
		

	END
	
	delete from Roster where RecordNo=@RecordNo;
End

GO
/****** Object:  StoredProcedure [dbo].[usp_DebtorPost_U]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_DebtorPost_U] (
	@invoiceNumber NVARCHAR(MAX),
	@batchNumber FLOAT,
	@branches VARCHAR(max) = null,
	@programs NVARCHAR(max) = null,
	@categories NVARCHAR(max) = null,
	@startdate NVARCHAR(10),
	@enddate NVARCHAR(10),
	@invoiceType NVARCHAR(max),
	@sepInvRecipient NVARCHAR(max),
	@operatorID NVARCHAR(MAX),
	@currentDateTime NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Records INTEGER
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @Parameters NVARCHAR(MAX)
	
	--====== CREATE OR ALTER temp table by using below query to store table colums for update records executing purpose =======

	SELECT ro.Batch#, pr.UserYesNo1, pr.[Type], ro.[status], re.BRANCH, ro.Program, ro.Anal, 
	ro.RecordNo, ro.BILLTO, ro.[CLIENT CODE], ro.[DATE], ro.[START TIME], ro.DURATION, ro.INVOICENUMBER, S.Invoice#
	INTO #tmpDeb
	FROM 
		Roster ro 
		LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.[Program]
		LEFT JOIN Recipients re ON ro.[Client Code] = re.[AccountNo]
		LEFT JOIN RECIPIENTS rec ON ro.BILLTO = rec.ACCOUNTNO
		CROSS JOIN SYSTABLE S
	WHERE 1=2;

	--=======Fill temp table values to update records===============

	SET @Parameters = '
		@invoiceNumber NVARCHAR(MAX),
		@batchNumber FLOAT,
		@branches VARCHAR(max),
		@programs NVARCHAR(max) = null,
		@categories NVARCHAR(max),
		@startdate NVARCHAR(10),
		@enddate NVARCHAR(10),
		@invoiceType NVARCHAR(max)'

	SET @SQL = '
		INSERT INTO #tmpDeb
		SELECT ro.Batch#, pr.UserYesNo1, pr.[Type], ro.[status], re.BRANCH, ro.Program, ro.Anal, 
		ro.RecordNo, ro.BILLTO, ro.[CLIENT CODE], ro.[DATE], ro.[START TIME], ro.DURATION, ro.INVOICENUMBER, S.Invoice#
		FROM 
			Roster ro 
			LEFT JOIN HumanResourceTypes pr ON pr.Name = ro.[Program]
			LEFT JOIN Recipients re ON ro.[Client Code] = re.[AccountNo]
			LEFT JOIN RECIPIENTS rec ON ro.BILLTO = rec.ACCOUNTNO
			CROSS JOIN SYSTABLE S
		WHERE 
			ro.[Date] > ''1920/01/01'' 
			AND (ro.[Date] Between @startdate AND @enddate)
			AND IsNull(pr.Type, '''') <> ''DOHA'' 
			AND ([pr].[Type] <> ''NDIA'')
			AND (ro.[Type] IN (2,3,4,5,6,7,8,9,10,11,12,14))  
			AND (ro.[BillTo] <> ''!INTERNAL'') 
			AND (ro.[Batch#] is null) 
			AND (ro.[Status] >= 2)  
			AND (ro.[Client Code] <> ''!MULTIPLE'')'

	IF @branches IS NOT NULL AND @branches <> ''
	SET @SQL = @SQL+ ' AND (re.[BRANCH] IN (select Val from ufn_splitstring(@branches,'','')))'
	
	IF @programs IS NOT NULL AND @programs <> ''
	SET @SQL = @SQL+ ' AND (ro.[Program] IN (select Val from ufn_splitstring(@programs,'','')))'
	
	IF @categories IS NOT NULL AND @categories <> ''
	SET @SQL = @SQL+ ' AND (ro.[Anal] IN (select Val from ufn_splitstring(@categories,'','')))'
			
	IF @invoiceType = 'Homecare Package Invoices'
			SET @SQL = @SQL+ ' AND IsNull(pr.UserYesNo1, 0) = 1 AND Round(Round(ro.[Billqty], 2) * ro.[Unit Bill Rate], 2) > 0'
		ELSE IF @invoiceType = 'NDIA Claim Update Invoices'
			SET @SQL = @SQL+ ' AND [pr].[Type] = ''NDIA'' AND isnull(NDIABATCH, 0) <> 0'
		ELSE IF @invoiceType = 'General'
			SET @SQL = @SQL+ ' AND IsNull([pr].[Type], '''') <> ''DOHA'' AND ([pr].[Type] <> ''NDIA'')'
	
	EXEC sp_Executesql @SQL, @Parameters,
	@invoiceNumber=@invoiceNumber, 
	@batchNumber=@batchNumber, 
	@branches=@branches, 
	@programs=@programs, 
	@categories=@categories, 
	@startdate=@startdate,
	@enddate=@enddate,
	@invoiceType=@invoiceType;

	--==========add conditions to update records=================

	UPDATE Roster SET 
	status = case when [status] = '2' then '3' else '4' end, 
	InvoiceNumber = @invoiceNumber,
	Batch# = @batchNumber
	WHERE RecordNo IN (SELECT tm.RecordNo FROM #tmpDeb tm)

	--==========Add another condition to update records==============

	IF @sepInvRecipient = 'true'
		UPDATE T SET INVOICENUMBER = NEW_INVOICE_NUMBER
		FROM
		(
		SELECT RO.RecordNo, RO.BILLTO, RO.[CLIENT CODE], RO.[DATE], RO.[START TIME], RO.DURATION, RO.INVOICENUMBER, S.Invoice#,
				S.Invoice# + DENSE_RANK() OVER( ORDER BY RO.BILLTO, RO.[CLIENT CODE]) AS NEW_INVOICE_NUMBER
		FROM ROSTER RO INNER JOIN RECIPIENTS REC ON RO.BILLTO = REC.ACCOUNTNO
						CROSS JOIN SYSTABLE S
		WHERE [DATE] BETWEEN @startdate AND @enddate
		AND RO.RecordNo IN (SELECT tm.RecordNo FROM #tmpDeb tm)
		AND RO.BILLTO IN (SELECT tm.BILLTO FROM #tmpDeb tm)
		AND RO.[CLIENT CODE] IN (SELECT tm.[CLIENT CODE] FROM #tmpDeb tm))
		AS T

	--===========Add another condition to update records==============

	IF @sepInvRecipient = 'false'
		UPDATE T SET INVOICENUMBER = NEW_INVOICE_NUMBER
		FROM
		(
		SELECT RO.RecordNo, RO.BILLTO, RO.[CLIENT CODE], RO.[DATE], RO.[START TIME], RO.DURATION, RO.INVOICENUMBER, S.Invoice#,
				S.Invoice# + DENSE_RANK() OVER( ORDER BY RO.BILLTO) AS NEW_INVOICE_NUMBER
		FROM ROSTER RO INNER JOIN RECIPIENTS REC ON RO.BILLTO = REC.ACCOUNTNO
						CROSS JOIN SYSTABLE S
		WHERE [DATE] BETWEEN @startdate AND @enddate
		AND RO.RecordNo IN (SELECT tm.RecordNo FROM #tmpDeb tm)
		AND RO.BILLTO IN (SELECT tm.BILLTO FROM #tmpDeb tm)
		AND RO.[CLIENT CODE] IN (SELECT tm.[CLIENT CODE] FROM #tmpDeb tm))
		AS T
	
	--=============Close conditions and get updated record count details that have been effected=======
	
	SELECT @Records = @@ROWCOUNT
	SELECT @Records AS updatedRecords

	--===========Insert / update Debtor records detail==============

	INSERT INTO batch_record (BCH_NUM, BCH_TYPE, BCH_USER, BCH_DATE) VALUES (@batchNumber, 'B', @operatorID, @currentDateTime)

	INSERT INTO PAY_BILL_BATCH (OperatorID, BatchDate, BatchNumber, BatchDetail, BatchType, Date1, Date2) VALUES (@operatorID, @currentDateTime, @batchNumber, 'Invoice Update', 'B', '', '')

	UPDATE Systable SET Invoice# = (SELECT Invoice# FROM Systable WHERE SQLID > 0) + 1

	--===============Delete tmpDeb table================

	DROP TABLE #tmpDeb

END
GO
/****** Object:  StoredProcedure [dbo].[usp_HumanResourceTypes_U]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER    PROCEDURE [dbo].[usp_HumanResourceTypes_U] (
	@closedate NVARCHAR(MAX),
	@program NVARCHAR(MAX),
	@funding NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Records INTEGER

	UPDATE HumanResourceTypes SET
	CloseDate = @closedate
	WHERE Name IN (select Val from ufn_splitstring(@program,','))
	AND Type IN (select Val from ufn_splitstring(@funding,','))

--=============Close conditions and get updated record count details that have been effected=======

SELECT @Records = @@ROWCOUNT
SELECT @Records AS updatedRecords

END
GO
/****** Object:  StoredProcedure [dbo].[usp_PayUpdatePost_U]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_PayUpdatePost_U] (
	@batchNumber FLOAT,
	@branches VARCHAR(max) = null,
	@programs NVARCHAR(max) = null,
	@categories NVARCHAR(max) = null,
	@startdate NVARCHAR(10),
	@enddate NVARCHAR(10),
	@chkStaffPays NVARCHAR(max),
	@chkPayBrokerage NVARCHAR(max),
	@operatorID NVARCHAR(MAX),
	@currentDateTime NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Records INTEGER
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @Parameters NVARCHAR(MAX)
	
	--====== CREATE OR ALTER temp table by using below query to store table colums for update records executing purpose =======

	SELECT [Roster].[recordno], [ROSTER].[Status], [ROSTER].[date payroll], [ROSTER].[TimesheetNumber], [ROSTER].[ros_pay_batch], 
		[Recipients].[BRANCH], [ROSTER].[Program], [ROSTER].[Anal], [Roster].[Date], [Roster].[Type], [Staff].[Category]
	INTO #tmpPay
	FROM  
		ROSTER 
		INNER JOIN Staff ON Roster.[Carer Code] = Staff.[AccountNo] 
		INNER JOIN [ItemTypes] ON Roster.[Service Description] = [ItemTypes].[Title] 
		INNER JOIN [ItemTypes] it1 ON Roster.[Service Type] = it1.[Title] 
		INNER JOIN [HumanResourceTypes] ON Roster.[Program] = [HumanResourceTypes].[Name] 
		LEFT JOIN RECIPIENTS ON [Roster].[BILLTO] = [Recipients].[ACCOUNTNO]
	WHERE 1=2;

	--=======Fill temp table values to update records===============

	SET @Parameters = '
		@batchNumber FLOAT,
		@branches VARCHAR(max),
		@programs NVARCHAR(max),
		@categories NVARCHAR(max),
		@startdate NVARCHAR(10),
		@enddate NVARCHAR(10),
		@chkStaffPays NVARCHAR(max),
		@chkPayBrokerage NVARCHAR(max),
		@currentDateTime NVARCHAR(MAX)'

	SET @SQL = '
		INSERT INTO #tmpPay
		SELECT [Roster].[recordno], [ROSTER].[Status], [ROSTER].[date payroll], [ROSTER].[TimesheetNumber], [ROSTER].[ros_pay_batch], 
		[Recipients].[BRANCH], [ROSTER].[Program], [ROSTER].[Anal], [Roster].[Date], [Roster].[Type], [Staff].[Category] 
		FROM  
		ROSTER 
			INNER JOIN Staff ON Roster.[Carer Code] = Staff.[AccountNo] 
			INNER JOIN [ItemTypes] ON Roster.[Service Description] = [ItemTypes].[Title] 
			INNER JOIN [ItemTypes] it1 ON Roster.[Service Type] = it1.[Title] 
			INNER JOIN [HumanResourceTypes] ON Roster.[Program] = [HumanResourceTypes].[Name] 
			LEFT JOIN RECIPIENTS ON [Roster].[BILLTO] = [Recipients].[ACCOUNTNO]
		WHERE 1=1
			AND ([Roster].[Date] > ''1920/01/01'')
			AND ([Roster].[Status] IN (2,3))
			AND ([Roster].[Date] Between @startdate AND @enddate)
			AND ([Roster].[Type] IN (2,3,4,5,6,7,8,9,10,11,12))'

			--AND ([Roster].[Date] Between ''' + @startdate  + ''' AND ''' + @enddate + ''')

	IF @branches IS NOT NULL AND @branches <> ''
	SET @SQL = @SQL+ ' AND ([Recipients].[BRANCH] IN (select Val from ufn_splitstring(@branches,'','')))'
	
	IF @programs IS NOT NULL AND @programs <> ''
	SET @SQL = @SQL+ ' AND ([Roster].[Program] IN (select Val from ufn_splitstring(@programs,'','')))'
	
	IF @categories IS NOT NULL AND @categories <> ''
	SET @SQL = @SQL+ ' AND ([Roster].[Anal] IN (select Val from ufn_splitstring(@categories,'','')))'
	
	IF @chkStaffPays = 'true' AND @chkPayBrokerage  = 'false'
	SET @SQL = @SQL+ ' AND ([Staff].[Category] IN (''STAFF'', ''VOLUNTEER''))'
	
	IF @chkStaffPays = 'false' AND @chkPayBrokerage  = 'true'
	SET @SQL = @SQL+ ' AND ([Staff].[Category] IN (''BROKERAGE ORGANISATION'', ''SUNDRY BROKERAGE SUPPLIER''))'
	
	IF @chkStaffPays = 'true' AND @chkPayBrokerage  = 'true'
	SET @SQL = @SQL+ ' AND ([Staff].[Category] IN (''BROKERAGE ORGANISATION'', ''SUNDRY BROKERAGE SUPPLIER'', ''STAFF'', ''VOLUNTEER''))'
	
	EXEC sp_Executesql @SQL, @Parameters,
	@batchNumber=@batchNumber,
	@branches=@branches,
	@programs=@programs,
	@categories=@categories,
	@startdate=@startdate,
	@enddate=@enddate,
	@chkStaffPays=@chkStaffPays,
	@chkPayBrokerage=@chkPayBrokerage,
	@currentDateTime=@currentDateTime;

	--==========add conditions to update records=================

	UPDATE Roster SET 
	[ROSTER].[Status] = CASE WHEN [ROSTER].[Status] = '2' THEN '5' ELSE '4' END, 
	[ROSTER].[date payroll] = CONVERT(date, @currentDateTime,111),
	[ROSTER].[TimesheetNumber] = (SELECT TsheetNum FROM systable) + 1,
	[ROSTER].[ros_pay_batch] = @batchNumber
	WHERE RecordNo IN (SELECT tm.RecordNo FROM #tmpPay tm)

	--=============Close conditions and get updated record count details that have been effected=======
	
	SELECT @Records = @@ROWCOUNT
	SELECT @Records AS updatedRecords

	--===========Insert / update Pay records detail==============

	INSERT INTO batch_record (BCH_NUM, BCH_TYPE, BCH_USER, BCH_DATE) VALUES (@batchNumber, 'P', @operatorID, @currentDateTime)

	INSERT INTO PAY_BILL_BATCH (OperatorID, BatchDate, BatchNumber, BatchDetail) VALUES (@operatorID, @currentDateTime, @batchNumber, 'ALL Dates, ALL Branches, ALL Categories, ALL Billing Cycles, EXCLUDE !INTERNAL')
	
	UPDATE Systable SET TSheetNum = (SELECT TSheetNum FROM systable) + 1
	
	--===============Delete tmp table================

	DROP TABLE #tmpPay

END

GO
/****** Object:  StoredProcedure [dbo].[usp_RollbackInvoiceBatch_U]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_RollbackInvoiceBatch_U] (
	@operatorID NVARCHAR(MAX),
	@currentDateTime NVARCHAR(MAX),
	@batchNumber NVARCHAR(MAX),
	@batchDescription NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Records INTEGER

	
--=============update and delete queries to RollBack Batch Invoices=======

DELETE FROM roster WHERE [batch#] = @batchNumber AND ((Type = 9 AND Left([Service Type], 12) = 'CONTRIBUTION') OR (Type = 9 AND Left([Service Type], 6) = 'CAPPED')) 

DELETE FROM roster WHERE [batch#] = @batchNumber AND [SERVICE TYPE] IN ('BASIC CARE FEE', 'INCOME TESTED FEE', 'AGREED TOP UP FEE')

UPDATE roster SET status = CASE WHEN status = '3' then '2' when status = '4' then '5' else status end,[unit bill rate] = oldbill, [billqty] = oldqty, [Batch#] = Null, [Date Invoice] = Null, [InvoiceNumber] = Null WHERE [batch#] = @batchNumber

DELETE FROM InvoiceHeader WHERE (HType <> 'R' OR (Htype = 'R' AND Type1 = 'PRSNL')) AND batchnumber = @batchNumber

UPDATE InvoiceHeader SET Updated = 0, BatchNumber = Null WHERE batchnumber = @batchNumber

Update batch_record Set bch_type='S' where bch_num = @batchNumber and bch_type = 'B' 

INSERT INTO PAY_BILL_BATCH (OperatorID, BatchDate, BatchNumber, BatchDetail)VALUES (@operatorID, @currentDateTime, @batchNumber, @batchDescription)

--=============Close conditions and get updated record count details that have been effected=======

SELECT @Records = @@ROWCOUNT
SELECT @Records AS updatedRecords

END

GO
/****** Object:  StoredProcedure [dbo].[usp_RollbackPayrollBatch_U]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER PROCEDURE [dbo].[usp_RollbackPayrollBatch_U] (
	@operatorID NVARCHAR(MAX),
	@currentDateTime NVARCHAR(MAX),
	@batchNumber NVARCHAR(MAX),
	@batchDescription NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Records INTEGER

--=============update and delete queries to RollBack Batch Payroll=======

UPDATE roster SET status = CASE WHEN status = '5' then '2' when status = '4' then '3' else status end,[Ros_Pay_Batch] = Null, [Date Payroll] = Null, [TimesheetNumber] = Null WHERE [ros_pay_batch] = @batchNumber

DELETE FROM TsheetHeader WHERE batchnumber = @batchNumber

Update batch_record Set bch_type='Q' where bch_num = @batchNumber and bch_type = 'P' 

INSERT INTO PAY_BILL_BATCH (OperatorID, BatchDate, BatchNumber, BatchDetail) VALUES (@operatorID, @currentDateTime, @batchNumber, @batchDescription)

--=============Close conditions and get updated record count details that have been effected=======

SELECT @Records = @@ROWCOUNT
SELECT @Records AS updatedRecords

END

GO
/****** Object:  StoredProcedure [dbo].[usp_RollbackRosterBatch_U]    Script Date: 27/04/2022 10:57:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER PROCEDURE [dbo].[usp_RollbackRosterBatch_U] (
	@operatorID NVARCHAR(MAX),
	@currentDateTime NVARCHAR(MAX),
	@batchNumber NVARCHAR(MAX),
	@batchDescription NVARCHAR(MAX)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Records INTEGER

--=============update and delete queries to RollBack Batch Roster=======

DELETE FROM roster WHERE [ros_copy_batch#] = @batchNumber

DELETE FROM rostercopy WHERE rsc_id = @batchNumber

--=============Close conditions and get updated record count details that have been effected=======

SELECT @Records = @@ROWCOUNT
SELECT @Records AS updatedRecords

END

GO

Create or ALTER procedure [dbo].[spCreate_PayMakeUp](@s_Records varchar(MAX))
As
begin
 INSERT INTO MakeUpPays (StaffCode, ServiceID, ServiceDate, ExpiryDate, PayQty, TimebandType) 
SELECT StaffCode, ServiceID, ServiceDate, ExpiryDate, PayQty, 
                            CASE 
                            WHEN ISNULL(PublicHoliday, 0) > 0 AND 
                                (ISNULL(PHState, '') > '' AND PHState = reState)  AND 
                                (ISNULL(PHPublicHolidayRegion, '') > '' AND PHPublicHolidayRegion = STPublicHolidayRegion)  THEN 'PUB HOL' 
                            WHEN DATENAME(WEEKDAY, ServiceDate) IN ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday') THEN 'WEEKDAY NORMAL' 
                            ELSE UPPER (DATENAME(Weekday, ServiceDate)) 
                            END 
                            AS TimebandType 
                            FROM 
                            (
                      
                            SELECT 
                            ro.[RecordNo] AS ServiceID, 
                            [Carer Code] AS StaffCode, 
                            ro.[Date] AS ServiceDate, 
                            ro.[CostQty] AS PayQty, 
                            CONVERT(VARCHAR, DATEADD(DAY, 42, ro.[Date]), 111) AS ExpiryDate, 
                            ph.recordno AS PublicHoliday, ph.[Stats] AS PHState, ph.PublicHolidayRegion AS PHPublicHolidayRegion, st.PublicHolidayRegion AS STPublicHolidayRegion, 
                            CASE 
                            WHEN LEFT(na.Postcode, 1) = 2 THEN 'NSW' 
                            WHEN LEFT(na.Postcode, 1) = 3 THEN 'VIC' 
                            WHEN LEFT(na.Postcode, 1) = 4 THEN 'QLD' 
                            WHEN LEFT(na.Postcode, 1) = 5 THEN 'SA' 
                            WHEN LEFT(na.Postcode, 1) = 6 THEN 'WA' 
                            WHEN LEFT(na.Postcode, 1) = 7 THEN 'TAS' 
                            END AS reState, 
                            na.Postcode, 
                            DATENAME(WEEKDAY, ro.[Date]) AS DayName, 
                            [client code] AS Client 
                    
                            FROM ROSTER ro 
                            LEFT JOIN STAFF st ON ro.[carer code] = st.AccountNo 
                            LEFT JOIN RECIPIENTS re ON ro.[Client Code] = re.AccountNo 
                            LEFT JOIN PUBLIC_HOLIDAYS ph ON ro.[Date] = ph.Date 
                            LEFT JOIN NamesAndAddresses na ON re.UniqueID = na.PersonID AND na.PrimaryAddress = 1 
                            WHERE ro.[Type] > 1 AND [Carer Code] > '!z' 
                            ) t 
                            WHERE ServiceID IN  ( @s_Records);
                            
                       
                                         


End
Go
---------------------------------Cancel Shift Script---------------------------------------------

Create OR ALTER procedure [dbo].[spCancelShift](@RecordNo varchar(20),@ClientCode varchar(100), @CancelPayType varchar(500),@CancelShiftServiceType varchar(500), @CancelShiftProgram varchar(500),@CancelCode varchar(500),@AlternateRecipient varchar(100)
,@s_Reason varchar(500),@Operator varchar(50), @b_KeepInMDS bit, @b_BillAsOriginal bit,@b_RetainDataset bit=1,@s_CancelRate bit, @RemDate Varchar(10),@OPNote ntext,@Category varchar(100), @ReminderUser varchar(50)) 
AS
begin  
    
--Declare @RosterYearNo, RosterMonthNo, RosterDayNo, RosterBlockNo Asint
Declare @strMessage varchar(500)
Declare @s_LeaveRecordNo varchar(500)
Declare @s_OldActivity varchar(500)
Declare @s_CloseDate varchar(500) ;
Declare @s_Worker varchar(500)
Declare @s_NewDatasetCode varchar(500)
Declare @s_NewBillingRate  float
Declare @s_NewBillingUnit varchar(500)
Declare @BillQty  int;
Declare @BillRate  float;
Declare @BillUnit  varchar(50);
Declare @s_NewDebtor varchar(500)
Declare @b_NDIAAbsenceCapExceeded  bit
Declare @b_CancelledAfter3pmPreviousDay  bit

Declare @HACCType varchar(50)

Declare @Error  varchar(500)
Declare @NewRecordNo int;
Declare @s_AbsenceType varchar(500)
declare @CloseDate  varchar(50);
Declare @OldActivity varchar(500);
Declare @OldProgram varchar(500);
Declare @OldPaytype varchar(500);
Declare @b_NDIA bit;
declare @rosterDate varchar(10);
declare @startTime  varchar(10);
declare @CarerCode  varchar(10);
DEclare @PersonID varchar(50);
Declare @BillTo varchar(100);
Declare @i_NDIAPercent int;


select @rosterDate=[date],@startTime=[Start Time],@OldActivity=[Service Type],@OldProgram=[program],@OldPaytype=[Service Description],
@BillQty=billqty,@BillRate=[Unit Bill Rate],@BillUnit=[BillUnit],@BillTo=BillTo,@CarerCode=[Carer Code]
from roster  where RecordNo=@RecordNo;
select @CloseDate=convert(varchar,isnull(CloseDate,''),111),@b_NDIA=case when type='NDIA' then 1 else 0 end from HumanResourceTypes where [Name] = @OldProgram;
select @s_NewDatasetCode=HACCType from ItemTypes where Title=@OldActivity;

select @PersonID=UniqueId from Recipients where AccountNo=@ClientCode;

set @s_AbsenceType=@CancelShiftProgram;
	--if (@s_AbsenceType = '' or @s_AbsenceType is null)
          --If Not EvaluateActivityType('SELECT ACTIVITY', 'SELECT ACTIVITY', 'RECPTABSENCE', 'ATTRIBUTABLE', 'EVENT', s_AbsenceType) Then GoTo x_CancelEntry
		--	set @s_AbsenceType=@OldProgram;

    -- if (@s_Reason = '' )      
    --    set @Error='Insert a short description of the reason for the cancellation';
		
        --CREATE NEW ROSTER ENTRY STARTING WITH A COMPLETE COPY OF ALL FIELDS
		
	INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate]
		   )

		   
select	 	isnull(@ClientCode, [Client Code])
           , '!INTERNAL'
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate] 
		   
		   from Roster where RecordNo=@RecordNo;

      select @NewRecordNo=@@IDENTITY;
       
      
--CHANGE FIELDS TO NEW DETAIL FOR NEWLY CREATED ABSENCE RECORD
         set @strMessage = 'ORIGINALLY SCHEDULED FOR ' + @s_AbsenceType + ' BY ' + @RecordNo + '';
         
       
         if (@CloseDate<>'')
         begin
           
            if ( @rosterDate > '2000/01/01' and @rosterDate <= @CloseDate )
				begin
					set @Error='Rostering for program ' + @OldProgram + ' Closed before or on ' + @CloseDate;                
					return;
				end
         end

    

          if (@b_NDIA =1)
		  begin

            select  @i_NDIAPercent = ndiaCancelFeePercent from registration

                if (@b_BillAsOriginal=1)
                    set @s_NewBillingRate = @billrate * ((@i_NDIAPercent / 100)) ;
                else
                    set  @s_NewBillingRate = 0;

                set @s_NewDebtor = 'NDIA'
          end 
		  else 
		  begin
             if (@b_BillAsOriginal=0)
			 begin
                SELECT  @s_NewBillingRate=dbo.GetBillingRate(@clientCode,@CancelCode, @oldProgram),
						@s_NewBillingUnit=isnull(unittype,@billunit),						
						@s_NewDebtor=isnull(servicebiller,'') 			
				FROM    serviceoverview
				WHERE   personid = @PersonID
						AND serviceprogram = @oldProgram
						AND [service type] = @OldActivity
			       
         
                if (@s_NewBillingUnit = 'SERVICE') 
                    set @BillQty = 1;    
                if (@b_RetainDataset=1 ) 
					set @HACCType = @s_NewDatasetCode;

                    
            end 
			else  begin
              
                set @s_NewDebtor = @BillTo
                set @s_NewBillingRate =@billrate
                set @s_NewBillingUnit=@billunit
                if (@s_CancelRate> '' ) 
                    set @s_NewBillingRate =round((@s_NewBillingRate) * (@s_CancelRate) / 100,2)
                
            end
            
        End

         if (@s_NewBillingRate != 0 and isnull(@s_NewDebtor,'') = '')                 
              set @s_NewDebtor= @BillTo;
         else
			set   @s_NewDebtor = @AlternateRecipient;
        
        --if (@b_KeepInMDS ) s_OldActivity = this.shift.activity;
        
		 If (@CancelShiftProgram = '') set @CancelShiftProgram=@OldProgram;

		
            
            
            --'QUERY - SHOULD status be reliant on autoapprove fuinctionality for recipient absences
            Declare @AutoApprove bit;
            Declare @autoAutoApproveRecipientAdmin bit;
			Declare @status int;
			Declare @PayPeriodEndDate varchar(50);

			select @AutoApprove=IsNull(AutoApprove, 0) from ItemTypes where Title = @OldActivity AND ProcessClassification = 'OUTPUT';
			select @autoAutoApproveRecipientAdmin=autoAutoApproveRecipientAdmin from registration;
			select @PayPeriodEndDate=PayPeriodEndDate from SysTable ;
           
                if (@AutoApprove=1 and @carercode<>'BOOKED' )                    
                    set @status = 2;
				else
					set @PayPeriodEndDate=null;

                if (@rosterDate>'2000/01/01' and @autoAutoApproveRecipientAdmin=1)                    
                    set @status = 2;
                else
                   set @status = 1;
     declare @haccid int;
	 set @haccid=case when @b_KeepInMDS=1 then -1 else -0 end         
	 declare @shiftmame  varchar(500);

	  If (@b_KeepInMDS=1)  
		set @shiftmame = @CancelShiftServiceType;
	else
		set @shiftmame=@s_OldActivity;

       update Roster set
         [ShiftName] = @shiftmame,
         [Unit Pay Rate] = 0,
         [Carer Code] = '!INTERNAL',      
         [Service Type] = @CancelCode,
         [Service Description] = 'N/A',
         [Type] = 4,
         VariationReason = @s_reason,
         [Program] = @CancelShiftProgram,
         BillTo = @s_NewDebtor, 
         [Unit Bill Rate] =@s_NewBillingRate, BillQTy = @BillQty ,        
		 [ServiceSetting] = '',
		 [Date Entered] = getDate(),
		 [Creator] = '${this.user}',
		 [Date Last Mod] = getDate(),		 
		[Date Invoice] = Null,
		[InvoiceNumber] = Null,
		[TimesheetNumber] = Null,
		[batch#] = Null,
		[NDIABatch] = Null,
		InUse = 0,
		Transferred = 0,
		ROS_PANZTEL_UPDATED = 0,
		TA_EarlyStart = 0,
		TA_LateStart = 0,
		TA_EarlyGo = 0,
		TA_LateGo = 0,
		TA_TooShort = 0,
		TA_TooLong = 0,
		Editer = @Operator,
		HACCID= @haccid,
		status=@status,
		[Date Timesheet]=@PayPeriodEndDate
		where RecordNo=@NewRecordNo;
			               
                   
        insert into [Audit](Operator,ActionDate,ActionOn,WhoWhatCode,TraccsUser,AuditDescription) 
        values(@Operator, getDate(), 'Web Day Manager', convert(varchar,@NewRecordNo), @Operator,
                    'AUTO CREATE CANCELLED SHIFT PAY ' + convert(varchar,@NewRecordNo) +' ' +  @rosterDate + 'at' +  @startTime + 'for'  + @CancelShiftServiceType + ' WITH ' +   isnull(@AlternateRecipient,@ClientCode)  + '-SERVICE CANCELLED')
          
		              
		--Creating OP Note
		 insert into History(PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator, AlarmDate)
					Values (@PersonID, getDate(), @OPNote,'OPNOTE', @Category, @ClientCode , @ReminderUser, @RemDate );            
     
	 --SVC note for new Record
        insert into History ( PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator)
		Values (@NewRecordNo, getDate(), 'ORIGINALLY SCHEDULED FOR ' + @CancelShiftServiceType +' BY ' + @CarerCode
		,'SVCNOTE', '#AUTO-CANCEL', @NewRecordNo, @Operator  ); 
        
		if (isnull(@CancelPayType,'')='')  
		begin
		
			 insert into [Audit](Operator,ActionDate,ActionOn,WhoWhatCode,TraccsUser,AuditDescription) 
			values(@Operator, getDate(), 'Roster', convert(varchar,@RecordNo), @Operator,
                    'DELETED DAYMANAGER')

        	INSERT INTO ROSTER_Del SELECT * FROM Roster WHERE RecordNo=@RecordNo;
			delete from roster where recordNo=@RecordNo;
		  
			return
		End
		  
		  --SVC note for old cancelled record 
		insert into History(PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, AlarmDate,Creator)
        Values( @RecordNo, getDate(), 'AUTOMATIC LEAVE PAY ITEM FOR SHIFT# ' + Convert(varchar,@RecordNo) + ' : ' +  @rosterDate + ' at ' + @startTime  + ' for '  + @OldActivity + ' WITH '+ @ClientCode + ' -SERVICE CANCELLED', 
                    'SVCNOTE', '#AUTO-CANCEL', Convert(varchar,@RecordNo), @RemDate , @Operator);
                
     

       Update Roster  SET [Client Code] = '!INTERNAL', BillTo = '!INTERNAL', [Service Description] = @CancelPayType, [Service Type] = @CancelShiftServiceType, 
       [Type] = 6, [Unit Bill Rate] = 0, BillQTy = CostQty, Duration = (CostQty * 60) / 5
	  where RecordNo=@RecordNo; 
      
	  INSERT INTO MakeUpPays (StaffCode, ServiceID, ServiceDate, ExpiryDate, PayQty, TimebandType) 
	  SELECT StaffCode, ServiceID, ServiceDate, ExpiryDate, PayQty, 
                            CASE 
                            WHEN ISNULL(PublicHoliday, 0) > 0 AND 
                                (ISNULL(PHState, '') > '' AND PHState = reState)  AND 
                                (ISNULL(PHPublicHolidayRegion, '') > '' AND PHPublicHolidayRegion = STPublicHolidayRegion)  THEN 'PUB HOL' 
                            WHEN DATENAME(WEEKDAY, ServiceDate) IN ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday') THEN 'WEEKDAY NORMAL' 
                            ELSE UPPER (DATENAME(Weekday, ServiceDate)) 
                            END 
                            AS TimebandType 
                            FROM 
                            (
                      
                            SELECT 
                            ro.[RecordNo] AS ServiceID, 
                            [Carer Code] AS StaffCode, 
                            ro.[Date] AS ServiceDate, 
                            ro.[CostQty] AS PayQty, 
                            CONVERT(VARCHAR, DATEADD(DAY, 42, ro.[Date]), 111) AS ExpiryDate, 
                            ph.recordno AS PublicHoliday, ph.[Stats] AS PHState, ph.PublicHolidayRegion AS PHPublicHolidayRegion, st.PublicHolidayRegion AS STPublicHolidayRegion, 
                            CASE 
                            WHEN LEFT(na.Postcode, 1) = 2 THEN 'NSW' 
                            WHEN LEFT(na.Postcode, 1) = 3 THEN 'VIC' 
                            WHEN LEFT(na.Postcode, 1) = 4 THEN 'QLD' 
                            WHEN LEFT(na.Postcode, 1) = 5 THEN 'SA' 
                            WHEN LEFT(na.Postcode, 1) = 6 THEN 'WA' 
                            WHEN LEFT(na.Postcode, 1) = 7 THEN 'TAS' 
                            END AS reState, 
                            na.Postcode, 
                            DATENAME(WEEKDAY, ro.[Date]) AS DayName, 
                            [client code] AS Client 
                    
                            FROM ROSTER ro 
                            LEFT JOIN STAFF st ON ro.[carer code] = st.AccountNo 
                            LEFT JOIN RECIPIENTS re ON ro.[Client Code] = re.AccountNo 
                            LEFT JOIN PUBLIC_HOLIDAYS ph ON ro.[Date] = ph.Date 
                            LEFT JOIN NamesAndAddresses na ON re.UniqueID = na.PersonID AND na.PrimaryAddress = 1 
                            WHERE ro.[Type] > 1 AND [Carer Code] > '!z' 
                            ) t 
                            WHERE ServiceID IN  ( @RecordNo);
                            
							              

--// 'CREATE THE ROSTER RECORD
--// 280         If SaveLeaveAbsence(rsNewRecord, s_LeaveRecordNo) Then
--// 282             CreateServiceNote Now, operatorid, s_LeaveRecordNo, strMessage & ' As ' & s_Reason, '#AUTO-CANCEL', s_status
--// TraccsDb.Execute 'UPDATE Audit SET WhoWhatCode = '' & N2S(s_LeaveRecordNo) & '' WHERE WhoWhatCode = '' & s_RosterRecordint & '''
--// End If

--// 'Anwaar 22Jan16As
--// Declare @s_MobileFutureLimit varchar(500)
--// 284         s_MobileFutureLimit = DBLookup(TraccsDb, 'ISNULL(MobileFutureLimit , 0) AS MobileFutureLimit ', 'UserInfo', 'StaffCode  = '' & RosterRecord(2) & ''', 1)
--// 286         If s_MobileFutureLimit = '' Then s_MobileFutureLimit = '0'

--// 288         If DBLookup(TraccsDb, 'ISNULL(FLAG_MessageOnShiftChange, 0) AS FLAG_MessageOnShiftChange', 'Staff', 'AccountNo = '' & RosterRecord(2) & ''', 1) _
--// And Format$(N2S(RosterRecord(6)), 'yyyy/mm/dd') < Format$(DateAdd('d', s_MobileFutureLimit, Now), 'yyyy/mm/dd') _
--// And Format$(N2S(RosterRecord(6)), 'yyyy/mm/dd') >= Format$(Now, 'yyyy/mm/dd') _
--// Then
--// 'Anwaar 26Nov2020As
--// '290            If MsgBox('Alert Staff', vbYesNo + vbQuestion) = vbYes Then
--// rFormOpen.Arguments(0) = 'ALERTSTAFF'As rFormOpen.Arguments(1) = 'SHIFT CANCELLED'
--// rFormOpen.Arguments(2) = 'Shift CANCELLED As' & vbCrLf & Format$(N2S(RosterRecord(6)), 'dd/mm/yyyy') & 'As' & N2S(RosterRecord(7)) & 'As' & vbCrLf & N2S(RosterRecord(1))
--// frmScript.Show 1
--// If rFormClose.Status = 'OK' Then
--// '-----
--// 292                 TRACCSMessage 'STAFF', N2S(RosterRecord(2)), rFormClose.Arguments(0), rFormClose.Arguments(1)
--// End If
--// End If
--// '---------------------

End

Go







Create or ALTER   Procedure [dbo].[spUnallocateStaff](@RecordNo varchar(MAX), @carerCode varchar(100), @ServiceType varchar(500), @ServiceDescription varchar(500),@program varchar(500), @Branch varchar(100), @user varchar(50), @reallocate bit=0)
as
begin

declare @DMTransID int;


update SysTable set DMTransID=isnull(DMTransID,0)+1 
select @DMTransID=DMTransID from SysTable;

INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate]
		   )
SELECT '!INTERNAL'                 AS [Client Code],
       [ro].[carer code],
       @ServiceType         AS [Service Type],
       @ServiceDescription     AS [Service  Description],
       @program AS [Program],
       [ro].[date],
       [ro].[start time],
       [ro].[duration],
       '0'                         AS [Unit Pay Rate],
       0                           AS [Unit Bill Rate],
       [ro].[yearno],
       [ro].[monthno],
       [ro].[dayno],
       [ro].[blockno],
       [ro].[notes],
       [ro].[carerphone],
       [ro].[ubdref],
       6                           AS [Type],
       '1'                         AS [Status],
       [ro].[anal],
       Getdate()                   AS [Date Entered],
       Getdate()                   AS [Date Last Mod],
       NULL                        AS [Date Timesheet],
       NULL                        AS [Date Invoice],
       NULL                        AS [Date Payroll],
       NULL                        AS [InvoiceNumber],
       NULL                        AS [TimesheetNumber],
       NULL                        AS [Batch#],
       0                           AS [Transferred],
       0                           AS [GroupActivity],
       [ro].[billtype],
       [ro].[billto],
       'HOUR'                      AS [Unit Pay Rate],
       [ro].[costqty],
       [ro].[hacctype],
       [ro].[haccid],
       [ro].[billunit],
       [ro].[billqty],
       [ro].[taxpercent],
       [ro].[taxamount],
       NULL                        AS [Link],
       [ro].[tagged],
       [ro].[uniqueid],
       0                           AS [ROS_PANZTEL_UPDATED],
       NULL                        AS [ROS_COPY_BATCH#],
       NULL                        AS [ROS_PAY_BATCH],
       [ro].[nrcp_carer_code],
       [ro].[nrcp_contact_urgency],
       [ro].[nrcp_contact_time],
       [ro].[nrcp_referral_service],
       [ro].[servicesetting],
       [ro].[ros_daelibs_updated],
       0                           AS [InUse],
       [ro].[dischargereasontype],
       [ro].[datasetclient],
       [ro].[nrcp_typeofassitance],
       [ro].[nrcp_referraltoservices],
       [ro].[nrcp_volunteerservices],
       [ro].[nrcp_reasonnoassistance],
       [ro].[dmstat],
       @user                   AS [Creator],
       @user                    AS [Editer],
       [ro].[datedeleted],
       [ro].[deletedrecord],
       [ro].[coid],
       [ro].[brid],
       [ro].[dpid],
       [ro].[nrcp_episode],
       [ro].[attendees],
       [ro].[oldstatus],
       [ro].[oldbill],
       [ro].[oldqty],
       [ro].[timelogid],
       [ro].[recurrencestate],
       [ro].[busystatus],
       [ro].[importancelevel],
       [ro].[labelid],
       [ro].[scheduleid],
       [ro].[recurrencepatternid],
       [ro].[isrecurrenceexceptiondeleted],
       [ro].[rexceptionstarttimeorig],
       [ro].[rexceptionendtimeorig],
       [ro].[isalldayevent],
       [ro].[ismeeting],
       [ro].[isprivate],
       [ro].[isreminder],
       [ro].[reminderminutesbeforestart],
       [ro].[branch],
       [ro].[shiftname],
       1                           AS [HasServiceNotes],
       [ro].[time2],
       [ro].[ta_earlystart],
       [ro].[ta_latestart],
       [ro].[ta_earlygo],
       [ro].[ta_latego],
       [ro].[ta_tooshort],
       [ro].[ta_toolong],
       [ro].[billdesc],
       [ro].[variationreason],
       NULL                        AS [ClaimedStart],
       NULL                        AS [ClaimedEnd],
       NULL                        AS [ClaimedDate],
       NULL                        AS [ClaimedBy],
       0                           AS [KM],
       [ro].[staffposition],
       0                           AS [StartKM],
       0                           AS [EndKM],
       NULL                        AS [APInvoiceDate],
       NULL                        AS [APInvoiceNumber],
       [ro].[ta_excludegeolocation],
       [ro].[ta_excludefromappalerts],
       [ro].[interpreterpresent],
       [ro].[extraitems],
       NULL                        AS [CloseDate],
       [ro].[charge],
       [ro].[tamode],
       [ro].[ta_multishift],
       NULL                        AS [TravelBatch],
       NULL                        AS [NDIABatch],
       [ro].[ownvehicle],
       [ro].[clientapproved],
       [ro].[disable_shift_start_alarm],
       [ro].[disable_shift_end_alarm],
       @DMTransID AS DMTransID,
       [ro].[datasetqty],
       [ro].[recipient_signature],
       [ro].[shiftlocked],
       [ro].[servicetypeportal],
       [ro].[remindersent],
       [ro].[xenddate]
    
FROM   roster ro
       LEFT JOIN itemtypes it
              ON ro.[service type] = it.title
WHERE  RecordNo IN ( SELECT * FROM Split(@RecordNo,',') )
       AND ([carer code] = @carerCode );
	   
INSERT INTO [dbo].[Roster]
           ([Client Code]
           ,[Carer Code]
           ,[Service Type]
           ,[Service Description]
           ,[Program]
           ,[Date]
           ,[Start Time]
           ,[Duration]
           ,[Unit Pay Rate]
           ,[Unit Bill Rate]
           ,[YearNo]
           ,[MonthNo]
           ,[Dayno]
           ,[BlockNo]
           ,[Notes]
           ,[CarerPhone]
           ,[UBDRef]
           ,[Type]
           ,[Status]
           ,[Anal]
           ,[Date Entered]
           ,[Date Last Mod]
           ,[Date Timesheet]
           ,[Date Invoice]
           ,[Date Payroll]
           ,[InvoiceNumber]
           ,[TimesheetNumber]
           ,[Batch#]
           ,[Transferred]
           ,[GroupActivity]
           ,[BillType]
           ,[BillTo]
           ,[CostUnit]
           ,[CostQty]
           ,[HACCType]
           ,[HACCID]
           ,[BillUnit]
           ,[BillQty]
           ,[TaxPercent]
           ,[TaxAmount]
           ,[Link]
           ,[Tagged]
           ,[UniqueID]
           ,[ROS_PANZTEL_UPDATED]
           ,[ROS_COPY_BATCH#]
           ,[ROS_PAY_BATCH]
           ,[NRCP_CARER_CODE]
           ,[NRCP_CONTACT_URGENCY]
           ,[NRCP_CONTACT_TIME]
           ,[NRCP_REFERRAL_SERVICE]
           ,[ServiceSetting]
           ,[ROS_DAELIBS_UPDATED]
           ,[InUse]
           ,[DischargeReasonType]
           ,[DATASETClient]
           ,[NRCP_TypeOfAssitance]
           ,[NRCP_ReferralToServices]
           ,[NRCP_VolunteerServices]
           ,[NRCP_ReasonNoAssistance]
           ,[DMStat]
           ,[Creator]
           ,[Editer]
           ,[DateDeleted]
           ,[DeletedRecord]
           ,[COID]
           ,[BRID]
           ,[DPID]
           ,[NRCP_Episode]
           ,[Attendees]
           ,[OldStatus]
           ,[OldBill]
           ,[OldQty]
           ,[TimeLogID]
           ,[RecurrenceState]
           ,[BusyStatus]
           ,[ImportanceLevel]
           ,[LabelID]
           ,[ScheduleID]
           ,[RecurrencePatternID]
           ,[IsRecurrenceExceptionDeleted]
           ,[RExceptionStartTimeOrig]
           ,[RExceptionEndTimeOrig]
           ,[IsAllDayEvent]
           ,[IsMeeting]
           ,[IsPrivate]
           ,[IsReminder]
           ,[ReminderMinutesBeforeStart]
           ,[Branch]
           ,[ShiftName]
           ,[HasServiceNotes]
           ,[Time2]
           ,[TA_EarlyStart]
           ,[TA_LateStart]
           ,[TA_EarlyGo]
           ,[TA_LateGo]
           ,[TA_TooShort]
           ,[TA_TooLong]
           ,[BillDesc]
           ,[VariationReason]
           ,[ClaimedStart]
           ,[ClaimedEnd]
           ,[ClaimedDate]
           ,[ClaimedBy]
           ,[KM]
           ,[StaffPosition]
           ,[StartKM]
           ,[EndKM]
           ,[APInvoiceDate]
           ,[APInvoiceNumber]
           ,[TA_EXCLUDEGEOLOCATION]
           ,[TA_EXCLUDEFROMAPPALERTS]
           ,[InterpreterPresent]
           ,[ExtraItems]
           ,[CloseDate]
           ,[charge]
           ,[TAMode]
           ,[TA_Multishift]
           ,[TravelBatch]
           ,[NDIABatch]
           ,[OwnVehicle]
           ,[ClientApproved]
           ,[disable_shift_start_alarm]
           ,[disable_shift_end_alarm]
           ,[DMTransID]
           ,[DatasetQty]
           ,[Recipient_Signature]
           ,[ShiftLocked]
           ,[ServiceTypePortal]
           ,[ReminderSent]
           ,[xEndDate]
		   )
SELECT '!INTERNAL'                 AS [Client Code],
       case when @reallocate=1 then @carerCode else [ro].[carer code] end as carerCode,
       'UNAVAILABLE'         AS [Service Type],
       'UNAVAILABLE'    AS [Service  Description],
       '!INTERNAL' AS [Program],
       [ro].[date],
      '00:00' as [start time],
       288 as [duration],
       '0'                         AS [Unit Pay Rate],
       0                           AS [Unit Bill Rate],
       [ro].[yearno],
       [ro].[monthno],
       [ro].[dayno],
       0 as [blockno],
       'ON LEAVE' as [notes],
       [ro].[carerphone],
       [ro].[ubdref],
       13                          AS [Type],
       '1'                         AS [Status],
       [ro].[anal],
       Getdate()                   AS [Date Entered],
       Getdate()                   AS [Date Last Mod],
       NULL                        AS [Date Timesheet],
       NULL                        AS [Date Invoice],
       NULL                        AS [Date Payroll],
       NULL                        AS [InvoiceNumber],
       NULL                        AS [TimesheetNumber],
       NULL                        AS [Batch#],
       0                           AS [Transferred],
       0                           AS [GroupActivity],
       [ro].[billtype],
       '!INTERNAL' as [billto],
       'HOUR'                      AS [Unit Pay Rate],
       [ro].[costqty],
       [ro].[hacctype],
       [ro].[haccid],
       [ro].[billunit],
       [ro].[billqty],
       [ro].[taxpercent],
       [ro].[taxamount],
       NULL                        AS [Link],
       [ro].[tagged],
       [ro].[uniqueid],
       0                           AS [ROS_PANZTEL_UPDATED],
       NULL                        AS [ROS_COPY_BATCH#],
       NULL                        AS [ROS_PAY_BATCH],
       [ro].[nrcp_carer_code],
       [ro].[nrcp_contact_urgency],
       [ro].[nrcp_contact_time],
       [ro].[nrcp_referral_service],
       [ro].[servicesetting],
       [ro].[ros_daelibs_updated],
       0                           AS [InUse],
       [ro].[dischargereasontype],
       [ro].[datasetclient],
       [ro].[nrcp_typeofassitance],
       [ro].[nrcp_referraltoservices],
       [ro].[nrcp_volunteerservices],
       [ro].[nrcp_reasonnoassistance],
       [ro].[dmstat],
       @user                   AS [Creator],
       @user                    AS [Editer],
       [ro].[datedeleted],
       [ro].[deletedrecord],
       [ro].[coid],
       [ro].[brid],
       [ro].[dpid],
       [ro].[nrcp_episode],
       [ro].[attendees],
       [ro].[oldstatus],
       [ro].[oldbill],
       [ro].[oldqty],
       [ro].[timelogid],
       [ro].[recurrencestate],
       [ro].[busystatus],
       [ro].[importancelevel],
       [ro].[labelid],
       [ro].[scheduleid],
       [ro].[recurrencepatternid],
       [ro].[isrecurrenceexceptiondeleted],
       [ro].[rexceptionstarttimeorig],
       [ro].[rexceptionendtimeorig],
       [ro].[isalldayevent],
       [ro].[ismeeting],
       [ro].[isprivate],
       [ro].[isreminder],
       [ro].[reminderminutesbeforestart],
       [ro].[branch],
       [ro].[shiftname],
       1                           AS [HasServiceNotes],
       [ro].[time2],
       [ro].[ta_earlystart],
       [ro].[ta_latestart],
       [ro].[ta_earlygo],
       [ro].[ta_latego],
       [ro].[ta_tooshort],
       [ro].[ta_toolong],
       [ro].[billdesc],
       [ro].[variationreason],
       NULL                        AS [ClaimedStart],
       NULL                        AS [ClaimedEnd],
       NULL                        AS [ClaimedDate],
       NULL                        AS [ClaimedBy],
       0                           AS [KM],
       [ro].[staffposition],
       0                           AS [StartKM],
       0                           AS [EndKM],
       NULL                        AS [APInvoiceDate],
       NULL                        AS [APInvoiceNumber],
       [ro].[ta_excludegeolocation],
       [ro].[ta_excludefromappalerts],
       [ro].[interpreterpresent],
       [ro].[extraitems],
       NULL                        AS [CloseDate],
       [ro].[charge],
       [ro].[tamode],
       [ro].[ta_multishift],
       NULL                        AS [TravelBatch],
       NULL                        AS [NDIABatch],
       [ro].[ownvehicle],
       [ro].[clientapproved],
       [ro].[disable_shift_start_alarm],
       [ro].[disable_shift_end_alarm],
       null AS DMTransID,
       [ro].[datasetqty],
       [ro].[recipient_signature],
       [ro].[shiftlocked],
       [ro].[servicetypeportal],
       [ro].[remindersent],
       [ro].[xenddate]
    
FROM   roster ro
       LEFT JOIN itemtypes it
              ON ro.[service type] = it.title
WHERE  RecordNo IN ( SELECT * FROM Split(@RecordNo,',') )
       AND ([carer code] = @carerCode or @reallocate=1 );

if (@reallocate=0) begin
	UPDATE Roster SET [ShiftName] = [Carer Code], [Carer Code] = 'BOOKED', [Branch] = @Branch, [TYPE] = 1, [SERVICE DESCRIPTION] = 'NOPAY', [Unit Pay Rate] = 0  WHERE RecordNo IN (SELECT * FROM Split(@RecordNo,','));
End

	INSERT INTO AUDIT (Operator, ActionDate, AuditDescription, ActionOn, WhoWhatCode, TraccsUser)  
	SELECT @user AS Operator,getDate() AS ActionDate, 'UNALLOCATE STAFF: ' + [Carer Code] + '-' + [Date] + '-' + [Start Time] AS AuditDescription, 'Roster' AS ActionOn, [RecordNo] AS WhoWhatCode, @user AS TraccsUser 
	FROM Roster WHERE RecordNo IN (SELECT * FROM Split(@RecordNo,','))


	INSERT INTO History (PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator) 
                      SELECT RecordNo, GetDate(), 
                      'UNALLOCATED-ORIGINALLY ALLOCATED TO ' + [ShiftName] + ' BUT WORKER UNABLE TO DO SHIFT DUE TO ' + @ServiceDescription AS Detail, 'SVCNOTE', '', [Client Code], @user
					  FROM Roster WHERE RecordNo IN (SELECT * FROM Split(@RecordNo,',')) AND Date > '2000/01/01';

     INSERT INTO History (PersonID, DetailDate, Detail, ExtraDetail1, ExtraDetail2, WhoCode, Creator) 
                           SELECT RecordNo, GetDate(), 
                           [Carer Code] + ' UNAVAILABLE FOR WORK DUE TO ' + [Service Type] AS Detail, 'SVCNOTE', '', [Client Code],  @user
                            FROM Roster WHERE  DMTransID = @DMTransID AND Date > '2000/01/01';       

			DELETE FROM Roster WHERE Recordno IN (SELECT * FROM Split(@RecordNo,',')) AND Type in (6,13);
End

--============================================

CREATE or Alter PROCEDURE [dbo].[getDayManager_Del](@sDate varchar(10),@eDate varchar(10), @dmType varchar(10))
As
begin
 if (@dmType='1')
	 SELECT DISTINCT
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 1 OR DMStat = 9 )) t  
	ORDER BY [Date], ShiftName, [Start Time], Recipient

 else if (@dmType='2' or @dmType='0' or isnull(@dmType,'')='' )
 --Staff Management
	 SELECT DISTINCT
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo 
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
	FROM Roster_Del ro LEFT JOIN Recipients ON 
	[ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 3 OR [ro].[Type] = 5 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 OR [ro].[Type] = 1 OR [ro].[Type] = 13 OR ([ro].[Type] = 6 AND [ItemTypes].[MinorGroup] <> 'LEAVE') OR ([ItemTypes].[MinorGroup] = 'LEAVE') )) t  
	ORDER BY Staff, Date, [Start Time], Recipient

 else if (@dmType='3')
 --Transport Recipients
	 SELECT DISTINCT 
	ShiftName as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
	
	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') 
	AND ([ItemTypes].[RosterGroup] = 'TRANSPORT' )
	
	) t 
	ORDER BY ShiftName, Date, [Start Time], Recipient
else if(@dmType='4')
--Transport Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
	
	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') 
	AND ([ItemTypes].[RosterGroup] = 'TRANSPORT' )
	) t 
	ORDER BY [Setting/Location], Date, [Start Time], Staff

--else if(@dmType='5')
	--Transport Daily Planner

else if(@dmType='6')
--Facilities Recipients
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
		[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
 
	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
	
	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'CENTREBASED' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Recipient
	
else if(@dmType='7')
--Facilities Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
	
	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'CENTREBASED' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Staff
else if(@dmType='8')
--Group Recipients
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

    FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode

	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'GROUPACTIVITY' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Recipient, Activity

else if(@dmType='9')
--Group Staff
	SELECT DISTINCT 
	[Setting/Location] as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo
	
	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	 ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode

	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ItemTypes].[RosterGroup] = 'GROUPACTIVITY' )) t  
	ORDER BY [Setting/Location], Date, [Start Time], Staff
else if(@dmType='10')
	--Grp/Trns/Facility- Recipients
	SELECT DISTINCT 
	Recipient as [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
	,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode
		
	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 )) t  
	ORDER BY Recipient, Date, [Start Time], Staff
else if(@dmType='11')
	--Grp/Trns/Facility-Staff
	SELECT DISTINCT 
	[Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
   ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode

	FROM Roster_Del ro LEFT JOIN Recipients ON 
	[ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID 
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Carer Code] > '!z') AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 3 OR [ro].[Type] = 5 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 OR [ro].[Type] = 1 OR [ro].[Type] = 13 OR ([ro].[Type] = 6 AND [ItemTypes].[MinorGroup] <> 'LEAVE') OR ([ItemTypes].[MinorGroup] = 'LEAVE') )) t  
	ORDER BY Staff, Date, [Start Time], Recipient

Else if(@dmType='12')
	--Recipient Management
	SELECT DISTINCT 
	recipient [Staff],[uniqueid], [CoOrdinator], [Service Order/Grid No],RecipientType, [Category], [Gender], [Recipient],[carer code] as carercode,[Activity],BillQty,billunit,BillRate,TaxAmount,TaxPercent,CostUnit,payQty,payRate,[DMColor],[infoonly],[RosterGroup],[jobtype],[minorgroup],[nochangedate],[nochangetime],[timechangelimit],[starttimelimit],[endtimelimit],[daymask],[staff category],[team],[staffbranch],RecipientBranch,[stfgender],[hrs_fnightly_min],[hrs_fnightly_max],[hrs_weekly_min],[hrs_weekly_max],[ShiftType],[servicesetting],
	[rprogram],analysisCode,recipient_Category,[recordno],[date],[yearno],[monthno],[dayno],[start time],[start time] as startTime,[duration],[wkdhrs],[type],[status],[notes],[hasservicenotes],[dmstat],[shiftname],[snotes],[address],[suburb],billto, [contact]  as staffPhone,[Setting/Location] , [Service Order/Grid No] as ServiceOrderGridNo

	 FROM (SELECT DISTINCT CASE WHEN [ro].[Carer Code] = 'BOOKED' THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'REFERRAL') 
	THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'RECIPIENT') THEN ' 
	BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'CARER/RECIPIENT') THEN ' BOOKED' WHEN ([ro].[Type] = 1 AND [Recipients].[Type] = 'WAITING LIST') THEN ' 
	WAITING LIST' WHEN ([ro].[Type] = 1) THEN ' BOOKED' ELSE [Carer Code] END as Staff, [Recipients].[UniqueID], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE 
	N2.Address END  AS Address, CASE WHEN N1.Suburb <> '' THEN  N1.Suburb ELSE N2.Suburb END  AS Suburb, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact 
	END AS Contact, [Recipients].[RECIPIENT_CoOrdinator] As CoOrdinator, [Recipients].[UBDMap] As [Service Order/Grid No], [Recipients].[Type] As [RecipientType], 
	[Recipients].[AgencyDefinedGroup] As [Category], [Recipients].[Gender], [Recipients].[Branch] AS RecipientBranch, [ro].[Client Code] As Recipient, [ro].[Carer Code], [ro].[Service Type] As Activity, [ro].[BillQty] As BillQty, [ro].[Branch] AS RosterBranch, ISNULL([ItemTypes].[DMColor],'') AS DMColor, 
	[ItemTypes].[InfoOnly], [ItemTypes].[RosterGroup], [ItemTypes].[JobType], [ItemTypes].[MinorGroup], [ItemTypes].[NoChangeDate], [ItemTypes].[NoChangeTime], 
	[ItemTypes].[TimeChangeLimit], [ItemTypes].[StartTimeLimit], [ItemTypes].[EndTimeLimit], ISNULL(CONVERT(VARCHAR, NoMonday), '0') + ISNULL(CONVERT(VARCHAR, 
	NoTuesday), '0') + ISNULL(CONVERT(VARCHAR, NoWednesday), '0') + ISNULL(CONVERT(VARCHAR, NoThursday), '0') + ISNULL(CONVERT(VARCHAR, NoFriday), '0') + 
	ISNULL(CONVERT(VARCHAR, NoSaturday), '0') + ISNULL(CONVERT(VARCHAR, NoSunday), '0') + ISNULL(CONVERT(VARCHAR, NoPubHol), '0') AS DayMask, [Staff].[StaffGroup] 
	As [Staff Category], [Staff].[StaffTeam] As Team, [Staff].[STF_DEPARTMENT] AS StaffBranch, [Staff].[Gender] As StfGender, [Staff].[HRS_FNIGHTLY_MIN], 
	[Staff].[HRS_FNIGHTLY_MAX], [Staff].[HRS_WEEKLY_MIN], [Staff].[HRS_WEEKLY_MAX], [Staff].[CH_2_1], [Staff].[CH_2_2], [Staff].[CH_2_3], [Staff].[CH_2_4], 
	[Staff].[CH_2_5], [Staff].[CH_2_6], [Staff].[CH_2_7], [BR].BranchBranch, [ro].[Service Description] As ShiftType, CASE WHEN ISNULL([ro].[ServiceSetting], '*NO 
	GROUP ASSIGNED') > '!' THEN [ServiceSetting] ELSE '*NO GROUP ASSIGNED' END AS [Setting/Location], [ro].[Program] As rProgram, [ro].[Anal] As [Recipient 
	Category/Region], [ro].[RecordNo], [ro].[Date], [ro].[YearNo], [ro].[MonthNo], [ro].[Dayno], CASE WHEN [ro].[Type] = 14 THEN '00:00' ELSE [ro].[Start Time] END 
	AS [Start Time], CASE WHEN [ro].[Type] = 14 THEN 3 ELSE [ro].[Duration] END AS [Duration], CASE WHEN (ISNULL(InfoOnly, 0) = 1 OR ISNULL(NoOvertimeAccumulation, 
	0) = 1 OR [ro].[Type] = 13) THEN 0 ELSE [ro].[Duration] END AS WkdHrs, [ro].[Type], [ro].[Status], CASE WHEN [ro].Notes Not Like '' THEN 'YES' ELSE '' END AS 
	Notes, [ro].HasServiceNotes, [ro].[DMStat], [ro].[ShiftName] , CAST([ro].[Notes] as NVarchar(4000)) as sNotes 
   ,[ro].BillUnit AS billunit,[ro].[unit Bill rate] AS BillRate,	[ro].TaxAmount AS TaxAmount,[ro].TaxPercent	 AS TaxPercent,	[ro].CostUnit AS CostUnit,[ro].CostQty As payQty,[ro].[unit Pay rate]  As payRate, ro.billto,[servicesetting], anal as recipient_Category, anal as analysisCode

	FROM Roster_Del ro LEFT JOIN Recipients ON [ro].[Client Code] = [Recipients].[AccountNo]  LEFT JOIN ItemTypes ON [ro].[Service Type] = ItemTypes.Title  LEFT JOIN Staff ON [ro].[Carer Code] = [Staff].[AccountNo]  LEFT JOIN (SELECT PersonID, HumanResources.[Name] As [BranchBranch]  FROM HumanResources  WHERE HumanResources.[Group] = 'RECIPBRANCHES') AS BR ON [Recipients].UniqueID = BR.PersonID LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PERSONID, SUBURB,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE [Description] = '<USUAL>')  AS N2 ON N2.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Recipients.UniqueID  LEFT JOIN (SELECT TOP 1 PersonID,  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE [Type] = '<HOME>')  AS P2 ON P2.PersonID = Recipients.UniqueID  
	WHERE (Date >= @sDate AND Date <= @eDate) AND ([ro].Type <> 9) AND ([Client Code] > '!z') AND ([ro].[Type] = 7 OR [ro].[Type] = 2 OR [ro].[Type] = 8 OR [ro].[Type] = 10 OR [ro].[Type] = 11 OR [ro].[Type] = 12 )) t  
	ORDER BY Recipient, Date, [Start Time], Staff

END


IF NOT EXISTS(select 1 from sys.columns  where Name = N'MMPublishPrintRosters ' and Object_ID = Object_ID(N'UserInfo'))
BEGIN
	ALTER TABLE UserInfo ADD MMPublishPrintRosters int not null
END
GO


Create or ALTER        PROCEDURE [dbo].[CreatePoEntry]
	@ClientCode NVARCHAR(max),			
	@CarerCode NVARCHAR(max),			
	@ServiceType  NVARCHAR(max),
	@ServiceDesc  NVARCHAR(max),
	@Program NVARCHAR(MAX),				
	@Date DATETIME,		
	@Time NVARCHAR(max),	
	@Creator NVARCHAR(max),				
	@Editer NVARCHAR(max),				
	@BillUnit NVARCHAR(max),	
	@BillQty int,
	@AgencyDefinedGroup NVARCHAR(max),  
	@ReferralCode NVARCHAR(max),		
	@Type SMALLINT,						
	@Duration SMALLINT,                 
	@TimePercent NVARCHAR(max),         
	@Notes NVARCHAR(max),               
	@BlockNo REAL,                      
	@ReasonType INT,					
	@TabType NVARCHAR(max),
	@HaccType NVARCHAR(MAX),			
	@BillTo NVARCHAR(MAX),				
	@CostUnit NVARCHAR(MAX)	= 'HOUR',	
	@BillDesc NVARCHAR(MAX) = '',
	@UnitPayRate FLOAT = 0,
	@UnitBillRate FLOAT = 0,
	@TaxPercent FLOAT = 0,
	@APIInvoiceDate NVARCHAR(MAX) = '',
	@APIInvoiceNumber NVARCHAR(MAX) = '',
	@PODetail NVARCHAR(MAX) = ''
		
AS	

begin

 declare @RecordNo int;
 declare @PersonId varchar(50);
 declare @ItemID int;

 select @HACCType=HACCType from ItemTypes where Title = @ServiceType AND ProcessClassification = 'OUTPUT';
 select @AgencyDefinedGroup=AgencyDefinedGroup from Recipients where AccountNo = @ClientCode;
INSERT INTO
  roster (
    [client code],
    [carer code],
    [service type],
    [service description],
    [program],
    [date],
    [hacctype],
    [start time],
    [duration],
    [unit pay rate],
    [unit bill rate],
    [taxpercent],
    [yearno],
    [monthno],
    [dayno],
    [blockno],
    [notes],
    [type],
    [status],
    [anal],
    [date entered],
    [date last mod],
    [groupactivity],
    [billtype],
    [billto],
    [apinvoicedate],
    [apinvoicenumber],
    [costunit],
    [costqty],
    [billunit],
    [billqty],
    [servicesetting],
    [dischargereasontype],
    [datasetclient],
    [nrcp_referral_service],
    [creator],
    [editer],
    [inuse],
    [billdesc],
    [transferred],
	[HasServiceNotes],
	PO_State
  )
VALUES
  (
    @ClientCode,
	CASE WHEN  @CarerCode = 'INTERNAL' THEN '!INTERNAL' ELSE @CarerCode END, 
    @ServiceType,
    @serviceDesc,
    @Program,
    FORMAT(@Date, 'yyyy/MM/dd', 'en-US'),
    @HaccType,
    @Time,
    @Duration,
    @UnitPayRate,
    @UnitBillRate,
    @TaxPercent,
    YEAR(@Date),
    MONTH(@Date),
    DAY(@Date),
    @BlockNo,
    @Notes,
    @Type,
    CASE 
		WHEN @Type IN (7) THEN 2
		ELSE
			CASE 
				WHEN @CarerCode IN ('!INTERNAL') THEN 2 
				ELSE 1 
			END
	END,
    @AgencyDefinedGroup,
    GetDate(),
    GetDate(),
    '0',
    '',
    @BillTo,
    @APIInvoiceDate,
    @APIInvoiceNumber,
    @CostUnit,
    @BillQty,
    @BillUnit,
    @BillQty,
    '',
    @ReasonType,
    @ClientCode,
    @ReferralCode,
    @Creator,
    @Editer,
    0,
    @BillDesc,
    0,
	1,
	1
  )
 
  select @RecordNo=@@IDENTITY;
INSERT INTO
  audit (
    operator,
    actiondate,
    auditdescription,
    actionon,
    whowhatcode,
    traccsuser
	
  )
VALUES
  (
    @Creator,
    @Date,
    'PO created ' + @TabType,
    'ROSTER',
    SCOPE_IDENTITY(),
    @Editer
  );

    Insert into History(
				   PersonID, 
                   DetailDate, 
                   Detail, 
                   ExtraDetail1, 
                   ExtraDetail2, 
                   WhoCode, 
                   Creator)
        Values (@RecordNo  , 
                   getdate(),
                   @PODetail, 
                   'SVCNOTE', 
                   '#BROKERAGE REQUEST', 
                   @RecordNo  , 
                   @Creator )

	
	select @PersonId=UniqueID from Recipients where AccountNo=@ClientCode;
	select @ItemID=max(ItemID)+1  from SysTable;
	select Top 1 'PO-'+ @PersonId +'-' + Convert(varchar,@ItemID) as PONo, @RecordNo as RecordNo, @Date as [date], @time as time,@Duration as duration from Registration;

	END

  
Create or ALTER   procedure [dbo].[sp_UpdateVehicle] (@RosterID int,@Priority varchar(50), @PickUpAddress1 varchar(MAX), @DropOffAddress1 varchar(MAX),@PUGridRef varchar(10),@DOGridRef varchar(10), 
@PickUpAddress3 varchar(MAx), @DropOffAddress3 varchar(MAX), @StartOdo varchar(10), @EndOdo varchar(10), @TripKm varchar(10),@DeadKm varchar(10),@apptime varchar(5), @Notes varchar(MAX))
AS
begin

UPDATE TransportDetail SET PickUpAddress1 = PickUpAddress1, PUGridRef = @PUGridRef, DropOffAddress1 = @DropOffAddress1, DOGridRef = @DOGridRef, PickUpAddress3 = @PickUpAddress3, DropOffAddress3 = @DropOffAddress3  WHERE RosterID = @RosterID
UPDATE Roster SET [Notes] = @Notes, Time2 = @apptime WHERE RecordNo = @RosterID
UPDATE R SET ExtraItems = '0000000000' FROM ROSTER R INNER JOIN ITEMTYPES I ON R.[SERVICE TYPE] = I.TITLE WHERE I.IT_DATASET = 'DEX' AND I.DEXID = @PUGridRef AND R.RECORDNO = @RosterID
  
end