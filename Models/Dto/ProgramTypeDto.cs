﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class ProgramTypeDto
    {
        public string PersonId { get; set; }
        public string Type { get; set; }

        public string Name { get; set; }

         public string otherInfo { get; set; }
    }
}
