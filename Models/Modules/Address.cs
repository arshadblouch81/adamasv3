namespace Adamas.Models.Modules{
    public class Address {
        public int? RecordNumber { get; set; }
        public string PrimaryAddress { get; set; }
        public string Type { get; set; }
        public string AddressDetails { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Description { get; set; }
        public bool Primary { get; set; }


        public Address Create()
        {
            return new Address
            {
                RecordNumber = RecordNumber,
                PrimaryAddress = PrimaryAddress,               
                Type = Type,
                AddressDetails = AddressDetails
            };
        }

        internal static Address Create(Address addressDetails)
        {
            return new Address
            {
                RecordNumber = addressDetails.RecordNumber,
                PrimaryAddress = addressDetails.PrimaryAddress,
                Type = addressDetails.Type,
                AddressDetails = addressDetails.AddressDetails,
               
            };
        }
    }
}