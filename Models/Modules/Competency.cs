using System;
using System.Collections.Generic;
namespace Adamas.Models.Modules
{
    public class Competency
    {
      public int? recordNumber { get; set; } = 0;
      public string personID { get; set; }
      public string notes { get; set; }
      public string competencyValue { get; set; }
      public bool? mandatory {get;set;} = false;

      public string? Creator { get; set; }
      public  List<String> selectedcompetency {get;set;}
      
    }
}
