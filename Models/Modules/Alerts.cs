using System;

namespace Adamas.Models.Modules{
    public class Alerts {
        public int? SqlId { get; set; }
        public string IssueType { get; set; }
        public string Notes { get; set; }
        public string UniqueId { get; set; }
        public string ContactIssues { get; set; }
        public string RosterAlerts { get; set; }
        public string RunsheetAlerts { get; set; }
        public string KLMArrangements { get; set; }
    }
}