using System;

namespace Adamas.Models.Modules
{
    public class Balances
    {
        public DateTime PeriodEnd { get; set; }
        public decimal Balance { get; set; }
        public decimal? BankedContingency { get; set; }
    }
}