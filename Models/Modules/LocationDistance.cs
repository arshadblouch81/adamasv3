using System;

namespace Adamas.Models.Modules
{
    public class LocationDistance
    {
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string GoogleCustID { get; set; }
        public string TravelProvider { get; set; }
        public string MapApiKey { get; set; }

    }
}