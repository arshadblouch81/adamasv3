    
    using System;

namespace Adamas.Models.Modules{
    public class ActivityParms {
        
        public string Recipient { get; set; }
        public string Program { get; set; }
        public Boolean ForceAll { get; set; }
        public string MainGroup { get; set; }
        public string SubGroup { get; set; }
        public string ViewType { get; set; }
        public string AllowedDays { get; set; }
        public string Duration { get; set; }
        
        
    }
}
 