using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class LoanItemsDTO
    {
        public int RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string EquipmentCode{ get; set; }
        public string Group { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string SerialNumber{ get; set; }
         public string LockBoxCode{ get; set; }
        public string LockBoxLocation{ get; set; }
        public string PurchaseAmount{ get; set; }
        public DateTime? PurchaseDate{ get; set; } = null;
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public DateTime? Date1 { get; set; } = null;
        public DateTime? Date2 { get; set; } = null;
        public DateTime? DateInstalled{ get; set; } = null;
    }
}
