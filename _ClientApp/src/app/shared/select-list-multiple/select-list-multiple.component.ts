import { Component, OnInit, forwardRef, ElementRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SqlWizardService } from '../../services/sqlwizard.service';

@Component({
  host:{
    '(document:click)': 'onClick($event)'
  },
  selector: 'select-list-multiple',
  templateUrl: './select-list-multiple.component.html',
  styleUrls: ['./select-list-multiple.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectListMultipleComponent),
      multi: true
    }
  ]
})
export class SelectListMultipleComponent implements OnInit, ControlValueAccessor {

  @Input() program;
  lists = []

  show: boolean = false;
  selectedList: Array<any> = [];

  // Function to call when the rating changes.
  onChange = (data: any) => {};

  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {};

  constructor(
    private elem: ElementRef,
    private sqlWizS: SqlWizardService
  ) { }

  onClick(event: Event){
    if(!this.elem.nativeElement.contains(event.target))
        this.show = false;
  }

  ngOnInit() {
    console.log(this.program)
    this.sqlWizS.GetReferralCode({
      program: this.program,
      tab: 10
    }).subscribe(data => {
      this.lists = data.map(x => {
        return {
          status: false,
          name: x['activity']
        }
      })
    })
  }

  remove(index: any){   
    var hehe = this.lists.indexOf(this.selectedList[index])
    this.lists[hehe].status = false
    setTimeout(() => {
      this.selectedList.splice(index, 1)
    }, 100);

  }

  onSelect(state: any, index: any){     
    if(state.target.checked){
      this.selectedList.push(this.lists[index])
    }else{
      var hehe = this.selectedList.indexOf(this.lists[index])
      this.selectedList.splice(hehe, 1)
    }
    this.onChange(this.selectedList)
  }

  writeValue(value: any): void {
  
  }

  // Allows Angular to register a function to call when the model (rating) changes.
  // Save the function as a property to call later here.
  registerOnChange(fn: (rating: number) => void): void {
    this.onChange = fn;
  }

  // Allows Angular to register a function to call when the input has been touched.
  // Save the function as a property to call later here.
  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }



}
