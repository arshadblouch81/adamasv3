
import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { FormControl, FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { StaffService, GlobalService, TimeSheetService, EmailService } from '../../services/index';

import { Subject } from 'rxjs'


import * as moment from 'moment';

const leaveTypes: string[] = ['', 'OTHER LEAVE', 'REC LEAVE', 'SICK LEAVE'];
@Component({
    selector: '',
    templateUrl: './home.html',
    styles: [
        `
        .leaves{
            width: 100%;
            height: 30px;
        }
        textarea{
            width: 100%;
            height: 7rem;
            resize: none;
            padding: 0.7em;
        }

        `
    ]
})

export class HomeProvider implements OnInit, OnDestroy {
    private unsubscribe$ = new Subject()
    private token: any;
    private uniqueId: any;

    leaveShow: boolean = false;
    leaves: Array<string> = leaveTypes;
    leaveForm: FormGroup;

    loading: boolean = false;

    constructor(
        private staffS: StaffService,
        private globalS: GlobalService,
        private timeS: TimeSheetService,
        private emailS: EmailService,
        private formBuilder: FormBuilder
    ) {

        this.leaveForm = new FormGroup({
            StartDate: new FormControl(new Date(), []),
            EndDate: new FormControl(new Date(), []),
            Leave: new FormControl(leaveTypes, []),
            Notes: new FormControl('', [])
        })
    }

    ngOnInit() {
        this.token = this.globalS.decode();

        this.staffS.getprofile(this.token.code).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.uniqueId = data.uniqueID;
        });
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    reset() {
        this.leaveForm.reset();
    }

    emitOutput(data: any) {
        if (data === 'leave') {
            this.leaveShow = !this.leaveShow;
            this.reset();
        }
        if (data === 'document') {

        }
    }

    saveLeave() {
        var form = this.leaveForm.value;
        const timeDuration = moment.duration(moment(form.EndDate).diff(form.StartDate));

        if (timeDuration.days() < 0) {
            this.globalS.eToast('Invalid Date', 'Please the dates')
            return false;
        }

        if (this.globalS.isEmpty(form.Leave) || this.globalS.isEmpty(form.Notes)) {
            this.globalS.eToast('Input Required', 'Leave type and Notes are required');
            return false;
        }

        const durationStr = timeDuration.days() < 1 ? {
            day: '1 day',
            date: `${moment(form.EndDate).format('MMM DD, YYYY')}`
        } : {
                day: `${timeDuration.days() + 1} days `,
                date: `${moment(form.StartDate).format('MMM DD, YYYY')} - ${moment(form.EndDate).format('MMM DD, YYYY')}`
            }



        let leave: Dto.LeaveEntry = {
            StaffCode: this.uniqueId,
            StartDate: moment(form.StartDate).format('YYYY-MM-DD'),
            EndDate: moment(form.EndDate).format('YYYY-MM-DD'),
            Message: {
                Subject: `Leave Application: ${this.token.code}`,
                Content: `${durationStr.day} on dates ${durationStr.date}`,
                LeaveType: `${form.Leave}`,
                Notes: `${form.Notes}`
            },
            CoordinatorEmail: {
                AccountName: this.token.code,
                IsRecipient: false
            }
        }
        this.loading = true;

        this.staffS.postleave(leave).pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.loading = false;
            if (data) {
                this.globalS.sToast('Success', 'Leave filed');
                this.leaveShow = false;
                return false;
            }

            this.leaveForm.reset();
        }, err => {
            this.globalS.eToast('Error', 'Email Server is not setup')
        });




    }
}