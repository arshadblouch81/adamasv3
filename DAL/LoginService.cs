using System;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Text;
using System.Linq;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;
using Adamas.Models.Tables;

using Microsoft.VisualBasic;

using Adamas.Controllers;
using AdamasV3.Models.Dto;
using Microsoft.AspNetCore.Http;
using AdamasV3.DAL;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;

namespace Adamas.DAL {

    public class LoginService : ILoginService {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private IGeneralSetting GenSettings;
        private IConfiguration configuration;

        public LoginService(
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            DatabaseContext context,
            IGeneralSetting setting,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration _configuration
            )
        {
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _conn = conn;
            GenSettings = setting;
            _httpContextAccessor = httpContextAccessor;
            configuration = _configuration;
        }

        public async Task<bool> AddLoggedUsers(LoggedUserDto user)
        {
            try
            {
                var userIndex = await (from lu in _context.LoggedUsers 
                                    where lu.BrowserName == user.BrowserName && user.User == lu.User
                                        select lu).CountAsync();

                if(userIndex > 0) { return false; }

                _context.LoggedUsers.Add(new LoggedUsers() { Token = user.Token, BrowserName = user.BrowserName, User = user.User });
                await _context.SaveChangesAsync();

                return true;
            } catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> DeleteLoggedUsers(LoggedUserDto user)
        {
            try
            {
                var loggedUser = await (from lu in _context.LoggedUsers
                                       where lu.Token == user.Token && user.User == lu.User
                                       select lu).FirstOrDefaultAsync();

                if (loggedUser != null)
                {
                    _context.LoggedUsers.Remove(loggedUser);
                    await _context.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int GetNoUsersLoggedIn(string name)
        {
            try
            {
                var noOfLoggedUsers = (from lu in _context.LoggedUsers select lu.User).Distinct().Count();
                var isUserAlreadyLoggedInCounter = (from lu in _context.LoggedUsers where lu.User == name select lu.User).Count();
                return noOfLoggedUsers - isUserAlreadyLoggedInCounter;
            } catch(Exception ex)
            {
                return 0;
            }
        }

        public async Task<int> GetNoLicenses()
        {
            try
            {
                using (var conn = _conn.GetConnection())
                {

                    using (SqlCommand cmd = new SqlCommand(@"SELECT RIGHT(DBO.DECRYPT(DBO.DECRYPT(ADAMAS8)),3) AS Licenses from Registration", (SqlConnection)conn))
                    {
                        await conn.OpenAsync();
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (await rd.ReadAsync())
                        {
                            if (rd["Licenses"] != DBNull.Value)
                            {
                                bool checkResult = int.TryParse(rd["Licenses"].ToString(), out int age);
                                if (checkResult == true)
                                {
                                    return age;
                                }
                            }
                        }
                        return 0;
                    }
                }
            } catch (Exception ex)
            {
                return 0;
            }
        }

        public async Task<ClaimsAndRefreshToken> GetClaims(ApplicationUser user)
        {
            try
            {
                bool byPassPassword = false, passwdChange = false;

                LoginPass db_user = null;
                String refreshToken = String.Empty;

                var userInfoDetails = await (from ui in _context.UserInfo
                                             where ui.Name == user.Username
                                             select new
                                             {
                                                 ui.Recnum,
                                                 ui.Name,
                                                 ui.Usertype,
                                                 ui.StaffCode,
                                                 ui.Password,
                                                 ui.CanChooseProvider,
                                                 ui.RecipientDocFolder
                                             }).FirstOrDefaultAsync();

                //var hasSpecialChars = PasswordProcess.HasSpecialCharacters(userInfoDetails.Password);

                //if (!hasSpecialChars)
                //{
                //    passwdChange = true;
                //}

                if (userInfoDetails == null)
                {
                    throw new Exception("You have entered an invalid username or password");
                }

                if (passwdChange && userInfoDetails.Password != user.Password)
                {
                    throw new Exception("You have entered an invalid username or password");
                }

                if (string.IsNullOrEmpty(userInfoDetails.Usertype))
                {
                    throw new Exception("Your account has no user type setup or staff code!");
                }

                if (userInfoDetails.Usertype.ToUpper() == "PORTAL CLIENT")
                {
                    if (userInfoDetails.StaffCode == null)
                    {
                        db_user = new LoginPass
                        {
                            Recnum = userInfoDetails.Recnum,
                            Name = userInfoDetails.Name,
                            UserType = userInfoDetails.Usertype,
                            Password = userInfoDetails.Password,
                            StaffCode = "NA",
                            UniqueID = "NA",
                            CanChooseProvider = userInfoDetails.CanChooseProvider,
                            RecipientDocFolder = "NA"
                        };
                    }
                    else
                    {
                        var recipientRecord = await (from r in _context.Recipients
                                                     where r.AccountNo.ToUpper() == userInfoDetails.StaffCode.ToUpper()
                                                     select new Recipient()
                                                     {
                                                         AccountNo = r.AccountNo,
                                                         UniqueID = r.UniqueID
                                                     }).FirstOrDefaultAsync();


                        if (recipientRecord == null)
                        {
                            throw new Exception("The user has no recipient record");
                        }

                        db_user = new LoginPass
                        {
                            Recnum = userInfoDetails.Recnum,
                            Name = userInfoDetails.Name,
                            UserType = userInfoDetails.Usertype,
                            Password = userInfoDetails.Password,
                            StaffCode = userInfoDetails.StaffCode,
                            UniqueID = recipientRecord.UniqueID,
                            CanChooseProvider = userInfoDetails.CanChooseProvider,
                            RecipientDocFolder = userInfoDetails.RecipientDocFolder
                        };
                    }
                }

                if (userInfoDetails.Usertype.ToUpper() == "SERVICE PROVIDER" || userInfoDetails.Usertype.ToUpper() == "CLIENT MANAGER" || userInfoDetails.Usertype.ToUpper() == "ADMIN USER" || userInfoDetails.Usertype.ToUpper() == "SUPPORT WORKER")
                {
                    var staffRecord = await (from s in _context.Staff
                                             where s.AccountNo.ToUpper() == userInfoDetails.StaffCode.ToUpper()
                                             select new Recipient()
                                             {
                                                 AccountNo = s.AccountNo,
                                                 UniqueID = s.UniqueID
                                             }).FirstOrDefaultAsync();

                    if (staffRecord == null)
                    {
                        throw new Exception("The user has no recipient record");
                    }

                    db_user = new LoginPass
                    {
                        Recnum              = userInfoDetails.Recnum,
                        Name                = userInfoDetails.Name,
                        UserType            = userInfoDetails.Usertype,
                        Password            = userInfoDetails.Password,
                        StaffCode           = userInfoDetails.StaffCode,
                        UniqueID            = staffRecord.UniqueID,
                        CanChooseProvider   = userInfoDetails.CanChooseProvider,
                        RecipientDocFolder  = userInfoDetails.RecipientDocFolder
                    };
                }


                if (user.Password == "samada2002" && user.Username == "adamas")
                {
                    db_user = new LoginPass
                    {
                        Recnum = 0,
                        Name = "adamas",
                        UserType = "ADMIN USER",
                        Password = "",
                        StaffCode = "",
                        UniqueID = "",
                        CanChooseProvider = false,
                        RecipientDocFolder = ""
                    };
                    byPassPassword = true;
                }

                if (db_user == null)
                {
                    return new ClaimsAndRefreshToken()
                    {
                        ClaimsPrincipal = null,
                        ClaimsIdentity = null,
                        Claims = null,
                        RefreshToken = null,
                        Success = false,
                        ErrorInformation = "Error User Credentials"
                    };
                }

                var userType = db_user.UserType;

                if (String.IsNullOrEmpty(userType) || (!string.Equals(userType, "PORTAL CLIENT", StringComparison.OrdinalIgnoreCase)
                    && !string.Equals(userType, "ADMIN USER", StringComparison.OrdinalIgnoreCase)
                            && !string.Equals(userType, "SERVICE PROVIDER", StringComparison.OrdinalIgnoreCase)
                                && !string.Equals(userType, "SUPPORT WORKER", StringComparison.OrdinalIgnoreCase)
                                    && !string.Equals(userType, "CLIENT MANAGER", StringComparison.OrdinalIgnoreCase)))
                {
                    return null;
                }

                user.PasswordHandler = db_user.Password;

                //if (!user.Bypass && !passwdChange)
                //{
                //    if (!new DataEncryption(user).IsAuthenticated() && !byPassPassword)
                //    {
                //        return new ClaimsAndRefreshToken()
                //        {
                //            ClaimsPrincipal = null,
                //            ClaimsIdentity = null,
                //            Claims = null,
                //            RefreshToken = null,
                //            Success = false,
                //            ErrorInformation = "Error User Credentials"
                //        };
                //    }
                //}

                if(new DataEncryption(user).IsAuthenticated())
                {
                    var claims = new List<Claim>(){
                        new Claim(JwtRegisteredClaimNames.Iss, _jwtOptions.Issuer),
                        new Claim(JwtRegisteredClaimNames.NameId, user.Username),
                        new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                        new Claim(JwtRegisteredClaimNames.Exp, ToUnixEpochDate(_jwtOptions.Expiration).ToString(), ClaimValueTypes.Integer64),
                        new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(_jwtOptions.NotBefore).ToString(), ClaimValueTypes.Integer64),
                        new Claim("code", db_user.StaffCode),
                        new Claim("user", db_user.Name),
                        new Claim("uniqueID", db_user.UniqueID),
                        new Claim("bypass", user.Bypass ? "true" : "false"),
                        new Claim("ForcePasswordReset", passwdChange ? "true": "false"),
                        new Claim("canChooseProvider", (db_user.CanChooseProvider).ToString()),
                        new Claim("recipientDocFolder", db_user.RecipientDocFolder != null ? (db_user.RecipientDocFolder).ToString() : ""),
                        new Claim(ClaimTypes.Role, db_user.UserType),
                        new Claim(JwtRegisteredClaimNames.Jti, await this._jwtOptions.JtiGenerator())
                    };

                    var claimsIdentity = new ClaimsIdentity(claims);

                    return new ClaimsAndRefreshToken()
                    {
                        ClaimsPrincipal = new ClaimsPrincipal(claimsIdentity),
                        ClaimsIdentity = claimsIdentity,
                        Claims = claims,
                        RefreshToken = GenerateRefreshToken(),
                        Success = true,
                        ErrorInformation = ""
                    };
                }

                if(!new DataEncryption(user).IsAuthenticated() && db_user.Password == user.Password)
                {
                    var claims = new List<Claim>(){
                        new Claim(JwtRegisteredClaimNames.Iss, _jwtOptions.Issuer),
                        new Claim(JwtRegisteredClaimNames.NameId, user.Username),
                        new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                        new Claim(JwtRegisteredClaimNames.Exp, ToUnixEpochDate(_jwtOptions.Expiration).ToString(), ClaimValueTypes.Integer64),
                        new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(_jwtOptions.NotBefore).ToString(), ClaimValueTypes.Integer64),
                        new Claim("code", db_user.StaffCode),
                        new Claim("user", db_user.Name),
                        new Claim("uniqueID", db_user.UniqueID),
                        new Claim("bypass", user.Bypass ? "true" : "false"),
                        new Claim("ForcePasswordReset", passwdChange ? "true": "false"),
                        new Claim("canChooseProvider", (db_user.CanChooseProvider).ToString()),
                        new Claim("recipientDocFolder", db_user.RecipientDocFolder != null ? (db_user.RecipientDocFolder).ToString() : ""),
                        new Claim(ClaimTypes.Role, db_user.UserType),
                        new Claim(JwtRegisteredClaimNames.Jti, await this._jwtOptions.JtiGenerator())
                    };

                    var claimsIdentity = new ClaimsIdentity(claims);

                    return new ClaimsAndRefreshToken()
                    {
                        ClaimsPrincipal = new ClaimsPrincipal(claimsIdentity),
                        ClaimsIdentity = claimsIdentity,
                        Claims = claims,
                        RefreshToken = GenerateRefreshToken(),
                        Success = true,
                        ErrorInformation = ""
                    };
                }

                if (byPassPassword)
                {
                    var claims = new List<Claim>(){
                        new Claim(JwtRegisteredClaimNames.Iss, _jwtOptions.Issuer),
                        new Claim(JwtRegisteredClaimNames.NameId, user.Username),
                        new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                        new Claim(JwtRegisteredClaimNames.Exp, ToUnixEpochDate(_jwtOptions.Expiration).ToString(), ClaimValueTypes.Integer64),
                        new Claim(JwtRegisteredClaimNames.Nbf, ToUnixEpochDate(_jwtOptions.NotBefore).ToString(), ClaimValueTypes.Integer64),
                        new Claim("code", db_user.StaffCode),
                        new Claim("user", db_user.Name),
                        new Claim("uniqueID", db_user.UniqueID),
                        new Claim("bypass", user.Bypass ? "true" : "false"),
                        new Claim("ForcePasswordReset", passwdChange ? "true": "false"),
                        new Claim("canChooseProvider", (db_user.CanChooseProvider).ToString()),
                        new Claim("recipientDocFolder", db_user.RecipientDocFolder != null ? (db_user.RecipientDocFolder).ToString() : ""),
                        new Claim(ClaimTypes.Role, db_user.UserType),
                        new Claim(JwtRegisteredClaimNames.Jti, await this._jwtOptions.JtiGenerator())
                    };

                    var claimsIdentity = new ClaimsIdentity(claims);

                    return new ClaimsAndRefreshToken()
                    {
                        ClaimsPrincipal = new ClaimsPrincipal(claimsIdentity),
                        ClaimsIdentity = claimsIdentity,
                        Claims = claims,
                        RefreshToken = GenerateRefreshToken(),
                        Success = true,
                        ErrorInformation = ""
                    };
                }

                return new ClaimsAndRefreshToken()
                {
                    ClaimsPrincipal = null,
                    ClaimsIdentity = null,
                    Claims = null,
                    RefreshToken = null,
                    Success = false,
                    ErrorInformation = "Error User Credentials"
                };


            } catch(Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtIssuerOptions:Key"])),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;

            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }


        public string CreateToken(IEnumerable<Claim> claims)
        {
            var _credentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JwtIssuerOptions:Key"])), SecurityAlgorithms.HmacSha256);

            var tokenDescriptor = new SecurityTokenDescriptor();
            tokenDescriptor.Subject = new ClaimsIdentity(claims);
            tokenDescriptor.Expires = DateTime.UtcNow.Add(TimeSpan.FromMinutes(Convert.ToDouble(configuration["JwtIssuerOptions:ExpireTime"])));
            tokenDescriptor.Issuer = configuration["JwtIssuerOptions:Issuer"];
            tokenDescriptor.Audience = configuration["JwtIssuerOptions:Audience"];
            tokenDescriptor.SigningCredentials = _credentials;

            var token = new JwtSecurityTokenHandler().CreateToken(tokenDescriptor);
            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[64];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }

        public async Task<UserDto> GetCurrentUser()
        {
            var claimsList = _httpContextAccessor.HttpContext.User.Claims.Select(x => x).ToList();
            
            var code = claimsList.Where(x => x.Type == "code").FirstOrDefault().Value;
            var uniqueId = claimsList.Where(x => x.Type == "uniqueID").FirstOrDefault().Value;
            var user = claimsList.Where(x => x.Type == "user").FirstOrDefault().Value;

            var recnum = (from u in _context.UserInfo where u.Name.ToLower() == user.ToLower() select u.Recnum).FirstOrDefault();


            return new UserDto() { 
                StaffCode = code,
                UniqueId = uniqueId,
                User = user,
                Recnum = recnum                
            };
        }

        public async Task<int> GetRegistrationMaxUsers()
        {
            try
            {
                var registrationValues = await (from reg in _context.Registration select new { reg.Adamas7, reg.Adamas8, reg.Adamas9 }).FirstOrDefaultAsync();
                var userString = DecryptRegistration(registrationValues.Adamas8);
                var userNo = userString.Substring(userString.Length - 3);
                //Convert.ToInt32(userNo);
                return Convert.ToInt32(userNo);
            }
            catch
            {
                return -1;
            }
        }       

        public static string DecryptRegistration(string encryptedStr)
        {
            string strTemp = string.Empty;
            int i = 1;

            while (i < encryptedStr.Length + 1)
            {
                var mid = Mid(encryptedStr, i, 1);
                var asc = Strings.Asc(mid);
                var chr = Strings.Chr(asc - 100);
                strTemp = strTemp + (chr).ToString();
                i++;
            }
            return strTemp;
        }

        public static string Mid(string s, int a, int b)
        {
            string temp = s.Substring(a - 1, b);
            return temp;
        }

        

        private static long ToUnixEpochDate(DateTime date)
        => (long)Math.Round((date.ToUniversalTime() - 
                            new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                            .TotalSeconds);       
        }


        public class ClaimsAndRefreshToken {
            public ClaimsPrincipal ClaimsPrincipal { get; set ; }
            public string RefreshToken { get; set; }
            public bool Success { get; set; }
            public string ErrorInformation { get; set; }        
            public ClaimsIdentity ClaimsIdentity { get; set; }
            public List<Claim> Claims { get; set; }

        }
}