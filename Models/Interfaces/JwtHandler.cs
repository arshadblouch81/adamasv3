using Microsoft.Extensions.Options;

namespace Adamas.Models.Interfaces {
    public class JwtHandler: IJwtHandler {
        private readonly JwtIssuerOptions _options;
        public JwtHandler(IOptions<JwtIssuerOptions> options){
            _options = options.Value;
        }

        public JwtIssuerOptions Create(string username, string password){
            return _options;
        }
    }
}