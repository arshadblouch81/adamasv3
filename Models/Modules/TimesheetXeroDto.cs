﻿using System.Collections.Generic;
using Xero.NetStandard.OAuth2.Model.PayrollAu;

namespace AdamasV3.Models.Modules
{
    public class TimesheetXeroDto
    {
        public List<Timesheet> Timesheets { get; set; }
    }
}
