using System.Collections.Generic;

namespace Adamas.Models.Modules{
    public class UserDefinedGroup {
      public string Group { get; set; }
      public string Notes { get; set; }
      public string PersonID { get; set; }
      public int RecordNumber { get; set; }
      public List<string> GroupList { get; set; }
    }
}