using System;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Text;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Configuration;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;


using Adamas.Controllers;

namespace Adamas.DAL
{

    public class BillingService : IBillingService
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;

        public BillingService(
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            DatabaseContext context,
            IGeneralSetting setting
            )
        {
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _conn = conn;
            GenSettings = setting;
        }

    }
}