using System;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;

using Microsoft.Extensions.Options;

using Adamas.Models;
using Adamas.Models.Modules;

using Syncfusion.DocIO.DLS;
using System.IO;
using Syncfusion.DocIO;
using Microsoft.Office.Interop.Word;
using Adamas.Models.DTO;
using Repositories;
using Adamas.Models.Tables;
using Adamas.Models.DTO;
using System.Collections.Generic;
using AdamasV3.Models.Dto;

namespace Adamas.DAL
{

    public class WordService : IWordService
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private IUploadRepository uploadRepo;

        public WordService(
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            DatabaseContext context,
            IGeneralSetting setting,
            IUploadRepository uploadRepo
            )
        {
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _conn = conn;
            GenSettings = setting;
            this.uploadRepo = uploadRepo;
        }


        public async Task<bool> CreateDocument()
        {
            try
            {
                //Create an instance for word app  
                Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();

                //Set animation status for word application  
                winword.ShowAnimation = false;

                //Set status for word application is to be visible or not.  
                winword.Visible = false;

                //Create a missing variable for missing value  
                object missing = System.Reflection.Missing.Value;

                //Create a new document  
                Microsoft.Office.Interop.Word.Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

                //Add header into the document  
                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {
                    //Get the header range and add the header details.  
                    Microsoft.Office.Interop.Word.Range headerRange = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    headerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlue;
                    headerRange.Font.Size = 10;
                    headerRange.Text = "Header text goes here";

                    Microsoft.Office.Interop.Word.Range headerRange2 = section.Headers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange2.Fields.Add(headerRange, Microsoft.Office.Interop.Word.WdFieldType.wdFieldPage);
                    headerRange2.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    headerRange2.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdBlue;
                    headerRange2.Font.Size = 10;
                    headerRange2.Text = "Header text goes here";
                }

                //Add the footers into the document  
                foreach (Microsoft.Office.Interop.Word.Section wordSection in document.Sections)
                {
                    //Get the footer range and add the footer details.  
                    Microsoft.Office.Interop.Word.Range footerRange = wordSection.Footers[Microsoft.Office.Interop.Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    footerRange.Font.ColorIndex = Microsoft.Office.Interop.Word.WdColorIndex.wdDarkRed;
                    footerRange.Font.Size = 10;
                    footerRange.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
                    footerRange.Text = "Footer text goes here";
                }

                //adding text to document  
                document.Content.SetRange(0, 0);
                document.Content.Text = "This is test document " + Environment.NewLine;

                //Add paragraph with Heading 1 style  
                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading1 = "Heading 1";
                para1.Range.set_Style(ref styleHeading1);
                para1.Range.Text = "Para 1 text";
                para1.Range.InsertParagraphAfter();

                //Add paragraph with Heading 2 style  
                Microsoft.Office.Interop.Word.Paragraph para2 = document.Content.Paragraphs.Add(ref missing);
                object styleHeading2 = "Heading 2";
                para2.Range.set_Style(ref styleHeading2);
                para2.Range.Text = "Para 2 text";
                para2.Range.InsertParagraphAfter();

                //Create a 5X5 table and insert some dummy record  
                Table firstTable = document.Tables.Add(para1.Range, 5, 5, ref missing, ref missing);

                firstTable.Borders.Enable = 1;
                foreach (Row row in firstTable.Rows)
                {
                    foreach (Cell cell in row.Cells)
                    {
                        //Header row  
                        if (cell.RowIndex == 1)
                        {
                            if(cell.ColumnIndex == 1)
                            {
                                cell.Range.Columns.Width = 70.57f;
                            }
                            cell.Range.Text = "Column " + cell.ColumnIndex.ToString();
                            cell.Range.Font.Bold = 1;
                            //other format properties goes here  
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 10;
                            //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                            cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;
                            //Center alignment for the Header cells  
                            cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                        }
                        //Data row  
                        else
                        {
                            cell.Range.Text = (cell.RowIndex - 2 + cell.ColumnIndex).ToString();
                        }
                    }
                }
                //Save the document  
                object filename = @"C:\Users\mark\Desktop\ahak.docx";
                document.SaveAs2(ref filename);
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;
                //MessageBox.Show("Document created successfully !");
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public async Task<(MemoryStream, string, string)?> PrintQuotes(QuoteHeaderDTO quote, string sourcePath, string destinationPath, string filename, QuoteMergeFields person)
        {

            if (!File.Exists(sourcePath))
            {
                return null;
            }


            var title = "Operational Notes For Morganica Abbotts";

            var defaultPath = Path.Combine(destinationPath, $"{filename}.docx");
            File.Copy(Path.GetFullPath(sourcePath), defaultPath);


            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            //Set animation status for word application  
            wordApp.ShowAnimation = false;

            //Set status for word application is to be visible or not.  
            wordApp.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document document = wordApp.Documents.Open(defaultPath, ReadOnly: false, Visible: true);

            try
            {
                document.Activate();

                FindAndReplace(wordApp, "<<AccountCode>>", person.AccountNo);
                FindAndReplace(wordApp, "<<TodaysDate>>", "TodaysDate");
                FindAndReplace(wordApp, "<<PlanStartDate>>", "PlanStartDate");
                FindAndReplace(wordApp, "<<PlanEndDate>>", "PlanEndDate");

                FindAndReplace(wordApp, "<<QUOTENUMBER>>", "QUOTENUMBER");
                FindAndReplace(wordApp, "<<QUOTEBASE>>", "QUOTEBASE");

                FindAndReplace(wordApp, "<<FullNameSurnameLast>>", person.FullNameSurnameLast);
                FindAndReplace(wordApp, "<<UsualStreetAddress>>", person.UsualStreetAddress);
                FindAndReplace(wordApp, "<<UsualSuburb>>", person.UsualSuburb);
                FindAndReplace(wordApp, "<<UsualPostcode>>", person.UsualPostcode);

                FindAndReplace(wordApp, "<<PACKAGELEVEL>>", "GRAFTON NSW 2460");

                FindAndReplace(wordApp, "<<GovtContribution>>", "Govt Contrib");
                FindAndReplace(wordApp, "<<BasicCareFee>>", "BasicCareFee");
                FindAndReplace(wordApp, "<<IncomeTestedFee>>", "IncomeTestedFee");
                FindAndReplace(wordApp, "<<TotalPackageValue>>", "TotalPackageValue");

                FindAndReplace(wordApp, "<<BaseQAmount>>", "BaseQAmount");
                FindAndReplace(wordApp, "<<Admin>>", "Admin");
                FindAndReplace(wordApp, "<<QuoteTotal>>", "QuoteTotal");

                CreateQuoteTable(wordApp, "<<QUOTE>>", quote);

                // Save Document
                //string filename = @"quote_lines1.docx";
                //object path = Path.Combine(Environment.CurrentDirectory, @"document\", filename);
                //document.SaveAs2(ref path);

                // Close Document and Application

                document.Save();
                document.Close(ref missing, ref missing, ref missing);
                document = null;

                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return await uploadRepo.DownloadFileInDocumentDirectory(defaultPath, filename); ;
            }
            catch (Exception ex)
            {
                // Close Document and Application when error occurs
                document.Close(ref missing, ref missing, ref missing);
                document = null;

                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return null;
            }
        }

        public async Task<Result> MergeFieldsDocuments(QuoteMergeFields person, string sourcePath, string destinationPath, string filename)
        {
            //var packageLevel = await (from hr in _context.HumanResourceTypes where hr.Name == quote.Program select hr.User3).FirstOrDefaultAsync();

            if (!Directory.Exists(destinationPath))
            {
                return new Result()
                {
                    Success = false,
                    Error = @"Document Location has not been set. Please see your TRACCS Administrator"
                };
            }

            if (!File.Exists(sourcePath))
            {
                return new Result()
                {
                    Success = false,
                    Error = @"Source directory doesn't exist"
                };
            }


            var defaultPath = Path.Combine(destinationPath, $"{filename}.docx");
            File.Copy(Path.GetFullPath(sourcePath), defaultPath);

            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            //Set animation status for word application  
            wordApp.ShowAnimation = false;

            //Set status for word application is to be visible or not.  
            wordApp.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document aDoc = wordApp.Documents.Open(defaultPath, ReadOnly: false, Visible: true);

            try
            {

                aDoc.Activate();

                //FindAndReplace(wordApp, "<<CustomerId>>", quote.PersonId);
                //FindAndReplace(wordApp, "<<Quote#>>", hdr.RecordNumber);
                //FindAndReplace(wordApp, "<<QuoteBase>>", hdr.QuoteBase);
                //FindAndReplace(wordApp, "<<PackageLevel>>", packageLevel);

                FindAndReplace(wordApp, "<<FullNameSurnameLast>>", person.FullNameSurnameLast);
                FindAndReplace(wordApp, "<<FullNameSurnameFirst>>", person.FullNameSurnameFirst);
                FindAndReplace(wordApp, "<<Gender>>", person.Gender);

                FindAndReplace(wordApp, "<<PrimaryStreetAddress>>", person.PrimaryStreetAddress);
                FindAndReplace(wordApp, "<<PrimarySuburb>>", person.PrimarySuburb);
                FindAndReplace(wordApp, "<<PrimaryState>>", person.PrimaryState);
                FindAndReplace(wordApp, "<<PrimaryPostcode>>", person.PrimaryPostcode);
                FindAndReplace(wordApp, "<<UsualStreetAddress>>", person.UsualStreetAddress);
                FindAndReplace(wordApp, "<<UsualSuburb>>", person.UsualSuburb);
                FindAndReplace(wordApp, "<<UsualState>>", person.UsualState);
                FindAndReplace(wordApp, "<<UsualPostcode>>", person.UsualPostcode);
                FindAndReplace(wordApp, "<<ContactAddress>>", person.ContactAddress);
                FindAndReplace(wordApp, "<<ContactSuburb>>", person.ContactSuburb);
                FindAndReplace(wordApp, "<<ContactState>>", person.ContactState);
                FindAndReplace(wordApp, "<<ContactPostcode>>", person.ContactPostcode);

                FindAndReplace(wordApp, "<<PrimaryPhone>>", person.PrimaryPhone);
                FindAndReplace(wordApp, "<<HomePhone>>", person.HomePhone);
                FindAndReplace(wordApp, "<<WorkPhone>>", person.WorkPhone);
                FindAndReplace(wordApp, "<<MobilePhone>>", person.MobilePhone);
                FindAndReplace(wordApp, "<<Fax>>", person.Fax);
                FindAndReplace(wordApp, "<<Email>>", person.Email);

                FindAndReplace(wordApp, "<<OHSProfile>>", person.OHSProfile);
                FindAndReplace(wordApp, "<<Notes>>", person.Notes);
                FindAndReplace(wordApp, "<<Type>>", person.Type);
                FindAndReplace(wordApp, "<<Email>>", person.Email);
                FindAndReplace(wordApp, "<<Branch>>", person.Branch);

                //CreateGoalsTable(wordApp, "<<GOALS>>", quote);
                //CreateQuoteTable(wordApp, "<<QUOTE>>", quote);

                aDoc.Save();
                aDoc.Close(ref missing, ref missing, ref missing);
                aDoc = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return new Result()
                {
                    Success = true,
                    Error = ""
                };
            }
            catch (Exception ex)
            {
                aDoc.Close(ref missing, ref missing, ref missing);
                aDoc = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return new Result
                {
                    Success = true,
                    Error = ex.ToString()
                };
            }
        }


        public async Task<Result> MergeFieldsDocumentUsingInterop(QuoteMergeFields person, string sourcePath, string destinationPath, string filename, QuoteHeaderDTO quote, Qte_Hdr hdr)
        {
            var packageLevel = await (from hr in _context.HumanResourceTypes where hr.Name == quote.Program select hr.User3).FirstOrDefaultAsync();

            if (!Directory.Exists(destinationPath))
            {
                return new Result()
                {
                    Success = false,
                    Error = @"Document Location has not been set. Please see your TRACCS Administrator"
                };
            }

            if (!File.Exists(sourcePath))
            {
                return new Result()
                {
                    Success = false,
                    Error = @"Source directory doesn't exist"
                };
            }


            var defaultPath = Path.Combine(destinationPath, $"{filename}.docx");
            File.Copy(Path.GetFullPath(sourcePath), defaultPath);

            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            //Set animation status for word application  
            wordApp.ShowAnimation = false;

            //Set status for word application is to be visible or not.  
            wordApp.Visible = false;

            //Create a missing variable for missing value  
            object missing = System.Reflection.Missing.Value;

            Microsoft.Office.Interop.Word.Document aDoc = wordApp.Documents.Open(defaultPath, ReadOnly: false, Visible: true);

            try
            {
                
                aDoc.Activate();

                FindAndReplace(wordApp, "<<CustomerId>>", quote.PersonId);
                FindAndReplace(wordApp, "<<Quote#>>", hdr.RecordNumber);
                FindAndReplace(wordApp, "<<QuoteBase>>", hdr.QuoteBase);
                FindAndReplace(wordApp, "<<PackageLevel>>", packageLevel);

                FindAndReplace(wordApp, "<<FullNameSurnameLast>>", person.FullNameSurnameLast);
                FindAndReplace(wordApp, "<<FullNameSurnameFirst>>", person.FullNameSurnameFirst);
                FindAndReplace(wordApp, "<<Gender>>", person.Gender);

                FindAndReplace(wordApp, "<<PrimaryStreetAddress>>", person.PrimaryStreetAddress);
                FindAndReplace(wordApp, "<<PrimarySuburb>>", person.PrimarySuburb);
                FindAndReplace(wordApp, "<<PrimaryState>>", person.PrimaryState);
                FindAndReplace(wordApp, "<<PrimaryPostcode>>", person.PrimaryPostcode);
                FindAndReplace(wordApp, "<<UsualStreetAddress>>", person.UsualStreetAddress);
                FindAndReplace(wordApp, "<<UsualSuburb>>", person.UsualSuburb);
                FindAndReplace(wordApp, "<<UsualState>>", person.UsualState);
                FindAndReplace(wordApp, "<<UsualPostcode>>", person.UsualPostcode);
                FindAndReplace(wordApp, "<<ContactAddress>>", person.ContactAddress);
                FindAndReplace(wordApp, "<<ContactSuburb>>", person.ContactSuburb);
                FindAndReplace(wordApp, "<<ContactState>>", person.ContactState);
                FindAndReplace(wordApp, "<<ContactPostcode>>", person.ContactPostcode);

                FindAndReplace(wordApp, "<<PrimaryPhone>>", person.PrimaryPhone);
                FindAndReplace(wordApp, "<<HomePhone>>", person.HomePhone);
                FindAndReplace(wordApp, "<<WorkPhone>>", person.WorkPhone);
                FindAndReplace(wordApp, "<<MobilePhone>>", person.MobilePhone);
                FindAndReplace(wordApp, "<<Fax>>", person.Fax);
                FindAndReplace(wordApp, "<<Email>>", person.Email);

                FindAndReplace(wordApp, "<<OHSProfile>>", person.OHSProfile);
                FindAndReplace(wordApp, "<<Notes>>", person.Notes);
                FindAndReplace(wordApp, "<<Type>>", person.Type);
                FindAndReplace(wordApp, "<<Email>>", person.Email);
                FindAndReplace(wordApp, "<<Branch>>", person.Branch);

                CreateGoalsTable(wordApp, "<<GOALS>>", quote);
                CreateQuoteTable(wordApp, "<<QUOTE>>", quote);

                aDoc.Save();
                aDoc.Close(ref missing, ref missing, ref missing);
                aDoc = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return new Result()
                {
                    Success = true,
                    Error = ""
                };
            }
            catch (Exception ex)
            {
                aDoc.Close(ref missing, ref missing, ref missing);
                aDoc = null;
                wordApp.Quit(ref missing, ref missing, ref missing);
                wordApp = null;

                return new Result
                {
                    Success = true,
                    Error = ex.ToString()
                };
            }
        }


        public async Task<Result> MergeFieldsDocument(QuoteMergeFields person, string sourcePath, string destinationPath, string filename)
        {
            if(!Directory.Exists(destinationPath)){
                return new Result(){
                    Success = false,
                    Error = @"Document Location has not been set. Please see your TRACCS Administrator"
                };
            }

            if (!File.Exists(sourcePath))
            {
                return new Result()
                {
                    Success = false,
                    Error = @"Source directory doesn't exist"
                };
            }

            try
            {
                    //Creates new Word document instance for word processing.
                    using (WordDocument document = new WordDocument())
                    {
                        Stream docStream = System.IO.File.OpenRead(Path.GetFullPath(sourcePath));
                        document.Open(docStream, FormatType.Docx);
                        docStream.Dispose();

                        //Finds all occurrences of a misspelled word and replaces with properly spelled word.
                        document.Replace("<<FullNameSurnameLast>>", person.FullNameSurnameLast, true, true);
                        document.Replace("<<FullNameSurnameFirst>>", person.FullNameSurnameFirst, true, true);
                        document.Replace("<<Gender>>", person.Gender, true, true);

                        document.Replace("<<PrimaryStreetAddress>>", person.PrimaryStreetAddress, true, true);
                        document.Replace("<<PrimarySuburb>>", person.PrimarySuburb, true, true);
                        document.Replace("<<PrimaryState>>", person.PrimaryState, true, true);
                        document.Replace("<<PrimaryPostcode>>", person.PrimaryPostcode, true, true);
                        document.Replace("<<UsualStreetAddress>>", person.UsualStreetAddress, true, true);
                        document.Replace("<<UsualSuburb>>", person.UsualSuburb, true, true);
                        document.Replace("<<UsualState>>", person.UsualState, true, true);
                        document.Replace("<<UsualPostcode>>", person.UsualPostcode, true, true);
                        document.Replace("<<ContactAddress>>", person.ContactAddress, true, true);
                        document.Replace("<<ContactSuburb>>", person.ContactSuburb, true, true);
                        document.Replace("<<ContactState>>", person.ContactState, true, true);
                        document.Replace("<<ContactPostcode>>", person.ContactPostcode, true, true);
                        document.Replace("<<PrimaryPhone>>", person.PrimaryPhone, true, true);
                        document.Replace("<<HomePhone>>", person.HomePhone, true, true);
                        document.Replace("<<WorkPhone>>", person.WorkPhone, true, true);
                        document.Replace("<<MobilePhone>>", person.MobilePhone, true, true);
                        document.Replace("<<Fax>>", person.Fax, true, true);
                        document.Replace("<<Email>>", person.Email, true, true);

                        document.Replace("<<OHSProfile>>", person.OHSProfile, true, true);
                        document.Replace("<<Notes>>", person.Notes, true, true);
                        document.Replace("<<Type>>", person.Type, true, true);
                        document.Replace("<<Email>>", person.Email, true, true);
                        document.Replace("<<Branch>>", person.Branch, true, true);


                        IWSection section = document.AddSection();
                        //Adds a new paragraph into Word document and appends text into paragraph
                        IWTextRange textRange = section.AddParagraph().AppendText("Price Details");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        section.AddParagraph();
                        //Adds a new table into Word document
                        IWTable table = section.AddTable();
                        //Specifies the total number of rows & columns
                        table.ResetCells(3, 2);
                        //Accesses the instance of the cell (first row, first cell) and adds the content into cell
                        textRange = table[0, 0].AddParagraph().AppendText("Item");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        //Accesses the instance of the cell (first row, second cell) and adds the content into cell
                        textRange = table[0, 1].AddParagraph().AppendText("Price($)");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 12;
                        textRange.CharacterFormat.Bold = true;
                        //Accesses the instance of the cell (second row, first cell) and adds the content into cell
                        textRange = table[1, 0].AddParagraph().AppendText("Apple");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 10;
                        //Accesses the instance of the cell (second row, second cell) and adds the content into cell
                        textRange = table[1, 1].AddParagraph().AppendText("50");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 10;
                        //Accesses the instance of the cell (third row, first cell) and adds the content into cell
                        textRange = table[2, 0].AddParagraph().AppendText("Orange");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 10;
                        //Accesses the instance of the cell (third row, second cell) and adds the content into cell
                        textRange = table[2, 1].AddParagraph().AppendText("30");
                        textRange.CharacterFormat.FontName = "Arial";
                        textRange.CharacterFormat.FontSize = 10;

                        
                        //Saves the resultant file in the given path.
                        docStream = System.IO.File.Create(Path.GetFullPath(Path.Combine(destinationPath, $"{filename}.docx")));
                        document.Save(docStream, FormatType.Docx);
                        docStream.Dispose();

                        return new Result{
                            Success = true,
                            Error = ""
                        };
                    }
            } catch(Exception ex)
            {
                return new Result{
                    Success = true,
                    Error = ex.ToString()
                };
            }
        }


        private void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        private void CreateGoalsTable(Microsoft.Office.Interop.Word.Application doc, object findText, QuoteHeaderDTO quote)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            object start = 0, end = 0;


            object oMissing = System.Reflection.Missing.Value;
            var range = doc.Selection.Range;

            if (range.Find.Execute(ref findText, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing))
            {
                doc.Selection.Select();
            }
            else
            {
                return;
            }

            //Create a 5X5 table and insert some dummy record  
            Table firstTable = doc.Selection.Tables.Add(range, quote.Goals.Count + 1, 3, ref oMissing, ref oMissing);

            firstTable.Borders.Enable = 1;

            foreach (Row row in firstTable.Rows)
            {
                
                foreach (Cell cell in row.Cells)
                {
                    //Header row  
                    if (cell.RowIndex == 1)
                    {
                        if (cell.ColumnIndex == 1)
                        {
                            cell.Range.Text = "";
                            cell.Range.Columns.Width = 50.57f;
                        }

                        if (cell.ColumnIndex == 2)
                        {
                            cell.Range.Text = "My Goals and Strategies";
                            cell.Range.Columns.Width = 250.57f;
                        }

                        if (cell.ColumnIndex == 3)
                        {
                            cell.Range.Text = "Date By";
                            cell.Range.Columns.Width = 150.57f;
                        }

                        cell.Range.Font.Bold = 1;
                        //other format properties goes here  
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 9;
                        //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                        cell.Shading.BackgroundPatternColor = WdColor.wdColorGray05;
                        //Center alignment for the Header cells  
                        cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalBottom;
                        cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                    }
                    //Data row  
                    else
                    {
                        if (cell.ColumnIndex == 1)
                        {
                            cell.Range.Text = (cell.RowIndex - 1).ToString();
                        }

                        if (cell.ColumnIndex == 2)
                        {
                            var goals = quote.Goals.Count > 0 ? quote.Goals[cell.RowIndex - 2].Goal : "";
                            var tempStr = $"{goals} \r\n";

                            foreach(var strat in quote.Goals[cell.RowIndex - 2].Strategies)
                            {
                                tempStr = tempStr + $"{strat} \r\n";
                            }
                            cell.Range.Text = tempStr;
                        }

                        if (cell.ColumnIndex == 3)
                        {
                            cell.Range.Text = "N/A";
                        }
                    }
                }
            }
        }



        private void CreateQuoteTable(Microsoft.Office.Interop.Word.Application doc, object findText, QuoteHeaderDTO quote)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            object start = 0, end = 0;


            object oMissing = System.Reflection.Missing.Value;
            var range = doc.Selection.Range;

            if (range.Find.Execute(ref findText, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing))
            {
                doc.Selection.Select();
            }
            else
            {
                return;
            }
            
            //Create a 5X5 table and insert some dummy record  
            Table table1 = doc.Selection.Tables.Add(range, quote.QuoteLines.Count + 2, 3, ref oMissing, ref oMissing);

            table1.Borders.Enable = 0;

            decimal sum = 0;

            quote.QuoteLines.ForEach(x =>
            {
                sum = sum + ((Convert.ToDecimal(x.LengthInWeeks)) * Convert.ToDecimal(x.Qty) * Convert.ToDecimal(x.UnitBillRate));
            });


            foreach (Row row in table1.Rows)
            {
                row.Cells.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;

                if (row.IsLast)
                {
                    row.Cells.Shading.BackgroundPatternColor = WdColor.wdColorGray20;


                    row.Cells[1].Range.Text = "TOTAL";
                    

                    row.Cells[1].Merge(row.Cells[2]);
                    row.Cells[1].Range.Font.Bold = 1;
                    row.Cells[1].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                    row.Cells[1].TopPadding = 8f;

                    row.Cells[2].Range.Text = $"${sum.ToString("F")}";
                    row.Cells[2].Range.Font.Bold = 1;
                    row.Cells[2].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                    row.Cells[2].TopPadding = 8f;
                    break;
                }

                foreach (Cell cell in row.Cells)
                {
                    //Header row  
                    if (cell.RowIndex == 1)
                    {
                        if(cell.ColumnIndex == 1)
                        {
                            cell.Range.Text = "";
                            cell.Range.Columns.Width = 50.57f;

                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                        }

                        if (cell.ColumnIndex == 2)
                        {
                            cell.Range.Text = "Agreed Services And Charges";
                            cell.Range.Columns.Width = 350.57f;

                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                        }

                        if (cell.ColumnIndex == 3)
                        {
                            cell.Range.Text = "";
                            cell.Range.Columns.Width = 150.57f;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                        }

                        cell.Range.Font.Bold = 1;

                        //other format properties goes here  
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 9;

                        //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                        cell.Shading.BackgroundPatternColor = WdColor.wdColorGray05;

                        //Center alignment for the Header cells  
                        cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalBottom;
                    }
                    //Data row  
                    else
                    {
                        
                        var quoteData = quote.QuoteLines[cell.RowIndex - 2];           
                    
                        if (cell.ColumnIndex == 1)
                        {
                          
                                cell.Range.Text = (cell.RowIndex - 1).ToString();
                                cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                            
                        }

                        if (cell.ColumnIndex == 2)
                        {
                            
                                cell.Range.Text = $"{quoteData.DisplayText} {quoteData.Qty} {quoteData.Frequency}";
                                cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                            
                        }

                        if (cell.ColumnIndex == 3)
                        {
                            
                                var total = Convert.ToDecimal(quoteData.Qty) * Convert.ToDecimal(quoteData.LengthInWeeks) * Convert.ToDecimal(quoteData.UnitBillRate);
                                cell.Range.Text = $"${total.ToString("F")}";
                                cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
                            
                        }
                    }
                }
            }


        }


        private void PrintQuote(Microsoft.Office.Interop.Word.Application doc, object findText, QuoteHeaderDTO quote)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            object start = 0, end = 0;


            object oMissing = System.Reflection.Missing.Value;
            var range = doc.Selection.Range;

            if (range.Find.Execute(ref findText, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing))
            {
                doc.Selection.Select();
            }
            else
            {
                return;
            }

            //Create a 5X5 table and insert some dummy record  
            Table firstTable = doc.Selection.Tables.Add(range, quote.QuoteLines.Count + 1, 3, ref oMissing, ref oMissing);

            firstTable.Borders.Enable = 1;

            foreach (Row row in firstTable.Rows)
            {
                foreach (Cell cell in row.Cells)
                {
                    //Header row  
                    if (cell.RowIndex == 1)
                    {
                        if (cell.ColumnIndex == 1)
                        {
                            cell.Range.Text = "";
                            cell.Range.Columns.Width = 50.57f;
                        }

                        if (cell.ColumnIndex == 2)
                        {
                            cell.Range.Text = "Agreed Services And Charges";
                            cell.Range.Columns.Width = 350.57f;
                        }

                        if (cell.ColumnIndex == 3)
                        {
                            cell.Range.Text = "";
                            cell.Range.Columns.Width = 150.57f;
                        }

                        cell.Range.Font.Bold = 1;
                        //other format properties goes here  
                        cell.Range.Font.Name = "verdana";
                        cell.Range.Font.Size = 9;
                        //cell.Range.Font.ColorIndex = WdColorIndex.wdGray25;                              
                        cell.Shading.BackgroundPatternColor = WdColor.wdColorGray05;
                        //Center alignment for the Header cells  
                        cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalBottom;
                        cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;

                    }
                    //Data row  
                    else
                    {
                        var quoteData = quote.QuoteLines[cell.RowIndex - 2];
                        if (cell.ColumnIndex == 1)
                        {
                            cell.Range.Text = (cell.RowIndex - 1).ToString();
                        }

                        if (cell.ColumnIndex == 2)
                        {
                            cell.Range.Text = $"{quoteData.DisplayText} {quoteData.Qty} {quoteData.Frequency}";
                        }

                        if (cell.ColumnIndex == 3)
                        {
                            var total = Convert.ToDecimal(quoteData.Qty) * Convert.ToDecimal(quoteData.LengthInWeeks) * Convert.ToDecimal(quoteData.UnitBillRate);
                            cell.Range.Text = $"{ String.Format("{0:.##}", total) }";
                        }
                    }
                }


            }
        }
    }

    public class Result {
        public bool Success { get; set; }
        public string Error { get ; set; }
    }
}