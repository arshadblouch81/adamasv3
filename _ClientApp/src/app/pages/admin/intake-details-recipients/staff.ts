
import {forkJoin,  Subscription ,  Observable } from 'rxjs';
import { Component, OnInit,OnDestroy,ViewChild } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, ListService } from '@services/index'
import { GlobalService } from '@services/global.service'

import { BsModalComponent } from 'ng2-bs3-modal';
import * as moment from 'moment';
@Component({
    selector: '',
    templateUrl:'./staff.html'
})

export class IntakeStaff{
    @ViewChild('addStaffModal', {static:false}) addStaffModal: BsModalComponent;

    user:any
    dbAction: number = 0
    whatstaff: number
    private staff$: Subscription
    loading: boolean

    excludedStaff: Array<any>
    includedStaff: Array<any>

    staffGroup: FormGroup
    staffListArr: Array<string>
    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private listS: ListService,
            private formBuilder: FormBuilder,
            private router: Router,
            private globalS: GlobalService
        ){
            this.staff$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'staff')){
                    this.search();
                }
            });
    }

    ngOnInit(){
        this.resetGroup()
        this.reloadAll()
    }

    ngOnDestroy(){
        this.staff$.unsubscribe();
    }

    resetGroup(){
        this.staffGroup = this.formBuilder.group({
            recordNumber: null,
            personID: '',
            dateCreated: null,
            staffCategory: 0,
            notes: '',
            name: new FormControl('',[Validators.required])
        })
    }

    reloadAll(){
        this.search();
        this.listDropDown()
    }

    listDropDown(){
        this.listS.getintakestaff(this.user.uniqueID)
                    .subscribe(data => this.staffListArr = data)
    }

    search(){
        this.user = this.sharedS.getPicked();
        this.loading = true;
        forkJoin([
            this.timeS.getexcludedstaff(this.user.uniqueID),
            this.timeS.getincludedstaff(this.user.uniqueID)
        ]).subscribe(staff => {
            this.loading = false;
            this.excludedStaff = staff[0];
            this.includedStaff = staff[1];
        })       
    }

    staffProcess(){
        this.staffGroup.controls['personID'].setValue(this.user.uniqueID)
        this.staffGroup.controls['dateCreated'].setValue(moment().format())

        if(this.dbAction == 0){
            this.staffGroup.controls['staffCategory'].setValue(0)
            const staff = this.staffGroup.value
            this.timeS.postintakestaff(staff)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Staff Inserted')
                            }
                        })
        }

        if(this.dbAction == 1){
            this.staffGroup.controls['staffCategory'].setValue(1)
            const staff = this.staffGroup.value
            this.timeS.updateintakestaff(staff)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Staff Inserted')
                            }
                        })
        }
    }

    updatestaff(data: any){
        this.addStaffModal.open();
        this.staffGroup.patchValue({
            recordNumber: data.recordNumber,
            notes: data.notes,
            name: data.name
        })
    }

    deletestaff(data: any){
        this.timeS.deleteintakestaff(data.recordNumber)
                    .subscribe(data =>{
                        if(data){
                            this.reloadAll()
                            this.globalS.sToast('Success','Staff Deleted')
                        }
                    })
    }
}