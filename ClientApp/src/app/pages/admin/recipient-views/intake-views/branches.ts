import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService,PrintService, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd';


@Component({
    selector: '',
    styles: [`
        nz-select{
            width:100%
        }
        nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
        }

        nz-table tr{
          
            font-family: 'Segoe UI';
            font-size: 14px;
            font-weight: 300;
        }
        
    label{
        font-family: 'Segoe UI';
            font-size: 16px;
            font-weight: 500;
            margin-top: 10px;
    }

    .selected{
        background-color: #85B9D5;
        color: white;
    }

    `],
    templateUrl: './branches.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class IntakeBranches implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;
    inputForm: FormGroup;

    tableData: Array<any> = [];
    branches: Array<any> = [];
    activeRowData:any;
    selectedRowIndex:number;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    private default: any = {
        recordNumber: '',
        personID: '',
        branch: null,
        notes: ''
    }
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private printS:PrintService,
        private sanitizer:DomSanitizer,
        private ModalS:NzModalService,
        private cd: ChangeDetectorRef,
        private nzContextMenuService: NzContextMenuService
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'branches')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.search(this.user);
        this.buildForm();
    }   

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            branch: [null, [Validators.required]],
            notes: ''
        });
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(view: number, data: any): void {
      
          data = this.activeRowData;
          this.delete(this.selectedRowIndex);
        

    }
    search(user: any = this.user) {
        this.cd.reattach();
        this.loading = true;
        this.timeS.getbranches(user.id).subscribe(branches => {
            this.loading = false;
            this.tableData = branches;
            this.cd.detectChanges();
        });

        this.listDropDown();
    }

    listDropDown(user: any = this.user) {
        this.branches = [];
        this.listS.getintakebranches(user.id)
            .subscribe(data => this.branches = data)
    }

    save() {
        
        if (!this.globalS.IsFormValid(this.inputForm))
            return; 
        
        this.inputForm.controls['personID'].setValue(this.user.id);

        this.loading = true;
        if (this.addOREdit == 1) {
            this.timeS.postbranches(this.inputForm.value)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Branch Inserted');
                    this.search();
                    this.handleCancel();
                });
        }

        if (this.addOREdit == 2) {
            this.timeS.updatebranches(this.inputForm.value)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Branch Updated');
                    this.search();
                    this.handleCancel();
                });
        }
    }

    handleCancel() {
        this.modalOpen = false;
        this.loading = false;
        this.inputForm.reset(this.default);
    }

    trackByFn(index, item) {
        return item.id;
    }

    selectedItemGroup(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
      }

    showAddModal() {
        this.addOREdit = 1;
        this.listDropDown();
        this.modalOpen = true;
    }

    showEditModal(index: number) {
        this.activeRowData = this.activeRowData;
        this.selectedRowIndex=index;
        this.addOREdit = 2;
        const { branch, recordNumber, notes } = this.activeRowData;
        this.inputForm.patchValue({
            recordNumber,
            branch,
            notes
        });

        this.modalOpen = true;
    }

    delete(index: number) {
        const { recordNumber } = this.activeRowData;
        this.timeS.deletebranches(recordNumber)
                    .subscribe(data => {
                        this.globalS.sToast('Success', 'Branch Deleted');
                        this.search();
                    })
    }

    contextMenu($event: MouseEvent, menu: NzDropdownMenuComponent, data:any): void {
        this.activeRowData = data;
        this.selectedRowIndex = this.tableData.indexOf(data);
        this.nzContextMenuService.create($event, menu);
      }
    menuclick(event:any,i:number){
        if (i==1){
        this.showEditModal(this.selectedRowIndex);

        }else if (i==2){
            this.delete(this.selectedRowIndex);
        }
    }
    
    generatePdf(){
      
        
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Branches",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Branches "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;  
                    this.cd.detectChanges();                     
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
}
