import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit, Injectable, SimpleChanges, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ElementRef, inject, VERSION } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, PrintService } from '@services/index';

@Component({
    selector: 'app-hcp-rates-list',
    templateUrl: './setHcpRatesList.component.html',
    styles: [`
    .orange-text{
      font-size: 14px;
      text-align: left;
      margin-left: 2%;
      color:#f18805;
      margin-top: 10%;
    }
.tab {
    display: block;
    border: 2px solid #85B9D5;
    border-radius: 5px;
    background-color: white;
    height: 24vh;
    padding: 5px;
    margin-top: -28px;
    overflow-y: auto;
    width: auto;
}
.btn {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
    font-weight: bold;
}
.btn1 {
    border: none;
    cursor: pointer;
    outline: none;
    background-color: #85B9D5;
    color: white;
    padding: 5px 7px;
    border-radius: 7px !important;
    text-align: center !important;
    width: 100px !important;
    font-size: 14px;
    font-family: Segoe UI;
}
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    line-height: 24px;
    background: #fff;
    height: 25px;
    border-radius: 15px 15px 0 0;
    margin: 0 0px 0 0;
    text-align: center;
    border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
    background: #85B9D5;
    color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
    min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
    border-bottom: 0px;
}

`]
})

export class SetHcpRatesComponent implements OnInit {

    @Input() open: any;
    @Input() option: any;
    @Input() user: any;

    isVisible: boolean = false;
    modalOpen: boolean = false;
    showHcpRatesSetup: any;
    
    listData: Array<any>;
    loading: boolean = false;
    inputForm: FormGroup;
    check: boolean = false;
    userRole: string = "userrole";
    mainTitle: string = "Add/Change Package Claim Rates";
    modelTitle: string;
    popupTitle: string;
    popupContent: string;
    tocken: any;
    columnstoSee: any = true;
    bodystyle: object;
    private unsubscribe: Subject<void> = new Subject();
    tmpValue: any;
    confirmModal?: NzModalRef;
    dateFormat: string = 'dd/MM/yyyy';
    selectedItem: any;
    enteredRate: any;
    listDisable: any;
    updatedRecords: any;
    recordNumber: any;
    
    constructor(
        private globalS: GlobalService,
        private timeS: TimeSheetService,
        private menuS: MenuService,
        private formBuilder: FormBuilder,
        private router: Router,
        private billingS: BillingService,
        private modal: NzModalService,
        private host: ElementRef<Element> = inject(ElementRef)
    ) { }

    enterFullscreen() {
        this.host.nativeElement.requestFullscreen();
    }
    exitFullscreen() {
        document.exitFullscreen();
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.userRole = this.tocken.role;
        this.buildForm();
        this.loadData();
        this.loading = false;
        this.modalOpen = false;
    }

    loadTitle() {
        return this.mainTitle;
    }

    handleCancel() {
        this.modalOpen = false;
        this.isVisible = false;
        this.router.navigate(['/admin/hcp']);
    }

    handleCancelModal() {
        this.modalOpen = false;
        this.selectedItem = '';
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            code: '',
            description: '',
            category: '',
            enteredRate: null,
            selectedItem: '',
            listDisable: true,
        });
    }
    ngOnChanges(changes: SimpleChanges): void {
        for (let property in changes) {
            if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
                this.isVisible = true;
            }
        }
    }

    keyPressDecimalNumbers(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57)) {
          event.preventDefault();
          return false;
        }
        return true;
    }

    resetModal() {
        this.inputForm.reset();
        this.loading = false;
    }

    loadData() {
        this.loading = true;
        this.billingS.getHcpPackageClaimList().subscribe(getListDetail => {
            this.listData = getListDetail;
            this.loading = false;
        });
    }

    showAddModal() {
        this.resetModal();
        this.modelTitle = "Add New Package Claim Rates";
        this.listDisable = false;
        this.modalOpen = true;
    }
    
    showEditModal(index: any) {
        this.resetModal();
        this.modelTitle = "Change Package Claim Rates";
        this.listDisable = true;
        this.modalOpen = true;
      const { 
        claimType,
        rate,
        recordNumber
      } = this.listData[index];
      this.inputForm.patchValue({
        selectedItem: claimType,
        enteredRate:rate
      });
      this.recordNumber = recordNumber;
    }

    delete(data: any){

    }

    delete1(data: any) {
        const group = this.inputForm;
        this.confirmModal = this.modal.confirm({
            nzTitle: 'Do you want to Delete?',
            nzContent: 'Are you sure you want to delete the HCP Package Claim Rate "' + data.code + '" ?',
            nzOnOk: () => {
                this.tmpValue = data.code;
                this.billingS.deleteAwardRuleSet(data.recordNo)
                    .pipe(takeUntil(this.unsubscribe)).subscribe(dataDeleted => {
                        if (dataDeleted) {
                            this.billingS.postAuditHistory({
                                Operator: this.tocken.user,
                                actionDate: this.globalS.getCurrentDateTime(),
                                auditDescription: 'DELETE AWARD ' + data.code,
                                actionOn: 'AWARDPOS',
                                whoWhatCode: data.recordNo, //inserted
                                TraccsUser: this.tocken.user,
                            }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                                this.globalS.sToast('Success', 'Award Rule set "' + this.tmpValue + '" has been deleted successfully.');
                            }
                            );
                            this.loadData();
                            return;
                        }
                    });
            },
            nzOnCancel: () => {
                // this.globalS.sToast('Success', 'CANCEL Button Selected.')
            }
        });
        this.tmpValue = "";
    }

    runProcess() {
        this.selectedItem = this.inputForm.get('selectedItem').value;
        this.enteredRate = this.inputForm.get('enteredRate').value;

        console.log(this.selectedItem);
        console.log(this.enteredRate);

        if (this.selectedItem != null && this.enteredRate != null) {
            this.enteredRate = this.enteredRate.split('$').pop();
            if (this.listDisable == true) { // Update
                this.popupTitle = 'Change Rate Confirmation';
                this.popupContent = 'Are you sure you want to Change the CDC Rate against ' + this.selectedItem + ' ?';
            } else if (this.listDisable == false) { // Add
                this.popupTitle = 'Add Package Confirmation';
                this.popupContent = 'Are you sure you want to Add the package "' + this.selectedItem + '" with rate ' + this.enteredRate + '?';                
            }
            this.confirmModal = this.modal.confirm({
                nzTitle: this.popupTitle,
                nzContent: this.popupContent,
                nzOnOk: () => {
                    // this.globalS.iToast('Information', 'OK Button Selected.')
                    if (this.listDisable == true) { // Update
                        this.processPackageClaimRate();
                    } else if (this.listDisable == false) { // Add
                        this.addCdcPackageClaim();
                    }
                },
                nzOnCancel: () => {
                    // this.globalS.sToast('Success', 'CANCEL Button Selected.')
                }
            });
        } else if (this.selectedItem == null && this.enteredRate == null) {
            this.globalS.eToast('Error', 'CDC Package and CDC Rate must have a valid entry.')
        } else if (this.selectedItem != null && this.enteredRate == null) {
            this.globalS.eToast('Error', 'CDC Rate must have a valid entry.')
        } else if (this.selectedItem == null && this.enteredRate != null) {
            this.globalS.eToast('Error', 'CDC Package must have a valid entry.')
        }
    }

    addCdcPackageClaim(){
        this.billingS.addCdcPackageClaim({
            ClaimDescription: this.selectedItem,
            ClaimUser1: this.enteredRate,
        }).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    this.updatedRecords = data[0].updatedRecords;
                    this.globalS.sToast('Success', 'The CDC Package Claim ' + this.selectedItem + ' has been successfully added.');
                    Promise.resolve().then(() => {
                        this.inputForm.setControl('selectedItem', new FormControl());
                    })
                }
                this.resetModal();
                this.ngOnInit();
                return false;
            });
    }

    processPackageClaimRate(){
        this.billingS.changePackageClaimRates({
            ClaimDescription: this.selectedItem,
            ClaimUser1: this.enteredRate,
            ClaimRecordNumber: this.recordNumber,
        }).pipe(
            takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    this.updatedRecords = data[0].updatedRecords;
                    this.globalS.sToast('Success', 'The Package Claim Rate against ' + this.selectedItem + ' has been successfully updated.');
                    Promise.resolve().then(() => {
                        this.inputForm.setControl('selectedItem', new FormControl());
                    })
                }
                this.resetModal();
                this.ngOnInit();
                return false;
            });
    }
}
