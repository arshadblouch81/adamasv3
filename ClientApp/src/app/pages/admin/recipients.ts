import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  AfterViewInit,
  Renderer2,
  ViewChild,
  ElementRef,
  EventEmitter,
  
} from '@angular/core'
import {Router, ActivatedRoute} from '@angular/router';

import {
  GlobalService,
  StaffService,
  nodes,
  checkOptionsOne,
  sampleList,
  ShareService,
  leaveTypes,
  ListService,
  TimeSheetService,
  PrintService
} from '@services/index';
import {forkJoin, of, Subject, Observable, observable, EMPTY} from 'rxjs';
import {RECIPIENT_OPTION, User} from '../../modules/modules';
import {NzFormatEmitEvent} from 'ng-zorro-antd/core';
import format from 'date-fns/format';
import {filter, takeUntil,debounceTime,distinctUntilChanged } from 'rxjs/operators';
import {HttpClient, HttpEvent, HttpEventType, HttpRequest, HttpResponse} from '@angular/common/http';
import {NzMessageService} from 'ng-zorro-antd/message';
import {UploadChangeParam} from 'ng-zorro-antd/upload';
import {FormBuilder, FormGroup, Validators, FormControl, Form} from '@angular/forms';
import { title } from 'process';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { error } from 'console';
const inputFormDefault ={
  coreONI:[false],
  functionalProfile:[false],
  carerProfile:[false],
  livingArrangements:[false],
  culturalProfile:[false],
  healthConditionsProfile:[false],
  psychoSocialProfile:[false],
  DSQ:[false],
  allProfiles:[false],

  name:['SYSMGR'],
  agency:['Agency'],
  designation:['Coordinator'],
  phoneNumber:[''],
  date:[''],

}



@Component({
  styleUrls: ['./recipient.css'],
  templateUrl: './recipients.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})


export class RecipientsAdmin implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('menuSub') menuSub: ElementRef;
  @ViewChild('toggleMenu') toggleMenu: ElementRef

  @ViewChild('SearchListComponent')  SearchListComponent: any
  private unsubscribe: Subject<void> = new Subject();
   loadOption: Subject<any> = new Subject();
  private newLink: Subject<string> = new Subject<string>();
  option: string = 'add';
  current_tab: string ='Personal';
  private unsubscribe$ = new Subject()
  allBranches: boolean = true;
  allBranchIntermediate: boolean = false;

  allProgarms: boolean = true;
  allprogramIntermediate: boolean = false;

  allCordinatore: boolean = true;
  allCordinatorIntermediate: boolean = false;

  allcat: boolean = true;
  allCatIntermediate: boolean = false;


  allChecked: boolean = true;
  indeterminate: boolean = false;
  QuicksearchOptions:Array<any> = [];
  QuicksearchValue:any;

  user: any = null;
  nzSelectedIndex: number = 0;
  isFirstLoad: boolean = false;
  programModalOpen: boolean = false;
  findModalOpen: boolean = false;
  modalOpen: boolean = false;
  modalOpen2: boolean = false;
  modalPrintONI: boolean = false;
  sample: any;

  newReferralModal: boolean = false;
  newQuoteModal: boolean = false;
  quoteModal: boolean = false;
  saveModal: boolean = false;
  selectedProgram:any;
  selectedServiceAgreement:any;
  RecipientCode:string;
  topBelow = { top: '20px' }
  changeCodeOpen: boolean = false;

  newOtherModal: boolean = false;
  loading: boolean = false;
  isLoading: boolean = false;
  current: number = 0;
  alist:Array<any> = [];
  alist_original:Array<any> = [];
  alist2:Array<any> = [];
  alist2_original:Array<any> = [];
  HighlightRow2:number;
  selectedValue: any;
  value: any;
  onCallText:any;
  hide:boolean;
  show:boolean;
  activeRowData:any;
  selectedRowIndex:number;
  status: any = null;
  statusTab = new Subject<any>();
  QuicksearchType:boolean=true;
  Unique_ID: string;
  Account_No: string;
  /**
   * Filter Data Vars
   */
  isActive: boolean = true;
  inActive: boolean = false;

  recipientOptionOpen: any;
  recipientOption: string;
  from: any = {display: 'admit'};
  fileList2: Array<any> = [];
  urlPath: string = `api/v2/file/upload-document-remote`;
  acceptedTypes: string = "image/png,image/jpeg,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf";
  file: File;
  referdocument: boolean = false;

  RECIPIENT_OPTION = RECIPIENT_OPTION;

  recipientStatus: string = 'active';
  recipientType: any;
  selectedRecipient: any;
  searched:Subject<boolean>= new Subject()
  programs: Array<any> = [];
  activePrograms: string = null;
  tabs = [1, 2, 3];
  checked: any;
  sampleList: Array<any> = sampleList;
  cariteriaList: Array<any> = [];
  nodelist: Array<any> = [];
  HighLightRow2:number;
  
  checkOptionsOne = checkOptionsOne;
  navigationExtras: { state: { StaffCode: string; ViewType: string; IsMaster: boolean; }; };
  currentDate = new Date();
  longMonth = this.currentDate.toLocaleString('en-us', {month: 'long'});
  sampleModel: any;

  switchValue: boolean = false;

  record:any;
  headerInformation: any ;

  columns: Array<any> = [
    {
      name: 'ID',
      checked: false
    },
    {
      name: 'URNumber',
      checked: false
    },
    {
      name: 'AccountNo',
      checked: false
    },
    {
      name: 'Surname',
      checked: false
    },
    {
      name: 'Firstname',
      checked: false
    },
    {
      name: 'Fullname',
      checked: false
    },
    {
      name: 'Gender',
      checked: true
    },
    {
      name: 'DOB',
      checked: true
    },
    {
      name: 'Address',
      checked: true
    },
    {
      name: 'Contact',
      checked: true
    },
    {
      name: 'Type',
      checked: true
    },
    {
      name: 'Branch',
      checked: true
    },
    {
      name: 'Coord',
      checked: false
    },
    {
      name: 'Category',
      checked: false
    },
    {
      name: 'ONI',
      checked: false
    },
    {
      name: 'Activated',
      checked: false
    },
    {
      name: 'Deactivated',
      checked: false
    },
    {
      name: 'Suburb',
      checked: false
    }
  ]

  data = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    }
  ];

  casemanagers: any;
  categories: any;
  programsList: any;
  branchesList: any;
  filters: any;
  dateFormat: string = 'dd/MM/yyyy';
  quicksearch: any;
  ExpandAlertsForm:FormGroup;
  selectedRecpientTypes: any[];
  types: any[];
  groupAlerts:boolean = false;
  extendedSearch: any;
  filteredResult: any;
  selectedTypes: any;
  selectedbranches: any[];
  testcheck: boolean = false;
  categoriesList: any;
  selectedPrograms: any;
  selectedCordinators: any;
  selectedCategories: any;
  rights: any;
  tocken: any;

  loading2:boolean;

  inputForm: FormGroup;

  coreONI: boolean ;
  functionalProfile: boolean ;
  carerProfile: boolean ;
  livingArrangements: boolean ;
  culturalProfile: boolean ;
  healthConditionsProfile: boolean;
  psychoSocialProfile: boolean;
  DSQ: boolean ;
  allProfiles: boolean;
  name: string;
  agency: string;
  designation: string;
  phoneNumber: string;
  date: string;
  

  activeInactive: boolean = false

  tryDoctype : any; 
  drawerVisible: boolean;
  pdfTitle: string;

  // changeStatusBool(data: any) {
  //   this.activeInactive = data;
  //   this.SearchListComponent.searchRecipient();
  // }


      
      changeStatusBool(data: any) {
        this.activeInactive = data;
        this.SearchListComponent.searchRecipient();
      }

  nzEvent(event: NzFormatEmitEvent): void {
    if (event.eventName === 'click') {
      var title = event.node.origin.title;

      this.extendedSearch.patchValue({
        title: title,
      });
      var keys = event.keys;

    }

  }

  SaveAll_Changes(){
    
    let data = {
      type:'Recipient',
      tab: this.current_tab
    }
    
    this.sharedS.emitSaveAll_Changes(data)
  }
  log(event: any, index: number) {
    this.testcheck = true;
    if (index == 1)
      this.selectedbranches = event;
    if (index == 2)
      this.selectedPrograms = event;
    if (index == 3)
      this.selectedCordinators = event;
    if (index == 4)
      this.selectedCategories = event;
  }

  setCriteria() {

    this.cariteriaList.push({
      fieldName: this.extendedSearch.value.title,
      searchType: this.extendedSearch.value.rule,
      textToLoc: this.extendedSearch.value.from,
      endText: this.extendedSearch.value.to,
    })
  }

  getRights(loginuser) {
    this.timeS.getbuttonstatusofwizard(loginuser).pipe(takeUntil(this.unsubscribe))
      .subscribe(data => {
        this.rights = data[0];
        //console.log(this.rights)
        this.detectChanges();
      });
  }

  getPermisson(index: number) {
    var permissoons = this.rights.recipientRecordView;
    return permissoons.charAt(index - 1);
  }


  listChange(event: any) {
    if (event == null) {
      this.user = null;
      this.isFirstLoad = false;
      this.sharedS.emitChange(this.user);
     
      return;
    }

    if (!this.isFirstLoad) {
      this.view(0);
      // this.view(10);

      this.isFirstLoad = true;
    }

    // console.log(JSON.stringify(event));
    // console.log(event);
    // console.log(JSON.stringify(event));
    this.globalS.id = event.uniqueID;

   
    this.user = {
      code: event.accountNo,
      id: event.uniqueID,
      view: event.view,
      agencyDefinedGroup: event.agencyDefinedGroup,
      sysmgr: event.sysmgr
    }

    this.sharedS.emitChange(this.user);

   

    this.listS.getstatusofwizard(this.user.id).pipe(takeUntil(this.unsubscribe))
      .subscribe(data => {
        this.status = data;
        this.detectChanges();
      });

      if (this.user != null) {
        //let sql=`SELECT [Program] FROM RecipientPrograms rp INNER JOIN HumanResourceTypes pr ON rp.Program = pr.Name  WHERE  [PersonID] = '${this.user.id}' AND IsNull([User2], '') <> 'Contingency' AND rp.ProgramStatus <> 'INACTIVE' ORDER BY [Program]`;
        this.timeS.getfunding(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {      
            this.alist = data;
            this.alist_original = data; 
            if (this.alist.length <= 0)
              this.allPrpograms();
           
        });

       
  }
      
  }

  allPrpograms(){
   
    this.timeS.getPrograms(this.user.id).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.alist = data;    
      this.alist_original = data; 
    
  });
}


  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private sharedS: ShareService,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private listS: ListService,
    private timeS: TimeSheetService,
    private globalS: GlobalService,
    private http: HttpClient,
    private msg: NzMessageService,
    private renderer2: Renderer2,
    private printS: PrintService,
    private sanitizer:DomSanitizer,
    private ModalS: NzModalService,
  ) {

    this.sharedS.emitProfileStatus$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      
      this.selectedRecipient = data;
      this.headerInformation = data.type + ' / ' + this.current_tab + ' / ' + data.accountNo + ' / ' + data.uniqueID;
      this.onCallText =' On Call ' + data.accountNo + ' ' + data.uniqueID;
      this.recipientType = data.type == null || data.type.trim() == "" ? null : data.type;
      // if(data.admissionDate == null && data.dischargeDate == null){
      //     this.recipientStatus = null;
      //     return;
      // }
      this.timeS.getrecipientAlerts(this.selectedRecipient.uniqueID).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.recipientAlerts = data.map(x=>x.name);
    
      if (data.length > 0) {
        this.groupAlerts = true;
      }else{
        this.groupAlerts = false;
      }
    error => {
      this.groupAlerts = false;
    }
    });
      if (this.globalS.doc != null) {
        this.addRefdoc();
      }
      this.globalS.var1 = data.uniqueID;
      this.globalS.var2 = data.accountNo;

      if (data.admissionDate != null && data.dischargeDate == null) {
        this.recipientStatus = 'active';
      } else {
        this.recipientStatus = 'inactive';
          
      }
      this.hasHoldProgram();
      this.listS.getProfileActiveprogram(this.selectedRecipient.uniqueID).pipe(takeUntil(this.unsubscribe))
      .subscribe(data => {
        this.activePrograms = data.toString();

        if (typeof this.activePrograms !== 'undefined' && this.activePrograms.length === 0) {
        }else{
        }
        this.detectChanges();
      })

      // this.sharedS.emitSaveAll$.subscribe(data => {
      //   this.SaveAll_Changes();
      // });
      // this.activePrograms = this.listS.getProfileActiveprogram(data.uniqueID);
      
      
    

    })

    
     this.newLink.pipe(
          debounceTime(600), // Wait 500ms after the user stops typing
         // distinctUntilChanged() // Only emit if the value changes
        ).subscribe(value => {
          this.processlink(value); // Call your processing function here
        });

  }

  listOfData: Array<{ name: string; age: number; address: string }> = [];
 
  ngOnInit(): void {

    // this.globalS.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwMDAvIiwiYXVkIjoiY2xpZW50cG9ydGFsIiwibmFtZWlkIjoic3lzbWdyIiwiaWF0IjoxNjQyMTI1NDcyLCJleHAiOjE2NDIxNzk0NzUsIm5iZiI6MTY0MjEyNTQ3MiwiY29kZSI6IlNLRUVORSBST0RFUklHTyIsInJvbGUiOiJBRE1JTiBVU0VSIiwidXNlciI6IlNZU01HUiIsInVuaXF1ZUlEIjoiUzAxMDAwMDUzOTMiLCJjYW5DaG9vc2VQcm92aWRlciI6IkZhbHNlIiwicmVjaXBpZW50RG9jRm9sZGVyIjoiQzpcXEFkYW1hc1xcU3VwcG9ydFxcQ3JhbmVzIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQURNSU4gVVNFUiIsImp0aSI6ImZhMzU2ZmU0LWU4N2QtNGExOC05MDA4LTYwNDQyNWQyZTUwMyJ9.LOmuL9GwvWAtacoStsIl9-cAESqClezqCeege08G52I"
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    // this.tocken ={"iss":"http://localhost:5000/","aud":"clientportal","nameid":"sysmgr","iat":1642115732,"exp":1642169735,"nbf":1642115732,"code":"SKEENE RODERIGO","role":"ADMIN USER","user":"SYSMGR","uniqueID":"S0100005393","canChooseProvider":"False","recipientDocFolder":"C:\\Adamas\\Support\\Cranes","http://schemas.microsoft.com/ws/2008/06/identity/claims/role":"ADMIN USER","jti":"18aecec4-c164-448a-b8d6-d714eb50cd19"}

    // console.log(JSON.stringify(this.tocken));
    this.inputForm = this.fb.group(inputFormDefault);


    for (let i = 0; i < 100; i++) {
      this.listOfData.push({
        name: `Edward King`,
        age: 32,
        address: `LondonLondonLondonLondonLondon`
      });
    }
    this.nodelist = nodes;
  
  
      this.getRights(this.tocken.user);
      this.getUserData();
      this.buildForm();
  
   
   
  }

  ngOnDestroy(): void {

  }

  ngAfterViewInit() {
    // this.renderer2.listen('window', 'click',(e:Event)=>{
    //     if(e.target !== this.menuSub.nativeElement){
    //           this.showSubMenu = !this.showSubMenu;
    //     }
    // });
   
  }

  


  tab(tb:any){  
    this.current_tab=tb  ;
    console.log(this.current_tab)
    
    this.headerInformation = this.selectedRecipient.type + ' / ' + this.current_tab + ' / ' + this.selectedRecipient.accountNo;
   
  }
  showSubMenu: boolean = false;
  showSubMenuroster:boolean=false;
  showSubMenureferel:boolean=false;

  searchData(): void {
    this.loading = true;

    this.selectedTypes = this.checkOptionsOne
      .filter(opt => opt.checked)
      .map(opt => opt.value).join("','")

    this.selectedPrograms = this.programsList
      .filter(opt => opt.checked)
      .map(opt => opt.name).join("','")

    this.selectedCordinators = this.casemanagers
      .filter(opt => opt.checked)
      .map(opt => opt.uniqueID).join("','")

    this.selectedCategories = this.categoriesList
      .filter(opt => opt.checked)
      .map(opt => opt.description).join("','")

    this.selectedbranches = this.branchesList
      .filter(opt => opt.checked)
      .map(opt => opt.description).join("','")

      
    var postdata = {
      active: this.quicksearch.value.active,
      inactive: this.quicksearch.value.inactive,
      alltypes: this.allChecked,
      selectedTypes: this.selectedTypes,
      allBranches: this.allBranches,
      selectedbranches: (this.allBranches == false) ? this.selectedbranches : '',
      allProgarms: this.allProgarms,
      selectedPrograms: (this.allProgarms == false) ? this.selectedPrograms : '',
      allCordinatore: this.allCordinatore,
      selectedCordinators: (this.allCordinatore == false) ? this.selectedCordinators : '',
      allcat: this.allcat,
      selectedCategories: (this.allcat == false) ? this.selectedCategories : '',
      activeprogramsonly: this.filters.value.activeprogramsonly,
      surname: this.quicksearch.value.surname,
      firstname: this.quicksearch.value.firstname,
      phoneno: this.quicksearch.value.phoneno,
      suburb: this.quicksearch.value.suburb,
      dob: (!this.globalS.isEmpty(this.quicksearch.value.dob)) ? this.globalS.convertDbDate(this.quicksearch.value.dob, 'yyyy-MM-dd') : '',
      fileno: this.quicksearch.value.fileno,
      searchText: this.quicksearch.value.searchText,
      criterias: this.cariteriaList // list of rules
    }

    this.timeS.postrecipientquicksearch(postdata).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.filteredResult = data;
      this.loading = false;
      this.detectChanges();
    });

  }
  closePop(){
    this.hide=true;
   
  }
  updateAllChecked(): void {
    this.indeterminate = false;
    if (this.allChecked) {
      this.checkOptionsOne = this.checkOptionsOne.map(item => ({
        ...item,
        checked: true
      }));
    } else {
      this.checkOptionsOne = this.checkOptionsOne.map(item => ({
        ...item,
        checked: false
      }));
    }
  }

  updateAllCheckedFilters(filter: any): void {

    if (filter == 1 || filter == -1) {

      console.log(this.testcheck + "test flag");

      if (this.testcheck == false) {  // why its returing undefined
        if (this.allBranches) {
          this.branchesList.forEach(x => {
            x.checked = true;
          });
        } else {
          this.branchesList.forEach(x => {
            x.checked = false;
          });
        }
      }
    }

    if (filter == 2 || filter == -1) {
      if (this.testcheck == false) {
        if (this.allProgarms) {
          this.programsList.forEach(x => {
            x.checked = true;
          });
        } else {
          this.programsList.forEach(x => {
            x.checked = false;
          });
        }
      }
    }
    if (filter == 3 || filter == -1) {
      if (this.testcheck == false) {
        if (this.allCordinatore) {
          this.casemanagers.forEach(x => {
            x.checked = true;
          });
        } else {
          this.casemanagers.forEach(x => {
            x.checked = false;
          });
        }
      }
    }

    if (filter == 4 || filter == -1) {
      if (this.testcheck == false) {
        if (this.allcat) {
          this.categoriesList.forEach(x => {
            x.checked = true;
          });
        } else {
          this.categoriesList.forEach(x => {
            x.checked = false;
          });
        }
      }
    }
  }

  updateSingleChecked(): void {
    if (this.checkOptionsOne.every(item => !item.checked)) {
      this.allChecked = false;
      this.indeterminate = false;
    } else if (this.checkOptionsOne.every(item => item.checked)) {
      this.allChecked = true;
      this.indeterminate = false;
    } else {
      this.indeterminate = true;
      this.allChecked = false;
    }
  }

  updateSingleCheckedFilters(index: number): void {
    if (index == 1) {
      if (this.branchesList.every(item => !item.checked)) {
        this.allBranches = false;
        this.allBranchIntermediate = false;
      } else if (this.branchesList.every(item => item.checked)) {
        this.allBranches = true;
        this.allBranchIntermediate = false;
      } else {
        this.allBranchIntermediate = true;
        this.allBranches = false;
      }
    }
    if (index == 2) {
      if (this.programsList.every(item => !item.checked)) {
        this.allProgarms = false;
        this.allprogramIntermediate = false;
      } else if (this.programsList.every(item => item.checked)) {
        this.allProgarms = true;
        this.allprogramIntermediate = false;
      } else {
        this.allprogramIntermediate = true;
        this.allProgarms = false;
      }
    }
    if (index == 3) {
      if (this.casemanagers.every(item => !item.checked)) {
        this.allCordinatore = false;
        this.allCordinatorIntermediate = false;
      } else if (this.casemanagers.every(item => item.checked)) {
        this.allCordinatore = true;
        this.allCordinatorIntermediate = false;
      } else {
        this.allCordinatorIntermediate = true;
        this.allCordinatore = false;
      }
    }
    if (index == 4) {
      if (this.categoriesList.every(item => !item.checked)) {
        this.allcat = false;
        this.allCatIntermediate = false;
      } else if (this.categoriesList.every(item => item.checked)) {
        this.allcat = true;
        this.allCatIntermediate = false;
      } else {
        this.allCatIntermediate = true;
        this.allcat = false;
      }
    }
  }

  buildForm() {
    // alltypes: true,
    this.quicksearch = this.fb.group({
      active: true,
      inactive: false,
      surname: '',
      firstname: '',
      phoneno: '',
      suburb: '',
      dob: '',
      fileno: '',
      searchText: '',
    });

    this.filters = this.fb.group({
      activeprogramsonly: false,
    });

   
    this.extendedSearch = this.fb.group({
      title: '',
      rule: '',
      from: '',
      to: '',

      activeonly: true,
    });

    this.quicksearch = this.fb.group({
      active: false,
      inactive: false,
      activeonly: true,
    });

    this.ExpandAlertsForm = this.fb.group({
      ExpandAlerts: false,
      ExpandAllGroups: false,
      activeonly: true,
    });
    
  }

  getUserData() {
    return forkJoin([
      this.listS.getlistbranchesObj(),
      this.listS.getprogramsobj(),
      this.listS.getcoordinatorslist(),
      this.listS.getcategoriesobj(),
    ]).subscribe(x => {
      this.branchesList = x[0];
      this.programsList = x[1];
      this.casemanagers = x[2];
      this.categoriesList = x[3];
    });
  }

  view(index: number) {
    this.nzSelectedIndex = index;

    if (index == 0) {
      this.router.navigate(['/admin/recipient/personal'])
    }
    if (index == 1) {
      this.router.navigate(['/admin/recipient/contacts']);
    }
    if (index == 2) {
      this.router.navigate(['/admin/recipient/intake'])
    }
    if (index == 3) {
      this.router.navigate(['/admin/recipient/reminders'])
    }
    if (index == 4) {
      this.router.navigate(['/admin/recipient/opnote'])
    }
    if (index == 5) {
      this.router.navigate(['/admin/recipient/casenote'])
    }
    if (index == 6) {
      this.router.navigate(['/admin/recipient/incidents'])
    }
    if (index == 7) {
      this.router.navigate(['/admin/recipient/perm-roster'])
    }
    if (index == 8) {
      this.router.navigate(['/admin/recipient/history'])
    }
    if (index == 9) {
      this.router.navigate(['/admin/recipient/insurance-pension'])
    }
    if (index == 10) {
      this.router.navigate(['/admin/recipient/quotes'])
    }
    if (index == 11) {
      this.router.navigate(['/admin/recipient/documents'])
    }
    if (index == 12) {
      this.router.navigate(['/admin/recipient/attendance'])
    }
    if (index == 13) {
      this.router.navigate(['/admin/recipient/others'])
    }
    if (index == 14) {
      this.router.navigate(['/admin/recipient/accounting'])
    }
    if (index == 15) {
      this.router.navigate(['/admin/recipient/media'])
    }
    if (index == 16) {
      this.router.navigate(['/admin/recipient/clinical'])
    }
    if (index == 18) {
      this.router.navigate(['/admin/recipient/dataset'])
    }
    if (index == 19) {
      this.router.navigate(['/admin/recipient/shift-reports'])
    }
    if (index == 20) {
      this.router.navigate(['/admin/recipient/suspensions'])
    }
  }

  handleCancel() {
    this.findModalOpen = false;
    this.referdocument = false;
    this.drawerVisible = false;

  }

  handleOk() {
    //  this.referdocument = false;
  }
  initiateReport(rptid){

    var s_name = this.inputForm.value.name;
    var s_agency = this.inputForm.value.agency ;
    var s_designation = this.inputForm.value.designation;
    //var s_sign = this.inputForm.value.????;
    var s_date = this.inputForm.value.date ;
    var s_contact = this.inputForm.value.phoneNumber;
    const footer = [s_name,s_agency,s_designation,s_date,s_contact,];
    const DataArray = [footer];
    //DataArray[0] is for footer Info
    //console.log(DataArray);
    //console.log(DataArray[0][0]);        
    this.RenderReport(rptid,DataArray[0],'');

  }
  
  RenderReport(RptType,DataArray,RptTitle){
    /*var Rptid;
    if(RptType == "blank"){
      Rptid = "J2t6epLG3UTTb77d"
    } else{
      Rptid =
    }  */
          const data = {
            "template": { "_id": "J2t6epLG3UTTb77d" },
                        
            "options": {
                "reports": { "save": false },            
                "sql": DataArray,            
                "userid": this.tocken.user,
                "txtTitle":RptTitle,                                                                                                     
            }
          }
                             
            this.loading = true;
           
          this.tryDoctype = "";
          this.loading = true; 
          this.drawerVisible = true; 
          
  
      this.printS.printControl(data).subscribe((blob: any) => {
          this.pdfTitle = "ONI Report.pdf"
          this.drawerVisible = true;                   
          let _blob: Blob = blob;
          let fileURL = URL.createObjectURL(_blob);
          this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
          this.loading = false;
          this.cd.detectChanges();
      }, err => {
          console.log(err);
          this.loading = false;
          this.ModalS.error({
              nzTitle: 'TRACCS',
              nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
              nzOnOk: () => {
                  this.drawerVisible = false;
              },
          });
      });
      
      
      return;
  
  }
  detectChanges() {
    this.cd.markForCheck();
    this.cd.detectChanges();
  }

  closeProgram() {
    this.programModalOpen = false;
  }

  currentMonthRoster() {
    console.log(this.user);
    this.navigationExtras = {state: {StaffCode: this.user.code, ViewType: 'Recipient', IsMaster: false}};
    this.router.navigate(["/admin/rosters"], this.navigationExtras)
  }

  rosterMaster() {
    console.log(this.user);
    this.navigationExtras = {state: {StaffCode: this.user.code, ViewType: 'Recipient', IsMaster: true}};
    this.router.navigate(["/admin/rosters"], this.navigationExtras)
  }

  loadPrograms() {
    if (!this.selectedRecipient) {
      return;
    }
    this.listS.getrecipientprograms(this.selectedRecipient.uniqueID).pipe(takeUntil(this.unsubscribe))
      .subscribe(data => {
        this.programs = data;
        this.detectChanges();
      })
  }

  openReferInModal: any;
  profileData: any;
  AddNewReferralDone:boolean;
  openReferModal(user: any) {
    console.log(user.toString());

    this.sample = user;    
    this.profileData = user;
    this.user = user;
    this.AddNewReferralDone=true;
      // Assume SearchListComponent returns a Promise
    this.sharedS.emitOnSearchListNext(user.code)    
    this.SearchListComponent.searchRecipient();
    this.cd.detectChanges();
  
   
  }

  LoadReferalModel(){
   
      this.recipientOption = this.RECIPIENT_OPTION.REFER_IN;
      this.recipientOptionOpen = {};
      this.loadOption.next(this.user);
      this.sharedS.setRecipientData(this.selectedRecipient);
      this.cd.detectChanges();
  
   
  }

  newReferralOther(){
    let user = this.user;
    // this.newOtherModal = true;
    this.newReferralModal = !this.newReferralModal;
    setTimeout (() => {
      this.loadOption.next(this.user);
      this.cd.detectChanges();
      },300)
    // this.sharedS.emitOnSearchListNext(user.code);
    // this.profileData = user;
    // this.recipientOption = this.RECIPIENT_OPTION.REFER_IN;
    // this.user = user;
    // this.recipientOptionOpen = {};
  }
  isSubMenuOpen:boolean;

  clicky(index: number) {
    this.sharedS.setRecipientData(this.selectedRecipient);    
    if (index == 0) {
      if (this.status?.b_IsDead || this.status?.b_HasReferrals==null){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.REFER_IN;
      this.recipientOptionOpen = {};
    }

    if (index == 1) {
      if (this.status?.b_IsDead || this.status?.b_HasReferrals==null){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.REFER_ON;
      this.recipientOptionOpen = {};
    }

    if (index == 2) {
      if (this.status?.b_IsDead || !this.status?.b_HasReferrals){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.NOT_PROCEED;
      this.recipientOptionOpen = {};
    }

    if (index == 3) {
      if (this.status?.b_IsDead || !this.status?.b_HasReferrals){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.ASSESS;
      this.recipientOptionOpen = {};
    }

    if (index == 4) {
      if (this.status?.b_IsDead || !this.status?.b_HasReferrals){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.ADMIT;
      this.recipientOptionOpen = {};
    }

    if (index == 5) {
      if (this.status?.b_IsDead || !this.status?.b_HasActivePrograms){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.WAIT_LIST;
      this.recipientOptionOpen = {};
    }

   
    if (index == 6) {
      if (this.status?.b_IsDead || !this.status?.b_HasActivePrograms){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.DISCHARGE;
      this.recipientOptionOpen = {};
    }

    if (index == 7) {
      if (this.status?.b_IsDead || !this.status?.b_HasActivePrograms){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.SUSPEND;
      this.recipientOptionOpen = {};
    }

    if (index == 8) {
      if (this.status?.b_IsDead || (!this.status?.b_HasOnHoldPrograms && !this.status?.b_HasOnHoldServices)){
      //  return
      }
      this.recipientOption = this.RECIPIENT_OPTION.REINSTATE;
      this.recipientOptionOpen = {};
    }

    if (index == 9) {
      if (this.status?.b_IsDead || this.status == null){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.DECEASE;
      this.recipientOptionOpen = {};
    }

    if (index == 10) {
      if (this.status?.b_IsDead || this.status == null){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.ADMIN;
      this.recipientOptionOpen = {};
      //this.isSubMenuOpen = !this.isSubMenuOpen;
    }

    if (index == 11) {
      if (this.status?.b_IsDead || this.status == null){
        return
      }
      this.recipientOption = this.RECIPIENT_OPTION.ITEM;
      this.recipientOptionOpen = {};
    }
    if (index == 12) {
      if (this.status?.b_IsDead || this.status == null){
        return
      }      
      this.router.navigate(['/admin/Print'])
    }
    if (index == 13) {
      if (this.status?.b_IsDead || this.status == null){
        return
      }
      this.modalPrintONI= true;
    }
    
    if (index==101){
      this.activationDone();
      return;
    }
    if (index == 102) {
     
      this.recipientOption = this.RECIPIENT_OPTION.DEACTIVATE;
      this.recipientOptionOpen = {};
    }
    if (index == 103) {
     
      this. openRecipientchangeCodeModel();
      
    }
    if (index == 104) {
     
      this.showConfirm();
    }
    setTimeout(()=>{
    this.loadOption.next(this.user);
    this.cd.detectChanges();
    },300);
  }

  showConfirm(): void {
    //var deleteRoster = new this.deleteRoster();
    var msg =`
      Ther are unfinalized roster entries for the recipient, if you delete the recipient and subsequently run the timesheet update, 
      you will receive warning message for any services being updated for this recipient,
      Please use timesheet entry/edit to view a service list for the recipient,
      and either delete or approve the entries
      Do you still want to permentatly DELTE the recipient.
    `;
    this.ModalS.confirm({
      nzTitle: 'Disclaimer!!',
      nzContent: msg,
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
      new Promise((resolve,reject) => {
        setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
       
        this.showConfirm2();
      }).catch(() => console.log('Recipient is not deleted due to some errors!'))

      
    });
  }
  showConfirm2(): void {
    //var deleteRoster = new this.deleteRoster();
    var msg =`
      Ther operation will irretrievably remove the recipient from databae,
       Are you sure ? (If in any any dout, please make it sure you have a current Backup, you can refer to if necessary).
    `;
    this.ModalS.confirm({
      nzTitle: 'Disclaimer!!',
      nzContent: msg,
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
      new Promise((resolve,reject) => {
        setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
       
        this.deleteRecipient();
      }).catch(() => console.log('Recipient is not deleted due to some errors!'))

      
    });
  }
  deleteRecipient(){
    let input ={
      code : this.user.code,
      user : this.tocken.user,
      winUser : this.tocken.user
    }
    this.listS.deleteRecipient(input).pipe(takeUntil(this.unsubscribe$)).subscribe(d=>{
      this.globalS.sToast('Recipient', 'Recipient Deleted Successfully')
     this.sample=null;
     this.cd.detectChanges();
      
     window.location.reload();

    });
  }
  SearchOnEnter(event:KeyboardEvent){
    if (event.key === 'Enter' ) {
      event.preventDefault();
      this.searchData();
    }
  }
  bookingData = new Subject<any>();
  openFindModal() {
  
   // this.findModalOpen = true;
   setTimeout(() => {
    this.bookingData.next({type: 'recipient', data: this.selectedRecipient});
   }, 500);
   
  }
   
  openFindModal2() {
    this.tabFindIndex = 0;
    this.findModalOpen = true;
    this.filteredResult=[];
    let lstAddress=[];
    this.listS.getAddressTypes().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      lstAddress=data.map(x=> {
        return {
          key: x,
          title: x,
          isLeaf: true
        }
      })

      let index = 0; //this.nodelist.indexOf(x=>x.key=='100');
      let node = this.nodelist.find(x=>x.key=='100');
      let insertAfterIndex = node.children.findIndex(x=>x.key=='1008');
      node.children.splice(insertAfterIndex + 1, 0, ...lstAddress);

      this.nodelist[index] = node;
      this.cd.detectChanges();
    });
    
    
  }
  openBudgetModal() {
    this.tabFindIndex = 0;
    this.txtSearch="";
    if (this.alist.length>1){
      this.modalOpen = true;
      this.cd.detectChanges();
     
    } else  if (this.alist.length==1){
      this.selectedProgram=this.alist[0];
     
      this.openServiceAgreementModal();     
    
      this.loading2=true;
    
    }
    
    //let sql=`SELECT IDENT_CURRENT('Documents') AS [DocID],* FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY Title`;

    this.listS.gettemplatelistAll().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        this.alist2 = data;
        this.alist2_original=data;
        this.loading2=false;
        this.cd.detectChanges()
       
    });
 

  }
  openServiceAgreementModal() {
    this.tabFindIndex = 0;
    this.modalOpen2 = true;
    this.txtSearch="";
   /* let sql=`SELECT * FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY Title`;
    this.listS.getlist(sql).subscribe(data => {
        this.alist2 = data;
       
    });*/
    
  }
  tabFindIndex: number = 0;

  tabFindChange(index: number) {
    if (index == 1) {
      this.updateAllCheckedFilters(-1);
    }
    this.tabFindIndex = index;
  }

  filterChange(index: number) {

  }

  addRefdoc() {
    //console.log(this.globalS.doc.toString());
    /*if (this.globalS.doc.toString() != null){
      console.log(this.globalS.doc.toString());
      this.referdocument = true;
    } */


    this.referdocument = true;
    this.globalS.doc = null;


  }

  customReq = () => {
    //console.log(this.globalS.doc.label)

    console.log(this.file);
    this.referdocument = false;
    const formData = new FormData();

    //const { program, discipline, careDomain, classification, category, reminderDate, publishToApp, reminderText, notes  } = this.incidentForm.value;

    formData.append('file', this.file as any);
    /*formData.append('data', JSON.stringify({
      PersonID: this.innerValue.id,
      DocPath: this.token.recipientDocFolder,

      Program: program,
      Discipline: discipline,
      CareDomain: careDomain,
      Classification: classification,
      Category: category,
      ReminderDate: reminderDate,
      PublishToApp: publishToApp,
      ReminderText: reminderText,
      Notes: notes,
      SubId: this.innerValue.incidentId
    })) */

    const req = new HttpRequest('POST', this.urlPath, formData, {
      reportProgress: true,
      withCredentials: true
    });

    var id = this.globalS.loadingMessage(`Uploading file ${this.file.name}`)
    this.http.request(req).pipe(filter(e => e instanceof HttpResponse)).subscribe(
      (event: HttpEvent<any>) => {
        this.msg.remove(id);
        this.globalS.sToast('Success', 'Document uploaded');
      },
      err => {
        console.log(err);
        this.msg.error(`${this.file.name} file upload failed.`);
        this.msg.remove(id);
      }
    );
  };

  handleChange({file, fileList}: UploadChangeParam): void {
    const status = file.status;
    if (status !== 'uploading') {
      // console.log(file, fileList);
    }
    if (status === 'done') {
      this.globalS.sToast('Success', `${file.name} file uploaded successfully.`);


    } else if (status === 'error') {
      this.globalS.sToast('Error', `${file.name} file upload failed.`);

    }
  }

  selectRecipient(data:any){
    this.selectedRecipient=data;
    this.user ={
      code: data.accountNo,
      id: data.id,
      view: 'recipient',
      agencyDefinedGroup: data.category,
      sysmgr: this.tocken.user=='sysmgr' ? true :false
    }
  }
  
  AllocateRecipient(data:any){
    this.selectedRecipient=data;
    this.user ={
      code: data.accountNo,
      id: data.id,
      view: 'recipient',
      agencyDefinedGroup: data.category,
      sysmgr: this.tocken.user=='sysmgr' ? true :false
    }
    //this.sample =data.accountNo;
    this.searched.next(data.accountNo);
    this.router.navigate(['admin/recipient/personal'], { state: { data: data } });
    this.sharedS.emitChange(this.user);
   
    this.findModalOpen=false;
  }
  delCriteria(i:number){
      this.cariteriaList.splice(i,1);
  }
  handleClose() {
    this.newReferralModal = false;
    this.saveModal = false;
    this.quoteModal = false;
    this.newOtherModal = false;
    this.findModalOpen = false;
    this.referdocument = false;
    this.quicksearch.reset();
  }

  clickOutsideMenu(data: any) {
    if (data.value) {
      this.showSubMenu = false;
    }
  }

  clickOutsideMenuRoster(data: any) {
    if (data.value) {
      this.showSubMenuroster = false;
    }
  }
  clickOutsideMenuReferel(data:any){
    if(data.value){
      this.showSubMenureferel=false;
    }
  }
  selectProgram(data:any, i:number){
    this.selectedProgram=data;
    this.HighLightRow2=i;
  }
  AllocateProgram(data:any, i:number){
    
    this.selectedProgram=data;
    this.modalOpen=false;
    this.openServiceAgreementModal();
    
  //   recordNumber: data.recordNumber,
  //   program: data.program,
  //   no: data.docNo,
   // this.router.navigate(['admin/recipient/quotes'], { state: { data: data } });
  }
  selectService(data:any, i:number){
    this.selectedServiceAgreement=data;
    this.HighLightRow2=i;
  }
  AllocateServiceAgreeement(data:any, i:number){
    this.selectedServiceAgreement=data;
    this.option = 'add';
      // Usage example
      if(!data.exists){
        this.globalS.eToast('Error', "Document can't be found");
        return;
    }

    this.modalOpen2 = false;
    this.newQuoteModal=true;

    this.record  =  {    
                recordNumber: data.recordNumber,
                program: this.selectedProgram,
                template: data.title,
                docNo: 0,
                templateFile : data.template,
                maingroup : data.mainGroup,
                docID : 0,
                plan : 'CP_QUOTE',
                }   
// this.router.navigate(['admin/recipient/quotes'], { state: { data: data } });
}
  refreshQuote(data: any){
    if(data){
        console.log('refresh')
       // this.search();
    }
}

onKeyPress(event: KeyboardEvent): void {
  if (event.key === 'ArrowDown' || event.key === 'ArrowUp') {
    event.preventDefault();
  }
  // switch (event.key) {
  //   case 'ArrowDown':
  //     this.selectedIndex = (this.selectedIndex + 1) % this.lists.length;
  //     break;
  //   case 'ArrowUp':
  //     this.selectedIndex = (this.selectedIndex - 1 + this.lists.length) % this.lists.length;
  //     break;
  //   case 'Enter':
  //     if (this.selectedIndex >= 0) {
  //       alert(`Selected: ${this.lists[this.selectedIndex]}`);
  //     }
  //     break;
  // }
}

txtSearch:string;


onTextChangeEvent(event:any, listTye:any){
  // console.log(this.txtSearch);
   let value = this.txtSearch.toUpperCase();

   if (listTye=='Program'){
   this.alist=this.alist_original.filter(element=>element.program.includes(value));

}else   if (listTye=='Service'){
  this.alist2=this.alist2_original.filter(element=>element.title.includes(value));

}
}
emitEvent(event: any) {

  switch(event){
    case 'New' :
      this.newReferralModal = true;
      break;
    case 'Save' :
      this.SaveAll_Changes();
      break;
      case 'Change' :
      this.openRecipientchangeCodeModel();
      break;
    case 'Delete' :
      this.showConfirm();
      break;
    case 'export' :
     // this.print();
      break;
    case 'New Referral' :
      if ( this.AddNewReferralDone){
        this.LoadReferalModel();
        this.AddNewReferralDone=false;
        
      }
        
         break;
  }

}
prntSearchList(){

}
onRecipientSearch(data:any){
  this.findModalOpen=false;
  this.selectRecipient(data);
  this.AllocateRecipient(this.selectRecipient)
 
}

isDropdownVisible:boolean;
showRecipientAlert:boolean=false;
recipientAlerts:Array<any>=[];

showRecipientAlerts(){
  
  this.showRecipientAlert = !this.showRecipientAlert;
  this.isDropdownVisible=true;
}
onDropdownVisibleChange(visible: boolean): void {
  this.isDropdownVisible = visible; // Update visibility when dropdown state changes
}
onCloseClick(event: MouseEvent): void {
  event.stopPropagation(); // Prevent dropdown or parent from intercepting the click
  this.isDropdownVisible=false;
  this.showRecipientAlert = false; // Execute your logic
}

ngModelQChange(event:any){
 
  this.QuicksearchOptions.push(event);
}
 onSearch(value: string): void {
  this.QuicksearchValue=value
    // You can implement search logic here if needed
  }
  ExpandAlerts:boolean=false;
  ExpandAlertsChecked(){

  }
  previous_tab_link:any;
  previous_tab:any;
  onHover(isHovered: boolean): void {
    
    if (isHovered) {
     // console.log('Hovered over Clinical submenu!');
      this.previous_tab_link = this.router.url;
      this.previous_tab = this.current_tab;
      this.tab('Clinical Detail');    
      if (this.previous_tab != 'Personal'){
        this.newLink.next('/admin/recipient/clinical');

    
  
      if (this.previous_tab == 'Personal'){
     
        this.router.navigate(['/admin/recipient/personal']);
      }

    // this.router.navigate([this.previous_tab_link]);
    
    }  else{
       if (this.previous_tab != '' ){
        this.tab(this.previous_tab);       
        this.router.navigate([this.previous_tab_link]);
      } else{
        this.router.navigate(['/admin/recipient/personal']);
      }
        
        
    }
  }

}
  processlink(value: string): void {
    this.router.navigate( [value]);
  }

  activationDone(){
  
        let sql :any= {TableName:'',Columns:'',ColumnValues:'',SetClause:'',WhereClause:''};
              
        sql.TableName='Recipients '; 
        sql.SetClause=`SET [Type] = 'RECIPIENT', DischargeDate = Null, DischargedBy = Null, AdmissionDate = convert(varchar,getDate(),111), AdmittedBy = '${this.tocken.user}', ExcludeFromRosterCopy = 1 `;
        sql.WhereClause=` WHERE UniqueID = '${this.user.id}' `;
  
         this.listS.updatelist(sql).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
          this.globalS.sToast('Recipient','Recipient Deactivated');
      })
    }
    openRecipientchangeCodeModel(){
      this.RecipientCode = this.user.code;
      this.changeCodeOpen=true;
    }
    changeRecipientCode(){
      
      let sql=` select UniqueID from Recipients WHERE  [AccountNo]= '${this.RecipientCode}' `;

      this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
       if (data.length>0){
        this.globalS.iToast("Recipient", 'This code already exists')
        return
       }else{
        this.updateRecipientCode();
       }
       
   })
    }

    updateRecipientCode(){
      
      let input :any= {oldCode: this.user.code, newCode:this.RecipientCode};
      var notifId=this.globalS.loadingMessage('Updating Recipeint Code...');   
       this.listS.updateRecipientCode(input).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
        this.changeCodeOpen=false;
       
        this.msg.remove(notifId);
        this.changeCodeOpen=false;
        this.sample=this.RecipientCode;
        this.user.code = this.RecipientCode       
       
        this.sharedS.emitOnSearchListNext( this.RecipientCode)    
        this.SearchListComponent.searchRecipient();
        this.cd.detectChanges();
       
    })
    }

    hasHoldProgram(){
      
      let sql=` select RecordNumber from RecipientPrograms WHERE  [PersonID]= '${this.user.id}' AND ProgramStatus = 'ON HOLD' `;

      this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe$)).subscribe(data=>{
       if (data.length>0){
        this.recipientStatus ='ON HOLD'
        this.cd.detectChanges();
        return true
       }else{
        return false;
       }
       
   })
    }
   
}

