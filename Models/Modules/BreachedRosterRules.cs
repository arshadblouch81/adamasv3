using System;
namespace Adamas.Models.Modules{
    public class BreachedRosterRules{
       public string sMode { get; set; }
       public string sStaffCode { get; set; }
       public string sClientCode { get; set; }
       public string sProgram { get; set; }
       public string sDate { get; set; }                             
       public string sStartTime { get; set; }     
        public string sDuration { get; set; }                                    
        public string sActivity { get; set; }                                                                 
        public string sRORecordno { get; set; }    
        public string sState { get; set; }  
        public bool bEnforceActivityLimits { get; set; }  
         public bool bUseAwards { get; set; }                                          
        public bool bDisallowOT { get; set; }      
         public bool bDisallowNoBreaks { get; set; }                         
        public bool bDisallowConflicts { get; set; } 
        public bool bForceNote { get; set; }                                        
        public string sOldDuration { get; set; } 
        public string sExcludeRecords { get; set; } 
        public bool bSuppressErrorMessages { get; set; } 
        public string sStatusMsg { get; set; }                      
        public string PasteAction { get; set; }                      
									
    }
    }