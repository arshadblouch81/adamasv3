import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { UploadFileComponent } from './upload-file/upload-file.component'
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { NzUploadModule } from 'ng-zorro-antd/upload';

import { RemoveFirstLast, FilterPipe, KeyFilter, MomentTimePackage, KeyValueFilter, FileNameFilter, FileSizeFilter, MonthPeriodFilter,GetTextFromHtml, SplitArrayPipe, DayManagerPopulate } from '@pipes/pipes';
import { ProfileComponent } from './profile/profile.component'

import { NgSelectModule } from '@ng-select/ng-select';

import {DragDropModule} from '@angular/cdk/drag-drop';

import {
  SuburbComponent
} from '@components/index';

import {
  ClickOutsideDirective,
  NumberDirective
} from "../directives/index";

import { CalendarComponent } from './calendar/calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { HeaderNavComponent } from './header-nav/header-nav.component';
import { ActionComponent } from './action/action.component';
import { SearchListComponent } from './search-list/search-list.component';
import { ContactsDetailsComponent } from './contacts-details/contacts-details.component';
import { SearchTimesheetComponent } from './search-timesheet/search-timesheet.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { DmCalendarComponent } from './dm-calendar/dm-calendar.component';
import { RecipientPopupComponent } from './recipient-popup/recipient-popup.component';
import { PhonefaxComponent } from './phonefax/phonefax.component';
import { MediaComponent } from './media/media.component';
import { ImageCropperComponent } from './image-cropper/image-cropper.component';
import { AddReferralComponent } from './add-referral/add-referral.component';
import { ReferInComponent } from './refer-in/refer-in.component';
import { IntervalDesignComponent } from './interval-design/interval-design.component';
import { StaffPopupComponent } from './staff-popup/staff-popup.component';
import { LeaveApplicationComponent } from './leave-application/leave-application.component';
import { IncidentProfileComponent } from './incident-profile/incident-profile.component';
import { UploadSharedComponent } from './upload-shared/upload-shared.component';
import { MembersComponent } from './members/members.component';
import { IncidentPostComponent } from './incident-post/incident-post.component';
import { AddStaffComponent } from './add-staff/add-staff.component';
import { IncidentDocumentsComponent } from './incident-documents/incident-documents.component';
import { RecipientsOptionsComponent } from './recipients-options/recipients-options.component';
import { SelectListRecipientComponent } from './select-list-recipient/select-list-recipient.component';
import { FilterComponent } from './filter/filter.component';
import { IntervalQuoteComponent } from './interval-quote/interval-quote.component';
import { PrintPdfComponent } from './print-pdf/print-pdf.component';
import { AddQuoteComponent } from './add-quote/add-quote.component';
import { DexUploadComponent } from './dex-upload/dex-upload.component';
import { CdcClaimPreparationComponent } from './cdc-claim-preparation/cdc-claim-preparation.component';
import { CdcClaimUpdateComponent } from './cdc-claim-update/cdc-claim-update.component';
import { CdcProdaClaimUpdateComponent } from './cdc-proda-claim-update/cdc-proda-claim-update.component';
import { NdiaClaimUpdateComponent } from './ndia-claim-update/ndia-claim-update.component';
import { ClosePayRosterComponent } from './pay-module/closePayRoster.component';
import { CloseBillingRosterComponent } from './billing-module/closeBillingRoster.component';
import { PayComponent } from './pay-module/pay.component';
import { PayIntegrityComponent } from './pay-module/payIntegrity.component';
import { BillingIntegrityComponent } from './billing-module/billingIntegrity.component';
import { BulkPriceUpdateComponent } from './billing-module/bulkPriceUpdate.component';
import { PrintEmailInvoicesComponent } from './billing-module/printEmailInvoices.component';
import { PayPeriodDateComponent } from './pay-module/payPeriodDate.component';
import { RollbackPayrollComponent } from './pay-module/rollbackPayroll.component'; 
import { RollbackHcpClaimPrepComponent } from './hcp-module/rollbackHcpClaimPrep.component';
import { RollbackHcpAgingComponent } from './hcp-module/rollbackHcpAging.component';
import { RollbackHcpClaimUploadComponent } from './hcp-module/rollbackHcpClaimUpload.component';
import { SetHcpRatesComponent } from './hcp-module/setHcpRatesList.component';
import { HcpCdcClaimRateUpdateComponent } from './hcp-module/hcpCdcClaimRateUpdate.component';
import { HCPBulkBalanceEditComponent } from './hcp-module/hcpBulkBalanceEdit.component';
import { HCPDataCheck } from './hcp-module/hcpDataCheck.component';
import { HCPPackageAuditReport } from './hcp-module/hcpPkgAuditReport.component';
import { HCPPackageStatusSummary } from './hcp-module/hcpPkgStatusSummary.component';
import { HCPCreateClaimUpload } from './hcp-module/hcpCreateClaimUpload.component';
import { HCPClaimPreparation } from './hcp-module/hcpClaimPreparation.component';
import { HCPImportPriceGuide } from './hcp-module/hcpImportPriceGuide.component';
import { HCPBalanceExport } from './hcp-module/hcpBalanceExport.component';
import { NDIACreateClaimUpload } from './ndia-module/ndiaCreateClaimUpload.component';
import { NDIAImportPriceGuide } from './ndia-module/ndiaImportPriceGuide.component';
import { RollbackNdiaAgingComponent } from './ndia-module/rollbackNdiaAging.component';
import { RollbackNdiaClaimComponent } from './ndia-module/rollbackNdiaClaim.component';
import { CreateNDIAInvoiceComponent } from './ndia-module/createNDIAInvoice.component';
import { AgeMonthlyBalancesComponent } from './ndia-module/ageMonthlyBalances.component';
import { AwardListComponent } from './award-setup/awardList.component';
import { AwardSetupComponent } from './award-setup/awardSetup.component';
import { TravelComponent } from './pay-module/travel.component';
import { ImportReceiptsComponent } from './billing-module/importReceipts.component';
import { DebtorComponent } from './billing-module/debtor.component';
import { RollbackInvoiceComponent } from './billing-module/rollbackInvoice.component';
import { RollbackRosterComponent } from './billing-module/rollbackRoster.component';
import { ClientHoursComponent } from './billing-module/clientHours.component';
import { WorkHoursComponent } from './pay-module/workHours.component';
import { GlobalAlertsComponent } from './global-alerts/global-alerts.component';
import { WorkflowsComponent } from './workflows/workflows.component';
import { SearchListTwoComponent } from './search-list-two/search-list-two.component';
import { PackageView } from './package/packageview';
import { GraphComponent } from './graph/graph';
import { NeedsRisksComponent } from './needsrisks/needsrisks';
import { DischargeProgram } from './discharge-program/discharge';
import {AppDocument} from './document/app-document';
import {RecipientSearch} from './recipient-search/recipient-search';


@NgModule({
  declarations: [
    UploadFileComponent,

    //Directives,
    ClickOutsideDirective, NumberDirective,

    RemoveFirstLast, FilterPipe, KeyFilter, MomentTimePackage, KeyValueFilter, FileNameFilter, FileSizeFilter, MonthPeriodFilter,GetTextFromHtml,SplitArrayPipe, DayManagerPopulate,
    ProfileComponent,
    SuburbComponent,
    CalendarComponent,
    HeaderNavComponent,    
    ActionComponent,
    SearchListComponent,
    ContactsDetailsComponent,
    SearchTimesheetComponent,
    BreadcrumbsComponent,
    DmCalendarComponent,
    RecipientPopupComponent,
    PhonefaxComponent,
    MediaComponent,
    ImageCropperComponent,
    AddReferralComponent,
    ReferInComponent,
    IntervalDesignComponent,
    StaffPopupComponent,
    LeaveApplicationComponent,
    IncidentProfileComponent,
    UploadSharedComponent,
    MembersComponent,
    IncidentPostComponent,
    AddStaffComponent,
    IncidentDocumentsComponent,
    RecipientsOptionsComponent,
    SelectListRecipientComponent,
    FilterComponent,
    IntervalQuoteComponent,
    PrintPdfComponent,
    AddQuoteComponent,
    DexUploadComponent,
    CdcClaimPreparationComponent,
    CdcClaimUpdateComponent,
    CdcProdaClaimUpdateComponent,
    NdiaClaimUpdateComponent,
    ClosePayRosterComponent,
    CloseBillingRosterComponent,
    PayComponent,
    PayIntegrityComponent,
    BillingIntegrityComponent,
    BulkPriceUpdateComponent,
    PrintEmailInvoicesComponent,
    PayPeriodDateComponent,
    RollbackPayrollComponent,
    RollbackHcpClaimPrepComponent,
    RollbackHcpAgingComponent,
    RollbackHcpClaimUploadComponent,
    SetHcpRatesComponent,
    HcpCdcClaimRateUpdateComponent,
    HCPBulkBalanceEditComponent,
    HCPDataCheck,
    HCPPackageAuditReport,
    HCPPackageStatusSummary,
    HCPCreateClaimUpload,
    HCPClaimPreparation,
    HCPImportPriceGuide,
    HCPBalanceExport,
    NDIACreateClaimUpload,
    NDIAImportPriceGuide,
    RollbackNdiaAgingComponent,
    RollbackNdiaClaimComponent,
    CreateNDIAInvoiceComponent,
    AgeMonthlyBalancesComponent,
    AwardListComponent,
    AwardSetupComponent,
    TravelComponent,
    ImportReceiptsComponent,
    DebtorComponent,
    RollbackInvoiceComponent,
    RollbackRosterComponent,
    ClientHoursComponent,
    WorkHoursComponent,
    GlobalAlertsComponent,
    WorkflowsComponent,
    SearchListTwoComponent,
    PackageView,
    GraphComponent,
    NeedsRisksComponent,
    DischargeProgram,
    AppDocument,
    RecipientSearch
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgZorroAntdModule,
    NzUploadModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    DragDropModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports: [
    UploadFileComponent,
    MonthPeriodFilter,
    GetTextFromHtml,
    SuburbComponent,
    ProfileComponent,
    HeaderNavComponent,
    CalendarComponent,
    ActionComponent,
    SearchListComponent,
    ContactsDetailsComponent,
    SearchTimesheetComponent,
    BreadcrumbsComponent,
    DmCalendarComponent,
    DayManagerPopulate,
    RecipientPopupComponent,
    MediaComponent,
    ImageCropperComponent,
    AddReferralComponent,
    IntervalDesignComponent,
    StaffPopupComponent,
    LeaveApplicationComponent,
    IncidentProfileComponent,
    UploadSharedComponent,
    MembersComponent,
    IncidentPostComponent,
    AddStaffComponent,
    IncidentDocumentsComponent,
    RecipientsOptionsComponent,
    NgSelectModule,
    FilterComponent,
    IntervalQuoteComponent,
    AddQuoteComponent,
    DexUploadComponent,
    CdcClaimPreparationComponent,
    CdcClaimUpdateComponent,
    CdcProdaClaimUpdateComponent,
    NdiaClaimUpdateComponent,
    ClosePayRosterComponent,
    CloseBillingRosterComponent,
    PayComponent,
    PayIntegrityComponent,
    BillingIntegrityComponent,
    BulkPriceUpdateComponent,
    PrintEmailInvoicesComponent,
    PayPeriodDateComponent,
    RollbackPayrollComponent,
    RollbackHcpClaimPrepComponent,
    RollbackHcpAgingComponent,
    RollbackHcpClaimUploadComponent,
    SetHcpRatesComponent,
    HcpCdcClaimRateUpdateComponent,
    HCPBulkBalanceEditComponent,
    HCPDataCheck,
    HCPPackageAuditReport,
    HCPPackageStatusSummary,
    HCPCreateClaimUpload,
    HCPClaimPreparation,
    HCPImportPriceGuide,
    HCPBalanceExport,
    NDIACreateClaimUpload,
    NDIAImportPriceGuide,
    RollbackNdiaAgingComponent,
    RollbackNdiaClaimComponent,
    CreateNDIAInvoiceComponent,
    AgeMonthlyBalancesComponent,
    AwardListComponent,
    AwardSetupComponent,
    TravelComponent,
    ImportReceiptsComponent,
    DebtorComponent,
    RollbackInvoiceComponent,
    RollbackRosterComponent,
    ClientHoursComponent,
    WorkHoursComponent,
    ClickOutsideDirective,
    GlobalAlertsComponent,
    WorkflowsComponent,
    SearchListTwoComponent,
    PackageView,
    GraphComponent,
    NeedsRisksComponent,
    DischargeProgram,
    AppDocument,
    RecipientSearch
  ],
  providers: [
    
  ]
})
  
export class ComponentModule { }
