﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class GoalsStrategiesDto
    {
        public int Recordnumber { get; set; }
        public DateTime? LastReviewed { get; set; }
        public DateTime? Anticipated { get; set; }
        public DateTime? DateArchived { get; set; }
        public string Percent { get; set; }
        public string Goal { get; set; }
        public string Notes { get; set; }
        public string Level { get; set; }
        public bool? Completed { get; set; }
        public List<string> Strategies { get; set; }
    }
}
