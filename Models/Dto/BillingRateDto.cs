﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class BillingRateDto
    {
        public int Precedence { get; set; }
        public decimal Rate { get; set; }
        public string Unit { get; set; }
        public string Debtor { get; set; }
        public string Tax { get; set; }
    }
}
