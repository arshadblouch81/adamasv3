using System;
namespace Adamas.Models.Modules{
    public class ChargeTypeInput
    {
        public int Index { get; set; }
        public string Program { get; set; }
    }
}