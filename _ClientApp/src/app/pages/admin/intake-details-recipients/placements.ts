import { Component, OnInit,OnDestroy } from '@angular/core'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService } from '@services/timesheet.service'
import { GlobalService } from '@services/global.service'

@Component({
    selector: '',
    templateUrl:'./placements.html'
})

export class IntakePlacements{
    private placements$: Subscription;
    placements: Array<any>;
    loading: boolean;
    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private router: Router,
            private globalS: GlobalService
        ){
            this.placements$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'placements')){
                    this.search();
                }
            });
    }

    ngOnInit(){
        this.search();
    }

    ngOnDestroy(){
        this.placements$.unsubscribe();
    }

    search(){
        const recipient = this.sharedS.getPicked();         
        this.loading = true;
        this.timeS.getplacements(recipient.uniqueID).subscribe(placements => {
            this.loading = false;
            this.placements = placements
        })
    }
}