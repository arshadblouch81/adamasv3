using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.Function;
using Microsoft.Extensions.Configuration;
using Staff = Adamas.Models.Modules.Staff;

using MimeKit;
using MimeKit.Text;
using Newtonsoft.Json.Linq;

using Adamas.Model.Report;
using jsreport.AspNetCore;
using jsreport.Types;
using System.IO;

using jsreport.AspNetCore;
using jsreport.Binary;
using jsreport.Local;



namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientReportController : Controller
    {
        public IJsReportMVCService JsReportMVCService { get; }
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        public ClientReportController(
            IJsReportMVCService jsReportMVCService,
            IGeneralSetting setting,
            DatabaseContext context)
        {
            _context = context;
            GenSettings = setting;
            JsReportMVCService = jsReportMVCService;
        }

        public IActionResult Index()
        {
            return View();
        }

        // [HttpGet("invoice")]
        // [MiddlewareFilter(typeof(JsReportPipeline))]
        // public IActionResult Invoice([FromQuery(Name = "name")] string name)
        // {
        //     var sample = name;
        //     HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf);
        //     return View(InvoiceModel.Example());
        // }

        [HttpPost("invoice")]
        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> StaffListing(StaffListingModel data)
        {
            HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf)
                        .Configure(req => req.Template.Chrome = new Chrome { 
                            Landscape = true,
                            MarginTop = "2cm",
                            MarginLeft = "1cm",
                            MarginBottom = "1cm",
                            MarginRight = "1cm"
                        });
            
            StaffListingModel staff = new StaffListingModel();
            staff.CompanyName = data.CompanyName;
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT UniqueID, AccountNo, STF_CODE, StaffGroup, [LastName], UPPER(Staff.[LastName]) + ', ' + CASE WHEN FirstName <> '' THEN FirstName  ELSE ' '  END as StaffName, Address1, Address2, Suburb, Postcode, CommencementDate, TerminationDate, HRS_DAILY_MIN, HRS_DAILY_MAX, HRS_WEEKLY_MIN, HRS_WEEKLY_MAX
                    FROM Staff WHERE [Category] = 'STAFF'
                    AND ([commencementdate] is not null and [terminationdate] is null)
                    ORDER BY Staff.[LastName], Staff.[FirstName]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<StaffList> list = new List<StaffList>();
                        while(await rd.ReadAsync())
                        {
                            list.Add(new StaffList(){
                                Name = GenSettings.Filter(rd["AccountNo"]),
                                StartDate = ((DateTime) GenSettings.Filter(rd["CommencementDate"])).ToString("dd/MM/yyyy"),
                                Phone = "",
                                Address = GenSettings.Filter(rd["Address1"]),
                                DMax = GenSettings.Filter(rd["HRS_DAILY_MAX"]),
                                DMin = GenSettings.Filter(rd["HRS_DAILY_MIN"]),
                                WMax = GenSettings.Filter(rd["HRS_WEEKLY_MAX"]),
                                WMin = GenSettings.Filter(rd["HRS_WEEKLY_MIN"])
                            });
                        }
                        staff.Data = list;
                    }
                }
            }

            return View(staff);
        }

        [HttpPost("staff-listing")]
        // [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> StaffListing([FromBody] StaffList list)
        {
            var rs = new LocalReporting().UseBinary(JsReportBinary.GetBinary()).AsUtility().Create();


            //HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf);
            // return View(list);

            // var report  = await rs.RenderByNameAsync("Invoice", InvoiceModel.Example());

            var report = await rs.RenderAsync(new RenderRequest()
            {
                Template = new Template()
                {
                    Recipe = Recipe.ChromePdf,
                    Engine = Engine.None,                    
                    Content = "Hello from pdf"
                }
            });

            var memoryStream = new MemoryStream();
            await report.Content.CopyToAsync(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);
            return File(memoryStream, "application/pdf", "out.pdf");

            // return new FileStreamResult(memoryStream, "application/pdf") { FileDownloadName = "out.pdf" };
        }

        [HttpGet("invoice-download")]
        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> InvoiceDownload()
        {
            // HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf)
            //     .OnAfterRender((r) => HttpContext.Response.Headers["Content-Disposition"] = "attachment; filename=\"myReport.pdf\"");
            
            var rs = new LocalReporting().UseBinary(JsReportBinary.GetBinary()).AsUtility().Create();
            var report = await rs.RenderAsync(new RenderRequest()
            {
                Template = new Template()
                {
                    Recipe = Recipe.ChromePdf,
                    Engine = Engine.None,
                    Content = "Hello from pdf"
                }
            });
            var memoryStream = new MemoryStream();
            await report.Content.CopyToAsync(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(memoryStream, "application/pdf") { FileDownloadName = "out.pdf" };
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> InvoiceWithHeader()
        {
            var header = await JsReportMVCService.RenderViewToStringAsync(HttpContext, RouteData, "Header", new { });

            HttpContext.JsReportFeature()
                .Recipe(Recipe.ChromePdf)
                .Configure((r) => r.Template.Chrome = new Chrome
                {
                    HeaderTemplate = header,
                    DisplayHeaderFooter = true,
                    MarginTop = "1cm",
                    MarginLeft = "1cm",
                    MarginBottom = "1cm",
                    MarginRight = "1cm"
                });

            return View("Invoice", InvoiceModel.Example());
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult Items()
        {
            //HttpContext.JsReportFeature()
            //    .Recipe(Recipe.HtmlToXlsx)
            //    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });

            HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf);
            return View(InvoiceModel.Example());
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult Products()
        {
            //HttpContext.JsReportFeature()
            //    .Recipe(Recipe.HtmlToXlsx)
            //    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });

            HttpContext.JsReportFeature().Recipe(Recipe.ChromePdf);
            return View(ProductModel.Example());
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult ItemsExcelOnline()
        {
            HttpContext.JsReportFeature()
                .Configure(req => req.Options.Preview = true)
                .Recipe(Recipe.HtmlToXlsx)
                .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });

            return View("Items", InvoiceModel.Example());
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult InvoiceDebugLogs()
        {
            HttpContext.JsReportFeature()
                .DebugLogsToResponse()
                .Recipe(Recipe.ChromePdf);

            return View("Invoice", InvoiceModel.Example());
        }
    }
}
