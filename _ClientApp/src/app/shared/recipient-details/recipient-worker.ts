
import {forkJoin,  Observable } from 'rxjs';
import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { ClientService } from '@services/index'

@Component({
    selector: 'recipient-worker',
    templateUrl: './recipient-worker.html',
    styles: [`
    .table-body-container {
        overflow-y: scroll;
        max-height: 9rem;
        font-size: 12px;
    }
    .table-toggle-wrapper div{
        font-size: 12px;
    }
    `],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RecipientWorkerComponent),
            multi: true
        }
    ]
})

export class RecipientWorkerComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    result: any;

    constructor(
        private clientS: ClientService
    ){ }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        let workerInput: Dto.WorkerInput = { 
            ClientCode: user.name, 
            StartDate: user.startDate, 
            EndDate: user.endDate 
        }

        forkJoin([
            this.clientS.getrostermaster(workerInput),
            this.clientS.getrosterworker(workerInput)
        ]).subscribe(data => {
            this.result = {
                permStaff: data[0],
                rosterStaff: data[1]
            }
            console.log(this.result)
        })
    }
}