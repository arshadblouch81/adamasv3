namespace Adamas.Models.Modules{
    public class UserSettings {
        public int Recnum { get; set; }
        public int Recipients { get; set; }
        public string AllowSetTime { get; set; }
        public string TMMode { get; set; }
        public string MobileFutureLimit { get; set; }
        public string AllowPicUpload { get; set; }
        public string Apply_Goe_Location_Setting { get; set; }
        public string KMAgainstTravelOnly { get; set; }
        public string mobilegeocodelimit { get; set; }      
        public string StaffLocationUpdateInterval { get; set; }
        public string AllowIncidentEntry { get; set; }
        public string AllowTravelEntry { get; set;}
        public string AllowClientNoteEntry { get; set; }
        public string AllowRosterNoteEntry { get; set; }
        public string StaffCode { get; set; }
        public string AllowRegisterSign { get; set; }
        public string ToDate { get; set; }
        public string Time { get; set; }
        public string UserSessionLimit { get; set; }
        public string Enable_Shift_End_Alarm { get; set; }
        public string ShowClientPhoneInApp { get; set; }
        public string TA_TRAVELDEFAULT { get; set; }
        public string CheckAlertInterval { get; set; }
        public string AllowOPNote { get; set; }
        public string AllowCaseNote { get; set; }
        public string AllowIncidentNote { get; set; }
        public string AppUsesSMTP { get; set; }
        public string RosterRequested { get; set; }
        public string AllowLeaveEntry { get; set; }
        public string EnableRosterDelivery { get; set; }
        public string MinimumInternetSpeedForOnline { get; set; }
        public string GeolocateEnabled { get; set; }
        public string AllowViewBookings { get; set; }
        public string EnableViewNoteCases { get; set; }
        public string HideGeolocation { get; set; }
        public string Enable_Shift_Start_Alarm { get; set; }
        public string BookingLeadTime { get; set; }
        public string ShortNoticeThreshold { get; set; }
        public string NoNoticeThreshold { get; set; }
        public string CancellationFeeText { get; set; }
        public string AllowBooking { get; set; }
        public string CanChooseProvider { get; set; }
        public string CanSeeProviderReviews { get; set; }
        public string CanEditProviderReviews { get; set; }
        public string CanSeeProviderPhoto { get; set; }
        public string CanSeeProviderGender { get; set; }        
        public string CanSeeProviderAge { get; set; }
        public string CanManagePreferences { get; set; }       
        public string CanManageServices { get; set; }
        public string CanCancelService { get; set; }
        public string CanQueryService { get; set; }
        public string ViewPackageStatement { get; set; }
        public string AllowsMarketing { get; set; }
        public string MinimumCancellationLeadTime { get; set; }
        public string financial { get; set; }
        public string UseAwards { get; set; }
        public bool? CloudAdmin { get; set; }
        public bool? FullAdmin  {get;set;}
        public string RecipientRecordView { get; set; }
        public string StaffRecordView { get; set; }

        public int MMPublishPrintRosters { get; set; }
        public int MMTimesheetProcessing { get; set; }
        public int MMBilling { get; set; }
        public int MMPriceUpdates { get; set; }
        public int MMDEXUploads { get; set; }
        public int MMNDIA { get; set; }
        public int MMHACC { get; set; }
        public int MMOtherDS { get; set; }
        public int MMAccounting { get; set; }
        public bool MMAnalyseBudget { get; set; }
        public bool MMAtAGlance { get; set; }
        public bool AccessCDC { get; set; }
        public int? System { get; set; }

        public  string AllowcaseNote   { get; set; }
        public  string EnableViewNotecases   { get; set; }
         public  string WizardDefaultNote   { get; set; }

    }
}