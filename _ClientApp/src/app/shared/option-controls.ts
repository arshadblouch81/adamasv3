import { Component, Output, EventEmitter } from '@angular/core'

@Component({
    selector: 'option-controls',
    templateUrl: './option-controls.html',
    styles:[
        `
        ul{
            list-style:none;
            margin: 12px 0;
        }
        ul li{
            display:inline;
        }
        ul li i{
            padding: 5px;
            margin-right: 15px;
            border-radius: 10px;            
            cursor:pointer;
            transition: 0.2s;
        }
        .add:hover{
            background: #59ce82;
            color: #fff;
        }
        .update:hover{
            background: #c7ad4d;
            color: #fff;
        }
        .erase:hover{
            background: #f36e6e;
            color: #fff;
        }
        
        `
    ]
})

export class OptionControlComponent {
    @Output() clickEmit: EventEmitter<any> = new EventEmitter();

    add(){
        this.clickEmit.emit('add');
    }

    edit(){
        this.clickEmit.emit('edit');
    }

    delete(){
        this.clickEmit.emit('delete');
    }
}