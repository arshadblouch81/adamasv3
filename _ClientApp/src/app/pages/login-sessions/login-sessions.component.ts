import { Component, OnInit } from '@angular/core';
import { CacheService, GlobalService } from '../../services/index';

@Component({
  selector: 'app-login-sessions',
  templateUrl: './login-sessions.component.html',
  styleUrls: ['./login-sessions.component.css']
})
export class LoginSessionsComponent implements OnInit {

  source: any = null;
  users: Array<any>;
  loading: boolean = false;

  constructor(
    private cacheS: CacheService,
    private globalS: GlobalService
  ) { }

  ngOnInit(){
    this.getUsers();
  }

  getUsers(){
      this.loading = true;
      this.cacheS.getusers()
          .subscribe(data => {            
              this.users = data.map(x => {
                  var token = this.globalS.decode(x.value);
                  return {
                      key: x.key,
                      token
                  };
              });
              this.loading = false;
          });
  }

  logout(index: number){
      var id = this.users[index].key;

      this.cacheS.deleteuser(id)
          .subscribe(data => {
              if(data){
                  this.globalS.sToast('Success','User has been logged out')
                  this.getUsers();
              }
          })

  }
}
