using System;

namespace Adamas.Models.Modules
{
    public class TravelUpdate
    {
        public string Group { get; set; }
        public string Branches { get; set; }
        public string Programs { get; set; }
        public string Categories { get; set; }
        public string Staffs { get; set; }
        public string Fundings { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }

    }
}