
import { takeUntil, switchMap } from 'rxjs/operators';
import {
    Component,
    OnInit,
    HostListener,
    OnDestroy
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService, GlobalService, StaffService } from '../../services/index';
//import * as CryptoJS from 'crypto-js';
import { Subject } from 'rxjs'
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    templateUrl: './login.html',
    styles: [`
    .logo{
        background:url(./assets/logo/header-logo.png) no-repeat;
        background-size: contain;
        height: 2rem;
        width: 100%;
    }
    .back-bg{
        position: absolute;
        height: 100%;
        width: 100%;
        background: linear-gradient(#0079b87a, #639ab7c7, #0079b88a), url(./assets/bg.jpg) no-repeat center;
        background-size: cover;  
    }
    .login-wrapper{
        background:linear-gradient(rgb(25, 164, 234), rgb(0, 82, 124)) !important;
    }
    .login-wrapper .login{
        z-index:1;
    }
    .fb-logo{
        background-position: -2px -2px !important;
    }
    .twitter-logo{        
        background-position: -2px -59px !important;
    }
    .youtube-logo{
        background-position: -2px -31px !important;
    }
    .socials-logo{
        background: url(./assets/logo/social.png) no-repeat;
        width: 30px;
        height: 30px;
        background-size: 4rem;
    }
    .socials{
        display: flex;
        justify-content: space-around;
        margin-top: 1rem;
    }
    `]
})

export class LoginComponent implements OnInit, OnDestroy {
    private unsubscribe$ = new Subject()
    loading: boolean = false;
    unauthorized: boolean = false;
    unauthorizedStr: string;

    incomplete: boolean = false;
    timeout: any;

    ugroup: FormGroup;
    users: Array<any> = [
        { id: 1, name: 'Client' },
        { id: 2, name: 'Provider' },
        { id: 3, name: 'Administrator' }
    ]

    _users: any = "";

    @HostListener('document:keydown', ['$event'])
    onKeyDown(event: KeyboardEvent) {
        if (event.keyCode == 65 && event.shiftKey && event.ctrlKey) {
            var phrase = prompt("Enter phrase");
            var pass = prompt("Enter key")

            // if(CryptoJS.HmacSHA1(phrase,pass).toString() === '97702959ed699db9b850a607b97f1ec773bea6f5'){
            //     this.logS.testservice().pipe(
            //         takeUntil(this.unsubscribe$))
            //         .subscribe(data => {
            //             if(data > 0){
            //                 this.globalS.sToast('Success', 'Connected')
            //                 return;
            //             }                        
            //         }, error => {
            //             this.globalS.eToast('Error','Not Connected')
            //         })
            // } 
        }
    }


    constructor(
        private logS: LoginService,
        private globalS: GlobalService,
        private staffS: StaffService
    ) { }

    ngOnInit() {
        this.ugroup = new FormGroup({
            roles: new FormControl('', [Validators.required]),
            user: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required])
        });
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    login(data: FormGroup) {

        let ugroup = data.controls;
        if (this.globalS.isVarNull(ugroup.user.value) || this.globalS.isVarNull(ugroup.password.value)) {
            clearTimeout(this.timeout);
            this.incomplete = true;
            this.timeout = setTimeout(() => {
                this.incomplete = false;
            }, 3000);
            return false;
        }

        let user: Dto.ApplicationUser = {
            Username: ugroup.user.value,
            Password: ugroup.password.value
        }

        this.loading = true;
        this.unauthorized = false;
        this.logS.login(user)
            .pipe(
                switchMap((user: any) => {
                    this.globalS.token = user.access_token;
                    return this.staffS.getsettings(user.user);
                })
            ).subscribe(settings => {
                this.globalS.userSettings = settings;

                setTimeout(() => {
                    this.globalS.viewRender(this.globalS.token);
                }, 400);

                this.reset();
            }, (error: HttpErrorResponse) => {
                this.unauthorized = true;
                if (error.status == 401) this.unauthorizedStr = 'Invalid user name or password';
                if (error.status == 400) this.unauthorizedStr = error.error.message;
                this.reset();
                throw (error);
            });
    }

    reset() {
        this.loading = false;
        this.ugroup.reset();
        setTimeout(() => {
            this.unauthorized = false;
        }, 2000);
    }

}
