import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListService, MenuService, NavigationService, PrintService } from '@services/index';
import { GlobalService,notificationTypes } from '@services/global.service';
import { takeUntil, switchMap } from 'rxjs/operators';
import { Subject, EMPTY } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notificationlist',
  templateUrl: './notificationlist.component.html',
  styles:[`
  .mrg-btm{
    margin-bottom:0rem !important;
  }
  span.small-font{
    font-size:12px;
  }
  `]
})
export class NotificationlistComponent implements OnInit {
  events: Array<any>;
  
  tableData: Array<any>;
  staff:Array<any>;
  listType:Array<any>;
  program:Array<any>;
  recipients:Array<any>;
  locations:Array<any>;
  services:Array<any>;
  branches: Array<any>;
  coordinators: Array<any>;
  severity:Array<any>;
  checked = true;
  checked2=true;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  heading:string = "Add Notification List";
   
  token:any;
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;  
  dateFormat: string = 'dd/MM/yyyy';
  check : boolean = false;
  userRole:string="userrole";
  whereString :string="WHERE ListName IN ('ADMISSION NOTIFICATION','DOCUSIGN','EVENT','INCIDENT','INCIDENT CLOSE','INCIDENT FINALISE','REFERRAL NOTIFICATION','STAFFONBOARD NOTIFICATION') AND ISNULL(xDeletedRecord,0) = 0 AND (xEndDate Is Null OR xEndDate >= GETDATE()) ";
  staffList: any;
  allStaff:boolean = false;
  allstaffIntermediate: boolean = false;
  selectedStaff: any[] = [];
  funding_source: any;
  
  constructor(
    private globalS: GlobalService,
    private cd: ChangeDetectorRef,
    private listS:ListService,
    private menuS:MenuService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private printS:PrintService,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private navigationService: NavigationService,
    private router: Router,
    private ModalS: NzModalService
    ){}
    private unsubscribe: Subject<void> = new Subject();
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.populateDropdowns();
      this.loadData();
      this.loading = false;
      this.cd.detectChanges();
    }
    fetchAll(e){
      if(e.target.checked){
        this.loadData(true);
      }else{
        this.loadData();
      }
    }
    loadData(isChecked: boolean = false){
      this.loading = true;
      this.listS.getnotificationslist(isChecked).subscribe(data => {
        this.tableData = data;
        this.loading = false;
      })
    }
    populateDropdowns(){
      this.listType = notificationTypes;
      
      this.menuS.workflowstafflist().subscribe(data  =>  {this.staffList   = data});

      this.listS.getfundingsource().subscribe(data => this.funding_source = data);

      let branchSql  = "SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'BRANCHES' ORDER BY Description";
      this.listS.getlist(branchSql).subscribe(data => {
        this.branches = data;
        let da ={
          "description" :"ALL"
        };
        this.branches.unshift(da);
      });

      let coordinatorSql  = "SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'CASE MANAGERS' ORDER BY Description";
      this.listS.getlist(coordinatorSql).subscribe(data => {
        this.coordinators = data;
        let da ={
          "description" :"ALL"
        };
        this.coordinators.unshift(da);
      });

      let sql  = "select distinct [Title] from ItemTypes";
      this.listS.getlist(sql).subscribe(data => {
        this.services = data;
        let da ={
          "title" :"ALL"
        };
        this.services.unshift(da);
      });
      
      let staff_query = "SELECT distinct [AccountNo] as name from Staff Where AccountNo not like '!%' Order BY [AccountNo] ";
      this.listS.getlist(staff_query).subscribe(data => {
        this.staff = data;
        let da ={
          "name" :"ALL"
        };
        this.staff.unshift(da);
      });

      let prog = "SELECT [NAME] as name FROM HumanResourceTypes WHERE [GROUP] = 'PROGRAMS' AND ENDDATE IS NULL";
      this.listS.getlist(prog).subscribe(data => {
        this.program = data;
        let da ={
          "name" :"ALL"
        };
        this.program.unshift(da);
      });
      let loca = "select distinct [Name]  as name from CSTDAOutlets";
      this.listS.getlist(loca).subscribe(data => {
        this.locations = data;
        let da ={
          "name" :"ALL"
        };
        this.locations.unshift(da);
      });
      let recip = "select distinct [AccountNo] as name from Recipients Where AccountNo not like '!%'";
      this.listS.getlist(recip).subscribe(data => {
        this.recipients = data;
        let da ={
          "name" :"ALL"
        };
        this.recipients.unshift(da);
      });
      
      this.severity = ['ALL','LOW','MEDIUM','HIGH','CRITICAL'];
    }
    showAddModal() {
      this.heading = "Add Notification List"
      this.resetModal();

      this.inputForm.patchValue({
        ltype:'ALL',
        staff:'',
        service:'ALL',
        assignee:false,
        prgm:'ALL',
        location:'ALL',
        recepient:'ALL',
        funding_source:'ALL',
        saverity:'ALL',
        end_date:'',
        mandatory:false,
        recordNo:null,
        event: null,
        coordinator: 'ALL',
        branch: 'ALL'
      });

      this.modalOpen = true;
    }
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.heading  = "Edit Notification List"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      const { 
        ltype,
        staff,
        assignee,
        program,
        location,
        recipient,
        activity,
        funding_source,
        severity,
        mandatory,
        end_date,
        recordNo,
        branch,
        coordinator
      } = this.tableData[index];

       console.log(this.tableData[index]);
      // return;

      this.inputForm.patchValue({
        ltype:ltype,
        staff:staff,
        service:activity,
        assignee:(assignee == "True") ? true : false,
        prgm:program,
        location:location,
        recepient:recipient,
        funding_source:funding_source,
        saverity:severity,
        mandatory:mandatory,
        end_date: end_date == null ? null : new Date(end_date),
        recordNo:recordNo,
        branch: branch,
        coordinator:coordinator
      });
    }
    trueString(data: any): string{
      return data ? '1': '0';
    }
    isChecked(data: string): boolean{
      return '1' == data ? true : false;
    }
    loadTitle()
    {
      return this.heading;
    }
    
    handleCancel() {
      this.modalOpen = false;
      this.isUpdate  = false;
      this.staffList.forEach(x => {
        x.checked = false;
      });
      this.selectedStaff = [];
    }
    updateAllCheckedFilters(filter: any): void {
      this.selectedStaff = [];
      if (this.allStaff) {
        this.staffList.forEach(x => {
          x.checked = true;
          this.selectedStaff.push(x.staffCode);
        });
      }else{
        this.staffList.forEach(x => {
          x.checked = false;
        });
        this.selectedStaff = [];
      }
      console.log(this.selectedStaff);
    }
    updateSingleCheckedFilters(index:number): void {
      if (this.staffList.every(item => !item.checked)) {
        this.allStaff = false;
        this.allstaffIntermediate = false;
      } else if (this.staffList.every(item => item.checked)) {
        this.allStaff = true;
        this.allstaffIntermediate = false;
      } else {
        this.allstaffIntermediate = true;
        this.allStaff = false;
      }
    }
    log(event: any) {
      this.selectedStaff = event;
      console.log(this.selectedStaff);
    }
    pre(): void {
      this.current -= 1;
    }
    
    next(): void {
      this.current += 1;
    }
    save() {

      var { branch, coordinator } = this.inputForm.value;

      if(!this.isUpdate){        
        this.postLoading = true;   
        const group    = this.inputForm;
        let flag       = false;
        let _sql = "";

       
        if(this.selectedStaff.length > 0){

          this.selectedStaff.forEach(staf => {

            let ltype           = this.globalS.isValueNull(group.get('ltype').value);
            let _branch         = this.globalS.isValueNull(branch);
            let staff           = staf;
            let service         = this.globalS.isValueNull(group.get('service').value);
            let prgm            = this.globalS.isValueNull(group.get('prgm').value);
            let location        = this.globalS.isValueNull(group.get('location').value);
            let recepient       = this.globalS.isValueNull(group.get('recepient').value);
            let funding_source  = this.globalS.isValueNull(group.get('funding_source').value);
            let saverity        = this.globalS.isValueNull(group.get('saverity').value); 


            let mandatory     = this.trueString(group.get('mandatory').value);
            let assignee      = this.trueString(group.get('assignee').value);
            let end_date      = !(this.globalS.isVarNull(group.get('end_date').value)) ?  "'"+this.globalS.convertDbDate(group.get('end_date').value)+"'" : null;
            let values        = recepient+","+service+","+location+","+prgm+",'"+staff+"',"+mandatory+","+assignee+","+saverity+","+ltype+","+funding_source+","+end_date+","+_branch;
            let sql           = "insert into IM_DistributionLists([Recipient],[Activity],[Location],[Program],[Staff],[Mandatory],[DefaultAssignee],[Severity],[ListName],[ListGroup],[xEndDate],[Branch]) Values ("+values+");"; 
            _sql = sql;

            console.log(_sql);

            this.menuS.InsertDomain(_sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{

            _sql = "";

            });

          });
          
          flag = true;
          this.loadData();
          
        }
        else{
          this.globalS.sToast('Success', 'Select Atleast One Staff');
          return false;
        }
        this.globalS.sToast('Success', 'Saved successful');
        this.loading = true;  
        this.loadData();
        this.postLoading = false;          
        this.handleCancel();
        this.resetModal();
      } else{

        const group           = this.inputForm;
        let ltype             = this.globalS.isValueNull(group.get('ltype').value);
        let _branch           = this.globalS.isValueNull(branch);
        let staff             = this.globalS.isValueNull(group.get('staff').value);
        let service           = this.globalS.isValueNull(group.get('service').value);
        let prgm              = this.globalS.isValueNull(group.get('prgm').value);
        let location          = this.globalS.isValueNull(group.get('location').value);
        let recepient         = this.globalS.isValueNull(group.get('recepient').value);
        let funding_source    = this.globalS.isValueNull(group.get('funding_source').value);
        let saverity          = this.globalS.isValueNull(group.get('saverity').value);
        let mandatory         = this.trueString(group.get('mandatory').value);
        let assignee          = this.trueString(group.get('assignee').value);
        let end_date          = !(this.globalS.isVarNull(group.get('end_date').value)) ?  "'"+this.globalS.convertDbDate(group.get('end_date').value)+"'" : null;
        let recordNo          = group.get('recordNo').value;

        let sql  = "Update IM_DistributionLists SET [Recipient]="+ recepient + ",[Activity] ="+ service + ",[Program] ="+ prgm +",[Staff] ="+ staff+",[Severity] ="+ saverity +",[Mandatory] ="+ mandatory +",[DefaultAssignee] ="+ assignee +",[ListName] ="+ltype+",[xEndDate] = "+end_date+ ",[Location] ="+ location+ ",[ListGroup] ="+ funding_source+ ",[Branch]="+ _branch + " WHERE [recordNo] ='"+recordNo+"'";

        this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{        
          this.globalS.sToast('Success', 'Saved successful');
          this.loading = true;  
          this.loadData();
          this.postLoading = false;          
          this.handleCancel();
          this.resetModal(); 
        });
      }
    }
    
    delete(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.deleteDistributionlist(data.recordNo)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadData();
          return;
        }
      });
    }    
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activateDistributionlist(data.recordNo)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    } 
    buildForm() {
      this.inputForm = this.formBuilder.group({
        ltype:'ALL',
        staff:'',
        service:'ALL',
        assignee:false,
        prgm:'ALL',
        location:'ALL',
        recepient:'ALL',
        funding_source:'ALL',
        saverity:'ALL',
        end_date:'',
        mandatory:false,
        recordNo:null,
        event: null,
        coordinator: null,
        branch: null
      });
      
      this.inputForm.get('ltype').valueChanges
      .pipe(
        switchMap(x => {
          if(x != 'EVENT')
          return EMPTY;
          
          return this.listS.geteventlifecycle()
        })
        )
        .subscribe(data => {
          this.events = data;
        })
      }
      
      handleOkTop() {
        this.generatePdf();
        this.tryDoctype = ""
        this.pdfTitle = ""
      }
      handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
      }
      
      generatePdf(){
      
                    
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.tableData,                        
                "userid": this.tocken.user,
                "txtTitle":  "Notification List",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Notification List.pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }    
      goBack(): void {
        const previousUrl = this.navigationService.getPreviousUrl();
        const previousTabIndex = this.navigationService.getPreviousTabIndex();
      
        if (previousUrl) {
          this.router.navigate(['/admin/configuration'], {
            queryParams: { tab: previousTabIndex } // Pass tab index in query params
          });
        } else {
          this.router.navigate(['/admin/configuration'], {
            queryParams: { tab: previousTabIndex } // Pass tab index in query params
          });
        }
      }
    }
    