﻿
using System;

namespace AdamasV3.Models.Tables
{
    public class WebPortalLog
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string Custom { get; set; }
    }
}
