﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Adamas.Models.Dto
{
    public class QuoteResults
    {
        public int? RecordNumber { get; set; }
        public string CarePlan { get; set;  }
        public string Created { get; set; }
        public int? DocID { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartDate { get; set; }
        public string Filename { get; set; }
        public string Modified { get; set; }
        public string PlanType { get; set; }
        public string Program { get; set;  }
        public string ProgramStatus { get; set; }
        public string QuoteNumber { get; set; }
        public string St { get; set; }

    }
}
