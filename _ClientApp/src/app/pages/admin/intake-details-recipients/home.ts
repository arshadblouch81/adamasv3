import { Component } from '@angular/core'


@Component({
    selector: '',
    templateUrl:'./home.html',
    styles:[`
        table thead tr th {
            padding: 3px 0;
        }
        i{
            float:left;
            margin-right:10%;
        }
        ul{
            font-size:12px;
        }

        @media (min-width: 991.98px) { 
            i{
                float: inherit; 
                margin:0;               
            }
            span{
                text-align:center;
                line-height:13px;
            }
            span b{
                display:block;
                font-weight:100;
            }
        }

    `]
})

export class IntakeHome{

}