import { Component, OnInit,OnDestroy } from '@angular/core'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService } from '@services/timesheet.service'
import { GlobalService } from '@services/global.service'

@Component({
    selector: '',
    templateUrl:'./serviceplan.html'
})

export class IntakeCarePlan implements OnInit, OnDestroy{
    private careplan$: Subscription;
    careplans: Array<any>;
    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private router: Router,
            private globalS: GlobalService
        ){
            this.careplan$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'care-plan')){
                    this.search();
                }
            });
    }

    ngOnInit(){
        this.search();
    }

    ngOnDestroy(){
        this.careplan$.unsubscribe();
    }

    search(){
        const recipient = this.sharedS.getPicked();         
        this.timeS.getcareplans(recipient.uniqueID).subscribe(careplans => {
            this.careplans = this.careplans
        })
    }
}