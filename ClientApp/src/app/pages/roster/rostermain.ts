import { Component, OnInit, OnDestroy, Input, AfterViewInit, ChangeDetectorRef, } from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ListService, PrintService, GlobalService, TimeSheetService, MenuService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { AnyMxRecord } from 'dns';
import { NzMessageService } from 'ng-zorro-antd/message';
import { UploadChangeParam } from 'ng-zorro-antd/upload';


//Sets defaults of Criteria Model

interface Item {
    Name: string,
    selected: Boolean
}
interface Roster {
    clientCode: string;
    staffCode: string;
    program: string;
    serviceType: string;
    payType: string;
    date: string;
    startTime: string;
    duration: string;
    groupFlag: string;
    billQty: string;
    payQty: string;
    payUnit: string;
    BillUnit: string;
    location: string;
    notes: string;

}
interface selectedInputs {
    User: string,
    Password: string,
    b_AutosetLeaveOptions: boolean,
    b_DeleteAdminTravel: boolean,
    b_ProcessUnapproved: boolean,
    i_DayOne: number,
    s_Branches: string,
    s_Clients: string,
    s_Cycles: string,
    s_DestinationStartDate: string,
    s_DestinationEndDate: string,
    s_Recipients: string,
    s_Programs: string,
    s_Staff: string


}



@Component({

    styleUrls: ['./rostermain.css'],
    templateUrl: './rostermain.html',
    selector: 'rostermain',
})
export class RosterMain implements OnInit, OnDestroy, AfterViewInit {

    // drawerVisible: boolean = false;
    inputs: any = {} as selectedInputs;

    index: number = 0;
    mainTitle: string = 'Manual Roster Creation';
    tabTitle: string = 'Dates'
    defaultOpenValue = new Date(0, 0, 0, 9, 0, 0);
    startTime: any;
    endTime: any;
    rosterForm: FormGroup;
    delAdmin: boolean
    
    loading: boolean = false;
    label: string = 'Select All';
    show_views: boolean = false;
    show_Importviews: boolean = false;
    show_RollbackRosterBatch: boolean = false;
    autoLeave: boolean;
    processUnAppLeave: boolean;
    selectAllCycles: boolean;
    batchSelected: any;
    HighlightRow: number;
    date1 = new Date();
    date2 = new Date();
    masterdate = new Date();
    day: any;
    dateFormat: string = 'dd/MM/yyyy'
    today = new Date();
    IncludeFareStaffTimeSheet: boolean;
    IncludeFareTransport: boolean;
    IncludeFareGroups: boolean;
    IncludeInActiveSatff: boolean;
    IncludeSatffForEmail: boolean;
    IncludeSatffNotForEmail: boolean;

    areAllSelected = true;
    areAllBranchSelected = true;
    areAllProgramSelected = true;
    areAllStaffSelected = true;
    areAllRecipientSelected = true;
    areAllCategorySelected = true;
    areAllCoordinatorSelected = true;
    areAllteamSelected = true;
    areAllLocationSelected = true;
    areAllGroupSelected = true;
    areAllVehicleSelected = true;

    lstBranches: Array<Item> = [];
    lstPrograms: Array<Item> = [];
    lstStaff: Array<Item> = [];
    lstRecipients: Array<Item> = [];
    lstHistory: Array<Item> = [];
    lstCategories: Array<Item> = [];
    lstCoordinators: Array<Item> = [];
    lstTeams: Array<Item> = [];
    lstLocations: Array<Item> = [];
    lstGroups: Array<Item> = [];
    lstVehicles: Array<Item> = [];
    lstBatches: Array<Item> = [];

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;

    lstdays: any = [
        'Day 1',
        'Day 2',
        'Day 3',
        'Day 4',
        'Day 5',
        'Day 6',
        'Day 7',
        'Day 8',
        'Day 9',
        'Day 10',

    ]
    lstInterval: any = [
        { name: 'Fortnightly Roster', selected: true },
        { name: '4 Weekly Roster', selected: true },
        { name: 'Cycle 3', selected: true },
        { name: 'Cycle 4', selected: true },
        { name: 'Cycle 5', selected: true },
        { name: 'Cycle 6', selected: true },
        { name: 'Cycle 7', selected: true },
        { name: 'Cycle 8', selected: true },
        { name: 'Cycle 9', selected: true },
        { name: 'Cycle 10', selected: true }

    ]

    printOption: any = 'Print As Staff Timesheet'
    lstPrintOptions: any = [
        'Print As Staff Timesheet',
        'Print As Address Attachment',
        'Print As Staff Job Details Sheet',
        'Print As Payment Confirmation Sheet',
        'Print Blank Timesheet'
    ]


    generalFormat: any = 'Format 1 - Default';
    lstgeneralFormat: any = [
        'Format 1 - Default',
        'Format 2 - Fixed'
    ]

    timeFormat: any = 'hh:mm';
    lstTimeFormat: any = [
        'hh:mm',
        'hh:mm am/pm'
    ]
    blankShift: any = 'NONE';
    lstBlankShift: any = [
        'NONE',
        '1 PER Day',
        '2 PER Day',
        '3 PER Day',
        '4 PER Day',
    ]
    fontSizeNote: any = '8';
    fontSizeAddress: any = '8';
    lstFontSize: any = [
        '7',
        '8',
        '9',
        '10',
        '11',
        '12'
    ]
    RecipientInfoOption: any;
    lstRecipientInfoOption: any = [
        'Show RECIPIENT CODE',
        'Show RECIPIENT First Name',
        'Show RECIPIENT CODE + First Name',
        'Show RECIPIENT Number'
    ]


    OptionList: any = [
        { name: 'Remove Shading From Print', selected: false, visible: true },
        { name: 'Show FAX prompt', selected: false, visible: true },
        { name: 'Show ALL Outline Boxes', selected: false, visible: true },
        { name: 'Hide Company ID', selected: false, visible: true },
        { name: 'Include All Services', selected: false, visible: true },
        { name: 'Hide Drivers License', selected: false, visible: true },
        { name: 'Include Extra Stops Box', selected: false, visible: true },
        { name: 'Include Return Driver', selected: false, visible: true },
        { name: 'Include Collected Box', selected: false, visible: true },
        { name: 'IHide Time', selected: false, visible: true },
        { name: 'Force Location Display', selected: false, visible: true },
        { name: 'Display queries to text', selected: false, visible: true }

    ]
    OptionList2: any = [
        { name: 'Show Recipient Phone Number', selected: false, visible: true },
        { name: 'Show Recipient Address', selected: false, visible: true },
        { name: 'Include Service Plan', selected: false, visible: true },
        { name: 'Show Start Time', selected: false, visible: true },
        { name: 'Show End Time', selected: false, visible: true },
        { name: 'Show Duration', selected: false, visible: true },
        { name: 'Show && Concatenate Program && Activity', selected: false, visible: true },
        { name: 'Show Program', selected: false, visible: true },
        { name: 'Use GL Code Instead of Program Name', selected: false, visible: true },
        { name: 'Show Activity Code', selected: false, visible: true },
        { name: 'Show Pay Type ', selected: false, visible: true },
        { name: 'Show Runsheet Alerts', selected: false, visible: true },
        { name: 'Show Service Additional Info ', selected: false, visible: true },
        { name: 'Show Timelogging ID ', selected: false, visible: true },
        { name: 'Include Unallocated Bookings', selected: false, visible: true },
        { name: 'Show Recipient Address', selected: false, visible: true },
        { name: 'Show Bill Amount', selected: false, visible: true },
        { name: 'Show Daily Hours Total', selected: false, visible: true },
        { name: 'Show Debtor Code', selected: false, visible: true },
        { name: 'Show Recipient Phone', selected: false, visible: true },
        { name: 'Include CHSP Goods Type', selected: false, visible: true }

    ]

    displayCode: any = 'DISPLAY BY STAFF CODE'
    lstdisplayCode: any = [
        'DISPLAY BY STAFF CODE',
        'DISPLAY BY RECIPIENT CODE'
    ]

  
    recordsAdded: Array<any> = [];
    processing: boolean;
    JSONData: any;
    text: any;
    allowedExt = ["csv"];

    constructor(
        private router: Router,
        private http: HttpClient,
        private fb: FormBuilder,
        private formBuilder: FormBuilder,
        private sanitizer: DomSanitizer,
        private modalService: NzModalService,
        private listS: ListService,
        private menuS: MenuService,
        private timeS: TimeSheetService,
        private printS: PrintService,
        private cd: ChangeDetectorRef,
        private GlobalS: GlobalService,
        private msg: NzMessageService,
    ) {

    }

    ngOnInit(): void {

        this.tocken = this.GlobalS.pickedMember ? this.GlobalS.GETPICKEDMEMBERDATA(this.GlobalS.GETPICKEDMEMBERDATA) : this.GlobalS.decode();

        this.day = 'Day 1';
        var date = new Date();
        let d_strdate = new Date(date.getFullYear(), date.getMonth(), 1);
        let d_enddate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        let d_agedate = new Date(date.getFullYear(), 11, 22);

        // this.startdate = d_strdate;
        // this.enddate = d_enddate;
        // this.agedate = d_agedate;

        this.buildForm();


    }
    ngAfterViewInit(): void {

        // //testing 
        // this.listS.getreportcriterialist({
        //     listType: 'BRANCHES',
        //     includeInactive: false
        // }).subscribe(x => this.branchesArr = x);

        this.load_Batches();


    }
    ngOnDestroy(): void {


    }
    
    handleCancel() {

    }

    buildForm() {
        // this.rosterForm = this.formBuilder.group({
        //     recordNo: [''],
        //     date1: [this.today, Validators.required],
        //     date2: [this.today, Validators.required],
        //     serviceType: ['', Validators.required],
        // })
    }

    print() {

    }
    generatePdf(){
      
        
        const data = {
            "template": { "_id": "6BoMc2ovxVVPExC6" },
            "options": {
                "reports": { "save": false },                        
                "sql": this.lstHistory,                        
                "userid": this.tocken.user,
                "txtTitle":  "History",                      
            }
        }
        this.loading = true;           
                    
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "History "
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;                       
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.modalService.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
        });
      }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
    createRange(number) {
        // return new Array(number);
        return new Array(number).fill(0)
            .map((n, index) => index + 1);
    }

    getHistory() {
        let sql2 = `Select RSC_ID AS srNo, RSC_USER AS [Operator], RSC_COPY_DATE AS [Date], RSC_Branch AS [Branch], RSC_PROGRAM AS [Program], RSC_CYCLE AS [Cycle], RSC_Recipients AS [Recipients], RSC_Staff AS [Staff], Left(RSC_SourceStart, 2) + ' TO ' + Left(RSC_SourceEnd, 2) AS [Source Days], RSC_DestinationStart + '-' + RSC_DestinationEnd AS [To Dates] FROM ROSTERCOPY ORDER BY rsc_copy_date DESC`;
        this.loading = true;
        this.listS.getlist(sql2).subscribe(d => {

            this.lstHistory = d
            this.loading = false;
        });

    }
    toggleAllSelection(type: any) {
        if (type == 'Interval') {
            this.lstInterval = this.lstInterval.map(item => ({ ...item, selected: this.areAllSelected }));
        } else if (type == 'Branch') {
            if (this.lstBranches.length <= 0 || this.lstBranches == null)
                this.load_Data(type)
            this.lstBranches = this.lstBranches.map(item => ({ ...item, selected: this.areAllBranchSelected }));
        } else if (type == 'Program') {
            if (this.lstPrograms.length <= 0 || this.lstPrograms == null)
                this.load_Data(type)
            this.lstPrograms = this.lstPrograms.map(item => ({ ...item, selected: this.areAllProgramSelected }));
        } else if (type == 'Staff') {
            if (this.lstStaff.length <= 0 || this.lstStaff == null)
                this.load_Data(type)

            this.lstStaff = this.lstStaff.map(item => ({ ...item, selected: this.areAllStaffSelected }));
        } else if (type == 'Recipient') {
            if (this.lstRecipients.length <= 0 || this.lstRecipients == null)
                this.load_Data(type)

            this.lstRecipients = this.lstRecipients.map(item => ({ ...item, selected: this.areAllRecipientSelected }));
        }
        else if (type == 'Category') {
            if (this.lstCategories.length <= 0 || this.lstCategories == null)
                this.load_Data(type)
            this.lstCategories = this.lstCategories.map(item => ({ ...item, selected: this.areAllCategorySelected }));

        }
        else if (type == 'Coordinator') {
            if (this.lstCoordinators.length <= 0 || this.lstCoordinators == null)
                this.load_Data(type)
            this.lstCoordinators = this.lstCoordinators.map(item => ({ ...item, selected: this.areAllCoordinatorSelected }));

        }
        else if (type == 'StaffTeam') {
            if (this.lstTeams.length <= 0 || this.lstTeams == null)
                this.load_Data(type)
            this.lstTeams = this.lstTeams.map(item => ({ ...item, selected: this.areAllteamSelected }));

        } else if (type == 'Location') {
            if (this.lstLocations.length <= 0 || this.lstLocations == null)
                this.load_Data(type)
            this.lstLocations = this.lstLocations.map(item => ({ ...item, selected: this.areAllLocationSelected }));

        } else if (type == 'Group') {
            if (this.lstGroups.length <= 0 || this.lstGroups == null)
                this.load_Data(type)
            this.lstGroups = this.lstGroups.map(item => ({ ...item, selected: this.areAllGroupSelected }));

        } else if (type == 'Vehicle') {
            if (this.lstVehicles.length <= 0 || this.lstVehicles == null)
                this.load_Data(type)
            this.lstVehicles = this.lstVehicles.map(item => ({ ...item, selected: this.areAllVehicleSelected }));

        }



    }

    selectAll(type: any) {
        if (type == 'Interval')
            this.areAllSelected = !this.areAllSelected;
        else if (type == 'Branch')
            this.areAllBranchSelected = !this.areAllBranchSelected;
        else if (type == 'Program')
            this.areAllProgramSelected = !this.areAllProgramSelected;
        else if (type == 'Staff')
            this.areAllStaffSelected = !this.areAllStaffSelected;
        else if (type == 'Recipient')
            this.areAllRecipientSelected = !this.areAllRecipientSelected;
        else if (type == 'Category')
            this.areAllCategorySelected = !this.areAllCategorySelected;
        else if (type == 'Coordinator')
            this.areAllCoordinatorSelected = !this.areAllCoordinatorSelected;
        else if (type == 'StaffTeam')
            this.areAllteamSelected = !this.areAllteamSelected;
        else if (type == 'Location')
            this.areAllLocationSelected = !this.areAllLocationSelected;
        else if (type == 'Group')
            this.areAllGroupSelected = !this.areAllGroupSelected;
        else if (type == 'Vehicle')
            this.areAllVehicleSelected = !this.areAllVehicleSelected;


        this.toggleAllSelection(type);
    }

    view(index: number) {
        this.index = index;
        if (index == 0) {
            this.mainTitle = 'Manual Roster Creation';
            this.tabTitle = 'Dates';
            //this.router.navigate(['/admin/branches'])
            this.show_views = true;
        }
        if (index >= 1 && index <= 6) {
            this.mainTitle = 'Staff Roster Print';
            this.tabTitle = 'Dates, Staff/Locations';
            //this.router.navigate(['/admin/funding-region'])
            this.show_views = true;
        }
        if (index == 7) {
            //this.router.navigate(['/admin/claim-rates'])
            //this.load_Batches();
            this.show_RollbackRosterBatch = true;

        }
        if (index == 8) {
            // this.router.navigate(['/admin/target-groups'])
            //this.show_views = true;
        }
        if (index == 9) {
            // this.router.navigate(['/admin/target-groups'])
            this.show_Importviews = true;
        }
    }

    load_Batches() {
        let sql = `SELECT rsc_id as bch_num, rsc_user as bch_user, rsc_copy_date as bch_date,
                 convert(varchar,rsc_id) + '  ' + rsc_user + '  ' + convert(varchar(10),rsc_copy_date,103) as batch
                 FROM rostercopy ORDER BY bch_date DESC`
        this.listS.getlist(sql).subscribe(d => {

            this.lstBatches = d//.map(x=>x.batch)

        });

    }
    load_Data(type: string) {
        if (type == 'Branch') {
            this.listS.getlistbranches().subscribe(d => {

                this.lstBranches = d.map(x => {
                    return {
                        name: x,
                        selected: false
                    }
                });
            });
        } else if (type == 'Program') {
            this.listS.GetAllPrograms().subscribe(d => {

                this.lstPrograms = d.map(x => {
                    return {
                        name: x,
                        selected: false
                    }
                });
            });

        } else if (type == 'Category') {

            let sql3 = `select [Description] as [name] from DataDomains WHERE [Domain] = 'STAFFGROUP'`;

            this.listS.getlist(sql3).subscribe(d => {

                this.lstCategories = d.map(x => {
                    return {
                        name: x.name,
                        selected: false
                    }
                });
            });

        } else if (type == 'Recipient') {
            let sql2 = `select AccountNo from Recipients where AccountNo > '!z' AND  ISNULL(AdmissionDate, '') <> '' AND ISNULL(DischargeDate, '') = ''`;

            if (this.lstRecipients.length == 0 || this.lstRecipients == null) {
                this.listS.getlist(sql2).subscribe(d => {

                    this.lstRecipients = d.map(x => {
                        return {
                            name: x.accountNo,
                            selected: false
                        }
                    });

                });

            }
        } else if (type == 'Staff') {
            let sql = `select AccountNo from staff 
                where ISNULL(CommencementDate,'')<>'' AND CommencementDate <=convert(varchar(10),getDate(),111)
                 AND (ISNULL(TerminationDate,'')='' or TerminationDate >= convert(varchar(10),getDate(),111))  
                 AND (ACCOUNTNO <> '!INTERNAL' AND AccountNo <> '!MULTIPLE')  `

            this.listS.getlist(sql).subscribe(d => {

                this.lstStaff = d.map(x => {
                    return {
                        name: x.accountNo,
                        selected: false
                    }

                });

            });

        } else if (type == 'Coordinator') {

            let sql3 = `select [Description] as [name] from DataDomains WHERE [Domain] = 'CASE MANAGERS'`;

            this.listS.getlist(sql3).subscribe(d => {

                this.lstCoordinators = d.map(x => {
                    return {
                        name: x.name,
                        selected: false
                    }
                });
            });

        } else if (type == 'StaffTeam') {

            let sql3 = `select [Description] as [name] from DataDomains WHERE [Domain] = 'STAFFTEAM'`;

            this.listS.getlist(sql3).subscribe(d => {

                this.lstTeams = d.map(x => {
                    return {
                        name: x.name,
                        selected: false
                    }
                });
            });
        } else if (type == 'Location') {


            let sql3 = `SELECT DISTINCT UPPER([Name]) as name FROM CSTDAOutlets`;

            this.listS.getlist(sql3).subscribe(d => {

                this.lstLocations = d.map(x => {
                    return {
                        name: x.name,
                        selected: false
                    }
                });
            });
        } else if (type == 'Group') {



            let sql3 = `SELECT DISTINCT UPPER(Description) as name FROM DataDomains WHERE [Domain] = 'ACTIVITYGROUPS' `;

            if (this.index == 5)
                sql3 = sql3 + ` and [Description]  like '%MEAL%'`

            this.listS.getlist(sql3).subscribe(d => {

                this.lstGroups = d.map(x => {
                    return {
                        name: x.name,
                        selected: false
                    }
                });
            });
        } else if (type == 'Vehicle') {


            let sql3 = `SELECT DISTINCT UPPER(Description) as name FROM DataDomains WHERE [Domain] = 'VEHICLES' `;

            this.listS.getlist(sql3).subscribe(d => {

                this.lstVehicles = d.map(x => {
                    return {
                        name: x.name,
                        selected: false
                    }
                });
            });
        }


    }



    Build_Criteria() {
        let criteria: string;
        let s_StartDate: any = format(this.date1, 'yyy/MM/dd');
        let s_EndDate: any = format(this.date2, 'yyy/MM/dd');


        criteria = ` ([Roster].[Date] BETWEEN '${s_StartDate}' AND '${s_EndDate}') AND ([Roster].[Start Time] BETWEEN '${this.startTime}' AND '${this.endTime}')`


        this.inputs.s_DestinationStartDate = String(s_StartDate).toString();
        this.inputs.s_DestinationEndDate = String(s_EndDate).toString();

        let selectedvalues = this.lstInterval.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        this.inputs.s_Cycles = selectedvalues

        selectedvalues = this.lstBranches.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND (([Recipients].[Branch] IN (${selectedvalues})) OR ([Staff].[STF_DEPARTMENT] IN (${selectedvalues})) OR ([Roster].[BRANCH] IN (${selectedvalues})))`
        this.inputs.s_Branches = selectedvalues

        selectedvalues = this.lstPrograms.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ([Roster].[Program] IN (${selectedvalues})) `
        this.inputs.s_Programs = selectedvalues

        selectedvalues = this.lstStaff.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ([Roster].[Carer Code] > '!z' AND [Roster].[Carer Code] IN (${selectedvalues})) `
        this.inputs.s_Staff = selectedvalues

        selectedvalues = this.lstRecipients.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ([Roster].[Client Code] > '!z' AND [Roster].[Client Code] IN (${selectedvalues})) `
        this.inputs.s_Recipients = selectedvalues
        this.inputs.s_Clients = selectedvalues

        //  'STAFF MANAGER [Staff].[StaffManager]
        selectedvalues = this.lstCoordinators.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ([Staff].[PAN_Manager] IN (${selectedvalues})) `


        //STAFF StaffTeam [Staff].[StaffTeam]
        selectedvalues = this.lstTeams.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ([Staff].[StaffTeam] IN (${selectedvalues})) `

        //STAFF CATEGORY [Staff].[StaffGroup]
        selectedvalues = this.lstCategories.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ([Staff].[StaffGroup] IN (${selectedvalues})) `



        //LOCATION [Roster].[ServiceSetting]
        selectedvalues = this.lstCategories.filter(x => x.selected == true);
        selectedvalues = selectedvalues.map(x => x.name)
        selectedvalues = JSON.stringify(selectedvalues);
        criteria = `AND ( [Roster].[ServiceSetting] IN (${selectedvalues})) `


    }

    getBatchValue(batchSelected) {
        console.log(this.batchSelected);
    }
    Run_Rollback() {
        console.log(this.batchSelected);
    }
    Run_Process() {

        if (this.index == 9) {
            this.ImportRoster();
            return;
        }

        this.Build_Criteria()


        this.inputs.User = "sysmgr"
        this.inputs.Password = "sysmgr"
        this.inputs.b_AutosetLeaveOptions = this.autoLeave
        this.inputs.b_DeleteAdminTravel = this.delAdmin;
        this.inputs.b_ProcessUnapproved = this.processUnAppLeave;
        this.inputs.i_DayOne = 5;

        console.log(this.inputs);

        this.timeS.postCreateCopyRoster(this.inputs).subscribe(d => {
            console.log(d)
        })

    }

    processLeave(n: number) {

    }



    ImportRoster() {
        let listRoster: any = <Roster>JSON.parse(this.JSONData);
        // console.log(listRoster);
        let values = '';
        let s_SQLInsert = '';
        this.processing = true;
        const obsvr = new Observable(observer => {
            setTimeout(() => {

                listRoster.forEach(rst => {

                    let date = new Date(rst.date)
                    let stime = new Date(rst.date + ' ' + rst.startTime)
                    let etime = new Date(rst.date + ' ' + rst.startTime)

                    etime.setMinutes(stime.getMinutes() + (rst.duration * 5));

                    let durationObject = this.GlobalS.computeTimeDATE_FNS(stime, etime);


                    let inputs = {

                        clientCode: rst.clientCode,
                        carerCode: rst.carerCode,
                        date: format(date, 'yyyy/MM/dd'),
                        dayno: parseInt(format(date, 'd')),
                        startTime: format(stime, 'HH:mm'),
                        duration: durationObject.duration,
                        blockNo: durationObject.blockNo,
                        monthNo: format(date, 'M'),
                        yearNo: format(date, 'yyyy'),
                        program: rst.program,
                        serviceDescription: rst.payType,
                        serviceSetting: rst.serviceSetting || "",
                        serviceType: rst.serviceType || "",
                        paytype: rst.payType,
                        billQty: rst.billQty || 0,
                        billTo: rst.billTo,
                        billUnit: rst.billUnit || 'HOUR',
                        costQty: rst.payQty || 0,
                        costUnit: rst.payUnit || 'HOUR',
                        groupActivity: false,
                        haccType: rst.haccType || "",
                        anal: rst.analysisCode == '' || rst.analysisCode == null ? rst.anal : rst.analysisCode,
                        staffPosition:  "",
                        status: "1",
                        taxPercent: rst.tax || 0,
                        transferred: 0,
                        type: this.GetRosterType(rst.serviceType),
                        unitBillRate: (rst.bill_Rate || 0),
                        unitPayRate: rst.pay_Rate || 0,
                        serviceTypePortal: rst.serviceType,
                        recordNo: rst.recordNo,
                        date_Timesheet: format(date, 'yyyy/MM/dd'),
                        dischargeReasonType: rst.haccCode || '',
                        creator: this.tocken.user,
                        datasetClient: rst.clientCode,
                        groupFlag: rst.groupFlag

                    };

                    this.timeS.posttimesheet(inputs).subscribe(data => {
                       
                        this.recordsAdded.push(data);

                        let history: any = {
                            operator: this.tocken.user,
                            actionDate: format(this.today, 'yyyy/MM/dd HH:MM:ss'),
                            auditDescription: 'CREATE NEW ROSTER ENTRY FROM IMPORT',
                            actionOn: 'WEB-ROSTER',
                            whoWhatCode: data,
                            traccsUser: this.tocken.user
                        }
                        this.timeS.postaudithistory(history).subscribe(d => { });

                       // this.GlobalS.iToast("Import", `Process comspleted Succssfully ${+ this.NRecordNo}`)


                    });
                });

                observer.next(this.recordsAdded);

            }, 1000);
        });

        obsvr.subscribe(result => {
            setTimeout(() => {
                console.log(result);
                let d = result[0];
                this.processing = false;
                this.processing = false;
                if (d!=null) {
                    this.GlobalS.sToast('Purcahse Order', `Import procesed successfully!`);
                } else {
                    this.GlobalS.eToast('Purcahse Order', `Something went wrong while processing! `);
                }
            }, 2000);
        });



    }



    GetRosterType(s_TextType: string) {

        var i_Type: number;

        switch (s_TextType) {
            case 'ADMINISTRATION':
                i_Type = 6;
                break;
            case 'ADMISSION':
                i_Type = 7
                break;

            case 'BROKERED SERVICE':
                i_Type = 3
                return;
            case 'CENTREBASED':
                i_Type = 11;
                break;
            case 'ONEONONE':
                i_Type = 2;
                break;
            case 'GROUPACTIVITY':
                i_Type = 12
                break;
            case 'LEAVE/ABSENCE':
                i_Type = 4;
                break;
            case 'SLEEPOVER':
                i_Type = 8;
                break;
            case 'TRANSPORT':
                i_Type = 10;
                break;
            case 'TRAVEL TIME':
                i_Type = 5;
                break;
            case 'UNAVAILABILITY':
                i_Type = 13;
                break;
            case 'ITEM':
                i_Type = 14;
                break;
            case 'FEE':
                i_Type = 1;
                break;
            default:
                {
                    if (s_TextType == 'ALLOWANCE' || s_TextType == 'ALLOWANCE CHARGEABLE' || s_TextType == 'ALLOWANCE NON-CHARGEABLE' || s_TextType == 'CHARGEABLE ITEM')
                        i_Type = 9;
                    else
                        i_Type = 2;
                    break;
                }
        }
        return i_Type;

    }

    csvJSON(csvText) {
        var lines = csvText.split("\n");

        var result = [];

        var headers = lines[0].split(",");
        console.log(headers);
        headers = this.rest_header(headers);
        console.log(headers);

        for (var i = 1; i < lines.length - 1; i++) {
            var obj = {};
            var currentline = lines[i].split(",");

            for (var j = 0; j < headers.length; j++) {
                obj[headers[j]] = currentline[j];
            }

            result.push(obj);
        }

        //return result; //JavaScript object
        //console.log(JSON.stringify(result)); //JSON
        this.JSONData = JSON.stringify(result);
    }

    rest_header(headers: any) {
        let new_headers: any = headers;

        let s_Fields: string;
        let regExp: any;
        let i: number = 0;
        headers.forEach(x => {


            s_Fields = x.toUpperCase();
            if (s_Fields.includes('PROGRAM'))
                new_headers[i] = "program";
            else if (s_Fields.includes('STAFF'))
                new_headers[i] = "carerCode";
            else if (s_Fields.includes('CLIENT'))
                new_headers[i] = "clientCode";
            else if (s_Fields.includes('RECIPIENT'))
                new_headers[i] = "clientCode";
            else if (s_Fields.includes('SERVICE'))
                new_headers[i] = "serviceType";
            else if (s_Fields.includes('PAYTYPE') || s_Fields.includes('PAY TYPE'))
                new_headers[i] = "payType";
            else if (s_Fields.includes('DATE'))
                new_headers[i] = "date";
            else if (s_Fields.includes('START') || s_Fields.includes('TIME'))
                new_headers[i] = "startTime";
            else if (s_Fields.includes('DURATION'))
                new_headers[i] = "duration";
            else if (s_Fields.includes('GROUP') || s_Fields.includes('GROUPFLAG'))
                new_headers[i] = "groupFlag";
            else if (s_Fields.includes('BILLQTY') || s_Fields.includes('BILL QTY'))
                new_headers[i] = "billQty";
            else if (s_Fields.includes('PAYQTY') || s_Fields.includes('PAY QTY'))
                new_headers[i] = "payQty";
            else if (s_Fields.includes('LOCATION') || s_Fields.includes('LOC'))
                new_headers[i] = "location";
            else if (s_Fields.includes('NOTES') || s_Fields.includes('NOTE'))
                new_headers[i] = "notes";

            i = i + 1;

        });

        return new_headers;

    }

    convertFile(e) {
        const reader = new FileReader();
        const file = e.files[0].name;
        let ext: string = file.split('.')[1]

        if (this.isInArray(this.allowedExt, ext)) {
            reader.readAsText(e.files[0]);
            reader.onload = () => {
                let text = reader.result;
                this.text = text;
                // console.log(text);
                this.csvJSON(text);
                // this.ImportRoster()
            };
        } else {
            console.log('file not supported');
        }
    }

    isInArray(array, word) {
        return array.indexOf(word.toLowerCase()) > -1;
    }
    handleChange({ file, fileList }: UploadChangeParam): void {
        const status = file.status;
        if (status !== 'uploading') {
            // console.log(file, fileList);
        }
        if (status === 'done') {
            this.msg.success(`${file.name} file uploaded successfully.`);
            //this.loadFiles();
        } else if (status === 'error') {
            this.msg.error(`${file.name} file upload failed.`);
        }
    }

}
