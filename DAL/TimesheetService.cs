using System;
using System.ComponentModel;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using System.Text;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Security.Cryptography;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json;
using System.Threading;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Configuration;

using Adamas.Models;
using Adamas.Models.Modules;
using Adamas.DAL;


using Adamas.Controllers;
using Adamas.Models.Tables;

namespace Adamas.DAL
{

    public class TimesheetService : ITimesheetService
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ISqlDbConnection _conn;
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;

        public TimesheetService(
            IOptions<JwtIssuerOptions> jwtOptions,
            ISqlDbConnection conn,
            DatabaseContext context,
            IGeneralSetting setting
            )
        {
            _jwtOptions = jwtOptions.Value;
            _context = context;
            _conn = conn;
            GenSettings = setting;
        }


        public async Task<dynamic> PostFundingAsync(dynamic obj)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                dynamic _final = new JObject();

                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    string ps = "";
                    string program = obj.program ?? "";
                    string packageType = obj.packageType ?? "";
                    string programStatus = obj.programStatus ?? "";
                    string expireUsing = obj.expireUsing ?? "";
                    string priority = obj.priority ?? "";
                    string itemUnit = obj.itemUnit ?? "";
                    string quantity = obj.quantity ?? "";
                    string perUnit = obj.perUnit ?? "";
                    string timeUnit = obj.timeUnit ?? "";
                    string period = obj.period ?? "";
                    string personID = obj.personID ?? "";
                    string user = obj.user ?? "";

                    string sDate = obj.startDate;
                    DateTime? dt1 = null;
                    if (!string.IsNullOrEmpty(sDate))
                    {
                        dt1 = DateTime.Parse(sDate);
                    }

                    string eDate = obj.expiryDate;
                    DateTime? dt2 = null;
                    if (!string.IsNullOrEmpty(eDate))
                    {
                        dt2 = DateTime.Parse(eDate);
                    }

                    string rDate = obj.reminderDate;
                    DateTime? dt3 = null;
                    if (!string.IsNullOrEmpty(rDate))
                    {
                        dt3 = DateTime.Parse(rDate);
                    }

                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO recipientprograms (personid, program, programstatus, quantity, itemunit, perunit, timeunit, period, startdate, totalallocation, expirydate, reminderdate, autorenew, rolloverremainder, expireusing, used, billing, percentage, capped, cappedat, reminderleadtime, deactivateonexpiry, packagetype, packagelevel, packagetermtype, programsummary, priority, contingency, commonwealthcont, dailyincometestedfee, dailyagreedtopup, dailybasiccarefee, clientcont, incometested, agreedtopup, contingency_buildcycle, contingency_start, autoreceipt, autobill, ap_basedon, ap_costtype, ap_perunit, ap_period, ap_yellowqty, ap_orangeqty, ap_redqty, alertstartdate, packagesupplements, adminamount_perc, hardshipsupplement) 
                        VALUES(@PersonID, @Program, @Status, '100', @ItemUnit, @PerUnit, @Period, @Claimed, @DateCurrentPackage, '100', @DateCurrentFunding, @DateReminder, 0, 0, @ExpireUsing, '', '', '', 0, '', '', 0, @Type, '', '', 'ONGOING WITH PERIODICAL REVIEW', 'HIGH', 2, 0, 0, 0, 1234, 1234, 0, 0, '', '12', 0, 0, 0, 'HOURS', 'BAL', '', 0, 0, 0, '', '00000000000000000000000000', '12', '0')", conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@PersonID", personID);
                        cmd.Parameters.AddWithValue("@Program", program ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Type", packageType ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Status", programStatus ?? (object)DBNull.Value);

                        cmd.Parameters.AddWithValue("@ExpireUsing", expireUsing ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Priority", priority ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ItemUnit", itemUnit ?? (object)DBNull.Value);
                        // cmd.Parameters.AddWithValue("@Amount", ps);
                        // cmd.Parameters.AddWithValue("@CostType", ps);

                        cmd.Parameters.AddWithValue("@PerUnit", perUnit ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Period", timeUnit ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@Claimed", period ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateCurrentPackage", dt1 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateCurrentFunding", dt2 ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@DateReminder", dt3 ?? (object)DBNull.Value);

                        await cmd.ExecuteNonQueryAsync();
                    }

                    // using (SqlCommand cmd = new SqlCommand(@"INSERT INTO humanresources (personid, [group], [type], [name], date1, date2, creator, remindercreator, notes, reminderscope) 
                    //     VALUES( @PersonID, 'RECIPIENTALERT', 'RECIPIENTALERT', 'PROGRAM EXPIRY', '2021/01/31', '2021/01/31', 'sysmgr', 'sysmgr', '**AWARD CSTDA will expire on 2021/01/31', 'CC' )", conn, transaction))
                    // {
                    //     cmd.Parameters.AddWithValue("@PersonID", personID);
                    //     cmd.Parameters.AddWithValue("@Program", ps);
                    //     cmd.Parameters.AddWithValue("@Type", ps);
                    //     cmd.Parameters.AddWithValue("@Status", ps);
                    // }

                    using (SqlCommand cmd = new SqlCommand(@"INSERT INTO audit (operator, actiondate, auditdescription, actionon, whowhatcode, traccsuser) 
                        VALUES(@user, convert(varchar, getdate(), 22), 'CHANGE PROGRAM', @Program, @PersonID, 'sysmgr')", conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@PersonID", personID);
                        cmd.Parameters.AddWithValue("@user", user);
                        cmd.Parameters.AddWithValue("@Program", program);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                    return obj;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return null;
                }
            }
        }

        public async Task<SysTable> GetSysTable()
        {
            return await (from sysTable in _context.SysTable select sysTable).FirstOrDefaultAsync();
        }

    }
}