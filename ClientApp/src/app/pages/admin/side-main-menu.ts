import { Component, OnInit, OnDestroy, Input } from '@angular/core'

import { GlobalService } from '@services/index';

interface SPECIAL_ROUTE_GUARDS {
  mmAccounting: number,
  mmAnalyseBudget: boolean,
  mmAtAGlance: boolean,
  mmBilling: number,
  mmOtherDS: number,
  mmPriceUpdates: number,
  mmPublishPrintRosters: number,
  mmTimesheetProcessing: number,
  mmdexUploads: number,
  mmhacc: number,
  mmndia: number,
  accessCDC: boolean,
  system?: number
}

@Component({
    styles: [`
      .bg{
        height: 80vh;
      }
      .landing-container{
        display: grid;
        grid-template-columns: minmax(min-content, 1fr) minmax(min-content, 1fr) minmax(min-content, 1fr);
        grid-gap: 1rem;
        width: 50%;
        margin: 0 auto;
        padding-top: 5%;
      }
      figure{
        min-width:4rem;
        background-color:#85B9D5;
        color:#fff;
        border-radius: 3px;
        padding: 20px;
        font-size: 30px;
        text-align: center;
        margin:0;
        position: relative;
        height: 8rem;
        cursor:pointer;
      }
      figure:hover{
        background-color: #2466b9;
        color:#fff;
        -webkit-transition: background-color 300ms linear;
        -ms-transition: background-color 300ms linear;
        transition: background-color 300ms linear;
      }
      figure i,span{
        font-size: 2.7rem;
      }
      figcaption{
        font-size: 1.12rem;
        position: absolute;
        bottom: 1rem;
        left: 0;
        right: 0;
      }

      .icon{
        padding:2px; 
        height:30px; width:25px;
        border: 1px solid #360d34;
        border-radius: 5px;
        background:transparent;

      }
    .header{
      display: flex;       
      flex-direction:row; 
      align-items: flex-end;
      border: 1px solid #360d34;
      border-radius: 5px;
      background: #360d34;
      height: 40px;
      font-family: Segoe UI;
      font-size: 14px;
      color: white;
      text-align: center;
      padding: 5px;
    }
      .chatbox{       
        position: fixed;
        right:5px;
        bottom:20px;
        height: 55vh;
        width:21vw;
        background : #85B9D5 ;
        z-index: 2;
        border: 1px solid #1E91E8;
        border-radius: 7px;
        
      }
      .chaticon{
        display: flex;
        position: fixed;
        right:20px;
        bottom:20px;
        height: 50px;
        width:50px;
      }

      .fade-in-text {
          font-family: Segoe UI;
          font-size: 14px;
          animation: fadeIn 1.0s infinite alternate;

      }
      
        @keyframes fadeIn {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }
     
    `],
    templateUrl: './side-main-menu.html'
})


export class SideMainMenu implements OnInit, OnDestroy {

    SPECIAL_ROUTE_GUARDS: SPECIAL_ROUTE_GUARDS;
    showChat:boolean;

    constructor(private globalS: GlobalService) {

    }

    ngOnInit(): void {
      const settings: any = this.globalS.settings;

      this.SPECIAL_ROUTE_GUARDS = {
          mmAccounting: settings.mmAccounting,
          mmAnalyseBudget: settings.mmAnalyseBudget,
          mmAtAGlance: settings.mmAtAGlance,
          mmBilling: settings.mmBilling,
          mmOtherDS: settings.mmOtherDS,
          mmPriceUpdates: settings.mmPriceUpdates,
          mmPublishPrintRosters: settings.mmPublishPrintRosters,
          mmTimesheetProcessing: settings.mmTimesheetProcessing,
          mmdexUploads: settings.mmdexUploads,
          mmhacc: settings.mmhacc,
          mmndia: settings.mmndia,
          accessCDC: settings.accessCDC,
          system: settings.system
      }
    }

    ngOnDestroy(): void {

    }
}