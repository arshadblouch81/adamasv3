import { Component,AfterViewInit,Input,Output,EventEmitter,ChangeDetectorRef,ViewChild, ViewContainerRef, ComponentRef, ComponentFactoryResolver } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { GlobalService,staffnodes,StaffService,sbFieldsSkill, ShareService,timeSteps,conflictpointList,checkOptionsOne,sampleList,genderList,statusList, ListService, TimeSheetService,   } from '@services/index';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { filter, switchMap } from 'rxjs/operators';
import format from 'date-fns/format';
import { NzFormatEmitEvent, NzTreeHigherOrderServiceToken } from 'ng-zorro-antd/core';
import { EMPTY, forkJoin } from 'rxjs';
import { stringify } from '@angular/compiler/src/util';
import { forEach } from 'lodash';
import { Subscription, Subject } from 'rxjs';
import { XmlParser } from '@angular/compiler';
//import { RostersAdmin } from './rosters';
import * as moment from 'moment';
import { RostersAdmin } from '../roster/rosters';
import { takeUntil } from 'rxjs/operators';
import { error } from 'console';

class Address {
  postcode: string;
  address: string;
  suburb: string;
  state: string;

  constructor(postcode: string, address: string, suburb: string, state: string) {
      this.suburb = suburb.trim();
      this.address = address;
      this.postcode = postcode;
      this.state = state;
  }

  getAddress() {
      var _address = `${this.address} ${this.suburb} ${this.postcode}`;
      return (_address.split(' ').join('+')).split('/').join('%2F');
  }
}

interface Staff {
  accountNo: string;
  gaps : any[];
  durations: any[][] ;
}

@Component({
    selector: 'staff-search',
    templateUrl: './staffsearch.html',
    styleUrls: ['./staffsearch.css']
  })
  export class StaffSearch implements AfterViewInit{
      @Input() findModalOpen:boolean=false;      
      @Input() bookingData = new Subject<any>();
      @Output() searchDone:EventEmitter<any>= new EventEmitter();
      //@ViewChild('RostersAdmin') rosterForm: RostersAdmin;
      @ViewChild('dynamicRoster', {read: ViewContainerRef})
      dynamicRosterContainer: ViewContainerRef;
      private _dynamicRoster: ComponentRef<RostersAdmin>;
      private unsubscribe = new Subject();
      reloadRoster = new Subject<any>();
      
      info = {StaffCode:'', ViewType:'Staff',IsMaster:false, date:''}; 

      menuItems= new Subject<number>();
      booking :any;
      selectAll:boolean;
      showMenu:boolean;
      selectedStaff:any;
      extendedSearch: any;
      allBranches:boolean = true;
      allBranchIntermediate:boolean = false;
      filteredResult: any;
      originalList: any;
      selectedTypes:any;
      selectedbranches: any[];
      testcheck : boolean = false;
      selectedPrograms: any;
      selectedCordinators: any;
      selectedCategories: any;
      selectedSkills:any;
      txtSearch:string;
      numbers: number[] = Array.from({ length: 24 }, (_, i) => i);
      durations: any[] = Array.from({ length: 12 }, (_, i) => i);

      loading: boolean;    
      allProgarms:boolean = true;
      allprogramIntermediate:boolean = false;
      quicksearch: FormGroup;
      avilibilityForm: FormGroup;
      filters: FormGroup;

      genderList:any = genderList;
      statusList:any = statusList;
      branchesList: any;
      diciplineList: any;
      casemanagers: any;
      categoriesList: any;
      skillsList:any;
      conflictpointList:any = conflictpointList;

      allCordinatore:boolean = true;
      allCordinatorIntermediate:boolean = false;
      searchAvaibleModal : boolean = false;
      @Input() AllocateStaff:boolean;
      @Output() AllocatedStaff:EventEmitter<any>= new EventEmitter();
      allocated_staff_list:Array<any> = [];
   dateFormat:string="dd/MM/YYYY"
   fromDate:Date;
   toDate:Date;
    allcat:boolean = true;
    allCatIntermediate:boolean = false;
  
  
    allChecked: boolean = true;
    indeterminate: boolean = false;
    tabFindIndex:number=0;
    address:any;
  

    sampleList: Array<any> = sampleList;
    cariteriaList:Array<any> = [];
    nodelist:Array<any> = [];
    checkOptionsOne = checkOptionsOne;
    sbFieldsSkill:any;
    openRoster:boolean;

    nzEvent(event: NzFormatEmitEvent): void {
      if (event.eventName === 'click') {
        var title = event.node.origin.title;
        
        this.extendedSearch.patchValue({
          title : title,
        });
        var keys       = event.keys;
        
      }
      
    }
    
      tabFindChange(index: number){
        this.tabFindIndex = index;
        if  (index==4){
          //if (this.filteredResult==null || this.filteredResult.length==0)
          this.load_all_staff();
        // else
        //   this.staffList= this.filteredResult;
        
      }
      }

    constructor(private router: Router,
      private activeRoute: ActivatedRoute,
      private timeS: TimeSheetService,
      private sharedS: ShareService,
      private globalS: GlobalService,
      private listS: ListService,
      private cd: ChangeDetectorRef,
      private fb: FormBuilder,
      private componentFactoryResolver: ComponentFactoryResolver,
      private location: ViewContainerRef
      ){
      
    }
    ngOnInit(){
      this.sbFieldsSkill = sbFieldsSkill;
      this.nodelist = staffnodes;
        this.buildForms();
        this.getUserData();

        this.bookingData.subscribe(data=>{
          this.loadModel(data);
        })

        this.menuItems.subscribe(index => {
          // console.log(data);
          this.showMenu=false;
          switch(index){  
            case 0:
              this.toMap();
            break;
          case 1:
            this.toLastKnownMap();
            break;
          case 2:
            this.openRoster=true;
            this.info.StaffCode=this.selectedStaff.accountno;
            let dt:Date= new Date();
            this.info.date= moment(dt).format('YYYY/MM/DD');
            this.reloadRoster.next(this.info);   
            // if (this._dynamicRoster==null)
            //   this.loadRosterComponent();
            // else{
            //   this._dynamicRoster.instance.reloadRoster.next(this.info);
            //   this._dynamicRoster.instance.fixSize=true;
            // }
            break;
          case 3:
            this.openRoster=true;
            this.info.StaffCode=this.selectedStaff.accountno;          
            this.info.date= '';
            this.reloadRoster.next(this.info);
           
          //  if (this._dynamicRoster==null)
          //     this.loadRosterComponent();
          //   else{
          //     this._dynamicRoster.instance.reloadRoster.next(this.info);
          //     this._dynamicRoster.instance.fixSize=true;
          //   }
            break;
           case 4:
             this.selectAll=true;
            break;  
              
          }
        });
    }
    ngAfterViewInit(){

      }

      async loadRosterComponent() {
        const {RostersAdmin} = 
          await import('../roster/rosters');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(RostersAdmin);
        
        // create and inject component into dynamicContainer
        this._dynamicRoster = 
          this.dynamicRosterContainer.createComponent(dynamicComponentFactory, this.location.length, null);
       
       
      
        this._dynamicRoster.instance.reloadRoster=this.reloadRoster;
        this._dynamicRoster.instance.reloadRoster.next(this.info);
        this._dynamicRoster.instance.fixSize=true;
      
      }

    handleCancel(){
        this.findModalOpen=false;
    }

    handleOk(){ 
      if (this.selectedStaff==null) return;
      this.searchDone.emit(this.selectedStaff);
      this.findModalOpen=false;
    }
    loadModel(data:any){
      this.booking=data;
      this.findModalOpen=true;
    }
    buildForms(){
        
        this.quicksearch = this.fb.group({
          availble: false,
          option: false,
          status:'Active',
          gender:'Any Gender',
          surname:'',
          staff:true,
          brokers:true,
          volunteers:true,
          onleaveStaff:true,
          previousWork:false,
          searchText:''
        });
        
        this.avilibilityForm = this.fb.group({
          date  :[new Date()],
          start :'09:00',
          end   :'10:00',
          drtn  :'01:00',
          conflict:true,
          conflictminutes:'',
        });
        
        this.filters = this.fb.group({
          activeprogramsonly:false,
        });
        
        this.extendedSearch = this.fb.group({
          title:'',
          rule:'',
          from:'',
          to:'',
          
          activeonly: true,
        });
        this.quicksearch.get("searchText").valueChanges.subscribe(x => {
          //  console.log('form value changed')
           // console.log(x)
            this.txtSearch=x;
            this.onTextChangeEvent(null);
        })
   
      }

      getUserData() {
        forkJoin([
          this.listS.getlisttimeattendancefilter("BRANCHES"),
          this.listS.getlisttimeattendancefilter("STAFFTEAM"),
          this.listS.getlisttimeattendancefilter("STAFFGROUP"),
          this.listS.getlisttimeattendancefilter("CASEMANAGERS"),
          this.listS.getskills(),
        ]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.branchesList = data[0].map(x => {
            return {
              label: x,
              value: x,
              checked: false
            }
          });
          this.diciplineList = data[1].map(x => {
            return {
              label: x,
              value: x,
              checked: false
            }
          });
          this.categoriesList = data[2].map(x => {
            return {
              label: x,
              value: x,
              checked: false
            }
          });
          this.casemanagers = data[3].map(x => {
            return {
              label: x,
              value: x,
              checked: false
            }
          });
          this.skillsList = data[4];
        });
      }
     
      searchData() : void{
        this.loading = true;      
        
        this.selectedTypes = this.checkOptionsOne
        .filter(opt => opt.checked)
        .map(opt => opt.value).join("','")
        
        this.selectedPrograms = this.diciplineList
        .filter(opt => opt.checked)
        .map(opt => opt.value)
        
        this.selectedCordinators = this.casemanagers
        .filter(opt => opt.checked)
        .map(opt => opt.value)
        
        this.selectedCategories = this.categoriesList
        .filter(opt => opt.checked)
        .map(opt => opt.value)
        
        this.selectedbranches = this.branchesList
        .filter(opt => opt.checked)
        .map(opt => opt.value)
        
        this.selectedSkills  = this.skillsList
        .filter(opt => opt.checked)
        .map(opt => this.sbFieldsSkill[opt.identifier])
        
        // if (this.booking==null){
        //   this.booking={
        //     recipientCode: 'TT',
        //     userName:'sysmgr',
        //     date:'2022/01/01',
        //     startTime:'07:00',
        //     endTime:'17:00',
        //     endLimit:'20:00'
        //   }
        // }
      
        if (this.booking==null){
          let dt= new Date();
          this.booking={
            recipientCode: '',
            userName:'',
            date: '',
            startTime:'',
            endTime:'',
            endLimit:''
          }
        }
      
        var postdata = {
          firstRecipient:this.booking.recipientCode,
          userName:this.booking.userName,
          date:this.booking.date,
          startTime:this.booking.startTime,
          endTime:this.booking.endTime,
          endLimit:this.booking.endLimit,
          status:this.quicksearch.value.status,
          gender:this.quicksearch.value.gender=="Any Gender"? "MALE,FEMALE" : this.quicksearch.value.gender,
          staff:this.quicksearch.value.staff,
          brokers:this.quicksearch.value.brokers,
          volunteers:this.quicksearch.value.volunteers,
          onleaveStaff:this.quicksearch.value.onleaveStaff,
          searchText:this.quicksearch.value.searchText,
          
          allTeamAreas      : this.allProgarms,
          selectedTeamAreas : (this.allProgarms == false) ? this.selectedPrograms : [],
          
          allcat:this.allcat,
          selectedCategories:(this.allcat == false) ? this.selectedCategories : [],
          
          allBranches:this.allBranches,
          selectedbranches:(this.allBranches == false) ? this.selectedbranches : [],
          
          allCordinatore:this.allCordinatore,
          selectedCordinators:(this.allCordinatore == false) ? this.selectedCordinators : [],
          
          allSkills:(this.selectedSkills.length) ? false : true,
          selectedSkills: (this.selectedSkills.length) ? this.selectedSkills : [],
          criterias:this.cariteriaList
          // list of rules
        }
        
        let value:string;
        if (this.txtSearch!=null)
          value = this.txtSearch.toUpperCase();
            
        //console.log (this.selectedSkills);
        this.timeS.getQualifiedStaff(postdata).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          
          this.filteredResult = data;
          this.originalList=data;
          this.loading = false;
          this.cd.detectChanges();
          if (value!=null && value!=''){
            this.filteredResult=this.originalList.filter(element=>element.name.includes(value));
          }
        });
      }
    
      onTextChangeEvent(event:any){
        // console.log(this.txtSearch);
        let value = this.txtSearch.toUpperCase();
        if (this.originalList==null){
          this.searchData();
          return;
        }
         
         //console.log(this.serviceActivityList[0].description.includes(value));
         if (this.originalList.length>0)
          this.filteredResult=this.originalList.filter(element=>element.name.includes(value));
     }

     onItemSelected(sel:any ) : void {
      if (sel==null) return;
      this.selectAll=false;
      this.selectedStaff=sel; 
      if (this.allocated_staff_list.indexOf(sel)==-1)
        this.allocated_staff_list.push(sel.accountno);
      //console.log(this.selectedStaff)    
  
  }
  onItemDbClick(sel:any ) : void {
      if (sel==null) return;

      this.selectedStaff=sel;
      this.searchDone.emit(sel);
      this.findModalOpen=false;
  
  }
  
      allcompetencieschecked(): void {
        console.log("added");
        this.skillsList = this.skillsList.map(item => 
          (
            {
              ...item,
              checked: true
            }
            )
            );
          }
       
      allcompetenciesunchecked(): void {
        this.skillsList = this.skillsList.map(item => ({
          ...item,
          checked: false,
        }));
      }
      updateAllCheckedFilters(filter: any): void {
        console.log(this.testcheck + "test flag");
        if(filter == 1 || filter == -1){
          if(this.testcheck == false){  // why its returing undefined 
            if (this.allBranches) {
              this.branchesList.forEach(x => {
                x.checked = true;
              });
            }else{
              this.branchesList.forEach(x => {
                x.checked = false;
              });
            }
          }
        }
        if(filter == 2 || filter == -1){
          if(this.testcheck == false){
            if (this.allProgarms) {
              this.diciplineList.forEach(x => {
                x.checked = true;
              });
            }else{
              this.diciplineList.forEach(x => {
                x.checked = false;
              });
            }
          }
        }
        if(filter == 3 || filter == -1){
          if(this.testcheck == false){
            if (this.allCordinatore) {
              this.casemanagers.forEach(x => {
                x.checked = true;
              });
            }else{
              this.casemanagers.forEach(x => {
                x.checked = false;
              });
            }
          }
        }
        if(filter == 4 || filter == -1){
          if(this.testcheck == false){
            if (this.allcat) {
              this.categoriesList.forEach(x => {
                x.checked = true;
              });
            }else{
              this.categoriesList.forEach(x => {
                x.checked = false;
              });
            }
          }
        }
      }
      updateSingleCheckedFilters(index:number): void {
        if(index == 1){
          this.testcheck = false;
          if (this.branchesList.every(item => !item.checked)) {
            this.allBranches = false;
            this.allBranchIntermediate = false;
          } else if (this.branchesList.every(item => item.checked)) {
            this.allBranches = true;
            this.allBranchIntermediate = false;
          } else {
            this.allBranchIntermediate = true;
            this.allBranches = false;
          }
        }
        if(index == 2){
          this.testcheck = false;
          if (this.diciplineList.every(item => !item.checked)) {
            this.allProgarms = false;
            this.allprogramIntermediate = false;
          } else if (this.diciplineList.every(item => item.checked)) {
            this.allProgarms = true;
            this.allprogramIntermediate = false;
          } else {
            this.allprogramIntermediate = true;
            this.allProgarms = false;
          }
        }
        if(index == 3){
          this.testcheck = false;
          if (this.casemanagers.every(item => !item.checked)) {
            this.allCordinatore = false;
            this.allCordinatorIntermediate = false;
          } else if (this.casemanagers.every(item => item.checked)) {
            this.allCordinatore = true;
            this.allCordinatorIntermediate = false;
          } else {
            this.allCordinatorIntermediate = true;
            this.allCordinatore = false;
          }
        }
        if(index == 4){
          this.testcheck = false;
          if (this.categoriesList.every(item => !item.checked)) {
            this.allcat = false;
            this.allCatIntermediate = false;
          } else if (this.categoriesList.every(item => item.checked)) {
            this.allcat = true;
            this.allCatIntermediate = false;
          } else {
            this.allCatIntermediate = true;
            this.allcat = false;
          }
        }
      }
      openFindModal(){
      //  this.tabFindIndex = 0;
        
        this.updateAllCheckedFilters(-1);
        
        this.findModalOpen = true;
        
      }
    
      log(event: any,index:number) {
        this.testcheck = false;   
        if(index == 1)
        this.selectedbranches = event;
        if(index == 2)
        this.selectedPrograms = event;
        if(index == 3)
        this.selectedCordinators = event;
        if(index == 4)
        this.selectedCategories = event;  
        if(index == 5 && event.target.checked){
          this.searchAvaibleModal = true;
        }
      }
   
      setCriteria(){ 
        this.cariteriaList.push({
          fieldName  : this.extendedSearch.value.title,
          searchType : this.extendedSearch.value.rule,
          textToLoc  : this.extendedSearch.value.from,
          endText    : this.extendedSearch.value.to,
        })
      }

      rightClickMenu(event:any,data:any){
        event.preventDefault();
        this.showMenu=true;    
        this.selectedStaff=data;
      }

      
      toMap(){
        this.address=this.selectedStaff.address;
        if(this.address.length > 0){         
          window.open(`https://www.google.com/maps/dir///@${this.address}`,'_blank');

            // var adds = this.address.reduce((acc,data) => {
            //     var { postCode, address1, suburb, state } = data;
            //     var address = new Address(postCode, address1, suburb, state);
            //     return acc.concat('/',address.getAddress());                
            // }, []);
  //          console.log(adds)
    //        console.log(adds.join(''))
                
            //window.open('https://www.google.com/maps/search/?api=1&query=' + encoded,'_blank');
           // window.open(`https://www.google.com/maps/dir${adds.join('')}`,'_blank');
            return false;
        }
        this.globalS.eToast('No address found','Error');
    }
    toLastKnownMap(){
      if (this.selectedStaff.lastKnownLocation=='' || this.selectedStaff.lastKnownLocation==null)
        this.address=this.selectedStaff.address;
      else{
       
        let addr = this.selectedStaff.lastKnownLocation;
         addr=addr.replace("<Lattitude>","").replace("<Lattitude/>","")
         this.address=addr.replace("<Longditude>","").replace("<Longditude/>","")
      }
     
      if(this.address.length > 0){         
     
        window.open(`https://www.google.com/maps/dir///@${this.address}`,'_blank');

          // window.open(`https://www.google.com/maps/@${this.address}`);
        // var adds = this.address.reduce((acc,data) => {
        //       var { postCode, address1, suburb, state } = data;
        //       var address = new Address(postCode, address1, suburb, state);
        //       return acc.concat('/',address.getAddress());                
        //   }, []);
        //  console.log(adds)
      //    console.log(adds.join(''))
              
          //window.open('https://www.google.com/maps/search/?api=1&query=' + encoded,'_blank');
//          window.open(`https://www.google.com/maps/dir${adds.join('')}`,'_blank');
          return false;
      }
      this.globalS.eToast('No address found','Error');
  }

  Allocate(){
    this.AllocatedStaff.emit(this.allocated_staff_list);
    this.findModalOpen=false;
    this.allocated_staff_list=[];
  }
  staffList:Array<any>=[];
  gapList:Array<Staff>=[];

  load_all_staff(){
    this.loading = true;
    this.listS.getUnavailableStaffList().pipe(takeUntil(this.unsubscribe)).subscribe(data => {
   
   
      this.staffList = data.map(staff => {
       
        staff.startBlock = this.TimeToblock(staff.startTime);
        staff.endBlock = staff.startBlock + staff.duration;
        staff.color = this.gapColor(staff.rosterGroup, staff.serviceType);
        
        return staff;
      });
     
     this.gapList=[];
   
      this.setGapList();   
      this.cd.detectChanges ();
      this.loading = false;
      error=> {
       
        this.loading = false;
      }
  });
        
  
  }
  

setGapList(){
  let duplicates ;
  let durations ;
  this.staffList.forEach((item) => {
   if(this.gapList.filter(x=>x.accountNo==item.accountNo).length <=0){
    duplicates = this.staffList
      .filter(x => x.accountNo == item.accountNo)
      .map(x => { return { "startBlock": x.startBlock, "endBlock": x.endBlock, "color": x.color }; })
      .sort((a, b) => b.endBlock - a.endBlock);
    durations = this.setDuration(duplicates);
      this.gapList.push( {
        accountNo: item.accountNo,
        gaps: duplicates,
        durations: durations
      });
        
 
  }
  
  
  });
}

setDuration(duplicates: any) {
  let duration = [];
  let subDuration =[];
  let number = this.numbers;
  this.numbers.forEach(n=>{
    subDuration =[];
    this.durations.forEach(d=>{
      let t=duplicates.filter(x=> x.startBlock<=n*12+d && x.endBlock>=n*12+d && x.endBlock>0);
      if (t.length>0)
        subDuration.push(t[t.length-1].color?? '#fff');
      else
        subDuration.push('#fff');
      });
      duration.push(subDuration);
  });

  return duration;
}
  getDuplicates(list: any[], key: string, value:string) {
    return list.filter(
      (item, index, array) =>
        array.findIndex((obj) => obj[key] === item[key]) !== index
    );
  }
  
 
  getDuplicatesSimple(list: any[], key: string) {
    const seen = new Set();
    const duplicates = new Set();
  
    list.forEach((item) => {
      if (seen.has(item[key])) {
        duplicates.add(item[key]);
      } else {
        seen.add(item[key]);
      }
    });
  
    return list.filter((item) => duplicates.has(item[key]));
  }
  
  gapColor(type: any, serviceType: any) {
  //   let time='';
  //   if (data.startTime==null) return 'white';
  //     time = data.startTime;

  //   let num1 = this.TimeToblock(data.startTime);
  //   let duration = data.duration;
  //   let num2 = num*d;

  //  if (!(num2>=num1 && num2<=(num1+duration)) ) return 'white';

    //let type = data.rosterGroup;
    let color = 'white';
    switch (type) {
      case 'LEAVE':
        color = '#F18805'
        break;
      case 'ADMINISTRATION':
        if (serviceType == 'UNAVAILABLE')
          color = '#880404';
        else
          color = '#F18805';
        
        break;
      case 'UNAVAILABILITY':
        color = '#880404';
        break;
      case 'ONEONONE':
        color = '#85B9D5';
        break;
      default:
        color = '#FFF';
        break;
    }

    return color;
  }

  useMaster:boolean=false;
  expandAllDays:boolean=false;
  usingMaster($event){
    this.useMaster=$event;
  }
  exapndTable($event){
    this.expandAllDays=$event;
  }

  TimeToblock(startTime: any) {
    if (startTime==null) return 0;
    let time = startTime.split(':');
    
    let hrs = parseInt(time[0]);
    let min = parseInt(time[1]);
    
    let blocks =hrs * 12 + min/5;
     
    return blocks;
    
  }

  draggedItem: any = null;
  activeCell:number=0;
  column = 0;
  row=0;

  onCellClick(item: any,col:number, d:number, i:number) {
    this.column=col;
    this.activeCell=d;
    this.draggedItem=item;
    this.row=i;
    
  }

  onDragStart(event: DragEvent, item: any) {
    this.draggedItem = item;
    event.dataTransfer?.setData('text', JSON.stringify(item));
  }

  onDragOver(event: DragEvent) {
    event.preventDefault(); // Allow drop
  }

  onDrop(event: DragEvent, targetItem: any) {
    event.preventDefault();
     const draggedIndex = this.numbers.indexOf(this.draggedItem);
     const targetIndex = this.numbers.indexOf(targetItem);

    // Swap items in the grid
    if (draggedIndex > -1 && targetIndex > -1) {
      [this.numbers[draggedIndex], this.numbers[targetIndex]] = [
        this.numbers[targetIndex],
        this.numbers[draggedIndex]
      ];
    }
  }
  
}