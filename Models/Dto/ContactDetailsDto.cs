﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class ContactDetailsDto
    {
        public int RecordNumber { get; set; }
        public string Source { get; set; }
        public string State { get; set; }
        public string ContactType { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public string EquipmentCode { get; set; }
        public string Mobile { get; set; }
        public string SubType { get; set; }
        public string Postcode { get; set; }
        public string ContactName { get; set; }
        public bool EmergencyContact { get; set; }
        public string Group { get; set; }
        
    }
}
