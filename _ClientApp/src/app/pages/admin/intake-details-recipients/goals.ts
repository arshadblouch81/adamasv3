import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Component, OnInit,OnDestroy, ViewChild } from '@angular/core'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { BsModalComponent } from 'ng2-bs3-modal';
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, GlobalService, ListService, achievementIndex } from '@services/index'

@Component({
    selector: '',
    templateUrl:'./goals.html',
    styles:[`
        .option-wrap{
            width:20rem !important;
        }
    `]
})

export class IntakeGoals implements OnInit, OnDestroy{
    @ViewChild('addGoalModal', { static: false }) addGoalModal: BsModalComponent;

    dbAction: number=0;
    user:any;
    private goal$: Subscription;
    goals: Array<any>;
    loading: boolean;
    goalsArr: Array<string>
    achievementIndexArr: Array<string> = achievementIndex;
    goalGroup: FormGroup;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: SharedService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder
    ){

        this.goal$ =  this.sharedS.changeEmitted$.subscribe(data => {
            if(this.globalS.isCurrentRoute(this.router, 'goals'))
                this.search();
        });
    }

    ngOnInit(){
        this.resetGroup()
        this.reloadAll()
    }

    ngOnDestroy(){
        this.goal$.unsubscribe();
    }
    
    resetGroup(){
        this.goalGroup = this.formBuilder.group({
            recordNumber: '',
            personID: '',
            goal: new FormControl('',[Validators.required]),
            notes: '',
            achievementIndex: '',
            achievementDate: null,
            lastReviewed: null,
            dateAchieved: null,
            achievement: ''            
         })
    }

    reloadAll(){
        this.search();
        this.listDropDowns();
    }

    listDropDowns(){
        this.listS.getintakegoals(this.user.uniqueID)
                    .subscribe(data => this.goalsArr = data)
    }

    search(){
        this.loading = true;
        this.user = this.sharedS.getPicked();
        this.timeS.getgoals(this.user.uniqueID).subscribe(goals => {
            this.loading = false;
            this.goals = goals;
        });
    }

    goalProcess(){
        this.goalGroup.controls['personID'].setValue(this.user.uniqueID)
        const goal = this.goalGroup.value;

        if(this.dbAction == 0){
            this.timeS.postgoals(goal)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll();
                                this.globalS.sToast('Success','Goal Inserted')
                            }
                        })
        }

        if(this.dbAction == 1){
            this.timeS.updategoals(goal)
                        .subscribe(data =>{
                            if(data){
                                this.reloadAll();
                                this.globalS.sToast('Success','Goal Inserted')
                            }
                        })
        }
    }

    updategoal(data: any){
        this.addGoalModal.open();
   
        this.goalGroup.patchValue({
            recordNumber: data.recordNumber,
            goal: data.goal,
            notes: data.notes,
            achievementIndex: data.achievementIndex != null ? data.achievementIndex.toUpperCase() : '',
            achievementDate: data.achievementDate,
            lastReviewed: data.lastReviewed,
            dateAchieved: data.dateAchieved,
            achievement: data.achievement           
         })
    }

    delete(data: any){
        this.timeS.deletegoals(data.recordNumber).subscribe(data => {
            if(data){
                this.reloadAll();
                this.globalS.sToast('Success','Goal Deleted')
            }
        })
    }
}