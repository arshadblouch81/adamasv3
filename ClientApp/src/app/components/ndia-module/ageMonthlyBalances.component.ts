import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { debounceTime, timeout } from 'rxjs/operators';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';

import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-ndia-age-monthly-balances',
  templateUrl: './ageMonthlyBalances.component.html',
  styleUrls: ['./ageMonthlyBalances.component.css'],
})
export class AgeMonthlyBalancesComponent implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;
  operatorID: any;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  loading: boolean = false;
  token: any;
  check: boolean = false;
  userRole: string = "userrole";
  id: string;
  private unsubscribe: Subject<void> = new Subject();
  dateFormat: string = 'dd/MM/yyyy';
  title: string = "Package Period End Aging";
  tableData: Array<any>;
  updatedRecords: any;

  batchNumber: any;
  startDate: any;
  endDate: any;
  currentDateTime: any;
  packageBalancesDate: any;
  packageTypesChecked: any = true;
  lockpackageTypes: any = false;
  batchHistoryList: Array<any>;
  sPackagesList: Array<any>;
  sPackageAccountNo: any;
  sPackageName: any;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.userRole = this.token.role;
    this.buildForm();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.loading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/ndia']);
  }

  buildForm() {
    this.inputForm = this.formBuilder.group({
      packageBalancesDate: '01/12/2021',
    });

    this.loading = true;
    this.billingS.getPackageBalancesDate().subscribe(dataPackageBalancesDate => {
      this.packageBalancesDate = dataPackageBalancesDate[0].packageBalancesDate;
      this.inputForm.patchValue({
        packageBalancesDate: this.packageBalancesDate,
      });
    });
    this.billingS.getPaysBatchHistory().subscribe(dataBatchHistory => {
      this.batchHistoryList = dataBatchHistory;
      this.tableData = dataBatchHistory;
    });
    this.loading = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  runProcess() {
    this.loading = true;

    var now = new Date();
    this.startDate = new Date(now.getFullYear(), now.getMonth() - 1, 1);
    this.endDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth() + 1, 0);
    this.startDate = formatDate(this.startDate, 'yyyy-MM-dd', 'en_US');
    this.endDate = formatDate(this.endDate, 'yyyy-MM-dd', 'en_US');
    this.packageBalancesDate = this.inputForm.get('packageBalancesDate').value;
    this.packageBalancesDate = formatDate(this.packageBalancesDate, 'yyyy/MM/dd', 'en_US');

    this.billingS.getBatchRecord(null).subscribe(dataBatch => {
      if (dataBatch) {
        this.batchNumber = dataBatch[0].batchRecordNumber;
        this.batchNumber = this.batchNumber + 1;
        this.billingS.insertBatchRecord({
          actionCode: this.batchNumber,
          actionDescription: 'I',
          Operator: this.token.nameid,
          actionDate: this.globalS.getCurrentDateTime(),
        }).pipe(takeUntil(this.unsubscribe)).subscribe(dataInsertedBatch => {
          if (dataInsertedBatch) {
            this.billingS.getSystableRecord(null).subscribe(dataSystable => {
              if (dataSystable) {
                this.billingS.insertPayBillBatch({
                  Operator: this.token.nameid,
                  actionDate: this.globalS.getCurrentDateTime(),
                  actionCode: this.batchNumber,
                  actionOn: 'NDIA Aging',
                  actionDescription: 'I',
                  actionStartDate: this.startDate,
                  actionEndDate: this.endDate,
                }).pipe(takeUntil(this.unsubscribe)).subscribe(dataInsertedPayBillBatch => {
                  if (dataInsertedPayBillBatch) {
                    // this.billingS.getSPackagesDetail(null).subscribe(dataSPackagesDetail => {
                    //   this.sPackagesList = dataSPackagesDetail;
                    //   if (dataSPackagesDetail.length > 0) {
                    //     let i = 0;
                    //     while (i < dataSPackagesDetail.length) {
                    //       this.sPackageAccountNo = dataSPackagesDetail[i].sPackages.substr(0, dataSPackagesDetail[i].sPackages.iOf('<==>'));
                    //       this.sPackageName = dataSPackagesDetail[i].sPackages.split('<==>')[1];
                    //       console.log("At " + i + " #, UniqueID is: " + this.sPackageName)
                    //       console.log("At " + i + " #, Recordnumber is: " + this.sPackageAccountNo)
                    //       console.log("=============================================");
                    //       i++;
                    //     }
                    //   }
                    // });
                    this.billingS.processAgePackageBalance({
                      actionCode: this.batchNumber,
                      actionOn: 'NDIA',
                      otherDate: formatDate(this.packageBalancesDate, 'yyyy/MM/dd', 'en_US'),
                      actionStartDate: formatDate(this.startDate, 'yyyy/MM/dd', 'en_US'),
                      actionEndDate: formatDate(this.endDate, 'yyyy/MM/dd', 'en_US'),
                    }).pipe(takeUntil(this.unsubscribe)).subscribe(dataSuccessProcess => {              
                      if (dataSuccessProcess) {
                        this.updatedRecords = dataSuccessProcess[0].updatedRecords;
                        if (this.updatedRecords == 0) {
                          this.globalS.iToast('Information', 'There are no records to update.')
                        } else {
                          this.globalS.sToast('Success', this.updatedRecords + ' - Records Updated.')
                        }
                        this.ngOnInit();
                        return false;
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });

    this.loading = false;
  }

  validateData(): void {
    this.operatorID = this.token.nameid;
    this.batchNumber = this.batchNumber + 1;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');
    this.packageBalancesDate = this.inputForm.get('packageBalancesDate').value;
    this.packageBalancesDate = formatDate(this.packageBalancesDate, 'yyyy/MM/dd', 'en_US');

    if (this.packageBalancesDate != '') {
      this.globalS.sToast('Success', 'Data updated!')
    } else if (this.packageBalancesDate == '') {
      this.globalS.eToast('Error', 'Please select date to proceed')
    }
  }
}

