using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Adamas.Models.Tables {
    public class Medias
    {
        [Key]
        public int Media_Id { get; set; }
        [Required]
        public string MediaDisplay { get; set; }
        [Required]
        public string MediaText { get; set; }
        [Required]
        public string Media { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public string Program { get; set; }
        public string Item { get; set; }
        public string Target { get; set; }
        public string MediaGroup { get; set; }
        public string Type { get; set; }
    }
}