using System.Text.Json;
using System.Text.Json.Serialization;
using System;

namespace Adamas.Converters
{
    public class DateTimeOffsetConverter : JsonConverter<DateTimeOffset>
    {
        public override DateTimeOffset Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options) =>
                DateTime.Parse(reader.GetString());

        public override void Write(
            Utf8JsonWriter writer,
            DateTimeOffset dateTimeValue,
            JsonSerializerOptions options) =>
                writer.WriteStringValue(dateTimeValue.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss"));
        
    }
}