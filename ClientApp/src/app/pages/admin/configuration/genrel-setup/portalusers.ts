import { ChangeDetectorRef, Component, OnInit,HostListener } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ListService } from '@services/list.service';
import { MenuService } from '@services/menu.service';
import { GlobalService } from '@services/global.service';
import { SwitchService } from '@services/switch.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import { PrintService } from '@services/print.service';
import { NavigationService, TimeSheetService } from '@services/index';
import { Router } from '@angular/router';


@Component({
  selector: 'app-portalusers',
  templateUrl: './portalusers.html',
  styles: [`
  tr.highlight {
    background-color: #85B9D5 !important; /* Change to your preferred highlight color */
  }
  `]
})
export class PortalUsers implements OnInit {
  
  tableData: Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  modalVariables: any;
  dateFormat: string ='dd/MM/yyyy';
  inputVariables:any;
  inputForm: FormGroup;
  postLoading: boolean = false;
  isUpdate: boolean = false;  
  private unsubscribe: Subject<void> = new Subject();
  token:any;
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;  
  check : boolean = false; 
  userRole:string="userrole";
  temp_title: any;
  searchTerm: string = '';
  currentIndex: number | null = null;

  constructor(
    private globalS: GlobalService,
    private cd: ChangeDetectorRef,
    private listS:ListService,
    private printS:PrintService,
    private menuS:MenuService,
    private timeS:TimeSheetService,
    private switchS:SwitchService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private ModalS: NzModalService,
    private navigationService: NavigationService,
    private router: Router
    ){}
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.loadData();
      this.loading = false;
      this.cd.detectChanges();
    }
    
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    }
  
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    handleCancel() {
      this.modalOpen = false;
    }
    
    loadData(){
      this.loading = true;
      this.menuS.getloggedPortalUsers().subscribe(data => {
        this.tableData = data;
        this.loading = false;
      });
    }
    buildForm() {
      this.inputForm = this.formBuilder.group({
        fclass: '',
        end_date:'',
        recordNumber:null
      });
    }
    handleOkTop() {
      this.generatePdf();
      this.tryDoctype = ""
      this.pdfTitle = ""
    }
    handleCancelTop(): void {
      this.drawerVisible = false;
      this.pdfTitle = ""
    }
    generatePdf(){
      this.drawerVisible = true;
      
      this.loading = true;
      
      var fQuery = "Select [User] as Field1,[BrowserName] as Field2 from LoggedUsers";
      
      const data = {
        "template": { "_id": "0RYYxAkMCftBE9jc" },
        "options": {
          "reports": { "save": false },
          "txtTitle": "Brower User Listing",
          "sql": fQuery,
          "userid":this.tocken.user,
          "head1" : "Windows User Name",
          "head2" : "Browser",
        }
      }
      
      this.printS.printControl(data).subscribe((blob: any) => {
        let _blob: Blob = blob;
        let fileURL = URL.createObjectURL(_blob);
        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
        this.loading = false;
        this.pdfTitle ="Browser Users.pdf"
      }, err => {
        this.loading = false;
        this.ModalS.error({
          nzTitle: 'TRACCS',
          nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
          nzOnOk: () => {
            this.drawerVisible = false;
          },
        });
      });
      
      this.loading = true;
      this.tryDoctype = "";
      this.pdfTitle = "";
      
    }
    goBack(): void {
      const previousUrl = this.navigationService.getPreviousUrl();
      const previousTabIndex = this.navigationService.getPreviousTabIndex();
    
      if (previousUrl) {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      } else {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      } 
    }
    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
      // Handle character input
      if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
          this.searchTerm += event.key.toLowerCase();
          this.scrollToRow();
      } 
      // Handle backspace
      else if (event.key === 'Backspace') {
          // Only reset if there's something to remove
          this.searchTerm = '';
          if (this.searchTerm.length > 0) {
              this.searchTerm = this.searchTerm.slice(0, -1);
              this.scrollToRow();
          }
      }
      // Handle other keys (optional)
      else if (event.key === 'Escape') {
          // Reset search term on escape key
          this.searchTerm = '';
          this.scrollToRow();
      }
    }

    scrollToRow() {
        // Find the index of the first matching item
        const matchIndex = this.tableData.findIndex(item => 
            item.windowsUserName.toLowerCase().startsWith(this.searchTerm)
        );
        // Scroll to the matching row if it exists
        if (matchIndex !== -1) {
            const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
            if (tableRow) {
                tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                this.highlightRow(tableRow);
            }
        } else {
        }
    }

    highlightRow(row: HTMLElement) {
        row.classList.add('highlight');
        setTimeout(() => row.classList.remove('highlight'), 2000);
    }
  }
  