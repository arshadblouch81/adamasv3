using System;

namespace Adamas.Models.Modules
{
    public static class Validators
    {
        public static DateTime? ValidateDate(DateTime? value)
        {
            if (value == null)
            {
                return null;
            }
            else
            {
                return value;
            }
        }

        public static T ValidateValue<T>(object value)
        {
            if (value == null)
            {
                return default(T);
            }

            if (typeof(T) == typeof(Int32)) return (T)(object)value;
            if (typeof(T) == typeof(Boolean)) return (T)(object)value;
            if (typeof(T) == typeof(DateTime)) return (T)(object)value;
            if (typeof(T) == typeof(String)) return (T)(object)value;

            return default;
        }
    }
}