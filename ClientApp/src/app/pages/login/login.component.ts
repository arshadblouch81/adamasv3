import { AfterViewInit, Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { LoginService, GlobalService, TYPE_MESSAGE, TimeSheetService, JsreportService, TimerService } from '@services/index';
import { SettingsService } from '@services/settings.service';
import { ApplicationUser } from '@modules/modules';

import {
  Router
} from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { XeroService } from '@services/XeroService';
import { MsalService } from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;
  loading: boolean = false;
  unauthorized: boolean = false;
  unauthorizedStr: string;
  processing:boolean;
  logoPath: any;
  version:string;

  submitForm(): void {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }
  }

  constructor(
    private fb: FormBuilder,
    private loginS: LoginService,
    private globalS: GlobalService,
    private settingS: SettingsService,
    private timeS: TimeSheetService,
    private sanitizer: DomSanitizer,
    private jsreportS: JsreportService,
    private router: Router,
    private xero:XeroService,
    private authService: MsalService,
    private timerS: TimerService,
    private http: HttpClient
  ) { }



  ngOnInit(): void {    

    this.loginForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });

    // const userAgent = navigator.userAgent;
    // const match = userAgent.match(/Windows NT ([0-9.]+); Win32; (.*)/);
    // if (match) {
    //   const username = match[2];
    //   console.log('Logged-in Windows user:', username);
    // } else {
    //   console.error('Could not extract Windows username from user agent');
    // }

    // const platform = window.navigator.platform;


    // this.timeS.getbrandinglogo().subscribe(blob => {
    //   let objectURL = 'data:image/jpeg;base64,' + blob;
    //   this.logoPath = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    // })

    if (this.isLoggedIn()){
      this.globalS.setMicrosoftLoginService=this.authService;
      let account =this.authService.instance.getActiveAccount();
      this.loginS.getuserwithmsaccount(account.username).subscribe(res=>{
        let data:any=res;

          if (res!=null){
            this.loginForm.patchValue({
                  userName:data.name,
                  password:data.password
                });
            this.login_after_microsoft_authentication(data);
          }
          
        });
    }

    this.authService.instance.handleRedirectPromise().then(
        res=>{
          if (res!=null && res.account!=null){
            this.authService.instance.setActiveAccount(res.account);
          }
    });

    this.getCurrentVersion();
  }

  ngAfterViewInit(): void {
      this.getCurrentVersion();
  }

  getCurrentVersion(): void {
    this.loginS.getcurrentversion().subscribe(d => this.version = d);
  }

  isLoggedIn(): boolean {
    return this.authService.instance.getActiveAccount() != null
  }

   user:any;
  login_microsoft(){    
    this.processing=true;
   // this.authService.loginRedirect();
    this.authService.loginPopup().subscribe(async (response: AuthenticationResult) => {
      this.authService.instance.setActiveAccount(response.account);
      this.globalS.setMicrosoftLoginService=this.authService;

    //   this.user = {
    //     userName: response.account.username,
    //     Password: '',
    //     NewPassword: '',
    //     PasswordHandler: '',
    //     Bypass: ''
    // }
      this.loginS.getuserwithmsaccount(response.account.username).subscribe(res=>{
        let data:any=res;

          if (res!=null){
            this.loginForm.patchValue({
                  userName:data.name,
                  password:data.password
                });
            this.login_after_microsoft_authentication(data);
          }
          
        });
     },
     error => {
      
       alert('User cancel the request');
             
      this.processing=false;
     })

     
  }

  logout() {
    this.authService.logout();
    this.reset();
  }

  login_after_microsoft_authentication(muser:any) {

       
    if (!this.isLoggedIn()) return;
   // if (!this.loginForm.valid && ) return;
    this.loading = true;

    let user: ApplicationUser = {
      Username: muser.name, 
      Password: muser.password 
    }
   
    this.settingS.getSettingsObservable(user.Username).pipe(
      switchMap(x => {
        this.globalS.settings = x;
        this.globalS.originalSettings = x;

        return this.jsreportS.getconfiguration()
      }),
      switchMap(x => {
        this.globalS.jsreportSettings = x;
        return this.loginS.login(user);
      }) 
    ).subscribe(data => {
        this.globalS.token = data.access_token;
        if (this.globalS.redirectURL) {
          this.globalS.ISTAFF_BYPASS = 'true';
          this.router.navigateByUrl(this.globalS.redirectURL);
          return;
        }       

        setTimeout(() => {
          this.globalS.viewRender(this.globalS.token);
          this.processing=false;
        }, 200);

        this.reset();
       

      }, (error: HttpErrorResponse) => {
        this.unauthorized = true;

        this.globalS.createMessage(TYPE_MESSAGE.error,'The credentials you entered is incorrect');
        if (error.status == 401) this.unauthorizedStr = 'Invalid user name or password';
        if (error.status == 400) this.unauthorizedStr = error.error.message;
        this.reset();
        throw (error);
      });
  }



  login() {

    if (!this.loginForm.valid  ) return;
    this.loading = true;

    let user: ApplicationUser = {
      Username: this.loginForm.get('userName').value,
      Password: this.loginForm.get('password').value,
      BrowserName: this.globalS.getBrowserType()
    }
   
    this.settingS.getSettingsObservable(user.Username).pipe(
      switchMap(x => {
        this.globalS.settings = x;
        this.globalS.originalSettings = x;

        return this.jsreportS.getconfiguration()
      }),
      switchMap(x => {
        this.globalS.jsreportSettings = x;
        return this.loginS.login(user);
      }) 
    ).subscribe(data => {
        this.timerS.setInterval = data.expires * 1000 * 60;
        this.globalS.token = data.access_token;

        this.globalS.access = data;

        if (this.globalS.redirectURL) {
          this.globalS.ISTAFF_BYPASS = 'true';
          this.router.navigateByUrl(this.globalS.redirectURL);
          return;
        }       

        setTimeout(() => {
          this.globalS.viewRender(this.globalS.token);
        }, 200);

        this.reset();
       
        this.timerS.startTimer();
        this.timerS.startActivityTimer();
      }, (error: HttpErrorResponse) => {
        this.unauthorized = true;

        this.globalS.createMessage(TYPE_MESSAGE.error,'The credentials you entered is incorrect');
        if (error.status == 401) this.unauthorizedStr = 'Invalid user name or password';
        if (error.status == 400) this.unauthorizedStr = error.error.message;
        this.reset();
        throw (error);
      });
  }

  reset() {
    this.loading = false;
    this.loginForm.reset();
    setTimeout(() => {
      this.unauthorized = false;
    }, 2000);
  }

}
