namespace Adamas.Models.Modules{
    public class Contacts {
        public int? RecordNumber { get; set; }
        public string PrimaryContact { get; set; }
        public string ContactType { get; set; }
        public string Contact { get; set; }
        public bool Primary { get; set; }

        public Contacts Create()
        {
            return new Contacts
            {
                RecordNumber = RecordNumber,
                PrimaryContact = PrimaryContact,
                ContactType = ContactType,
                Contact = Contact
            };
        }

        internal static Contacts Create(Contacts contactDetails)
        {
            return new Contacts
            {
                RecordNumber = contactDetails.RecordNumber,
                PrimaryContact = contactDetails.PrimaryContact,
                ContactType = contactDetails.ContactType,
                Contact = contactDetails.Contact,
               
            };
        }
    }
}