import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, TimeSheetService, ShareService, leaveTypes, ClientService,PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
    styles: [`
        ul{
            list-style:none;
        }

        div.divider-subs div{
            margin-top:2rem;
        }
        .spinner{
            margin:1rem auto;
            width:1px;
        }
        .selected{
        background-color: #85B9D5;
        color: white;
    }
    nz-table th{
    font-weight: 600;
    font-family: 'Segoe UI';
    font-size: 14px;
    border: 1px solid #f3f3f3;
}
    `],
    templateUrl: './perm-roster.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class RecipientPermrosterAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;

    checked: boolean = false;
    isDisabled: boolean = false;
    loading: boolean = false;

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    selectedRowIndex:any;
    activeRowData:any;
    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];
    timeSheetVisible:Subject<any> = new Subject<boolean>();
    openModel:boolean;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private cd: ChangeDetectorRef,
        private printS: PrintService,
        private sanitizer: DomSanitizer,
        private modalService: NzModalService
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'perm-roster')) {
                this.user = data;
                this.search(data);
            }
        });
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.user = this.sharedS.getPicked();
        this.search(this.user);
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
    showEditModal(data:any, i:number){
        this.selectedRowIndex=i;
        this.activeRowData=data;
       this. find_roster(data.recordNumber)
      
       // this.loadComponentAsync(data);
    }
    find_roster(RecordNo:number):any{
        let rst:any;
     

    this.timeS.getrosterRecord(RecordNo).pipe(takeUntil(this.unsubscribe)).subscribe(r => {;
        
        rst = {
         
            "shiftbookNo": r.recordNo,
            "date": r.roster_Date,
            "startTime": r.start_Time,
            "endTime":    r.end_Time,
            "duration": r.duration,
            "durationNumber": r.dayNo,
            "recipient": r.clientCode,
            "program": r.program,
            "activity": r.serviceType,
            "payType": r.payType,   
            "paytype": r.payType.paytype,  
            "pay": r.pay,                   
            "bill": r.bill,            
            "approved": r.Approved,
            "billto": r.billedTo,
            "debtor": r.billedTo,
            "notes": r.notes,
            "selected": false,
            "serviceType": r.type,
            "recipientCode": r.clientCode,            
            "staffCode": r.carerCode,  
            "serviceActivity": r.serviceType,
            "serviceSetting": r.serviceSetting,
            "analysisCode": r.anal,
            "serviceTypePortal": "",
            "status": r.status,
            "recordNo": r.recordNo
            
        }
        this.timeSheetVisible.next(rst);
        this.openModel=true;
        this.cd.detectChanges();
    });
        
    
  
} 

  shiftChanged(event:any){

    }

    selectData(data:any, i:number){
        this.activeRowData = data;
        this.selectedRowIndex = i;
    }
    search(user: any = this.user) {
        this.loading = true;
        this.clientS.getpermroster(user.code).subscribe(data => {
            this.tableData = data.list;
            this.originalTableData = this.tableData;
            this.loading = false;
            this.cd.markForCheck();
        });
    }

    trackByFn(index, item) {
        return item.id;
    }

    //Mufeed 10 April 2024
    handleCancelTop(){
        this.drawerVisible = false;
        this.pdfTitle = "";
        this.tryDoctype="";
    }    
    InitiatePrint(){

        var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
        
        fQuery = " SELECT  [Recordno] As [RecordNumber],[Client Code] As [Recipient],FORMAT(convert(datetime,[Date]), 'dd/MM/yyyy') As [Date], format (convert(datetime,[Start Time] ,108),'hh:mm tt')As [Start Time], (([Duration] * 5)/60) As [Duration],[Service Type] As [Activity],[Carer Code] As [Staff], [Anal] As [Analysis],[Program] As [Program],[HACCType] As [Dataset Type], ([CostQty] * [Unit Pay Rate]) As [Service Cost] FROM Roster WHERE [Client Code] =  '"+this.user.code+"' AND [Date] BETWEEN '1900/01/01' AND '1905/05/28' ORDER BY Date, [Start Time]  ";
        //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
        txtTitle = "Permanent Rosters ";       
        Rptid =   "sbrWZJ79LiEpfCb5"  

        //console.log(fQuery)
        const data = {
                    
            "template": { "_id": Rptid },                                
            "options": {
                "reports": { "save": false },                
                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "txtTitle": txtTitle,
                "Extra": this.user.code,
                                                                
            }
        }
        this.loading = true;
        this.drawerVisible = true;         
            this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = txtTitle+".pdf"
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();
            }, err => {
                console.log(err);
                this.loading = false;
                this.modalService.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            return;      
    }



    columnDictionary = [{
        key: 'Start Time',
        value: 'startTime'
    },{
        key: 'Duration',
        value: 'duration'
    },{
        key: 'Activity',
        value: 'activity'
    },
    ,{
        key: 'Staff',
        value: 'staffLocation'
    }
    ,{
        key: 'Program',
        value: 'program'
    } 
    ,{
        key: 'Dataset Type',
        value: 'dataSet'
    }
    ,{
        key: 'Service Cost',
        value: 'serviceCost'
    }

];
    
    
    

    dragDestination = [
        'Start Time',
        'Duration',
        'Activity',
        'Staff',
        'Program',
        'Dataset Type',
        'Service Cost'
    ];


    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }
}