using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Unicode;
using System.Drawing;

namespace Adamas.Models.Modules
{
    public class Staff
    {
   
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string AccountNo  { get; set; }
        public string Title { get; set; }
        public string DOB { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public string KM { get; set; }
        public string Price { get; set; }
        public string SpecialSkill { get; set; }
        public string FilePhoto { get; set; }
        public string Rating { get; set; }
        public string SpecialSkill_Short { get; set; }
        public string SpecialSkill_lined { get; set; }
        public string agency_medal { get; set; }
        public string Reviewes { get; set; }
    }
}
