using System;

namespace Adamas.Models.Tables {
    public class UpdateRecord
    {
        public string Operator { get; set; }
        public DateTime? actionDate { get; set; }
        public string actionStartDate { get; set; }
        public string actionEndDate { get; set; }
        public string otherDate { get; set; }
        public string actionOn { get; set; }
        public string actionCode { get; set; }
        public string actionUser { get; set; }
        public string actionDescription { get; set; }
        public string value1 { get; set; }
        public string value2 { get; set; }
        public string value3 { get; set; }
        public string value4 { get; set; }
        public string recordNumber { get; set; }
    }
}    