﻿using System;
using System.Threading.Tasks;

namespace AdamasV3.DAL
{
    public interface ILogService
    {
        Task InsertError(Exception e);
        Task InsertError(Exception e, string custom);
    }
}
