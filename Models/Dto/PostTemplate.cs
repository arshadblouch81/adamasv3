﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class PostTemplate
    {
        public string User { get; set; }
        public string PersonID { get; set; }
        public string OriginalFileName { get; set; }
        public string NewFileName { get; set; }
        public string Path { get; set; }

        public string Title { get; set; }
        public string Program { get; set; }
        public string Discipline { get; set; }
        public string CareDomain { get; set; }
        public string Classification { get; set; }
        public string Category { get; set; }
        public string ReminderDate { get; set; }
        public string ReminderText { get; set; }
        public string PublishToApp { get; set; }
        public string Notes { get; set; }
        public string DocumentType { get; set; }

    }
}
