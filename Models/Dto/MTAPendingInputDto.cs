﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class MTAPendingInputDto
    {
        public string Date { get; set; }
        public int LocalTimezoneOffset { get; set; }
        public string Branches { get; set; }
        public string Coordinators { get; set; }
        public string Categories { get; set; }
        public string Teams { get; set; }
        
        
        public int TAType { get; set; }
        public string GenerateDateString()
        {
            return string.Format("'{0}'", Date);
        }

        public string GenerateCategoryString()
        {
            return string.Join(",", Categories.Split(',').Select(x => string.Format("'{0}'", x)).ToList());
        }

        public string GenerateBranchesString()
        {
            return string.Join(",", Branches.Split(',').Select(x => string.Format("'{0}'", x)).ToList());
        }

        public string GenerateCoordinatorString()
        {
            return string.Join(",", Coordinators.Split(',').Select(x => string.Format("'{0}'", x)).ToList());
        }
    }
}
