using System;

namespace Adamas.Models.DTO{
    public class QuoteLineDTO {
        public int? RecordNumber { get; set; }
        public int? DocHdrId { get; set; }
        public int? LineNo { get; set; }
        public int? COID { get; set; }
        public int? BRID { get; set; }
        public int? DPID { get; set; }
        public int? ItemId { get; set; }
        public string DisplayText { get; set; }
        public double? Qty { get; set; }
        public string Frequency { get; set; }
        public int? LengthInWeeks { get; set; }
        public decimal? QuoteQty { get; set; }
        public string BillUnit { get; set; }
        public decimal? UnitBillRate { get; set; }
        public decimal? Tax { get; set; }
        public string PriceType { get; set; }
        public string QuotePerc { get; set; }
        public string BudgetPerc { get; set; }
        public string Roster { get; set; }
        public int? StrategyId { get; set; }
        public string RCycle { get; set; }
        public string Notes { get; set; }
        public int SortOrder { get; set; }

        // EXTRA
        public string ServiceType { get; set; }
        public string Code { get; set; } 
        public string MainGroup { get; set; }

    }
}