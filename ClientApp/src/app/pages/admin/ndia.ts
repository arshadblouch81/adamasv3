import { Component, OnInit, OnDestroy, Input, AfterViewInit, ChangeDetectorRef } from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ListService, PrintService, GlobalService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin } from 'rxjs';


const inputFormDefault = {

    branchesArr: [[]],
    allBranches: [true],

    recipientArr: [[]],
    allRecipients: [true],

    inputBatchnNumber: [1],
    packagesArr: [[]],
    allPackages: [true],
    batchNoArr: [[]],
    batchclientsArr: [[]],
    batchprogramArr: [[]],
    batchPatientCodesArr: [[]],
}
//Sets defaults of Criteria Model

@Component({
    styleUrls: ['./billing.css'],
    templateUrl: './ndia.html',
    selector: 'NDIAAdmin',
})
export class NDIAAdmin implements OnInit, OnDestroy {

    ndiaClaimUpdateOpen: any;

    NDIA_isVisibleTop = false;
    NDIA_BatchNumModal = false;
    NDIA_ModalName: string;
    NDIA_modalbodystyle: object;
    id: string;
    btnid: string;
    inputForm: FormGroup;
    drawerVisible: boolean = false;
    loading: boolean = false;
    frm_Recipients: boolean;
    frm_BatchNo: boolean;
    frm_InpBatchNo: boolean;
    frm_Branches: boolean;
    frm_Packages: boolean;
    frm_Date: boolean;
    enddate: Date;
    startdate: Date;
    branchesArr: Array<any> = [];
    recipientArr: Array<any> = [];
    packagesArr: Array<any> = [];
    batchNoArr: Array<any> = [];
    batchclientsArr: Array<any> = [];
    batchprogramArr: Array<any> = [];
    batchPatientCodesArr: Array<any> = [];
    dateFormat: string = 'dd/MM/yyyy'
    tryDoctype: any;
    tocken: any;
    pdfTitle: string;
    inputBatchnNumber: number;
    s_BranchSQL: string;
    s_PackageSQL: string;
    s_RecipientSQL: string;
    s_DateSQL: string;
    s_BatchSQL: string;
    eventID: string;
    rollbackClaimBatch: any;
    rollbackAgingBatch: any;
    createNDIAInvoice: any;
    ageMonthlyBalances: any;
    createClaimUpload: any;
    importPriceGuide: any;

    format1 = (): string => `1`;
    format2 = (): string => `2`;
    format3 = (): string => `3`;
    format4 = (): string => `4`;
    format5 = (): string => `5`;
    format6 = (): string => `6`;
    CircleSize = 26;

    constructor(
        private fb: FormBuilder,
        private listS: ListService,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private printS: PrintService,
        private GlobalS: GlobalService,
    ) {
    }

    ngOnInit() {

        var date = new Date();
        this.startdate = new Date(date.getFullYear(), date.getMonth(), 1);
        this.enddate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        this.inputForm = this.fb.group(inputFormDefault);

        this.inputForm.get('allBranches').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                branchesArr: []
            });
        });
        this.inputForm.get('allRecipients').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                recipientArr: []
            });
        });
        this.inputForm.get('allPackages').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                packagesArr: []
            });
        });
    }

    ngAfterViewInit(): void {
        this.listS.getreportcriterialist({
            listType: 'BRANCHES',
            includeInactive: false
        }).subscribe(x => this.branchesArr = x);
        this.listS.GetRecipientAll().subscribe(x => this.recipientArr = x);
        this.listS.GetNDIApackages().subscribe(x => this.packagesArr = x);
        this.listS.GetBatchNo().subscribe(x => { this.batchNoArr = x; });
        this.tocken = this.GlobalS.pickedMember ? this.GlobalS.GETPICKEDMEMBERDATA(this.GlobalS.GETPICKEDMEMBERDATA) : this.GlobalS.decode();
    }

    ngOnDestroy(): void {

    };

    clickEvent(i) {
        i = i || window.event;
        i = i.target || i.srcElement;
        this.eventID = (i.id).toString();
        // console.log(this.eventID)
        switch (this.eventID) {
            case 'btn-rollbackClaimBatch':
                this.rollbackClaimBatch = {}
                break;
            case 'btn-rollbackAgingBatch':
                this.rollbackAgingBatch = {}
                break;
            case 'btn-createNDIAInvoice':
                this.createNDIAInvoice = {}
                break;
            case 'btn-ageMonthlyBalances':
                this.ageMonthlyBalances = {}
                break;
            case 'btn-createClaimUpload':
                this.createClaimUpload = {}
                break;
            case 'btn-importPriceGuide':
                this.importPriceGuide = {}
                break;
        
            default:
                break;
        }
    }

    view(index: string) {
        if (index == 'ndia-claim') this.ndiaClaimUpdateOpen = {}
    }

    //Mufeed
    Prompt_NDIA_Modal(e) {
        //console.log(e)
        e = e || window.event;
        e = e.target || e.srcElement;
        this.btnid = (e.id).toString();

        switch (this.btnid) {
            case 'btn-reviewUnclaimedSvc':

                //this.NDIA_modalbodystyle = { height:'520px', overflow: 'auto'}
                this.NDIA_ModalName = "NDIA Unclaimed Items Report"
                this.frm_Date = true;
                this.frm_Branches = true;
                this.frm_Recipients = true;
                this.NDIA_isVisibleTop = true;
                break;
            case 'btn-ndiaBatchRegister':
                //this.NDIA_modalbodystyle = {  width: '200px', overflow: 'auto'}
                this.NDIA_ModalName = "NDIA Batch Number"
                this.frm_InpBatchNo = true;
                this.NDIA_isVisibleTop = true;

                break;
            case 'btn-NDIA-Packagestatement':
                this.NDIA_ModalName = "NDIA Package Statement"
                this.frm_BatchNo = true;
                this.frm_Branches = true;
                this.frm_Packages = true;
                this.NDIA_isVisibleTop = true;

                break;

            default:
                console.log(this.btnid)
                break;
        }
    }
    NDIA_MOdalCancel() {
        this.NDIA_isVisibleTop = false;
        this.NDIA_BatchNumModal = false;
        this.drawerVisible = false;
        this.ResetVisibility();

    }
    NDIA_MOdalOk() {
        this.ReportRender(this.btnid)
        this.ResetVisibility();
        this.NDIA_isVisibleTop = false;
        this.NDIA_BatchNumModal = false;
        this.tryDoctype = "";
        this.btnid = "";

    }
    ReportRender(btnid) {
        var strdate;
        var endate;
        var date = new Date();

        if (this.startdate != null) { strdate = format(this.startdate, 'dd/MM/yyyy') } else {
            //strdate = "2020-07-01"              
            strdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'dd/MM/yyyy')
        }
        if (this.enddate != null) { endate = format(this.enddate, 'dd/MM/yyyy') } else {
            // endate = "2020-07-31" strdate endate
            endate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'dd/MM/yyyy');
        }

        var s_Branches = this.inputForm.value.branchesArr;
        var s_Recipient = this.inputForm.value.recipientArr;
        var s_BatchNo = this.inputForm.value.inputBatchnNumber;
        var s_Package = this.inputForm.value.packagesArr;
        var s_Batch = this.inputForm.value.batchNoArr;



        switch (btnid) {
            case 'btn-NDIA-UnclaimedItems':
                this.NDIAUnclaimedItems(s_Branches, s_Recipient, strdate, endate);
                break;
            case 'btn-ndiaBatchRegister':
                this.NDIABatchRegister(s_BatchNo);
                break;
            case 'btn-NDIA-Packagestatement':
                let temp = forkJoin([
                    this.listS.GetNDIApackages(),
                    this.listS.GetBatchClients(s_Batch)

                ])
                temp.subscribe(data => {
                    this.batchprogramArr = data[0];
                    this.batchclientsArr = data[1];
                    this.NDIAPackageStatement(this.batchprogramArr, this.batchclientsArr, s_Branches, s_Package)
                });
                break;

            default:
                break;
        }

    }
    ResetVisibility() {
        this.frm_Date = false;
        this.frm_Branches = false;
        this.frm_Recipients = false;
        this.frm_BatchNo = false;
        this.frm_InpBatchNo = false;
        this.frm_Packages = false;

        this.inputForm = this.fb.group(inputFormDefault);
    }

    NDIAUnclaimedItems(branch, recipient, startdate, enddate) {

        var lblcriteria;
        var fQuery = "SELECT * FROM (SELECT ro.Recordno as [Shift#],ro.Date, ro.[Client Code] AS Client, ro.[Carer Code] AS Staff, ro.[Program], r.BRANCH, r.RECIPIENT_CoOrdinator, ro.[Service type] AS [Item/Activity], ISNULL([Unit Bill Rate], 0) * ro.BillQty AS ClaimAmount, ro.Duration / 12 AS ClaimHours, CASE WHEN ro.Status = 1 AND ro.[Type] = 1 THEN '1 - UNASSIGNED BOOKINGS'      WHEN ro.Status = 1 AND ro.[Type] <> 1 THEN '2 - UNAPPROVED SERVICES'      WHEN ro.Status IN (2, 5) AND ISNULL(ro.NDIABatch, 0) = 0 THEN '3 - APPROVED NOT CLAIMED OR BILLED'      WHEN ro.Status IN (2, 5) AND ISNULL(ro.NDIABatch, 0) <> 0 THEN '4 - APPROVED, CLAIMED NOT BILLED' END AS [Type] FROM ROSTER ro INNER JOIN HumanResourceTypes pr ON ro.[Program] = pr.[Name] INNER JOIN ItemTypes it ON ro.[Service Type] = it.[Title] LEFT JOIN Recipients r ON r.AccountNo = ro.[Client Code] WHERE pr.[Type] = 'NDIA' AND it.it_dataset = 'NDIS' AND ro.status IN (1, 2, 5) AND NOT (ro.[Type] = 8 and ro.[Start Time] = '00:00')   "

        if (startdate != "" || enddate != "") {
            this.s_DateSQL = " (ro.Date BETWEEN '" + startdate + ("' AND '") + enddate + "')";
            if (this.s_DateSQL != "") { fQuery = fQuery + " AND " + this.s_DateSQL };
        }

        if (branch != "") {
            this.s_BranchSQL = "R.[BRANCH] in ('" + branch.join("','") + "')";
            if (this.s_BranchSQL != "") { fQuery = fQuery + " AND " + this.s_BranchSQL }

        }




        if (startdate != "") {
            lblcriteria = " Date Between " + startdate + " and " + enddate + "; "
        }
        else { lblcriteria = " All Dated " }
        if (branch != "") {
            lblcriteria = lblcriteria + "Branches:" + branch.join(",") + "; "
        } else { lblcriteria = lblcriteria + " All Branches " }
        if (recipient != "") {
            lblcriteria = lblcriteria + " Recipients:" + recipient.join(",") + "; "
        } else { lblcriteria = lblcriteria + "All Recipients" }



        fQuery = fQuery + " ) t ORDER BY [Type], [Client], [Date]  "


        var Title = "NDIA Unclaimed Items Report"


        //console.log(fQuery)
        const data = {

            "template": { "_id": "ZSowzSUylA32icQq" },
            "options": {
                "reports": { "save": false },
                //   "sql": "SELECT DISTINCT R.UniqueID, R.AccountNo, R.AgencyIdReportingCode, R.[Surname/Organisation], R.FirstName, R.Branch, R.RECIPIENT_COORDINATOR, R.AgencyDefinedGroup, R.ONIRating, R.AdmissionDate As [Activation Date], R.DischargeDate As [DeActivation Date], HumanResourceTypes.Address2, RecipientPrograms.ProgramStatus, CASE WHEN RecipientPrograms.Program <> '' THEN RecipientPrograms.Program + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.Quantity <> '' THEN RecipientPrograms.Quantity + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.ItemUnit <> '' THEN RecipientPrograms.ItemUnit + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.PerUnit <> '' THEN RecipientPrograms.PerUnit + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.TimeUnit <> '' THEN RecipientPrograms.TimeUnit + ' ' ELSE ' ' END + CASE WHEN RecipientPrograms.Period <> '' THEN RecipientPrograms.Period + ' ' ELSE ' ' END AS FundingDetails, UPPER([Surname/Organisation]) + ' ' + CASE WHEN FirstName <> '' THEN FirstName ELSE ' ' END AS RecipientName, CASE WHEN N1.Address <> '' THEN  N1.Address ELSE N2.Address END  AS ADDRESS, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact END AS CONTACT, (SELECT TOP 1 Date FROM Roster WHERE Type IN (2, 3, 7, 8, 9, 10, 11, 12) AND [Client Code] = R.AccountNo ORDER BY DATE DESC) AS LastDate FROM Recipients R LEFT JOIN RecipientPrograms ON RecipientPrograms.PersonID = R.UniqueID LEFT JOIN HumanResourceTypes ON HumanResourceTypes.Name = RecipientPrograms.Program LEFT JOIN ServiceOverview ON ServiceOverview.PersonID = R.UniqueID LEFT JOIN (SELECT PERSONID,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = R.UniqueID LEFT JOIN (SELECT PERSONID,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress <> 1)  AS N2 ON N2.PersonID = R.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = R.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone <> 1)  AS P2 ON P2.PersonID = R.UniqueID WHERE R.[AccountNo] > '!MULTIPLE'   AND (R.DischargeDate is NULL)  AND  (RecipientPrograms.ProgramStatus = 'REFERRAL')  ORDER BY R.ONIRating, R.[Surname/Organisation]"
                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "txtTitle": Title,

            }
        }
        this.loading = true;


        this.drawerVisible = true;

        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = "NDIA Unclaimed Items.pdf"
            this.drawerVisible = true;
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            //console.log(err);
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });

        return;

    }
    NDIABatchRegister(batch) {

        var lblcriteria;
        var fQuery = "SELECT NDIABatch, Roster.RecordNo AS [Claim Ref ID], Roster.[Client Code], Recipients.[NDISNumber], Roster.[Date], Roster.[Start Time], Roster.[Service Type], CASE WHEN Roster.Type = 4 THEN AltItem.NDIA_ID ELSE ItemTypes.NDIA_ID END AS [NDIA_ID], Roster.[BillQty] , [Unit Bill Rate] , ISNULL(Roster.[Unit Bill Rate], 0) * Roster.[BillQty] AS LineTotal FROM Roster INNER JOIN Recipients ON roster.[client code] = recipients.accountno INNER JOIN ItemTypes ON ItemTypes.Title = Roster.[Service Type] LEFT JOIN ItemTypes AltItem ON AltItem.Title = Roster.[ShiftName] Where  "

        if (batch != "") {
            this.s_BatchSQL = " ( NDIABatch = '" + batch + "' ) ";
            if (this.s_BatchSQL != "") { fQuery = fQuery + "  " + this.s_BatchSQL };
        }
        if (batch != "") {
            lblcriteria = " Batch Number: " + batch + "; "
        }
        fQuery = fQuery + "ORDER BY Roster.[Client Code], Recipients.NDISNumber, Roster.[Date], Roster.[Start Time] "

        //console.log(fQuery)
        const data = {
            "template": { "_id": "T11evi50v5VQP3Jw" },
            "options": {
                "reports": { "save": false },
                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
            }
        }
        this.loading = true;
        this.drawerVisible = true;


        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = "NDIA Batch Register.pdf"
            this.drawerVisible = true;
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });
    }
    NDIAPackageStatement(pro, client, branch, packages) {

        var batchclients = "('" + client.join("','") + "')"
        var batchprogram = "('" + pro.join("','") + "')"

        //console.log(batchclients)
        //console.log(batchprogram)
        if (branch != "") {
            this.s_BranchSQL = " ( in ('" + branch.join("','") + "'))";
        }
        if (packages != "") {
            this.s_PackageSQL = " (package in ('" + packages.join("','") + "'))";
        }

        var lblcriteria;
        /*if (batch != "") {
            lblcriteria = lblcriteria + " Batch Number: " + batch + "; "
        }
        else {
            lblcriteria = lblcriteria + " All Batches "
        } */
        if (branch != "") {
            lblcriteria = lblcriteria + " Service Type " + branch.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + " All Svc. Types " }
        if (packages != "") {
            lblcriteria = lblcriteria + " Service Type " + packages.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + " All Packages " }


        this.drawerVisible = true;

        const data = {
            "template": { "_id": "q6UDawfEPuZTYe56" },
            "options": {
                "reports": { "save": false },

                //    "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "branches": "",
                "packages": "",
                "PatientCode": batchclients,
                "Programs": batchprogram,
                "ClientCode": batchclients



            }
        }
        this.loading = true;
        this.drawerVisible = true;

        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = "NDIA Package Statement.pdf"
            this.drawerVisible = true;
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });
    }
    //Mufeed's End

}
