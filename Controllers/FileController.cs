using System;
using System.IO;

using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Transactions;
using System.Text.Json;
using System.Linq;
using System.Text.Json.Serialization;
using System.Security.Permissions;
using System.Security;
using System.Security.AccessControl;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Collections.Generic;

using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Text.RegularExpressions;

using ProgramEntities = Adamas.Models.Modules.Program;
using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;
using ImageMagick;

using Repositories;
using Adamas.SessionConfigs;
using System.IO;
using Microsoft.EntityFrameworkCore;

//using ImageMagick;

namespace Adamas.Controllers
{
    [Route("api/v2/file")]
    public class FileController : Controller
    {
        private IUploadRepository uploadRepo;
        private readonly DatabaseContext context;
        private readonly RemoteDownloadDirectory remoteDirectory;
        private readonly string pathString;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private readonly IUserService userService;

        public FileController(
            IUploadRepository uploadRepo,
            IWebHostEnvironment hostingEnvironment,
            IOptions<RemoteDownloadDirectory> _remoteDirectory,
            IHttpContextAccessor httpContextAccessor,
            IUserService _userService,
            DatabaseContext context)
        {
            this.uploadRepo = uploadRepo;
            this.context = context;
            this.pathString = hostingEnvironment.ContentRootPath;
            this.remoteDirectory = _remoteDirectory.Value;
            this._httpContextAccessor = httpContextAccessor;
            userService = _userService;
        }

        [HttpPost("upload/document/{personID}/{user}")]
        public async Task<IActionResult> UploadFile(string personID, string user)
        {
            var files = Request.Form.Files;
            if(await uploadRepo.InsertDocuments(personID, user, files))
                return Ok(true);

            return BadRequest();
        }

        [HttpPost("{PERSONID}")]
        public async Task<IActionResult> Upload(string PERSONID)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var files = Request.Form.Files;
                    var file = files[0];

                    // Insert data in Database
                    await uploadRepo.InsertFileToDatabase(PERSONID, file).ConfigureAwait(false);

                    // Get File path
                    var path = await uploadRepo.GetUploadDownloadDirectoryAsync().ConfigureAwait(false);

                    // Download file using the path
                    await uploadRepo.InsertFileToFileSystemAsync(PERSONID, file, path).ConfigureAwait(false);

                    // Commit Transaction
                    scope.Complete();

                    return Ok(JsonSerializer.Serialize(new {
                        success = true
                    }));
                }
            } 
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("copy_document")]
        public async Task<IActionResult> CopyDocument(CopyDocument doc){
           try{
                var transfer_process = context.Database.ExecuteSqlRaw($@"dbo.[Copy_Document] @SourcePath = {doc.SourcePath}, @DestinationPath = {doc.DestinationPath}, @SourceFileName = {doc.SourceFileName}, @DestinationFileName = {doc.DestinationFileName}");
                return Ok(transfer_process);
           } catch(Exception ex){
               return BadRequest(ex);
           }
        }


        [HttpPost("download-document-network")]
        public async Task<IActionResult> DownloadDocumentRemoteNetwork([FromBody] CopyAndPasteFile doc)
        {
            var extension = Path.GetExtension(doc.FileName).ToLower();
            string dir = Path.GetDirectoryName(doc.SourceDocPath);

            string pathWithoutRoot = dir.Replace(Directory.GetParent(dir).Root.ToString(), string.Empty);
            var finalPath = Path.Combine(this.remoteDirectory.OriginPath, pathWithoutRoot);


            using (var command = context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = "[Copy_mta_document]";
                command.CommandType = CommandType.StoredProcedure;

                DbParameter dbParam = command.CreateParameter();
                dbParam.ParameterName = "@PersonID";
                dbParam.Value = "asd";
                command.Parameters.Add(dbParam);

                DbParameter dbParam2 = command.CreateParameter();
                dbParam2.ParameterName = "@Extension";
                //dbParam2.Value = ".pdf";
                dbParam2.Value = Path.GetExtension(doc.FileName);
                command.Parameters.Add(dbParam2);

                DbParameter dbParam3 = command.CreateParameter();
                dbParam3.ParameterName = "@FileName";
                //dbParam3.Value = "64887.PDF";
                dbParam3.Value = doc.FileName;
                command.Parameters.Add(dbParam3);

                DbParameter dbParam4 = command.CreateParameter();
                dbParam4.ParameterName = "@DocPath";
                //dbParam4.Value = "C:\\Adamas\\Support\\Cranes";
                dbParam4.Value = pathWithoutRoot;
                command.Parameters.Add(dbParam4);

                bool wasOpen = command.Connection.State == ConnectionState.Open;
                if (!wasOpen) command.Connection.Open();               

                try {
                    var mimeTypeStr = UploadService.GetMimeType(extension);
                    var streamFile = await command.ExecuteScalarAsync();                   
                 
                    byte[] arr = streamFile as byte[];
                    return File(arr, mimeTypeStr, doc.FileName);
                }
                finally
                {
                    if (!wasOpen) command.Connection.Close();
                }
            }
        }

        [HttpPost("download-document-remote-alara")]
        public async Task<IActionResult> DownloadDocumentRemoteAlaraServer([FromBody] CopyAndPasteFile doc)
        {
            //CopyAndPasteFile doc = new CopyAndPasteFile() { 
            //    FileName = "T0100004652-AAATEST.docx",
            //    DestinationDocPath = "",
            //    PersonId = "",
            //    SourceDocPath = "C:\\Adamas\\Support\\Cranes\\"
            //};

            var user = await userService.GetUserClaims();
            var sharedRegistration = string.Empty;

            var networkPathDrive = "\\\\alaraprofiles.file.core.windows.net\\data";
            //var networkPathDrive = "\\\\DESKTOP-UAFQI25\\AdamasDestination";

            if (doc.SourceDocPath == null || doc.SourceDocPath == "")
            {
                throw new Exception("Source path is empty");
            }

            string sourceDocPath = doc.SourceDocPath;

            if (!string.IsNullOrEmpty(sourceDocPath))
            {
                var splitPath = doc.SourceDocPath.Split(':');

                if (splitPath != null && splitPath.Length > 1)
                {
                    sharedRegistration = String.Concat(networkPathDrive, splitPath[1]);
                }

            }

            var destinationDirectory = this.remoteDirectory.IsRemote ? this.remoteDirectory.DestinationPath : Path.Combine(this.pathString, "document");
            var downloadPathFile = Path.Combine(this.pathString, "document", doc.FileName);


            using (var conn = context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();
                using (SqlCommand cmd = new SqlCommand(@"[CopyAndPasteFileWithOutSharedDocument]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FileName", doc.FileName);
                    cmd.Parameters.AddWithValue("@SourceDocPath", sharedRegistration);
                    //cmd.Parameters.AddWithValue("@DestinationDocPath", "C:\\Users\\User\\Desktop\\Programming\\adamas\\adamasv3\\document");
                    cmd.Parameters.AddWithValue("@DestinationDocPath", destinationDirectory);
                    cmd.ExecuteNonQuery();
                }
            }

            if (System.IO.File.Exists(downloadPathFile))
            {

                var dl = await uploadRepo.DownloadFileInDocumentDirectory(downloadPathFile);

                if (dl == null)
                {
                    return BadRequest();
                }

                MemoryStream ms = dl.Value.Item1;
                //UploadService.GetMimeType(extension) dl
                string filetype = dl.Value.Item2;

                await uploadRepo.DeleteFileByNameToFileSystemAsync(downloadPathFile);
                return File(ms, filetype, doc.FileName);
            }

            return Ok(null);
        }

        // USED BY DOCUMENTS MODULE IN PORTAL CLIENT
        [HttpPost("download-document-portal-client")]
        public IActionResult DownloadDocumentPortalClient([FromBody] CopyAndPasteFile doc)
        {
            var fileMemStream = uploadRepo.DownloadDocumentPortalClient(doc);
            return File(fileMemStream, "application/octet-stream");
        }

        // USED BY DOCUMENTS MODULE IN PORTAL CLIENT
        [HttpPost("download-document-remote")]
        public async Task<IActionResult> DownloadDocumentRemoteServer([FromBody] CopyAndPasteFile doc){
           try
           {
                var user = HttpContext.Session.Get<Token>("token");

                var sharedRegistration = await uploadRepo.GetSharedDocumentRegistration();
                var docFolderPath = await uploadRepo.GetUploadDownloadDirectoryAsync(user.User);

                var downloadPathFile = Path.Combine(this.pathString, "document", doc.FileName);
                var destinationDirectory = this.remoteDirectory.IsRemote ? this.remoteDirectory.DestinationPath : Path.Combine(this.pathString, "document");

                // RecipientDocFolder required
                // if(string.IsNullOrEmpty(docFolderPath)){
                //     return BadRequest("Your provider has not yet enabled document upload for you. Please contact your provider to setup a Document Folder");
                // }

                if(!string.IsNullOrEmpty(sharedRegistration) && !string.IsNullOrEmpty(docFolderPath) && this.remoteDirectory.IsRemote)
                {
                    var splitPath = doc.SourceDocPath.Split(':');

                    if(splitPath != null && splitPath.Length > 1){
                        sharedRegistration = String.Concat(sharedRegistration, splitPath[1]);
                    } else {
                        sharedRegistration = String.Concat(sharedRegistration, doc.SourceDocPath);
                    }

                } else {
                    sharedRegistration = doc.SourceDocPath;
                }

                if(sharedRegistration.Substring(sharedRegistration.Length - 1) == @"\")
                {
                    sharedRegistration = sharedRegistration.Remove(sharedRegistration.Length - 1);
                }

                // return Ok(new {
                //     FileName = doc.FileName,
                //     SourceDocPath = sharedRegistration,
                //     DestinationDocPath = destinationDirectory
                // });

                using(var conn = context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    using(SqlCommand cmd = new SqlCommand(@"[CopyAndPasteFileWithOutSharedDocument]",(SqlConnection) conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileName", doc.FileName);
                        cmd.Parameters.AddWithValue("@SourceDocPath", sharedRegistration);
                        cmd.Parameters.AddWithValue("@DestinationDocPath", destinationDirectory);
                        cmd.ExecuteNonQuery();
                    }
                }
               
                if(System.IO.File.Exists(downloadPathFile)){

                    var dl = await uploadRepo.DownloadFileInDocumentDirectory(downloadPathFile);

                    if(dl == null)
                    {
                        return BadRequest();
                    }

                    MemoryStream ms = dl.Value.Item1;
                    string filetype = dl.Value.Item2;

                    await uploadRepo.DeleteFileByNameToFileSystemAsync(downloadPathFile);
                    return File(ms, filetype, doc.FileName);
                }

                return Ok(new {
                    FileName = doc.FileName,
                    SourceDocPath = sharedRegistration,
                    DestinationDocPath = doc.DestinationDocPath,
                    DownloadPathFile = downloadPathFile
                });
           } catch(Exception ex){
               return BadRequest(ex);
           }
        }

        [HttpPost("download-document")]
        public async Task<IActionResult> DownloadInDocumentDirectory([FromBody] GetDocument doc)
        {
            string SourcePath = Path.Combine(this.pathString, "document");

            var transfer_process = context.Database.ExecuteSqlRaw($@"dbo.[Copy_Document] @SourcePath = {doc.DocPath}, @DestinationPath = {SourcePath}, @SourceFileName = {doc.FileName}, @DestinationFileName = {doc.FileName}");
            var dl =  await uploadRepo.DownloadFileInDocumentDirectory(doc, SourcePath);

            await uploadRepo.DeleteFileByNameToFileSystemAsync(doc.FileName, SourcePath).ConfigureAwait(false);

            MemoryStream ms = dl.Item1;
            string filetype = dl.Item2;

            return File(ms, filetype, doc.FileName);
        }
        
        [HttpGet("quote/template")]
        public async Task<IActionResult> GetQuoteTemplate(){
           var templates = await (from doc in context.DOC_Associations 
                                    where (doc.IsForm == null || doc.IsForm == false) && 
                                        (doc.CanCreateFile == false) &&
                                        (doc.TRACCSType == "CAREPLAN") select doc).ToListAsync();
            if(templates == null){
                return BadRequest();
            }

            List<dynamic> list = new List<dynamic>();

            foreach(var template in templates)
            {
                list.Add(new {
                    Name = template.Title,
                    Directory = template.Template,
                    Exists = System.IO.File.Exists(template.Template)
                });
            }

            return Ok(list);
        }

        // USED BY DOCUMENTS MODULE IN PORTAL CLIENT FOR UPLOAD
        [HttpPost("upload-document-portal-client")]
        public async Task<IActionResult> UploadDocumentPortalClient()
        {
            CopyAndPasteFile doc = JsonSerializer.Deserialize<CopyAndPasteFile>(Request.Form["data"]);
            IFormFileCollection files = Request.Form.Files;
            // RETURN FAILURE IF NO FILE DETECTED
            if (files.Count == 0)
            {
                return BadRequest("No File uploaded!");
            }
            var file = files[0];

            await uploadRepo.UploadDocumentPortalClient(doc,file);
            return Ok(new
            {
                SourceDocPath = this.remoteDirectory.IsRemote ? this.remoteDirectory.OriginPath : Path.Combine(this.pathString, "document", file.FileName),
                DestinationDocPath = "",
                Filename = files[0].FileName
            });
        }

        // USED BY DOCUMENTS MODULE IN PORTAL CLIENT
        [HttpPost("upload-document-remote")]
        public async Task<IActionResult> UploadDocumentRemoteServer(){
            
           try
           {    
                // GET INPUT FILE AND DATA
                CopyAndPasteFile doc = JsonSerializer.Deserialize<CopyAndPasteFile>(Request.Form["data"]);

                // GET COOKIE USER
                var user = _httpContextAccessor.HttpContext.User.Claims.Where(c => c.Type == "user").FirstOrDefault().Value;

                IFormFileCollection files = Request.Form.Files;
            
                // RETURN FAILURE IF NO FILE DETECTED
                if(files.Count == 0){
                    return BadRequest("No File uploaded!");
                }

                var file = files[0];

                // StaffDocFolder in USERINFO
                var docFolderPath = await uploadRepo.GetUploadDownloadDirectoryAsync(user);

                if(string.IsNullOrEmpty(docFolderPath)){
                    return BadRequest("Your provider has not yet enabled document upload for you. Please contact your provider to setup a Document Folder");
                }

                // GET SHAREDOCUMENTPATH in REGISTRATION Table
                var sharedDocPath = await uploadRepo.GetSharedDocumentRegistration();

                // APPEND SHAREDOCUMENT PATH & RECIPIENTDOCFOLDER
                if(!string.IsNullOrEmpty(sharedDocPath) && !(string.IsNullOrEmpty(docFolderPath)) && this.remoteDirectory.IsRemote){
                    var splitPath = docFolderPath.Split(":");
                    if(splitPath != null && splitPath.Length > 1){
                        sharedDocPath = String.Concat(sharedDocPath, splitPath[1]);
                    } else {
                        sharedDocPath = String.Concat(sharedDocPath, doc.SourceDocPath);
                    }                        
                } else {
                    sharedDocPath = docFolderPath;
                }

                if(sharedDocPath.Substring(sharedDocPath.Length - 1) == @"\")
                {
                    sharedDocPath = sharedDocPath.Remove(sharedDocPath.Length - 1);
                }

                var filteredFileName=  this.uploadRepo.FilterFileNameAnomalies(file.FileName);

                // UPLOAD FILE TO Project Document Folder
                var downloadPathFile = Path.Combine(this.pathString, "document", filteredFileName);

                // if(this.remoteDirectory.IsRemote){
                //     downloadPathFile = this.remoteDirectory.OriginPath;
                // }
                
                using (var stream = new FileStream(downloadPathFile, FileMode.Create)) await file.CopyToAsync(stream);

                // return Ok(new {
                //     Filename = file.FileName,
                //     SourceDocPath = doc.SourceDocPath ?? downloadPath,
                //     DestinationDocPath = doc.DestinationDocPath,
                //     SharedDocPath = sharedDocPath
                // });

                if(System.IO.File.Exists(downloadPathFile)){

                     // Use COPYANDPASTEFILE to Copy File from Document Folder to the Remote Folder Directory

                    // return Ok(new {
                    //     FileName = files[0].FileName,
                    //     SourcePath = downloadPath,
                    //     DocPath = doc.SourceDocPath,
                    //     DestinationDocPath = doc.DestinationDocPath
                    // });

                    var whatDirectory = this.remoteDirectory.IsRemote ? this.remoteDirectory.OriginPath : Path.Combine(this.pathString, "document");

                    using(var conn = context.Database.GetDbConnection() as SqlConnection)
                    {
                        await conn.OpenAsync();
                        
                        using(SqlCommand cmd = new SqlCommand(@"[CopyAndPasteFileWithOutSharedDocument]",(SqlConnection) conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@FileName", filteredFileName);
                            cmd.Parameters.AddWithValue("@SourceDocPath", whatDirectory);
                            cmd.Parameters.AddWithValue("@DestinationDocPath", sharedDocPath);                        
                            
                            cmd.ExecuteNonQuery();
                        }
                    }

                    await uploadRepo.InsertFileToDatabase(doc.PersonId, file, user).ConfigureAwait(false);

                    // DELETE Downloaded File on the Document Folder
                    // await uploadRepo.DeleteFileByNameToFileSystemAsync(downloadPathFile);
                } else{
                    return BadRequest("Something wrong on CopyAndPasteFile Procedure");
                }
                
                return Ok(new {
                    SourceDocPath = this.remoteDirectory.IsRemote ? this.remoteDirectory.OriginPath : Path.Combine(this.pathString, "document", file.FileName),
                    DestinationDocPath = sharedDocPath,
                    Filename = files[0].FileName
                });
           } catch(Exception ex){
                throw new Exception(ex.Message);
                //return BadRequest(ex);
           }
        }

        [HttpGet("copy_pastefile")]
        public async Task<IActionResult> CopyPasteFile(CopyAndPasteFile doc){
           try{
                
                using(var conn = context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    
                    using(SqlCommand cmd = new SqlCommand(@"[CopyAndPasteFile]",(SqlConnection) conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FileName", doc.FileName);
                        cmd.Parameters.AddWithValue("@SourceDocPath", doc.SourceDocPath);
                        cmd.Parameters.AddWithValue("@DestinationDocPath", doc.DestinationDocPath);                        
                        
                        await cmd.ExecuteNonQueryAsync();
                        return Ok(true);
                    }
                }

           } catch(Exception ex){
               return BadRequest(ex);
           }
        }

        [HttpGet("copy_mta_document")]
        public async Task<IActionResult> CopyMTADocument(CopyMtaDocument doc){
           try{
                Stream s;
                byte[] byteArr;

                string SourcePath = Path.Combine(this.pathString, "document");
                using(var conn = context.Database.GetDbConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    
                    using(SqlCommand cmd = new SqlCommand(@"[Copy_mta_document]",(SqlConnection) conn))
                    {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PersonId", doc.PersonId);
                            cmd.Parameters.AddWithValue("@Extension", doc.Extension);
                            cmd.Parameters.AddWithValue("@FileName", doc.Filename);
                            cmd.Parameters.AddWithValue("@DocPath", doc.DocPath);
                            
                            
                            using(var rd = await cmd.ExecuteReaderAsync())
                            {
                                while(await rd.ReadAsync())
                                {
                                    byteArr = ReadFully(rd.GetStream(0));
                                    
                                    System.IO.File.WriteAllBytes(
                                        Path.Combine(SourcePath, doc.Filename),
                                        byteArr
                                    );
                                }
                            }
                    }
                }

                // var transfer_process = context.Database.ExecuteSqlCommand($@"dbo.[Copy_mta_document] 
                // @PersonID = {doc.PersonId}, 
                // @Extension = {doc.Extension}, 
                // @FileName = {doc.Filename}, 
                // @DocPath = {doc.DocPath}");
                
                return Ok(true);
           } catch(Exception ex){
               return BadRequest(ex);
           }
        }


        [HttpPost("upload-incident-document-procedure")]
        public async Task<IActionResult> UploadIncidentDocument()
        {
            try
            {
                var user = HttpContext.Session.Get<Token>("token");

                var files = Request.Form.Files;
                var file = files[0];
                
                GetDocument doc = JsonSerializer.Deserialize<GetDocument>(Request.Form["data"]);
                var DestinationPath = doc.DocPath;

                // Insert data in Database
                await uploadRepo.InsertIncidentDocument(doc.PersonID, doc, file).ConfigureAwait(false);

                string SourcePath = Path.Combine(this.pathString, "document");
                string FileName = await uploadRepo.InsertFileToFileSystemAsyncReturnFileNameString(doc.PersonID, file, SourcePath).ConfigureAwait(false);

              //  var transfer_process = context.Database.ExecuteSqlRaw($@"dbo.[Copy_Document] @SourcePath = {SourcePath}, @DestinationPath = {DestinationPath}, @SourceFileName = {FileName}, @DestinationFileName = {FileName}");
               // await uploadRepo.DeleteFileByNameToFileSystemAsync(FileName, SourcePath).ConfigureAwait(false);
              //  return Ok(transfer_process);   
            using (var conn = context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Copy_Document", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SourcePath", SourcePath);
                    cmd.Parameters.AddWithValue("@DestinationPath", DestinationPath);
                    cmd.Parameters.AddWithValue("@SourceFileName", FileName);                  
                    cmd.Parameters.AddWithValue("@DestinationFileName", FileName);

                    await conn.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();

                    await uploadRepo.DeleteFileByNameToFileSystemAsync(FileName, SourcePath).ConfigureAwait(false);
                    return Ok(true);          
                }
            }             
            } 
            catch (Exception ex)
            {
                return BadRequest(new Error{
                    Message = ex.ToString()
                });
            }
        }

        [HttpPost("upload-document-procedure")]
        public async Task<IActionResult> UploadUsingProcedure()
        {

            try
            {
                var user = HttpContext.Session.Get<Token>("token");

                var files = Request.Form.Files;
                var file = files[0];

                GetDocument doc = JsonSerializer.Deserialize<GetDocument>(Request.Form["data"]);

                // string SourcePath = "C:\\Adamas\\Support\\Cranes";
                // string DestinationPath = "C:\\Users\\mark1\\Desktop\\traccsportal";
                // string SourceFileName = "62752.pdf";
                // string DestinationFileName = "62752.pdf";             
                // var DestinationPath = await uploadRepo.GetUploadRecipientDirectory(doc.PersonID);

                var DestinationPath = doc.DocPath;

                // if(!(HasWritePermissionOnDir(DestinationPath))){
                //     return BadRequest(new Error(){
                //         Message = $"Directory has no permissions"
                //     });
                // }

                // if(string.IsNullOrEmpty(DestinationPath)){
                //     return BadRequest(new Error(){
                //         Message = $"RecipientDocFolder on user {doc.PersonID} is empty"
                //     });
                // }

                // FileIOPermission f = new FileIOPermission(PermissionState.None);
                // f.AllLocalFiles = FileIOPermissionAccess.Read;
                // try
                // {
                //     f.Demand();
                // }
                // catch (SecurityException ex)
                // {
                //     return BadRequest(ex);
                // }


                // if (!(System.IO.Directory.Exists(DestinationPath)))
                // {
                //     return BadRequest(new Error(){
                //         Message = $"Directory does not exist - {DestinationPath}"
                //     });
                // }

                // Insert data in Database
                await uploadRepo.InsertFileToDatabase(doc.PersonID, file, user.User).ConfigureAwait(false);

                string SourcePath = Path.Combine(this.pathString, "document");
                string FileName = await uploadRepo.InsertFileToFileSystemAsyncReturnFileNameString(doc.PersonID, file, SourcePath).ConfigureAwait(false);

               // var transfer_process = context.Database.ExecuteSqlRaw($@"dbo.[Copy_Document] @SourcePath = {SourcePath}, @DestinationPath = {DestinationPath}, @SourceFileName = {FileName}, @DestinationFileName = {FileName}");
              using (var conn = context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Copy_Document", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SourcePath", SourcePath);
                    cmd.Parameters.AddWithValue("@DestinationPath", DestinationPath);
                    cmd.Parameters.AddWithValue("@SourceFileName", FileName);                  
                    cmd.Parameters.AddWithValue("@DestinationFileName", FileName);
                 

                    await conn.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();


                    await uploadRepo.DeleteFileByNameToFileSystemAsync(FileName, SourcePath).ConfigureAwait(false);
                    return Ok(true);          
                }
            }
                     
            } 
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
           
        [HttpPost("download")]
        public async Task<IActionResult> Download([FromBody] GetDocument doc)
        {
            var dl =  await uploadRepo.DownloadFile(doc);
            MemoryStream ms = dl.Item1;
            string filetype = dl.Item2;

            return File(ms, filetype, doc.FileName);
        }

        [HttpPost("download-via-db")]
        public async Task<IActionResult> DownloadViaDb([FromBody] GetDocument doc)
        {
            var dl =  await uploadRepo.DownloadFileInDocumentDirectory(doc, @"\\SJCC.local\CompanyData\Corp\CLIENT FILES\CAIRNS\TESTRECIPIENT D");
            MemoryStream ms = dl.Item1;
            string filetype = dl.Item2;

            return File(ms, filetype, doc.FileName);
        }

        [HttpDelete("delete-document-portal-client/{PERSONID}")]
        public async Task<IActionResult> Delete(string PERSONID, FileForm file)
        {
            try
            {
                await uploadRepo.DeleteFileToDatabase(PERSONID, file);
                await uploadRepo.DeleteFileToFileSystemAsync(file);

                return Ok(JsonSerializer.Serialize(new
                {
                    success = true
                }));
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("{name}/{view}")]
        public async Task<IActionResult> GetFile(string name, string view)
        {
            var user = HttpContext.Session.Get<Token>("token");
            var files = await uploadRepo.GetFileFromDatabase(name, view, "");
            return Ok(files);
        }



        public static bool HasWritePermissionOnDir(string path)
        {
            var writeAllow = false;
            var writeDeny = false;
            var accessControlList = new System.IO.FileInfo(path).GetAccessControl();
            if (accessControlList == null)
                return false;
            var accessRules = accessControlList.GetAccessRules(true, true, 
                                        typeof(System.Security.Principal.SecurityIdentifier));
            if (accessRules ==null)
                return false;

            foreach (FileSystemAccessRule rule in accessRules)
            {
                if ((FileSystemRights.Write & rule.FileSystemRights) != FileSystemRights.Write) 
                    continue;

                if (rule.AccessControlType == AccessControlType.Allow)
                    writeAllow = true;
                else if (rule.AccessControlType == AccessControlType.Deny)
                    writeDeny = true;
            }

            return writeAllow && !writeDeny;
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

    }
    

}