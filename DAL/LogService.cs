﻿using Adamas.Models;
using AdamasV3.Models.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.ServiceModel.Channels;
using System.Threading.Tasks;

namespace AdamasV3.DAL
{
    public class LogService : ILogService
    {
        private readonly DatabaseContext dbContext;
        public LogService(DatabaseContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task InsertError(Exception e)
        {
            var logs = new WebPortalLog
            {
                Message     = e.Message.ToString(),
                Date        = DateTime.Now,
                Source      = e.Source.ToString(),
                Custom      = ""
            };

            dbContext.Add(logs);
            await dbContext.SaveChangesAsync();
        }

        public async Task InsertError(Exception e, string custom)
        {
            var logs = new WebPortalLog
            {
                Message = e.Message.ToString(),
                Date = DateTime.Now,
                Source = e.Source.ToString(),
                Custom = custom
            };

            dbContext.Add(logs);
            await dbContext.SaveChangesAsync();
        }


    }
}
