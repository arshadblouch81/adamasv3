import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'

import  { TimeSheetService, GlobalService, ClientService, statuses, contactGroups } from '../services/index';

@Component({
    selector: 'contact-details',
    templateUrl: './contact-details.html',
    styles:[`
    button{
        min-width:3rem;
    }
    input:not([type=email]){
        text-transform:uppercase;
    }
    `]
})

export class ContactDetails implements OnChanges {
    @Input() user: any;
    @Input() role: string;
    @Output() reload = new EventEmitter();

    data: any;

    kindetailsGroup: FormGroup;

    constructor(
        private timeS: TimeSheetService,        
        private formBuilder: FormBuilder,
        private globalS: GlobalService
    ){
        this.kindetailsGroup = this.formBuilder.group({
            listOrder: [''],
            type: [''],
            name: [''],
            email: [''],
            address1: [''],
            address2: [''],
            suburbcode: [''],
            suburb: [''],
            postcode: [''],
            phone1: [''],
            phone2: [''],
            mobile: [''],
            fax: [''],
            notes: [''],
            oni1: false,
            oni2: false,
            ecode: [''],
            creator:[''],
            recordNumber: null
        });
    }

    ngOnChanges(changes: SimpleChanges){
        if(!changes['user'].firstChange && changes['user'].currentValue){
            this.search(changes.user.currentValue)
        }
    }

    search(data: any){
        this.data = data;
        if(this.role === 'recipient'){
            this.timeS.getcontactskinrecipientdetails(data.recordNumber)
                        .subscribe(data => {
                            this.kindetailsGroup.patchValue({
                                address1: data.address1,
                                address2: data.address2,
                                name: data.contactName,
                                type: data.subType,
                                email: data.email,
                                fax: data.fax,
                                mobile: data.mobile,
                                notes: data.notes,
                                phone1: data.phone1,
                                phone2: data.phone2,
                                suburbcode: (data.postcode || '').trim()+ ' ' + (data.suburb || '').trim(),
                                suburb: data.suburb,
                                postcode: data.postcode,
                                listOrder: data.state,  
                                oni1: (data.equipmentCode || '').toUpperCase() == 'PERSON1',
                                oni2: (data.equipmentCode || '').toUpperCase() == 'PERSON2',
                                recordNumber: data.recordNumber
                            })        
                        })
        }

        if(this.role === 'staff'){
            this.timeS.getcontactskinstaffdetails(data.recordNumber)
                        .subscribe(data => {
                            this.kindetailsGroup.patchValue({
                                address1: data.address1,
                                address2: data.address2,
                                name: data.contactName,
                                type: data.contactType,
                                email: data.email,
                                fax: data.fax,
                                mobile: data.mobile,
                                notes: data.notes,
                                phone1: data.phone1,
                                phone2: data.phone2,
                                suburbcode: (data.postcode || '').trim()+ ' ' + (data.suburb || '').trim(),
                                suburb: data.suburb,
                                postcode: data.postcode,
                                listOrder: data.state,  
                                oni1: (data.equipmentCode || '').toUpperCase() == 'PERSON1',
                                oni2: (data.equipmentCode || '').toUpperCase() == 'PERSON2',
                                recordNumber: data.recordNumber
                            })        
                        })
        }
    }

    update(){
        if(this.role === 'recipient' && this.kindetailsGroup.dirty){

            const rs = this.kindetailsGroup.get('suburbcode').value;
            let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0] : "";
            let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0] : "";
    
            this.kindetailsGroup.controls["postcode"].setValue(pcode)
            this.kindetailsGroup.controls["suburb"].setValue(suburb)

            if(this.kindetailsGroup.get('oni1').value){
                this.kindetailsGroup.controls['ecode'].setValue('PERSON1')
            } else if (this.kindetailsGroup.get('oni2').value){
                this.kindetailsGroup.controls['ecode'].setValue('PERSON2')
            }        

            this.timeS.updatecontactskinrecipientdetails(
                this.kindetailsGroup.value,
                this.data.recordNumber
            ).subscribe(data => {
                if (data) {
                    this.reload.emit(2);
                    this.globalS.sToast('Success','Contact Updated');
                } else{
                    this.globalS.eToast('Error','Process Not Completed')
                }
            })
        }

        if(this.role === 'staff' && this.kindetailsGroup.dirty){
            const rs = this.kindetailsGroup.get('suburbcode').value;
            let pcode = /(\d+)/g.test(rs) ? rs.match(/(\d+)/g)[0] : "";
            let suburb = /(\D+)/g.test(rs) ? rs.match(/(\D+)/g)[0] : "";
    
            this.kindetailsGroup.controls["postcode"].setValue(pcode)
            this.kindetailsGroup.controls["suburb"].setValue(suburb)

            if(this.kindetailsGroup.get('oni1').value){
                this.kindetailsGroup.controls['ecode'].setValue('PERSON1')
            } else if (this.kindetailsGroup.get('oni2').value){
                this.kindetailsGroup.controls['ecode'].setValue('PERSON2')
            }

            this.timeS.updatecontactskinstaffdetails(
                this.kindetailsGroup.value,
                this.data.recordNumber
            ).subscribe(data => {
                if (data) {
                    this.reload.emit(2);
                    this.globalS.sToast('Success','Contact Updated');
                } else{
                    this.globalS.eToast('Error','Process Not Completed')
                }
            })
        }
    }

    delete(){
        if(this.kindetailsGroup.value.recordNumber){
            this.timeS.deletecontactskin(this.kindetailsGroup.value.recordNumber).subscribe(data =>{
                if (data) this.globalS.sToast('Success','Contact Deleted')
                else this.globalS.eToast('Error','Process Not Completed')
                this.reload.emit(2);
            })
        }
    }

    oniProcess(data: any, source: string, group: FormGroup){
        group.patchValue({
            oni1: (data && source == 'oni1') ? true : false,
            oni2: (data && source == 'oni2') ? true : false
        });
    }

}