import { Component, OnInit, ViewEncapsulation, forwardRef } from '@angular/core';
import  {  ListService, GlobalService } from '../../services/index';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'recipient-popup',
  templateUrl: './recipient-popup.component.html',
  styleUrls: ['./recipient-popup.component.css'],
  // encapsulation: ViewEncapsulation.None,
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RecipientPopupComponent),
      multi: true      
    }
  ]
})
export class RecipientPopupComponent implements OnInit, ControlValueAccessor {

  recipientModal: boolean = false;
  recipientSearchList: Array<any> = []
  counterRecipient: number = 0;
  disableSearchOnScroll: boolean = false;
  selectedRecipient: any;

  index:number = -1;

  
  value: any = '';

  private onChange: Function;
  private onTouch: Function;


  constructor(
    private listS: ListService,
    private globalS: GlobalService
  ) { }

  ngOnInit() {  }

  openRecipientModal(){
    this.recipientModal = true;
    this.populateRecipientSearch();
  }

  populateRecipientSearch(){
    this.recipientSearchList = []
    this.listS.getrecipientsearch({
        AccountNo: '',
        RowNo: this.counterRecipient
    }).subscribe(data => this.recipientSearchList = data)
  }

  
  onScroll(){
    if (this.disableSearchOnScroll) return;

    this.listS.getrecipientsearch({
      AccountNo: '',
      RowNo: this.counterRecipient += 200
    }).subscribe(data => {
      data.forEach(e => {
        this.recipientSearchList.push(e);
      })
    });
  }


  searchRecipient(data: any){
    this.recipientSearchList = [];
    this.index = -1;

    if(this.globalS.isEmpty(data))  {
        this.disableSearchOnScroll = false;
    } else{
        this.disableSearchOnScroll = true;
    }              

    this.listS.getrecipientsearch({
        AccountNo: data,
        RowNo: 0
    }).subscribe(data => this.recipientSearchList = data)
  }

  emitRecipient(){
    this.value = this.selectedRecipient.accountNo;
    this.onChange(this.selectedRecipient.accountNo); 
  }


  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  writeValue(value: any): void {

      this.value = value;
    
  }
  
  select(index: number){
    this.selectedRecipient = this.recipientSearchList[index];
    this.index = index;
  }

}
