import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit, ViewChild, Renderer2,ElementRef,HostListener } from '@angular/core';
import { FormGroup, FormBuilder,FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GlobalService, ListService, MenuService, PrintService,NavigationService} from '@services/index';
import { SwitchService } from '@services/switch.service';
import { isEmpty } from 'lodash';
import { NzModalService } from 'ng-zorro-antd';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { empty, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { saveAs } from 'file-saver';
@Component({
  selector: 'app-program-packages',
  templateUrl: './program-packages.component.html',
  styles: [`
  .mrg-btm{
    margin-bottom:5px;
  },
  textarea{
    resize:none;
  },
  .ant-modal-body {
    min-height:80vh
  },
  .staff-wrapper{
    height: 10rem;
    width: 100%;
    overflow: auto;
    padding: .5rem 1rem;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
  }
  tr.highlight {
        background-color: #85B9D5 !important; /* Change to your preferred highlight color */
  }
  `]
  })
  export class ProgramPackagesComponent implements OnInit {
    
    temp:Array<any>;
    period: Array<any>;
    tableData: Array<any>;
    targetGroups:Array<any>;
    budgetEnforcement:Array<any>;
    budgetRoasterEnforcement:Array<any>;
    packedTypeProfile:Array<any>;
    packedTypeProfileCopy:Array<any>;
    careWorkers:Array<any>;
    expireUsing:Array<any>;
    dailyArry:Array<any>;
    activityTimeSheet:Array<any>;
    cycles:Array<any>;
    activityTypes:Array<any>;
    unitsArray:Array<any>;
    quoutetemplates:Array<any>;
    competencyList:Array<any>;
    competencyListCopy:Array<any>;
    careWorkersExcluded:Array<any>;
    PackgeLeaveTypecheckedList:Array<any>;
    checkedList:string[];
    checkedListExcluded:Array<any>;
    checkedListApproved:Array<any>;
    checkedPackageProfileServices:Array<any>;
    branches:Array<any>;
    paytypes:Array<any>;
    alerts:Array<any>;
    DefPeriod:Array<any>;
    adminTypesArray:Array<any>;
    subgroups:Array<any>;
    caredomain:Array<any>;
    contingency:Array<any>;
    fundingRegion:Array<any>;
    levels:Array<any>;
    programz:Array<any>;
    budgetGroup:Array<any>;
    diciplines:Array<any>;
    groupAgency:Array<any>;
    states:Array<any>;
    staffTeams:Array<any>;
    staffCategory:Array<any>;
    staff:Array<any>;
    recepients:Array<any>;
    recepitnt_copy:Array<any>;
    leaveTypes:Array<any>;
    accoumulationTypes:Array<any>;
    leaveTypes_copy:Array<any>;
    activities:Array<any>;
    types:Array<any>;
    fundingTypes:Array<any>;
    fundingSources:Array<any>;
    programCordinates:Array<any>;
    individual:boolean= false;
    aged:boolean= false;
    visibleRecurrent:boolean=false;
    template:boolean=false;
    packageLevel:boolean=false;
    ServiceData:Array<any>;
    items:Array<any>;
    jurisdiction:Array<any>;
    loading: boolean = false;
    modalOpen: boolean = false;
    servicesModal:boolean = false;
    staffApproved: boolean = false;
    staffUnApproved: boolean = false;
    competencymodal: boolean = false;
    packageLeaveModal:boolean = false;
    check : boolean = false;
    current: number = 0;
    checked:boolean=false;
    checkedflag:boolean = true;
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
    dateFormat: string = 'dd/MM/yyyy';
    inputvalueSearch:string;
    inputForm: FormGroup;
    modalVariables:any;
    inputVariables:any; 
    postLoading: boolean = false;
    isUpdate: boolean = false;
    radioSelcted = 'program'
    title:string = "Add New Program/Packages";
    whereString:string = "WHERE ( [group] = 'PROGRAMS' ) AND ( enddate IS NULL OR enddate >= getDate() )";
    private unsubscribe: Subject<void> = new Subject();
    userRole:string="userrole";
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
     
    isUpdatePackageType: boolean = false;
    tableDataPackageLeaveTypes: any;
    personId:0;
    listCompetencyByPersonId: any;
    listApprovedServicesByPersonId: any;
    listApprovedStaffByPersonId:any;
    listExcludedStaffByPersonId:any;
    CompetencycheckedList: any;
    careWorkersCopy: any[];
    careWorkersExcludedCopy: any;
    @ViewChild('fileInput', { static: false }) fileInput: ElementRef<HTMLInputElement>;
    clonemodalOpen: boolean;
    operation: number;
    selectedClonedRecord: any;
    activityCode:string;
    
    mergeHistorymodalOpen:boolean=false;
    activeRowData:any;
    selectedRowIndex:number;
    mergeactivitycodesList: Array<any>=[];
    originalList: Array<any>=[];
    txtSearch:string;
    selectedmergecodes: Array<any>=[];
    selectedCode: any;
    HighlightRow2: number;
    mergestatus:boolean=false;
    selectedRows: any[] = [];
    searchTerm: string = '';
    currentIndex: number | null = null;

    constructor(
      private globalS: GlobalService,
      private cd: ChangeDetectorRef,
      private switchS:SwitchService,
      private listS:ListService, 
      private menuS:MenuService,
      private printS:PrintService,
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private fb: FormBuilder,
      private sanitizer: DomSanitizer,
      private ModalS: NzModalService,
      private renderer: Renderer2,
      private navigationService: NavigationService,
    private router: Router
    ){}
    
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.CompetencycheckedList = new Array<string>();
      this.PackgeLeaveTypecheckedList = new Array <string>();
      this.checkedListExcluded =new Array<string>();
      this.checkedListApproved =new Array<string>();
      this.checkedPackageProfileServices =new Array<string>();
      this.loadData();
      this.populateDropdowns();
      this.loadPackageLeaveTypes();
      this.loadApprovedStaff();
      this.loadExcludedStaff();
      this.loadCompetency();
      this.loadApprovedServices();
      this.buildForm();
      this.loading = false;
      this.cd.detectChanges();
    }
    showAddModal() {
      this.title = "Add New Program/Packages"
      this.resetModal();
      this.inputForm.patchValue({
        line_1  :'Unused funds are returned to the Department on package cessation',
        line_2  :'For Queries and Inquiries Please Phone',
      });
      this.modalOpen = true;
    }
    
    showPackageLeaveModal(){
      this.leaveTypes.forEach(x => {
        x.checked = false
      });
      this.PackgeLeaveTypecheckedList = [];
      this.packageLevel = true;
    }
    savePackageTypes(){
      this.postLoading = true;     
      const group = this.inputForm;
      let insertOne = false;
      if(!this.isUpdatePackageType){
        this.PackgeLeaveTypecheckedList.forEach( (element) => {
          let is_exist   = this.globalS.isPackageLeaveTypeExists(this.tableDataPackageLeaveTypes,element);
          if(!is_exist){
            let sql = "INSERT INTO HumanResources (PersonID, [Group], Type, Name, Notes) VALUES ('', 'PROG_LEAVE', 'PROG_LEAVE','"+element+"', '')";
            this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{  
              insertOne = true;
            });
          }
        });
        if(insertOne){
          this.globalS.sToast('Success', 'Saved successful');
        }
        this.postLoading = false;
        this.handlePackageLevelCancel();
        this.loading = true;
        this.loadPackageLeaveTypes();
        this.PackgeLeaveTypecheckedList = [];
      }else{
        this.postLoading = true;
        const group = this.inputForm;
        let LeaverecordNumber  =  group.get('LeaverecordNumber').value;
        let leaveActivityCode  =  this.globalS.isValueNull(group.get('leaveActivityCode').value);
        let accumulatedBy      =  this.globalS.isValueNull(group.get('accumulatedBy').value);
        let maxDays            =  this.globalS.isValueNull(group.get('maxDays').value);
        let claimReducedTo     =  this.globalS.isValueNull(group.get('claimReducedTo').value);
        
        let sql  = "UPDATE HumanResources SET [PersonID] = '', [NAME] = "+leaveActivityCode+",[SubType] = "+accumulatedBy+", [User3] = "+maxDays+", [User4] = "+claimReducedTo+", [GROUP] = 'PROG_LEAVE', [TYPE] = 'PROG_LEAVE' WHERE RecordNumber ='"+LeaverecordNumber+"'";
        this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
          if (data) 
            this.globalS.sToast('Success', 'Updated successful');     
          else
          this.globalS.sToast('Unsuccess', 'Updated successful');
          this.postLoading = false;
          this.loadPackageLeaveTypes();     
          this.handlePackageLevelCancel();  
          this.PackgeLeaveTypecheckedList = [];
          this.isUpdatePackageType = false;
          this.loading = false;
        });
      }
    }
    saveCompetency(){
      this.postLoading = true;     
      const group = this.inputForm;
      let insertOne = false;
      if(!this.isUpdatePackageType){
        this.CompetencycheckedList.forEach( (element) => {
          let is_exist   = this.globalS.isCompetencyExists(this.listCompetencyByPersonId,element);
          if(!is_exist){
            let sql = "INSERT INTO HumanResources (PersonID, [Group], Type, Name, Notes) VALUES ('"+this.personId+"', 'PROG_COMP', 'PROG_COMP', '"+element+"', '')";
            this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{  
              insertOne = true;
            });
          }
        });
        if(insertOne){
          this.globalS.sToast('Success', 'Saved successful');
        }
        this.postLoading = false;
        this.handleCompetencyCancel();
        this.loading = true;
        this.loadCompetency();
        this.CompetencycheckedList = [];
      }else{
        this.postLoading = true;
        const group = this.inputForm;
        let competenctNumber  =  group.get('competenctNumber').value;
        let leaveActivityCode  =  this.globalS.isValueNull(group.get('leaveActivityCode').value);
        let accumulatedBy      =  this.globalS.isValueNull(group.get('accumulatedBy').value);
        let maxDays            =  this.globalS.isValueNull(group.get('maxDays').value);
        let claimReducedTo     =  this.globalS.isValueNull(group.get('claimReducedTo').value);
        
        let sql  = "UPDATE HumanResources SET [PersonID] = '"+this.personId+"', [NAME] = "+leaveActivityCode+",[SubType] = "+accumulatedBy+", [User3] = "+maxDays+", [User4] = "+claimReducedTo+", [GROUP] = 'PROG_LEAVE', [TYPE] = 'PROG_LEAVE' WHERE RecordNumber ='"+competenctNumber+"'";
        this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
          if (data) 
            this.globalS.sToast('Success', 'Updated successful');     
          else
          this.globalS.sToast('Unsuccess', 'Updated successful');
          this.postLoading = false;
          this.loadCompetency();     
          this.handleCompetencyCancel();  
          this.CompetencycheckedList = [];
          this.isUpdatePackageType = false;
          this.loading = false;
        });
      }
    }
    saveApprovedServices(){
      this.postLoading = true;     
      const group = this.inputForm;
      let insertOne = false;
      if(!this.isUpdatePackageType){
        this.checkedPackageProfileServices.forEach( (element) => {
          let sql = "INSERT INTO ServiceOverview([Service Type], PersonID, [ServiceProgram], [ServiceStatus]) VALUES ('"+element+"','"+this.personId+"', 'CAPACITY BUILDING - 31600', 'ACTIVE')";
          this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{  
            insertOne = true;
          });
        });
        if(insertOne){
          this.globalS.sToast('Success', 'Saved successful');
        }
        this.postLoading = false;
        this.handleServicesCancel();
        this.loading = true;
        this.loadApprovedServices();
        this.checkedPackageProfileServices = [];
      }else{
        //   this.postLoading = true;
        //   const group = this.inputForm;
        //   let competenctNumber  =  group.get('competenctNumber').value;
        //   let leaveActivityCode  =  this.globalS.isValueNull(group.get('leaveActivityCode').value);
        //   let accumulatedBy      =  this.globalS.isValueNull(group.get('accumulatedBy').value);
        //   let maxDays            =  this.globalS.isValueNull(group.get('maxDays').value);
        //   let claimReducedTo     =  this.globalS.isValueNull(group.get('claimReducedTo').value);
        
        //   let sql  = "UPDATE HumanResources SET [PersonID] = '', [NAME] = "+leaveActivityCode+",[SubType] = "+accumulatedBy+", [User3] = "+maxDays+", [User4] = "+claimReducedTo+", [GROUP] = 'PROG_LEAVE', [TYPE] = 'PROG_LEAVE' WHERE RecordNumber ='"+competenctNumber+"'";
        //   this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
        //       if (data) 
        //       this.globalS.sToast('Success', 'Updated successful');     
        //       else
        //       this.globalS.sToast('Unsuccess', 'Updated successful');
        //       this.postLoading = false;
        //       this.loadPackageLeaveTypes();     
        //       this.handlePackageLevelCancel();  
        //       this.PackgeLeaveTypecheckedList = [];
        //       this.isUpdatePackageType = false;
        //       this.loading = false;
        //     });
      }
    }
    saveApprovedStaff(){
      this.postLoading = true;     
      const group = this.inputForm;
      let insertOne = false;
      if(!this.isUpdatePackageType){
        this.checkedListApproved.forEach( (element) => {
          let is_exist   = this.globalS.isStaffexist(this.listApprovedStaffByPersonId,element);
          if(!is_exist){
            console.log("second check")
            let is_exist   = this.globalS.isStaffexist(this.listExcludedStaffByPersonId,element);
            if(!is_exist){
              console.log("come in insertion")
              let sql = "INSERT INTO HumanResources (PersonID, [Group], Type, Name, Notes) VALUES ('"+this.personId+"', 'PROG_STAFFI', 'PROG_STAFFI','"+element+"', '')";
              this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{  
                insertOne = true;
              });
            }
          }
          console.log("outside")
        });
        if(insertOne){
          this.globalS.sToast('Success', 'Saved successful');
        }
        this.postLoading = false;
        this.handleAprfCancel();
        // this.loading = true;
        this.loadApprovedStaff();
        this.checkedListApproved = [];
      }else{
        //   this.postLoading = true;
        //   const group = this.inputForm;
        //   let competenctNumber  =  group.get('competenctNumber').value;
        //   let leaveActivityCode  =  this.globalS.isValueNull(group.get('leaveActivityCode').value);
        //   let accumulatedBy      =  this.globalS.isValueNull(group.get('accumulatedBy').value);
        //   let maxDays            =  this.globalS.isValueNull(group.get('maxDays').value);
        //   let claimReducedTo     =  this.globalS.isValueNull(group.get('claimReducedTo').value);
        
        //   let sql  = "UPDATE HumanResources SET [PersonID] = '', [NAME] = "+leaveActivityCode+",[SubType] = "+accumulatedBy+", [User3] = "+maxDays+", [User4] = "+claimReducedTo+", [GROUP] = 'PROG_LEAVE', [TYPE] = 'PROG_LEAVE' WHERE RecordNumber ='"+competenctNumber+"'";
        //   this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
        //       if (data) 
        //       this.globalS.sToast('Success', 'Updated successful');     
        //       else
        //       this.globalS.sToast('Unsuccess', 'Updated successful');
        //       this.postLoading = false;
        //       this.loadPackageLeaveTypes();     
        //       this.handlePackageLevelCancel();  
        //       this.PackgeLeaveTypecheckedList = [];
        //       this.isUpdatePackageType = false;
        //       this.loading = false;
        //     });
      }
    }
    saveExcludeStaff(){
      this.postLoading = true;     
      const group = this.inputForm;
      let insertOne = false;
      if(!this.isUpdatePackageType){
        this.checkedListExcluded.forEach( (element) => {
          let is_exist   = this.globalS.isStaffexist(this.listExcludedStaffByPersonId,element);
          if(!is_exist){
            let is_exist   = this.globalS.isStaffexist(this.listApprovedStaffByPersonId,element);
            if(!is_exist){
              let sql = "INSERT INTO HumanResources (PersonID, [Group], Type, Name, Notes) VALUES ('"+this.personId+"', 'PROG_STAFFX', 'PROG_STAFFX','"+element+"', '')";
              this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{  
                insertOne = true;
              });
            }
          }
        });
        if(insertOne){
          this.globalS.sToast('Success', 'Saved successful');
        }
        this.postLoading = false;
        this.handleUnAprfCancel();
        // this.loading = true;
        this.loadExcludedStaff();
        this.checkedListExcluded = [];
      }else{
        //   this.postLoading = true;
        //   const group = this.inputForm;
        //   let competenctNumber  =  group.get('competenctNumber').value;
        //   let leaveActivityCode  =  this.globalS.isValueNull(group.get('leaveActivityCode').value);
        //   let accumulatedBy      =  this.globalS.isValueNull(group.get('accumulatedBy').value);
        //   let maxDays            =  this.globalS.isValueNull(group.get('maxDays').value);
        //   let claimReducedTo     =  this.globalS.isValueNull(group.get('claimReducedTo').value);
        
        //   let sql  = "UPDATE HumanResources SET [PersonID] = '', [NAME] = "+leaveActivityCode+",[SubType] = "+accumulatedBy+", [User3] = "+maxDays+", [User4] = "+claimReducedTo+", [GROUP] = 'PROG_LEAVE', [TYPE] = 'PROG_LEAVE' WHERE RecordNumber ='"+competenctNumber+"'";
        //   this.menuS.InsertDomain(sql).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
        //       if (data) 
        //       this.globalS.sToast('Success', 'Updated successful');     
        //       else
        //       this.globalS.sToast('Unsuccess', 'Updated successful');
        //       this.postLoading = false;
        //       this.loadPackageLeaveTypes();     
        //       this.handlePackageLevelCancel();  
        //       this.PackgeLeaveTypecheckedList = [];
        //       this.isUpdatePackageType = false;
        //       this.loading = false;
        //     });
      }
    }
    loadPackageLeaveTypes(){
      this.menuS.getlistPackageLeaveTypes().subscribe(data => {
        this.tableDataPackageLeaveTypes = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    loadApprovedStaff(){
      this.menuS.getlistApprovedStaffByPersonId(this.personId).subscribe(data => {
        this.listApprovedStaffByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    loadExcludedStaff(){
      this.menuS.getlistExcludedStaffByPersonId(this.personId).subscribe(data => {
        this.listExcludedStaffByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    loadCompetency(){
      this.menuS.getlistCompetencyByPersonId(this.personId).subscribe(data => {
        this.listCompetencyByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    loadApprovedServices(){
      this.menuS.getlistApprovedServicesByPersonId("this.personId").subscribe(data => {
        this.listApprovedServicesByPersonId = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    showPackageLeaveEditModal(index: any){
      this.packageLevel = true;
      this.isUpdatePackageType = true;
      const { 
        leaveActivityCode,
        claimReducedTo,
        accumulatedBy,
        maxDays,
        recordNumber,
      } = this.tableDataPackageLeaveTypes[index];
      this.inputForm.patchValue({
        claimReducedTo: claimReducedTo,
        maxDays: maxDays,
        leaveActivityCode: leaveActivityCode,
        accumulatedBy:accumulatedBy,
        LeaverecordNumber:recordNumber,
      });
    }
    showComptencyEditModal(index: any){
      this.competencymodal = true;
      this.isUpdatePackageType = true;
      const { 
        competency,
        mandatory,
        personId,
        recordNumber,
      } = this.listCompetencyByPersonId[index];
      this.inputForm.patchValue({
        competency: competency,
        mandatory: (mandatory == null) ? false : true,
        personId: personId,
        LeaverecordNumber:recordNumber,
      });
    }
    handlePackageLevelCancel() {
      this.packageLevel = false;
    }
    showServicesModal(){
      this.servicesModal  =true;
    }
    handleServicesCancel() {
      this.packedTypeProfile.forEach(x => {
        x.checked = false
      });
      this.servicesModal = false;
      this.checkedPackageProfileServices=[];
    }
    showstaffApprovedModal(){
      // this.resetModal();
      this.staffApproved = true;
    }
    handleAprfCancel(){
      this.careWorkers.forEach(x => {
        x.checked = false
      });
      this.staffApproved = false;
    }
    showstaffUnApprovedModal(){
      this.staffUnApproved = true;
    }
    handleUnAprfCancel(){
      this.careWorkers.forEach(x => {
        x.checked = false
      });
      this.staffUnApproved = false;
    }
    showCompetencyModal(){
      this.competencyList.forEach(x => {
        x.checked = false
      });
      this.CompetencycheckedList = [];
      this.competencymodal = true;
      this.isUpdatePackageType = false;
    }
    handleCompetencyCancel(){
      this.competencymodal = false;
      // this.isUpdatePackageType = false;
    }
    //End Opening of All Modals
    loadTitle()
    {
      return this.title;
    }
    onCheckboxServiceChange(option,event){
      if(event.target.checked){
        this.checkedPackageProfileServices.push(option.title);
      }else{
        this.checkedPackageProfileServices.filter(m=>m != option.title);
      }
    }
    searchPackageLeaves(event){
      this.temp = [];
      this.leaveTypes = this.leaveTypes_copy;
      if(event.target.value != ""){
        this.temp = this.leaveTypes.filter(res=>{
          return res.title.toLowerCase().match(event.target.value.toLowerCase());
        })
        this.leaveTypes = this.temp;
      }else if(event.target.value == ""){
        this.leaveTypes = this.leaveTypes_copy;
      }
    }
    searchApprovedServices(event){
      this.temp = [];
      this.packedTypeProfile = this.packedTypeProfileCopy;
      if(event.target.value != ""){
        this.temp = this.packedTypeProfile.filter(res=>{
          return res.title.toLowerCase().match(event.target.value.toLowerCase());
        })
        this.packedTypeProfile = this.temp;
      }else if(event.target.value == ""){
        this.packedTypeProfile = this.packedTypeProfileCopy;
      }
    }
    searchCompetenncy(event){
      this.temp = [];
      this.competencyList = this.competencyListCopy;
      if(event.target.value != ""){
        this.temp = this.competencyList.filter(res=>{
          return res.name.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
        })
        this.competencyList = this.temp;
      }else if(event.target.value == ""){
        this.competencyList = this.competencyListCopy;
      }
    }
    searchStaff(event){
      this.temp = [];
      this.careWorkers = this.careWorkersCopy;
      if(event.target.value != ""){
        this.temp = this.careWorkers.filter(res=>{
          return res.accountno.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
        })
        this.careWorkers = this.temp;
      }else if(event.target.value == ""){
        this.careWorkers = this.careWorkersCopy;
      }      
    }
    searchStaffExcluded(event){
      this.temp = [];
      this.careWorkersExcluded = this.careWorkersExcludedCopy;
      if(event.target.value != ""){
        this.temp = this.careWorkersExcluded.filter(res=>{
          return res.accountno.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
        })
        this.careWorkersExcluded = this.temp;
      }else if(event.target.value == ""){
        this.careWorkersExcluded = this.careWorkersExcludedCopy;
      }      
    }
    onCheckboxChangePackageTypes(option,event){
      if(event.target.checked){
        this.PackgeLeaveTypecheckedList.push(option.title);
      } else {
        this.PackgeLeaveTypecheckedList = this.PackgeLeaveTypecheckedList.filter(m=>m!= option.title)
      }
    }
    
    onCompetencyCheckboxChange(option, event) {
      if(event.target.checked){
        this.CompetencycheckedList.push(option.name);
      } else {
        this.CompetencycheckedList = this.CompetencycheckedList.filter(m=>m!= option.name)
      }
    }
    onCheckboxUnapprovedChange(option, event) {
      if(event.target.checked){
        this.checkedListExcluded.push(option.accountno);
      } else {
        this.checkedListExcluded = this.checkedListExcluded.filter(m=>m!= option.accountno)
      }
    }
    onCheckboxapprovedChange(option, event)
    {
      if(event.target.checked){
        this.checkedListApproved.push(option.accountno);
      } else {
        this.checkedListApproved = this.checkedListApproved.filter(m=>m!= option.accountno)
      }
    }
    
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    
    changePageNumber(page){
      console.log(page);  
    }
    
    showEditModal(index: any) {
      this.title = "Edit Program/Packages"
      this.isUpdate = true;
      this.current = 0;
      this.modalOpen = true;
      
      
      
      
      const {
        fundingSource,
        type,
        title,
        agencyID,
        state,
        gst,
        rate,
        glExp,
        fundingRegion,
        gLRev,
        gLSuper,
        budgetdollar,
        budgetHrs,
        careDomain,
        closeDate,
        bgtCycle,
        cordinators,
        fundingType,
        contiguency,
        individuallyFunded,
        level,
        budgetRosterEnforcement,
        budgetEnforcement,
        programJob,
        template,
        agedCarePackage,
        conguencyPackage,
        targetGroup,
        user11,
        user12,
        user13,
        cdcStatementText1,
        defaultCHGTRAVELBetweenProgram,
        defaultCHGTRAVELWithInProgram,
        defaultCHGTravelBetweenActivity,
        defaultCHGTravelBetweenPayType,
        defaultCHGTravelWithinActivity,
        defaultCHGTravelWithinPayType,
        defaultNCTravelBetweenActivity,
        defaultNCTravelBetweenPayType,
        defaultNCTravelBetweenProgram,
        defaultNCTravelWithinActivity,
        defaultNCTravelWithinPayType,
        defaultNCTravelWithinProgram,
        p_Def_Admin_AdminDay,
        p_Def_Admin_AdminFrequency,
        p_Def_Admin_AdminType,
        p_Def_Admin_CMDay,
        p_Def_Admin_CMFrequency,
        p_Def_Admin_CMType,
        p_Def_Admin_CM_PercAmt,
        p_Def_Admin_Admin_PercAmt,
        p_Def_Alert_Allowed,
        p_Def_Alert_BaseOn,
        p_Def_Alert_Orange,
        p_Def_Alert_Period,
        p_Def_Alert_Red,
        p_Def_Alert_Type,
        p_Def_Alert_Yellow,
        p_Def_Contingency_Max,
        p_Def_Contingency_PercAmt,
        p_Def_Expire_Amount,
        p_Def_Expire_CostType,
        p_Def_Expire_Length,
        p_Def_Expire_Period,
        p_Def_Expire_Unit,
        p_Def_Expire_Using,
        defaultDailyFee,
        p_Def_Fee_BasicCare,
        p_Def_IncludeBasicCareFeeInAdmin,
        p_Def_IncludeClientFeesInCont,
        p_Def_IncludeIncomeTestedFeeInAdmin,
        p_Def_IncludeTopUpFeeInAdmin,
        p_Def_QueryAutoDeleteAdmin,
        p_Def_StdDisclaimer,
        defaultNoNoticeBillProgram,
        defaultNoNoticeCancel,
        defaultNoNoticePayProgram,
        defaultNoNoticePayType,
        defaultShortNoticeBillProgram,
        defaultShortNoticeCancel,
        defaultShortNoticePayProgram,
        defaultShortNoticePayType,
        defaultWithNoticeCancel,
        defaultWithNoticeProgram,
        noNoticeCancelRate,
        noNoticeLeadTime,
        noNoticeLeaveActivity,
        shortNoticeCancelRate,
        shortNoticeLeadTime,
        shortNoticeLeaveActivity,
        recordNumber,
      } = this.tableData[index-1];
      this.inputForm.patchValue({
        funding_source:type,
        name:title,
        agency_id:agencyID,
        state:state,
        gst:(gst) ? true : false,
        gst_Percent:rate,
        coordinator:cordinators,
        glrev:gLRev,
        glexp:glExp,
        glsuper:gLSuper,
        period:bgtCycle,
        bamount:budgetdollar,
        bhrs:budgetHrs,
        care:careDomain,
        funding_region:fundingRegion,
        funding_type:fundingType,
        close_date:closeDate,
        budget_enfor:budgetEnforcement,
        roster_enfor:budgetRosterEnforcement,
        level:level,
        target_g:targetGroup,
        template:template,
        individual:individuallyFunded,
        radioValue:programJob,
        radioValue2:contiguency,
        aged:agedCarePackage,
        contigency:conguencyPackage,
        p_alert_type:p_Def_Alert_Type,
        p_alert_period:p_Def_Alert_Period,
        allowed:p_Def_Alert_Allowed,
        yellow:p_Def_Alert_Yellow,
        green:p_Def_Alert_Orange,
        red:p_Def_Alert_Red,
        expire_amount:p_Def_Expire_Amount,
        expire_costType:p_Def_Expire_CostType,
        expire_unit:p_Def_Expire_Unit,
        expire_period:p_Def_Expire_Period,
        expire_length:p_Def_Expire_Length,        
        expire_using:p_Def_Expire_Using,
        adminType:p_Def_Admin_AdminType,
        admincmType:p_Def_Admin_CMType,
        admin_parc_amt:p_Def_Admin_Admin_PercAmt,
        admin_cm_parc_amt:p_Def_Admin_CM_PercAmt,
        adminFrequency:p_Def_Admin_AdminFrequency,
        cmFrequency:p_Def_Admin_CMFrequency,
        cycle:user12,
        standard_quote:p_Def_StdDisclaimer,
        default_daily_fees:defaultDailyFee,
        max_contiguency:p_Def_Contingency_Max,
        perc_amt:p_Def_Contingency_PercAmt,
        defaultbasiccarefee:p_Def_Fee_BasicCare,
        IncludeTopUp:p_Def_IncludeTopUpFeeInAdmin,
        IncludeCare:p_Def_IncludeBasicCareFeeInAdmin,
        includetested:p_Def_IncludeIncomeTestedFeeInAdmin,
        includeClientFeesCont:p_Def_IncludeClientFeesInCont,
        adminDay:p_Def_Admin_AdminDay,
        cmday:p_Def_Admin_CMDay,
        quoute_template:user13,
        line_1:this.SplitData(cdcStatementText1,0),
        line_2:this.SplitData(cdcStatementText1,1),
        nprogram:defaultNCTravelBetweenProgram,
        nactivity:defaultNCTravelBetweenActivity,
        npay:defaultNCTravelBetweenPayType,
        cprogram:defaultCHGTRAVELBetweenProgram,
        cactivity:defaultCHGTravelBetweenActivity,
        cpay:defaultCHGTravelBetweenPayType,
        wnprogram:defaultNCTravelWithinProgram,
        wnactivity:defaultNCTravelWithinActivity,
        wnpay:defaultNCTravelWithinPayType,
        wcprogram:defaultCHGTRAVELWithInProgram,
        wcactivity:defaultCHGTravelWithinActivity,
        wcpay:defaultCHGTravelWithinPayType,
        defaultNoNoticeBillProgram:defaultNoNoticeBillProgram,
        defaultNoNoticeCancel:defaultNoNoticeCancel,
        defaultNoNoticePayProgram:defaultNoNoticePayProgram,
        defaultNoNoticePayType:defaultNoNoticePayType,
        defaultShortNoticeBillProgram:defaultShortNoticeBillProgram,
        defaultShortNoticeCancel:defaultShortNoticeCancel,
        defaultShortNoticePayProgram:defaultShortNoticePayProgram,
        defaultShortNoticePayType:defaultShortNoticePayType,
        defaultWithNoticeCancel:defaultWithNoticeCancel,
        defaultWithNoticeProgram:defaultWithNoticeProgram,
        noNoticeCancelRate:noNoticeCancelRate,
        noNoticeLeadTime:noNoticeLeadTime,
        noNoticeLeaveActivity:noNoticeLeaveActivity,
        shortNoticeCancelRate:shortNoticeCancelRate,
        shortNoticeLeadTime:shortNoticeLeadTime,
        shortNoticeLeaveActivity:shortNoticeLeaveActivity,
        recordNumber:recordNumber
      });
    }
    
    SplitData(cdcStatementText1,position){
      if(cdcStatementText1 != '' && cdcStatementText1 != null){
        let index  = 0 ;
        if(position == 1){
          index = 1;
        }
        var result = cdcStatementText1.split("||");
        return result[index];
      }else{
        return '';
      }
    }
    handleCancel() {
      this.modalOpen = false;
    }
    onIndexChange(index: number): void {
      this.current = index;
      
      if(this.current == 2 || this.current == 3 || this.current == 4){
        const validateForm = this.inputForm ;
        let fundingSource =  this.globalS.isValueNull(validateForm.get('funding_source').value);
        let title         =  this.globalS.isValueNull(validateForm.get('name').value);
        if(this.globalS.isVarNull(fundingSource) || this.globalS.isVarNull(title)){
          this.globalS.iToast('Alert', 'Please Add atleast Funding Source and Name First !');
          this.current = 0; 
        }
      }
      
    }
    save() {
      this.postLoading = true;     
      const group = this.inputForm;
      if(!this.isUpdate){
        
        let fundingSource =  this.globalS.isValueNull(group.get('funding_source').value);
        let title         =  this.globalS.isValueNull(group.get('name').value.trim().toUpperCase());
        let agencyID      =  this.globalS.isValueNull(group.get('agency_id').value);
        let state         =  this.globalS.isValueNull(group.get('state').value);
        let gst           =  this.trueString(group.get('gst').value);
        let rate          =  this.globalS.isValueNull(group.get('gst_Percent').value);
        let cordinators   =  this.globalS.isValueNull(group.get('coordinator').value);
        let glExp         =  this.globalS.isValueNull(group.get('glrev').value);
        let glRev         =  this.globalS.isValueNull(group.get('glexp').value);
        let glSuper       =  this.globalS.isValueNull(group.get('glsuper').value);
        let bgtCycle      =  this.globalS.isValueNull(group.get('period').value);
        let budget$       =  this.globalS.isValueNull(group.get('bamount').value);
        let budgetHrs     =  this.globalS.isValueNull(group.get('bhrs').value);
        let careDomain    =  this.globalS.isValueNull(group.get('care').value);
        let fundingRegion =  this.globalS.isValueNull(group.get('funding_region').value);
        let fundingType   =  this.globalS.isValueNull(group.get('funding_type').value);
        let end_date      =  !(this.globalS.isVarNull(group.get('end_date').value)) ?  "'"+this.globalS.convertDbDate(group.get('end_date').value)+"'" : null;
        let closeDate     =  !(this.globalS.isVarNull(group.get('close_date').value)) ?  "'"+this.globalS.convertDbDate(group.get('close_date').value)+"'" : null;
        let budgetEnforcement       = this.globalS.isValueNull(group.get('budget_enfor').value);
        let budgetRosterEnforcement = this.globalS.isValueNull(group.get('roster_enfor').value);
        let level                   = this.globalS.isValueNull(group.get('level').value);
        let targetGroup             = this.globalS.isValueNull(group.get('target_g').value);
        let template                = this.trueString(group.get('template').value);
        let individuallyFunded      = this.trueString(group.get('individual').value);
        let programJob              = this.globalS.isValueNull(group.get('radioValue').value);
        let contiguency             = this.globalS.isValueNull(group.get('radioValue2').value);
        let agedCarePackage         = this.trueString(group.get('aged').value);
        let conguencyPackage        = this.globalS.isValueNull(group.get('contigency').value); 
        let user8                   = "'RECURRENT FUNDING'";
        let user9                   = "'AT FUNDING EXPIRY'";
        let User11                  = "''";
        let User12                  = this.globalS.isValueNull(group.get('cycle').value);
        let P_Def_Alert_Type    = this.globalS.isValueNull(group.get('p_alert_type').value);
        let P_Def_Alert_Period  = this.globalS.isValueNull(group.get('p_alert_period').value);
        let allowed             = this.globalS.isValueNull(group.get('allowed').value);
        let yellow              = this.globalS.isValueNull(group.get('yellow').value);
        let green               = this.globalS.isValueNull(group.get('green').value);
        let red                 = this.globalS.isValueNull(group.get('red').value);
        let expire_amount       = this.globalS.isValueNull(group.get('expire_amount').value);
        let expire_costType     = this.globalS.isValueNull(group.get('expire_costType').value);
        let expire_unit         = this.globalS.isValueNull(group.get('expire_unit').value);
        let expire_period       = this.globalS.isValueNull(group.get('expire_period').value);
        let expire_length       = this.globalS.isValueNull(group.get('expire_length').value);
        let defaultbasiccarefee = this.globalS.isValueNull(group.get('defaultbasiccarefee').value);
        let expire_using        = this.globalS.isValueNull(group.get('expire_using').value);
        let adminType           = this.globalS.isValueNull(group.get('adminType').value);
        let admin_parc_amt      = this.globalS.isValueNull(group.get('admin_parc_amt').value); 
        let admin_cm_parc_amt   = this.globalS.isValueNull(group.get('admin_cm_parc_amt').value); 
        let adminFrequency      = this.globalS.isValueNull(group.get('adminFrequency').value);
        let cmFrequency         = this.globalS.isValueNull(group.get('cmFrequency').value);
        let admincmType         = this.globalS.isValueNull(group.get('admincmType').value);
        let standard_quote      = this.globalS.isValueNull(group.get('standard_quote').value);
        let defaultdailyfee     = this.globalS.isValueNull(group.get('defaultdailyfee').value);
        let max_contiguency     = this.globalS.isValueNull(group.get('max_contiguency').value);
        let perc_amt            = this.globalS.isValueNull(group.get('perc_amt').value);
        let adminDay            = this.globalS.isValueNull(group.get('adminDay').value);
        let cmday               = this.globalS.isValueNull(group.get('cmday').value);
        let default_daily_fees  = this.globalS.isValueNull(group.get('default_daily_fees').value);
        let IncludeTopUp        = this.trueString(group.get('IncludeTopUp').value);
        let IncludeCare         = this.trueString(group.get('IncludeCare').value);
        let includetested       = this.trueString(group.get('includetested').value);
        let includeClientFeesCont = this.trueString(group.get('includeClientFeesCont').value);
        let quoute_template = this.globalS.isValueNull(group.get('quoute_template').value); 
        let line1           = this.globalS.isValueNull(group.get('line1').value);
        line1 = isEmpty(line1) ? "Unused funds are returned to the Department on package cessation" : line1;        
        let line2           = this.globalS.isValueNull(group.get('line2').value);
        line2 = isEmpty(line2) ? "For Queries and Inquiries Please Phone" : line2;  
        let text1           =  line1+"||"+line2;
        let nprogram        =  this.globalS.isValueNull(group.get('nprogram').value); 
        let nactivity       =  this.globalS.isValueNull(group.get('nactivity').value); 
        let npay            =  this.globalS.isValueNull(group.get('npay').value); 
        let cprogram        =  this.globalS.isValueNull(group.get('cprogram').value); 
        let cactivity       =  this.globalS.isValueNull(group.get('cactivity').value); 
        let cpay            =  this.globalS.isValueNull(group.get('cpay').value); 
        let wnprogram       =  this.globalS.isValueNull(group.get('wnprogram').value); 
        let wnactivity      =  this.globalS.isValueNull(group.get('wnactivity').value); 
        let wnpay           =  this.globalS.isValueNull(group.get('wnpay').value); 
        let wcprogram       =  this.globalS.isValueNull(group.get('wnprogram').value); 
        let wcactivity      =  this.globalS.isValueNull(group.get('wcactivity').value); 
        let wcpay           =  this.globalS.isValueNull(group.get('wcpay').value); 
        let  defaultNoNoticeBillProgram    =   this.globalS.isValueNull(group.get('defaultNoNoticeBillProgram').value);
        let  defaultNoNoticeCancel         =   this.globalS.isValueNull(group.get('defaultNoNoticeCancel').value);
        let  defaultNoNoticePayProgram     =   this.globalS.isValueNull(group.get('defaultNoNoticePayProgram').value);
        let  defaultWithNoticeCancel       =   this.globalS.isValueNull(group.get('defaultWithNoticeCancel').value);
        let  defaultWithNoticeProgram      =   this.globalS.isValueNull(group.get('defaultWithNoticeProgram').value);
        let  noNoticeCancelRate            =   this.globalS.isValueNull(group.get('noNoticeCancelRate').value);
        let  noNoticeLeadTime              =   this.globalS.isValueNull(group.get('noNoticeLeadTime').value);
        let  noNoticeLeaveActivity         =   this.globalS.isValueNull(group.get('noNoticeLeaveActivity').value);
        let  shortNoticeCancelRate         =   this.globalS.isValueNull(group.get('shortNoticeCancelRate').value);
        let  shortNoticeLeadTime           =   this.globalS.isValueNull(group.get('shortNoticeLeadTime').value);
        let  shortNoticeLeaveActivity      =   this.globalS.isValueNull(group.get('shortNoticeLeaveActivity').value);
        let  defaultShortNoticePayType     =   this.globalS.isValueNull(group.get('defaultShortNoticePayType').value);
        let  defaultShortNoticePayProgram  =   this.globalS.isValueNull(group.get('defaultShortNoticePayProgram').value);
        let  defaultShortNoticeBillProgram =   this.globalS.isValueNull(group.get('defaultShortNoticeBillProgram').value);
        let  defaultShortNoticeCancel      =   this.globalS.isValueNull(group.get('defaultShortNoticeCancel').value);
        let  defaultNoNoticePayType        =   this.globalS.isValueNull(group.get('defaultNoNoticePayType').value);
        
        let values = "'PROGRAMS'"+","+title+","+fundingSource+","+agencyID+","+cordinators+","+fundingRegion
        +","+careDomain+","+fundingType+","+state+","+gst
        +","+rate+","+budget$+","+budgetHrs+","+bgtCycle
        +","+glExp+","+glRev+","+glSuper+","+closeDate
        +","+programJob+","+template+","+contiguency+","+level
        +","+targetGroup+","+conguencyPackage+","+budgetEnforcement
        +","+budgetRosterEnforcement+","+agedCarePackage
        +","+individuallyFunded
        +","+user8+","+user9+","+User11+","+User12+","+P_Def_Alert_Type+","+P_Def_Alert_Period
        +","+allowed+","+yellow+","+green+","+red
        +","+expire_amount+","+expire_costType+","+expire_unit+","+expire_period+","+expire_length
        +","+defaultbasiccarefee+","+perc_amt+","+adminType
        +","+admincmType+","+admin_cm_parc_amt+","+admin_parc_amt+","+standard_quote
        +","+adminFrequency+","+cmFrequency+","+adminDay+","+cmday+","+expire_using+","+max_contiguency
        +","+wcactivity+","+wcpay+","+wnprogram+","+wnactivity+","+wnpay
        +","+cactivity+","+cpay+","+nprogram
        +","+nactivity+","+npay+","+cprogram+","+wcprogram
        +","+includeClientFeesCont+","+includetested+","+IncludeCare+","+IncludeTopUp
        +",'"+text1+"',"+default_daily_fees+","+noNoticeLeadTime+","+shortNoticeLeadTime+","+noNoticeLeaveActivity
        +","+shortNoticeLeaveActivity+","+defaultNoNoticeCancel+","+defaultNoNoticeBillProgram+","+defaultNoNoticePayProgram+","+defaultNoNoticePayType
        +","+defaultShortNoticeCancel+","+defaultShortNoticeBillProgram+","+defaultShortNoticePayProgram+","+defaultShortNoticePayType
        +","+defaultWithNoticeCancel+","+noNoticeCancelRate+","+shortNoticeCancelRate+","+defaultWithNoticeProgram;
        
        let sqlz = "insert into humanresourcetypes ([Group],[Name],[type],[address1],[address2],[Suburb],[HRT_DATASET],[USER1],[Phone2],[gst],[GSTRate],[budgetamount],[budget_1],[budgetperiod],[fax],[email],[phone1],[CloseDate],[Postcode],[UserYesNo3],[User2],[User3],[User4],[User10],[BudgetEnforcement],[BudgetRosterEnforcement],[UserYesNo1],[UserYesNo2],[User8],[User9],[User11],[User12],[P_Def_Alert_Type],[P_Def_Alert_Period],[P_Def_Alert_Allowed],[P_Def_Alert_Yellow],[P_Def_Alert_Orange],[P_Def_Alert_Red],[P_Def_Expire_Amount],[P_Def_Expire_CostType],[P_Def_Expire_Unit],[P_Def_Expire_Period],[P_Def_Expire_Length],[P_Def_Fee_BasicCare],[P_Def_Contingency_PercAmt],[P_Def_Admin_AdminType],[P_Def_Admin_CMType],[P_Def_Admin_CM_PercAmt],[p_Def_Admin_Admin_PercAmt],[P_Def_StdDisclaimer],[P_Def_Admin_AdminFrequency],[P_Def_Admin_CMFrequency],[P_Def_Admin_AdminDay] ,[P_Def_Admin_CMDay],[P_Def_Expire_Using],[P_Def_Contingency_Max],[DefaultCHGTravelWithinActivity],[DefaultCHGTravelWithinPayType],[DefaultNCTravelWithinProgram],[DefaultNCTravelWithinActivity],[DefaultNCTravelWithinPayType],[DefaultCHGTravelBetweenActivity],[DefaultCHGTravelBetweenPayType],[DefaultNCTravelBetweenProgram],[DefaultNCTravelBetweenActivity],[DefaultNCTravelBetweenPayType],[DefaultCHGTRAVELBetweenProgram],[DefaultCHGTRAVELWithInProgram],[P_Def_IncludeClientFeesInCont],[P_Def_IncludeIncomeTestedFeeInAdmin],[P_Def_IncludeBasicCareFeeInAdmin],[P_Def_IncludeTopUpFeeInAdmin],[CDCStatementText1],[DefaultDailyFee],[NoNoticeLeadTime],[ShortNoticeLeadTime],[NoNoticeLeaveActivity],[ShortNoticeLeaveActivity],[DefaultNoNoticeCancel],[DefaultNoNoticeBillProgram],[DefaultNoNoticePayProgram],[DefaultNoNoticePayType],[DefaultShortNoticeCancel],[DefaultShortNoticeBillProgram],[DefaultShortNoticePayProgram],[DefaultShortNoticePayType],[DefaultWithNoticeCancel],[NoNoticeCancelRate],[ShortNoticeCancelRate],[DefaultWithNoticeProgram]) values("+values+");select @@IDENTITY";
        
        this.menuS.InsertDomain(sqlz).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
          if (data){
            this.personId = data;
            this.globalS.sToast('Success', 'Saved successful');
            this.loadData();
            this.postLoading = false;   
            this.loading = false;
          }
          else{
            this.personId = data;
            this.globalS.sToast('Success', 'Saved successful');
            this.loadData();
            this.loading = false;   
            this.postLoading = false;
          }
        });
      }
      else
      {
        this.postLoading = true;     
        const group = this.inputForm;
        let fundingSource =  this.globalS.isValueNull(group.get('funding_source').value);
        let title         =  this.globalS.isValueNull(group.get('name').value);
        let agencyID      =  this.globalS.isValueNull(group.get('agency_id').value);
        let state         =  this.globalS.isValueNull(group.get('state').value);
        let gst           =  this.trueString(group.get('gst').value);
        let rate          =  this.globalS.isValueNull(group.get('gst_Percent').value);
        let cordinators   =  this.globalS.isValueNull(group.get('coordinator').value);
        let glExp         =  this.globalS.isValueNull(group.get('glrev').value);
        let glRev         =  this.globalS.isValueNull(group.get('glexp').value);
        let glSuper       =  this.globalS.isValueNull(group.get('glsuper').value);
        let bgtCycle      =  this.globalS.isValueNull(group.get('period').value);
        let budget$       =  this.globalS.isValueNull(group.get('bamount').value);
        let budgetHrs     =  this.globalS.isValueNull(group.get('bhrs').value);
        let careDomain    =  this.globalS.isValueNull(group.get('care').value);
        let fundingRegion =  this.globalS.isValueNull(group.get('funding_region').value);
        let fundingType   =  this.globalS.isValueNull(group.get('funding_type').value);
        let end_date      =  !(this.globalS.isVarNull(group.get('end_date').value)) ?  "'"+this.globalS.convertDbDate(group.get('end_date').value)+"'" : null;
        let closeDate     =  !(this.globalS.isVarNull(group.get('close_date').value)) ?  "'"+this.globalS.convertDbDate(group.get('close_date').value)+"'" : null;
        let budgetEnforcement       = this.globalS.isValueNull(group.get('budget_enfor').value);
        let budgetRosterEnforcement = this.globalS.isValueNull(group.get('roster_enfor').value);
        let level                   = this.globalS.isValueNull(group.get('level').value);
        let targetGroup             = this.globalS.isValueNull(group.get('target_g').value);
        let template                = this.trueString(group.get('template').value);
        let individuallyFunded      = this.trueString(group.get('individual').value);
        let programJob              = this.globalS.isValueNull(group.get('radioValue').value);
        let contiguency             = this.globalS.isValueNull(group.get('radioValue2').value);
        let agedCarePackage         = this.trueString(group.get('aged').value);
        let conguencyPackage        = this.globalS.isValueNull(group.get('contigency').value); 
        let user8                   = "'RECURRENT FUNDING'";
        let user9                   = "'AT FUNDING EXPIRY'";
        let User11                  = "''";
        let User12                  = this.globalS.isValueNull(group.get('cycle').value);
        let P_Def_Alert_Type    = this.globalS.isValueNull(group.get('p_alert_type').value);
        let P_Def_Alert_Period  = this.globalS.isValueNull(group.get('p_alert_period').value);
        let allowed             = this.globalS.isValueNull(group.get('allowed').value);
        let yellow              = this.globalS.isValueNull(group.get('yellow').value);
        let green               = this.globalS.isValueNull(group.get('green').value);
        let red                 = this.globalS.isValueNull(group.get('red').value);
        let expire_amount       = this.globalS.isValueNull(group.get('expire_amount').value);
        let expire_costType     = this.globalS.isValueNull(group.get('expire_costType').value);
        let expire_unit         = this.globalS.isValueNull(group.get('expire_unit').value);
        let expire_period       = this.globalS.isValueNull(group.get('expire_period').value);
        let expire_length       = this.globalS.isValueNull(group.get('expire_length').value);
        let defaultbasiccarefee = this.globalS.isValueNull(group.get('defaultbasiccarefee').value);
        let expire_using        = this.globalS.isValueNull(group.get('expire_using').value);
        let adminType           = this.globalS.isValueNull(group.get('adminType').value);
        let admin_parc_amt      = this.globalS.isValueNull(group.get('admin_parc_amt').value); 
        let admin_cm_parc_amt   = this.globalS.isValueNull(group.get('admin_cm_parc_amt').value); 
        let adminFrequency      = this.globalS.isValueNull(group.get('adminFrequency').value);
        let cmFrequency         = this.globalS.isValueNull(group.get('cmFrequency').value);
        let admincmType         = this.globalS.isValueNull(group.get('admincmType').value);
        let standard_quote      = this.globalS.isValueNull(group.get('standard_quote').value);
        let defaultdailyfee     = this.globalS.isValueNull(group.get('defaultdailyfee').value);
        let max_contiguency     = this.globalS.isValueNull(group.get('max_contiguency').value);
        let perc_amt            = this.globalS.isValueNull(group.get('perc_amt').value);
        let adminDay            = this.globalS.isValueNull(group.get('adminDay').value);
        let cmday               = this.globalS.isValueNull(group.get('cmday').value);
        let default_daily_fees  = this.globalS.isValueNull(group.get('default_daily_fees').value);
        let IncludeTopUp        = this.trueString(group.get('IncludeTopUp').value);
        let IncludeCare         = this.trueString(group.get('IncludeCare').value);
        let includetested       = this.trueString(group.get('includetested').value);
        let includeClientFeesCont = this.trueString(group.get('includeClientFeesCont').value);
        let quoute_template = this.globalS.isValueNull(group.get('quoute_template').value); 
        
        let line1           = this.globalS.isValueNull(group.get('line1').value);
        line1 = isEmpty(line1) ? "Unused funds are returned to the Department on package cessation" : line1;        
        
        let line2           = this.globalS.isValueNull(group.get('line2').value);
        line2 = isEmpty(line2) ? "For Queries and Inquiries Please Phone" : line2;  
        
        let text1           =  line1+"||"+line2;
        
        let nprogram        =  this.globalS.isValueNull(group.get('nprogram').value); 
        let nactivity       =  this.globalS.isValueNull(group.get('nactivity').value); 
        let npay            =  this.globalS.isValueNull(group.get('npay').value); 
        let cprogram        =  this.globalS.isValueNull(group.get('cprogram').value); 
        let cactivity       =  this.globalS.isValueNull(group.get('cactivity').value); 
        let cpay            =  this.globalS.isValueNull(group.get('cpay').value); 
        let wnprogram       =  this.globalS.isValueNull(group.get('wnprogram').value); 
        let wnactivity      =  this.globalS.isValueNull(group.get('wnactivity').value); 
        let wnpay           =  this.globalS.isValueNull(group.get('wnpay').value); 
        let wcprogram       =  this.globalS.isValueNull(group.get('wnprogram').value); 
        let wcactivity      =  this.globalS.isValueNull(group.get('wcactivity').value); 
        let wcpay           =  this.globalS.isValueNull(group.get('wcpay').value); 
        let  defaultNoNoticeBillProgram    =   this.globalS.isValueNull(group.get('defaultNoNoticeBillProgram').value);
        let  defaultNoNoticeCancel         =   this.globalS.isValueNull(group.get('defaultNoNoticeCancel').value);
        let  defaultNoNoticePayProgram     =   this.globalS.isValueNull(group.get('defaultNoNoticePayProgram').value);
        let  defaultWithNoticeCancel       =   this.globalS.isValueNull(group.get('defaultWithNoticeCancel').value);
        let  defaultWithNoticeProgram      =   this.globalS.isValueNull(group.get('defaultWithNoticeProgram').value);
        let  noNoticeCancelRate            =   this.globalS.isValueNull(group.get('noNoticeCancelRate').value);
        let  noNoticeLeadTime              =   this.globalS.isValueNull(group.get('noNoticeLeadTime').value);
        let  noNoticeLeaveActivity         =   this.globalS.isValueNull(group.get('noNoticeLeaveActivity').value);
        let  shortNoticeCancelRate         =   this.globalS.isValueNull(group.get('shortNoticeCancelRate').value);
        let  shortNoticeLeadTime           =   this.globalS.isValueNull(group.get('shortNoticeLeadTime').value);
        let  shortNoticeLeaveActivity      =   this.globalS.isValueNull(group.get('shortNoticeLeaveActivity').value);
        let  defaultShortNoticePayType     =   this.globalS.isValueNull(group.get('defaultShortNoticePayType').value);
        let  defaultShortNoticePayProgram  =   this.globalS.isValueNull(group.get('defaultShortNoticePayProgram').value);
        let  defaultShortNoticeBillProgram =   this.globalS.isValueNull(group.get('defaultShortNoticeBillProgram').value);
        let  defaultShortNoticeCancel      =   this.globalS.isValueNull(group.get('defaultShortNoticeCancel').value);
        let  defaultNoNoticePayType        =   this.globalS.isValueNull(group.get('defaultNoNoticePayType').value); 
        let  recordNumber       = group.get('recordNumber').value;
        let prog = "'PROGRAMS'";
        
        let sqlUpdate = "Update humanresourcetypes SET [Group]="+ prog + ",[Name]="+ title + ",[type]="+ fundingSource + ",[address1]="+ agencyID + ",[address2]="+ cordinators + ", [Suburb]="+ fundingRegion + ",[HRT_DATASET]="+ careDomain + ",[USER1]="+ fundingType + ",[Phone2]="+ state + ",[gst]="+ gst + ",[GSTRate]="+ rate + ",[budgetamount]="+ budget$ + ",[budget_1]="+ budgetHrs + ",[budgetperiod]="+bgtCycle+",[fax]="+ glExp + ",[email]="+ glRev + ",[phone1]="+ glSuper + ",[CloseDate]="+ closeDate + ",[Postcode]="+ programJob + ",[UserYesNo3]="+ template + ",[User2]="+ contiguency + ",[User3]="+ level + ",[User4]="+ targetGroup + ",[User10]="+ conguencyPackage + ",[BudgetEnforcement]="+ budgetEnforcement + ",[BudgetRosterEnforcement]="+ budgetRosterEnforcement + ",[UserYesNo1]="+ agedCarePackage + ",[UserYesNo2]="+ individuallyFunded + ",[User8]="+ user8 + ",[User9]="+ user9 + ",[User11]="+ User11 + ",[User12]="+ User12 + ",[P_Def_Alert_Type]="+ P_Def_Alert_Type + ",[P_Def_Alert_Period]="+ P_Def_Alert_Period + ",[P_Def_Alert_Allowed]="+ allowed + ",[P_Def_Alert_Yellow]="+ yellow + ",[P_Def_Alert_Orange]="+ green + ",[P_Def_Alert_Red]="+ red + ",[P_Def_Expire_Amount]="+ expire_amount + ",[P_Def_Expire_CostType]="+ expire_costType + ",[P_Def_Expire_Unit]="+ expire_unit + ",[P_Def_Expire_Period]="+ expire_period + ",[P_Def_Expire_Length]="+ expire_length + ",[P_Def_Fee_BasicCare]="+ defaultbasiccarefee + ",[P_Def_Contingency_PercAmt]="+ perc_amt + ",[P_Def_Admin_AdminType]="+ adminType + ",[P_Def_Admin_CMType]="+ admincmType + ",[P_Def_Admin_CM_PercAmt]="+admin_cm_parc_amt+",[p_Def_Admin_Admin_PercAmt]="+admin_parc_amt+",[P_Def_StdDisclaimer]="+standard_quote+",[P_Def_Admin_AdminFrequency]="+adminFrequency+",[P_Def_Admin_CMFrequency]="+cmFrequency+",[P_Def_Admin_AdminDay]="+adminDay+",[P_Def_Admin_CMDay]="+cmday+",[P_Def_Expire_Using]="+expire_using+",[P_Def_Contingency_Max]="+max_contiguency+",[DefaultCHGTravelWithinActivity]="+wcactivity+",[DefaultCHGTravelWithinPayType]="+wcpay+",[DefaultNCTravelWithinProgram]="+wnprogram+",[DefaultNCTravelWithinActivity]="+wnactivity+",[DefaultNCTravelWithinPayType]="+wnpay+",[DefaultCHGTravelBetweenActivity]="+cactivity+",[DefaultCHGTravelBetweenPayType]="+cpay+",[DefaultNCTravelBetweenProgram]="+nprogram+",[DefaultNCTravelBetweenActivity]="+nactivity+",[DefaultNCTravelBetweenPayType]="+npay+",[DefaultCHGTRAVELBetweenProgram]="+cprogram+",[DefaultCHGTRAVELWithInProgram]="+wcprogram+",[P_Def_IncludeClientFeesInCont]="+includeClientFeesCont+",[P_Def_IncludeIncomeTestedFeeInAdmin]="+includetested+",[P_Def_IncludeBasicCareFeeInAdmin]="+IncludeCare+",[P_Def_IncludeTopUpFeeInAdmin]="+IncludeTopUp+",[CDCStatementText1]='"+text1+"',[DefaultDailyFee]="+default_daily_fees+",[NoNoticeLeadTime]="+noNoticeLeadTime+",[ShortNoticeLeadTime]="+shortNoticeLeadTime+",[NoNoticeLeaveActivity]="+noNoticeLeaveActivity+",[ShortNoticeLeaveActivity]="+shortNoticeLeaveActivity+",[DefaultNoNoticeCancel]="+defaultNoNoticeCancel+",[DefaultNoNoticeBillProgram]="+defaultNoNoticeBillProgram+",[DefaultNoNoticePayProgram]="+defaultNoNoticePayProgram+",[DefaultNoNoticePayType]="+defaultNoNoticePayType+",[DefaultShortNoticeCancel]="+defaultShortNoticeCancel+",[DefaultShortNoticeBillProgram]="+defaultShortNoticeBillProgram+",[DefaultShortNoticePayProgram]="+defaultShortNoticePayProgram+",[DefaultShortNoticePayType]="+defaultShortNoticePayType+",[DefaultWithNoticeCancel]="+defaultWithNoticeCancel+",[NoNoticeCancelRate]="+noNoticeCancelRate+",[ShortNoticeCancelRate]="+shortNoticeCancelRate+",[DefaultWithNoticeProgram]="+defaultWithNoticeProgram+" WHERE [RecordNumber] ='"+recordNumber+"'";
        console.log(sqlUpdate);
        this.menuS.InsertDomain(sqlUpdate).pipe(takeUntil(this.unsubscribe)).subscribe(data=>{
          if (data){
            this.globalS.sToast('Success', 'Saved successful');
            this.loadData();
            this.postLoading = false;   
            this.loading = false;       
            this.handleCancel();
            this.resetModal();
          }
          else{
            this.globalS.sToast('Success', 'Saved successful');
            this.loadData();
            this.loading = false;   
            this.postLoading = false;          
            this.handleCancel();
            this.resetModal();
          }
        });
        
      }
    }
    trueString(data: any): string{
      return data ? '1': '0';
    }
    
    isChecked(data: string): boolean{
      return '1' == data ? true : false;
    }
    packgChange(e){
    }
    recurrentChange(e){
      if(e.target.checked){
        this.visibleRecurrent = true;
      }else{
        this.visibleRecurrent = false;
      }
    }
    loadData(){
      this.loading = true;
      this.menuS.getlistProgramPackages(this.check).subscribe(data => {
        this.tableData = data;
        this.loading = false;
        this.cd.detectChanges();
      });
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = " WHERE ( [group] = 'PROGRAMS' ) ";
        this.loadData();
      }else{
        this.whereString = " WHERE ( [group] = 'PROGRAMS' ) AND ( enddate IS NULL OR enddate >= getDate() ) ";
        this.loadData();
      }
    }
    populateDropdowns(): void {
      
      this.states = ['ALL','NSW','NT','QLD','SA','TAS','VIC','WA','ACT'];
      this.fundingTypes  = ['FUNDED','UNFUNDED'];
      this.period = ['ANNUAL','MONTH','QUARTER'];
      this.levels = ['Level 1','Level 2','Level 3','Level 4','STRC'];
      this.cycles = ['CYCLE 1','CYCLE 2','CYCLE 3','CYCLE 4','CYCLE 5','CYCLE 6','CYCLE 7','CYCLE 8','CYCLE 9','CYCLE 10'];
      this.budgetEnforcement = ['HARD','SOFT'];
      this.alerts   = ['HOURS', 'DOLLARS', 'SERVICES'];
      this.DefPeriod = ['DAY','WEEK','FORTNIGHT','4 WEEKS','MONTH','6 WEEKS','QUARTER','6 MONTHS','YEAR']
      this.expireUsing   = ['Activity AVG COST','CHARGE RATE','PAY UNIT RATE']
      this.unitsArray    = ['PER','TOTAL'];
      this.dailyArry     = ['DAILY'];
      this.quoutetemplates = ['CAREPLAN SAVED AS TEMPLATE.OD'];
      this.accoumulationTypes = ['CONSECUTIVE DAYS','CUMULATIVE DAYS'];
      this.adminTypesArray = ['**CDC – CARE MGMT','**CDC FEE-ADMIN','~CASE MGT~','~PACKAGE ADMIN~','AGREED TOP UP FEE','BASIC CARE FEE','CDC  EXIT FEE','COORDINATION OF SUPPORTS','INCOME TESTED FEE','NDIA FAINANCIAL INTERMEDIATOR','YLYC CASE MAN. 2.066'];
      this.budgetRoasterEnforcement = ['NONE','NOTIFY','NOTIFY AND UNALLOCATE'];
      this.listS.getcaredomain().subscribe(data => this.caredomain = data);
      this.listS.getliststaffteam().subscribe(data=>this.staffTeams= data);
      let todayDate  = this.globalS.curreentDate();
      let funding = "SELECT RecordNumber, Description from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND Domain =  'FUNDREGION' ORDER BY Description";
      this.listS.getlist(funding).subscribe(data => {
        this.fundingRegion = data;
      });
      
      let fundings = "SELECT RecordNumber, Description from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND Domain =  'FUNDINGBODIES' ORDER BY Description";
      this.listS.getlist(fundings).subscribe(data => {
        this.fundingSources = data;
      });
      
      let progcor = "SELECT Description from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND Domain = 'CASE MANAGERS' ORDER BY Description";
      this.listS.getlist(progcor).subscribe(data => {
        this.programCordinates = data;
      });
      
      let target = "SELECT RecordNumber, Description from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND Domain =  'CDCTARGETGROUPS' ORDER BY Description";
      
      this.listS.getlist(target).subscribe(data => {
        this.targetGroups = data;
      });
      
      let reci = "SELECT ACCOUNTNO as name FROM RECIPIENTS WHERE AdmissionDate IS NOT NULL AND DischargeDate IS NULL AND ACCOUNTNO > '!Z'";
      this.listS.getlist(reci).subscribe(data => {
        this.recepients = data;
        this.recepitnt_copy = this.recepients;
      });
      let leavetype = "SELECT Title FROM ItemTypes WHERE RosterGroup = 'RECPTABSENCE' AND EndDate IS NULL or EndDate > GETDate() ORDER BY Title";
      this.listS.getlist(leavetype).subscribe(data => {
        this.leaveTypes = data;
        this.leaveTypes_copy = this.leaveTypes;
      });
      
      
      let acti = "SELECT TITLE FROM ITEMTYPES WHERE ProcessClassification IN ('OUTPUT', 'EVENT', 'ITEM') AND ENDDATE IS NULL";
      this.listS.getlist(acti).subscribe(data => {
        this.activities = data;
      });
      let prog = "select distinct Name from HumanResourceTypes WHERE [GROUP]= 'PROGRAMS' AND ((EndDate IS NULL) OR (EndDate > GETDATE()))";
      this.listS.getlist(prog).subscribe(data => {
        this.programz = data;
      });
      
      let ptype ="SELECT [recnum] AS [RecordNumber], [title] AS [Code] FROM itemtypes WHERE processclassification = 'INPUT' AND ( enddate IS NULL OR enddate >= '04-05-2019' ) ORDER BY title";
      this.loading = true;
      this.listS.getlist(ptype).subscribe(data => {
        this.paytypes = data;
      });
      
      let sc = "select distinct Name from HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND User2 = 'Contingency'";
      this.listS.getlist(sc).subscribe(data => {
        this.contingency = data;
      });
      
      let pckg_type_profile = "SELECT DISTINCT [Title] FROM ItemTypes WHERE ProcessClassification IN ('OUTPUT', 'EVENT') AND (EndDate Is Null OR EndDate >= GETDATE()) ORDER BY [Title]"
      this.listS.getlist(pckg_type_profile).subscribe(data => {
        this.packedTypeProfile = data;
        this.packedTypeProfileCopy = data;
      });
      let careWorker = "SELECT DISTINCT [Accountno] FROM Staff WHERE CommencementDate is not null and terminationdate is null ORDER BY [AccountNo]";
      this.listS.getlist(careWorker).subscribe(data => {
        this.careWorkers = data;
        this.careWorkersCopy = data;
        this.careWorkersExcluded = data;
        this.careWorkersExcludedCopy = data;
      });
      
      let comp = "SELECT Description as name from DataDomains Where ISNULL(DataDomains.DeletedRecord, 0) = 0 AND Domain = 'STAFFATTRIBUTE' ORDER BY Description";
      this.listS.getlist(comp).subscribe(data => {
        this.competencyList = data;
        this.competencyListCopy = data;
      });
      
      let fee = "SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >= GETDATE()) AND MinorGroup = 'FEE'"
      this.listS.getlist(fee).subscribe(data => {
        this.adminTypesArray = data;
        
      });
      let activity = "select distinct Title from ItemTypes WHERE ProcessClassification = 'EVENT' AND ((EndDate IS NULL) OR (EndDate > GETDATE())) AND RosterGroup = 'RECPTABSENCE'";
      this.listS.getlist(activity).subscribe(data => {
        this.activityTypes = data;
      });
      
      let timesheet = "select distinct Title from ItemTypes WHERE ProcessClassification = 'OUTPUT' AND ((EndDate IS NULL) OR (EndDate > GETDATE())) AND RosterGroup = 'ADMINISTRATION'"
      this.listS.getlist(timesheet).subscribe(data => {
        this.activityTimeSheet = data;
      });
    }
    log(value: string[]): void {
      // console.log(value);
    }
    delete(data: any) {
      const group = this.inputForm;
      this.menuS.deleteProgarmPackageslist(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadData();
          return;
        }
      });
    }
    deletePackageLeaveType(data:any){
      const group = this.inputForm;
      this.loading = true;
      this.menuS.deletePackageLeaveTypelist(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadPackageLeaveTypes();
          return;
        }
      });
    }
    deleteApprovedStaff(data:any){
      const group = this.inputForm;
      // this.loading = true;
      this.menuS.deleteApprovedStaff(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadApprovedStaff();
          return;
        }
      });
    }
    deleteExcludedStaff(data:any){
      const group = this.inputForm;
      // this.loading = true;
      this.menuS.deleteExcludedStaff(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadExcludedStaff();
          return;
        }
      });
    }
    deleteCompetency(data:any){
      const group = this.inputForm;
      this.loading = true;
      this.menuS.deleteCompetency(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadCompetency();
          return;
        }
      });
    }
    
    deleteApprovedService(data:any){
      const group = this.inputForm;
      this.loading = true;
      this.menuS.deleteApprovedService(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Deleted!');
          this.loadApprovedServices();
          return;
        }
      });
    }
    buildForm() {
      this.inputForm = this.formBuilder.group({
        funding_source:'',
        name:'',
        agency_id:'',
        state:'',
        coordinator:'',
        glrev:'',
        glexp:'',
        glsuper:'',
        period:'',
        bamount:'',
        bhrs:'',
        care:'',
        gst:false,
        gst_Percent:'',
        funding_region:'',
        funding_type:'',
        close_date:'',
        end_date:'',
        radioValue:'',
        radioValue2:'',
        template:false,
        continguency:'',
        mods:'',
        day_center:'',
        act_center:'', 
        individual:false,
        aged:'',
        level:'',
        target_g:'',
        contigency:'',
        budget_enfor:'',
        roster_enfor:'',
        p_alert_type:'', //default 1
        p_alert_period:'',
        expire_amount:'',
        expire_costType:'',
        expire_unit:'',
        expire_period:'',
        expire_length:'',
        allowed:'',
        yellow:'',
        green:'',
        line1:'',
        line2:'',
        red:'',
        expire_using:'',
        adminType:'',
        admincmType:'',
        adminPercAmt:'',
        adminFrequency:'',
        cmFrequency:'',
        adminDay:'',
        admin_cm_parc_amt:'',
        cmaddminType:'',
        admin_parc_amt:'',
        cmday:'',
        cycle:'',
        defaultdailyfee:'',
        IncludeTopUp:false,
        IncludeCare:false,
        includetested:false,
        includeClientFeesCont:false,
        defaultbasiccarefee:'',
        max_contiguency:'',
        perc_amt:'',
        default_daily_fees:'',
        quoute_template:'',
        line_1:'',
        line_2:'',
        nprogram:'',
        nactivity:'',
        npay:'',
        cprogram:'',
        cactivity:'',
        cpay:'',
        wnprogram:'',
        wnactivity:'',
        wnpay:'',
        wcprogram:'',
        wcactivity:'',
        wcpay:'',
        defaultNoNoticeBillProgram:'',
        defaultNoNoticeCancel:'',
        defaultNoNoticePayProgram:'',
        defaultNoNoticePayType:'',
        defaultShortNoticeBillProgram:'',
        defaultShortNoticeCancel:'',
        defaultShortNoticePayProgram:'',
        defaultShortNoticePayType:'',
        defaultWithNoticeCancel:'',
        defaultWithNoticeProgram:'',
        noNoticeCancelRate:0,
        noNoticeLeadTime:'',
        noNoticeLeaveActivity:'',
        shortNoticeCancelRate:'',
        shortNoticeLeadTime:'',
        shortNoticeLeaveActivity:'',
        no_notice:'',
        recurant:false,
        packg_balance:false,
        type: '',
        vehicledef:false,
        outletid:'',
        cstdaoutlet:'',
        dsci:'',
        branch:'',
        places:'',
        standard_quote:'',
        cat:'',
        category:'',
        unaprstaff:'',
        aprstaff:'',
        competences:'',
        agencysector:'',
        servicetype:'',
        fundingjunc:'',
        fundingtype:'',
        sheetalert:'',
        description: '',
        asset_no:'',
        serial_no:'',
        purchase_am:'',
        purchase_date:'',
        last_service:'',
        lockloct:'',
        lockcode:'',
        disposal:'',
        notes:'',
        glRevene:'',
        glCost:'',
        centerName:'',
        addrLine1:'',
        addrLine2:'',
        Phone:'',
        recordNumber:null,
        leaveActivityCode:'', //package leave types modal edit
        claimReducedTo:'',
        accumulatedBy:'',
        maxDays:'',
        LeaverecordNumber:null,
        competency:'',//competency modal edit
        mandatory:false,
        personId:null,
        competenctNumber:null,
      });
      
      // this.inputForm.get('packg_balance').valueChanges.subscribe(data => {
      //   if(!data){
      //       this.inputForm.controls['recurant'].enable()
      //   } else {
      //       this.inputForm.controls['packg_balance'].enable()
      //   }
      // })
    }
    handleOkTop() {
      this.generatePdf();
      this.tryDoctype = ""
      this.pdfTitle = ""
    }
    handleCancelTop(): void {
      this.drawerVisible = false;
      this.pdfTitle = ""
    }
    generatePdf(){
      this.drawerVisible = true;
      
      this.loading = true;
      
      var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY [name]) AS Field1,[name] AS [Field2], [type] AS [Field3], [address1] AS [Field4],[gstrate] AS [Field5], [budgetamount] AS [Field6], [budget_1] AS [Budget Hrs], [budgetperiod] AS [Field7], [fax] AS [Field8], [email] AS [Field9], [phone1] AS [Field10] FROM humanresourcetypes WHERE ( [group] = 'PROGRAMS' ) AND ( enddate IS NULL OR enddate >= '04-05-2019' )";
      
      const headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }
      
      const requestOptions = {
        headers: new HttpHeaders(headerDict)
      };
      
      const data = {
        "template": { "_id": "0RYYxAkMCftBE9jc" },
        "options": {
          "reports": { "save": false },
          "txtTitle": "Program-packages",
          "sql": fQuery,
          "userid":this.tocken.user,
          "head1" : "Sr#",
          "head2" : "Title",
          "head3" : "Funding Source",
          "head4" : "AgencyID",
          "head5" : "Rate",
          "head6" : "Budget$",
          "head7" : "Bgt Cycle",
          "head8" : "GL Exp A/C",
          "head9" : "GL Rev A/C",
          "head10" : "GL Super A/C",
        }
      }
      this.printS.printControl(data).subscribe((blob: any) => { 
        let _blob: Blob = blob;
        let fileURL = URL.createObjectURL(_blob);
        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
        this.loading = false;
        this.pdfTitle = "Program packages.pdf";
        }, err => {

        this.loading = false;
        this.ModalS.error({
          nzTitle: 'TRACCS',
          nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
          nzOnOk: () => {
            this.drawerVisible = false;
          },
        });
      });
      this.loading = true;
      this.tryDoctype = "";
      this.pdfTitle = "";
    }
    clone(){
      if(this.operation == 1){
        this.menuS.postProgramPackagesClone(this.selectedClonedRecord.recordNumber,this.activityCode).subscribe(
          response => {
            this.loading = false;
            this.globalS.sToast('Success', 'cloned successfully!');
            this.loadData();
            this.clonemodalOpen = false;
            this.activityCode   = '';
            
          },
          error => {
            this.loading = false;
            
            this.cd.detectChanges();
            this.globalS.eToast('ERROR', 'SomeThing Went Wring Try Again!');
          }
        );
      }
    }
    clonehandleCancel(){
      this.clonemodalOpen = false;
    }
    showActivityMOdal(item: any,operation:number){
      this.clonemodalOpen = true;
      this.operation = operation;
      this.selectedClonedRecord = item;
    }
    
    
    // fileName: string = 'TraccsProgramPackagesExport.csv'; // Default file name
    
    // export(): void {
    //   if (this.fileInput) {
    //     this.fileInput.nativeElement.setAttribute('nwsaveas', 'TraccsProgramPackagesExport.csv');
    //     this.fileInput.nativeElement.click();
    //   }
    // }
    
    // onFileSelected(event: Event): void {
    //   const input = event.target as HTMLInputElement;
    //   if (input.files && input.files.length > 0) {
    //     const file = input.files[0];
    //     const csvContent = this.generateCSVContent();
    //     const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
    
    //     let fullFileName = file.name.trim(); // Use the selected file's name
    
    //     // Save the file with the selected filename
    //     saveAs(blob, fullFileName);
    //   }
    // }
    
    // generateCSVContent(): string {
    //   const data = [
    //     ['Name', 'Age', 'Country'],
    //     ['John Doe', '30', 'USA'],
    //     ['Jane Smith', '25', 'Canada'],
    //     ['Sam Brown', '22', 'UK']
    //   ];
    //   return data.map(row => row.join(',')).join('\n');
    // }
    
    fileName: string = 'TraccsProgramPackagesExport.csv'; // Default file name
    
    export(): void {
      const csvContent = this.generateCSVContent();
      const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
      
      // Create a temporary anchor element
      const a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = this.fileName;
      
      // Append anchor to body to trigger download
      document.body.appendChild(a);
      
      // Programmatically trigger a click on the anchor element
      a.click();
      
      // Clean up: remove anchor and revoke object URL
      document.body.removeChild(a);
      URL.revokeObjectURL(a.href);
    }
    
    generateCSVContent(): string {
      
      
      const data = this.convertResponseToOutput(this.tableData);
      return data.map(row => row.join(',')).join('\n');
      
    }
    convertResponseToOutput(response: any[]): any[] {
      
      
      
      const headers = ['Title', 'Type', 'Agency ID', 'GST', 'Rate', 'Budget $', 'Budget Hrs', 'Budget Cycle', 'GL EXP A/C', 'GL REV A/C', 'GL Super A/C'];
      
      // Map each record to an array of values in the specified order
      const dataRows = response.map(record => [
        record.title,
        record.type,
        record.agencyID,
        record.gst,
        record.rate,
        record.budgetdollar,
        record.budgetHrs,
        record.bgtCycle,
        record.glExp,
        record.gLRev,
        record.gLSuper,
      ]);
      
      // Return an array that starts with headers followed by data rows
      return [headers, ...dataRows];
      
    }
    
    
    selectCode(){
      
      var endDate = new Date('2024-07-02');
      var type ='';
      
      if (this.selectedRows.length > 0) {
        type = this.selectedRows[0].type;
      }
      
      this.listS.getProgramMergeCodes(endDate, type).subscribe(
        (data) => {
          this.mergeactivitycodesList = data;
        },
        (error) => {
          console.error('There was an error!', error);
        }
      );
      
      this.mergeHistorymodalOpen = true;
    }
    
    mergeHistoryModalClose(){
      this.mergeHistorymodalOpen = false;
      this.selectedRows=[];
      this.loadData();
    }
    
    onTextChangeEvent(event:any){
      let value = this.txtSearch.toUpperCase();
      this.mergeactivitycodesList=this.originalList.filter(element=>element.title.includes(value));
    }
    
    selectedItemGroup(data:any, i:number){
      this.selectedRowIndex=i;
      this.activeRowData=data;
    }




    
    RecordClicked(event: any, value: any, i: number) {
      
      this.selectedRowIndex =i;
      this.selectedCode=value;


      if (event.ctrlKey || event.shiftKey) {
        this.selectedmergecodes.push(value)
      } else {
        this.selectedmergecodes = [];
        if (this.selectedmergecodes.length <= 0) {
          this.selectedmergecodes.push(value)
        }
      }
    }
    // onItemSelected(sel: any, i:number): void {
    //   this.selectedCode=sel;    
    //   this.HighlightRow2=i;
    // }



    onItemDbClick(sel:any , i:number) : void {
      this.HighlightRow2=i;
      this.selectedCode=sel;
      this.mergehistories();
    }
    
    mergehistories() {
      


      const audit = {
        Operator: this.tocken.user,
        ActionDate: new Date().toISOString(),
        AuditDescription: '',
        ActionOn: 'PROGRAMS',
        WhoWhatCode: '',
        TraccsUser: this.tocken.user,
      };
      
      let newServiceType: string;  // Declare newServiceType outside the if block
      newServiceType = this.selectedCode.name;
      const existingServiceTypes = Array.from(new Set(this.selectedRows.map(row => row.title)));
      
      this.ModalS.confirm({
        nzTitle: 'Do You Want to Merge?',
        nzContent: '',
        nzOkText: 'Yes',
        nzOnOk: () => {
          this.menuS.programMergeHistories(newServiceType,{
            audit:audit,
            existingServiceTypes:existingServiceTypes
          }).subscribe(
            response => {
              // console.log('Merge successful:', response);
              this.mergeHistorymodalOpen = false;
              this.globalS.sToast('Sucess', 'Merge done successful');
              this.loadData();
            },
            error => {
             // console.error('Merge failed:', error);
              this.globalS.eToast('Error', 'Merge failed');
            }
          );
        },
        nzCancelText: 'No',
        nzOnCancel: () => {
          this.mergeHistorymodalOpen = false;
          this.loadData();  // Assuming this method reloads data after canceling
        }
      });


    }
    isSelected(data: any): boolean {
      return this.selectedRows.includes(data);
    }
    
    onRowClick(event: MouseEvent, data: any): void {
      if (event.ctrlKey) {
        if (this.isSelected(data)) {
          this.selectedRows = this.selectedRows.filter(row => row !== data);
        } else {
          this.selectedRows.push(data);
        }
      } else {
        this.selectedRows = [data];
      }
      this.selectedCode = data;
      this.mergestatus = this.selectedRows.length > 1 ? true : false;
    }
    goBack(): void {
      const previousUrl = this.navigationService.getPreviousUrl();
      const previousTabIndex = this.navigationService.getPreviousTabIndex();
    
      if (previousUrl) {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      } else {
        this.router.navigate(['/admin/configuration'], {
          queryParams: { tab: previousTabIndex } // Pass tab index in query params
        });
      }
      
    }
    @HostListener('document:keydown', ['$event'])
      handleKeyboardEvent(event: KeyboardEvent) {
        // Handle character input
        if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
            this.searchTerm += event.key.toLowerCase();
            this.scrollToRow();
        } 
        // Handle backspace
        else if (event.key === 'Backspace') {
            // Only reset if there's something to remove
            this.searchTerm = '';
            if (this.searchTerm.length > 0) {
                this.searchTerm = this.searchTerm.slice(0, -1);
                this.scrollToRow();
            }
        }
        // Handle other keys (optional)
        else if (event.key === 'Escape') {
            // Reset search term on escape key
            this.searchTerm = '';
            this.scrollToRow();
        }
      }
  
      scrollToRow() {
          // Find the index of the first matching item
          const matchIndex = this.tableData.findIndex(item => 
              item.title.toLowerCase().startsWith(this.searchTerm)
          );
          // Scroll to the matching row if it exists
          if (matchIndex !== -1) {
              const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
              if (tableRow) {
                  tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                  this.highlightRow(tableRow);
              }
          } else {
          }
      }
  
      highlightRow(row: HTMLElement) {
          row.classList.add('highlight');
          setTimeout(() => row.classList.remove('highlight'), 2000);
      }   
    
    
  }