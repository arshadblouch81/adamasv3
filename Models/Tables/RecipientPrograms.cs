using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;


namespace Adamas.Models.Tables{
    
    public class RecipientPrograms {
        [Key]
        public int? RecordNumber { get; set; }
        public string PersonID { get; set; }
        public string Program { get; set; }
        public string Quantity { get; set; }
        public string ItemUnit { get; set; }
        public string PerUnit { get; set; }
        public string TimeUnit { get; set; }
        public string Period { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public Double? Used { get; set; }
        public Double? Remaining { get; set; }
        public Double? TotalAllocation { get; set; }
        public bool DeletedRecord { get; set; }
        public string Billing { get; set; }
        public Double? CappedAt { get; set; }
        public bool? Capped { get; set; }
        public string Percentage { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? AlertStartDate {get;set;}
        public bool? AutoRenew { get; set; }
        public string LastReferrer { get; set; }
        public string ProgramStatus { get; set; }
        public bool? RolloverRemainder { get; set; }
        public string ExpireUsing { get; set; }
        public bool? ReminderProcessed { get; set; }
        public bool? RenewalProcessed { get; set; }
        public DateTime? ReminderDate { get; set; }
        public int? ReminderLeadTime { get; set; }
        public bool? DeactivateOnExpiry { get; set; }
        public bool? AutoReceipt { get; set; }
        public string PackageLevel { get; set; }
        public string PackageTermType { get; set; }
        public string PackageType { get; set; }
        public string Priority { get; set; }
        public string PackageSupplements { get; set; }
        public string HardShipSupplement { get; set; }
        public string ProgramSummary { get; set; }
        public string Contingency { get; set; }
        public string IncomeTestedFee { get; set; }
        public string ClientCont { get; set; }
        public string AgreedTopUp { get; set; }
        public string DailyIncomeTestedFee { get; set; }
        public string DailyBasicCareFee { get; set; }
        public string DailyAgreedTopUp { get; set; }
        public string CommonwealthCont { get; set; }
        public string Contingency_Start { get; set; }
        public string Contingency_BuildCycle { get; set; }
        public bool? AutoBill { get; set; }
        public string AP_BasedOn { get; set; }
        public string AP_CostType { get; set; }
        public string AP_PerUnit { get; set; }
        public string AP_Period { get; set; }
        public string AP_YellowAmtPerc { get; set; }
        public string AP_OrangeAmtPerc { get; set; }
        public string AP_RedAmtPerc { get; set; }
        public string AP_YellowQty { get; set; }
        public string AP_OrangeQty { get; set; }
        public string AP_RedQty { get; set; }
        public string AdminAmount_Perc { get; set; }   
      
        public bool? HCPOptIn { get; set; }
        }

}