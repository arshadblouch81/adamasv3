using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using ExcelDataReader;

using Adamas.Models.Modules;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    public class ExcelReaderController : Controller
    {
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        [HttpGet("[action]")]
        //[Authorize(Roles="PORTAL CLIENT")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            // System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            // using (var stream = System.IO.File.Open(@"C:\Users\mark\Desktop\Programming\Adamas\Timesheet.xlsx", FileMode.Open, FileAccess.Read))
            // {
            //     using (var reader = ExcelReaderFactory.CreateReader(stream))
            //     {
            //         do
            //         {
            //             var counter = 0;
            //             while (reader.Read()) //Each ROW
            //             {
                            
            //                 for (int column = 0; column < reader.FieldCount; column++)
            //                 {
            //                     //Console.WriteLine(reader.GetString(column));//Will blow up if the value is decimal etc. 
            //                     Console.WriteLine(reader.GetValue(column));//Get Value returns object
            //                 }
            //                 // counter++;
            //             }
            //         } while (reader.NextResult()); //Move to NEXT SHEET

            //     }
            // }

            var xero = ProcessFile(@"C:\Users\mark\Desktop\Programming\Adamas\Timesheet.csv");

            var distinctUsers = xero.Select(x => x.Staff).Distinct().ToList();

            distinctUsers.ForEach(data => {
                var lists = xero.Where(x => x.Staff == data).Select(x => x);


            });

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }
        private static List<XeroUser> ProcessFile(string path){
            return System.IO.File.ReadAllLines(path)
                    .Skip(1)
                    .Where(line => line.Length > 1)
                    .Select(XeroUser.ParseFromCSV)
                    .ToList();
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
