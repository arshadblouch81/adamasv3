﻿using Microsoft.AspNetCore.Http;
using System;
using System.Web;
using Microsoft.AspNetCore.Mvc;

namespace Adamas.Utilities
{
    public class SessionUtilities 
    {
        public static void SetSession(string name, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
            {
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            }
            else
            {
                option.Expires = DateTime.Now.AddMinutes(60);
            }

            
        }
    }
}
