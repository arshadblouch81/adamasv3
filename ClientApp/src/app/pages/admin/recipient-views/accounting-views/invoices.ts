import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes,PrintService, ClientService, AccountReceiptDto, AccountAdjustmentDto } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { milliseconds } from 'date-fns';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: '',
    templateUrl: './invoices.html',
    styles: [`
        .controls button{
            margin-right: 5px;
        }
        textarea{
            height:5rem;
        }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class Invoices implements OnInit, OnDestroy {

    private unsubscribe: Subject<void> = new Subject();
    
    user: any;
    loading: boolean = false;
    modalOpen: boolean = false;
    addOREdit: number;

    inputForm: FormGroup;
    recipientGroup: FormGroup;
    adjustmentGroup: FormGroup;

    tableData: Array<any> = [];
    alist: Array<any> = [];

    receiptModal: boolean = false;
    adjustmentModal: boolean = false;

    branches: Array<any> = [];
    programs: Array<any> = [];
    sources: Array<any> = ['N/A','PERSONAL CONTRIBUTION','3RD PARTY CONTRIBUTION','GOVERNMENT CONTIRBUTION'];
    types: Array<any> = ['RECEIPT','ADJUSTMENT']
    activeRowData:any;
    selectedRowIndex:number;
    showFilters:boolean=false;
    filterClicked:boolean=false;
    filters: any;
    LIMIT_TABS : Array<any> = ['CARE DOMAIN','CREATOR','DISCIPLINE','PROGRAM'];

    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private printS: PrintService,
        private ModalS: NzModalService,
        private sanitizer:DomSanitizer,
    ) {
        cd.detach();

        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'accounting')) {
         
                if('code' in data){
                    this.user = data;
                    this.search(this.user);
                }
            }
        });
    }

    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
        this.user = this.sharedS.getPicked();
        console.log(this.user);

        this.search(this.user);
        this.buildForm();
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    trackByFn(index, item) {
        return item.id;
    }

    getPermisson(index:number){
        var permissoons = this.globalS.getRecipientRecordView();
        return permissoons.charAt(index-1);
      }
      getStaffPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();
        return permissoons.charAt(index-1);
      }
      confirm(view: number, data: any): void {
      
        if (view == 1) {
          data = this.activeRowData;
          this.delete(this.selectedRowIndex);
        }

    }
    
    search(user: any = this.user) {
        this.cd.reattach();
        this.loading = true;

        this.listS.getaccountinginvoices(user.code).subscribe(plans => {
            this.tableData = plans;
            this.loading = false;
            this.cd.markForCheck();
        });
    }

    buildForm() {
        this.recipientGroup = this.formBuilder.group({
            branch: null,
            source: null,
            programs: null,
            type: null,
            date: null,
            amount: null,
            notes: null
        });
    }

    showEditModal(index: number) {

    }

    delete(index: number) {

    }

    handleCancel() {
        this.receiptModal = false;
        this.adjustmentModal = false;
    }

    handleOk(){
        
    }

    showAddModal() {
        this.addOREdit = 1;
        this.modalOpen = true;
    }

    Refresh(){
        this.search(this.user);
    }
    print(){

    }
    generatePdf(){
        var Title;            
      //console.log(this.dataSet)
        
          Title = "Invoices";
          this.loading = true;
          this.drawerVisible = true;
        
                   
            //console.log(this.rptData)      
            
                const data = {
                    "template": { "_id": "6BoMc2ovxVVPExC6" },
                    "options": {
                        "reports": { "save": false },                        
                        "sql": this.tableData,                        
                        "userid": this.tocken.user,
                        "txtTitle":  Title,                      
                    }
                }              
                this.loading = true;           
                        
                this.drawerVisible = true;
                this.printS.printControl(data).subscribe((blob: any) => {
                            this.pdfTitle = Title+".pdf"
                            this.drawerVisible = true;                   
                            let _blob: Blob = blob;
                            let fileURL = URL.createObjectURL(_blob);
                            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                            this.loading = false;  
                            this.cd.detectChanges();                       
                        }, err => {
                            console.log(err);
                            this.loading = false;
                            this.modalService.error({
                                nzTitle: 'TRACCS',
                                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                                nzOnOk: () => {
                                    this.drawerVisible = false;
                                },
                            });
                });                            
                return;        
               
                 
    }
      handleCancelTop(): void {
      this.drawerVisible = false;
      this.loading = false;
      this.pdfTitle = ""
      this.tryDoctype = ""
      }
    close(){
        this.router.navigate(['/admin/recipient/personal']);
    }
 filterChange(data: any){
       
        this.filters=data;
        this.search(this.user);
    }
    Filters(){
        this.showFilters=!this.showFilters;
        this.filterClicked=true;
       // this.search();
      }

}