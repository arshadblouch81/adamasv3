import { ChangeDetectorRef, Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService } from '@services/index';
import { takeUntil, timeout } from 'rxjs/operators';
import { setDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs';
import * as moment from 'moment';
import { DatePipe } from '@angular/common'
import { formatDate } from '@angular/common';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import addYears from 'date-fns/addYears';
import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';

@Component({
  selector: 'app-close-pay-roster-period',
  templateUrl: './closePayRoster.component.html',
  styles: [`
  .orange-text{
    font-size: 14px;
    text-align: left;
    margin-left: 2%;
    color:#f18805;
    margin-top: 10%;
  }
.tab {
  display: block;
  border: 2px solid #85B9D5;
  border-radius: 5px;
  background-color: white;
  height: 74vh;
  width: 104vh;
  padding: 5px;
  margin-top: -28px;
  overflow-y: auto;
}
.btn {
  border: none;
  cursor: pointer;
  outline: none;
  background-color: #85B9D5;
  color: white;
  padding: 5px 7px;
  border-radius: 7px !important;
  text-align: center !important;
  width: 100px !important;
  font-size: 14px;
  font-family: Segoe UI;
  font-weight: bold;
}
.orange-text{
  font-size: 14px;
  text-align: left;
  margin-left: 2%;
  color:#f18805;
  margin-top: 10%;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
  line-height: 24px;
  background: #fff;
  height: 25px;
  border-radius: 15px 15px 0 0;
  margin: 0 0px 0 0;
  text-align: center;
  border-color: #85B9D5;
}

nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab.ant-tabs-tab-active {
  background: #85B9D5;
  color: #fff;
}
nz-container>>>nz-tabset>>>div div.ant-tabs-nav-container div.ant-tabs-nav-wrap div.ant-tabs-nav-scroll div.ant-tabs-nav div div.ant-tabs-tab {
  min-width: auto;
}
nz-tabset>>>.ant-tabs-bar {
  border-bottom: 0px;
}

.mrg-btm{
    margin-bottom:0.3rem;
  }
.uns-button{
      font-size: 10pt; 
      text-align: right;
      background-color: #0E86D4;
      border-color: #0E86D4;
      padding: 3px; 
      height: 1.75pc; 
      width: 5.5pc;
} 
.sav-button{
      font-size: 14pt;
      margin-top: 10px; 
      height: 2.4pc; 
      float: right;
      text-align: center;
      background-color: #85B9D5;
      padding: 3px; 
      width: 6pc;
}
  `]

  // .ant-checkbox-checked .ant-checkbox-inner {
  //   background-color: #85B9D5 !important;
  //   border-color: #000000 !important;
  // }

})
export class ClosePayRosterComponent implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;

  fundingList: Array<any>;
  programList: Array<any>;
  tableData: Array<any>;
  loading: boolean = false;
  modalOpen: boolean = false;
  current: number = 0;
  inputForm: FormGroup;
  chkId: string;
  confirmModal?: NzModalRef;
  modalVariables: any;
  dateFormat: string = 'dd/MM/yyyy';
  inputVariables: any;
  postLoading: boolean = false;
  isUpdate: boolean = false;
  title: string = "Close Rostering";
   
  token: any;
  tocken: any;
  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean = false;
  check: boolean = false;
  temp_title: any;
  settingForm: FormGroup;
  userRole: string = "userrole";
  whereString: string = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  dtpEndDate: any;
  lastMonthEndDate: any;
  id: string;
  btnid: any;
  btnid1: any;
  private unsubscribe: Subject<void> = new Subject();
  allFundingChecked: boolean;
  allProgramsChecked: boolean;
  selectedPrograms: any;
  selectedFunding: any;
  date: moment.MomentInput;
  updatedRecords: any;
  chkBlockFunded: boolean = true;
  chkIndividualPackages: boolean = true;
  chkNonCDCPackages: boolean = true;
  chkCDCPackages: boolean = true;
  // vBlockFunded: number;
  // vIndividualPackages: number;
  // vNonCDCPackages: number;
  // vCDCPackages: number;
  // confirmModal?: NzModalRef;
  vBlockFunded: string = '1';
  vIndividualPackages: string = '1';
  vNonCDCPackages: string = '1';
  vCDCPackages: string = '1';
  xValue: string;
  xCondition: string;
  lockNonCDCPackages: any = true;
  lockCDCPackages: any = true;
  lockFunding: any = true;
  lockProgram: any = true;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private modal: NzModalService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    public datepipe: DatePipe,
  ) { }

  ngOnInit(): void {
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.buildForm();
    // this.loadPrograms();
    this.loading = false;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.postLoading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/pays']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      dtpEndDate: null,
      chkBlockFunded: true,
      chkIndividualPackages: true,
      chkNonCDCPackages: true,
      chkCDCPackages: true,
    });
    this.EndofMOnth();
    this.loadFunding();
    this.getProgramPackageDetail();
  }
  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  EndofMOnth() {
    const lastMonthEndDate = new Date();
    lastMonthEndDate.setMonth(lastMonthEndDate.getMonth() - 1);
    this.dtpEndDate = endOfMonth(lastMonthEndDate);
    this.inputForm.patchValue({
      dtpEndDate: this.dtpEndDate,
    });
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedFunding = event;
    if (index == 2)
      this.selectedPrograms = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      // this.fundingList.forEach(x => {
      //   x.checked = true;
      //   this.allFundingChecked = x.description;
      //   this.allFundingChecked = true;
      // });
      if (this.allFundingChecked == false) {
        this.lockFunding = false;
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = true;
      }
    }
    // if (index == 1) {
    //   this.lockFunding = true;
    //   this.fundingList.forEach(x => {
    //     x.checked = true;
    //     this.allFundingChecked = x.description;
    //     this.allFundingChecked = true;
    //   });
    // }
    if (index == 2) {
      // this.programList.forEach(x => {
      //   x.checked = true;
      //   this.allProgramsChecked = x.description;
      //   this.allProgramsChecked = true;
      // });
      if (this.allProgramsChecked == false) {
        this.lockProgram = false;
      }
      else {
        this.programList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockProgram = true;
      }
    }
    // if (index == 2) {
    //   this.lockProgram = true;
    //   this.programList.forEach(x => {
    //     x.checked = true;
    //     this.allProgramsChecked = x.description;
    //     this.allProgramsChecked = true;
    //   });
    // }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockFunding = false;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
    if (index == 2) {
      this.lockProgram = false;
      this.programList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
  }

  checkedStatus(index: any): void {
    if (index == 11) {
      if (this.chkBlockFunded == false) {
        this.vBlockFunded = '0';
      }
      else {
        this.vBlockFunded = '1';
      }
    }
    if (index == 12) {
      if (this.chkIndividualPackages == false) {
        this.lockNonCDCPackages = false;
        this.lockCDCPackages = false;
        this.vIndividualPackages = '0';
      }
      else {
        this.vIndividualPackages = '1';
        this.chkNonCDCPackages = true;
        this.chkCDCPackages = true;
        this.vNonCDCPackages = '1';
        this.vCDCPackages = '1';
        this.lockNonCDCPackages = true;
        this.lockCDCPackages = true;
      }
    }
    if (index == 13) {
      if (this.chkNonCDCPackages == false) {
        this.vNonCDCPackages = '0';
      }
      else {
        this.vNonCDCPackages = '1';
      }
    }
    if (index == 14) {
      if (this.chkCDCPackages == false) {
        this.vCDCPackages = '0';
      }
      else {
        this.vCDCPackages = '1';
      }
    }
    this.getProgramPackageDetail();
  }

  loadFunding() {
    this.loading = true;
    this.menuS.getlistFundingSource(this.check).subscribe(data => {
      this.fundingList = data;
      this.tableData = data;
      this.loading = false;
      this.allFundingChecked = true;
      this.checkAll(1);
    });
  }

  getProgramPackageDetail() {

    this.xValue = this.vBlockFunded + this.vIndividualPackages + this.vNonCDCPackages + this.vCDCPackages;

    // console.log(this.xValue)

    switch (this.xValue) {
      case ("0000" || "0001" || "0010" || "0011" || "0100"):
        this.confirmModal = this.modal.info({
          nzTitle: 'Alert',
          nzContent: 'You must have at least Block Funded or Individual Funding type of package/program selected for display.',
          nzOnOk: () => { }
        });
        break;
      case ("1000" || "1001" || "1011" || "1010" || "1100"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 0"
        break;
      case ("0101"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 1"
        break;
      case ("0110"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 0"
        break;
      case ("0111"):
        this.xCondition = " AND ISNULL(UserYesNo2, 0) = 1"
        break;
      case ("1110"):
        this.xCondition = " AND (ISNULL(UserYesNo2, 0) = 0) OR (ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 0)"
        break;
      case ("1101"):
        this.xCondition = " AND (ISNULL(UserYesNo2, 0) = 0) OR (ISNULL(UserYesNo2, 0) = 1 AND ISNULL(UserYesNo1, 0) = 1)"
        break;
      case ("1111"):
        this.xCondition = " AND (ISNULL(UserYesNo2, 0) = 0) OR (ISNULL(UserYesNo2, 0) = 1)"
        break;
      default:
        break;
    }

    let dataPass = {
      condition: this.xCondition
    }
    this.billingS.getProgramPackageslist(dataPass).subscribe(data => {
      this.programList = data;
      this.tableData = data;
      this.allProgramsChecked = true;
      this.checkAll(2);
      this.loading = false;
      this.postLoading = false;
    });
    // console.log(this.chkBlockFunded + " , " + this.chkIndividualPackages + " , " + this.chkNonCDCPackages + " , " + this.chkCDCPackages);
  }

  // loadPrograms() {
  //   this.loading = true;
  //   this.menuS.getlistProgramPackages(this.check).subscribe(data => {
  //     this.programList = data;
  //     this.tableData = data;
  //     this.loading = false;
  //     this.allProgramsChecked = true;
  //     this.checkAll(2);
  //   });
  // }

  // fetchAll(e) {
  //   if (e.target.checked) {
  //     this.whereString = "WHERE";
  //     this.loadPrograms();
  //   } else {
  //     this.whereString = "Where ISNULL(DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
  //     this.loadPrograms();
  //   }
  // }

  checkValidation() {
    this.selectedFunding = this.fundingList
      .filter(opt => opt.checked)
      .map(opt => opt.description).join(",")

    this.selectedPrograms = this.programList
      .filter(opt => opt.checked)
      .map(opt => opt.ppName).join(",")

    this.dtpEndDate = this.inputForm.get('dtpEndDate').value;
    this.dtpEndDate = formatDate(this.dtpEndDate, 'yyyy-MM-dd hh:mm', 'en_US');

    if (this.selectedFunding != '' && this.selectedPrograms != '' && this.dtpEndDate != '') {
      this.closeRosterPeriod();
    } else if (this.selectedFunding == '') {
      this.globalS.eToast('Error', 'Please select atleast one Funding to proceed')
    } else if (this.selectedPrograms == '') {
      this.globalS.eToast('Error', 'Please select atleast one Program to proceed')
    } else if (this.dtpEndDate == '') {
      this.globalS.eToast('Error', 'Please select valid Date to proceed')
    }
  }

  closeRosterPeriod() {
    this.postLoading = true;
    this.billingS.closeRosterPeriod({
      Closedate: this.dtpEndDate,
      Programs: this.selectedPrograms,
      Fundings: this.selectedFunding,
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'No Roster Entries are existing to Close Date.')
          } else {
            this.globalS.sToast('Success', 'Close Roster Date has been updated successfully on ' + this.updatedRecords + ' Records.')
          }
          this.postLoading = false;
          this.ngOnInit();
          return false;
        }
      });
  }
}


