import { ChangeDetectorRef, Component, OnInit,HostListener } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ListService } from '@services/list.service';
import { MenuService } from '@services/menu.service';
import { GlobalService } from '@services/global.service';
import { SwitchService } from '@services/switch.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import { PrintService } from '@services/print.service';
import { TimeSheetService } from '@services/timesheet.service';
import { Router } from '@angular/router';
import { NavigationService } from '@services/navigation.service';

@Component({
  selector: 'app-religion',
  templateUrl: './religion.component.html',
  styles: [`
  tr.highlight {
    background-color: #85B9D5; /* Change to your preferred highlight color */
  }
  `]
  })
  export class ReligionComponent implements OnInit {
    
    tableData: Array<any>;
    loading: boolean = false;
    modalOpen: boolean = false;
    current: number = 0;
    inputForm: FormGroup;
    modalVariables: any;
    dateFormat: string ='dd/MM/yyyy';
    inputVariables:any;
    postLoading: boolean = false;
    isUpdate: boolean = false;
    title:string = "Add New Religion";
    private unsubscribe: Subject<void> = new Subject();
    rpthttp = 'https://www.mark3nidad.com:5488/api/report'
    token:any;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;  
    check : boolean = false;
    userRole:string="userrole";
    whereString :string="Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
    temp_title: any;
    searchTerm: string = '';
    currentIndex: number | null = null;
    
    constructor(
      private globalS: GlobalService,
      private cd: ChangeDetectorRef,
      private listS:ListService,
      private menuS:MenuService,
      private switchS:SwitchService,
      private formBuilder: FormBuilder,
      private timeS:TimeSheetService,
      private printS:PrintService,
      private sanitizer: DomSanitizer,
      private navigationService: NavigationService,
      private router: Router,
      private ModalS: NzModalService
    ){}
    
    ngOnInit(): void {
      this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
      this.userRole = this.tocken.role;
      this.buildForm();
      this.loadData();
      this.loading = true;
      this.cd.detectChanges();
    }
    loadTitle()
    {
      return this.title
    }
    fetchAll(e){
      if(e.target.checked){
        this.whereString = "WHERE";
        this.loadData();
      }else{
        this.whereString = "Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND";
        this.loadData();
      }
    }
    activateDomain(data: any) {
      this.postLoading = true;     
      const group = this.inputForm;
      this.menuS.activeDomain(data.recordNumber)
      .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.globalS.sToast('Success', 'Data Activated!');
          this.loadData();
          return;
        }
      });
    }
    showAddModal() {
      this.title = "Add New Religion";
      this.resetModal();
      this.modalOpen = true;
    }
    
    resetModal() {
      this.current = 0;
      this.inputForm.reset();
      this.postLoading = false;
    }
    
    showEditModal(index: any) {
      this.isUpdate = true;
      this.modalOpen = true;
      this.title = "Edit Religion";
      this.current = 0;
      
      const { 
        name,
        end_date,
        recordNumber
      } = this.tableData[index];
      this.inputForm.patchValue({
        name: name,
        end_date: end_date,
        recordNumber:recordNumber
      });
      this.temp_title = name;
    }
    
    handleCancel() {
      this.modalOpen = false;
    }
    pre(): void {
      this.current -= 1;
    }
    
    next(): void {
      this.current += 1;
    }
    save() {
      this.postLoading = true;     
      const group = this.inputForm;
      if(!this.isUpdate){      
        let name        = group.get('name').value.trim().toUpperCase();
        let is_exist    = this.globalS.isNameExists(this.tableData,name);
        if(is_exist){
          this.globalS.sToast('Unsuccess', 'Title Already Exist');
          this.postLoading = false;
          return false;   
        }   
        this.switchS.addData(  
          this.modalVariables={
            title: 'Religion'
          }, 
          this.inputVariables = {
            display: group.get('name').value,
            end_date:!(this.globalS.isVarNull(group.get('end_date').value)) ? this.globalS.convertDbDate(group.get('end_date').value) : null,
            domain: 'RELIGION', 
          }
        ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          if (data) 
            this.globalS.sToast('Success', 'Saved successful');     
          else
          this.globalS.sToast('Unsuccess', 'Data not saved' + data);
          this.loadData();
          this.postLoading = false;   
          this.loading = false;       
          this.handleCancel();
          this.resetModal();
        });
      }else{
        this.postLoading = true;     
        const group = this.inputForm;
        let name        = group.get('name').value.trim().toUpperCase();
        if(this.temp_title != name){
          let is_exist    = this.globalS.isNameExists(this.tableData,name);
          if(is_exist){
            this.globalS.sToast('Unsuccess', 'Title Already Exist');
            this.postLoading = false;
            return false;   
          }
        }
        this.switchS.updateData(  
          this.modalVariables={
            title: 'Religion'
          }, 
          this.inputVariables = {
            display: group.get('name').value,
            primaryId:group.get('recordNumber').value,
            end_date:!(this.globalS.isVarNull(group.get('end_date').value)) ? this.globalS.convertDbDate(group.get('end_date').value) : null,
            domain: 'RELIGION',
          }
          
        ).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          if (data)
            {
            this.timeS.postaudithistory({
              Operator:this.tocken.user,
              actionDate:this.globalS.getCurrentDateTime(),
              auditDescription:'RELIGION Changed',
              actionOn:'RELIGION',
              whoWhatCode:group.get('recordNumber').value, //inserted
              TraccsUser:this.tocken.user,
            }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
              this.globalS.sToast('Success', 'Update successful');
            }
          );
        }else
        {
          this.globalS.sToast('Unsuccess', 'Data Not Update' + data);
        }
        this.loadData();
        this.postLoading = false;  
        this.loading = false;    
        this.isUpdate = false;    
        this.handleCancel();
        this.resetModal();
      });
    }
  }
  loadData(){
    this.loading = true;
    this.menuS.getDataDomainByType("RELIGION",this.check).subscribe(data => {
      this.tableData = data;
      this.cd.detectChanges();
      this.loading = false;
    });
  }
  delete(data: any) {
    this.postLoading = true;     
    const group = this.inputForm;
    this.menuS.deleteDomain(data.recordNumber)
    .pipe(takeUntil(this.unsubscribe)).subscribe(datas => {
      if (datas) {
        this.timeS.postaudithistory({
          Operator:this.tocken.user,
          actionDate:this.globalS.getCurrentDateTime(),
          auditDescription:'RELIGION Deleted',
          actionOn:'RELIGION',
          whoWhatCode:data.recordNumber, //inserted
          TraccsUser:this.tocken.user,
        }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
          this.globalS.sToast('Success', 'Deleted successful');
        }
      );
      this.loadData();
      return;
    }
  });
}
buildForm() {
  this.inputForm = this.formBuilder.group({
    name: '',
    end_date:'',
    recordNumber:null
  });
}
handleOkTop() {
  this.generatePdf();
  this.tryDoctype = ""
  this.pdfTitle = ""
}
handleCancelTop(): void {
  this.drawerVisible = false;
  this.pdfTitle = ""
}
generatePdf(){
  this.drawerVisible = true;
  
  this.loading = true;
  
  var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY Description) AS Field1,Description as Field2,DeletedRecord as is_deleted,CONVERT(varchar, [enddate],105) as Field3 from DataDomains "+this.whereString+" Domain='RELIGION'";
  const data = {
    "template": { "_id": "0RYYxAkMCftBE9jc" },
    "options": {
      "reports": { "save": false },
      "txtTitle": "Religion List",
      "sql": fQuery,
      "userid":this.tocken.user,
      "head1" : "Sr#",
      "head2" : "Name",
      "head3" : "End Date"
    }
  }
  
  this.printS.printControl(data).subscribe((blob: any) => {
    let _blob: Blob = blob;
    let pdfTitle = "Religions List";
    let fileURL  = URL.createObjectURL(_blob);
    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
    this.loading = false;
    this.pdfTitle = "Religions.pdf";
  }, err => {
    this.loading = false;
    this.ModalS.error({
      nzTitle: 'TRACCS',
      nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
      nzOnOk: () => {
        this.drawerVisible = false;
      },
    });
  });
  
  
  this.loading = true;
  this.tryDoctype = "";
  this.pdfTitle = "";
}
goBack(): void {
  const previousUrl = this.navigationService.getPreviousUrl();
  const previousTabIndex = this.navigationService.getPreviousTabIndex();
  
  if (previousUrl) {
    this.router.navigate(['/admin/configuration'], {
      queryParams: { tab: previousTabIndex } // Pass tab index in query params
    });
  } else {
    this.router.navigate(['/admin/configuration'], {
      queryParams: { tab: previousTabIndex } // Pass tab index in query params
    });
  }
}
ngAfterViewInit() {
  console.log('Component initialized');
}

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
      // Handle character input
      if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
          this.searchTerm += event.key.toLowerCase();
          this.scrollToRow();
      } 
      // Handle backspace
      else if (event.key === 'Backspace') {
          // Only reset if there's something to remove
          this.searchTerm = '';
          if (this.searchTerm.length > 0) {
              this.searchTerm = this.searchTerm.slice(0, -1);
              this.scrollToRow();
          }
      }
      // Handle other keys (optional)
      else if (event.key === 'Escape') {
          // Reset search term on escape key
          this.searchTerm = '';
          this.scrollToRow();
      }
    }

    scrollToRow() {
        // Find the index of the first matching item
        const matchIndex = this.tableData.findIndex(item => 
            item.name.toLowerCase().startsWith(this.searchTerm)
        );
        // Scroll to the matching row if it exists
        if (matchIndex !== -1) {
            const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
            if (tableRow) {
                tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                this.highlightRow(tableRow);
            }
        } else {
        }
    }

    highlightRow(row: HTMLElement) {
        row.classList.add('highlight');
        setTimeout(() => row.classList.remove('highlight'), 2000);
    }
    
}
