using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Adamas.Models.XeroModels {
    public class XeroConfigurationPKCE
    {
        public string ClientID { get; set; }
        public string CodeVerifier { get; set;}
        public string Scopes { get; set; }
        public string UriCallback { get; set; }
        public string State { get; set; }
        public string AuthorisationUrl { get; set; }
        public string ConnectionsUrl { get; set; }
        public string XeroApiUrlBase { get; set; }
        public string Challenge { get; set; }
        public string XeroApiPayrollUrlBase { get; set; }
    }
}