import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';

import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { NgZorroAntdModule, NZ_I18N, en_US, NzMenuModule, NzIconModule, NzDropDownModule,NzCheckboxModule } from 'ng-zorro-antd';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { AppComponent } from './app.component';
import { registerLocaleData, CommonModule, CurrencyPipe, DatePipe, TitleCasePipe ,DecimalPipe, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ReplaceNullWithTextPipe, TablarizePipe} from '@pipes/pipes';
import en from '@angular/common/locales/en';

import { NZ_DATE_CONFIG, NzI18nService} from 'ng-zorro-antd/i18n';

import { LoginComponent } from './pages/login/login.component';

import { ToastrModule } from 'ngx-toastr';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { JwtModule } from '@auth0/angular-jwt';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
// import { NgSelectModule } from '@ng-select/ng-select';

// import { AppRoutingModule, PAGE_COMPONENTS } from './app-routing.module';
import { AppRoutingModule, PAGE_COMPONENTS } from './app-lazy-routing.module';


import {
  AuthService,
  LoginService,
  GlobalService,
  ClientService,
  StaffService,
  ReportService,
  EmailService,
  RouteGuard,
  AdminStaffRouteGuard,
  LoginGuard,
  TimeSheetService,
  BillingService,
  ListService,
  UploadService,
  MemberService,
  ExecutableService,
  LandingService,
  JobService,
  CacheService,
  ScriptService,
  ShareService,
  SettingsService,
  VersionCheckService,
  MenuService,
  NavigationService,
  JsreportService,
  PrintService,
  ByPassGuard,
  AdminRouteGuard,
  MapService,
  UserService,
  ClientGuard,
  TimerService,
  DllService,
  DocusignService
} from '@services/index';

//  import { IconModule } from '@ant-design/icons-angular';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzModalModule } from 'ng-zorro-antd/modal';

import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { ComponentModule } from '@components/component.module'
import { AgGridModule } from 'ag-grid-angular';
import { BranchesComponent } from './pages/admin/configuration/genrel-setup/branches/branches.component';
import { FundingRegionsComponent } from './pages/admin/configuration/genrel-setup/funding-regions/funding-regions.component';
import { SpreadSheetsModule } from "@grapecity/spread-sheets-angular";
import {DocusignComponent} from './pages/docusign/docusign'

import { ContextMenuModule } from 'ngx-contextmenu';

import { NgSelectModule } from '@ng-select/ng-select';
import { RecipientComponent } from './pages/standalone-app/recipient/recipient.component';


import {DragDropModule} from '@angular/cdk/drag-drop';
import { XeroService } from '@services/XeroService';

import {MSAL_INSTANCE, MsalModule, MsalService} from '@azure/msal-angular'
import { IPublicClientApplication, PublicClientApplication } from '@azure/msal-browser';
import { MatMenuModule } from '@angular/material/menu';

//  import { CanvasJSAngularChartsModule } from '@canvasjs/angular-charts';

registerLocaleData(en);

export function tokenGetter(request) {
  return localStorage.getItem('access_token');
}

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  timeGridPlugin
]);



export function MSALInstanceFactory(): IPublicClientApplication {
  return new PublicClientApplication({
    auth: {
      clientId: '80c6baac-1f64-4229-897d-7924a29d0d9d',
      redirectUri: 'https://adamas.traccs.cloud:9000'
    }
  });
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PAGE_COMPONENTS,
    BranchesComponent,
    FundingRegionsComponent,
    DocusignComponent,
    ReplaceNullWithTextPipe,
    TablarizePipe,
    RecipientComponent ,
    
  ],
  imports: [
    BrowserModule,
    MatMenuModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularEditorModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ComponentModule,
    // CanvasJSAngularChartsModule,
    
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      autoDismiss: true,
      maxOpened: 5
    }),
    // IconModule,
    NgZorroAntdModule,
    NzLayoutModule,
    NzButtonModule,
    NzGridModule,
    NzTableModule,
    NzModalModule,
    NzMenuModule ,
    NzButtonModule ,
    NzIconModule,
    NzDropDownModule,
    NzCheckboxModule,
    NzPopconfirmModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["localhost:5000","https://adamas.traccs.cloud:9000/"],
      }
    }),
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    AgGridModule.withComponents([]),
    InfiniteScrollModule,
    FullCalendarModule,
    DragDropModule,
    SpreadSheetsModule,
    BrowserModule,
    ContextMenuModule.forRoot(),
    BrowserModule,
    MsalModule,
    
    
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_DATE_CONFIG, useValue: { firstDayOfWeek: 1 } }, // Set Monday as first day
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    { provide: Window, useValue: window },
    AuthService,
    LoginService,
    GlobalService,
    ClientService,
    StaffService,
    ReportService,
    EmailService,
    RouteGuard,
    AdminRouteGuard,
    AdminStaffRouteGuard,
    ClientGuard,
    LoginGuard,
    TimeSheetService,
    BillingService,
    ListService,
    UploadService,
    MemberService,
    ExecutableService,
    LandingService,
    JobService,
    CacheService,
    ScriptService,
    ShareService,
    SettingsService,
    VersionCheckService,
    CurrencyPipe, DatePipe, DecimalPipe, TitleCasePipe,
    MenuService,
    NavigationService,
    JsreportService,
    PrintService,
    ByPassGuard,
    MapService,
    DllService,
    XeroService,
    UserService,
    {
      provide: MSAL_INSTANCE,
      useFactory: MSALInstanceFactory
    },
    MsalService,
    TimerService,
    DocusignService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }

declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}
