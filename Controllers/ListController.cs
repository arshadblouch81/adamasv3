
using System;
using System.Globalization;
using System.Data;
using Microsoft.Data.SqlClient;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

using Adamas.DAL;
using Adamas.Models;
using Adamas.Models.Tables;
using Adamas.Models.Modules;
using Adamas.Models.DTO;
using AdamasV3.Models.Dto;

using Microsoft.Extensions.Configuration;
using Staff = Adamas.Models.Modules.Staff;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocuSign.eSign.Model;
using Repositories;

namespace Adamas.Controllers
{
    [Route("api/[controller]")]
    // [Authorize(Roles="PORTAL CLIENT, ADMIN USER, SERVICE PROVIDER, CLIENT MANAGER")]
    public class ListController : Controller
    {
        private readonly DatabaseContext _context;
        private IGeneralSetting GenSettings;
        private IListService listService;
        private IWordService wordService;
        private IPrintService printService;
        private IUploadRepository uploadRepo;
        private ILoginService loginService;

        private static Dictionary<string, string> SQL_STARTSWITH_HISTORY = new Dictionary<string, string>()
        {
            { "CARE DOMAIN",  "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN'" },
            { "DISCIPLINE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE'" },
            { "PROGRAMS",  "SELECT DISTINCT Name FROM HUMANRESOURCETYPES WHERE [Group] = 'PROGRAMS'" },
            { "INCIDENT TYPE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'INCIDENT TYPE'" },
            { "CREATOR", "SELECT DISTINCT [Name] FROM USERINFO WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = ''" },
            { "ROSTER/SVC GROUP", "SELECT 1" },
            { "CATEGORY","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'CASENOTEGROUPS'" },
            { "DOCUMENT CATEGORY", "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DOCCAT'" },
            { "FILE CLASSIFICATION", "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'FILECLASS'"}
        };

        private static Dictionary<string, string> SQL_STARTSWITH_OPNOTE = new Dictionary<string, string>()
        {
            { "CARE DOMAIN",  "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN'" },
            { "CATEGORY","select distinct Description FROM DATADOMAINS WHERE DOMAIN = 'RECIPOPNOTEGROUPS'" },
            { "CREATOR", "SELECT DISTINCT [Name] FROM USERINFO WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = ''" },
            { "DISCIPLINE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE'" },
            { "PROGRAMS",  "SELECT DISTINCT Name FROM HUMANRESOURCETYPES WHERE [Group] = 'PROGRAMS'" }
        };

        private static Dictionary<string, string> SQL_STARTSWITH_CASENOTE = new Dictionary<string, string>()
        {
            { "CARE DOMAIN",  "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN'" },
            { "CATEGORY","select distinct Description FROM DATADOMAINS WHERE DOMAIN = 'CASENOTEGROUPS'" },
            { "CREATOR", "SELECT DISTINCT [Name] FROM USERINFO WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = ''" },
            { "DISCIPLINE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE'" },
            { "PROGRAMS",  "SELECT DISTINCT Name FROM HUMANRESOURCETYPES WHERE [Group] = 'PROGRAMS'" }
        };

        private static Dictionary<string, string> SQL_STARTSWITH_INCIDENTS = new Dictionary<string, string>()
        {
            { "CARE DOMAIN",  "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN'" },
            { "CREATOR", "SELECT DISTINCT [Name] FROM USERINFO WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = ''" },
            { "DISCIPLINE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE'" },
            { "INCIDENT TYPE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'INCIDENT TYPE'" },
            { "PROGRAMS",  "SELECT DISTINCT Name FROM HUMANRESOURCETYPES WHERE [Group] = 'PROGRAMS'" }
        };

        private static Dictionary<string, string> SQL_STARTSWITH_DOCUMENTS = new Dictionary<string, string>()
        {
            { "CARE DOMAIN",  "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN'" },
            { "CREATOR", "SELECT DISTINCT [Name] FROM USERINFO WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = ''" },
            { "DISCIPLINE","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE'" },
            { "DOCUMENT CATEGORY","SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'DOCCAT'" },
            { "FILE CLASSIFICATION",  "SELECT DISTINCT Description FROM DATADOMAINS WHERE DOMAIN = 'FILECLASS'" },
            { "PROGRAMS",  "SELECT DISTINCT Name FROM HUMANRESOURCETYPES WHERE [Group] = 'PROGRAMS'" }
        };


        public ListController(
            DatabaseContext context,
            IConfiguration config,
            IGeneralSetting setting,
            IPrintService _printService,
            IWordService _wordService,
            ILoginService _loginService,
            IUploadRepository uploadRepo,
            IListService _listService)
        {
            _context = context;
            GenSettings = setting;
            listService = _listService;
            wordService = _wordService;
            printService = _printService;
            this.uploadRepo = uploadRepo;
            loginService = _loginService;
        }

        [HttpPut("accounting-profile/{id}")]
        public async Task<IActionResult> UpdateAccountingProfile([FromBody] AccountingProfileDto profile, string id)
        {
            try
            {


                using (var db = _context)

                {
                    var rec = await (from r in _context.Recipients
                               where r.UniqueID == id
                               select new
                               {
                                   Recipients = r
                               }).FirstOrDefaultAsync();


                              rec.Recipients.Notes = profile.Notes;
                              rec.Recipients.OhsProfile= profile.OhsProfile;
                              rec.Recipients.BillTo= profile.BillTo;
                              rec.Recipients.DirectDebit= profile.DirectDebit;
                              rec.Recipients.BPayRef= profile.BPayRef;
                              rec.Recipients.DvaCoBiller= profile.DvaCoBiller;
                              rec.Recipients.Recipient_Split_Bill= profile.Recipient_Split_Bill;
                              rec.Recipients.BillingCycle= profile.BillingCycle;
                              rec.Recipients.BillingMethod= profile.BillingMethod;
                              rec.Recipients.CappedBill= profile.CappedBill;
                              rec.Recipients.PercentageRate= profile.PercentageRate;
                              rec.Recipients.DonationAmount= profile.DonationAmount;
                              rec.Recipients.AccountingIdentifier= profile.AccountingIdentifier;
                              rec.Recipients.OrderNo= profile.OrderNo;
                              rec.Recipients.NDISNumber= profile.NDISNumber;
                              rec.Recipients.ReportingId= profile.ReportingId;
                              rec.Recipients.PrintInvoice= profile.PrintInvoice;
                              rec.Recipients.EmailInvoice= profile.EmailInvoice;
                              rec.Recipients.PrintStatement= profile.PrintStatement;
                              rec.Recipients.EmailStatement= profile.EmailStatement;
                              rec.Recipients.Terms= profile.Terms;
                              rec.Recipients.BillProfile= profile.BillProfile;
                              rec.Recipients.CreditCardType= profile.CreditCardType;
                              rec.Recipients.CreditCardTypeOther= profile.CreditCardTypeOther;
                              rec.Recipients.CreditCardNumber= profile.CreditCardNumber;
                              rec.Recipients.CreditCardExpiry= profile.CreditCardExpiry;
                              rec.Recipients.CreditCardName= profile.CreditCardName;
                              rec.Recipients.CreditCardCCV= profile.CreditCardCCV;
                              rec.Recipients.Fdp= profile.Fdp;
                              rec.Recipients.AdmittedBy= profile.AdmittedBy;
                              rec.Recipients.AdmissionDate= profile.AdmissionDate;
                              rec.Recipients.Branch= profile.Branch;
                              rec.Recipients.Recipient_Coordinator= profile.Recipient_Coordinator;
                              rec.Recipients.MainSupportWorker= profile.MainSupportWorker;
                              rec.Recipients.Whs= profile.Whs;
                              rec.Recipients.Type= profile.Type;
                              rec.Recipients.DischargeDate= profile.DischargeDate;
                              rec.Recipients.DischargedBy= profile.DischargedBy;
                              rec.Recipients.InterpreterRequired= profile.InterpreterRequired;
                              rec.Recipients.Occupation= profile.Occupation;
                              rec.Recipients.FinancialClass= profile.FinancialClass;
                              rec.Recipients.ExcludeFromRosterCopy= profile.ExcludeFromRosterCopy;
                              rec.Recipients.CompanyFlag= profile.CompanyFlag;
                              rec.Recipients.CareplanChange= profile.CareplanChange;
                              rec.Recipients.HideTransportFare=profile.HideTransportFare;
                                        


                await db.SaveChangesAsync();
                return Ok(rec);
            }
            }catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpGet("accounting-profile/{id}")]
        public async Task<IActionResult> GetAccountingProfile(string id)
        {
            var result = await (from r in _context.Recipients
                                join it in _context.ItemTypes on (double)r.ReportingId equals Convert.ToDouble(it.Recnum) into rit
                                from _rit in rit.DefaultIfEmpty()

                                where r.UniqueID == id
                                select new
                                {
                                    r.SqlId,
                                    r.UniqueID,
                                    r.Notes,
                                    r.OhsProfile,
                                    r.BillTo,
                                    r.DirectDebit,
                                    r.BPayRef,
                                    r.DvaCoBiller,
                                    r.Recipient_Split_Bill,
                                    r.BillingCycle,
                                    r.BillingMethod,
                                    r.CappedBill,
                                    r.PercentageRate,
                                    r.DonationAmount,
                                    r.AccountingIdentifier,
                                    r.OrderNo,
                                    r.NDISNumber,
                                    _rit.Title,
                                    r.ReportingId,
                                    r.PrintInvoice,
                                    r.EmailInvoice,
                                    r.PrintStatement,
                                    r.EmailStatement,
                                    r.Terms,
                                    r.BillProfile,
                                    r.CreditCardType,
                                    r.CreditCardTypeOther,
                                    r.CreditCardNumber,
                                    r.CreditCardExpiry,
                                    r.CreditCardName,
                                    r.CreditCardCCV,
                                    r.Fdp,
                                    r.AdmittedBy,
                                    r.AdmissionDate,
                                    r.Branch,
                                    r.Recipient_Coordinator,
                                    r.MainSupportWorker,
                                    r.Whs,
                                    r.Type,
                                    r.DischargeDate,
                                    r.DischargedBy,
                                    r.InterpreterRequired,
                                    r.Occupation,
                                    r.FinancialClass,
                                    r.ExcludeFromRosterCopy,
                                    r.CompanyFlag,
                                    r.CareplanChange,
                                    r.HideTransportFare
                                }).FirstOrDefaultAsync();

            return Ok(result);

        }

        [HttpGet("doctors-information")]
        public async Task<IActionResult> GetDoctorInformation()
        {
            var docInfos = await (from hr in _context.HumanResourceTypes
                                  where hr.Group == "3-MEDICAL" && hr.Type == "GP"
                                  select new
                                  {
                                      hr.Group,
                                      hr.Type,
                                      hr.Name,
                                      hr.Address1,
                                      hr.Address2,
                                      hr.Suburb,
                                      hr.Phone1,
                                      hr.Phone2,
                                      hr.Fax,
                                      hr.Mobile,
                                      hr.Email
                                  }).ToListAsync();

            return Ok(docInfos);
        }


        //[HttpGet("accounting-history/{name}")]
        //public async Task<IActionResult> GetAccountingHistoryData(string name)
        //{
        //    using (var conn = _context.Database.GetDbConnection() as SqlConnection)
        //    {
        //        using (SqlCommand cmd = new SqlCommand(@"
        //                SELECT   sqlid,
        //                     [Traccs Processing Date]         AS [Date],
        //                     [Invoice Number]                 AS [Number],
        //                     CONVERT(MONEY, [Invoice Amount]) AS amount,
        //                     CASE
        //                              WHEN htype = 'r' THEN 0
        //                              ELSE CONVERT(money, [Invoice Tax])
        //                     END                                                           AS gst,
        //                     CONVERT(money, isnull([Invoice Amount], 0) - isnull(paid, 0)) AS [O/S] ,
        //                     CASE
        //                              WHEN LEFT([Invoice Number], 1) = 'r' THEN 'payment'
        //                              WHEN LEFT([Invoice Number], 1) = 'a' THEN 'adjust'
        //                              WHEN LEFT([Invoice Number], 1) = 'c' THEN 'credit'
        //                              ELSE 'invoice'
        //                     END AS [Type],
        //                     package,
        //                     notes
        //            FROM     invoiceheader
        //            WHERE    [Client Code] = @name
        //            ORDER BY [TRACCS PROCESSING DATE]", (SqlConnection)conn))
        //        {
        //            // ABBOTS MORGANICA
        //            cmd.Parameters.AddWithValue("@name", name);
        //            await conn.OpenAsync();

        //            List<dynamic> list = new List<dynamic>();
        //            SqlDataReader rd = await cmd.ExecuteReaderAsync();

        //            while (await rd.ReadAsync())
        //            {
        //                list.Add(new
        //                {
        //                    SqlId = GenSettings.Filter(rd["sqlid"]),
        //                    Date = GenSettings.Filter(rd["Date"]),
        //                    Number = GenSettings.Filter(rd["Number"]),
        //                    Amount = GenSettings.Filter(rd["amount"]),
        //                    Gst = GenSettings.Filter(rd["gst"]),
        //                    Os = GenSettings.Filter(rd["O/S"]),
        //                    Type = GenSettings.Filter(rd["Type"]),
        //                    Package = GenSettings.Filter(rd["package"]),
        //                    Notes= GenSettings.Filter(rd["notes"])
        //                });
        //            }

        //            return Ok(list);
        //        }
        //    }
        //}

        [HttpGet("list-medical")]
        public async Task<IActionResult> GetMedicalTypes(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
	                    SELECT [recordnumber]
                           AS
                           [RecordNumber],
                           [name],
                           [type],
                           LTRIM(CASE WHEN address1 <> '' THEN address1 ELSE '' END + ' ' + CASE
                                 WHEN
                                 address2 <>
                                 '' THEN address2 ELSE '' END 
                                 ) AS
                           [Address],
                           [phone1]
                           AS Phone,
                           [fax],
                           [email],
						   [Mobile],
                           [Email],[Phone1],[Phone2],[Mobile],[Suburb],[Postcode]
                    FROM   humanresourcetypes
                    WHERE  ( [group] = '3-MEDICAL' )
                    ORDER  BY NAME ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<MedicalStaffDto> list = new List<MedicalStaffDto>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new MedicalStaffDto()
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            Name = GenSettings.Filter(rd["Name"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Address = GenSettings.Filter(rd["Address"]),
                            Phone = GenSettings.Filter(rd["Phone"]),
                            Phone1 = GenSettings.Filter(rd["Phone"]),
                            Phone2 = GenSettings.Filter(rd["Phone2"]),
                            Fax = GenSettings.Filter(rd["Fax"]),
                            Email = GenSettings.Filter(rd["Email"]),
                            Mobile = GenSettings.Filter(rd["Mobile"]),
                            Postcode = GenSettings.Filter(rd["Postcode"]),
                            Suburb = GenSettings.Filter(rd["Suburb"])
                        });
                    }

                    return Ok(list);
                }
            }
        }
        [HttpGet("other-contacts")]
        public async Task<IActionResult> GetOtherContacts(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
	                    SELECT 
                            [RecordNumber] AS [RecordNumber], 
                            [Name], 
                            [Type], 
                            LTRIM(
                                CASE WHEN Address1 <> '' THEN Address1 ELSE '' END + ' ' + 
                                CASE WHEN Address2 <> '' THEN Address2 ELSE '' END + ' ' + 
                                CASE WHEN Suburb <> '' THEN Suburb ELSE '' END + ' ' + 
                                CASE WHEN Postcode <> '' THEN Postcode ELSE '' END
                            ) AS [Address], 
                            [Phone1] AS [Phone], 
                            [Fax], 
                            [Email] ,[Phone2],[Mobile],[Suburb],[Postcode],
                            'Update' AS [Action], 
                            'Delete' AS ' ' 
                        FROM 
                            HumanResourceTypes 
                        WHERE 
                            [Group] = '8-OTHER'  
                        ORDER BY 
                            [Name];
                        ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<MedicalStaffDto> list = new List<MedicalStaffDto>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new MedicalStaffDto()
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            Name = GenSettings.Filter(rd["Name"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Address = GenSettings.Filter(rd["Address"]),
                            Phone = GenSettings.Filter(rd["Phone"]),
                            Phone1 = GenSettings.Filter(rd["Phone"]),
                            Phone2 = GenSettings.Filter(rd["Phone2"]),
                            Fax = GenSettings.Filter(rd["Fax"]),
                            Email = GenSettings.Filter(rd["Email"]),
                            Mobile = GenSettings.Filter(rd["Mobile"]),
                            Postcode = GenSettings.Filter(rd["Postcode"]),
                            Suburb = GenSettings.Filter(rd["Suburb"])
                        });
                    }

                    return Ok(list);
                }
            }
        }

        [HttpGet("destination-list")]
        public async Task<IActionResult> GetDestinationAddress(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
	                    SELECT 
                            [RecordNumber] AS [RecordNumber], 
                            [Name], 
                            [Type], 
                            LTRIM(
                                CASE WHEN Address1 <> '' THEN Address1 ELSE '' END + ' ' + 
                                CASE WHEN Address2 <> '' THEN Address2 ELSE '' END + ' ' + 
                                CASE WHEN Suburb <> '' THEN Suburb ELSE '' END + ' ' + 
                                CASE WHEN Postcode <> '' THEN Postcode ELSE '' END
                            ) AS [Address], 
                            [Phone1] AS [Phone], 
                            [Fax], 
                            [Email] ,[Phone2],[Mobile],[Suburb],[Postcode],
                            'Update' AS [Action], 
                            'Delete' AS ' ' 
                        FROM 
                            HumanResourceTypes 
                        WHERE 
                            [Group] = 'DESTINATION'  
                        ORDER BY 
                            [Name];
                        ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<MedicalStaffDto> list = new List<MedicalStaffDto>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new MedicalStaffDto()
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            Name = GenSettings.Filter(rd["Name"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Address = GenSettings.Filter(rd["Address"]),
                            Phone = GenSettings.Filter(rd["Phone"]),
                            Phone1 = GenSettings.Filter(rd["Phone"]),
                            Phone2 = GenSettings.Filter(rd["Phone2"]),
                            Fax = GenSettings.Filter(rd["Fax"]),
                            Email = GenSettings.Filter(rd["Email"]),
                            Mobile = GenSettings.Filter(rd["Mobile"]),
                            Postcode = GenSettings.Filter(rd["Postcode"]),
                            Suburb = GenSettings.Filter(rd["Suburb"])
                        });
                    }

                    return Ok(list);
                }
            }
        }




 [HttpGet("referral-reason")]
        public async Task<IActionResult> GetreferralReason(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM Datadomains WHERE Domain = 'REFERRALREASON' ORDER BY [Description]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new 
                        {
                            Description = GenSettings.Filter(rd["Description"]),
                           
                          
                        });
                    }

                    return Ok(list);
                }
            }
        }
        [HttpGet("program-properties/{program}")]
        public async Task<IActionResult> GetProgramProperties(string program)
        {

            var itemtypes = _context.ItemTypes;

            var itList = await (from it in itemtypes
                                let classification = new List<string>() { "OUTPUT", "EVENT" }
                                where classification.Contains(it.ProcessClassification) && it.Title == program
                                select new
                                {
                                    it.Amount,
                                    it.Price2,
                                    it.Price3,
                                    it.Price4,
                                    it.Price5,
                                    it.Price6,
                                    it.Unit,
                                    it.HACCType,
                                    it.BillText,
                                    it.MainGroup,
                                    it.MinorGroup,
                                    it.RosterGroup,
                                    it.Recnum,
                                    it.noMonday,
                                    it.noTuesday,
                                    it.noWednesday,
                                    it.noThursday,
                                    it.noFriday,
                                    it.noSaturday,
                                    it.noSunday,
                                    it.noPubHol
                                }).FirstOrDefaultAsync();


            return Ok(itList);

        }


    
    [HttpGet("service-properties")]
        public async Task<IActionResult> GetServiceProperties(ServiceParameter service)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
              
               using (SqlCommand cmd = new SqlCommand("spServiceDetail", (SqlConnection)conn))
                {
                   
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AccountNo", service.AccountNo);
                    cmd.Parameters.AddWithValue("@ServiceType", service.ServiceType);
                    cmd.Parameters.AddWithValue("@Program", service.Program);
                    cmd.Parameters.AddWithValue("@docHdrId", service.DocHdrId);
                    cmd.Parameters.AddWithValue("@viewFactor", service.ViewFactor?? "1");

                    await conn.OpenAsync();
                    dynamic obj = null;
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                      obj= new 
                        {
                            recnum = GenSettings.Filter(rd["Recnum"]),
                            Title = GenSettings.Filter(rd["Title"]),
                            Amount = GenSettings.Filter(rd["Amount"]),
                            Price2 = GenSettings.Filter(rd["Price2"]),
                            Price3 = GenSettings.Filter(rd["Price3"]),
                            Price4 = GenSettings.Filter(rd["Price4"]),
                            Price5 = GenSettings.Filter(rd["Price5"]),
                            Price6 = GenSettings.Filter(rd["Price6"]),
                            Unit = GenSettings.Filter(rd["Unit"]),
                            HACCType = GenSettings.Filter(rd["HACCType"]),
                            BillText = GenSettings.Filter(rd["BillText"]),
                            MainGroup = GenSettings.Filter(rd["MainGroup"]),
                            MinorGroup = GenSettings.Filter(rd["MinorGroup"]),
                            RosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                            noMonday = GenSettings.Filter(rd["noMonday"]),
                            noTuesday = GenSettings.Filter(rd["noTuesday"]),
                            noWednesday = GenSettings.Filter(rd["noWednesday"]),
                            noThursday = GenSettings.Filter(rd["noThursday"]),
                            noFriday = GenSettings.Filter(rd["noFriday"]),
                            noSaturday = GenSettings.Filter(rd["noSaturday"]),
                            noSunday = GenSettings.Filter(rd["noSunday"]),
                            noPubHol = GenSettings.Filter(rd["noPubHol"]),
                            GstRate = GenSettings.Filter(rd["GstRate"]),
                            BudgetGroup = GenSettings.Filter(rd["Budgetgroup"]),
                            BTotal = GenSettings.Filter(rd["BTotal"]),
                            BillingMethod = GenSettings.Filter(rd["BillingMethod"]),
                            PercentageRate = GenSettings.Filter(rd["PercentageRate"]),
                            ADTotal = GenSettings.Filter(rd["ADTotal"]),
                            GSTotal = GenSettings.Filter(rd["GSTotal"])
                       
                          
                        };
                    }

                    return Ok(obj);
                }
            }

        }
      

        [HttpGet("waitlist")]
        public async Task<IActionResult> GetWaitingList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT   [Title]
                            FROM     itemtypes
                            WHERE    processclassification = 'output'
                            AND      [RosterGroup] IN ('admission')
                            AND      [MinorGroup]  IN ('other',
                                                       'review')
                            AND      (
                                              enddate IS NULL
                                     OR       enddate >= (SELECT FORMAT (getdate(), 'MM-dd-yyyy')))
                            ORDER BY [Title]",
                    (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Title"]).ToUpper());
                    }

                    return Ok(list);
                }
            }
        }

        [HttpGet("pension-and-percent-fee")]
        public async Task<IActionResult> GetPensionAndPercentFee(string program)
        {

            var registration = _context.Registration;
            var fee = await (from reg in registration select new { reg.Pension, reg.BasicFeePercent }).FirstOrDefaultAsync();
            return Ok(fee);

            //using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            //{
            //    using (SqlCommand cmd = new SqlCommand(@"SELECT TOP  1 Pension, BasicFeePercent  FROM Registration", (SqlConnection)conn))
            //    {
            //        await conn.OpenAsync();                    

            //        PensionPercentDto list = new PensionPercentDto();
            //        SqlDataReader rd = await cmd.ExecuteReaderAsync();

            //        if(await rd.ReadAsync())
            //        {
            //            list = new {
            //                Pension = GenSettings.Filter(rd["Pension"]),
            //                BasicFeePercent = GenSettings.Filter(rd["BasicFeePercent"])
            //            };
            //        }

            //        return Ok(list);
            //    program-level
            //}
        }

        [HttpGet("humanresourcetypes/type")]
        public async Task<IActionResult> GetHumanResourceType(ProgramStatusDto prog)
        {
            var type = await (from ht in _context.HumanResourceTypes where ht.Name == prog.Program select ht.Type)
                            .FirstOrDefaultAsync();

            return Ok(type);
        }

        [HttpGet("program-level/{program}")]
        public async Task<IActionResult> GetProgramLevel(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT 
                    ISNULL(hr.UserYesNo1,0) AS UserYesNo1,
                    ISNULL(hr.User3, 0) AS User3,
                    ISNULL(hr.User2, 0) AS User2,
                    (
	                    SELECT User1 FROM DataDomains WHERE Dataset = 'CDC' AND Domain = 'PackageRates' AND [Description] = hr.User3
                    ) AS Quantity,
                    rp.TimeUnit,
                    hr.DefaultDailyFee,
                    hr.[Type],
                    hr.P_Def_Expire_Using as expire_using,hr.RecordNumber 
                    FROM HumanResourceTypes hr LEFT JOIN RecipientPrograms rp ON hr.[Name] = rp.Program 
                    WHERE [Name] = @program AND [Group] = 'PROGRAMS';", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@program", program);
                    await conn.OpenAsync();

                    ProgramLevelDto list = new ProgramLevelDto();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    if (await rd.ReadAsync())
                    {
                        list = new ProgramLevelDto()
                        {
                            IsCDC = Convert.ToInt32(GenSettings.Filter(rd["UserYesNo1"])) == 1 ? true : false,
                            Level = GenSettings.Filter(rd["User3"]),
                            Quantity = GenSettings.Filter(rd["Quantity"]),
                            TimeUnit = GenSettings.Filter(rd["TimeUnit"]),
                            User2 = GenSettings.Filter(rd["User2"]),
                            ExpireUsing = GenSettings.Filter(rd["expire_using"]),
                            DefaultDailyFee = Convert.ToDouble(GenSettings.Filter(rd["DefaultDailyFee"])),
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            Type = GenSettings.Filter(rd["Type"])
                        };
                    }

                    return Ok(list);
                }
            }
        }

    [HttpGet("financialclass")]
    public async Task<IActionResult> GetFinancialClasses(string contactGroup)
    {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DataDomains WHERE DOMAIN = 'FINANCIALCLASS'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
    }

        [HttpGet("contact-types/{group}")]
        public async Task<IActionResult> GetContactType(string contactGroup)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select DISTINCT UPPER(DESCRIPTION) from datadomains WHERE Domain = 'CONTACTSUBGROUP' AND HACCCode like @contactGroup", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@contactGroup", contactGroup);
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("type-kin")]
        public async Task<IActionResult> GetTypeKin()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER(DESCRIPTION) as DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'CARER RELATIONSHIP' OR (HACCCODE = '1-NEXT OF KIN')", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("destination-types")]
        public async Task<IActionResult> GetDestinationTypes()
        {
            var dbConnection = _context.Database.GetDbConnection();
            if (dbConnection is not SqlConnection conn)
            {
                return StatusCode(500, "Database connection is not a valid SqlConnection.");
            }

            try
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = new SqlCommand(
                    @"SELECT DISTINCT [Type] FROM HumanResourceTypes WHERE [GROUP] = 'DESTINATION'", conn))
                {
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();

                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(Convert.ToString(rd["Type"]) ?? ""));
                        }

                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
            finally
            {
                await conn.CloseAsync();
            }
        }

        

        [HttpGet("type-other/{caseName}")]
        public async Task<IActionResult> GetTypeOther(string caseName)
        {

            // return Ok(await (from dd in _context.DataDomains 
            //                     where dd.HACCCode == caseName && dd.Domain == "CONTACTSUBGROUP"
            //                         select dd.Description).ToListAsync());

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER(DESCRIPTION) as [DESCRIPTION] FROM DATADOMAINS WHERE HACCCODE = @case AND DOMAIN = 'CONTACTSUBGROUP'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@case", caseName);
                    await conn.OpenAsync();


                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("recipient-sqlid/{id}")]
        public async Task<IActionResult> GetRecipientSQLID(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                var sqlid = await (from r in _context.Recipients where r.UniqueID == id select r.SqlId).FirstOrDefaultAsync();
                return Ok(sqlid);
            }
        }



        [HttpGet("clientportalmethod")]
        public async Task<IActionResult> GetClientPortalMethod()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                var portalMethod = await (from r in _context.Registration select r.ClientPortalManagerMethod).FirstOrDefaultAsync();

                return Ok((portalMethod == "MANAGE BY CLIENT CATEGORY") ? true : false);
            }
        }

        [HttpPut("clientportalmethod/{value}")]
        public async Task<IActionResult> PutClientPortalMethod(bool value)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                try
                {
                    using (var db = _context)
                    {

                        var query = await (from r in _context.Registration select r).FirstOrDefaultAsync();
                        query.ClientPortalManagerMethod = value ? "MANAGE BY CLIENT CATEGORY" : "";
                        await db.SaveChangesAsync();
                    }
                    return Ok(new
                    {
                        success = true
                    });
                }
                catch (Exception ex)
                {
                    return BadRequest(ex);
                }
            }
        }



        [HttpPost("in")]
        public async Task<IActionResult> PostSomething()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();

                List<string> list = new List<string>() { "BALL W", "ADDISON B" };
                var nameParameters = new List<string>();
                using (SqlCommand cmd = new SqlCommand())
                {
                    int i = 0;
                    foreach (var user in list.Select(x => x))
                    {
                        var paramName = "@user" + i.ToString();
                        cmd.Parameters.AddWithValue(paramName, user.ToString());
                        nameParameters.Add(paramName);
                        i++;
                    }

                    cmd.CommandText = String.Format("UPDATE Recipients SET Alias = 'ARIS' WHERE AccountNo IN ({0})", String.Join(",", nameParameters));
                    cmd.Connection = conn;
                    await cmd.ExecuteNonQueryAsync();
                }
                return Ok(true);
            }
        }

        [HttpGet("portal-manager")]
        public async Task<IActionResult> GetPortalManager()
        {
            var manager = await (from ui in _context.UserInfo where ui.EndDate == null select ui.Name).ToListAsync();
            return Ok(manager);
        }

        [HttpGet("billingrate")]
        public async Task<IActionResult> GetActivitiesDropDown()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT dbo.GetBillingRate('ABBOTS MORGANICA','*ADMISSION','CSTDA ECI - 30501')", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    double billingRate = default(double);

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        if (await rd.ReadAsync())
                        {
                            billingRate = rd[0] != DBNull.Value ?
                                            rd.GetDouble(0) :
                                                default(double);
                        }

                        billingRate = Math.Round(billingRate, 2);

                        return Ok(billingRate);
                    }
                }
            }
        }


        [HttpGet("reminders-recipient")]
        public async Task<IActionResult> GetRemindersRecipient()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Name] FROM UserInfo where isnull(enddate, '') = ''", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(rd.GetString(0));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("reasons")]
        public async Task<IActionResult> GetReasons()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' AND DATASET = 'DEX'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(rd.GetString(0));
                        }
                        return Ok(list);
                    }
                }
            }
        }
         [HttpGet("suspend/reasons")]
        public async Task<IActionResult> GetsuspendReasons()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' AND DATASET = 'DEX'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(rd.GetString(0));
                        }
                        return Ok(list);
                    }
                }
            }
        }
     [HttpGet("admission/types/{program}")]
        public async Task<IActionResult> GetAdmissionType(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE SO.ServiceProgram = '" + program + "' AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] IN ('ADMISSION') AND ITM.[MinorGroup] IN ('ADMISSION') AND (ITM.EndDate Is Null OR ITM.EndDate >= convert(varchar,getDate(),111))) ORDER BY [Service Type]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(rd.GetString(0));
                        }
                        return Ok(list);
                    }
                }
                
            }
        }
        [HttpGet("discharge/loan")]
        public async Task<IActionResult> getDischargeLoans(Code_Program code_program)
        {
            var sql=@"SELECT [Name] FROM HumanResources WHERE [Group] = 'LOANITEMS' AND [PersonID] = @id AND Date2 is Null AND [Address1] IN (select * from split(@program,','))";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", code_program.id);
                    cmd.Parameters.AddWithValue("@program", code_program.program);
                    await conn.OpenAsync();

                        List<dynamic> list = new List<dynamic>();

                        using (var rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                            {
                                list.Add(new
                                {
                                    
                                    Name = GenSettings.Filter(rd["Name"]),
                                   
                                });
                            }
                        }
                        return Ok(list);
                }
            }
        }
         [HttpGet("discharge/staff")]
        public async Task<IActionResult> getDischargeStaff(Code_Program code_program)
        {
            var sql=@"SELECT DISTINCT Staff.[UniqueID] ,UPPER([Staff].[LastName]) + ', ' + CASE WHEN [Staff].[FirstName] <> '' THEN [Staff].[FirstName] ELSE ' ' END + CASE WHEN [Staff].[MiddleNames] <> '' Then [Staff].[MiddleNames] ELSE ' ' END AS [Name], CASE WHEN N1.Address <> '' THEN  N1.Address ELSE N2.Address END  AS Address, CASE WHEN P1.Contact <> '' THEN  P1.Contact ELSE P2.Contact END AS Contact 
            FROM ROSTER AS R INNER JOIN STAFF ON R.[Carer Code] = Staff.AccountNo LEFT JOIN (SELECT PERSONID, Suburb,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress = 1)  AS N1 ON N1.PersonID = Staff.UniqueID LEFT JOIN (SELECT PERSONID,  CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE ' ' END +  CASE WHEN Address2 <> '' THEN Address2 + ' ' ELSE ' ' END +  CASE WHEN Suburb <> '' THEN Suburb + ' ' ELSE ' ' END +  CASE WHEN Postcode <> '' THEN Postcode ELSE ' ' END AS Address  FROM NamesAndAddresses WHERE PrimaryAddress <> 1)  AS N2 ON N2.PersonID = Staff.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone = 1)  AS P1 ON P1.PersonID = Staff.UniqueID LEFT JOIN (SELECT PersonID,  PhoneFaxOther.Type + ' ' +  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END AS Contact  FROM PhoneFaxOther WHERE PrimaryPhone <> 1)  AS P2 ON P2.PersonID = Staff.UniqueID 
                    WHERE Status = 1 AND [Client Code] = @code AND [Program] IN (select * from split(@program,',')) AND [Type] IN (2, 3, 4, 5, 8, 10, 11, 12) AND Date > convert(varchar,getDate(),111) AND [Carer Code] > '!z' ";
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@code", code_program.code);
                    cmd.Parameters.AddWithValue("@program", code_program.program);
                    await conn.OpenAsync();

                        List<dynamic> list = new List<dynamic>();

                        using (var rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                            {
                                list.Add(new
                                {
                                    UniqueID = GenSettings.Filter(rd["UniqueID"]),
                                    Name = GenSettings.Filter(rd["Name"]),
                                    Address = GenSettings.Filter(rd["Address"]),
                                    Contact = GenSettings.Filter(rd["Contact"])
                                });
                            }
                        }
                        return Ok(list);
                }
            }
        }

  [HttpGet("affected-staff/{code}")]
        public async Task<IActionResult> geAffectedStaff(string code)
        {
            var sql=@"SELECT DISTINCT STAFF.UniqueID,AccountNo, [Roster].[Carer Code] + ' - ' + CASE WHEN [Staff].[Contact1] <> '' THEN [Staff].[Contact1] ELSE ' ' END + ', ' + CASE WHEN [Staff].[Contact2] <> '' THEN [Staff].[Contact2] ELSE ' ' END As [ContactDetails]
                    FROM ROSTER INNER JOIN STAFF ON Roster.[Carer Code] = Staff.AccountNo 
                    WHERE Status = 1 AND [Client Code] = @code AND Yearno > 2000 AND [Carer Code] <> '!MULTIPLE' AND [Carer Code] <> '!INTERNAL'";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@code", code);
                  
                    await conn.OpenAsync();

                        List<dynamic> list = new List<dynamic>();

                        using (var rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                            {
                                list.Add(new
                                {
                                    UniqueID = GenSettings.Filter(rd["UniqueID"]),
                                    AccountNo = GenSettings.Filter(rd["AccountNo"]),
                                    ContactDetails = GenSettings.Filter(rd["ContactDetails"])
                                   
                                });
                            }
                        }
                        return Ok(list);
                }
            }
        }
        [HttpGet("check-package-name-exist/{package}")]
        public async Task<IActionResult> GetPackageNameExist(string package)
        {
            var result = (from hr in _context.HumanResourceTypes where hr.Name == package select hr).Count();
            return Ok(result > 0);
        }

        [HttpPost("activities/all")]
        public async Task<IActionResult> PostGetActivitiesDropDown([FromBody] DboGetActivities activities)
        {
            //return Ok(activities);

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    var allowedDateString = await GenSettings.AllowedDaysString(activities.Date);
                    using (SqlCommand cmd = new SqlCommand(@"[GetActivities]", (SqlConnection)conn))
                    {
                        await conn.OpenAsync();
                        cmd.CommandType = CommandType.StoredProcedure;

                        // EXEC 
                        // dbo.[GetActivities]
                        // @Recipient = 'LEIGHFIELD CHEN',
                        // @Program = 'HCP-L4 LEIGHFIELD C',
                        // @ForceAll = '',
                        // @MainGroup = 'ADMISSION',
                        // @SubGroup = '',
                        // @ViewType = 'Staff',
                        // @AllowedDays = '0',
                        // @Duration = '0'


                        // ADMINISTRATION
                        // ADMISSION
                        // ALLOWANCE
                        // CENTREBASED
                        // GROUPACTIVITY
                        // ITEM
                        // ONEONONE
                        // RECPTABSENCE
                        // SALARY
                        // SLEEPOVER
                        // TRANSPORT
                        // TRAVELTIME

                        // @Program should be the program
                        // 11:55
                        // @MainGroup should be the rostergroup i.e. admission, oneonone etc
                        // 11:55
                        // @subgroup should be th minorgroup if applicable - for timesheets you can probably set to '' as it only really affects meal activities
                        // 11:56
                        // @ViewType shoule be eithe 'STAFF' or 'RECIPIENT' depending on the timesheet view they are in when calling the procedure
                        // 11:56
                        // @AllowedDays should be a string of 8 characters '00000000'
                        // 11:57
                        // with the value changed to 1 for the bit that today is
                        // 11:57
                        // eg Monday is position 1
                        // 11:57
                        // tuesday 2, wednesday 3, thursday4, friday 5, saturday 6, sunday 7, public holiday 7
                        // 11:58
                        // So you need to evaludate the date of the activity and dtermine if it is mon, tue, wed etc or PublicHoliday - and then switch that character to 1
                        // 11:58
                        // SO for instance if it is Wednesday and not a public holiday it would be '00100000'
                        // 11:59
                        // if date is monday but it is a public holiday it would be '00000001'
                        // 11:59
                        // if date is monday and not a public holiday it would be '10000000'

                        // var dayMask = await GenSettings.GetDateMaskValue(activities.AllowedDays.ToString());
                        var dayMask = "";


                        cmd.Parameters.AddWithValue("@Recipient", activities.Recipient);
                        cmd.Parameters.AddWithValue("@Program", activities.Program);
                        cmd.Parameters.AddWithValue("@ForceAll", Convert.ToInt32(activities.ForceAll));
                        cmd.Parameters.AddWithValue("@MainGroup", activities.MainGroup);
                        cmd.Parameters.AddWithValue("@SubGroup", activities.SubGroup ?? "");
                        cmd.Parameters.AddWithValue("@ViewType", activities.ViewType);

                        // cmd.Parameters.AddWithValue("@Date", activities.Date);
                        // cmd.Parameters.AddWithValue("@StartTime", activities.StartTime);
                        // cmd.Parameters.AddWithValue("@EndTime", activities.EndTime);  

                        cmd.Parameters.AddWithValue("@AllowedDays", allowedDateString);
                        cmd.Parameters.AddWithValue("@Duration", activities.Duration ?? "0");

                        List<dynamic> list = new List<dynamic>();

                        using (var rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                            {
                                list.Add(new
                                {
                                    Activity = rd.GetString(0)
                                });
                            }
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        [HttpGet("activities/program")]
        public async Task<IActionResult> GetProgramActivities(DboGetActivities activities)
        {
            //return Ok(activities);
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand("", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.CommandType = CommandType.StoredProcedure;

                    var dayMask = "";

                    DateTime today = DateTime.Today;
                    string curr_date = today.ToString("yyyy/MM/dd");

                    string sql = "SELECT DISTINCT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid " +
                    " WHERE SO.serviceprogram = '" + activities.Program + "'" +
                    " AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] = 'ADMINISTRATION' AND processclassification = 'OUTPUT' " +
                    " AND ( ITM.enddate IS NULL  )) ORDER BY [service type]";

                    //OR ITM.enddate >= '" + curr_date + "'

                    cmd.CommandText = sql;

                    List<dynamic> list = new List<dynamic>();

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Activity = rd.GetString(0)
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
         [HttpGet("clinical/diagonsiscode/{name}/{type}")]
        public async Task<IActionResult> GetDiagnosisCode(String name, int type )
        {
            var sql="SELECT DISTINCT Code,Description FROM NDIAGNOSISTYPES WHERE Description=@name ";
            if (type==2){
                sql="SELECT DISTINCT  Code,Description FROM MDiagnosisTypes WHERE Description=@name";
            }
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                 dynamic obj=null ;
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                      cmd.Parameters.AddWithValue("@name", name);
                    await conn.OpenAsync();
                   
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                       
                        while (await rd.ReadAsync())
                        {

                              obj=new
                            {
                                Code = rd.GetString(0),
                                Description =rd.GetString(1)

                            };

                           
                        }
                        return Ok(obj);
                    }
                }
            }
        }
          [HttpGet("clinical/diagonsis/{personId}/{type}")]
        public async Task<IActionResult> GetDiagnosisItems(String personId, int type )
        {
            var sql="SELECT DISTINCT Code,Description FROM NDIAGNOSISTYPES WHERE Description NOT IN (SELECT Description FROM NDIAGNOSIS WHERE PersonID = @personId AND NDIAGNOSIS.Description = NDIAGNOSISTYPES.Description) ORDER BY Description";
            if (type==2){
                sql="SELECT DISTINCT '111' Code,Description FROM MDiagnosisTypes WHERE Description NOT IN (SELECT Description FROM MDIAGNOSIS WHERE PersonID = @personId AND MDIAGNOSIS.Description = MDIAGNOSISTYPES.Description) ORDER BY Description";
            }
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                      cmd.Parameters.AddWithValue("@personId", personId);
                    await conn.OpenAsync();

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();
                        while (await rd.ReadAsync())
                        {

                              list.Add(new
                            {
                                Code = rd.GetString(0),
                                Description =rd.GetString(1)

                            });

                           
                        }
                        return Ok(list);
                    }
                }
            }
        }
   [HttpGet("oni/mainissues/{id}/{type}")]
        public async Task<IActionResult> oniMianIssues(string id, int type)
        {
            string query = string.Empty;

            if (type==1)
                query = " SELECT RecordNumber,PersonID, Description, Action,'' as Code FROM ONIMainIssues WHERE PersonID = @id ORDER BY DESCRIPTION";
            else if (type==2)
                query = " SELECT RecordNumber,PersonID, Description, Action,'' as Code FROM ONISecondaryIssues WHERE PersonID = @id ORDER BY DESCRIPTION";
            else if (type==3)
                    query = "SELECT RecordNumber, PersonID, Service as DESCRIPTION, Information as Action, '' as Code FROM ONIServices WHERE  PersonID = @id ORDER BY DESCRIPTION";
            else if (type==4)
                    query = "SELECT RecordNumber, PersonID, Description , '' as Action, '' as Code FROM ONIHealthConditions WHERE  PersonID = @id ORDER BY DESCRIPTION";
            else if (type==5)
                    query = "SELECT RecordNumber, PersonID, Description , ICDCODE as Action, Code FROM NDIAGNOSIS WHERE  PersonID = @id ORDER BY DESCRIPTION";
            else if (type==6)
                    query = "SELECT RecordNumber, PersonID, Description , ICDCODE as Action, Code FROM MDIAGNOSIS WHERE  PersonID = @id ORDER BY DESCRIPTION";




            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@query, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {

                             list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),                               
                                Description = GenSettings.Filter(rd["Description"]),
                                Action = GenSettings.Filter(rd["Action"]),
                                PersonId = GenSettings.Filter(rd["PersonID"]),
                                Code = GenSettings.Filter(rd["Code"])
                               
                            });

                          

                        }
                    }
                    return Ok(list);
                }
            }
        }
       
        [HttpPost("oni/mainissues")]
        public async Task<IActionResult> PostoniMianIssues([FromBody] OniIssues oni)
        {
            string query = string.Empty;

            if (oni.type==1)
                query = " INSERT INTO ONIMainIssues(PersonId, Description, Action) VALUES  (@id,@Description,@Action)";
            else if (oni.type==2)              
                query = " INSERT INTO ONISecondaryIssues(PersonId, Description, Action) VALUES  (@id,@Description,@Action)";
              else if (oni.type==3)              
                query = " INSERT INTO ONIServices(PersonId, Service, Information) VALUES  (@id,@Description,@Action)";
            else if (oni.type==4)
                query = " INSERT INTO ONIHealthConditions(PersonId, Description) VALUES  (@id,@Description)";
            else if (oni.type==5)
                query = " INSERT INTO NDIAGNOSIS(PersonId, Description,ICDCODE) VALUES  (@id,@Description,@Action)";
            else if (oni.type==6)
                query = " INSERT INTO MDIAGNOSIS(PersonId, Description,ICDCODE) VALUES  (@id,@Description,@Action)";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@query, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", oni.PersonId);
                    cmd.Parameters.AddWithValue("@Description", oni.Description);
                    cmd.Parameters.AddWithValue("@Action", oni.Action ?? "");
                    await conn.OpenAsync();                   
                    await cmd.ExecuteNonQueryAsync();
                    return Ok(true);
                }
            }
        }
        [HttpPut("oni/mainissues")]
        public async Task<IActionResult> UpdateOniMianIssues( [FromBody] OniIssues oni)
        {
            string query = string.Empty;

            if (oni.type==1)
                query = " Update  ONIMainIssues set PersonId=@id, Description=@Description, Action=@Action where RecordNumber=@RecordNumber";
            else if (oni.type==2)        
                query = " Update  ONISecondaryIssues set PersonId=@id, Description=@Description, Action=@Action where RecordNumber=@RecordNumber";      
      
            else if (oni.type==3)  
                query = " Update  ONIServices set PersonId=@id, Service=@Description, Information=@Action where RecordNumber=@RecordNumber";                
           else if (oni.type==4)  
                query = " Update  ONIHealthConditions set PersonId=@id, Description=@Description where RecordNumber=@RecordNumber";                
            else if (oni.type==5)  
                query = " Update  NDIAGNOSIS set PersonId=@id, Description=@Description,ICDCODE=@Action, code=@Code where RecordNumber=@RecordNumber";                
            else if (oni.type==5)  
                query = " Update  MDIAGNOSIS set PersonId=@id, Description=@Description,ICDCODE=@Action, code=@Code where RecordNumber=@RecordNumber";                

            
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@query, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", oni.PersonId);
                    cmd.Parameters.AddWithValue("@Description", oni.Description);
                    cmd.Parameters.AddWithValue("@Action", oni.Action?? "");
                    cmd.Parameters.AddWithValue("@code", oni.code?? "");
                    cmd.Parameters.AddWithValue("@RecordNumber", oni.RecordNumber);
                    await conn.OpenAsync();                   
                    await cmd.ExecuteNonQueryAsync();
                    return Ok(true);
                }
            }
        }
          [HttpDelete("oni/mainissues/{RecordNumber}/{type}")]
        public async Task<IActionResult> DeleteOniMianIssues(string RecordNumber, int type)
        {
            string query = string.Empty;

            if (type==1)
                query = " delete from  ONIMainIssues  where RecordNumber=@RecordNumber";
            else if (type==2)        
                query = " delete from  ONISecondaryIssues  where RecordNumber=@RecordNumber";     
            else if (type==3)        
                query = " delete from  ONIServices  where RecordNumber=@RecordNumber";   
            else if (type==4)        
                query = " delete from  ONIHealthConditions  where RecordNumber=@RecordNumber";    
            else if (type==5)        
                query = " delete from  NDIAGNOSIS  where RecordNumber=@RecordNumber";   
             else if (type==6)        
                query = " delete from  MDIAGNOSIS  where RecordNumber=@RecordNumber";  
      
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@query, (SqlConnection)conn))
                {
                  
                    cmd.Parameters.AddWithValue("@RecordNumber", RecordNumber);
                    await conn.OpenAsync();                   
                    await cmd.ExecuteNonQueryAsync();
                    return Ok(true);
                }
            }
        }
        // CREATE PROCEDURE
        [HttpGet("contact/group")]
        public async Task<IActionResult> GetcontactGroup()
        {
            string query = string.Empty;

          
                query = " SELECT  [Description], UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Description, '1-', ''), '2-', ''), '3-', ''), '4-', ''), '5-', ''), '6-', ''), '7-', ''), '8-', ''), '9-', '')) AS [Type] FROM DataDOmains WHERE DOMain = 'CONTACTGROUP' ORDER BY DESCRIPTION";


            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@query, (SqlConnection)conn))
                {
                    
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {

                             list.Add(new
                            {
                                Description = GenSettings.Filter(rd["Description"]).ToUpper(),
                                Type = GenSettings.Filter(rd["type"]).ToUpper()
                               
                            });

                          

                        }
                    }
                    return Ok(list);
                }
            }
        }

        // CREATE PROCEDURE
        [HttpGet("kin-type/{contactGroup}")]
        public async Task<IActionResult> GetListTypeKin(string contactGroup)
        {
            string query = string.Empty;

            if (contactGroup == "1-NEXT OF KIN" || contactGroup == "2-CARER" || contactGroup == "CARER")
                query = "SELECT DISTINCT UPPER(Description) FROM DATADOMAINS WHERE DOMAIN = 'CARER RELATIONSHIP' OR (HACCCODE = @contactGroup)";

            else if (contactGroup == "3-MEDICAL" || contactGroup == "MEDICAL")
                query = "SELECT DISTINCT UPPER(Description) FROM DATADOMAINS WHERE DOMAIN = 'CONTACTSUBGROUP' AND (HACCCODE = @contactGroup)";
            else if(contactGroup == "ALLIED HEALTH" || contactGroup == "4-ALLIED HEALTH")
                query = "SELECT DISTINCT UPPER(Description) FROM DATADOMAINS WHERE HACCCODE IN ('ALLIED HEALTH', '4-ALLIED HEALTH')";
            else if(contactGroup == "6-POWER OF ATTORNEY" || contactGroup == "POWER OF ATTORNEY")
                query = "SELECT DISTINCT UPPER(Description) FROM DATADOMAINS WHERE HACCCODE IN ('6-POWER OF ATTORNEY')";
            else if(contactGroup == "7-LEGAL-OTHER")
                query = "SELECT DISTINCT UPPER(Description) FROM DATADOMAINS WHERE HACCCODE IN ('7-LEGAL-OTHER')";
            else
                query = "SELECT DISTINCT UPPER(Description) FROM DATADOMAINS WHERE HACCCODE IN ('OTHER', '8-OTHER')";


            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@query, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@contactGroup", contactGroup);
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                            list.Add(rd.GetString(0));
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("sample")]
        public async Task<IActionResult> GetSample()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT  [Recordno] As [RecordNumber],[Client Code] As [Recipient],[Date] As [Date], [Start Time] As [Start Time],
                    (([Duration] * 5)/60) As [Duration],[Service Type] As [Activity],[Carer Code] As [Staff], [Anal] As [Analysis], [Program] As [Program],[HACCType] As [Dataset Type], ([CostQty] * [Unit Pay Rate]) As [Service Cost]
                    FROM Roster WHERE [Client Code] = 'MTA-YU G' AND [Date] BETWEEN '1900/01/01' AND '1905/05/28' ORDER BY Date, [Start Time]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Dictionary<string, object>> dicList = new List<Dictionary<string, object>>();
                    int numCol = rd.FieldCount;

                    while (await rd.ReadAsync())
                    {
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        for (var a = 0; a < numCol; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), GenSettings.Filter(rd.GetValue(a)));
                        }
                        dicList.Add(dic);
                    }

                    var uniqueDates = dicList.Select(x => x["Date"]).Distinct().ToList();

                    Dictionary<string, object> finalList = new Dictionary<string, object>();

                    foreach (string date in uniqueDates)
                    {
                        finalList.Add(date, dicList.Where(x => x["Date"].ToString() == date).Select(x => x));
                    }

                    return Ok(finalList);
                }
            }
        }

         [HttpGet("quote/chargeType")]
        public async Task<IActionResult> GetChargeType(ChargeTypeInput type)
        {

            string sqlCmd = string.Empty;

            sqlCmd = "spQuoteChargeType";
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sqlCmd, (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AccountNo", type.AccountNo);
                    cmd.Parameters.AddWithValue("@chargeType", type.Index);
                    cmd.Parameters.AddWithValue("@program", type.Program);

                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0].ToString().Trim()));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpPost("merge-fields/{personID}")]
        public async Task<IActionResult> GetMergeFields([FromBody] QuoteHeaderDTO quote, string personID)
        {

            if (string.IsNullOrEmpty(personID))
            {
                return BadRequest();
            }

            var fields = await this.listService.GetRecipientQuoteMergeFields(personID);

            string sourcePath = @"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan.docx";
            string destinationPath = @"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan_Copy.docx";

            var mergeResult = await wordService.MergeFieldsDocument(fields, sourcePath, destinationPath, "Careplan_Copy");

            if (!mergeResult.Success)
            {
                return BadRequest(mergeResult.Error);
            }

            return Ok(true);
        }

        [HttpGet("template/list")]
        public async Task<IActionResult> GetGlobalTemplate()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title AS FileTypes 
                FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 
                AND TRACCSType = 'CAREPLAN' ORDER BY FileTypes ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]).ToUpper());
                        }
                        return Ok(list);
                    }
                }
            }
        }
        //

        [HttpGet("program/contingency/list/{personID}")]

        public async Task<IActionResult> GetProgramContingency(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT([Program]) FROM RecipientPrograms rp INNER JOIN HumanResourceTypes pr ON rp.Program = pr.Name  WHERE  [PersonID] = @personID AND IsNull([User2], '') <> 'Contingency' ORDER BY [Program]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("qccs/mds/generalDemography/{personID}")]

        public async Task<IActionResult> GetGeneralDemography(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT ExcludeFromStats,[Surname/Organisation], FirstName, DateOfBirth,
                    Gender, Suburb + ' ' + Postcode AS Suburb,
                    CountryOfBirth, HomeLanguage, IndiginousStatus, MaritalStatus, CALDStatus, RemoteStatus, FinancialStatus,
                    RECIPT_DVA_Card_Holder_Status, RECIPT_Paid_Employment_Participation, NRCP_GovtPensionStatus,
                    RECIPT_Date_Caring_Role_Commenced, NRCP_YearsSpentCaring, NRCPCarerYesNo, RECIPT_Care_Role, RECIPT_Care_Need,
                    RECIPT_Number_Care_Recipients, RECIPT_Time_Spent_Caring, NRCP_CurrentUseFormalServices, NRCP_InformalSupport,
                    RECIPT_Challenging_Behaviour, RECIPT_Care_Recipients_Primary_Disability,
                    RECIPT_Care_Recipients_Primary_Care_Needs, RECIPT_Care_Recipients_Level_Need, DatasetCarer,
                    NRCP_CarerRelationship, NRCP_CarerCoResidency, RECIPT_Dementia
                    FROM Recipients WHERE UniqueID = @personID ", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            excludeFromStats = (GenSettings.Filter(rd["ExcludeFromStats"])),
                            surnameOrg = (GenSettings.Filter(rd["Surname/Organisation"])),
                            firstName = (GenSettings.Filter(rd["firstName"])),
                            dateOfDeath = (GenSettings.Filter(rd["DateOfBirth"])),
                            gender = (GenSettings.Filter(rd["Gender"])),
                            suburb = (GenSettings.Filter(rd["Suburb"])),
                            countryOfBirth = (GenSettings.Filter(rd["CountryOfBirth"])),
                            homeLanguage = (GenSettings.Filter(rd["HomeLanguage"])),
                            indiginousStatus = (GenSettings.Filter(rd["IndiginousStatus"])),
                            maritalStatus = (GenSettings.Filter(rd["MaritalStatus"])),
                            CALDStatus = (GenSettings.Filter(rd["CALDStatus"])),
                            remoteStatus = (GenSettings.Filter(rd["RemoteStatus"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("qccs/mds/functionlStatus/{personID}")]

        public async Task<IActionResult> GetfunctionlStatus(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT ExcludeFromStats, FP1_Housework, FP2_WalkingDistance, FP3_Shopping,
                    FP4_Medicine, FP5_Money, FP6_Walking, FP7_Bathing, FP8_Memory, FP9_Behaviour, FPA_Communication, FPA_Dressing,
                    FPA_Eating, FPA_Toileting, FPA_GetUp 
                    FROM Recipients 
                    LEFT JOIN ONI ON Recipients.UniqueID = ONI.PersonID WHERE UniqueID = @personID", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            excludeFromStats = (GenSettings.Filter(rd["ExcludeFromStats"])),
                            FP1_Housework = (GenSettings.Filter(rd["FP1_Housework"])),
                            FP2_WalkingDistance = (GenSettings.Filter(rd["FP2_WalkingDistance"])),
                            FP3_Shopping = (GenSettings.Filter(rd["FP3_Shopping"])),
                            FP4_Medicine = (GenSettings.Filter(rd["FP4_Medicine"])),
                            FP5_Money = (GenSettings.Filter(rd["FP5_Money"])),
                            FP6_Walking = (GenSettings.Filter(rd["FP6_Walking"])),
                            FP7_Bathing = (GenSettings.Filter(rd["FP7_Bathing"])),
                            FP8_Memory = (GenSettings.Filter(rd["FP8_Memory"])),
                            FP9_Behaviour = (GenSettings.Filter(rd["FP9_Behaviour"])),
                            CALDStatus = (GenSettings.Filter(rd["FPA_Communication"])),
                            FPA_Dressing = (GenSettings.Filter(rd["FPA_Dressing"])),
                            FPA_Eating = (GenSettings.Filter(rd["FPA_Eating"])),
                            FPA_Toileting = (GenSettings.Filter(rd["FPA_Toileting"])),
                            FPA_GetUp = (GenSettings.Filter(rd["FPA_GetUp"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("qccs/mds/carerData/{personID}")]

        public async Task<IActionResult> GetcarerData(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"DatasetCarer,CarerAvailability,CarerRelationship,CarerResidency,CARER_MORE_THAN_ONE 
                  FROM Recipients
                  LEFT JOIN ONI ON Recipients.UniqueID = ONI.PersonID 
                  WHERE UniqueID = @personID", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            DatasetCarer = (GenSettings.Filter(rd["DatasetCarer"])),
                            CarerAvailability = (GenSettings.Filter(rd["CarerAvailability"])),
                            CarerRelationship = (GenSettings.Filter(rd["CarerRelationship"])),
                            CarerResidency = (GenSettings.Filter(rd["CarerResidency"])),
                            CARER_MORE_THAN_ONE = (GenSettings.Filter(rd["CARER_MORE_THAN_ONE"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("dataset/ChildSafety/{personID}")]

        public async Task<IActionResult> GetChildSafety(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select CSReferToolOnFile,CSATCOnFile,CSCarer,CSPlacementType,CSStartPlacement ,CSEndPlacement
                  LEFT JOIN ONI ON Recipients.UniqueID = ONI.PersonID 
                  WHERE UniqueID = @personID", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            CSCarer = (GenSettings.Filter(rd["CSCarer"])),
                            CSPlacementType = (GenSettings.Filter(rd["CSPlacementType"])),
                            CSStartPlacement = (GenSettings.Filter(rd["CSStartPlacement"])),
                            CSEndPlacement = (GenSettings.Filter(rd["CSEndPlacement"])),
                            CSReferToolOnFile = (GenSettings.Filter(rd["CSReferToolOnFile"])),
                            CSATCOnFile = (GenSettings.Filter(rd["CSATCOnFile"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("is-hcp-cdc-programs")]
        public async Task<IActionResult> GetisHCPCDCProgram(string name)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [NAME], [User3] FROM HumanResourceTypes WHERE Group='PROGRAMS' AND Type = 'DOHA' AND UserYesNo1 = 1 AND [Name] = @name", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = (GenSettings.Filter(rd["Name"])).ToUpper(),
                            Level = (GenSettings.Filter(rd["User3"])).ToUpper()
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("quote/careplangoal/list/{personID}")]
        public async Task<IActionResult> GetCareplangoalList(string personID)
        {
            List<GoalsStrategiesDto> listGoalsAndStrategies = new List<GoalsStrategiesDto>();

            var humanresourcedata = await (from hr in _context.HumanResources
                                           where hr.PersonID == personID && hr.Type == "CAREPLANGOALS"
                                           select new
                                           {
                                               hr.RecordNumber,
                                               hr.Date1,
                                               hr.Date2,
                                               hr.DateInstalled,
                                               hr.State,
                                               hr.User1,
                                               hr.Notes,
                                               hr.User5,
                                               hr.Completed,
                                               hr.PersonID
                                           }).ToListAsync();

            humanresourcedata.ForEach(x =>
            {
                var strategies = (from hr in _context.HumanResources
                                  where hr.PersonID == Convert.ToString(x.RecordNumber) && hr.Type == "PLANSTRATEGY"
                                  select hr.Notes).ToList();

                listGoalsAndStrategies.Add(new GoalsStrategiesDto()
                {
                    Recordnumber = (int)x.RecordNumber,
                    Anticipated = x.Date1,
                    LastReviewed = x.Date2,
                    DateArchived = x.DateInstalled,
                    Percent = x.State,
                    Goal = x.User1,
                    Notes = x.Notes,
                    Level = x.User5,
                    Completed = x.Completed,
                    Strategies = strategies
                });

            });

            return Ok(listGoalsAndStrategies);

            //using(var conn = _context.Database.GetDbConnection() as SqlConnection)
            //using(SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber as recordnumber,Date1 as achievementDate, Date2 AS lastReviewed, DateInstalled as dateAchieved,[State] as [achievement], User1 AS Goal,Notes as notes,User5 as achievementIndex,Completed as completed FROM HumanResources WHERE PersonID = @personID AND [Type] = 'CAREPLANGOALS' ORDER BY Name",(SqlConnection) conn))
            //{
            //    cmd.Parameters.AddWithValue("@personID", personID);
            //    await conn.OpenAsync();

            //    using(SqlDataReader rd = await cmd.ExecuteReaderAsync())
            //    {
            //        List<dynamic> list = new List<dynamic>();
            //        while (await rd.ReadAsync())
            //    {
            //        list.Add(new
            //        {
            //            Recordnumber = GenSettings.Filter(rd["recordnumber"]), 
            //            achievementDate = GenSettings.Filter(rd["achievementDate"]),
            //            lastReviewed = GenSettings.Filter(rd["lastReviewed"]),
            //            dateAchieved =  GenSettings.Filter(rd["dateAchieved"]),
            //            achievement  = GenSettings.Filter(rd["achievement"]),
            //            Goal = GenSettings.Filter(rd["Goal"]),
            //            completed = GenSettings.Filter(rd["completed"]),
            //            notes = GenSettings.Filter(rd["notes"]), 
            //        });
            //    }
            //    return Ok(list);
            //        return Ok(list);
            //    }                    
            //}
        }
        [HttpGet("quote/careplangoal/strtegies/{personID}")]
        public async Task<IActionResult> GetStrategiesList(string personID)
        {

            var results = await (from hr in this._context.HumanResources
                                 where hr.PersonID == personID
                                 select new
                                 {
                                     recordnumber = hr.RecordNumber,
                                     strategy = hr.Notes,
                                     achieved = hr.Address1,
                                     state = hr.State,
                                     dsServices = hr.User1
                                 }).ToListAsync();

            return Ok(results);
        }

        [HttpGet("leave-balances/{uniqueId}")]
        public async Task<IActionResult> GetLeaveBalances(string uniqueId)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, Name As [Leave Type Code], Address1 AS [Leave Description], 
                    Date1 As [Pro Rata Date], Address2 AS [Balance Hours] FROM HumanResources 
                    WHERE PersonID = @uniqueId AND [Group] = 'LEAVEBALANCE' ORDER BY  RecordNumber DESC", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@uniqueId", uniqueId);
                    await conn.OpenAsync();

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd[0]),
                                LeaveTypeCode = GenSettings.Filter(rd[1]),
                                LeaveDescription = GenSettings.Filter(rd[2]),
                                ProRataDate = GenSettings.Filter(rd[3]),
                                BalanceHours = GenSettings.Filter(rd[4])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("leave-activity-codes")]
        public async Task<IActionResult> GetLeaveActivityCodes(ActivityCodesInput date)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                //MM/dd/YYYY
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Title FROM ItemTypes WHERE MinorGroup = 'LEAVE' AND ISNULL(EndDate, @startDate) >=  @startDate", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@startDate", date.StartDate);

                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                            list.Add(rd.GetString(0));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("leave-activities")]
        public async Task<IActionResult> GetLeaveTypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                //MM/dd/YYYY
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes WHERE ProcessClassification = 'INPUT' AND MinorGroup IN ('PAID LEAVE', 'UNPAID LEAVE')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                            list.Add(rd.GetString(0));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("leave-pay-types")]
        public async Task<IActionResult> GetLeavePayTypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                //MM/dd/YYYY
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND ISNULL(EndDate, '') = '' AND MinorGroup = 'LEAVE'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                            list.Add(rd.GetString(0));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("programs")]
        public async Task<IActionResult> GetPrograms(ActivityCodesInput date)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                //MM/dd/YYYY
                using (SqlCommand cmd = new SqlCommand(@"SELECT Name FROM HumanResourceTypes HR WHERE  [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= @date)  ORDER BY [Name]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@date", date.EndDate);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(rd.GetString(0));
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("roster-type")]
        public async Task<IActionResult> GetRosterType(ActivityCodesInput date)
        {
            List<string> list = new List<string>(){
                "01-BOOKINGS","02-DIRECT CARE","03-BROKERAGE","04-LEAVE/ABSENCE","05-TRAVEL TIME","06-ADMINISTRATION","07-ADMISSION SERVICES","08-SLEEPOVERS"
                ,"09-ALLOWANCES","10-TRANSPORT","11-CENTRE BASED ACTIVITIES","12-GROUP BASED ACTIVITIES","13-MEALS"
            };
            return Ok(list);
        }

        // [HttpPost("StaffOnLeave")]
        // public async Task<IActionResult> Post



        [HttpGet("GetReportCriteriaList")]
        public async Task<IActionResult> GetReportCriteriaList(ReportCriteria criteria)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"[GetReportCriteriaList]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ListType", criteria.ListType);
                    cmd.Parameters.AddWithValue("@IncludeInactive", criteria.IncludeInactive);

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    List<string> list = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd[0]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("paycode")]
        public async Task<IActionResult> GetPayCodes(ActivityCodesInput date)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    //MM/dd/YYYY
                    using (SqlCommand cmd = new SqlCommand(@"SELECT Recnum, Title FROM ItemTypes WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' AND ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= @date) ORDER BY TITLE", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@date", date.EndDate.HasValue ? (date.EndDate).Value.ToString("MM/dd/yyyy") : "");
                        await conn.OpenAsync();

                        List<dynamic> list = new List<dynamic>();
                        using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                            {
                                list.Add(new
                                {
                                    RecordNumber = GenSettings.Filter(rd[0]),
                                    Title = GenSettings.Filter(rd[1])
                                });
                            }
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpGet("programmergeCodes/{endDate}/{type}")]
        public async Task<IActionResult> GetProgrameMergeCodes(DateTime? endDate,string type)
        {
            if (string.IsNullOrEmpty(type))
            {
                return BadRequest("The 'type' parameter is required.");
            }


            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT Name FROM HumanResourceTypes HR WHERE [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= @endDate) AND [Type] = @type ORDER BY [Name]", conn))
                    {
                        cmd.Parameters.AddWithValue("@endDate", endDate.HasValue ? (object)endDate.Value : DBNull.Value);
                        cmd.Parameters.AddWithValue("@type", type);
                        await conn.OpenAsync();

                        List<dynamic> list = new List<dynamic>();
                        using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                        {
                            while (await rd.ReadAsync())
                            {
                                list.Add(new
                                {
                                    Name = rd["Name"].ToString()
                                });
                            }
                        }
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


    [HttpGet("mergeCodes/{parameter}/{endDate}")]
    public async Task<IActionResult> GetMergeCodes(int parameter,string endDate)
    {
        try
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                string sqlQuery = GetSqlQueryBasedOnParameter(parameter);

                using (SqlCommand cmd = new SqlCommand(sqlQuery, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@date",endDate);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Title = GenSettings.Filter(rd[0])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        catch (Exception ex)
        {
            return BadRequest(ex);
        }
    }

    private string GetSqlQueryBasedOnParameter(int parameter)
    {
        switch (parameter)
        {
            case 1:
                return @"SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >=  @date) AND RosterGroup = 'ADMISSION'  AND Status = 'ATTRIBUTABLE'  AND ProcessClassification = 'OUTPUT'  ORDER BY Title";
            case 2:
                return @"SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >=  @date) AND RosterGroup = 'ITEM'  AND Status = 'ATTRIBUTABLE'  AND ProcessClassification = 'OUTPUT'  ORDER BY Title";
            case 3:
                return @"SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >=  @date) AND RosterGroup = 'GROUPACTIVITY' AND Status = 'ATTRIBUTABLE' AND ProcessClassification = 'OUTPUT' ORDER BY Title";
            case 4:
                return @"SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >=  @date) AND RosterGroup = 'RECPTABSENCE' AND Status = 'ATTRIBUTABLE' AND ProcessClassification = 'EVENT' ORDER BY Title";
            case 5:
                return @"SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >=  @date) AND RosterGroup = 'ONEONONE'  AND Status = 'ATTRIBUTABLE'  AND ProcessClassification = 'OUTPUT'  ORDER BY Title";
            case 6:
                return @"SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >=  @date) AND RosterGroup = 'ADMINISTRATION'  AND Status = 'NONATTRIBUTABLE'  AND ProcessClassification = 'OUTPUT'  ORDER BY Title";
            default:
                throw new ArgumentException("Invalid parameter value");
        }
    }




        //Combined Checking of same package names with ClonePackage Stored Procedure
        [HttpPost("clonepackage")]
        public async Task<IActionResult> PostCheckClonePackag([FromBody] ProcedureClonePackage pckge)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    SqlCommand cmd1 = new SqlCommand(@"SELECT COUNT([Name]) AS NUMROWS FROM HumanResourceTypes WHERE Name = @name", (SqlConnection)conn);
                    cmd1.Parameters.AddWithValue("@name", pckge.NewPackageName);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd1.ExecuteReaderAsync();

                    if (await rd.ReadAsync())
                    {
                        int row = rd.GetInt32(0);
                        if (row > 0)
                            return Ok(false);
                    }
                    //return Ok(true);

                    SqlCommand cmd2 = new SqlCommand(@"[ClonePackage]", (SqlConnection)conn, transaction);
                    cmd2.Parameters.AddWithValue("@SourcePackage", pckge.SourcePackage);
                    cmd2.Parameters.AddWithValue("@NewPackageName", pckge.NewPackageName);
                    cmd2.Parameters.AddWithValue("@Type", pckge.NewPackageName);
                    cmd2.Parameters.AddWithValue("@Level", pckge.NewPackageName);

                    cmd2.CommandType = CommandType.StoredProcedure;

                    await cmd2.ExecuteNonQueryAsync();

                    transaction = conn.BeginTransaction();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        [HttpGet("admit-program/{personID}")]
        public async Task<IActionResult> GetAdmitPrograms(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [Program] AS Program FROM RecipientPrograms RP INNER JOIN HumanResourceTypes HRT ON HRT.[Name] = RP.[Program] WHERE [PersonID] = @personId AND ProgramStatus = 'REFERRAL' AND [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= GETDATE()) ORDER BY [Program]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personId", personID);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> list = new List<string>();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Program"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("admit-services/{program}")]
        public async Task<IActionResult> GetAdmitServices(string program)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] NOT IN ('ADMISSION', 'ADMINISTRATION') AND (ITM.EndDate Is Null OR ITM.EndDate >= '03-29-2019')) ORDER BY [Service Type]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@program", program);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> list = new List<string>();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Activity"]));
                    return Ok(list);
                }
            }
        }

        [HttpPost("suspension")]
        public async Task<IActionResult> PostSuspension([FromBody] CallAssessmentProcedure trans)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        [HttpGet("programs-recipient/{id}")]
        public async Task<IActionResult> GetRecipientPrograms(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                                SELECT DISTINCT RecordNumber,
                                @id              AS PersonID,
                                program,
                                Isnull(ap_basedon, 0)      AS Allowed,
                                Isnull(ap_costtype, '')    AS CostType,
                                Isnull(ap_perunit, '')     AS AP_PerUnit,
                                Isnull(ap_period, '')      AS AP_Period,
                                Isnull(expireusing, '')    AS ExpireUsing,
                                Isnull(alertstartdate, '') AS AlertStartDate,
                                '0'                        AS Balance,
                                Isnull(ap_redqty, 0)       AS [RedAmount],
                                Isnull(ap_orangeqty, 0)    AS [GreenAmount],
                                Isnull(ap_yellowqty, 0)    AS [YellowAmount]
                FROM   recipientprograms RP
                WHERE  personid = @id
                    AND Isnull(programstatus, 'INACTIVE') <> 'INACTIVE'
                    AND Isnull(program, '') <> ''
                ORDER  BY RP.program", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@id", id);

                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                PersonID = GenSettings.Filter(rd["PersonID"]),
                                Program = GenSettings.Filter(rd["Program"]),
                                Allowed = GenSettings.Filter(rd["Allowed"]),

                                CostType = GenSettings.Filter(rd["CostType"]),
                                APUnit = GenSettings.Filter(rd["AP_PerUnit"]),
                                APPeriod = GenSettings.Filter(rd["AP_Period"]),
                                ExpireUsing = GenSettings.Filter(rd["ExpireUsing"]),

                                AlertStartDate = GenSettings.Filter(rd["AlertStartDate"]),
                                Balance = GenSettings.Filter(rd["Balance"]),
                                RedAmount = GenSettings.Filter(rd["RedAmount"]),
                                GreenAmount = GenSettings.Filter(rd["GreenAmount"]),
                                YellowAmount = GenSettings.Filter(rd["YellowAmount"]),



                            });
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("programs/incident/recipient/list/{id}")]
        public async Task<IActionResult> GetProgramsIncidentRecipients(string id)
        {
            var _programs = await (from incidents in _context.RecipientPrograms
                                   where incidents.PersonID == id
                                   group incidents by new { Program = incidents.Program } into programs
                                   select new RecipientPrograms
                                   {
                                       Program = programs.FirstOrDefault().Program
                                   }).ToListAsync();

            var list = new List<string>();

            foreach (var program in _programs)
            {
                list.Add(program.Program);
            }

            return Ok(list);
        }

        [HttpGet("category-incident-note")]
        public async Task<IActionResult> GetCategoryIncidentNote()
        {
            var list = await (from dd in _context.DataDomains
                              where dd.Domain == "RECIMNTECAT"
                              orderby dd.Description descending
                              select dd.Description).ToListAsync();
            return Ok(list);
        }


 [HttpGet("recipient/list")]
        public async Task<IActionResult> getRecipientList()
        {
            string  sql ="Select UniqueId, AccountNo from Recipients WHERE AccountNo > '!z' AND ((AdmissionDate is not null) and (DischargeDate is null)) OR (([Type] = 'REFERRAL') AND (DischargeDate is null))  ORDER BY AccountNo";

               using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<dynamic> list = new List<dynamic>();
                        while (await rd.ReadAsync())
                        {
                            list.Add( new {
                               UniqueId = GenSettings.Filter(rd[0]),
                               AccountNo = GenSettings.Filter(rd[1])
                            });
                        }
                        return Ok(list);
                    }
                }
            }
            
           
          
        }

        [HttpGet("servicetype/incident/recipient/list")]
        public async Task<IActionResult> GetServiceTypeIncidentRecipient(InputUser user)
        {
            var servicetype = await (from prog in _context.ServiceOverview
                                     where (prog.PersonID == user.Id) && (prog.ServiceProgram == user.Program)
                                     group prog by new { Program = prog.ServiceType } into distinctPrograms
                                     select new ServiceOverview
                                     {
                                         ServiceType = distinctPrograms.FirstOrDefault().ServiceType
                                     }).ToListAsync();

            var list = new List<string>();

            foreach (var type in servicetype)
            {
                list.Add(type.ServiceType);
            }
            return Ok(list);
        }

        [HttpGet("activities")]
        public async Task<IActionResult> GetActivities()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Title FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND ((EndDate IS NULL) OR (EndDate > GETDATE())) ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

 

        // DONE TESTED
        [HttpPost("item")]
        public async Task<IActionResult> PostItem([FromBody] CallAssessmentProcedure trans)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand sp = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value ?? "");

                        await sp.ExecuteNonQueryAsync();
                    }

                    var roster = trans.Roster.Select(x => x).FirstOrDefault();

                    using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                    {
                        haccCmd.Parameters.AddWithValue("@referralType", roster.ServiceType);
                        using (var rd = await haccCmd.ExecuteReaderAsync())
                        {
                            if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                        }
                    }

                    using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                    {
                        billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                        using (var rd1 = await billCmd.ExecuteReaderAsync())
                        {
                            if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                        }
                    }
                    var programJoin = String.Join("|", trans.Roster.Select(x => x.Program));

                    using (SqlCommand sp = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@ClientCode", roster.ClientCode);
                        sp.Parameters.AddWithValue("@CarerCode", roster.CarerCode);
                        sp.Parameters.AddWithValue("@ServiceType", roster.ServiceType);
                        sp.Parameters.AddWithValue("@Program", programJoin);
                        sp.Parameters.AddWithValue("@Date", roster.GetDate());
                        sp.Parameters.AddWithValue("@Time", roster.Time);
                        sp.Parameters.AddWithValue("@Creator", roster.Creator);
                        sp.Parameters.AddWithValue("@Editer", roster.Editer);
                        sp.Parameters.AddWithValue("@BillUnit", roster.BillUnit);
                        sp.Parameters.AddWithValue("@AgencyDefinedGroup", roster.AgencyDefinedGroup);
                        sp.Parameters.AddWithValue("@ReferralCode", roster.ReferralCode);
                        sp.Parameters.AddWithValue("@TimePercent", roster.TimePercent);
                        sp.Parameters.AddWithValue("@Notes", roster.Notes);
                        sp.Parameters.AddWithValue("@Type ", roster.Type);
                        sp.Parameters.AddWithValue("@Duration", roster.Duration);
                        sp.Parameters.AddWithValue("@BlockNo", roster.BlockNo);
                        sp.Parameters.AddWithValue("@ReasonType", roster.ReasonType);
                        sp.Parameters.AddWithValue("@TabType", roster.TabType);
                        sp.Parameters.AddWithValue("@HaccType", hacc);
                        sp.Parameters.AddWithValue("@BillTo", billTo);

                        sp.Parameters.AddWithValue("@CostUnit", roster.CostUnit);
                        sp.Parameters.AddWithValue("@BillDesc", roster.BillDesc);
                        sp.Parameters.AddWithValue("@UnitBillRate", roster.UnitBillRate);
                        sp.Parameters.AddWithValue("@APIInvoiceDate", roster.GetAPIDate());
                        sp.Parameters.AddWithValue("@APIInvoiceNumber", roster.APIInvoiceNumber);

                        await sp.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        // DONE TESTED
        [HttpPost("discharge")]
        public async Task<IActionResult> PostDischarge([FromBody] CallAssessmentProcedure trans)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand sp = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value);

                        await sp.ExecuteNonQueryAsync();
                    }

                    foreach (var program in trans.Roster)
                    {

                        // UPDATE RecipientPrograms | INACTIVE
                        using (SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'INACTIVE' WHERE PROGRAM = @Program AND PERSONID = @PersonId", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // UPDATE ServiceOverview | INACTIVE
                        using (SqlCommand cmd = new SqlCommand(@"UPDATE ServiceOverview SET ServiceStatus = 'INACTIVE' WHERE SERVICEPROGRAM = @Program AND PERSONID = @PersonId", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            await cmd.ExecuteNonQueryAsync();
                        }
                        // UPDATE Discharge Date
                        using (SqlCommand cmd = new SqlCommand(@"UPDATE HumanResourceTypes SET EndDate = @date WHERE [NAME] = @Program ", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@date", program.officialDischargeDate);
                            await cmd.ExecuteNonQueryAsync();
                        }


                        // DELETE Roster
                        if (program.deleteUnApproved==true) {
                            using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE (([Roster].[Status] = 1 OR [Roster].[Type] IN (1, 4, 7, 14)) AND ([Client Code] = @Name) AND ([Program] = @Program) AND (Date > @Date))", (SqlConnection)conn, transaction))
                            {
                                cmd.Parameters.AddWithValue("@Program", program.Program);
                                cmd.Parameters.AddWithValue("@Name", program.ClientCode);
                                cmd.Parameters.AddWithValue("@Date", program.GetDate());
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                           if (program.deleteApproved==true) {
                            using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE [Roster].[Status] = 2 AND [Client Code] = @Name AND [Program] = @Program AND Date > @Date ", (SqlConnection)conn, transaction))
                            {
                                cmd.Parameters.AddWithValue("@Program", program.Program);
                                cmd.Parameters.AddWithValue("@Name", program.ClientCode);
                                cmd.Parameters.AddWithValue("@Date", program.deleteDate);
                                await cmd.ExecuteNonQueryAsync();
                            }
                         }
                          if (program.deleteMaster==true) {
                            using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE ([Client Code] = @Name) AND ([Program] = @Program) AND (Yearno < 1940)", (SqlConnection)conn, transaction))
                            {
                                cmd.Parameters.AddWithValue("@Program", program.Program);
                                cmd.Parameters.AddWithValue("@Name", program.ClientCode);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }

                        using (SqlCommand sp = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            sp.CommandType = CommandType.StoredProcedure;

                            sp.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp.Parameters.AddWithValue("@Program", program.Program);
                            sp.Parameters.AddWithValue("@Date", program.GetDate());
                            sp.Parameters.AddWithValue("@Time", program.Time);
                            sp.Parameters.AddWithValue("@Creator", program.Creator);
                            sp.Parameters.AddWithValue("@Editer", program.Editer);
                            sp.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp.Parameters.AddWithValue("@Notes", program.Notes);
                            sp.Parameters.AddWithValue("@Type ", program.Type);
                            sp.Parameters.AddWithValue("@Duration", program.Duration);
                            sp.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp.Parameters.AddWithValue("@TabType", program.TabType);
                            sp.Parameters.AddWithValue("@HaccType", hacc);
                            sp.Parameters.AddWithValue("@BillTo", billTo);
                            await sp.ExecuteNonQueryAsync();
                        }
                    }

                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        // DONE TESTED
        [HttpPost("waitlist")]
        public async Task<IActionResult> PostWaitList([FromBody] CallReferralOutProcedure trans)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand sp = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value);

                        await sp.ExecuteNonQueryAsync();
                    }

                    foreach (var program in trans.Roster)
                    {
                        using (SqlCommand cmd = new SqlCommand(@"UPDATE serviceoverview SET servicestatus = 'WAIT LIST' WHERE personid = @PersonId AND [service type] + ' ' + [serviceprogram] = @Program", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    
                    using(SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'ACTIVE' WHERE Program IN (SELECT ServiceProgram FROM ServiceOverview SO WHERE PersonID = @PersonId AND AND ServiceStatus IN ('ACTIVE'))  AND PersonID = @PersonId", (SqlConnection) conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            await cmd.ExecuteNonQueryAsync();                        
                        }


                        using(SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'WAITING LIST' WHERE Program IN (SELECT ServiceProgram FROM ServiceOverview SO WHERE PersonID = @PersonId AND ServiceStatus IN ('WAIT LIST')) AND PersonID = @PersonId", (SqlConnection) conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            await cmd.ExecuteNonQueryAsync();                        
                        }


                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }

                        using (SqlCommand sp = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            sp.CommandType = CommandType.StoredProcedure;

                            sp.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp.Parameters.AddWithValue("@Program", program.Program);
                            sp.Parameters.AddWithValue("@Date", program.GetDate());
                            sp.Parameters.AddWithValue("@Time", program.Time);
                            sp.Parameters.AddWithValue("@Creator", program.Creator);
                            sp.Parameters.AddWithValue("@Editer", program.Editer);
                            sp.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp.Parameters.AddWithValue("@Notes", program.Notes);
                            sp.Parameters.AddWithValue("@Type ", program.Type);
                            sp.Parameters.AddWithValue("@Duration", program.Duration);
                            sp.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp.Parameters.AddWithValue("@TabType", program.TabType);
                            sp.Parameters.AddWithValue("@HaccType", hacc);
                            sp.Parameters.AddWithValue("@BillTo", billTo);
                            await sp.ExecuteNonQueryAsync();
                        }
                    }

                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }
  [HttpPost("suspend")]
        public async Task<IActionResult> PostSuspend([FromBody] CallReferralOutProcedure procedure)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand sp = new SqlCommand(@"[spSuspendRecipientNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", procedure.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", procedure.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", procedure.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", procedure.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", procedure.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", procedure.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", procedure.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", procedure.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", procedure.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", procedure.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", procedure.Note.ReminderTo?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@CancellationCode", procedure.Note.CancellationCode);
                        sp.Parameters.AddWithValue("@Reasontype", procedure.Note.Reasontype);
                        sp.Parameters.AddWithValue("@StartDate", procedure.Note.StartDate);
                        sp.Parameters.AddWithValue("@EndDate", procedure.Note.EndDate);


                        await sp.ExecuteNonQueryAsync();
                    }
                  
                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", procedure.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }
                    foreach (var program in procedure.Roster)
                    {

                          using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }


                        using (SqlCommand sp = new SqlCommand(@"[spSuspendRecipient]", (SqlConnection)conn, transaction))
                        {
                            sp.CommandType = CommandType.StoredProcedure;

                            sp.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp.Parameters.AddWithValue("@Program", program.Program);
                            sp.Parameters.AddWithValue("@Date", program.GetDate());
                            sp.Parameters.AddWithValue("@Time", program.Time);
                            sp.Parameters.AddWithValue("@Creator", program.Creator);
                            sp.Parameters.AddWithValue("@Editer", program.Editer);
                            sp.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp.Parameters.AddWithValue("@Notes", program.Notes);
                            sp.Parameters.AddWithValue("@Type ", program.Type);
                            sp.Parameters.AddWithValue("@Duration", program.Duration);
                            sp.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp.Parameters.AddWithValue("@TabType", program.TabType);
                            sp.Parameters.AddWithValue("@HaccType", hacc);
                            sp.Parameters.AddWithValue("@BillTo", billTo);                              
                            sp.Parameters.AddWithValue("@StartDate", procedure.Note.StartDate);
                            sp.Parameters.AddWithValue("@EndDate", procedure.Note.EndDate);

                            await sp.ExecuteNonQueryAsync();
                        }

                    }
                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }
        [HttpPost("not-proceed")]
        public async Task<IActionResult> PostNotProceed([FromBody] CallReferralOutProcedure procedure)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand sp = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", procedure.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", procedure.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", procedure.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", procedure.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", procedure.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", procedure.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", procedure.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", procedure.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", procedure.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", procedure.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", procedure.Note.ReminderTo?? (object)DBNull.Value);

                        await sp.ExecuteNonQueryAsync();
                    }

                    foreach (var program in procedure.Roster)
                    {

                        using (SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'INACTIVE' WHERE PersonID = @PersonId AND [PROGRAM] = @Program", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@PersonId", procedure.Note.PersonId);
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand cmd = new SqlCommand(@"UPDATE ServiceOverview SET ServiceStatus = 'INACTIVE' WHERE PersonID = @PersonId AND [SERVICEPROGRAM] = @Program", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@PersonId", procedure.Note.PersonId);
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", procedure.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }

                        using (SqlCommand sp = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            sp.CommandType = CommandType.StoredProcedure;

                            sp.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp.Parameters.AddWithValue("@Program", program.Program);
                            sp.Parameters.AddWithValue("@Date", program.GetDate());
                            sp.Parameters.AddWithValue("@Time", program.Time);
                            sp.Parameters.AddWithValue("@Creator", program.Creator);
                            sp.Parameters.AddWithValue("@Editer", program.Editer);
                            sp.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp.Parameters.AddWithValue("@Notes", program.Notes);
                            sp.Parameters.AddWithValue("@Type ", program.Type);
                            sp.Parameters.AddWithValue("@Duration", program.Duration);
                            sp.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp.Parameters.AddWithValue("@TabType", program.TabType);
                            sp.Parameters.AddWithValue("@HaccType", hacc);
                            sp.Parameters.AddWithValue("@BillTo", billTo);
                            await sp.ExecuteNonQueryAsync();
                        }

                    }
                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        // DONE TESTED
        [HttpPost("administration")]
        public async Task<IActionResult> PostAdmin([FromBody] CallAssessmentProcedure trans)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;

                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;


                    using (SqlCommand sp2 = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp2.CommandType = CommandType.StoredProcedure;

                        sp2.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp2.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp2.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp2.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp2.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp2.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp2.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp2.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp2.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp2.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp2.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value);
                        await sp2.ExecuteNonQueryAsync();
                    }

                    foreach (var program in trans.Roster)
                    {

                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }


                        using (SqlCommand sp1 = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            sp1.CommandType = CommandType.StoredProcedure;
                            sp1.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp1.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp1.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp1.Parameters.AddWithValue("@Program", program.Program);
                            sp1.Parameters.AddWithValue("@Date", program.Date);
                            sp1.Parameters.AddWithValue("@Time", program.Time);
                            sp1.Parameters.AddWithValue("@Creator", program.Creator);
                            sp1.Parameters.AddWithValue("@Editer", program.Editer);
                            sp1.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp1.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp1.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp1.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp1.Parameters.AddWithValue("@Notes", program.Notes);
                            sp1.Parameters.AddWithValue("@Type ", program.Type);
                            sp1.Parameters.AddWithValue("@Duration", program.Duration);
                            sp1.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp1.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp1.Parameters.AddWithValue("@TabType", program.TabType);

                            sp1.Parameters.AddWithValue("@HaccType", hacc);
                            sp1.Parameters.AddWithValue("@BillTo", billTo);
                            await sp1.ExecuteNonQueryAsync();
                        }
                    }



                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        [HttpPost("reinstate")]
        public async Task<IActionResult> PostReinstate([FromBody] CallAssessmentProcedure trans)
        {

            // ISSUE with invalid column name in TRELLO
            if (trans==null) return Ok(false);
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                             using (SqlCommand sp = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value);

                        await sp.ExecuteNonQueryAsync();
                    }
                    foreach (var program in trans.Roster)
                    {
                            using(SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'ACTIVE' WHERE PROGRAM = @Program AND PERSONID = @PersonId", (SqlConnection) conn, transaction)){
                                cmd.Parameters.AddWithValue("@Program", program.Program);
                                cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                                await cmd.ExecuteNonQueryAsync();
                            }

                            using(SqlCommand cmd = new SqlCommand(@"UPDATE ServiceOverview SET ServiceStatus = 'ACTIVE' WHERE [SERVICEPROGRAM] = @Program AND [SERVICE TYPE] = @ServiceType AND PERSONID = @PersonId", (SqlConnection) conn, transaction)){
                                cmd.Parameters.AddWithValue("@Program", program.Program);
                                cmd.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                                cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                                await cmd.ExecuteNonQueryAsync();
                            }
                         
                         var sql = @"INSERT INTO ROSTER ([Client Code], [Carer Code], [Service Type], [Service Description], [Program], [Date], [Start Time], [Duration], [Unit Pay Rate], [Unit Bill Rate], [YearNo], [MonthNo], [Dayno], [BlockNo], [Notes], [CarerPhone], [UBDRef], [Type], [Status], [Anal], [Date Entered], [Date Last Mod], [Date Timesheet], [Date Invoice], [Date Payroll], [InvoiceNumber], [TimesheetNumber], [Batch#], [Transferred], [GroupActivity], [BillType], [BillTo], [CostUnit], [CostQty], [HACCType], [HACCID], [BillUnit], [BillQty], [TaxPercent], [TaxAmount], [Link], [Tagged], [UniqueID], [ROS_PANZTEL_UPDATED], [ROS_COPY_BATCH#], [ROS_PAY_BATCH], [NRCP_CARER_CODE], [NRCP_CONTACT_URGENCY], [NRCP_CONTACT_TIME], [NRCP_REFERRAL_SERVICE], [ServiceSetting], [ROS_DAELIBS_UPDATED], [InUse], [DischargeReasonType], [DATASETClient], [AIBatch], [NRCP_TypeOfAssitance], [NRCP_ReferralToServices], [NRCP_VolunteerServices], [NRCP_ReasonNoAssistance], [DMStat], [Creator], [Editer], [DateDeleted], [DeletedRecord], [COID], [BRID], [DPID], [NRCP_Episode], [Attendees], [OldStatus], [OldBill], [OldQty], [TimeLogID], [RecurrenceState], [BusyStatus], [ImportanceLevel], [LabelID], [PO_State], [RecurrencePatternID], [IsRecurrenceExceptionDeleted], [ServiceRating], [RExceptionStartTimeOrig], [RExceptionEndTimeOrig], [IsDocumentationOnly], [IsMeeting], [IsPrivate], [IsReminder], [ReminderMinutesBeforeStart], [Branch], [ShiftName], [HasServiceNotes], [Time2], [TA_EarlyStart], [TA_LateStart], [TA_EarlyGo], [TA_LateGo], [TA_TooShort], [TA_TooLong], [BillDesc], [VariationReason], [StaffPosition], [ClaimedStart], [ClaimedEnd], [ClaimedDate], [ClaimedBy], [KM], [StartKM], [EndKM], [APInvoiceDate], [APInvoiceNumber], [TA_EXCLUDEGEOLOCATION], [TA_EXCLUDEFROMAPPALERTS], [InterpreterPresent], [ExtraItems], [CloseDate], [NDIABatch], [ClientApproved], [OwnVehicle], [Recipient_Signature], [DMTransID], [DatasetQty], [charge], [TAMode], [TA_Multishift], [TravelBatch], [disable_shift_start_alarm], [disable_shift_end_alarm], [ShiftLocked], [Enable_Shift_Start_Alarm], [Enable_Shift_End_Alarm], [ReminderSent], [ServiceTypePortal], [EndDate], [PayTravelTI], [MedicareClaimable], [Ratio], [ScheduleID], [GovtPays], [UserPays], [ExcludeFromAppLogging], [groupFlag], [IsAllDayEvent]) 
                                    SELECT [Client Code], [Carer Code], [Service Type], [Service Description], [Program], [Date], [Start Time], [Duration], [Unit Pay Rate], [Unit Bill Rate], [YearNo], [MonthNo], [Dayno], [BlockNo], [Notes], [CarerPhone], [UBDRef], [Type], [Status], [Anal], [Date Entered], [Date Last Mod], [Date Timesheet], [Date Invoice], [Date Payroll], [InvoiceNumber], [TimesheetNumber], [Batch#], [Transferred], [GroupActivity], [BillType], [BillTo], [CostUnit], [CostQty], [HACCType], [HACCID], [BillUnit], [BillQty], [TaxPercent], [TaxAmount], [Link], [Tagged], [UniqueID], [ROS_PANZTEL_UPDATED], [ROS_COPY_BATCH#], [ROS_PAY_BATCH], [NRCP_CARER_CODE], [NRCP_CONTACT_URGENCY], [NRCP_CONTACT_TIME], [NRCP_REFERRAL_SERVICE], [ServiceSetting], [ROS_DAELIBS_UPDATED], [InUse], [DischargeReasonType], [DATASETClient], [AIBatch], [NRCP_TypeOfAssitance], [NRCP_ReferralToServices], [NRCP_VolunteerServices], [NRCP_ReasonNoAssistance], [DMStat], [Creator], [Editer], [DateDeleted], [DeletedRecord], [COID], [BRID], [DPID], [NRCP_Episode], [Attendees], [OldStatus], [OldBill], [OldQty], [TimeLogID], [RecurrenceState], [BusyStatus], [ImportanceLevel], [LabelID], [PO_State], [RecurrencePatternID], [IsRecurrenceExceptionDeleted], [ServiceRating], [RExceptionStartTimeOrig], [RExceptionEndTimeOrig], [IsDocumentationOnly], [IsMeeting], [IsPrivate], [IsReminder], [ReminderMinutesBeforeStart], [Branch], [ShiftName], [HasServiceNotes], [Time2], [TA_EarlyStart], [TA_LateStart], [TA_EarlyGo], [TA_LateGo], [TA_TooShort], [TA_TooLong], [BillDesc], [VariationReason], [StaffPosition], [ClaimedStart], [ClaimedEnd], [ClaimedDate], [ClaimedBy], [KM], [StartKM], [EndKM], [APInvoiceDate], [APInvoiceNumber], [TA_EXCLUDEGEOLOCATION], [TA_EXCLUDEFROMAPPALERTS], [InterpreterPresent], [ExtraItems], [CloseDate], [NDIABatch], [ClientApproved], [OwnVehicle], [Recipient_Signature], [DMTransID], [DatasetQty], [charge], [TAMode], [TA_Multishift], [TravelBatch], [disable_shift_start_alarm], [disable_shift_end_alarm], [ShiftLocked], [Enable_Shift_Start_Alarm], [Enable_Shift_End_Alarm], [ReminderSent], [ServiceTypePortal], [EndDate], [PayTravelTI], [MedicareClaimable], [Ratio], [ScheduleID], [GovtPays], [UserPays], [ExcludeFromAppLogging], [groupFlag], [IsAllDayEvent] 
                                    FROM Roster_Hold WHERE [Yearno] < 1920 AND [Client Code] = @ClientCode AND [Service Type] = @ServiceType AND [Program] = @Program;
                                    DELETE FROM ROSTER_Hold WHERE [Yearno] < 1920 AND [Client Code] = @ClientCode AND [Service Type] =  @ServiceType AND [Program] = @Program;
                                    UPDATE RecipientPrograms SET ProgramStatus = 'ACTIVE' WHERE PROGRAM = @Program AND PERSONID = @PersonId;
                                    UPDATE ServiceOverview SET ServiceStatus = 'ACTIVE' WHERE [SERVICEPROGRAM] = @Program AND [SERVICE TYPE] = @ServiceType AND PERSONID =  @PersonId;
                                ";
                       // Console.WriteLine(sql);
                        using (SqlCommand sp1 = new SqlCommand(@sql, (SqlConnection)conn, transaction))
                        {
                            sp1.CommandType = CommandType.Text;
                            sp1.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp1.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp1.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp1.Parameters.AddWithValue("@Program", program.Program);
                             sp1.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            // sp1.Parameters.AddWithValue("@Date", program.Date);
                            // sp1.Parameters.AddWithValue("@Time", program.Time);
                            // sp1.Parameters.AddWithValue("@Creator", program.Creator);
                            // sp1.Parameters.AddWithValue("@Editer", program.Editer);
                            // sp1.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            // sp1.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            // sp1.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            // sp1.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            // sp1.Parameters.AddWithValue("@Notes", program.Notes);
                            // sp1.Parameters.AddWithValue("@Type ", program.Type);
                            // sp1.Parameters.AddWithValue("@Duration", program.Duration);
                            // sp1.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            // sp1.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            // sp1.Parameters.AddWithValue("@TabType", program.TabType);
                           
                           // sp1.Parameters.AddWithValue("@HaccType", hacc);
                           // sp1.Parameters.AddWithValue("@BillTo", billTo);
                            await sp1.ExecuteNonQueryAsync();
                        }
                    }
                    
                    
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }


        [HttpPost("death")]
        public async Task<IActionResult> PostDeath([FromBody] CallDeceaseProcedure trans)
        {
            //return Ok(trans);

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                // init string
                string hacc = string.Empty, billTo = string.Empty;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand cmd = new SqlCommand(@"UPDATE Recipients SET DateOfDeath = @DateOfDeath WHERE UniqueID = @personId", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@DateOfDeath", trans.DateOfDeath);
                        cmd.Parameters.AddWithValue("@personId", trans.Note.PersonId);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    // Note is inserted ONCE ONLY - VARIOUS IF MANY
                    using (SqlCommand sp = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp.CommandType = CommandType.StoredProcedure;

                        sp.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value);

                        await sp.ExecuteNonQueryAsync();
                    }

                    foreach (var program in trans.Roster)
                    {


                        // UPDATE RecipientPrograms | INACTIVE
                        using (SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'INACTIVE' WHERE PROGRAM = @Program AND PERSONID = @PersonId", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // UPDATE ServiceOverview | INACTIVE
                        using (SqlCommand cmd = new SqlCommand(@"UPDATE ServiceOverview SET ServiceStatus = 'INACTIVE' WHERE SERVICEPROGRAM = @Program AND PERSONID = @PersonId", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        // DELETE Roster
                        using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE (([Roster].[Status] = 1 OR [Roster].[Type] IN (1, 4, 7, 14)) AND ([Client Code] = @Name) AND ([Program] = @Program) AND (Date > @Date))", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@Name", program.ClientCode);
                            cmd.Parameters.AddWithValue("@Date", program.GetDate());
                            await cmd.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE [Roster].[Status] = 2 AND [Client Code] = @Name AND [Program] = @Program AND Date > @Date ", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@Name", program.ClientCode);
                            cmd.Parameters.AddWithValue("@Date", program.GetDate());
                            await cmd.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE ([Client Code] = @Name) AND ([Program] = @Program) AND (Yearno < 1940)", (SqlConnection)conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@Program", program.Program);
                            cmd.Parameters.AddWithValue("@Name", program.ClientCode);
                            await cmd.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }

                        using (SqlCommand sp = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            sp.CommandType = CommandType.StoredProcedure;

                            sp.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp.Parameters.AddWithValue("@Program", program.Program);
                            sp.Parameters.AddWithValue("@Date", program.GetDate());
                            sp.Parameters.AddWithValue("@Time", program.Time);
                            sp.Parameters.AddWithValue("@Creator", program.Creator);
                            sp.Parameters.AddWithValue("@Editer", program.Editer);
                            sp.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp.Parameters.AddWithValue("@Notes", program.Notes);
                            sp.Parameters.AddWithValue("@Type ", program.Type);
                            sp.Parameters.AddWithValue("@Duration", program.Duration);
                            sp.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp.Parameters.AddWithValue("@TabType", program.TabType);
                            sp.Parameters.AddWithValue("@HaccType", hacc);
                            sp.Parameters.AddWithValue("@BillTo", billTo);
                            await sp.ExecuteNonQueryAsync();
                        }

                    }

                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }
// De-activate
        [HttpPost("recipient/deactivate")]
        public async Task<IActionResult> deactivateRecipient([FromBody] Deactivate input)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                  
                 if (input.deleteRecipientRostersDateRange){
                    using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE ([Roster].[Status] = 1) AND ([Client Code] = @code) AND (Date BETWEEN @startDate AND @endDate)", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@code", input.code);
                        cmd.Parameters.AddWithValue("@startDate", input.startDate);
                        cmd.Parameters.AddWithValue("@endDate", input.endDate);
                        await cmd.ExecuteNonQueryAsync();
                        
                    }
                 }
                
                if (input.deleteRecipientMasterRosters){
                    using (SqlCommand cmd = new SqlCommand(@"DELETE FROM ROSTER WHERE ([Client Code] = @code) AND (Yearno < 1940)", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@code", input.code);                      
                        await cmd.ExecuteNonQueryAsync();
                        
                    }
                 }
                   
                if (input.deleteRecipient){
                    using (SqlCommand cmd = new SqlCommand(@"UPDATE Recipients SET AdmissionDate = Null, AdmittedBy = Null, DischargeDate = getDate(), DischargedBy = @user, ExcludeFromRosterCopy = 0 WHERE AccountNo = @code", (SqlConnection)conn, transaction))
                    {
                          cmd.Parameters.AddWithValue("@code", input.code);
                          cmd.Parameters.AddWithValue("@user", input.user);                      
                        await cmd.ExecuteNonQueryAsync();
                        
                    }
                    using (SqlCommand cmd = new SqlCommand(@"Delete From UserInfo WHERE [StaffCode] = @code", (SqlConnection)conn, transaction))
                    {
                        cmd.Parameters.AddWithValue("@code", input.code);                       
                        await cmd.ExecuteNonQueryAsync();
                        
                    }
                 }
                   
                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }
        // Delete Recipient
        [HttpPost("recipient/delete")]
        public async Task<IActionResult> deleteRecipient([FromBody] Deactivate input)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand cmd = new SqlCommand(@"deleteRecipient", (SqlConnection)conn, transaction))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AccountNo", input.code);                        
                        cmd.Parameters.AddWithValue("@winUser", input.user);
                        cmd.Parameters.AddWithValue("@user", input.user);
                        await cmd.ExecuteNonQueryAsync();
                        
                    }
                                             
                   
                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }
         // Delete Recipient
        [HttpPost("recipient/update/code")]
        public async Task<IActionResult> UpdateRecipientCode([FromBody] Codes input)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    using (SqlCommand cmd = new SqlCommand(@"UpdateRecipientCode", (SqlConnection)conn, transaction))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OldCode", input.OldCode);                        
                        cmd.Parameters.AddWithValue("@NewCode", input.NewCode);
                        
                        await cmd.ExecuteNonQueryAsync();
                        
                    }
                                             
                   
                    transaction.Commit();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }
        [HttpPost("admission-accept-quote")]
        public async Task<IActionResult> PostAdmitAcceptQupte([FromBody] AdmitProcedure trans)
        {
            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    SqlTransaction transaction = null;
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand sp2 = new SqlCommand(@"[AcceptQuote]", (SqlConnection)conn, transaction))
                    {
                        sp2.CommandType = CommandType.StoredProcedure;

                        sp2.Parameters.AddWithValue("@DocRecNo", trans.DocId);
                        sp2.Parameters.AddWithValue("@PersonID", trans.NoteDetails.PersonId);
                        sp2.Parameters.AddWithValue("@AdmissionDate", trans.NoteDetails.DetailDate.ToString("yyyy/MM/dd"));
                        sp2.Parameters.AddWithValue("@User", trans.Editer);
                        sp2.Parameters.AddWithValue("@RecipientStatus", "REFERRAL");
                        sp2.Parameters.AddWithValue("@AdmissionTime", trans.Time);
                        sp2.Parameters.AddWithValue("@AdmissionDuration", trans.TimeSpent);
                        sp2.Parameters.AddWithValue("@AdmissionActivity", trans.AdmissionType);
                        sp2.Parameters.AddWithValue("@DeleteExistingService", 0);

                        sp2.Parameters.AddWithValue("@UpdateMasterFees", 1);
                        sp2.Parameters.AddWithValue("@UpdateMasterPrices", 1);
                        sp2.Parameters.AddWithValue("@UpdateRosteredFees", 1);
                        sp2.Parameters.AddWithValue("@UpdateRosteredPrices", 1);
                        sp2.Parameters.AddWithValue("@DeleteMasterActivities", 1);
                        sp2.Parameters.AddWithValue("@DeleteCurrentActivities", 1);
                        sp2.Parameters.AddWithValue("@CreateMasterBookings", 1);
                        sp2.Parameters.AddWithValue("@CreateCurrentBookings", 1);
                        sp2.Parameters.AddWithValue("@StartRosters", trans.StartDate.ToString("yyyy/MM/dd"));
                        sp2.Parameters.AddWithValue("@EndRosters", trans.EndDate.ToString("yyyy/MM/dd"));
                        sp2.Parameters.AddWithValue("@AutoPopulatePackageStartBalance", 1);

                        await sp2.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                    return Ok(trans);
                }
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    DocRecNo = trans.DocId,
                    PersonID = trans.NoteDetails.PersonId,
                    AdmissionDate = trans.NoteDetails.DetailDate.ToString("yyyy/MM/dd"),
                    User = trans.Editer,
                    RecipientStatus = trans.TabType,
                    AdmissionTime = trans.Time,
                    AdmissionDuration = trans.TimeSpent,
                    AdmissionActivity = trans.AdmissionType,

                    DeleteExistingService = 0,
                    UpdateMasterFees = 1,
                    UpdateMasterPrices = 1,
                    UpdateRosteredFees = 1,
                    UpdateRosteredPrices = 1,
                    DeleteMasterActivities = 1,
                    DeleteCurrentActivities = 1,
                    CreateMasterBookings = 1,
                    CreateCurrentBookings = 1,
                    StartRosters = trans.StartDate.ToString("yyyy/MM/dd"),
                    EndRosters = trans.EndDate.ToString("yyyy/MM/dd"),
                    AutoPopulatePackageStartBalance = 1
                });
            }

        }

        [HttpGet("last-roster-publish-date")]
        public async Task<IActionResult> GetRosterPublishDates()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select TOP 1 NewDate 
                            FROM (Select RSC_Cycle, right(RSC_DestinationEnd, 4) + '/' + substring(rsc_destinationend, 4,2) + '/' + left(rsc_destinationend, 2) as NewDate 
                            from rostercopy ) t where isnull(newdate, '') <> '' and convert(varchar, RSC_Cycle) IN ('ALL', 'CYCLE 1') ORDER BY NewDate desc", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    string date = string.Empty;

                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        List<string> list = new List<string>();
                        if (await rd.ReadAsync())
                        {
                            date = rd.GetString("NewDate");
                        }
                        return Ok(new
                        {
                            Date = DateTime.ParseExact(date, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture)
                        });
                    }
                }
            }
        }

        [HttpGet("program-status-program")]
        public async Task<IActionResult> GetProgramStatus(ProgramStatusDto psd)
        {

            var program = await (from qt in _context.Qte_Hdr
                                 join hr in _context.HumanResourceTypes on qt.ProgramId equals hr.RecordNumber
                                 where qt.RecordNumber == psd.RecordNumber
                                 select hr.Name).FirstOrDefaultAsync();

            var status = await (from rp in _context.RecipientPrograms where rp.PersonID == psd.PersonId && rp.Program == program select rp.ProgramStatus).FirstOrDefaultAsync();

            return Ok(status);
        }

        [HttpPost("admission")]
        public async Task<IActionResult> PostAdmit([FromBody] CallAdmissionProcedure trans)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;

                // -1 to be caught in try catch statement
                int recordNo = -1;

                // init string
                string hacc = string.Empty, billTo = string.Empty;

                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand sp2 = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp2.CommandType = CommandType.StoredProcedure;
                        sp2.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp2.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp2.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp2.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp2.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp2.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp2.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp2.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp2.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp2.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate);
                        sp2.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo);

                        await sp2.ExecuteNonQueryAsync();
                    }

                    // using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber FROM RecipientPrograms WHERE PersonID = @personId AND Program = @program", (SqlConnection)conn, transaction))
                    // {
                    //     cmd.Parameters.AddWithValue("@personId", trans.Note.PersonId);
                    //     cmd.Parameters.AddWithValue("@program", trans.Roster.Program);

                    //     using (var rd = await cmd.ExecuteReaderAsync())
                    //     {
                    //         if (await rd.ReadAsync()) recordNo = GenSettings.Filter(rd["RecordNumber"]);
                    //     }
                    // }

                    // // UPDATE RecipientPrograms | INSERT ServiceOverView | UPDATE Recipients
                    // using (SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET [ProgramStatus] = 'ACTIVE' ,[ExpireUsing] = '' ,[Quantity] = '' ,[ItemUnit] = '' ,[PerUnit] = '' ,[TimeUnit] = '' ,[Period] = '' ,[AP_BasedOn] = '' ,[AP_CostType] = '' ,[AP_Period] = '' ,[AP_PerUnit] = '' ,[AP_YellowQty] = '' ,[AP_OrangeQty] = '' ,[AP_RedQty] = '' WHERE RecordNumber = @recordNo", (SqlConnection)conn, transaction))
                    // {
                    //     cmd.Parameters.AddWithValue("@recordNo", recordNo);
                    //     await cmd.ExecuteNonQueryAsync();
                    // }

                    // using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                    // {
                    //     haccCmd.Parameters.AddWithValue("@referralType", trans.Roster.AdmissionType);
                    //     using (var rd = await haccCmd.ExecuteReaderAsync())
                    //     {
                    //         if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                    //     }
                    // }

                    // using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                    // {
                    //     billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                    //     using (var rd1 = await billCmd.ExecuteReaderAsync())
                    //     {
                    //         if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                    //     }
                    // }

                    // using (SqlCommand sp1 = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                    // {
                    //     sp1.CommandType = CommandType.StoredProcedure;
                    //     sp1.Parameters.AddWithValue("@ClientCode", trans.Roster.ClientCode);
                    //     sp1.Parameters.AddWithValue("@CarerCode", trans.Roster.CarerCode);
                    //     sp1.Parameters.AddWithValue("@ServiceType", trans.Roster.AdmissionType);
                    //     sp1.Parameters.AddWithValue("@Program", trans.Roster.Program);
                    //     sp1.Parameters.AddWithValue("@Date", trans.Roster.Date);
                    //     sp1.Parameters.AddWithValue("@Time", trans.Roster.Time);
                    //     sp1.Parameters.AddWithValue("@Creator", trans.Roster.Creator);
                    //     sp1.Parameters.AddWithValue("@Editer", trans.Roster.Editer);
                    //     sp1.Parameters.AddWithValue("@BillUnit", trans.Roster.BillUnit);
                    //     sp1.Parameters.AddWithValue("@AgencyDefinedGroup", trans.Roster.AgencyDefinedGroup);
                    //     sp1.Parameters.AddWithValue("@ReferralCode", trans.Roster.ReferralCode);
                    //     sp1.Parameters.AddWithValue("@TimePercent", trans.Roster.TimePercentage);
                    //     sp1.Parameters.AddWithValue("@Notes", trans.Roster.Notes);
                    //     sp1.Parameters.AddWithValue("@Type ", trans.Roster.Type);
                    //     sp1.Parameters.AddWithValue("@Duration", trans.Roster.Duration);
                    //     sp1.Parameters.AddWithValue("@BlockNo", trans.Roster.BlockNo);
                    //     sp1.Parameters.AddWithValue("@ReasonType", trans.Roster.ReasonType);
                    //     sp1.Parameters.AddWithValue("@TabType", trans.Roster.TabType);
                    //     sp1.Parameters.AddWithValue("@HaccType", hacc);
                    //     sp1.Parameters.AddWithValue("@BillTo", billTo);
                    //     await sp1.ExecuteNonQueryAsync();
                    // }

                    foreach(var program in trans.Roster){
                    
                        using(SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber FROM RecipientPrograms WHERE PersonID = @personId AND Program = @program", (SqlConnection) conn, transaction)){
                            cmd.Parameters.AddWithValue("@personId", trans.Note.PersonId);
                            cmd.Parameters.AddWithValue("@program", program.Program);
                    
                            using(var rd = await cmd.ExecuteReaderAsync()){
                            if(await rd.ReadAsync())    recordNo = GenSettings.Filter(rd["RecordNumber"]);
                            }
                        }
                    
                        // UPDATE RecipientPrograms | INSERT ServiceOverView | UPDATE Recipients
                        using(SqlCommand cmd = new SqlCommand(@"UPDATE RecipientPrograms SET [ProgramStatus] = 'ACTIVE' ,[ExpireUsing] = '' ,[Quantity] = '' ,[ItemUnit] = '' ,[PerUnit] = '' ,[TimeUnit] = '' ,[Period] = '' ,[AP_BasedOn] = '' ,[AP_CostType] = '' ,[AP_Period] = '' ,[AP_PerUnit] = '' ,[AP_YellowQty] = '' ,[AP_OrangeQty] = '' ,[AP_RedQty] = '' WHERE RecordNumber = @recordNo",(SqlConnection) conn, transaction))
                        {
                            cmd.Parameters.AddWithValue("@recordNo", recordNo);
                            await cmd.ExecuteNonQueryAsync();
                        }
                    
                        foreach(var service in program.Services){
                            using(SqlCommand cmd = new SqlCommand(@"INSERT INTO ServiceOverview([Service Type], PersonID, [ServiceProgram], [ServiceStatus]) VALUES (@service, @personId, @program, 'ACTIVE')",(SqlConnection) conn, transaction)){
                                cmd.Parameters.AddWithValue("@service", service.Name);
                                cmd.Parameters.AddWithValue("@personId", trans.Note.PersonId);
                                cmd.Parameters.AddWithValue("@program", program.Program);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                    
                        using(SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection) conn, transaction)){
                            haccCmd.Parameters.AddWithValue("@referralType", program.AdmissionType);
                            using(var rd = await haccCmd.ExecuteReaderAsync()){
                                if(await rd.ReadAsync())    hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }
                    
                        using(SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient",(SqlConnection) conn, transaction)){
                            billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                            using(var rd1 = await billCmd.ExecuteReaderAsync()){
                                if(await rd1.ReadAsync())   billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }
                    
                        using(SqlCommand sp1 = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection) conn, transaction)){                        
                            sp1.CommandType = CommandType.StoredProcedure;
                            sp1.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp1.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp1.Parameters.AddWithValue("@ServiceType", program.AdmissionType);                    
                            sp1.Parameters.AddWithValue("@Program", program.Program);
                            sp1.Parameters.AddWithValue("@Date", program.Date);
                            sp1.Parameters.AddWithValue("@Time", program.Time);
                            sp1.Parameters.AddWithValue("@Creator", program.Creator);
                            sp1.Parameters.AddWithValue("@Editer", program.Editer);
                            sp1.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp1.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp1.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp1.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp1.Parameters.AddWithValue("@Notes", program.Notes);
                            sp1.Parameters.AddWithValue("@Type ", program.Type);
                            sp1.Parameters.AddWithValue("@Duration", program.Duration);   
                            sp1.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp1.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp1.Parameters.AddWithValue("@TabType", program.TabType);
                            sp1.Parameters.AddWithValue("@HaccType", hacc);
                            sp1.Parameters.AddWithValue("@BillTo", billTo);
                            
                            await sp1.ExecuteNonQueryAsync();
                        }
                    }

                    //UPDATE RecipientPrograms Set StartDate = @date , AlertStartDate = (CASE WHEN ISNULL(AP_CostType, '') <> '' AND  ISNULL(AP_PerUnit, '') = 'REC' THEN @date END) WHERE Program IN (@program) AND PersonID = @personId
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@personId", trans.Note.PersonId);
                        cmd.Parameters.AddWithValue("@date", trans.Note.DetailDate);    // 03-29-2019

                        // int i = 0;
                        // var programParam = new List<string>();
                        //
                        // foreach(var program in trans.Programs.Select(x => x.Program)){
                        //     var param = "@program" + i.ToString();
                        //     cmd.Parameters.AddWithValue(param, program.ToString());
                        //     programParam.Add(param);
                        //     i++;
                        // }

                        cmd.CommandText = String.Format("UPDATE RecipientPrograms Set StartDate = @date , AlertStartDate = (CASE WHEN ISNULL(AP_CostType, '') <> '' AND  ISNULL(AP_PerUnit, '') = 'REC' THEN @date END) WHERE Program IN ('{0}') AND PersonID = @personId", trans.Program);

                        cmd.Connection = conn;
                        cmd.Transaction = transaction;
                        await cmd.ExecuteNonQueryAsync();
                    }

                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        // DONE TESTED
        [HttpPost("assessment")]
        public async Task<IActionResult> PostAssessment([FromBody] CallAssessmentProcedure trans)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                string hacc = null;
                string billTo = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand sp2 = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        sp2.CommandType = CommandType.StoredProcedure;

                        sp2.Parameters.AddWithValue("@PersonId", trans.Note.PersonId);
                        sp2.Parameters.AddWithValue("@Program", trans.Note.Program);
                        sp2.Parameters.AddWithValue("@DetailDate", trans.Note.DetailDate);
                        sp2.Parameters.AddWithValue("@ExtraDetail1", trans.Note.ExtraDetail1);
                        sp2.Parameters.AddWithValue("@ExtraDetail2", trans.Note.ExtraDetail2);
                        sp2.Parameters.AddWithValue("@WhoCode", trans.Note.WhoCode);
                        sp2.Parameters.AddWithValue("@PublishToApp", trans.Note.PublishToApp);
                        sp2.Parameters.AddWithValue("@Creator", trans.Note.Creator);
                        sp2.Parameters.AddWithValue("@Note", trans.Note.Note);
                        sp2.Parameters.AddWithValue("@AlarmDate", trans.Note.AlarmDate?? (object)DBNull.Value);
                        sp2.Parameters.AddWithValue("@ReminderTo", trans.Note.ReminderTo?? (object)DBNull.Value);

                        await sp2.ExecuteNonQueryAsync();
                    }

                    foreach (var program in trans.Roster)
                    {
                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }


                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", trans.Note.WhoCode);
                            using (var rd = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) billTo = GenSettings.Filter(rd["BillTo"]);
                            }
                        }

                        using (SqlCommand sp1 = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            sp1.CommandType = CommandType.StoredProcedure;

                            sp1.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp1.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp1.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp1.Parameters.AddWithValue("@Date", program.Date);
                            sp1.Parameters.AddWithValue("@Time", program.Time);
                            sp1.Parameters.AddWithValue("@Creator", program.Creator);
                            sp1.Parameters.AddWithValue("@Editer", program.Editer);
                            sp1.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp1.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp1.Parameters.AddWithValue("@ReferralCode", program.ReferralCode);
                            sp1.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp1.Parameters.AddWithValue("@Notes", program.Notes);
                            sp1.Parameters.AddWithValue("@Type ", program.Type);
                            sp1.Parameters.AddWithValue("@Duration", program.Duration);
                            sp1.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp1.Parameters.AddWithValue("@ReasonType", program.ReasonType);
                            sp1.Parameters.AddWithValue("@TabType", program.TabType);
                            sp1.Parameters.AddWithValue("@Program", program.Program);

                            sp1.Parameters.AddWithValue("@HaccType", hacc);
                            sp1.Parameters.AddWithValue("@BillTo", billTo);

                            await sp1.ExecuteNonQueryAsync();
                        }
                    }

                    transaction.Commit();
                    return Ok(true);

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        // DONE TESTED
        [HttpPost("referral-out")]
        public async Task<IActionResult> PostReferralOut([FromBody] CallReferralOutProcedure procedure)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                string hacc = string.Empty, billTo = string.Empty;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();

                    using (SqlCommand cmd3 = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        cmd3.CommandType = CommandType.StoredProcedure;
                        // Input Parameters for SP3
                        cmd3.Parameters.AddWithValue("@PersonId", procedure.Note.PersonId);
                        cmd3.Parameters.AddWithValue("@Program", procedure.Note.Program);
                        cmd3.Parameters.AddWithValue("@DetailDate", procedure.Note.DetailDate);
                        cmd3.Parameters.AddWithValue("@ExtraDetail1", procedure.Note.ExtraDetail1);
                        cmd3.Parameters.AddWithValue("@ExtraDetail2", procedure.Note.ExtraDetail2);
                        cmd3.Parameters.AddWithValue("@WhoCode", procedure.Note.WhoCode);
                        cmd3.Parameters.AddWithValue("@PublishToApp", procedure.Note.PublishToApp);
                        cmd3.Parameters.AddWithValue("@Creator", procedure.Note.Creator);
                        cmd3.Parameters.AddWithValue("@Note", procedure.Note.Note);
                        cmd3.Parameters.AddWithValue("@AlarmDate", procedure.Note.AlarmDate?? (object)DBNull.Value);
                        cmd3.Parameters.AddWithValue("@ReminderTo", procedure.Note.ReminderTo?? (object)DBNull.Value);
                        await cmd3.ExecuteNonQueryAsync();
                    }

                    foreach (var program in procedure.Roster)
                    {

                        using (SqlCommand updateRecipientProgram = new SqlCommand(@"UPDATE RecipientPrograms SET ProgramStatus = 'INACTIVE' WHERE PersonID = @uniqueId AND [PROGRAM] = @program", (SqlConnection)conn, transaction))
                        {
                            updateRecipientProgram.Parameters.AddWithValue("@uniqueId", procedure.Note.PersonId);
                            updateRecipientProgram.Parameters.AddWithValue("@program", program.Program);
                            await updateRecipientProgram.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand updateServiceOverview = new SqlCommand(@"UPDATE ServiceOverview SET ServiceStatus = 'INACTIVE' WHERE PersonID = @uniqueId AND [SERVICEPROGRAM] = @program", (SqlConnection)conn, transaction))
                        {
                            updateServiceOverview.Parameters.AddWithValue("@uniqueId", procedure.Note.PersonId);
                            updateServiceOverview.Parameters.AddWithValue("@program", program.Program);
                            await updateServiceOverview.ExecuteNonQueryAsync();
                        }

                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", procedure.Note.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }

                        using (SqlCommand sp1 = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {

                            sp1.CommandType = CommandType.StoredProcedure;
                            sp1.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            sp1.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            sp1.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            sp1.Parameters.AddWithValue("@Date", program.Date);
                            sp1.Parameters.AddWithValue("@Time", program.Time);
                            sp1.Parameters.AddWithValue("@Creator", program.Creator);
                            sp1.Parameters.AddWithValue("@Editer", program.Editer);
                            sp1.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            sp1.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            sp1.Parameters.AddWithValue("@ReferralCode", program.ReferralCode ?? "");
                            sp1.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            sp1.Parameters.AddWithValue("@Notes", program.Notes);
                            sp1.Parameters.AddWithValue("@Type ", program.Type);
                            sp1.Parameters.AddWithValue("@Duration", program.Duration);
                            sp1.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            sp1.Parameters.AddWithValue("@ReasonType", program.ReasonType ?? "");
                            sp1.Parameters.AddWithValue("@TabType", program.TabType);

                            sp1.Parameters.AddWithValue("@Program", program.Program);
                            sp1.Parameters.AddWithValue("@HaccType", hacc);
                            sp1.Parameters.AddWithValue("@BillTo", billTo);

                            await sp1.ExecuteNonQueryAsync();
                        }
                    }
                    transaction.Commit();
                    return Ok(procedure);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        [HttpDelete("delete-latest-quote")]
        public async Task<IActionResult> DeleteLatestQuote()
        {
            using (var transaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var headerId = (from hdr in _context.Qte_Hdr orderby hdr.RecordNumber descending select hdr).FirstOrDefault();

                    if (headerId == null)
                    {
                        return BadRequest();
                    }

                    var allDoc = await (from doc in _context.Documents where doc.Doc_Id == headerId.CPID select doc).ToListAsync();
                    _context.Documents.RemoveRange(allDoc);

                    var allQuoteLines = await (from lne in _context.Qte_Lne where lne.Doc_Hdr_Id == headerId.RecordNumber select lne).ToListAsync();

                    _context.Qte_Lne.RemoveRange(allQuoteLines);
                    _context.Qte_Hdr.Remove(headerId);

                    await _context.SaveChangesAsync();
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest();
                }
            }
            return Ok(true);
        }

        [HttpDelete("delete-temp-doc/{docId}")]
        public async Task<IActionResult> DeleteCreateTempDocumentRecord(int docId)
        {
            var documents = await (from doc in _context.Documents where doc.Doc_Id == docId select doc).FirstOrDefaultAsync();
            var qteHeader = await (from hdr in _context.Qte_Hdr where hdr.CPID == docId select hdr).ToListAsync();

            if (qteHeader.Count > 0)
            {
                return Ok("document not deleted");
            }

            _context.Documents.Remove(documents);
            await _context.SaveChangesAsync();
            return Ok("document deleted");
        }

        [HttpPost("create-quote-line")]
        public async Task<IActionResult> PostQuoteLine([FromBody] QuoteLineDTO quoteLine)
        {
            try
            {
                Qte_Lne newLine = new Qte_Lne()
                {
                    Doc_Hdr_Id = quoteLine.DocHdrId,
                    ItemId = quoteLine.ItemId,
                    Qty = quoteLine.Qty,
                    BillUnit = quoteLine.BillUnit,
                    UnitBillRate = quoteLine.UnitBillRate,
                    DisplayText = quoteLine.DisplayText,
                    Roster = quoteLine.Roster,
                    Frequency = quoteLine.Frequency,
                    QuoteQty = quoteLine.QuoteQty,
                    LengthInWeeks = quoteLine.LengthInWeeks,
                    RCycle = quoteLine.RCycle,
                    Notes = quoteLine.Notes,
                    SortOrder = quoteLine.SortOrder,
                    QuotePerc = quoteLine.QuotePerc,
                    BudgetPerc = quoteLine.BudgetPerc,                 
                    PriceType = quoteLine.PriceType,
                    Tax = quoteLine.Tax,
                    LineNo = quoteLine.LineNo
                    
                };

                _context.Qte_Lne.Add(newLine);
                await _context.SaveChangesAsync();

                var result = await (from so in _context.ServiceOverview
                                    join itm in _context.ItemTypes on so.ServiceType equals itm.Title
                                    where so.ServiceType == quoteLine.Code
                                    select new
                                    {
                                        ServiceType = so.ServiceType,
                                        MainGroup = itm.MainGroup,
                                        RosterGroup = itm.RosterGroup
                                    }).FirstOrDefaultAsync();

                string mainGroup = GenSettings.DetermineMainGroup(result.RosterGroup, result.MainGroup);

                return Ok(new QuoteLineDTO()
                {
                    Code = quoteLine.Code,
                    DisplayText = newLine.DisplayText,
                    Qty = newLine.Qty,
                    BillUnit = newLine.BillUnit,
                    Frequency = newLine.Frequency,
                    QuoteQty = newLine.QuoteQty,
                    UnitBillRate = newLine.UnitBillRate,
                    Tax = newLine.Tax,
                    MainGroup = mainGroup,
                    RecordNumber = newLine.RecordNumber
                });

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

  [HttpDelete("document/{id}")]
        public async Task<IActionResult> DeleteContactsKin(int id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Update  Documents set DeletedRecord=1 WHERE DOC_ID = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();

                    int row = await cmd.ExecuteNonQueryAsync();
                    return Ok(row > 0);
                }
            }
        }

        [HttpPost("create-temp-doc")]
        public async Task<IActionResult> PostCreateTempDocumentRecord([FromBody] QuoteHeaderDTO quote)
        {
            // return Ok(46116);
            try
            {
                decimal docIdentity = 0;

                await listService.DeleteDocumentTempFiles();

                using (var conn = _context.GetConnection() as SqlConnection)
                {
                    await conn.OpenAsync();
                    using (SqlCommand billCmd = new SqlCommand(@"SELECT IDENT_CURRENT('Documents') AS [LastID]", (SqlConnection)conn))
                    {
                        using (var rd1 = await billCmd.ExecuteReaderAsync())
                        {
                            if (await rd1.ReadAsync())
                            {
                                docIdentity = GenSettings.Filter(rd1["LastID"]);
                            }
                        }
                    }
                    conn.Close();
                }


                var docId = Convert.ToInt32(docIdentity) + 1;

                Documents newDoc = new Documents()
                {
                    FileName = $@"TEMP{docId}",
                    Title = $@"TEMP{docId}",
                    PersonID = quote.PersonId,
                    DocumentGroup = quote.DocumentGroup ?? "CP_QUOTE",
                    DocNo = docId.ToString(),
                    Status = "O"
                };

                _context.Documents.Add(newDoc);
                await _context.SaveChangesAsync();

                Qte_Hdr newQuoteHeader = new Qte_Hdr()
                {
                    CPID = newDoc.Doc_Id
                };

                _context.Qte_Hdr.Add(newQuoteHeader);

                await _context.SaveChangesAsync();

                return Ok(new
                {
                    QuoteHeaderId = newQuoteHeader.RecordNumber,
                    DocId = newDoc.Doc_Id
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        //[HttpGet("sample-document")]
        //public async Task<IActionResult> SampleWord(string personid, string originfile, string destinationfile)
        //{
        //    var fields = await this.listService.GetRecipientQuoteMergeFields(personid);

        //    var newFileName = $@"{fields.Id}-{fields.AccountNo} {DateTime.Now.ToString("dd_M_yyyy")}";

        //    List<string> goals = new List<string>() { "Goal1", "Goal2" };
        //    QuoteHeaderDTO quote = new QuoteHeaderDTO();
        //    quote.Goals.AddRange(goals);


        //    List<QuoteLineDTO> quotes = new List<QuoteLineDTO>()
        //    {
        //        new QuoteLineDTO(){
        //            DisplayText = "Quote1",
        //            Qty = 1,
        //            LengthInWeeks = 52,
        //            UnitBillRate = (decimal?) 62.00
        //        },
        //        new QuoteLineDTO(){
        //             DisplayText = "Quote2",
        //            Qty = 1,
        //            LengthInWeeks = 52,
        //             UnitBillRate = (decimal?) 542.00
        //        },
        //    };

        //    quote.QuoteLines.AddRange(quotes);

        //    var mergeResult = await wordService.MergeFieldsDocumentUsingInterop(fields, originfile, destinationfile, newFileName, quote);

        //    return Ok(mergeResult);
        //}

        [HttpPost("print-quote")]
        public async Task<IActionResult> PrintQuoteLine()
        {
            string docFolderSource = $@"C:\\Users\\arshad\\Desktop\\Programming\\Adamas\\adamasv3\\quote_lines.docx";
            //string docFolderLoggedUser = $@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3";

            var memStream = await printService.PrintQuotes(docFolderSource);

            if (memStream == null)
            {
                return BadRequest();
            }

            MemoryStream ms = memStream.Value.Item1;
            string filetype = memStream.Value.Item2;

            return File(ms, filetype, "quotes-line.docx");

        }

        [HttpPost("print-quote-line")]

        public async Task<IActionResult> PrintQuoteLines([FromBody] QuoteHeaderDTO quote)
        {
            var path = Path.Combine(Environment.CurrentDirectory);
            var docFolderSource = Path.Combine(path, "QuoteTemplate.docx");
            var docFolderLoggedUser = path;

            var fields = await this.listService.GetRecipientQuoteMergeFields(quote.PersonId);

            var memStream = await wordService.PrintQuotes(quote, docFolderSource, docFolderLoggedUser, "ote", fields);


            if (memStream == null)
            {
                return BadRequest();
            }

            MemoryStream ms = memStream.Value.Item1;
            string filetype = memStream.Value.Item2;

            return File(ms, filetype, "ote.docx");
        }

        [HttpPost("check-post-quote")]
        public async Task<IActionResult> CheckPostQuote([FromBody] QuoteHeaderDTO quote)
        {
            var fileExists = await this.listService.CheckQuoteDocument(quote);
            return Ok(fileExists);
        }

        [HttpPost("post-quote")]
        public async Task<IActionResult> PostQuote([FromBody] QuoteHeaderDTO quote)
        {

            if (!quote.RecordNumber.HasValue)
            {
                return BadRequest("RecordNumber is null");
            }
           
            //using (var context = await _context.Database.BeginTransactionAsync())
            using (var context = this._context)
            {
                try
                {
                    var hdr = await (from qteHeader in context.Qte_Hdr where qteHeader.RecordNumber == quote.RecordNumber select qteHeader).FirstOrDefaultAsync();
                    var docIdList = await (from qteHeader in context.Qte_Hdr where qteHeader.DocId != null  select Convert.ToInt32(qteHeader.DocId)).ToListAsync();
                    var docIdMax = docIdList.Cast<int>().Max() != null ? docIdList.Cast<int>().Max() : 0;
                    if ( hdr.DocId==null)
                        hdr.DocId = (docIdMax + 1).ToString();

                    hdr.ClientId = quote.ClientId;
                    hdr.ProgramId = quote.ProgramId;
                    hdr.CPID = quote.DocumentId;
                    hdr.Contribution = quote.Contribution;
                    hdr.DaysCalc = quote.DaysCalc;
                    hdr.Budget = quote.Budget;
                    hdr.QuoteBase = quote.QuoteBase;
                    hdr.IncomeTestedFee = quote.IncomeTestedFee;
                    hdr.GovtContribution = quote.GovtContribution;
                    hdr.PackageSupplements = quote.PackageSupplements;
                    hdr.AgreedTopUp = quote.AgreedTopUp;
                    hdr.BalanceAtQuote = quote.BalanceAtQuote;
                    hdr.CLAssessedIncomeTestedFee = quote.CLAssessedIncomeTestedFee;
                    hdr.FeesAccepted = quote.FeesAccepted;
                    hdr.BasePension = quote.BasePension;
                    hdr.DailyBasicCareFee = $"${quote.DailyBasicCareFee.Replace("$", "")}";
                    hdr.DailyIncomeTestedFee = $"${quote.DailyIncomeTestedFee.Replace("$", "")}";
                    hdr.DailyAgreedTopUp = quote.DailyAgreedTopUp;
                    hdr.HardshipSupplement = quote.HardshipSupplement;
                    hdr.QuoteView = quote.QuoteView;

                    await context.SaveChangesAsync();

                    //await context.Qte_Lne.AddRangeAsync(quote.QuoteLines.Select(x => new Qte_Lne(){                        
                    //    Doc_Hdr_Id = hdr.RecordNumber,
                    //    ItemId = (from item in context.ItemTypes where item.Title == x.ServiceType select item.Recnum).FirstOrDefault(),
                    //    Qty = x.Qty,
                    //    BillUnit = x.BillUnit,
                    //    UnitBillRate = x.UnitBillRate,
                    //    Tax = x.Tax,
                    //    COID = x.COID,
                    //    BRID = x.BRID,
                    //    DPID = x.DPID,
                    //    DisplayText = x.DisplayText,
                    //    LineNo = x.LineNo,
                    //    Frequency = x.Frequency,
                    //    QuoteQty = x.QuoteQty,
                    //    PriceType = x.PriceType,
                    //    QuotePerc = x.QuotePerc,
                    //    BudgetPerc = x.BudgetPerc,
                    //    LengthInWeeks = x.LengthInWeeks,
                    //    StrategyId = x.StrategyId,
                    //    SortOrder = x.SortOrder,
                    //    Roster = x.Roster
                    //}));
                    //await context.SaveChangesAsync();

                    var docFolderLoggedUser = await (from ui in context.UserInfo where ui.Name == quote.User select ui.RecipientDocFolder).FirstOrDefaultAsync();
                    var docFolderSource = await (from da in context.DOC_Associations where da.Title == quote.Template.ToString().TrimEnd() && da.TRACCSType == "CAREPLAN" select da.Template).FirstOrDefaultAsync();

                    // UPDATE Documents
                    var insertedDocument = await (from docIns in context.Documents where docIns.Doc_Id == quote.DocumentId select docIns).FirstOrDefaultAsync();

                    var newFileName = string.Empty;
                    var newRecord = false;
           
                    if (string.IsNullOrEmpty(quote.NewFileName)){
                         newRecord=true;
                        newFileName = $@"{quote.PersonId}-{quote.Program}-{DateTime.Now.ToString("dd_M_yyyy")}";
                     } else
                    {
                        var hasExtension = Path.GetExtension(quote.NewFileName);
                        if (!string.IsNullOrEmpty(hasExtension))
                        {
                            newFileName = Path.GetFileNameWithoutExtension(quote.NewFileName);
                        }
                        else
                        {
                            newFileName = quote.NewFileName;
                        }
                    }

                   var fileExists = await this.listService.CheckQuoteDocument(quote);

                    if (!string.IsNullOrEmpty(fileExists) && newRecord)
                    {
                        return BadRequest(new { Message = "Filename already exists" });
                    }

                    insertedDocument.Created = DateTime.Now;
                    insertedDocument.Modified = DateTime.Now;
                    insertedDocument.FileName = $"{newFileName}.docx";
                    insertedDocument.Department = (quote.ProgramId).ToString();

                    insertedDocument.OriginalLocation = docFolderLoggedUser;
                    insertedDocument.Title = quote.Template;
                    insertedDocument.Classification = "CAREPLAN";
                    insertedDocument.Category = "CAREPLAN";
                    insertedDocument.SubId = (from dd in context.DataDomains where dd.Description == quote.Type && dd.Domain == "CAREPLANTYPES" select dd.RecordNumber ).FirstOrDefault();

                    insertedDocument.DocumentType = ".docx";
                    await context.SaveChangesAsync();
                    // insertedDocument


                    // GET MERGE FIELDS of 
                    var fields = await this.listService.GetRecipientQuoteMergeFields(quote.PersonId);

                    //docFolderSource = $@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3\\Careplan.docx";
                    //docFolderLoggedUser = $@"C:\\Users\\mark\\Desktop\\Programming\\Adamas\\adamasv3";

                    //return Ok(new
                    //{
                    //    DocumentSource = docFolderSource,
                    //    DocumentDestination = docFolderLoggedUser,
                    //    DocumentFileFolder = System.IO.Path.Combine(docFolderLoggedUser, newFileName)
                    //});

                    if (docFolderSource==null || !newRecord){
                        return Ok(quote);
                    }
                   
                    var mergeResult = await wordService.MergeFieldsDocumentUsingInterop(fields, docFolderSource, docFolderLoggedUser, newFileName, quote, hdr);
                    if (!mergeResult.Success)
                    {
                        //  transaction.Rollback();
                        return BadRequest(mergeResult);
                    }

                    // transaction.Commit();
                    return Ok(new
                    {
                        DocumentSource = docFolderSource,
                        DocumentDestination = docFolderLoggedUser,
                        DocumentFileFolder = System.IO.Path.Combine(docFolderLoggedUser, newFileName)
                    });
                }
                catch (Exception ex)
                {
                    //  transaction.Rollback();
                    return BadRequest(ex.ToString());
                }

            }
            return Ok(quote);
        }

        // DONE TESTED
        [HttpPost("referral-in")]
        public async Task<IActionResult> PostReferralIn([FromBody] CallProcedure procedure)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    await conn.OpenAsync();
                    transaction = conn.BeginTransaction();
                    string hacc = string.Empty, billTo = string.Empty;

                    /*  INSERTS INTO History AND HumanResources */
                    using (SqlCommand cmd = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection)conn, transaction))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        // Input Parameters for SP3
                        cmd.Parameters.AddWithValue("@PersonId", procedure.StaffNote.PersonId);
                        cmd.Parameters.AddWithValue("@Program", procedure.StaffNote.Program);
                        cmd.Parameters.AddWithValue("@DetailDate", procedure.StaffNote.DetailDate);
                        cmd.Parameters.AddWithValue("@ExtraDetail1", procedure.StaffNote.ExtraDetail1);
                        cmd.Parameters.AddWithValue("@ExtraDetail2", procedure.StaffNote.ExtraDetail2);
                        cmd.Parameters.AddWithValue("@WhoCode", procedure.StaffNote.WhoCode);
                        cmd.Parameters.AddWithValue("@PublishToApp", procedure.StaffNote.PublishToApp);
                        cmd.Parameters.AddWithValue("@Creator", procedure.StaffNote.Creator);
                        cmd.Parameters.AddWithValue("@Note", procedure.StaffNote.Note);
                        cmd.Parameters.AddWithValue("@AlarmDate", procedure.StaffNote.AlarmDate ?? (object)DBNull.Value);
                        cmd.Parameters.AddWithValue("@ReminderTo", procedure.StaffNote.ReminderTo ?? (object)DBNull.Value);
                        await cmd.ExecuteNonQueryAsync();
                    }

                    if (procedure.IsNDIAHCP)
                    {
                        /* INSERTS INTO humanresourcetypes */
                        using (SqlCommand cmd0 = new SqlCommand(@"[ClonePackage]", (SqlConnection)conn, transaction))
                        {
                            cmd0.CommandType = CommandType.StoredProcedure;
                            cmd0.Parameters.AddWithValue("@NewPackage", procedure.NewPackage);
                            cmd0.Parameters.AddWithValue("@SourcePackage", procedure.OldPackage);
                            // cmd0.Parameters.AddWithValue("@Type", procedure.OldPackage);
                            // cmd0.Parameters.AddWithValue("@Level", procedure.Level);
                            await cmd0.ExecuteNonQueryAsync();
                        }
                    }

                    foreach (var program in procedure.Roster)
                    {

                        using (SqlCommand haccCmd = new SqlCommand(@"SELECT HACCType FROM ItemTypes WHERE Title = @referralType AND ProcessClassification = 'OUTPUT'", (SqlConnection)conn, transaction))
                        {
                            haccCmd.Parameters.AddWithValue("@referralType", program.ServiceType);
                            using (var rd = await haccCmd.ExecuteReaderAsync())
                            {
                                if (await rd.ReadAsync()) hacc = GenSettings.Filter(rd["HACCType"]);
                            }
                        }

                        using (SqlCommand billCmd = new SqlCommand(@"SELECT BillTo FROM Recipients WHERE AccountNo = @recipient", (SqlConnection)conn, transaction))
                        {
                            billCmd.Parameters.AddWithValue("@recipient", procedure.StaffNote.WhoCode);
                            using (var rd1 = await billCmd.ExecuteReaderAsync())
                            {
                                if (await rd1.ReadAsync()) billTo = GenSettings.Filter(rd1["BillTo"]);
                            }
                        }
                        /* INSERTS INTO RecipientPrograms  */
                        using (SqlCommand cmd1 = new SqlCommand(@"[SetClientPackage]", (SqlConnection)conn, transaction))
                        {
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.AddWithValue("@NewPackage", program.Program);
                            cmd1.Parameters.AddWithValue("@ClientCode", procedure.StaffNote.PersonId);
                            cmd1.Parameters.AddWithValue("@Status", program.PackageStatus);
                            await cmd1.ExecuteNonQueryAsync();
                        }

                        /* INSERTS INTO Roster & Audit */
                        using (SqlCommand cmd2 = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn, transaction))
                        {
                            cmd2.CommandType = CommandType.StoredProcedure;
                            // Input Parameters for SP2
                            cmd2.Parameters.AddWithValue("@ClientCode", program.ClientCode);
                            cmd2.Parameters.AddWithValue("@CarerCode", program.CarerCode);
                            cmd2.Parameters.AddWithValue("@ServiceType", program.ServiceType);
                            cmd2.Parameters.AddWithValue("@Date", program.Date);
                            cmd2.Parameters.AddWithValue("@Time", program.Time);
                            cmd2.Parameters.AddWithValue("@Creator", program.Creator);
                            cmd2.Parameters.AddWithValue("@Editer", program.Editer);
                            cmd2.Parameters.AddWithValue("@BillUnit", program.BillUnit);
                            cmd2.Parameters.AddWithValue("@AgencyDefinedGroup", program.AgencyDefinedGroup);
                            cmd2.Parameters.AddWithValue("@ReferralCode", program.ReferralCode ?? "");
                            cmd2.Parameters.AddWithValue("@TimePercent", program.TimePercent);
                            cmd2.Parameters.AddWithValue("@Notes", program.Notes);
                            cmd2.Parameters.AddWithValue("@Type ", program.Type);
                            cmd2.Parameters.AddWithValue("@Duration", program.Duration);
                            cmd2.Parameters.AddWithValue("@BlockNo", program.BlockNo);
                            cmd2.Parameters.AddWithValue("@ReasonType", program.ReasonType ?? "");
                            cmd2.Parameters.AddWithValue("@TabType", program.TabType);
                            cmd2.Parameters.AddWithValue("@Program", program.Program);
                            cmd2.Parameters.AddWithValue("@HaccType", hacc ?? "");
                            cmd2.Parameters.AddWithValue("@BillTo", billTo);
                            await cmd2.ExecuteNonQueryAsync();
                        }
                    }

                    transaction.Commit();

                    return Ok(procedure);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }



        [HttpPost("get-staff-branch")]
        public async Task<IActionResult> GetStaffBranch([FromBody] string[] branches)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {

                var insertString = new StringBuilder("SELECT [Name], s.stf_department FROM USERINFO u inner join staff s on u.staffcode = s.accountno WHERE STF_DEPARTMENT IN ");
                insertString.Append("(");
                int counter = 0;
                foreach (var branch in branches)
                {
                    if (counter != 0)
                    {
                        insertString.Append(",");
                    }
                    insertString.Append("'" + branch + "'");
                    counter++;
                }
                insertString.Append(") ORDER BY [Name]");
                List<string> list = new List<string>();
                using (SqlCommand cmd = new SqlCommand(insertString.ToString(), (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["name"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("set-client-package")]
        public async Task<IActionResult> PostClientPackage([FromBody] ProcedureSetClientPackage package)
        {
            //return Ok(package);
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"[SetClientPackage]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@PackageStatus", package.PackageStatus);
                    cmd.Parameters.AddWithValue("@PackageName", package.PackageName);
                    cmd.Parameters.AddWithValue("@ClientCode", package.ClientCode);
                    await conn.OpenAsync();

                    cmd.CommandTimeout = 20000;
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPost("clone-package")]
        public async Task<IActionResult> PostClonePackage([FromBody] ProcedureClonePackage package)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"[ClonePackage]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SourcePackage", package.SourcePackage);
                    cmd.Parameters.AddWithValue("@NewPackageName", package.NewPackageName);
                    await conn.OpenAsync();

                    cmd.CommandTimeout = 20000;
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpGet("charge-amount/{title}")]
        public async Task<IActionResult> GetChargeAmount(string title)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Amount, Price2, Price3, Price4, Price5, Price6, Unit, HACCType, MainGroup, MinorGroup, RosterGroup 
                    FROM ItemTypes WHERE ProcessClassification IN ('OUTPUT', 'EVENT') And Title = @Title", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@Title", title);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    dynamic obj = null;
                    if (await rd.ReadAsync())
                    {
                        obj = new
                        {
                            Amount = GenSettings.Filter(rd["Amount"]),
                            Unit = GenSettings.Filter(rd["Unit"]),
                            HaccType = GenSettings.Filter(rd["HACCType"])
                        };
                    }
                    return Ok(obj);
                }
            }
        }

        [HttpGet("event-life-cycle")]
        public async Task<IActionResult> GetEventLifeCycle()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS EVent from datadomains where DOMAIN = 'LIFECYCLEEVENTS'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["Event"]).ToUpper());
                    }
                    return Ok(obj);
                }
            }
        }

        [HttpGet("accomodation-setting")]
        public async Task<IActionResult> GetAccomodationSetting()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'ACCOMODATION SETTING'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("indignious-status")]
        public async Task<IActionResult> GetIndigniousStatus()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'INDIGINOUS STATUS'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("living-arrangments")]
        public async Task<IActionResult> GetLivingArrangments()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'LIVING ARRANGEMENTS'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("hacc-va-card-status")]
        public async Task<IActionResult> GetHACCVaCardStatus()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'HACCDVACardStatus'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }

       

   [HttpGet("hacc-consent/{personId}")]
        public async Task<IActionResult> GetHACCConsent(string personId)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@" 
                        select CASE WHEN ISNULL(COInfo.[Name], '') = '' THEN 0 ELSE 1 END  AS InfoConsent, 
                        CASE WHEN ISNULL(COContact.[Name], '') = '' THEN 0 ELSE 1 END AS ContactConsent
                        From Recipients Re
                        LEFT JOIN HumanResources COInfo ON RE.Uniqueid = COInfo.Personid AND CoInfo.[Name] = 'DSS Info Consent' 
                        LEFT JOIN HumanResources COContact ON RE.Uniqueid = COContact.Personid AND COContact.[Name] = 'DSS Contact Consent'
                        WHERE UniqueID = @PersonId", (SqlConnection)conn))
                {

                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@PersonId", personId); 
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();                 

                  dynamic obj =null;;
                    if (await rd.ReadAsync())
                    {
                        obj = new 
                        {
                            InfoConsent = GenSettings.Filter(rd["InfoConsent"]),
                            ContactConsent = GenSettings.Filter(rd["ContactConsent"])
                        };
                    }
                    return Ok(obj);
                }
            }
        }

        [HttpGet("staff-roster-entry/{staffcode}")]
        public async Task<IActionResult> GetRosterEntry(string staffcode)
        {
            await using var conn = _context.Database.GetDbConnection() as SqlConnection;
            if (conn == null) return StatusCode(500, "Database connection failed.");

            await using var cmd = new SqlCommand(
                @"SELECT RecordNo FROM Roster WHERE [Carer Code] = @staffcode AND Status = 1", conn);

            cmd.Parameters.AddWithValue("@staffcode", staffcode);

            try
            {
                await conn.OpenAsync();
                await using var rd = await cmd.ExecuteReaderAsync();

                var result = new
                {
                    RecordNo = 0  // Default value if no entry is found
                };

                if (await rd.ReadAsync())
                {
                   result = new
{
    RecordNo = (int)GenSettings.Filter(rd["RecordNo"])  // Assuming it returns an integer
};
                }

                return Ok(result); // Always return a success response
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }




        [HttpGet("hacc-referral_source")]
        public async Task<IActionResult> GetHACCReferralSource()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'REFERRAL SOURCE'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("referral_services")]
        public async Task<IActionResult> GetReferralServices()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'REFERRAL SERVICES'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }

        [HttpGet("carer-data-recipientcarer")]
        public async Task<IActionResult> GetCarerDataRecipientcarer()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct AccountNo as Name FROM Recipients WHERE [Type] IN ('CARER', 'CARER/RECIPIENT')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["Name"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("carer-data-relationship")]
        public async Task<IActionResult> GetCarerDataRelationship()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'CARER RELATIONSHIP'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("carer-data-availability")]
        public async Task<IActionResult> GetCarerDataAvailability()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'CARER AVAILABILITY'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("carer-data-residency")]
        public async Task<IActionResult> GetCarerDataResidency()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'CARER RESIDENCY'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
  [HttpGet("carer-sustainability")]
        public async Task<IActionResult> GetCarerSustainability()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description AS description from datadomains where DOMAIN = 'CARER SUSTAINABILITY'", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("haccSex")]
        public async Task<IActionResult> GetHaccSex()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'SEX' AND DATASET = 'HACC' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }

        [HttpGet("countries")]
        public async Task<IActionResult> GetCountries()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'COUNTRIES' ORDER BY Description", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();

                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["Description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("languages")]
        public async Task<IActionResult> GetLanguages()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'HACC LANGUAGE' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }

 [HttpGet("gender/identies")]
        public async Task<IActionResult> GetGenderIdenties()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'SEX' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("religions")]
        public async Task<IActionResult> GetReligions()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'RELIGION' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
      
         [HttpGet("citizenship")]
        public async Task<IActionResult> GetCitizenship()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'RELIGION' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
         [HttpGet("referral-type")]
        public async Task<IActionResult> GetReferralTypeAll()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"  SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'REFERRAL SOURCE'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
        [HttpGet("referral-type/{packageTemplate}")]
        public async Task<IActionResult> GetReferralType(string packageTemplate)
        {

            int count = 0;
            List<string> list = new List<string>();

            try
            {
                using (SqlConnection conn = _context.Database.GetDbConnection() as SqlConnection)
                {

                    await conn.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand(@"
                        SELECT Count([service type]) AS ActivityCount
                            FROM   serviceoverview SO
                                   INNER JOIN humanresourcetypes HRT
                                           ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid
                            WHERE  SO.serviceprogram = @template
                                   AND EXISTS (SELECT title
                                               FROM   itemtypes ITM
                                               WHERE  title = SO.[service type]
                                                      AND ITM.[rostergroup] IN ( 'ADMISSION' )
                                                      AND ITM.[minorgroup] IN ( 'REFERRAL-IN' )
                                                      AND ( isnull(ITM.enddate,'') =''
                                                             OR ITM.enddate >= convert(varchar,getDate(),111) )) ", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@template", packageTemplate);
                         cmd.Parameters.AddWithValue("@date", DateTime.Now);

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        if (await rd.ReadAsync())
                        {
                            count = GenSettings.Filter(rd["ActivityCount"]);
                        }

                        rd.Close();
                    }

                    if (count > 0)
                    {

                        using (SqlCommand cmd2 = new SqlCommand(@"SELECT DISTINCT [service type] AS Activity
                                    FROM   serviceoverview SO
                                            INNER JOIN humanresourcetypes HRT
                                                    ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid
                                    WHERE  SO.serviceprogram = @template
                                            AND EXISTS (SELECT title
                                                        FROM   itemtypes ITM
                                                        WHERE  title = SO.[service type]
                                                                AND ITM.[rostergroup] IN ( 'ADMISSION' )
                                                                AND ITM.[minorgroup] IN ( 'REFERRAL-IN' )
                                                                AND ( isnull(ITM.enddate,'') =''
                                                                        OR ITM.enddate >= convert(varchar,getDate(),111) ))
                                    ORDER  BY [service type] ", (SqlConnection)conn))
                        {
                            cmd2.Parameters.AddWithValue("@template", packageTemplate);
                            cmd2.Parameters.AddWithValue("@date", DateTime.Now);

                            SqlDataReader rd2 = await cmd2.ExecuteReaderAsync();
                            while (await rd2.ReadAsync())
                            {
                                list.Add(GenSettings.Filter(rd2["Activity"]));
                            }

                            rd2.Close();
                        }
                    }
                    else
                    {

                        using (SqlCommand cmd3 = new SqlCommand(@"SELECT DISTINCT [Title] As Activity FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ADMISSION') AND [MinorGroup] IN ('REFERRAL-IN') AND (isnull(EndDate,'')='' OR Enddate >= convert(varchar,getDate(),111)) ORDER BY [Title]", (SqlConnection)conn))
                        {

                            cmd3.Parameters.AddWithValue("@date", DateTime.Now);
                            SqlDataReader rd3 = await cmd3.ExecuteReaderAsync();
                            while (await rd3.ReadAsync())
                            {
                                list.Add(GenSettings.Filter(rd3["Activity"]));
                            }
                            rd3.Close();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }


            return Ok(list);
        }
 [HttpGet("address/types")]
        public async Task<IActionResult> getAddressTypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                var sql= @"SELECT Domain,'(a) ' + Description + ' ' + A.item as [Description], A.item FROM 
                            DataDomains 
                            ,
                            (select *  from  Split('Address-Line1,Address-Line2,Address-Suburb,Address-Postcode,Address-State',',')) A
                            WHERE Domain in ( 'ADDRESSTYPE')
                            Union
                            SELECT Domain,'(c) ' + [Description], Domain as item  FROM 
                            DataDomains 
                            WHERE Domain in ( 'CONTACTTYPE') and ( [Description] like '%EMAIL%' OR  [Description] like '%FAX%')
                            Union
                            SELECT Domain,'(c) ' + [Description] +' Phone', Domain as item FROM 
                            DataDomains 
                            WHERE Domain in ( 'CONTACTTYPE') and NOT ( [Description] like '%EMAIL%' OR  [Description] like '%FAX%')
                            order by Domain,Description,item";

                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> obj = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        obj.Add(GenSettings.Filter(rd["description"]));
                    }
                    return Ok(obj);
                }
            }
        }
       
        
        [HttpGet("package-template/{type}")]
        public async Task<IActionResult> GetPackageTemplate(string type)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"[GetPackageTemplates]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Type", type);
                    await conn.OpenAsync();

                    cmd.CommandTimeout = 20000;
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = GenSettings.Filter(rd["Name"]),
                            PackageLevel = GenSettings.Filter(rd["PackageLevel"])
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("referral-source-v2")]
        public async Task<IActionResult> GetReferralSourceV2(ReferralSourceDto program)
        {

            string sql = string.Empty;

            if (program.ProgramType == ProgramRecipientTypes.REFER_ON)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE SO.serviceprogram = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'REFERRAL-OUT' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (program.ProgramType == ProgramRecipientTypes.NOT_PROCEED)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'NOT PROCEEDING' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (program.ProgramType == ProgramRecipientTypes.ASSESS)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'ASSESSMENT' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (program.ProgramType == ProgramRecipientTypes.ADMIT)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION','ADMINISTRATION' )  AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (program.ProgramType == ProgramRecipientTypes.ADMIN)
            {
                

                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'OTHER', 'REVIEW' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (program.ProgramType == ProgramRecipientTypes.DISCHARGE)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'DISCHARGE' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (program.ProgramType == ProgramRecipientTypes.ITEM)
            {
                sql = @"SELECT [Title] AS activity FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ITEM') AND (EndDate Is Null OR EndDate >= @date) ORDER BY [Title]";
            }

            using (var conn = _context.GetConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@date", DateTime.Now);

                    if (!(program.ProgramType == ProgramRecipientTypes.ITEM))
                    {
                        cmd.Parameters.AddWithValue("@program", program.ProgramName);
                    }

                    await conn.OpenAsync();
                    List<string> list = new List<string>();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["activity"]));
                    }

                    return Ok(list);
                }
            }
        }


        [HttpGet("referral-type-v2")]
        public async Task<IActionResult> GetReferralTypeV2(string program, string option)
        {

            string sql = string.Empty;

            if (option == ProgramRecipientTypes.REFER_ON)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE SO.serviceprogram = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'REFERRAL-OUT' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (option == ProgramRecipientTypes.NOT_PROCEED)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'NOT PROCEEDING' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (option == ProgramRecipientTypes.ASSESS)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'ASSESSMENT' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (option == ProgramRecipientTypes.ADMIT)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'ADMISSION' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (option == ProgramRecipientTypes.ADMIN)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'OTHER', 'REVIEW' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (option == ProgramRecipientTypes.DISCHARGE)
            {
                sql = @"SELECT [service type] AS activity FROM serviceoverview SO INNER JOIN humanresourcetypes HRT ON CONVERT(NVARCHAR, HRT.recordnumber) = SO.personid WHERE HRT.[name] = @program AND EXISTS (SELECT title FROM itemtypes ITM WHERE title = SO.[service type] AND ITM.[rostergroup] IN ( 'ADMISSION' ) AND ITM.[minorgroup] IN ( 'DISCHARGE' ) AND ( ITM.enddate IS NULL OR ITM.enddate >= @date )) ORDER BY [service type]";
            }

            if (option == ProgramRecipientTypes.ITEM)
            {
                sql = @"SELECT [Title] AS activity FROM ItemTypes WHERE ProcessClassification = 'OUTPUT' AND [RosterGroup] IN ('ITEM') AND (EndDate Is Null OR EndDate >= @date) ORDER BY [Title]";
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@date", DateTime.Now);

                    if (!(option == ProgramRecipientTypes.ITEM))
                    {
                        cmd.Parameters.AddWithValue("@program", program);
                    }

                    await conn.OpenAsync();
                    List<string> list = new List<string>();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["activity"]));
                    }

                    return Ok(list);
                }
            }
        }

        [HttpPost("deceased")]
        public async Task<IActionResult> PostDeceased([FromBody] dynamic trans)
        {
            return Ok(trans);

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                SqlTransaction transaction = null;
                try
                {
                    SqlCommand cmd = new SqlCommand(@"UPDATE Recipients SET DateOfDeath = @dateOfDeath WHERE UniqueID = @uniqueId", (SqlConnection)conn, transaction);
                    cmd.Parameters.AddWithValue("@uniqueId", trans);
                    cmd.Parameters.AddWithValue("@dateOfDeath", trans);


                    transaction.Rollback();
                    return Ok(true);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest(ex);
                }
            }
        }

        // [HttpPost("create-note")]
        // public async Task<IActionResult> PostRosterNote([FromBody] ProcedureClientStaffNote note){
        //     using(var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         using(SqlCommand cmd = new SqlCommand(@"[CreateClientStaffNote]", (SqlConnection) conn))
        //         {
        //             cmd.CommandType = CommandType.StoredProcedure;
        //             cmd.Parameters.AddWithValue("@Recipient", note.Recipient);
        //             cmd.Parameters.AddWithValue("@Program", note.Program);
        //             cmd.Parameters.AddWithValue("@Creator", note.Creator);
        //             cmd.Parameters.AddWithValue("@bMultiplePrograms", note.BMultiplePrograms);
        //             cmd.Parameters.AddWithValue("@NoteCategory", note.NoteCategory);
        //             cmd.Parameters.AddWithValue("@NoteType", note.NoteType);
        //             cmd.Parameters.AddWithValue("@Notes", note.Notes);
        //             cmd.Parameters.AddWithValue("@PublishToApp", note.PublishToApp);
        //             cmd.Parameters.AddWithValue("@AlarmDate", note.AlarmDate);


        //             cmd.CommandTimeout = 20000;
        //             await conn.OpenAsync();
        //             return Ok(await cmd.ExecuteNonQueryAsync() > 0);
        //         }
        //     }
        // }

        [HttpPost("create-roster")]
        public async Task<IActionResult> PostRoster([FromBody] ProcedureRoster rosterInput)
        {
            //return Ok(rosterInput);
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"[CreateShortRosterEntry]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@ClientCode", rosterInput.ClientCode);
                    cmd.Parameters.AddWithValue("@CarerCode", rosterInput.CarerCode);
                    cmd.Parameters.AddWithValue("@ServiceType", rosterInput.ServiceType);
                    cmd.Parameters.AddWithValue("@Date", rosterInput.Date);
                    cmd.Parameters.AddWithValue("@Time", rosterInput.Time);
                    cmd.Parameters.AddWithValue("@Creator", rosterInput.Creator);
                    cmd.Parameters.AddWithValue("@Editer", rosterInput.Editer);
                    cmd.Parameters.AddWithValue("@BillUnit", rosterInput.BillUnit);
                    cmd.Parameters.AddWithValue("@AgencyDefinedGroup", rosterInput.AgencyDefinedGroup);
                    cmd.Parameters.AddWithValue("@ReferralCode", rosterInput.ReferralCode);
                    cmd.Parameters.AddWithValue("@TimePercent", rosterInput.TimePercent);
                    cmd.Parameters.AddWithValue("@Notes", rosterInput.Notes);
                    cmd.Parameters.AddWithValue("@Type ", rosterInput.Type);
                    cmd.Parameters.AddWithValue("@Duration", rosterInput.Duration);
                    cmd.Parameters.AddWithValue("@BlockNo", rosterInput.BlockNo);
                    cmd.Parameters.AddWithValue("@ReasonType", rosterInput.ReasonType);
                    cmd.Parameters.AddWithValue("@TabType", rosterInput.TabType);

                    // cmd.Parameters.AddWithValue("@StartTime", rosterInput.StartTime);
                    // cmd.Parameters.AddWithValue("@Duration", rosterInput.Duration);
                    // cmd.Parameters.AddWithValue("@Recipient", rosterInput.Recipient);
                    // cmd.Parameters.AddWithValue("@Staff", rosterInput.Staff);
                    // cmd.Parameters.AddWithValue("@Program", rosterInput.Program);
                    // cmd.Parameters.AddWithValue("@Activity", rosterInput.Activity);
                    // cmd.Parameters.AddWithValue("@BillRate", rosterInput.BillRate);
                    // cmd.Parameters.AddWithValue("@BillUnit", "HOUR");
                    // cmd.Parameters.AddWithValue("@BillQty", rosterInput.BillQty);
                    // cmd.Parameters.AddWithValue("@PayType", "");
                    // cmd.Parameters.AddWithValue("@PayRate", "");
                    // cmd.Parameters.AddWithValue("@PayUnit", "");
                    // cmd.Parameters.AddWithValue("@PayQty", rosterInput.PayQty);
                    // cmd.Parameters.AddWithValue("@Notes", rosterInput.Notes);
                    // cmd.Parameters.AddWithValue("@Type", rosterInput.Type);
                    // cmd.Parameters.AddWithValue("@RStatus", rosterInput.RStatus);
                    // cmd.Parameters.AddWithValue("@RecipientGroup", rosterInput.RecipientGroup);
                    // cmd.Parameters.AddWithValue("@GroupActivity", "0");
                    // cmd.Parameters.AddWithValue("@BillType", "0");
                    // cmd.Parameters.AddWithValue("@BillTo", rosterInput.BillTo);
                    // cmd.Parameters.AddWithValue("@ServiceSetting", "");
                    // cmd.Parameters.AddWithValue("@ReasonType", rosterInput.ReasonType);
                    // cmd.Parameters.AddWithValue("@DatasetClient", rosterInput.DatasetClient);
                    // cmd.Parameters.AddWithValue("@Referrer", rosterInput.Referrer);
                    // cmd.Parameters.AddWithValue("@BillDesc", rosterInput.BillDesc);
                    // cmd.Parameters.AddWithValue("@TaxPerc", rosterInput.TaxPerc);
                    // cmd.Parameters.AddWithValue("@APInvoiceDate", rosterInput.APInvoiceDate);
                    // cmd.Parameters.AddWithValue("@APInvoiceNumber", rosterInput.APInvoiceNumber);
                    // cmd.Parameters.AddWithValue("@BillerCode", rosterInput.BillerCode);
                    // cmd.Parameters.AddWithValue("@WindowsUser", rosterInput.WindowsUser);
                    // cmd.Parameters.AddWithValue("@TraccsUser", rosterInput.TraccsUser);                    

                    cmd.CommandTimeout = 20000;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpGet("imlocation")]
        public async Task<IActionResult> GetLocation()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT UPPER([Description]) from DataDomains where domain = 'imlocation' order by Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("plangoalachivement")]
        public async Task<IActionResult> GetPlanGoalAchivement()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select '(' + HaccCode + ') ' + Description FROM DataDomains WHERE Domain = 'PLANGOALACHIEVEMENT' order by recordnumber", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("cstdaoutlets")]
        public async Task<IActionResult> GetCSTDAOutlets()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Name] from CSTDAOutlets ORDER BY [Name]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }



        [HttpGet("case-note-category/{index}")]
        public async Task<IActionResult> GetCaseNoteCategory(int index)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                const string caseNote = "CASENOTEGROUPS";
                const string opNote = "RECIPOPNOTEGROUPS";
                const string cliNote = "CLINNOTEGROUPS";

                string note = index == 0 ? caseNote :
                                index == 1 ? opNote :
                                    index == 2 ? cliNote : "";

                if (string.IsNullOrEmpty(note))
                    return BadRequest();

                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER([Description]) AS Description FROM DATADOMAINS WHERE DOMAIN = @Note", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@Note", note);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd["Description"]));
                        }
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("wizard-reminder-to")]
        public async Task<IActionResult> GetReminderTo()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Name] FROM UserInfo [Name] WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = '' ORDER BY [NAME]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Name"]));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("app-note-category")]
        public async Task<IActionResult> GetAppNoteCategory()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE Domain in ('RECIPOPNOTEGROUPS','CASENOTEGROUPS') ORDER BY DESCRIPTION", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Description"]));
                    }
                    return Ok(list);
                }
            }
        }

 [HttpGet("invoice-header/{accountNo}")]
        public async Task<IActionResult> GetInvoiceHeader(string accountNo)
        {
            //return Ok(user);
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT SQLID, isnull(Tagged,0) as 'S', [Traccs Processing Date] AS [Date], [Invoice Number] AS [Number], [Invoice Amount] AS Amount, [Invoice Tax] as GST, ISNULL([Invoice Amount], 0) - ISNULL(Paid, 0) AS [OS], Notes 
                                                        FROM InvoiceHeader WHERE [Client Code] = @AccountNo 
                                                        AND ISNULL([Invoice Amount], 0) - ISNULL(Paid, 0) > 0 AND ISNULL(Htype, '') IN ('I', '')  ORDER BY [TRACCS PROCESSING DATE]", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@AccountNo", accountNo);

                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    int numRows = rd.FieldCount;
                    List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                    Dictionary<string, object> dic = null;
                    while (await rd.ReadAsync())
                    {
                         dic = new Dictionary<string, object>();
                        for (var a = 0; a < numRows; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), rd.GetValue(a) != DBNull.Value
                                                        ? rd.GetValue(a)
                                                        : "");
                        }
                        list.Add(dic);
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("status-of-wizards/{personId}")]
        public async Task<IActionResult> GetStatusOfWizards(string personId)
        {
            //return Ok(user);
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT accountno, (SELECT recipients FROM userinfo WHERE [name] = 'SYSMGR') AS i_SecLevel, CASE WHEN Isnull(R.dateofdeath, '') <> '' THEN 1 ELSE 0 END AS b_IsDead, CASE WHEN Isnull(R.admissiondate, '') <> '' AND Isnull(dischargedate, '') = '' THEN 1 ELSE 0 END AS b_IsActive, CASE WHEN (SELECT Count(program) FROM recipientprograms WHERE programstatus = 'REFERRAL' AND Isnull([program], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasReferrals, CASE WHEN (SELECT Count(program) FROM recipientprograms WHERE programstatus IN ( 'ACTIVE', 'ON HOLD', 'WAITING LIST' ) AND Isnull([program], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasDischargeablePrograms, CASE WHEN (SELECT Count(program) FROM recipientprograms WHERE programstatus = 'WAITING LIST' AND Isnull([program], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasWaitListPrograms, CASE WHEN (SELECT Count(program) FROM recipientprograms WHERE programstatus = 'ACTIVE' AND Isnull([program], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasActivePrograms, CASE WHEN (SELECT Count(program) FROM recipientprograms WHERE programstatus = 'ON HOLD' AND Isnull([program], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasOnHoldPrograms, CASE WHEN (SELECT Count([service type]) FROM serviceoverview WHERE servicestatus = 'ACTIVE' AND Isnull([service type], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasActiveServices, CASE WHEN (SELECT Count([service type]) FROM serviceoverview WHERE servicestatus = 'ON HOLD' AND Isnull([service type], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasOnHoldServices, CASE WHEN (SELECT Count([service type]) FROM serviceoverview WHERE servicestatus = 'WAIT LIST' AND Isnull([service type], '') <> '' AND personid = R.uniqueid) > 0 THEN 1 ELSE 0 END AS b_HasWaitListServices FROM recipients R WHERE uniqueid = @PersonId", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@PersonId", personId);

                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    int numRows = rd.FieldCount;
                    dynamic list = null;
                    Dictionary<string, object> dic = new Dictionary<string, object>();
                    if (await rd.ReadAsync())
                    {
                        for (var a = 0; a < numRows; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), rd.GetValue(a) != DBNull.Value
                                                        ? rd.GetValue(a)
                                                        : "");
                        }
                    }
                    return Ok(dic);
                }
            }
        }

        [HttpGet("notifications-list/{check}")]
        public async Task<IActionResult> GetNotificationsList(bool check)
        {
            string whereSql = string.Empty;

            using (var conn = _context.Database.GetDbConnection())
            {
                if (check)
                {
                    whereSql = "";
                    // whereSql = "WHERE ListName IN ('Referral Notification','Assessment Notification','Admission Notification','Refer On Notification','Not Proceed Notification','Discharge Notification','Suspend Notification','Reinstate Notification','Admin Notification','Lifecycle Event Notification','Staff Onboard Notification','Staff Terminate Notification')";
                }
                else
                {
                    // whereSql = "";
                    whereSql = "WHERE ISNULL(xDeletedRecord,0) = 0 AND (xEndDate Is Null OR xEndDate >= GETDATE())";
                }

                string sql = String.Format("SELECT RecordNo, Recipient,Activity,Location,Program,Staff as StaffToNotify,ListGroup as funding_source,Mandatory as mandatory,DefaultAssignee as assignee,ListName as ltype,Severity ,xDeletedRecord as is_deleted,xEndDate as end_date, Branch from IM_DistributionLists {0} order by Recipient", whereSql);

                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNo = GenSettings.Filter(rd["RecordNo"]),
                            Recipient = GenSettings.Filter(rd["Recipient"]),
                            Activity = GenSettings.Filter(rd["Activity"]),
                            Location = GenSettings.Filter(rd["location"]),
                            Program = GenSettings.Filter(rd["program"]),
                            Staff = GenSettings.Filter(rd["StaffToNotify"]),
                            Funding_source = GenSettings.Filter(rd["funding_source"]),
                            Mandatory = GenSettings.Filter(rd["mandatory"]),
                            Assignee = GenSettings.Filter(rd["assignee"]),
                            Ltype = GenSettings.Filter(rd["ltype"]),
                            Severity = GenSettings.Filter(rd["severity"]),
                            Branch = GenSettings.Filter(rd["Branch"]),
                            // Coordinator = GenSettings.Filter(rd["Coordinator"]),
                            Is_deleted = GenSettings.Filter(rd["is_deleted"]),
                            End_date = GenSettings.Filter(rd["end_date"])
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("data-domains/{domain}")]
        public async Task<IActionResult> GetDataDomains(string domain)
        {
            var coordinator = await (from dd in _context.DataDomains where dd.Domain == domain select dd.Description).OrderBy(x => x).ToListAsync();
            return Ok(coordinator);
        }

        [HttpGet("coordinators-list-data-domains")]
        public async Task<IActionResult> GetCoordinatorsDataDomains()
        {
            var coordinator = await (from dd in _context.DataDomains where dd.Domain == "CASE MANAGERS" select dd.Description).OrderBy(x => x).ToListAsync();
            return Ok(coordinator);
        }

        [HttpGet("itemtypesparams")]
        public async Task<IActionResult> Getitemtypesparams()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"select COLUMN_NAME as name,DATA_TYPE as type,IS_NULLABLE as isnull 
                   from INFORMATION_SCHEMA.COLUMNS
                   where TABLE_NAME='ItemTypes'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            name = GenSettings.Filter(rd["name"]),
                            type = GenSettings.Filter(rd["type"]),
                            isnull = GenSettings.Filter(rd["isnull"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("coordinators")]
        public async Task<IActionResult> GetCoordinators()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Staff.UniqueID, CASE WHEN Staff.LastName = '' THEN ' ' ELSE Staff.LastName END + ', ' + CASE WHEN Staff.Firstname = '' THEN ' ' ELSE Staff.FirstName END As [Name] FROM STAFF WHERE CaseManager = 1 ORDER BY LastName", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Id = GenSettings.Filter(rd["UniqueID"]),
                            Name = GenSettings.Filter(rd["Name"])
                        });
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("coordinatorslist")]
        public async Task<IActionResult> GetCoordinatorslist()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct AccountNo,UniqueID FROM Staff WHERE CaseManager = 1 ORDER BY Accountno", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            uniqueID = GenSettings.Filter(rd["UniqueID"]),
                            AccountNo = GenSettings.Filter(rd["AccountNo"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("wizards-programs/{personID}")]
        public async Task<IActionResult> GetWizardsPrograms(string personID)
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"[GetAdminPrograms]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UniqueId", personID);
                    cmd.CommandTimeout = 20000;
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = GenSettings.Filter(rd["name"]),
                            Type = GenSettings.Filter(rd["type"])
                        });
                    }

                    list.Add(new { Name = "*HCP REFERRAL", Type = "DOHA" });
                    list.Add(new { Name = "*NDIA REFERRAL", Type = "NDIA" });
                    var newList = list.Select(x => x).OrderBy(x => x.Name);
                    return Ok(newList);
                }
            }
        }

        [HttpGet("wizards-note/{whatnote}")]
        public async Task<IActionResult> GetNotesValues(string whatnote)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM DataDomains WHERE DOMAIN = @whatnote", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@whatnote", whatnote);
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> list = new List<string>();

                    while (await rd.ReadAsync())
                        list.Add(GenSettings.Filter(rd["Description"]));

                    return Ok(list);
                }
            }
        }

        [HttpGet("wizards-referral-code")]
        public async Task<IActionResult> GetReferralCode()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [surname/organisation] + CASE WHEN firstname IS NULL OR firstname = '' THEN '' ELSE ', ' + firstname END AS Referrer FROM recipients WHERE [generatesreferrals] = 1
                                                        UNION SELECT [Name] AS Referrer from HumanResourceTypes WHERE [GROUP] = '3-MEDICAL'
                                                        ORDER BY [Referrer]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> list = new List<string>();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Referrer"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("wizards-referral-type")]
        public async Task<IActionResult> GetWizardsReferralType(ProcedureAdminServices service)
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"[GetAdminServices]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Program", service.Program);
                    cmd.Parameters.AddWithValue("@TabType", service.TabType);

                    cmd.CommandTimeout = 5000;
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<dynamic> list = new List<dynamic>();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Activity = GenSettings.Filter(rd[0])
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("wizards-referral-source/{dataset}")]
        public async Task<IActionResult> GetWizardsReferralSource(string dataset)
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                string _dataset = dataset.ToUpper();
                string sql = string.Empty;
                switch (_dataset)
                {
                    case "HACC":
                        sql = "SELECT Description,HACCCode, RecordNumber FROM DataDomains WHERE Domain = 'REFERRAL SOURCE' AND Dataset = 'HACC'";
                        break;
                    case "DEX":
                        sql = "SELECT Description,HACCCode, RecordNumber FROM DataDomains WHERE Domain = 'REFERRAL SOURCE' AND Dataset = 'DEX'";
                        break;
                    default:
                        sql = "SELECT Description,HACCCode, RecordNumber FROM DataDomains WHERE Domain = 'REFERRAL SOURCE' ORDER BY Description";
                        break;
                }
                List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    int numCol = rd.FieldCount;
                    while (await rd.ReadAsync())
                    {
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        for (var a = 0; a < numCol; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), rd.GetValue(a) != DBNull.Value
                                ? rd.GetValue(a)
                                : "");
                        }
                        list.Add(dic);
                    }
                    return Ok(list);
                }
            }
        }
        [HttpPost("get-list")]
        public async Task<IActionResult> GetSql([FromBody] SqlString sql)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql.Sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    int numRows = rd.FieldCount;
                    List<dynamic> list = new List<dynamic>();
                    while (await rd.ReadAsync())
                    {
                        Dictionary<string, object> dic = new Dictionary<string, object>();
                        for (var a = 0; a < numRows; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), rd.GetValue(a) != DBNull.Value
                                                            ? rd.GetValue(a).ToString().Trim()
                                                            : "");
                        }
                        list.Add(dic);
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("getquotetype/{cpid}")]
        public async Task<IActionResult> GetQuotetype(string cpid)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT CASE WHEN itm.MainGroup = 'GOODS/EQUIPMENT' THEN 'GOODS AND CONSUMABLES'      WHEN itm.MainGroup = 'CASE MANAGEMENT' THEN 'PACKAGE MANAGEMENT'      WHEN itm.MainGroup = 'PACKAGE ADMIN' THEN 'ADMINISTRATION' ELSE 'SERVICES' END AS [Budget Category] FROM Qte_Lne qd INNER JOIN Qte_Hdr qh ON Doc_Hdr_ID = qh.RecordNumber INNER JOIN ItemTypes itm ON qd.ItemID = itm.Recnum AND ProcessClassification = 'OUTPUT' WHERE qh.CPID = @cpid", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@cpid", cpid);
                        await conn.OpenAsync();

                        List<string> list = new List<string>();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Budget Category"]));
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }
        [HttpGet("getdailyliving/{id}")]
        public async Task<IActionResult> GetDailyliving(string id)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT budgetgroup, CONVERT(DECIMAL (10,2), Sum(Total)) AS BTotal  FROM  (  SELECT ql.doc_hdr_id, it.budgetgroup,  ql.QuoteQty * [unit bill rate] + (isnull(tax, 0)/100 * (ql.QuoteQty * [unit bill rate]))as Total  FROM qte_lne ql INNER JOIN itemtypes it ON itemid = it.Recnum  WHERE  doc_hdr_id = @id AND it.BudgetGroup = 'DAILY LIVING'  ) t  GROUP BY doc_hdr_id, BudgetGroup  ORDER BY doc_hdr_id desc", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        await conn.OpenAsync();

                        List<string> list = new List<string>();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["BTotal"]));
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }
        [HttpGet("getgovtcontrib/{id}")]
        public async Task<IActionResult> GetGovtContrib(string id)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT User1 FROM DataDomains WHERE DOMAIN = 'PACKAGERATES' AND DESCRIPTION = @id", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        await conn.OpenAsync();

                        List<string> list = new List<string>();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["User1"]));
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }

        [HttpGet("accept-charges/{program}")]
        public async Task<IActionResult> GetAcceptCharges(string program)
        {
            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@"SELECT
                        P_Def_Admin_AdminType,
                        P_Def_Admin_Admin_PercAmt,
                        P_Def_Admin_AdminFrequency,
                        P_Def_Admin_CMType,
                        P_Def_Admin_CM_PercAmt,
                        P_Def_Admin_CMFrequency,
                        P_Def_Fee_BasicCare,
                        P_Def_IncludeBasicCareFeeInAdmin,
                        P_Def_IncludeIncomeTestedFeeInAdmin
                        FROM HumanResourceTypes WHERE [GROUP] = 'PROGRAMS' and [Name] = @program", (SqlConnection)conn))
                    {
                        cmd.Parameters.AddWithValue("@program", program);
                        await conn.OpenAsync();

                        dynamic list = null;
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();

                        if (await rd.ReadAsync())
                        {
                            list = new
                            {
                                P_Def_Admin_AdminType = (GenSettings.Filter(rd["P_Def_Admin_AdminType"])),
                                P_Def_Admin_Admin_PercAmt = (GenSettings.Filter(rd["P_Def_Admin_Admin_PercAmt"])),
                                P_Def_Admin_AdminFrequency = (GenSettings.Filter(rd["P_Def_Admin_AdminFrequency"])),
                                P_Def_Admin_CMType = (GenSettings.Filter(rd["P_Def_Admin_CMType"])),
                                P_Def_Admin_CM_PercAmt = (GenSettings.Filter(rd["P_Def_Admin_CM_PercAmt"])),
                                P_Def_Admin_CMFrequency = (GenSettings.Filter(rd["P_Def_Admin_CMFrequency"])),
                                P_Def_Fee_BasicCare = (GenSettings.Filter(rd["P_Def_Fee_BasicCare"])),
                                P_Def_IncludeBasicCareFeeInAdmin = (GenSettings.Filter(rd["P_Def_IncludeBasicCareFeeInAdmin"])),
                                P_Def_IncludeIncomeTestedFeeInAdmin = (GenSettings.Filter(rd["P_Def_IncludeIncomeTestedFeeInAdmin"])),
                                IsPercent = Convert.ToString(GenSettings.Filter(rd["P_Def_Admin_Admin_PercAmt"])).IndexOf("%") > 0 ? true : false
                            };
                        }

                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok(false);
            }
        }

        [HttpGet("getbasiccare/{id}")]
        public async Task<IActionResult> GetBasicCare(string id)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@id, (SqlConnection)conn))
                    {

                        await conn.OpenAsync();

                        List<string> list = new List<string>();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["P_Def_IncludeBasicCareFeeInAdmin"]));
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }
        [HttpGet("gettopup/{id}")]
        public async Task<IActionResult> GetTOpUP(string id)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@id, (SqlConnection)conn))
                    {

                        await conn.OpenAsync();

                        List<string> list = new List<string>();
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["P_Def_IncludeTopUpFeeInAdmin"]));
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }
        [HttpGet("getcmperc/{id}")]
        public async Task<IActionResult> GetCMPERC(string id)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@id, (SqlConnection)conn))
                    {

                        await conn.OpenAsync();

                        string list = string.Empty;
                        SqlDataReader rd = await cmd.ExecuteReaderAsync();
                        if (await rd.ReadAsync())
                            list = (GenSettings.Filter(rd["P_Def_Admin_CM_PercAmt"]));
                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }
        [HttpGet("getadmperc/{id}")]
        public async Task<IActionResult> GetAdmPerc(string id)
        {

            try
            {
                using (var conn = _context.Database.GetDbConnection() as SqlConnection)
                {
                    using (SqlCommand cmd = new SqlCommand(@id, (SqlConnection)conn))
                    {

                        await conn.OpenAsync();

                        string list = string.Empty;

                        SqlDataReader rd = await cmd.ExecuteReaderAsync();

                        if (await rd.ReadAsync())
                            list = (GenSettings.Filter(rd["P_Def_Admin_Admin_PercAmt"]));

                        return Ok(list);
                    }
                }
            }
            catch (Exception ex)
            { return Ok(false); }
        }

        // [HttpPost("insertSql")]
        // public async Task<IActionResult> PostSql([FromBody] JObject obj){

        //     var objFilterVariables =  obj.SelectToken("variables").Children().OfType<JProperty>().ToDictionary(p => p.Name , p => p.Value);

        //     var column = objFilterVariables.Select(x => x.Key).ToList();
        //     var colValues = objFilterVariables.Select(x => (x.Value.Type == JTokenType.String) ? "'"+ x.Value+"'" : 
        //                         (x.Value.Type == JTokenType.Integer) ? x.Value : 
        //                             (x.Value.Type == JTokenType.Null) ? "NULL" : x.Value).ToList();
        //     var tableName = obj["table"];
        //     using(var conn = _context.Database.GetDbConnection() as SqlConnection)
        //     {
        //         using(SqlCommand cmd = new SqlCommand()){
        //             cmd.CommandText = $"INSERT INTO {tableName} ({ string.Join(",", column) }) VALUES({ string.Join(",", colValues) })";
        //             cmd.Connection = conn;
        //             await conn.OpenAsync();
        //             return Ok(await cmd.ExecuteNonQueryAsync() > 0);
        //         }
        //     }  
        // }

        [HttpPost("insertSql")]
        public async Task<IActionResult> PostSql([FromBody] JObject obj)
        {
            var objFilterVariables = obj.SelectToken("variables").Children().OfType<JProperty>().ToDictionary(p => p.Name, p => p.Value);
            var column = objFilterVariables.Select(x => x.Key).ToList();
            var colValues = objFilterVariables.Select(x => (x.Value.Type == JTokenType.String) ? "'" + x.Value + "'" :
                                (x.Value.Type == JTokenType.Integer) ? x.Value :
                                    (x.Value.Type == JTokenType.Null) ? "NULL" : x.Value).ToList();
            var tableName = obj["table"];
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = $"INSERT INTO {tableName} ({string.Join(",", column)}) VALUES({string.Join(",", colValues)});select @@identity";
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    // return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                    Int64 id = 0;
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        if (await rd.ReadAsync())
                        {
                            id = Convert.ToInt64(rd[0].ToString());
                        }
                    }
                    return Ok(id);
                }
            }
        }

        [HttpPost("deleteSql")]
        public async Task<IActionResult> DeleteSql([FromBody] JObject obj)
        {

            var objFilterVariables = obj.SelectToken("variables").Children().OfType<JProperty>().ToDictionary(p => p.Name, p => p.Value);
            var column = objFilterVariables.Select(x => x.Key).ToList();
            var colValues = objFilterVariables.Select(x => (x.Value.Type == JTokenType.String) ? "'" + x.Value + "'" :
                                (x.Value.Type == JTokenType.Integer) ? x.Value :
                                    (x.Value.Type == JTokenType.Null) ? "NULL" : x.Value).ToList();

            var tableName = obj["table"];
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = $"DELETE FROM {tableName} WHERE {string.Join(",", column)} = {string.Join(",", colValues)} ";
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("updateSql")]
        public async Task<IActionResult> UpdateSql([FromBody] JObject obj)
        {
            var objFilterVariables = obj.SelectToken("variables").Children().OfType<JProperty>().ToDictionary(p => p.Name, p => p.Value);
            var column = objFilterVariables.Select(x => x.Key).ToList();
            var colValues = objFilterVariables.Select(x => (x.Value.Type == JTokenType.String) ? "'" + x.Value + "'" :
                                (x.Value.Type == JTokenType.Integer) ? x.Value :
                                    (x.Value.Type == JTokenType.Null) ? "NULL" : x.Value).ToList();

            var tableName = obj["table"];
            var where = obj["where"];

            if (column.Count != colValues.Count) return BadRequest();
            List<string> setList = new List<string>();
            for (var a = 0; a < column.Count; a++)
            {
                setList.Add(column[a] + "=" + colValues[a]);
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = $"UPDATE {tableName} SET {string.Join(",", setList)} {where}";
                    cmd.Connection = conn;
                    //return Ok(cmd.CommandText);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }

        }

        [HttpPost("insert-sql")]
        public async Task<IActionResult> InsertSql([FromBody] SqlVariable sql)
        {

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = $"INSERT INTO {sql.TableName} ({sql.Columns}) VALUES ({sql.ColumnValues} )";
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpDelete("delete-sql")]
        public async Task<IActionResult> DeleteSql1(SqlVariable sql)
        {
            //return Ok(sql)  ;
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = $"DELETE FROM {sql.TableName} {sql.WhereClause}";
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPut("update-sql")]
        public async Task<IActionResult> UpdateSql([FromBody] SqlVariable sql)
        {
            //return Ok(sql); 

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = $"UPDATE {sql.TableName} {sql.SetClause} {sql.WhereClause}";
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpGet("execute-sql/{sql}")]
        public async Task<IActionResult> ExecuteSql(string sql)
        {
            //return Ok(sql); 

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = sql;
                    cmd.Connection = conn;
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }
        [HttpGet("funding/package-purpose/list")]
        public async Task<IActionResult> GetPackagePurpose()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DataDomains WHERE Domain = 'PKGPURPOSE'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("funding/priority/list")]
        public async Task<IActionResult> GetPriority()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DataDomains WHERE Domain = 'ADMITPRIORITY'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }
          [HttpGet("funding/supliment/aria")]
        public async Task<IActionResult> GetFundingAria()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description,User1 FROM DataDomains WHERE Dataset = 'CDC' AND Description like 'ARIA SCORE %' ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                     List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                     while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Description = GenSettings.Filter(rd["Description"]),
                            User1 = GenSettings.Filter(rd["User1"])
                            
                        });
                    }
                    return Ok(list);

                    

                    // while (await rd.ReadAsync()) list.Add(new {
                    //     GenSettings.Filter(rd["Description"])
                    //     });
                    // return Ok(list);
                }
            }
        }
        [HttpGet("level-rate/{level}")]
        public async Task<IActionResult> GetLevelRate(string level)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT User1 FROM DataDomains WHERE Dataset = 'CDC' AND Description = @level", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@level", level);
                    await conn.OpenAsync();

                    dynamic list = null;
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    if (await rd.ReadAsync())
                    {
                        list = new
                        {
                            LevelRate = GenSettings.Filter(rd["User1"]),
                        };
                    }

                    return Ok(list);
                }
            }
        }



        [HttpGet("funding/packages/list/{personID}")]
        public async Task<IActionResult> GetPackageList(string personID)
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Name] FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= GETDATE()) AND [Name] NOT IN (SELECT [Program] FROM RecipientPrograms WHERE RecipientPrograms.PersonID = @personID) ORDER BY [NAME]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@personID", personID);

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Name"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("active-programs")]
        public async Task<IActionResult> GetActivePrograms()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER([Name]) as ProgramName FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= '02-21-2019') ORDER BY [ProgramName]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["ProgramName"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("leave/programs")]
        public async Task<IActionResult> GetLeavePrograms()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Name FROM HumanResourceTypes HR WHERE  [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= GETDATE())  ORDER BY [Name]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Name"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("programs/obj")]
        public async Task<IActionResult> GetLeaveProgramsObj()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Name FROM HumanResourceTypes HR WHERE  [HR].[Group] = 'PROGRAMS' AND ([HR].[EndDate] Is Null OR [HR].[EndDate] >= GETDATE())  ORDER BY [Name]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = (GenSettings.Filter(rd["Name"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }



        [HttpGet("categories/obj")]
        public async Task<IActionResult> GetCategoriesObj()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DataDomains WHERE Domain = 'GROUPAGENCY' ORDER BY Description ASC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Description = (GenSettings.Filter(rd["Description"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("reason-unavailability")]
        public async Task<IActionResult> GetReasonUnavailability()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Title FROM ItemTypes WHERE MinorGroup = 'LEAVE' AND ISNULL(EndDate, '2018/11/29') >= '2018/11/29'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Title"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("staff-category")]
        public async Task<IActionResult> GetStaffCategory()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT(Description) FROM DataDomains WHERE Domain = 'SVCNOTECAT' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("address-types")]
        public async Task<IActionResult> GetAdressTypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT(Description) FROM DataDomains WHERE Domain = 'ADDRESSTYPE' AND (EndDate Is Null or EndDate >= GETDATE()) ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }
        [HttpGet("postcodes-suburb")]
        public async Task<IActionResult> GetPostCodes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Recnum, Suburb, State, Postcode, Suburb + ' ' + Postcode as FullDetail FROM PCodes  ORDER BY Suburb", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["FullDetail"]));
                    return Ok(list);
                }
            }
        }


        [HttpGet("staff-Job-category-list")]
        public async Task<IActionResult> GetStaffCategoryList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT(Description) FROM DataDomains Where DOMAIN = 'STAFFGROUP' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            description = GenSettings.Filter(rd["Description"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("staff-discipline")]
        public async Task<IActionResult> GetStaffDiscipline()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT(Description) FROM DataDomains WHERE Domain = 'DISCIPLINE' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("staff-caredomain")]
        public async Task<IActionResult> GetStaffCareDomain()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT(Description) FROM DataDomains WHERE Domain = 'CAREDOMAIN' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }


        [HttpGet("resources")]
        public async Task<IActionResult> GetResources()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description FROM datadomains WHERE domain IN ( 'VEHICLES', 'ACTIVITYGROUPS' ) AND description IS NOT NULL AND ( enddate IS NULL OR enddate >= '01-18-2019' ) UNION SELECT DISTINCT [name] FROM cstdaoutlets WHERE [name] IS NOT NULL AND Isnull(enddate, '1899/12/31') > '' AND ( [name] NOT IN (SELECT DISTINCT rCtr.[name] AS Centre FROM cstdaoutlets rCtr INNER JOIN humanresources rX ON rX.personid = CONVERT(VARCHAR, rCtr.recordnumber) AND rX.[type] = 'CENTRE_STAFFX' WHERE rX.[name] = '') OR [name] IN (SELECT DISTINCT rCtr.[name] AS Centre 
                    FROM cstdaoutlets rCtr INNER JOIN humanresources rX ON rX.personid = CONVERT(VARCHAR, rCtr.recordnumber) AND rX.[type] = 'CENTRE_STAFFI' WHERE rX.[name] = '') ) AND [name] NOT IN (SELECT centre FROM (SELECT rco.[name] AS Competency, ctr.NAME AS Centre FROM humanresources rco INNER JOIN cstdaoutlets ctr ON rco.personid = CONVERT( VARCHAR, ctr.recordnumber) WHERE [type] = 'CENTRE_COMP' AND Isnull(useryesno1, 0) = 1 EXCEPT SELECT co.[name], 'x' FROM humanresources co INNER JOIN staff st ON personid = uniqueid AND [type] = 'STAFFATTRIBUTE' 
                    INNER JOIN datadomains coMaster ON co.[name] = coMaster.description WHERE st.accountno = '' AND ( Isnull(coMaster.undated, 0) = 1 OR Isnull(CONVERT(VARCHAR, co.date1, 106), '1900/01/01') > '' )) t) ORDER BY description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/awards/list")]
        public async Task<IActionResult> GetAwards()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select [Description] from datadomains Where DOMAIN = 'AWARDLEVEL'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/careplan/list")]
        public async Task<IActionResult> GetCarePlan()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select [Description] from datadomains Where DOMAIN = 'CAREPLANTYPES'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("goalplan/list")]
        public async Task<IActionResult> GetGoalPlan()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select DISTINCT [Description] from datadomains Where DOMAIN = 'CAREPLANTYPES'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/caredomain/list")]
        public async Task<IActionResult> GetCareDomain()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'CAREDOMAIN' ORDER BY [Description]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }


        [HttpGet("intake/discipline/list")]
        public async Task<IActionResult> GetDiscipline()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE' ORDER BY [Description]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("staff/dicipline/teams/list")]
        public async Task<IActionResult> GetDisciplineList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct [Description] FROM DATADOMAINS WHERE DOMAIN = 'DISCIPLINE' ORDER BY [Description]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            description = GenSettings.Filter(rd["Description"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/file-classification/list")]
        public async Task<IActionResult> GetFileClassification()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT distinct UPPER([Description]) AS [Description] FROM DATADOMAINS WHERE DOMAIN = 'FILECLASS' ORDER BY [Description]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/doc-category/list")]
        public async Task<IActionResult> GetDocumentCategory()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT distinct UPPER([Description]) AS [Description] FROM DATADOMAINS WHERE DOMAIN = 'DOCCAT' ORDER BY [Description]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["Description"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/template/list")]
        public async Task<IActionResult> GetTemplateAll()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title AS FileTypes FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY FileTypes", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["FileTypes"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/template/listall")]
        public async Task<IActionResult> GetTemplateListAll()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT IDENT_CURRENT('Documents') AS [DocID],recordNo,Title ,Template,MainGroup,MinorGroup,TraccsType FROM DOC_Associations WHERE (IsForm is null OR IsForm = 0) AND CanCreateFile = 1 AND TRACCSType = 'CAREPLAN' ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    // list.Add(GenSettings.Filter(rd["FileTypes"]));
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["recordNo"]),
                            DocID = GenSettings.Filter(rd["DocID"]),
                            Title = GenSettings.Filter(rd["Title"]),
                            Template = GenSettings.Filter(rd["Template"]),
                            MainGroup = GenSettings.Filter(rd["MainGroup"]),
                            MinorGroup = GenSettings.Filter(rd["MinorGroup"]),
                            TraccsType = GenSettings.Filter(rd["TraccsType"]),
                            Exists = System.IO.File.Exists(GenSettings.Filter(rd["Template"]))
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/recipients/all")]
        public async Task<IActionResult> GetRecipientAll()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT AccountNo FROM Recipients WHERE AccountNo > '!z'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    return Ok(list);
                }
            }
        }

        [HttpGet("pension/all")]
        public async Task<IActionResult> GetPensionAllList(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DATADOMAINS WHERE DOMAIN = 'PENSION'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["Description"])).ToUpper());
                    return Ok(list);
                }
            }
        }

        [HttpGet("pension/{personID}")]
        public async Task<IActionResult> GetPensionList(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DATADOMAINS WHERE DOMAIN = 'PENSION' AND DESCRIPTION NOT IN (
                    SELECT [Name] AS Description FROM HumanResources WHERE PersonID = @personID AND [Name] = Description AND [Group] = 'PENSION') ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["Description"])).ToUpper());
                    return Ok(list);
                }
            }
        }

        [HttpGet("loans/types")]
        public async Task<IActionResult> GetLaontypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT ItemID as Title FROM EQUIPMENT  ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Title = GenSettings.Filter(rd["Title"])
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("loans/goods")]
        public async Task<IActionResult> GetLaonGoods()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER(Description) as Title FROM DATADOMAINS WHERE DOMAIN = 'GOODS' order by Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Title = GenSettings.Filter(rd["Title"])
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("competencies/all")]
        public async Task<IActionResult> GetCompetencies()
        {
            return Ok(await _context.DataDomains.Where(x => x.Domain == "STAFFATTRIBUTE").OrderBy(x => x.Description).ToListAsync());
        }

        [HttpGet("attributes-list/all")]
        public async Task<IActionResult> GetCompetenciesAll()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DATADOMAINS WHERE Domain = 'STAFFATTRIBUTE' AND User5 = 'ATTRIBUTE' AND (EndDate Is Null OR EndDate >= '12-30-2024') ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<string> list = new List<string>();

                    while (await rd.ReadAsync())
                        list.Add(GenSettings.Filter(rd.GetString(0)));

                    return Ok(list);
                }
            }
        }


        [HttpGet("intake/services/programs/{personID}")]
        public async Task<IActionResult> GetIntakePrograms(string personID)
        {

            return Ok(await (from rp in _context.RecipientPrograms
                             where rp.PersonID == personID && rp.ProgramStatus == "ACTIVE"
                             select rp.Program.ToUpper()).OrderBy(x => x).ToListAsync());
        }

        [HttpGet("peronal/active/programs/{personID}")]
        public async Task<IActionResult> GetPersonalActivePrograms(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Program FROM RecipientPrograms WHERE RecipientPrograms.PersonID = @personID AND ProgramStatus = 'ACTIVE'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Program"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/activity/{personID}/{program}/{date}")]
        public async Task<IActionResult> GetIntakeActivity(string personID, string program, string date)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [Service Type] AS Activity FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT 
                    ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID WHERE HRT.[Name] = @program AND EXISTS (SELECT Title FROM ItemTypes ITM WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] 
                    NOT IN ('ADMISSION', 'ADMINISTRATION') AND (ITM.EndDate Is Null OR ITM.EndDate >= @date)) AND (SO.[Service Type] NOT IN (SELECT [Service Type] FROM ServiceOverview WHERE PersonID = @personID 
                    AND ServiceProgram = @program))", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    cmd.Parameters.AddWithValue("@program", program);
                    cmd.Parameters.AddWithValue("@date", date);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Activity"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical/nursingdiagnose/{personID}")]
        public async Task<IActionResult> GetClinicalnursingDiagnose(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, ICDCODE,Code, Description FROM NDIAGNOSIS WHERE PersonID = @personID ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                ICDCODE = GenSettings.Filter(rd["ICDCODE"]),
                                Code = GenSettings.Filter(rd["Code"]),
                                Description = GenSettings.Filter(rd["Description"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("clinical/addnursingdiagnose/{personID}")]
        public async Task<IActionResult> GetAddClinicalnursingDiagnose(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM NDIAGNOSISTYPES WHERE Description NOT IN (SELECT Description FROM NDIAGNOSIS WHERE PersonID = @personID AND NDIAGNOSIS.Description = NDIAGNOSISTYPES.Description) ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Description = GenSettings.Filter(rd["Description"]),
                               
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical/medicationdiagnose/{personID}")]
        public async Task<IActionResult> GetClinicalmedicationDiagnose(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, ICDCODE,Code, Description FROM MDIAGNOSIS WHERE PersonID = @personID ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                ICDCODE = GenSettings.Filter(rd["ICDCODE"]),
                                Code = GenSettings.Filter(rd["Code"]),
                                Description = GenSettings.Filter(rd["Description"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("clinical/addmedicationdiagnose/{personID}")]
        public async Task<IActionResult> GetAddClinicalmedicationDiagnose(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Description FROM MDIAGNOSISTYPES WHERE Description NOT IN (SELECT Description FROM MDIAGNOSIS WHERE PersonID = @personID AND MDIAGNOSIS.Description = MDIAGNOSISTYPES.Description) ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Description = GenSettings.Filter(rd["Description"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("clinical/procedure/{personID}")]
        public async Task<IActionResult> GetClinicalProcedure(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, ICDCODE,Code, Description FROM MPROCEDURES WHERE PersonID = @personID ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                ICDCODE = GenSettings.Filter(rd["ICDCODE"]),
                                Code = GenSettings.Filter(rd["Code"]),
                                Description = GenSettings.Filter(rd["Description"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical/medications/{personID}")]
        public async Task<IActionResult> GetClinicalmedications(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, Description FROM ONIMedications WHERE PersonID = @personID", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                Description = GenSettings.Filter(rd["Description"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }




        [HttpGet("clinical/reminder/{personID}")]
        public async Task<IActionResult> GetClinicalReminder(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT
                 RecordNumber, PersonID,Name As Alert, Date1 As [Reminder Date], Date2 as [Expiry Date],
                 Notes,Address1,Address2,Email, Recurring,SameDate,SameDay,State
                 FROM HumanResources 
                 WHERE PersonID = @personID AND [Type] = 'CLINICALREMIND' AND
                 IsNull(ReminderScope, '') <> 'CC' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Alert = GenSettings.Filter(rd["Alert"]).ToUpper(),
                                ReminderDate = GenSettings.Filter(rd["Reminder Date"]),
                                DueDate = GenSettings.Filter(rd["Expiry Date"]),
                                Notes = GenSettings.Filter(rd["Notes"]),
                                PersonID = GenSettings.Filter(rd["PersonID"]),
                                Recurring = GenSettings.Filter(rd["Recurring"]),
                                SameDate = GenSettings.Filter(rd["SameDate"]),
                                SameDay = GenSettings.Filter(rd["SameDay"]),
                                Address1 = GenSettings.Filter(rd["Address1"]),
                                Address2 = GenSettings.Filter(rd["Address2"]),
                                Email = GenSettings.Filter(rd["Email"]),
                                State = GenSettings.Filter(rd["State"]),
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"])
                            });

                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical/alert/{personID}")]
        public async Task<IActionResult> GetClinicalAlert(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT
                 RecordNumber, PersonID,Name As Alert, Date1 As [Reminder Date], Date2 as [Expiry Date],
                 Notes,Address1,Address2,Email, Recurring,SameDate,SameDay,State
                 FROM HumanResources
                 WHERE PersonID = @personID AND [Type] = 'CLINICALALERT' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Alert = GenSettings.Filter(rd["Alert"]).ToUpper(),
                                ReminderDate = GenSettings.Filter(rd["Reminder Date"]),
                                DueDate = GenSettings.Filter(rd["Expiry Date"]),
                                Notes = GenSettings.Filter(rd["Notes"]),
                                PersonID = GenSettings.Filter(rd["PersonID"]),
                                Recurring = GenSettings.Filter(rd["Recurring"]),
                                SameDate = GenSettings.Filter(rd["SameDate"]),
                                SameDay = GenSettings.Filter(rd["SameDay"]),
                                Address1 = GenSettings.Filter(rd["Address1"]),
                                Address2 = GenSettings.Filter(rd["Address2"]),
                                Email = GenSettings.Filter(rd["Email"]),
                                State = GenSettings.Filter(rd["State"]),
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical/notes/{personID}")]
        public async Task<IActionResult> GetClinicalNotes(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, PersonID, WhoCode, DetailDate,dbo.RTF2Text(Detail) AS Detail, ' ' As Blank,
                    AlarmDate, 16447220 AS kCOLOR_WASH1,ExtraDetail2 as Category,extraDetail2,Program, PrivateFlag, Discipline, CareDomain,
                    16182760 AS kCOLOR_WASH, 15522770 AS kCOLOR_PAGEHEADER, 14928315 AS kCOLOR_SCREENHEADER,Restrictions,
                    Creator  FROM History WHERE (PersonID = @personID) AND (ExtraDetail1 = 'CLINNOTE') AND (DeletedRecord <> 1)
                    ORDER BY DetailDate DESC, RecordNumber DESC", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                                WhoCode = GenSettings.Filter(rd["WhoCode"]),
                                DetailDate = (DateTime)(rd["DetailDate"] == DBNull.Value ? "" : rd["DetailDate"]),
                                Detail = GenSettings.Filter(rd["Detail"]),
                                Blank = GenSettings.Filter(rd["Blank"]),
                                AlarmDate = GenSettings.Filter(rd["AlarmDate"]),
                                Restrictions = GenSettings.Filter(rd["Restrictions"]),
                                Discipline = GenSettings.Filter(rd["Discipline"]),
                                CareDomain = GenSettings.Filter(rd["CareDomain"]),
                                Program = GenSettings.Filter(rd["Program"]),
                                PrivateFlag = GenSettings.Filter(rd["PrivateFlag"]),
                                extraDetail2 = GenSettings.Filter(rd["extraDetail2"]),
                                Creator = GenSettings.Filter(rd["Creator"]),
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/staff/{personID}")]
        public async Task<IActionResult> GetIntakeStaff(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT AccountNo FROM Staff WHERE AccountNo > '!z' AND CommencementDate is Not Null AND TerminationDate is Null AND AccountNo NOT IN(
                    SELECT [Name] FROM HumanResources WHERE PersonID = @personID  AND [Name] = AccountNo AND [Group] = 'EXCLUDEDSTAFF' UNION
                        SELECT [Name] FROM HumanResources WHERE PersonID = @personID AND [Group] = 'INCLUDEDSTAFF' 
                    ) ORDER BY AccountNo", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("centerLocation/staff")]
        public async Task<IActionResult> GetcenterLocationIntakeStaff()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT AccountNo FROM Staff WHERE AccountNo > '!z' AND CommencementDate is Not Null AND TerminationDate is Null AND AccountNo NOT IN(
                    SELECT [Name] FROM HumanResources  WHERE [Name] = AccountNo AND [Group] = 'CENTRE_STAFFX' UNION
                        SELECT [Name] FROM HumanResources WHERE [Group] = 'CENTRE_STAFFI'
                    ) ORDER BY AccountNo", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("Users/TraccsStaffCodes")]
        public async Task<IActionResult> GetTraccsStaffCodes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT AccountNo FROM Staff WHERE CommencementDate is Not Null AND TerminationDate is Null AND AccountNo > '!z' ORDER BY AccountNo", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("Users/TraccsStaffGroups")]
        public async Task<IActionResult> GetTraccsStaffGroups()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct AccountNo from Staff WHERE AccountNo > '0' AND AccountNo <> '!INTERNAL' AND AccountNo <> '!MULTIPLE' AND (((CommencementDate IS NULL) AND (TerminationDate IS NULL)) OR  ((CommencementDate IS NOT NULL) AND (TerminationDate IS NULL)))", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("Users/TraccsClientCodes")]
        public async Task<IActionResult> GetTraccsClientCodes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT AccountNo FROM Recipients WHERE AdmissionDate is Not Null AND DischargeDate is Null AND AccountNo > '!z' ORDER BY AccountNo", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("intake/competencies/{personID}")]
        public async Task<IActionResult> GetIntakeCompetencies(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DATADOMAINS WHERE DOMAIN = 'STAFFATTRIBUTE' AND DESCRIPTION NOT IN (
	                SELECT [Name] AS Description FROM HumanResources WHERE PersonID = @personID AND [Name] = Description AND [Group] = 'RECIPATTRIBUTE') ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("intake/goals/{personID}")]
        public async Task<IActionResult> GetGoals(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DATADOMAINS WHERE DOMAIN = 'GOALOFCARE' AND DESCRIPTION NOT IN (
	                SELECT [User1] AS Description FROM HumanResources WHERE PersonID = @personID AND [User1] = Description AND [Group] = 'RECIPIENTGOALS') ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("intake/branches/{personID}")]
        public async Task<IActionResult> GetIntakeBranches(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DATADOMAINS WHERE DOMAIN = 'BRANCHES' AND DESCRIPTION NOT IN (
	                SELECT [Name] AS Description FROM HumanResources WHERE PersonID = @personID AND [Name] = Description AND [Group] = 'RECIPBRANCHES') ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("loan/items")]
        public async Task<IActionResult> GetLoanItems()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Type], ItemID FROM EQUIPMENT ORDER BY ItemID", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Type = GenSettings.Filter(rd["Type"]),
                                Item = GenSettings.Filter(rd["ItemID"])
                            });
                        }
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("loan/programs")]
        public async Task<IActionResult> GetLoanPrograms()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER([Name]) as ProgramName FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= Getdate())  ORDER BY [ProgramName]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (var rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["ProgramName"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("loan/{personID}")]
        public async Task<IActionResult> PostLoan([FromBody] Loans loans, string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HumanResources(PersonID, [Group],[Type],[Name], Address1, Date1, Date2, DateInstalled, Notes, Creator, ReminderCreator, SubType, User2) 
                    VALUES(@personID, 'LOANITEMS',@type,@item,@program,@loanDate,@collectedDate,@dateInstalled,@notes,'SYSMGR','SYSMGR','LOANITEMS','INVOICE')", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    cmd.Parameters.AddWithValue("@type", loans.Type.ToUpper());
                    cmd.Parameters.AddWithValue("@item", loans.Item);
                    cmd.Parameters.AddWithValue("@program", loans.Program);
                    cmd.Parameters.AddWithValue("@loanDate", loans.Loaned ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@collectedDate", loans.Collected ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@dateInstalled", loans.DateInstalled ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", loans.Notes);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);
                }
            }
        }

        [HttpPost("loan/recipient/{personID}")]
        public async Task<IActionResult> PostRecipientLoan([FromBody] Loans loans, string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"INSERT INTO HumanResources(PersonID, [Group],[Type],[Name], Address1, Date1, Date2, DateInstalled, Notes, Creator, ReminderCreator, SubType, User2,
                                                    EquipmentCode,SerialNumber,LockBoxCode,LockBoxLocation,PurchaseAmount,PurchaseDate) 
                                                    VALUES(@personID, 'LOANITEMS',@type,@item,@program,@loanDate,@collectedDate,@dateInstalled,@notes,'SYSMGR','SYSMGR','LOANITEMS','INVOICE'
                                                    ,@EquipmentCode,@SerialNumber,@LockBoxCode,@LockBoxLocation,@PurchaseAmount,@PurchaseDate)", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    cmd.Parameters.AddWithValue("@type", loans.Type.ToUpper());
                    cmd.Parameters.AddWithValue("@item", loans.Item);
                    cmd.Parameters.AddWithValue("@program", loans.Program);
                    cmd.Parameters.AddWithValue("@loanDate", loans.Loaned ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@collectedDate", loans.Collected ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@dateInstalled", loans.DateInstalled ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", loans.Notes);
                    cmd.Parameters.AddWithValue("@EquipmentCode", loans.EquipmentCode);
                    cmd.Parameters.AddWithValue("@SerialNumber", loans.SerialNumber);
                    cmd.Parameters.AddWithValue("@LockBoxCode", loans.LockBoxCode);
                    cmd.Parameters.AddWithValue("@LockBoxLocation", loans.LockBoxLocation);
                    cmd.Parameters.AddWithValue("@PurchaseAmount", loans.PurchaseAmount);
                    cmd.Parameters.AddWithValue("@PurchaseDate", loans.PurchaseDate);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);

                }
            }
        }
        [HttpPut("loan/recipient/{personID}")]
        public async Task<IActionResult> UpdateRecipientLoan([FromBody] Loans loans, string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"update  HumanResources set PersonID=@personID,[Type]=@type,[Name]=@item, Address1=@program, Date1=@loanDate, Date2=@collectedDate,
                                                    DateInstalled=@dateInstalled, Notes=@notes,EquipmentCode=@EquipmentCode,SerialNumber=@SerialNumber,LockBoxCode=@LockBoxCode,LockBoxLocation=@LockBoxLocation
                                                    ,PurchaseAmount=@PurchaseAmount,PurchaseDate=@PurchaseDate
                                                    where RecordNumber=@RecordNumber", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    cmd.Parameters.AddWithValue("@type", loans.Type.ToUpper());
                    cmd.Parameters.AddWithValue("@item", loans.Item);
                    cmd.Parameters.AddWithValue("@program", loans.Program);
                    cmd.Parameters.AddWithValue("@loanDate", loans.Loaned ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@collectedDate", loans.Collected ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@dateInstalled", loans.DateInstalled ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@notes", loans.Notes);
                    cmd.Parameters.AddWithValue("@EquipmentCode", loans.EquipmentCode);
                    cmd.Parameters.AddWithValue("@SerialNumber", loans.SerialNumber);
                    cmd.Parameters.AddWithValue("@LockBoxCode", loans.LockBoxCode);
                    cmd.Parameters.AddWithValue("@LockBoxLocation", loans.LockBoxLocation);
                    cmd.Parameters.AddWithValue("@PurchaseAmount", loans.PurchaseAmount);
                    cmd.Parameters.AddWithValue("@PurchaseDate", loans.PurchaseDate);
                    cmd.Parameters.AddWithValue("@RecordNumber", loans.RecordNumber);
                    await conn.OpenAsync();
                    return Ok(await cmd.ExecuteNonQueryAsync() > 0);

                }
            }
        }

        [HttpGet("recipient/reminders")]
        public async Task<IActionResult> GetRecipientReminders()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DataDomains WHERE Domain = 'RECIPIENTALERT' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    return Ok(list);
                }
            }
        }

        [HttpGet("recipient/remindersObj")]
        public async Task<IActionResult> GetRecipientRemindersObj()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DataDomains WHERE Domain = 'RECIPIENTALERT' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            description = GenSettings.Filter(rd["DESCRIPTION"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("customdatasetObj")]
        public async Task<IActionResult> customdatasetObj()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DataDomains WHERE Domain = 'CUSTOM DATASETS' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            description = GenSettings.Filter(rd["DESCRIPTION"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("quote/goalofcare")]
        public async Task<IActionResult> GetGoalOfCare()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Description] FROM DataDomains WHERE Domain = 'GOALOFCARE' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add(GenSettings.Filter(rd["DESCRIPTION"]));
                    return Ok(list);
                }
            }
        }

        [HttpGet("reminders")]
        public async Task<IActionResult> GetReminders()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DATADOMAINS WHERE Domain = 'STAFFALERT' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("branches")]
        public async Task<IActionResult> GetBranches()
        {

            return Ok(await (from dd in _context.DataDomains
                             where dd.Domain == "BRANCHES"
                             select dd.Description).OrderBy(x => x).ToListAsync());
        }

        [HttpGet("branchesObj")]
        public async Task<IActionResult> GetbranchesObj()
        {
            using (var conn = _context.Database.GetDbConnection())
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'BRANCHES' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            description = GenSettings.Filter(rd["DESCRIPTION"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("staffgroup")]
        public async Task<IActionResult> GetStaffGroup()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'STAFFGROUP' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("staffadmin")]
        public async Task<IActionResult> GetStaffAdmin()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'STAFFADMINCAT'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("staffteam")]
        public async Task<IActionResult> GetStaffTeam()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'STAFFTEAM'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["DESCRIPTION"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("employeeOf")]
        public async Task<IActionResult> GetStaffBrok()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select distinct AccountNo from STAFF WHERE Left(Category, 4) = 'BROK'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["AccountNo"]).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("specific-email-manager/{accountno}")]
        public async Task<IActionResult> GetSpecificEmailManager(string accountno)
        {
            string email = string.Empty;

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();

                using (SqlCommand cmd2 = new SqlCommand(@"SELECT TOP 1 pf.Detail FROM ( 
                    SELECT d.RecordNumber, d.[Description], s.UniqueID FROM Staff s 
                    RIGHT JOIN DataDomains d ON d.[HACCCode] = s.[AccountNo] WHERE d.[domain] = 'CASE MANAGERS' AND HACCCode = @accountNo
                    ) sr 
                    LEFT JOIN PhoneFaxOther pf ON sr.[UniqueID] = pf.[PersonID] AND 
                    pf.[Type] = '<EMAIL>'", (SqlConnection)conn))
                {
                    cmd2.Parameters.AddWithValue("@accountNo", accountno);
                    using (var rd1 = await cmd2.ExecuteReaderAsync())
                    {
                        if (await rd1.ReadAsync()) email = GenSettings.Filter(rd1["Detail"]);
                    }
                }
            }
            return Ok(email);
        }

        [HttpGet("specific-branch/{personid}")]
        public async Task<IActionResult> GetSpecificBranch(string personid)
        {
            var branch = await (from rec in _context.Recipients
                                where rec.UniqueID == personid
                                select new
                                {
                                    Branch = rec.Branch,
                                    Coordinator = rec.Recipient_Coordinator
                                })
                            .FirstOrDefaultAsync();
            return Ok(branch);
        }

        [HttpGet("primary-phone-address")]
        public async Task<IActionResult> GetPrimaryPhoneAndAddress(ProgramTypeDto program)
        {
            string phoneNo = string.Empty;
            string address = string.Empty;
            string level = string.Empty;

            string name = await (from rec in _context.Recipients where rec.UniqueID == program.PersonId select rec.FirstName + ' ' + rec.SurnameOrg).FirstOrDefaultAsync();

            var detailPhonePrimary = await (from pf in _context.PhoneFaxOther
                                            where pf.PersonID == program.PersonId
                                            select pf).ToListAsync();

            var detailAddressPrimary = await (from pf in _context.NamesAndAddresses
                                              where pf.PersonID == program.PersonId
                                              select pf).ToListAsync();

           level = await (from ht in _context.HumanResourceTypes
            where ht.Name == program.Name
            select ht.User3).FirstOrDefaultAsync();

             
            string sql = "";

            if (detailPhonePrimary.Count == 1)
            {
                phoneNo = detailPhonePrimary.Select(x => x.Detail).FirstOrDefault();
            }
            else
            {
                phoneNo = await (from pf in _context.PhoneFaxOther
                                 where pf.PersonID == program.PersonId && pf.PrimaryPhone == true
                                 select pf.Detail).FirstOrDefaultAsync();
            }


            if (detailAddressPrimary.Count == 1)
            {
                sql = @"SELECT CASE WHEN Address1 <> ' ' THEN Address1 ELSE ' ' END + CASE WHEN Address2 <> ' ' 
                        THEN ' ' + Address2 ELSE ' ' END + CASE WHEN Suburb <> ' ' THEN ' ' + Suburb ELSE ' ' END + 
                        CASE WHEN Postcode <> ' ' THEN ' ' + Postcode ELSE ' ' END AS FullAddress  
                        FROM NamesAndAddresses WHERE PersonID = @personid";
            }
            else
            {
                sql = @"SELECT CASE WHEN Address1 <> ' ' THEN Address1 ELSE ' ' END + CASE WHEN Address2 <> ' ' 
                        THEN ' ' + Address2 ELSE ' ' END + CASE WHEN Suburb <> ' ' THEN ' ' + Suburb ELSE ' ' END + 
                        CASE WHEN Postcode <> ' ' THEN ' ' + Postcode ELSE ' ' END AS FullAddress  
                        FROM NamesAndAddresses WHERE PersonID = @personid AND PrimaryAddress = 1";
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                await conn.OpenAsync();

                using (SqlCommand cmd2 = new SqlCommand(sql, (SqlConnection)conn))
                {
                    cmd2.Parameters.AddWithValue("@personid", program.PersonId);
                    using (var rd1 = await cmd2.ExecuteReaderAsync())
                    {
                        if (await rd1.ReadAsync()) address = GenSettings.Filter(rd1["FullAddress"]);
                    }
                }
            }

            string result = $"{program.Type}/INQUIRY FROM: {name} {program.otherInfo} {level}\rPhone: {phoneNo}\rAddress: {address}\r\rNOTES:";

            if (program.Type == "ADMISSION")
                result = $"{program.Type}: {name} {program.otherInfo} {level}\rPhone: {phoneNo}\rAddress: {address}\r\rNOTES:";
            return Ok(result);
        }

        [HttpGet("specific-type/{program}")]
        public async Task<IActionResult> GetSpecificType(string program)
        {
            var type = await (from hr in _context.HumanResourceTypes
                              where hr.Name == program
                              select hr.Type).FirstOrDefaultAsync();

            return Ok(type);
        }

        [HttpGet("staff/activities")]
        public async Task<IActionResult> GetStaffActivities()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                   SELECT DISTINCT [title] AS Activity
                        FROM   itemtypes
                        WHERE  processclassification = 'OUTPUT'
                               AND [rostergroup] IN ( 'ADMISSION' )
                               AND [minorgroup] IN ( 'STAFF ONBOARDING' )
                               AND ( enddate IS NULL
                                      OR enddate >= Getdate() )
                        ORDER  BY [title] 
                    ", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Activity"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("notifications")]
        public async Task<IActionResult> GetNotifications(string branch, string coordinator, string fundingsource, string listname = "Referral Notification")
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                //SELECT StaffToNotify, ISNULL([Mandatory], 0) AS Mandatory FROM IM_DistributionLists where Branch IN(@branch, 'ALL') AND Coordinator IN(@coordinator, 'ALL') AND ListName = 'NOTIFICATION_REFERRAL'
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT DISTINCT id.stafftonotify,
                           Isnull([mandatory], 0) AS Mandatory,
                           pf.detail              AS Email
                    FROM   im_distributionlists id
                           JOIN staff st
                             ON st.accountno = id.stafftonotify
                           JOIN phonefaxother pf
                             ON pf.personid = st.uniqueid
                    WHERE  branch IN ( @branch, 'ALL' )
                           --AND coordinator IN ( @coordinator, 'ALL' )
                           AND id.listname IN (@listname,'ALL')
                           AND id.listgroup IN (@fundingsource,'ALL')
                           AND Isnull(id.xdeletedrecord, 0) = 0
                           AND pf.type = '<EMAIL>'
                            OR pf.type = 'EMAIL'
                    ", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@branch", branch);
                    cmd.Parameters.AddWithValue("@coordinator", coordinator);
                    cmd.Parameters.AddWithValue("@listname", listname);
                    cmd.Parameters.AddWithValue("@fundingsource", fundingsource);


                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            StaffToNotify = (GenSettings.Filter(rd["StaffToNotify"])).ToUpper(),
                            Mandatory = Convert.ToBoolean((GenSettings.Filter(rd["Mandatory"]))),
                            Email = GenSettings.Filter(rd["Email"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("staff-competency-list")]
        public async Task<IActionResult> GetStaffCompetencyList(ProgramInputs inputs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT DISTINCT [name] AS Competency
                    FROM   humanresources
                    WHERE  personid = 'W2'
                    AND [group] = @group
                    AND [type] = @type
                    AND [user2] IN ('ALL', @branch)
                    AND [user3] IN('ALL', @category)", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@branch", inputs.Branch);
                    cmd.Parameters.AddWithValue("@type", inputs.Type);
                    cmd.Parameters.AddWithValue("@category", inputs.FundingType);
                    cmd.Parameters.AddWithValue("@group", inputs.Group);

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Competency"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("documents-list")]
        public async Task<IActionResult> GetDocumentsIntake(ProgramInputs inputs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [name] AS Document
        FROM   humanresources
        WHERE  personid = 'W1'
       AND [group] IN (@group,'ALL')
	   AND [type] IN (@type,'ALL')
	   AND [user2] IN (@branch,'ALL')
	   AND [user3] IN (@fundingtype,'ALL')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@branch", inputs.Branch);
                    cmd.Parameters.AddWithValue("@type", inputs.Type);
                    cmd.Parameters.AddWithValue("@fundingType", inputs.FundingType);
                    cmd.Parameters.AddWithValue("@group", inputs.Group);

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Document"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("staff-documents-list")]
        public async Task<IActionResult> GetStaffDocumentsIntake(ProgramInputs inputs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [name] AS Document
        FROM   humanresources
        WHERE  personid = 'W2'
       AND [group] IN (@group,'ALL')
	   AND [type] IN (@type,'ALL')
	   AND [user2] IN (@branch,'ALL')
	   AND [user3] IN (@fundingtype,'ALL')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@branch", inputs.Branch);
                    cmd.Parameters.AddWithValue("@type", inputs.Type);
                    cmd.Parameters.AddWithValue("@fundingType", inputs.FundingType);
                    cmd.Parameters.AddWithValue("@group", inputs.Group);

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Document"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }



        [HttpGet("data-list")]
        public async Task<IActionResult> GetDataIntake(ProgramInputs inputs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT DISTINCT [name]   AS Form,
                           address1 AS Link
                    FROM   humanresources
                    WHERE  personid = 'W1'
                           AND [group] IN (@group,'ALL') 
                           AND [type] IN (@type,'ALL')
	                       AND [user2] IN( @branch,'ALL')
	                       AND [user3] IN (@fundingtype,'ALL')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@branch", inputs.Branch);
                    cmd.Parameters.AddWithValue("@type", inputs.Type);
                    cmd.Parameters.AddWithValue("@fundingType", inputs.FundingType);
                    cmd.Parameters.AddWithValue("@group", inputs.Group);

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Form = (GenSettings.Filter(rd["Form"])).ToUpper(),
                            Link = (GenSettings.Filter(rd["Link"])).ToUpper()
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("followups")]
        public async Task<IActionResult> GetFollowUps(ProgramInputs inputs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT DISTINCT [name]                                       AS Reminder,
                           Dateadd(day, CONVERT(INT, user1), Getdate()) AS FollowUpDate,
                            User1
                    FROM   humanresources
                    WHERE  personid = 'W1'
                           AND [group] IN (@group, 'ALL')
                           AND [type] IN ( @type,'ALL')
	                       AND [user2] IN( @branch, 'ALL')
	                       AND [user3] IN (@fundingType ,'ALL')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@branch", inputs.Branch);
                    cmd.Parameters.AddWithValue("@type", inputs.Type);
                    cmd.Parameters.AddWithValue("@fundingType", inputs.FundingType);
                    cmd.Parameters.AddWithValue("@group", inputs.Group);

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Reminders = (GenSettings.Filter(rd["Reminder"])).ToUpper(),
                            User1 = (GenSettings.Filter(rd["User1"]))
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("staff-followups")]
        public async Task<IActionResult> GetStaffFollowUps(ProgramInputs inputs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT DISTINCT [name] AS Reminder,
                           Dateadd(day, CONVERT(INT, user1), Getdate()) AS FollowUpDate,
                            User1
                    FROM   humanresources
                    WHERE  personid = 'W2'
                           AND [group] IN (@group, 'ALL')
                           AND [type] IN ( @type,'ALL')
	                       AND [user2] IN( @branch, 'ALL')
	                       AND [user3] IN (@fundingType ,'ALL')", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@branch", inputs.Branch);
                    cmd.Parameters.AddWithValue("@type", inputs.Type);
                    cmd.Parameters.AddWithValue("@fundingType", inputs.FundingType);
                    cmd.Parameters.AddWithValue("@group", inputs.Group);

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Reminders = (GenSettings.Filter(rd["Reminder"])).ToUpper(),
                            User1 = (GenSettings.Filter(rd["User1"]))
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("casemanagers")]
        public async Task<IActionResult> GetCaseManagers()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'CASE MANAGERS' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("casemanagerslist")]
        public async Task<IActionResult> GetCaseManagersList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'CASE MANAGERS' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            description = GenSettings.Filter(rd["Description"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpDelete("quote-lines/{recordNo}")]
        public async Task<IActionResult> DeleteQuoteLine(int recordNo)
        {
            try
            {
                var quoteLine = await (from lne in _context.Qte_Lne where lne.RecordNumber == recordNo select lne).FirstOrDefaultAsync();
                _context.Qte_Lne.Remove(quoteLine);
                await _context.SaveChangesAsync();
                return Ok(true);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("quote-line/list/{recordNo}")]
        public async Task<IActionResult> GetQuoteLines(int recordNo)
        {
            var quoteLine = await (from lne in _context.Qte_Lne where lne.Doc_Hdr_Id == recordNo select lne).ToListAsync();
            return Ok(quoteLine);
        }

        [HttpPut("quotes-line-details/{recordNo}")]
        public async Task<IActionResult> UpdateQuoteLineDetails([FromBody] QuoteLineDTO _quoteLines, int recordNo)
        {

            var quoteLine = await (from lne in _context.Qte_Lne where lne.RecordNumber == recordNo select lne).OrderBy(lne => lne.RecordNumber).FirstOrDefaultAsync();

            if (quoteLine == null)
            {
                return BadRequest();
            }

            quoteLine.BillUnit = _quoteLines.BillUnit;
            quoteLine.Qty = _quoteLines.Qty;
            quoteLine.DisplayText = _quoteLines.DisplayText;
            quoteLine.Frequency = _quoteLines.Frequency;
            quoteLine.LengthInWeeks = _quoteLines.LengthInWeeks;
            quoteLine.Roster = _quoteLines.Roster;
            quoteLine.ItemId = (from item in _context.ItemTypes where item.Title == _quoteLines.ServiceType select item.Recnum).FirstOrDefault();
            quoteLine.UnitBillRate = _quoteLines.UnitBillRate;
            quoteLine.SortOrder = _quoteLines.SortOrder;
            quoteLine.QuoteQty = _quoteLines.QuoteQty;


            await _context.SaveChangesAsync();

            QuoteLineDTO newQuote = new QuoteLineDTO()
            {
                Code = _quoteLines.Code,
                DisplayText = quoteLine.DisplayText,
                Qty = quoteLine.Qty,
                QuoteQty = quoteLine.QuoteQty,
                Frequency = quoteLine.Frequency,
                LengthInWeeks = quoteLine.LengthInWeeks,
                Tax = quoteLine.Tax,
                BillUnit = quoteLine.BillUnit,
                UnitBillRate = quoteLine.UnitBillRate,
                RecordNumber = quoteLine.RecordNumber
            };

            return Ok(newQuote);
        }

        [HttpGet("quotes-line-details/{recordNo}")]
        public async Task<IActionResult> GetQuoteLineDetails(int recordNo)
        {

            var results = await (from ql in _context.Qte_Lne
                                 join itm in _context.ItemTypes on ql.ItemId equals itm.Recnum
                                 where ql.RecordNumber == recordNo
                                 select new
                                 {
                                     Doc_Hdr_Id = ql.Doc_Hdr_Id,
                                     RecordNumber = ql.RecordNumber,
                                     Title = itm.Title,
                                     RosterGroup = itm.RosterGroup,
                                     MainGroup = GenSettings.DetermineMainGroup(itm.RosterGroup, itm.MainGroup),
                                     BudgetGroup = itm.BudgetGroup,
                                     DisplayText = ql.DisplayText,
                                     Qty = ql.Qty,
                                     Frequency = ql.Frequency,
                                     LengthInWeeks = ql.LengthInWeeks,
                                     QuoteQty = ql.QuoteQty,
                                     BillUnit = ql.BillUnit,
                                     Rate = ql.UnitBillRate,
                                     Tax = ql.Tax,
                                     PriceType = ql.PriceType,
                                     QuotePerc = ql.QuotePerc,
                                     BudgetPerc = ql.BudgetPerc,
                                     Notes = ql.Notes,
                                     Roster = ql.Roster,
                                     SortOrder = ql.SortOrder,
                                     StrategyID = ql.StrategyId
                                 }).FirstOrDefaultAsync();

            return Ok(results);

            //            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            //            {
            //                using (SqlCommand cmd = new SqlCommand(@"SELECT ql.doc_hdr_id,
            //       ql.recordnumber,
            //       itm.title,
            //       itm.rostergroup,
            //       itm.maingroup,
            //       itm.budgetgroup,
            //       ql.displaytext,
            //       ql.qty,
            //       ql.frequency,
            //       ql.lengthinweeks,
            //       ql.quoteqty,
            //       ql.[bill unit]                 AS BillUnit,
            //       ql.[unit bill rate]            AS Rate,
            //       ql.tax,
            //       ql.pricetype,
            //       ql.quoteperc,
            //       ql.budgetperc,
            //       ql.notes,
            //       CONVERT(VARCHAR (364), roster) AS Roster,
            //       sortorder,
            //       strategyid
            //FROM   qte_lne ql
            //       INNER JOIN itemtypes itm
            //               ON itm.recnum = ql.itemid
            //WHERE  ql.recordnumber = @recordNo", (SqlConnection)conn))
            //                {
            //                    await conn.OpenAsync();
            //                    cmd.Parameters.AddWithValue("@recordNo", recordNo);

            //                    dynamic list = null;
            //                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

            //                    if (await rd.ReadAsync())
            //                    {
            //                        list = new
            //                        {
            //                            Doc_Hdr_Id = GenSettings.Filter(rd["Doc_Hdr_Id"]),
            //                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
            //                            Title = GenSettings.Filter(rd["Title"]),
            //                            RosterGroup = GenSettings.Filter(rd["RosterGroup"]),
            //                            MainGroup = GenSettings.Filter(rd["MainGroup"]),
            //                            BudgetGroup = GenSettings.Filter(rd["BudgetGroup"]),
            //                            DisplayText = GenSettings.Filter(rd["DisplayText"]),
            //                            Qty = GenSettings.Filter(rd["Qty"]),
            //                            Frequency = GenSettings.Filter(rd["Frequency"]),
            //                            LengthInWeeks = GenSettings.Filter(rd["LengthInWeeks"]),
            //                            QuoteQty = GenSettings.Filter(rd["QuoteQty"]),
            //                            BillUnit = GenSettings.Filter(rd["BillUnit"]),
            //                            Rate = GenSettings.Filter(rd["Rate"]),
            //                            Tax = GenSettings.Filter(rd["Tax"]),
            //                            PriceType = GenSettings.Filter(rd["PriceType"]),
            //                            QuotePerc = GenSettings.Filter(rd["QuotePerc"]),
            //                            BudgetPerc = GenSettings.Filter(rd["BudgetPerc"]),
            //                            Notes = GenSettings.Filter(rd["Notes"]),
            //                            Roster = GenSettings.Filter(rd["Roster"]),
            //                            SortOrder = GenSettings.Filter(rd["SortOrder"]),
            //                            StrategyID = GenSettings.Filter(rd["StrategyID"])
            //                        };
            //                    }

            //                    return Ok(results);
            //                }
            //            }
        }

        [HttpGet("quotes-details/{recordNo}")]
        public async Task<IActionResult> GetQuoteDetails(int recordNo)
        {

            var quoteHeader = await (from hdr in _context.Qte_Hdr where hdr.RecordNumber == recordNo select hdr).FirstOrDefaultAsync();
            var quoteLines = await (from lne in _context.Qte_Lne where lne.Doc_Hdr_Id == recordNo select lne).ToListAsync();
            var prog = await (from hr in _context.HumanResourceTypes where hr.RecordNumber == quoteHeader.ProgramId select hr).FirstOrDefaultAsync();
           
            var documents = await (from doc in _context.Documents where doc.Doc_Id == quoteHeader.CPID select doc).FirstOrDefaultAsync();

            var userid = Convert.ToInt32(documents.Author);

          //  var user = await (from usr in _context.UserInfo where usr.Recnum == userid select usr).FirstOrDefaultAsync();

            var domian = await (from dom in _context.DataDomains where dom.RecordNumber == documents.SubId select dom).FirstOrDefaultAsync();
            


            //  using (SqlCommand cmd = new SqlCommand(@"SELECT created, modified, careplanid, plantype, wadscplan, publishtoapp, dsoutletid, wadscoptout, wadscfundingchanged, wadscplanheaderonly, wadscsupportingevidence, Isnull(program, 'VARIOUS')    AS Program, Isnull(discipline, 'VARIOUS') AS Discipline, Isnull(caredomain, 'VARIOUS') AS CareDomain, title, docstartdate, docenddate, alarmdate, alarmtext, quotetemplate FROM   (SELECT publishtoapp, wadscplan, dsoutletid, wadscoptout, wadscfundingchanged, wadscplanheaderonly, wadscsupportingevidence, CONVERT(VARCHAR(10), D.created, 103) + ' ' + (SELECT [name] FROM   userinfo WHERE  recnum = D.author)         AS Created, CONVERT(VARCHAR(10), D.modified, 103) + ' ' + (SELECT [name] FROM   userinfo WHERE  recnum = D.typist) AS Modified, D.doc# AS CareplanID, (SELECT [name] FROM   humanresourcetypes WHERE  recordnumber = D.department AND [group] = 'PROGRAMS')    AS Program, (SELECT [description] FROM   datadomains WHERE  recordnumber = D.dpid)       AS Discipline, (SELECT [description] FROM   datadomains WHERE  recordnumber = D.caredomain) AS CareDomain, (SELECT [description] FROM   datadomains WHERE  recordnumber = D.subid)      AS PlanType, D.title, D.docstartdate, D.docenddate, D.alarmdate, D.alarmtext, quotetemplate FROM   documents D WHERE  doc_id = @docId) D1", (SqlConnection)conn))
            //     {
            //         cmd.Parameters.AddWithValue("@docId", obj.DocId);

            //         using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
            //         {
            //             if (await rd.ReadAsync())
            //             {
            //                 quote.DocStartDate = GenSettings.Filter(rd["docstartdate"]);
            //                 quote.DocEndDate = GenSettings.Filter(rd["docenddate"]);
            //             }
            //         }
            //     }

            var FinalQuoteLines = quoteLines.Select(x => new
            {
                groups = (from so in _context.ServiceOverview
                          join ITM in _context.ItemTypes on so.ServiceType equals ITM.Title
                          where ITM.Recnum == x.ItemId
                          select new
                          {
                              ServiceType = so.ServiceType,
                              MainGroup = ITM.MainGroup,
                              RosterGroup = ITM.RosterGroup
                          }).FirstOrDefault(),
                res = x
            })

                                .Select(x => new QuoteLineDTO()
                                {
                                    DocHdrId = x.res.Doc_Hdr_Id,
                                    Qty = x.res.Qty,
                                    BillUnit = x.res.BillUnit,
                                    UnitBillRate = x.res.UnitBillRate,
                                    Tax = x.res.Tax,
                                    COID = x.res.COID,
                                    BRID = x.res.BRID,
                                    DPID = x.res.DPID,
                                    DisplayText = x.res.DisplayText,
                                    Frequency = x.res.Frequency,
                                    QuoteQty = x.res.QuoteQty,
                                    PriceType = x.res.PriceType,
                                    LengthInWeeks = x.res.LengthInWeeks,
                                    ItemId = x.res.ItemId,
                                    RecordNumber = x.res.RecordNumber,
                                    SortOrder = x.res.SortOrder,
                                    MainGroup = GenSettings.DetermineMainGroup(x.groups.RosterGroup, x.groups.MainGroup),
                                    Code = (from SO in _context.ServiceOverview
                                            join ITM in _context.ItemTypes on SO.ServiceType equals ITM.Title
                                            where ITM.Recnum == x.res.ItemId
                                            select ITM.Title).Distinct().FirstOrDefault()
                                }).OrderBy(x => x.SortOrder).ToList();

            QuoteHeaderDTO headerDTO = new QuoteHeaderDTO()
            {
                RecordNumber = quoteHeader.RecordNumber,
                Program = prog.Name,
                
                CPID = quoteHeader.CPID,
                BasePension = quoteHeader.BasePension,
                AgreedTopUp = quoteHeader.AgreedTopUp,
                BalanceAtQuote = quoteHeader.BalanceAtQuote,
                Budget = quoteHeader.Budget,
                CLAssessedIncomeTestedFee = quoteHeader.CLAssessedIncomeTestedFee,
                ClientId = quoteHeader.ClientId,
                Contribution = quoteHeader.Contribution,
                DailyAgreedTopUp = quoteHeader.DailyAgreedTopUp,
                DailyBasicCareFee = quoteHeader.DailyBasicCareFee,
                DailyCDCRate = quoteHeader.DailyCDCRate,
                DailyIncomeTestedFee = quoteHeader.DailyIncomeTestedFee,

                GovtDistribution = quoteHeader.GovtContribution,

                DaysCalc = quoteHeader.DaysCalc,
                DocNo = quoteHeader.DocId,
                GovtContribution = quoteHeader.GovtContribution,
                HardshipSupplement = quoteHeader.HardshipSupplement,
                IncomeTestedFee = quoteHeader.IncomeTestedFee,

                PackageSupplements = quoteHeader.PackageSupplements,
                ProgramId = quoteHeader.ProgramId,

                QuoteBase = quoteHeader.QuoteBase,
                QuoteView = quoteHeader.QuoteView,
                Basis = quoteHeader.Basis,
                Created = documents.Created,
                Modified = documents.Modified,
               // Type =  domian.Description,
                User = "",
                NewFileName = documents.FileName,
                Level = prog.User3,
                IsCDC = prog.UserYesNo1,
                QuoteLines = FinalQuoteLines
            };

            return Ok(headerDTO);
        }

        [HttpPost("notes-print")]
        public async Task<IActionResult> PrintNotes()
        {
            var memStream = await printService.PrintNotes();

            if (memStream == null)
            {
                return BadRequest();
            }

            MemoryStream ms = memStream.Value.Item1;
            string filetype = memStream.Value.Item2;

            return File(ms, filetype, "quotes.docx");
        }



        [HttpPost("quotes-print")]
        public async Task<IActionResult> PrintQuotes([FromBody] DocumentQuoteInputDto input)
        {
            var memStream = await printService.PrintQuotes(input);

            if (memStream == null)
            {
                return BadRequest();
            }

            MemoryStream ms = memStream.Value.Item1;
            string filetype = memStream.Value.Item2;

            return File(ms, filetype, "quotes.docx");
        }

        [HttpPost("quotes")]
        public async Task<IActionResult> GetQuotes([FromBody] DocumentQuoteInputDto input)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                 string DateFilter = string.Empty;
                string Criteria = string.Empty;
                string displayLast = "20";

                string IsAccepted =  "" ;
                string IsArchived = " AND (D.DeletedRecord = 0 OR D.DeletedRecord Is NULL)";
                Criteria = " Where 1=1 ";

                if (input.Filters!=null){
                    IsAccepted = input.Filters.AcceptedQuotes ? ", 'CAREPLAN'" : "";
                    IsArchived = input.Filters.ArchiveDocs ? "" : " AND (D.DeletedRecord = 0 OR D.DeletedRecord Is NULL)";            
                     displayLast = input.Filters.Display.ToString();

                    if (!input.Filters.AllDates)
                    {
                        var startDate = input.Filters.StartDate?.ToString("yyyy-MM-dd");
                        var endDate = input.Filters.EndDate?.ToString("yyyy-MM-dd");
                        DateFilter = $@" AND docstartdate > '{startDate}' AND docenddate <= '{endDate}'";
                    }

                    if (input.Filters.Criteria != "")
                    {
                       // var list = input.Filters.Criteria;
                    // Console.WriteLine(input.Filters.Criteria);
                        Criteria = " Where 1=1 " + input.Filters.Criteria;
                    }

                }

                string sql = $@"SELECT DISTINCT TOP   {displayLast}  d1.RecordNumber,doc_id,
                                quotenumber,
                            
                                title                         AS [Careplan Name],
                                CareplanID,
                                [plantype],
                                Isnull(caredomain, 'VARIOUS') AS CareDomain,
                                Isnull(discipline, 'VARIOUS') AS Discipline,
                                Isnull(d1.program, 'VARIOUS')    AS Program,
                                Isnull([status], '')          AS [St],
                                docstartdate                  AS [Start Date],
                                docenddate                    AS [End Date],
                                [created],
                                [modified],
                                filename,D1.PackageSupplements,OriginalLocation, D1.DocumentType,alarmtext,alarmdate,Classification,PublishToApp,
                                isnull(hrt.User3,'') as Level, isnull(hrt.UserYesNo1,0) as IsCDC
                        
                    FROM   (SELECT qh.RecordNumber,doc_id, qh.PackageSupplements,
                                CONVERT(VARCHAR(10), D.created, 103) + ' '
                                + (SELECT [name]
                                    FROM   userinfo
                                    WHERE  recnum = D.author)         AS Created,
                                CONVERT(VARCHAR(10), D.modified, 103) + ' '
                                + (SELECT [name]
                                    FROM   userinfo
                                    WHERE  recnum = D.typist)         AS Modified,
                                D.doc#                               AS CareplanID,
                                (SELECT description
                                    FROM   datadomains
                                    WHERE  recordnumber = subid)        AS PlanType,
                                (SELECT [name]
                                    FROM   humanresourcetypes
                                    WHERE  recordnumber = D.department
                                        AND [group] = 'PROGRAMS')    AS Program,
                                (SELECT [description]
                                    FROM   datadomains
                                    WHERE  recordnumber = D.dpid)       AS Discipline,
                                (SELECT [description]
                                    FROM   datadomains
                                    WHERE  recordnumber = D.caredomain) AS CareDomain,
                                D.title,
                                D.filename,
                                    convert(varchar(MAX),D.OriginalLocation) as OriginalLocation,
                                D.status,
                                D.docstartdate,
                                D.docenddate,
                                D.alarmdate,
                                D.alarmtext,
                                QH.doc#                              AS QuoteNumber,
                                DocumentType, Classification, isnull(PublishToApp,0) as PublishToApp              
                            FROM   documents D
                                LEFT JOIN qte_hdr QH
                                        ON cpid = doc_id
                            WHERE  personid = @personID
                                AND Isnull([status], '') <> ''
                                AND title NOT LIKE ('%TEMP%')
                                {DateFilter}
                                AND documentgroup IN ( 'CP_QUOTE' {IsAccepted}) {IsArchived}) D1
                        LEFT JOIN recipientprograms rp
                                    ON        rp.program = d1.program 
                        LEFT JOIN HumanResourceTypes hrt on hrt.Name=rp.Program AND hrt.[Group] = 'PROGRAMS'
                        " + Criteria +
                        "ORDER  BY [docstartdate] DESC ";
              // Console.Write(sql);
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", input.Quote.PersonId);

                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            DocID = GenSettings.Filter(rd["doc_id"]),                            
                            QuoteNumber = GenSettings.Filter(rd["QuoteNumber"]),
                            CarePlanId = GenSettings.Filter(rd["CarePlanId"]),
                            CarePlan = GenSettings.Filter(rd["CarePlan Name"]),
                            PlanType = GenSettings.Filter(rd["PlanType"]),
                            St = GenSettings.Filter(rd["St"]),
                            StartDate = GenSettings.Filter(rd["Start Date"]),
                            EndDate = GenSettings.Filter(rd["End Date"]),
                            Created = GenSettings.Filter(rd["Created"]),
                            Modified = GenSettings.Filter(rd["Modified"]),
                            Filename = GenSettings.Filter(rd["Filename"]),
                            OriginalLocation = GenSettings.Filter(rd["OriginalLocation"]),
                            Program = GenSettings.Filter(rd["Program"]),
                            CareDomian = GenSettings.Filter(rd["CareDomain"]),
                            PackageSupplements = GenSettings.Filter(rd["PackageSupplements"]),
                            DocumentType = GenSettings.Filter(rd["DocumentType"]),
                            AlarmText = GenSettings.Filter(rd["alarmtext"]),
                            AlarmDate = GenSettings.Filter(rd["alarmdate"]),
                            Classification = GenSettings.Filter(rd["Classification"]),
                            Discipline  = GenSettings.Filter(rd["Discipline"]),
                            PublishToApp = GenSettings.Filter(rd["PublishToApp"]),
                            IsCDC = GenSettings.Filter(rd["IsCDC"]),
                            Level = GenSettings.Filter(rd["Level"]),
                        });
                    }

                    return Ok(list);
                }
            }
        }

        [HttpGet("indigenous")]
        public async Task<IActionResult> GetIndigenous()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'CSTDA_INDIGINOUS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["DESCRIPTION"])));
                    return Ok(list);
                }
            }
        }


        [HttpGet("notifymail/{Detail}")]
        public async Task<IActionResult> getnotifyaddresses(string Detail)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select Detail From PhoneFaxOther where PersonID = (select UniqueID from Staff where AccountNo = @Account) and  type = '<EMAIL>'", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@Account", Detail);
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Detail"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }


        // primdisabilities
        // otherdisabilities
        [HttpGet("primdisabilities")]
        public async Task<IActionResult> GetPrimdisabilitiesGroup()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'CSTDA_DISABILITYGROUP'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("otherdisabilities")]
        public async Task<IActionResult> GetOtherdisabilitiesGroup()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'CSTDA_DISABILITYGROUP'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("disabilities")]
        public async Task<IActionResult> GetDisabilitiesGroup()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'CSTDA_DISABILITYGROUP'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("hrgroups")]
        public async Task<IActionResult> GetHRList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION]  FROM DATADOMAINS WHERE DOMAIN = 'HRGROUPS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("opgroups")]
        public async Task<IActionResult> GetOPList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION]  FROM DATADOMAINS WHERE DOMAIN = 'OPGROUPS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("serviceregion")]
        public async Task<IActionResult> GetServiceRegionList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'GROUPAGENCY'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("strategy-list/{docID}")]
        public async Task<IActionResult> GetListPositions(string docID)
        {

            var sql = @"SELECT ST.Notes AS Strategy, ST.RecordNumber FROM HumanResources GL LEFT JOIN HumanResources ST ON ST.PersonID = CONVERT(VARCHAR, GL.RecordNumber) WHERE GL.PersonID = @docID AND GL.[Type] = 'CAREPLANGOALS' AND ST.Notes <> '' ORDER BY GL.Name ";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@docID", docID);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Strategy = (GenSettings.Filter(rd["Strategy"])).ToUpper(),
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"])
                        });
                    }
                    return Ok(list);
                }
            }
        }

       
        [HttpGet("positions")]
        public async Task<IActionResult> GetListPositions()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'STAFFPOSITION'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("staff/userdefined-1")]
        public async Task<IActionResult> GetListUserDefined1()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'STAFFTYPE'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("staff/userdefined-2")]
        public async Task<IActionResult> GetListUserDefined2()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DESCRIPTION FROM DATADOMAINS WHERE DOMAIN = 'STAFFPREF'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("headerinfo/{personID}")]
        public async Task<IActionResult> getheaderinfo(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT concat(title, ' ', AccountNo) title,LastName,FirstName,AccountNo,STF_CODE as code from Staff where AccountNo=@personID", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            title = GenSettings.Filter(rd["title"]),
                            code = GenSettings.Filter(rd["code"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("consent/{personID}")]
        public async Task<IActionResult> GetConsents(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT(Description) FROM DataDomains WHERE Domain = 'RECIPIENTCONSENTS' AND Description NOT IN (SELECT [Name] AS Description FROM HumanResources 
                    WHERE PersonID = @personID AND [Name] = Description AND [Group] = 'RECIPIENTCONSENT') ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("procedure/{personID}")]
        public async Task<IActionResult> GetMedicalProcedure(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT description FROM   mproceduretypes WHERE  description NOT IN (SELECT description FROM   mprocedures WHERE  personid = @personID AND mprocedures.description = mproceduretypes.description) ORDER  BY description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("medication/{personID}")]
        public async Task<IActionResult> GetMedication(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT description
                        FROM   datadomains
                        WHERE  domain = 'MEDICATIONS'
                               AND description NOT IN (SELECT description
                                                       FROM   onimedications
                                                       WHERE  personid = @personID
                                                              AND onimedications.description = datadomains.description) ", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical-reminders/{personID}")]
        public async Task<IActionResult> GetClinicalReminders(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'CLINICALREMIND'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("clinical-alertt/{personID}")]
        public async Task<IActionResult> GetClinicallAlert(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'CLINICALALERT'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("clinical-alert/{personID}")]
        public async Task<IActionResult> GetClinicalAlertV2(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [Name] as [Description] FROM HumanResources WHERE PersonID = @personID AND [Type] = 'CLINICALALERT' ORDER BY Name", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("group/usergroup/{personID}")]
        public async Task<IActionResult> GetUserGroup(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DATADOMAINS WHERE Domain = 'RECIPTYPE' AND Description NOT IN (SELECT [Name] AS Description FROM HumanResources 
                    WHERE PersonID = @personID AND [Name] = Description AND [Group] = 'RECIPTYPE') ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("group/recipient-preference/{personID}")]
        public async Task<IActionResult> GetRecipientPreference(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT * FROM DATADOMAINS WHERE DOMAIN = 'RECIPPREF' AND DESCRIPTION NOT IN (
                        SELECT [Name] AS Description FROM HumanResources WHERE PersonID = @personID AND [Name] = Description AND [Group] = 'RECIPPREF'
                    ) ORDER BY Description", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@personID", personID);
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("insurance/pension-status")]
        public async Task<IActionResult> GetPensionStatus()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'PENSION'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("funding-source-per-program/{program}")]
        public async Task<IActionResult> GetFundingSourcePerProgram(string program)
        {
            return Ok(await (from hrt in _context.HumanResourceTypes where hrt.Name == program select hrt.Type).FirstOrDefaultAsync());
        }

        [HttpGet("funding-source")]
        public async Task<IActionResult> GetFundingSource()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'FUNDINGBODIES'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("insurance/card-status")]
        public async Task<IActionResult> GetCardStatus()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [DESCRIPTION] FROM DATADOMAINS WHERE DOMAIN = 'HACCDVACardStatus'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["DESCRIPTION"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("mark-generate-referral-id")]
        public async Task<IActionResult> GetHCPPackageGenerate(GenerateReferralId _vars)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"[MARK_GENERATE_REFERRAL_ID]", (SqlConnection)conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Name", _vars.AccountNo);
                    cmd.Parameters.AddWithValue("@Template", _vars.Template);
                    cmd.Parameters.AddWithValue("@Type", _vars.Type);
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    string result = string.Empty;

                    if (await rd.ReadAsync())
                    {
                        result = GenSettings.Filter(rd["Uname"]);
                    }
                    return Ok(new { result });
                }
            }
        }

        [HttpGet("hcp-programs")]
        public async Task<IActionResult> GetHCP()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [NAME], [User3] FROM HumanResourceTypes WHERE UserYesNo3 = 1 AND LEFT([Type], 4) = 'DOHA' AND User6 <> 'SAH'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = (GenSettings.Filter(rd["Name"])).ToUpper(),
                            Level = (GenSettings.Filter(rd["User3"])).ToUpper()
                        });
                    }
                    return Ok(list);
                }
            }
        }
  [HttpGet("sah-programs")]
        public async Task<IActionResult> GetSAH()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [NAME], [User3] FROM HumanResourceTypes WHERE UserYesNo3 = 1 AND LEFT([Type], 4) = 'DOHA' AND User6 = 'SAH'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = (GenSettings.Filter(rd["Name"])).ToUpper(),
                            Level = (GenSettings.Filter(rd["User3"])).ToUpper()
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("ndia-programs")]
        public async Task<IActionResult> GetNDIA()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [NAME] FROM HumanResourceTypes WHERE UserYesNo3 = 1 AND LEFT([Type], 4) = 'NDIA'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Name"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("ndia-items")]
        public async Task<IActionResult> GetNDIAItems()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct [New Support Item Number] as NDIAITEM from NDIAItems", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["NDIAITEM"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("ndia-itemss")]
        public async Task<IActionResult> GetNDIAItemss()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT [New Support Item Number] + '<:>' + [Support Item] AS NDIAITEM FROM NDIAItems ORDER BY [New Support Item Number]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["NDIAITEM"])));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("competenciesheader/{id}")]
        public async Task<IActionResult> Getcompetenciesheader(string id)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select CompetencyException,EmailCompetencyReminders,SB1,SB2,SB3,SB4,SB5,SB6,SB7,SB8,SB9,SB10,SB11,SB12,SB13,SB14,SB15,SB16,SB17,SB18,SB19,SB20,SB21,SB22,SB23,SB24,SB25,SB26,SB27,SB28,SB29,SB30,SB31,SB32,SB33,SB34,SB35 from Staff where UniqueID = @id", (SqlConnection)conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    await conn.OpenAsync();
                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            CompetencyException = GenSettings.Filter(rd["CompetencyException"]),
                            EmailCompetencyReminders = GenSettings.Filter(rd["EmailCompetencyReminders"]),
                            SB1 = GenSettings.Filter(rd["SB1"]),
                            SB2 = GenSettings.Filter(rd["SB2"]),
                            SB3 = GenSettings.Filter(rd["SB3"]),
                            SB4 = GenSettings.Filter(rd["SB4"]),
                            SB5 = GenSettings.Filter(rd["SB5"]),
                            SB6 = GenSettings.Filter(rd["SB6"]),
                            SB7 = GenSettings.Filter(rd["SB7"]),
                            SB8 = GenSettings.Filter(rd["SB8"]),
                            SB9 = GenSettings.Filter(rd["SB9"]),
                            SB10 = GenSettings.Filter(rd["SB10"]),
                            SB11 = GenSettings.Filter(rd["SB11"]),
                            SB12 = GenSettings.Filter(rd["SB12"]),
                            SB13 = GenSettings.Filter(rd["SB13"]),
                            SB14 = GenSettings.Filter(rd["SB14"]),
                            SB15 = GenSettings.Filter(rd["SB15"]),
                            SB16 = GenSettings.Filter(rd["SB16"]),
                            SB17 = GenSettings.Filter(rd["SB17"]),
                            SB18 = GenSettings.Filter(rd["SB18"]),
                            SB19 = GenSettings.Filter(rd["SB19"]),
                            SB20 = GenSettings.Filter(rd["SB20"]),
                            SB21 = GenSettings.Filter(rd["SB21"]),
                            SB22 = GenSettings.Filter(rd["SB22"]),
                            SB23 = GenSettings.Filter(rd["SB23"]),
                            SB24 = GenSettings.Filter(rd["SB24"]),
                            SB25 = GenSettings.Filter(rd["SB25"]),
                            SB26 = GenSettings.Filter(rd["SB26"]),
                            SB27 = GenSettings.Filter(rd["SB27"]),
                            SB28 = GenSettings.Filter(rd["SB28"]),
                            SB29 = GenSettings.Filter(rd["SB29"]),
                            SB30 = GenSettings.Filter(rd["SB30"]),
                            SB31 = GenSettings.Filter(rd["SB31"]),
                            SB32 = GenSettings.Filter(rd["SB32"]),
                            SB33 = GenSettings.Filter(rd["SB33"]),
                            SB34 = GenSettings.Filter(rd["SB34"]),
                            SB35 = GenSettings.Filter(rd["SB35"]),
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("skills")]
        public async Task<IActionResult> GetSkills()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select SQLID,Identifier,Text from FieldNames where Identifier like 'fStaffContainer9-Competencies%' Order By Text", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            sqlid = (GenSettings.Filter(rd["SQLID"])),
                            identifier = (GenSettings.Filter(rd["Identifier"])),
                            text = (GenSettings.Filter(rd["Text"])),
                        });
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("dex-programs/{personID}")]
        public async Task<IActionResult> GetDex(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Upper([name]) AS Name 
                    FROM   humanresourcetypes 
                        LEFT JOIN (SELECT [program] 
                                    FROM   recipientprograms 
                                    WHERE  personid = @personID 
                                            AND programstatus IN ( 'REFERRAL', 'ACTIVE', 
                                                                    'WAITING LIST', 
                                                                    'ON HOLD' ) 
                                    GROUP  BY program) AS RPRog1 
                                ON RProg1.program = humanresourcetypes.NAME 
                    WHERE  (( ( [group] = 'PROGRAMS' ) 
                            AND ( enddate IS NULL 
                                    OR enddate >= GETDATE() ) 
                            AND ( Isnull(useryesno2, 0) = 0 ) 
                            AND ( RPRog1.program IS NULL ) )) 
                        AND Isnull(user2, '') <> 'Contingency' 
                        AND Isnull(useryesno3, 0) <> 1 
                        AND Isnull(useryesno2, 0) = 0 
                        AND [type] = 'DSS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@personID", personID);

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Name"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("other-programs/{personID}")]
        public async Task<IActionResult> GetOther(string personID)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT Upper([name]) AS Name
                        FROM   humanresourcetypes 
                            LEFT JOIN (SELECT [program] 
                                        FROM   recipientprograms 
                                        WHERE  personid = @personID 
                                                AND programstatus IN ( 'REFERRAL', 'ACTIVE', 
                                                                        'WAITING LIST', 
                                                                        'ON HOLD' ) 
                                        GROUP  BY program) AS RPRog1 
                                    ON RProg1.program = humanresourcetypes.NAME 
                        WHERE  (( ( [group] = 'PROGRAMS' ) 
                                AND ( enddate IS NULL 
                                        OR enddate >= GETDATE() ) 
                                AND ( Isnull(useryesno2, 0) = 0 ) 
                                AND ( RPRog1.program IS NULL ) )) 
                            AND Isnull(user2, '') <> 'Contingency' 
                            AND Isnull(useryesno3, 0) <> 1 
                            AND Isnull(useryesno2, 0) = 0 
                            AND [type] NOT IN ( 'DOHA', 'DSS', 'NDIA' ) ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@personID", personID);

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add((GenSettings.Filter(rd["Name"])).ToUpper());
                    }
                    return Ok(list);
                }
            }
        }


        [HttpGet("recipient-search")]
        public async Task<IActionResult> GetRecipientSearch(RecipientSearch rs)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT   * FROM     ( SELECT DISTINCT r.uniqueid              AS [ID], r.agencyidreportingcode AS [UR Number], r.accountno             AS [Account#], r.[surname/organisation], r.firstname AS [First Name], Upper([surname/organisation]) + ', ' + CASE WHEN firstname <> '' THEN firstname ELSE ' ' END + CASE WHEN middlenames <> '' THEN ' ' + middlenames ELSE ' ' END AS [Name], r.gender, r.dateofbirth AS [D.O.B.], CASE WHEN n1.address <> '' THEN n1.address ELSE n2.address END AS address, CASE WHEN p1.contact <> '' THEN p1.contact ELSE p2.contact END AS contact, r.type, r.branch, CONVERT(NVARCHAR(4000), r.notes) AS alerts, r.recipient_coordinator          AS [Primary Coord], r.agencydefinedgroup             AS [Category], r.onirating, r.admissiondate AS [Activation Date], r.dischargedate AS [DeActivation Date], n1.suburb FROM            recipients R LEFT JOIN       recipientprograms ON              recipientprograms.personid = r.uniqueid LEFT JOIN       humanresourcetypes ON              humanresourcetypes.NAME = recipientprograms.program LEFT JOIN ( SELECT personid, suburb, CASE WHEN address1 <> '' THEN address1 + ' ' ELSE ' ' END + CASE WHEN address2 <> '' THEN address2 + ' ' ELSE ' ' END + CASE WHEN suburb <> '' THEN suburb + ' ' ELSE ' ' END + CASE WHEN postcode <> '' THEN postcode ELSE ' ' END AS address FROM   namesandaddresses WHERE  primaryaddress = 1) AS n1 ON              n1.personid = r.uniqueid LEFT JOIN ( SELECT personid, CASE WHEN address1 <> '' THEN address1 + ' ' ELSE ' ' END + CASE WHEN address2 <> '' THEN address2 + ' ' ELSE ' ' END + CASE WHEN suburb <> '' THEN suburb + ' ' ELSE ' ' END + CASE WHEN postcode <> '' THEN postcode ELSE ' ' END AS address FROM   namesandaddresses WHERE  primaryaddress <> 1) AS n2 ON              n2.personid = r.uniqueid LEFT JOIN ( SELECT personid, phonefaxother.type + ' ' + CASE WHEN detail <> '' THEN detail ELSE ' ' END AS contact FROM   phonefaxother WHERE  primaryphone = 1) AS p1 ON              p1.personid = r.uniqueid LEFT JOIN ( SELECT personid, phonefaxother.type + ' ' + CASE WHEN detail <> '' THEN detail ELSE ' ' END AS contact FROM   phonefaxother WHERE  primaryphone <> 1) AS p2 ON              p2.personid = r.uniqueid WHERE           r.[accountno] LIKE @accountNo AND             ( ( admissiondate IS NOT NULL ) AND             ( dischargedate IS NULL ) ) ) AS rec ORDER BY rec.[Account#]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@accountNo", string.Format("{0}%", rs.AccountNo ?? ""));
                    // cmd.Parameters.AddWithValue("@pageNo", rs.RowNo);
                    // cmd.Parameters.AddWithValue("@resultNo", 200);                    

                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                AccountNo = GenSettings.Filter(rd["Account#"]),
                                Name = GenSettings.Filter(rd["Name"]),
                                Gender = GenSettings.Filter(rd["Gender"]),
                                Birthdate = GenSettings.Filter(rd["D.O.B."]),
                                Address = GenSettings.Filter(rd["Address"]),
                                Contact = GenSettings.Filter(rd["Contact"]),
                                Type = GenSettings.Filter(rd["Type"]),
                                Branch = GenSettings.Filter(rd["Branch"]),
                                Suburb = GenSettings.Filter(rd["Suburb"]),
                            });
                        }
                    }

                    return Ok(list);
                }
            }
        }
[HttpGet("debtors")]
        public async Task<IActionResult> GetDebtors()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select accountNo,[Type] from Recipients where accountno <> '!MULTIPLE' AND accountno <> '!INTERNAL'  ORDER BY [accountNo]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                 
                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                AccountNo = GenSettings.Filter(rd["accountNo"]),
                               
                            });
                        }
                    }

                    return Ok(list);
                }
            }
        }
          [HttpGet("userlist")]
        public async Task<IActionResult> GetUserList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select [Name], ViewFilterBranches as branch, UserType as type from UserInfo WHERE LoginMode = 'OPEN' AND IsNull(EndDate,'') = '' ORDER BY [Name]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                 
                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                Name = GenSettings.Filter(rd["Name"]),
                                Type = GenSettings.Filter(rd["Type"]),
                                Branch = GenSettings.Filter(rd["branch"])
                            });
                        }
                    }

                    return Ok(list);
                }
            }
        }
        [HttpGet("stafflist")]
        public async Task<IActionResult> GetStaffList()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select accountNo, Category as [Type], STF_DEPARTMENT as branch from Staff where accountno <> '!MULTIPLE' AND accountno <> '!INTERNAL'  ORDER BY [accountNo]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                 
                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                AccountNo = GenSettings.Filter(rd["accountNo"]),
                                Type = GenSettings.Filter(rd["Type"]),
                                Branch = GenSettings.Filter(rd["branch"])
                            });
                        }
                    }

                    return Ok(list);
                }
            }
        }
          [HttpGet("unavailable-staff")]
        public async Task<IActionResult> GetStaffUnavailableList()
        {
            var sql=@"SELECT DISTINCT convert(varchar,getDate(),111) AS [Date], [AccountNo], [LastName], NULL AS [JobNo], NULL AS [Client Code], NULL AS [Start Time], NULL AS [Duration], NULL AS [Type], NULL AS [RosterGroup], NULL AS [MinorGroup], NULL AS [Service Type], NULL AS [ServiceSetting] FROM Staff WHERE (AccountNo > '!z') 
                UNION 
                SELECT DISTINCT [Date], [AccountNo], [LastName], RecordNo as JobNo, [Client Code], [Start Time], [Duration],
                [Type], [RosterGroup], [MinorGroup], [Service Type], [ServiceSetting] FROM Staff INNER JOIN ROSTER ON Accountno = [Carer Code] 
                INNER JOIN ItemTypes ON [Roster].[Service Type] = [ItemTypes].[Title] 
                WHERE (AccountNo > '!z') AND (CommencementDate is not null AND (TerminationDate is null OR TerminationDate > convert(varchar,getDate(),111))) 
                AND ([Category] = 'STAFF') AND ([Date] = convert(varchar,getDate(),111))  
                AND ([Type] IN (2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13)) AND ([Start Time] IS NOT Null 
                AND [Duration] IS NOT Null AND [Start Time] >= '00:00' AND LEN([Start Time]) = 5 AND [Duration] > 1) AND [Minorgroup] <> 'GAP' 
                ORDER BY AccountNo, [Date], [Start Time]";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                 
                    List<dynamic> list = new List<dynamic>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(new
                            {
                                AccountNo = GenSettings.Filter(rd["accountNo"]),
                                JobNo = GenSettings.Filter(rd["JobNo"]),
                                ClientCode = GenSettings.Filter(rd["Client Code"]),
                                startTime = GenSettings.Filter(rd["Start Time"]),
                                Duration = GenSettings.Filter(rd["Duration"]),
                                Type = GenSettings.Filter(rd["type"]),
                                RosterGroup = GenSettings.Filter(rd["RosterGroup"]),
                                MinorGroup = GenSettings.Filter(rd["MinorGroup"]),
                                ServiceType = GenSettings.Filter(rd["Service Type"]),
                                ServiceSetting = GenSettings.Filter(rd["ServiceSetting"])

                            });
                        }
                    }

                    return Ok(list);
                }
            }
        }
        [HttpGet("staffservices")]
        public async Task<IActionResult> GetStaffServiceTypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes WHERE Status = 'ATTRIBUTABLE' AND (ProcessClassification = 'OUTPUT') AND (EndDate Is Null or EndDate >= GETDATE()) ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("staff-record-view/{user}")]
        public async Task<IActionResult> GetStaffRecordView(string user)
        {

            var userview = await (from ui in _context.UserInfo
                                  where ui.Name == user
                                  select new { StaffRecordView = ui.StaffRecordView, Staff = ui.Staff }).SingleOrDefaultAsync();

            return Ok(userview);
        }


        [HttpGet("vehicles")]
        public async Task<IActionResult> GetVehicles()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT UPPER([Description]) as CategoryName FROM DataDomains WHERE [Domain] = 'VEHICLES' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }


        [HttpGet("CriterialistPrograms")]
        public async Task<IActionResult> GetAllPrograms()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER([Name]) as ProgramName FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= getdate())  ORDER BY [ProgramName]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("Copetency-Group")]
        public async Task<IActionResult> GetCopetencyGroup()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT UPPER([Description]) as CategoryName FROM DataDomains WHERE [Domain] = 'COMPETENCYGROUP' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("loanitems")]
        public async Task<IActionResult> GetrptLoanItems()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT [Name] FROM HumanResources WHERE [Group] = 'LOANITEMS' ORDER BY [Name]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("incidenttype")]
        public async Task<IActionResult> Getrptincidents()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DataDomains WHERE Domain = 'INCIDENT TYPE' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("loantypes")]
        public async Task<IActionResult> GetLoantypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Description FROM DataDomains WHERE Domain = 'GOODS' ORDER BY Description", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("planitype")]
        public async Task<IActionResult> Getrptiplantypes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Distinct (SELECT Description FROM DataDomains WHERE RecordNumber = SubId) AS PlanType FROM Documents Order By PlanType", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }


        [HttpGet("casenotesgroup")]
        public async Task<IActionResult> Getrptcasenotes()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct Description from DataDomains WHERE Domain = 'CASENOTEGROUPS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("hrnotescategory")]
        public async Task<IActionResult> GetrptHrnotescategory()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"Select Description from DataDomains WHERE Domain = 'HRGROUPS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }



        //MUfeed

        [HttpGet("trainingtype")]
        public async Task<IActionResult> Getrpttrainingtype()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes WHERE [MinorGroup] = 'TRAINING' AND Status = 'NONATTRIBUTABLE' AND (ProcessClassification = 'OUTPUT') AND (EndDate Is Null OR EndDate >= Convert(nvarchar,GETDATE(),23)) ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("traccsuser")]
        public async Task<IActionResult> Getrpttraccsuser()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT UPPER([NAME]) as Name FROM userinfo Order by Name", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("agencyid")]
        public async Task<IActionResult> Getrptagencyid()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Distinct Address1 AS AgencyID FROM HumanResourceTypes WHERE [GROUP] = 'PROGRAMS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("mdstype")]
        public async Task<IActionResult> Getrptmdstype()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select Distinct [HACCType] AS MDSType from ItemTypes WHERE ISNULL(IT_DATASET, 'OTHER') <> 'OTHER' ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("paytype")]
        public async Task<IActionResult> Getrptpaytype()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes WHERE (ProcessClassification = 'INPUT') AND (EndDate Is Null OR EndDate >= getdate()) UNION Select ' ALL' AS Title FROM ItemTypes ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("travelandAlternateCode")]
        public async Task<IActionResult> GettravelandAlternateCode()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM Itemtypes WHERE ProcessCLassification = 'OUTPUT' and ISNULL(EndDate, '') = ''", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("activity")]
        public async Task<IActionResult> Getrptactivity()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Title FROM ItemTypes WHERE (ProcessClassification = 'OUTPUT' OR ProcessClassification = 'EVENT') AND (EndDate Is Null OR EndDate >= getdate()) UNION Select ' ALL' AS Title FROM ItemTypes ORDER BY Title", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }
        [HttpGet("settings_vehicles")]
        public async Task<IActionResult> Getrptsettings_vehicles()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER([SETTING]) FROM (SELECT [NAME] AS [SETTING] FROM CSTDAOUTLETS UNION SELECT DESCRIPTION AS [SETTING] FROM DATADOMAINS WHERE DOMAIN IN ('VEHICLES', 'ACTIVITYGROUPS')) AS T", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd[0]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet("rosterpublished-end-date")]
        public async Task<IActionResult> GetRostersPublishedEndDate()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT TOP 1 NewDate FROM ( Select RSC_Cycle, right(RSC_DestinationEnd, 4) + '/' + substring(rsc_destinationend, 4,2) + '/' + left(rsc_destinationend, 2) as NewDate FROM rostercopy ) t WHERE isnull(newdate, '') <> '' and convert(varchar, RSC_Cycle) IN ('ALL', 'CYCLE 1') ORDER BY NewDate DESC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    DateTime date = new DateTime();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        if (await rd.ReadAsync())
                        {
                            DateTime.TryParseExact(GenSettings.Filter(rd["NewDate"]), "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
                        }
                        return Ok(date);
                    }
                }
            }
        }

        [HttpGet, Route("payperiod")]
        public async Task<IActionResult> GetPayPeriod()
        {
            return Ok(await GenSettings.GetPayPeriod());
        }
           [HttpPost, Route("payperiod")]
        public async Task<IActionResult> SetPayPeriod([FromBody] PayPeriod period)
        {
             using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"usp_PayPeriodDates_U", (SqlConnection)conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
          
                    cmd.Parameters.AddWithValue("@payPeriodEndDate", period.PayPeriodEndDate);
                    cmd.Parameters.AddWithValue("@startDate", period.Start_Date);
                    cmd.Parameters.AddWithValue("@endDate", period.End_Date);                   

                    await conn.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();

                    return Ok(true);
                }
            }
        }

        [HttpGet("recipients/active")]
        public async Task<IActionResult> GetRecipientActive()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT AccountNo FROM Recipients WHERE AccountNo > '!z' AND (AdmissionDate is not null) and (DischargeDate is null)", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["AccountNo"])).ToUpper());
                    return Ok(list);
                }
            }
        }
        [HttpGet("packagesDetail")]
        public async Task<IActionResult> GetpackagesDetail()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT sPackage FROM (SELECT DISTINCT R.Accountno + ' <==> '+ RP.Program AS sPackage FROM HumanResourceTypes PR INNER JOIN RecipientPrograms RP ON  PR.Name = RP.Program INNER JOIN Recipients R ON R.UniqueID = RP.PersonID WHERE R.Accountno > '!z' AND PR.[group] = 'PROGRAMS' AND PR.UserYesNo2 = 1 AND PR.User2 <> 'Contingency'  AND PR.Type = 'NDIA') t", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["sPackage"])).ToUpper());
                    return Ok(list);
                }
            }
        }
        [HttpGet("NDIApackages")]
        public async Task<IActionResult> GetNDIApackages()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Packages FROM (SELECT DISTINCT R.Accountno + ' <==> '+ RP.Program AS sPackage,RP.Program AS Packages FROM HumanResourceTypes PR INNER JOIN RecipientPrograms RP ON  PR.Name = RP.Program INNER JOIN Recipients R ON R.UniqueID = RP.PersonID WHERE R.Accountno > '!z' AND PR.[group] = 'PROGRAMS' AND PR.UserYesNo2 = 1 AND PR.User2 <> 'Contingency'  AND PR.Type = 'NDIA') t", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["Packages"])).ToUpper());
                    return Ok(list);
                }
            }
        }
        [HttpGet("CDCpackages")]
        public async Task<IActionResult> GetCDCpackages()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT Packages FROM (SELECT DISTINCT R.Accountno + ' <==> '+ RP.Program AS sPackage,RP.Program AS Packages FROM HumanResourceTypes PR INNER JOIN RecipientPrograms RP ON  PR.Name = RP.Program INNER JOIN Recipients R ON R.UniqueID = RP.PersonID WHERE R.Accountno > '!z' AND PR.[group] = 'PROGRAMS' AND PR.UserYesNo2 = 1 AND PR.User2 <> 'Contingency'  AND PR.Type = 'DOHA' AND RP.[ProgramStatus] <> 'INACTIVE' ) t", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync()) list.Add((GenSettings.Filter(rd["Packages"])).ToUpper());
                    return Ok(list);
                }
            }
        }
        [HttpGet("batch-clients/{batch}")]
        public async Task<IActionResult> GetBatchClients(int batch)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RE.AccountNo FROM PackageBalances PB INNER JOIN Recipients RE ON RE.SQLID = PB.PersonID WHERE PB.BatchNumber =  " + batch, (SqlConnection)conn))
                {
                    //cmd.Parameters.AddWithValue("@batchNo", batchNumber);
                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd[0]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("batchnumbers")]
        public async Task<IActionResult> GetBatchNo()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct 'Batch# ' + CONVERT(nVarchar, pb.BatchNumber) + ' Period End ' + Convert(nVarChar, [Date], 103) AS BatchDetail, Convert(nVarChar, [Date], 111) AS OrderByDate, pb.BatchNumber,b.BatchType  from PackageBalances pb INNER JOIN PAY_BILL_BATCH b ON pb.BatchNumber = b.BatchNumber WHERE      pb.BatchNumber > 0  AND b.BatchType = 'I'   ORDER BY Convert(nVarChar, [Date], 111) DESC, pb.BatchNumber DESC", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd[0]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("CDCbatchnumbers")]
        public async Task<IActionResult> GetCDCBatchNo()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select distinct 'Batch# ' + CONVERT(nVarchar, BatchNumber) + ' Period End ' + Convert(nVarChar, [Date], 103) AS BatchDetail, Convert(nVarChar, [Date], 111) AS OrderByDate, BatchNumber  from PackageBalances WHERE BatchNumber > 0 AND BatchType = 'CDC' ORDER BY Convert(nVarChar, [Date], 111) DESC, BatchNumber DESC", (SqlConnection)conn))
                {

                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd[0]));
                    }
                    return Ok(list);
                }
            }
        }
        [HttpGet("Billingbatchnumbers")]
        public async Task<IActionResult> GetBillingBatchNo()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT CONVERT(nVarchar, bch_num) +' ' + bch_user+ ' - '+ Convert(nVarChar, bch_date, 103) AS BatchDetail FROM batch_record WHERE bch_type = 'B' ORDER BY bch_date DESC, bch_num", (SqlConnection)conn))
                {

                    await conn.OpenAsync();
                    List<string> list = new List<string>();
                    using (SqlDataReader rd = await cmd.ExecuteReaderAsync())
                    {
                        while (await rd.ReadAsync())
                        {
                            list.Add(GenSettings.Filter(rd["BatchDetail"]));
                        }
                        return Ok(list);
                    }
                }
            }
        }

        [HttpGet, Route("case-staff-list/{uniqueid}")]
        public async Task<IActionResult> GetCaseStaff(string uniqueid)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
               SELECT ss.[Contact Details], ss.UniqueID FROM (
		            SELECT staff.sqlid,
			               CASE WHEN staff.lastname = '' THEN ' ' ELSE staff.lastname END + ', ' +
			               CASE
			               WHEN staff.firstname = '' THEN ' ' ELSE staff.firstname END AS
			               [Contact Details],
			               staff.UniqueID
		            FROM   staff
		            WHERE  casemanager = 1
            ) ss where ss.[Contact Details] NOT IN (
	            SELECT staffGroup.[Contact Details]  FROM (SELECT s.sqlid,
			               CASE WHEN s.lastname = '' THEN ' ' ELSE s.lastname END + ', ' +
			               CASE
			               WHEN s.firstname = '' THEN ' ' ELSE s.firstname END AS
			               [Contact Details]
		            FROM   staff s 
		            left join HumanResources hr on hr.[Name] = s.UniqueID
		            WHERE s.casemanager = 1 AND
		            hr.PersonID = @id
	            ) staffGroup
            ) ORDER BY ss.[Contact Details]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@id", uniqueid);

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            Name = GenSettings.Filter(rd["Contact Details"]),
                            UniqueId = GenSettings.Filter(rd["UniqueID"])
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("case-staff-programs")]
        public async Task<IActionResult> GetCaseStaffPrograms()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER([Name]) as ProgramName FROM HumanResourceTypes WHERE [Group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >= GETDATE())  ORDER BY [ProgramName]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["ProgramName"]));
                    }
                    return Ok(list);
                }
            }
        }
        //SELECT DISTINCT UPPER(Description) FROM DataDomains WHERE [Domain] = 'ACTIVITYGROUPS' AND User2 = 'MEALS' 	
        [HttpGet("group-meals-runsheet")]
        public async Task<IActionResult> GetGroupMeals()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT UPPER(Description) as MealsGroup FROM DataDomains WHERE [Domain] = 'ACTIVITYGROUPS' AND User2 = 'MEALS'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["MealsGroup"]));
                    }
                    return Ok(list);
                }
            }
        }

        [HttpPost("case-staff")]
        public async Task<IActionResult> PostCaseStaff([FromBody] CaseStaffDto details)
        {
            await listService.PostCaseStaff(details);
            return Ok();
        }

        [HttpDelete("case-staff/{recordNumber}")]
        public async Task<IActionResult> PostCaseStaff(int recordNumber)
        {
            await listService.DeleteCaseStaff(recordNumber);
            return Ok();
        }

        [HttpGet("case-staff-populate-field/{uniqueid}")]
        public async Task<IActionResult> GetCaseStaffPopulate(string uniqueid)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
SELECT [personid],
       recordnumber,
       [contact type],
       [name],
       [program],
       CASE WHEN primary_contact <> '' THEN primary_contact ELSE '' END + ', ' +
       CASE
       WHEN other_contact <> '' THEN other_contact ELSE '' END AS
       [Contact Details]
FROM   (SELECT humanresources.personid                                   AS
               [PERSONID],
               recordnumber,
               humanresources.[group]                                    AS
               [Category],
               Upper(humanresources.[type])                              AS
                      [Contact Type],
               CASE WHEN staff.firstname = '' THEN ' ' ELSE staff.firstname END
               + ' ' +
                      CASE
               WHEN staff.lastname = '' THEN ' ' ELSE staff.lastname END AS
               [Name],
               humanresources.[address1]                                 AS
               [Program],
               (SELECT TOP 1 primarycontact
                FROM   (SELECT P.personid,
                               P.type + ' ' + CASE WHEN detail <> '' THEN detail
                               ELSE
                               ' ' END
                               AS
                                       PrimaryContact
                        FROM   phonefaxother AS P
                        WHERE  primaryphone = 1
                               AND ( private IS NULL
                                      OR private = 0 )
                               AND P.personid = staff.uniqueid) AS P1)   AS
                      PRIMARY_CONTACT,
               (SELECT TOP 1 othercontact
                FROM   (SELECT P.personid,
                               P.type + ' ' + CASE WHEN detail <> '' THEN detail
                               ELSE
                               ' ' END
                               AS
                                       OtherContact
                        FROM   phonefaxother AS P
                        WHERE  primaryphone <> 1
                               AND ( private IS NULL
                                      OR private = 0 )
                               AND P.personid = staff.uniqueid) AS P2)   AS
                      OTHER_CONTACT
        FROM   humanresources
               INNER JOIN staff
                       ON humanresources.[name] = staff.uniqueid
        WHERE  humanresources.personid = @id
               AND [group] = 'COORDINATOR') AS T ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();
                    cmd.Parameters.AddWithValue("@id", uniqueid);

                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["recordnumber"]),
                            Name = GenSettings.Filter(rd["Name"]),
                            Program = GenSettings.Filter(rd["Program"]),
                            ContactDetails = GenSettings.Filter(rd["Contact Details"])
                        });
                    }
                    return Ok(list);
                }
            }
        }

        [HttpGet("populate-time-attendance-field/{field}")]
        public async Task<IActionResult> GetPopulateTimeAttendanceFilters(string field)
        {
            var user = await loginService.GetCurrentUser();

            var userInfo = await (from ui in _context.UserInfo
                                  where ui.Name == user.User
                                  select new
                                  {
                                      ui.ViewAllBranches,
                                      ui.ViewFilterBranches,
                                      ui.ViewAllCategories,
                                      ui.ViewFilterCategory,
                                      ui.ViewAllCoordinators,
                                      ui.ViewFilterCoord,
                                      ui.ViewAllStaffCategories,
                                      ui.ViewFilterStaffCategory
                                  }).FirstOrDefaultAsync();

            var filterString = string.Empty;
            var sql = string.Empty;
            List<string> list = new List<string>();

            if (field == "BRANCHES")
            {
                if (userInfo.ViewAllBranches == true)
                {
                    return Ok(await (from dd in _context.DataDomains where dd.Domain == "BRANCHES" orderby dd.Description select dd.Description).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterBranches;
                    sql = string.Format(@"SELECT [description]
                    FROM   datadomains dd
                    WHERE  dd.domain = 'BRANCHES'
                    AND dd.[description] IN (
	                    SELECT DISTINCT(Branch) FROM Recipients WHERE {0}
	                ) ORDER BY [description]", filterString);
                }
            }

            if (field == "STAFFTEAM")
            {
                return Ok(await (from dd in _context.DataDomains where dd.Domain == "STAFFTEAM" orderby dd.Description select dd.Description).ToListAsync());
            }

            if (field == "STAFFGROUP")
            {
                if (userInfo.ViewAllCategories== true)
                {
                    return Ok(await (from dd in _context.DataDomains where dd.Domain == "STAFFGROUP" orderby dd.Description select dd.Description).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterCategory;
                    sql = string.Format(@"SELECT [description]
                        FROM   datadomains dd
                        WHERE  dd.domain = 'STAFFGROUP'
                        AND dd.[description] IN (
	                    SELECT DISTINCT(AgencyDefinedGroup ) FROM Recipients WHERE {0}
                    ) ORDER BY [description]", filterString);
                }
            }

            if (field == "CASEMANAGERS")
            {
                if (userInfo.ViewAllCoordinators== true)
                {
                    return Ok(await (from dd in _context.DataDomains where dd.Domain == "CASE MANAGERS" orderby dd.Description select dd.Description).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterCoord;
                    sql = string.Format(@"SELECT [description]
                    FROM   datadomains dd
                    WHERE  dd.domain = 'CASE MANAGERS'
                    AND dd.[description] IN (
	                    SELECT DISTINCT([PAN_MANAGER]) FROM Staff WHERE {0}
	                ) ORDER BY [description]", filterString);
                }
            }
            if (field == "RECIPIENTCOORDINATOR")
            {


                sql = string.Format(@"SELECT [description]
                    FROM   datadomains dd
                    WHERE  dd.domain = 'CASE MANAGERS'
                    AND dd.[description] IN (
	                    SELECT DISTINCT([RECIPIENT_CoOrdinator]) FROM Recipients
	                ) ORDER BY [description]");

            }
            if (field == "VEHICLES")
            {
                if (userInfo.ViewAllCoordinators== true)
                {
                    return Ok(await (from dd in _context.DataDomains where dd.Domain == "VEHICLES" orderby dd.Description select dd.Description).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterCoord;
                    sql = string.Format(@"SELECT [description]
                    FROM   datadomains dd
                    WHERE  dd.domain = 'CASE MANAGERS'
                    AND dd.[description] IN (
	                    SELECT DISTINCT(RECIPIENT_CoOrdinator) FROM Recipients WHERE {0}
	                ) ORDER BY [description]", filterString);
                }
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["description"]));
                    }
                }
            }

            return Ok(list);
        }

        [HttpGet("user-view-tab-filters/{field}/{name}")]
        public async Task<IActionResult> GetViewTabFilters(string field, string name)
        {
            var user = await loginService.GetCurrentUser();

            var userInfo = await (from ui in _context.UserInfo
                                  where ui.Name == user.User
                                  select new
                                  {
                                      ui.ViewAllBranches,
                                      ui.ViewFilterBranches,
                                      ui.ViewAllCategories,
                                      ui.ViewFilterCategory,
                                      ui.ViewAllCoordinators,
                                      ui.ViewFilterCoord,
                                      ui.ViewAllStaffCategories,
                                      ui.ViewFilterStaffCategory

                                  }).FirstOrDefaultAsync();

            var filterString = string.Empty;
            var sql = string.Empty;
            List<string> list = new List<string>();

           if (field == "BRANCHES")
            {

                //Console.WriteLine(userInfo.ViewAllBranches);

                if (userInfo.ViewAllBranches)
                {
                    return Ok(await (
                        from dd in _context.DataDomains
                        where dd.Domain == "BRANCHES"
                        orderby dd.Description
                        select dd.Description
                    ).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterBranches;

                    var query = @"
                        SELECT [description]
                        FROM   datadomains dd
                        WHERE  dd.domain = 'BRANCHES'
                        AND    dd.[description] IN (
                            SELECT DISTINCT(Branch) 
                            FROM Recipients 
                            WHERE " + filterString + @"
                        )
                        ORDER BY [description]";

                    var descriptions = await _context.DataDomains
                        .FromSqlRaw(query)
                        .Select(dd => dd.Description)
                        .ToListAsync();

                    return Ok(descriptions);
                }
            }

            if (field == "STAFFTEAM")
            {
                return Ok(await (from dd in _context.DataDomains where dd.Domain == "STAFFTEAM" orderby dd.Description select dd.Description).ToListAsync());
            }

            if (field == "STAFFGROUP")
            {
                if (userInfo.ViewAllCategories)
                {
                    return Ok(await (from dd in _context.DataDomains where dd.Domain == "STAFFGROUP" orderby dd.Description select dd.Description).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterCategory;
                    sql = string.Format(@"SELECT [description]
                        FROM   datadomains dd
                        WHERE  dd.domain = 'STAFFGROUP'
                        AND dd.[description] IN (
	                    SELECT DISTINCT(AgencyDefinedGroup ) FROM Recipients WHERE {0}
                    ) ORDER BY [description]", filterString);
                }
            }

            if (field == "CASEMANAGERS")
            {
                if (userInfo.ViewAllCoordinators)
                {
                    return Ok(await (from dd in _context.DataDomains where dd.Domain == "CASE MANAGERS" orderby dd.Description select dd.Description).ToListAsync());
                }
                else
                {
                    filterString = userInfo.ViewFilterCoord;
                    sql = string.Format(@"SELECT [description]
                    FROM   datadomains dd
                    WHERE  dd.domain = 'CASE MANAGERS'
                    AND dd.[description] IN (
	                    SELECT DISTINCT(RECIPIENT_CoOrdinator) FROM Recipients WHERE {0}
	                ) ORDER BY [description]", filterString);
                }
            }


            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(sql, (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["description"]));
                    }
                }
            }

            return Ok(list);
        }


        [HttpPost("mtapending")]
        public async Task<IActionResult> GetMtaPending([FromBody] MTAPendingInputDto input)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"GetMTAPending", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.CommandType = CommandType.StoredProcedure;

                    // cmd.Parameters.AddWithValue("@Date", input.GenerateDateString());
                    // cmd.Parameters.AddWithValue("@LocalTimezoneOffset", input.LocalTimezoneOffset);
                    // cmd.Parameters.AddWithValue("@Branches", input.GenerateBranchesString());
                    // cmd.Parameters.AddWithValue("@Managers", input.GenerateCoordinatorString());
                    // cmd.Parameters.AddWithValue("@JobCategories", input.GenerateCategoryString());

                    cmd.Parameters.AddWithValue("@Date", input.Date);
                    cmd.Parameters.AddWithValue("@LocalTimezoneOffset", input.LocalTimezoneOffset);
                    cmd.Parameters.AddWithValue("@Branches", input.Branches);
                    cmd.Parameters.AddWithValue("@Managers", input.Coordinators);
                    cmd.Parameters.AddWithValue("@JobCategories", input.Categories);
                    cmd.Parameters.AddWithValue("@teams", input.Teams);
                    cmd.Parameters.AddWithValue("@TAType", input.TAType);

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();
                    List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                    int numCol = rd.FieldCount;
                    while (rd.Read())
                    {
                        // CREATE KEYVALUE DICTIONARY
                        Dictionary<string, object> dic = new Dictionary<string, object>();

                        // LOOP COLUMNS IN ROW
                        for (var a = 0; a < numCol; a++)
                        {
                            dic.Add(rd.GetName(a).Replace(" ", ""), rd.GetValue(a) != DBNull.Value
                                ? rd.GetValue(a)
                                : "");
                        }


                        list.Add(dic);
                    }


                    // List<MTAPendingDto> list = new List<MTAPendingDto>();

                    // //await rd.NextResultAsync();
                    // //await rd.NextResultAsync();

                    // while (await rd.ReadAsync())
                    // {
                    //     list.Add(new MTAPendingDto()
                    //     {
                    //         JobNo =  GenSettings.Filter(rd["JobNo"]),
                    //         Staff = GenSettings.Filter(rd["Staff"]),
                    //         Recipient = GenSettings.Filter(rd["Recipient"]),
                    //         ServiceType = GenSettings.Filter(rd["Service Type"]),
                    //         RosterStart = GenSettings.Filter(rd["Roster Start"]),

                    //         Duration = GenSettings.Filter(rd["Durtn"]),
                    //         RosterEnd = GenSettings.Filter(rd["Roster End"]),
                    //         S1 = GenSettings.Filter(rd["S1"]),
                    //         S2 = GenSettings.Filter(rd["S2"]),
                    //         StartVar = GenSettings.Filter(rd["StartVAR"]),
                    //         PanlateStartTh = Convert.ToString(GenSettings.Filter(rd["PanlateStartTh"])),
                    //         TaMultishift = GenSettings.Filter(rd["TA_Multishift"]),
                    //     });
                    // }
                    return Ok(list);
                }
            }
        }

        [HttpGet("accounting-history/{clientCode}")]
        public async Task<IActionResult> GetAccountingHistory(string clientCode)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                    SELECT   sqlid,
                             [Traccs Processing Date]         AS [Date],
                             [Invoice Number]                 AS [Number],
                             CONVERT(MONEY, [Invoice Amount]) AS amount,
                             CASE
                                      WHEN htype = 'r' THEN 0
                                      ELSE CONVERT(money, [Invoice Tax])
                             END                                                           AS gst,
                             CONVERT(money, isnull([Invoice Amount], 0) - isnull(paid, 0)) AS [O/S] ,
                             CASE
                                      WHEN LEFT([Invoice Number], 1) = 'r' THEN 'payment'
                                      WHEN LEFT([Invoice Number], 1) = 'a' THEN 'adjust'
                                      WHEN LEFT([Invoice Number], 1) = 'c' THEN 'credit'
                                      ELSE 'invoice'
                             END AS [Type],
                             package,
                             notes
                    FROM     invoiceheader
                    WHERE    [Client Code] = @clientCode
                    ORDER BY [TRACCS PROCESSING DATE]", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@clientCode", clientCode);

                    List<AccountingHistoryDto> list = new List<AccountingHistoryDto>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new AccountingHistoryDto()
                        {
                            SqlId = GenSettings.Filter(rd["SqlId"]),
                            Date = GenSettings.Filter(rd["Date"]),
                            Number = GenSettings.Filter(rd["Number"]),
                            Amount = GenSettings.Filter(rd["Amount"]),
                            Gst = GenSettings.Filter(rd["Gst"]),
                            Os = GenSettings.Filter(rd["O/S"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Package = GenSettings.Filter(rd["Package"]),
                            Notes = GenSettings.Filter(rd["Notes"])
                        });
                    }

                    return Ok(list);
                }
            }
        }

        [HttpGet("accounting-invoices/{clientCode}")]
        public async Task<IActionResult> GetAccountingInvoices(string clientCode)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                                 SELECT sqlid,
                                [traccs processing date]                                      AS [Date],
                                [invoice number]                                              AS [Number]
                                ,
                                CONVERT(MONEY, [invoice amount])                              AS
                                Amount,
                                CASE
                                    WHEN htype = 'R' THEN 0
                                    ELSE CONVERT(MONEY, [invoice tax])
                                END                                                           AS GST,
                                CONVERT(MONEY, Isnull([invoice amount], 0) - Isnull(paid, 0)) AS [O/S],
                                CASE
                                    WHEN LEFT([invoice number], 1) = 'R' THEN 'PAYMENT'
                                    WHEN LEFT([invoice number], 1) = 'A' THEN 'ADJUST'
                                    WHEN LEFT([invoice number], 1) = 'C' THEN 'CREDIT'
                                    ELSE 'INVOICE'
                                END                                                           AS [Type],
                                package,
                                notes
                            FROM   invoiceheader
                            WHERE  [client code] = @clientCode
                                AND htype = 'I'
                            ORDER  BY [traccs processing date] ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@clientCode", clientCode);

                    List<AccountingHistoryDto> list = new List<AccountingHistoryDto>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new AccountingHistoryDto()
                        {
                            SqlId = GenSettings.Filter(rd["SqlId"]),
                            Date = GenSettings.Filter(rd["Date"]),
                            Number = GenSettings.Filter(rd["Number"]),
                            Amount = GenSettings.Filter(rd["Amount"]),
                            Gst = GenSettings.Filter(rd["Gst"]),
                            Os = GenSettings.Filter(rd["O/S"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Package = GenSettings.Filter(rd["Package"]),
                            Notes = GenSettings.Filter(rd["Notes"])
                        });
                    }

                    return Ok(list);
                }
            }
        }


        [HttpGet("accounting-receipts/{clientCode}")]
        public async Task<IActionResult> GetAccountingReceipts(string clientCode)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"
                                SELECT sqlid,
                                [traccs processing date]                                      AS [Date],
                                [invoice number]                                              AS [Number]
                                ,
                                CONVERT(MONEY, [invoice amount])                              AS
                                Amount,
                                CASE
                                    WHEN htype = 'R' THEN 0
                                    ELSE CONVERT(MONEY, [invoice tax])
                                END                                                           AS GST,
                                CONVERT(MONEY, Isnull([invoice amount], 0) - Isnull(paid, 0)) AS [O/S],
                                CASE
                                    WHEN LEFT([invoice number], 1) = 'R' THEN 'PAYMENT'
                                    WHEN LEFT([invoice number], 1) = 'A' THEN 'ADJUST'
                                    WHEN LEFT([invoice number], 1) = 'C' THEN 'CREDIT'
                                    ELSE 'INVOICE'
                                END                                                           AS [Type],
                                package,
                                notes
                            FROM   invoiceheader
                            WHERE  [client code] = @clientCode
                                AND htype IN ( 'R', 'A' )
                            ORDER  BY [traccs processing date] ", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@clientCode", clientCode);

                    List<AccountingHistoryDto> list = new List<AccountingHistoryDto>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new AccountingHistoryDto()
                        {
                            SqlId = GenSettings.Filter(rd["SqlId"]),
                            Date = GenSettings.Filter(rd["Date"]),
                            Number = GenSettings.Filter(rd["Number"]),
                            Amount = GenSettings.Filter(rd["Amount"]),
                            Gst = GenSettings.Filter(rd["Gst"]),
                            Os = GenSettings.Filter(rd["O/S"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            Package = GenSettings.Filter(rd["Package"]),
                            Notes = GenSettings.Filter(rd["Notes"])
                        });
                    }

                    return Ok(list);
                }
            }
        }

        [HttpGet("receipts/loans/{clientCode}")]
        public async Task<IActionResult> getRecipientLoans(string clientCode)
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT RecordNumber, Name, Date1 As [DateLoaned], Date2 As [Collected] ,
                                                        PersonID, [Group],[Type], Address1 as program, DateInstalled, Notes, Creator, ReminderCreator, SubType, User2,
                                                        EquipmentCode,SerialNumber,LockBoxCode,LockBoxLocation,PurchaseAmount,PurchaseDate
                                                        FROM HumanResources 
                                                        WHERE [PersonID] = @clientCode AND [GROUP] = 'LOANITEMS' 
                                                        ORDER BY State ASC, [Name] ASC", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    cmd.Parameters.AddWithValue("@clientCode", clientCode);



                    List<dynamic> list = new List<dynamic>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(new
                        {
                            RecordNumber = GenSettings.Filter(rd["RecordNumber"]),
                            Name = GenSettings.Filter(rd["Name"]),
                            DateLoaned = GenSettings.Filter(rd["DateLoaned"]),
                            Collected = GenSettings.Filter(rd["Collected"]),
                            PersonID = GenSettings.Filter(rd["PersonID"]),
                            Group = GenSettings.Filter(rd["Group"]),
                            Type = GenSettings.Filter(rd["Type"]),
                            program = GenSettings.Filter(rd["program"]),
                            DateInstalled = GenSettings.Filter(rd["DateInstalled"]),
                            Notes = GenSettings.Filter(rd["Notes"]),
                            EquipmentCode = GenSettings.Filter(rd["EquipmentCode"]),
                            SerialNumber = GenSettings.Filter(rd["SerialNumber"]),
                            LockBoxCode = GenSettings.Filter(rd["LockBoxCode"]),
                            LockBoxLocation = GenSettings.Filter(rd["LockBoxLocation"]),
                            PurchaseAmount = GenSettings.Filter(rd["PurchaseAmount"]),
                            PurchaseDate = GenSettings.Filter(rd["PurchaseDate"])

                        });
                    }
                    return Ok(list);


                }
            }
        }


        [HttpGet("contribution-activity")]
        public async Task<IActionResult> GetContributionActivity()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"select Title from itemtypes where processclassification = 'OUTPUT' AND ISNULL(EndDate, '') = ''", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["Title"]));
                    }

                    return Ok(list);
                }
            }
        }

        [HttpGet("staff-column")] 
         public async Task<IActionResult> GetStaffCoulmnNames()
        {
            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(@"SELECT COLUMN_NAME
                    FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE TABLE_NAME = 'Staff' AND COLUMN_NAME != 'SQLID'", (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    List<string> list = new List<string>();
                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd["COLUMN_NAME"]));
                    }

                    return Ok(list);
                }
            }
        }

        [HttpPost("staff-dataforcsv")]
        public async Task<IActionResult> GetStaffData([FromBody] List<string> selectedColumns)

        {
            if (selectedColumns == null || selectedColumns.Count == 0)
            {
                return BadRequest("No columns selected");
            }

            // Ensure column names are safe to use in SQL (avoid SQL injection)
            string columns = string.Join(", ", selectedColumns.Select(col => $"[{col}]")); // Wrap columns in brackets

            string query = $"SELECT {columns} FROM Staff";

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    await conn.OpenAsync();
                    SqlDataReader reader = await cmd.ExecuteReaderAsync();

                    List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

                    while (await reader.ReadAsync())
                    {
                        Dictionary<string, object> row = new Dictionary<string, object>();
                        foreach (var col in selectedColumns)
                        {
                            row[col] = reader[col]; // Get data dynamically
                        }
                        data.Add(row);
                    }

                    return Ok(data);
                }
            }
        }






        [HttpGet("starts-with-value/{tabs}/{key}")]
        public async Task<IActionResult> GetStartsWithValues(string tabs, string key)
        {
            List<string> list = new List<string>();
            bool hasValue = false;
            string value = "";

            if (tabs == "rhistory")
            {
                hasValue = SQL_STARTSWITH_HISTORY.TryGetValue(key, out value);
            }

            if (tabs == "ropnote")
            {
                hasValue = SQL_STARTSWITH_OPNOTE.TryGetValue(key, out value);
            }

            if (tabs == "rcasenote")
            {
                hasValue = SQL_STARTSWITH_CASENOTE.TryGetValue(key, out value);
            }

            if (tabs == "rincidents")
            {
                hasValue = SQL_STARTSWITH_INCIDENTS.TryGetValue(key, out value);
            }

            if (tabs == "rdocuments")
            {
                hasValue = SQL_STARTSWITH_DOCUMENTS.TryGetValue(key, out value);
            }

            if (!hasValue)
            {
                return Ok(list);
            }

            using (var conn = _context.Database.GetDbConnection() as SqlConnection)
            {
                using (SqlCommand cmd = new SqlCommand(value, (SqlConnection)conn))
                {
                    await conn.OpenAsync();

                    SqlDataReader rd = await cmd.ExecuteReaderAsync();

                    while (await rd.ReadAsync())
                    {
                        list.Add(GenSettings.Filter(rd[0]));
                    }

                    return Ok(list);
                }
            }
        }

        //[HttpGet("intake/programs/{personID}")]
        //public async Task<IActionResult> GetIntakePrograms(string personID)
        //{
        //    return await (from rp in _context.RecipientPrograms
        //                  join hr in _context.HumanResourceTypes on rp.Program equals hr.Name
        //                  where rp.PersonID == personID && rp.ProgramStatus != "INACTIVE"
        //                  select rp.Program).OrderBy(x => x).ToListAsync();
        //}


    }


    public class RecipientSearch
    {
        public string AccountNo { get; set; }
        public int RowNo { get; set; }
    }


    public class ReportCriteria
    {
        public string ListType { get; set; }
        public bool IncludeInactive { get; set; }
    }

    public class ProgramOption
    {
        public string Program { get; set; }
        public string Option { get; set; }
    }

    public class ProgramInputs
    {
        public string Branch { get; set; }
        public string FundingType { get; set; }
        public string Type { get; set; }
        public string Group { get; set; }
    }

  public class  ChargeTypeInput{
     public int Index { get; set; }
    public string Program { get; set; }
    public string AccountNo { get; set; }
    
  }

    public class notifyaddresses
    {
        public string label { get; set; }
        public bool check { get; set; }
    }


    public static class ProgramRecipientTypes
    {
        public const string REFER_IN = "REFER_IN";
        public const string REFER_ON = "REFER_ON";
        public const string NOT_PROCEED = "NOT_PROCEED";
        public const string ASSESS = "ASSESS";
        public const string ADMIT = "ADMIT";
        public const string WAIT_LIST = "WAIT_LIST";
        public const string DISCHARGE = "DISCHARGE";
        public const string SUSPEND = "SUSPEND";
        public const string REINSTATE = "REINSTATE";
        public const string DECEASE = "DECEASE";
        public const string ADMIN = "ADMIN";
        public const string ITEM = "ITEM";
    }

    public class ServiceParameter{
    public string AccountNo { get; set; }
    public string ServiceType { get; set; }
    public string Program { get; set; }
     public string DocHdrId { get; set; }
     public string ViewFactor { get; set; }

     
    
}
public class OniIssues{
    public string PersonId { get; set; }
    public string Action { get; set; }
    public string Description { get; set; }
     public string RecordNumber { get; set; }

     public int type { get; set; }

    public string code { get; set; }
   

     
}
public class Code_Program{
    public string id { get; set; }
    public string code { get; set; }
    public string program { get; set; }  
     
}
public class Deactivate{
  
    public string id { get; set; }
      public string code { get; set; }
    public string startDate { get; set; }
    public string endDate { get; set; }
     public bool deleteRecipient { get; set; }
    public bool deleteRecipientMasterRosters { get; set; }
    public bool deleteRecipientRostersDateRange { get; set; }   
    public bool FinalConfirmation { get; set; }   
    public string user  { get; set; }  

}
public class Codes{
    public string OldCode { get; set; }
    public string NewCode { get; set; }
  
     
}
  public class Pay_Period {        
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
        public int? PayPeriod_Length { get; set; }
   
        public string PayPeriodEndDate { get; set; }

        
    }
}