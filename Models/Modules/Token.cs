namespace Adamas.Models.Modules{
    public class Token{
        public string User { get; set; }
        public string Access_token { get; set; }
        public double Expires { get; set; }
        public string Refresh_token { get; set; }
    }
}