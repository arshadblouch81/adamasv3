import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { forkJoin,  Subject ,  Observable } from 'rxjs';
import  { GlobalService, TimeSheetService, ClientService } from '@services/index';

@Component({
  selector: 'modal-details',
  templateUrl: './modal-details.component.html',
  styleUrls: ['./modal-details.component.css']
})
export class ModalDetailsComponent implements OnInit {

  @Input() open: boolean = false;
  @Input() data: any;

  private _tempStore: any;
  private serviceInfoDuration =  new Subject<any>();

  selectedOption: any;

  windowShow = new Subject<number>();
  window: number = 1;

  whoTab: any = {
      sInfo: true,
      billing: false,
      payInfo: false,
      mta: false
  }

  duration: any;

  categories: Array<any>;
  programs: Array<any>;
  activities: Array<any>;

  constructor(
    private globalS: GlobalService,
    private clientS: ClientService,
    private timeS: TimeSheetService
  ) { 

    this.serviceInfoDuration.subscribe(data => {
        this.duration = this.globalS.computeTime(this.selectedOption.starttime, this.selectedOption.endtime).durationStr
    });

    this.windowShow.subscribe(data => {
      if(this.window == 1){
        this.resetShowDetail()
      }
      this.window = data;
    });

  }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges){
    for(let property in changes){
        this.resetShowDetail();
        if(property == 'open' && !changes[property].firstChange && changes[property].currentValue != null){
            this.open = true;
        }

        if(property == 'data' && !changes[property].firstChange && changes[property].currentValue != null){
           this.selectedOption = null;
           this.selectedOption = this.mapData(changes[property].currentValue)
           this.populate(this.selectedOption)
           console.log(this.selectedOption)
        }
    }
  }

  mapData(data: any): any{

    return {
      recipient: data.clientCode,
      recordno: data.recordNo,
      starttime: data.start_Time,
      endtime: data.end_Time,
      date: data.roster_Date,
      program: data.program,
      activity: data.serviceType,
      location: data.serviceSetting,
      attendees: data.attendees,
      category: data.anal
    }

  }

  resetShowDetail(){
    this.whoTab = {
        sInfo: true,
        billing: false,
        payInfo: false,
        mta: false
    }
    this.window = 1;
  }

  populate(data: any){
    this.timeS.getlistcategories().subscribe(data => this.categories = data);
    this.clientS.getactiveprogram({
        IsActive: true,
        Code: data.recipient
    }).subscribe(data => this.programs = data.data );

    this.timeS.getlistservices(data.recipient).subscribe(result => {  
        let res = result;                  
        res.push(data.activity);
        this.activities = res;            
    });
  }





}
