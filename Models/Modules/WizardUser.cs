namespace Adamas.Models.Modules{
    public class WizardUser{
        public string User { get; set; }
        public string PersonId { get; set; }
    }
}