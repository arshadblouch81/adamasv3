import { Component, OnInit, OnDestroy, Input, AfterViewInit, ChangeDetectorRef } from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ListService, GlobalService, PrintService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { AnyMxRecord } from 'dns';
import { NzMessageService } from 'ng-zorro-antd/message';
import { UploadChangeParam } from 'ng-zorro-antd/upload';

const inputFormDefault = {

    branchesArr: [[]],
    allBranches: [true],

    managersArr: [[]],
    allManager: [true],
    
    recipientmanagersArr: [[]],
    recipientManager: [false],

    programsArr: [[]],
    allPrograms: [true],

    categoriesArr: [[]],
    allCategories: [true],

    inputBatchnNumber: [1],
    packagesArr: [[]],
    allPackages: [true],
    batchNoArr: [[]],
    batchclientsArr:[[]],
    batchprogramArr:[[]],

    groupbyCaseManagers: [false],    
}

@Component({
    styleUrls: ['./billing.css'],
    templateUrl: './hcp.html',
    selector: 'hcp-component',
})
export class HCPComponent implements OnInit, OnDestroy, AfterViewInit {

    dateFormat: string = 'dd/MM/yyyy';
    format1 = (): string => `1`;
    format2 = (): string => `2`;
    format3 = (): string => `3`;
    format4 = (): string => `4`;
    format5 = (): string => `5`;
    format6 = (): string => `6`;
    format7 = (): string => `7`;
    format8 = (): string => `8`;
    format9 = (): string => `9`;
    CircleSize = 26;


    hcpRollbackClaimPrep: any;
    hcpRollbackAging: any;
    hcpRollbackClaimUpload: any;
    setHcpRates: any;
    importPriceGuide: any;
    hcpClaimRateUpdate: any;
    hcpBulkBalanceEdit: any;
    hcpDataCheck: any;
    hcpPackageAuditReport: any;
    hcpBalanceExport: any;
    hcpCreateClaimUpload: any;
    hcpClaimPreparation: any;
    hcpPackageStatusSummary: any;
    eventID: string;

    //Mufeed
    HCP_isVisibleTop = false;
    HCP_ModalName: string;
    HCP_modalbodystyle: object;
    id: string;
    btnid: string;
    inputForm: FormGroup;
    drawerVisible: boolean = false;
    loading: boolean = false;
    frm_Recipients: boolean;
   
    frm_BatchNo : boolean;
    frm_Branches: boolean;
    frm_Programs: boolean;
    frm_Categories: boolean;
    frm_Managers: boolean;
    frm_Packages: boolean;
   
    frm_Date: boolean;
    enddate: Date;
    startdate: Date;
    branchesArr: Array<any> = [];
    programsArr: Array<any> = [];
    managersArr: Array<any> = [];
    recipientmanagersArr: Array<any> = [];
    categoriesArr: Array<any> = [];
    packagesArr: Array<any> = [];
    batchNoArr: Array<any> = [];
    batchclientsArr : Array<any> = [];
    batchprogramArr:Array<any> = [];
    tryDoctype: any;
    tocken: any;
    pdfTitle: string;
    s_BranchSQL: string;
    s_PackageSQL:string;
    s_ProgramSQL: string;
    s_CategorySQL: string;
    s_ManagersSQL: string;
    s_RecipCoordinatorSQL: string;
    s_DateSQL: string;
    chkbx_grpbyCaseManagers: boolean;
    groupbyCaseManagers: [false];
    


    //Mufeed's END

    constructor(
        private fb: FormBuilder,
        private listS: ListService,
        private ModalS: NzModalService,
        private cd: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private printS: PrintService,
        private GlobalS: GlobalService,
    ) {

    }

    ngOnInit(): void {
        //Mufeed
        var date = new Date();
        this.startdate = new Date(date.getFullYear(), date.getMonth(), 1);
        this.enddate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        this.inputForm = this.fb.group(inputFormDefault);

        this.inputForm.get('allBranches').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                branchesArr: []
            });
        });
        this.inputForm.get('allPrograms').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                programArr: []
            });
        });
        this.inputForm.get('allCategories').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                categoriesArr: []
            });
        });
        this.inputForm.get('allManager').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                managersArr: []
            });
        });
        this.inputForm.get('recipientManager').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                recipientmanagersArr: []
            });
        }); 
        this.inputForm.get('allPackages').valueChanges.subscribe(data => {
            this.inputForm.patchValue({
                packagesArr: []
            });
        });
          


        //Mufeed END

    }

    ngOnDestroy(): void {

    };

    ngAfterViewInit(): void {

        //mufeed
        /*this.listS.getreportcriterialist({
            listType: 'BRANCHES',
            includeInactive: false
        }).subscribe(x => this.branchesArr = x); */
        this.listS.getreportcriterialist({
            listType: 'PROGRAMS',
            includeInactive: false
        }).subscribe(x => this.programsArr = x);
        this.listS.getserviceregion().subscribe(x => this.categoriesArr = x);
        this.listS.getlisttimeattendancefilter("BRANCHES").subscribe(x => this.branchesArr = x);
        this.listS.getlisttimeattendancefilter("CASEMANAGERS").subscribe(x => this.managersArr = x);
        this.listS.getlisttimeattendancefilter("RECIPIENTCOORDINATOR").subscribe(x => this.recipientmanagersArr = x);
        this.listS.GetCDCpackages().subscribe(x => this.packagesArr = x);
        this.listS.GetCDCBatchNo().subscribe(x => {this.batchNoArr = x;});        
        this.tocken = this.GlobalS.pickedMember ? this.GlobalS.GETPICKEDMEMBERDATA(this.GlobalS.GETPICKEDMEMBERDATA):this.GlobalS.decode();
        //end mufeed

    };

    hcpEvent(i) {
        i = i || window.event;
        i = i.target || i.srcElement;
        this.eventID = (i.id).toString();

        switch (this.eventID) {
            case 'btn-rollbackClaimPrepBatch':
                this.hcpRollbackClaimPrep = {}
                break;
            case 'btn-rollbackHcpAging':
                this.hcpRollbackAging = {}
                break;
            case 'btn-rollbackClaimUploadBatch':
                this.hcpRollbackClaimUpload = {}
                break;
            case 'btn-setHcpRates':
                this.setHcpRates = {}
                break;
            case 'btn-importPriceGuide':
                this.importPriceGuide = {}
                break;
            case 'btn-updateHcpRates':
                this.hcpClaimRateUpdate = {}
                break;
            case 'btn-bulkBalanceEdit':
                this.hcpBulkBalanceEdit = {}
                break;
            case 'btn-hcpDataCheck':
                this.hcpDataCheck = {}
                break;
            case 'btn-packageAuditReport':
                this.hcpPackageAuditReport = {}
                break;
            case 'btn-packageStatusSummary':
                this.hcpPackageStatusSummary = {}
                break;
            case 'btn-hcpBalanceExport':
                this.hcpBalanceExport = {}
                break;
            case 'btn-hcpCreateClaimUpload':
                this.hcpCreateClaimUpload = {}
                break;
            case 'btn-hcpClaimPreparation':
                this.hcpClaimPreparation = {}
                break;
            case 'btn-HCPClientBilling':
                break;

            default:
                break;
        }
    }

    //Mufeed
    HCP_Modal(e) {
        //console.log(e)
        e = e || window.event;
        e = e.target || e.srcElement;
        this.btnid = (e.id).toString();


        switch (this.btnid) {
            case 'btn-FeeVerification':
                this.HCP_ModalName = "HCP Fee Verificaton Report"
                //this.frm_Date = true;
                this.frm_Branches = true;
                this.frm_Programs = true;
                this.frm_Categories = true;
                this.frm_Managers = true;
                this.HCP_isVisibleTop = true;
                break;
            case 'btn-LeaveVerification':
                this.HCP_ModalName = "HCP Leave Verificaton Report"
                this.frm_Date = true;
                this.frm_Branches = true;
                this.HCP_isVisibleTop = true;
                break;            
            case 'btn-ClaimVerification':                                
                this.HCP_ModalName = "HCP Claim Verificaton Report"                
                this.frm_Branches = true; 
                this.frm_Programs = true; 
                this.frm_Categories = true; 
                this.frm_Managers = true; 
                this.chkbx_grpbyCaseManagers = true;               
                this.HCP_isVisibleTop = true;
                break;
            case 'btn-PackageAuditReport':                                               
                break;
            case 'btn-HCPStatement':              
                this.HCP_ModalName = "HCP Print Statement" 
                this.frm_BatchNo = true;               
                this.frm_Branches = true; 
                this.frm_Packages = true;                            
                this.HCP_isVisibleTop = true;                                            
                break;
            case 'btn-HCPDataCheck':
                var Title = "Package Integrity Check";
                var id = "pzVJRyM6MMWmVbdH"
                this.HCPListing(id,Title);
                break;
            case 'btn-HCPUnApprovedServices':
                var Title = "HCP UnApproved Services";
                var id = "LFhfPLSvvLh3o9Ye"
                this.HCPListing(id,Title);            
                break;
            default:
                console.log(this.btnid)
                break;

        }


    }
    HCP_MOdalCancel(){
        this.HCP_isVisibleTop = false;        
        this.drawerVisible = false;
        this.ResetVisibility();

    }
    HCP_MOdalOk() {
        this.ReportRender(this.btnid)
        this.ResetVisibility();
        this.HCP_isVisibleTop = false;

        this.tryDoctype = "";
        this.btnid = "";

    }
    ReportRender(btnid) {

        var strdate;
        var endate;
        var date = new Date();

        if (this.startdate != null) { strdate = format(this.startdate, 'yyyy/MM/dd') } else {
            //strdate = "2020-07-01"              
            strdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy/MM/dd')
        }
        if (this.enddate != null) { endate = format(this.enddate, 'yyyy/MM/dd') } else {
            // endate = "2020-07-31" strdate endate
            endate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy/MM/dd');
        }

        var s_Branches = this.inputForm.value.branchesArr;               
        var s_Categories = this.inputForm.value.categoriesArr;
        var s_Programs = this.inputForm.value.programsArr;        
        var s_Managers = this.inputForm.value.managersArr;
        var s_RecipientManagers = this.inputForm.value.recipientmanagersArr;
        var s_groupby = this.inputForm.value.groupbyCaseManagers;
        var s_Batch = this.inputForm.value.batchNoArr;
        var s_Package = this.inputForm.value.packagesArr;

        switch (btnid) {
            case 'btn-FeeVerification':
                this.FeeVerification(s_Branches, s_Managers,s_RecipientManagers,s_Categories,s_Programs,s_groupby);
                break;
            case 'btn-LeaveVerification':
                this.LeaveVerification(s_Branches,strdate,endate);
                break;                
            case 'btn-ClaimVerification':
                this.ClaimVerification(s_Branches, s_Managers,s_Categories,s_Programs,s_groupby);
                break;
            case 'btn-PackageAuditReport':
                this.PackageAuditReport();
                break;
                case 'btn-HCPStatement':
                let temp = forkJoin([
                    this.listS.GetCDCpackages() ,                        
                    this.listS.GetBatchClients(s_Batch)                    
                ]) 
                temp.subscribe(data => {       
                    this.batchprogramArr = data[0];
                    this.batchclientsArr = data[1] ;  
                        this.PackageStatement(this.batchprogramArr,this.batchclientsArr,s_Branches,s_Package)              
                }); 
                break;
                
            default:
                break;
        }

    }
    ResetVisibility() {
        this.frm_Date = false;
        this.frm_Branches = false;
        this.frm_Recipients = false;
        this.frm_Programs = false; 
        this.frm_Categories = false; 
        this.frm_Packages = false;
        this.frm_BatchNo= false;
        this.frm_Managers = false;
        this.pdfTitle = "";
        this.chkbx_grpbyCaseManagers = false;
        
       
       
    
        this.inputForm = this.fb.group(inputFormDefault);
    }
    FeeVerification(branch,program,categories,managers,RecipientManagers,groupby){
        
        var lblcriteria;
        var fQuery = " SELECT   RE.Accountno, RE.Branch, RE.AgencyDefinedGroup AS Category, RE.RECIPIENT_CoOrdinator,   IsNull(RE.FirstName, '') +   CASE WHEN IsNull(RE.FirstName, '') <> '' THEN ' ' + RE.[Surname/Organisation] ELSE RE.[Surname/Organisation] END AS RecipientName,   RE.BillTo AS DebtorName,   RP.Program AS Package,   ISNULL(RP.DailyBasicCareFee, 0) AS BasicFee,   ISNull(RP.DailyIncomeTestedFee, 0) AS IncomeTestedFee,   ISNull(RP.DailyAgreedTopUp, 0) as TopUpFee   FROM RecipientPrograms RP   INNER JOIN Recipients RE ON RP.PersonID = RE.UniqueID  WHERE PackageType = 'CDC-HCP' AND Right(Program, 11) <> 'CONTINGENCY' "

        if (branch != "") {
            this.s_BranchSQL = "[BRANCH] in ('" + branch.join("','") + "')";
            if (this.s_BranchSQL != "") { fQuery = fQuery + " AND " + this.s_BranchSQL }
        }
        if (program != "") {
            this.s_ProgramSQL = " (Program in ('" + program.join("','") + "'))";
            if (this.s_ProgramSQL != "") { fQuery = fQuery + " AND " + this.s_ProgramSQL }
        }
        if (categories != "") {
            this.s_CategorySQL = "[AgencyDefinedGroup] in ('" + categories.join("','") + "')";
            if (this.s_CategorySQL != "") { fQuery = fQuery + " AND " + this.s_CategorySQL }
        }
        if (managers != "") {
            this.s_ManagersSQL = "[PAN_MANAGER] in ('" + managers.join("','") + "')";
            if (this.s_ManagersSQL != "") { fQuery = fQuery + " AND " + this.s_ManagersSQL };
        }
        if (RecipientManagers != "") {
            this.s_RecipCoordinatorSQL = "re.[RECIPIENT_COORDINATOR] in ('" + RecipientManagers.join("','") + "')";
            if (this.s_RecipCoordinatorSQL != "") { fQuery = fQuery + " AND " + this.s_RecipCoordinatorSQL };
        }



        if (branch != "") {
            lblcriteria = " Branches:" + branch.join("','") + "; "
        }
        else { lblcriteria = lblcriteria + " All Branches " }

        if (categories != "") {

            lblcriteria = lblcriteria + " Regions: " + categories.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + "All Regions," }

        if (managers != "") {
            lblcriteria = lblcriteria + " Manager: " + managers.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + "All Managers," }

        if (program != "") {
            lblcriteria = lblcriteria + " Programs " + program.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + "All Programs." }


        fQuery = fQuery + " ORDER BY RECIPIENT_CoOrdinator, Accountno "
        
       //console.log(fQuery)
        const data = {
            "template": { "_id": "ijP4s274R5TQJ9EV" },

            "options": {
                "reports": { "save": false },

                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "groupby": groupby,                
                
            }
        }
        this.loading = true;
        var Title = "HCP Fee Verification Report"

        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = "HCP Fee Verification Report.pdf"
            this.drawerVisible = true;
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            console.log(err);
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });


        return;

    }
    LeaveVerification(branch, startdate, enddate) {

        var lblcriteria;
        var fQuery = " SELECT [CLIENT CODE], CDC_Level ,  [LEAVETYPE], Convert(nvarchar, DateAdd(D, 0, MIN([DATE])),23) AS START_DATE,convert(nvarchar, DATEADD(D, 0, MAX([DATE])),23) AS END_DATE, COUNT(*) AS CONTINUOUS_DAYS FROM ( SELECT DISTINCT [CLIENT CODE], IT.[MINORGROUP] AS LEAVETYPE, HRT.User3 as CDC_Level, RE.[BRANCH], RO.[DATE], DATEADD(D,-DENSE_RANK() OVER ( PARTITION BY [CLIENT CODE] ORDER BY [DATE]),[DATE] ) AS RANKDATE FROM ROSTER RO  INNER JOIN RECIPIENTS RE ON RO.[Client Code] = RE.Accountno  INNER JOIN ITEMTYPES IT ON RO.[Service Type] = IT.Title  INNER JOIN HumanResourceTypes HRT ON HRT.Name = RO.Program AND HRT.[GROUP] = 'PROGRAMS'  WHERE "


        //"(RO.[DATE] BETWEEN '2017/08/01' AND '2020/08/31') "
        if (startdate != "" || enddate != "") {
            this.s_DateSQL = "( RO.[DATE] BETWEEN '" + startdate + ("'AND'") + enddate + "')";
            if (this.s_DateSQL != "") { fQuery = fQuery + " " + this.s_DateSQL };
        }
        fQuery = fQuery + " AND IT.[MINORGROUP] IN ('FULL DAY-HOSPITAL', 'FULL DAY-RESPITE', 'FULL DAY-SOCIAL LEAVE', 'FULL DAY-TRANSITION')"

        if (branch != "") {
            this.s_BranchSQL = "RE.[BRANCH] in ('" + branch.join("','") + "')";
            if (this.s_BranchSQL != "") { fQuery = fQuery + " AND " + this.s_BranchSQL }
        }
        fQuery = fQuery + ") AS T GROUP BY [CLIENT CODE], CDC_Level, [LEAVETYPE], RANKDATE, BRANCH "

        if (startdate != "") {
            lblcriteria = " Date Between " + startdate + " and " + enddate + "; "
        }
        if (branch != "") {
            lblcriteria = " Branches:" + branch.join("','") + "; "

        }

        else { lblcriteria = lblcriteria + " All Branches " }

        //console.log(fQuery)

        const data = {

            "template": { "_id": "PEsoPqmbOzdz6shH" },

            "options": {
                "reports": { "save": false },

                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,

            }
        }
        this.loading = true;
        var Title = "HCP Leave Verification Report"

        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = "HCP Leave Verification Report.pdf"
            this.drawerVisible = true;
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
            this.cd.detectChanges();
        }, err => {
            console.log(err);
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });


        return;
    }
    ClaimVerification(branch,program,categories,managers,groupby){
        
        var lblcriteria;
        var fQuery = " SELECT * FROM (SELECT RE.Accountno, RE.Branch, RE.AgencyDefinedGroup AS Category, RE.[MedicareRecipientID],  RE.RECIPIENT_CoOrdinator, IsNull(RE.FirstName, '') + CASE WHEN IsNull(RE.FirstName, '') <> '' THEN ' ' + RE.[Surname/Organisation] ELSE RE.[Surname/Organisation] END AS RecipientName, RP.Program AS Package, Quantity AS [Daily Claim], case when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 1, 5) <> '00000' Then 1 ELSE 0 END AS [Dementia/Cognition/Veterans], case when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 6, 1) <> '0' Then 1 ELSE 0 END AS [Oxygen], case when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 8, 1) <> '0' Then 1 ELSE 0 END AS [Enteral-Bolus], case when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 9, 1) <> '0' Then 1 ELSE 0 END AS [Enteral-Non Bolus], case when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 10, 1) <> '0' Then 1 ELSE 0 END AS [EACHD], case when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 12, 7) <> '0000000' Then 1      when substring(ISNULL(packagesupplements, '00000000000000000000000000') + '00000000000000000000000000', 20, 7) <> '0000000' Then 1 ELSE 0 END AS [Viability], convert(Money, IsNull(HardshipSupplement, 0))   As HardshipSupplement FROM RecipientPrograms RP INNER JOIN Recipients RE ON RP.PersonID = RE.UniqueID INNER JOIN HumanResourceTypes PR ON RP.Program = PR.[Name] WHERE ISNULL(UserYesNo1, 0) = 1 AND UPPER(ISNULL(User2, '')) <> 'CONTINGENCY'  AND ISNULL(UserYesNo3, 0) = 0) t where AccountNo is not null "
            
       
        if (branch != "") {                        
            this.s_BranchSQL = "[BRANCH] in ('" + branch.join("','") + "')";         
               if (this.s_BranchSQL != "") { fQuery = fQuery + " AND " + this.s_BranchSQL }        }       
        if (program != "") {
            this.s_ProgramSQL = " (Package in ('" + program.join("','") + "'))";
            if (this.s_ProgramSQL != "") { fQuery = fQuery + " AND " + this.s_ProgramSQL }
        }
        if (categories != "") {
            this.s_CategorySQL = "[Category] in ('" + categories.join("','") + "')";
            if (this.s_CategorySQL != "") { fQuery = fQuery + " AND " + this.s_CategorySQL }
        }
        if (managers != "") {
            this.s_ManagersSQL = "[RECIPIENT_CoOrdinator] in ('" + managers.join("','") + "')";
            if (this.s_ManagersSQL != "") { fQuery = fQuery + " AND " + this.s_ManagersSQL };
        }


        
        if (branch != "") {
            lblcriteria = " Branches:" + branch.join("','") + "; "
        }
        else { lblcriteria =  " All Branches " }
        
        if (categories != "") {

            lblcriteria = lblcriteria + " Regions: " + categories.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + "All Regions," }

        if (managers != "") {
            lblcriteria = lblcriteria + " Manager: " + managers.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + "All Managers," }

        if (program != "") {
            lblcriteria = lblcriteria + " Programs " + program.join(",") + "; "
        }
        else { lblcriteria = lblcriteria + "All Programs." }


        fQuery = fQuery + " ORDER BY  RECIPIENT_CoOrdinator, Accountno  "



        //console.log(fQuery)
        const data = {
            "template": { "_id": "U4rB7nwpLXzpYQxw" },
            
            "options": {
                "reports": { "save": false },

                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "groupby": groupby,                
                
            }
        }
        this.loading = true;
       var Title = "HCP Claim Verification Report" 
               
         this.drawerVisible = true;
       this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "HCP Claim Verification Report.pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
                });
            

        return;

    }
    PackageAuditReport(){    
        
                       
            const data = {
                "template": { "_id": "01BzAAKU6wg1ihHo" },                
                "options": {
                    "reports": { "save": false },                                            
                    "userid": this.tocken.user,
                    //"groupby": groupby,                                    
                }
            }
            this.loading = true;
            var Title = "Package Audit List" 
                   
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
                        this.pdfTitle = "Package Audit List.pdf"
                        this.drawerVisible = true;                   
                        let _blob: Blob = blob;
                        let fileURL = URL.createObjectURL(_blob);
                        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                        this.loading = false;
                        this.cd.detectChanges();
                    }, err => {
                        console.log(err);
                        this.loading = false;
                        this.ModalS.error({
                            nzTitle: 'TRACCS',
                            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                            nzOnOk: () => {
                                this.drawerVisible = false;
                            },
                        });
                    }); 
                    this.tryDoctype = "";        
                    this.btnid = "";                           
            return;        
    }
    PackageStatusReport(){    
            
                        
        const data = {
            "template": { "_id": "BShdx1HihQlxzrsO" },                
            "options": {
                "reports": { "save": false },                                            
                "userid": this.tocken.user,
                //"groupby": groupby,                                    
            }
        }
        this.loading = true;
        var Title = "Package Status Summary List" 
            
        this.drawerVisible = true;
        this.printS.printControl(data).subscribe((blob: any) => {
                    this.pdfTitle = "Package Status Summary.pdf"
                    this.drawerVisible = true;                   
                    let _blob: Blob = blob;
                    let fileURL = URL.createObjectURL(_blob);
                    this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                    this.loading = false;
                    this.cd.detectChanges();
                }, err => {
                    console.log(err);
                    this.loading = false;
                    this.ModalS.error({
                        nzTitle: 'TRACCS',
                        nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                        nzOnOk: () => {
                            this.drawerVisible = false;
                        },
                    });
                });    
                this.tryDoctype = "";        
                this.btnid = "";                
        return;        
    }
    PackageStatement(pro,client,branch,packages) {

        var batchclients = "('"+client.join("','")+"')"
        var batchprogram = "('"+pro.join("','")+"')"
         
         if (branch != "") {
             this.s_BranchSQL = " ( in ('" + branch.join("','") + "'))";      
         }
         if (packages != "") {
             this.s_PackageSQL = " (package in ('" + packages.join("','") + "'))";       
         }
         
         var lblcriteria;
         /*if (batch != "") {
             lblcriteria = lblcriteria + " Batch Number: " + batch + "; "
         }
         else {
             lblcriteria = lblcriteria + " All Batches "
         } */
         if (branch != "") {
             lblcriteria = lblcriteria + " Service Type " + branch.join(",") + "; "
         }
         else { lblcriteria = lblcriteria + " All Svc. Types " }
         if (packages != "") {
             lblcriteria = lblcriteria + " Service Type " + packages.join(",") + "; "
         }
         else { lblcriteria = lblcriteria + " All Packages " }
             
         
         this.drawerVisible = true;
         
         const data = {
             "template": { "_id": "QXubVDxampiDXX2o" },
             "options": {
                 "reports": { "save": false },
                 
             //    "sql": fQuery,
                 "Criteria": lblcriteria,
                 "userid": this.tocken.user, 
                 "branches": "",
                 "packages":"",                          
                 "PatientCode" :batchclients,
                 "Programs":batchprogram,
                 "ClientCode": batchclients
     
                 
                 
             }
         }
         this.loading = true;
         this.drawerVisible = true;
         
         this.printS.printControl(data).subscribe((blob: any) => {
             this.pdfTitle = "HCP Package Statement.pdf"
             this.drawerVisible = true;                   
             let _blob: Blob = blob;
             let fileURL = URL.createObjectURL(_blob);
             this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
             this.loading = false;
             this.cd.detectChanges();
         }, err => {
             this.loading = false;
                 this.ModalS.error({
                     nzTitle: 'TRACCS',
                     nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                     nzOnOk: () => {
                         this.drawerVisible = false;
                     },
                 });
         });
     }
     HCPListing(id,Title){

        var date = new Date();
        var startdate = format(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy/MM/dd')
        var enddate = format(new Date(date.getFullYear(), date.getMonth() + 1, 0), 'yyyy/MM/dd');
                              
            const data = {
                "template": { "_id": id },                
                "options": {
                    "reports": { "save": false },                                            
                    "userid": this.tocken.user,                    
                    "sdate": "(Date Between '" +startdate +"' and '" + enddate + "')",                    
                    //"groupby": groupby,                                    
                }
            }
            this.loading = true;                             
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
                        this.pdfTitle = Title+".pdf"
                        this.drawerVisible = true;                   
                        let _blob: Blob = blob;
                        let fileURL = URL.createObjectURL(_blob);
                        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                        this.loading = false;
                        this.cd.detectChanges();
                    }, err => {
                        console.log(err);
                        this.loading = false;
                        this.ModalS.error({
                            nzTitle: 'TRACCS',
                            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                            nzOnOk: () => {
                                this.drawerVisible = false;
                            },
                        });
                    });    
                    this.tryDoctype = "";        
                    this.btnid = "";                
            return;                
     }
     
    //End Mufeed


}
