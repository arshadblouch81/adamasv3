import { Component, OnInit, OnDestroy, Input, AfterViewInit, SimpleChanges, OnChanges, ChangeDetectorRef } from '@angular/core'
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray, } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ClientService, GlobalService, ListService, PrintService, TimeSheetService } from '@services/index';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient, HttpHeaders, HttpParams, } from '@angular/common/http';
import { Observable, Observer, Subject, observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import 'bootstrap/dist/css/bootstrap.min.css';

import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'purchase-order',
  templateUrl: './purchaseorder.html',
  styleUrls: ['./purchaseorder.css']


})

export class PurchaseOrder implements OnInit, OnDestroy {

  dateFormat: string = 'dd/MM/yyyy';
  format1 = (): string => `1`;
  format2 = (): string => `2`;
  format3 = (): string => `3`;
  format4 = (): string => `4`;
  format5 = (): string => `5`;
  private unsubscribe = new Subject();
  tab: number = 1;
  activeButton: boolean = true;
  staffCode: any;
  FeeCode: any;
  PONo: any;
  NRecordNo: any;
  recipient: any;
  selectedProgram: any;
  selectedService: any;
  SuppliedUnit: any;
  BilledUnit: any;
  date: any;
  sDate: any;
  eDate: any;
  Requestor: any
  HighlightRow: number = 0;
  HighlightRow2: number = 0;
  HighlightRow3: number = 0;
  lstOrderCreated: Array<any> = [];
  rptDataSet: Array<any> = [];
  lstPrograms: Array<any> = [];
  lstServices: Array<any> = [];
  lstStaff: Array<any> = [];
  lstRecipients: Array<any> = [];
  lstFeeCodes: Array<any> = [];
  lstFormats: Array<any> = [
    { name: 'Adobe Acrobat (.PDF)', format: '.pdf' },
    { name: 'Microsoft Excel (.XLS)', format: '.xls' },
    { name: 'Microsoft Rich Text (.RTF)', format: '.rtf' },
    { name: 'Web File (.html)', format: '.html' },
    { name: 'Tagged Image Format (.TIF)', format: '.tif' },
    { name: 'Standard Text (.TXT)', format: '.txt' },
    { name: 'Comma Seprated File (.CSV)', format: '.csv' },

  ];
  selectedFormat: string = '.pdf';
  FileName: string = 'Traccs Report.pdf';
  Name: string;
  Phone: string;
  Email: string;
  jobContactName: string;
  jobContactNo: string
  Jobtype: string = 'Home modification job or equipement/supply purchase';
  specialInstrtuction: string;
  orderDetail: string;
  billingDetail: string;
  ItemUnit: string;
  SupplierQty: number;
  SupplierPrice: number;
  SupplierTotal: number;
  Frequency: string = 'ONCE OFF';
  PoType: any;
  dayslected: any
  period: any;

  s_StartTime: string = '09:00'
  s_Duration: number = 24;
  billRate: string;
  enableNext: boolean;
  Form1: FormGroup;
  token: any;
  today = new Date();
  blockNo: number = 108;
  defaultStartTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);
  defaultDuration: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 2, 0, 0);
  showProgram: boolean;
  showExport: boolean;
  done: boolean;
  refreshModel: boolean;
  Pattern: any = 'First';
  IsMaster: boolean;
  processing: boolean;
  reportView: boolean;

  pdfTitle: string;
  tryDoctype: any;
  drawerVisible: boolean =  false;
  tocken: any;
  loading: boolean = false;

  constructor(
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private modalService: NzModalService,
    private timesheetS: TimeSheetService,
    private listS: ListService,
    private clientS: ClientService,
    private globalS: GlobalService,
    private formBuilder: FormBuilder,
    private printS: PrintService,
    private cd: ChangeDetectorRef,
  ) {

  }
  ngOnInit(): void {
    //this.buildForm();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    this.token = this.globalS.decode();
  }


  ngAfterViewInit(): void {
    this.loading_Staff()
  }

  ngModelChangeStart(event): void {

    this.s_StartTime = event

  }
  ngModelChangeDuration(event): void {

    this.defaultDuration = event
    this.setDuration();

  }

  setDuration() {
    let defaultStartTime = (this.defaultStartTime);
    // let date: Date = new Date();
    //date.setDate(date.getDate() + this.defaultStartTime.getDate());
    let defaultEndTime = new Date(this.defaultDuration);

    defaultEndTime.setHours(defaultStartTime.getHours() + defaultEndTime.getHours());
    defaultEndTime.setMinutes(defaultStartTime.getMinutes() + defaultEndTime.getMinutes());

    let durationObject = this.globalS.computeTimeDATE_FNS(defaultStartTime, defaultEndTime);

    this.s_Duration = durationObject.duration;
    this.blockNo = durationObject.blockNo;

  }
  buildForm() {
    this.Form1 = this.formBuilder.group({
      Name: ['', Validators.required],
      Email: ['', Validators.required],
      Phone: ['', Validators.required],

    });
    this.Form1.get('Name').valueChanges.pipe(
      takeUntil(this.unsubscribe)
    ).subscribe(d => {
      const { Name, Email, Phone } = this.Form1.value;
      this.Name = Name;
      this.Email = Email;
      this.Phone = Phone;
      this.setNextButtonStatus();

    });
  }
  onDayChange({ target }) {
    this.dayslected = target.value;
    this.setNextButtonStatus();
  }
  onPeriodChange({ target }) {
    this.period = target.value;
    this.setNextButtonStatus();
  }
  onChangeDate({ target }) {
    this.setNextButtonStatus();
  }
  onOptionChange({ target }) {

    this.PoType = target.value;
    console.log(this.PoType);

  }
  setNextButtonStatus() {

    console.log(this.staffCode)
    this.enableNext = false;
    if (this.PoType == 'Recurring Service')
      this.date = this.sDate;

    if ((this.tab == 1 && this.Name != null && this.Email != null && this.Phone != null)
      || (this.tab == 2 && this.date != null && this.selectedProgram != null && this.selectedService != null)
      || (this.tab == 3 && this.staffCode != null && this.FeeCode != null && this.SuppliedUnit != null && this.SupplierQty != null && this.SupplierPrice != null && this.BilledUnit != null && this.billRate != null)
      || (this.tab == 4 && this.jobContactName != null && this.jobContactNo != null && this.billingDetail != null && this.orderDetail != null && this.specialInstrtuction != null)
    ) {

      if ((this.tab == 2 && this.PoType == 'Recurring Service') && (this.dayslected == null || this.period == null || this.Pattern == null || this.sDate == null || this.eDate == null))
        this.enableNext = false;
      else
        this.enableNext = true;
    }
  }


  onPoChange({ target }) {
    if (this.PoType == this.Jobtype) {
      this.s_StartTime = '09:00'
      this.s_Duration = 24;
    }

    this.enableNext = false;
  }
  onValueChanges({ target }) {

    //console.log(target.value);
    this.setNextButtonStatus();

  }
  onChange(result: Date): void {
    // console.log('onChange: ', result);
    this.date = result;

    this.setNextButtonStatus()
  }

  nextClick() {
    //this.showExport=true
    this.enableNext = false;
    this.tab += 1;
    this.view(this.tab);
    this.setNextButtonStatus();
  }
  prevClick() {
    this.tab -= 1;
    this.view(this.tab);
    this.setNextButtonStatus();
  }
  calculate() {
    this.SupplierTotal = this.SupplierPrice * this.SupplierQty;
    this.setNextButtonStatus();
  }
  onItemSelected(type: string, data: any, i: number) {
    if (type == 'Program') {
      this.HighlightRow = i;
      this.selectedProgram = data.name;
      this.load_Data('Service')
    } else if (type == 'Service') {
      this.selectedService = data.name;
      this.HighlightRow2 = i;
    }

    this.setNextButtonStatus();
  }
  onFormatSelected(data: any, i: number) {

    this.HighlightRow3 = i;
    this.selectedFormat = data.format;
    this.FileName = this.PONo + this.selectedFormat;
  }
  view(index: number) {

    this.tab = index;
    this.activeButton = false;
    // console.log(index);
    if (index == 1) {
      //  this.router.navigate(['/admin/close-roster-period']);
    }
    if (index == 2) {
      if (this.lstRecipients.length <= 0)
        this.load_Data('Recipient');

    }
    if (index == 3) {
      if (this.lstStaff.length <= 0)
        this.load_Data('Staff');
      if (this.lstFeeCodes.length <= 0)
        this.load_Data('Fee');
      //   this.router.navigate(['/admin/rollback-invoice-batch']);
    }

  }
  view2(index: number) {

  }
  ngOnDestroy(): void {
  }

  loading_Staff() {
    let sql = `SELECT ISNULL([FirstName], '') + ' ' + ISNULL([LastName], '') AS RequestorName, 
              (SELECT TOP 1 CASE WHEN Detail <> '' THEN Detail ELSE ' ' END  
              FROM PhoneFaxOther WHERE PersonID = UniqueID AND Type = '<EMAIL>') AS Email, 
              (SELECT TOP 1 CASE WHEN Detail <> '' THEN Detail ELSE ' ' END   
              FROM PhoneFaxOther WHERE PersonID = UniqueID AND Type = '<WORK>') AS WorkPhone, 
              (SELECT TOP 1  CASE WHEN Detail <> '' THEN Detail ELSE ' ' END  
              FROM PhoneFaxOther WHERE PersonID = UniqueID AND PrimaryPhone = 1) AS PrimaryPhone, 
              (SELECT TOP 1 CASE WHEN Detail <> '' THEN Detail ELSE ' ' END
              FROM PhoneFaxOther WHERE PersonID = UniqueID AND Type = '<MOBILE>') AS MobilePhone
              FROM Staff s INNER JOIN UserInfo u ON u.StaffCode = s.AccountNo WHERE [Name] = '${this.token.user}'`
    this.listS.getlist(sql).subscribe(d => {

      this.Requestor = d[0];

      this.Name = this.Requestor.requestorName;
      this.Email = this.Requestor.email;


      if (this.Requestor.workPhone != null && this.Requestor.workPhone != '')
        this.Phone = this.Requestor.workPhone;
      else if (this.Requestor.primaryPhone != null && this.Requestor.primaryPhone != '')
        this.Phone = this.Requestor.primaryPhone;
      else if (this.Requestor.mobilePhone != null && this.Requestor.mobilePhone != '')
        this.Phone = this.Requestor.mobilePhone;
      this.enableNext = true;
    });

  }
  load_Data(type: string) {




    if (type == 'Service') {
      let sql = ` SELECT DISTINCT [Service Type] AS name, RosterGroup FROM ServiceOverview sv  INNER JOIN Recipients re ON re.uniqueid = sv.personid  INNER JOIN ItemTypes it ON it.Title = sv.[Service Type]  WHERE ServiceStatus = 'ACTIVE' 
            AND re.AccountNo = '${this.recipient}' AND [ServiceProgram] ='${this.selectedProgram}'  AND (IsNull(it.NoThursday, 0) = 0) AND ((ISNULL(IT.MinDurtn, 0) = 0 AND ISNULL(IT.MAXDurtn, 0) = 0) OR (ISNULL(IT.MinDurtn, 0) <= 0 AND IsNull(IT.MaxDurtn, 0) >= 0))`;
      this.listS.getlist(sql).subscribe(d => {

        this.lstServices = d.map(x => {
          return {
            name: x.name,
            group: x.rosterGroup
          }
        });
        this.setNextButtonStatus();
      });
    } else if (type == 'Program') {
      let sql2 = ` SELECT DISTINCT sv.[ServiceProgram] as name  FROM ServiceOverview sv  INNER JOIN Recipients re ON re.uniqueid = sv.personid  INNER JOIN ItemTypes it ON it.Title = sv.[Service Type]  WHERE ServiceStatus = 'ACTIVE' AND re.AccountNo = '${this.recipient}' `;

      this.listS.getlist(sql2).subscribe(d => {

        this.lstPrograms = d;
      });

    } else if (type == 'Recipient') {
      let sql2 = `select UniqueId, AccountNo from Recipients where AccountNo > '!z' AND  ISNULL(AdmissionDate, '') <> '' AND ISNULL(DischargeDate, '') = ''`;

      if (this.lstRecipients.length == 0 || this.lstRecipients == null) {
        this.listS.getlist(sql2).subscribe(d => {

          this.lstRecipients = d.map(x => {
            return {
              name: x.accountNo,
              uniqueId: x.uniqueId
            }
          });

        });

      }
    } else if (type == 'Staff') {
      let sql = `select s.AccountNo as accountNo, n.Address1 + ', ' + n.Address2 + ' ' + n.Suburb +' ' + n.Postcode as address, p.Detail as telephone, e.Detail as email
                from  Staff s 
                left join NamesAndAddresses n on s.UniqueID=n.PersonID and n.PrimaryAddress=1
                left join PhoneFaxOther p on p.PersonID=s.UniqueID and p.PrimaryPhone=1 and p.[Type]='<HOME>'
                left join PhoneFaxOther e on e.PersonID=s.UniqueID and e.PrimaryPhone=1 and e.[Type]='<EMAIL>'
                 WHERE ISNULL(CommencementDate, '') <> '' AND ISNULL(TerminationDate, '') = '' 
                      AND Category = 'BROKERAGE ORGANISATION' 
                      ORDER BY AccountNo`

      this.listS.getlist(sql).subscribe(d => {

        this.lstStaff = d.map(x => {
          return {
            name: x.accountNo,
            address: x.address,
            telephone: x.telephone,
            email: x.email
          }

        });

      });

    } else if (type == 'Fee') {
      let sql = `SELECT Recnum, Title FROM ItemTypes 
                      WHERE RosterGroup = 'SALARY' AND Status = 'NONATTRIBUTABLE' AND ProcessClassification = 'INPUT' 
                      AND (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111)) 
                      ORDER BY TITLE `

      this.listS.getlist(sql).subscribe(d => {

        this.lstFeeCodes = d.map(x => {
          return {
            name: x.title,
            selected: false
          }

        });

      });

    }


  }

  ProcessPO() {
    //var s_StartTime = Format$(txt(1), "00") & ":" & Format$(txt(2), "00")
    //var s_Duration = ((N2L(txt(3)) * 60) + N2L(txt(4))) / 5


    if (this.PoType == 'Recurring Service') {

      this.CreateRecurrentPO();


      return;


    }
    this.processing = true;
    let detail = this.getDetail();
    var poRoster: any = {
      ClientCode: this.recipient,
      CarerCode: this.staffCode,
      ServiceType: this.selectedService,
      ServiceDesc: this.FeeCode,
      Date: moment(this.date).format('YYYY/MM/DD'),
      Time: this.s_StartTime == null ? '09:00' : this.s_StartTime,
      Duration: this.s_Duration == null ? 108 : this.s_Duration,
      BlockNo: this.blockNo,
      Creator: this.token.user,
      Editer: this.token.user,
      BillUnit: this.BilledUnit,
      BillQty: this.SupplierQty,
      AgencyDefinedGroup: 'GRAFTAN',
      ReferralCode: '',
      TimePercent: 0,
      Notes: detail,
      Type: this.GetRosterType(),
      TabType: 'NEW DOCUMENT',
      HaccType: this.selectedService,
      Program: this.selectedProgram,
      BillTo: this.recipient,
      CostUnit: this.SuppliedUnit,
      BillDesc: this.billingDetail,
      UnitPayRate: this.SupplierPrice,
      UnitBillRate: this.billRate,
      TaxPercent: 0,
      ReasonType: '',
      PODetail: detail
    }
    console.log(poRoster);

    this.timesheetS.postpurchaseorder(poRoster).subscribe(d => {
      this.PONo = d.poNo;
      this.NRecordNo = d.recordNo;
      console.log(d);
      this.done = true;

      this.globalS.sToast('Purcahse Order', `Purcahse Order procesed successfully! (${this.PONo})`)
      this.processing = false;
      this.lstOrderCreated.push({
        poNo: d.poNo, recordNo: d.recordNo, date: poRoster.Date, time: this.s_StartTime, duration: this.s_Duration,
        frequency: this.Frequency, billRate: this.SupplierQty, total: this.SupplierTotal,
        supplier: this.lstStaff.filter(x => x.name == this.staffCode)[0],
        recipient: this.lstRecipients.filter(x => x.name == this.recipient)[0],
        requestor: { name: this.Name, email: this.Email, phone: this.Phone, address: this.Requestor.address },
        detail: { billDetail: this.billingDetail, orderDetail: this.orderDetail, specialInstrtuction: this.specialInstrtuction }

      })      
      this.rptDataSet = this.lstOrderCreated[0]
      console.log(this.rptDataSet)
    },
      error => {
        this.globalS.eToast('Purcahse Order', error)
        console.log(error);
      }

    );


  }

  getDetail() {
    var detail: string;
    detail = `Requestor Name : ${this.Name}  
               Requestor email : ${this.Email}  
               Requestor Phone : ${this.Phone}    
               Job Contact Name : ${this.jobContactName}
               Job Contact Number : ${this.jobContactNo}    
               Recipient : ${this.recipient}  
               Type : ${this.PoType}  
               Item/Service : ${this.selectedService}  
               Supplier : ${this.staffCode}    
               Billing Details : ${this.billingDetail}    
               Order Details : ${this.orderDetail}    
               Special Instructions :  ${this.specialInstrtuction}    
               Unit : ${this.BilledUnit}  
               Price : $ ${this.SupplierPrice}  
               Bill Qty : ${this.SupplierQty}  
               Frequency : ${this.Frequency}  
               Total : $ ${this.SupplierTotal}`;
    return detail;
  }

  CreateRecurrentPO() {
    let sdate = this.sDate;
    let edate = this.eDate;
    this.lstOrderCreated = [];
    this.processing = true;
    const obsvr = new Observable(observer => {
      setTimeout(() => {

        let detail = this.getDetail();
        while (sdate <= edate) {

          let dd = this.GetWeekday(sdate);

          if (dd == this.dayslected) {

            if (this.period == 'Fortnightly' && this.Pattern == 'Second') {
              let week = this.getWeekOfMonth(sdate) % 2;
              if (week == 1) {
                sdate.setDate(sdate.getDate() + 1);
                continue;
              }
            }

            var poRoster: any = {
              ClientCode: this.recipient,
              CarerCode: this.staffCode,
              ServiceType: this.selectedService,
              ServiceDesc: this.FeeCode,
              Date: moment(sdate).format('YYYY/MM/DD'),
              Time: this.s_StartTime == null ? '09:00' : this.s_StartTime,
              Duration: this.s_Duration == null ? 108 : this.s_Duration,
              BlockNo: this.blockNo,
              Creator: this.token.user,
              Editer: this.token.user,
              BillUnit: this.BilledUnit,
              BillQty: this.SupplierQty,
              AgencyDefinedGroup: 'GRAFTAN',
              ReferralCode: '',
              TimePercent: 0,
              Notes: detail,
              Type: this.GetRosterType(),
              TabType: 'NEW DOCUMENT',
              HaccType: this.selectedService,
              Program: this.selectedProgram,
              BillTo: this.recipient,
              CostUnit: this.SuppliedUnit,
              BillDesc: this.billingDetail,
              UnitPayRate: this.SupplierPrice,
              UnitBillRate: this.billRate,
              TaxPercent: 0,
              ReasonType: '',
              PODetail: detail
            }


            this.timesheetS.postpurchaseorder(poRoster).subscribe(d => {
              this.PONo = d.poNo;
              this.NRecordNo = d.recordNo;
              this.lstOrderCreated.push({
                poNo: d.poNo, recordNo: d.recordNo, date: d.date, time: d.time, duration: this.defaultDuration,
                frequency: this.Frequency, billRate: this.SupplierQty, total: this.SupplierTotal,
                supplier: this.lstStaff.filter(x => x.name == this.staffCode)[0],
                recipient: this.lstRecipients.filter(x => x.name == this.recipient)[0],
                requestor: { name: this.Name, email: this.Email, phone: this.Phone, Address: this.Requestor.address },
                detail: { billDetail: this.billingDetail, orderDetail: this.orderDetail, specialInstrtuction: this.specialInstrtuction }

              })
              this.rptDataSet = this.lstOrderCreated[0]
              this.done = true;


            },
              error => {
                // this.globalS.eToast('Purcahse Order', error)
                console.log(error);
              }

            );
            if (this.IsMaster) {
              poRoster.date = moment(new Date('1900/' + sdate.getMonth() + 1 + "/" + sdate.getDate())).format('YYYY/MM/DD');
              this.timesheetS.postpurchaseorder(poRoster).subscribe(d => {
                this.PONo = d.poNo;
                this.NRecordNo = d.recordNo;
                this.done = true;


              });
            }

            if (this.period == 'Weekly')
              sdate.setDate(sdate.getDate() + 7);
            if (this.period == 'Fortnightly')
              sdate.setDate(sdate.getDate() + 14);

          } else {
            sdate.setDate(sdate.getDate() + 1);
          }

        }

        observer.next(this.lstOrderCreated);

      }, 1000);
    });

    obsvr.subscribe(result => {
      setTimeout(() => {
        console.log(result);
        let d = result[0];
        this.processing = false;
        if (d.poNo != null) {
          this.globalS.sToast('Purcahse Order', `Purcahse Order procesed successfully! (${this.PONo})`);
        } else {
          this.globalS.eToast('Purcahse Order', `Something went wrong while processing! `);
        }
      }, 2000);
    });



    // if (this.lstDaysCreated.length > 0 && sdate > edate)      
    //   return true;
    // else
    //   return false;
    //this.globalS.sToast('Purcahse Order', `Purcahse Order procesed successfully! (${this.PONo})`)

  }



  GetWeekday(date: string): string {

    let dt = new Date(date);

    // return  this.DayOfWeek(dt.toLocaleDateString('en-US', { weekday: 'long' }));
    return dt.toLocaleDateString('en-US', { weekday: 'long' });

  }

  checkWeek() {

    let sdate = this.sDate;
    let edate = this.eDate;
    this.lstOrderCreated = [];
    while (sdate <= edate) {

      let dd = this.getWeekOfMonth(sdate)
      this.lstOrderCreated.push(moment(sdate).format('DD/MM/YYYY') + ' -> Week : ' + dd);
      sdate.setDate(sdate.getDate() + 1);
    }

    console.log(this.lstOrderCreated);



  }
  getWeekOfMonth(dt: Date) {
    var firstWeekday = new Date(dt.getFullYear(), dt.getMonth(), 1).getDay() - 1;
    if (firstWeekday < 0) firstWeekday = 6;
    var offsetDate = dt.getDate() + firstWeekday - 1;
    return Math.floor(offsetDate / 7) + 1;
  }
  AddRecurrentRoster() {
    let sdate = moment(this.sDate).format('YYYY/MM/DD');
    let edate = moment(this.eDate).format('YYYY/MM/DD');
    //let stime=  moment(this.sDate).format('YYY/MM/DD');
    let starttime = this.s_StartTime// format(this.s_StartTime,'HH:mm');

    let recordNo = this.NRecordNo

    if (this.period != "Weekly" || this.Pattern == null)
      this.Pattern = "Weekly";
    let inputs = {
      "recordNo": recordNo,
      "startDate": sdate,
      "endDate": edate,
      "days": this.dayslected,
      "frequency": this.period,
      "pattern": this.Pattern,
      "clientCodes": this.recipient
    }
    this.timesheetS.addRecurrentRosters(inputs).subscribe(data => {

      this.globalS.sToast('Purcahse Order', `Purcahse Orders procesed successfully! )`)


    });
  }

  GetRosterType(): number {
    var s_TextType: string = this.lstServices.filter(x => x.name == this.selectedService)[0].group;
    var i_Type: number;

    switch (s_TextType) {
      case 'ADMINISTRATION':
        i_Type = 6;
        break;
      case 'ADMISSION':
        i_Type = 7
        break;

      case 'BROKERED SERVICE':
        i_Type = 3
        return;
      case 'CENTREBASED':
        i_Type = 11;
        break;
      case 'ONEONONE':
        i_Type = 2;
        break;
      case 'GROUPACTIVITY':
        i_Type = 12
        break;
      case 'LEAVE/ABSENCE':
        i_Type = 4;
        break;
      case 'SLEEPOVER':
        i_Type = 8;
        break;
      case 'TRANSPORT':
        i_Type = 10;
        break;
      case 'TRAVEL TIME':
        i_Type = 5;
        break;
      case 'UNAVAILABILITY':
        i_Type = 13;
        break;
      case 'ITEM':
        i_Type = 14;
        break;
      case 'FEE':
        i_Type = 1;
        break;
      default:
        {
          if (s_TextType == 'ALLOWANCE' || s_TextType == 'ALLOWANCE CHARGEABLE' || s_TextType == 'ALLOWANCE NON-CHARGEABLE' || s_TextType == 'CHARGEABLE ITEM')
            i_Type = 9;
          else
            i_Type = 2;
          break;
        }
    }
    return i_Type;

  }
  export() {
    let personID: string = this.lstRecipients.filter(x => x.name == this.recipient)[0].uniqueId;
    let ddate: any;
    if (this.PoType == 'Recurring Service')
      ddate = this.sDate;
    else
      ddate = this.date;

    var data: any = {
      Program: this.selectedProgram,
      Creator: this.token.user,
      Filename: this.FileName,
      Title: this.PONo,
      PersonID: personID,
      FileClass: '0',
      Category: '',
      DocumentType: this.selectedFormat,
      OriginalLocation: '',
      Paramaters: '',
      DocumentGroup: 'DOCUMENT',
      SubCat: '',
      SubID: '0',
      D_ID: '0',
      AlarmDate: moment(ddate).format('YYYY/MM/DD'),
      AlarmText: '',
      Editer: this.token.user,

    }
    console.log(data);
    this.timesheetS.createdocument(data).subscribe(d => {
          
      //here code to export 
      
      this.closeExport();
      this.refreshModel = true;

    });



  }
  handleCancelTop(): void {
    this.drawerVisible = false;
    this.pdfTitle = ""
    this.tryDoctype = ""
}
  generatePdf(){
    var Title;            
  //console.log(this.dataSet)
    
      Title = "Transport Sheet";
      this.loading = true;
      this.drawerVisible = true;
    
               
        console.log(this.rptDataSet)      
        
            const data = {
                "template": { "_id": "IbzS5v8nFO47Phj1" },
                "options": {
                    "reports": { "save": false },                        
                    "sql": this.rptDataSet,                        
                    "userid": this.tocken.user,
                    "txtTitle":  Title,                      
                }
            }              
            this.loading = true;           
                    
            this.drawerVisible = true;
            this.printS.printControl(data).subscribe((blob: any) => {
                        this.pdfTitle = Title+".pdf"
                        this.drawerVisible = true;                   
                        let _blob: Blob = blob;
                        let fileURL = URL.createObjectURL(_blob);
                        this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                        this.loading = false;
                        this.cd.detectChanges();                       
                    }, err => {
                        console.log(err);
                        this.loading = false;
                        this.modalService.error({
                            nzTitle: 'TRACCS',
                            nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                            nzOnOk: () => {
                                this.drawerVisible = false;
                            },
                        });
            });                            
            return;        
           
             
}
  closeExport(){
   
    this.showExport = false
    this.refreshModel=true;
  }
  refreshForm() {
    this.tab = 1;
    this.refreshModel = false;
    this.showExport = false;
    this.done = false;
    this.showProgram = false;

    this.Name = null;
    this.Email = null;
    this.Phone = null;
    this.recipient = null;
    this.staffCode = null;
    this.selectedService = null;
    this.FeeCode = null;
    this.date = new Date();
    this.s_StartTime = null;
    this.s_Duration = null;
    this.blockNo = null;

    this.BilledUnit = null;
    this.SupplierQty = null;
    this.billingDetail = null;
    this.selectedService = null;
    this.selectedProgram = null;
    this.SuppliedUnit = null;
    this.billingDetail = null;
    this.orderDetail = null;
    this.specialInstrtuction = null;
    this.jobContactName = null;
    this.jobContactNo = null;
    this.SupplierPrice = null;
    this.billRate = null;
    this.lstPrograms = null;
    this.lstServices = null

  }

  public convetToPDF_print() {
    
    //if (this.tab==9)
  
    var data = document.getElementById('reportPO');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      const htmlWidth = document.getElementById('reportPO').clientWidth
      const htmlHeight = document.getElementById('reportPO').clientHeight;
      var imgWidth = htmlWidth;
      var pageHeight = 295;
      var imgHeight = htmlHeight; //canvas.height;// * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      let pages:number=this.lstOrderCreated.length;
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'A4'); // A4 size page of PDF
      var position = 0;
     console.log(htmlWidth,htmlHeight)
      for (let i = 1; i < pages; i++) {
        pdf.addPage([imgWidth, imgHeight], 'p');
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
       // 
       // pdf.addImage(imgData, 'png', topLeftMargin, - (pdfHeight * i) + (topLeftMargin * 4), canvasImageWidth, canvasImageHeight);
      }

      pdf.save( this.PONo + '.pdf'); // Generated PDF
      
    });
  }
  public convetToPDF() {
    
    //const htmlWidth2 = $("#print-section").width();
   
    const htmlWidth = document.getElementById('reportPO').clientWidth
    const htmlHeight = document.getElementById('reportPO').clientHeight;

    const topLeftMargin = 15;

    let pdfWidth = htmlWidth + (topLeftMargin * 2);
    let pdfHeight = (pdfWidth * 1.5) + (topLeftMargin * 2);

    const canvasImageWidth = htmlWidth;
    const canvasImageHeight = htmlHeight;

    const totalPDFPages = Math.ceil(htmlHeight / pdfHeight) - 1;

    const data = document.getElementById('reportPO');
    html2canvas(data, { allowTaint: true }).then(canvas => {

      canvas.getContext('2d');
      const imgData = canvas.toDataURL("image/jpeg", 1.0);
      let pdf = new jsPDF('p', 'pt', [pdfWidth, pdfHeight]);
      pdf.addImage(imgData, 'png', topLeftMargin, topLeftMargin, canvasImageWidth, canvasImageHeight);

      for (let i = 1; i <= totalPDFPages; i++) {
        pdf.addPage([pdfWidth, pdfHeight], 'p');
        pdf.addImage(imgData, 'png', topLeftMargin, - (pdfHeight * i) + (topLeftMargin * 4), canvasImageWidth, canvasImageHeight);
      }

      pdf.save(`${this.PONo }.pdf`);

    });
  }

}