import { Component, Input, OnInit, ElementRef, Output, EventEmitter, ViewChild, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core'
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling'

import { TimeSheetService, GlobalService } from '@services/index'
import { SharedService } from './sharedService/shared-service'
import { Subject } from 'rxjs'
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';

@Component({
    host: {
        '(document:click)': 'onClick($event)'
    },
    selector: 'searchlist',
    templateUrl: './search-list.html',
    styles: [`
    .search-container{
        position:relative;
    }
    input{
        height: 1rem;
        font-size: 15px;
        padding-bottom: 10px;
    }
    .list-scroll{
        position:absolute;
        background: #fff;
        width:100%;
        z-index: 10;
        box-shadow:0 3px 8px 0 rgba(0,0,0,0.2), 0 0 0 1px rgba(0,0,0,0.08);
        border-radius:3px;
    }
    ul{
        list-style:none;
    }
    li{
        padding:3px 10px;
    }
    ul li:hover:not(.active){
        cursor:pointer;
        background:#f2f2f2;
    }
    .active{
        background:#efefef;
        color:#177dff;
    }
    `]
})

export class SearchListComponent implements OnInit, AfterViewInit {
    @ViewChild(CdkVirtualScrollViewport, { static: false }) viewPort: CdkVirtualScrollViewport;

    @Output() picked = new EventEmitter();
    @Input() type: string = 'r';

    lists: Array<any>;

    show: boolean = false;
    showSeach: boolean = false;

    search: string;
    textChanges = new Subject<string>();
    index: number = 0;

    private _lists: Array<any>;

    constructor(
        private elem: ElementRef,
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private sharedS: SharedService
    ) {
        this.sharedS.emitOnSearchList$.subscribe(data => {
            if (data) {
                this.searchConstruct(data);
            }
        });
    }

    onClick(event: Event) {
        if (!this.elem.nativeElement.contains(event.target))
            this.show = false;
    }

    ngAfterViewInit() {
        const selectedIndex = this.lists.findIndex(elem => elem.accountNo === this.search);
        if (selectedIndex > -1) {
            this.viewPort.scrollToIndex(30);
        }
    }

    ngOnInit() {
        this.searchConstruct();
    }

    searchConstruct(accountName: any = null): void {
        this.textChanges
            .pipe(
                debounceTime(500),
                distinctUntilChanged()
            ).subscribe(data => {
                this.lists = this._lists.filter(x => {
                    if ((x.accountNo).indexOf(data.toUpperCase()) > -1) {
                        return x;
                    }
                })
                this.show = true;
            });

        if (this.type == 'r')
            this.searchRecipient(true, accountName);

        if (this.type == 's')
            this.searchStaff(true);
    }

    searchRecipient(initLoad: boolean = false, accountName: any = null): void {
        this.lists = []
        this.timeS.getrecipients({
            User: this.globalS.decode().nameid,
            SearchString: ''
        }).subscribe(data => {
            this._lists = data;
            this.lists = this._lists;
            this.showSeach = true;

            if (initLoad) {
                const index = accountName ? data.map((x: any) => x.accountNo).indexOf(accountName) : null;
                //133
                this.initLoad(2);
            }
        });
    }

    initLoad(index: number = null): void {
        //34
        const _index = index || -1;
        if (_index > -1)
            this.selected(_index)
    }

    openScroll() {
        this.show = true;
        if (this.viewPort) {
            this.viewPort.scrollToIndex(this.index || 0, 'smooth');
        }
    }

    searchStaff(initLoad: boolean = false) {
        this.lists = []
        this.timeS.getstaff({
            User: this.globalS.decode().nameid,
            SearchString: ''
        }).subscribe(data => {
            this._lists = data;
            this.lists = this._lists;
            this.showSeach = true;

            if (initLoad)
                this.initLoad()
        });
    }

    selected(index: number) {
        this.index = index;
        console.log(index);
        const _list = this.lists[index];

        this.search = _list.accountNo;
        this.picked.emit(_list);

        if (this.viewPort) {
            this.viewPort.scrollToIndex(index, 'smooth');
        }
    }

    get height(): string {
        if (this.lists.length > 10) {
            return 10 + 'rem';
        }
        return (this.lists.length * 30) + 'px';
    }

}