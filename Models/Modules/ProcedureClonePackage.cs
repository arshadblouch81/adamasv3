namespace Adamas.Models.Modules{
    public class ProcedureClonePackage {
        public string SourcePackage { get; set; }
        public string NewPackageName { get; set; }
        public string Type { get; set; }
        public string Level { get; set; }

       
    }
}