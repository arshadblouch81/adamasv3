
import {forkJoin ,  Subscription ,  Observable } from 'rxjs';
import { Component, OnInit,OnDestroy, ViewChild } from '@angular/core'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, GlobalService, ListService } from '@services/index'

import { BsModalComponent } from 'ng2-bs3-modal';
@Component({
    selector: '',
    templateUrl:'./groups.html'
})

export class IntakeGroups implements OnInit,OnDestroy{
    @ViewChild('userGroupModal', { static: false }) userGroupModal: BsModalComponent;
    @ViewChild('preferencesModal', { static: false }) preferencesModal: BsModalComponent;

    user: any;
    dbAction: number = 0

    private groups$: Subscription;
    groupTypes: Array<any>;
    groupPreferences: Array<any>;
    loading: boolean;

    dropDowns: {
        userGroups: Array<string>,
        preferences: Array<string>
    }

    userGroupForm: FormGroup;
    preferenceForm: FormGroup;

    constructor(
            private timeS: TimeSheetService,
            private sharedS: SharedService,
            private listS: ListService,
            private router: Router,
            private globalS: GlobalService,
            private formBuilder: FormBuilder
        ){
            this.groups$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'groups')){
                    this.search();
                }
            });
    }

    ngOnInit(){
        this.user = this.sharedS.getPicked()
        this.resetGroup();
        this.reloadAll();
    }

    ngOnDestroy(){
        this.groups$.unsubscribe();
    }

    resetGroup(){
        this.userGroupForm = this.formBuilder.group({
            group: new FormControl('', [Validators.required]),
            notes: new FormControl(''),
            personID: new FormControl(''),
            recordNumber: new FormControl(0),
            alert: new FormControl(false),
         })

         this.preferenceForm = this.formBuilder.group({
            preference: new FormControl('', [Validators.required]),
            notes: new FormControl(''),
            personID: new FormControl(''),
            recordNumber: new FormControl(0)
         })
    }

    reloadAll(){
        this.search()
        this.listDropDowns()
    }

    search(){
        this.loading = true;     
        forkJoin([
            this.timeS.getgrouptypes(this.user.uniqueID),
            this.timeS.getgrouppreferences(this.user.uniqueID)
        ]).subscribe(data => {
            this.loading = false;
            this.groupTypes = data[0];
            this.groupPreferences = data[1];
        });
    }

    listDropDowns(){
        forkJoin([
            this.listS.getusergroup(this.user.uniqueID),
            this.listS.getrecipientpreference(this.user.uniqueID)
        ]).subscribe(data => {
            this.loading = false;
            this.dropDowns = {
                userGroups: data[0],
                preferences: data[1]
            }
        });
    }

    preferenceProcess(){
        this.preferenceForm.controls['personID'].setValue(this.user.uniqueID)

        const preferences = this.preferenceForm.value;

        if(this.dbAction == 0){
            this.timeS.postrecipientpreference(preferences)
                        .subscribe(data => {
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Data Inserted')
                            }
                        })
        }

        if(this.dbAction == 1){
            this.timeS.updateusrecipientpreference(preferences)
                .subscribe(data => {
                    if(data){
                        this.reloadAll()
                        this.globalS.sToast('Success','Data Updated')
                    }
                })
        }
    }

    updatePreference(data: any){
        this.preferencesModal.open();
        this.preferenceForm.patchValue({
            preference: data.preference,
            notes: data.notes,
            recordNumber: data.recordNumber
        })
    }

    deletePreference(data: any){
        this.timeS.deleterecipientpreference(data.recordNumber)
                    .subscribe(data =>{
                        if(data){
                            this.reloadAll()
                            this.globalS.sToast('Success','Data Deleted')
                        }
                    })
    }

    updateUserGroup(data: any){
        this.userGroupModal.open();
        this.userGroupForm.patchValue({
            group: data.group,
            notes: data.notes,            
            recordNumber: data.recordNumber,
            alert: data.mobileAlert,
        })
    }

    deleteUserGroup(data: any){
        this.timeS.deleteusergroup(data.recordNumber).subscribe(data =>{
            if(data){
                this.reloadAll()
                this.globalS.sToast('Success','Data Deleted')
            }
        })
    }

    userGroupProcess(){
        this.userGroupForm.controls['personID'].setValue(this.user.uniqueID)        
        const group = this.userGroupForm.value;
        if(this.dbAction == 0){
            this.timeS.postusergroup(group)
                .subscribe(data => {
                    if(data){
                        this.reloadAll()
                        this.globalS.sToast('Success','Data Inserted')
                    }
                })
        }

        if(this.dbAction == 1){
            console.log(group)
            this.timeS.updateusergroup(group)
                        .subscribe(data =>{
                            if(data){
                                this.reloadAll()
                                this.globalS.sToast('Success','Data Update')
                            }
                        })
        }
    }
}