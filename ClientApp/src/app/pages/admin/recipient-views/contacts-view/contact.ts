import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, AfterContentInit } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
    templateUrl: './contact.html',
    styles: [`
        ul{
            list-style:none;
        }
        
        div.divider-subs div{
            margin-top:2rem;
        }
        nz-divider{
            margin: 0;
        }
        p{
            margin: 0;
            cursor:pointer;
            padding:10px;
            height:50px;
            border: 1px solid #f3f3f3;
            font-size: 16px;
            font-family: 'Segoe UI';
            font-weight: 300;
        }
        .active-tab{
            background: #85B9D5;
            color: #fff;
            padding:10px;
            font-weight: 600;
        }
        `],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ContactsAdmin implements OnInit, OnDestroy, AfterContentInit {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    
    isDisabled: boolean = false;

    visible: boolean = false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
    ) {
        // this.cd.detectChanges();
        
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (this.globalS.isCurrentRoute(this.router, 'contact-admin')) {
                this.user = {...data};
                this.cd.markForCheck();
            }
        });
        
    }

    ngAfterContentInit(){
        this.visible = true;
    }

    ngOnInit(): void {
        this.user = Object.assign({}, this.sharedS.getPicked());        
        this.cd.markForCheck();
    }

    ngOnDestroy(): void {
        this.visible = false;
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}