import { Component, OnInit,OnDestroy,ViewChild } from '@angular/core'
import { FormBuilder, FormGroup } from '@angular/forms'
import { Subscription } from 'rxjs';

import { Router } from '@angular/router'
import { SharedService } from '@shared/sharedService/shared-service'
import { TimeSheetService, ListService } from '@services/index'
import { GlobalService } from '@services/global.service'

import * as moment from 'moment';
import { BsModalComponent } from 'ng2-bs3-modal';

@Component({
    selector: '',
    templateUrl:'./consents.html',
    styles:[`
    bs-modal >>> .modal-body   {
        overflow-y: hidden !important;
        min-height:13rem;
    }
    `]
})

export class IntakeConsents implements OnInit,OnDestroy {
    @ViewChild('addConsentModal', { static: false }) addConsentModal: BsModalComponent;

    dbAction: number;
    private consents$: Subscription;
    consents: Array<any>;
    loading: boolean;

    consentGroup: FormGroup;
    lists: Array<string>;

    user: any;

    constructor(
            private timeS: TimeSheetService,
            private listS: ListService,
            private sharedS: SharedService,
            private router: Router,
            private globalS: GlobalService,
            private formBuilder: FormBuilder
        ){
            this.consents$ = this.sharedS.changeEmitted$.subscribe(data => {
                if(this.globalS.isCurrentRoute(this.router, 'consents')){
                    this.search();
                    this.listDropDowns();
                }
            });
    }

    ngOnInit(){
        this.resetAll();
    }

    resetAll(){
        this.search();
        this.resetGroup();
        this.listDropDowns();
    }

    ngOnDestroy(){
        this.consents$.unsubscribe();
    }

    resetGroup(){
        this.consentGroup = this.formBuilder.group({
           recordNumber: '',
           personID: '',
           consent: '',
           notes: '',
           expiryDate: null
        })
    }

    search(){
        this.loading = true;
        this.user = this.sharedS.getPicked();      

        this.timeS.getconsents(this.user.uniqueID).subscribe(consents => {
            this.loading = false;
            this.consents = consents;
        })        
    }


    listDropDowns(){
        this.listS.getconsents(this.user.uniqueID)
            .subscribe(data => this.lists = data)
    }

    consentProcess(){
        const group = this.consentGroup.value
        console.log(group)
        let _consentGroup: Dto.Consents = {
            recordNumber: group.recordNumber,
            personID: this.user.uniqueID,
            notes: group.notes,
            date1: group.expiryDate ? moment(group.expiryDate).format() : null,
            name: group.consent
        }

        if(this.dbAction == 0){            
            this.timeS.postconsents(_consentGroup).subscribe(data => {
                if(data){
                    this.resetAll();
                    this.globalS.sToast('Success','Consent Inserted')
                }
            })
        }

        if(this.dbAction == 1){
            this.timeS.updateconsents(_consentGroup).subscribe(data => {
                if(data){
                    this.resetAll();
                    this.globalS.sToast('Success','Consent Updated')
                }
            })
        }
    }

    updateconsentmodal(data: any){

        this.addConsentModal.open();
        this.consentGroup.patchValue({
            recordNumber: data.recordNumber,
            personID: data.personID,
            consent: data.consent,
            notes: data.notes,
            expiryDate: data.expiryDate
        });
    }

    deleteconsent(data: any){
        this.timeS.deleteconsents(data.recordNumber)
                    .subscribe(data => {
                        if(data){
                            this.resetAll();
                            this.globalS.sToast('Success','Consent Deleted')
                        }
                    })
    }
}