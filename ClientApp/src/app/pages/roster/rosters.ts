import { Component, Input, ViewChild, ChangeDetectorRef,ElementRef,ViewEncapsulation,    
    AfterViewInit,   
    OnDestroy,
    OnInit, Renderer2,
    HostListener,
    ViewContainerRef,
    ComponentRef,
    ComponentFactoryResolver,
    TemplateRef,
} from '@angular/core'
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { getLocaleDateFormat, getLocaleFirstDayOfWeek, Time,DatePipe } from '@angular/common';

import { forkJoin, Subscription, Observable, Subject, EMPTY, of,fromEvent, observable } from 'rxjs';
import {MatMenuModule} from '@angular/material/menu';

import {debounceTime, distinctUntilChanged, takeUntil,mergeMap, concatMap, switchMap,buffer,map, bufferTime, filter, tap, retryWhen} from 'rxjs/operators';
import { TimeSheetService, GlobalService, view, ClientService, StaffService,ShareService, ListService, UploadService, months, days, gender, types, titles, caldStatuses, roles } from '@services/index';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzRadioModule  } from 'ng-zorro-antd/radio';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import * as _ from 'lodash';
import {ShiftDetail} from './shiftdetail'
import {StaffExternal} from './StaffExternal'
import {RecipientExternal} from './RecipientExternal'


import { NzFormatEmitEvent, NzTreeNodeOptions } from 'ng-zorro-antd/core';
import { NzStepsModule, NzStepComponent } from 'ng-zorro-antd/steps';


//import parse from 'date-fns/parse';
import { PROCESS } from '../../modules/modules';
import { format, formatDistance, formatRelative, nextDay, subDays } from 'date-fns'

import parseISO from 'date-fns/parseISO'
import addMinutes from 'date-fns/addMinutes'
import isSameDay from 'date-fns/isSameDay'
import { isValid } from 'date-fns';

import startOfMonth from 'date-fns/startOfMonth';
import endOfMonth from 'date-fns/endOfMonth';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import * as moment from 'moment';
import * as $ from 'jquery';
import { SpreadSheetsModule } from '@grapecity/spread-sheets-angular';
import * as GC from "@grapecity/spread-sheets";

import { NZ_ICONS, NZ_ICON_DEFAULT_TWOTONE_COLOR } from 'ng-zorro-antd';

import { NzTableModule  } from 'ng-zorro-antd/table';
import { Router,ActivatedRoute } from '@angular/router';
import { BarTooltipRendererParams, SetLeftFeature } from 'ag-grid-community';
import { style } from '@angular/animations';
import { stringify } from '@angular/compiler/src/util';
import { environment } from 'src/environments/environment';
import { addListener } from 'process';
import { XeroService } from '@services/XeroService';

import { NzModalRef } from 'ng-zorro-antd/modal';
import { CanDeactivate } from '@angular/router';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
 
interface AddTimesheetModalInterface {
    index: number,
    name: string
}
interface UserView{
    staffRecordView: string,
    staff: number
}

//const license = "20.213.152.181,E215293891689378#B0ZlhcHlXcrknQxQjev9Ga7hFM8d4aYRVeFp7Rz94atlkUTN6T9tEUBpnZ8M5KVNkcqNEVIdXc4JVVrU6R9UWbOl4ckhDdXF5SqBFesJEWzkVVYhEWjVWRQF4VCZXO0Jzah9EUKZWSKhTNEBVbIpGSxxGMnNzQJdEMlJzMm9GOzNWd9VXWjxmNyk6R5p7RrgmdxdXdilmZuN4L5EkWaF7c8ZnMvpmVjxmWyZ5KhRzc4pHTHRnTWJ7cRZ6UVFEeygzQOF6dCh6Z4FjQHhHcXFnNIxUOht4cK9Eckp5VqdHSNdETxInMz44Uw2CTxhnUrZjSLVzNqVUbxNDcTF5U5dzSv2ichtmI0IyUiwiICJUM6gTRBRjI0ICSiwSMxADMxcjNzkTM0IicfJye35XX3JSRFlkViojIDJCLiQTMuYHIgMlSgQWYlJHcTJiOi8kI1tlOiQmcQJCLiYTN5AzMwASOxITMyIDMyIiOiQncDJCLiQTMxEzMyAjMiojIwhXRiwiIxgTMuITNx8yMxIjLwIjI0IyctRkIsIycu3Wa4VHbvNFIlRXYy3Gcy36QgMXYtFGZBJiOiEmTDJCLlVnc4pjIsZXRiwiI8czM9gjNxkDOzkjM5EjMiojIklkIs4XXbpjInxmZiwSZzxWYmpjIyNHZisnOiwmbBJye0ICRiwiI34zdwgHTvREcMt4QrdnS5REe8ZmUyYzL4t6K6pXdah5apJEdv5mMoRFM5AjN6NmcthGVtBDWwE6UzgWUvhDSRNnV0BXU854a8lWaZl7LnNHWLNyS"
const license = "20.213.152.181|adamas.traccs.cloud,E258614243233711#B0zUv4bEJXQWhjd8w6LZh5RXV7dEN7NFdlZtFlUGlWeZRTaKllUhRjcSVFNYRDeWlXMWVnR6xUdHVTepNXS584RBNHcmNWa5UWVy4mRMVXN7FGZt3ScrdzSvdnQL5UTwRmeXtiaENFZwRTbKR7aih4TSNFTHREaWx4UDF7NvZDNN3CbZZWQhF6Qrp4YP9GTRFXTUJUUWdTRaJ4QpZ4YT9GdrcmUwgmNGlzSjljU7M7KwJ6YjRTcx8kQW36NQdHePhWeuxkVrYUemJ7UyhXQyIWVpZWTRpFe9U7dnRUUwEUTNd6ZyETV4M6KjhneM34VlpVeyh6ZsV4ZIFnNJhWWihXNuJ4NNdXZUNUc5N7YsNmWiojITJCLicDM5YjNDZkI0ICSiwSMwUDMxADNzkTM0IicfJye&Qf35VfiUURJZlI0IyQiwiI4EjL6BCITpEIkFWZyB7UiojIOJyebpjIkJHUiwiI9UTOyMDMgcTM8AzMyAjMiojI4J7QiwiIwITOwQjMwIjI0ICc8VkIsICZ53Gbj9ycjNWYyRnLzFWbhRWYsEDOx8iM5EjLzEjMuAjMiojIz5GRiwiIz96bpRXds36UgUGdhJ7bwJ7bDBych5WYkFkI0ISYONkIsUWdyRnOiwmdFJCLiETM7MzMyMDNyQTM6gTNyIiOiQWSiwSfdtlOicGbmJCLlNHbhZmOiI7ckJye0ICbuFkI1pjIEJCLi4TP7RDMItWRDZDSqFUS9kTSNp5SMZUexoEUOtURt3WYwQzNxQWMxQmTmlVROVFSBBVaYB5RIFkS7YlTwAlRS96RIVGNVBHO2z1"

function IconCellType(img) {
    this.typeName = "IconCellType";
    this.img = img;
   
}

IconCellType.prototype = new GC.Spread.Sheets.CellTypes.Base();
// EllipsisTextCellType.prototype
IconCellType.prototype.paint = function (ctx, value, x, y, w, h, style, context) {            
    if (!ctx) {
        console.log('No icon loaded')
        return;
        
    }
    ctx.save();

    // draw inside the cell's boundary
    ctx.rect(x, y, w, h);
    
    ctx.clip();

    // draw text
    GC.Spread.Sheets.CellTypes.Base.prototype.paint.apply(this, [ctx, value, x+22, y, w-22, h, style, context]);
    ctx.beginPath();
    // let img = document.getElementById('icon-lock');
  
    ctx.fillStyle = "#ffffcc" // "#85B9D5";   
   
  
    ctx.fillRect(x, y, 26, 26);       
    ctx.drawImage(this.img, x+3 , y+2, 22, 22);
   // ctx.arc(x,y,15,0,15,true)
    ctx.fill();
   
    ctx.stroke();
    ctx.restore();
};

function IconCellType2(img) {
    this.typeName = "IconCellType";
    this.img = img;
    
}

IconCellType2.prototype = new GC.Spread.Sheets.CellTypes.Base();
// EllipsisTextCellType.prototype
IconCellType2.prototype.paint = function (ctx, value, x, y, w, h, style, context) {            
    if (!ctx) {
        console.log('No icon found')
        return;
    }
    ctx.save();

    // draw inside the cell's boundary
    ctx.rect(x, y, w, h);
    ctx.clip();

    // draw text
    GC.Spread.Sheets.CellTypes.Base.prototype.paint.apply(this, [ctx, value, x, y, w, h, style, context]);
    ctx.beginPath();
       
    ctx.fill();
    ctx.stroke();
    ctx.fillStyle = "#ffffcc";
   // ctx.arc(x,y,5,0,90,true)
    ctx.restore();
};


  
@Component({
    selector: 'roster-component',
    styleUrls: [ './rosters.css'],
    templateUrl: './rosters.html'
})

export class RostersAdmin implements OnInit, AfterViewInit, OnDestroy, CanDeactivate<any>  {

    
    // Add an event listener for mouseup to end the drag
    @HostListener('document:mouseup', ['$event']) endDrag(event: MouseEvent) {
        if (this.draggingCell) {
            console.log('Drag ended');
            
            
            // Remove the 'dragging' class from the cell
            const cell = document.querySelector('.dragging');
            cell?.classList.remove('dragging');

            // Reset the draggingCell
            this.draggingCell = null;
        }
    }

    spreadBackColor = "white";  
    sheetName = "Staff Rosters";  

    hostStyle = {
        width: '88vw',
        height: '85vh',        
    };


    hostStyle2 = {  
        width: 'calc(100% - 50px)',
        height: '500px',
        overflow: 'hidden',
        float: 'left'
    };  

   rows = 15;
  columns =15;
  gridData: any[][] = [];

    divCSS=`
        position:relative;
        padding: 2px;
        margin-top: 0px;       
        font-size: 12px;
        overflow: hidden;
        text-align: initial;
        border: 1px solid #f9cf9b;
        border-width: 1px; 
        border-radius: 5px;         
        background: rgb(200, 226, 168);
        color:#fff(158, 17, 17);
        background: linear-gradient(to bottom, #ffffcc 0%, #ffffff 100%);
       
    `;
//85B9D5
@Input() info = {StaffCode:'', ViewType:'',IsMaster:false, date:''}; 
@Input() reloadRoster = new Subject<any>();
@Input() fixSize:boolean;

@ViewChild('customFooter', { static: true })  customFooter!: TemplateRef<{ }>;
@ViewChild('customTitle', { static: true })  customTitle!: TemplateRef<{ }>;
@ViewChild('customContent', { static: true })  customContent!: TemplateRef<{ }>;
cancelTrigger: boolean = false;
willSaveChanges: boolean = false;

rosterFormGroup: FormGroup;

reloadSearchList = new Subject<any>();
reloadMenu = new Subject<any>();
menuItems = new Subject<number>();

//@ViewChild(ShiftDetail) detail:ShiftDetail;
@ViewChild('dynamicContainer', {read: ViewContainerRef})
dynamicContainer: ViewContainerRef;
private _dynamicInstance: ComponentRef<ShiftDetail>;

@ViewChild('dynamicContainerRecipientexternal', {read: ViewContainerRef})
dynamicContainerRecipientexternal: ViewContainerRef;
private _dynamicContainerRecipientexternal: ComponentRef<RecipientExternal>;

@ViewChild('dynamicContainerStaffExternal', {read: ViewContainerRef})
dynamicContainerStaffExternal: ViewContainerRef;
private _dynamicContainerStaffExternal: ComponentRef<StaffExternal>;


unlistener: () => void;

selectedrow: string ="class.selected"   
timesheets: Array<any> = [];
timesheetsGroup: Array<any> = [];   
selectedProgram: any = null;
defaultProgram: any = null;
defaultActivity: any = null;
selectedActivity: any = null;
defaultCategory: any = null;
Timesheet_label:any="Add Timesheet";
personList:Array<any>=[];

payTotal:any;
HighlightRow!: number;
HighlightRow2!: number;
HighlightRow3!: number;
HighlightRow4!: number;
HighlightRow5!: number;
HighlightRow6!: number;
recurrentStartTime:string;
recurrentEndTime:string;
recipientexternal:boolean=false;
staffexternal:boolean=false;
testComp:boolean=false;

recurrentStartDate: Date | null = null;
recurrentEndDate: Date | null = null;
create_Recurrent_Rosters:boolean=false;

weekDay:any;
Frequency:string="Weekly";
Pattern:string;
haccCode:string;

defaultCode:string;
masterCycle:string="CYCLE 1";
masterCycleNo:number=1;
Days_View:number=14;
dval:number=14;
publicHolidayRegionData:any;
lstPublicHolidays: Array<any> =[];
Error_Messages : Array<any> =[];
Errors_Messages_summary:any;
data:any=[];  
ActiveCellText:any;
startRoster:any;
endRoster:any;
searchAvaibleModal:boolean=false;

  rosters: Array<any> = [];
  current_roster:any;
  selected_data:any;
  time_map = new Map();
  Already_loaded:boolean=false;
  prev_cell:any = {row:0,col:0,duration:0, type:0, recordNo:0, service:""} 
  cell_value:any = {row:0,col:0,duration:0, type:0,recordNo:0, service:""} 
  copy_value:any = {row:0,col:0,duration:0, type:0,recordNo:0, service:""} 
  show_More:boolean=false;
  show_views:boolean=false;
  notes:string="";
  clientCodes:string="";
  bodyText:string;
  recipientDetailsModal:boolean=false;
  operation:string="";
  columnWidth = 100;
  view:number=0;
  i:number=0;
  eventLog: string;
  token:any;
  userSettings:any
  addBookingModel:boolean=false;
  type_to_add:number;
  select_StaffModal:boolean=false;
  select_RecipientModal:boolean=false;
  booking_case:number=0;
  showTransportModal:boolean=false;
  showRecurentModal:boolean=false;
  recurrenceView:boolean=false;
  Transport_Form_Title:string=""
  include_fee:boolean=false;
  include_item:boolean=false;
  spreadsheet:any;
  customizeHref: string;
  time_slot:number=96;
  ShowCentral_Location:boolean=false;
  IsClientCancellation:boolean=false;
  showtMultiRecipientModal:boolean=false;
  IsGroupShift:boolean=false;
  GroupShiftCategory:string="";
  listOfSelection: Array <any> = [];
  NRecordNo:string;
  recordsAdded:Array<any>=[];
  timeList:Array<any>=[];
  addressList:Array<any>=[]
  mobilityList:Array<any>=[]
    
  show_alert:boolean=false; 
  txtAlertSubject:string="Shift Alerts";
  txtAlertMessage:string="Shift Alert";
  errorValue:number=0;
  b_BrokenShift:boolean;

  isVisible: boolean = false;
    hahays = new Subject<any>();
    optionsModal:boolean=false;
    showDate:boolean=false;
    monthFormat:string="MMM yyyy"
    CalendarMode:any='month'
    HighlightColum_index:number=-1;

    enable_buttons :boolean=false;
    isPaused:boolean;    
    private picked$: Subscription;   
    isConfirmLoading = false;
    changeModalView = new Subject<number>();
    changeViewRecipientDetails = new Subject<number>();
    changeViewRosterDetails = new Subject<number>();
    AddViewRosterDetails    = new Subject<number>();
    listAdminExplanation: Array<any> = [];
    showDetermineAdminExplanation:boolean;
     s_LookupText:string;
    _highlighted: Array<any> = [];
    user:any;
    selectedOption:any;
    today = new Date();
    rosterGroup: string;
    rosterForm: FormGroup;
    bookingForm: FormGroup;
    TransportForm:FormGroup
    RecurrentServiceForm:FormGroup;
    AlertForm:FormGroup;
    showDiv:boolean=true;
    pasting:boolean=true;
    breachRoster:boolean=false;
    Error_Msg:string;
    continueError:boolean=false;
    rDate:any;
    selected_Cell:any;
    sel:any;
    viewType: any ;
    s_DayMask:string
    // start_date:string="";
    // end_date:string=""
    ForceAll:Boolean=true;
    subGroup:String="";
    RosterDate:String="";
    StartTime:String="";
    EndTime:String=""
    Duration:String="5";
    EnforceActivityLimits:boolean=true;

    openSearchStaffModal:boolean;
    booking:{
        recipientCode: string,
        userName:'sysmgr',
        date:'2022/01/01',
        startTime:string,
        endTime:string,
        endLimit:'20:00'
      };
    @Input() bookingData = new Subject<any>();
    bookingDataRecipient = new Subject<any>();
   
    showContextMenu:boolean;
    master:boolean=false;
    Master_Roster_label="Current Roster";
    tval:number;
    screenHeight:number;
    screenWidth:number;
    sample: any;
    searchStaffModal:boolean=false;
    ViewStaffDetail:boolean=false;
    ViewServiceDetail:boolean=false;
    ViewAdditionalModal:boolean=false;
    isFirstLoad:boolean=false;
    userview: UserView;
    selectedCarer:any;
    UnAllocateStaffModal:boolean=false;
    ClearMultiShiftModal:boolean=false;
    SetMultiShiftModal:boolean=false;
    deleteRosterModal:boolean=false;
    isTravel:boolean=false;
    add_UnAllocated:boolean=false;

    showGroupShiftModal:boolean=false;
    showGroupShiftRecipient:boolean=false;
    setOfCheckedId = new Set<any>();
 
    dateFormat: string = 'dd/MM/yyyy'
    selectAll: boolean = false;
    overlapVisible: boolean = false;
    addTimesheetVisible: boolean = false;
    multipleRecipientShow: boolean = false;
    isTravelTimeChargeable: boolean = false;
    isSleepOver: boolean = false;
    payUnits: any;
    addRecurrent:boolean=false
    promise: any;
    dblclick:boolean
    

    parserPercent = (value: string) => value.replace(' %', '');
    parserDollar = (value: string) => value.replace('$ ', '');
    formatterDollar = (value: number) => `${value > -1 || !value ? `$ ${value}` : ''}`;
    formatterPercent = (value: number) => `${value > -1 || !value ? `% ${value}` : ''}`;

    AddTime(interval:number=5){
        for (let h=0; h<24; h++)
            for (let t=0; t<60; t=t+interval)
             this.timeList.push(this.numStr(h) + ":"+ this.numStr(t))
    }
    changeHeight() {
        // this.hostStyle.height = this.hostStyle.height === "50%" ? "100%" : "50%";
        setTimeout(() => {
        this.spreadsheet.refresh();
        });
        }
    
    DayOfWeek(n:number): String{

    let day:String="";
    switch(n){
    case 1 : day="Mon"; break;
    case 2 : day="Tue" ; break;
    case 3 : day="Wed" ; break;
    case 4 : day="Thu" ; break;
    case 5 : day="Fri" ; break;
    case 6 : day="Sat" ; break;
    case 0 : day="Sun" ; break;
    }
    return day;
  
  }
  
  FullDayOfWeek(n:number): String{

    let day:String="";
    switch(n){
    case 1 : day="Monday"; break;
    case 2 : day="Tuesday" ; break;
    case 3 : day="Wednesday" ; break;
    case 4 : day="Thursday" ; break;
    case 5 : day="Friday" ; break;
    case 6 : day="Saturday" ; break;
    case 0 : day="Sunday" ; break;
    }
    return day;
  
  }
 
 
  listChange(event: any) {

    if (event == null) {
        this.user = null;
        this.isFirstLoad = false;
        this.sharedS.emitChange(this.user);
        return;
    }

   
      this.selectedCarer=event.accountNo;
      this.bookingForm.patchValue({
            staffCode:event.accountNo
      });


    this.user = {
        code: event.accountNo,
        id: event.uniqueID,
        view: event.view,
        agencyDefinedGroup: event.agencyDefinedGroup,
        sysmgr: event.sysmgr
    }

    
    if (this.viewType=='Recipient'){
        this.getPublicHolidyas(this.selectedCarer);

    }
    this.sharedS.emitChange(this.user);
    this.cd.detectChanges();
}

normalRoutePass(): void{
    const { user } = this.globalS.decode();

 

    this.listS.getstaffrecordview(user).subscribe(data => {
        this.userview = data;
        this.cd.detectChanges();
    })

   
    this.isFirstLoad = false;   

    

    this.sharedS.emitRouteChangeSource$.subscribe(data => {
        console.log(data);
    });
}
 roundToTwo(num) {    
    return Math.round((num + Number.EPSILON) * 100) / 100;
}
cancel_GroupShift(){
    this.showGroupShiftModal=false; 
    this.showGroupShiftRecipient=false
    this.IsGroupShift=false;
}
get_group_Shift_Setting()
{
   
    this.GET_GROUP_RECIPIENTS().subscribe(d=>{
        this.RecipientList=d;//.map ( x => x.accountNo);
        this.showGroupShiftRecipient=true;
        this.setOfCheckedId.clear();

      
    })
    this.GET_ADDRESS().subscribe(d=>{
        this.addressList=d.map ( x => x.address);             
      
    })

    this.GET_MOBILITY().subscribe(d=>{
        this.mobilityList=d.map ( x => x.description);             
      
    })

    
}

Save_Transport(){
    
    const tdata =  this.TransportForm.value;
    let inputs ={
        P1:this.current_roster.recordNo,
        P2:tdata.pickupFrom,
        P3:tdata.zipCodeFrom,
        P4:tdata.pickupTo,
        P5:tdata.zipCodeTo,     
        P6:tdata.mobility,
        P7:tdata.returnVehicle,
        P8:tdata.appmtTime,
        P9:tdata.jobPriority,
        P10:tdata.transportNote
    };
    this.timeS.addtransport(inputs).subscribe(data => {
        
            this.globalS.sToast('Success', 'Transport details have been recorded');

            this.showTransportModal=false;
    });
}
close(){
    this.showRecurentModal=false;
    //this.recurrenceView=false;
    
}

showRecurrentView(){
    this.showRecurentModal=true;
    this.recurrenceView=true;
    if (this.master){
        this.RecurrentServiceForm.patchValue({
            startDate :'1900/01/01',
            endDate :'1900/01/31'
        })
       
    }else{
        this.RecurrentServiceForm.patchValue({
            startDate : this.today,
            endDate :this.today
        })
    }
    
}
cancelRecurrent(){
    this.recurrenceView=false;
    this.create_Recurrent_Rosters=false;
}
set_RecurentView(){
    this.recurrenceView=true;
    this.showRecurentModal=false;
    const reuc = this.RecurrentServiceForm.value;
    this.recurrentStartDate =reuc.startDate
    this.recurrentEndDate =reuc.endDate
    this.date = moment(reuc.startDate).format('YYYY/MM/DD');
    
}
createRecurrent_rosters(){
    this.create_Recurrent_Rosters=true;
    this.AddRoster_Entry()
}

setPattern(d:string){
    this.Pattern=d;
}
setFrequency(d:string){
    this.Frequency=d;
}

Cancel_ProceedBreachRoster(){
    this.breachRoster=false;
    if (this.operation=='copy' ||this.operation=='cut'){
        //this.load_rosters();
        this.pasting=false;
    }
    this.isPaused=false;
    this.processing=false;
}
ProceedBreachRoster(){
    this.breachRoster=false;
    this.processing=false;
    if (this.token.role!= "ADMIN USER")
    {
        this.globalS.eToast("Permission Denied","You don't have permission to add conflicting rosters");
        return; 
    }
    
    let sheet = this.spreadsheet.getActiveSheet();
    if (this.operation=='copy' ||this.operation=='cut'){
        // if (this.operation=="cut"){
        //     this.ProcessRoster("Cut",this.current_roster.recordNo,this.rDate);
        //     this.remove_Cells(sheet,this.copy_value.row,this.copy_value.col,this.copy_value.duration)
        // }else
        //     this.ProcessRoster("Copy",this.current_roster.recordNo,this.rDate);  
        this.pasting=false;
        this.Pasting_Records(this.selected_Cell,this.sel)
    }else{
        this.AddRoster_Entry();
    }

    this.isPaused=false;

}
Check_BreachedRosterRules_Paste(RecNo:number, action:string,row : number, col : number):any{

    //It is still not being used
    this.current_roster = this.find_roster(RecNo)
   // let clientCode= this.current_roster.clientCode;
    let sheet = this.spreadsheet.getActiveSheet();
    //     let range = sheet.getSelections();
    //     let col= range[0].col;
        let dt= sheet.getTag(0,col,GC.Spread.Sheets.SheetArea.colHeader);                       
        this.rDate = dt.getFullYear() + "/" + this.numStr(dt.getMonth()+1) + "/" + this.numStr(dt.getDate());
  
        let f_row = row;             
        let startTime =   sheet.getCell(f_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();
     

    let inputs_breach={
        sMode : 'Add', 
        sStaffCode: this.current_roster.staffCode, 
        sClientCode: this.current_roster.recipientCode, 
        sProgram: this.current_roster.program, 
        sDate : this.rDate, 
        sStartTime :startTime, 
        sDuration : this.current_roster.duration, 
        sActivity : this.current_roster.activity,
        sRORecordno : this.current_roster.recordNo, 
         PasteAction : action=="cut" ? "Cut": "Copy"
    };

    return inputs_breach; 
   // return   this.timeS.Check_BreachedRosterRules(inputs_breach);

    
}
Check_BreachedRosterRules(){

    const tsheet= this.bookingForm.value;
    let clientCode ='';
    let carerCode = '';
    if (this.viewType=="Staff"){
        if (this.IsGroupShift)
            clientCode="!MULTIPLE";
        else
            clientCode = tsheet.recipientCode;

        carerCode= this.selected.data;
    }
    
    if (this.viewType=="Recipient"){
        carerCode = this.selectedCarer
        clientCode=this.recipient.data
    }
    var durationObject = (this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime));        

    let inputs_breach={
        sMode : 'Add', 
        sStaffCode: carerCode, 
        sClientCode: clientCode, 
        sProgram: tsheet.program, 
        sDate : format(this.date,'yyyy/MM/dd'), 
        sStartTime :format(this.defaultStartTime,'HH:mm'), 
        sDuration : durationObject.duration, 
        sActivity : tsheet.serviceActivity.activity,
        // sRORecordno : '-', 
        // sState : '-', 
        // bEnforceActivityLimits :0, 
        // bUseAwards:0, 
        // bDisallowOT :0, 
        // bDisallowNoBreaks :0, 
        // bDisallowConflicts :0, 
        // bForceNote :0, 
        // sOldDuration : '-', 
        // sExcludeRecords : '-', 
        // bSuppressErrorMessages  :0, 
        // sStatusMsg : '-',
        // PasteAction :'-'
    };

    this.timeS.Check_BreachedRosterRules(inputs_breach).subscribe(data=>{
        let res=data
        if (res.errorValue>0){
           // this.globalS.eToast('Error', res.errorValue +", "+ res.msg);
           //this.addBookingModel=false;
            this.continueError=res.continueError;
            this.Error_Msg=res.errorValue +", "+ res.msg ;
            this.breachRoster=true;
            this.addBookingModel=false;
            this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + this.Error_Msg);           
                this.AddRoster_Entry();           

            return; 
        }else{

           this.CheckBrokenShift(this.date,this.defaultStartTime, this.defaultEndTime)
                
        }

    });
}
doneBooking(){
    //For recurrent view
    this.rosterFormGroup.markAsDirty();
    this.addBookingModel=false;
    if (this.master)
        this.AddRoster_Entry();
    else
         this.Check_BreachedRosterRules();
    
    
}

AddRoster_Entry(){

   
    this.addBookingModel=false;
    this.add_UnAllocated=false;
    this.select_StaffModal=false;
    this.ShowCentral_Location=false;
  

    this.current = 0;
    this.booking_case=0;
 
    if (this.viewType=="Staff"  && this.IsGroupShift && this.showGroupShiftRecipient==false){
        this.addBookingModel=false;
        this.get_group_Shift_Setting()    
        return;        
    }    
    this.showGroupShiftRecipient=false;

    if (this.type_to_add<=0){
        if (this.rosterGroup=="")
            this.rosterGroup= this.defaultActivity.rosterGroup;
        this.serviceType=this.DETERMINE_SERVICE_TYPE_NUMBER(this.rosterGroup)
    }else
        this.serviceType=this.type_to_add;
    //const { recipientCode, Program, serviceActivity, isMultipleRecipient } = this.bookingForm.value;

    //this.fixStartTimeDefault();
      

        let date=this.date;
        let time = {startTime:this.defaultStartTime, endTime:this.defaultEndTime, duration:0};
        const tsheet =  this.bookingForm.value;
      
        if (this.type_to_add!=13  && (tsheet.serviceActivity==null || tsheet.serviceActivity=="")){
           
            this.globalS.eToast('Error', 'No Service type is selected');
            return;            
        }
        let clientCode ='';
        let carerCode = '';
        if (this.viewType=="Staff"){
            if (this.IsGroupShift)
                clientCode="!MULTIPLE";
            else
                clientCode = tsheet.recipientCode;

            carerCode= this.selected;
        }
        
        if (this.viewType=="Recipient"){
            carerCode = this.selectedCarer
            clientCode=this.recipient.data
        }
       
                
        if ( this.serviceType==1)
            carerCode = "BOOKED"

        var durationObject = (this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime));        
        this.date = parseISO(this.datepipe.transform(this.date, 'yyyy-MM-dd'));
        tsheet.date=this.date;
        if (this.create_Recurrent_Rosters){
            tsheet.date= this.recurrentStartDate;
            let stime=  parseISO(new Date(this.recurrentStartTime).toISOString());
            let etime=  parseISO(new Date(this.recurrentEndTime).toISOString());
            let dd:any;
            if (this.master){   
                tsheet.date= new Date(this.recurrentStartDate)             
                this.defaultStartTime = parseISO(new Date(this.recurrentStartDate + " " + format(stime,'HH:mm')).toISOString());
                this.defaultEndTime = parseISO(new Date(this.recurrentEndDate+ " " + format(etime,'HH:mm') ).toISOString());
            }else {
                dd=this.recurrentStartDate.getFullYear() + '/' +  this.numStr( this.recurrentStartDate.getMonth()+1) +'/' + this.numStr(this.recurrentStartDate.getDate());
            
                this.defaultStartTime = parseISO(new Date(dd + " " + format(stime,'HH:mm')).toISOString());
                this.defaultEndTime = parseISO(new Date(dd+ " " + format(etime,'HH:mm') ).toISOString());
            }    
            time = {startTime:stime, endTime:etime, duration:0};
            durationObject = (this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime));        
           
            //return;
        }

        if (this.defaultActivity==null){
            this.defaultActivity={
                fixedTime: 0,
                autoApprove:0
            }
        }
        tsheet.recordNo=0;
        let inputs = {
           
            billQty: (tsheet.bill.quantity==null || tsheet.bill.quantity==0) ? (tsheet.bill.pay_Unit=='HOUR'? this.roundToTwo(durationObject.duration/12) : 1):0 || 0,
            billTo: clientCode,
            billUnit: tsheet.bill.pay_Unit || 'HOUR',
            blockNo: durationObject.blockNo,
            carerCode: this.selected.option == 0 ? this.selected.data : carerCode,
            clientCode: this.selected.option == 0 ? clientCode : this.selected.data,
            costQty: (tsheet.pay.quantity==null || tsheet.pay.quantity==0)? (tsheet.pay.pay_Unit=='HOUR'? this.roundToTwo(durationObject.duration/12) : 1 ):0 || 0,
            costUnit: tsheet.pay.pay_Unit || 'HOUR',
            date: format(tsheet.date,'yyyy/MM/dd'),
            dayno: parseInt(format(tsheet.date, 'd')),
            duration: (this.defaultActivity.fixedTime>0 && this.defaultActivity.fixedTime!=null)  ? (this.defaultActivity.fixedTime/5) :  durationObject.duration,
            groupActivity: false,
            haccType: tsheet.haccType || "",
            monthNo: parseInt(format(tsheet.date, 'M')),
            program: tsheet.program,
            serviceDescription:  tsheet.payType==null || tsheet.payType==''  ? tsheet.serviceActivity.service_Description :  tsheet.payType,
            serviceSetting: tsheet.serviceSetting || "",
            serviceType: tsheet.serviceActivity.activity || "",
            paytype: tsheet.payType,
            anal: tsheet.analysisCode=='' ||  tsheet.analysisCode==null ? tsheet.serviceActivity.anal :  tsheet.analysisCode,
            staffPosition:  tsheet.staffPosition || "",
            startTime: format(time.startTime,'HH:mm'),
            status: this.defaultActivity.autoApprove=='True' ? "2" : "1",
            taxPercent: tsheet.bill.tax || 0,
            transferred: 0,            
            type: this.serviceType,
            unitBillRate:( tsheet.bill.bill_Rate || 0),
            unitPayRate: tsheet.pay.pay_Rate || 0,
            yearNo: parseInt(format(tsheet.date, 'yyyy')),
            serviceTypePortal: tsheet.serviceType,
            recordNo: tsheet.recordNo,
            date_Timesheet: this.date_Timesheet,
            dischargeReasonType:this.haccCode,
            creator: this.token.user,
            datasetClient :  clientCode
            
        };

           
    if (!this.isBookingValid(inputs)){
        this.globalS.eToast('Error', 'There are some invalid entries');
        return;
        
    }
    if (this.create_Recurrent_Rosters){
        this.timeS.posttimesheet(inputs).subscribe(data => {
            this.NRecordNo=data;
            this.AddRecurrentRosters();
        });
                           
        this.addRecurrent=false;   
        return;
    }

        var sheet = this.spreadsheet.getActiveSheet();
        var sels = sheet.getSelections();
        var sel = sels[0];
        var row = sel.row;
        
        for (let i=0; i<sels[0].colCount; i++)
         {   

            this.timeS.posttimesheet(inputs).subscribe(data => {
                this.NRecordNo=data;
                this.recordsAdded.push(data);
                let history:any ={
                                    operator: this.token.user,
                                    actionDate:format(this.today,'yyyy/MM/dd HH:MM:ss'),
                                    auditDescription:'CREATE NEW ROSTER ENTRY',
                                    actionOn:'WEB-ROSTER',
                                    whoWhatCode:data,
                                    traccsUser:this.token.user
                                }
               if (this.create_Recurrent_Rosters==false)
                    this.timeS.postaudithistory(history).subscribe(d=>{});

                if  (this.create_Recurrent_Rosters==false &&  this.add_multi_roster==false) {
                   // this.globalS.sToast('Success', 'Roster has been added successfully');
                    //this.searchRoster(tsheet.date)
                    this.upORdown.next(true);
                 }
                  this.addTimesheetVisible = false;
             
     
               // this.picked(this.selected);
                this.IsGroupShift=false;
               console.log(data)

               if (this.add_multi_roster && this.create_Recurrent_Rosters==false){
               
                this.AddMultiShiftRosters();
                this.Transport_Form_Title=this.date + " " + this.defaultActivity
                this.TransportForm.reset();
                if (this.GroupShiftCategory=='TRANSPORT')
                  this.showTransportModal=true;
                this.searchRoster(tsheet.date)
            }
        
            this.processing=false;
            
            if (this.viewType=='Staff'){
                    
                this.txtAlertSubject= 'NEW SHIFT ADDED : ' ;
                this.txtAlertMessage= 'NEW SHIFT ADDED : \n' + format(tsheet.date,'dd/MM/yyyy') + ' : \n' + inputs.clientCode + '\n'  ;
                this.clientCodes=inputs.clientCode;

                if (this.Error_Messages.length<=0)
                    this.show_alert=true;
            }
               
           });
           tsheet.date=this.addDays(tsheet.date,1);
           inputs.date=format(tsheet.date,'yyyy/MM/dd')
           inputs.dayno= parseInt(format(tsheet.date, 'd'));
           inputs.monthNo= parseInt(format(tsheet.date, 'M'));
           inputs.yearNo= parseInt(format(tsheet.date, 'yyyy'));
        }
            this.addRecurrent=false;
            
           // this.resetBookingFormModal()
    
}
addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
}
Save_Roster_as_Master(){
    let r= this.rosters[0];

    if (r.blockNo==null)
        this.searchRoster (r.roster_Date)

    let rostDate;
    this.rosters.forEach(rost => {           

        rostDate= "1900/01/" + this.numStr(rost.dayNo);
       // var durationObject = (this.globalS.computeTimeDATE_FNS(rost.startTime, rost.endTime));    
        let inputs = {

           
            billQty: rost.bill.quantity,
            billTo: rost.billedTo,
            billUnit:rost.bill.pay_Unit,
            blockNo: rost.blockNo,
            carerCode: rost.carerCode,
            clientCode: rost.clientCode,
            costQty: rost.pay.quantity,
            costUnit: rost.pay.pay_Unit,
            date: rostDate,
            dayno: rost.dayNo,
            duration: rost.duration,
            groupActivity: rost.groupActivity,
            haccType: rost.haccType || "",
            monthNo: rost.monthNo,
            program: rost.program,
            serviceDescription:  rost.service_Description,
            serviceSetting: rost.serviceSetting || "",
            serviceType: rost.serviceType || "",
            paytype: rost.payType.paytype,
            anal: rost.anal,
            staffPosition: rost.staffPosition || "",
            startTime: rost.start_Time,
            status: rost.status,
            taxPercent: rost.bill.tax || 0,
            transferred: 0,            
            type: rost.type,
            unitBillRate:( rost.bill.bill_Rate || 0),
            unitPayRate: rost.pay.pay_Rate || 0,
            yearNo: 1900,
            serviceTypePortal: rost.serviceType,
            recordNo: rost.recordNo,
            date_Timesheet: rost.date_Timesheet ,
            dischargeReasonType:rost.dischargeReasonType || '',
            creator: this.token.user,
            datasetClient :  rost.clientCode
            
        };
  
        this.processing=true;
       this.timeS.posttimesheet(inputs).subscribe(data => {
           this.NRecordNo=data;
           let history:any ={
                               operator: this.token.user,
                               actionDate:format(this.today,'yyyy/MM/dd HH:MM:ss'),
                               auditDescription:'CREATE NEW MASTER ROSTER ENTRY',
                               actionOn:'WEB-ROSTER',
                               whoWhatCode:data,
                               traccsUser:this.token.user
                           }
        

    //       if (this.add_multi_roster && this.create_Recurrent_Rosters==false){
          
    //        this.AddMultiShiftRosters();
    //        this.Transport_Form_Title=this.date + " " + this.defaultActivity
    //        this.TransportForm.reset();
    //        if (this.GroupShiftCategory=='TRANSPORT')
    //          this.showTransportModal=true;
         
    //    }
   
       this.processing=false;
       
      });

    });
}
RecordStaffAdmin(){
    this.resetBookingFormModal();
    this.booking_case=6;
    this.isTravel=false;
    this.ShowCentral_Location=true;

    this.serviceType ="ADMINISTRATION";
    // this.rosterForm.patchValue({
    //     serviceType:"ADMINISTRATION"
    // });
  
        if (this.viewType=='Staff'){
            this.bookingForm.patchValue({
                recipientCode:"!INTERNAL",
                analysisCode:"!INTERNAL"
            });
        }else{
            this.bookingForm.patchValue({
                staffCode:"!INTERNAL"
            });
        }

        this.addBookingModel=true;
        this.addBooking(6);
    
}

SetMultiRecipient(val:boolean){
    this.showtMultiRecipientModal=false;
    this.GroupShiftCategory="";

    if (val){
        this.IsGroupShift=true;
        this.showGroupShiftModal=true;
        this.ShowCentral_Location=true;
        this.bookingForm.patchValue({
                analysisCode:"INTERNAL"
                })
    }else{
        this.IsGroupShift=false;
        this.openRecipientModal();
        // this.select_RecipientModal=true;    
        // if (this.FetchCode!=null && this.FetchCode!=''){
        //     this.bookingForm.patchValue({
        //         recipientCode:this.FetchCode
        //     })
        // }
    }
        
       
}

setCentral_Location(val:any){
   
    //this.ShowCentral_Location=true;
    this.serviceSetting="";

   this.showDone2;
    
}
recordCancellation(){
    this.resetBookingFormModal();
    this.booking_case=8;
    this.serviceType ="RECPTABSENCE";
    this.bookingForm.patchValue({
        staffCode:"!INTERNAL"
    });
    this.addBooking(4);
}
setUnavailablity(){
    this.resetBookingFormModal();
    this.booking_case=7;
    this.serviceType ="UNAVAILABLE";
    this.rosterGroup="UNAVAILABLE";
 
  
        if (this.viewType=='Staff'){
            this.bookingForm.patchValue({
                recipientCode:"!INTERNAL",
                program:"!INTERNAL",
                analysisCode:"!INTERNAL",
                serviceType:13,
                payType:"UNAVAILABLE",
                staffCode:this.data.data,
                serviceActivity: {
                    activity:"UNAVAILABLE",
                    service_Description:"UNAVAILABLE",
                    fixedTime: 0,
                    autoApprove:0
                    
                }
            });
        }        
        if (!this.recurrenceView)
            this.addBooking(0);
    
}

openRecipientModal(){
   
     this.select_RecipientModal=true;
    
    
        this.booking = {
                     recipientCode: this.FetchCode,
                     userName:this.token.user,
                     date:this.date,
                     startTime:format(this.defaultStartTime,'HH:mm'),
                     endTime:format(this.defaultStartTime,'HH:mm'),
                     endLimit:'20:00'
                    };
                
    this.bookingDataRecipient.next(this.booking) ;               
}
onRecipientSearch(data:any){
    this.select_RecipientModal=false;
    this.FetchCode=data.accountNo;

    this.bookingForm.patchValue({
        recipientCode:this.FetchCode,
        analysisCode:data.category
    });
   
  //  if (!this.recurrenceView)
    this.addBooking(0);
}
setTravel(val:number){
    this.resetBookingFormModal();
    this.isTravel=false;
    this.serviceType="TRAVELTIME"
    // this.rosterForm.patchValue({
    //     serviceType:"TRAVEL TIME"
    // });
    this.type_to_add=5;
    if (val==1){
            if (this.viewType=='Staff'){
                this.openRecipientModal();

                // this.select_RecipientModal=true;                  

                // if (this.FetchCode!=null && this.FetchCode!=''){
                //     this.bookingForm.patchValue({
                //         recipientCode:this.FetchCode
                //     })
                // } 
            } else
                this.select_StaffModal=true;
    }else{
      
        if (this.viewType=='Staff'){
            this.bookingForm.patchValue({
                recipientCode:"!INTERNAL"
            });
        }else{
            this.bookingForm.patchValue({
                staffCode:"!INTERNAL"
            });
        }

        this.addBookingModel=true;
        this.addBooking(5);
    }
}
start_adding_Booking(bCase:any){
    
    //let obs=this.xero.getUrl();

    this.resetBookingFormModal();
    this.booking_case=bCase;
    this.isTravel=false;
    this.add_multi_roster=false;
    this.GroupShiftCategory="";
    
    if (this.booking_case==1){
        this.selectedCarer='BOOKED';
        this.serviceType="BOOKED";
         this.bookingForm.patchValue({
            staffCode:"BOOKED"
         });
         this.addBooking(1);
         return;
    } else if (this.booking_case==2){
        this.serviceType="ADMISSION";
        // this.rosterForm.patchValue({
        //     serviceType:"ADMISSION"
        // });
    }else if (this.booking_case==4){
       
        this.serviceType="GROUPBASED";
        if (this.viewType=="Staff"){
            this.showtMultiRecipientModal=true;
            return;
        }
        // this.rosterForm.patchValue({
        //     serviceType:"ADMISSION"
        // });
    }  else if (this.booking_case==5){
        //this.isTravelTimeChargeable=true;
        this.isTravel=true;
        this.serviceType="TRAVELTIME";
        // this.rosterForm.patchValue({            
        //     serviceType:"TRAVEL TIME"
        // });
        
        return;

    }  else if (this.booking_case==6){
        //this.isTravelTimeChargeable=true;
        this.isTravel=true;
        this.serviceType="ADMINISTRATION";
        // this.rosterForm.patchValue({            
        //     serviceType:"TRAVEL TIME"
        // });
        
        return;
        
    }else{
        //  this.rosterForm.patchValue({
        //     serviceType:""
        // });  
        this.serviceType="";
    }
    if (this.viewType=="Staff"){
        this.openRecipientModal();
        // this.select_RecipientModal=true;    
        // if (this.FetchCode!=null && this.FetchCode!=''){
        //     this.bookingForm.patchValue({
        //         recipientCode:this.FetchCode
        //     })
        // } 
        
    }else {
        
        if (this.booking_case==2){
            this.booking_case=3;          
        }
        this.select_StaffModal=true;
       
        //this.addBookingModel=true;
        //this.addBooking(0);
    }

    
}


async  getStaffAllocation (startTime:string){
  
          
        let res = await this.IsStaffAllocated(this.selectedCarer,this.date,startTime,this.durationObject.duration);
       res.subscribe(data=>{
           console.log(data);
       })
        if (status){
            this.globalS.eToast('Booking', 'Staff is already allocated in this date and time slot');
        }
    }

   
addBooking(type:any){    
   
    this.select_StaffModal=false;
    this.select_RecipientModal=false;
    //this.ShowCentral_Location=false;
    
   
    this.current=0;
    
    this.type_to_add=type;
   if (type==1){
    this.add_UnAllocated=true;
    this.type_to_add=1;
   }else
   {
    this.add_UnAllocated=false;
   }
   
    this.Timesheet_label = "Add Timesheet " 
   
    this.addRecurrent=true;
   
    let sheet = this.spreadsheet.getActiveSheet();
    var range=sheet.getSelections();
    let startTime="";
    let endTime = "";

    //let dt = moment.utc(this.date).local();
    if ((range==null || range.length==0) && !this.recurrenceView){
        this.globalS.eToast('Booking', 'Please select some time range to proceed');
        return;
    }
    
    if (!this.recurrenceView){
        let col=range[0].col;
        let date = sheet.getCell(0,col,GC.Spread.Sheets.SheetArea.colHeader).tag();
       
        this.date = parseISO(this.datepipe.transform(date, 'yyyy-MM-dd'));
        let dt= new Date(this.date);
        date = dt.getFullYear() + "-" + this.numStr(dt.getMonth()+1) + "-" + this.numStr(dt.getDate());
        let f_row= range[0].row;
        let l_row=f_row+range[0].rowCount;
        startTime = sheet.getCell(f_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag()
        endTime =   sheet.getCell(l_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();
        //let endTime =sheet.getTag(l_row,0,GC.Spread.Sheets.SheetArea.viewport);

        this.defaultStartTime = parseISO(new Date(date + " " + startTime).toISOString());
        this.defaultEndTime = parseISO(new Date(date + " " + endTime).toISOString());

        this.date = parseISO(this.datepipe.transform(date, 'yyyy-MM-dd'));

      
    }

    this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
   
   // this.bookingForm.patchValue({date:date})

   if (this.booking_case==7){
       this.type_to_add=13;
        this.doneBooking();
        return;
   }
       
    
    //this.date = parseISO(new Date(date).toISOString());
    const { recipientCode, debtor, serviceType, isMultipleRecipient } = this.bookingForm.value;
    
    if (this.viewType=="Staff"){
        this.FetchCode = recipientCode;
        if (this.IsGroupShift)
            this.FetchCode="!MULTIPLE"
    }else
        this.FetchCode = this.selected.data;
    
     
        let dt= moment(this.date).format('YYYY/MM/DD')

        this.GETPROGRAMS( this.FetchCode,dt).subscribe(d => {
        this.programActivityList=d;
        this.programsList = d.map(x => x.progName);
        //remove duplicate values
        this.programsList = this.programsList.filter((v, i, a) => a.indexOf(v) === i);
        //this.serviceActivityList = d.map(x => x.serviceType);
       // this.serviceActivityList = this.serviceActivityList.filter((v, i, a) => a.indexOf(v) === i);
      
        if (this.programsList==null || this.programsList.length==0){
          // 
            this.globalS.sToast('Booking', 'No Program setting found to proceed');
            this.addBookingModel=false;
        }else{
            if (this.programsList.length==1){
                this.defaultProgram=this.programsList[0];
                this.selectedProgram= this.programActivityList[0];
                this.bookingForm.patchValue({
                    program:this.defaultProgram
                })
                
                this.current+=1;
            }
            let status:boolean=false;
            if (this.viewType=="Recipient" && this.type_to_add>1){  
              
               // this.getStaffAllocation(startTime);   
               if (this.selectedCarer=='!MULTIPLE' || this.selectedCarer=='!INTERNAL')
                    this.addBookingModel=true;   
               else  this.IsStaffAllocated(this.selectedCarer,this.date,startTime,this.durationObject.duration).subscribe(data=>{
                   
                    if (data.length>0){
                        this.globalS.wToast('Booking', `Staff ${this.selectedCarer} is already allocated in this date and time slot`);
                        status=true;
                        this.addBookingModel=true;
                        //return;
                    }else
                    this.addBookingModel=true;
                });
               
               
            }else
                this.addBookingModel=true;
                
        }
    });
 
    
}
SaveAdditionalInfo(notes:string){
    this.notes=notes;
    if (this.cell_value==null || this.cell_value.recordNo==0) return;
    this.ProcessRoster("Additional", this.cell_value.recordNo);
  
   
}
deleteRoster_old(){
    if (this.cell_value==null || this.cell_value.recordNo==0) return;

    this.ProcessRoster("Delete",this.cell_value.recordNo);

    let sheet=this.spreadsheet.getActiveSheet();
    this.spreadsheet.suspendPaint();
    this.remove_Cells(sheet,this.cell_value.row,this.cell_value.col,this.cell_value.duration)
    this.operation="Delete";                 
    this.spreadsheet.resumePaint();
    this.deleteRosterModal=false;

   
    if (this.viewType=='Staff'){
        this.current_roster = this.find_roster(this.cell_value.recordNo)
        let clientCode =this.current_roster.recipientCode;
        let date= this.current_roster.date
        this.txtAlertSubject = 'SHIFT DELETED : ' ;
        this.txtAlertMessage = 'SHIFT DELETED : \n' + date + ' : \n'  + clientCode + '\n'  ;
        if (this.Error_Messages.length<=0)
            this.show_alert=true;
    }
}
getStatus(status:string){
    let val: string;
   
    switch(parseInt(status)){
        case 1:
            val="Unapproved"
            break
        case 2:
            val="Approved"
            break
        case 3:
            val="Billed"
            break
        case 4:
            val="Paid and Billed"
            break
        case 5:
            val="Paid"
            break
        default:
            val="Undefined"       
    }

    return val;
}
deleteRoster(){

    var sheet = this.spreadsheet.getActiveSheet();
    var selected_Cell:any;                   
    var sels = sheet.getSelections();
    var sel = sels[0];
    var row = sel.row;
    selected_Cell=sel;
    let selected_columns = selected_Cell.col + selected_Cell.colCount;
   
    let data_row=0;
    let row_iterator=0;
    let recdNo=0;

    let someRecordDeleted:boolean=false;
    this.deleteRosterModal=false;
    this.processing=true;
    
    for (let i=selected_Cell.col; i<selected_columns; i++)
    {
        data_row=selected_Cell.row;
        
        for ( row_iterator=0; row_iterator<=selected_Cell.rowCount; row_iterator++){
           if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)==null ){
            data_row=data_row+1;
            continue;
           }                      
                         
         if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)!=null)
            this.cell_value=sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)
        
        if (this.cell_value.recordNo==null || this.cell_value.recordNo==0){           
            data_row=data_row+1;
            continue;
        } 

        if (this.cell_value.recordNo==recdNo){
            data_row=data_row+1;
            continue;
        }
                    
        recdNo=this.cell_value.recordNo;
        let v= this.find_roster(recdNo);
        
        if (v.status!=1){
            let sts=this.getStatus(v.status)
            this.globalS.eToast("Day Manager", `${sts}  roster (${v.startTime}-${v.endTime}) cannot be deleted`);
            continue;
        } 
          
        //if (this.cell_value==null || this.cell_value.recordNo==0) return;
        this.ProcessRoster("Delete",this.cell_value.recordNo);
        someRecordDeleted=true;
        this.remove_Cells(sheet,this.cell_value.row,this.cell_value.col,this.cell_value.duration)
        this.operation="Delete";   
        data_row=data_row+1;
        
      }

    
    }
    this.spreadsheet.resumePaint();
   
    this.processing=false;
   
    if (this.viewType=='Staff' && someRecordDeleted){
        this.current_roster = this.find_roster(this.cell_value.recordNo)
        let clientCode =this.current_roster.recipientCode;
        let date= this.current_roster.date
        this.txtAlertSubject = 'SHIFT DELETED : ' ;
        this.txtAlertMessage = 'SHIFT DELETED : \n' + date + ' : \n'  + clientCode + '\n'  ;
        if (this.Error_Messages.length<=0)
            this.show_alert=true;
    }
}
reAllocate(){
   // if (this.cell_value==null || this.cell_value.recordNo==0) return;

    let sheet=this.spreadsheet.getActiveSheet();
    var selected_Cell:any;                   
    var sels = sheet.getSelections();
    var sel = sels[0];
    var row = sel.row;
    selected_Cell=sel;
    let selected_columns = selected_Cell.col + selected_Cell.colCount;
   
    this.spreadsheet.suspendPaint();
    let data_row=0;
    let row_iterator=0;
    let recdNo=0;
 
    for (let i=selected_Cell.col; i<selected_columns; i++)
    {
        data_row=selected_Cell.row;
        
        for ( row_iterator=0; row_iterator<=selected_Cell.rowCount; row_iterator++){
           if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)==null ){
            data_row=data_row+1;
            continue;
           }                      
                         
         if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)!=null)
            this.cell_value=sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)
        
        if (this.cell_value.recordNo==null || this.cell_value.recordNo==0){           
            data_row=data_row+1;
            continue;
        } 

        if (this.cell_value.recordNo==recdNo){
            data_row=data_row+1;
            continue;
        }
                    
        recdNo=this.cell_value.recordNo;
 
        //if (this.cell_value==null || this.cell_value.recordNo==0) return;
        this.operation="Re-Allocate";  
        
    
     
            this.ProcessRoster("Re-Allocate", recdNo+"");
            
                // if (this.errorValue==0){
               
                //     var text=   this.selectedCarer + " (" + this.cell_value.service.replace('BOOKED','') + ")";            
                //     this.cell_value.type= this.DETERMINE_SERVICE_TYPE_NUMBER(this.cell_value.service) ;
                //     this.draw_Cells(sheet,this.cell_value.row,this.cell_value.col,this.cell_value.duration,this.cell_value.type,this.cell_value.recordNo,text)
                // }
            
           

        data_row=data_row+1;
        
      }
    }
    
    
    //this.remove_Cells(sheet,this.cell_value.row,this.cell_value.col,this.cell_value.duration)
    //this.cell_value.type = this.DETERMINE_SERVICE_TYPE_NUMBER(this.cell_value.service);
    
    this.spreadsheet.resumePaint();
   

}


UnAllocate(){
   // if (this.cell_value==null || this.cell_value.RecordNo==0) return;

   
   let sheet=this.spreadsheet.getActiveSheet();
   var selected_Cell:any;                   
   var sels = sheet.getSelections();
   var sel = sels[0];
   var row = sel.row;
   selected_Cell=sel;
   let selected_columns = selected_Cell.col + selected_Cell.colCount;
  
   this.spreadsheet.suspendPaint();
   let data_row=0;
   let row_iterator=0;
   let recdNo=0;

   for (let i=selected_Cell.col; i<selected_columns; i++)
   {
       data_row=selected_Cell.row;
       
       for ( row_iterator=0; row_iterator<=selected_Cell.rowCount; row_iterator++){
          if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)==null ){
           data_row=data_row+1;
           continue;
          }                      
                        
        if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)!=null)
           this.cell_value=sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)
       
       if (this.cell_value.recordNo==null || this.cell_value.recordNo==0){           
           data_row=data_row+1;
           continue;
       } 

       if (this.cell_value.recordNo==recdNo){
           data_row=data_row+1;
           continue;
       }
                   
       recdNo=this.cell_value.recordNo;

       //if (this.cell_value==null || this.cell_value.recordNo==0) return;
       this.operation="Un-Allocate";         
       this.ProcessRoster("Un-Allocate", recdNo+"");
   
       var text= "";//  this.selectedCarer + " (" + this.cell_value.service.replace('BOOKED','') + ")";            
       this.cell_value.type= this.DETERMINE_SERVICE_TYPE_NUMBER(this.cell_value.service) ;
        if (this.viewType=='Staff')
            this.remove_Cells(sheet,this.cell_value.row,this.cell_value.col,this.cell_value.duration)
        else{
            this.cell_value.type = 1;
            var service =this.cell_value.service.split("(")[1];
            var text=    "BOOKED (" + service + ")"; 

            this.draw_Cells(sheet,this.cell_value.row,this.cell_value.col,this.cell_value.duration,this.cell_value.type,this.cell_value.recordNo,text)
        }
   
       data_row=data_row+1;
       
     }
   }
   
   
    this.spreadsheet.resumePaint();


}
showViews(){
    this.show_views=false;
    this.Already_loaded=false;
    this.prepare_Sheet();
     this.load_rosters();
     this.setCalanderView();
}
show_MoreOptions(){
    this.show_More=!this.show_More;
}


hide_MoreOptions(i:number){
    if (i==0)
        this.include_fee=true;
    if (i==1)
        this.include_item=true;

    this.show_More=false;
}
SaveMasterRosters(){

    if (this.rosters==null) return;
    if (this.rosters.length<=0) return;

    let rst:any=this.rosters[0];
    
   let RecordNo=rst.recordNo;
    if (this.viewType=="Staff")
        this.ProcessRoster("Staff Master", RecordNo);
    else if (this.viewType=="Recipient")
        this.ProcessRoster("Client Master", RecordNo);
}
SetMultishift(){
    if (this.cell_value==null || this.cell_value.recordNo==0) return;

    
    this.ProcessRoster("SetMultishift", this.cell_value.recordNo);
    

}
ClearMultishift(){
    if (this.cell_value==null || this.cell_value.recordNo==0) return;

    this.ProcessRoster("ClearMultishift", this.cell_value.recordNo);
    

}
  
load_rosters(){
    
  //  this.prepare_Sheet();

    if (this.spreadsheet==null) return;
    
    this.spreadsheet.suspendPaint();
    let sheet = this.spreadsheet.getActiveSheet()
    var cell:any;
    
    if (this.viewType=="Staff"){
        sheet.name("Staff Rosters");
        this.sheetName="Staff Rosters";
    }else{
        sheet.name("Recipient Rosters");
        this.sheetName="Recipient Rosters";
    }
    var typeName:string='';
    var instance = new GC.Spread.Sheets.FloatingObjects.FloatingObjectCollection(sheet, typeName);
    var value; // Type: any
    value = instance.clear();
   
    //this.AddFloatingObjectTemp();
   

     cell= sheet.getRange(0, 0, this.time_slot, this.Days_View, GC.Spread.Sheets.SheetArea.viewport)
    
     
   // cell.setBorder(new GC.Spread.Sheets.LineBorder("#C3C1C1", GC.Spread.Sheets.LineStyle.thin), {all:true});
   cell.setBorder(new GC.Spread.Sheets.LineBorder("#F0F0F0", GC.Spread.Sheets.LineStyle.thin), {all:true});
   //cell.borderTop( new GC.Spread.Sheets.LineBorder("#F0F0F0",GC.Spread.Sheets.LineStyle.empty));
   //cell.borderBottom( new GC.Spread.Sheets.LineBorder("#F0F0F0",GC.Spread.Sheets.LineStyle.empty));
   
    // var rectangle = this.spreadsheet.getActiveSheet().shapes.add("rectangle", GC.Spread.Sheets.Shapes.AutoShapeType.rectangle, 40, 230, 150, 150);
    //  let rectangleStyle = rectangle.style();
    //  rectangleStyle.fill.color = "#FFFFFF";
    //  rectangle.style(rectangleStyle);
    //  var donutShape = sheet.Shapes.add('autoShape',GC.Spread.Sheets.Shapes.AutoShapeType.donut,100,50,150,150);          
    //  // Adding text to built-in shape "donut"
    //  donutShape.text("Donut Shape");
    //  // Adding style to built-in shape "donut"
    //  var shapeStyle = donutShape.style();
    //  shapeStyle.textEffect.color = "Red";
    //  shapeStyle.textEffect.font = "18px Arial";
    //  shapeStyle.textFrame.hAlign = GC.Spread.Sheets.HorizontalAlign.center;
    //  shapeStyle.textFrame.vAlign = GC.Spread.Sheets.VerticalAlign.center;
    //  donutShape.style(shapeStyle);

    if (this.master){
        //cell.backColor("#FF8080");
        cell.backColor("white");
        cell.text("").cellType(new IconCellType2(document.getElementById('icon-21')));
        
    }else{
        cell.backColor("white");
        cell.text("").cellType(new IconCellType2(document.getElementById('icon-21')));
    }
    cell.text("")
    cell.tag(null);
    
          
    let row=-1, col=-1;
    if (this.rosters==null) {
        this.spreadsheet.resumePaint();
        return;
    }
    var text,code;
    for(var r of this.rosters){
            
       // if (r.dayNo>this.Days_View) break;
      
            
            if (this.Days_View>=30)
                col=r.dayNo-1;
            else{
               // let dt= this.startRoster;//format(new Date(this.startRoster),"YYYY/MM/dd");
                //col=r.dayNo- Number(dt.getDate());
                
                col=this.getColum(r.roster_Date)
                }
            row = this.getrow(r.start_Time);//self.time_map.get(r.Start_Time); //
            if (this.viewType=="Staff")
                code= r.clientCode ;
            else
                code= r.carerCode 
            
            if (code=="!MULTIPLE")
                    code="GROUP SHIFT";
            if (code=="!INTERNAL")
                    code="ADMIN SHIFT";
                
              //  text=  r.start_Time + "-" + r.end_Time + " " + code + " (" + r.serviceType + ")";

              text=   code + " (" + r.serviceType + ")";

            if (row!=null && col !=null)
            this.draw_Cells(sheet,row,col,r.duration, r.type, r.recordNo, text);
           // this.draw_Cells2(sheet,row,col,r.duration, r.type, r.recordNo, text,r);
        
    }

    this.spreadsheet.resumePaint();
  }
  
  setMasterRoster($event:any){
      console.log("Master Roster")
      
      
      //this.master=!this.master;
      this.master=$event;
      if (this.master){
        this.date="1900/01/01";
        this.Master_Roster_label='Master Roster'
        //this.startRoster="1900/01/01";
       // this.endRoster="1900/01/31";
      }else{        
        this.date = moment()  
        this.CalendarDate = new Date(this.date) ;   
        this.Master_Roster_label='Current Roster';
        //this.startRoster=this.date;
       // this.endRoster=this.date;
    }
    // if(this.Days_View==31 || this.Days_View==30){
    //     this.date = moment(this.date).add('month', 1);       
    //     this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
    //     this.endRoster =moment(this.date).endOf('month').format('YYYY/MM/DD')
    // }else{
                   
        this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
         this.date = moment(this.startRoster).add('day', this.Days_View-1);
         this.endRoster = moment(this.date).format('YYYY/MM/DD');
         this.prepare_Sheet();
       //}
    this.searchRoster(this.date);
     
    var divMaster = document.getElementById("divMaster");    
   // divMaster.style.backgroundColor= this.bgcolor;
    
  }
  
  AddFloatingObjectTemp(){
    let sheet = this.spreadsheet.getActiveSheet();  
    sheet.suspendPaint();
    let customFloatingObject = new GC.Spread.Sheets.FloatingObjects.FloatingObject("f", 10, 10, 60,64)
    customFloatingObject.startRow(1);
    customFloatingObject.startColumn(1);
    customFloatingObject.endColumn(2);
    customFloatingObject.endRow(2);
    let div = document.createElement('div');
    div.innerHTML = "<span>SpreadJS supports FloatingObject.</span><div style='border: 1px dotted red; width: 80%; margin:auto;'><ul><li>I am list one.</li><li>I am list two.</li><li>I am list three.</li></ul></div>";
    div.style.background = 'gray';
    customFloatingObject.content(div);
    sheet.floatingObjects.add(customFloatingObject);
    sheet.resumePaint();
  }
  
  prepare_Sheet(){

    if (this.spreadsheet==null){
       
        return;
    }

   let sheet:any=this.spreadsheet.getActiveSheet(); 
   

   //this.changeHeight()
   this.spreadsheet.suspendPaint();
  
     // Set the default size.
    // if (this.screenWidth<300)
    //     this.screenWidth=1000;
    //     if (!this.fixSize)
    //      this.spreadsheet.getHost().style.width = (this.screenWidth - 260) + 'px';
    //      this.spreadsheet.getHost().style.height = (this.screenHeight - 170) + 'px';
        // if (this.fixSize){
        //    this.spreadsheet.getHost().style.width =  '1100px';
        //    this.spreadsheet.getHost().style.height =  '500px';
           
        // }
    
    sheet.clearSelection(); 
    
    
    var style = new GC.Spread.Sheets.Style();
    style.font = "10pt Segoe UI";     
    style.themeFont = "Segoe UI";
    style.cellPadding = "3";
    
    sheet.setDefaultStyle(style, GC.Spread.Sheets.SheetArea.viewport);
   
   
   
     let date:Date = new Date(this.date);

    if (this.startRoster==null){
        date =  new Date(this.date);
        let d = date.getDate()-1;
        date.setDate(date.getDate()-d)  
        
    }else
        date = new Date(this.startRoster);

    let m = date.getMonth()+1;
    let y=date.getFullYear();
  
     
   //
    
    let days:number =this.getDaysInMonth(m,y);

   if (this.Days_View>=30){
    this.Days_View=days
   }
   
    sheet.setColumnCount(this.Days_View, GC.Spread.Sheets.SheetArea.viewport);
    sheet.setRowCount(this.time_slot, GC.Spread.Sheets.SheetArea.viewport);
    sheet.setColumnResizable(0,true, GC.Spread.Sheets.SheetArea.colHeader);

    //This example uses the highlightStyle method.
   // sheet.getRange(0, 0, this.time_slot, 31, GC.Spread.Sheets.SheetArea.viewport).borderTop( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.empty));
   // sheet.getRange(0, 0, this.time_slot, 31, GC.Spread.Sheets.SheetArea.viewport).borderBottom( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.empty));
   
   var isHoliday:boolean=false;

    for (let i=0; i<=this.Days_View ; i++)   
    {
   
        
      var head_txt="";
      if (this.Days_View>=30){
            head_txt=this.DayOfWeek( date.getDay())  + " "  + date.getDate() ;   
            //sheet.setValue(0, i, { richText: [{ style: { font: '10px Tahoma ', foreColor: 'white' }, text: head_txt   }] }, GC.Spread.Sheets.SheetArea.colHeader);        
    }else{
         head_txt=this.FullDayOfWeek( date.getDay())  + " "  + date.getDate() ;   
         //sheet.setValue(0, i, { richText: [{ style: { font: '10px Tahoma ', foreColor: 'white' }, text: head_txt   }] }, GC.Spread.Sheets.SheetArea.colHeader);        

      }
      var col_header = sheet.getRange(i, -1, 1, -1, GC.Spread.Sheets.SheetArea.colHeader);
      
      col_header.setBorder(new GC.Spread.Sheets.LineBorder("#ffffff", GC.Spread.Sheets.LineStyle.thin), {all:true}); 
      //col_header.borderTop(new GC.Spread.Sheets.LineBorder("#000000", GC.Spread.Sheets.LineStyle.double));
     // col_header.borderTop(new GC.Spread.Sheets.LineBorder("#ffffff", GC.Spread.Sheets.LineStyle.thin));
     // col_header.borderLeft(new GC.Spread.Sheets.LineBorder("#ffffff", GC.Spread.Sheets.LineStyle.thin));
     // col_header.borderRight(new GC.Spread.Sheets.LineBorder("#FFFFFF", GC.Spread.Sheets.LineStyle.thick));
     //col_header.borderBottom(new GC.Spread.Sheets.LineBorder("#ffffff", GC.Spread.Sheets.LineStyle.thin));
      sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).tag(date);

     var new_width = 100 / this.Days_View;
     
     //setting column height
     sheet.setRowHeight(0, 40.0,GC.Spread.Sheets.SheetArea.colHeader);
    //setting column width
    
     sheet.setColumnWidth(i, new_width +"*",GC.Spread.Sheets.SheetArea.viewport);
     sheet.setColumnResizable(i,true, GC.Spread.Sheets.SheetArea.colHeader);

     if (this.DayOfWeek( date.getDay())=="Wed" ){
        sheet.setColumnWidth(i, (new_width+1) +"*",GC.Spread.Sheets.SheetArea.viewport);
     }
    
      if (this.DayOfWeek( date.getDay())=="Sat" || this.DayOfWeek( date.getDay())=="Sun")
      {
            if (this.master){
                sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).backColor("#E0C1A3"); //FFDEDB
                
                if (this.Days_View>=28)
                    sheet.setValue(0, i, { richText: [{ style: { font: '10px Tahoma ', foreColor: '#000000' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
                else
                    sheet.setValue(0, i, { richText: [{ style: { font: '12px Tahoma ', foreColor: '#000000' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
                
            }else{
                sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).backColor("#F18805"); //FFDEDB
                if (this.Days_View>=28)
                    sheet.setValue(0, i, { richText: [{ style: { font: '10px Tahoma ', foreColor: '#ffffff' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
                else
                    sheet.setValue(0, i, { richText: [{ style: { font: '12px Tahoma ', foreColor: '#ffffff' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
                
            }    
            
    
        }else
        {
            if (this.master){
                sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).backColor("#E0BCB5"); //FFDEDB                
               
                if (this.Days_View>=28)
                    sheet.setValue(0, i, { richText: [{ style: { font: '10px Tahoma',foreColor: '#000000' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
                else
                    sheet.setValue(0, i, { richText: [{ style: { font: '12px Tahoma',foreColor: '#000000' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        

            }else{
                sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).backColor("#002060"); //FFDEDB
               
                if (this.Days_View>=28)
                    sheet.setValue(0, i, { richText: [{ style: { font: '10px Tahoma',foreColor: '#ffffff' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
                else
                    sheet.setValue(0, i, { richText: [{ style: { font: '12px Tahoma',foreColor: '#ffffff' }, text: head_txt }] }, GC.Spread.Sheets.SheetArea.colHeader);        
            }
        
            isHoliday= this.IsPublicHoliday1(moment(date).format('YYYY/MM/DD'))
             
            if (isHoliday){
                sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).backColor("#87D068"); //FFDEDB
            }
           // sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).get('gc-columnHeader-highlight').backgroundColor="#002060";
           
        }
            date.setDate(date.getDate()+1);
           // sheet.getCell(0, i, GC.Spread.Sheets.SheetArea.colHeader).cell('10 10 10 10');
            //sheet.getCell(0,i,style,GC.Spread.Sheets.SheetArea.colHeader).set
    }
 

    let time:Time;
    time={hours:0,
        minutes:0}
        

    for (let j=0; j<this.time_slot; j++)   {      
   
        var row_txt=date.getDate() + " " + this.DayOfWeek( date.getDay());
        
        if (time.minutes==0){
            row_txt = this.numStr(time.hours)  + ":" + this.numStr(time.minutes) 
        }else if (time.minutes%15==0)
            row_txt= "";// row_txt = "     "+this.numStr(time.minutes)  ;
        else
            row_txt= "";
            sheet.setValue(j, 0, { richText: [{ style: { font: '14px Tahoma ', foreColor: 'black' }, text: row_txt   }] }, GC.Spread.Sheets.SheetArea.rowHeader);
           
         //sheet.setValue(j, 0, { richText: [{ style: { font: '12px Segoe UI ', foreColor: 'white' }, text: row_txt   }] }, GC.Spread.Sheets.SheetArea.rowHeader);        
        // sheet.getRange(j, 0, 1, 1).tag(this.numStr(time.hours)  + ":" + this.numStr(time.minutes));
        sheet.getCell(j, 0, GC.Spread.Sheets.SheetArea.rowHeader).tag(this.numStr(time.hours)  + ":" + this.numStr(time.minutes));

        this.time_map.set(j,this.numStr(time.hours)  + ":" + this.numStr(time.minutes))
        sheet.getCell(j, 0, GC.Spread.Sheets.SheetArea.rowHeader).backColor("#F18805");
        sheet.getCell(j, 0, GC.Spread.Sheets.SheetArea.rowHeader).foreColor("#000000");
        //setting row height
        sheet.setRowHeight(j, 28.0,GC.Spread.Sheets.SheetArea.viewport);
        //setting row width
        sheet.setColumnWidth(0, 60.0,GC.Spread.Sheets.SheetArea.rowHeader);
        sheet.setRowResizable(j,true, GC.Spread.Sheets.SheetArea.rowHeader);
        var row_header = sheet.getRange(j, -1, 1, -1, GC.Spread.Sheets.SheetArea.rowHeader);      
        //row_header.setBorder(new GC.Spread.Sheets.LineBorder("#000000", GC.Spread.Sheets.LineStyle.thin), {all:true}); 
        row_header.borderLeft(new GC.Spread.Sheets.LineBorder("#000000", GC.Spread.Sheets.LineStyle.double));
      
        if (this.time_slot==288)
            time.minutes+=5;
        else if (this.time_slot==144)
            time.minutes+=10;
        else if (this.time_slot==96)
            time.minutes+=15;
        else if (this.time_slot==48)
            time.minutes+=30;
        else if (this.time_slot==36)
            time.minutes+=45;
        else if (this.time_slot==24)
            time.minutes+=60;

        if (time.minutes==60){
          time.minutes=0;
          time.hours+=1;
        }
      
        
    }

        if (this.time_slot==288){
            sheet.setActiveCell(96,0)
            sheet.showCell (96,0)
        } else if (this.time_slot==144){
            sheet.setActiveCell(48,0)
            sheet.showCell (48,0)
        } else if (this.time_slot==96){
            sheet.setActiveCell(32,0)
            sheet.showCell (32,0)
        } else if (this.time_slot==48){
            sheet.setActiveCell(16,0)
            sheet.showCell (16,0)
        }else if (this.time_slot==36){
            sheet.setActiveCell(12,0)
            sheet.showCell (12,0)
        }else if (this.time_slot==24){
            sheet.setActiveCell(8,0)
            sheet.showCell (8,0)
        }
        
        sheet.options.isProtected = true;
        sheet.options.protectionOptions.allowDeleteRows  = false;
        sheet.options.protectionOptions.allowDeleteColumns = false;
        sheet.options.protectionOptions.allowInsertRows = false;
        sheet.options.protectionOptions.allowInsertColumns = false;
        sheet.options.protectionOptions.allowDargInsertRows = false;
        sheet.options.protectionOptions.allowDragInsertColumns = false;        
        sheet.options.resizeZeroIndicator = GC.Spread.Sheets.ResizeZeroIndicator.enhanced;

      
        this.Already_loaded=true;
        this.spreadsheet.resumePaint();
        
  }

  set_Time_Interval(t:number)
  {
     // this.show_views=false;
      this.time_slot=t;
      this.tval=t;
      console.log(this.tval);
  }  
  
  set_day_view(d:number)
  {
    //this.show_views=false;
      this.Days_View=d;
      this.dval=d;
    //   this.prepare_Sheet();
    //   this.load_rosters();
        if (this.Days_View>30){ 
            
            this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
            this.endRoster =moment(this.date).endOf('month').format('YYYY/MM/DD')
        }else{
            this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
            this.date = moment(this.startRoster).add('day', this.Days_View-1);
            this.endRoster = moment(this.date).format('YYYY/MM/DD');
        }

        this.date= moment(this.startRoster).add('day', 0);
       
    console.log(this.dval);
  }  
  
  setIcon(r:number, c:number, type:number,RecordNo:number,Servicetype:any) {

    var sheet = this.spreadsheet.getActiveSheet();
    this.spreadsheet.suspendPaint();
    sheet.setValue(r,c,Number(type),GC.Spread.Sheets.SheetArea.viewport);
    var text="";
   var range =[new GC.Spread.Sheets.Range(r,c,r,c+1)]      
    if(RecordNo==0)
        text="";
    else
        text=Servicetype + "-" +RecordNo + ", type=" + type;
    switch(Number(type)){
        case 1:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-1')));                    
        break;
        case 2:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-2')));
            break;
        case 3:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-3')));
            //<i nz-icon *ngIf="value.type==2" [nzType]="'heart'" [nzTheme]="'twotone'" [nzTwotoneColor]="'#eb2f96'" class="icon"></i>
            break;
        case 4:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-4')));
            break;
        case 5:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-5')));
            break;
        case 6:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-6')));
            break;
        case 7:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-7')));
            break;           
        case 8:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-8')));
            break;   
        case 9:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-9')));
            break;   
        case 10:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-10')));
            break;
        case 11:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-11')));
            break;        
         case 12:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-12')));
            break;
        case 13:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-13')));
            break;
        case 14:
            sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-14')));
            break; 
        case 15:
                sheet.getCell(r,c).text(text).cellType(new IconCellType(document.getElementById('icon-15')));
                break; 
        case 20:
                sheet.getCell(r,c).text("").cellType(new IconCellType2(document.getElementById('icon-20')));
                break; 
        default:
           sheet.getCell(r,c).text("").cellType(new IconCellType2(document.getElementById('icon-21')));
            
    }
    //sheet.getCell(r,c).backgroundColor="#85B9D5";
    sheet.getCell(r,c).backColor("#ffffcc"); //85B9D5
    
    this.spreadsheet.resumePaint();   
       
  }

  getDaysInMonth(m:number, y:number):number{
      let n:number=0;
      if (m==1 || m==3 || m==5 || m==7 || m==8 || m==10 || m==12) {
        n=31;
      }else if (m==2 && y%4==0){
          n=29;
      }else if (m==2 ){
        n=28;
      }else{
            n=30;
       }
    
      return n;
  }
  getColum(date:any):number{
    let col:number;
    var sheet = this.spreadsheet.getActiveSheet();
    let hdate=this.startRoster;
    var date1 = new Date(hdate); 
	var date2 = new Date(date)
    
    var Time = date1.getTime() - date2.getTime(); 
    var Days = Time / (1000 * 3600 * 24); //Diference in Days

    col = Math.abs(Days);  //this.Days_View-Days;
     return col;
  }
  getrow(starttime:string):number{
   let h,m,r;
    h=Number(starttime.substr(0,2));
    m=Number(starttime.substr(3,2));
    if (this.time_slot==288)
        r=h*12+Math.floor(m/5);
    else if (this.time_slot==144)
        r=h*6+Math.floor(m/10);
    else if (this.time_slot==96)
        r=h*4+Math.floor(m/15);
    else if (this.time_slot==48)
        r=h*4+Math.floor(m/30);
    else if (this.time_slot==36)
        r=h*4+Math.floor(m/45);
    else if (this.time_slot==24)
        r=h*4+Math.floor(m/60);

    return r;
  
  }
  draw_Cells2(sheet:any,r:number, c:number, duration:number, type:number, RecordNo:number,service:any, roster:any)
    {
        var new_duration:number=0;

        if (this.time_slot==288)
            new_duration=duration;
        else if (this.time_slot==144)
            new_duration= Math.ceil(duration/2); 
        else if (this.time_slot==96)
            new_duration= Math.ceil(duration/3); 
        else if (this.time_slot==48)
            new_duration= Math.ceil(duration/6); 
        else if (this.time_slot==36)
            new_duration= Math.ceil(duration/9); 
        else if (this.time_slot==24)
            new_duration= Math.ceil(duration/12); 

    if (new_duration<1)
        new_duration=1;
       

    let customFloatingObject = new GC.Spread.Sheets.FloatingObjects.FloatingObject("f"+RecordNo, 10, 10, 60,64);//new this.spreadsheet.Sheets.FloatingObjects.FloatingObject("f"+RecordNo);
    customFloatingObject.startRow(r);
    customFloatingObject.startColumn(c);
    customFloatingObject.endColumn(c+1);
    customFloatingObject.endRow(r+new_duration);
    let div = document.createElement('div');
    div.innerHTML = `<div id='roster'  >
    <span style='white-space: nowrap; overflow: hidden;  text-overflow: ellipsis; '>
    <img id="icon-1" style="background-color: transparent; width:20px; height:20px"  src="../../../assets/media/r${roster.type}.png"/>
     ${service}
     </span>
    </div>`
   
    div.style.cssText=this.divCSS    
    
   
    customFloatingObject.content(div);    
    sheet.floatingObjects.add(customFloatingObject);
    
    // let floatingObjectEvent = document.getElementById('floatingObjectEvent') as HTMLTextAreaElement;

    // this.spreadsheet.bind(GC.Spread.Sheets.Events.FloatingObjectChanged, function (e, args) {
    //         let floatingObject = args.floatingObject;
    //         let host = floatingObject.getHost();
    //         if (host) {
    //             if (!host[0].getAttribute('clickListeners')) {
    //                 host[0].addEventListener("click", function(e: any) {
    //                     console.log("click");
    //                 });
    //                 host[0].addEventListener("contextmenu", function(e: any) {
    //                     console.log("right click");
    //                 })
    //                 host[0].setAttribute('clickListeners', true)
    //             } 
    //         }
    //     });


  }

  displayFullDescription() {
    this.globalS.iToast("Div","Clicking on div");
  }

    draw_Cells(sheet:any,r:number, c:number, duration:number, type:number, RecordNo:number,service:any){
     
        
        this.draw_Cells3(sheet,r,c,duration,type,RecordNo,service);
        if(1==1) return;
      //  this.current_roster = this.find_roster(RecordNo);
      this.cell_value ={"row":r,"col":c,"duration":duration, "type":type, "recordNo":RecordNo, "service":service};
      var rowImage = "/assets/images/r1.jpg";
     
     // sheet.options.isProtected = true;
      var cell= sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport);
     // cell.borderRight(new GC.Spread.Sheets.LineBorder("#084F58", GC.Spread.Sheets.LineStyle.thin), {all:true});
      //cell.setBorder(new GC.Spread.Sheets.LineBorder("#CAF0F5", GC.Spread.Sheets.LineStyle.thin), {all:true}); 
     // cell.setBorder(new GC.Spread.Sheets.LineBorder("#85B9D5", GC.Spread.Sheets.LineStyle.thin), {all:true}); 
      //sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight(new GC.Spread.Sheets.LineBorder("#084F58", GC.Spread.Sheets.LineStyle.thin))
    
      var new_duration:number=0;

      if (this.time_slot==288)
        new_duration=duration;
    else if (this.time_slot==144)
        new_duration= Math.ceil(duration/2); 
    else if (this.time_slot==96)
        new_duration= Math.ceil(duration/3); 
    else if (this.time_slot==48)
        new_duration= Math.ceil(duration/6); 
    else if (this.time_slot==36)
        new_duration= Math.ceil(duration/9); 
    else if (this.time_slot==24)
        new_duration= Math.ceil(duration/12); 

    if (new_duration<1)
        new_duration=1;

      for (let m=0; m<new_duration; m++){
      if (m==0) {
           //sheet.getCell(r,c).backColor("#CAF0F5");
              //sheet.getCell(r+m,c).backColor({degree: 90, stops: [{position:0, color:"#CAF0F5"},{position:0.5, color:"#0A8598"},{position:1, color:"#80B1B9"},]});
              //sheet.getCell(r+m,c).backColor({degree: 95, stops: [{position:0, color:"#CAF0F5"},{position:0.5, color:"#B3F2FF"},{position:1, color:"#B3F2FF"},]});
              //sheet.getCell(r+m,c).backColor({degree: 95, stops: [{position:0, color:"White"},{position:0.5, color:"White"},{position:1, color:"#0D76C2"},]});
              //sheet.getCell(r+m,c).backColor({type: "path", left: 0.2, top: 0.2, right: 0.8, bottom: 0.8, stops: [{ position: 0, color: "White" }, { position: 0.5, color: "white" }, { position: 1, color: "#0D76C2" },] });
              sheet.getCell(r+m,c).backColor("#ffffcc");//85B9D5
              sheet.getCell(r+m,c).foreColor("Black");
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderTop( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.double));
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderLeft( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.double));
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.double));
            //  sheet.getCell(r,c).backColor("#FFFFFF");
             // sheet.getCell(r,c).backgroundImage(rowImage)
             this.setIcon(r,c,type,RecordNo, service);
             var pat = { type: 1, backgroundColor: "#B7DEED", patternColor: "#21B4E2" };
             // var pat = { type: 1, backgroundColor: "red", patternColor: "yellow" };
              var style = new GC.Spread.Sheets.Style();
              style.backColor = pat;
              
              //sheet.setStyle(r, c, style)
       }  
       else{
        
              // sheet.getCell(r+m,c).backColor("#CAF0F5");
              //  sheet.getCell(r+m,c).backColor("#4D6390");
            sheet.getCell(r+m,c).backColor("#FEFFF0"); // 85B9D5
            
            //sheet.getCell(r+m,c).backColor({type: "path", left: 0.2, top: 0.2, right: 0.8, bottom: 0.8, stops: [{ position: 0, color: "White" }, { position: 0.5, color: "#0D76C2" }, { position: 1, color: "#0D76C2" },] });           // sheet.getCell(r+m,c).backColor({degree: 95, stops: [{position:0, color:"#ABD8DE"},{position:0.5, color:"#CAF0F5"},{position:1, color:"#CAF0F5"},]});
            //  sheet.getCell(r,c).backColor("#FFFFFF");
            //  this.setIcon(r+m,c,20,RecordNo, "");
            //sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#C8CCCC", GC.Spread.Sheets.LineStyle.thin), {all:true});
            sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderTop( new GC.Spread.Sheets.LineBorder("#FEFFF0",GC.Spread.Sheets.LineStyle.medium)); //85B9D5 
            sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderLeft( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.double)); //FFEDA3
            sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.double));
            sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderBottom( new GC.Spread.Sheets.LineBorder("#FEFFF0",GC.Spread.Sheets.LineStyle.medium)); //85B9D5 
           
            if (m==new_duration-1){
                sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderBottom( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.double)); //85B9D5 
           
            }
            var pat = { type: 1, backgroundColor: "#ffffcc", patternColor: "#eeffdd" };
           // var pat = { type: 1, backgroundColor: "red", patternColor: "yellow" };
            var style = new GC.Spread.Sheets.Style();
            style.backColor = pat;
            
            //sheet.setStyle(r+m, c, style)
        }
        //sheet.getCell(r+m,c).field=duration;
        sheet.getCell(r+m,c, GC.Spread.Sheets.SheetArea.viewport).locked(true);
        sheet.getRange(r+m, c, 1, 1).tag(this.cell_value)

      
       }
       // Merging complete sheet cells when AutoMergeDirection is set to Column & AutoMergeMode is set to restricted mode
       //var range = sheet.getRange(r, c, r+new_duration,1);
       //var range = new GC.Spread.Sheets.Range(-1, -1, -1, -1);

       // Auto merge SelectionMode is merged
     //  sheet.autoMerge(range, GC.Spread.Sheets.AutoMerge.AutoMergeDirection.column, GC.Spread.Sheets.AutoMerge.AutoMergeMode.restricted, GC.Spread.Sheets.SheetArea.viewport, GC.Spread.Sheets.AutoMerge.SelectionMode.merged);
      //sheet.autoMerge(range);
       
    }
    draw_Cells3(sheet:any,r:number, c:number, duration:number, type:number, RecordNo:number,service:any){
     
        //  this.current_roster = this.find_roster(RecordNo);
        this.cell_value ={"row":r,"col":c,"duration":duration, "type":type, "recordNo":RecordNo, "service":service};
        var rowImage = "/assets/images/r1.jpg";
       
       // sheet.options.isProtected = true;
        var cell= sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport);
       // cell.borderRight(new GC.Spread.Sheets.LineBorder("#084F58", GC.Spread.Sheets.LineStyle.thin), {all:true});
        //cell.setBorder(new GC.Spread.Sheets.LineBorder("#CAF0F5", GC.Spread.Sheets.LineStyle.thin), {all:true}); 
       // cell.setBorder(new GC.Spread.Sheets.LineBorder("#85B9D5", GC.Spread.Sheets.LineStyle.thin), {all:true}); 
        //sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight(new GC.Spread.Sheets.LineBorder("#084F58", GC.Spread.Sheets.LineStyle.thin))
        
        var new_duration:number=0;
  
        if (this.time_slot==288)
            new_duration=duration;
        else if (this.time_slot==144)
            new_duration= Math.ceil(duration/2); 
        else if (this.time_slot==96)
            new_duration= Math.ceil(duration/3); 
        else if (this.time_slot==48)
            new_duration= Math.ceil(duration/6); 
        else if (this.time_slot==36)
            new_duration= Math.ceil(duration/9); 
        else if (this.time_slot==24)
            new_duration= Math.ceil(duration/12); 
  
      if (new_duration<1)
          new_duration=1;

    //       sheet.addSpan(r, c,new_duration,1 )
    //       sheet.getCell(r , c).setBorder(new GC.Spread.Sheets.LineBorder("#f9cf9b", GC.Spread.Sheets.LineStyle.thin), {all:true});
    //       this.setIcon(r,c,type,RecordNo, service);
    //     // sheet.getCell(r,c).backColor("#ffffcc");//85B9D5
    //   //   sheet.getCell(r + m, c).backColor({type: GC.Spread.Sheets.PatternType.lightHorizontal, backgroundColor: "#ffffcc", patternColor: "#f9cf9b"});
    //      sheet.getCell(r , c).backColor({degree: 90, stops: [{position:0, color:"#eff389"},{position:0.5, color:"#ffffcc"},{position:1, color:"#fffff0"},]});
       
 

  //new_duration
        for (let m=0; m<new_duration; m++){
        if (m==0) {
             //sheet.getCell(r,c).backColor("#CAF0F5");
                //sheet.getCell(r+m,c).backColor({degree: 90, stops: [{position:0, color:"#CAF0F5"},{position:0.5, color:"#0A8598"},{position:1, color:"#80B1B9"},]});
                //sheet.getCell(r+m,c).backColor({degree: 95, stops: [{position:0, color:"#CAF0F5"},{position:0.5, color:"#B3F2FF"},{position:1, color:"#B3F2FF"},]});
                //sheet.getCell(r+m,c).backColor({degree: 95, stops: [{position:0, color:"White"},{position:0.5, color:"White"},{position:1, color:"#0D76C2"},]});
                //sheet.getCell(r+m,c).backColor({type: "path", left: 0.2, top: 0.2, right: 0.8, bottom: 0.8, stops: [{ position: 0, color: "White" }, { position: 0.5, color: "white" }, { position: 1, color: "#0D76C2" },] });
                sheet.getCell(r+m,c).backColor("#ffffcc");//85B9D5
                sheet.getCell(r+m,c).foreColor("Black");
                sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderTop( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));
                sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderLeft( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));
                sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));
                sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderBottom( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.empty));
              //  sheet.getCell(r,c).backColor("#FFFFFF");
               // sheet.getCell(r,c).backgroundImage(rowImage)
               this.setIcon(r,c,type,RecordNo, service);
              // sheet.addSpan(r, c,new_duration,1 )
               sheet.getCell(r + m, c).backColor({degree: 90, stops: [{position:0, color:"#f9ee9b"},{position:0.5, color:"#f1f7c4"},{position:1, color:"#FEFFF0"},]});
             //sheet.getCell(r + m, c).backColor({type: GC.Spread.Sheets.PatternType.lightHorizontal, backgroundColor: "#ffffcc", patternColor: "#f9cf9b"});
             
             //  sheet.getCell(r + m, c).backColor({type:"path", left: 0.4, top: 0.4, right: 0.6, bottom: 0.6, stops: [{position:0, color:"#f1f7c4"},{position:0.5, color:"#f1f7c4"},{position:1, color:"#FEFFF0"},]});
            
             sheet.getCell(r,c).cellPadding('3 3 3 3').labelOptions({
                foreColor: '#333333',              
                visibility: 0,
                font: '9px Arial',
                margin: '5 5 5 15'
            });
    
               //f5f4bc
            }  
         else{
          
           
                // sheet.getCell(r+m,c).backColor("#CAF0F5");
                //  sheet.getCell(r+m,c).backColor("#4D6390");
              sheet.getCell(r+m,c).backColor("#FEFFF0"); // 85B9D5
              
              //sheet.getCell(r+m,c).backColor({type: "path", left: 0.2, top: 0.2, right: 0.8, bottom: 0.8, stops: [{ position: 0, color: "White" }, { position: 0.5, color: "#0D76C2" }, { position: 1, color: "#0D76C2" },] });           // sheet.getCell(r+m,c).backColor({degree: 95, stops: [{position:0, color:"#ABD8DE"},{position:0.5, color:"#CAF0F5"},{position:1, color:"#CAF0F5"},]});
              //  sheet.getCell(r,c).backColor("#FFFFFF");
              //  this.setIcon(r+m,c,20,RecordNo, "");
              //sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#C8CCCC", GC.Spread.Sheets.LineStyle.thin), {all:true});
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderTop( new GC.Spread.Sheets.LineBorder("#FEFFF0",GC.Spread.Sheets.LineStyle.empty)); //85B9D5 
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderLeft( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin)); //FFEDA3
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));
              sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderBottom( new GC.Spread.Sheets.LineBorder("#FEFFF0",GC.Spread.Sheets.LineStyle.empty)); //85B9D5 
             
              if (m==(new_duration-1) || new_duration==1){
                  sheet.getRange(r + m, c, 1, 1, GC.Spread.Sheets.SheetArea.viewport).borderBottom( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin)); //85B9D5 
             
              }
             
             // sheet.getCell(r + m, c).backColor({degree: 90, stops: [{position:0, color:"#fff9f9"},{position:0.5, color:"#FEFFF0"},{position:1, color:"#fff9f0"},]});
            //  sheet.getCell(r + m, c).backColor({type:"path", left: 0.4, top: 0.4, right: 0.6, bottom: 0.6, stops: [{position:0, color:"#f1f7c4"},{position:0.5, color:"#f1f7c4"},{position:1, color:"#FEFFF0"},]});
            
          }
            
          //sheet.getCell(r+m,c).field=duration;
          sheet.getCell(r+m,c, GC.Spread.Sheets.SheetArea.viewport).locked(true);
          sheet.getRange(r+m, c, 1, 1).tag(this.cell_value)
  
        
         }
         // Merging complete sheet cells when AutoMergeDirection is set to Column & AutoMergeMode is set to restricted mode
         //var range = sheet.getRange(r, c, r+new_duration,1);
         //var range = new GC.Spread.Sheets.Range(-1, -1, -1, -1);
  
         // Auto merge SelectionMode is merged
       //  sheet.autoMerge(range, GC.Spread.Sheets.AutoMerge.AutoMergeDirection.column, GC.Spread.Sheets.AutoMerge.AutoMergeMode.restricted, GC.Spread.Sheets.SheetArea.viewport, GC.Spread.Sheets.AutoMerge.SelectionMode.merged);
        //sheet.autoMerge(range);
         
      }
    remove_Cells(sheet:any,r:number, c:number, duration:number){
      
      
      //sheet.getRange(r, c, duration, 1, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#C6EFEC", GC.Spread.Sheets.LineStyle.thin), {all:true});
      this.cell_value ={row:-1,col:-1,duration:0};
      var new_duration:number=0;

      if (this.time_slot==288)
        new_duration=duration;
     else if (this.time_slot==144)
         new_duration= Math.ceil(duration/2); 
    else if (this.time_slot==96)
        new_duration= Math.ceil(duration/3); 
    else if (this.time_slot==48)
        new_duration= Math.ceil(duration/4); 
    else if (this.time_slot==36)
        new_duration= Math.ceil(duration/5); 
    else if (this.time_slot==24)
        new_duration= Math.ceil(duration/6); 
  
      if (new_duration<=0)
          new_duration=1;

          var cell = sheet.getRange(r, c, new_duration, 1);
          cell.setBorder(new GC.Spread.Sheets.LineBorder("#F0F0F0", GC.Spread.Sheets.LineStyle.thin), {all:true});
          cell.borderTop( new GC.Spread.Sheets.LineBorder("#F0F0F0",GC.Spread.Sheets.LineStyle.thin));
          cell.borderBottom( new GC.Spread.Sheets.LineBorder("#F0F0F0",GC.Spread.Sheets.LineStyle.thin));

      for (let m=0; m<new_duration; m++){
        if (m==0) { 
          sheet.getCell(r,c).backgroundImage(null)
          this.setIcon(r,c,21,0, "");
         }  
        
          //sheet.getCell(r+m,c).field=duration;
          sheet.getCell(r+m,c).backColor("white");
          sheet.getCell(r+m,c, GC.Spread.Sheets.SheetArea.viewport).locked(true);
          sheet.getRange(r+m, c, 1, 1).tag(null);
          sheet.getRange(r+m, c, 1, 1).text("");
         // sheet.getCell(r+m,c, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#C3C1C1", GC.Spread.Sheets.LineStyle.thin), {all:true});
      
        }
       
    
    }
    numStr(n:number):string {
      let val="" + n;
      if (n<10) val = "0" + n;
      
      return val;
    }

    checkPasteBroken(Records_to_paste:any):Promise<any>{
        let response:Array<any>=[]       
           return Promise.resolve((() => {
            // code here
            Records_to_paste.forEach(x=>{                          
                this.defaultActivity=x.defaultActivity;
                // this.CheckBrokenShift(x.rstdate,x.startTime,x.endTime,true)
                let tt:any;
                setTimeout(() =>{
                     tt=  this.CheckBrokenShift(x.rstdate,x.startTime,x.endTime,true);   
                },1000);
               
               
                response.push(tt)
              
               
               
               });
            return response; // return whatever you want not neccessory
        })());
           
           
    }
    
     CheckBrokenShift(rosterdate:any, StartTime:any, EndTime:any, pasting:boolean=false):any{
       
        this.b_BrokenShift=false;
        let b_BrokenShifts:boolean=false;
        let b_TeaBreakExists : boolean=false;
        let l_TeaCount : number=0;
        let i_StartTime = 0;
        let lstDayShifts : Array<any>=[];
        let d_ContinuousTime = 0;
        let l_BlockCount = 0;
        let s_LastEndTime:any;
        let s_StartTimeOfBlock:any;
        let s_CurrentStartTime:any;
        let s_CurrentEndTime:any;
        let s_ShiftType:any;
        let l_SvcMinutes:number;
        let s_LastStartTime:any;
        let s_EndTimeOfBlock:any;
        let d_TeaBreakAfter:any;
        let b_BreakRequired:boolean=false;
        let b_MinEngagementLastShift:boolean=false;
        let b_FirstShiftOfTimeBlock:boolean=false;
        let d_MinEngagement:any;
        let b_SuppressErrorMessages:boolean=false;
        let b_BelowMinEngagement:boolean=false;
        let b_BelowMinEngagements:boolean=false;
        let l_BlockCtr:number;
        let l_BreakCtr:number;
        let s_Activity:string;
        let s_ActivityType:string;
        let a_Block:Array<any>=[];
        let a_MBreak:Array<any>=[];
        let s_OldDuration=''
        let d_MealBreakAfter :any;
        let b_CheckTeaBreaks:boolean=false;
        let b_CheckLunchBreaks:boolean=false;
        let s_AWMsg:string='';
        let s_StatusMsg:string;
        let b_DisallowNoMinEngage : boolean=false;

        b_MinEngagementLastShift=false;
        s_Activity =this.defaultActivity.activity;

        let staffCode:any =this.viewType=='Staff' ? this.selected.data : this.selectedCarer;
       
        let date=format(rosterdate,'yyyy/MM/dd');
        let setting:any
        let UserSetting:any

        let b_EnforceValidPositions:boolean;
        let b_EnforceActivityLimits:boolean;      
        let b_UseAwards:boolean;
        let s_AwardEnforcementLevel:string;
        let s_AWState:string;
        let b_DisallowOT:boolean;
        let b_DisallowNoBreaks:boolean;
        let b_DisallowConflicts:boolean;
        let b_DisallowBrokenShift:boolean;
        let l_MealCount:number=0;
        let b_AutoBreak: boolean=false;
        this.breachRoster=false;
        let b_MealBreakExists:boolean=false;

        b_BreakRequired=false;
        let s_StartTime:string;
        let s_Duration:number;
        let s_EndTime:string;

        

        var durationObject = (this.globalS.computeTimeDATE_FNS(StartTime, EndTime)); 

        s_StartTime= format(StartTime,'HH:mm');
        s_EndTime=format(EndTime,'HH:mm');
        s_Duration=durationObject.duration*5;

        let sql=`SELECT CanRosterOvertime, CanRosterBreakless, CanRosterConflicts, CanRosterBrokenShift, CanRosterNoMinEngagement, AWARDEnforcement
        ,UsePositions, EnforceActivityLimits, UseAwards, RosterAwardEnforcement, payCheckerType
         FROM UserInfo, Registration WHERE [name] = '${this.token.user}'`;

    
        let  s4 = `select st.accountno, [Day_MaxOrdHr], [Week_MaxOrdHr], [FN_MaxOrdHr], [TeaBreakStartAt], [MinHoursPerService],
                    [UnpaidMealBreakStartAt], [MinBetweenShiftBreak], [MinBreakBetweenSleepovers], CASE WHEN DisableBrokenShiftAllowance = 0 THEN 1 ELSE 0 END as [PayBrokenShifts] 
                    from staff st 
                    inner join awardpos aw on st.award = aw.code 
                    where st.accountno = '${staffCode}'`;
    

        let SQLStmt = ` Select  [Start Time] as startTime ,
                (Duration * 5)  as SvcMinutes,
                left(convert(varchar,DATEADD(Minute,duration*5,[start time]),108),5) as EndTime ,
                CASE WHEN ISNULL(it.JobType, '') = '' THEN 'WORK' ELSE it.jobtype END  as shiftType
                from Roster ro INNER JOIN ItemTypes it ON ro.[Service Type] = it.title 
                Where [date] = '${date}' AND [carer code] = '${staffCode}' 
                AND ((ISNULL(it.InfoOnly, 0) = 0    OR it.JobType  IN ('TEA BREAK','MEAL BREAK')) AND Type NOT IN (9, 13)) 
                UNION SELECT '${s_StartTime}', ${s_Duration}  , '${s_EndTime}', 'NEWSHIFT'
                ORDER BY [Start Time] ` ;
           
               forkJoin(  
                    this.listS.getlist(sql),
                    this.listS.getlist(s4),                       
                    this.listS.getlist(SQLStmt)                     
                    ).subscribe(data => {
                
                    UserSetting = data[0][0];
                    setting = data[1][0];
                    lstDayShifts = data[2];

                    if (!UserSetting.useAwards && !pasting){
                        this.AddRoster_Entry();
                        return 'valid';
                    }

                    b_EnforceValidPositions = UserSetting.usePositions
                    b_EnforceActivityLimits = UserSetting.enforceActivityLimits
                    b_UseAwards = UserSetting.useAwards;
                    s_AwardEnforcementLevel = UserSetting.rosterAwardEnforcement
                    s_AWState = "ALL"
                 
        
                     b_DisallowOT = UserSetting.canRosterOvertime
                     b_DisallowNoBreaks = UserSetting.canRosterBreakless
                     b_DisallowConflicts = UserSetting.canRosterConflicts
                     
                     b_DisallowNoMinEngage= UserSetting.canRosterNoMinEngagement;
                     b_DisallowBrokenShift = UserSetting.canRosterBrokenShift;
                     s_AwardEnforcementLevel = UserSetting.awardEnforcement;

                    if (lstDayShifts.length<=0) return true;

                    let firstRecord:any;
                    firstRecord=lstDayShifts[0];

                    s_LastStartTime = firstRecord.startTime
                    s_LastEndTime = firstRecord.startTime  ;            
                    s_StartTimeOfBlock = firstRecord.startTime;
                    b_FirstShiftOfTimeBlock = true;
                    l_MealCount=0;
                    l_TeaCount=0;
                    let d_OldHrs=0;
                    let d_Hrs=0;

                    let d_NewHrs = ((firstRecord.duration) * 5) / 60
                    if (s_OldDuration != '' )
                        d_OldHrs = 0//TimeToDecimal(s_OldDuration)
                    else
                        d_OldHrs = 0
                   
                    d_Hrs = d_NewHrs - d_OldHrs
                    d_TeaBreakAfter = (setting.teaBreakStartAt) * 60;
                    b_CheckTeaBreaks = d_TeaBreakAfter > 0 ? true :false;
                    d_MealBreakAfter = parseInt(setting.unpaidMealBreakStartAt) * 60;
                    b_CheckLunchBreaks = d_MealBreakAfter > 0 ? true :false;
                    d_MinEngagement = (setting.minHoursPerService) * 60

                    lstDayShifts.forEach(element => {
                        //'FIND THE FIRST SHIFT
                            
                        s_CurrentStartTime = element.startTime;
                        s_CurrentEndTime = element.endTime;
                        s_ShiftType = element.shiftType
                        l_SvcMinutes = parseInt(element.svcMinutes);

                        if (s_ShiftType == 'MEAL BREAK'){
                            //'IF A TEA BREAK EXISTS THERE MAY BE MORE THAN ONE ON THE DAY SO STORE THE CONTENTS
                            b_MealBreakExists = true;
                            //a_MBreak[0][l_BlockCount] = s_CurrentStartTime
                            //a_MBreak[1][l_BlockCount] = s_CurrentEndTime
                            //a_MBreak[2] [l_BlockCount] = d_ContinuousTime
                            a_MBreak.push({"startTime": s_CurrentStartTime,
                                            "endTime": s_CurrentEndTime,
                                            "continuousTime": d_ContinuousTime,
                                            "blockCount" : l_MealCount
                            
                                        });
                            l_MealCount = l_MealCount + 1
                        }
                        //b_FirstShiftOfTimeBlock = ! (s_LastEndTime >= s_CurrentStartTime)
                        if (s_LastEndTime >= s_CurrentStartTime ){
                               // 'IF THE START TIME OF THE SHIFT IS LESS THAN OR EQUAL TO THE END TIME OF THE PREVIOUS SHIFT THERE IS NO GAP
                               // 'ACCUMULATE THE CONTINOUS TIME SO FAR
                                if (s_LastEndTime == s_CurrentStartTime ){
                                    d_ContinuousTime = d_ContinuousTime + l_SvcMinutes
                                    s_EndTimeOfBlock = element.endTime;
                                }
                        }else{
                                //'THERE IS A GAP, SO AT LEAST 1 BLOCK ONF CONTINUOUS TIME
                                //'THERE MAY BE MULTIPLE BLOCKS PER DAY
                                if (d_ContinuousTime > d_MealBreakAfter ){
                                    //'IF THE CURRENT BLOCK EXCEEDS TEA BREAK RULE STORE THE BLOCK DETAILS FOR LATER CHECKING
                                    //'INCREMENT THE BLOCK COUNTER
                                    //'SET BREAKREQUIRED TRUE
                                    //a_Block[i_StartTime][l_BlockCount] = s_StartTimeOfBlock;
                                    //a_Block[1][l_BlockCount] = s_EndTimeOfBlock;
                                    //a_Block[2] [l_BlockCount] = d_ContinuousTime;
                                    a_Block.push({"startTime": s_StartTimeOfBlock,
                                            "endTime": s_EndTimeOfBlock,
                                            "continuousTime": s_StartTimeOfBlock,
                                            "blockCount" : l_BlockCount
                            
                                        });
                                    s_StartTimeOfBlock = element.startTime;
                                    l_BlockCount = l_BlockCount + 1
                                    b_BreakRequired = true;
                                }
                                                          
                          
                            //  '---+-------------------------------------------------------------------------
                            //  '   |MINIMUM ENGAGEMENT AND BROKEN SHIFT CHECK
                            //  '   +-------------------------------------------------------------------------
                            //      'IF WE ARE IN THIS BLOCK OF CODE BY DEFINITION IT IS A BROKEN SHIFT
                            //      'BUT BROKEN SHIFT DOES NOT APPLY IF START TIME OF THIS SERVICE IS WITHIN
                            //      'MINIMUM ENGAGEMENT PERIOD OF THE PREVIOUS SHIFT
                         
                                                  
                            let diff= 0; 
                            if (s_LastStartTime!=s_CurrentStartTime) {
                                // let dt1 = parseISO(new Date(date + ' ' + s_LastStartTime).toISOString());
                                // let dt2 = parseISO(new Date(date + ' ' + s_CurrentStartTime).toISOString());     
                                let dt1 = new Date(date + ' ' + s_LastStartTime); //,'yyyy/MM/dd HH:mm');
                                let dt2 = new Date(date + ' ' + s_CurrentStartTime); //,'yyyy/MM/dd HH:mm');    
                                let durationObj = this.globalS.computeTimeDATE_FNS(dt1, dt2);                                
                                diff= durationObj.duration   

                            }

                             if(b_MinEngagementLastShift && diff <= d_MinEngagement )
                                 this.b_BrokenShift = false
                             else{
                                 this.b_BrokenShift = true
                                 b_BrokenShifts = true
                            }
                                 
                            let minutes:number=d_ContinuousTime +l_SvcMinutes ;

                             if (minutes >= d_MinEngagement ){
                                 b_BelowMinEngagement = false
                                 b_MinEngagementLastShift = false
                             }else{
                                 b_BelowMinEngagement = true
                                 b_BelowMinEngagements = true
                                 b_MinEngagementLastShift = true
                             }

                                //' +-------------------------------------------------------------------------
                                //' |MIN ENGAGEMENT CHECK
                                //' ---+-------------------------------------------------------------------------
                                    
                                // 'RESET THE ACCUMULATED CONTINUOUS TIME                         
                                if (lstDayShifts.indexOf(element)<(lstDayShifts.length-1)){
                                 d_ContinuousTime = 0
                                 b_BelowMinEngagement = false
                                 this.b_BrokenShift = false
                                }
                            }
                            if (element.endTime > s_LastEndTime ) s_LastEndTime = element.endTime;
                            if (element.startTime > s_LastStartTime ) s_LastStartTime = element.startTime;
                                 
                            });                  
                            // 'PROCESS LAST RECORD
                            if (d_ContinuousTime > d_MealBreakAfter && !b_MealBreakExists ) {
                                // 'IF THE CURRENT BLOCK EXCEEDS TEA BREAK RULE STORE THE BLOCK DETAILS FOR LATER CHECKING
                                //'INCREMENT THE BLOCK COUNTER
                                // 'SET BREAKREQUIRED TRUE
                                //a_Block[i_StartTime][l_BlockCount] = s_StartTimeOfBlock
                                //a_Block[1][l_BlockCount] = s_EndTimeOfBlock
                                //a_Block[2][l_BlockCount] = d_ContinuousTime
                                a_Block.push({"startTime": s_StartTimeOfBlock,
                                "endTime": s_EndTimeOfBlock,
                                "continuousTime": s_StartTimeOfBlock,
                                "blockCount" : d_ContinuousTime
            
                                });
                                l_BlockCount = l_BlockCount + 1
                                b_BreakRequired = true;
                             }
                                                                                            
                              if(lstDayShifts.length == 1 )
                                 b_BreakRequired = d_ContinuousTime > d_MealBreakAfter ? true : false;
                                               
                              //'RESET THE ACCUMULATED CONTINOUS TIME
                              
                               if (d_ContinuousTime > 0 && d_ContinuousTime < d_MinEngagement && lstDayShifts.length>1) {
                                    b_BelowMinEngagements = true
                                }
                                d_ContinuousTime = 0
                        
                                if (b_BreakRequired && b_MealBreakExists == true && l_BlockCount > 0 ){
                                // 'IF THERE IS 1 OR MORE TEA BREAKS ON THE DAY
                                // 'AND THERE IS 1 OR MORE BLOCKS OF CONTINUOUS TIME
                                // 'THEN WE NEED TO CHECK EACH BLOCK TO SEE IF ONE OR MORE OF THE TEA BREAKS SATISFIES THE REQUIREMENTS
                                let MealBreakMissing:boolean=false;
                                for (l_BlockCtr = 0; l_BlockCtr<=l_BlockCount - 1; l_BlockCtr++)                             
                                {
                                        for(l_BreakCtr = 0;  l_BreakCtr< l_MealCount; l_BreakCtr++){
                                           // if (a_MBreak[i_StartTime][l_BreakCtr] > this.DateAdd('n', d_MealBreakAfter, a_Block[i_StartTime][l_BlockCtr],'HH:mm'))
                                           if (a_MBreak[l_BreakCtr].startTime > this.DateAdd('n', d_MealBreakAfter, date + ' ' + a_MBreak[l_BlockCtr].startTime,'HH:mm'))
                                            {
                                                  MealBreakMissing=true;
                                                break;
                                               
                                            }
                                        }
                                }
                                if (!MealBreakMissing) // alternate of jump statement
                                    b_BreakRequired = false;
                                }
                    
                                if (s_Activity != '')
                                { 
                                // s_ActivityType = DBLookup(TraccsDb, "JobType", "ItemTypes", Title = ' ${s_Activity}') 
                                s_ActivityType = this.defaultActivity.jobtype;
                                }else{
                                    s_ActivityType = 'NEWSHIFT'
                                }
                   
                            

                                //'---+-------------------------------------------------------------------------
                                // '   |MINIMUM ENGAGEMENT CHECK (calculated during MEAL BREAK CHECK Code)
                                //'   +-------------------------------------------------------------------------

                                if (b_BelowMinEngagements){

                                     s_AWMsg = `006-Insufficient Time to meet Minimum Engagement rule. Minimum engagement of ${d_MinEngagement}  minutes is required.`
                                   
                                     s_StatusMsg = (s_StatusMsg == '') ? 'UNDER MINIMUM ENGAGEMENT' : s_StatusMsg + " ,UNDER MINIMUM ENGAGEMENT";

                                       
                                     if (b_DisallowNoMinEngage){

                                         if(!b_SuppressErrorMessages ){
                                            // this.globalS.eToast("Roster", s_AWMsg + "You do not have authority to roster shifts not meeting Minimum Engagement rules")
                                          
                                             this.Error_Messages.push(format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + s_AWMsg);
                                             this.b_BrokenShift=true;
                                             this.breachRoster=true;
                                             this.Error_Msg=s_AWMsg;
                                             //return s_AWMsg;
                                             return Promise.resolve((() => {
                                                // code here
                                                return true; // return whatever you want not neccessory
                                            })());
                                        }
                                
                                    }else{
                            
                                         if (!b_SuppressErrorMessages){

                                            switch(s_AwardEnforcementLevel){
                                            case "PREVENT BREACH" :
                                                         
                                                         if(b_SuppressErrorMessages) // Then MsgBox s_AWMsg & "You do not have authority to roster shifts not meeting Minimum Engagement rules", vbCritical
                                                       //  this.globalS.eToast("Roster", s_AWMsg + "You do not have authority to roster shifts not meeting Minimum Engagement rules")
                                                       this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm')  + ' ' + s_AWMsg);  
                                                       
                                                       this.b_BrokenShift=true;
                                                       this.breachRoster=true;
                                                       this.Error_Msg=s_AWMsg;
                                                         return s_AWMsg;
                                                    break;
                                            case "WARN ON BREACH" :
                                                         s_AWMsg = s_AWMsg  + "\nDO YOU WISH TO PROCEED??";
                                                        // Response = MsgBox(s_AWMsg, vbCritical + vbYesNo)

                                                       //  If Response <> vbYes Then b_NoBreachedRosterRules = False Else b_NoBreachedRosterRules = True
                                                       this.b_BrokenShift=true;
                                                       this.continueError=true;
                                                       this.Error_Msg=s_AWMsg ;
                                                       this.breachRoster=true;
                                                       this.addBookingModel=false;
                                                       this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm')   + ' ' + this.Error_Msg)
                                                       
                                                       return s_AWMsg; 
                                              }
                                    
                                 }
                                }
                            }
                             

                        
                                // '   +-------------------------------------------------------------------------
                                // '   |END MINIMUM ENGAGEMENT CHECK (calculated during MEAL BREAK CHECK Code)
                                // '---+-------------------------------------------------------------------------
                                
                                // '---+-------------------------------------------------------------------------
                                // '   |BROKEN SHIFT CHECK (calculated during MEAL BREAK CHECK Code)
                                // '   +-------------------------------------------------------------------------
                        
                                 if (b_BrokenShifts && setting.payBrokenShifts == "1" ){
                                    
                                   //  s_StatusMsg = IIf(s_StatusMsg = "", "BROKEN SHIFT", s_StatusMsg & "," & "BROKEN SHIFT")
                                     s_AWMsg = "007-Rostering will cause a Broken Shift penalty."

                                     //if(b_SuppressErrorMessages) 
                                     //this.globalS.eToast("Roster", s_AWMsg );
                                     this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + s_AWMsg);

                                     if (b_DisallowBrokenShift ){                                

                                         if (!b_SuppressErrorMessages){
                                           // s_AWMsg= s_AWMsg + "You do not have authority to roster shifts causing a broken shift penalty";
                                            // this.globalS.eToast("Roster", s_AWMsg );
                                            // this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + s_AWMsg);
                                            this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + s_AWMsg); 
                                             this.b_BrokenShift=true;
                                             this.breachRoster=true;
                                             this.Error_Msg=s_AWMsg;
                                             return s_AWMsg;
                                         }
                                   }                                
                                }else {
                            
                                    if (!b_SuppressErrorMessages){

                                           switch(s_AwardEnforcementLevel.toUpperCase()) {
                                            case "PREVENT BREACH" :                                                
                                                
                                              //  s_AWMsg= s_AWMsg + "You do not have authority to roster shifts causing a broken shift penalty";
                                               // this.globalS.eToast("Roster", s_AWMsg );
                                                // this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + s_AWMsg);
                                                this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + s_AWMsg); 
                                                this.b_BrokenShift=true;
                                                this.breachRoster=true;
                                                this.Error_Msg=s_AWMsg;
                                                 return s_AWMsg;
                                                 break;
                                             case "WARN ON BREACH" :
                                                 s_AWMsg = s_AWMsg + "\nDO YOU WISH TO PROCEED??"
                                                 this.b_BrokenShift=true;
                                                // Response = MsgBox(s_AWMsg, vbCritical + vbYesNo)

                                                // If Response <> vbYes Then b_NoBreachedRosterRules = False Else b_NoBreachedRosterRules = True
                                                this.continueError=true;
                                                this.Error_Msg=s_AWMsg ;
                                                this.breachRoster=true;
                                                this.addBookingModel=false;
                                                this.breachRoster=true;
                                                this.Error_Msg=s_AWMsg;
                                               
                                                this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + this.Error_Msg); 
                                                return s_AWMsg; 
                                           }
                                    
                                    
                                }
                             
                            
                            }  
                           
                                //  If Not b_NoBreachedRosterRules Then GoTo x_CancelEntry                        
                                //'   +-------------------------------------------------------------------------
                                // '   |END BROKEN SHIFT CHECK (calculated during MEAL BREAK CHECK Code)
                                // '---+-------------------------------------------------------------------------
                        
                                // 698                                 If s_ActivityType <> "MEAL BREAK" Then
                                // 'END NEW MEAL BREAK CODE
                                        
                              
                            //'------------------------------------------------------------------------------------------------------
                          
                         if (s_ActivityType != "MEAL BREAK") {
                            if (b_BreakRequired){
                               // b_NoBreachedRosterRules = False
                                if (b_AutoBreak){
                                        //'AUTOBREAK CODE
                                }else{
                                        
                                  s_StatusMsg = s_StatusMsg = ''? "MISSING MEAL BREAK" : s_StatusMsg +"," + "MISSING MEAL BREAK";

                                if (b_DisallowNoBreaks){

                                 if(!b_SuppressErrorMessages) {
                                   // s_AWMsg=s_AWMsg + "You do not have authority to roster this shift without a Meal Break";
                                   // b_NoBreachedRosterRules = False
                                   // GoTo x_CancelEntry
                                    //this.globalS.eToast("Roster",s_AWMsg);
                                    this.breachRoster=true;
                                    this.Error_Msg=s_AWMsg;
                                  //  this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + s_AWMsg);
                                    this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' + s_AWMsg); 
                                    this.Error_Msg
                                    return s_AWMsg;
                                                
                                 } else{
                                            
                                    if (!b_SuppressErrorMessages){

                                        switch(s_AwardEnforcementLevel.toUpperCase()){
                                        case "PREVENT BREACH" :
                                                
                                                if(! b_SuppressErrorMessages){
                                                  //  s_AWMsg= s_AWMsg + "You do not have authority to roster this shift without a Meal Break";
                                                   // this.globalS.eToast("Roster",s_AWMsg);
                                                  // this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + s_AWMsg);
                                                  this.breachRoster=true;
                                                  this.Error_Msg=s_AWMsg;  
                                                  return s_AWMsg;
                                                }
                                               
                                                break;
                                        case "WARN ON BREACH" :
                                                         s_AWMsg = s_AWMsg + "\n\nDO YOU WISH TO PROCEED??"
                                                        // Response = MsgBox(s_AWMsg, vbCritical + vbYesNo)

                                                         //If Response <> vbYes Then b_NoBreachedRosterRules = False Else b_NoBreachedRosterRules = True
                                                         this.continueError=true;
                                                         this.Error_Msg=s_AWMsg ;
                                                         this.breachRoster=true;
                                                         this.addBookingModel=false;
                                                        
                                                         this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm')  + ' ' + this.Error_Msg); 
                                                         this.breachRoster=true;
                                                         this.Error_Msg=s_AWMsg;
                                                         return s_AWMsg; 
                                                     
                                        }
                                    
                                    }
                                }
                            }
                         }
                     }

                }
                    //'   +-------------------------------------------------------------------------
                  //  '   |**END** LUNCH BREAK CHECK
                 //   '---+-------------------------------------------------------------------------
                
                  //  738                     If Not b_NoBreachedRosterRules Then GoTo x_CancelEntry
                   //     '---+-------------------------------------------------------------------------
                   //     '   |**START** TEA BREAK CHECK
                   //     '   +-------------------------------------------------------------------------
                   b_BreakRequired=false;    
                   if (b_CheckTeaBreaks && d_TeaBreakAfter > 0 )
                                
                             s_AWMsg = "005b-Tea Break is missing. A tea break is required after " + d_TeaBreakAfter + " minutes.";
    
                                
                            //'------------------------------------------------------------------------------------------------------
                            //'START NEW TEA BREAK CODE
                                    
                                 a_Block=[]
                                 a_MBreak=[]
                                                           
                                 i_StartTime = 0
                                   
                                 s_LastStartTime = firstRecord.startTime
                                 s_LastEndTime = firstRecord.startTime  ;            
                                 s_StartTimeOfBlock = firstRecord.startTime;
                                 b_FirstShiftOfTimeBlock = true;
                                 
                                d_ContinuousTime = 0
                                l_BlockCount = 0
                                l_TeaCount = 0
                                //'IF THERE ARE ANY SHIFTS ON THE DAY
                             lstDayShifts.forEach(element => {
                                        //'FIND THE FIRST SHIFT
                                            
                                        s_CurrentStartTime = element.startTime;
                                        s_CurrentEndTime = element.endTime;
                                        s_ShiftType = element.shiftType
                                        l_SvcMinutes = parseInt(element.svcMinutes);

                                    if (s_ShiftType == 'Tea BREAK'){
                                        //'IF A TEA BREAK EXISTS THERE MAY BE MORE THAN ONE ON THE DAY SO STORE THE CONTENTS
                                        b_TeaBreakExists = true;
                                        //a_MBreak[0][l_BlockCount] = s_CurrentStartTime
                                        //a_MBreak[1][l_BlockCount] = s_CurrentEndTime
                                        //a_MBreak[2] [l_BlockCount] = d_ContinuousTime
                                        a_MBreak.push({"startTime": s_CurrentStartTime,
                                                        "endTime": s_CurrentEndTime,
                                                        "continuousTime": d_ContinuousTime,
                                                        "blockCount" : l_MealCount
                                        
                                                    });
                                        l_MealCount = l_MealCount + 1
                                    }
                                    if (s_LastEndTime >= s_CurrentStartTime ){
                                        // 'IF THE START TIME OF THE SHIFT IS LESS THAN OR EQUAL TO THE END TIME OF THE PREVIOUS SHIFT THERE IS NO GAP
                                        // 'ACCUMULATE THE CONTINOUS TIME SO FAR
                                         if (s_LastEndTime == s_CurrentStartTime ){
                                             d_ContinuousTime = d_ContinuousTime + l_SvcMinutes
                                             s_EndTimeOfBlock = element.endTime;
                                         }
                                 }else{
                                    //'THERE IS A GAP, SO AT LEAST 1 BLOCK ONF CONTINUOUS TIME
                                    //'THERE MAY BE MULTIPLE BLOCKS PER DAY
                                    if (d_ContinuousTime > d_TeaBreakAfter ){
                                        //'IF THE CURRENT BLOCK EXCEEDS TEA BREAK RULE STORE THE BLOCK DETAILS FOR LATER CHECKING
                                        //'INCREMENT THE BLOCK COUNTER
                                        //'SET BREAKREQUIRED TRUE
                                        //a_Block[i_StartTime][l_BlockCount] = s_StartTimeOfBlock;
                                        //a_Block[1][l_BlockCount] = s_EndTimeOfBlock;
                                        //a_Block[2] [l_BlockCount] = d_ContinuousTime;
                                        a_Block.push({"startTime": s_StartTimeOfBlock,
                                                "endTime": s_EndTimeOfBlock,
                                                "continuousTime": s_StartTimeOfBlock,
                                                "blockCount" : l_BlockCount
                                
                                            });
                                        s_StartTimeOfBlock = element.startTime;
                                        l_BlockCount = l_BlockCount + 1
                                        b_BreakRequired = true;
                                    }
                                       
                                    //'RESET THE ACCUMULATED CONTINOUS TIME
                                      d_ContinuousTime = 0  
                                      s_LastEndTime = element.endTime;
                                }
                                           
                             });                
                              //              'PROCESS LAST RECORD
                              if (d_ContinuousTime > d_TeaBreakAfter ){
                                // 'IF THE CURRENT BLOCK EXCEEDS TEA BREAK RULE STORE THE BLOCK DETAILS FOR LATER CHECKING
                                //'INCREMENT THE BLOCK COUNTER
                                // 'SET BREAKREQUIRED TRUE
                                //a_Block[i_StartTime][l_BlockCount] = s_StartTimeOfBlock
                                //a_Block[1][l_BlockCount] = s_EndTimeOfBlock
                                //a_Block[2][l_BlockCount] = d_ContinuousTime
                                a_Block.push({"startTime": s_StartTimeOfBlock,
                                "endTime": s_EndTimeOfBlock,
                                "continuousTime": s_StartTimeOfBlock,
                                "blockCount" : d_ContinuousTime
            
                                });
                                l_BlockCount = l_BlockCount + 1
                                b_BreakRequired = true;
                             }
                             if(lstDayShifts.length == 1 )
                             b_BreakRequired = d_ContinuousTime > d_MealBreakAfter ? true : false;
                                           
                          //'RESET THE ACCUMULATED CONTINOUS TIME
                          d_ContinuousTime = 0
                                            
                          if (b_BreakRequired && b_TeaBreakExists == true && l_BlockCount > 0 ){
                            // 'IF THERE IS 1 OR MORE TEA BREAKS ON THE DAY
                            // 'AND THERE IS 1 OR MORE BLOCKS OF CONTINUOUS TIME
                            // 'THEN WE NEED TO CHECK EACH BLOCK TO SEE IF ONE OR MORE OF THE TEA BREAKS SATISFIES THE REQUIREMENTS
                            let TeaBreakMissing:boolean=false;
                            for (l_BlockCtr = 0; l_BlockCtr<=l_BlockCount - 1; l_BlockCtr++)                             
                            {
                                    for(l_BreakCtr = 0;  l_BreakCtr< l_TeaCount; l_BreakCtr++){
                                       // if (a_MBreak[i_StartTime][l_BreakCtr] > this.DateAdd('n', d_MealBreakAfter, a_Block[i_StartTime][l_BlockCtr],'HH:mm'))
                                       if (a_MBreak[l_BreakCtr].startTime > this.DateAdd('n', d_TeaBreakAfter, date + ' ' + a_MBreak[l_BlockCtr].startTime,'HH:mm'))
                                        {
                                            TeaBreakMissing=true;
                                        break;
                                            //GoTo TeaBreakMissing
                                        }
                                    }
                            }
                            if (!TeaBreakMissing) // alternate of jump statement
                                b_BreakRequired = false;
                            }                                         
                
                            if (s_Activity != '')
                            { 
                            // s_ActivityType = DBLookup(TraccsDb, "JobType", "ItemTypes", Title = ' ${s_Activity}') 
                            s_ActivityType = this.defaultActivity.jobtype;
                            }else{
                                s_ActivityType = 'NEWSHIFT'
                            }
               
                                     
    //'END NEW TEA BREAK CODE
    //'------------------------------------------------------------------------------------------------------
                            if (s_ActivityType != "Tea BREAK") {
                                if (b_BreakRequired){
                                // b_NoBreachedRosterRules = False
                                    if (b_AutoBreak){
                                            //'AUTOBREAK CODE
                                    }else{
                                            
                                    s_StatusMsg = s_StatusMsg = ''? "MISSING TEA BREAK" : s_StatusMsg +"," + "MISSING TEA BREAK";

                                    if (b_DisallowNoBreaks){

                                    if(!b_SuppressErrorMessages) {
                                      //  s_AWMsg=s_AWMsg + "You do not have authority to roster this shift without a Tea Break";
                                    // b_NoBreachedRosterRules = False
                                    // GoTo x_CancelEntry
                                        //this.globalS.eToast("Roster",s_AWMsg);
                                        this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + '  Broken Shift '); 
                                        
                                        this.breachRoster=true;
                                        this.Error_Msg=s_AWMsg;
                                        return s_AWMsg;
                                                    
                                    } else{
                                                
                                        if (!b_SuppressErrorMessages){

                                            switch(s_AwardEnforcementLevel.toUpperCase()){
                                            case "PREVENT BREACH" :
                                                    
                                                    if(!b_SuppressErrorMessages){
                                                     //   s_AWMsg= s_AWMsg + "You do not have authority to roster this shift without a Tea Break";
                                                        //this.globalS.eToast("Roster",s_AWMsg);
                                                        
                                                        return s_AWMsg;
                                                    }
                                                
                                                    break;
                                            case "WARN ON BREACH" :
                                                            s_AWMsg = s_AWMsg + "\n\nDO YOU WISH TO PROCEED??"
                                                            // Response = MsgBox(s_AWMsg, vbCritical + vbYesNo)

                                                            //If Response <> vbYes Then b_NoBreachedRosterRules = False Else b_NoBreachedRosterRules = True
                                                            this.continueError=true;
                                                            this.Error_Msg=s_AWMsg ;
                                                            this.breachRoster=true;
                                                            this.addBookingModel=false;
                                                             this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + '  Broken Shift '); 
                                                            this.breachRoster=true;
                                                            this.Error_Msg=s_AWMsg;
                                                            return s_AWMsg; 
                                                        
                                            }
                                        
                                        }
                                    }
                                }
                            }
                        }

                        }
                        //'   +-------------------------------------------------------------------------
                        //'   |**END** TEA BREAK CHECK
                        //'---+-------------------------------------------------------------------------
                    
                //'   +-------------------------------------------------------------------------
               // '   |**END** TEA BREAK AND LUNCH BREAK CHECK
              //  '---+-------------------------------------------------------------------------
            
           
            // '   +=====================================================================================================================================
            // '   |**END** CHECK AWARD (OVERTIME, UNDERTIME, BREAKS, BROKEN SHIFTS) COMPLIANCE IF ENABLED
            // '===+======================================================================================================================================
    
            if (!this.b_BrokenShift && !this.breachRoster && !pasting){
                this.AddRoster_Entry();
            }else if (pasting) {
                this.Pasting_Records(this.selected_Cell,this.sel);
            }

          


      }); // main query response code
}
   

    DateAdd(type:any,n:number,date1:any, format:any=''){
        let date2: Date;
        
        date2 = new Date(date1);
        let val:any=date1;
        switch(type){
           
            case 'n':
                date2.setMinutes(n);  
                break;
            case 'h':
                date2.setHours(n);               
                break;
            case 'd':
                date2.setDate(n);
                break;
            case 'm':
                date2.setMonth(n);
                break;
            case 'y':
                date2.setFullYear(n);
                break;
        }

        if (format!='')
            val=moment(date2).format('HH:mm');

        return val;
    }
    
    
ProcessDetermineAdminExplanation(){
   
    this.showDetermineAdminExplanation=false;
    }
 DetermineAdminExplanation(s_Group : string, s_Program : string) 
{   
        let AdminExplainCode:string;
        let s_MDS : string;
        let SQLStmt:any;
        let type:string=this.selectedProgram.type;   
        s_MDS=this.userSettings.prodDesc;

        switch(s_Group){
        case "TRAINING" :
             AdminExplainCode = s_Group
             break;   
        case "REFERRAL-IN" :
                s_MDS = this.userSettings.prodDesc;
                if (s_MDS == "NSW" )
                    SQLStmt = "SELECT HACCCode, Description FROM DataDomains WHERE Domain = 'NSW-REFERRAL SOURCE'"
                else
                    SQLStmt = "SELECT HACCCode, Description FROM DataDomains WHERE Domain = 'REFERRAL SOURCE'"
                
                this.s_LookupText = "SELECT REFERRAL SOURCE"
            break;
        case "REFERRAL-OUT" :
                s_MDS = this.userSettings.prodDesc;
                if (s_MDS == "NSW" )
                    SQLStmt = "SELECT HACCCode, Description FROM DataDomains WHERE Domain = 'NSW-REFERRAL SOURCE'"
                else
                    SQLStmt = "SELECT HACCCode, Description FROM DataDomains WHERE Domain = 'REFERRAL SOURCE'"
               
                this.s_LookupText = "SELECT WHO REFERRING TO"
                break;
        case "ASSESSMENT" :
                //DetermineAdminExplanation = True
             break;  
        case "ADMISSION" :
               // DetermineAdminExplanation = True
               break;
        case "DISCHARGE" :
            this.s_LookupText = "SELECT REASON FOR CESSATION"
            //let  type:any= '';// Select Case UCase$(DBLookup(TraccsDb, "Type", "HumanResourceTypes", "Name = '" & s_Program & "'"))
            switch(type)
            {   
            case "CSTDA" :
                    SQLStmt = "SELECT DISTINCT HACCCode,Description FROM DataDomains WHERE Domain = 'CSTDA_CESSATION' AND Embedded = 1 AND DATASET = 'CSTDA' ORDER BY Description"
                break;
            case "HACC" :
                    SQLStmt = "SELECT DISTINCT HACCCode,Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' AND Embedded = 1 AND DATASET = 'HACC' ORDER BY Description"
                    break;
            case "NRCP" :
                    SQLStmt = "SELECT DISTINCT HACCCode,Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' AND Embedded = 1 AND DATASET = 'HACC' ORDER BY Description"
                    break;
            default:
                    SQLStmt = "SELECT DISTINCT HACCCode,Description FROM DataDomains WHERE Domain = 'REASONCESSSERVICE' AND Embedded = 0 ORDER BY Description"
                    break;
            }
        
        default:
            if (this.GroupShiftCategory=='TRANSPORT'){
                this.s_LookupText = "SELECT TRANSPORT REASON"
                SQLStmt = "SELECT DISTINCT HACCCode,Description FROM DataDomains WHERE Domain = 'TRANSPORTREASON' ORDER BY Description"
                break;
        
            }
           // DetermineAdminExplanation = True

           //this.listS.getlist(SQLStmt).pipe
            
    }

    if (SQLStmt==null) return;

    this.listS.getlist(SQLStmt).pipe(takeUntil(this.unsubscribe))
    .subscribe(data => {
        this.listAdminExplanation = data;
        this.showDetermineAdminExplanation=true;
        
    });
  
}

SelectedAdminExplainCode(s_Group:string, SelectedCode:any)
{
   
      let s_MDS:string='';    
      let AdminExplainCode  : any;
      let type:string=this.selectedProgram.type;
      let sql='';
      s_MDS=this.userSettings.prodDesc;
         switch(s_Group)
         {
           case  "REFERRAL-IN" :
           case  "REFERRAL-OUT":
                if (s_MDS == "NSW" )
                   sql=`select HACCCode from  DataDomains where Description = '${SelectedCode }' AND Domain = 'NSW-REFERRAL SOURCE'`;
                else
                sql=`select HACCCode from  DataDomains where Description = '${SelectedCode }' AND Domain = 'REFERRAL SOURCE'`;
                   //AdminExplainCode = '';//DBLookup(TraccsDb, "HACCCode", "DataDomains", "Description = '" & x_fLookupList.SelectedCode & "' AND Domain = 'REFERRAL SOURCE'")
                break;
            case "ASSESSMENT" :
            case "ADMISSION" :
            case "DISCHARGE" :
                switch (type) //UCase$(DBLookup(TraccsDb, "Type", "HumanResourceTypes", "Name = '" & s_Program & "'")))
                {
                   case "CSTDA" :
                            sql = `Select HACCCode from DataDomains Where Description = '${SelectedCode}' AND Domain = 'CSTDA_CESSATION'`;
                            break;
                        case "HACC" :
                            sql = `Select HACCCode from DataDomains Where Description = '${SelectedCode}' AND Domain = 'REASONCESSSERVICE'`;
                            break;
                        case "NRCP" :
                            sql = `Select HACCCode from DataDomains Where Description = '${SelectedCode}' AND Domain = 'REASONCESSSERVICE'`;
                            break;                            
                        default:
                            sql = `Select HACCCode from DataDomains Where Description = '${SelectedCode}' AND Domain = 'REASONCESSSERVICE'`;
                            break
                        }
                        
                    }
                    if (sql==null || sql=='') return;
                    this.listS.getlist(sql).pipe(takeUntil(this.unsubscribe))
                    .subscribe(data => {
                        this.haccCode = data[0].haccCode;
                        
                    });
                  
}

Pasting_Records(selected_Cell:any,sel:any){
    let self=this;
    let sheet=this.spreadsheet.getActiveSheet();
    
    var col = sel.col;
    var row = sel.row;

    let selected_columns = selected_Cell.col + selected_Cell.colCount;
                        let dt= new Date(self.date);        
                        let data_row=0;
                        let row_iterator=0;
                        let recdNo=0;
                        let breachStatus:boolean=false;
                        col=col-1;
                        for (let i=selected_Cell.col; i<selected_columns; i++)                     
                            {
                                data_row=selected_Cell.row;
                              
                                col=col+1;
                            
                                for ( row_iterator=0; row_iterator<=selected_Cell.rowCount; row_iterator++){
                                if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)==null ){
                                    data_row=data_row+1;
                                    continue;
                                }
                                                            
                                if (sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)!=null)
                                    self.copy_value=sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)
                                
                                if (self.copy_value.recordNo==recdNo){
                                    data_row=data_row+1;
                                    continue;
                                }
                                
                                recdNo=self.copy_value.recordNo;
                               // var cell_col_text=sheet.getValue(0,col,GC.Spread.Sheets.SheetArea.colHeader);
                              //  console.log(cell_col_text);
                               // var col_date= cell_col_text.substring(cell_col_text.length-2,cell_col_text.length);
                                let dt= sheet.getTag(0,col,GC.Spread.Sheets.SheetArea.colHeader);                       
                                let rdate = dt.getFullYear() + "/" + this.numStr(dt.getMonth()+1) + "/" + this.numStr(dt.getDate());
                          
                              //  let rdate = dt.getFullYear() + "/" + self.numStr(dt.getMonth()+1) + "/" + self.numStr(col_date.trim());
                                let n_row=row+row_iterator;
                                let rtime =  sheet.getCell(n_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();
                               
                               
                        
                                if (self.copy_value==null || self.copy_value.recordNo==null || self.copy_value.recordNo==0){
                                    continue;
                                }

                            
                                //if (self.copy_value.row>=0){
                                
                                self.draw_Cells(sheet,n_row,col,self.copy_value.duration,self.copy_value.type,self.copy_value.recordNo,self.copy_value.service)
                                self.isPaused=true;
                            
                                if (this.viewType=='Recipient'){
                                    this.current_roster=this.find_roster(recdNo);
                                    this.selectedCarer=this.current_roster.staffCode
                                }
                                                                                
                                if (self.operation==="cut"){
                                    self.ProcessRoster("Cut",self.copy_value.recordNo,rdate,rtime);
                                    self.remove_Cells(sheet,self.copy_value.row,self.copy_value.col,self.copy_value.duration)
                                }else
                                    self.ProcessRoster("Copy",self.copy_value.recordNo,rdate,rtime);    
                                
                                data_row=data_row+1;                 
                                }// rows loop
                                   
                            } // column loop
                        
                            this.upORdown.next(true);
                      
}
showConfirm(): void {
    //var deleteRoster = new this.deleteRoster();
    this.modalService.confirm({
      nzTitle: 'Confirm',
      nzContent: 'Are you sure you want to delete roster',
      nzOkText: 'Yes',
      nzCancelText: 'No',
      nzOnOk: () =>
      new Promise((resolve,reject) => {
        this.deleteRosterModal=false;
        setTimeout(Math.random() > 0.5 ? resolve : reject, 100);
        this.deleteRoster();
       
      }).catch(() => console.log('Oops errors!'))

      
    });
  }
    selectedDays(value: string[]): void {
        this.weekDay=value
        console.log(value);
      }

    updateCheckedSet(id: any, checked: boolean): void {
        if (checked) {
          this.setOfCheckedId.add(id);
        } else {
          this.setOfCheckedId.delete(id);
        }
      }
      refreshCheckedStatus(): void {
       
          //this.checked = listOfEnabledData.every(({ id }) => this.setOfCheckedId.has(id));
        //  this.indeterminate = listOfEnabledData.some(({ id }) => this.setOfCheckedId.has(id)) && !this.checked;
      }
      onItemChecked(id: any, checked: boolean): void {
        this.updateCheckedSet(id, checked);
        this.refreshCheckedStatus();
      }

    onItemSelected(sel: any, i:number, type:string): void {
            console.log(sel)
            
           
            if (type=="program"){      
                
                this.HighlightRow=i;   
                this.defaultProgram=sel;

                this.selectedProgram= this.programActivityList[i];
               
                this.bookingForm.patchValue({
                    program:sel
                });
                
               
            // if (sel != "" ){          
            //         GST = sel.GST
            //         GSTRate = sel.GSTRate            
            // }
        

            }else if (type=="service"){
                this.HighlightRow2=i;
                this.defaultActivity=sel;//this.serviceActivityList[i];
                // this.bookingForm.controls["serviceActivity"].valueChanges.subscribe(() => {
                //     console.log("modified");}, () => { console.log("error") }, () => { console.log("completed") });
        
                this.bookingForm.patchValue({
                    serviceActivity:sel
                });               
                            
                     this.DetermineAdminExplanation(this.defaultActivity.minorGroup, this.defaultProgram);
                
            }else if (type=="location"){
                this.HighlightRow3=i;
                this.serviceSetting=sel;
                this.bookingForm.patchValue({
                    serviceSetting:sel
                });
              
            }else if (type=="Category"){
                this.HighlightRow4=i;
                this.GroupShiftCategory=sel;
                this.recipientCode='!MULTIPLE'
                this.bookingForm.patchValue({
                    recipientCode:'!MULTIPLE'
                });
                
            }else if (type=="PayType"){
                this.HighlightRow5=i;
                if (this.haccCode=='' || this.haccCode==null)
                    this.haccCode=sel.haccCode
                this.bookingForm.patchValue({
                    payType:sel.title
                
                })
            
        } else if (type=="Admin"){
            this.HighlightRow5=i;
           
            this.SelectedAdminExplainCode (this.defaultActivity.minorGroup,sel.description);
           
            this.bookingForm.patchValue({
                haccType:this.defaultActivity.haccType
            
            })
         }
            
      
      }
     
    onItemDbClick(sel: any, i:number, type:string) : void {
        
        console.log(sel)
        this.HighlightRow=i;               

        if (type=="program"){          
            this.defaultProgram=sel;
            this.selectedProgram= this.programActivityList[i];
            this.dblclick=true;
            this.bookingForm.patchValue({
                 program:sel
             })
             
             this.GETSERVICEACTIVITY(sel).subscribe((d: Array<any>) => {
                this.serviceActivityList = d;
                this.next_tab();
                   setTimeout(() => {
                       this.bookingForm.patchValue({
                           serviceActivity: this.defaultActivity                         
                       });                   
                   }, 0);            
              
               if(d && d.length == 1){
                   this.bookingForm.patchValue({
                       serviceActivity: d[0]               
                      
                   });

                }
             });
                   
                
               
        }else if (type=="service"){
            this.defaultActivity=sel;
            
            this.bookingForm.patchValue({
                serviceActivity:sel
            })
              
            this.DetermineAdminExplanation(this.defaultActivity.minorGroup, this.defaultProgram);

        }else if (type=="location"){
            this.serviceSetting=sel;
            this.bookingForm.patchValue({
                serviceSetting:sel
            })
          
        }else if (type=="Category"){
            this.HighlightRow4=i;
            this.GroupShiftCategory=sel;
          
            this.recipientCode='!MULTIPLE'
            this.bookingForm.patchValue({
                recipientCode:'!MULTIPLE'
            });

            this.addGroupShift();
            return;
            
        } else if (type=="PayType"){
            this.HighlightRow5=i;
            if (this.haccCode=='' || this.haccCode==null)
                    this.haccCode=sel.haccCode
            this.bookingForm.patchValue({
                payType:sel.title
            })
            
        }else if (type=="Admin"){
            this.HighlightRow5=i;
           
            this.SelectedAdminExplainCode (this.defaultActivity.minorGroup,sel.description);
           
            this.bookingForm.patchValue({
                haccType:this.defaultActivity.haccType
            
            })

            this.showDetermineAdminExplanation=false;
         }
          
         if (this.showDone2 && this.rosterGroup!=""  ){
            this.addBookingModel=false;
            this.doneBooking();
            return;
        }
        
          if (this.current==1 && (this.activity_value==12 || this.booking_case==8)){
            this.next_tab();
          }else if(this.showDone2==false && type!="program" ){              
              this.next_tab();
          }
  
   
  }

  showRecipientStaffDetail(){

    this.selected_data = this.selected;
                          
      //this.router.navigate(['/roster/recipient-external']);
      //this.router.navigate(['/admin/recipient/intake'])
      if (this.viewType=='Staff'){
            this.staffexternal=true;
        // if (this._dynamicContainerStaffExternal==null)
        //     this.loadComponentAsync_Staff();
        // else{
        //    this._dynamicContainerStaffExternal.instance.AccountNo =this.selected_data;
        //    this._dynamicContainerStaffExternal.instance.isVisible=true;
       
        // }
       } else{
        this.recipientexternal=true;
            // if (this._dynamicContainerRecipientexternal==null)
            //      this.loadComponentAsync_Recipient();
            // else{
            //     this._dynamicContainerRecipientexternal.instance.AccountNo =this.selected_data;
            //     this._dynamicContainerRecipientexternal.instance.isVisible=true;
            
            // }
      }
       
  }

  onAllChecked(e:any){

  }

  addGroupShiftRecipients(){
    if (!this.recurrenceView)
        this.doneBooking();
    else
        this.cancel_GroupShift();

    
  }
  
  openStaffModal(){
    this.openSearchStaffModal=true;
    
    if (this.selectedOption!=null)
        this.booking = {
                     recipientCode: this.current_roster.recipientCode,
                     userName:this.token.user,
                     date:this.current_roster.date,
                     startTime:this.current_roster.startTime,
                     endTime:this.current_roster.endTime,
                     endLimit:'20:00'
                    };
     else  if (this.viewType=='Recipient')
        this.booking = {
            recipientCode: this.selected.data,
            userName:this.token.user,
            date:this.date ,
            startTime:format(this.defaultStartTime,'HH:mm'),
            endTime:format(this.defaultStartTime,'HH:mm'),
            endLimit:'20:00'
        };
             
    this.bookingData.next(this.booking) ;               
}
onStaffSearch(data:any){
    this.openSearchStaffModal=false;
    this.selectedCarer=data.accountno;
}

  addGroupShift(){
      this.showGroupShiftModal=false;
      this.addBooking(0);
      this.add_multi_roster=true;
  }
    ProcessRoster(Option:any, recordNo:string, rdate:string="", start_Time:string=""):any {
        
        
        let dt= new Date(this.date);
        
        let sheet = this.spreadsheet.getActiveSheet();
        let range = sheet.getSelections();
        let date = dt.getFullYear() + "/" + this.numStr(dt.getMonth()+1) + "/" + this.numStr(range[0].col+1);
        if (rdate!=""){
            date=rdate;
        }
        let f_row= range[0].row;
        let l_row=f_row+range[0].rowCount;
       // let startTime=sheet.getTag(f_row,0,GC.Spread.Sheets.SheetArea.viewport);
        let startTime =   sheet.getCell(f_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();
        let endTime =   sheet.getCell(l_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();
        if (start_Time!="")
            startTime=start_Time

        // if (this.master){

        //     startTime="00:00"
        // }
        var sels = sheet.getSelections();
        var sel = sels[0];
        
        let inputs = {
            "opsType": Option,
            "user": this.token.user,
            "recordNo": recordNo,
            "isMaster": this.master,
            "roster_Date" : date,
            "start_Time": startTime,
            "carer_code": this.viewType =="Staff" && Option!='Re-Allocate' ? this.recipient.data : this.selectedCarer,
            "recipient_code" : this.viewType =="Staff" ? "" : this.recipient.data,
            "notes" : this.notes,
            'clientCodes' : this.clientCodes
        }
        this.timeS.ProcessRoster(inputs).subscribe(data => {
        //if  (this.ClearMultiShiftModal==false &&  this.SetMultiShiftModal==false) 
             //  this.globalS.sToast('Success', 'Timesheet '  + Option + ' operation has been completed');
        //this.selectedCarer="";
        this.searchStaffModal=false;
        this.UnAllocateStaffModal=false;
        
        this.ClearMultiShiftModal=false;
        this.SetMultiShiftModal=false;
        this.ViewAdditionalModal=false;
        this.ViewServiceDetail=false;
        this.ViewStaffDetail=false;
        this.deleteRosterModal=false;

        let res=data;
        this.errorValue=0;

            if (res.errorValue>0){
                this.errorValue=res.errorValue;
                this.globalS.eToast('Error', res.errorValue +", "+ res.msg);
                if( Option=='Copy' || Option=='Cut' )
                    this.load_rosters();
                return; 
            } if( Option=='Copy' ||Option=='Cut' ){

                if (this.viewType=='Staff'){
                    this.current_roster = this.find_roster(parseInt(recordNo));
                    let clientCode =this.current_roster.recipientCode;
                    let date= this.current_roster.date

                    this.txtAlertSubject = 'NEW SHIFT ADDED : ' ;
                    this.txtAlertMessage = 'NEW SHIFT ADDED : \n' + date + ' : \n'  + clientCode + '\n'  ;
                    if (this.Error_Messages.length<=0)
                        this.show_alert=true;
                   
                }
            }else if (Option=='Re-Allocate' || Option=='Additional'){
                this.upORdown.next(true);
            }
          
    });
    
    
}

    find_roster(RecordNo:number):any{
        let rst:any;
      if(this.rosters==null){
          return null;
      }

    let r = this.rosters.filter(x=>x.recordNo==RecordNo)[0];
        
        rst = {
         
            "shiftbookNo": r.recordNo,
            "date": r.roster_Date,
            "startTime": r.start_Time,
            "endTime":    r.end_Time,
            "duration": r.duration,
            "durationNumber": r.dayNo,
            "recipient": r.clientCode,
            "program": r.program,
            "activity": r.serviceType,
            "payType": r.payType,   
            "paytype": r.payType.paytype,  
            "pay": r.pay,                   
            "bill": r.bill,            
            "approved": r.Approved,
            "billto": r.billedTo,
            "debtor": r.billedTo,
            "notes": r.notes,
            "selected": false,
            "serviceType": r.type,
            "recipientCode": r.clientCode,            
            "staffCode": r.carerCode,  
            "serviceActivity": r.serviceType,
            "serviceSetting": r.serviceSetting,
            "analysisCode": r.anal,
            "serviceTypePortal": "",
            "status": r.status,
            "recordNo": r.recordNo
            
        }
            
        
    
    return rst;
}   

find_last_roster(date: any, startTime:any):any{
    let rst:any;
    for(var r of this.rosters)
   {
            if (r.roster_Date == date && r.start_Time==startTime ){
                rst= r;
                break;
            }
        
    } 
    
    rst = {
        "shiftbookNo": r.recordNo,
        "date": r.roster_Date,
        "startTime": r.start_Time,
        "endTime":    r.end_Time,
        "duration": r.duration,
        "durationNumber": r.dayNo,
        "recipient": r.clientCode,
        "program": r.program,
        "activity": r.serviceType,
        "payType": r.payType,   
        "paytype": r.payType.paytype,  
        "pay": r.pay,                   
        "bill": r.bill,            
        "approved": r.Approved,
        "billto": r.billedTo,
        "debtor": r.billedTo,
        "notes": r.notes,
        "selected": false,
        "serviceType": r.type,
        "recipientCode": r.clientCode,            
        "serviceActivity": r.serviceType,
        "serviceSetting": r.serviceSetting,
        "analysisCode": r.anal,
        "serviceTypePortal": "",
        "recordNo": r.recordNo
        
    }
        
    

return rst;
}   
    showRecipient(): boolean  {
        const { serviceType, isMultipleRecipient, isTravelTimeChargeable } = this.rosterForm.value;
        // console.log(serviceType + '' + isTravelTimeChargeable)

        if(serviceType === 'TRAVELTIME' && isTravelTimeChargeable){
            return true;
        }       


        if(((serviceType !== 'ADMINISTRATION' && serviceType !== 'ALLOWANCE NON-CHARGEABLE' && serviceType !=='TRAVELTIME') && !isMultipleRecipient)){
            return true;
        }

        return false;
    }

    get showTime(): boolean {
        const { serviceType } = this.rosterForm.value;
        if(serviceType === 'ALLOWANCE CHARGEABLE' || serviceType === 'ALLOWANCE NON-CHARGEABLE')
            return false;

        return true;
    }

    get showEndTime(): boolean{
        const { serviceType } = this.rosterForm.value;
        if(serviceType === 'SLEEPOVER'){
            return false;
        }
        return true;
    }

      
    // onRightClick(e){
     
    //     e.default=false;
    //     this.optionsModal=true;
        
    //     console.log(this.optionsModal);
    //     return false;
    // }

    // handleClick(e){
    //     console.log(e);
    //      this.addTimesheetVisible = true;
    //      this.current_roster = this.rosters[1];
    //     this.details(this.current_roster);
        
      //  this.optionsModal=true;
       // console.log(this.optionsModal);
    //}
   
    add_Shift(){
        this.Timesheet_label = "Add Timesheet " 
        this.whatProcess = PROCESS.ADD;
        this.addTimesheetVisible = true;
       // this.resetAddTimesheetModal();
        this.AddViewRosterDetails.next(2);
        let sheet = this.spreadsheet.getActiveSheet();
        var range=sheet.getSelections();
        // console.log(range)
        let dt= new Date(this.date);
        //let dt = moment.utc(this.date).local();
        let f_row= range[0].row;
        let l_row=f_row+range[0].rowCount;        
        let col=range[0].col;
       
        let date = sheet.getCell(0,col,GC.Spread.Sheets.SheetArea.colHeader).tag();
       //  date = dt.getFullYear() + "/" + this.numStr(dt.getMonth()+1) + "/" + this.numStr(range[0].col+1);
        date = format(date, 'yyyy/MM/dd');

         let startTime =   sheet.getCell(f_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();
         let endTime =   sheet.getCell(l_row,0,GC.Spread.Sheets.SheetArea.rowHeader).tag();

        // let startTime=sheet.getTag(f_row,0,GC.Spread.Sheets.SheetArea.viewport);
        // let endTime =sheet.getTag(l_row,-1,GC.Spread.Sheets.SheetArea.viewport);

        this.defaultStartTime = parseISO(new Date(date + " " + startTime).toISOString());
        this.defaultEndTime = parseISO(new Date(date + " " + endTime).toISOString());
        this.durationObject = this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime);
        this.current = 0;
        
        this.date = parseISO(this.datepipe.transform(date, 'yyyy-MM-dd'));
        this.rosterForm.patchValue({date:date})

        //this.date = parseISO(new Date(date).toISOString());
        
    }
    mySub: Subscription;
    currentDate: string;

    dateStream = new Subject<any>();
    userStream = new Subject<string>();

    date:any = moment();
    CalendarDate:Date;
    options: any;
    recipient: any;
    serviceType:any;
    recipientCode:string;
    loading: boolean = true;
    formloading: boolean = false;
    processing: boolean = false;
    basic: boolean = false;
    Select_Pay_Type:string="Select Pay Type"
   // data: any;
    add_multi_roster:boolean=false;
    private unsubscribe = new Subject();
  //  private rosters: Array<any>;
    //private current_roster: any;
     upORdown = new Subject<boolean>();

     modalDeActivate: NzModalRef;

    constructor(
        private router: Router,
        private staffS: StaffService,
        private timeS: TimeSheetService,
        private globalS: GlobalService,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private formBuilder: FormBuilder,
        private clientS: ClientService,
        private listS: ListService,
        public datepipe: DatePipe,
        private sharedS: ShareService,
        private route: ActivatedRoute,
        private renderer2: Renderer2,
        private elementRef: ElementRef,
        private componentFactoryResolver: ComponentFactoryResolver,
        private location: ViewContainerRef,
        private xero:XeroService,
        private modal: NzModalService,
        private breakpointObserver: BreakpointObserver
      
    ) {
        this.initializeGrid();

        if (this.router.getCurrentNavigation()==null) return;
        const navigation = this.router.getCurrentNavigation();
        const state = navigation.extras.state as {
            StaffCode: string,
            ViewType: string,
            IsMaster: boolean
        };

        if (state!=null){       
            this.info = {StaffCode:state.StaffCode, ViewType:state.ViewType, IsMaster:state.IsMaster,date:''} ;

         }
        
   
        this.currentDate = format(new Date(), 'yyyy/MM/dd');

        this.dateStream.pipe(
            distinctUntilChanged(),          
            takeUntil(this.unsubscribe),)
            .subscribe(data =>{
                this.searchRoster(this.date);
            });

        this.userStream.pipe(
            distinctUntilChanged(),
            debounceTime(500),
            takeUntil(this.unsubscribe))
            .subscribe(data =>{
                this.loading = true;
                this.recipient = data;
                this.searchRoster(this.date);
            });

        this.upORdown.pipe(debounceTime(300))
            .subscribe(data => {
                this.loading = true;                         
                this.searchRoster(this.date);          
            });

            this.AddViewRosterDetails.subscribe(data => {
                // console.log(data);
                this.user = {
                   // name: this.selectedOption.Recipient,
                   // code: this.selectedOption.uniqueid,
                    startDate: '2019/01/15',
                    endDate: '2019/01/29'     
                                 
                    
                }   
                console.log(data);         
                
                this.addTimesheetVisible = true;
                this.optionsModal=false;  
            });
            this.changeViewRosterDetails.subscribe(data => {
                // console.log(data);
                this.user = {
                   // name: this.selectedOption.Recipient,
                   // code: this.selectedOption.uniqueid,
                    startDate: '2019/01/15',
                    endDate: '2019/01/29'     
                              
                    
                }   
                console.log(data);         
                this.details(data) ;
                this.addTimesheetVisible = true;
                this.optionsModal=false;  
            });
            this.changeViewRecipientDetails.subscribe(data => {
                // console.log(data);
                this.user = {
                    //name: this.selectedOption.Recipient,
                  //  code: this.selectedOption.uniqueid,
                    startDate: '2019/01/15',
                    endDate: '2019/01/29'
                }            
                // this.tabvrd = data;
                console.log(data);
                this.recipientDetailsModal = true;
                this.optionsModal=false;  
            });
    
           
    
            this.changeModalView.subscribe(data => {
                console.log(data);
            });
        
    }

    

    async loadComponentAsync(index:any) {
        const {ShiftDetail} = 
          await import('../roster/shiftdetail');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(ShiftDetail);
        
        // create and inject component into dynamicContainer
        this._dynamicInstance = 
          this.dynamicContainer.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
       
        
         this._dynamicInstance.instance.timesheetDone.subscribe(data=>{
            this.shiftChanged(data);
         });
         this._dynamicInstance.instance.data =index;
         this._dynamicInstance.instance.isVisible=true;
         this._dynamicInstance.instance.viewType =this.viewType;
         this._dynamicInstance.instance.editRecord=false;
        // this._dynamicInstance.instance.ngAfterViewInit();
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      
      async loadComponentAsync_Recipient() {
        const {RecipientExternal} = 
          await import('../roster/RecipientExternal');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(RecipientExternal);
        
        // create and inject component into dynamicContainer
        this._dynamicContainerRecipientexternal = 
          this.dynamicContainerRecipientexternal.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
        this.recipientexternal = true;
        
         this._dynamicContainerRecipientexternal.instance.recipientexternalDone.subscribe(data=>{
            this.cancelRecipientExternal(data);
         });
         this._dynamicContainerRecipientexternal.instance.AccountNo =this.selected_data;
         this._dynamicContainerRecipientexternal.instance.isVisible=true;
         this._dynamicContainerRecipientexternal.instance.load_data();
         
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }
      async loadComponentAsync_Staff() {
        const {StaffExternal} = 
          await import('../roster/StaffExternal');
        
        // create component factory for DynamicComponent
        const dynamicComponentFactory = 
          this.componentFactoryResolver.resolveComponentFactory(StaffExternal);
        
        // create and inject component into dynamicContainer
        this._dynamicContainerStaffExternal = 
          this.dynamicContainerStaffExternal.createComponent(dynamicComponentFactory, this.location.length, null);
       
        this.optionsModal=false;
        this.staffexternal = true;
        
         this._dynamicContainerStaffExternal.instance.staffexternalDone.subscribe(data=>{
            this.cancelStaffExternal(data);
         });
         this._dynamicContainerStaffExternal.instance.AccountNo =this.selected_data;
         this._dynamicContainerStaffExternal.instance.isVisible=true;
        
        //this._dynamicInstance.changeDetectorRef.detectChanges();
      }

    whatProcess = PROCESS.ADD;

    details(index: any){
        this.loadComponentAsync(index);
        // this.detail.isVisible=true;
        //    this.detail.data=index;
        //    this.detail.viewType =this.viewType
        //    this.detail.editRecord=false;
        //    this.detail.ngAfterViewInit();
          // this.detail.details(index);
        return;
    }

    
    @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
          this.screenHeight = window.innerHeight;
          this.screenWidth = window.innerWidth;
          console.log('onReszie = ' + this.screenHeight, this.screenWidth);
          this.hostStyle = {
            width: '85vw',
            height: '82vh',
           
        };
          if (this.screenWidth<=600 ) {
            this.Days_View=1;
          }else if (this.screenWidth>=600 && this.screenWidth<=900) {
            this.Days_View=5;
           
          }else if (this.screenWidth>=900 && this.screenWidth<=1200) {
              this.Days_View=7;
            
          }else if (this.screenWidth>1200){
                this.Days_View=14;
               
          }
          this.prepare_Sheet();
          this.load_rosters();
    }


    ngOnInit(): void {
        this.rosterFormGroup = this.formBuilder.group({ });

    //    this.token = this.globalS.decode(); 
        this.token = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
    
        this.timeS.getuserpermissionsettings(this.token.user).pipe(takeUntil(this.unsubscribe))
        .subscribe(data => {
            this.userSettings=data;
            console.log(this.userSettings);
        });
      

        //console.log(this.token);
        if (this.token==null){

            this.globalS.eToast("Authentication","Invalid  User, please login again");
            
            this.router.navigate(['/']);
            return;

            
        }
     
        GC.Spread.Sheets.LicenseKey = license;

        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
      //  console.log('Screen Size =' + this.screenHeight + ', '+ this.screenWidth)
    //   if (!this.fixSize){
    //     this.hostStyle = {
    //         width: '100%',
    //         height: `${this.screenHeight- 170}px`,
    //         overflow: 'hidden',
    //         float: 'left'
    //     };
        
    //     if (this.spreadsheet!=null){
    //         this.spreadsheet.getHost().style.width = "100%";
    //         this.spreadsheet.getHost().style.height = window.innerHeight;
              
    //       }
    //  }
       
      
        this.date = moment();
     
        this.AddTime();
        this.buildForm(); 
          
         this.tval=96;
         this.dval=14;
     
         this.reloadRoster.subscribe(d=>{
            
             this.info=d;
             this.defaultCode=this.info.StaffCode;
             this.viewType=this.info.ViewType;
             let data ={data:this.defaultCode, option:this.viewType}  

            if (this.viewType=='Recipient'){
                data.option="1"
                this.view=1;
            }else{
                data.option="0"
                this.view=0;
            }
               
            if (this.info.date!=''){             
                let dt=new Date(this.info.date);
                this.date=dt;
                let dat = this.date;// moment(this.date).add('day', 1);
                this.startRoster = moment(dat).format('YYYY/MM/DD');
                this.endRoster = moment(dat).format('YYYY/MM/DD');
                this.reloadSearchList.next({defautValue:this.defaultCode, view:this.view});
                
                this.staffS.getroster({
                    RosterType: data.option == '1' ? 'PORTAL CLIENT' : 'SERVICE PROVIDER',            
                    AccountNo: this.defaultCode,
                    StartDate: this.startRoster,
                    EndDate: this.endRoster
                }).pipe(takeUntil(this.unsubscribe)).subscribe(roster => {
        
                    this.rosters = roster;
                });    
            }else{
                let dt:Date= new Date();
                this.date= moment(dt).format('YYYY/MM/DD');
                this.startRoster=null;
                this.reloadSearchList.next({defautValue:this.defaultCode, view:this.view});
                this.ngAfterViewInit();
          }  //this.onChange(dt);

        
        })
     
        this.reloadMenu.subscribe(data=>{
               
                this.optionsModal=true;
        });
            
        this.menuItems.subscribe(data=>{
            this.optionsModal=false;
            switch(data){
                case 1:
                    this.spreadsheet.commandManager().execute({cmd: "Copy", sheetName: this.sheetName, row: 1, col: 2, backColor: "red", index: 3, count: 5});
     
                break;
                case 2:
                    this.spreadsheet.commandManager().execute({cmd: "Cut", sheetName: this.sheetName, row: 1, col: 2, backColor: "red", index: 3, count: 5});
     
                break;
                case 3:
                    this.spreadsheet.commandManager().execute({cmd: "Paste", sheetName: this.sheetName, row: 1, col: 2, backColor: "red", index: 3, count: 5});
     
                break;
            }
    });

    this.breakpointObserver.observe(['(min-width: 300px) and (max-width: 500px)']).subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.Days_View = 5;
      } else {
        this.Days_View = 14;
      }
    });

    
         
    }


ngAfterViewInit(){
  this.formloading=true;
    console.log("ngAfterViewInit");   
    this.CalendarDate=  new Date(this.date);

    if (this.info.StaffCode!='' && this.info.StaffCode!=null){
        this.defaultCode=this.info.StaffCode
        this.viewType=this.info.ViewType
        this.master=this.info.IsMaster
        let data ={data:this.defaultCode, option:this.viewType}   
       
        if (this.viewType=='Recipient'){
            data.option="1"
            this.view=1;
        }else{
            data.option="0"
            this.view=0;
        }
            
         this.picked(data);
         //this.searchRoster(this.date);
    }

    // const navbarToggler =
    //   this.elementRef.nativeElement.querySelector('divroster');
    // navbarToggler.addEventListener('click', () => {
    //   navbarToggler.classList.toggle('class');
    //   console.log('Even Test')
    // });
   
}

onChange(result: Date): void {
 
    console.log('Selected Time: ', result);
    this.date = moment(result).format('YYYY/MM/DD');    
    this.startRoster=this.date;
    let dt = moment(this.startRoster).add('day', this.Days_View-1);
    this.endRoster = moment(dt).format('YYYY/MM/DD');
    this.prepare_Sheet();
    this.picked(this.selected);
    this.upORdown.next(true);
  }

 
   
ngOnDestroy(){
    console.log("ngDestroy");
    //window.location.reload();  
}

refreshPage() {
    //this._document.defaultView.location.reload();
    window.location.reload();
    //this.ngOnInit();
   
  }
reloadVal: boolean = false;
reload(reload: boolean){
    
    this.reloadVal = !this.reloadVal;

}

    public clickStream;
    private clicks = 0;

    DETERMINE_SERVICE_TYPE(index: any): any{
        console.log(index);
        const { serviceType, debtor } = index;

        // ALLOWANCE NON CHARGEABLE 
        if(serviceType == 9 && debtor == '!INTERNAL'){
            return 'ALLOWANCE NON-CHARGEABLE';
            //return this.modalTimesheetValues[2];
        }

        // ALLOWANCE CHARGEANLE 
        if(serviceType == 9 && debtor != '!INTERNAL'){
            return 'ALLOWANCE CHARGEABLE';
        }

        // ADMINISTRATION
        if(serviceType == 6){            
            return 'ADMINISTRATION';
        }

        // CASE MANAGEMENT
        if(serviceType == 7){
            return 'CASE MANAGEMENT';
        }

        // ITEM
        if(serviceType == 15){
            return 'ITEM';
        }

        // SLEEPOVER
        if(serviceType == 8){

            return 'SLEEPOVER';
        }

        // TRAVEL TIME
        if(serviceType == 5){

            return 'TRAVELTIME';
        }

        //SERVICE
        return 'SERVICE';

     }

     DETERMINE_SERVICE_TYPE_NUMBER(index: string): number{
        // ALLOWANCE NON CHARGEABLE 
        
        if(index == "ONEONONE")
                 return 2
        else if(index ==  "ADMINISTRATION"){
                 if (this.defaultActivity == "UNAVAILABLE" )
                     return 13
                 else
                     return 6                 
                 }
        else if(index ==  "ADMISSION")
                 return 7
        else if(index ==  "ALLOWANCE")
                 return 9;
        else if (index ==   "CENTREBASED")
                 return 11;
         else if (index ==  "GROUPACTIVITY")
                 return 12;
         else if (index ==  "RECPTABSENCE")
                 return 4;
         else if (index ==  "SLEEPOVER")
                 return 8;
         else if (index ==  "TRANSPORT")
                 return 10;
         else if (index ==  "TRAVELTIME")
                 return 5;
         else if (index ==  "UNAVAILABLE")
                 return 13;
         else if (index ==  "ITEM")
                 return 14;
        else
                return 2;

                 
     }

selected_person(event:any){
    

    this.picked(event);
    setTimeout(() => {
        this.prepare_Sheet();
      }, 1000);

       
     

}
picked(data: any) {

   

    if (data==null){       
        return;
    }
        console.log(data);
        this.userStream.next(data);

        if (!data.data) {
            this.rosters = [];
            this.selected = null;
            this.enable_buttons=false;
            return;
        }
        //this.prepare_Sheet(this.spreadsheet);
      
        this.selected = data;
        if (this.master){           
            this.set_Cycle_Dates();
        }else if (this.startRoster==null){
            
            this.startRoster= moment(this.date).startOf('month').format('YYYY/MM/DD')
            if (this.Days_View>=28)
                this.endRoster= moment(this.date).endOf('month').format('YYYY/MM/DD')
            else{
                this.date = moment(this.startRoster).add('day', this.Days_View-1);
                this.endRoster = moment(this.date).format('YYYY/MM/DD');
                this.date= this.startRoster;
               
                
            }
        }  
        this.viewType = this.whatType(data.option);
        
        this.loading = true;
        this.enable_buttons=true;

        if (this.picked$) {
            this.picked$.unsubscribe();
        }

        if(this.viewType == 'Recipient'){
         //   this.rosterForm.patchValue ({recipientCode:data.data});
            this.bookingForm.patchValue ({recipientCode:data.data});

            this.clientS.getagencydefinedgroup(this.selected.data)
                .subscribe(data => {
                    this.agencyDefinedGroup = data.data;
                });
        } 
        if(this.viewType == 'Staff')
            this.getPublicHolidyas(data.data);
        
        this.processing=true;
        this.picked$ = this.timeS.gettimesheets({
            AccountNo: data.data,            
            personType: this.viewType,
            startDate: moment(this.startRoster).format('YYYY/MM/DD'),
            endDate: moment(this.endRoster).format('YYYY/MM/DD'),
        }).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
                console.log(data);
                this.loading = false;
              //  this.selected=data;
                this.rosters = data.map(x => {
                    return {
                        shiftbookNo: x.shiftbookNo,
                        recordNo: x.shiftbookNo,
                        date: x.activityDate,
                        startTime: this.fixDateTime(x.activityDate, x.activity_Time.start_time),
                        endTime: this.fixDateTime(x.activityDate, x.activity_Time.end_Time),
                        duration: x.activity_Time.calculated_Duration,                                         
                        durationNumber: x.activity_Time.duration,
                        recipient: x.recipientLocation,
                        program: x.program.title,
                        activity: x.activity.name,
                        paytype: x.payType.paytype,                     
                        payquant: x.pay.quantity,
                        payrate: x.pay.pay_Rate,
                        billquant: x.bill.quantity,
                        billrate: x.bill.bill_Rate,
                        approved: x.approved,
                        billto: x.billedTo.accountNo,
                        notes: x.note,
                        selected: false,
                        serviceType: x.roster_Type,
                        recipientCode: x.recipient_staff.accountNo,
                        debtor: x.billedTo.accountNo,
                        serviceActivity: x.activity.name,
                        serviceSetting: x.recipientLocation,                        
                        type:x.type,
                        bill:x.bill,
                        pay:x.pay,
                        dayNo: x.dayNo,
                        monthNo: x.monthNo,
                        yearNo: x.yearNo,
                        analysisCode: x.anal

                    }
                   
                });
                //this.setCalanderView();
                //console.log(this.timesheets);
                this.processing=false;
            });
        
       // this.getComputedPay(data).subscribe(x => this.computeHoursAndPay(x));
       this.selectAll = false;
     

        
    }
    setCalanderView(){

        if (this.master) return;
        if (this.Days_View>=28){
            this.upORdown.next(true);
            return;
        }

            let diff = Math.floor(this.today.getDate()/(this.Days_View));

            if (diff<=0)
                diff=1;
            
            this.startRoster =moment(this.today).startOf('month').format('YYYY/MM/DD')           
            this.date = new Date (this.startRoster);             
            this.date = moment(this.date).add('day', this.Days_View*diff);            
           // this.startRoster  = moment(this.date).format('YYYY/MM/DD');
            //this.date = moment(this.date).add('day', this.Days_View-1);
            this.endRoster = moment(this.date).format('YYYY/MM/DD');

            this.date = new Date (this.startRoster);
            this.CalendarDate=new Date(this.date);
            this.prepare_Sheet();  
            this.upORdown.next(true);
       
       //else if (this.today.getDate()>this.Days_View)
        //this.next_date();
    }

    eventRender(e: any){
        e.el.querySelectorAll('.fc-title')[0].innerHTML = e.el.querySelectorAll('.fc-title')[0].innerText;
    }
    computeHoursAndPay(compute: any): void{
        var hourMinStr;

        if (compute.workedHours && compute.workedHours > 0) {
            const hours = Math.floor(compute.workedHours * 60 / 60);
            const minutes = ('0' + compute.workedHours * 60 % 60).slice(-2);

            hourMinStr = `${hours}:${minutes}`
        }

        var _temp = {
            KMAllowancesQty: compute.kmAllowancesQty || 0,
            AllowanceQty: compute.allowanceQty || 0,
            WorkedHours: compute.workedHours || 0,
            PaidAsHours: compute.paidAsHours || 0,
            PaidAsServices: compute.paidAsServices || 0,
            WorkedAttributableHours: compute.workedAttributableHours || 0,
            PaidQty: compute.paidQty || 0,
            PaidAmount: compute.paidAmount || 0,
            ProvidedHours: compute.providedHours || 0,
            BilledAsHours: compute.billedAsHours || 0,
            BilledAsServices: compute.billedAsServices || 0,
            BilledQty: compute.billedQty || 0,
            BilledAmount: compute.billedAmount || 0,
            HoursAndMinutes: hourMinStr
        };

        this.payTotal = _temp;
    }
    getComputedPay(data: any = this.selected): Observable<any>{
        return this.timeS.getcomputetimesheet({
            AccountName: data.data,
            IsCarerCode: this.viewType == 'Staff' ? true : false
        });
    }

    fixDateTime(date: string, timedate: string) {
        var currentDate = parseISO(date);
        var currentTime = parseISO(timedate);

        var newDate = format(
            new Date(
                currentDate.getFullYear(),
                currentDate.getMonth(),
                currentDate.getDate(),
                currentTime.getHours(),
                currentTime.getMinutes(),
                currentTime.getSeconds()
            ), "yyyy-MM-dd'T'hh:mm:ss");
        

        return newDate;
    }
    searchRoster(date: any): void{

       if (this.startRoster==null){                 
            this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
            this.endRoster =moment(this.date).endOf('month').format('YYYY/MM/DD')
            this.date= this.endRoster;
       }
       if(!this.recipient) return;
        
           this.ActiveCellText="";     
         
      this.staffS.getroster({
            RosterType: this.recipient.option == '1' ? 'PORTAL CLIENT' : 'SERVICE PROVIDER',            
            AccountNo: this.recipient.data,
            StartDate: this.startRoster,
            EndDate: this.endRoster
        }).pipe(takeUntil(this.unsubscribe)).subscribe(roster => {

            this.rosters = roster;
            console.log(roster)

                this.options = null;
                var events = roster.map(x => {
                    return {
                        id: x.recordNo,
                        raw: `<b class="title" data-toggle="tooltip" data-placement="top" title="${ x.serviceType }">${ x.carerCode }</b>`,
                        start: `${ moment(x.shift_Start).format("YYYY-MM-DD HH:mm:00") }`,
                        end: `${ this.detectMidNight(x) }`
                    }
                });
                
                var time = events.map(x => x.start);
                var timeStart = moment(this.globalS.getEarliestTime(time)).subtract(20,'m').format('hh:mm:ss');             

                if(timeStart != null){
                    this.options = {
                        show: true,
                        scrollTime:  timeStart,
                        events: events
                    }
                }
                else {
                    this.options = {
                        show: true,
                        scrollTime:  '00:00:00',
                        events: events
                    }
                }
                this.load_rosters();
                this.loading = false;

               
               
               // this.globalS.sToast('Roster Notifs',`There are ${(this.options.events).length} rosters found!`)
               
        });
    }

    next_date(){
        if (this.master){
            if (this.masterCycleNo>10 )
            this.masterCycleNo=1
            else
                this.masterCycleNo=this.masterCycleNo+1;
            this.masterCycle = "CYCLE " + this.masterCycleNo
            if (this.master)
            this.set_Cycle_Dates();
           // this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
           // this.endRoster =moment(this.date).endOf('month').format('YYYY/MM/DD')

        }else if(this.Days_View==31 || this.Days_View==30){
            this.date = moment(this.date).add('month', 1);       
            this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
            this.endRoster =moment(this.date).endOf('month').format('YYYY/MM/DD')
        }else{
             this.date = new Date (this.endRoster);             
             this.date = moment(this.date).add('day', 1);            
             this.startRoster  = moment(this.date).format('YYYY/MM/DD');
             this.date = moment(this.date).add('day', this.Days_View-1);
             this.endRoster = moment(this.date).format('YYYY/MM/DD');             
           }

           this.CalendarDate=new Date(this.date);
           this.prepare_Sheet();    
           this.upORdown.next(true);
    }

    set_Cycle_Dates(){
        switch(this.masterCycleNo){
            case 1:
                this.startRoster= "1900/01/01"
                this.endRoster= "1900/01/07"
            break;
            case 2:
                this.startRoster= "1900/01/08"
                this.endRoster= "1900/01/14"
            break;
            case 3:
                this.startRoster= "1900/01/15"
                this.endRoster= "1900/01/23"
            break;
            case 4:
                this.startRoster= "1900/01/24"
                this.endRoster= "1900/01/31"
            break;
            default:
                this.startRoster= "1900/01/01"
                this.endRoster= "1900/01/31"
            }
    }

    prev_date(){
        
            if (this.master){
               
                if (this.masterCycleNo<=1 )
                    this.masterCycleNo=1
                else
                    this.masterCycleNo=this.masterCycleNo-1;
                this.masterCycle = "CYCLE " + this.masterCycleNo
               
                if (this.master)
                 this.set_Cycle_Dates();

            } else if(this.Days_View==31 || this.Days_View==30){
                this.date = moment(this.date).subtract('month', 1);       
                this.startRoster =moment(this.date).startOf('month').format('YYYY/MM/DD')
                this.endRoster =moment(this.date).endOf('month').format('YYYY/MM/DD')
            }else{
                 this.date = new Date (this.startRoster);             
                 this.date = moment(this.date).subtract('day', 1);                             
                 this.endRoster  = moment(this.date).format('YYYY/MM/DD');
                 this.date = moment(this.date).subtract('day', this.Days_View-1);
                 this.startRoster = moment(this.date).format('YYYY/MM/DD');
                 
               }
               this.CalendarDate=new Date(this.date);
               this.prepare_Sheet();
                this.upORdown.next(false);
    }

   
    detectMidNight(data: any){
        if(Date.parse(data.shift_Start) >= Date.parse(data.shift_End)){
            return moment(data.shift_End).format("YYYY-MM-DD 24:00:00");
        }
        return moment(data.shift_End).format("YYYY-MM-DD HH:mm:00");
    }

    handleDateClick({ event }){
        console.log(event);
        this.basic = !this.basic;
        this.data = this.search(this.rosters, 'recordNo', event.id);
    }

    eventMouseEnter(event){     
        $(event.jsEvent.target).closest('a').css({'cursor':'pointer','background-color':'#4396e8'})
    }

    eventMouseLeave(event){
        $(event.jsEvent.target).closest('a').css({'cursor':'pointer','background-color':'#3788d8'})
    }

    search(arr: Array<any>, key: string, name: any): any{
        return arr.find(o => o[key] === name);
    }

    eventDragStart(event){
        console.log(event)
    }

    eventDrop(event){
        console.log(event.event)
        console.log(event.oldEvent)
    } 

    eventRightClick(data: any){
        console.log(data);
        this.optionsModal=true;
    }

    handleCancel(){
            this.addTimesheetVisible=false;
            this.addBookingModel=false;
            this.ShowCentral_Location=false;
            this.testComp=false;
            this.cancelTrigger = true;

            if(this.modalDeActivate)
                this.modalDeActivate.close();

            if (this.viewType=="Staff" && this.recurrenceView && this.IsGroupShift && this.showGroupShiftRecipient==false){
                this.addBookingModel=false;
                this.get_group_Shift_Setting()    
                return;        
            }   
    }
    handleOk(){
        this.isConfirmLoading = true;
        setTimeout(() => {
          this.isVisible = false;
          this.isConfirmLoading = false;
        }, 1000);
    }

    AddMultiShiftRosters(){

        let rdate = format(this.date, 'yyyy/MM/dd');
        let time= format(this.defaultStartTime,'HH:mm')

       // this.current_roster= this.find_last_roster(rdate ,time)
        //console.log(lroster)

        this.clientCodes="-"
        this.setOfCheckedId.forEach(element => {
            this.clientCodes = this.clientCodes + "," + element;
        });  
        
       // this.setOfCheckedId.clear();

        this.ProcessRoster("GroupShift",this.NRecordNo);

            
    }

    AddRecurrentRosters(){
        let sdate:any;
        let edate:any;
        this.clientCodes="-"
        if (this.add_multi_roster){
            this.setOfCheckedId.forEach(element => {
                this.clientCodes = this.clientCodes + "," + element;
            });  
        }
        if (this.master){
             sdate=this.recurrentStartDate;
             edate=this.recurrentEndDate;
   
        }else{

             sdate=this.recurrentStartDate.getFullYear() + '/' +  this.numStr(this.recurrentStartDate.getMonth()+1) +'/' + this.numStr(this.recurrentStartDate.getDate());
             edate=this.recurrentEndDate.getFullYear() + '/' +  this.numStr(this.recurrentEndDate.getMonth()+1) +'/' + this.numStr(this.recurrentEndDate.getDate());
        }
        
        let stime=  parseISO(new Date(this.recurrentStartTime).toISOString());
        let starttime =  format(stime,'HH:mm');

        //this.current_roster= this.find_last_roster(sdate ,starttime)
        let recordNo=this.NRecordNo
        //this.ProcessRoster("RecurrentRoster",this.current_roster.recordNo)      
       
        if (this.Frequency!="Monthly" || this.Pattern==null)
            this.Pattern="All";
        let inputs={            
            "recordNo": recordNo,
            "startDate" : sdate,
            "endDate":edate,
            "days":this.weekDay,
            "frequency":this.Frequency,
            "pattern" : this.Pattern,
            "clientCodes" : this.clientCodes
        }
        this.timeS.addRecurrentRosters(inputs).subscribe(data => {
        
              // this.globalS.sToast('Success', 'Recurrent Roater added successfully');
               this.searchRoster(sdate)
      
    });
}

    GETPROGRAMS(type: string, s_EarliestDate:any): Observable<any> {

        
        let sql;
        if (!type) return EMPTY;
        //const { isMultipleRecipient } = this.bookingForm.value;
        if ( this.IsGroupShift && this.GroupShiftCategory=="TRANSPORT" ) {
            sql = `SELECT DISTINCT [Name] AS ProgName FROM HumanResourceTypes pr WHERE [group] = 'PROGRAMS' AND  (ISNULL(pr.CloseDate, '2000/01/01') < '${s_EarliestDate}') AND (EndDate Is Null OR EndDate >= '${this.currentDate}')  ORDER BY [ProgName]`;
            //
           // return this.listS.getlist(`SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'VEHICLES' AND Description Is NOT Null  AND (EndDate Is Null OR EndDate >= (select top 1 PayPeriodEndDate from systable)) ORDER BY DESCRIPTION`);
        }else if (this.GroupShiftCategory=="GROUPBASED" || this.GroupShiftCategory=='GROUPACTIVITY')   {         
        
            sql = `SELECT DISTINCT [Name] AS ProgName FROM HumanResourceTypes pr WHERE [group] = 'PROGRAMS' AND  (ISNULL(pr.CloseDate, '2000/01/01') < '${s_EarliestDate}') AND (EndDate Is Null OR EndDate >= '${this.currentDate}')  ORDER BY [ProgName]`;
       
        }else if ( this.IsGroupShift || type =='!INTERNAL' || type === 'ADMINISTRATION' || type === 'ALLOWANCE NON-CHARGEABLE' || type === 'ITEM' || (type == 'SERVICE')) {
            sql = `SELECT Distinct [Name] AS ProgName FROM HumanResourceTypes WHERE [group] = 'PROGRAMS' AND (EndDate Is Null OR EndDate >=  '${this.currentDate}') ORDER BY [ProgName]`;
       
        }else {
            return this.SelectFundingProgram(type,false,true);
        
            // sql = `SELECT Distinct [Program] AS ProgName FROM RecipientPrograms 
            //     INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID 
            //     WHERE Recipients.AccountNo = '${type}' AND RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') ORDER BY [ProgName]`

            
            // // sql =`SELECT ACCOUNTNO, PROGRAM AS ProgName, [SERVICE TYPE] as serviceType, [SERVICESTATUS],
            //     (CASE WHEN ISNULL(S.ForceSpecialPrice,0) = 0 THEN
            //     (CASE WHEN C.BillingMethod = 'LEVEL1' THEN I.PRICE2
            //      WHEN C.BillingMethod = 'LEVEL2' THEN I.PRICE3
            //      WHEN C.BillingMethod = 'LEVEL3' THEN I.PRICE4
            //      WHEN C.BillingMethod = 'LEVEL4' THEN I.PRICE5                       
            //      WHEN C.BillingMethod = 'LEVEL5' THEN I.PRICE6
            //     ELSE I.Amount END )
            //     ELSE S.[UNIT BILL RATE] END ) AS BILLRATE
            //     FROM RECIPIENTS C INNER JOIN RECIPIENTPROGRAMS RP ON C.UNIQUEID = RP.PERSONID 
            //     INNER JOIN ServiceOverview S ON C.UNIQUEID = S.PersonID AND RP.PROGRAM = S.ServiceProgram
            //     INNER JOIN ITEMTYPES I ON S.[SERVICE TYPE] = I.TITLE AND ProcessClassification IN ('OUTPUT', 'EVENT', 'ITEM')
            //     WHERE ACCOUNTNO = '${type}'`
       }
       if (!sql) return EMPTY;
        return this.listS.getlist(sql);
        
    }

  SelectFundingProgram(RosterRecipient:string, b_FORCEAll:boolean=false, b_LimitScope:boolean=false)
 {   
    
    let SQLStmt='';
    let s_ProgramFilter : string = this.userSettings.viewFilter;
    let s_Garbage : string
    let s_LimitScopeSQL :string
    let s_OpenScopeSQL : string
    let s_CloseDateSQL : string
    let s_EarliestDate : string= moment(this.date).format('YYYY/MM/DD');
    
    let GST : boolean = false
    let GSTRate :number = 0;
    if (s_EarliestDate !='' && s_EarliestDate > "1999/12/21" )
        s_CloseDateSQL = ` (ISNULL(pr.CloseDate, '2000/01/01') < '${s_EarliestDate}') AND `;
    else
        s_CloseDateSQL = "";
    
    
     s_OpenScopeSQL = `SELECT DISTINCT [Name] AS ProgName, pr.Type FROM HumanResourceTypes pr 
                        WHERE [group] = 'PROGRAMS' AND 
                      ${s_CloseDateSQL}
                     (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111)) 
                      ORDER BY [ProgName]`
    
    s_LimitScopeSQL = `SELECT DISTINCT [Program] AS ProgName,pr.Type FROM RecipientPrograms 
                      INNER JOIN Recipients ON RecipientPrograms.PersonID = Recipients.UniqueID 
                      INNER JOIN HumanResourceTypes pr ON RecipientPrograms.Program = pr.name 
                      WHERE Recipients.AccountNo = '${RosterRecipient}' AND 
                      ${s_CloseDateSQL}
                      RecipientPrograms.ProgramStatus IN ('ACTIVE', 'WAITING LIST') 
                      ORDER BY [ProgName]`

    let s_LimitProgramSQL = ""

        if (s_ProgramFilter!="" )
        {
            s_ProgramFilter = s_ProgramFilter.substring (s_ProgramFilter.length - 4 - s_ProgramFilter.indexOf("WHERE"));
            s_ProgramFilter = s_ProgramFilter.replace("RecipientPrograms.Program", "[Name]");
            s_LimitProgramSQL = `SELECT DISTINCT [Name] AS ProgName, pr.Type FROM HumanResourceTypes pr 
                                WHERE [GROUP] = 'PROGRAMS' AND 
                                ${s_ProgramFilter} AND 
                                ${s_CloseDateSQL}
                                (EndDate Is Null OR EndDate >= convert(varchar,getDate(),111)) 
                                ORDER BY [ProgName]`;
    
        }
    
    if (b_FORCEAll)        
        SQLStmt = s_OpenScopeSQL
    else{
        if (this.viewType=='Recipient') {
            if (this.LimitLookupScope("PROGRAM"))
                SQLStmt = s_LimitScopeSQL 
            else 
                SQLStmt = s_OpenScopeSQL
        }else  if (this.viewType=='Staff') {
            if (b_LimitScope ){
                if (this.LimitLookupScope("PROGRAM"))
                    SQLStmt = s_LimitScopeSQL
                else
                    if (s_LimitProgramSQL != "" ) SQLStmt = s_LimitProgramSQL 
                     else SQLStmt = s_OpenScopeSQL
                
            }else
                if (s_LimitProgramSQL != "") SQLStmt = s_LimitProgramSQL; else SQLStmt = s_OpenScopeSQL;
           
        }
    }
    
    if (!SQLStmt) return EMPTY;
    return this.listS.getlist(SQLStmt);


}

LimitLookupScope(s_Lookup :string) 
{
 let status :boolean = false
 let s_Extent:string;
    
        switch (s_Lookup.toUpperCase())
        {
            case "PROGRAM" :
                status =this.userSettings.limitProgramLookups;
                break;
            case "ACTIVITY":                
                status =this.userSettings.limitServiceLookups;
                break;
            case  "SERVICE":
                status =this.userSettings.limitServiceLookups;
                //status =this.userSettings.LimitStaffLookups;
                if (this.userSettings.LimitStaffLookups)  s_Extent = "LINKED_ONLY"
                break;
            case  "PAYTYPE" :
                status=this.userSettings.LimitPayTypeLookups;
                break;
            case "STAFF":
                status=this.userSettings.LimitStaffLookups
            break;
        }
        return status;
    }

   

    GETRECIPIENT(view: number): string {
        const { recipientCode, debtor, serviceType } = this.bookingForm.value;
        if(view == 0){
            if(serviceType == 'SERVICE') return '!MULTIPLE';
            if(this.globalS.isEmpty(recipientCode)) return '!INTERNAL';
            return recipientCode;
        }

        if(view == 1){
            return debtor;
        }
    }
  

    GET_ADDRESS(): Observable<any> {
        let sql;            
       
            sql = `SELECT CASE WHEN Address1 <> '' THEN Address1 + ' ' ELSE '' END + CASE WHEN Address2 <> '' THEN Address2 + ''  ELSE '' END + CASE WHEN Suburb <> '' THEN ', ' + Suburb ELSE '' END AS Address, GoogleAddress 
            FROM NamesAndAddresses WHERE  Description = 'DESTINATION'`
            
        if (!sql) return EMPTY;
        return this.listS.getlist(sql);
    }
    GET_MOBILITY(): Observable<any> {
        let sql;            
       
            sql = `select Description from datadomains where Domain like 'Mobility' and [DeletedRecord]=0`
            
        if (!sql) return EMPTY;
        return this.listS.getlist(sql);
    }

 GetStateFromPostcode(s_Postcode : string) 
{
    let StateFromPostcode = ""

    switch(Number(s_Postcode.substring(0, 1))){
    case 0 :
        StateFromPostcode = "NT";
        break;
    case 2 :
            var x = Number(s_Postcode);
            switch(true){
            case (x>=2600 && x<=2618): StateFromPostcode = "ACT";  break;
            case (x>=2900 && x<=2999): StateFromPostcode = "ACT";  break;
            default: StateFromPostcode = "NSW";  break;
            }
            break;
    case 3:
        StateFromPostcode = "VIC";
        break;
    case 4:
        StateFromPostcode = "QLD";
        break;
    case 5 :
        StateFromPostcode = "SA";
        break;
    case 6:
        StateFromPostcode = "WA";
        break;
    case 7:
        StateFromPostcode = "TAS";
        break;
    }
    
    return StateFromPostcode;
}

IsStaffAllocated(s_StaffCode:string, s_Date : string, s_time:string, duration:number) 
{
    let status:any;
   
    

    let sql=`SELECT [Client Code],[Start Time], LEFT(CONVERT(VARCHAR,DATEADD(Minute,[duration]*5,[Start Time]),108),5) as end_time FROM Roster 
    where [date] = '${moment(s_Date).format('YYYY/MM/DD')}' AND [Carer code]= '${s_StaffCode}' and
    ([start Time]  between  '${s_time}'  and  LEFT(CONVERT(VARCHAR,DATEADD(Minute,${duration}*5,'${s_time}'),108),5)
     OR 
     ('${s_time}' between [Start Time] and LEFT(CONVERT(VARCHAR,DATEADD(Minute,duration*5,[start time]),108),5))
    )`
   return  this.listS.getlist(sql);
    // this.listS.getlist(sql).subscribe(data=>{
    //     if (data.length>0)
    //         return true;
    //     else
    //         return false;
    //  });   
    
    //return  this.listS.getlist(sql).subscribe(data=>{})
}
  

getPublicHolidyas(s_StaffCode:string){
  
    let s_PubHolState:string
    let s_PubHolRegion:string;
    let s_PubHolFilter:string;
    let s_FilterMask:string

       let sql=`SELECT st.PublicHolidayRegion, sta.Postcode FROM Staff st INNER JOIN NamesAndAddresses sta ON st.Uniqueid = sta.personid WHERE (ISNULL(sta.primaryAddress, 0) = 1 or sta.Type = '<USUAL>') AND st.AccountNo = '${s_StaffCode}'`

      // this.listS.getlist(sql) 
       let sql2="";
        
            new Observable(observer => {
                this.listS.getlist(sql)          
            .pipe(
                tap(output => {
                    console.log(output);
                    this.publicHolidayRegionData=output[0];
                    s_PubHolRegion = this.publicHolidayRegionData.publicHolidayRegion;
                    s_PubHolState = this.GetStateFromPostcode(this.publicHolidayRegionData.postcode);//(N2S(!postcode))
                 
                    s_FilterMask = (s_PubHolState != ""? 1: 0)  + "" + (s_PubHolRegion != ""? 1: 0)
                    s_PubHolFilter="";   
                    switch(s_FilterMask){
                    case "00" : ""; break;
                    case "01": s_PubHolFilter = s_PubHolFilter + " AND ( (ISNULL(Stats, '') IN ('', 'ALL')) OR (PublicHolidayRegion = '" + s_PubHolRegion + "'))" ; break;
                    case "10": s_PubHolFilter = s_PubHolFilter + " AND ( (ISNULL(Stats, '') IN ('', 'ALL')) OR (Stats = '" + s_PubHolState + "'))" ; break;
                    case "11": s_PubHolFilter = s_PubHolFilter + " AND ( (ISNULL(Stats, '') IN ('', 'ALL')) OR ((Stats = '" + s_PubHolState + "') OR ( (Stats = '" + s_PubHolState + "') AND (PublicHolidayRegion = '" + s_PubHolRegion + "'))))" ; break;
                    }
                    let dd= new Date (this.date);
                    sql2=`select [DATE] from PUBLIC_HOLIDAYS where month([DATE])=${dd.getMonth()+1} and year([DATE])=  ${dd.getFullYear()}  ${s_PubHolFilter} `
        
                }),
                switchMap(output =>                     
                    forkJoin( 
                        this.listS.getlist(sql2)  
                    )) ,
                tap(output2 => {
                    console.log(output2);
                    this.lstPublicHolidays=output2[0];
                })
            

            ).subscribe(output2 => console.log( output2))
        
        }).subscribe(data=>{
            console.log(data);
        });

    
}

IsPublicHoliday1(s_Date : String) : boolean
{
    let status=false;
   if (this.lstPublicHolidays!=null){
        var target=this.lstPublicHolidays.find(tmp=>tmp.date==s_Date)
        if(target!=null)
            status=true;
   }
    return status;
    
}


GetDayMask()
{
        let s_Date:string
        let s_Weekday:string;
        let b_PublicHoliday : any
        
        let ip : Array<string>=["0","0","0","0","0","0","0","0"];
     
        let DayNo:number;
        let Yearno:number;
        let MonthNo:number;
        
        var sheet=this.spreadsheet.getActiveSheet();

         var sels = sheet.getSelections();
         var sel = sels[0];
         var row = sel.row;
         
         for (let i=sel.col; i<(sel.col+sels[0].colCount); i++)
        {
           // DayNo=i;              
            var cell_col_text=sheet.getValue(0,i,GC.Spread.Sheets.SheetArea.colHeader);
            
            var col_day= cell_col_text.substring(cell_col_text.length-2,cell_col_text.length);
            if (!this.recurrenceView){
                DayNo   = col_day.trim()    ;
                MonthNo = parseInt(format(this.date, 'M'));
                Yearno = parseInt(format(this.date, 'yyyy'));
            }else{
                let dats=this.date;
                if (this.date.toString().indexOf('/')<=0){
                    dats=  moment(this.date).format('YYYY/MM/DD') 
                }
                let  dts=dats.split("/");
                DayNo   = dts[2]   ;
                MonthNo = dts[1] ;  
                Yearno = dts[0] //this.date.substring(0, 4)  ; 
            }
       // If NullToStr(s_EarliestDate) = "" Then s_EarliestDate = Format$(Yearno & "/" & Monthno & "/" & IIf(b_SDay, DayNo, dCtr), "yyyy/mm/dd")
          s_Date = Yearno + "/" + this.numStr(MonthNo) + "/" + this.numStr(DayNo);
          b_PublicHoliday =   this.IsPublicHoliday1(s_Date);       
     
          
         ip[7] = "0"
         if (b_PublicHoliday)
             ip[7] = "1"
         else {           
            s_Weekday = this.GetWeekday(s_Date);
            switch (s_Weekday.toUpperCase())
            {        
             case "MONDAY": 
                ip[0] = "1" 
                break;
             case "TUESDAY": 
                ip[1] = "1" 
                break;
             case "WEDNESDAY": 
                ip[2] = "1"
                break;
             case "THURSDAY": 
                ip[3] = "1"
                break;
             case "FRIDAY": 
                ip[4] = "1"
                break;
             case "SATURDAY": 
                ip[5] = "1";
                break;
             case "SUNDAY": 
                ip[6] = "1";
                break;
            }
                        
         }
    }
    var DaysString="";
         for( let i = 0; i<=7; i++)
            DaysString = DaysString + ip[i];
        
    return DaysString;
}

GetWeekday(date:string) : string {

    let dt= new Date(date);
   // return  this.DayOfWeek(dt.toLocaleDateString('en-US', { weekday: 'long' }));
   return  dt.toLocaleDateString('en-US', { weekday: 'long' });

}

GETSERVICEACTIVITY(program: any): Observable<any> {

//    const { serviceType, date, time } = this.bookingForm.value;
    let serviceType=this.serviceType;
    var { recipientCode }  = this.bookingForm.value;
    if (recipientCode=='' && this.viewType=='Recipient')
        recipientCode= this.selected.data;
    this.recipientCode=recipientCode;

    var _date = this.date;
    if (!program) return EMPTY;
    

    if ( serviceType != 'ALLOWANCE NON-CHARGEABLE' && serviceType != 'ITEM'  && serviceType != 'SERVICE') {

        if(typeof _date === 'string'){
            _date = parseISO(_date);
        }

              
         this.s_DayMask= this.GetDayMask();
       // return this.listS.getserviceactivityall({
           return this.timeS.getActivities({            
            recipient: recipientCode,
            program:program,  
            forceAll: "0", //recipientCode=='!MULTIPLE' || recipientCode=='!INTERNAL' ? "1" : "0",   
            mainGroup: this.IsGroupShift ? this.GroupShiftCategory : serviceType,
            subGroup: '-',           
            viewType: this.viewType,
            AllowedDays: this.s_DayMask,
            duration: this.durationObject?.duration            
        });
    }
    else {
        let  sql="";

        return this.timeS.getActivities({            
            recipient: recipientCode,
            program:program,  
            forceAll: "1", //recipientCode=='!MULTIPLE' || recipientCode=='!INTERNAL' ? "1" : "0",   
            mainGroup: this.IsGroupShift ? this.GroupShiftCategory : 'ALL',
            subGroup: '-',           
            viewType: this.viewType,
            AllowedDays: "0",
            duration: this.durationObject?.duration            
        });

    //   ql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,
    //     (CASE WHEN ISNULL(SO.ForceSpecialPrice,0) = 0 THEN
    //     (CASE WHEN C.BillingMethod = 'LEVEL1' THEN I.PRICE2
    //      WHEN C.BillingMethod = 'LEVEL2' THEN I.PRICE3
    //      WHEN C.BillingMethod = 'LEVEL3' THEN I.PRICE4
    //      WHEN C.BillingMethod = 'LEVEL4' THEN I.PRICE5
    //      WHEN C.BillingMethod = 'LEVEL5' THEN I.PRICE6
    //     ELSE I.Amount END )
    //     ELSE SO.[UNIT BILL RATE] END ) AS BILLRATE,
    //     isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,hrt.GST,I.unit as UnitType,
    //     (select case when UseAwards=1 then 'AWARD' ELSE '' END from registration) as Service_Description,
    //     HACCType,c.AgencyDefinedGroup as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
    //     FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
    //     INNER JOIN Recipients C ON C.AccountNO = '${this.FetchCode}'
    //     INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
    //     WHERE SO.ServiceProgram = '${program}' AND [SO].[ServiceStatus] = 'ACTIVE' 
    //     AND EXISTS
    //     (SELECT Title
    //     FROM ItemTypes ITM
    //     WHERE Title = SO.[Service Type] 
    //     AND ITM.[Status] = 'ATTRIBUTABLE' AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
    //     ORDER BY [Service Type]`; 
    
    sql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,         
        I.AMOUNT AS BILLRATE,
        isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,0 as GST,I.unit as UnitType,
        'N/A' as Service_Description,
        HACCType,'' as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
        FROM ServiceOverview SO         
        INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
        WHERE SO.ServiceProgram = '${program}' 
        AND EXISTS
        (SELECT Title
        FROM ItemTypes ITM
        WHERE  RosterGroup = 'RECPTABSENCE' And Title = SO.[Service Type] And ProcessClassification = 'EVENT' 
        AND ITM.[Status] = 'ATTRIBUTABLE' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
        ORDER BY [Service Type]`;
    
//        return this.listS.getlist(sql);
    }
}

GETSERVICEACTIVITY2(program: any): Observable<any> {

        let serviceType=this.serviceType;
        const { recipientCode }  = this.bookingForm.value;

        if (recipientCode!="" && recipientCode!=null){
            this.FetchCode=recipientCode;
          }
        let sql ="";
         if (!program) return EMPTY;
    
        // console.log(this.rosterForm.value)

    //     var durationObject = (this.globalS.computeTimeDATE_FNS(this.defaultStartTime, this.defaultEndTime));        
    
    //    let s_DayMask:string;
    //    let s_DayTimeSQL:string="";
    //    let s_Duration:number;
      
    //    s_Duration=durationObject.duration;
    //     if (this.EnforceActivityLimits == true)
    //     {
    //         if ((s_DayMask) != "" && s_DayMask != "00000000")
    //         {
    //             if (s_DayMask.slice(0, 1) == "1")
    //                 s_DayTimeSQL = "IsNull(ITM.NoMonday, 0) = 0";
    //             if (s_DayMask.slice(1, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == ""? "IsNull(ITM.NoTuesday, 0) = 0": s_DayTimeSQL + " AND IsNull(ITM.NoTuesday, 0) = 0";
    //             if (s_DayMask.slice(2, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == ""? "IsNull(ITM.NoWednesday, 0) = 0": s_DayTimeSQL + " AND IsNull(ITM.NoWednesday, 0) = 0";
    //             if (s_DayMask.slice(3, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == ""? "IsNull(ITM.NoThursday, 0) = 0" : s_DayTimeSQL + " AND IsNull(ITM.NoThursday, 0) = 0";
    //             if (s_DayMask.slice(4, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == "" ? "IsNull(ITM.NoFriday, 0) = 0": s_DayTimeSQL + " AND IsNull(ITM.NoFriday, 0) = 0";
    //             if (s_DayMask.slice(5, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == ""? "IsNull(ITM.NoSaturday, 0) = 0": s_DayTimeSQL + " AND IsNull(ITM.NoSaturday, 0) = 0";
    //             if (s_DayMask.slice(6, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == "" ? "IsNull(ITM.NoSunday, 0) = 0": s_DayTimeSQL + " AND IsNull(ITM.NoSunday, 0) = 0";
    //                 if (s_DayMask.slice(7, 1) == "1")
    //                 s_DayTimeSQL = s_DayTimeSQL == "" ? "IsNull(ITM.NoPubHol, 0) = 0" : s_DayTimeSQL + " AND IsNull(ITM.NoPubHol, 0) = 0";
    //             if (s_DayTimeSQL != "")
    //                 s_DayTimeSQL = " AND (" + s_DayTimeSQL + ")";
    //         }
    
    //         s_DayTimeSQL = s_DayTimeSQL + " AND (" + "(ISNULL(ITM.MinDurtn, 0) = 0 AND ISNULL(ITM.MAXDurtn, 0) = 0) OR (ISNULL(ITM.MinDurtn, " + s_Duration + ") <= " + s_Duration + " AND IsNull(ITM.MaxDurtn, " + s_Duration + ") >= " + s_Duration + "))";
    
    //     }
    
    
        
       if (serviceType == 'ADMINISTRATION' ){
        // const { recipientCode, debtor } = this.rosterForm.value;
        sql =` SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,     
              I.Amount AS BILLRATE,
              I.unit as UnitType,isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,hrt.GST,
              (select case when UseAwards=1 then 'AWARD' ELSE '' END from registration) as Service_Description,
              HACCType,'' as Anal,
              (select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
              FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID        
              INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
              WHERE SO.ServiceProgram = '${program}'  and I.[Status] = 'NONATTRIBUTABLE'
              AND EXISTS
              (SELECT Title
              FROM ItemTypes ITM
              WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = 'ADMINISTRATION'
              AND ITM.[Status] = 'NONATTRIBUTABLE' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
              ORDER BY [Service Type]`;

    }else if (serviceType == 'ADMISSION' || serviceType =='ALLOWANCE NON-CHARGEABLE' || serviceType == 'ITEM'  || serviceType == 'SERVICE') {
            // const { recipientCode, debtor } = this.rosterForm.value;
            sql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,
            (CASE WHEN ISNULL(SO.ForceSpecialPrice,0) = 0 THEN
            (CASE WHEN C.BillingMethod = 'LEVEL1' THEN I.PRICE2
             WHEN C.BillingMethod = 'LEVEL2' THEN I.PRICE3
             WHEN C.BillingMethod = 'LEVEL3' THEN I.PRICE4
             WHEN C.BillingMethod = 'LEVEL4' THEN I.PRICE5
             WHEN C.BillingMethod = 'LEVEL5' THEN I.PRICE6
            ELSE I.Amount END)
            ELSE SO.[UNIT BILL RATE] END ) AS BILLRATE,
            isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,0 as GST,I.unit as UnitType,
            (select case when UseAwards=1 then 'AWARD' ELSE '' END from registration) as Service_Description,
            HACCType,c.AgencyDefinedGroup as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
            FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
            INNER JOIN Recipients C ON C.AccountNO = '${this.FetchCode}'
            INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
            WHERE SO.ServiceProgram = '${ program}' 
            AND EXISTS
            (SELECT Title
            FROM ItemTypes ITM
            WHERE Title = SO.[Service Type] AND ITM.[RosterGroup] = '${serviceType}'
            AND ITM.[Status] = 'ATTRIBUTABLE' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
            ORDER BY [Service Type]`;
    
        }else if (serviceType == 'TRAVEL TIME' || serviceType == 'TRAVELTIME') {

            sql=` SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,
            (CASE WHEN ISNULL(SO.ForceSpecialPrice,0) = 0 THEN
            (CASE WHEN C.BillingMethod = 'LEVEL1' THEN I.PRICE2
             WHEN C.BillingMethod = 'LEVEL2' THEN I.PRICE3
             WHEN C.BillingMethod = 'LEVEL3' THEN I.PRICE4
             WHEN C.BillingMethod = 'LEVEL4' THEN I.PRICE5
             WHEN C.BillingMethod = 'LEVEL5' THEN I.PRICE6
            ELSE I.Amount END )
            ELSE SO.[UNIT BILL RATE] END ) AS BILLRATE,
            isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,hrt.GST,I.unit as UnitType,
            (select case when UseAwards=1 then 'AWARD' ELSE '' END from registration) as Service_Description,
            HACCType,c.AgencyDefinedGroup as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
            FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
            INNER JOIN Recipients C ON C.AccountNO = '${this.FetchCode}'
            INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
            WHERE SO.ServiceProgram = '${program}'
			AND I.[RosterGroup] = 'TRAVELTIME' AND (I.EndDate Is Null OR I.EndDate >='${this.currentDate}') `
            
           

         }else if (this.booking_case==4 && !this.IsGroupShift){
           
            sql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,
            (CASE WHEN ISNULL(SO.ForceSpecialPrice,0) = 0 THEN
            (CASE WHEN C.BillingMethod = 'LEVEL1' THEN I.PRICE2
             WHEN C.BillingMethod = 'LEVEL2' THEN I.PRICE3
             WHEN C.BillingMethod = 'LEVEL3' THEN I.PRICE4
             WHEN C.BillingMethod = 'LEVEL4' THEN I.PRICE5
             WHEN C.BillingMethod = 'LEVEL5' THEN I.PRICE6
            ELSE I.Amount END )
            ELSE SO.[UNIT BILL RATE] END ) AS BILLRATE,
            isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,0 as GST,I.unit as UnitType,
            (select case when UseAwards=1 then 'AWARD' ELSE '' END from registration) as Service_Description,
            HACCType,c.AgencyDefinedGroup as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
            FROM ServiceOverview SO 
            INNER JOIN Recipients C ON C.UNIQUEID=SO.PERSONID AND C.AccountNO = '${this.FetchCode}'
            INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
            WHERE SO.ServiceProgram = '${program}' 
            AND EXISTS
            (SELECT Title
            FROM ItemTypes ITM
            WHERE Title = SO.[Service Type] 
            AND ITM.[Status] = 'ATTRIBUTABLE' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
            ORDER BY [Service Type]`;
        
        }  else if (this.booking_case==4 && this.IsGroupShift){
           
                sql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,         
                I.AMOUNT AS BILLRATE,
                isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate, 0 as GST,I.unit as UnitType,
                'N/A' as Service_Description,
                HACCType,'' as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
                FROM ServiceOverview SO         
                INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
                WHERE SO.ServiceProgram = '${program}' 
                AND EXISTS
                (SELECT Title
                FROM ItemTypes ITM
                WHERE  RosterGroup = '${this.GroupShiftCategory}' And Title = SO.[Service Type] And ProcessClassification in ('EVENT','OUTPUT' )
                AND ITM.[Status] = 'ATTRIBUTABLE' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
                ORDER BY [Service Type]`;

    }else if (this.booking_case==8 ){
           
        sql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,         
        I.AMOUNT AS BILLRATE,
        isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,0 as GST,I.unit as UnitType,
        'N/A' as Service_Description,
        HACCType,'' as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
        FROM ServiceOverview SO         
        INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
        WHERE SO.ServiceProgram = '${program}' 
        AND EXISTS
        (SELECT Title
        FROM ItemTypes ITM
        WHERE  RosterGroup = 'RECPTABSENCE' And Title = SO.[Service Type] And ProcessClassification = 'EVENT' 
        AND ITM.[Status] = 'ATTRIBUTABLE' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
        ORDER BY [Service Type]`;
    }
     else {          

            sql =`  SELECT DISTINCT [Service Type] AS Activity,I.RosterGroup,
            (CASE WHEN ISNULL(SO.ForceSpecialPrice,0) = 0 THEN
            (CASE WHEN C.BillingMethod = 'LEVEL1' THEN I.PRICE2
             WHEN C.BillingMethod = 'LEVEL2' THEN I.PRICE3
             WHEN C.BillingMethod = 'LEVEL3' THEN I.PRICE4
             WHEN C.BillingMethod = 'LEVEL4' THEN I.PRICE5
             WHEN C.BillingMethod = 'LEVEL5' THEN I.PRICE6
            ELSE I.Amount END )
            ELSE SO.[UNIT BILL RATE] END ) AS BILLRATE,
            isnull([Unit Pay Rate],0) as payrate,isnull(TaxRate,0) as TaxRate,hrt.GST,I.unit as UnitType,
            (select case when UseAwards=1 then 'AWARD' ELSE '' END from registration) as Service_Description,
            HACCType,c.AgencyDefinedGroup as Anal,(select top 1 convert(varchar,convert(datetime,PayPeriodEndDate),111) as PayPeriodEndDate from SysTable) as date_Timesheet
            FROM ServiceOverview SO INNER JOIN HumanResourceTypes HRT ON CONVERT(nVarchar, HRT.RecordNumber) = SO.PersonID
            INNER JOIN Recipients C ON C.AccountNO = '${this.FetchCode}'
            INNER JOIN ItemTypes I ON I.Title = SO.[Service Type]
            WHERE SO.ServiceProgram = '${program}' AND [SO].[ServiceStatus] = 'ACTIVE' 
            AND EXISTS
            (SELECT Title
            FROM ItemTypes ITM
            WHERE Title = SO.[Service Type] 
            AND ITM.[Status] = 'ATTRIBUTABLE' AND ProcessClassification = 'OUTPUT' AND (ITM.EndDate Is Null OR ITM.EndDate >= '${this.currentDate}'))
            ORDER BY [Service Type]`; 

            
              }

            return this.listS.getlist(sql);
            
        
    };

    GETSERVICEGROUP(): Observable<any>{
        
        if (this.booking_case==8)
            return this.listS.getlist(`SELECT DISTINCT [Name] as Description FROM CSTDAOutlets WHERE [Name] Is NOT Null  AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY [Name]`);
        else if (this.IsGroupShift){
            if (this.GroupShiftCategory=="TRANSPORT" )
                return this.listS.getlist(`SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'VEHICLES' AND Description Is NOT Null  AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY DESCRIPTION`);
            else if (this.GroupShiftCategory=="CENTREBASED" )            
                return this.listS.getlist(`SELECT DISTINCT [Name] as  Description FROM CSTDAOutlets WHERE  Name Is NOT Null  AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY [DESCRIPTION]`);
            else if ( this.GroupShiftCategory=='GROUPACTIVITY' || this.GroupShiftCategory=='GROUPBASED')            
                return this.listS.getlist(`SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'ACTIVITYGROUPS' AND Description Is NOT Null  AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY DESCRIPTION`);
          
            else
                return this.listS.getlist(`SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'ACTIVITYGROUPS' AND Description Is NOT Null AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY DESCRIPTION`);
        }else
            return this.listS.getlist(`SELECT DISTINCT Description FROM DataDomains WHERE Domain = 'ACTIVITYGROUPS' AND Description Is NOT Null AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY DESCRIPTION`);
    }

    GETANALYSISCODE(): Observable<any>{
        return this.listS.getserviceregion();
    }

    GETROSTERGROUP(activity: string): Observable<any>{
        if (!activity) return EMPTY;
       
        return this.listS.getlist(`SELECT RosterGroup, Title FROM ItemTypes WHERE Title= '${activity}'`);
    }

    GETPAYTYPE(type: string): Observable<any> {
        // `SELECT TOP 1 RosterGroup, Title FROM  ItemTypes WHERE Title = '${type}'`
        // let sql;
        // if (!type) return EMPTY;
        // this.Select_Pay_Type="Select Pay Type"
        // if (type === 'ALLOWANCE CHARGEABLE' || type === 'ALLOWANCE NON-CHARGEABLE') {
        //     sql = `SELECT Recnum, Title, ''as HACCCode FROM ItemTypes WHERE RosterGroup = 'ALLOWANCE ' 
        //         AND Status = 'NONATTRIBUTABLE' AND ProcessClassification = 'INPUT' AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY TITLE`
        // } else if (this.IsGroupShift && this.GroupShiftCategory=="TRANSPORT" ){
        //     this.Select_Pay_Type="Select Transportation Reason";
        //     sql= `SELECT RecordNumber as Recnum, Description  AS Title,HACCCode FROM DataDomains WHERE Domain = 'TRANSPORTREASON' ORDER BY Description`
       
        // }else  {
        //   sql = `SELECT Recnum, LTRIM(RIGHT(Title, LEN(Title) - 0)) AS Title, '' as HACCCode
        //     FROM ItemTypes WHERE RosterGroup = 'SALARY'   AND Status = 'NONATTRIBUTABLE'   AND ProcessClassification = 'INPUT' AND Title BETWEEN '' 
        //     AND 'zzzzzzzzzz'AND (EndDate Is Null OR EndDate >= '${this.currentDate}') ORDER BY TITLE`
        // }
        // return this.listS.getlist(sql);

        //if (this.userSettings.useAwards) return null;

        let inputs={
            
            chooseEach :0,
            payTypeMode :'Filter for Pay Group',
            s_PayItem :'',
            s_PayUnit :'',
            s_PayRate :'',
            s_Status :'',
            s_DayMask : this.s_DayMask,
            b_Award :this.userSettings.useAwards,
            s_RosterStaff : this.viewType=='Staff' ? this.selected.data : this.selectedCarer,
            b_TestForSingle : 0,
            s_TimespanStart : moment(this.defaultStartTime).format('HH:mm'),
            s_TimespanEnd : moment(this.defaultEndTime).format('HH:mm'),
            
           }
   
           return this.timeS.determinePayType(inputs);
           
   
    }

    // Add Timesheet
   
    ifRosterGroupHasTimePayBills(rosterGroup: string) {
        return (
            rosterGroup === 'ADMINISTRATION' ||
            rosterGroup === 'ADMISSION' ||
            rosterGroup === 'CENTREBASED' ||
            rosterGroup === 'GROUPACTIVITY' ||
            rosterGroup === 'ITEM' ||
            rosterGroup === 'ONEONONE' ||
            rosterGroup === 'SLEEPOVER' ||
            rosterGroup === 'TRANSPORT' ||
            rosterGroup === 'TRAVELTIME'
        );
    }

    selected: any = null;
    FetchCode = "";
    payPeriodEndDate: Date;
    unitsArr: Array<string> = ['HOUR', 'SERVICE'];

    activity_value: number;
    durationObject: any;

    defaultStartTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
    defaultEndTime: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);



   // dateFormat: string = 'dd/MM/yyyy'
    checked = false;    
    indeterminate = false;
    
    modalTimesheetValues: Array<AddTimesheetModalInterface> = [
        {
            index: 1,
            name: 'ADMINISTRATION'
        },
        {
            index: 2,
            name: 'ALLOWANCE CHARGEABLE'
        },
        {
            index: 3,
            name: 'ALLOWANCE NON-CHARGEABLE'
        },
        {
            index: 4,
            name: 'CASE MANAGEMENT'
        },
        {
            index: 5,
            name: 'ITEM'
        },
        {
            index: 6,
            name: 'SLEEPOVER'
        },
        {
            index: 7,
            name: 'TRAVELTIME'
        },
        {
            index: 8,
            name: 'SERVICE'
        },
    ];

    agencyDefinedGroup: string;    

    current = 0;
    nextDisabled: boolean = false;
    programsList: Array<any> = [];
    serviceActivityList: Array<any>;
    payTypeList: Array<any> = [];
    analysisCodeList: Array<any> = [];
    programActivityList:Array<any>=[];
    ServiceGroups_list:Array<any>=[];
    groupShiftList:Array<any>=["CENTREBASED","GROUPACTIVITY","TRANSPORT"];
    RecipientList:Array<any>=[];

    serviceSetting:string;
    date_Timesheet:any;
    clearLowerLevelInputs() {

        this.bookingForm.patchValue({
            recipientCode: null,
            debtor: null,
            program: null,
            serviceActivity: null,
            analysisCode: null,
            time: {
                startTime: '',
                endTime: '',
            },
            pay: {
                pay_Unit: 'HOUR',
                pay_Rate: '0',
                quantity: '1',
                position: ''
            },
            bill: {
                pay_Unit: 'HOUR',
                bill_Rate: '0',
                quantity: '1',
                tax: '0'
            },
        });
    }

    

    
isServiceTypeMultipleRecipient(type: string): boolean {
        return type === 'SERVICE';
    }

    isTravelTimeChargeableProcess(type: string): boolean {
        return type === 'TRAVELTIME';
    }

    isSleepOverProcess(type: string): boolean {
        return type == 'SLEEPOVER';
    }


    whatType(data: number): string {
        return data == 0 ? 'Staff' : 'Recipient';
    }   
    buildForm() {

        
     //--------------------------an other booking form-------------------------------- 
        this.bookingForm = this.formBuilder.group({
            recordNo: [''],
            date: [this.payPeriodEndDate, Validators.required],
            serviceType: ['', Validators.required],
            program: ['', Validators.required],
            serviceActivity: ['', Validators.required],
            payType: ['', Validators.required],
            analysisCode: [''],
            recipientCode:  [''],
            haccType: '',
            staffCode:  [''],
            debtor:  [''],
            serviceSetting:'',
            isMultipleRecipient: false,
            isTravelTimeChargeable: false,
            sleepOverTime: '',
            time: this.formBuilder.group({
                startTime:  [''],
                endTime:  [''],
            }),
            pay: this.formBuilder.group({
                pay_Unit:  ['HOUR'],
                pay_Rate:  ['0.0'],
                quantity:  ['1'],
                position: ''
            }),
            bill: this.formBuilder.group({
                pay_Unit: ['HOUR'],
                bill_Rate: ['0.0'],
                quantity: ['1'],
                tax: '1.0'
            }),
            central_Location:false
            
        });
        
this.bookingForm.valueChanges.subscribe(x=>{
            console.log ('Value changes'+ x.program);

}) ;     

this.bookingForm.get('program').valueChanges.pipe(
            distinctUntilChanged(),
            switchMap(x => {
                if(!x) return EMPTY;
                this.serviceActivityList = [];
                this.bookingForm.patchValue({
                    serviceActivity: null
                });
                return   this.GETSERVICEACTIVITY(x);                
                                        
                
            })
        ).subscribe((d: Array<any>) => {

            this.serviceActivityList = d;//d.map(x => x.activity);
                setTimeout(() => {
                    this.bookingForm.patchValue({
                        serviceActivity: this.defaultActivity                     
                        
                    });
                   
                }, 0);
            
           
            if(d && d.length == 1){
                this.bookingForm.patchValue({
                    serviceActivity: d[0]               
                   
                });
               
                
            }
            
        });
     

    this.bookingForm.get('serviceActivity').valueChanges.pipe(
        distinctUntilChanged(),     

        switchMap(x => {
            if (!x) {
                this.rosterGroup = '';
                return EMPTY;
            };
            this.selectedActivity=x;

            return forkJoin(                
                this.GETROSTERGROUP(x.activity), 
                this.GETPAYTYPE(x.activity),
                this.GETANALYSISCODE(),
                this.GETSERVICEGROUP()                                  
                )
            
        })
    ).subscribe(d => {
        
        //console.log(d);
        //if (d.length > 1 || d.length == 0) return false;
        let lst =d[0];
        this.rosterGroup = this.selectedActivity.rosterGroup// ( lst[0].rosterGroup);
        this.GET_ACTIVITY_VALUE((this.rosterGroup).trim());
        this.payTypeList = d[1];
        this.analysisCodeList = d[2];
        this.analysisCodeList = d[2];
        if (this.activity_value==12 || this.booking_case==8 || this.booking_case==6 || this.IsGroupShift){
            this.ServiceGroups_list = d[3].map(x => x.description);;
        }  
        this.date_Timesheet=this.selectedActivity.date_Timesheet;

        this.bookingForm.patchValue({
           
            bill:{
            pay_Unit: this.selectedActivity.unitType,
            bill_Rate: this.selectedActivity.billrate,            
            tax: this.selectedActivity.taxrate,
            
            },
            pay:{
                pay_Unit: this.selectedActivity.unitType,
                pay_Rate: this.selectedActivity.payrate,
                tax: this.selectedActivity.taxrate
            },
            haccType: this.selectedActivity.haccType,
            //analysisCode: this.selectedActivity.anal,
            
        })
      
       if (this.booking_case==8){
           this.IsClientCancellation=true;
       }
    }); 
   
    this.TransportForm=this.formBuilder.group({
        
        pickupFrom : [''],
        pickupTo : [''],
        zipCodeFrom: [''],
        zipCodeTo: [''],
        appmtTime: [''],
        mobility: [''],
        returnVehicle: [''],
        jobPriority: [''],
        transportNote: ['']
    });

    this.RecurrentServiceForm=this.formBuilder.group({
        
        startDate : [''],
        endDate : ['']
        
    });

   
}
    GET_ACTIVITY_VALUE(roster: string) {
        // ADMINISTRATION
        // ADMISSION
        // ALLOWANCE
        // CENTREBASED
        // GROUPACTIVITY
        // ITEM
        // ONEONONE
        // RECPTABSENCE
        // SALARY
        // SLEEPOVER
        // TRANSPORT
        // TRAVELTIME

        this.activity_value = 0;

        if (roster === 'ADMINISTRATION') {
            this.activity_value = 6;
        }

        if (roster === 'ADMISSION') {
            this.activity_value = 7;
        }

        if (roster === 'ALLOWANCE') {
            this.activity_value = 9;
        }
        
        if (roster === 'CENTREBASED') {
            this.activity_value = 11;
        }

        if (roster === 'GROUPACTIVITY') {
            this.activity_value = 12;
        }

        if (roster === 'ITEM') {
            this.activity_value = 14;
        }

        if (roster === 'ONEONONE') {
            this.activity_value = 2;
        }

        if (roster === 'RECPTABSENCE') {
            this.activity_value = 6;
        }

        if (roster === 'SALARY') {
            this.activity_value = 0;
        }

        if (roster === 'SLEEPOVER') {
            this.activity_value = 8;
        }

        if (roster === 'TRANSPORT') {
            this.activity_value = 10;
        }

        if (roster === 'TRAVEL TIME' || roster ==='TRAVELTIME') {
            this.activity_value = 5;
        }
    }

    
    isEndSteps() {
        if (this.rosterGroup === 'ALLOWANCE') {
            return this.current >= 3;
        }
        else {
            return this.current >= 3;
        }
    }

    defaultOpenValue = new Date(0, 0, 0, 9, 0, 0);

    resetBookingFormModal() {
        this.current = 0;
        this.rosterGroup = '';
      //  this.selectedCarer="";
        this.defaultProgram=null;
        this.defaultActivity=null;
        this.selectedProgram=null;
        //this.sample="";
        this.defaultStartTime = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 8, 0, 0);
        this.defaultEndTime = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 9, 0, 0);        
        this.IsClientCancellation=false;
        
        this.bookingForm.reset({
            date: this.payPeriodEndDate,
            serviceType: '',
            program: '',
            serviceActivity: '',
            payType: '',
            analysisCode: '',
            recipientCode: '',
            debtor: '',
            serviceSetting:'',
            isMultipleRecipient: false,
            isTravelTimeChargeable: false,
            sleepOverTime: new Date(0, 0, 0, 9, 0, 0),
            time: this.formBuilder.group({
                startTime: '',
                endTime: '',
            }),
            pay: this.formBuilder.group({
                pay_Unit: 'HOUR',
                pay_Rate: '0',
                quantity: '1',
                position: ''
            }),
            bill: this.formBuilder.group({
                pay_Unit: 'HOUR',
                bill_Rate: 0,
                quantity: '1',
                tax: '1'
            }),
        });

           }
    pre_roster(): void {
        this.current -= 1;
    }

    next_roster(): void {
        this.current += 1;

        if(this.current == 1 && this.selected.option == 1){
            this.rosterForm.patchValue({
                debtor: this.selected.data
            });
        }

        if(this.current == 4){
            const { recipientCode, program, serviceActivity } = this.rosterForm.value;

            if(!this.globalS.isEmpty(recipientCode) &&
                    !this.globalS.isEmpty(serviceActivity) &&
                        !this.globalS.isEmpty(program)){
                this.timeS.getbillingrate({
                    RecipientCode: recipientCode,
                    ActivityCode: serviceActivity,
                    Program: program
                }).subscribe(data => {
                    this.rosterForm.patchValue({
                        bill: {
                            pay_Unit: data.unit,
                            bill_Rate: this.DEFAULT_NUMERIC(data.rate),
                            tax: this.DEFAULT_NUMERIC(data.tax)
                        }
                    });
                });
            }            
        }
    }

    onItemChange(item){
        this.weekDay=item;
    }

   pre_tab(): void {
        this.current -= 1;
        //this.viewType=="Staff" &&
        if ( this.current == 3 ){
            this.current -= 1;
        }
        if(this.current == 2 && !(this.activity_value==12 || this.ShowCentral_Location)){
            this.current -= 1;
        }
        if (this.current==3 && this.viewType=='Recipient' && this.serviceType=='BOOKED')
        this.current -= 1;
    }

    next_tab(): void {
        this.current += 1;
       
      
        if (this.current==1 && this.serviceActivityList.length<1 ){
            this.globalS.eToast('Error', 'There are no approved services linked to selectd program');
            this.current -= 1;
            return;
        }

        if (this.serviceActivityList.length==1){
        this.defaultActivity=this.serviceActivityList[0]  ;
        this.bookingForm.patchValue({
            serviceActivity: this.serviceActivityList[0]         
        
        });
        }
        
        if (this.current==1 && this.serviceActivityList.length==1){
           
            if (this.showDone2){
                this.addBookingModel=false;
                this.doneBooking();
                return;
            }
              
            if (!this.ShowCentral_Location)
                this.current+=2;
                
        }
       

        
        if(this.current == 2 && !this.IsGroupShift && !(this.activity_value==12 || this.ShowCentral_Location)){
            this.current += 1;
        }
        //console.log(this.current + ", " + this.ShowCentral_Location +", " + this.viewType + ", " + this.IsGroupShift )
                
        //this.viewType=="Staff" &&
        if ( this.current == 3 ){
     
            this.current += 1;
          
        }
       
        if (this.current==3 && this.viewType=='Recipient' && this.serviceType=='BOOKED')
            this.current += 1;
        
        if(this.userSettings.useAwards.toString().toLowerCase()=='true' && this.current>1 && !this.ShowCentral_Location)
            this.doneBooking();
        else if(this.userSettings.useAwards.toString().toLowerCase()=='true' && this.current>2 )
            this.doneBooking();
       
    }
    
    
    
    DEFAULT_NUMERIC(data: any): number{
        if(!this.globalS.isEmpty(data) && !isNaN(data)){
            return data;
        }
        return 0;
    }

    get nextCondition() {
        // console.log(this.rosterGroup)
        if (this.current == 2 && !this.ifRosterGroupHasTimePayBills(this.rosterGroup)) {
            return false; 
        }
        if(this.current == 3 && this.rosterGroup == 'ADMINISTRATION'){
            return false;
        }
        return this.current < 4;
    }


    get showDone(){
        return this.current >= 4 || (this.rosterGroup == 'ADMINISTRATION' && this.current>=3);
    }

    get showDone2(){

            if (this.current>=4)
                return true;
            
            if (this.selectedActivity!=null){
                if (this.selectedActivity.service_Description == '' || this.selectedActivity.service_Description==null)
                    return false;
            }

            if(this.userSettings.useAwards.toString().toLowerCase()=='true' && this.current>1 && this.defaultActivity!=null)
                return true;

            if ((this.current <3 && this.viewType=="Staff") && (this.IsGroupShift ))
                return false;            
            else if ((this.current >=1 && this.viewType=="Staff") && (this.activity_value!=12 && !this.ShowCentral_Location ) && this.userSettings.useAwards.toString().toLowerCase()=='true')                
                return true;
            else if  (this.current >=1 && (this.booking_case==1 || this.booking_case==5 ))
                return true;  
            else if  (this.current >=1 && (!this.ShowCentral_Location && this.booking_case==8))
                return true;  
            else if  (this.current ==2 && (this.ShowCentral_Location && this.booking_case==8))
                return true;                                    
            else if ((this.current >=3 ) ) //&& this.viewType=="Recipient"
                return true;
            else
                return false;
    }
    get isFormValid(){
        return  this.rosterForm.valid;
    }
 isBookingValid(input:any):boolean{
        //return this.bookingForm.valid;
        if (
            input.billQty==null ||
            input.billTo ==null ||
            input.billUnit==null || 
            input.blockNo ==null ||
            input.carerCode==null ||
            input.clientCode==null ||
            input.costQty==null ||
            input.costUnit==null ||
            input.date==null ||
            input.dayno ==null ||
            input.duration ==null ||
            input.monthNo ==null ||
            input.program ==null ||
            input.serviceDescription == null ||
            input.serviceSetting == null ||
            input.serviceType  == null ||
            input.paytype  == null ||
            input.anal  == null ||            
            input.startTime  == null ||
            input.taxPercent  == null ||
            input.type  == null ||
            input.unitBillRate  == null ||
            input.unitPayRate  == null ||
            input.yearNo  == null ||
            input.creator  == null            
        )
         return false;
        else
         return true;
    }
   

    generate_alert(){
        this.show_alert=false;
        this.notes= this.txtAlertSubject + "\n" + this.txtAlertMessage;
        
        this.ProcessRoster("Alert","1");
    }
    FIX_CLIENTCODE_INPUT(tgroup: any): string{
        if (tgroup.serviceType == 'ADMINISTRATION' || tgroup.serviceType == 'ALLOWANCE NON-CHARGEABLE' || tgroup.serviceType == 'ITEM') {
            return "!INTERNAL"
        }

        if (tgroup.serviceType == 'SERVICE' || tgroup.serviceType == 'TRAVELTIME') {
            if (tgroup.isMultipleRecipient) {
                return "!MULTIPLE"
            }
            return tgroup.recipientCode;            
        }

        return tgroup.recipientCode;
    }

    fixStartTimeDefault() {
        const { time } = this.rosterForm.value;
        if (!time.startTime) {
            this.ngModelChangeStart(this.defaultStartTime)
        }

        if (!time.endTime) {
            this.ngModelChangeEnd(this.defaultEndTime)
        }
    }

    ngModelChangeStart(event): void{
        this.rosterForm.patchValue({
            time: {
                startTime: event
            }
        })
    }

    ngModelChangeEnd(event): void {
        this.rosterForm.patchValue({
            time: {
                endTime: event
            }
        })
    }
   

     // Add Timesheet
     confirm(index: number) {
        if (!this.selected && this.timesheets.length > 0) return;

        if (index == 1) {
           // this.resetAddTimesheetModal();
            this.addTimesheetVisible = true;
            this.whatProcess = PROCESS.ADD
        }
    }
        
       
    GET_GROUP_RECIPIENTS(): Observable<any>{
        
        let sql =`SELECT DISTINCT [Recipients].[UniqueID], [Recipients].[AccountNo], [Recipients].[Surname/Organisation], [Recipients].[FirstName], [Recipients].[DateOfBirth], [Recipients].[pSuburb]  
                    FROM Recipients INNER JOIN ServiceOverview ON Recipients.UniqueID = ServiceOverview.Personid
                    WHERE ServiceOverview.[Service Type] = '${this.defaultActivity.activity}' AND ServiceOverview.ServiceStatus = 'ACTIVE'
                    AND  AccountNo not in  ('!INTERNAL','!MULTIPLE') AND admissiondate is not null AND (DischargeDate is null)  
                    AND NOT EXISTS (SELECT * FROM  ROSTER 
                     WHERE Date = '${this.date}' AND [Start Time] = '${this.defaultStartTime}' AND ServiceSetting = '${this.serviceSetting}' 
                    AND Roster.[Client Code] = Recipients.AccountNo ) 
                    ORDER BY AccountNo`;
        

           // console.log(sql);
        return this.listS.getlist(sql);
}      

cancelRecipientExternal(value:any){
    this.recipientexternal=value.recipientexternal;
 }
 cancelStaffExternal(value:any){
    this.staffexternal=value.staffexternal;
 }
 

shiftChanged(value:any){
   if (this.viewType=='Staff' && value.show_alert){
        this.txtAlertSubject= 'SHIFT DAY/TIME CHANGE : ';
        this.txtAlertMessage= 'SHIFT TIME CHANGE : \n Date: ' + format(value.date,'dd/MM/yyyy') + '  \n Recipient: ' + value.clientCode + '\n'  ,
        this.clientCodes=value.clientCode;      
        if (this.Error_Messages.length<=0)
            this.show_alert=true;
    }
    this.searchRoster(value.date)
}



canDeactivate(): Promise<boolean> | boolean {
    var cancelled = false;
   
    this.Errors_Messages_summary = '';//JSON.stringify(this.Error_Messages)
       
    this.Error_Messages=this.Error_Messages.filter((value, index, self) => self.indexOf(value) === index)
  

    this.Error_Messages.forEach(element => {
      this.Errors_Messages_summary =this.Errors_Messages_summary + JSON.stringify(element) + "\n";
    });

    if(this.rosterFormGroup.dirty && this.Error_Messages.length>0){
      this.modalDeActivate = this.modal.create({
        nzTitle: this.customTitle,
        nzContent: this.customContent,
        nzKeyboard: false,
        nzOnOk: () => {
          return new Promise(resolve => setTimeout(resolve, 1000))
        },
        nzOnCancel: (e) => {
          cancelled = true;
        },
        nzFooter: this.customFooter
      });

      return new Promise((resolve,reject) => {
        this.modalDeActivate.afterClose
            .pipe(
                switchMap(x => {
                    if(this.cancelTrigger)  return of(true)
                    if(this.willSaveChanges) return of(true);
                    return of(false)
                })
            )
            .subscribe(result => {
               if(result){
                return resolve(true);
               }
              return resolve(false);
            });
      });
    }
    return true;
  }

  handleCancelChanges(){
    this.cancelTrigger = false;
    
    if(this.modalDeActivate)
        this.modalDeActivate.close();
    // this.recordsAdded.forEach(element => {
    //     this.ProcessRoster("Delete",element);
    // });
  }
  handleSaveChanges(){
    this.willSaveChanges = true;
    this.modalDeActivate.close();
  }

  initializeGrid() {
    this.gridData = Array.from({ length: this.rows }, () =>
      Array.from({ length: this.columns }, () => null)
    );
  }
  
  cellClicked(row: number, col: number) {
    console.log(`Cell clicked: Row ${row}, Column ${col}`);
    // Add your click event handling logic here
  }

 
  draggingCell: { row: number; col: number } | null = null;

// ... (unchanged code)

startDrag(event: MouseEvent, row: number, col: number) {
event.preventDefault();
console.log(`Drag started: Row ${row}, Column ${col}`);

// Set the draggingCell to the current cell
this.draggingCell = { row, col };

// Add the 'dragging' class to the cell
//event.currentTarget?.classList.add('dragging');
const target = event.currentTarget as Element;
target.classList.add('dragging');
// Add your drag initialization logic here
// You may want to store the initial mouse position and the cell being dragged
// and update the cell position during the drag

}




      //MainSpread:any=GC.Spread;
    workbookInit(args) {  
      
        let spread: GC.Spread.Sheets.Workbook = args.spread;
       // this.MainSpread=args.spread;
        this.spreadsheet = args.spread;  
        spread = args.spread;  
        let sheet = spread.getActiveSheet();  
        
        sheet.setRowCount(this.time_slot, GC.Spread.Sheets.SheetArea.viewport);
        sheet.setColumnCount(31,GC.Spread.Sheets.SheetArea.viewport);
      
  
          spread.suspendPaint();
          let spreadNS = GC.Spread.Sheets;
          let self = this;
  
        
        spread.options.columnResizeMode = GC.Spread.Sheets.ResizeMode.split;
        spread.options.rowResizeMode = GC.Spread.Sheets.ResizeMode.split;
        spread.options.scrollbarAppearance = GC.Spread.Sheets.ScrollbarAppearance.mobile;
        spread.options.scrollByPixel = true;
        spread.options.scrollPixel = 5;
        sheet.options.selectionBorderColor = 'blue';
       // sheet.options.selectionBackColor = '#BDCED1';
       //sheet.options.selectionBackColor = 'transparent';
      
        spread.options.newTabVisible = false;
       
      
        
        var command = {
          canUndo: true,
          execute: function (context, options, isUndo) {
            var Commands = GC.Spread.Sheets.Commands;
            options.cmd = "ContextMenu";
            if (isUndo) {
              Commands.undoTransaction(context, options);
              return true;
            } else {
              Commands.startTransaction(context, options);
              var sheet = context.getSheetFromName(options.sheetName);
              var cell = sheet.getCell(options.row, options.col);
              cell.backColor(options.backColor);
              Commands.endTransaction(context, options);
              return true;
            }
          }
        };
        spread.commandManager().register('ContextMenu',command);
     
        spread.commandManager().register('myCopy',
        function AddRow() {                   
            //Click on a cell and press the Enter key.      
            console.log("ctrl+c");     
            spread.commandManager().execute({cmd: "Copy", sheetName: self.sheetName, index: 3, count: 5});
        }
      );
          spread.commandManager().register('myPast',
          function AddRow() {                   
             
              spread.commandManager().execute({cmd: "Paste", sheetName: self.sheetName, index: 3, count: 5});
          }
          );
          spread.commandManager().register('myCut',
          function AddRow() {                   
             
              spread.commandManager().execute({cmd: "Cut", sheetName: self.sheetName, index: 3, count: 5});
          }
          );
          spread.commandManager().register('myDelete',
          function AddRow() {                   
             
              spread.commandManager().execute({cmd: "Delete", sheetName: self.sheetName, index: 3, count: 5});
          }
          );
          spread.commandManager().setShortcutKey('myCopy', GC.Spread.Commands.Key.c, true, false, false, false);
          spread.commandManager().setShortcutKey('myPast', GC.Spread.Commands.Key.v, true, false, false, false);
          spread.commandManager().setShortcutKey('myCut', GC.Spread.Commands.Key.x, true, false, false, false);
          spread.commandManager().setShortcutKey('myDelete', GC.Spread.Commands.Key.del, true, false, false, false);
         
         
        // self.AddFloatingObjectTemp();
  
          let floatingObjectEvent = document.getElementById('floatingObjectEvent') as HTMLTextAreaElement;
  
         spread.bind(GC.Spread.Sheets.Events.FloatingObjectChanged, function (e, args) {
              if (args.floatingObject==null) return;
              let floatingObject = args.floatingObject;
              let host = floatingObject.getHost();
              
              if (host[0]==null) return;
              if (host) {
                  if (!host[0].getAttribute('clickListeners')) {
                      
  
                      host[0].addEventListener("click", function(e: any) {
                          console.log("click");
                         // self.showContextMenu=true;
                      });
                      host[0].addEventListener("contextmenu", function(e: any) {
                          console.log("right click");
                        //  var value = new GC.Spread.Sheets.ContextMenu.ContextMenu();
                         // var  value2 = spread.commandManager();
                         setTimeout(function(){
                          
                          self.reloadMenu.next(true)
                          spread.commandManager().execute({cmd: "ContextMenu", sheetName: self.sheetName, row: 1, col: 2, backColor: "red", index: 3, count: 5});
       
                         } ,100);
                      });
                      host[0].setAttribute('clickListeners', true)
                      
                  } 
              }
          });
      
            
          spread.bind(GC.Spread.Sheets.Events.SheetTabClick, function (sender, args) {
              if (args.sheet === null && args.sheetName === null) {
                  console.log("New button Clicked, new sheet added");
                  self.personList.push(this.data);
                  sheet = spread.getActiveSheet();  
                  self.prepare_Sheet();
              }
              else {
                  console.log(args.sheetName + " clicked");
                  sheet = spread.getActiveSheet();  
              }
          });
  
          spread.bind(GC.Spread.Sheets.Events.SheetChanged, function (sender, args) {
  
              this.data=  self.personList[args.sheetIndex ]
            
                  self.load_rosters();
          });
              sheet.bind(GC.Spread.Sheets.Events.LeaveCell, function (event, infos) {
                 var res:string = sheet.getCell(0, infos.col,GC.Spread.Sheets.SheetArea.colHeader).value()
                 // Reset the backcolor of cell before moving
                 if(1==1) return;
                 spread.suspendPaint();
                  
                  if ( res.endsWith("Sat") || res.endsWith("Sun") || res.endsWith("Saturday") || res.endsWith("Sunday")){                    
                      infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).backColor("#FFDEDB");
                      infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).foreColor("#000000");
                   } else{
                      infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).backColor("#F6F6F6");
                      infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).foreColor("#000000");
                      
                  }
                  
                      spread.resumePaint();
                  });
  
                  
      sheet.bind(GC.Spread.Sheets.Events.EnterCell, function (event, infos) {
         
           //infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).backColor("#002060");
          //  infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).foreColor("#ffffff");
          // infos.sheet.getCell(infos.row, infos.col).backColor("#002060");
          var row= infos.row;
          var col= infos.col;
          if (sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)!=null) {
              self.cell_value=sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)
          }
          if(1==1) return;
        
  
          var res:string = sheet.getCell(0, infos.col,GC.Spread.Sheets.SheetArea.colHeader).value()
          
          if ( res.endsWith("Sat") || res.endsWith("Sun")){
                      
              infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).backColor("#ffffff");
              infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).foreColor("#000000");
           } else{
              infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).backColor("#002060");
              infos.sheet.getCell(0, infos.col, GC.Spread.Sheets.SheetArea.colHeader).foreColor("#ffffff");
              
          }
          spread.resumePaint();
      
      
      });
              
           
      spread.bind(spreadNS.Events.CellClick, function (e: any, args: any) {
              let row,col, duration=0,type=0,service;
              
                let sheetArea = args.sheetArea === 0 ? 'sheetCorner' : args.sheetArea === 1 ? 'columnHeader' : args.sheetArea === 2 ? 'rowHeader' : 'viewPort';
                
                if(args.sheetArea==1 || args.sheetArea==2){
                  sheet.options.isProtected = false;
                  return;
  
                }
  
              
               
                sheet.options.isProtected = true;                
                
                self.eventLog =
                    'SpreadEvent: ' + GC.Spread.Sheets.Events.CellClick + ' event called' + '\n' +
                    'sheetArea: ' + sheetArea + '\n' +
                    'row: ' + args.row + '\n' +
                    'col: ' + args.col;      
              
                    console.log(self.eventLog );
                    row=args.row;
                    col=args.col;
                    let selection:any =sheet.getSelections();
  
                    if (row<=0 || selection[0].colCount>1 ||selection[0].rowCount>1) return;
                   // if (1==1) return;
                    spread.suspendPaint();
  
                    if (self.prev_cell.duration==null || self.prev_cell.duration==0)
                    self.prev_cell.duration=1;
                    var new_duration:number;
                   // setting back previous selected cells
                   duration=Number(self.prev_cell.duration)
                   
                    if (self.time_slot==288)
                      new_duration=Number(duration);
                    else if (self.time_slot==144)
                      new_duration= Math.ceil(Number(duration)/2); 
                    else if (self.time_slot==96)
                      new_duration= Math.ceil(Number(duration)/3);             
                  if (new_duration<=0)
                      new_duration=1;
                   
                   // sheet.getRange(self.prev_cell.row, self.prev_cell.col, new_duration, 1, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#C3C1C1", GC.Spread.Sheets.LineStyle.thin), {all:true});
     
                    if (sheet.getTag(self.prev_cell.row,self.prev_cell.col,GC.Spread.Sheets.SheetArea.viewport)!=null) {
                      sheet.getRange(self.prev_cell.row, self.prev_cell.col, new_duration, 1, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#FEFFF0", GC.Spread.Sheets.LineStyle.thin), {all:true});
     
                      let prev_rows=self.prev_cell.row+new_duration
                      var cell = sheet.getCell(self.prev_cell.row, self.prev_cell.col, GC.Spread.Sheets.SheetArea.viewport);
                      cell.borderTop(new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));  
                      
                      cell = sheet.getCell(prev_rows-1, self.prev_cell.col, GC.Spread.Sheets.SheetArea.viewport);
                      cell.borderBottom(new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));  
                    
                     sheet.getRange(self.prev_cell.row, self.prev_cell.col, new_duration, 1, GC.Spread.Sheets.SheetArea.viewport).borderLeft( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));
                     sheet.getRange(self.prev_cell.row, self.prev_cell.col, new_duration, 1, GC.Spread.Sheets.SheetArea.viewport).borderRight( new GC.Spread.Sheets.LineBorder("#f9cf9b",GC.Spread.Sheets.LineStyle.thin));
                   
                     //sheet.setRowCount(this.time_slot, GC.Spread.Sheets.SheetArea.viewport);
                    }       
  
                   ///---------------------setting current cell
                   if (sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)!=null) {
                      self.cell_value=sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)
                      
                      row=self.cell_value.row
                      col=self.cell_value.col
                      duration=Number(self.cell_value.duration)
                      type=self.cell_value.type;
                      service=self.cell_value.service;
                   
        
  
                    if (service!=null)
                      self.ActiveCellText=self.cell_value.recordNo + " - " + service 
                   
                  
                    if (self.time_slot==288)
                      new_duration=Number(duration);
                    else if (self.time_slot==144)
                      new_duration= Math.ceil(Number(duration)/2); 
                    else if (self.time_slot==96)
                      new_duration= Math.ceil(Number(duration)/3); 
              
                  if (new_duration<=0)
                      new_duration=1;
  
                        // duration=10;
                    // Allow selection of multiple ranges
                    sheet.selectionPolicy(GC.Spread.Sheets.SelectionPolicy.multiRange);
                   
                    // Create two different selection ranges.
                   sheet.addSelection(row, col, new_duration, 1);
                   // sheet.addSpan(row, col, new_duration,1 ,GC.Spread.Sheets.SheetArea.viewport);
  
                   let len =row+new_duration;
  
                     for (let i=row; i<len; i++){
                      var cell = sheet.getCell(i, col, GC.Spread.Sheets.SheetArea.viewport);
                        cell.borderLeft(new GC.Spread.Sheets.LineBorder("blue", GC.Spread.Sheets.LineStyle.medium));
                        cell.borderRight(new GC.Spread.Sheets.LineBorder("blue", GC.Spread.Sheets.LineStyle.medium));      
                        if (i==row)
                          cell.borderTop(new GC.Spread.Sheets.LineBorder("blue", GC.Spread.Sheets.LineStyle.medium));
                     
                        if (i==len-1)
                          cell.borderBottom(new GC.Spread.Sheets.LineBorder("blue",GC.Spread.Sheets.LineStyle.medium));                      
                       
                        
                    }
                  }else {
                    
                    // sheet.getCell(row, col).backColor("#cfcfca");
                    // sheet.getCell(row, col).setBorder(new GC.Spread.Sheets.LineBorder("#C3C1C1", GC.Spread.Sheets.LineStyle.thin), {all:true});
                   
              }
                    self.prev_cell = {row,col,duration, type,service};    
                         
                    spread.resumePaint();
            });
            spread.bind(GC.Spread.Sheets.Events.CellDoubleClick, function (sender, args) {
              console.log("Double clicked column index: " + args.row + ", " + args.col);
              //console.log("Double clicked row index: " + args.row);
              let col= args.col;
              let row=args.row;
  
              if (row<=0) return;
              
              self.ActiveCellText="";
              //sheet.getRange(self.prev_cell.row, self.prev_cell.col, self.prev_cell.duration, 1, GC.Spread.Sheets.SheetArea.viewport).setBorder(new GC.Spread.Sheets.LineBorder("#C3C1C1", GC.Spread.Sheets.LineStyle.thin), {all:true});
             if (self.prev_cell.service==null)
              sheet.getCell(self.prev_cell.row, self.prev_cell.col).backColor("#ffffff");
  
              self.cell_value=sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)
             
              if(self.cell_value == null){
                  return;
              }
              
              let data:any = self.find_roster(self.cell_value.recordNo);
             
              if (data!=null)
                  self.details(data);
  
              if(args.sheetArea === GC.Spread.Sheets.SheetArea.colHeader){
                  console.log("The column header was double clicked.");
              }
          
              if(args.sheetArea === GC.Spread.Sheets.SheetArea.rowHeader){
                  console.log("The row header was double clicked.");
              }
          
              if(args.sheetArea === GC.Spread.Sheets.SheetArea.corner){
                  console.log("The corner header was double clicked.");
              }
          
              
          });
          //   spread.bind(spreadNS.Events.SelectionChanging, function (e: any, args: any) {
          //       let selection = args.newSelections.pop();
          //       let sheetArea = args.sheetArea === 0 ? 'sheetCorner' : args.sheetArea === 1 ? 'columnHeader' : args.sheetArea === 2 ? 'rowHeader' : 'viewPort';
          //       self.eventLog =
          //           'SpreadEvent: ' + GC.Spread.Sheets.Events.SelectionChanging + ' event called' + '\n' +
          //           'sheetArea: ' + sheetArea + '\n' +
          //           'row: ' + selection.row + '\n' +
          //           'column: ' + selection.col + '\n' +
          //           'rowCount: ' + selection.rowCount + '\n' +
          //           'colCount: ' + selection.colCount;
                 
               
          //   });
            spread.bind(spreadNS.Events.SelectionChanged, function (e: any, args: any) {
                let selection = args.newSelections;
                //if (selection.rowCount > 1 && selection.colCount > 1) {
                  //   let sheetArea = args.sheetArea === 0 ? 'sheetCorner' : args.sheetArea === 1 ? 'columnHeader' : args.sheetArea === 2 ? 'rowHeader' : 'viewPort';
                    
                  //   if(args.sheetArea==1 || args.sheetArea==2){
                  //       return;
                  //   }
                  //   return;
                    self.eventLog =
                        'SpreadEvent: ' + GC.Spread.Sheets.Events.SelectionChanged + ' event called' + '\n' +
                        
                        'row: ' + selection.row + '\n' +
                        'column: ' + selection.col + '\n' +
                        'rowCount: ' + selection.rowCount + '\n' +
                        'colCount: ' + selection.colCount;
  
                        var len =selection[0].rowCount;
                        var cols = selection[0].colCount;
                        var row=selection[0].row;
                        var col = selection[0].col;
                        
  
                        spread.suspendPaint()
                        
                  
                  // Set the backcolor and forecolor for the entire column header.
                  var columns = sheet.getRange(0,col, len, cols, GC.Spread.Sheets.SheetArea.colHeader);
                  //columns.backColor("#002060");
                  //columns.foreColor("White");
  
                  // Set the backcolor of second row header.
                  //sheet.getCell(row, 0, GC.Spread.Sheets.SheetArea.rowHeader).backColor("Yellow");
                  var rows = sheet.getRange(row,0, len, 0, GC.Spread.Sheets.SheetArea.rowHeader);
                //  rows.backColor("#002060");
                 // rows.foreColor("White");
  
        
                      spread.resumePaint();
                      return;
  
                     
  
                
            });
            spread.bind(spreadNS.Events.EditStarting, function (e: any, args: any) {
                self.eventLog =
                    'SpreadEvent: ' + GC.Spread.Sheets.Events.EditStarting + ' event called' + '\n' +
                    'row: ' + args.row + '\n' +
                    'column: ' + args.col;
            });
            spread.bind(spreadNS.Events.EditEnded, function (e: any, args: any) {
                self.eventLog =
                    'SpreadEvent: ' + GC.Spread.Sheets.Events.EditEnded + ' event called' + '\n' +
                    'row: ' + args.row + '\n' +
                    'column: ' + args.col + '\n' +
                    'text: ' + args.editingText;
            });
    
          var menuData = spread.contextMenu.menuData;
          var sperator;
          menuData.forEach(function (item) {
              if(item){
                  if(item.type === "separator") {
                       
                    sperator=item;
                      return;
                  }
                  
              }
          });
    
            var newMenuData = [];
            var selected_Cell; 
            
            var copy = {
              iconClass : "gc-spread-copy",
              name : "Copy",
              text : "Copy    (Ctrl+C)",
              command : "Copy",
              workArea : "viewportcolHeaderrowHeadercorner"
          };       
            newMenuData.push(copy);
    
            var cut = {
              iconClass : "gc-spread-cut",
              name : "Cut",
              text : "Cut     (Ctrl+X)",
              command : "Cut",
              workArea : "viewportcolHeaderrowHeadercorner"
          };
            newMenuData.push(cut);
    
            var paste = {
              iconClass : "gc-spread-pasteAll",
              name : "Paste",
              text : "Paste   (Ctrl+V)",
              command : "Paste",
              workArea : "viewportcolHeaderrowHeadercorner"
          };       
            newMenuData.push(paste);     
           
            newMenuData.push(sperator);
    
            var del = {
              iconClass : "gc-spread-delete",
              name : "Delete",
              text : "Delete  (Ctrl+Del)",
              command : "Delete",
              workArea : "viewportcolHeaderrowHeadercorner"
          };
            newMenuData.push(del);
            newMenuData.push(sperator);
    
            var realocate = {         
              name : "ReAllocateStaff",
              text : "Re-Allocate Staff",
              command : "ReAllocateStaff",
              workArea : "viewportcolHeaderrowHeadercorner"
          };
            newMenuData.push(realocate);
           
            var unalocate = {         
              name : "UnAllocateStaff",
              text : "Un-Allocate Staff",
              command : "UnAllocateStaff",
              workArea : "viewportcolHeaderrowHeadercorner"
          };
            newMenuData.push(unalocate);
            newMenuData.push(sperator);
    
            var multi = {         
              name : "MultiShift",
              text : "Set MultiShift",
              command : "MultiShift",
              workArea : "viewportcolHeaderrowHeadercorner"
          };
            newMenuData.push(multi);
            var clear_multi = {         
              name : "ClearMultiShift",
              text : "Clear MultiShift",
              command : "ClearMultiShift",
              workArea : "viewportcolHeaderrowHeadercorner"
          };
            newMenuData.push( clear_multi);
    
            newMenuData.push(sperator);
            
            var viewStaff = {
              text:    'View Staff Detail',
              name: 'ViewStaffDetail',
              command:   "ViewStaffDetail",
              workArea: 'viewport'
          };        
             newMenuData.push(viewStaff);
    
             var viewRecipient = {
              text:    'View Recipient Detail',
              name: 'ViewRecipientDetail',
              command:   "ViewRecipientDetail",
              workArea: 'viewport'
          };        
             newMenuData.push(viewRecipient);
  
             var viewService= {
              text: 'View Service Detail',
              name: 'ViewServiceDetail',
              command: "ViewServiceDetail",
              workArea: 'viewport'
          };        
             newMenuData.push(viewService);
  
             var openDialog = {
              text: 'View Additional Information (Xtra Info)',
              name: 'ViewAdditional',
              command: "ViewAdditional",
              workArea: 'viewport'
          }; 
  
             newMenuData.push(openDialog);
             spread.contextMenu.menuData = newMenuData;
            
             spread.commandManager().register("Copy",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                    // add cmd here
                      options.cmd = "gc.spread.contextMenu.copy";
                   // options.cmd = "Copy";
                    
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                         
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                          self.operation="copy";
                          console.log("Row=" + sel.row  + ", col=" + sel.col)
                          selected_Cell=sel;
                          
    
                          if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)!=null)
                            self.copy_value=sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)
                          else
                            self.copy_value={row:-1,col:-1,duration:-1}
    
                          //  if (self.cell_value!=null)
                           //  self.current_roster = self.find_roster(self.cell_value.RecordNo);
                       
  
                       
                          if(sels && sels.length > 0){
                            console.log("Copy Operation\n" + sel + "\n");
                             
                          }
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              spread.commandManager().register("Cut",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "Cut";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
    
                          var sheet = spread.getActiveSheet();
                          console.log("Cut Operation")
                          self.operation="cut";
                          var sheet = spread.getActiveSheet();
                         
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                         
                          selected_Cell=sel;
                          
    
                          if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)!=null)
                            self.copy_value=sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)
                          else
                            self.copy_value={row:-1,col:-1,duration:-1}
                          //  if (self.cell_value!=null)
                             // self.current_roster = self.find_roster(self.cell_value.RecordNo);
                         
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });  
              spread.commandManager().register("Paste",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                      
                                   // add cmd here
                      //options.cmd = "gc.spread.contextMenu.pasteAll";
                      options.cmd = "Paste";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                          spread.suspendPaint()
                         // sheet.options.isProtected = false;
                          console.log("Paste Operation")
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var col = sel.col;
                          var row = sel.row;
                          console.log(selected_Cell);     
                          
                          let selected_columns = selected_Cell.col + selected_Cell.colCount;
                          
                          col=col-1;
                          self.selected_Cell=selected_Cell;
                          self.sel=sel;
                          var recordsData:Array<any>=[];
                          self.pasting=true;
                          let conflict:boolean=false;
                          let data_row=0;
                          let row_iterator=0;
                          let recdNo=0;
                          self.processing = true;
                          let rstdate:any;
                          let Records_to_paste:Array<any>=[];
                        
  
                              for (let i=selected_Cell.col; i<selected_columns && !conflict; i++){   
                                  data_row=selected_Cell.row;    
                                  col=col+1;
                                  rstdate=sheet.getTag(0,col,GC.Spread.Sheets.SheetArea.colHeader)
                                  // self.copy_value=sheet.getTag(selected_Cell.row,i,GC.Spread.Sheets.SheetArea.viewport)
                                  // if (self.copy_value==null) continue;
                                  // if (self.copy_value.recordNo==null || self.copy_value.recordNo==0) continue;
                                  //     recordsData.push(self.Check_BreachedRosterRules_Paste(self.copy_value.recordNo,self.operation,sel.row,sel.col))
  
                                  for ( row_iterator=0; row_iterator<selected_Cell.rowCount; row_iterator++){
                                                     
                                      self.copy_value=sheet.getTag(data_row,i,GC.Spread.Sheets.SheetArea.viewport)
                                      
                                      if (self.copy_value==null) {
                                          data_row=data_row+1;
                                          continue;
                                      }
                                      if (self.copy_value.recordNo==null || self.copy_value.recordNo==0){
                                          data_row=data_row+1;
                                          continue;
                                      } 
                                      if (self.copy_value.recordNo==recdNo){                                  
                                          data_row=data_row+1;   
                                          continue;
                                      }
                                      recdNo=self.copy_value.recordNo;
                                      recordsData.push(self.Check_BreachedRosterRules_Paste(self.copy_value.recordNo,self.operation,data_row,i))
                                      data_row=data_row+1;
  
                                      //code for checking broken shifts 
                                      let rst = self.current_roster;
                                      self.defaultActivity={"activity" : rst.activity, jobtype:''}
                                      let dt1 = new Date(rst.date+' ' + rst.startTime );
                                      let dt2 = new Date(rst.date + ' ' + rst.endTime);   
  
                                      Records_to_paste.push({"recordNo": recdNo,
                                                            "rstdate" :rstdate,
                                                            "startTime" :dt1,
                                                            "endTime": dt2  ,
                                                            "defaultActivity" : self.defaultActivity
                                                          });
  
                                      
                                  }                                
                                                                                                
                              }   
                              
                              
                                  let tt=JSON.stringify(recordsData)
                                  //console.log(tt);
                                  self.timeS.pastingRosters(tt).subscribe(data=>{
                                      let res=data[0];
                                     
                                      if (res.errorValue>0){
                                          
                                          self.Error_Msg=res.errorValue +", "+ res.msg ;  
                                          
                                          this.Error_Messages.push( format(this.date,'yyyy/MM/dd') + ' ' + format(this.defaultStartTime,'HH:mm') + ' ' +  self.Error_Msg); 
                                          self.breachRoster=true; 
                                      }else{
                                         
                                          
  
                                          if (Records_to_paste.length<=5){
                                              self.processing=true;
                                              Records_to_paste.forEach(x => {                                                
                                              
                                              //  let x= Records_to_paste[0];
                                                 self.CheckBrokenShift(x.rstdate,x.startTime,x.endTime,true);
                                                   
                                              });
                                            
                                              self.processing=false;
                                            }else{     
                                                  self.Pasting_Records(selected_Cell,sel) ;                            
                                            }
                                      }
                                  });  
                          
                      
                                        
                          Commands.endTransaction(context, options);
                        //  sheet.options.isProtected = true;
                          spread.resumePaint();
                         
                        
                         // self.load_rosters();
                          return true;
                      }
                  }
              });
              spread.commandManager().register("Delete",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "Delete";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
    
                          var sheet = spread.getActiveSheet();
                        
                          console.log("Delete Operation")
                         
                          var sheet = spread.getActiveSheet();
                          
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                 
                          selected_Cell=sel;
                          
                         // if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)==null)                       
                         //     return;
  
                           
                         // self.deleteRosterModal=true;   
                          self.showConfirm();
                          self.operation="Delete";    
                          Commands.endTransaction(context, options);
  
                        
                          return true;
                      }
                  }
              });  
  
              
              spread.commandManager().register("ReAllocateStaff",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "ReAllocateStaff";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                         
                          var sheet = spread.getActiveSheet();
                          
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                         
                          selected_Cell=sel;
    
                          if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)!=null)
                            self.cell_value=sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)
                          else
                            self.cell_value={row:-1,col:-1,duration:-1}
  
                          self.searchStaffModal=true;
                          
                          console.log("Reallocate Event called");
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              spread.commandManager().register("UnAllocateStaff",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "UnAllocateStaff";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                         
                          selected_Cell=sel;
    
                          if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)!=null)
                            self.cell_value=sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)
                          else
                            self.cell_value={row:-1,col:-1,duration:-1}
  
                          self.UnAllocateStaffModal=true;
                          
                                                  
                          console.log("UnAllocateStaff Event called");
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              spread.commandManager().register("MultiShift",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "MultiShift";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                       
                          var sheet = spread.getActiveSheet();
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                         
                          selected_Cell=sel;
    
                          if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)!=null)
                            self.cell_value=sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)
                          else
                            self.cell_value={row:-1,col:-1,duration:-1}
                            self.SetMultiShiftModal=true;                    
                          console.log("MultiShift Event called");
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              
              spread.commandManager().register("ClearMultiShift",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "ClearMultiShift";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                         
                          selected_Cell=sel;
    
                          if (sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)!=null)
                            self.cell_value=sheet.getTag(sel.row,sel.col,GC.Spread.Sheets.SheetArea.viewport)
                          else
                            self.cell_value={row:-1,col:-1,duration:-1}
                            
                            self.ClearMultiShiftModal=true;                
                          console.log("ClearMultiShift Event called");
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              
  
              spread.commandManager().register("ViewStaffDetail",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "ViewStaffDetail";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                          //self.ViewStaffDetail=true;
                          if (self.viewType=='Recipient'){
                              let ss:any= self.selected;
                              self.current_roster = self.find_roster(self.cell_value.recordNo);
                               self.selected_data ={ data:self.current_roster.staffCode, option:0}
                            
                             // self.router.navigate(['/roster/recipient-external',  {AccountNo: data}]);
                             self.staffexternal=true;
                          //    if (self._dynamicContainerStaffExternal==null)
                          //         self.loadComponentAsync_Staff();
                          //     else{
                          //         self.dynamicContainerStaffExternal=null;
                          //         self._dynamicContainerStaffExternal=null;
                          //         self.loadComponentAsync_Staff();
                                  
                          //     }
                         }else{
                          self.showRecipientStaffDetail();
                         }
                        
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              
              spread.commandManager().register("ViewRecipientDetail",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "ViewRecipientDetail";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                          //self.ViewStaffDetail=true;
                         // self.showRecipientStaffDetail();
                         if (self.viewType=='Staff'){
                              let ss:any= self.selected;
                              self.current_roster = self.find_roster(self.cell_value.recordNo);
                               self.selected_data ={ data:self.current_roster.recipientCode, option:1}
                            
                             // self.router.navigate(['/roster/recipient-external', {AccountNo: self.selected_data}]);
                              self.recipientexternal=true;
                              // if (self._dynamicContainerRecipientexternal==null)
                              //     self.loadComponentAsync_Recipient();
                              // else{
                                  
                              //     self.dynamicContainerRecipientexternal=null;
                              //     self._dynamicContainerRecipientexternal=null;
                              //     self.loadComponentAsync_Recipient();
                                 
                              // }
                         }else{
                          self.showRecipientStaffDetail();
                         }
  
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              
              spread.commandManager().register("ViewServiceDetail",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "ViewServiceDetail";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                        
                          var sheet = spread.getActiveSheet();
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                          var col= sel.col;
                         
              
                          self.cell_value=sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)
                         
                          let data:any = self.find_roster(self.cell_value.recordNo);
                         
                          if (data!=null)
                              self.details(data);
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
              
              spread.commandManager().register("ViewAdditional",
              {
                  canUndo: true,
                  execute: function (context, options, isUndo) {
                      var Commands = GC.Spread.Sheets.Commands;
                                   // add cmd here
                      options.cmd = "ViewAdditional";
                      if (isUndo) {
                          Commands.undoTransaction(context, options);
                          return true;
                      } else {
                          Commands.startTransaction(context, options);
                          var sheet = spread.getActiveSheet();
                          var sels = sheet.getSelections();
                          var sel = sels[0];
                          var row = sel.row;
                          var col= sel.col;
                         
              
                          self.cell_value=sheet.getTag(row,col,GC.Spread.Sheets.SheetArea.viewport)
                         
                          let data:any = self.find_roster(self.cell_value.recordNo);
                          self.notes ="";
                          self.notes=data.notes;
                          self.defaultStartTime=data.startTime;
                          self.date=data.date;
                          self.serviceType=data.serviceActivity;
  
                          self.ViewAdditionalModal=true;
                          console.log("ViewAdditional event called");
    
                          Commands.endTransaction(context, options);
                          return true;
                      }
                  }
              });
  
              // sheet.options.isProtected = true;
          spread.options.allowContextMenu = true;
  
          
          // //--------------------------Setting Border of Active Cell----------------------------------
         
       
          // sheet.options.selectionBorderColor = "blue";
          // sheet.options.selectionBackColor = "rgba(155, 225, 230, 0.2)";
          // sheet.options.selectedBorderColor = "blue";
  
          // var style = new GC.Spread.Sheets.Style();
          // style.font = "bold 22px Arial";
          // style.foreColor = "red";
          // style.backColor = "#D3F0E0";
          // style.hAlign = GC.Spread.Sheets.HorizontalAlign.center;
          // style.vAlign = GC.Spread.Sheets.VerticalAlign.center;
  
          // for (var i = 0; i < 7; i++) {
          //     sheet.setStyle(1, i, style, GC.Spread.Sheets.SheetArea.colHeader);
          //     sheet.setStyle(2, i, style, GC.Spread.Sheets.SheetArea.colHeader);
          //     sheet.setStyle(3, i, style, GC.Spread.Sheets.SheetArea.colHeader);
          // }
          
          // sheet.options.style=style;
   
    
            spread.resumePaint();
        
            self.prepare_Sheet();
  
           
            
    }  
}


