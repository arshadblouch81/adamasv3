using Microsoft.Graph;
using Azure.Identity;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

public class CustomAuthenticationProvider : IAuthenticationProvider
{
    private readonly ClientSecretCredential _credential;

    public CustomAuthenticationProvider(ClientSecretCredential credential)
    {
        _credential = credential;
    }

    public async Task AuthenticateRequestAsync(HttpRequestMessage request)
    {
        // Get the access token
        var token = await _credential.GetTokenAsync(
            new Azure.Core.TokenRequestContext(new[] { "https://graph.microsoft.com/.default" }),
            default);

        // Add the token to the request headers
        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.Token);
    }
}