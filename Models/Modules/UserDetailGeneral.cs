using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Adamas.Models.Modules{
    public class UserDetailGeneral {
        // first tab
        public int Recnum { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public string usertype { get; set; }
        public string staffCode { get; set; }
        public string loginMode { get; set; }
        public string homeBranch { get; set; }
        public int system { get; set; }
        public int Recipients { get; set; }
        public int Staff { get; set; }
        public int Roster { get; set; }
        public int DayManager {get;set;}
        public int timesheet {get;set;}
        public Boolean? timesheetPreviousPeriod { get; set; }
        public int statistics {get;set;}
        public int financial {get;set;}
        public Boolean? reportPreview { get; set; }
        public int invoiceEnquiry { get; set; }
        public Boolean? allowTypeAhead { get; set; }
        public int suggestedTimesheets { get; set; }
        public Boolean? payIntegrityCheck { get; set; }
        public int timesheetUpdate { get; set; }
        public Boolean? accessCDC { get; set; }
    }
        
    
}