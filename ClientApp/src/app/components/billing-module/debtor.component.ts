import { ChangeDetectorRef, Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { Router } from '@angular/router';
import format from 'date-fns/format';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SwitchService } from '@services/switch.service';
// import { ListService, MenuService } from '@services/index';
import { BillingService, TimeSheetService, GlobalService, ListService, MenuService, DllService } from '@services/index';
import { setDate, startOfMonth, toDate } from 'date-fns';
import { FormsModule } from '@angular/forms';
import { debounceTime, timeout, switchMap, takeUntil } from 'rxjs/operators';
import { formatDate } from '@angular/common';
import endOfMonth from 'date-fns/endOfMonth';
import { forkJoin, Observable, EMPTY, Subject, combineLatest } from "rxjs";


import { first } from 'lodash';
import { constrainPoint } from '@fullcalendar/angular';

@Component({
  selector: 'app-debtor-update',
  styleUrls: ['./debtor.component.css'],
  templateUrl: './debtor.component.html',
})
export class DebtorComponent implements OnInit {

  @Input() open: any;
  @Input() option: any;
  @Input() user: any;

  isVisible: boolean = false;

  loggedComputerName: any;
  loggedUserName: any;

  branchList: Array<any>;
  selectedBranches: any;
  allBranchesChecked: boolean;
  lockBranches: any = false;

  programList: Array<any>;
  selectedPrograms: any;
  allProgramsChecked: boolean;
  lockPrograms: any = false;

  categoriesList: Array<any>;
  selectedCategories: any;
  allCategoriesChecked: boolean;
  lockCategories: any = false;

  fundingList: Array<any>;
  selectedFunding: any;
  allFundingChecked: boolean;
  lockFunding: any = false;

  recipientsList: Array<any>;
  selectedRecipients: any;
  allRecipientsChecked: boolean;
  lockRecipients: any = false;

  billingCycleList: Array<any>;
  selectedBillingCycle: any;
  selectedWeeks: any;
  lockBillingCycle: any = false;
  allWeeksChecked: boolean;
  checkedWeekly: boolean;
  checkedFornightly: boolean;
  checkedMonthly: boolean;
  checked4Weekly: boolean;
  checkedMonthly1: boolean;
  checkedWeekly1: boolean;

  batchHistoryList: Array<any>;
  debtorRecordList: Array<any>;
  weeksList: Array<any>;
  tableData: Array<any>;
  PayPeriodLength: number;
  PayPeriodEndDate: any;
  loading: boolean = false;
  allchecked: boolean = false;
  modalOpen: boolean = false;
  billingType: Array<any>;
  invLineType: Array<any>;
  gstSetting: Array<any>;
  myOB: Array<any>;
  AccountPackage: Array<any>;
  invType: Array<any>;
  invoiceType: any;
  current: number = 0;
  inputForm: FormGroup;
  dateFormat: string = 'dd/MM/yyyy';
  postLoading: boolean = false;
  title: string = "Debtor Updates and Exports";
  token: any;
  tocken: any;
  check: boolean = false;
  userRole: string = "userrole";
  chkAllDates: boolean;
  endDate: any;
  startDate: any;
  id: string;
  private unsubscribe: Subject<void> = new Subject();
  indeterminate = true;
  batchNumber: any;
  BatchDetail: any;
  BatchType: any;
  operatorID: any;
  updatedRecords: any;
  currentDateTime: any;
  bankDetailsString: any;
  chkSepInvRecipient: any;
  chkSepPageRecipient: any;
  chkPrintZeroInvoices: any;
  chkPrintZeroInvoiceLine: any;
  chkGenInternalInvoice: any;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private globalS: GlobalService,
    private listS: ListService,
    private formBuilder: FormBuilder,
    private menuS: MenuService,
    private timesheetS: TimeSheetService,
    private billingS: BillingService,
    private Dlls: DllService
  ) { }

  ngOnInit(): void {
    this.token = this.globalS.decode();
    this.buildForm();
    this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA) : this.globalS.decode();
    this.userRole = this.tocken.role;
    this.modalOpen = true;
  }
  loadTitle() {
    return this.title
  }
  handleCancel() {
    this.inputForm.reset();
    this.buildForm();
    this.postLoading = false;
    this.modalOpen = false;
    this.isVisible = false;
    this.router.navigate(['/admin/billing']);
  }
  buildForm() {
    this.inputForm = this.formBuilder.group({
      //General Tab
      invoiceDate: new Date(),      
      chkAllDates: false,
      startDate: '01/12/2021',
      endDate: '01/12/2021',
      chkDebtorUpdate: true,
      chkReceiptUpdate: false,
      chkOtherMasterFile: false,
      chkForceAllDeb: false,
      
      //Branches & Funding
      invType: 'General',
      chkAutoGenReceipt: false,
      
      //Interface Tab
      chkCreateExportFile: false,
      invPrefix: '',
      AccPackage: '',
      gstSet: '',
      myOB: '',
      chkTraccsGL: false,
      chkConDateDes: false,
      chkUsePkgGL: false,
      chkPrefGL: false,
      chkAppendActy: false,
      chkTraccsDateMyOB: false,
      chkExcClientDesc: false,
      chkExcContIdDesc: false,
      chkHyphenatedGLAcc: false,
      chkUseActivityGL: false,
      chkUseNDIAItem: false,
      chkUseNDIAClaim: false,
      chkFirstNameSurNam: false,
      chkAppendInvT: false,
      chkRecipientTermDate: false,
      chkBlankInventoryCode: false,
      chkAlternativeDesc: false,
      chkBlankTrackingNamOpt: false,
      chkIncluEmailPosAdd: false,

      //Invoice Presentation Tab
      bankDetailsString: '',
      chkSepInvRecipient: false,
      chkSepPageRecipient: false,
      chkGenInternalInvoice: false,
      chkPrintZeroInvoices: false,
      chkPrintZeroInvoiceLine: false,
      printerCopies: '1',
      chkStaffCodeInv: false,
      rdoStaffCodeInv: "rdoAccountCode",
    });

    this.allWeeksChecked = true;
    this.checkedWeekly = true;
    this.checkedFornightly = true;
    this.checkedMonthly = true;
    this.checked4Weekly = true;
    this.checkedMonthly1 = true;
    this.checkedWeekly1 = true;

    this.loading = true;

    // Get Bank Details
    this.billingS.getBankDetails(null).subscribe(bankDetails => {
      var bankData = ('Bank: ' + bankDetails[0].bankName + ' BSB: ' + bankDetails[0].bankBSB + ' Account Name: ' + bankDetails[0].bankAccountName + ' A/c#: ' + bankDetails[0].bankAccountNumber);
      this.inputForm.patchValue({
        bankDetailsString: bankData,
      });
    });

    // Get PayPeriod End Date
    this.billingS.getSysTableDates().subscribe(dataPayPeriodEndDate => {
      if (dataPayPeriodEndDate[0].payPeriodEndDate != "") {
        this.endDate = dataPayPeriodEndDate[0].payPeriodEndDate;
        this.inputForm.patchValue({
          endDate: this.endDate,
        })
      }
      else {
        this.inputForm.patchValue({
          endDate: new Date()
        });
        this.endDate = new Date
      }
      this.billingS.getTableRegistration().subscribe(dataDefaultPayPeriod => {
        if (dataDefaultPayPeriod[0].defaultPayPeriod != "") {
          this.PayPeriodLength = dataDefaultPayPeriod[0].defaultPayPeriod
        }
        else {
          this.PayPeriodLength = 14
        }
        var firstDate = new Date(this.endDate);
        firstDate.setDate(firstDate.getDate() - (this.PayPeriodLength - 1));
        this.startDate = formatDate(firstDate, 'MM-dd-yyyy', 'en_US');
        this.inputForm.patchValue({
          startDate: this.startDate,
        });
      });
    });

    // Get Billing Cycle
    this.billingS.getBillingCycle(this.check).subscribe(data => {
      this.billingCycleList = data;
      this.tableData = data;
      this.allWeeksChecked = true;
      this.checkAll(6);
    });

    // Get Branches Details
    this.menuS.getlistbranches(this.check).subscribe(data => {
      this.branchList = data;
      this.tableData = data;
      this.allBranchesChecked = true;
      this.checkAll(1);
    });

    // Get Programs Details
    let dataPassPrograms = {
      condition: formatDate(new Date(), 'MM-dd-yyyy', 'en_US'),
    }
    this.billingS.getProgramslist(dataPassPrograms).subscribe(dataPrograms => {
      this.programList = dataPrograms;
      this.tableData = dataPrograms;
      this.allProgramsChecked = true;
      this.checkAll(2);
    });

    // Get Funding Details
    this.billingS.getFundingDetails().subscribe(dataFundingDetails => {
      this.fundingList = dataFundingDetails;
      this.tableData = dataFundingDetails;
      this.allFundingChecked = true;
      this.checkAll(5);
    });

    // Get Categories Details
    this.timesheetS.getlistcategories().subscribe(data => {
      this.categoriesList = data;
      this.tableData = data;
      this.allCategoriesChecked = true;
      this.checkAll(3);
    });

    // Get Recipients Details
    this.billingS.getRecipientsDetails().subscribe(dataRecipientsDetail => {
      this.recipientsList = dataRecipientsDetail;
      this.tableData = dataRecipientsDetail;
      this.allRecipientsChecked = true;
      this.checkAll(4);
    });

    //load Batch History Detail
    this.billingS.getBillingBatchHistory().subscribe(data => {
      this.batchHistoryList = data;
      this.tableData = data;
    });

    // Dropdown lists
    this.billingType = ['CONSOLIDATED BILLING', 'PROGRAM BILLING'];
    this.invLineType = ['PRODUCT LINE', 'SERVICE LINE', 'SUNDRY LINE'];
    this.gstSetting = ['AUTO', '0', '1', '2', '3', '4'];
    this.myOB = ['ITEM SALES', 'SERVICE SALES', 'TIME BILLING', 'TIME BILLING ORIGINAL', 'TIME BILLING ACCOUNT RIGHT 2015'];
    this.AccountPackage = [
      'ABM',
      'AFRHA',
      'ATTACHE',
      'AUTHORITY ACCOUNTING',
      'CPL NDIA',
      'EPICOR',
      'EPICOR9',
      'FAMMIS 2 EXPORT',
      'FAMMIS EXPORT',
      'FINANCE ONE',
      'GENERIC',
      'GREENTREE ACCOUNTING',
      'MS DYNAMICS GP',
      'MYOB',
      'PATHWAYS ACCOUNTING',
      'SAGE EVOLUTION',
      'SUN ACCOUNTING',
      'T1 ETL EXPORT',
      'T1 EXPORT',
      'XERO ACCOUNTING'
    ];
    this.invType = ['CDC Homecare Package Invoices', 'NDIA Claim Update Invoices', 'General', 'Re-Export Existing Batch', 'ALL'];

    this.loading = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    for (let property in changes) {
      if (property == 'open' && !changes[property].firstChange && changes[property].currentValue != null) {
        this.isVisible = true;
      }
    }
  }

  onKeyPress(data: KeyboardEvent) {
    return this.globalS.acceptOnlyNumeric(data);
  }

  checkedStatus(): void {
    if (this.allWeeksChecked == true) {
      this.checkedWeekly = true;
      this.checkedFornightly = true;
      this.checkedMonthly = true;
      this.checkAll(6);
    } else {
      this.checkedWeekly = false;
      this.checkedFornightly = false;
      this.checkedMonthly = false;
      this.allWeeksChecked = true;
      this.uncheckAll(6);
    }
  }

  log(event: any, index: number) {
    if (index == 1)
      this.selectedBranches = event;
    if (index == 2)
      this.selectedPrograms = event;
    if (index == 3)
      this.selectedCategories = event;
    if (index == 4)
      this.selectedRecipients = event;
    if (index == 5)
      this.selectedFunding = event;
    if (index == 6)
      this.selectedBillingCycle = event;
  }

  checkAll(index: number): void {
    if (index == 1) {
      if (this.allBranchesChecked == false) {
        this.lockBranches = true
      }
      else {
        this.branchList.forEach(x => {
          x.checked = true;
          this.allBranchesChecked = x.description;
          this.allBranchesChecked = true;
        });
        this.lockBranches = false
      }
    }
    if (index == 2) {
      if (this.allProgramsChecked == false) {
        this.lockPrograms = true
      }
      else {
        this.programList.forEach(x => {
          x.checked = true;
          this.allProgramsChecked = x.description;
          this.allProgramsChecked = true;
        });
        this.lockPrograms = false
      }
    }
    if (index == 3) {
      if (this.allCategoriesChecked == false) {
        this.lockCategories = true
      }
      else {
        this.categoriesList.forEach(x => {
          x.checked = true;
          this.allCategoriesChecked = x.description;
          this.allCategoriesChecked = true;
        });
        this.lockCategories = false
      }
    }
    if (index == 4) {
      if (this.allRecipientsChecked == false) {
        this.lockRecipients = true
      }
      else {
        this.recipientsList.forEach(x => {
          x.checked = true;
          this.allRecipientsChecked = x.description;
          this.allRecipientsChecked = true;
        });
        this.lockRecipients = false
      }
    }
    if (index == 5) {
      if (this.allFundingChecked == false) {
        this.lockFunding = true
      }
      else {
        this.fundingList.forEach(x => {
          x.checked = true;
          this.allFundingChecked = x.description;
          this.allFundingChecked = true;
        });
        this.lockFunding = false
      }
    }
    if (index == 6) {
      if (this.allWeeksChecked == false) {
        this.lockBillingCycle = true
      }
      else {
        this.billingCycleList.forEach(x => {
          x.checked = true;
          this.allWeeksChecked = x.description;
          this.allWeeksChecked = true;
          this.checkedWeekly = true;
          this.checkedFornightly = true;
          this.checkedMonthly = true;
        });
        this.lockBillingCycle = false
      }
    }
    if (index == 7) {
      if (this.chkGenInternalInvoice == true) {
        this.chkPrintZeroInvoices = true;
        this.chkPrintZeroInvoiceLine = true;
      }
      else {
        this.chkPrintZeroInvoices = false;
        this.chkPrintZeroInvoiceLine = false;
      }
    }
  }

  uncheckAll(index: number): void {
    if (index == 1) {
      this.lockBranches = true;
      this.branchList.forEach(x => {
        x.checked = false;
        this.allBranchesChecked = false;
        this.selectedBranches = [];
      });
    }
    if (index == 2) {
      this.lockPrograms = true;
      this.programList.forEach(x => {
        x.checked = false;
        this.allProgramsChecked = false;
        this.selectedPrograms = [];
      });
    }
    if (index == 3) {
      this.lockCategories = true;
      this.categoriesList.forEach(x => {
        x.checked = false;
        this.allCategoriesChecked = false;
        this.selectedCategories = [];
      });
    }
    if (index == 4) {
      this.lockRecipients = true;
      this.recipientsList.forEach(x => {
        x.checked = false;
        this.allRecipientsChecked = false;
        this.selectedRecipients = [];
      });
    }
    if (index == 5) {
      this.lockFunding = true;
      this.fundingList.forEach(x => {
        x.checked = false;
        this.allFundingChecked = false;
        this.selectedFunding = [];
      });
    }
    if (index == 6) {
      this.lockBillingCycle = true;
      this.billingCycleList.forEach(x => {
        x.checked = false;
        this.checkedWeekly = false;
        this.checkedFornightly = false;
        this.checkedMonthly = false;
        this.allWeeksChecked = false;
        this.selectedBillingCycle = [];
      });
    }
  }

  startProcess() {
    this.loading = true;
    this.billingS.getBatchRecord(null).subscribe(data => {
      this.batchNumber = data[0].batchRecordNumber;
      this.validateModalFields();
    });
  }

  validateModalFields(): void {
    this.operatorID = this.token.nameid;
    this.batchNumber = this.batchNumber + 1;
    this.currentDateTime = this.globalS.getCurrentDateTime();
    this.currentDateTime = formatDate(this.currentDateTime, 'yyyy-MM-dd hh:mm', 'en_US');
    this.startDate = this.inputForm.get('startDate').value;
    this.endDate = this.inputForm.get('endDate').value;
    this.startDate = formatDate(this.startDate, 'yyyy/MM/dd', 'en_US');
    this.endDate = formatDate(this.endDate, 'yyyy/MM/dd', 'en_US');
    this.invoiceType = this.inputForm.get('invType').value;

    if (this.lockBranches == false) {
      this.selectedBranches = null
    } else {
      this.selectedBranches = this.branchList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockPrograms == false) {
      this.selectedPrograms = null
    } else {
      this.selectedPrograms = this.programList
        .filter(opt => opt.checked)
        .map(opt => opt.title).join(",")
    }

    if (this.lockCategories == false) {
      this.selectedCategories = null
    } else {
      this.selectedCategories = this.categoriesList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockBillingCycle == false) {
      this.selectedBillingCycle = null
    } else {
      this.selectedBillingCycle = this.billingCycleList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.lockFunding == false) {
      this.selectedFunding = null
    } else {
      this.selectedFunding = this.fundingList
        .filter(opt => opt.checked)
        .map(opt => opt.description).join(",")
    }

    if (this.selectedBillingCycle != '' && this.selectedBranches != '' && this.selectedPrograms != '' && this.selectedCategories != '' && this.selectedFunding != '') {
      // console.log(this.batchNumber);
      // console.log(this.selectedBranches);
      // console.log(this.selectedPrograms);
      // console.log(this.selectedCategories);
      // console.log(this.startDate);
      // console.log(this.endDate);
      // console.log(this.invoiceType);
      // console.log(this.selectedBillingCycle);
      console.log(this.chkSepInvRecipient);
      console.log(this.operatorID);
      console.log(this.currentDateTime);
      // this.processExecution();
      // this.processExecutionTest();
    } else if (this.selectedBillingCycle == '') {
      this.globalS.eToast('Error', 'Please select atleast one Billing Cycle to proceed')
    } else if (this.selectedBranches == '') {
      this.globalS.eToast('Error', 'Please select atleast one Branch to proceed')
    } else if (this.selectedFunding == '') {
      this.globalS.eToast('Error', 'Please select atleast one Funding to proceed')
    } else if (this.selectedPrograms == '') {
      this.globalS.eToast('Error', 'Please select atleast one Program to proceed')
    } else if (this.selectedCategories == '') {
      this.globalS.eToast('Error', 'Please select atleast one Category to proceed')
    }
  }

  processExecution() {
    this.postLoading = true;
    const modalVal = this.inputForm;

    let _AccountPackage = modalVal.get('AccountPackage')?.value ?? '';
    let _myOB = modalVal.get('myOB')?.value ?? '';
    let _invLineType = modalVal.get('invLineType')?.value ?? '';
    let _billingType = modalVal.get('billingType')?.value ?? '';

    if (this.selectedBranches = null) { this.selectedBranches = false };
    if (this.selectedCategories = null) { this.selectedCategories = false };
    if (this.selectedFunding = null) { this.selectedFunding = false };
    if (this.selectedPrograms = null) { this.selectedPrograms = false };

    const params = {
      User: this.token.nameid,
      Password: '', // Ignored by Mansoor
      WindowsUser: this.loggedUserName,
      b_AllDates: modalVal.get('chkAllDates').value,
      b_Award: modalVal.get('chkAwardInterExport').value,
      b_Export: modalVal.get('chkCreateExportFile').value,
      b_ExportZeroValueLines: modalVal.get('chkExportZeroValueLines').value,
      b_GLCodesRequired: '', // Ignored by Mansoor
      b_IncludeADMINPaytype: modalVal.get('chkIncludeADMINPaytype').value,
      b_IncludeRecipientAdmin: modalVal.get('chkIncludeRecipientAdmin').value,
      b_IncUnapproved: '', // Ignored by Mansoor
      b_Option1: modalVal.get('chkIncActivityCode').value,
      b_Option2: modalVal.get('chkUseTraccsCostAcc').value,
      b_Option3: modalVal.get('chkActCostAcc').value,
      b_Option4: modalVal.get('chkStaffNumberPosID').value,
      b_Option5: modalVal.get('chkPrefixGL').value,
      b_PayBrokerage: modalVal.get('chkPayBrokerage').value,
      b_UpdateCompletion: '', // Ignored by Mansoor
      ProgressBar_Max: '',
      ProgressBar_Text: '',
      ProgressBar_Value: '',
      dt_ProcessStartTime: '',
      OperatorID: this.operatorID,
      s_ABMDefaultAccount: modalVal.get('inputDefaultAccount').value,
      s_ABMDefaultActivity: modalVal.get('inputDefaultActivity').value,
      s_ABMDefaultProject: modalVal.get('inputDefaultProject').value,
      s_EndDate: formatDate(modalVal.get('endDate').value, 'yyyy/MM/dd', 'en_US'),
      s_EndOfInvoiceString: '', // Ignored by Mansoor
      s_ExportLocation: modalVal.get('selectedDirectoryPath').value,
      s_Interface: modalVal.get('selectedPackage').value,
      s_Option1: modalVal.get('normalPayType')?.value ?? '',
      s_Option2: modalVal.get('superannuationType')?.value ?? '',
      s_Option3: modalVal.get('allowanceType')?.value ?? '',
      s_SelectedBranches: this.selectedBranches,
      s_SelectedCategories: this.selectedCategories,
      s_SelectedFunding: this.selectedFunding,
      s_SelectedPrograms: this.selectedPrograms,
      s_StartDate: formatDate(modalVal.get('startDate').value, 'yyyy/MM/dd', 'en_US'),
    };

    this.Dlls.PayBillUpdate({ //this should be for the billing update as when dll will be complete
      User: params.User,
      Password: params.Password,
      WindowsUser: params.WindowsUser,
      b_AllDates: params.b_AllDates,
      b_Award: params.b_Award,
      b_Export: params.b_Export,
      b_ExportZeroValueLines: params.b_ExportZeroValueLines,
      b_GLCodesRequired: params.b_GLCodesRequired,
      b_IncludeADMINPaytype: params.b_IncludeADMINPaytype,
      b_IncludeRecipientAdmin: params.b_IncludeRecipientAdmin,
      b_IncUnapproved: params.b_IncUnapproved,
      b_Option1: params.b_Option1,
      b_Option2: params.b_Option2,
      b_Option3: params.b_Option3,
      b_Option4: params.b_Option4,
      b_Option5: params.b_Option5,
      b_PayBrokerage: params.b_PayBrokerage,
      b_UpdateCompletion: params.b_UpdateCompletion,
      ProgressBar_Max: params.ProgressBar_Max,
      ProgressBar_Text: params.ProgressBar_Text,
      ProgressBar_Value: params.ProgressBar_Value,
      dt_ProcessStartTime: params.dt_ProcessStartTime,
      OperatorID: params.OperatorID,
      s_ABMDefaultAccount: params.s_ABMDefaultAccount,
      s_ABMDefaultActivity: params.s_ABMDefaultActivity,
      s_ABMDefaultProject: params.s_ABMDefaultProject,
      s_EndDate: params.s_EndDate,
      s_EndOfInvoiceString: params.s_EndOfInvoiceString,
      s_ExportLocation: params.s_ExportLocation,
      s_Interface: params.s_Interface,
      s_Option1: params.s_Option1,
      s_Option2: params.s_Option2,
      s_Option3: params.s_Option3,
      s_SelectedBranches: params.s_SelectedBranches,
      s_SelectedCategories: params.s_SelectedCategories,
      s_SelectedFunding: params.s_SelectedFunding,
      s_SelectedPrograms: params.s_SelectedPrograms,
      s_StartDate: params.s_StartDate,
    }).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      console.log(data)
      if (data == 'True') {
        this.updatedRecords = data[0].updatedRecords
        if (this.updatedRecords == 0) {
          this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
        } else {
          this.globalS.sToast('Success', this.updatedRecords + ' - Payroll Records Updated')
          this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Payroll Run is: ' + this.batchNumber)
        }
        this.postLoading = false;
        this.ngOnInit();
        return false;
      }
    });
  }
  

  processExecutionTest() {
    this.postLoading = true;
    this.billingS.postDebtorBilling({
      InvoiceNumber: 'N/A',
      BatchNumber: this.batchNumber,
      Branches: this.selectedBranches,
      Programs: this.selectedPrograms,
      Categories: this.selectedCategories,
      StartDate: this.startDate,
      EndDate: this.endDate,
      InvoiceType: this.invoiceType,
      sepInvRecipient: this.chkSepInvRecipient,
      OperatorID: this.operatorID,
      CurrentDateTime: this.currentDateTime
    }).pipe(
      takeUntil(this.unsubscribe)).subscribe(data => {
        if (data) {
          this.updatedRecords = data[0].updatedRecords
          if (this.updatedRecords == 0) {
            this.globalS.iToast('Information', 'There are no approved roster entries to process for the selected date range and program/s')
          } else {
            this.globalS.sToast('Success', this.updatedRecords + ' - Debtor Records Updated')
            this.globalS.iToast('Information', 'For your record and future reference, please note that the Batch# allocated to this Debtor Run is: ' + this.batchNumber)
          }
          this.postLoading = false;
          this.ngOnInit();
          return false;
        }
      });
  }
  
  async selectSavePath() {
    const modalVal = this.inputForm;
    var packageName = modalVal.get('selectedPackage').value;

    try {

      let s_Location: string = '';
      let suggestedName: string = '';
      let mimeType: string = '';
      let types: { description: string, accept: { [key: string]: string[] } }[] = [];
      let selectedDirectoryPath: string = '';

      //these should need to be udpate as per the same as defined for Debtor update module
      switch (packageName) {
        case 'AURION':
        case 'AURION (AWARD)':
          suggestedName = 'AURION.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'GREENTREE':
          suggestedName = 'GREENTREE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CHRIS21':
          suggestedName = 'PAYROLL.DAT';
          mimeType = 'application/dat';
          types = [
            {
              description: 'DAT Files',
              accept: { 'application/dat': ['.dat'] },
            },
          ];
          break;

        case 'MYOB':
          suggestedName = 'MYOB.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'QUICKBOOKS':
          suggestedName = 'QUICKBOOK.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'WAGE EASY PAYROLL':
          suggestedName = 'WAGE.WTC';
          mimeType = 'application/octet-stream';
          types = [
            {
              description: 'WTC Files',
              accept: { 'application/octet-stream': ['.wtc'] },
            },
          ];
          break;

        case 'HR3':
          suggestedName = 'HR3.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ABM':
          suggestedName = 'ABMSUPPLIER.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'QUICKEN PREMIER':
          suggestedName = 'QUICKEN.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'ATTACHE':
        case 'ATTACHE SUPPLIER':
          suggestedName = 'PAYTSHT.INP';
          mimeType = 'application/inp';
          types = [
            {
              description: 'INP Files',
              accept: { 'application/inp': ['.inp'] },
            },
          ];
          break;

        case 'CIM':
          s_Location = getOutputFolder('PayExportFolder', 'Force_PayExportFolder', 'Select the Payroll Export Folder', this.operatorID);
          break;

        case 'MICROPAY':
          suggestedName = 'MicroPayPayrollPath' + '\\TRACCS.MIF';
          mimeType = 'application/mif';
          types = [
            {
              description: 'MIF Files',
              accept: { 'application/mif': ['.mif'] },
            },
          ];
          break;

        case 'PAYGLOBAL':
          suggestedName = 'PAYGLOBAL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'EASYTIME':
          suggestedName = 'EASYTIME.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'NELLER-PRECEDA':
          suggestedName = 'PAY34T.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ALESCO':
          suggestedName = 'AL-PAY.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'LATTICE/CONSISTO':
          suggestedName = 'CONSISTO.TXT';
          mimeType = 'text/plain';
          types = [
            {
              description: 'Text Files',
              accept: { 'text/plain': ['.txt'] },
            },
          ];
          break;

        case 'ROSTERLIVE':
          suggestedName = 'ROSTERLIVE.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CLOUD PAYROLL':
          suggestedName = 'CLOUDPAYROLL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CONNX':
          suggestedName = 'CONNXPAYROLL.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        case 'CARE SYSTEMS':
          suggestedName = 'CARESYSTEMS.CSV';
          mimeType = 'application/csv';
          types = [
            {
              description: 'CSV Files',
              accept: { 'application/csv': ['.csv'] },
            },
          ];
          break;

        default:
          break;
      }

      function getOutputFolder(folder: string, forceFolder: string, dialogTitle: string, operatorId: string): string {
        return `/output/folder/path/${folder}`;
      }

      const handle = await (window as any).showSaveFilePicker({
        suggestedName: suggestedName,
        types: types,
      });

      const filePath = handle.name;
      const dirHandle = await (window as any).showDirectoryPicker();
      const dirName = filePath;
      const fileHandle = await dirHandle.getFileHandle(dirName, { create: true });

      // console.log(`File path in: ${dirHandle.name}/${dirName}`);

      this.inputForm.patchValue({
        selectedDirectoryPath: `C:\\Users\\${this.loggedUserName}\\Documents\\${dirHandle.name}\\${dirName}`
      })

    } catch (error) {
      // console.error('Failed to get path:', error);
      this.globalS.iToast('Information', 'Directory Path not selected.');
    }
  }
  selectedDirectoryPath: any;

}

