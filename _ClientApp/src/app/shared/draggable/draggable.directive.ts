import { 
    Directive, 
    ElementRef, 
    HostListener, 
    ViewChild,
    HostBinding, 
    Output, 
    EventEmitter,
    OnInit,
    NgZone,
    AfterViewInit,
    OnDestroy,
} from '@angular/core';

import { Subject } from 'rxjs';
import { switchMap, takeUntil, take, repeat } from 'rxjs/operators';


@Directive({
    selector: '[draggable-time]'
})

export class DraggableTimeDirective implements OnInit, AfterViewInit {

    @HostBinding('class.draggable') draggable = true;
    @ViewChild('dragArea', { static: false }) dragArea: ElementRef
    constructor(
        public ngZone: NgZone = null,
        public el: ElementRef = null
    ){

    }
    
    @Output() dragStart = new EventEmitter<PointerEvent>();
    @Output() dragMove = new EventEmitter<PointerEvent>();
    @Output() dragEnd = new EventEmitter<PointerEvent>();

    private pointerDown = new Subject<PointerEvent>();
    private pointerMove = new Subject<PointerEvent>();
    private pointerUp = new Subject<PointerEvent>();

    ngOnInit(){
        this.pointerDown.asObservable()
        .subscribe(event => {
            this.dragStart.emit(event)
        })

        this.pointerDown.pipe(
            switchMap(() => this.pointerMove),
            takeUntil(this.pointerUp),
            repeat()
        ).subscribe(event => {            
            this.el.nativeElement.style.backgroundColor = 'red'
            this.dragMove.emit(event)
        })

        this.pointerDown.pipe(
            switchMap(() => this.pointerUp),
            take(1),
            repeat()
        ).subscribe(event => {
            this.el.nativeElement.style.backgroundColor = ''
            this.dragEnd.emit(event)
        })
    }  

    // @HostListener('pointerdown',['$event'])
    // onPointerDown(event: PointerEvent){
    //     this.pointerDown.next(event)
    // }

    // @HostListener('document:pointermove',['$event'])
    // onPointerMove(event: PointerEvent){
    //     this.pointerMove.next(event)
    // }

    // @HostListener('document:pointerup',['$event'])
    // onPointerUp(event: PointerEvent){
    //     this.pointerUp.next(event)
    // }
  

    ngAfterViewInit(): void {
        const dateTime = document.querySelector('.date-time')

        this.el.nativeElement.addEventListener('pointerdown', (e: any) => {
            this.pointerDown.next(e)
        })        
      

        this.ngZone.runOutsideAngular(() => {   
                        
            dateTime.addEventListener('pointermove', (e: any) => {
                this.pointerMove.next(e)
            })
            dateTime.addEventListener('pointerup', (e: PointerEvent) => {
                this.pointerUp.next(e)
            })        
        })
    }


}