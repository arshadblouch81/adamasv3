namespace Adamas.Models.Modules{
    public class RosterNote {
        public string Note { get; set; }
        public string RecordNo { get; set; }
    }
}