import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
    selector: 'staff-competencies',
    templateUrl: './staff-competencies.html',
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => StaffCompetenciesComponent),
        multi: true
    }]
})

export class StaffCompetenciesComponent implements ControlValueAccessor {
    onChange = (_: any) => { };
    onTouch: () => {};

    private value: any;

    writeValue(value: any){
        if(value != null){
            this.value = value;
            this.search(this.value);
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    // -----------------------------------------

    search(user: any){
        console.log(user);
    }
}