import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { TimeSheetService } from '@services/index'
import * as moment from 'moment';

@Component({
    selector: 'dm-audit',
    templateUrl: './dm-audit.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DmAuditComponent),
            multi: true
        }
    ]
})

export class DmAuditComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    results: Array<any>

    constructor(
        private timeS: TimeSheetService
    ){ }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        this.timeS.getaudithistory(user.recordno)
                .subscribe(data => {
                    this.results = data.map(x => {
                        return {
                            date: moment(x.date).isValid() ? moment(x.date).format('DD/MM/YYYY') : null,
                            detail: x.detail,
                            traccsUser: x.traccsUser,
                            user: x.user
                        }
                    })
                })
    }
}