namespace Adamas.Models.Modules{
    public class Program {
        public string Title { get; set; }
        public string Funding_Source { get; set; }
        public int AgencyId { get; set; }
        public string GST { get; set; }
        public double Budget { get; set; }
        public int Budget_Hours { get; set; }
        public string BudgetCycle { get; set; }
        public string GL_Expense_Account { get; set; }
        public string GL_Revenue_Account { get; set; }
        public string GL_Super_Account { get; set; }
    }
}