﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class MedicalStaffDto
    {
        public int RecordNumber { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Postcode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Suburb { get; set; }
    }
}
