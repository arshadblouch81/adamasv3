
namespace Adamas.Models.Modules{
    public class AttendanceStaff {
        public string AutoLogout { get; set; }
        public bool? EmailMessage { get; set; }
        public bool? ExcludeShiftAlerts { get; set; }

        public bool? ExcludeFromTravelinterpretation { get; set; }
        public bool? InAppMessage { get; set; }
        public bool? LogDisplay { get; set; }
        public string Pin { get; set; }
        public string StaffTimezoneOffset { get; set; }
        
        public bool? RosterPublish { get; set; }
        public bool? ShiftChange { get; set; }
        public bool? SmsMessage { get; set; }
        public bool? EmailTimesheet { get; set; }
        public string Id { get; set; }
    }
}