﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using eg_03_csharp_auth_code_grant_core;
using User =  eg_03_csharp_auth_code_grant_core.Models.User;
using DSConfiguration =  eg_03_csharp_auth_code_grant_core.Models.DSConfiguration;
using Session = eg_03_csharp_auth_code_grant_core.Models.Session;
using Locals = eg_03_csharp_auth_code_grant_core.Models.Locals;

using eg_03_csharp_auth_code_grant_core.Common;
using DocuSign.eSign.Model;
using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using System.Security.Claims;

namespace AdamasV3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Signing2Controller : ControllerBase
    {
        protected IRequestItemsService RequestItemsService { get; }

        private string dsPingUrl;
        private string signerClientId = "1000";
        private string dsReturnUrl;
        private DSConfiguration config;
        
        public Signing2Controller(DSConfiguration config, IRequestItemsService requestItemsService)
        {
            RequestItemsService = requestItemsService;
            this.config = config;
            dsPingUrl = config.AppUrl + "/";
            dsReturnUrl = config.AppUrl + "/dsReturn";
        }

        [HttpGet("login")]
        public IActionResult Login()
        {
            RequestItemsService.UpdateUserFromJWT();
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            var locals = HttpContext.Session.GetObjectFromJson<Locals>("locals") ?? new Locals();

            if (identity != null && !identity.IsAuthenticated && (RequestItemsService.User?.AccessToken == null))
            {
                locals.Session = new Session();
                return Ok(false);
            }

            locals.User = HttpContext.Session.GetObjectFromJson<User>("user");

            if (locals.User == null)
            {
                locals.User = identity.IsAuthenticated
                ? new User
                {
                    Name = identity.FindFirst(x => x.Type.Equals(ClaimTypes.Name)).Value,
                    AccessToken = identity.FindFirst(x => x.Type.Equals("access_token")).Value ?? RequestItemsService.User.AccessToken,
                    RefreshToken = identity.FindFirst(x => x.Type.Equals("refresh_token"))?.Value,
                    ExpireIn = DateTime.Parse(identity.FindFirst(x => x.Type.Equals("expires_in")).Value ?? identity.Claims.First(x => x.Type.Equals("expires_in")).Value)
                }
                :
                new User
                {
                    Name = RequestItemsService.User?.Name,
                    AccessToken = RequestItemsService.User?.AccessToken,
                    RefreshToken = null,
                    ExpireIn = RequestItemsService.User?.ExpireIn
                };

                RequestItemsService.User = locals.User;                
            }

            if (locals.Session == null)
            {
                locals.Session = identity.IsAuthenticated
                    ? new Session
                    {
                        AccountId = identity.FindFirst(x => x.Type.Equals("account_id")).Value,
                        AccountName = identity.FindFirst(x => x.Type.Equals("account_name")).Value,
                        BasePath = identity.FindFirst(x => x.Type.Equals("base_uri")).Value
                    }
                    :
                    new Session
                    {
                        AccountId = RequestItemsService.Session.AccountId,
                        AccountName = RequestItemsService.Session.AccountName,
                        BasePath = RequestItemsService.Session.BasePath,
                    };

                RequestItemsService.Session = locals.Session;                
            }

            string accessToken = RequestItemsService.User.AccessToken;
            string basePath = RequestItemsService.Session.BasePath + "/restapi";
            string accountId = RequestItemsService.Session.AccountId;

            // bool tokenOk = CheckToken(3);
            
            // if (!tokenOk)
            // {
            //     return Ok(false);
            // }

            // string redirectUrl = DoWork("markaris.trinidad@gmail.com", "Mark Aris Trinidad", accessToken, basePath, accountId);

            // return Redirect(redirectUrl);

            return Ok(new {
                accessToken,
                basePath,
                accountId
            });
        }

        // private string DoWork(string signerEmail, string signerName,
        //     string accessToken, string basePath, string accountId)
        // {
        //     // Data for this method
        //     // signerEmail 
        //     // signerName
        //     // accessToken
        //     // basePath
        //     // accountId

        //     // dsPingUrl -- class global
        //     // signerClientId -- class global
        //     // dsReturnUrl -- class global

        //     // Step 1. Create the envelope definition
        //     EnvelopeDefinition envelope = MakeEnvelope(signerEmail, signerName);

        //     // Step 2. Call DocuSign to create the envelope                   
        //     var config = new Configuration(new ApiClient(basePath));
        //     config.AddDefaultHeader("Authorization", "Bearer " + accessToken);
        //     EnvelopesApi envelopesApi = new EnvelopesApi(config);
        //     EnvelopeSummary results = envelopesApi.CreateEnvelope(accountId, envelope);
        //     string envelopeId = results.EnvelopeId;

        //     // Save for future use within the example launcher
        //     RequestItemsService.EnvelopeId = envelopeId;

        //     // Step 3. create the recipient view, the Signing Ceremony
        //     RecipientViewRequest viewRequest = MakeRecipientViewRequest(signerEmail, signerName);
        //     // call the CreateRecipientView API
        //     ViewUrl results1 = envelopesApi.CreateRecipientView(accountId, envelopeId, viewRequest);

        //     // Step 4. Redirect the user to the Signing Ceremony
        //     // Don't use an iFrame!
        //     // State can be stored/recovered using the framework's session or a
        //     // query parameter on the returnUrl (see the makeRecipientViewRequest method)
        //     string redirectUrl = results1.Url;
        //     return redirectUrl;        }


        // private RecipientViewRequest MakeRecipientViewRequest(string signerEmail, string signerName)
        // {
        //     // Data for this method
        //     // signerEmail 
        //     // signerName
        //     // dsPingUrl -- class global
        //     // signerClientId -- class global
        //     // dsReturnUrl -- class global


        //     RecipientViewRequest viewRequest = new RecipientViewRequest();
        //     // Set the url where you want the recipient to go once they are done signing
        //     // should typically be a callback route somewhere in your app.
        //     // The query parameter is included as an example of how
        //     // to save/recover state information during the redirect to
        //     // the DocuSign signing ceremony. It's usually better to use
        //     // the session mechanism of your web framework. Query parameters
        //     // can be changed/spoofed very easily.
        //     viewRequest.ReturnUrl = "http://45.77.37.207:9000" +dsReturnUrl + "?state=123";

        //     // How has your app authenticated the user? In addition to your app's
        //     // authentication, you can include authenticate steps from DocuSign.
        //     // Eg, SMS authentication
        //     viewRequest.AuthenticationMethod = "none";

        //     // Recipient information must match embedded recipient info
        //     // we used to create the envelope.
        //     viewRequest.Email = signerEmail;
        //     viewRequest.UserName = signerName;
        //     viewRequest.ClientUserId = signerClientId;

        //     // DocuSign recommends that you redirect to DocuSign for the
        //     // Signing Ceremony. There are multiple ways to save state.
        //     // To maintain your application's session, use the pingUrl
        //     // parameter. It causes the DocuSign Signing Ceremony web page
        //     // (not the DocuSign server) to send pings via AJAX to your
        //     // app,
        //     viewRequest.PingFrequency = "600"; // seconds
        //                                        // NOTE: The pings will only be sent if the pingUrl is an https address
        //     viewRequest.PingUrl = "http://45.77.37.207:9000" + dsPingUrl; // optional setting

        //     return viewRequest;
        // }

        // private EnvelopeDefinition MakeEnvelope(string signerEmail, string signerName)
        // {
        //     // Data for this method
        //     // signerEmail 
        //     // signerName
        //     // signerClientId -- class global
        //     // Config.docPdf


        //     byte[] buffer = System.IO.File.ReadAllBytes(this.config.docPdf);
       
        //     EnvelopeDefinition envelopeDefinition = new EnvelopeDefinition();
        //     envelopeDefinition.EmailSubject = "Please sign this document";
        //     Document doc1 = new Document();

        //     String doc1b64 = Convert.ToBase64String(buffer);

        //     doc1.DocumentBase64 = doc1b64;
        //     doc1.Name = "Lorem Ipsum"; // can be different from actual file name
        //     doc1.FileExtension = "pdf";
        //     doc1.DocumentId = "3";

        //     // The order in the docs array determines the order in the envelope
        //     envelopeDefinition.Documents = new List<Document> { doc1 };

        //     // Create a signer recipient to sign the document, identified by name and email
        //     // We set the clientUserId to enable embedded signing for the recipient
        //     // We're setting the parameters via the object creation
        //     Signer signer1 = new Signer {
        //         Email = signerEmail,
        //         Name = signerName,
        //         ClientUserId = signerClientId,
        //         RecipientId = "1"
        //     };
           
        //     // Create signHere fields (also known as tabs) on the documents,
        //     // We're using anchor (autoPlace) positioning
        //     //
        //     // The DocuSign platform seaches throughout your envelope's
        //     // documents for matching anchor strings.
        //     SignHere signHere1 = new SignHere
        //     {
        //         AnchorString = "/sn1/",
        //         AnchorUnits = "pixels",
        //         AnchorXOffset = "10",
        //         AnchorYOffset = "20"
        //     };
        //     // Tabs are set per recipient / signer
        //     Tabs signer1Tabs = new Tabs
        //     {
        //         SignHereTabs = new List<SignHere> { signHere1 }
        //     };
        //     signer1.Tabs = signer1Tabs;

        //     // Add the recipient to the envelope object
        //     Recipients recipients = new Recipients
        //     {
        //         Signers = new List<Signer> { signer1 }
        //     };
        //     envelopeDefinition.Recipients = recipients;

        //     // Request that the envelope be sent by setting |status| to "sent".
        //     // To request that the envelope be created as a draft, set to "created"
        //     envelopeDefinition.Status = "sent";

        //     return envelopeDefinition;
        // }

        // // [HttpGet("session")]
        // // public IActionResult CheckSession(){
        // //     bool tokenOk = CheckToken(3);
            
        // //     if (!tokenOk)
        // //     {
        // //         return Ok(false);
        // //     }
        // //     return Ok(true);
        // // }

        // // [HttpGet("token")]
        // // public IActionResult Sample()
        // // {
        // //     string accessToken = RequestItemsService.User.AccessToken;
        // //     string basePath = RequestItemsService.Session.BasePath + "/restapi";
        // //     string accountId = RequestItemsService.Session.AccountId;
        // //     return Ok(new {
        // //         accessToken = accessToken,
        // //         basePath,
        // //         accountId
        // //     });
        // // }

        // protected bool CheckToken(int bufferMin = 60)
        // {
        //     return RequestItemsService.CheckToken(bufferMin);
        // }
    }
}
