﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Adamas.DAL
{
    public interface IUserService
    {
        Task<List<Claim>> GetUserClaims();
    }
}
