﻿using System;

namespace AdamasV3.Models.Dto
{
    public class FiltersDto
    {
        public bool AcceptedQuotes { get; set; }
        public bool AllDates { get; set; }
        public bool ArchiveDocs { get; set; }
        public int Display { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? StartDate { get; set; }
        public bool IncludeArchivedNotes { get; set; }
        public bool IncludeClosedIncidents { get; set; }
        public string LimitTo { get; set; }
        public string StartingWith { get; set; }
    }
}
