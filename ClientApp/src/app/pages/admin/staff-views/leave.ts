import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef,ViewChild } from '@angular/core'
import { ComponentFactoryResolver, ComponentRef, Injector,  ViewContainerRef} from '@angular/core'
import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject,EMPTY } from 'rxjs';
import { takeUntil,switchMap } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import { ContextMenuComponent } from 'ngx-contextmenu';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';

@Component({
    styles: [`
    nz-table{
        margin-top:0px;
    }
    nz-select{
        width:100%;
        height:37px !important;
        line-height:37px !important;
    }
    ul{
        list-style:none;
    }
    nz-table th{
            font-weight: 600;
            font-family: 'Segoe UI';
            font-size: 14px;
            border: 1px solid #f3f3f3;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    .ant-table-small>.ant-table-content>.ant-table-body {
                margin:0px
    }
    tbody  tr:nth-child(odd) {
    background-color: #E9F7FF;
      }
    tbody  tr:nth-child(even) {
        background-color: #fff;
    }
    ::ng-deep .ant-checkbox-inner {
                    border: 2px solid #002060 !important;
    }
    `],
    templateUrl: './leave.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class StaffLeaveAdmin implements OnInit, OnDestroy {
    
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    loading: boolean = false;
    modalOpen: boolean = false;
    dateFormat: string = 'MM/dd/yyyy';
    isLoading: boolean;
    
    leaveGroup: FormGroup;
    
    putonLeaveModal: boolean = false;
    editModalOpen:boolean=false;
    AddLeave: boolean = true;
    check : boolean = false;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
     
    loadingPDF: boolean;
    ModalS: any;
    leaveactivities: any;
    leavepaytypes: any;
    defaultLeave:any;
    defaultLeaveForm: FormGroup;
    operation: any;
    userUpdated: any;
    currentDate: string;
    updateString:string;
    selectedRow  : number = 0;
    activeRowData:any;
    @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;

    loadUnallocateStaff :Subject<any> = new Subject() ;

    startDate:any= new Date();
    endDate  :any= new Date();
    notes    :string = '';
    editoperation:any;
    addchangeleave: boolean;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private listS: ListService,
        private router: Router,
        private sanitizer: DomSanitizer,
        private http: HttpClient,
        private printS:PrintService,
        private globalS: GlobalService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private cd: ChangeDetectorRef,
        private componentFactoryResolver: ComponentFactoryResolver,
        private location: ViewContainerRef
        ) {
            
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/staff/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'leave')) {
                    this.user = data;
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked();
            if(this.user){
                this.search(this.user);
                this.buildForm();
                this.populate();
                return;
            }
            this.router.navigate(['/admin/staff/personal'])
        }
        
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        buildForm() {
            this.leaveGroup = this.formBuilder.group({
                user: '',
                staffcode: '',
                dates: [[], [Validators.required]],
                reminderDate:new Date(),
                requestedDate:new Date(),
                startDate:new Date(),
                endDate:new Date(),
                approved:false,
                makeUnavailable: true,
                unallocAdmin: false,
                unallocUnapproved: true,
                unallocMaster: false,
                explanation: '',
                activityCode: '',
                payCode: '',
                program: '',
                notes:'',
                name:'',
                completed:false,
                programShow: false
            })
            
            this.defaultLeaveForm = this.formBuilder.group({
                defaultUnallocateLeaveActivity:'',
                defaultUnallocateLeavePayType:'',
                cancelledApplications:false,
            });
        }
        
        search(user: any = this.user) {
            this.cd.reattach();
            this.loading = true;
            this.timeS.getleaveapplication(user.code).subscribe(data => {
                this.tableData = data;
                this.originalTableData = data;
                this.loading = false;
                this.selectedRow = 0;
                this.activeRowData = this.tableData[this.selectedRow];
                this.cd.detectChanges();
            });
            this.populate();
        }
        selectedItemGroup(data:any, i:number){
            this.selectedRow=i;
            this.activeRowData=data;
        }
        changeStatus(e,typ){
            if(e.target.checked){
            }else{
            }
        }
        leavepaytypechanged(){
            if(!this.globalS.isEmpty(this.defaultLeaveForm.get('defaultUnallocateLeavePayType').value)){

                this.updateString = encodeURIComponent(this.defaultLeaveForm.get('defaultUnallocateLeavePayType').value.toString());
                
                this.timeS.updateLeaveStatus(this.updateString,this.user.id)
                .subscribe(data => {
                  });
            }
        }
        leaveactivityChanged(){
                if(!this.globalS.isEmpty(this.defaultLeaveForm.get('defaultUnallocateLeaveActivity').value)){


                    this.updateString = encodeURIComponent(this.defaultLeaveForm.get('defaultUnallocateLeaveActivity').value.toString());
                        
                    this.timeS.updateLeaveStatustype(this.updateString,this.user.id)
                        .subscribe(data => {
                    });
                
                
                }
        }
        populate(){
            forkJoin([
                this.listS.getleaveactivities(),
                this.listS.getleavepaytypes(),
            ]).subscribe(data => {
                this.leaveactivities = data[0];
                this.leavepaytypes   = data[1];
            });
            
            this.timeS.getstaffunallocatedefault(this.user.id).subscribe(data => {
                this.defaultLeave = data[0];
                setTimeout(() => {                
                    this.defaultLeaveForm.patchValue({
                        defaultUnallocateLeaveActivity:this.defaultLeave.defaultLeaveActivity,
                        defaultUnallocateLeavePayType:this.defaultLeave.defaultLeavePayType,
                    });
                },5); 
                this.cd.detectChanges();
            });
        }
        trackByFn(index, item) {
            return item.id;
        }
        
        showAddModal() {
            this.operation = {
                process: 'ADD'
            }
            this.load_LeaveModel();
            // console.log(JSON.stringify(this.user) + "user");
            //this.putonLeaveModal = !this.putonLeaveModal;
        }
        
        showEditModal(data: any) {
            const {code, id, sysmgr, view } = this.user;
            
            var newPass = {
                code: code,
                id: id,
                sysmgr: sysmgr,
                view: view,
                operation: 'UPDATE',
                recordNo: this.activeRowData.recordNumber
            }
            this.operation = {
                process: 'UPDATE'
            }
            this.user = newPass;
            this.putonLeaveModal = !this.putonLeaveModal;
        }

        showLeaveEditModal(){
            
        if(this.globalS.getCurrentDate()>this.globalS.convertDbDate(this.activeRowData.startDate,'yyyy-MM-dd')){
                this.globalS.iToast('Success', 'You can not edit a past leave application');
                return false
        }
        this.leaveGroup.patchValue({
            name:this.activeRowData.leaveType,
            startDate :this.activeRowData.startDate,
            endDate :this.activeRowData.endDate,
            reminderDate: this.activeRowData.reminderDate != null ? this.activeRowData.reminderDate : this.activeRowData.startDate,
            notes:this.activeRowData.notes,
            approved:this.activeRowData.approved,
        });
        this.addchangeleave = true;
        this.editModalOpen = false;
        }
        
        handleCancel() {
            this.modalOpen = false;
            this.search(this.user);
            return;
        }
        UnallocateStaffDone(data:any){
            this.modalOpen = false;
            this.putonLeaveModal = false;
            this.search(this.user);
            return;
        }
        reset() {
            
        }
        delete(data: any) {
            this.timeS
            .deleteleaveapplication(this.activeRowData.recordNumber)
            .pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data) {
                    this.globalS.sToast('Success', 'Data Deleted!');
                    this.search(this.user);
                    return;
                }
            });
        }
        handleOkTop() {
            this.generatePdf();
            this.tryDoctype = ""
            this.pdfTitle = ""
        }
        handleCancelTop(): void {
            this.drawerVisible = false;
            this.pdfTitle = "";
            this.tryDoctype="";
        }
        generatePdf(){
            this.drawerVisible = true;
            this.loadingPDF = true;
            
            var fQuery = "SELECT RECORDNUMBER, NAME AS Field1,CONVERT(varchar, [DATE1],105) as Field2,CONVERT(varchar, [DATE2],105) as Field3,NOTES as Field4,"+
            "CONVERT(varchar,[DATEINSTALLED],105) as Field5,COMPLETED AS Field6 FROM HUMANRESOURCES HR INNER JOIN Staff ST ON ST.[UniqueId] = HR.[PersonID]"+
            "WHERE ST.[AccountNo] ='"+this.user.code+"' AND HR.[DELETEDRECORD] = 0"+
            "AND HR.[GROUP] = 'LEAVEAPP'"+
            "ORDER BY  DATE1 DESC";
            
            const data = {
                "template": { "_id": "0RYYxAkMCftBE9jc" },
                "options": {
                    "reports": { "save": false },
                    "txtTitle": "LEAVE APPLICATION FOR '"+this.user.code+"' ",
                    "sql": fQuery,
                    "userid": this.tocken.user,
                    "head1" : "Leave Type",
                    "head2" : "Start",
                    "head3" : "End",
                    "head4" : "Notes",
                    "head5" : "REMINDER DATE",
                    "head6" : "Approved",
                }
            }
            this.printS.printControl(data).subscribe((blob: any) => { 
                this.pdfTitle = "Leave Applications.pdf"
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();
            }, err => {
                this.loading = false;
                this.cd.detectChanges();
                this.ModalS.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            this.cd.detectChanges();
            this.loadingPDF = true;
            this.tryDoctype = "";
            this.pdfTitle = "";
        }

        reload(data: string){
            this.search();
        }

        isUpdateValid(data: any){
            var currDate = new Date();
            return !(currDate > new Date(this.tableData[this.selectedRow]));
        }

      
    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];

    columnDictionary = [{
        key: 'Leave Type',
        value: 'leaveType'
    },{
        key: 'Start',
        value: 'startDate'
    },{
        key: 'End',
        value: 'endDate'
    },{
        key: 'Notes',
        value: 'notes'
    },{
        key: 'Reminder Date',
        value: 'reminderDate'
    },{
        key:'Requested Date',
        value:'requestedDate'
    }];

    dragDestination = [       
        'Leave Type',
        'Start',
        'End',
        'Notes',
        'Reminder Date',
        'Requested Date'
    ];

    flattenObj = (obj, parent = null, res = {}) => {
        for (const key of Object.keys(obj)) {
            const propName = parent ? parent + '.' + key : key;
            if (typeof obj[key] === 'object') {
                this.flattenObj(obj[key], propName, res);
            } else {
                res[propName] = obj[key];
            }
        }
        return res;
    }

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }

    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }

    removeTodo(data: any){
        this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
        this.generate();
    }

    isArray(data: any){
        return Array.isArray(data);
    }
 
    isSome(data: any){
        if(data){
            return data.some(d => 'key' in d);
        }
        return true;        
    }
    getPermisson(index:number){
        var permissoons = this.globalS.getStaffRecordView();        
        return permissoons.charAt(index-1);
    }
    //Mufeed 15 April 2024            
    InitiatePrint(){

           
        var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid,strdate, endate; 
        
        fQuery = " Select RecordNumber, Name AS LeaveType, Creator, format(Date1,'dd/MM/yyyy') As [Start], format(Date2,'dd/MM/yyyy') as [End], format(DateInstalled,'dd/MM/yyyy') as [Reminder Date], format(PurchaseDate,'dd/MM/yyyy') as [Requested Date],Case when Completed = 1 then 'Yes' else '' End as Approved, Notes  From HumanResources WHERE PersonID = '"+this.user.id+"' AND DeletedRecord = 0  AND [Group] = 'LEAVEAPP' ORDER BY  Date1 DESC "                                        
       
        //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
        txtTitle = "Leave Applications ";       
        Rptid =   "8WTerq14eNFSAnz0"  

        // console.log(fQuery)
        const data = {
                    
            "template": { "_id": Rptid },                                
            "options": {
                "reports": { "save": false },                
                "sql": fQuery,
                "Criteria": lblcriteria,
                "userid": this.tocken.user,
                "txtTitle": txtTitle,
                "Extra": this.user.code,
                                                                
            }
        }
        this.loading = true;
        this.drawerVisible = true;         
            this.printS.printControl(data).subscribe((blob: any) => {
            this.pdfTitle = txtTitle+".pdf"
                this.drawerVisible = true;                   
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();
            }, err => {
                console.log(err);
                this.loading = false;
                this.ModalS.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            return;      
    }
    

      load_LeaveModel(){
        this.putonLeaveModal = true;
        let _highlighted: any = [];
            let selectedOption: any = {carercode:this.user.code,
                recipient:''
            };
           
         _highlighted.push(selectedOption);
          this.loadUnallocateStaff.next({diary:_highlighted,selected:selectedOption, AddLeave:true});

      }

      editLeaveApplication(item: any,operation:number) {
        
        this.editoperation = operation;
       
            if(this.editoperation){
                this.notes = this.activeRowData.notes
            }
            this.editModalOpen = true;
            this.addchangeleave = false;
        
    }
    onValueChange(value: Date): void {
        console.log(`Current value: ${value.toISOString().split('T')[0]}`);
    }
      handleCanceledit(): void{
        this.editModalOpen = false;
        this.addchangeleave = false;
      }
}