import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';

@Component({
    styles: [`
    ul{
        list-style:none;
    }

    div.divider-subs div{
        margin-top:2rem;
    }
    nz-divider{
        margin: 0;
    }
    p{
        margin: 0;
        cursor:pointer;
        padding:10px;
        height:50px;
        border: 1px solid #f3f3f3;
        font-size: 16px;
        font-family: 'Segoe UI';
        font-weight: 300;
    }
    .active-tab{
        background: #85B9D5;
        color: #fff;
        padding:10px;
        font-weight: 600;
    }
        
    `],
    templateUrl: './accounting.html',
})

export class RecipientAccountingAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;

    loading: boolean = false;

    constructor(
        private timeS: TimeSheetService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private listS: ListService,
        private router: Router,
        private globalS: GlobalService,
        private cd: ChangeDetectorRef
    ) {
        this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            if (data instanceof NavigationEnd) {
                if (!this.sharedS.getPicked()) {
                    this.router.navigate(['/admin/recipient/personal'])
                }
            }
        });

        this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
            this.user = data;
            if (this.globalS.isCurrentRoute(this.router, 'profile')) {
                // this.search(data);
            }
        });
    }

    ngOnInit(): void {        
        // this.user = this.sharedS.getPicked();
        // this.search(this.user);
    }

    ngOnDestroy(): void {
        // this.unsubscribe.next();
        // this.unsubscribe.complete();
    }


}