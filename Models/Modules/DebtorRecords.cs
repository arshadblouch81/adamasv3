using System;

namespace Adamas.Models.Modules
{
    public class DebtorRecords
    {
        public string Group { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Branches { get; set; }
        public string Programs { get; set; }
        public string Categories { get; set; }

    }
}