﻿using System.Collections.Generic;

namespace AdamasV3.Models.Dto
{
    public class XeroDto
    {
        public List<EmployeeXeroDto> Employees { get; set; }
    }
}
