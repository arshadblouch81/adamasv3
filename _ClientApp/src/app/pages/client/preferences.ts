
import {takeUntil} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import  { ClientService, GlobalService } from '../../services/index';
import { Subject } from 'rxjs';



@Component({
    selector: 'preferences-client',
    templateUrl: './preferences.html',
    styles:[
        `
        h3{
            margin:15px 0;
        }
        ul{
            list-style: none;
        }
        .list-pref{
            overflow-y: auto;
            height: auto;
            max-height: 25rem;
        }
        .list-pref-prim{
            overflow-y: auto;
            height: auto;
            max-height: 17rem;
        }
        li {
            width:100%;
            display:inline-block;
            padding:10px;
        }
        li:nth-child(odd){
            background:#f3f3f3;
        }
        li:nth-child(even){
            background:#e0e0e0;
        }
        li label{
            display:block;
        }
        div.list{
            display:block;
            margin:2px 5px;     
        }
        div.list clr-checkbox{
            display:inline-block;
            float:left;
            margin:0;
        }
        .switch{
            margin: 4rem 0;
            text-align: center;
        }
        
        `
    ]
})

export class PreferencesClient implements OnInit, OnDestroy {
    @Input() id;

    private unsubscribe$ = new Subject()
    user: any;    
    lists: Array<any> = [];

    constructor(
        private clientS: ClientService,
        private globalS: GlobalService
    ){

    }

    ngOnInit(){      
        this.user = this.id || this.globalS.userProfile.accountNo;
        this.clientS.getcompetencies(this.user)
            .pipe(takeUntil(this.unsubscribe$)).subscribe(data => {
            this.lists = data;
        })

        this.listpreferences();
    }
    
    ngOnDestroy(): void{
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }


    ifShow(data:any):boolean{
        var len = data.competenciesList.filter(x => x.selected == true);
        if(len.length > 0)
            return true;
        return false;
    }

    get listThatAreChecked(){
        if(this.lists && this.lists.length > 0){
            for(var list of this.lists){
                for(var comp of list.competenciesList){
                    if(comp.selected){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    getSelectedPreference(){
        var tempArr: Array<string> = [];

        this.lists.forEach(x => {
            x.competenciesList.forEach(b => {
                console.log(b)
                if(b.selected){
                    tempArr.push(b.description);
                }
            });
        });
        return tempArr;
    }

    postpreference(){
        this.clientS.postpreferences(this.globalS.decode().uniqueID, this.getSelectedPreference())
            .subscribe(data => {
                if(data){
                    this.globalS.sToast('Sucess','Your preferences has been changed');
                }
            })
    }

    listpreferences(){
        this.clientS.getpreferences(this.globalS.decode().uniqueID)
            .subscribe(data => console.log(data))
    }


}