import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';

import {
    RouteGuard,
    AdminStaffRouteGuard,
    CanDeactivateGuard,
    LoginGuard,
    AdminRouteGuard,
    ByPassGuard
  } from '@services/index'
  
import {  
    StaffAvailability,
    VehicleAvailability,
    AIAssistant,    
    DayManagerMain,
    DayManagerAdmin
  } from  './index';

  const routes: Routes = [
    {
        path: '',
        component: DayManagerMain,
        canDeactivate: [CanDeactivateGuard]
      },
{
      path: 'daymanager',
      component: DayManagerAdmin,
      canDeactivate: [CanDeactivateGuard]
    },
    {
      path: 'staff-availability',
      component: StaffAvailability
    },
    {
      path: 'vehicle-availability',
      component: VehicleAvailability
    },
    {
        path: 'ai-assistant',
        component: AIAssistant
      },
      {
      path: 'daymanager-main',
      component : DayManagerMain,
      children: [
        {
          path: '',
          loadChildren: () => import('./daymanager-main').then(m => m.DayManagerMain)
        }
      ]
    }
];

@NgModule({
    imports: [
      FormsModule, 
      CommonModule,
      ReactiveFormsModule,
      RouterModule.forChild(routes),
      NzMessageModule,
      NzNotificationModule,
      NzSelectModule,
      NzGridModule
      
    ],
    declarations: [
     
    ],
    providers: [
      DatePipe,
      DecimalPipe
    ],
    exports: [
  
    ]
  })
  
  export class DayManagerModule {}
  