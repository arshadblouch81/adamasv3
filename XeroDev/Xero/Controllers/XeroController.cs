﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Caching.Memory;

using Xero.Models;
using System.Net.Http.Headers;
using Xero.Services;
using Xero.Interfaces;

using Xero.NetStandard.OAuth2.Api;
using Xero.NetStandard.OAuth2.Client;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Model.PayrollAu;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Xero.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class XeroController : ControllerBase
    {
        private static string CLIENT_ID = "66AFB5EBEE304BE985F69083FAEEBD08";
        private static string SCOPES = "offline_access openid profile email accounting.transactions accounting.settings accounting.contacts payroll.timesheets payroll.timesheets.read payroll.payruns";
        private static string CODE_VERIFIER = "3kP9mLPhY-zJ1EhvKb-0Y81rGQQu9cpyHh__Fgei8h0";


        public const string ConnectionsUrl = "https://api.xero.com/connections";
        public const string REDIRECT_URI = "https://localhost:44393/api/xero/callback";

        private static string STATE = "123456789";

        public const string AuthorisationUrl = "https://login.xero.com/identity/connect/authorize";

        public static DateTime CurrentDateTime = DateTime.Now;


        private IMemoryCache _cache;
        private readonly IXeroService _xeroS;
        public XeroController(
            IMemoryCache cache,
            IXeroService xeroS
            )
        {
            _cache = cache;
            _xeroS = xeroS;
        }

        // GET: api/<XeroController>
        [HttpGet]
        public IActionResult GetUrl()
        {
            var clientId = CLIENT_ID;
            var scopes = Uri.EscapeUriString(SCOPES);
            var redirectUri = REDIRECT_URI;
            var state = STATE;

            string codeChallenge;
            using (var sha256 = SHA256.Create())
            {
                var challengeBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(CODE_VERIFIER));
                codeChallenge = Convert.ToBase64String(challengeBytes)
                    .TrimEnd('=')
                    .Replace('+', '-')
                    .Replace('/', '_');
            }
            var authLink = $"{AuthorisationUrl}?response_type=code&client_id={clientId}&redirect_uri={redirectUri}&scope={scopes}&state={state}&code_challenge={codeChallenge}&code_challenge_method=S256";
            return Redirect(authLink);
        }

        [HttpGet("callback")]
        public async Task<IActionResult> GetCallback()
        {
            string code = HttpContext.Request.Query["code"].ToString();
            string scope = HttpContext.Request.Query["scope"].ToString();
            string state = HttpContext.Request.Query["state"].ToString();

            const string url = "https://identity.xero.com/connect/token";

            var client = new HttpClient();
            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
                new KeyValuePair<string, string>("client_id", CLIENT_ID),
                new KeyValuePair<string, string>("code", code),
                new KeyValuePair<string, string>("redirect_uri", REDIRECT_URI),
                new KeyValuePair<string, string>("code_verifier", CODE_VERIFIER),
            });
           
            var response = await client.PostAsync(url, formContent);

            //read the response and populate the boxes for each token
            //could also parse the expiry here if required
            var content = await response.Content.ReadAsStringAsync();
            var tokens = JObject.Parse(content);

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromDays(1));

            string id_token = tokens["id_token"]?.ToString();
            _cache.Set("id_token", id_token, cacheEntryOptions);

            string access_token = tokens["access_token"]?.ToString();
            _cache.Set("access_token", access_token, cacheEntryOptions);


            string refresh_token = tokens["refresh_token"]?.ToString();
            _cache.Set("refresh_token", refresh_token, cacheEntryOptions);

            return Ok(new Tokens()
            {
                IdToken = id_token,
                AccessToken = access_token,
                RefreshToken = refresh_token
            });
        }

        [HttpGet("tokens")]
        public async Task<IActionResult> GetTokens()
        {
            string id_token, access_token, refresh_token;

            _cache.TryGetValue("id_token", out id_token);
            _cache.TryGetValue("access_token", out access_token);
            _cache.TryGetValue("refresh_token", out refresh_token);

            var tenants = await _xeroS.GetTenants();

            return Ok(new Tokens()
            {
                IdToken = id_token,
                AccessToken = access_token,
                RefreshToken = refresh_token,
                Tenants = tenants
            });
        }

        [HttpGet("tenants")]
        public async Task<IActionResult> GetTenants()
        {
            return Ok(await _xeroS.GetTenants());
        }

        //api/xero/payitems
        [HttpGet("payitems")]
        public async Task<IActionResult> GetPayItems()
        {
            return Ok(await _xeroS.GetPayItems());
        }

        //api/xero/employees
        [HttpGet("employees")]
        public async Task<IActionResult> GetEmployees()
        {
            return Ok(await _xeroS.GetEmployees());
        }

        //api/xero/employee
        [HttpGet("employee/{employeeID}")]
        public async Task<IActionResult> GetEmployees(string employeeID)
        {
            return Ok(await _xeroS.GetEmployee(employeeID));
        }

        //api/xero/timesheet
        [HttpGet("timesheet/{employeeID}")]
        public async Task<IActionResult> GetTimesheet(string employeeID)
        {
            return Ok(await _xeroS.GetTimesheet(employeeID));
        }

        //api/xero/timesheet
        [HttpGet("timesheets")]
        public async Task<IActionResult> GetTimesheets()
        {
            return Ok(await _xeroS.GetTimesheets());
        }


        //api/xero/timesheet
        // Add Payroll Calendar
        [HttpPost("timesheets")]
        public async Task<IActionResult> PostTimesheet()
        {
            List<Timesheet> timesheet = new List<Timesheet>() {
                new Timesheet(){
                    EmployeeID = Guid.Parse("80062955-9bc6-4c8e-8cb2-e86148d9923f"),
                    StartDate = DateTime.Now.AddMonths(-2),
                    EndDate = DateTime.Now.AddMonths(12),
                    Status = TimesheetStatus.DRAFT,
                    TimesheetLines = new List<TimesheetLine>() {
                        new TimesheetLine(){
                           EarningsRateID = Guid.Parse("146125ff-9611-4086-9b7c-fbe73b8ff706"),
                           NumberOfUnits = new List<double>(){ 0,8,8,8,8,8,0 }
                        }
                    }
                }
            };

            return Ok(await _xeroS.PostTimesheet(timesheet));
        }

        //api/xero/payroll-calendar
        [HttpPost("payroll-calendar")]
        public async Task<IActionResult> PostPayrollCalendar()
        {
            List<PayrollCalendar> payrollCalendar = new List<PayrollCalendar>() {
                new PayrollCalendar(){
                  Name = "Weekly",
                  CalendarType = CalendarType.WEEKLY,
                  StartDate = DateTime.Now,
                  PaymentDate = DateTime.Now.AddDays(7),
                }
            };

            return Ok(await _xeroS.PostPayrollCalendar(payrollCalendar));
        }

        //api/xero/payroll-calendar
        [HttpGet("payroll-calendar")]
        public async Task<IActionResult> GetPayrollCalendars()
        {
            return Ok(await _xeroS.GetPayrollCalendars());
        }

        //api/xero/payrun
        [HttpPost("payrun")]
        public async Task<IActionResult> PostPayRun()
        {
            List<PayRun> payRuns = new List<PayRun>();
            
            PayRun payRun = new PayRun();
            payRun.PayrollCalendarID = Guid.Parse("b64bda96-888b-46c8-a5e9-30fa696d3d74");

            payRuns.Add(payRun);

            return Ok(await _xeroS.PostPayRun(payRuns));
        }

        //api/xero/payruns
        [HttpGet("payruns")]
        public async Task<IActionResult> GetPayruns()
        {
            return Ok(await _xeroS.GetPayruns());
        }



    }


    public class Tokens
    {
        public string IdToken { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public List<Tenant> Tenants { get; set; }
    }
}
