
import {forkJoin,  Observable } from 'rxjs';
import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { ClientService } from '@services/index'

interface Results {
    approvedPrograms: Array<any>,
    approvedServices: Array<any>,
    servicePlan: Array<any>
}

@Component({
    selector: 'recipient-services',
    templateUrl: './recipient-services.html',
    providers: [
        {
           provide: NG_VALUE_ACCESSOR,
           useExisting: forwardRef(() => RecipientServicesComponent),
           multi: true
        }
    ]
})


export class RecipientServicesComponent implements ControlValueAccessor{
    
    private onChange: Function
    private onTouch: Function

    user: any
    result: Results
    loading: boolean = false

    constructor(
        private clientS: ClientService
    ){

    }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        const id = this.user.code
        this.loading = true
        forkJoin(            
            this.clientS.getprogramsapproved(id),
            this.clientS.getservicesapproved(id),
            this.clientS.getcurrentcareplan(id)
        ).subscribe(data => {
            this.loading = false
            this.result = {
                approvedPrograms: data[0],
                approvedServices: data[1],
                servicePlan: data[2]
            }
        })    
    }
}