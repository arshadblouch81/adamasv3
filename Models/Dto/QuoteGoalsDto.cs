﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class QuoteGoalsDto
    {
        public string Goal { get; set; }
        public List<string> Strategies { get; set; }
    }
}
