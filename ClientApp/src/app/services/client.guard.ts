import { Injectable } from '@angular/core';
import {
    CanActivate,
    Router,
    CanActivateChild,
    ActivatedRouteSnapshot,
    ActivatedRoute,
    RouterStateSnapshot
} from '@angular/router';
import { Observable, Subject, EMPTY, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
// import 'rxjs/add/operator/map'
import {  UserService } from '@services/index';
import { ViewClientPortalDto } from '@services/global.service';

@Injectable()
export class ClientGuard implements CanActivate{

    constructor(private router: Router, private userS: UserService) { }
    
    canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | boolean  {
        
        return this.userS.getclientPortalView().pipe(
            switchMap((data: ViewClientPortalDto) => {           
                if(state.url == '/client/document' && data.showDocuments == false){
                    return of(false);
                }

                if(state.url == '/client/notes' && data.showNotesTab == false){
                    return of(false);
                }
                return of(true);
            })
        );
    }
    

}