import {
    RouterModule,
    Routes
} from '@angular/router'

import { NgModule } from '@angular/core';

import { RemoveFirstLast, FilterPipe, KeyFilter, MomentTimePackage, KeyValueFilter, FileNameFilter, FileSizeFilter, MonthPeriodFilter, SplitArrayPipe } from './pipes/pipes';
import { LoginComponent } from './pages/login/login'
import { RouteGuard, LoginGuard } from './services/index'

// import { MembersComponent } from './shared/members/members.component'
// import { DetailsComponent } from './shared/members/details/details.component'

import {
    NumberDirective,
    RightClickDirective,
    NumberInputDirective,
    AccountGenerateDirective
} from './directive/index'

import {
    HomeClient,
    ProfileClient,
    BookingClient,
    CalendarClient,
    HistoryClient,
    PackageClient,
    PreferencesClient,
    ShiftClient,
    DocumentClient,
    NotesClient,
    SettingsClient
} from './pages/client/index';

import {
    HomeAdmin,
    DayManagerAdmin,
    AttendanceAdmin,
    RecipientsAdmin,
    ReportsAdmin,
    RostersAdmin,
    StaffAdmin,
    TimeSheetAdmin,
    BudgetAdmin,
    SessionsAdmin
} from './pages/admin/index';

import {
    ConfigurationComponent
} from './pages/admin/config'

import {
    LandingComponent
} from './pages/admin/landing'

import {
    HomeProvider,
    CalendarProvider,
    HistoryProvider,
    PackageProvider,
    ProfileProvider,
    DocumentProvider
} from './pages/provider/index'

import {
    ClientPackagePrint
} from './pages/print/index';

import {
    ActionButton,
    SideNav,
    HeaderNav,
    SuburbComponent,
    MonthPicker,
    TimePicker,
    DatePicker,
    SearchListComponent,
    ArisTable,
    SearchTimesheet,
    AllocateStaff,
    ProfilePage,
    Search,
    IntakeNoDataComponent,
    OptionControlComponent,
    OptionSaveEditComponent,
    ContactDetails,
    DragDropComponent,
    CheckboxComponent,
    RatingsComponent,
    PaginateComponent,
    RecipientDetailsComponent,
    RecipientServicesComponent,
    RecipientCaseNoteComponent,
    RecipientOPNoteComponent,
    RecipientWorkerComponent,
    StaffCompetenciesComponent,
    StaffDetailsComponent,
    StaffOpComponent,
    DmServiceNotesComponent,
    DmTasksComponent,
    DmExtraInfoComponent,
    DmAuditComponent,
    DmDataSetComponent,
    QualifiedStaffComponent,
    SelectListComponent,
    MembersComponent,
    DetailsComponent
} from './shared/index';

import {
    IntakeAlert,
    IntakeBranch,
    IntakeConsents,
    IntakeFunding,
    IntakeGoals,
    IntakeGroups,
    IntakeHome,
    IntakePlacements,
    IntakePlans,
    IntakeCarePlan,
    IntakeServices,
    IntakeStaff
} from './pages/admin/intake-details-recipients/index'

import {
    ClinicalAlerts,
    ClinicalDiagnoses,
    ClinicalHome,
    ClinicalMedications,
    ClinicalNotes,
    ClinicalProcedures,
    ClinicalReminder
} from './pages/admin/clinical/index'

import {
    PrintComponent
} from './pages/print/print'

import {
    LoginSessionsComponent
} from './pages/login-sessions/login-sessions.component'
import { ReportViewerComponent } from './pages/report-viewer/report-viewer.component';
import { ReportComponent } from './pages/reports/report';
import { WijmoComponent } from './pages/wijmo/wijmo';
import { SampleReports } from './pages/sample-reports/sample-reports'
import { ViewComponent } from './pages/viewer/view.component'
export const routes: Routes = [
    {
        path: '',
        canActivate: [LoginGuard],
        component: LoginComponent
    },
    {
        path: 'client',
        component: HomeClient,
        canActivate: [RouteGuard],
        canActivateChild: [RouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full'
            },
            {
                path: 'profile',
                component: ProfileClient
            },
            {
                path: 'booking',
                component: BookingClient
            },
            {
                path: 'calendar',
                component: CalendarClient
            },
            {
                path: 'notes',
                component: NotesClient
            },
            {
                path: 'history',
                component: HistoryClient
            },
            {
                path: 'package',
                component: PackageClient
            },
            {
                path: 'preferences',
                component: PreferencesClient
            },
            {
                path: 'timesheet',
                component: ShiftClient
            },
            {
                path: 'document',
                component: DocumentClient
            },
            {
                path: 'members',
                component: MembersComponent
            },
            {
                path: 'members/:id',
                component: DetailsComponent
            },
            {
                path: 'settings',
                component: SettingsClient
            }
        ]
    },
    {
        path: 'admin',
        component: HomeAdmin,
        canActivate: [RouteGuard],
        canActivateChild: [RouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'landing',
                pathMatch: 'full'
            },
            {
                path: 'landing',
                component: LandingComponent
            },
            {
                path: 'daymanager',
                component: DayManagerAdmin,
            },
            {
                path: 'config',
                component: ConfigurationComponent
            },
            {
                path: 'sessions',
                component: SessionsAdmin
            },
            {
                path: 'recipients',
                component: RecipientsAdmin,
                children: [
                    {
                        path: 'intake-details',
                        component: IntakeHome,
                        children: [
                            {
                                path: '',
                                redirectTo: 'branches',
                                pathMatch: 'full'
                            },
                            {
                                path: 'branches',
                                component: IntakeBranch,
                            },
                            {
                                path: 'funding',
                                component: IntakeFunding,
                            },
                            {
                                path: 'goals',
                                component: IntakeGoals,
                            },
                            {
                                path: 'plans',
                                component: IntakePlans,
                            },
                            {
                                path: 'care-plan',
                                component: IntakeCarePlan,
                            },
                            {
                                path: 'services',
                                component: IntakeServices,
                            },
                            {
                                path: 'placements',
                                component: IntakePlacements,
                            },
                            {
                                path: 'staff',
                                component: IntakeStaff,
                            },
                            {
                                path: 'alerts',
                                component: IntakeAlert,
                            },
                            {
                                path: 'consents',
                                component: IntakeConsents,
                            },
                            {
                                path: 'groups',
                                component: IntakeGroups,
                            },
                        ]
                    },
                    {
                        path: 'clinical',
                        component: ClinicalHome,
                        children: [
                            {
                                path: '',
                                redirectTo: 'diagnosis',
                                pathMatch: 'full'
                            },
                            {
                                path: 'diagnosis',
                                component: ClinicalDiagnoses
                            },
                            {
                                path: 'procedures',
                                component: ClinicalProcedures
                            },
                            {
                                path: 'medications',
                                component: ClinicalMedications
                            },
                            {
                                path: 'reminders',
                                component: ClinicalReminder
                            },
                            {
                                path: 'alerts',
                                component: ClinicalAlerts
                            },
                            {
                                path: 'notes',
                                component: ClinicalNotes
                            }
                        ]
                    }
                ]
            },
            {
                path: 'staff',
                component: StaffAdmin
            },
            {
                path: 'rosters',
                component: RostersAdmin
            },
            {
                path: 'timesheet',
                component: TimeSheetAdmin
            },
            {
                path: 'attendance',
                component: AttendanceAdmin
            },
            {
                path: 'reports',
                component: ReportsAdmin
            },
            {
                path: 'members',
                component: MembersComponent
            },
            {
                path: 'members/:id',
                component: DetailsComponent
            },
            {
                path: 'budget',
                component: BudgetAdmin
            }
        ]
    },
    {
        path: 'provider',
        component: HomeProvider,
        canActivate: [RouteGuard],
        canActivateChild: [RouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'profile',
                pathMatch: 'full'
            },
            {
                path: 'profile',
                component: ProfileProvider
            },
            {
                path: 'calendar',
                component: CalendarProvider
            },
            {
                path: 'history',
                component: HistoryProvider
            },
            {
                path: 'package',
                component: PackageProvider
            },
            {
                path: 'document',
                component: DocumentProvider
            }
        ]
    },
    {
        path: 'print',
        component: PrintComponent
    },
    {
        path: 'report',
        component: ReportComponent,
    },
    {
        path: 'wijmo',
        component: WijmoComponent
    },
    {
        path: 'traccsadmin',
        component: StaffAdmin
    },
    {
        path: 'print/package',
        component: ClientPackagePrint
    },
    {
        path: 'sample-reports',
        component: SampleReports,
        children: [
            {
                path: '',
                component: ViewComponent
            },
            {
                path: 'view/:id',
                component: ViewComponent
            }
        ]
    },
    {
        path: 'login-sessions',
        component: LoginSessionsComponent
    }
    // {
    //     path: '**',
    //     redirectTo: ''
    // },
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { useHash: true })
    ],
    exports: [
        RouterModule
    ]
})

export class RouteModule { }

export const components = [
    LoginComponent,
    HomeClient,
    ProfileClient,
    BookingClient,
    CalendarClient,
    HistoryClient,
    NotesClient,
    PackageClient,
    PreferencesClient,
    ShiftClient,
    DocumentClient,
    SettingsClient,
    HomeProvider,
    CalendarProvider,
    HistoryProvider,
    PackageProvider,
    ProfileProvider,
    DocumentProvider,
    HomeAdmin,
    AttendanceAdmin,
    DayManagerAdmin,
    RecipientsAdmin,
    ReportsAdmin,
    RostersAdmin,
    StaffAdmin,
    TimeSheetAdmin,
    ActionButton,
    SideNav,
    HeaderNav,
    RemoveFirstLast,
    FilterPipe,
    SplitArrayPipe,
    MomentTimePackage,
    KeyFilter,
    KeyValueFilter,
    SuburbComponent,
    MonthPicker,
    TimePicker,
    DatePicker,
    SearchListComponent,
    ArisTable,
    SearchTimesheet,
    ReportComponent,
    AllocateStaff,
    ProfilePage,
    PrintComponent,
    Search,
    IntakeAlert,
    IntakeBranch,
    IntakeConsents,
    IntakeFunding,
    IntakeGoals,
    IntakeHome,
    IntakeGroups,
    IntakePlacements,
    IntakePlans,
    IntakeCarePlan,
    IntakeServices,
    IntakeStaff,
    IntakeNoDataComponent,
    ClinicalAlerts,
    ClinicalDiagnoses,
    ClinicalHome,
    ClinicalMedications,
    ClinicalNotes,
    ClinicalProcedures,
    ClinicalReminder,
    OptionControlComponent,
    OptionSaveEditComponent,
    ConfigurationComponent,
    ContactDetails,
    DragDropComponent,
    CheckboxComponent,
    RatingsComponent,
    NumberDirective,
    RightClickDirective,
    NumberInputDirective,
    AccountGenerateDirective,
    PaginateComponent,
    RecipientDetailsComponent,
    RecipientServicesComponent,
    RecipientCaseNoteComponent,
    RecipientOPNoteComponent,
    RecipientWorkerComponent,
    WijmoComponent,
    StaffCompetenciesComponent,
    StaffDetailsComponent,
    StaffOpComponent,
    DmServiceNotesComponent,
    DmTasksComponent,
    DmExtraInfoComponent,
    DmAuditComponent,
    DmDataSetComponent,
    QualifiedStaffComponent,
    LandingComponent,
    SelectListComponent,
    FileNameFilter,
    FileSizeFilter,
    MonthPeriodFilter,
    MembersComponent,
    DetailsComponent,
    BudgetAdmin,
    ClientPackagePrint,
    SessionsAdmin,
    SampleReports,
    ReportViewerComponent,
    ViewComponent
]
