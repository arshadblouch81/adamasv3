import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

import { TimeSheetService, GlobalService } from '@services/index'

@Component({
    selector: 'dm-servicenotes',
    templateUrl: './dm-servicenotes.html',
    styles: [`
        .note-detail{
            display: flex;
            padding: 5px;
            margin: 7px 0;
            background: #f9f9f9;
            border: 1px solid #d8d8d8;
            border-radius: 5px;
        }
        .note-detail > div:first-child{
            flex: 1;
        }
        .note-detail > div:nth-child(2){
            flex:3;
        }
        .note-detail > div:last-child{
            display: flex;
            flex-direction: column;
            min-height: 2rem;
        }
    `],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DmServiceNotesComponent),
            multi: true
        }
    ]
})

export class DmServiceNotesComponent implements ControlValueAccessor {
    private onChange: Function
    private onTouch: Function

    user: any
    results: Array<any>

    constructor(
        private timeS: TimeSheetService,
        private globalS: GlobalService
    ){ }

    writeValue(value: any){
        if(value != null) {
            this.user = value
            this.search(this.user)
        }
    }

    registerOnChange(fn: any){
        this.onChange = fn
    }

    registerOnTouched(fn: any){
        this.onTouch = fn
    }

    search(user: any){
        this.timeS.getservicenotes(user.recordno)
            .subscribe(data => {
                this.results = data.map(x => {
                    return {
                        alarm: x.alarm,
                        creator: x.creator,
                        detail: x.detail,
                        date: this.globalS.filterDate(x.detailDate),
                        time: this.globalS.filterTime(x.detailDate),
                        personID: x.personID,
                        recordNo: x.recordNo,
                        whoCode: x.whoCode
                    }
                });
            })
    }
}