﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class PensionPercentDto
    {
        public string Pension { get; set; }
        public string BasicFeePercent { get; set; }
    }
}
