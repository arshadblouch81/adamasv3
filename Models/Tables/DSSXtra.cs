using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Adamas.Models.Tables
{
    public class DSSXtra
    {
    [Key]
    public int RecordNumber { get; set; }
    public string PersonID { get; set; }
    public string DEXReferralSource { get; set; }
    public string DEXIndiginousStatus { get; set; }
    public string DEXDVACardholderStatus { get; set; }
    public string DEXAccomodation { get; set; }
    public string ReferralPurpose { get; set; }
    public string ReferralType { get; set; }
    public string AssistanceReason { get; set; }
    public string AssistanceReasons { get; set; }
    public bool HasCarer { get; set; }
    public bool HasDisabilities { get; set; }
    public string FirstArrivalYear { get; set; }
    public string FirstArrivalMonth { get; set; }
    public string VisaCategory { get; set; }
    public string Ancestry { get; set; }
    public string ExitReasonCode { get; set; }
    public bool IsHomeless { get; set; }
    public string HouseholdComposition { get; set; }
    public string MainIncomSource { get; set; }
    public string IncomFrequency { get; set; }
    public string IncomeAmount { get; set; }
}
    }