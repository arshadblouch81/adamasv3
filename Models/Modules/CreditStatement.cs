using System;

namespace Adamas.Models.Modules{
    public class CreditStatement
    {
        public string ClientCode { get; set;}
        public string Program { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}