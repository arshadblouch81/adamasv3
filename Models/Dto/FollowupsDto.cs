﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdamasV3.Models.Dto
{
    public class FollowupsDto
    {
        public string Label { get; set; }
        public string DateCounter { get; set; }
    }
}
