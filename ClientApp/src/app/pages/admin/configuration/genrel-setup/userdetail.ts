import { ChangeDetectorRef, Component, OnInit,HostListener } from '@angular/core';
import { FormBuilder,Validators, FormGroup } from '@angular/forms';
import { GlobalService,ListService,TimeSheetService,RECIPIENT_SECURITY,STAFF_SECURITY,ROSTER_SECURITY,DAYMANAGER_SECURITY,ADMIN_lEVEL,AWARD_ENFORCEMENTS, NavigationService} from '@services/index';

import { SwitchService } from '@services/switch.service';
import { MenuService } from '@services/menu.service';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd';
import { PrintService } from '@services/print.service';
import { Router } from '@angular/router';
import { Console } from 'console';

@Component({
    templateUrl: './userdetail.html',
    styles: [`
        .force-chk{
            margin:7px 5px 0px 15px;
        }
        .ant-modal-header {
            padding: 10px 15px;
            text-align: center;
            border-bottom: 1px solid #eee;
            background-color: #85B9D5 !important;
            color: #fff !important;
            -webkit-border-top-left-radius: 1px;
            -webkit-border-top-right-radius: 1px;
            -moz-border-radius-topleft: 1px;
            -moz-border-radius-topright: 1px;
            border-top-left-radius: 1px;
            border-top-right-radius: 1px;
        }
        div > label{
            margin-bottom:2px;
            display:block;
        }
        .ant-card-small>.ant-card-head {
            min-height:30px
        }
        tr.highlight {
            background-color: #85B9D5 !important; /* Change to your preferred highlight color */
        }
    `],
})
export class UserDetail implements OnInit {
    
    tableData: Array<any>;
    loading: boolean = false;
    modalOpen: boolean = false;
    current: number = 0;
    inputForm: FormGroup;
    rosterForm: FormGroup;
    viewScopeForm: FormGroup;
    dayManagerForm:FormGroup;
    clientPortalForm:FormGroup;
    mainMenuForm:FormGroup;
    documents: FormGroup;
    mobileForm:FormGroup;
    recipientOptions:FormGroup;
    postLoading: boolean = false;
    isUpdate: boolean = false;
    modalVariables: any;
    dateFormat: string ='dd/MM/yyyy';
    inputVariables:any;
    title :string = "Add User Details";
    private unsubscribe: Subject<void> = new Subject();
     
    token:any;
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;  
    check : boolean = false;
    userRole:string="userrole";
    whereString :string="Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
    temp_title: any;
    branchesList: any;
    userList:any;
    diciplineList: any;
    casemanagers: any;
    categoriesList: any;
    selectedbranches: string[];
    testcheck : boolean = false;
    selectedPrograms: string[];
    selectedCordinators: string[];
    selectedReminders:string[];
    selectedCategories: string[];
    selectedStaffCategories: string[];
    
    
    allBranches:boolean = true;
    allBranchIntermediate:boolean = false;
    
    allProgarms:boolean = true;
    allprogramIntermediate:boolean = false;
    
    allcat:boolean = true;
    allCatIntermediate:boolean = false;
    
    allCordinatore:boolean = true;
    allCordinatorIntermediate:boolean = false;
    
    allReminders:boolean = true;
    allRemindersIntermediate:boolean = false;
    
    allStaffCat:boolean = true;
    allStaffCatIntermediate:boolean = false;
    
    staffjobcategories: any;
    
    checkBoxString: string = '10000000000000000000000';
    enableViewNoteCases:string = '00000';
    clientPortalViewNotes:string='00000';
    recipientRecordView : string = '1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
    userDetailObj: any;
    listbranches: any;
    traccsStaffCodes: any;
    traccsUserTypes: any;
    traccsLoginModes: any;
    recipient_security: any;
    staff_security: any;
    roster_security: any;
    daymanger_security: any;
    dashboard_admin_levels: string[];
    attendenceRecorModes: { "0": string; "3": string; "1": string; "2": string; };
    staffForm: FormGroup;
    passwordVisible = false;
    password?: string;
    awardenforcements: string[];
    searchTerm: string = '';
    currentIndex: number | null = null;
    reminderList: any;


    constructor(
        private globalS: GlobalService,
        private cd: ChangeDetectorRef,
        private listS:ListService,
        private timeS:TimeSheetService,
        private menuS:MenuService,
        private switchS:SwitchService,
        private formBuilder: FormBuilder,
        private http: HttpClient,
        private fb: FormBuilder,
        private printS:PrintService,
        private sanitizer: DomSanitizer,
        private navigationService: NavigationService,
        private router: Router,
        private ModalS: NzModalService
    ){}
    
    ngOnInit(): void {
        this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
        this.userRole = this.tocken.role;
        this.buildForm();
        this.loadData();
        this.loading = false;
        this.cd.detectChanges();
    }
    loadData(){
        this.loading = true;
        this.menuS.getUserList(this.check).subscribe(data => {
            this.tableData = data;
            this.userList  = data;
            this.loading = false;
        });
        this.populateDropDown();
    }
    populateDropDown(){
        this.listS.getlistbranches().subscribe(data => {
            this.listbranches = data;
        });
        this.listS.GetTraccsStaffCodes().subscribe(data => {
            this.traccsStaffCodes = data;
        });
        this.timeS.GetTracssUserTypes().subscribe(data => {
            this.traccsUserTypes = data;
        });
        this.timeS.GetTracssLoginModes().subscribe(data => {
            this.traccsLoginModes = data;
        });
        this.recipient_security   = RECIPIENT_SECURITY;
        this.staff_security       = STAFF_SECURITY;
        this.roster_security      = ROSTER_SECURITY;
        this.daymanger_security   = DAYMANAGER_SECURITY;
        this.dashboard_admin_levels = ADMIN_lEVEL;
        this.attendenceRecorModes = ['QRCODE','SIGNATURE','PIN CODE','BUTTONS'];
        this.awardenforcements     = AWARD_ENFORCEMENTS;
        this.getUserData();
    }
    getUserData() {
        return forkJoin([
            this.listS.getlistbranchesObj(),
            this.listS.getprogramsobj(),
            this.listS.getcategoriesobj(),
            this.listS.casemanagerslist(),
            this.listS.getstaffcategorylist(),
            this.menuS.getUserList(this.check),
        ]).subscribe(x => {
            this.branchesList       = x[0];
            this.diciplineList      = x[1];
            this.categoriesList     = x[2];
            this.casemanagers       = x[3];
            this.staffjobcategories = x[4];
            this.reminderList       = x[5];

            this.updateAllCheckedFilters(-1);
        });
    }
    
    updateAllCheckedFilters(filter: any): void {
        if (filter == 1 || filter == -1) {
            this.allBranchIntermediate = false; // Default to false before checking
    
            // If allBranches is true, check all branches
            if (this.allBranches) {
                this.branchesList = this.branchesList.map(item => ({
                    ...item,
                    checked: true // Check all branches
                }));
                this.selectedbranches = []; // Clear selected branches
            } else {
                // If allBranches is false, check only the branches in selectedbranches
                let selectedBranchesCount = 0;
                this.branchesList = this.branchesList.map(item => {
                    const isChecked = this.selectedbranches.includes(item.description);
                    if (isChecked) {
                        selectedBranchesCount++;
                    }
                    return {
                        ...item,
                        checked: isChecked
                    };
                });
    
                // If exactly one branch is checked, set intermediate to true
                if (selectedBranchesCount > 0 && selectedBranchesCount < this.branchesList.length) {
                    this.allBranchIntermediate = true;
                }
            }
        }
        if (filter === 2 || filter === -1) {
            this.allprogramIntermediate = false; // Reset intermediate state to false initially
        
            if (this.allProgarms) {
                // If allProgarms is true, check all items in diciplineList
                this.diciplineList = this.diciplineList.map(item => ({
                    ...item,
                    checked: true // Mark as checked
                }));
                this.selectedPrograms = []; // Clear selectedPrograms array
            } else {
                // If allProgarms is false, check only items in selectedPrograms
                let selectedProgramsCount = 0; // Track the count of selected programs
                this.diciplineList = this.diciplineList.map(item => {
                    const isChecked = this.selectedPrograms.includes(item.name); // Check if item is in selectedPrograms
                    if (isChecked) {
                        selectedProgramsCount++; // Increment count for selected programs
                    }
                    return {
                        ...item,
                        checked: isChecked // Set checked status based on selectedPrograms
                    };
                });
                // Set intermediate state if some (but not all) programs are selected
                if (selectedProgramsCount > 0 && selectedProgramsCount < this.diciplineList.length) {
                    this.allprogramIntermediate = true;
                }
            }
        }
        if (filter === 3 || filter === -1) {
            this.allCatIntermediate = false; // Reset intermediate state initially
        
            if (this.allcat) {
                // If allcat is true, mark all items in categoriesList as checked
                this.categoriesList = this.categoriesList.map(item => ({
                    ...item,
                    checked: true // Mark as checked
                }));
                this.selectedCategories = []; // Clear selectedCategories array
            } else {
                // If allcat is false, check only items in selectedCategories
                let selectedCategoriesCount = 0; // Track the count of selected categories
        
                this.categoriesList = this.categoriesList.map(item => {
                    const isChecked = this.selectedCategories.includes(item.description); // Check if item is in selectedCategories
                    if (isChecked) {
                        selectedCategoriesCount++; // Increment count for selected categories
                    }
                    return {
                        ...item,
                        checked: isChecked // Set checked status based on selectedCategories
                    };
                });
        
                // Set intermediate state if some (but not all) categories are selected
                if (selectedCategoriesCount > 0 && selectedCategoriesCount < this.categoriesList.length) {
                    this.allCatIntermediate = true;
                }
            }
        }
        
        if (filter === 4 || filter === -1) {
            this.allCordinatorIntermediate = false; // Reset intermediate state initially
        
            if (this.allCordinatore) {
                // If allCordinatore is true, mark all items in casemanagers as checked
                this.casemanagers = this.casemanagers.map(item => ({
                    ...item,
                    checked: true // Mark as checked
                }));
                this.selectedCordinators = []; // Clear selectedCordinators array
            } else {
                // If allCordinatore is false, check only items in selectedCordinators
                let selectedCordinatorsCount = 0; // Track the count of selected coordinators
        
                this.casemanagers = this.casemanagers.map(item => {
                    const isChecked = this.selectedCordinators.includes(item.description); // Check if item is in selectedCordinators
                    if (isChecked) {
                        selectedCordinatorsCount++; // Increment count for selected coordinators
                    }
                    return {
                        ...item,
                        checked: isChecked // Set checked status based on selectedCordinators
                    };
                });
        
                // Set intermediate state if some (but not all) coordinators are selected
                if (selectedCordinatorsCount > 0 && selectedCordinatorsCount < this.casemanagers.length) {
                    this.allCordinatorIntermediate = true;
                }
            }
        }
        
        if (filter === 5 || filter === -1) {
            this.allRemindersIntermediate = false; // Reset intermediate state initially
        
            if (this.allReminders) {
                // If allReminders is true, mark all items in reminderList as checked
                this.reminderList = this.reminderList.map(item => ({
                    ...item,
                    checked: true // Mark as checked
                }));
                this.selectedReminders = []; // Clear selectedReminders array
            } else {
                // If allReminders is false, check only items in selectedReminders
                let selectedRemindersCount = 0; // Track the count of selected reminders
        
                this.reminderList = this.reminderList.map(item => {
                    const isChecked = this.selectedReminders.includes(item.name); // Check if item is in selectedReminders
                    if (isChecked) {
                        selectedRemindersCount++; // Increment count for selected reminders
                    }
                    return {
                        ...item,
                        checked: isChecked // Set checked status based on selectedReminders
                    };
                });
        
                // Set intermediate state if some (but not all) reminders are selected
                if (selectedRemindersCount > 0 && selectedRemindersCount < this.reminderList.length) {
                    this.allRemindersIntermediate = true;
                }
            }
        }
        
        if (filter === 6 || filter === -1) {
            this.allStaffCatIntermediate = false; // Reset intermediate state initially
        
            if (this.allStaffCat) {
                // If allStaffCat is true, mark all items in staffjobcategories as checked
                this.staffjobcategories = this.staffjobcategories.map(item => ({
                    ...item,
                    checked: true // Mark as checked
                }));
                this.selectedStaffCategories = []; // Clear selectedStaffCategories array
            } else {
                // If allStaffCat is false, check only items in selectedStaffCategories
                let selectedStaffCategoriesCount = 0; // Track the count of selected staff categories
        
                this.staffjobcategories = this.staffjobcategories.map(item => {
                    const isChecked = this.selectedStaffCategories.includes(item.description); // Check if item is in selectedStaffCategories
                    if (isChecked) {
                        selectedStaffCategoriesCount++; // Increment count for selected categories
                    }
                    return {
                        ...item,
                        checked: isChecked // Set checked status based on selectedStaffCategories
                    };
                });
        
                // Set intermediate state if some (but not all) staff categories are selected
                if (selectedStaffCategoriesCount > 0 && selectedStaffCategoriesCount < this.staffjobcategories.length) {
                    this.allStaffCatIntermediate = true;
                }
            }
        }
        
    }
    updateSingleCheckedFilters(index:number): void {
        if (index === 1) {
            if (this.branchesList.every(item => !item.checked)) {
                this.allBranches = false;
                this.allBranchIntermediate = false;
              } else if (this.branchesList.every(item => item.checked)) {
                this.allBranches = true;
                this.allBranchIntermediate = false;
              } else {
                this.allBranchIntermediate = true;
              }
        }
        if(index == 2){
            if (this.diciplineList.every(item => !item.checked)) {
                this.allProgarms = false;
                this.allprogramIntermediate = false;
            } else if (this.diciplineList.every(item => item.checked)) {
                this.allProgarms = true;
                this.allprogramIntermediate = false;
            } else {
                this.allprogramIntermediate = true;
            }
        }
        
        if(index == 3){
            if (this.categoriesList.every(item => !item.checked)) {
                this.allcat = false;
                this.allCatIntermediate = false;
            } else if (this.categoriesList.every(item => item.checked)) {
                this.allcat = true;
                this.allCatIntermediate = false;
            } else {
                this.allCatIntermediate = true;
            }
        }
        if(index == 4){
            if (this.casemanagers.every(item => !item.checked)) {
                this.allCordinatore = false;
                this.allCordinatorIntermediate = false;
            } else if (this.casemanagers.every(item => item.checked)) {
                this.allCordinatore = true;
                this.allCordinatorIntermediate = false;
            } else {
                this.allCordinatorIntermediate = true;
            }
        }
        if(index == 5){
            if (this.reminderList.every(item => !item.checked)) {
                this.allReminders = false;
                this.allRemindersIntermediate = false;
            } else if (this.reminderList.every(item => item.checked)) {
                this.allReminders = true;
                this.allRemindersIntermediate = false;
            } else {
                this.allRemindersIntermediate = true;
            }
        }
        if(index == 6){
            if (this.staffjobcategories.every(item => !item.checked)) {
                this.allStaffCat = false;
                this.allStaffCatIntermediate = false;
            } else if (this.staffjobcategories.every(item => item.checked)) {
                this.allStaffCat = true;
                this.allStaffCatIntermediate = false;
            } else {
                this.allStaffCatIntermediate = true;
            }
        }
    }
    
    log(event: any,index:number) {
        this.testcheck = true;   
        if(index == 1)
            this.selectedbranches = event;
        if(index == 2)
            this.selectedPrograms = event;
        if(index == 3)
            this.selectedCategories = event;
        if(index == 4)
            this.selectedCordinators = event;  
        if(index == 5)
            this.selectedReminders = event;
        if(index == 6)
            this.selectedStaffCategories = event;
    }
   
    
    loadTitle()
    {
        return this.title
    }
    fetchAll(e){
        if(e.target.checked){
            this.whereString = "WHERE";
            this.loadData();
        }else{
            this.whereString = "Where ISNULL(DataDomains.DeletedRecord,0) = 0 AND (EndDate Is Null OR EndDate >= GETDATE()) AND ";
            this.loadData();
        }
    }
    
    showAddModal() {
        this.resetModal();
        this.inputForm.patchValue({
            recnum:0,
            name:'',
            password:'',
            staffcode:'',
            manualRosterCopy:false,
            autoCopyRoster:false,
            changeMasterRoster:false,
            ownRosterOnly:true,
            taServer:false,
            emailUserReports:true,
            awardEnforcement:'PREVENT',
            defAlertsTAB:'CONTACT ISSUES',
            keepOriginalAsImport:true,
            canMoveImportedDocuments:false,
            hideClientPhoneInApp:false,
            suppressEmailOnRosterNote:true,
            restrictTravelSameDay:false,
            viewClientDocuments:false,
            viewClientCareplans:false,
            useOPNoteAsShiftReport:false,
            allowTravelClaimWithoutNote:false,
            useServiceNoteAsShiftReport:false,
            multishiftAdminAndMultiple:false,
            allowClinicalNoteEntry:false,
            enableEmailNotification:true,
            pushPhonePrefix:false,
            enable_Shift_End_Alarm:false,
            enable_Shift_Start_Alarm:false,
            shiftReportReminder:false,
            viewClientPortalDocuments:false,
        })
        this.inputForm.patchValue({ //not added in forms and backend
            allowTypeAhead:false,
            limitProgramLookups:false,
            limitServiceLookups:false,
            limitStaffLookups:false,
            limitPayTypeLookups:false,
            allowProgramTransition:false,
            hideClientPhone:false,
            hiresGraphics:true,
            recipientCurrentDefault:true,
            staffCurrentDefault:true,
            MaxPreview:false,
            useQikShift:false,
            timesheetPreviousPeriod:false,
            accessCDC:false,
            reportPreview:false,
            sortTimesheetByDateTime:false,
        })
        
        this.inputForm.patchValue({
            approvedaymanager:false,
            recipmgtview:false,
            allowStaffSwap:false,
            adminChangeOutputType:true,
            adminChangeProgram:true,
            adminChangeActivityCode:true,
            adminChangePaytype:true,
            adminChangeDebtor:true,
            adminChangeBillAmount:true,
            adminChangeBillQty:true,
            attendeesChangeBillQty:true,
            attendeesChangeBillAmount:true,
            adminChangePayQty:true,
            lowChangeActivityCode:true,
            canEditNDIA:true,
            systemEmailer:false,
            addNewRecipient:false,
            azureaccount:"",
            sso:false,
            tmMode:0,
            mobileFutureLimit:0,
        })
        this.inputForm.patchValue({
            recipientDocFolder:null,
            force_RecipDocFolder:false,
            
            oniImportExportFolder:null,
            force_ONIImportFolder:false,
            
            oniArchiveFolder:null,
            force_ONIArchiveFolder:false,
            
            staffDocFolder:null,
            force_StaffDocFolder:false,
            
            staffRostersFolder:null,
            force_StaffRosterFolder:false,
            
            reportExportFolder:null,
            force_ReportExportFolder:false,
            
            reportSavesFolder:null,
            force_ReportSavesFolder:false,
            
            haccmdsFolder:null,
            force_HACCMDSFolder:false,
            
            cstdamdsFolder:null,
            force_CSTDAMDSFolder:false,
            
            nrcpmdsFolder:null,
            force_NRCPMDSFolder:false,
            
            payExportFolder:null,
            force_PayExportFolder:false,
            
            billingExportFolder:null,
            force_BillingExportFolder:false,
        })
        this.inputForm.patchValue({
            disableBtnReferral  : false,
            disableBtnReferOn   : false,
            disableBtnNotProceeding : false,
            disableBtnAssess    : false,
            disableBtnAdmit     : false,
            disableBtnUnWait    : false,
            disableBtnDischarge : false,
            disableBtnSuspend   : false,
            disableBtnReinstate : false,
            disableBtnDecease   : false,
            disableBtnAdmin     : false,
            disableBtnItem      : false,
            disableBtnPrint     : false,
            disableBtnRoster    : false,
            disableBtnMaster    : false,
            disableBtnOnHold    : false,
        })
        this.inputForm.patchValue({
            mmPublishPrintRosters:false,//cast to int
            mmTimesheetProcessing:false,//cast to int
            mmBilling:false,//cast to int
            mmPriceUpdates:false,//cast to int
            mmDexUploads:false,//cast to int
            mmndia:false,//cast to int
            mmHacc:false,//cast to int
            mmOtherDS:false,//cast to int
            mmAccounting:false,//cast to int
            mmAnalyseBudget:false,
            mmAtAGlance:false,
            UseCloudStaff:false,
            UseCloudRecipients:false,
            UseCloudRosters:false,
            UseCloudDaymanager:false,
            UseCloudTimesheets:false,
            UseCloudReports:false,
        })
        
        this.inputForm.patchValue({
            statistics:false,
            suggestedTimesheets:false,
            financial:false,
            CloudStaffLink:"",
            CloudReportsLink:"",
            CloudTimesheetsLink:"",
            CloudRostersLink:"",
            CloudDaymanagerLink:"",
            CloudRecipientsLink:"",
        })
        this.modalOpen = true;
    }
    
    resetModal() {
        this.current = 0;
        this.selectedbranches = [];
        
        this.allBranchIntermediate = false;
        this.allBranches = true;
        this.updateSingleCheckedFilters(1);

        this.allprogramIntermediate = false;
        this.allProgarms = true;
        this.updateSingleCheckedFilters(2);

        this.allCatIntermediate = false;
        this.allcat = false;
        this.updateSingleCheckedFilters(3);

        this.allCordinatorIntermediate = false;
        this.allCordinatore = false;
        this.updateSingleCheckedFilters(4);

        this.allRemindersIntermediate = false;
        this.allReminders = false;
        this.updateSingleCheckedFilters(5);

        this.allStaffCatIntermediate = false;
        this.allStaffCat = false;
        this.updateSingleCheckedFilters(6);

        this.inputForm.reset();
        this.postLoading = false;
    }


    getuserViewingTab(name:any){
        forkJoin([
            this.listS.getuserviewtabfilters("BRANCHES",name), //aganecy defined group / view filterbranches
            this.listS.getuserviewtabfilters("STAFFGROUP",name), //aganecy defined group / view filtercategories
            this.listS.getuserviewtabfilters("CASEMANAGERS",name), // recipient.coordintors /view filterCoord
            this.listS.getskills(),
        ]).subscribe(data => {
            
            this.branchesList.forEach(element => {
                if(data[0].includes(element.name)){
                    element.checked = true;
                }else{element.checked = false;}
            });
        });    
    }

    extractBranchNames(condition: string): string[] {
        // Use a regular expression to find branch names in the condition string
        const matches = condition.match(/Recipients\.Branch\s*=\s*'(\w+)'/g);
        
        if (!matches) {
            return []; // Return an empty array if no matches found
        }
    
        // Extract branch names from matches
        const branches = matches.map(match => match.match(/'(\w+)'/)?.[1] || '');
        
        // Filter out any empty strings (just in case) and return the array
        return branches.filter(branch => branch !== '');
    }
    extractProgramNames(condition: string): string[] {
        // Validate input to ensure it's a non-empty string
        if (typeof condition !== 'string' || !condition.trim()) {
            console.error('Invalid input: condition must be a non-empty string.');
            return [];
        }
    
        // Use a regular expression to match program names
        const matches = condition.match(/RecipientPrograms\.Program\s*=\s*'([^']+)'/g);
    
        if (!matches) {
            return []; // Return an empty array if no matches found
        }
    
        // Extract program names from matches
        const programNames = matches.map(match => match.match(/'([^']+)'/)?.[1] || '');
    
        // Filter out any empty strings and return the array
        return programNames.filter(name => name !== '');
    }
    extractCategories(condition: string): string[] {
        // Use a regular expression to find AgencyDefinedGroup values in the condition string
        const matches = condition.match(/Recipients\.AgencyDefinedGroup\s*=\s*'([^']+)'/g);
        
        if (!matches) {
            return []; // Return an empty array if no matches are found
        }
    
        // Extract group names from matches
        const groups = matches.map(match => match.match(/'([^']+)'/)?.[1] || '');
    
        // Filter out any empty strings (just in case) and return the array
        return groups.filter(group => group !== '');
    }
    extractCoordinators(condition: string): string[] {
        // Use a regular expression to match all RECIPIENT_CoOrdinator values in the condition string
        const matches = condition.match(/Recipients\.RECIPIENT_CoOrdinator\s*=\s*'([^']+)'/g);
    
        if (!matches) {
            return []; // Return an empty array if no matches are found
        }
    
        // Extract coordinator names from the matches
        const coordinators = matches.map(match => match.match(/'([^']+)'/)?.[1] || '');
    
        // Filter out any empty strings (just in case) and return the array
        return coordinators.filter(coordinator => coordinator !== '');
    }
    extractReminders(condition: string): string[] {
        // Use a regular expression to match all History.Creator values in the condition string
        const matches = condition.match(/History\.Creator\s*=\s*'([^']+)'/g);
    
        if (!matches) {
            return []; // Return an empty array if no matches are found
        }
    
        // Extract reminder (creator) names from the matches
        const reminders = matches.map(match => match.match(/'([^']+)'/)?.[1] || '');
    
        // Filter out any empty strings (just in case) and return the array
        return reminders.filter(reminder => reminder !== '');
    }
    extractStaffCategories(condition: string): string[] {
        // Use a regular expression to match all Staff.StaffGroup values in the condition string
        const matches = condition.match(/Staff\.StaffGroup\s*=\s*'([^']+)'/g);
    
        if (!matches) {
            return []; // Return an empty array if no matches are found
        }
    
        // Extract staff category names from the matches
        const categories = matches.map(match => match.match(/'([^']+)'/)?.[1] || '');
    
        // Filter out any empty strings (just in case) and return the array
        return categories.filter(category => category !== '');
    }
    showEditModal(index: any) {        
        
        this.title = "Edit User Detail"
        
        this.isUpdate = true;
        this.current = 0;


        const { 
            name,
        } = this.tableData[index];
        
        // this.getuserViewingTab(name);
        
        this.timeS.getuserinfodetail(name).subscribe(data => {
            this.userDetailObj = data;
            this.inputForm.patchValue(data);
            
            if(!this.traccsStaffCodes.includes(data['staffCode'])){
                this.traccsStaffCodes.push(data['staffCode']);
            }
            if(!this.listbranches.includes(data['homeBranch'])){
                this.listbranches.push(data['homeBranch']);
            }
            if(!this.traccsUserTypes.includes(data['usertype'])){
                this.traccsUserTypes.push(data['usertype']);
            }
            if(!this.traccsLoginModes.includes(data['loginMode'])){
                this.traccsLoginModes.push(data['loginMode']);
            }

            this.allBranches = data['viewAllBranches'];
            if(!this.allBranches){
                this.selectedbranches = this.extractBranchNames(data['viewFilterBranches']);
            }
            this.updateAllCheckedFilters(1);
            
            this.allProgarms = data['ViewAllProgram'];
            if(!this.allProgarms){
                this.selectedPrograms = this.extractProgramNames(data['viewFilter']);
            }
            
            this.updateAllCheckedFilters(2);

            this.allcat = data['viewAllCategories'];

            if(!this.allcat){
                this.selectedCategories = this.extractCategories(data['viewFilterCategory'])
            }

            this.updateAllCheckedFilters(3);

            this.allCordinatore = data['viewAllCoOrdinators'];
            

            if(!this.allCordinatore){
                this.selectedCordinators = this.extractCoordinators(data['viewFilterCoord'])
            }


            this.updateAllCheckedFilters(4);

            this.allReminders = data['viewAllReminders'];
            
            if(!this.allReminders){
                this.selectedReminders = this.extractReminders(data['viewFilterReminders'])
            }

            this.updateAllCheckedFilters(5);
            
            this.allStaffCat = data['viewAllStaffCategories'];

            if(!this.allStaffCat){
                this.selectedStaffCategories = this.extractStaffCategories(data['viewFilterStaffCategory'])
            }

            this.updateAllCheckedFilters(6);



            this.recipientRecordView = data['recipientRecordView'];
            this.checkBoxString      = data['staffRecordView'];
            this.clientPortalViewNotes = data['clientPortalViewNotes']
            

            this.inputForm.patchValue({
                fullAdmin: ((data['fullAdmin'] == 0 || data['fullAdmin'] == null) ? false : true),
                systemEmailer: ((data['systemEmailer'] == 0) ? false : true),
                sso: ((data['sso'] == 0 || data['sso'] == null) ? false : true),
                allowProgramTransition:((data['allowProgramTransition'] == 1) ? true : false),
                recipients: (this.inttoString(data['recipients'])),
                staff: (this.inttoString(data['staff'])),
                roster: (this.inttoString(data['roster'])),
                dayManager:(this.inttoString(data['dayManager'])),
                system: ((data['system'] == 999) ? true : false),
                financial:((data['financial'] == 999) ? true : false),
                invoiceEnquiry:((data['invoiceEnquiry'] == 999) ? true : false),
                statistics:((data['statistics'] == 999) ? true : false),
                timesheetUpdate:((data['timesheetUpdate'] == 999) ? true : false),
                manualRosterCopy:((data['manualRosterCopy'] == 999) ? true : false),
                autoCopyRoster:((data['autoCopyRoster'] == 999) ? true : false),
                mmBilling: ((data['mmBilling'] == 0) ? false : true),
                mmPriceUpdates: ((data['mmPriceUpdates'] == 0) ? false : true),
                mmDexUploads: ((data['mmDexUploads'] == 0) ? false : true),
                mmndia: ((data['mmndia'] == 0) ? false : true),
                mmHacc: ((data['mmHacc'] == 0) ? false : true),
                mmOtherDS: ((data['mmOtherDS'] == 0) ? false : true),
                mmAccounting: ((data['mmAccounting'] == 0) ? false : true),
                
                
            })
            
            this.inputForm.patchValue({
                mmPublishPrintRosters : ((data['mmPublishPrintRosters'] == 0) ? false : true),
                mmTimesheetProcessing : ((data['mmTimesheetProcessing'] == 0) ? false : true),
                suggestedTimesheets:((data['suggestedTimesheets'] == 999) ? true : false),
                timesheet:((data['timesheet'] == 999) ? true : false),
                accessCDC:((data['accessCDC'] ==   0 || !data['accessCDC']) ? false : true),
                hideClientPhoneInApp:((data['hideClientPhoneInApp'] == 0 || !data['hideClientPhoneInApp']) ? false : true),
                autoAdminTime:((data['autoAdminTime'] == 0 || !data['autoAdminTime']) ? false : true),
                staffBranchOperation:((data['staffBranchOperation'] == 0 || !data['staffBranchOperation']) ? false : true),
                limitProgramLookups: ((data['limitProgramLookups'] == 0 || !data['limitProgramLookups']) ? false : true),
                limitServiceLookups: ((data['limitServiceLookups'] == 0 || !data['limitServiceLookups']) ? false : true),
                limitStaffLookups: ((data['limitStaffLookups'] == 0 || !data['limitStaffLookups']) ? false : true),
                limitPayTypeLookups: ((data['limitPayTypeLookups'] == 0 || !data['limitPayTypeLookups']) ? false : true),
                payIntegrityCheck: ((data['payIntegrityCheck'] == 0 || !data['payIntegrityCheck']) ? false : true),
                branchReminderFilter: ((data['branchReminderFilter'] == 0 || !data['branchReminderFilter']) ? false : true),
                showAllProgramsOnActivate: ((data['showAllProgramsOnActivate'] == 0 || !data['showAllProgramsOnActivate']) ? false : true),
                
            })
            
            
            this.modalOpen = true;
        });
        
        this.cd.detectChanges();
    }
    
    tabFindIndex: number = 0;
    tabFindChange(index: number){
        this.tabFindIndex = index;        
    }
    tabFindIndexScope: number = 0;
    tabFindChangeScopes(index: number){
        this.tabFindIndexScope = index;
    }
    handleCancel() {
        this.resetModal();
        this.modalOpen = false;
    }
    pre(): void {
        this.current -= 1;
    }
    
    next(): void {
        this.current += 1;
    }
    
    stringtoInt(x){
        return parseInt(x);
    }
    inttoString(x){
        return x.toString();
    }
    
    generateprogramsQuery(programs: string[]): string | null {
        
        if (!Array.isArray(programs) || programs.length === 0) {
            return null; // Return null if programs array is empty or invalid
        }
    
        // Build the WHERE clause by mapping and joining the program conditions
        const whereClause = `(${programs.map(program => `RecipientPrograms.Program = '${program}'`).join(' OR ')})`;
    
        // Return the complete query with the WHERE clause
        return `INNER JOIN RecipientPrograms ON Recipients.UniqueID = RecipientPrograms.PersonID WHERE ${whereClause}`;
    }
    generateBranchQuery(branches: string[]): string | null {
        if (!Array.isArray(branches) || branches.length === 0) {
            return null; // Return null if programs array is empty or invalid
        }
        
        // Build the WHERE clause for branches
        const whereClause = `${branches.map(branch => `Recipients.Branch = '${branch}'`).join(' OR ')}`;
        return `(${whereClause})`;
    }
    generateReminderQuery(creators: string[]): string | null {
        if (!Array.isArray(creators) || creators.length === 0) {
            return null; // Return null if programs array is empty or invalid
        }
        
    
        // Build the WHERE clause for History.Creator
        const whereClause = creators.map(creator => `History.Creator = '${creator}'`).join(' OR ');
    
        // Return the query with the WHERE clause
        return `(${whereClause})`;
    }
    generateCategoriesQuery(groups: string[]): string | null {
        if (!Array.isArray(groups) || groups.length === 0) {
            return null; // Return null if programs array is empty or invalid
        }
    
        // Build the WHERE clause for Recipients.AgencyDefinedGroup
        const whereClause = groups.map(group => `Recipients.AgencyDefinedGroup = '${group}'`).join(' OR ');
    
        // Return the query with the WHERE clause
        return `(${whereClause})`;
    }
        // Function to generate query for Recipients.RECIPIENT_CoOrdinator
    generateCoordinatorQuery(coordinators: string[]): string | null {
        if (!Array.isArray(coordinators) || coordinators.length === 0) {
            return null; // Return null if programs array is empty or invalid
        }

        // Build the WHERE clause for Recipients.RECIPIENT_CoOrdinator
        const whereClause = coordinators.map(coordinator => `Recipients.RECIPIENT_CoOrdinator = '${coordinator}'`).join(' OR ');

        // Return the query with the WHERE clause
        return `(${whereClause})`;
    }
    // Function to generate query for Staff.StaffGroup
    generateStaffFilterCategories(staffGroups: string[]): string | null {

        if (!Array.isArray(staffGroups) || staffGroups.length === 0) {
            return null; // Return null if programs array is empty or invalid
        }

        // Build the WHERE clause for Staff.StaffGroup
        const whereClause = staffGroups.map(group => `Staff.StaffGroup = '${group}'`).join(' OR ');

        // Return the query with the WHERE clause
        return `(${whereClause})`;
    }
    save() {
        this.loading = true;
        
        if(this.inputForm.get('name').value =='' || this.inputForm.get('password').value =='' || this.inputForm.get('staffCode').value ==''){
            this.globalS.iToast('Alert', 'Name Password and ALL Security Profiles must be completed Before You can Save !');
            return false
        }

        this.inputForm.controls["recipients"].setValue(999);
        this.inputForm.controls["timesheet"].setValue(999);
        
        this.inputForm.controls["dayManager"].setValue(999);
        this.inputForm.controls["staff"].setValue(999);
        this.inputForm.controls["roster"].setValue(999);
        this.inputForm.controls["invoiceEnquiry"].setValue(0);
        this.inputForm.controls["timesheetUpdate"].setValue(0);


        this.inputForm.controls["viewAllBranches"].setValue(this.allBranches);
        this.inputForm.controls["viewFilterBranches"].setValue(this.generateBranchQuery(this.selectedbranches));

        this.inputForm.controls["viewAllProgram"].setValue(this.allProgarms);
        this.inputForm.controls["viewFilter"].setValue(this.generateprogramsQuery(this.selectedPrograms));
        
        this.inputForm.controls["viewAllReminders"].setValue(this.allReminders);
        this.inputForm.controls["viewFilterReminders"].setValue(this.generateReminderQuery(this.selectedReminders));

        this.inputForm.controls["viewAllCategories"].setValue(this.allcat);
        this.inputForm.controls["viewFilterCategory"].setValue(this.generateCategoriesQuery(this.selectedCategories));
        
        this.inputForm.controls["viewAllCoOrdinators"].setValue(this.allCordinatore);
        this.inputForm.controls["viewFilterCoOrd"].setValue(this.generateCoordinatorQuery(this.selectedCordinators));
        
        this.inputForm.controls["viewAllStaffCategories"].setValue(this.allStaffCat);
        this.inputForm.controls["viewFilterStaffCategory"].setValue(this.generateStaffFilterCategories(this.selectedStaffCategories));



        this.inputForm.controls["limitProgramLookups"].setValue(false);
        this.inputForm.controls["limitServiceLookups"].setValue(false);
        this.inputForm.controls["limitStaffLookups"].setValue(false);
        this.inputForm.controls["limitPayTypeLookups"].setValue(false);
        
        this.inputForm.controls["StaffBranchOperation"].setValue(true);
        


        
        this.inputForm.controls["BranchReminderFilter"].setValue(true);
        this.inputForm.controls["RecipientRecordView"].setValue(this.recipientRecordView);
        this.inputForm.controls["StaffRecordView"].setValue(this.checkBoxString);
        this.inputForm.controls["showAllProgramsOnActivate"].setValue(true);
        
        
        
        this.inputForm.controls["enableViewNoteCases"].setValue(this.enableViewNoteCases);
        this.inputForm.controls["clientPortalViewNotes"].setValue(this.clientPortalViewNotes);
        
        this.inputForm.controls["payIntegrityCheck"].setValue(false);
        this.inputForm.controls["autoAdminTime"].setValue(false);        
        // Menu form items cast to int
        var allowProgramTransition = this.inputForm.get('allowProgramTransition').value;
        this.inputForm.controls["allowProgramTransition"].setValue(((allowProgramTransition == false) ? 0 : 1));

        var mmPublishPrintRosters = this.inputForm.get('mmPublishPrintRosters').value;
        this.inputForm.controls["mmPublishPrintRosters"].setValue(((mmPublishPrintRosters == false) ? 0 : 1));

        var mmTimesheetProcessing = this.inputForm.get('mmTimesheetProcessing').value;
        this.inputForm.controls["mmTimesheetProcessing"].setValue(((mmTimesheetProcessing == false) ? 0 : 1));
        
        var mmBilling = this.inputForm.get('mmBilling').value;
        this.inputForm.controls["mmBilling"].setValue(((mmBilling == false) ? 0 : -1));
        
        var mmPriceUpdates = this.inputForm.get('mmPriceUpdates').value;
        this.inputForm.controls["mmPriceUpdates"].setValue(((mmPriceUpdates == false) ? 0 : 1));
        
        var mmDexUploads = this.inputForm.get('mmDexUploads').value;
        this.inputForm.controls["mmDexUploads"].setValue(((mmDexUploads == false) ? 0 : 1));
        
        var mmndia = this.inputForm.get('mmndia').value;
        this.inputForm.controls["mmndia"].setValue(((mmndia == false) ? 0 : 1));
        
        var mmHacc = this.inputForm.get('mmHacc').value;
        this.inputForm.controls["mmHacc"].setValue(((mmHacc == false) ? 0 : 1));
        
        var mmOtherDS = this.inputForm.get('mmOtherDS').value;
        this.inputForm.controls["mmOtherDS"].setValue(((mmOtherDS == false) ? 0 : 1));
        
        var mmAccounting = this.inputForm.get('mmAccounting').value;
        this.inputForm.controls["mmAccounting"].setValue(((mmAccounting == false) ? 0 : 1));
        
        var hideClientPhone = this.inputForm.get('hideClientPhone').value;
        this.inputForm.controls["hideClientPhone"].setValue(((hideClientPhone == false) ? 0 : 1));
    
        var manualRosterCopy = this.inputForm.get('manualRosterCopy').value;
        this.inputForm.controls["manualRosterCopy"].setValue(((manualRosterCopy == false) ? 0 : 999));

        var autoCopyRoster = this.inputForm.get('autoCopyRoster').value;
        this.inputForm.controls["autoCopyRoster"].setValue(((autoCopyRoster == false) ? 0 : 999));

        var timesheet = this.inputForm.get('timesheet').value;
        this.inputForm.controls["timesheet"].setValue(((timesheet == false) ? 0 : 999));
        
        var suggestedTimesheets = this.inputForm.get('suggestedTimesheets').value;
        this.inputForm.controls["suggestedTimesheets"].setValue(((suggestedTimesheets == false) ? 0 : 999));


        var system = this.inputForm.get('system').value;
        this.inputForm.controls["system"].setValue((system == false) ? 0 : 999);

        var statistics = this.inputForm.get('statistics').value;
        this.inputForm.controls["statistics"].setValue((statistics == false) ? 0 : 999);

        var financial = this.inputForm.get('financial').value;
        this.inputForm.controls["financial"].setValue((financial == false) ? 0 : 999);
        

        if(!this.isUpdate){
            this.menuS.postuserDetails(this.inputForm.value)
            .subscribe(insertedid => {
                console.log("inserted id"+insertedid);
                this.globalS.sToast('Success', 'Added Succesfully');
                this.loading = false;
                this.handleCancel();
                this.loadData()
            });
        }else{
             
            this.timeS.updateuserDetails(this.inputForm.value)
            .subscribe(data => {
                this.globalS.sToast('Success', 'updated Succesfully');
                this.loading = false;
                this.handleCancel();
                this.loadData()
            });


        }
    }

    buildForm() {
        this.inputForm = this.formBuilder.group({
            recnum:0,
            name: '',
            password: '',
            recipients:['0'],
            timesheet:['0'],
            statistics:false,
            financial:false,
            system:false,
            dayManager:false,
            staff:false,
            roster:false,
            suggestedTimesheets:false,
            invoiceEnquiry:false,
            manualRosterCopy:false,
            autoCopyRoster:false,
            timesheetUpdate:0,
            
            viewAllBranches:false,
            viewFilterBranches:this.selectedbranches,
            viewAllProgram:false,
            viewFilter:this.selectedPrograms,
            viewAllCategories:false,
            viewFilterCategory:this.selectedCategories,
            viewAllCoOrdinators:false,
            viewFilterCoOrd:this.selectedCordinators,
            viewAllReminders:false,
            viewFilterReminders:this.selectedReminders,
            viewAllStaffCategories:false,
            viewFilterStaffCategory:this.selectedStaffCategories,


            staffCode:'',
            recipientDocFolder:null,
            force_RecipDocFolder:false,
            oniImportExportFolder:null,
            force_ONIImportFolder:false,
            oniArchiveFolder:null,
            force_ONIArchiveFolder:false,
            staffDocFolder:null,
            force_StaffDocFolder:false,
            staffRostersFolder:null,
            force_StaffRosterFolder:false,
            reportExportFolder:null,
            force_ReportExportFolder:false,
            reportSavesFolder:null,
            force_ReportSavesFolder:false,
            haccmdsFolder:null,
            force_HACCMDSFolder:false,
            cstdamdsFolder:null,
            force_CSTDAMDSFolder:false,
            nrcpmdsFolder:null,
            force_NRCPMDSFolder:false,
            payExportFolder:null,
            force_PayExportFolder:false,
            billingExportFolder:null,
            force_BillingExportFolder:false,
            ownRosterOnly:true,
            approvedaymanager:false,
            loginMode:['', [Validators.required]],
            BranchReminderFilter:false,
            allowStaffSwap:false,
            keepOriginalAsImport:true,
            RecipientRecordView:'',
            StaffRecordView:'',
            showAllProgramsOnActivate:true,
            
            coordFilterType:'',
            tmMode:'',
            mobileFutureLimit:0,
            homeBranch:['', [Validators.required]],
            defAlertsTAB:'CONTACT ISSUES',
            taServer:false,
            emailUserReports:true,
            usertype:['', [Validators.required]],
            accessCDC:false,
            enableViewNoteCases:this.enableViewNoteCases,// its a string of 00000 contains 3 tickbox values
            allowProgramTransition:false,
            UseCloudStaff:false,
            CloudStaffLink:null,
            UseCloudReports:false,
            CloudReportsLink:null,
            UseCloudTimesheets:false,
            CloudTimesheetsLink:null,
            UseCloudRosters:false,
            CloudRostersLink:null,
            UseCloudDaymanager:false,
            CloudDaymanagerLink:null,
            UseCloudRecipients:false,
            CloudRecipientsLink:null,
            lowChangeActivityCode:true,
            payIntegrityCheck:false,
            canEditNDIA         :true,
            awardEnforcement:'PREVENT',
            systemEmailer:false,//new
            azureaccount:"",//new
            sso:false,//new
            clientPortalViewNotes:'',
            UseRecv2:false,
            // all below options not aded in profiler sql
            hiresGraphics:true,
            recipientCurrentDefault:true,
            staffCurrentDefault:true,
            limitProgramLookups:false,
            limitServiceLookups:false,
            limitStaffLookups:false,
            limitPayTypeLookups:false,
            reportPreview:false,
            StaffBranchOperation:true,
            autoAdminTime:false,
            useQikShift:false,
            // all above options not aded in profiler sql
            
            fullAdmin:false,//new
            changeMasterRoster:true,
            allowRosterReallocate:true,
            allowMasterSaveAs:true,
            canRosterOvertime:true,
            canRosterBreakless:true,
            canRosterConflicts:true,
            editRosterRecord:true,
            allowTypeAhead:false,
            // title:false,
            // EndDate:null,
            canMoveImportedDocuments:true,
            
            useDMv2:false,
            recipmgtview:false,
            adminChangeOutputType:false,
            adminChangeProgram:false,
            adminChangeActivityCode:false,
            adminChangePaytype:false,
            adminChangeDebtor:false,
            adminChangeBillAmount:false,
            adminChangeBillQty:false,
            attendeesChangeBillQty:false,
            attendeesChangeBillAmount:false,
            adminChangePayQty:false,
            
            title:false,
            allowTravelEntry:false,
            allowLeaveEntry:false,
            allowIncidentEntry:false,
            allowPicUpload:false,
            enableRosterAvailability:false,
            allowViewBookings:false,
            acceptBookings:false,
            viewClientDocuments:false,
            viewClientCareplans:false,
            allowViewGoalPlans:false,
            allowTravelClaimWithoutNote:false,
            allowMTASaveUserPass:false,
            
            allowOPNote:false,
            allowCaseNote:false,
            allowClinicalNoteEntry:false,
            allowRosterNoteEntry:false,
            suppressEmailOnRosterNote:true,
            enableEmailNotification:false,
            useOPNoteAsShiftReport:false,
            useServiceNoteAsShiftReport:false,
            
            
            shiftReportReminder:false,
            userSessionLimit:0,
            
            
            mtaAutRefreshOnLogin:false,
            
            hideClientPhoneInApp:false,
            
            hideClientPhone:false,

            hideAddress:false,
            
            allowSetTime:false,
            
            allowAddAttendee:false,
            
            multishiftAdminAndMultiple:false,
            
            restrictTravelSameDay:false,
            
            pushPhonePrefix:false,
            
            phonePrefix:'',
            
            enable_Shift_End_Alarm:false,
            
            enable_Shift_Start_Alarm:false,
            
            checkAlertInterval:0,
            
            allowsMarketing:false,
            viewPackageStatement:false,
            canManagePreferences:false,
            allowBooking:false,
            canCreateBooking:false,
            bookingLeadTime:0,
            canChooseProvider:false,
            showProviderPhoto:false,
            canSeeProviderPhoto:false,
            canSeeProviderGender:false,
            canSeeProviderAge:false,
            canSeeProviderReviews:false,
            canEditProviderReviews:false,
            hideProviderName:false,
            canManageServices:false,
            canCancelService:false,
            CanQueryService:false,

            accessTA:false,
            timelogging:'',
            disableBtnReferral  : false,
            disableBtnReferOn   : false,
            disableBtnNotProceeding : false,
            disableBtnAssess    : false,
            disableBtnAdmit     : false,
            disableBtnUnWait    : false,
            disableBtnDischarge :false,
            disableBtnSuspend   :false,
            disableBtnReinstate :false,
            disableBtnDecease   :false,
            disableBtnAdmin     :false,
            disableBtnItem      :false,
            disableBtnPrint     :false,
            disableBtnRoster    :false,
            disableBtnMaster    :false,
            disableBtnOnHold    :false,
            addNewRecipient     :false,    
            canChangeClientCode :true,
            
            mmPublishPrintRosters:false,//cast to int
            mmTimesheetProcessing:false,//cast to int
            sortTimesheetByDateTime:false,
            timesheetPreviousPeriod:false,
            mmBilling:false,//cast to int
            mmPriceUpdates:false,//cast to int
            mmDexUploads:false,//cast to int
            mmndia:false,//cast to int
            mmHacc:false,//cast to int
            mmOtherDS:false,//cast to int
            mmAccounting:false,//cast to int
            mmAnalyseBudget:false,
            mmAtAGlance:false,
            
        });

        this.mainMenuForm     = this.formBuilder.group({
            recnum:0,
            mmPublishPrintRosters:false,//cast to int
            mmTimesheetProcessing:false,//cast to int
            mmBilling:false,//cast to int
            mmPriceUpdates:false,//cast to int
            mmDexUploads:false,//cast to int
            mmndia:false,//cast to int
            mmHacc:false,//cast to int
            mmOtherDS:false,//cast to int
            mmAccounting:false,//cast to int
            mmAnalyseBudget:false,
            mmAtAGlance:false,
            UseCloudStaff:false,
            UseCloudRecipients:false,
            UseCloudRosters:false,
            UseCloudDaymanager:false,
            UseCloudTimesheets:false,
            UseCloudReports:false,
        })
        this.staffForm      = this.formBuilder.group({
            recnum:0,
            staffrecordview :null,
        })
    }
    
    handleOkTop() {
        this.generatePdf();
        this.tryDoctype = ""
        this.pdfTitle = ""
    }
    handleCancelTop(): void {
        this.drawerVisible = false;
        this.pdfTitle = ""
    }
    generatePdf(){
        this.drawerVisible = true;
        this.loading = true;
        var fQuery = "SELECT ROW_NUMBER() OVER(ORDER BY Description) AS Field1,Description as Field2 ,DeletedRecord as is_deleted,CONVERT(varchar, [enddate],105) as Field3 from DataDomains "+this.whereString+" Domain='CDCTARGETGROUPS'";
        
        const data = {
            "template": { "_id": "0RYYxAkMCftBE9jc" },
            "options": {
                "reports": { "save": false },
                "txtTitle": "CDC Target Group List",
                "sql": fQuery,
                "userid":this.tocken.user,
                "head1" : "Sr#",
                "head2" : "Name",
                "head3" : "End Date"
            }
        }
        
        this.printS.printControl(data).subscribe((blob: any) => {
            let _blob: Blob = blob;
            let fileURL = URL.createObjectURL(_blob);
            this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
            this.loading = false;
             this.pdfTitle = "CDC Target Groups.pdf";
        }, err => {
            this.loading = false;
            this.ModalS.error({
                nzTitle: 'TRACCS',
                nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                nzOnOk: () => {
                    this.drawerVisible = false;
                },
            });
        });
        
        this.loading = true;
        this.tryDoctype = "";
        this.pdfTitle = "";
    }
    
    
    changeCheckbox(index: number, val: boolean){
        let temp = Object.assign([], this.recipientRecordView);
        temp.splice(index,1, val ? '1' : '0')
        this.recipientRecordView = temp.join('')
        
    }
    staffchangeCheckbox(index: number, val: boolean){
        let temp = Object.assign([], this.checkBoxString);
        temp.splice(index,1, val ? '1' : '0')
        
        this.checkBoxString = temp.join('')
        console.log(this.checkBoxString);
    }
    enableNoteCases(index:number,val:boolean){
        let temp = Object.assign([], this.enableViewNoteCases);
        temp.splice(index,1, val ? '1' : '0')
        this.enableViewNoteCases = temp.join('')
        console.log(this.enableViewNoteCases)
    }
    enableclientPortalViewNotes(index:number,val:boolean){
        let temp = Object.assign([], this.clientPortalViewNotes);
        temp.splice(index,1, val ? '1' : '0')
        this.clientPortalViewNotes = temp.join('')
        console.log(this.clientPortalViewNotes)
    }
    goBack(): void {
        const previousUrl = this.navigationService.getPreviousUrl();
        const previousTabIndex = this.navigationService.getPreviousTabIndex();
      
        if (previousUrl) {
          this.router.navigate(['/admin/configuration'], {
            queryParams: { tab: previousTabIndex } // Pass tab index in query params
          });
        } else {
          this.router.navigate(['/admin/configuration'], {
            queryParams: { tab: previousTabIndex } // Pass tab index in query params
          });
        }
        
      }
      @HostListener('document:keydown', ['$event'])
      handleKeyboardEvent(event: KeyboardEvent) {
        // Handle character input
        if (event.key.length === 1 && /^[a-zA-Z]$/.test(event.key)) {
            this.searchTerm += event.key.toLowerCase();
            this.scrollToRow();
        } 
        // Handle backspace
        else if (event.key === 'Backspace') {
            // Only reset if there's something to remove
            this.searchTerm = '';
            if (this.searchTerm.length > 0) {
                this.searchTerm = this.searchTerm.slice(0, -1);
                this.scrollToRow();
            }
        }
        // Handle other keys (optional)
        else if (event.key === 'Escape') {
            // Reset search term on escape key
            this.searchTerm = '';
            this.scrollToRow();
        }
      }
  
      scrollToRow() {
          // Find the index of the first matching item
          const matchIndex = this.tableData.findIndex(item => 
              item.name.toLowerCase().startsWith(this.searchTerm)
          );
          // Scroll to the matching row if it exists
          if (matchIndex !== -1) {
              const tableRow = document.querySelectorAll('tr')[matchIndex + 1]; // +1 to skip header
              if (tableRow) {
                  tableRow.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
                  this.highlightRow(tableRow);
              }
          } else {
          }
      }
  
      highlightRow(row: HTMLElement) {
          row.classList.add('highlight');
          setTimeout(() => row.classList.remove('highlight'), 2000);
      }   
}
