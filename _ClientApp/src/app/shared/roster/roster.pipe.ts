import { Pipe, PipeTransform } from '@angular/core';

import * as format from "date-fns/format";


@Pipe({ name: 'rosterdmz' })
export class RosterPipe implements PipeTransform {
    transform(value: any, dayPop: any) { 
            let filtered: Array<any> = []; 
            if(value == null)
                return filtered;

            let total:number = 0;
            filtered = value.res.filter(x => { 
                if(x.roster_Date == dayPop.date)
                    return x;         
            }); 
            return filtered;        
    }
}