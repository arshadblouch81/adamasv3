import { Component, OnInit, OnDestroy, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core'

import { GlobalService, ListService, TimeSheetService, ShareService, leaveTypes, ClientService, recurringInt, recurringStr, PrintService } from '@services/index';
import { Router, NavigationEnd } from '@angular/router';
import { forkJoin, Subscription, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroup, Validators, FormBuilder, NG_VALUE_ACCESSOR, ControlValueAccessor, FormArray } from '@angular/forms';
import { Reminders } from '@modules/modules';
import { DomSanitizer } from '@angular/platform-browser';
import { NzModalService } from 'ng-zorro-antd/modal';
import { recurringValidator } from '../../../validators/index';
import format from 'date-fns/format';
import {CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import * as groupArray from 'group-array';


@Component({
    styles: [`
    nz-select{
        width:100%;
    }
    .spinner{
        margin:1rem auto;
        width:1px;
    }
    .selected{
        background-color: #85B9D5;
        color: white;
    }
    nz-table th{
    font-weight: 600;
    font-family: 'Segoe UI';
    font-size: 14px;
    border: 1px solid #f3f3f3;
}
    `],
    templateUrl: './suspensions.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})


export class RecipientSuspensionAdmin implements OnInit, OnDestroy {
    private unsubscribe: Subject<void> = new Subject();
    user: any;
    inputForm: FormGroup;
    tableData: Array<any>;
    
    checked: boolean = false;
    isDisabled: boolean = false;
    loading: boolean = false;
    isLoading: boolean = false;
    
    modalOpen: boolean = false;
    addOREdit: number;
    dateFormat: string = 'dd/MM/yyyy';
    dayInt = recurringInt;
    dayStr = recurringStr;
    
    private default: any = {
        recordNumber: '',
        personID: '',
        listOrder: '',
        followUpEmail: '',
        recurring: false,
        recurrInt: null,
        recurrStr: null,
        notes: '',
        reminderDate: null,
        dueDate: null,
        staffAlert: null
    }
   
    tocken: any;
    pdfTitle: string;
    tryDoctype: any;
    drawerVisible: boolean =  false;
    alist: Array<any> = [];
    lstPrograms: Array<any> = [];
    lstActivities: Array<any> = [];
    lstPaytypes: Array<any> = [];

    users:Array<any> = [];
    categories:Array<any> = [];
    cancelReason:Array<any> = ['','NSDF : No show due to family issues.'];
    cancelcodes:Array<any> = [];
    caseManagers:Array<any> = [];
    selectedRow  : number = 0;
    activeRowData:any;
    today = new Date();
    
    constructor(
        private timeS: TimeSheetService,
        private listS: ListService,
        private sharedS: ShareService,
        private clientS: ClientService,
        private router: Router,
        private globalS: GlobalService,
        private printS :PrintService,
        private formBuilder: FormBuilder,
        private modalService: NzModalService,
        private sanitizer: DomSanitizer,
        private cd: ChangeDetectorRef
        ) {
            this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (data instanceof NavigationEnd) {
                    if (!this.sharedS.getPicked()) {
                        this.router.navigate(['/admin/recipient/personal'])
                    }
                }
            });
            
            this.sharedS.changeEmitted$.pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                if (this.globalS.isCurrentRoute(this.router, 'suspensions')) {
                    this.user = data;
                    this.search(data);
                }
            });
        }
        
        ngOnInit(): void {
            this.tocken = this.globalS.pickedMember ? this.globalS.GETPICKEDMEMBERDATA(this.globalS.GETPICKEDMEMBERDATA):this.globalS.decode();
            this.user = this.sharedS.getPicked();
            this.search(this.user);
             this.buildForm();
            
        }
        selectedItemGroup(data:any, i:number){
            this.selectedRow=i;
            this.activeRowData=data;
        }
        ngOnDestroy(): void {
            this.unsubscribe.next();
            this.unsubscribe.complete();
        }
        
        listDropDowns() {
            
            let sql=`Select distinct '[' + CASE WHEN ([ServiceProgram] <> '') AND ([ServiceProgram] IS NOT Null) THEN [ServiceProgram] ELSE '?' END + '] ~> ' + [Service Type] As Details, [Service Type] as ServiceType,0 as selected   
            FROM ServiceOverview INNER JOIN RecipientPrograms ON ServiceOverview.PersonID = RecipientPrograms.PersonID 
            WHERE ServiceOverview.PersonID = '${this.user.id}' AND ProgramStatus = 'ACTIVE' AND ServiceStatus = 'ACTIVE'`;
   
            
            let sql2=` SELECT Title FROM ItemTypes WHERE (EndDate Is Null OR EndDate >= '${format(this.today,'yyyy/MM/dd')}') AND RosterGroup = 'RECPTABSENCE'  AND Status = 'ATTRIBUTABLE'  AND ProcessClassification = 'EVENT' ORDER BY Title`;
           

            let sql3=`SELECT DISTINCT UPPER([Name]) as name FROM USERINFO`;

            let sql4=` SELECT DISTINCT UPPER([Description]) as description FROM DATADOMAINS WHERE DOMAIN = 'CASENOTEGROUPS'`;
             
            let sql5=` SELECT DISTINCT [NAME] FROM HUMANRESOURCETYPES WHERE [GROUP]= 'PROGRAMS' AND ISNULL(ENDDATE,'2030/01/01') >  GETDATE() `;

            let sql6=` SELECT DISTINCT TITLE FROM ITEMTYPES WHERE PROCESSCLASSIFICATION IN ('OUTPUT','ITEM','EVENT') AND ISNULL(ENDDATE,'2030/01/01') >  GETDATE()  `;
            
            let sql7=` SELECT DISTINCT TITLE FROM ITEMTYPES WHERE PROCESSCLASSIFICATION = 'INPUT' AND ISNULL(ENDDATE,'2030/01/01') >  GETDATE()  `;



            forkJoin([
                this.listS.getlist(sql),
                this.listS.getlist(sql2),
                this.listS.getlist(sql3),
                this.listS.getlist(sql4)
            ]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                this.alist = data[0].map(x=>({details:x.details, serviceType : x.serviceType, selected : true}));
                this.cancelcodes = data[1];
                this.users= data[2].map(x=>x.name);
                this.categories= data[3].map(x=>x.description);
                this.cd.detectChanges();
                
            });

            if (this.addOREdit==2){
                setTimeout(() => {
                    forkJoin([
                        this.listS.getlist(sql5),
                        this.listS.getlist(sql6),
                        this.listS.getlist(sql7)               
                    ]).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
                        this.lstPrograms = data[0].map(x=>x.name);
                        this.lstActivities = data[1];
                        this.lstPaytypes= data[2];
                    
                        this.cd.detectChanges();
                        
                    });
           },1000)

            }
          }

        search(user: any = this.user) {
            this.loading = true;
            console.log(this.user)
            
            this.timeS.getsuspensions(user.id).subscribe(data => {
                this.tableData = data;
                this.originalTableData=data;
                this.loading = false;
                this.selectedRow = 0;
                this.cd.markForCheck();
            });
        }
       
        buildForm() {
            this.inputForm = this.formBuilder.group({
                recordNumber: '',
                personID: '',
                startDate:this.today,
                endDate:this.today,
                putMasterOnHold: false,
                recordFormalAbsence:false,
                record24Absence:false,              
                allServices: true,
                cancellation: 'NSDF : No show due to family issues.',
                cancellationcode: null,
                notes: '',
                noteType :'',
                category:'',
                reminder:'',
                reminderDate: null,
                dueDate: null,
                selectedServices: '',
                email: ['', [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
                user:'',
                winUser:'',
                altProgram:'',
                activity:'',
                billOriginalRate:false,
                retainOriginalRate:false,
                payType:''
            
            });
            
            // this.inputForm.controls['recurrStr'].disable();
            // this.inputForm.controls['recurrInt'].disable();
            
            // this.inputForm.controls['sameDay'].disable();
            // this.inputForm.controls['sameDate'].disable();
            
            // this.inputForm.get('recurring').valueChanges.subscribe(data => {
            //     if (!data) {
            //         this.inputForm.controls['recurrInt'].setValue(null)
            //         this.inputForm.controls['recurrStr'].setValue(null)
            //         this.inputForm.controls['recurrStr'].disable()
            //         this.inputForm.controls['recurrInt'].disable()
            //     } else {
            //         this.inputForm.controls['recurrStr'].enable()
            //         this.inputForm.controls['recurrInt'].enable()
            //     }
            // });
            // this.inputForm.get('recurrStr').valueChanges.subscribe(data => {
            //     if (data == 'Month/s') {
            //         this.inputForm.controls['sameDay'].enable();
            //         this.inputForm.controls['sameDate'].enable();
            //     } else {
            //         this.inputForm.controls['sameDay'].setValue(false);
            //         this.inputForm.controls['sameDate'].setValue(false);
            //         this.inputForm.controls['sameDay'].disable();
            //         this.inputForm.controls['sameDate'].disable();
            //     }
            // });
        }
        
        onKeyPress(data: KeyboardEvent) {
            return this.globalS.acceptOnlyNumeric(data);
        }
        
        showEditModal(data: any) {
            this.addOREdit=2;
          this.activeRowData=data;
            this.listDropDowns();
            const { recordNumber, personID, name, address1, address2, email, date1, date2, state, notes, recurring,sameDate,sameDay } = this.tableData[this.selectedRow];
            
            this.inputForm.patchValue({
                recordNumber: data.recordNumber,
                personID: data.personID,
                startDate:  new Date(data.date1),
                endDate: new Date(data.date2),
                putMasterOnHold: false,
                recordFormalAbsence:false,
                record24Absence:false,              
                allServices: true,
                cancellation: data.address1,
                cancellationcode: data.name,
                notes: data.notes,
                noteType : data.serialNumber,
                category: data.lockBoxCode,
                reminder: data.lockBoxLocation,
                reminderDate: new Date(data.dateInstalled),
                dueDate: null,
                selectedServices: '',
                email: [''],
                user:'',
                winUser:'',
                altProgram: data.equipmentCode,
                activity: data.email,
                billOriginalRate:false,
                retainOriginalRate:false,
                payType: data.suburb
            });
            
            this.addOREdit = 2;
            this.modalOpen = true;
        }
        
        save() {
            
            if (!this.globalS.IsFormValid(this.inputForm))
            return;
            
            const input = this.inputForm.value;
            
            input.personID = this.user.id;
            input.user= this.tocken.user;
            input.winUser = this.tocken.user;
            input.startDate = format(new Date(input.startDate),'yyyy/MM/dd');
            input.endDate = format(new Date(input.endDate),'yyyy/MM/dd') ;
            input.reminderDate = format(new Date(input.reminderDate),'yyyy/MM/dd') ;
            
            let list = this.alist.filter(x=>x.selected)
            list=list.map(x=>x.serviceType)
            let str=  list.join(",") 
            input.selectedServices =str;

            if(this.addOREdit == 1){
                this.timeS.postsuspension(input)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data added');
                    this.search();
                    this.handleCancel();
                });
            }
            console.log(this.addOREdit + "addOREdit")
            if (this.addOREdit == 2) {               

                this.timeS.updatesuspension(input)
                .subscribe(data => {
                    this.globalS.sToast('Success', 'Data updated');
                    this.search();
                    this.handleCancel();
                })
            }
            
           
        }
        
        canDeactivate() {
            if (this.inputForm && this.inputForm.dirty) {
                this.modalService.confirm({
                    nzTitle: 'Save changes before exiting?',
                    nzContent: '',
                    nzOkText: 'Yes',
                    nzOnOk: () => {
                        this.save();
                    },
                    nzCancelText: 'No',
                    nzOnCancel: () => {
                        
                    }
                });
            }
            return true;
        }

    originalTableData: Array<any>;
    dragOrigin: Array<string> = [];
    
    columnDictionary = [
        {
            key: 'Leave Type',
            value: 'name'
        },
        {
        key: 'Start',
        value: 'date1'
    },{
        key: 'End',
        value: 'date2'
    },{
        key: 'Notes',
        value: 'notes'
    },{
        key: 'Reminder Date',
        value: 'dateInstalled'
    },{
        key: 'Approved',
        value: 'recurring'
    }];
    
    
    

    dragDestination = [   
        'Leave Type',    
        'Start',
        'Name',
        'End',
        'Notes',
        'Reminder Date',
        'Approved'
    ];

   

    searchColumnDictionary(data: Array<any>, tobeSearched: string){
        let index = data.findIndex(x => x.key == tobeSearched);        
        return data[index].value;
    }
    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);            
        } else {
            if(!event.container.data.includes(event.item.data)){
                copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.container.data.length)
            }
        }
        this.generate();
    }

    generate(){
        const dragColumns = this.dragOrigin.map(x => this.searchColumnDictionary(this.columnDictionary, x));
        console.log(dragColumns)

        var convertedObj = groupArray(this.originalTableData, dragColumns);

        console.log(convertedObj)
        var flatten = this.flatten(convertedObj, [], 0);

        if(dragColumns.length == 0){
            this.tableData = this.originalTableData;
        } else {
            this.tableData = flatten;
        }
    }
    flatten(obj: any, res: Array<any> = [], counter = null){
        for (const key of Object.keys(obj)) {
            const propName = key;
            if(typeof propName == 'string'){                   
                res.push({key: propName, counter: counter});
                counter++;
            }
            if (!Array.isArray(obj[key])) {
                this.flatten(obj[key], res, counter);
                counter--;
            } else {
                res.push(obj[key]);
                counter--;
            }
        }
        return res;
    }
        removeTodo(data: any){
            this.dragOrigin.splice(this.dragOrigin.indexOf(data),1);
            this.generate();
        }
        isArray(data: any){
            return Array.isArray(data);
        }
     
        trackByFn(index, item) {
            return item.id;
        }
        isSome(data: any){
            if(data){
                return data.some(d => 'key' in d);
            }
            return true;        
        }
        showAddModal() {
           
            this.addOREdit = 1;
            this.modalOpen = true;
            this.listDropDowns();
            this.inputForm.patchValue({
                allServices:true
         } )
        }
        
        loadAll(){
            this.listDropDowns();
        }
        selectAllServices(): void {
          
            if (this.inputForm.value.allServices) {

                this.alist.forEach(item => {
                    item.selected = true;
                  });

             
               
                
            }else{
                this.alist.forEach(item => {
                    item.selected = false;
                  });

            }

           
          }
        
        delete(index: number) {
            let recordNumber = this.activeRowData.recordNumber;
            
            this.timeS.deletesuspension(recordNumber).subscribe(data => {
                this.globalS.sToast('Success', 'Data deleted');
                this.search();
                this.handleCancel();
            });
        }
        close(index: number){

        }
        handleCancel() {
            this.modalOpen = false;
            this.isLoading = false;
            this.inputForm.reset(this.default);
        }
        handleOkTop() {
            this.generatePdf();
            this.tryDoctype = ""
            this.pdfTitle = ""
        }
        generatePdf(){
            
            this.drawerVisible = true;
            
            this.loading = true;
            
            var fQuery = "Select Name As Field1, CONVERT(varchar, [Date1],105) As Field2, CONVERT(varchar, [Date2],105) as Field3,Notes as Field4 From HumanResources  HR INNER JOIN Staff ST ON ST.[UniqueID] = HR.[PersonID] WHERE ST.[AccountNo] = '"+this.user.code+"' AND HR.DeletedRecord = 0 AND [Group] = 'STAFFALERT' ORDER BY  RecordNumber DESC";
            
            const data = {
                "template": { "_id": "0RYYxAkMCftBE9jc" },
                "options": {
                    "reports": { "save": false },
                    "txtTitle": "Staff Suspensions List",
                    "sql": fQuery,
                    "userid":this.tocken.user,
                    "head1" : "Alert",
                    "head2" : "Reminder Date",
                    "head3" : "Expiry Date",
                    "head4" : "Notes",
                }
            }
            this.printS.printControl(data).subscribe((blob: any) => { 
                this.pdfTitle = "Staff Suspensions.pdf"
                let _blob: Blob = blob;
                let fileURL = URL.createObjectURL(_blob);
                this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
                this.loading = false;
                this.cd.detectChanges();    
            }, err => {
                this.loading = false;
                this.cd.detectChanges();
                this.modalService.error({
                    nzTitle: 'TRACCS',
                    nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                    nzOnOk: () => {
                        this.drawerVisible = false;
                    },
                });
            });
            
            this.cd.detectChanges();
            this.loading = true;
            this.tryDoctype = "";
            this.pdfTitle = "";
        }
//Mufeed 9 April 2024
handleCancelTop(){
    this.drawerVisible = false;
    this.pdfTitle = "";
    this.tryDoctype="";
  }
  
  InitiatePrint(){

    var lblcriteria,fQuery,fQuery_Recipient,txtTitle,Rptid;
    
    fQuery = " select PersonID,Name,Type, [Group], Address1, Address2 from HumanResources hr where PersonID = '"+this.user.id+"' AND Type = 'RECIPLEAVE'   ";
    //fQuery_Recipient =" SELECT AccountNo from Recipients where UniqueID = '"+this.user.id+"' "
    txtTitle = "Shift Report ";       
    Rptid =   "8WTerq14eNFSAnz0"  

    //console.log(fQuery)
      const data = {
                  
          "template": { "_id": Rptid },                                
          "options": {
              "reports": { "save": false },                
              "sql": fQuery,
              "Criteria": lblcriteria,
              "userid": this.tocken.user,
              "txtTitle": txtTitle,
              "Extra": this.user.code,
                                                              
          }
      }
      this.loading = true;
      this.drawerVisible = true;         
          this.printS.printControl(data).subscribe((blob: any) => {
          this.pdfTitle = txtTitle+".pdf"
              this.drawerVisible = true;                   
              let _blob: Blob = blob;
              let fileURL = URL.createObjectURL(_blob);
              this.tryDoctype = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
              this.loading = false;
              this.cd.detectChanges();
          }, err => {
              console.log(err);
              this.loading = false;
              this.modalService.error({
                  nzTitle: 'TRACCS',
                  nzContent: 'The report has encountered the error and needs to close (' + err.code + ')',
                  nzOnOk: () => {
                      this.drawerVisible = false;
                  },
              });
          });
          return;      
  }
        
   
   
   
    }