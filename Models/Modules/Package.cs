
namespace Adamas.Models.Modules{
    public class Package {        
        public string StatementId { get; set; }
        public string ServiceType { get; set; }
        public string BillUnit { get; set; }
        public string BillText { get; set; }
        public string Date { get; set; }
        public decimal BillQuant { get; set; }
        public decimal UnitBillRate { get; set; }
        public decimal ServiceCharge { get; set; }
        public string ClientCode { get; set; }
        public string ItemGroup { get; set; }
    }
}